<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032625308</ID>
<ANCIEN_ID>JG_L_2016_06_000000395310</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/62/53/CETATEXT000032625308.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème - 2ème chambres réunies, 01/06/2016, 395310, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-06-01</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>395310</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème - 2ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MASSE-DESSEN, THOUVENIN, COUDRAY</AVOCATS>
<RAPPORTEUR>Mme Sophie Roussel</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Gilles Pellissier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2016:395310.20160601</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés au secrétariat du contentieux du Conseil d'Etat les 15 décembre 2015 et 20 avril 2016, la Fédération des finances CFDT demande au Conseil d'Etat :  <br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision du 15 octobre 2015 par laquelle le directeur des ressources humaines de la Caisse des dépôts et consignations a refusé d'abroger la circulaire du 12 mars 2014 relative aux moyens syndicaux des organisations syndicales de droit public dans le cadre des nouvelles instances représentatives de l'établissement public ;<br/>
<br/>
              2°) d'enjoindre à la Caisse des dépôts et consignations d'abroger cette circulaire, dans un délai de 15 jours à compter de la décision à intervenir et sous astreinte de 150 euros par jour de retard ; <br/>
<br/>
              3°) de mettre à la charge de la Caisse des dépôts et consignations la somme de 3 500 euros sur le fondement de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code monétaire et financier ;<br/>
              - le décret n° 98-596 du 13 juillet 1998 ;<br/>
              - le décret n° 2004-883 du 27 août 2004 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Sophie Roussel, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Gilles Pellissier, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Masse-Dessen, Thouvenin, Coudray, avocat de la Fédération des finances CFDT ;<br/>
<br/>
<br/>
<br/>1. Considérant que la Fédération des finances CFDT a saisi le directeur des ressources humaines de la Caisse des dépôts et consignations d'une demande tendant à l'abrogation de la circulaire du 12 mars 2014 relative aux moyens syndicaux des organisations syndicales de droit public dans le cadre des nouvelles instances représentatives de l'établissement ; que, par une décision du 15 octobre 2015, le directeur des ressources humaines de la Caisse des dépôts et consignations a rejeté cette demande ; que la Fédération des finances CFDT demande l'annulation pour excès de pouvoir de cette décision de refus et à ce qu'il soit enjoint, sous astreinte, à la Caisse des dépôts et consignations d'abroger cette circulaire ; que sa requête doit toutefois être regardée comme dirigée uniquement contre le refus de la Caisse d'abroger les deux derniers alinéas du paragraphe 5-2-1 de la circulaire en litige ;<br/>
<br/>
              Sur la fin de non recevoir opposée par la Caisse des dépôts et consignations : <br/>
<br/>
              2. Considérant qu'aux termes des deux derniers alinéas du paragraphe 5-2-1 de la circulaire du 12 mars 2014 dont la fédération requérante demande l'abrogation : " Sur demande des représentants qui, en raison de l'exercice de leur mandat, s'estiment défavorisés dans leur rémunération, du fait de leur régime indemnitaire, par rapport à celle des autres fonctionnaires exerçant dans les services de l'établissement, il est procédé à un réajustement à titre individuel de cette rémunération./ L'évaluation préalable du montant global de cet ajustement est établie sur la moyenne des PVO [prime variable d'objectifs] (hors IIE) et sur la moyenne de la NBI [nouvelle bonification indiciaire] versées aux agents de droit public tous grades confondus. Le réajustement est intégré dans l'allocation complémentaire de fonction " ; <br/>
<br/>
              3. Considérant que les dispositions citées ci-dessus instituent un dispositif de complément de rémunération au profit des représentants syndicaux de droit public bénéficiant d'une décharge de service à temps complet pour l'exercice d'un mandat syndical ; que, contrairement à ce que soutient la Caisse des dépôts et consignations, elles ne constituent pas de simples orientations générales mais présentent un caractère impératif et sont dès lors susceptibles de faire l'objet d'un recours pour excès de pouvoir ; que, par suite, la fin de non recevoir opposée par la Caisse des dépôts et consignations ne peut qu'être écartée ; <br/>
<br/>
<br/>
<br/>
<br/>
              Sur la légalité des dispositions contestées : <br/>
<br/>
              4. Considérant, d'une part, qu'aux termes du I de l'article 14 du décret du 13 juillet 1998 : " Le comité technique est consulté sur les questions relatives :/ (...) 10° (...) aux règles relatives aux régimes indemnitaires des fonctionnaires de la Caisse des dépôts et consignations (...) " ; qu'il ressort des pièces du dossier que les dispositions en litige de la circulaire du 12 mars 2014, qui ont entendu édicter des règles relatives au régime indemnitaire de fonctionnaires de la Caisse des dépôts et consignations, n'ont pas été préalablement soumises au comité technique de l'établissement, contrairement à ce que prévoient les dispositions précitées du décret du 13 juillet 1998 ; <br/>
<br/>
              5. Considérant, d'autre part, que le fonctionnaire qui bénéficie d'une décharge totale de service pour l'exercice d'un mandat syndical a droit, durant l'exercice de ce mandat, que lui soit maintenu le bénéfice de l'équivalent des montants et droits de l'ensemble des primes et indemnités légalement attachées à l'emploi qu'il occupait avant d'en être déchargé pour exercer son mandat, à l'exception des indemnités représentatives de frais et des indemnités destinées à compenser des charges et contraintes particulières, tenant notamment à l'horaire, à la durée du travail ou au lieu d'exercice des fonctions, auxquelles le fonctionnaire n'est plus exposé du fait de la décharge de service ;<br/>
<br/>
              6. Considérant qu'il suit de là qu'en prenant comme référence, pour le calcul du complément de rémunération institué par les dispositions litigieuses au profit des fonctionnaires bénéficiant d'une décharge totale de service pour l'exercice d'un mandat syndical, la moyenne des primes versées aux agents de droit public tous grades confondus, alors que ne pouvait être prise comme référence que la situation des agents occupant un emploi comparable à celui qu'occupait l'agent avant de bénéficier d'une décharge syndicale, l'auteur de la circulaire en litige a méconnu les principes énoncés ci-dessus ; <br/>
<br/>
              7. Considérant qu'il résulte de ce qui précède, sans qu'il soit besoin d'examiner les autres moyens de la requête, que la Fédération des finances CFDT est fondée à demander l'annulation de la décision du 15 octobre 2015 en tant que, par cette décision, la Caisse des dépôts et consignations a refusé d'abroger les deux derniers alinéas du paragraphe 5-2-1 de la circulaire du 12 mars 2014 ; qu'il y a lieu d'enjoindre à la Caisse d'abroger ces dispositions dans un délai de deux mois à compter de la notification de la présente décision ; que, dans les circonstances de l'espèce, il n'y a pas lieu d'assortir cette injonction de l'astreinte demandée par la requérante ; <br/>
<br/>
              8. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la Caisse des dépôts et consignations une somme de 3 000 euros à verser à la Fédération des finances CFDT au titre des dispositions de l'article L. 761-1 du code de justice administrative ; que ces dispositions font en revanche obstacle à ce qu'une somme soit mise à ce titre à la charge de la Fédération des finances CFDT qui n'est pas, dans la présente instance, la partie perdante ; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La décision du 15 octobre 2015 par laquelle le directeur des ressources humaines de la Caisse des dépôts et consignations a refusé d'abroger la circulaire du 12 mars 2014 relative aux moyens syndicaux des organisations syndicales de droit public dans le cadre des nouvelles instances représentatives de l'établissement public est annulée en tant qu'elle porte sur les deux derniers alinéas du paragraphe 5-2-1 de cette circulaire. <br/>
Article 2 : Il est enjoint à la Caisse des dépôts et consignations d'abroger les deux derniers alinéas du paragraphe 5-2-1 de la circulaire du 12 mars 2014 dans un délai de deux mois à compter de la notification de la présente décision. <br/>
Article 3 : La Caisse des dépôts et consignations versera une somme de 3 000 euros à la Fédération des finances CFDT au titre de l'article L. 761-1 du code de justice administrative. <br/>
Article 4 : Le surplus des conclusions de la Fédération des finances CFDT est rejeté. <br/>
Article 5 : Les conclusions de la Caisse des dépôts et consignations présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
Article 6 : La présente décision sera notifiée à la Fédération des finances CFDT et à la Caisse des dépôts et consignations.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
