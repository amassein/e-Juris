<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038077315</ID>
<ANCIEN_ID>JG_L_2019_01_000000410534</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/07/73/CETATEXT000038077315.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème chambre, 30/01/2019, 410534, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-01-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>410534</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DELAMARRE, JEHANNIN</AVOCATS>
<RAPPORTEUR>M. Laurent-Xavier Simonel</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Laurent Cytermann</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2019:410534.20190130</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
               M. et Mme B...ont demandé au tribunal administratif de Grenoble de prononcer la décharge des cotisations supplémentaires d'impôt sur le revenu et de contributions sociales auxquelles ils ont été assujettis au titre des années 2008 et 2009 ainsi que des pénalités correspondantes. Par un jugement n° 1204257 du 19 mars 2015, le tribunal administratif de Grenoble a rejeté leur demande.<br/>
<br/>
              Par un arrêt n° 15LY01703 du 28 mars 2017, la cour administrative d'appel de Lyon, après avoir décidé qu'il n'y avait pas lieu de statuer à hauteur du dégrèvement accordé en cours d'instance, a rejeté le surplus des conclusions de l'appel formé par M. et Mme B...contre ce jugement.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 15 mai et 11 août 2017 au secrétariat du contentieux du Conseil d'Etat, M. et Mme B...demandent au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler l'article 2 de cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire entièrement droit à leur appel ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
               - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Laurent-Xavier Simonel, conseiller d'Etat en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Laurent Cytermann, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Delamarre, Jéhannin, avocat de M. et Mme B...;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond qu'à la suite de la vérification de la comptabilité de la société de droit portugais Trust Paving Unipessoal LDA dont M. B...est l'associé unique, l'administration a procédé à la taxation d'office des résultats réalisés de manière occulte par l'établissement stable de la société en France au titre des exercices clos de 2007 à 2009. Ces résultats ont été regardés comme des revenus réputés distribués entre les mains de M. B...sur le fondement des dispositions du 2° du 1 de l'article 109 du code général des impôts et du c de l'article 111 du même code. Par un jugement du 19 mars 2015, le tribunal administratif de Grenoble a rejeté leur demande tendant à la décharge des cotisations supplémentaires d'impôt sur le revenu et de contributions sociales auxquelles ils ont été assujettis au titre des années 2009 et 2010 ainsi que des pénalités correspondantes. Ils demandent l'annulation de l'article 2 de l'arrêt du 28 mars 2017 par lequel la cour administrative d'appel de Lyon, après avoir prononcé un non-lieu à statuer à hauteur du dégrèvement intervenu en cours d'instance, a rejeté le surplus des conclusions de leur requête. <br/>
<br/>
              2. Il ressort des écritures présentées en appel par les requérants que ceux-ci soutenaient que l'administration fiscale avait reconstitué de manière arbitraire les montants des chiffres d'affaires et bénéfices réalisés par l'établissement stable en France de la société de droit portugais Trust Paving Unipessoal LDA et avaient joint, en ce sens, une copie du mémoire par lequel cette société soutenait, dans le cadre du litige la concernant, que la méthode utilisée par l'administration était radicalement viciée. La cour n'a pas visé ce moyen, qui n'était pas inopérant, et n'y a pas répondu. M. et Mme B...sont, dès lors, fondés à demander l'annulation de l'article 2 de l'arrêt qu'ils attaquent, sans qu'il soit besoin d'examiner les autres moyens de leur pourvoi. <br/>
<br/>
              3. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 1 500 euros à verser à M. et Mme B...au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : L'article 2 de l'arrêt du 28 mars 2017 de la cour administrative d'appel de Lyon est annulé.<br/>
Article 2 : L'affaire est renvoyée, dans cette mesure, à la cour administrative d'appel de Lyon.<br/>
Article 3 : L'Etat versera à M. et Mme B...la somme de 1 500 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
Article 4 : La présente décision sera notifiée à M. et Mme A...B...et au ministre de l'action et des comptes publics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
