<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044545399</ID>
<ANCIEN_ID>JG_L_2021_12_000000450551</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/54/53/CETATEXT000044545399.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème - 7ème chambres réunies, 21/12/2021, 450551, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-12-21</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>450551</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème - 7ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Amélie Fort-Besnard</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Philippe Ranquet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:450551.20211221</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et deux mémoires, enregistrés les 10 mars, 3 septembre et 27 octobre 2021 au secrétariat du contentieux du Conseil d'Etat, le Comité inter-mouvements auprès des évacués (CIMADE), le Groupe d'information et de soutien des immigré.e.s (GISTI), la Ligue des droits de l'homme (LDH), la Fédération des associations de solidarité avec tou-te-s les immigré-e-s (FASTI) et le Comité pour la santé des exilés (COMEDE) demandent au Conseil d'Etat, dans le dernier état de leurs écritures :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir l'arrêté du 7 janvier 2021 et l'article 1er de l'arrêté du 7 avril 2021, pris en application de l'article L. 744-2 du code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la directive 2013/33/UE du Parlement européen et du Conseil du 26 juin 2013 ;<br/>
              - le code de l'action sociale et des familles ;<br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Amélie Fort-Besnard, maître des requêtes,<br/>
<br/>
              - les conclusions de M. Philippe Ranquet, rapporteur public.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1.	Aux termes de l'article L. 744-2 du code l'entrée et du séjour des étrangers et du droit d'asile, devenu les articles L. 551-1 à L. 551-6 du même code: " I. - Le schéma national d'accueil des demandeurs d'asile et d'intégration des réfugiés fixe la part des demandeurs d'asile accueillis dans chaque région ainsi que la répartition des lieux d'hébergement qui leur sont destinés. Il est arrêté par le ministre chargé de l'asile, après avis des ministres chargés du logement et des affaires sociales. Il est transmis au Parlement. / Un schéma régional est établi par le représentant de l'Etat dans la région, après avis d'une commission de concertation composée de représentants des collectivités territoriales, des services départementaux de l'éducation nationale, de gestionnaires de lieux d'hébergement pour demandeurs d'asile et d'associations de défense des droits des demandeurs d'asile et en conformité avec le schéma national d'accueil des demandeurs d'asile. (...) / II. - Lorsque la part des demandeurs d'asile résidant dans une région excède la part fixée pour cette région par le schéma national d'accueil des demandeurs d'asile et les capacités d'accueil de cette région, le demandeur d'asile peut être orienté vers une autre région, où il est tenu de résider le temps de l'examen de sa demande d'asile. / L'Office français de l'immigration et de l'intégration détermine la région de résidence en fonction de la part des demandeurs d'asile accueillis dans chaque région en application du schéma national et en tenant compte des besoins et de la situation personnelle et familiale du demandeur au regard de l'évaluation prévue à l'article L. 744-6 et de l'existence de structures à même de prendre en charge de façon spécifique les victimes de la traite des êtres humains ou les cas de graves violences physiques ou sexuelles. / (...) / Un décret en Conseil d'Etat fixe les conditions d'application du présent II. ". Et aux termes de l'article R. 744-13-1, devenu l'article R. 551-1 du même code : " En application du premier alinéa du II de l'article L. 744-2, le schéma national d'accueil des demandeurs d'asile et des réfugiés fixe, tous les deux ans, la part des demandeurs d'asile devant résider dans chaque région, hors outre-mer. Cette répartition tient compte des caractéristiques démographiques, économiques et sociales ainsi que des capacités d'accueil de chaque région. Le schéma fixe également par région la répartition des places d'hébergement destinées aux demandeurs d'asile et aux réfugiés. ".<br/>
<br/>
              2.	Par un arrêté du 7 janvier 2021 pris en application de l'article L. 744-2 du code de l'entrée et du séjour des étrangers et du droit d'asile, le ministre de l'intérieur a fixé, d'une part, la répartition des places d'hébergement dédiées à l'accueil des demandeurs d'asile et des réfugiés entre les régions métropolitaines, hors Corse, au 31 décembre 2021 (article 1er) et, d'autre part, pour chaque région, la part des demandeurs d'asile accueillis au titre de l'orientation vers une autre région que celle où ils ont déposé leur demande (article 2). Par un arrêté du 7 avril 2021 pris, en cours d'instance, pour l'application des mêmes dispositions, le ministre de l'intérieur a abrogé l'arrêté du 7 janvier, fixé la répartition des places d'hébergement dédiées à l'accueil des demandeurs d'asile et des réfugiés entre les régions métropolitaines, hors Corse, au 31 décembre 2021, de manière identique (article 1er) et fixé la part des demandeurs d'asile accueillis dans chaque région (article 2).<br/>
<br/>
              3.	Le Comité inter-mouvements auprès des évacués (CIMADE), le Groupe d'information et de soutien des immigré.e.s (GISTI), la Ligue des droits de l'homme (LDH), la Fédération des associations de solidarité avec tou-te-s les immigré-e-s (FASTI) et le Comité pour la santé des exilés (COMEDE) doivent être regardés comme demandant, dans le dernier état de leurs écritures, l'annulation pour excès de pouvoir des articles 1er et 2 de l'arrêté du 7 janvier 2021 et celle de l'article 1er de l'arrêté du 7 avril 2021.<br/>
<br/>
              Sur les fins de non-recevoir : <br/>
<br/>
              4.	En premier lieu, l'article 7.2 des statuts de La Cimade stipule que le conseil national " donne mandat au président pour représenter l'association, se porter partie civile et, de manière générale, pour ester en justice dans toute cause où La Cimade a intérêt à agir. En cas d'urgence, cette autorisation peut être donnée par le bureau national, sous réserve de ratification par le conseil national ". Pour justifier de sa capacité à représenter l'association en justice, son président produit une décision du bureau national de l'association du 9 mars 2021 qui l'autorise à contester, y compris par la voie du référé, l'arrêté du 7 janvier 2021, ainsi qu'un extrait des délibérations du conseil national des 22 et 23 octobre 2021 ratifiant cette autorisation. Dans ces conditions, la fin de non-recevoir tirée de ce qu'il n'a pas été régulièrement autorisé à représenter l'association à l'instance doit être écartée.<br/>
<br/>
              5.	En second lieu, le GISTI, la LDH, la FASTI et le COMEDE ont signé la requête et sont régulièrement représentés à l'instance. Dans ces conditions, la fin de non-recevoir tirée de ce que le président de la Cimade n'a pas justifié d'un mandat pour agir au nom des quatre autres associations, qu'il ne représente pas à l'instance, ne peut qu'être écartée.  <br/>
<br/>
              Sur les conclusions à fin d'annulation :<br/>
<br/>
              En ce qui concerne les articles 1ers des arrêtés litigieux :<br/>
<br/>
              6.	En premier lieu, s'il résulte des dispositions règlementaires reproduites au point 1 que le schéma national d'accueil des demandeurs d'asile doit fixer, tous les deux ans, la part des demandeurs d'asile devant résider dans chaque région, il ne résulte d'aucun texte que le ministre chargé de l'asile devrait programmer la répartition entre régions des places d'hébergement destinées à l'accueil des demandeurs d'asile et des réfugiés sur une période de deux ans. Dès lors, les associations requérantes ne peuvent utilement soutenir que le ministre aurait méconnu les dispositions précitées en fixant la répartition entre régions des places d'hébergement au 31 décembre 2021.<br/>
<br/>
              7.	En deuxième lieu, il ne résulte d'aucune disposition du code de l'entrée et du séjour des étrangers et du droit d'asile que le législateur a entendu exclure les collectivités d'outre-mer de la répartition des places d'hébergement pour demandeurs d'asile et réfugiés. Dès lors que les arrêtés attaqués n'incluent pas les collectivités d'outre-mer dans la répartition des places d'hébergement pour demandeurs d'asile, les associations requérantes sont fondées à soutenir qu'ils sont, dans cette mesure, illégaux.<br/>
<br/>
              8.	En troisième lieu, il ne résulte ni des dispositions reproduites au point 1, ni d'aucun texte que le ministre devrait répartir les places d'hébergement destinées aux demandeurs d'asile entre régions de manière proportionnelle au nombre de demandes d'asile présentées dans chaque région. Au contraire, le schéma national d'accueil des demandeurs d'asile doit notamment permettre d'orienter des demandeurs d'asile vers une autre région que celle dans laquelle ils ont présenté leur demande d'asile, lorsque celle-ci concentre une forte demande. Par suite, ni la circonstance qu'aucune place ne soit créée en centre d'accueil et d'examen des situations et en centre d'accueil de demandeurs d'asile en Ile-de-France, ni celle, à la supposer établie, que ces créations ne compenseraient pas, en région Provence-Alpes-Côte d'Azur, la diminution du nombre de places en hébergement d'urgence pour demandeurs d'asile ne sont, par elles-mêmes,  de nature à caractériser une erreur manifeste d'appréciation dans la répartition des places d'hébergement entre régions. <br/>
<br/>
              9.	En quatrième lieu, aux termes de l'article L. 744-3 du code l'entrée et du séjour des étrangers et du droit d'asile : " Sont des lieux d'hébergement pour demandeurs d'asile : / 1° Les centres d'accueil pour demandeurs d'asile mentionnés à l'article L. 348-1 du code de l'action sociale et des familles ; / 2° Toute structure bénéficiant de financements du ministère chargé de l'asile pour l'accueil de demandeurs d'asile et soumise à déclaration, au sens de l'article L. 322-1 du même code. / (...) ". <br/>
<br/>
              10.	En application des dispositions reproduites au point 1, le schéma national d'accueil des demandeurs d'asile a pour objet de répartir les demandeurs d'asile et les places d'hébergement entre régions, et non de définir pour chaque région les types de places d'hébergement créées. Par suite, les associations requérantes ne peuvent utilement soutenir que les arrêtés attaqués procéderaient à une répartition erronée du nombre de places par type d'hébergement pour demandeurs d'asile.<br/>
<br/>
              En ce qui concerne l'article 2 de l'arrêté du 7 janvier 2021 :<br/>
<br/>
              11.	Il résulte des dispositions du II de l'article L. 744-2 du code de l'entrée et du séjour des étrangers en France qu'il appartient à l'Office français de l'immigration et de l'intégration de décider l'orientation des demandeurs d'asile vers une autre région que celle dans laquelle ils ont déposé leur demande d'asile lorsque la part des demandeurs d'asile résidant dans cette région excède la part fixée par le schéma national d'accueil des demandeurs d'asile et les capacités d'accueil de cette région. A cette fin, le schéma national d'accueil des demandeurs d'asile doit fixer, en application de l'article R. 744-13-1, la part des demandeurs d'asile accueillis dans chaque région. L'article 2 de l'arrêté du 7 janvier 2021, désormais abrogé, a fixé la part des demandeurs d'asile orientés depuis la région Ile-de-France vers chacune des autres régions métropolitaines et non la répartition des demandeurs d'asile entre chaque région. Dès lors, les associations requérantes sont fondées à soutenir qu'il est entaché d'erreur de droit et à en demander l'annulation.<br/>
<br/>
              12.	Il résulte de tout ce qui précède que les associations requérantes sont seulement fondées à demander, d'une part, l'annulation des arrêtés attaqués en tant que les collectivités d'outre-mer sont exclues de la répartition des places d'hébergement entre régions et, d'autre part, l'annulation de l'article 2 de l'arrêté du 7 janvier 2021 en tant qu'il ne fixe pas la part des demandeurs d'asile accueillis par chaque région.<br/>
<br/>
              Sur l'application de l'article L. 911-1 du code de justice administrative : <br/>
<br/>
              13.	Aux termes de l'article L. 911-1 du code de justice administrative : " Lorsque sa décision implique nécessairement qu'une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public prenne une mesure d'exécution dans un sens déterminé, la juridiction, saisie de conclusions en ce sens, prescrit, par la même décision, cette mesure assortie, le cas échéant, d'un délai d'exécution. / La juridiction peut également prescrire d'office cette mesure. ".<br/>
<br/>
              14.	Le ministre chargé de l'asile a désormais fixé la répartition des demandeurs d'asile entre régions métropolitaines. Dès lors, l'exécution de la présente décision implique seulement que les collectivités d'outre-mer sur le territoire desquelles les dispositions des articles L. 551-1 à L. 551-6 du code de l'entrée et du séjour des étrangers et du droit d'asile sont applicables en vertu du titre IX du livre V de la partie législative de ce code soient inclues dans la répartition des places d'hébergement entre régions. Il y a lieu, par suite, d'enjoindre au ministre chargé de l'asile d'y procéder dans un délai de six mois à compter de la notification de la présente décision.<br/>
<br/>
              Sur les conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              15.	Les associations requérantes n'ayant pas eu recours à un avocat et ne justifiant pas de frais spécifiques qu'elles auraient exposés dans la présente instance, il n'y a pas lieu, dans les circonstances de l'espèce, de leur allouer une somme sur le fondement des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Les arrêtés du 7 janvier 2021 et du 7 avril 2021 sont annulés en tant qu'ils n'incluent pas les collectivités d'outre-mer dans la répartition des places d'hébergement pour demandeurs d'asile et réfugiés entre régions. <br/>
Article 2 : L'arrêté du 7 janvier 2021 est annulé en tant qu'il ne fixe pas la part des demandeurs d'asile accueillis dans chaque région.<br/>
Article 3 : Il est enjoint au ministre chargé de l'asile d'inclure les collectivités d'outre-mer sur le territoire desquelles les dispositions des articles L. 551-1 à L. 551-6 du code de l'entrée et du séjour des étrangers et du droit d'asile sont applicables en vertu du titre IX du livre V de la partie législative de ce code dans la répartition des places d'hébergement pour demandeurs d'asile dans un délai de six mois à compter de la notification de la présente décision. <br/>
Article 4 : Le surplus des conclusions de la requête est rejeté.<br/>
Article 5 : La présente décision sera notifiée au Comité Inter-Mouvements auprès des Evacués " (CIMADE), représentant unique, et au ministre de l'intérieur. <br/>
              Délibéré à l'issue de la séance du 1er décembre 2021 où siégeaient : Mme Christine Maugüé, présidente adjointe de la section du contentieux, présidant ; M. H... I..., M. Olivier Japiot, présidents de chambre ; M. B... G..., Mme A... J..., M. E... L..., M. F... K..., M. Jean-Yves Ollier, conseillers d'Etat et Mme Amélie Fort-Besnard, maître des requêtes-rapporteure. <br/>
<br/>
              Rendu le 21 décembre 2021.<br/>
<br/>
<br/>
                 La Présidente : <br/>
                 Signé : Mme Christine Maugüé<br/>
 		La rapporteure : <br/>
      Signé : Mme Amélie Fort-Besnard<br/>
                 La secrétaire :<br/>
                 Signé : Mme C... D...<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
