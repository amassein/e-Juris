<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000041575537</ID>
<ANCIEN_ID>JG_L_2020_02_000000430845</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/41/57/55/CETATEXT000041575537.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème chambre, 13/02/2020, 430845, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-02-13</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>430845</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MEIER-BOURDEAU, LECUYER</AVOCATS>
<RAPPORTEUR>M. Charles-Emmanuel Airy</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Karin Ciavaldini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2020:430845.20200213</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés les 17 mai et 30 septembre 2019 au secrétariat du contentieux du Conseil d'Etat, l'Association Anti-G demande au Conseil d'Etat, dans le dernier état de ses écritures :<br/>
<br/>
              1°) de constater l'inexistence matérielle de tout acte ayant pour objet la désaffectation des parcelles cadastrées K nos 849, 852, 997, 998 et 1000 situées sur l'emprise de l'ancienne de chemin de fer Dinan - Dinard et sur l'emprise de l'ancienne gare de Dinard ; <br/>
<br/>
              2°) de constater l'inexistence matérielle des formalités et procédures relatives à la cession de ces parcelles ;<br/>
<br/>
              3°) de constater l'absence d'attribution d'une concession d'aménagement à la société Dinard Newquay ;<br/>
<br/>
              4°) de constater la caducité du permis de construire du 11 juillet 2011 transféré à la société Dinard Newquay ;<br/>
<br/>
              5°) de mettre à la charge de la commune de Dinard, de la société Dinard Newquay et de l'Etat la somme de 1 500 euros chacun au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la loi n° 82-1153 du 30 décembre 1982 ;<br/>
              - les décrets n° 83-816 et n° 83-817 du 13 septembre 1983 ; <br/>
              - le décret du 18 septembre 1992 ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Charles-Emmanuel Airy, auditeur,  <br/>
<br/>
              - les conclusions de Mme Karin Ciavaldini, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Meier-Bourdeau, Lecuyer, avocat de la SNCF Réseau ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 4 février 2020, présentée par l'Association Anti-G.<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Le décret du 18 septembre 1992 portant retranchement et déclassement de sections de lignes dépendant du réseau ferré national géré par la Société nationale des chemins de fer français, pris en application de l'article 18 du décret du 13 septembre 1983 relatif au domaine confié à la société nationale des chemins de fer français, alors en vigueur, a constaté, en vue de procéder notamment au " retranchement et déclassement " de la ligne de chemin de fer de Dinan à Dinard (n°444 000), section de Saint-Samson à Dinard (Ille-et-Vilaine) et de l'ancienne gare de Dinard, que cette section de ligne et la gare n'étaient plus exploitées depuis le 1er février 1988, date de leur fermeture. Par suite, les conclusions de l'Association Anti-G, dont il appartient au Conseil d'Etat de connaître, tendant à ce que soit constatée l'absence d'existence matérielle de toute décision prenant acte de la désaffectation des parcelles situées sur l'emprise du chemin de fer Dinan - Dinard et sur l'emprise de l'ancienne gare de Dinard ne peuvent qu'être rejetées.<br/>
<br/>
              2. Par un arrêt n° 13NT03388 du 3 avril 2015 la cour administrative d'appel de Nantes s'est prononcée sur les conclusions de l'Association Anti-G relatives à la cession par Réseau ferré de France à la commune de Dinard, puis par la commune de Dinard à la société Eiffage, des parcelles situées sur l'emprise du chemin de fer Dinan - Dinard et sur l'emprise de l'ancienne gare de Dinard. Le pourvoi en cassation de l'Association Anti-G contre cet arrêt n'a pas été admis par une décision n° 394135 en date du 28 juillet 2017 du Conseil d'Etat, statuant au contentieux. Les conclusions, ayant le même objet, dont cette Association saisit à nouveau le Conseil d'Etat, présentées en méconnaissance de l'autorité de la chose jugée, doivent être regardées comme manifestement irrecevables. Elles ne peuvent dès lors qu'être rejetées.<br/>
<br/>
              3. Les conclusions de l'Association Anti-G enregistrées le 30 septembre 2019, par lesquelles elle demande de constater l'absence d'attribution d'une concession d'aménagement à la société Dinard Newquay et de constater la caducité du permis de construire du 11 juillet 2011 transféré à la société Dinard Newquay, sont identiques à celles qui ont été rejetées par un jugement n°1504789 du 6 juillet 2018 du tribunal administratif de Rennes, confirmé par une ordonnance nos 18NT03233, 18NT03284, 18NT03746 du 15 mai 2019 du président de la 5ème chambre de la cour administrative d'appel de Nantes, devenue définitive faute de pourvoi en cassation dans les deux mois de sa notification à l'association requérante, le 16 mai 2019. Elles sont donc présentées en méconnaissance de l'autorité de la chose jugée et doivent être regardées comme manifestement irrecevables. Elles ne peuvent, dès lors, qu'être rejetées, y compris en tant qu'elles tendent à la mise en oeuvre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              4. Il résulte de tout ce qui précède, sans qu'il soit besoin de se prononcer sur les autres fins de non-recevoir soulevées dans les mémoires de la commune de Dinard, de la société Eiffage Immobilier Grand Ouest et de SNCF Réseau, que les demandes de l'Association Anti-G doivent être rejetées.<br/>
<br/>
              5. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Association Anti-G la somme de 1 000 euros à verser respectivement à la commune de Dinard, à la société Eiffage Immobilier Grand Ouest et à SNCF Réseau au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de l'Association Anti-G est rejetée.<br/>
Article 2 : L'Association Anti-G versera à la commune de Dinard, à la société Eiffage Immobilier Grand Ouest et à SNCF Réseau une somme de 1 000 euros chacune au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée à l'Association Anti-G, au Premier ministre, à la ministre de la transition écologique et solidaire, à la commune de Dinard, à la société Eiffage Immobilier Grand Ouest et SNCF Réseau. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
