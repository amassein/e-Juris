<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000035317307</ID>
<ANCIEN_ID>JG_L_2017_07_000000404443</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/35/31/73/CETATEXT000035317307.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère chambre, 28/07/2017, 404443, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-07-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>404443</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Sabine Monchambert</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Rémi Decout-Paolini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2017:404443.20170728</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              1° Sous le n° 404443, par une requête et un mémoire en réplique, enregistrés les 13 octobre 2016 et 23 mars 2017 au secrétariat du contentieux du Conseil d'Etat, la société nationale d'exploitation industrielle des tabacs et allumettes (SEITA) demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler pour excès de pouvoir l'article 1er du décret n° 2016-1117 du 11 août 2016 relatif à la fabrication, à la présentation, à la vente et à l'usage des produits du tabac, des produits du vapotage et des produits à fumer à base de plantes autres que le tabac en tant qu'il introduit un article R. 3512-30 dans le code de la santé publique ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 20 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              2° Sous le n° 404447, par une requête sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 14 octobre 2016, 23 novembre 2016 et 18 avril 2017 au secrétariat du contentieux du Conseil d'Etat, la société British American Tobacco France demande au Conseil d'Etat d'annuler pour excès de pouvoir l'article 1er du même décret du 11 août 2016 en tant qu'il introduit un article R. 3512-30 dans le code de la santé publique.<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
              3° Sous le n° 407973, par une requête sommaire, un mémoire complémentaire et un nouveau mémoire, enregistrés les 14 février, 15 mai et 3 juillet 2017 au secrétariat du contentieux du Conseil d'Etat, la fédération des fabricants de cigares demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler pour excès de pouvoir le même décret du 11 août 2016 ainsi que la décision implicite du ministre des affaires sociales et de la santé rejetant son recours gracieux du 13 octobre 2016 ;<br/>
<br/>
              2°) d'enjoindre au Premier ministre, sous astreinte de 1 000 euros par jour de retard à l'expiration d'un délai de quinze jours courant à compter de la notification de sa décision, d'abroger ce décret ;<br/>
<br/>
              3°) subsidiairement, de surseoir à statuer jusqu'à ce que la Cour de justice de l'Union européenne ait répondu à la question préjudicielle transmise par la décision du Conseil d'Etat statuant au contentieux nos 401536, 401561, 401611, 401632 et 401668 du 10 mai 2017 ;<br/>
<br/>
              4°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu : <br/>
              - la Constitution ;<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales et son premier protocole additionnel ;<br/>
              - le traité sur le fonctionnement de l'Union européenne ;<br/>
              - la directive 2014/40/UE du Parlement européen et du Conseil du 3 avril 2014 ;<br/>
              - la directive (UE) 2015/1535 du Parlement européen et du Conseil du 9 septembre 2015 ;<br/>
              - le code de la santé publique ;<br/>
              - l'ordonnance n° 2016-623 du 19 mai 2016 ;<br/>
              - la décision du Conseil d'Etat statuant au contentieux nos 401536, 401561, 401611, 401632, 401668 du 10 mai 2017 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Sabine Monchambert, conseiller d'Etat, <br/>
<br/>
              - les conclusions de M. Rémi Decout-Paolini, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Piwnica, Molinié, avocat de la société British American Tobacco France et à la SCP Coutard, Munier-Apaire, avocat de la fédération des fabricants de cigares.<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Par trois requêtes qu'il y a lieu de joindre pour statuer par une seule décision, la société nationale d'exploitation industrielle des tabacs et allumettes, la société British American Tobacco France et la fédération des fabricants de cigares demandent l'annulation pour excès de pouvoir du décret du 11 août 2016 relatif à la fabrication, à la présentation, à la vente et à l'usage des produits du tabac, des produits du vapotage et des produits à fumer à base de plantes autres que du tabac, pour les deux premières en tant que ce décret insère dans le code de la santé publique un article R. 3512-30 relatif aux éléments et dispositifs contribuant à la promotion d'un produit du tabac et pour la troisième dans son entier.<br/>
<br/>
              Sur la recevabilité de la requête de la fédération des fabricants de cigare :<br/>
<br/>
              2. Eu égard aux termes du recours gracieux de la fédération des fabricants de cigares, celui-ci n'a pu conserver le délai de recours contentieux à l'égard du décret du 11 août 2016 qu'en tant qu'il insère l'article R. 3512-30 dans le code de la santé publique. Il suit de là que le surplus des conclusions d'annulation présentées par la fédération sont tardives et, par suite, irrecevables. <br/>
<br/>
              Sur la légalité externe des dispositions attaquées :<br/>
<br/>
              3. Aux termes de l'article L. 3512-21 inséré dans le code de la santé publique par l'ordonnance du 19 mai 2016 portant transposition de la directive 2014/40/UE sur la fabrication, la présentation et la vente des produits du tabac et des produits connexes : " I.- L'étiquetage des unités de conditionnement, tout emballage extérieur ainsi que le produit du tabac proprement dit ne peuvent comprendre aucun élément ou dispositif qui : / 1° Contribue à la promotion d'un produit du tabac ou incite à sa consommation en donnant une impression erronée quant aux caractéristiques, effets sur la santé, risques ou émissions de ce produit ; / 2° Ressemble à un produit alimentaire ou cosmétique. / II.- Les éléments et dispositifs qui sont interdits en vertu du I comprennent notamment les messages, symboles, noms, marques commerciales, signes figuratifs ou autres ". Aux termes de l'article L. 3512-26 inséré dans le code de la santé publique par la même ordonnance, dans sa rédaction issue de la décision du Conseil d'Etat statuant au contentieux du 10 mai 2017 : " Un décret en Conseil d'Etat détermine les conditions d'application du présent chapitre, notamment : (...) 5° Les catégories d'éléments ou dispositifs contribuant à la promotion d'un produit du tabac qui sont interdits par application du 1° de l'article L. 3512-21 (...) ".<br/>
<br/>
              4. En premier lieu, les dispositions de l'article R. 3512-30 insérées par le décret attaqué dans le code de la santé publique énumèrent les types de messages, symboles, marques, dénominations commerciales, signes figuratifs ou autres considérés comme des éléments et dispositifs qui contribuent à la promotion d'un produit du tabac, au sens du 1° du I de l'article L. 3512-21, par ce qu'ils suggèrent ou évoquent. Ces dispositions, qui n'excèdent pas le champ du 5° de l'article L. 3512-26, ne sont pas entachées d'incompétence.<br/>
<br/>
              5. En deuxième lieu, il résulte de l'article 22 de la Constitution du 4 octobre 1958 que les actes du Premier ministre sont contresignés, le cas échéant, par les ministres chargés de leur exécution. Les dispositions de l'article R. 3512-30 inséré dans le code de la santé publique par le décret du 11 août 2016 ne comportent nécessairement l'intervention d'aucune mesure réglementaire ou individuelle que le ministre de l'économie et des finances serait compétent pour signer ou contresigner. Dans ces conditions, l'absence de contreseing de ce ministre n'entache pas d'irrégularité les dispositions attaquées.<br/>
<br/>
              6. En troisième lieu, lorsqu'un décret doit être pris en Conseil d'Etat, le texte retenu par le Gouvernement ne peut être différent à la fois du projet qu'il avait soumis au Conseil d'Etat et du texte adopté par ce dernier. Il ressort des pièces produites par le ministre des affaires sociales et de la santé que les dispositions attaquées ne diffèrent pas à la fois du projet initial du Gouvernement et du texte adopté par la section sociale du Conseil d'Etat. Par suite, le moyen tiré de la méconnaissance des règles qui gouvernent l'examen par le Conseil d'Etat des projets de décret doit être écarté.<br/>
<br/>
              7. En dernier lieu, en application de l'article 5 de la directive (UE) 2015/1535 du Parlement européen et du Conseil du 9 septembre 2015 prévoyant une procédure d'information dans le domaine des réglementations techniques et des règles relatives aux services de la société de l'information, tout Etat membre qui souhaite adopter une nouvelle règle technique au sens de la directive doit en communiquer le projet à la Commission dans les conditions fixées par cet article. Toutefois, l'article 7 de la même directive prévoit que ces dispositions " ne s'appliquent pas aux dispositions législatives, réglementaires et administratives des États membres (...) par lesquels ces derniers : / a) se conforment aux actes contraignants de l'Union qui ont pour effet l'adoption de spécifications techniques ou de règles relatives aux services ". L'article R. 3512-30 inséré dans le code de la santé publique par le décret attaqué a été pris pour la transposition des dispositions de l'article 13 de la directive 2014/40/UE du 3 avril 2014 relative au rapprochement des dispositions législatives, réglementaires et administratives des États membres en matière de fabrication, de présentation et de vente des produits du tabac et des produits connexes, auquel il n'ajoute aucune règle technique nouvelle dont l'adoption aurait dû être précédée d'une communication au titre de la directive du 9 septembre 2015. Le moyen tiré de ce que le décret du 11 août 2016 aurait été adopté à la suite d'une procédure irrégulière, faute de communication à la Commission, doit donc être écarté.<br/>
<br/>
              Sur la légalité interne des dispositions attaquées : <br/>
<br/>
              8. L'article 13 de la directive 2014/40/UE du 3 avril 2014 dispose que : " 1. L'étiquetage des unités de conditionnement, tout emballage extérieur ainsi que le produit du tabac proprement dit ne peuvent comprendre aucun élément ou dispositif qui : / a) contribue à la promotion d'un produit du tabac ou incite à sa consommation en donnant une impression erronée quant aux caractéristiques, effets sur la santé, risques ou émissions du produit ; les étiquettes ne comprennent aucune information sur la teneur en nicotine, en goudron ou en monoxyde de carbone du produit du tabac ; / b) suggère qu'un produit du tabac donné est moins nocif que d'autres ou vise à réduire l'effet de certains composants nocifs de la fumée ou présente des propriétés vitalisantes, énergisantes, curatives, rajeunissantes, naturelles, biologiques ou a des effets bénéfiques sur la santé ou le mode de vie ; / c) évoque un goût, une odeur, tout arôme ou tout autre additif, ou l'absence de ceux-ci ; / d) ressemble à un produit alimentaire ou cosmétique ; / e) suggère qu'un produit du tabac donné est plus facilement biodégradable ou présente d'autres avantages pour l'environnement. / 2. Les unités de conditionnement et tout emballage extérieur ne suggèrent pas d'avantages économiques au moyen de bons imprimés, d'offres de réduction, de distribution gratuite, de promotion de type "deux pour le prix d'un" ou d'autres offres similaires. / 3. Les éléments et dispositifs qui sont interdits en vertu des paragraphes 1 et 2 peuvent comprendre notamment les messages, symboles, noms, marques commerciales, signes figuratifs ou autres ". Il résulte des dispositions de l'article 24 de la même directive que les Etats membres ne peuvent, pour des considérations relatives aux aspects réglementés par l'article 13, interdire ni restreindre la mise sur le marché des produits du tabac, dès lors qu'ils sont conformes à la directive. Par suite, les dispositions de l'article 13 constituent des mesures d'harmonisation complète.<br/>
<br/>
              9. Eu égard aux dispositions de l'article 88-1 de la Constitution, d'où découle une obligation constitutionnelle de transposition des directives, le contrôle de constitutionnalité et de légalité des dispositions d'une ordonnance ou d'un décret assurant directement cette transposition est appelé à s'exercer selon des modalités particulières dans le cas où leur contenu découle nécessairement des obligations prévues par les directives, sans que le Gouvernement ne dispose de pouvoir d'appréciation. Il appartient au juge administratif, saisi d'un moyen tiré de la méconnaissance d'une disposition ou d'un principe de valeur constitutionnelle, de rechercher s'il existe une règle ou un principe général du droit de l'Union européenne qui, eu égard à sa nature et à sa portée, tel qu'il est interprété en l'état actuel de la jurisprudence du juge de l'Union, garantit par son application l'effectivité du respect de la disposition ou du principe constitutionnel invoqué. Dans l'affirmative, il y a lieu pour le juge administratif, afin de s'assurer de la constitutionnalité de l'acte attaqué, de rechercher si la directive qui fait l'objet de la transposition est conforme à cette règle ou à ce principe général du droit de l'Union. Il lui revient, en l'absence de difficulté sérieuse, d'écarter le moyen invoqué ou, dans le cas contraire, de saisir la Cour de justice de l'Union européenne d'une question préjudicielle, dans les conditions prévues par l'article 267 du traité sur le fonctionnement de l'Union européenne. En revanche, s'il n'existe pas de règle ou de principe général du droit de l'Union garantissant l'effectivité du respect de la disposition ou du principe constitutionnel invoqué, il revient au juge administratif d'examiner directement la constitutionnalité des dispositions contestées.<br/>
<br/>
              10. Par sa décision du 10 mai 2017, le Conseil d'Etat, statuant au contentieux sur les recours dirigés contre l'ordonnance du 19 mai 2016, après avoir vérifié si les articles L. 3512-21 et L. 3512-26 que cette ordonnance insère dans le code de la santé publique procédaient à une exacte transposition des dispositions de la directive, a annulé le 5° de l'article L. 3512-26 en tant qu'il comportait l'adjectif " principales ", au motif qu'en renvoyant au pouvoir réglementaire le soin de déterminer " les principales catégories d'éléments ou dispositifs contribuant à la promotion d'un produit du tabac qui sont interdits par application du 1° de l'article L. 3512-21 ", le Gouvernement avait méconnu le caractère limitatif de l'énumération des éléments et dispositifs interdits en vertu des paragraphes 1 et 2 de l'article 13 de la directive et, ce faisant, étendu illégalement le champ des interdictions qu'ils posent. Il a, avant de statuer sur les conclusions tendant à l'annulation de l'article 1er de l'ordonnance du 19 mai 2016 en tant qu'il introduit dans le code de la santé publique les articles L. 3512-20 et L. 3512-21, saisi la Cour de justice, à titre préjudiciel en application de l'article 267 du traité sur le fonctionnement de l'Union européenne, des questions de savoir, notamment, si les dispositions des paragraphes 1 et 3 de l'article 13 de la directive du 3 avril 2014 doivent être interprétées en ce sens qu'elles proscrivent l'utilisation, sur les unités de conditionnement, sur les emballages extérieurs et sur les produits du tabac, de tout nom de marque évoquant certaines qualités, quelle que soit sa notoriété, et si ces dispositions, en tant qu'elles s'appliquent aux noms et marques commerciales, respectent le droit de propriété, la liberté d'expression, la liberté d'entreprise et les principes de proportionnalité et de sécurité juridique. <br/>
<br/>
              11. Il suit de là, en premier lieu, qu'en prévoyant, à l'article R. 3512-30 du code de la santé publique, pris pour l'application des articles L. 3512-21 et L. 3512-26 du même code, que : " Sont notamment considérés comme des éléments et dispositifs qui contribuent à la promotion d'un produit du tabac, au sens du 1° du I de l'article L. 3512-21, tous les messages, symboles, marques, dénominations commerciales, signes figuratifs ou autres " qui suggèrent ou évoquent les avantages ou qualités énumérés par cet article, le décret attaqué a, lui aussi, méconnu le caractère limitatif de l'énumération des éléments et dispositifs interdits en vertu des paragraphes 1 et 2 de l'article 13 de la directive. Par suite, les requérantes sont fondées à demander l'annulation de ces dispositions en tant qu'elles comportent l'adverbe " notamment ". <br/>
<br/>
              12. Il résulte également de ce qui a été dit ci-dessus qu'il y a lieu, avant de se prononcer sur les moyens des requêtes tirés, par la voie de l'exception, d'une part, de la contrariété à différentes règles et principes constitutionnels des articles L. 3512-21 et L. 3512-26 du même code issus de l'ordonnance du 19 mai 2016 et pris pour la transposition des mêmes dispositions de la directive, et, d'autre part, de la méconnaissance par l'article 13 de la directive, le cas échéant pris en combinaison avec le paragraphe 2 de son article 24, des articles 11, 16 et 17 de la charte des droits fondamentaux de l'Union européenne, de l'article 10 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, de l'article 1er du premier protocole additionnel à cette convention et des principes de proportionnalité et de sécurité juridique, de surseoir à statuer jusqu'à ce que la Cour de justice se soit prononcée sur les questions préjudicielles qui lui ont été renvoyées par la décision du 10 mai 2017. <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
                            --------------<br/>
<br/>
Article 1er : L'article 1er du décret du 11 août 2016 relatif à la fabrication, à la présentation, à la vente et à l'usage des produits du tabac, des produits du vapotage et des produits à fumer à base de plantes autres que le tabac est annulé en tant que l'article R. 3512-30 qu'il insère dans le code de la santé publique comporte, à son premier alinéa, l'adverbe " notamment ".<br/>
Article 2 : Les conclusions de la fédération des fabricants de cigares à fin d'annulation de dispositions du décret du 11 août 2016 autres que l'article R. 3512-30 sont rejetées.<br/>
<br/>
Article 3 : Il est sursis à statuer sur le surplus des conclusions des requêtes de la société nationale d'exploitation industrielle des tabacs et allumettes, de la société British American Tobacco France et de la fédération française des fabricants de cigares jusqu'à ce que la Cour de justice se soit prononcée sur les questions qui lui ont été transmises par la décision du Conseil d'Etat, statuant au contentieux, nos 401536, 401561, 401611, 401632, 401668 du 10 mai 2017.<br/>
Article 4 : La présente décision sera notifiée à la société nationale d'exploitation industrielle des tabacs et allumettes, à la société British American Tobacco France, à la fédération des fabricants de cigares, au Premier ministre et à la ministre des solidarités et de la santé. <br/>
Copie en sera adressée pour information au greffe de la Cour de justice de l'Union européenne. <br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
