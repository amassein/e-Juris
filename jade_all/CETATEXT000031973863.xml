<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031973863</ID>
<ANCIEN_ID>JG_L_2016_01_000000383926</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/97/38/CETATEXT000031973863.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème - 5ème SSR, 27/01/2016, 383926, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-01-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>383926</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème - 5ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Laurent Huet</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Gaëlle Dumortier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2016:383926.20160127</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, un mémoire en réplique et un nouveau mémoire enregistrés le 25 août 2014, le 12 mars 2015 et le 7 janvier 2016 au secrétariat du contentieux du Conseil d'Etat, M. A...D..., M. B...E...et M. F...C...demandent au Conseil d'Etat d'annuler pour excès de pouvoir l'arrêté du 6 juin 2014 par lequel le ministre de l'éducation nationale, de l'enseignement supérieur et de la recherche a fixé les modalités de remboursement des sommes dues par les élèves et anciens élèves des écoles normales supérieures en cas de rupture de l'engagement décennal.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de l'éducation ;<br/>
              - la loi n° 83-634 du 13 juillet 1983 ;<br/>
              - le décret n° 2011-21 du 5 janvier 2011 ;<br/>
              - le décret n° 2011-184 du 15 février 2011 ;<br/>
              - le décret n° 2012-715 du 7 mai 2012 ;<br/>
              - le décret n° 2013-924 du 17 octobre 2013 ;<br/>
              - le décret n° 2013-1140 du 9 décembre 2013 ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Laurent Huet, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Gaëlle Dumortier, rapporteur public ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 11 janvier 2016, présentée par M. D... et autres ; <br/>
<br/>
<br/>
<br/>
<br/>
              Sur la fin de non-recevoir soulevée par le ministre de l'éducation nationale, de l'enseignement supérieur et de la recherche :<br/>
<br/>
              1. Considérant que M. D...justifie, en sa qualité de membre du conseil d'administration de l'Ecole normale supérieure de Lyon, d'un intérêt lui donnant qualité pour demander l'annulation de l'arrêté du 6 juin 2014 par lequel le ministre de l'éducation nationale, de l'enseignement supérieur et de la recherche a fixé les modalités de remboursement des sommes dues par les élèves et anciens élèves des écoles normales supérieures en cas de rupture de l'engagement décennal ; que la fin de non-recevoir soulevée par le ministre de l'éducation nationale, de l'enseignement supérieur et de la recherche contre la requête en tant qu'elle émane de M. D...doit, par suite, être écartée  ;<br/>
<br/>
              Sur la légalité de l'arrêté attaqué :<br/>
<br/>
              2. Considérant, en premier lieu, que les décrets du 5 janvier 2011, du 7 mai 2012 et des 17 octobre et 9 décembre 2013 fixant, sur le fondement de l'article L. 716-1 du code de l'éducation, les règles d'organisation et de fonctionnement des quatre écoles normales supérieures de Cachan, Lyon, Rennes et Paris prévoient qu'en cas de rupture de leur engagement d'exercer une activité professionnelle dans certains types de services ou d'institutions durant dix ans comptés à partir de l'entrée dans ces écoles, " les traitements perçus doivent être remboursés, sous réserve de remise totale ou partielle accordée par le président de l'école, dans les conditions fixées par arrêté du ministre chargé de l'enseignement supérieur " ; que les dispositions de l'arrêté attaqué, qui se bornent à préciser les modalités de mise en oeuvre d'un tel remboursement et ne fixent pas de règle à caractère statutaire ou relative à l'organisation ou au fonctionnement de ces écoles n'avaient pas, par suite, à être prises par décret et ne sont donc pas, pour ce motif, entachées d'incompétence ;<br/>
<br/>
              3. Considérant, en deuxième lieu, que pour les mêmes motifs que ceux indiqués au point 2 ci-dessus, les dispositions de l'arrêté attaqué ne relèvent pas des questions et projets de textes pour lesquels l'article 34 du décret du 15 février 2011 relatif aux comités techniques dans les administrations et les établissements publics de l'Etat impose la consultation de ces comités ; que le moyen tiré du défaut de consultation préalable des comités techniques des écoles normales supérieures et du comité technique ministériel doit donc être écarté ;<br/>
<br/>
              4. Considérant, en troisième lieu, que le ministre de l'éducation nationale, de l'enseignement supérieur et de la recherche, et par délégation la directrice générale de l'enseignement supérieur et de l'insertion professionnelle, était compétent pour signer l'arrêté attaqué, sans que soit requis le contreseing du ministre chargé du budget ; que les requérants ne sauraient, par ailleurs, utilement soutenir que l'arrêté devait être signé par le secrétaire d'Etat chargé de l'enseignement supérieur et de la recherche ; <br/>
<br/>
              5. Considérant, en quatrième lieu, que les dispositions de l'article 1er de l'arrêté litigieux, dont M. D...et autres ne sont pas fondés à soutenir qu'elles méconnaissent l'objectif à valeur constitutionnelle d'intelligibilité, de clarté et d'accessibilité de la norme, prévoient qu'un élève ou un ancien élève d'une école normale supérieure peut être dégagé de manière anticipée de l'engagement décennal ; que ces dispositions, qui se combinent avec celles prévues à l'article 5, n'ont pas pour objet et ne sauraient légalement avoir pour effet de fixer des modalités de relèvement anticipé de l'engagement décennal mais se bornent à prévoir des modalités de mise en oeuvre des dispositions des décrets, mentionnés au point 2, qui permettent que le président de l'école dont un ancien élève n'accomplit pas l'obligation de service de dix ans puisse lui remettre totalement ou partiellement la somme due au titre du remboursement des traitements perçus pendant sa scolarité ; que, par suite, M. D... et autres ne sont pas fondés à soutenir que ces dispositions sont entachées d'incompétence ;<br/>
<br/>
              6. Considérant, en cinquième lieu, qu'en imposant aux anciens élèves des écoles normales supérieures une obligation d'information annuelle sur leur situation, l'article 2 de l'arrêté attaqué se borne à fixer une modalité pratique permettant de vérifier qu'ils respectent l'obligation décennale ; que la procédure de demande de remboursement qui peut, le cas échéant, résulter d'un manquement d'un ancien élève à cette obligation d'information annuelle, ne peut conduire à ce que l'ancien élève soit tenu de procéder à un remboursement qu'en cas de rupture effective de l'engagement décennal ; que ces dispositions n'ont ainsi pas méconnu les règles de remboursement fixées par les décrets du 5 janvier 2011, du 7 mai 2012 et des 17 octobre et 9 décembre 2013 ; que M. D...et autres ne sont pas davantage fondés à soutenir qu'elles sont entachées d'incompétence ;<br/>
<br/>
              7. Considérant, en sixième lieu, qu'aux termes de l'article 20 de la loi du 13 juillet 1983 portant droits et obligations des fonctionnaires : " Les fonctionnaires ont droit, après service fait, à une rémunération comprenant le traitement, l'indemnité de résidence, le supplément familial de traitement ainsi que les indemnités instituées par un texte législatif ou réglementaire (...) " ; que les décrets pour l'application desquels est pris l'arrêté attaqué ne prévoient le remboursement que des seuls " traitements perçus " ; que, par suite, l'article 3 de l'arrêté, en tant qu'il dispose que peuvent également faire l'objet d'un remboursement le supplément familial de traitement, l'indemnité de résidence et la prime de transport, est entaché d'incompétence ; que, par suite, les dispositions des 2°, 3° et 4° du I de cet article 3, qui sont divisibles des autres dispositions de l'arrêté, doivent être annulées ; <br/>
<br/>
              8. Considérant, en septième lieu, qu'aux termes de l'article L. 711-1 du code de l'éducation : " Les établissements publics à caractère scientifique, culturel et professionnel sont des établissements nationaux d'enseignement supérieur et de recherche jouissant de la personnalité morale et de l'autonomie pédagogique et scientifique, administrative et financière " ; qu'il résulte de ces dispositions que les écoles normales supérieures, qui ont la responsabilité d'assurer le versement des traitements et des indemnités de leurs élèves, disposent d'un budget autonome abondé notamment par des ressources propres ; que l'article 4 de l'arrêté attaqué pouvait, par suite, légalement prévoir le remboursement des sommes dues au bénéfice de l'école normale concernée plutôt qu'au profit du Trésor public ;<br/>
<br/>
              9. Considérant, en huitième lieu, que l'arrêté attaqué pouvait légalement prévoir que le conseil d'administration de chacune des écoles normales supérieures concernées soit consulté sur les demandes de dispense totale ou partielle de l'obligation de remboursement, un tel avis n'étant pas dépourvu de lien avec les domaines de compétences attribués à ces conseils par l'article L. 712-4 du code de l'éducation ; <br/>
<br/>
              10. Considérant enfin qu'aux termes de l'article 8 de l'arrêté attaqué : " Le règlement intérieur de chacune des écoles normales supérieures précise la procédure de suivi de l'engagement décennal ainsi que le contenu du dossier de demande de dispense " ; que ces dispositions relatives aux modalités pratiques du suivi de l'engagement décennal, qui relèvent du fonctionnement propre à chaque établissement, pouvaient, par suite, légalement être renvoyées au règlement intérieur de chacun d'eux ;<br/>
<br/>
              11. Considérant qu'il résulte de tout ce qui précède, que les requérants sont seulement fondés à demander l'annulation des dispositions des 2°, 3° et 4° du I de l'article 3 de l'arrêté attaqué ;<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Les 2°, 3° et 4° du I de l'article 3 de l'arrêté du 6 juin 2014 fixant les modalités de remboursement des sommes dues par les élèves et anciens élèves des écoles normales supérieures en cas de rupture de l'engagement décennal sont annulés.<br/>
Article 2 : Le surplus des conclusions de la requête de MM.D..., E...et C...est rejeté.<br/>
Article 3 : La présente décision sera notifiée à M. A...D..., à M. B...E..., à M. F... C...et à la ministre de l'éducation nationale, de l'enseignement supérieur et de la recherche.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
