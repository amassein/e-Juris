<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039120988</ID>
<ANCIEN_ID>JG_L_2019_09_000000423879</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/12/09/CETATEXT000039120988.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème - 3ème chambres réunies, 20/09/2019, 423879, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-09-20</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>423879</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème - 3ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR>M. Jean-Marc Vié</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Romain Victor</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:423879.20190920</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. C... a demandé au tribunal administratif de Basse-Terre de prononcer, à hauteur de la somme de 91 935 euros, la décharge de la cotisation d'impôt sur le revenu à laquelle il a été assujetti au titre de l'année 2012. Par un jugement n° 1400606 du 10 mars 2016, le tribunal administratif de la Guadeloupe a fait droit à sa demande.<br/>
<br/>
              Par un arrêt n° 16BX02059 du 5 juillet 2018, la cour administrative d'appel de Bordeaux a, sur appel du ministre de l'action et des comptes publics, annulé ce jugement et remis la somme de 91 935 euros à la charge de M. A....<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un nouveau mémoire, enregistrés les 4 septembre 2018, 4 décembre 2018 et 2 septembre 2019 au secrétariat du contentieux du Conseil d'Etat, M. B... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel du ministre ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - la loi n° 2010-1657 du 29 décembre 2010 ;<br/>
              - la loi n° 2012-1509 du 29 décembre 2012 ;<br/>
<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Marc Vié, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Romain Victor, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Piwnica, Molinié, avocat de M. B... ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article 150-0 A du code général des impôts dans sa rédaction applicable à l'année d'imposition en litige : " 1. Sous réserve des dispositions propres aux bénéfices industriels et commerciaux, aux bénéfices non commerciaux et aux bénéfices agricoles ainsi que des articles 150 UB et 150 UC, les gains nets retirés des cessions à titre onéreux, effectuées directement, par personne interposée ou par l'intermédiaire d'une fiducie, de valeurs mobilières, de droits sociaux, de titres mentionnés au 1° de l'article 118 et aux 6° et 7° de l'article 120, de droits portant sur ces valeurs, droits ou titres ou de titres représentatifs des mêmes valeurs, droits ou titres, sont soumis à l'impôt sur le revenu (...) ". <br/>
<br/>
              2. Le 2 de l'article 200 A du code général des impôts, dans sa rédaction issue de la loi du 29 décembre 2010 de finances pour 2011, disposait que les gains nets obtenus dans les conditions prévues à l'article 150-0 A étaient imposés au taux forfaitaire de 19 %. Le 7 du même article prévoyait que " le taux prévu au 2 est réduit de 30 % dans les départements de la Guadeloupe, de la Martinique et de la Réunion et de 40 % dans le département de la Guyane pour les gains mentionnés à l'article 150-0 A résultant de la cession de droits sociaux détenus dans les conditions du f de l'article 164 B (...) "<br/>
<br/>
              3. Le N du I de l'article 10 de la loi du 29 décembre 2012 de finances pour 2013 a modifié l'article 200 A du code général des impôts en remplaçant l'imposition forfaitaire de 19 % prévue au 2 de cet article par une prise en compte des gains nets obtenus dans les conditions prévues à l'article 150-0 A pour la détermination du revenu net global défini à l'article 158 du code. Il a par ailleurs inséré un 2 bis après le 2 de l'article 200 A prévoyant, par dérogation au 2, la possibilité d'imposer les gains nets obtenus dans les conditions prévues à l'article 150-0 A au taux forfaitaire de 19 % sur option du contribuable, lorsque certaines conditions sont remplies. Le V de l'article 10 de la loi de finances pour 2013 prévoit que ces dispositions s'appliquent aux gains nets et profits réalisés à compter du 1er janvier 2013.<br/>
<br/>
              4. Le législateur a en outre prévu, au IV de l'article 10 de la loi de finances pour 2013 que, par dérogation au 2 de l'article 200 A, les gains nets obtenus dans les conditions prévues à l'article 150-0 A du même code réalisés en 2012, à l'exception des gains mentionnés au 2 du II du même article, seraient imposés au taux forfaitaire de 24 %, en précisant que ces gains pouvaient, " sur option du contribuable, être imposés dans les conditions prévues au 2 bis de l'article 200 A du même code, dans sa rédaction en vigueur à compter du 1er janvier 2013, lorsque l'ensemble des conditions prévues à ce même 2 bis sont remplies ". Ces dispositions sont entrées en vigueur le lendemain de la publication au Journal officiel de la République française de la loi du 29 décembre 2012 de finances pour 2013, soit le 31 décembre 2012, et sont ainsi applicables pour l'imposition des gains nets obtenus dans les conditions prévues à l'article 150-0 A réalisés en 2012.<br/>
<br/>
              5. Il résulte de la combinaison de ces dispositions que, par dérogation à la règle d'imposition au taux forfaitaire de 19 % prévue par le 2 de l'article 200 A du code général des impôts dans sa rédaction en vigueur jusqu'au 1er janvier 2013, les gains nets obtenus en 2012 dans les conditions prévues à l'article 150-0 A réalisés sont imposables au taux forfaitaire de 24 %. Si le contribuable a la faculté, en vertu des dispositions de la loi de finances pour 2013 applicables à ces gains, de souscrire une option pour une imposition au taux forfaitaire de 19 %, ce mode d'imposition est celui que prévoit le 2 bis de l'article 200 A dans sa rédaction en vigueur à compter du 1er janvier 2013 et non celui qui est prévu au 2 du même article, en vigueur jusqu'à cette date. Dès lors, les contribuables résidant dans les départements de la Guadeloupe, de la Martinique, de la Réunion et de la Guyane ne peuvent bénéficier, en cas d'exercice de cette option, de l'abattement prévu au 7 de l'article 200 A du code général des impôts, qui s'applique uniquement au taux d'imposition de 19 % prévu au 2 de l'article 200 A dans sa rédaction antérieure au 1er janvier 2013 et ne saurait être étendu au taux optionnel de 19 % fixé par le 2 bis de cet article en vigueur à compter de cette date.<br/>
<br/>
              6. Par suite, la cour n'a pas commis d'erreur de droit en jugeant que la plus-value de cession de droits sociaux réalisée en 2012 par le requérant, dont il était constant qu'il remplissait les conditions prévues au 2 bis de l'article 200 A du code général des impôts, avait à bon droit été soumise à l'impôt sur le revenu au taux forfaitaire de 19 % prévu par cette disposition, sans pouvoir bénéficier de la réduction de taux de 30 % prévue par le 7 de l'article 200 A du code général des impôts.<br/>
<br/>
              7. Il résulte de tout ce qui précède que M. B... n'est pas fondé à demander l'annulation de l'arrêt de la cour administrative d'appel de Bordeaux qu'il attaque.<br/>
<br/>
              8. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de M. B... est rejeté.<br/>
Article 2 : La présente décision sera notifiée à M. C... et au ministre de l'action et des comptes publics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
