<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030704434</ID>
<ANCIEN_ID>JG_L_2015_06_000000384886</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/70/44/CETATEXT000030704434.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème SSJS, 03/06/2015, 384886, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-06-03</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>384886</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème SSJS</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>HAAS ; SCP ROUSSEAU, TAPIE</AVOCATS>
<RAPPORTEUR>M. Paul Bernard</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Xavier Domino</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2015:384886.20150603</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B... A...a demandé au tribunal administratif de Nice de condamner solidairement La Poste et l'Etat à lui verser une indemnité de 80 000 euros en réparation du préjudice qu'il allègue avoir subi du fait du blocage de sa carrière. <br/>
<br/>
              Par un jugement n° 0702578 du 16 décembre 2011, le tribunal administratif a condamné l'Etat et La Poste à lui verser solidairement une indemnité d'un montant de 5 000 euros, tous intérêts compris.<br/>
<br/>
              Saisie en appel par M. A...et par un appel incident du ministre chargé des postes, après avoir rendu un premier arrêt avant-dire-droit le 16 juillet 2013, la cour administrative d'appel de Marseille a, par un arrêt n° 12MA00578 du 10 juillet 2014, réformé le jugement du tribunal administratif et porté le montant de l'indemnité allouée à M. A...à la somme de 27 000 euros, tous intérêts compris.<br/>
<br/>
              Par un pourvoi et un mémoire en réplique enregistrés le 29 septembre 2014 et le 11 mai 2015 au secrétariat du contentieux du Conseil d'Etat, La Poste demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt ; <br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de M. A...; <br/>
<br/>
              3°) de mettre à la charge de M. A...la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la loi n° 83-634 du 13 juillet 1983 ;<br/>
              - la loi n° 84-16 du 11 janvier 1984 ;<br/>
              - la loi n° 90-568 du 2 juillet 1990 ;<br/>
              - le décret n° 90-1235 du 31 décembre 1990 ;<br/>
              - le décret n° 92-930 du 7 septembre 1992 ;<br/>
              - le décret n° 2009-1555 du 14 décembre 2009 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Paul Bernard, maître des requêtes, <br/>
<br/>
              - les conclusions de M. Xavier Domino, rapporteur public,<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Haas, avocat de La Poste, et à la SCP Rousseau, Tapie, avocat de M. A...; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1.	Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que M.A..., fonctionnaire de La Poste ayant accédé au grade d'agent d'administration principal du service général en 1984, a demandé au ministre de l'économie, des finances et de l'emploi et au président de La Poste l'indemnisation du préjudice qu'il estime avoir subi du fait de son absence de promotion ; que, par un jugement du 16 décembre 2011, le tribunal administratif de Nice a condamné solidairement l'Etat et La Poste à verser à M. A...une indemnité de 5 000 euros en réparation des troubles dans ses conditions d'existence et du préjudice moral ; que, par l'arrêt attaqué, la cour administrative d'appel de Marseille a réformé ce jugement et a condamné solidairement l'Etat et La Poste à verser à l'intéressé une indemnité de 27 000 euros au titre du préjudice de carrière, du préjudice moral et du préjudice de retraite ; que La Poste se pourvoit en cassation contre cet arrêt ;<br/>
<br/>
              2.	Considérant que la cour administrative d'appel, après avoir jugé que l'Etat et La Poste avaient commis des fautes de nature à engager leur responsabilité, a estimé que M. A... devait être regardé comme ayant été privé d'une chance sérieuse d'être promu dans le corps des contrôleurs à compter de l'année 1993, pour en déduire qu'il avait droit à une indemnité de 8 000 euros en réparation du préjudice de carrière résultant de cette perte de chance sérieuse de promotion et à une indemnité de 14 000 euros au titre du préjudice de retraite ; <br/>
<br/>
              3.	Considérant que, pour juger que M. A...avait été privé d'une chance sérieuse de promotion, la cour s'est essentiellement fondée sur la circonstance que l'évaluation de l'intéressé au titre de 2001 indiquait qu'il présentait une " excellente aptitude à exercer des fonctions différentes d'un niveau supérieur " ; que, toutefois, il ressort des pièces du dossier soumis aux juges du fond que l'évaluation considérée ne comporte pas une telle indication mais se borne à indiquer que le critère d'évaluation en cause est sans objet et ne peut être utilisé en l'espèce ; que La Poste est, par suite, fondée à soutenir que la cour administrative d'appel de Marseille a dénaturé les pièces du dossier qui lui étaient soumises ; que, dès lors, et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, La Poste est fondée à demander l'annulation de cet arrêt en tant qu'il l'a condamnée à verser une indemnité de 8 000 euros au titre du préjudice de carrière et une indemnité de 14 000 euros au titre du préjudice de retraite ; <br/>
<br/>
              4.	Considérant qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par La Poste, au titre des dispositions de l'article L. 761-1 du code de justice administrative ; que les dispositions de cet article font obstacle à ce qu'une somme soit mise à la charge de La Poste à ce titre ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Marseille du 10 juillet 2014 est annulé en tant qu'il a condamné La Poste à verser à M. A...une indemnité, tous intérêts compris, de 8 000 euros au titre du préjudice de carrière et une indemnité de 14 000 euros au titre du préjudice de retraite.<br/>
<br/>
Article 2 : L'affaire est, dans cette mesure, renvoyée à la cour administrative d'appel de Marseille.<br/>
<br/>
Article 3 : Les conclusions de M. A...présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
<br/>
Article 4 : Le surplus des conclusions du pourvoi de La Poste est rejeté.<br/>
<br/>
Article 5 : La présente décision sera notifiée à La Poste, à M. B...A...et au ministre de l'économie, de l'industrie et du numérique.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
