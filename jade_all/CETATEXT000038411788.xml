<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038411788</ID>
<ANCIEN_ID>JG_L_2019_04_000000421909</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/41/17/CETATEXT000038411788.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème chambre, 24/04/2019, 421909, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-04-24</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>421909</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP WAQUET, FARGE, HAZAN ; HAAS</AVOCATS>
<RAPPORTEUR>M. Charles-Emmanuel Airy</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Karin Ciavaldini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2019:421909.20190424</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société sportive professionnelle Olympique de Marseille a demandé au tribunal administratif de Marseille de condamner la commune  de Marseille à lui verser la somme de 1 003 325 euros, majorée des intérêts au taux légal ainsi que la capitalisation des intérêts, en réparation du préjudice qu'elle estime avoir subi du fait de l'indisponibilité du stade Vélodrome le 16 août 2009. Par un jugement n° 1304062 du 23 mai 2017, ce tribunal a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 17MA03262 du 23 mai 2018, la cour administrative d'appel de Marseille  a rejeté l'appel formé  par la société Olympique de Marseille contre ce jugement.  <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 2 juillet et le 2 octobre 2018 au secrétariat du contentieux du Conseil d'Etat, la société Olympique de Marseille demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de la commune de Marseille la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Alexandre Koutchouk, maître des requêtes en service extraordinaire ; <br/>
<br/>
              - les conclusions de Mme Karin Ciavaldini, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Waquet, Farge, Hazan, avocat de la société sportive professionnelle Olympique de Marseille et à Me Haas, avocat de la commune de Marseille ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que la commune de Marseille a conclu le 1er juillet 2009 avec la société sportive professionnelle Olympique de Marseille une convention de mise à disposition du stade Vélodrome, valable pour la période du 1er juillet 2009 au 31 juin 2011, en vue de l'organisation des rencontres de football programmées du club de l'Olympique de Marseille. La commune de Marseille a également conclu avec la société Live Nation une convention de mise à disposition de ce même stade pour la période du 15 au 21 juillet 2009 en vue l'organisation d'un concert. Le 16 juillet 2009, au cours des opérations de montage de la scène de spectacle édifiée en vue de cette manifestation, une structure métallique de la scène s'est effondrée, occasionnant le décès de deux personnes. A la suite de cet accident, le match de football devant opposer, le 16 août 2009, l'Olympique Marseille et le Lille Olympique Sporting Club n'a pu avoir lieu au stade Vélodrome, mais s'est tenu au stade de la Mosson à Montpellier. La société Olympique de Marseille a demandé à la commune de Marseille de lui verser la somme de 1 003 325 euros en réparation du préjudice qu'elle estime avoir subi du fait de l'indisponibilité du stade Vélodrome le 16 août 2009. Par un jugement du 23 mai 2017, le tribunal administratif de Marseille a rejeté sa demande. La société Olympique de Marseille se pourvoit en cassation contre l'arrêt du 23 mai 2018 par lequel la cour administrative d'appel de Marseille a rejeté l'appel qu'elle avait formé contre ce jugement.  <br/>
<br/>
              2. Il ressort des pièces du dossier soumis aux juges du fond qu'une réunion, destinée à faire le point sur les conséquences de l'accident survenu le 16 juillet 2009 et sur le calendrier de l'enquête judiciaire s'est tenue le 29 juillet 2009, en présence du procureur de la République de Marseille, du juge d'instruction chargé de l'enquête, et de représentants de la mairie de Marseille et de l'Olympique de Marseille. Il ressort également des pièces du dossier, d'une part, qu'à l'issue de cette réunion, il a été constaté que les investigations liées à l'enquête judiciaire ne permettraient pas de mettre le stade Vélodrome à disposition de l'Olympique de Marseille pour la rencontre prévue le 16 août, constat qui a été confirmé par un communiqué de presse publié par la mairie de Marseille le 29 juillet 2009, et, d'autre part, que si la société Olympique de Marseille a informé le 3 août 2019 la commission des compétitions de la Ligue de Football professionnel de l'organisation d'une réunion le 5 août suivant en vue de déterminer les mesures de sécurité nécessaires au bon déroulement de cette rencontre sportive au stade de la Mosson à Montpellier, elle s'est, ce faisant, bornée à tirer les conséquences de la situation. Par suite, en estimant, pour rejeter l'appel dont elle était saisie, que la décision de délocaliser le match devant opposer l'Olympique de Marseille au Lille Olympique Sporting Club le 16 août 2009 avait été prise dès le 3 août 2009 de manière unilatérale et irrévocable par la société Olympique de Marseille, de sorte que le préjudice subi par cette dernière devait être regardé comme résultant de cette seule décision, la cour a dénaturé les pièces du dossier qui lui était soumis. Il s'ensuit, et alors que ce moyen, né de l'arrêt attaqué, pouvait être régulièrement soulevé sans qu'un prétendu changement de position de la société requérante puisse être utilement allégué à l'encontre de sa recevabilité, que la société Olympique de Marseille est fondée, sans qu'il soit besoin d'examiner les autres moyens de son pourvoi, à demander l'annulation de l'arrêt qu'elle attaque.<br/>
<br/>
              3. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la commune de Marseille la somme de 3 000 euros à verser à la société Olympique de Marseille au titre de l'article L. 761-1 du code de justice administrative. Ces dispositions font en revanche obstacle à ce qu'une somme soit mise à ce titre à la charge de la société Olympique de Marseille, qui n'est pas la partie perdante.<br/>
<br/>
<br/>
<br/>                  D E C I D E :<br/>
                               --------------<br/>
<br/>
Article 1er : L'arrêt n° 17MA03262 du 23 mai 2018 de la cour administrative d'appel de Marseille est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Marseille.<br/>
<br/>
Article 3 : La commune de Marseille versera une somme de 3 000 euros à la société Olympique de Marseille au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : Les conclusions présentées par la commune de Marseille sur le fondement de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 5 : La présente décision sera notifiée à la société sportive professionnelle Olympique de Marseille et à la commune de Marseille.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
