<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044190314</ID>
<ANCIEN_ID>JG_L_2021_10_000000451628</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/19/03/CETATEXT000044190314.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème - 9ème chambres réunies, 11/10/2021, 451628, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-10-11</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>451628</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème - 9ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP WAQUET, FARGE, HAZAN ; SCP BORE, SALVE DE BRUNETON, MEGRET</AVOCATS>
<RAPPORTEUR>Mme Isabelle Lemesle</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Arnaud Skzryerbak</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:451628.20211011</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
<br/>
              M. A... B... a demandé au tribunal administratif de Rouen d'annuler pour excès de pouvoir le certificat d'urbanisme du 15 mars 2017 par lequel le maire de Honguemare-Guenouville (Eure) a déclaré non réalisable l'opération consistant en la construction de deux maisons jumelées, attenantes par un garage, sur la parcelle cadastrée ZE 155, située rue de la Mare Floréas, ainsi que la décision du 24 mai 2017 rejetant son recours gracieux. Par un jugement n° 1702240 du 28 février 2019, le tribunal administratif de Rouen a annulé les décisions des 15 mars et 24 mai 2017.<br/>
<br/>
              Par un arrêt n° 19DA00937 du 9 février 2021, la cour administrative d'appel de Douai a, sur appel de la commune de Honguemare-Guenouville, annulé ce jugement et rejeté la demande de M. B....<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 12 avril et 12 juillet 2021 au secrétariat du contentieux du Conseil d'Etat, M. B... demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de la commune de Honguemare-Guenouville ;<br/>
<br/>
              3°) de mettre à la charge de la commune de Honguemare-Guenouville la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Par un mémoire et un mémoire en réplique, enregistrés les 12 juillet et 16 septembre 2021 au secrétariat du contentieux du Conseil d'Etat, présentés en application de l'article 23-5 de l'ordonnance n° 58-1067 du 7 novembre 1958, M. A... B... demande au Conseil d'Etat, à l'appui de son pourvoi en cassation, de renvoyer au Conseil constitutionnel la question de la conformité aux droits et libertés garantis par la Constitution des dispositions combinées de l'article L. 174-4 du code de l'urbanisme et de l'article L. 123-1 de ce code dans sa rédaction antérieure au 15 décembre 2000.<br/>
<br/>
<br/>
<br/>
                   Vu les autres pièces du dossier ;<br/>
<br/>
                   Vu :<br/>
                   - la Constitution, notamment son Préambule et ses articles 34 et 61-1 ;<br/>
                   - l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
                   - le code de l'urbanisme ;<br/>
                   - la loi n° 2000-1208 du 13 décembre 2000 ;<br/>
                   - la loi n° 2014-366 du 24 mars 2014 ;<br/>
                   - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Isabelle Lemesle, conseillère d'Etat,  <br/>
<br/>
              - les conclusions de M. Arnaud Skzryerbak, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Waquet, Farge, Hazan, avocat de M. A... B... ;<br/>
<br/>
                   Vu la note en délibéré, enregistrée le 23 septembre 2021, présentée par M. B... ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              Sur la question prioritaire de constitutionnalité :<br/>
<br/>
              1. Aux termes du premier alinéa de l'article 23-5 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel : " Le moyen tiré de ce qu'une disposition législative porte atteinte aux droits et libertés garantis par la Constitution peut être soulevé (...) à l'occasion d'une instance devant le Conseil d'Etat (...) ". Il résulte des dispositions de ce même article que le Conseil constitutionnel est saisi de la question prioritaire de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux.<br/>
<br/>
              2. Aux termes de l'article L. 123-1 du code de l'urbanisme, dans sa version immédiatement antérieure à la loi du 13 décembre 2000 relative à la solidarité et au renouvellement urbains : " Les plans d'occupation des sols fixent, dans le cadre des orientations des schémas directeurs ou des schémas de secteur, s'il en existe, les règles générales et les servitudes d'utilisation des sols, qui peuvent notamment comporter l'interdiction de construire (...). / Les plans d'occupation des sols doivent être compatibles dans les conditions fixées par l'article L. 111-1-1 avec les orientations des schémas directeurs et des schémas de secteur ou les directives territoriales d'aménagement ou les lois d'aménagement ou d'urbanisme, et respecter les servitudes d'utilité publique affectant l'utilisation du sol ainsi que les dispositions nécessaires à la mise en œuvre de projets d'intérêt général relevant de l'Etat, de la région, du département ou d'autres intervenants. Ils prennent en considération les dispositions des programmes locaux de l'habitat lorsqu'ils existent ". Il résulte de ces dispositions que le plan d'occupation des sols peut fixer au titre de l'article L. 123-1 du code de l'urbanisme, dans sa rédaction antérieure à la loi du 13 décembre 2000 relative à la solidarité et au renouvellement urbains, des règles relatives à la superficie minimale des terrains constructibles. Aux termes de l'article L. 174-4 du même code, créé par l'ordonnance du 23 septembre 2015 relative à la partie législative du livre Ier de ce code, qui reprend les alinéas 1 à 4 et 7 de l'article L. 123-19 : " Les plans d'occupation des sols maintenus provisoirement en vigueur en application des dispositions du présent chapitre ont les mêmes effets que les plans locaux d'urbanisme./ Ils sont soumis au régime juridique des plans locaux d'urbanisme défini par le titre V du présent livre./ Les dispositions de l'article L. 123-1 dans leur rédaction antérieure au 15 décembre 2000 leur demeurent applicables./ Ils peuvent faire l'objet :/ 1° D'une modification lorsqu'il n'est pas porté atteinte à l'économie générale du plan et hors les cas prévus aux 2° et 3° de l'article L. 153-31 ;/ 2° D'une mise en compatibilité selon les modalités définies par les articles L. 153-54 à L. 153-59./ Lorsqu'un plan d'occupation des sols a été approuvé avant le classement des carrières dans la nomenclature des installations classées, seules sont opposables à l'ouverture des carrières les dispositions du plan les visant expressément ".Il résulte de ces dispositions que la possibilité de fixer des règles relatives à la superficie minimale des terrains constructibles est demeurée ouverte pour les plans d'occupation des sols maintenus provisoirement en vigueur.<br/>
<br/>
              3. M. B... soutient que les dispositions combinées de l'article L. 174-4 du code de l'urbanisme et de l'article L. 123-1 du même code, dans sa version immédiatement antérieure à la loi du 13 décembre 2000, méconnaissent le principe d'égalité devant la loi garanti par l'article 6 de la Déclaration des droits de l'homme et du citoyen du 26 août 1789, en ce qu'elles établissent une différence de traitement manifestement injustifiée entre les pétitionnaires selon que leur commune est régie par un plan d'occupation des sols ou par un plan local d'urbanisme, ainsi que les dispositions de l'article 34 de la Constitution et des articles 2 et 17 de la Déclaration des droits de l'homme et du citoyen, en n'encadrant ou en ne limitant pas la faculté de fixer des règles relatives à la superficie minimale des terrains constructibles. <br/>
<br/>
              4. En premier lieu, aux termes de l'article 6 de la Déclaration des droits de l'homme et du citoyen du 26 août 1789 : " La loi doit être la même pour tous, soit qu'elle protège, soit qu'elle punisse ". Le principe d'égalité devant la loi ne s'oppose ni à ce que le législateur règle de façon différente des situations différentes, ni à ce qu'il déroge à l'égalité pour des raisons d'intérêt général, pourvu que, dans l'un et l'autre cas, la différence de traitement qui en résulte soit en rapport direct avec l'objet de la loi qui l'établit. Par ailleurs, la différence de traitement qui résulte de la succession de deux régimes juridiques dans le temps n'est pas, en elle-même, contraire au principe d'égalité.<br/>
<br/>
              5. Il résulte des travaux préparatoires de la loi relative à la solidarité et au renouvellement urbains que le législateur a entendu, de manière transitoire, soumettre les plans d'occupation des sols au régime juridique des plans locaux d'urbanisme, tout en maintenant en vigueur les dispositions relatives à leur contenu. La possibilité ainsi ouverte d'un traitement différent des propriétés foncières au regard de la règle relative à la surface minimale des terrains constructibles, pendant la période transitoire durant laquelle les plans d'occupation des sols ont été maintenus en vigueur afin d'être transformés en plan locaux d'urbanisme ou en plans locaux d'urbanisme intercommunaux, faute de quoi ils ont été frappés de caducité, résulte de la volonté du législateur de rénover progressivement le cadre juridique des politiques d'aménagement de l'espace, dans un délai raisonnable qui tienne compte des contraintes inhérentes à l'élaboration d'un plan local d'urbanisme et sans que les communes concernées ne se trouvent dans l'obligation de faire dans cette attente application du seul règlement national d'urbanisme. Cette différence de traitement est en rapport direct avec l'objet de la loi qui l'a établie. <br/>
<br/>
              6. En second lieu, aux termes de l'article 2 de la Déclaration des droits de l'homme et du citoyen du 26 août 1789 : " Le but de toute association politique est la conservation des droits naturels et imprescriptibles de l'homme. Ces droits sont la liberté, la propriété, la sûreté, et la résistance à l'oppression ". L'article 17 de la même Déclaration prévoit que : " La propriété étant un droit inviolable et sacré, nul ne peut en être privé, si ce n'est lorsque la nécessité publique, légalement constatée, l'exige évidemment, et sous la condition d'une juste et préalable indemnité ". Selon l'article 34 de la Constitution : " (...) La loi détermine les principes fondamentaux (...)/ - du régime de la propriété (...) ". La méconnaissance par le législateur de sa propre compétence ne peut être invoquée à l'appui d'une question prioritaire de constitutionnalité que dans le cas où cette méconnaissance affecte par elle-même un droit ou une liberté que la Constitution garantit.<br/>
<br/>
              7. D'une part, dès lors que les dispositions combinées de l'article L. 174-4 du code de l'urbanisme et de l'article L. 123-1 du même code dans sa version immédiatement antérieure à la loi du 13 décembre 2000, en tant qu'elles permettent, de manière transitoire, au règlement des plans d'occupation des sols de maintenir en vigueur les règles relatives à la superficie minimale des terrains constructibles, ne constituent pas une privation du droit de propriété, M. B... ne peut utilement invoquer la méconnaissance par le législateur de sa propre compétence conjuguée à celle de l'article 17 de la Déclaration des droits de l'homme et du citoyen du 26 août 1789.<br/>
<br/>
              8. D'autre part, il est loisible au législateur d'apporter aux conditions d'exercice du droit de propriété des personnes privées, protégé par l'article 2 de la Déclaration des droits de l'homme et du citoyen du 26 août 1789, des limitations liées à des exigences constitutionnelles ou justifiées par l'intérêt général, à la condition qu'il n'en résulte pas d'atteintes disproportionnées au regard de l'objectif poursuivi. Les dispositions de l'article L. 123-1 du code de l'urbanisme dans la version immédiatement antérieure à la loi du 13 décembre 2000permettaient une limitation des conditions de l'exercice du droit de propriété justifiée par l'intérêt général qui s'attache à la maîtrise, par les collectivités publiques, de l'occupation des sols et du développement urbain, dans le cadre des orientations des schémas directeurs ou des schémas de secteur, en fonction des situations locales et dans un principe d'équilibre entre urbanisation et protection des espaces. Elle était accompagnée, sous le contrôle du juge de l'excès de pouvoir, de garanties de fond, qui résultaient notamment du dernier alinéa de l'article L. 123-1 du code de l'urbanisme. Elle était également accompagnée de garanties de procédure liées aux conditions dans lesquelles sont adoptés les documents d'urbanisme. Il suit de là que les dispositions critiquées, qui encadraient avec une précision suffisante la possibilité pour les règlements des plans d'occupation des sols de prévoir des règles relatives à la superficie minimale des terrains constructibles, ne portaient pas à l'exercice du droit de propriété une atteinte disproportionnée au regard de l'objectif qu'elles poursuivaient et ne méconnaissaient pas, dans des conditions affectant ce droit, la compétence que le législateur tient de l'article 34 de la Constitution pour déterminer les principes fondamentaux du régime de la propriété, sans qu'ait d'incidence la circonstance qu'il ait fait le choix, ultérieurement, de supprimer la faculté de fixer une telle règle.<br/>
<br/>
              9. Il résulte de ce qui précède qu'il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité invoquée, qui n'est pas nouvelle et ne présente pas de caractère sérieux.<br/>
<br/>
<br/>
              Sur l'admission du pourvoi de M. B... :<br/>
<br/>
              10. Aux termes de l'article L. 822-1 du code de justice administrative : " Le pourvoi en cassation devant le Conseil d'Etat fait l'objet d'une procédure préalable d'admission. L'admission est refusée par décision juridictionnelle si le pourvoi est irrecevable ou n'est fondé sur aucun moyen sérieux ".<br/>
<br/>
              11. Pour demander l'annulation de l'arrêt qu'il attaque, M. B... soutient que de la cour administrative d'appel de Douai a :<br/>
<br/>
              - entaché son arrêt d'une erreur de droit en considérant que le règlement du plan d'occupation des sols de la commune d'Honguemare-Guenouville, maintenu provisoirement en vigueur jusqu'au 26 mars 2017 dans l'attente de l'approbation d'un plan local d'urbanisme, pouvait légalement, en application des dispositions combinées de l'article L. 174-4 du code de l'urbanisme et de l'article L. 123-1 du même code dans sa rédaction antérieure au 15 décembre 2000, comporter une règle relative à la superficie minimale des terrains constructibles en zone NB ;<br/>
              - fondé son arrêt sur des dispositions qui ne sont pas conformes à la Constitution, à supposer que les dispositions combinées de l'article L. 174-4 du code de l'urbanisme et de l'article L. 123-1 de ce code dans sa rédaction antérieure à la loi du 13 décembre 2000 relative à la solidarité et au renouvellement urbains, doivent être regardées comme permettant le maintien des surfaces minimales de terrain constructibles inscrites dans les règlements des plans d'occupation des sols maintenus provisoirement en vigueur après la loi du 24 mars 2014 pour l'accès au logement et un urbanisme rénové.<br/>
<br/>
              12. Aucun de ces moyens n'est de nature à permettre l'admission du pourvoi.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
      D E C I D E :<br/>
      --------------<br/>
<br/>
Article 1er : Il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée par M. B....<br/>
Article 2 : Le pourvoi de M. B... n'est pas admis. <br/>
Article 3 : La présente décision sera notifiée à M. A... B..., au ministre de la transition écologique et à la commune de Honguemare-Guenouville.<br/>
Copie en sera adressée au Premier ministre, au Conseil constitutionnel et au ministre de la cohésion des territoires et des relations avec les collectivités territoriales.<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
