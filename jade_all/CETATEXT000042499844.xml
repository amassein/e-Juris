<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042499844</ID>
<ANCIEN_ID>JG_L_2020_11_000000429211</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/49/98/CETATEXT000042499844.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème - 9ème chambres réunies, 04/11/2020, 429211, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-11-04</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>429211</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème - 9ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP PIWNICA, MOLINIE ; SCP BAUER-VIOLAS, FESCHOTTE-DESBOIS, SEBAGH</AVOCATS>
<RAPPORTEUR>Mme Isabelle Lemesle</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Alexandre Lallet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:429211.20201104</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
<br/>
              La société Brimo de Laroussilhe a demandé au tribunal administratif de Paris de surseoir à statuer dans l'attente de la décision définitive de l'autorité judiciaire sur la question de la propriété du fragment du jubé gothique de la cathédrale de Chartres, dit " fragment à l'Aigle ", d'annuler la décision du 12 mars 2007 par laquelle le directeur de l'architecture et du patrimoine du ministère de la culture et de la communication a refusé de lui délivrer un certificat pour l'exportation de ce fragment et d'enjoindre au ministre de la culture et de la communication de lui délivrer ce certificat d'exportation, dans un délai de quinze jours à compter de la notification du jugement à intervenir, sous astreinte de 3 000 euros par jour de retard. Par un jugement n° 0707297/4-1 du 29 juin 2017, le tribunal administratif de Paris a rejeté cette demande.<br/>
<br/>
              Par un arrêt n° 17PA02928 du 29 janvier 2019, la cour administrative d'appel de Paris a rejeté l'appel formé par la société Brimo de Laroussilhe contre ce jugement.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et deux mémoires en réplique enregistrés les 28 mars 2019, 27 juin 2019, 3 juin 2020, et 29 septembre 2020 au secrétariat du contentieux du Conseil d'Etat, la société Brimo de Laroussilhe demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
                 Vu les autres pièces du dossier ;<br/>
<br/>
                 Vu :<br/>
                 - le code civil ;<br/>
                 - le code général de la propriété des personnes publiques ;<br/>
                 - le code du patrimoine ;<br/>
                 - la loi du 9 décembre 1905 ;<br/>
                 - le décret de l'Assemblée constituante du 2 novembre 1789 ;<br/>
                 - le décret de l'Assemblée constituante des 25, 26, 29 juin et 9 juillet 1790 ;<br/>
                 - le décret de l'Assemblée constituante des 22 novembre et 1er décembre 1790 ;<br/>
                 - le décret n°93-124 du 29 janvier 1993 ;<br/>
                 - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Isabelle Lemesle, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Alexandre Lallet, rapporteur public ;<br/>
<br/>
     La parole ayant été donnée, avant et après les conclusions, à la SCP Piwnica, Molinié, avocat de la Société Galerie Brimo de Larousshilhe  et à la SCP Bauer-Violas, Feschotte-Desbois, Sebagh, <br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que la société Brimo de Laroussilhe a acquis en 2002 un fragment de pierre sculptée de 163 cm sur 48 cm, présenté comme datant de la Renaissance italienne. Il a toutefois résulté de recherches qu'il s'agit d'un fragment du jubé de la cathédrale de Chartres, désigné comme le " fragment à l'Aigle ", partie supérieure d'un autre fragment intitulé le " fragment à l'Ange ", les deux réunis représentant " l'Agneau divin ". Le 5 septembre 2003, le ministre de la culture et de la communication a opposé un refus à la demande de la société Brimo de Laroussilhe tendant à la délivrance d'un certificat d'exportation pour le " fragment à l'Aigle ", au motif qu'il s'agit d'un bien appartenant au domaine public mobilier de l'Etat, constituant de ce fait un " trésor national " au sens des dispositions de l'article L. 111-1 du code du patrimoine. S'estimant propriétaire de ce bien, la société Brimo de Laroussilhe n'a pas déféré à la demande du directeur de l'architecture et du patrimoine du ministère de la culture et de la communication du 12 février 2007 la mettant en demeure de le restituer. Elle a présenté une nouvelle demande de certificat d'exportation qui a été rejetée par une décision du 12 mars 2017. Par un jugement du 29 juin 2017, le tribunal administratif de Paris a rejeté la demande de la société Brimo de Laroussilhe tendant à l'annulation de ce refus. Celle-ci se pourvoit en cassation contre l'arrêt du 29 janvier 2019 par lequel la cour administrative d'appel de Paris a rejeté son appel contre ce jugement. <br/>
<br/>
              Sur les conclusions à fin de non-lieu du ministre de la culture :<br/>
<br/>
              2. Le ministre de la culture fait valoir en défense que par un arrêt du 18 janvier 2018, devenu définitif à la suite du rejet par la 1ère chambre civile de la Cour de cassation, par un arrêt du 13 février 2019, du pourvoi formé par la société Brimo de Laroussilhe à son encontre, la cour d'appel de Paris a confirmé le jugement du 26 novembre 2015 par lequel le tribunal de grande instance de Paris, saisi par l'Etat d'une action en revendication, a jugé que le " fragment à l'Aigle " n'avait jamais cessé d'être propriété de l'Etat et a ordonné sa restitution. Toutefois, cette circonstance n'est pas de nature à priver d'objet le pourvoi dirigé contre l'arrêt du 29 janvier 2019 par lequel la cour administrative d'appel de Paris a confirmé le jugement du tribunal administratif de Paris du 29 juin 2017 rejetant la demande de la société Brimo de Laroussilhe tendant à l'annulation de la décision du 12 mars 2007 par laquelle le ministre chargé de la culture a refusé de lui délivrer un certificat pour l'exportation de ce bien culturel.<br/>
<br/>
              Sur les conclusions du pourvoi :<br/>
<br/>
              3. L'article L. 111-2 du code du patrimoine dispose que : " L'exportation temporaire ou définitive hors du territoire douanier des biens culturels, autres que les trésors nationaux, qui présentent un intérêt historique, artistique ou archéologique et entrent dans l'une des catégories définies par décret en Conseil d'Etat est subordonnée à l'obtention d'un certificat délivré par l'autorité administrative (...) ". Il résulte de l'article 2 du décret du 29 janvier 1993 relatif aux biens culturels soumis à certaines restrictions de circulation, en vigueur à la date du refus litigieux opposé à la société Brimo de Laroussilhe, dont les dispositions sont désormais reprises à l'article R. 111-4 du code du patrimoine, que la demande de certificat d'exportation d'un bien culturel doit émaner du propriétaire du bien, agissant directement ou par l'intermédiaire d'un mandataire. L'Etat ne peut légalement faire droit à une demande présentée par une personne qui ne présente pas cette qualité. <br/>
<br/>
              4. Le décret de l'Assemblée constituante du 2 novembre 1789 dispose que : " tous les biens ecclésiastiques sont à la disposition de la Nation (...) ". Aux termes de l'article 8 du décret de l'Assemblée constituante des 22 novembre et 1er décembre 1790 relatif aux domaines nationaux, aux échanges et concessions et aux apanages : " Les domaines nationaux et les droits qui en dépendent, sont et demeurent inaliénables sans le consentement et le concours de la nation ; mais ils peuvent être vendus et aliénés à titre perpétuel et incommutable, en vertu d'un décret formel du corps législatif, sanctionné par le Roi, en observant les formalités prescrites pour la validité de ces sortes d'aliénations ". Aux termes de l'article 36 du même décret : " La prescription aura lieu à l'avenir pour les domaines nationaux dont l'aliénation est permise par les décrets de l'assemblée nationale, et tous les détenteurs d'une portion quelconque desdits domaines, qui justifieront en avoir joui par eux-mêmes ou par leurs auteurs, à titre de propriétaires, publiquement et sans trouble, pendant quarante ans continuels à compter du jour de la publication du présent décret, seront à l'abri de toute recherche ". Il résulte de ces dispositions, d'une part, que le décret du 2 novembre 1789 a incorporé au domaine national l'ensemble des biens ecclésiastiques et, d'autre part, que si en mettant fin à la règle d'inaliénabilité du domaine national, le décret des 22 novembre et 1er décembre 1790 a rendu possible, pendant qu'il était en vigueur, l'acquisition par prescription des biens relevant de ce domaine, cette possibilité, circonscrite dans le temps et encadrée dans ses modalités, n'a été ouverte que pour les biens dont " un décret formel du corps législatif, sanctionné par le Roi " avait préalablement autorisé l'aliénation. <br/>
<br/>
              5. Comme il a été dit au point 2, par un arrêt du 18 janvier 2018, la cour d'appel de Paris a rejeté l'appel formé par la société Brimo de Laroussilhe contre le jugement du tribunal de grande instance de Paris du 26 novembre 2015 faisant droit à l'action en revendication du " fragment à l'Aigle " introduite par l'Etat et ordonnant à la société Brimo de Laroussilhe de restituer ce bien à ce dernier au motif que le " fragment à l'Aigle " appartient au domaine public de l'Etat. Il résulte de cette décision définitive du juge judiciaire qui reconnaît le droit de propriété de l'Etat sur ce bien depuis l'intervention du décret de l'Assemblée constituante du 2 novembre 1789 que l'Etat était tenu de rejeter la demande de certificat d'exportation du " fragment à l'Aigle " formée par la société Brimo de Laroussilhe. Ce motif, qui n'appelle l'appréciation d'aucune circonstance de fait nouvelle et suffit à justifier légalement le dispositif de l'arrêt attaqué, doit être substitué à ceux sur lesquels la cour s'est fondée.<br/>
<br/>
              6. Il résulte de ce qui précède, sans qu'il soit besoin de se prononcer sur les moyens soulevés par le pourvoi, que la société Brimo de Laroussilhe n'est pas fondée à demander l'annulation de l'arrêt qu'elle attaque. Ses conclusions doivent être rejetées, y compris celles tendant à ce qu'une somme soit mise à la charge de l'Etat au titre de l'article L. 761-1 du code de justice administrative. En revanche, dans les circonstances de l'espèce, il y a lieu de mettre à la charge de la société Brimo de Laroussilhe la somme de 3 000 euros à verser à l'Etat au titre de ces mêmes dispositions.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
       --------------<br/>
<br/>
<br/>
Article 1er : Le pourvoi de la société Brimo de Laroussilhe est rejeté.<br/>
Article 2 : La société Brimo de Laroussilhe versera à l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée à société Brimo de Laroussilhe et au ministre de la culture<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
