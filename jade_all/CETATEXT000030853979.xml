<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030853979</ID>
<ANCIEN_ID>JG_L_2015_07_000000386769</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/85/39/CETATEXT000030853979.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère / 6ème SSR, 01/07/2015, 386769</TITRE>
<DATE_DEC>2015-07-01</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>386769</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère / 6ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>DELAMARRE ; SCP MONOD, COLIN, STOCLET</AVOCATS>
<RAPPORTEUR>M. Laurent Cytermann</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Rémi Decout-Paolini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:386769.20150701</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
Procédure contentieuse antérieure<br/>
<br/>
              Mme B...a demandé au juge des référés du tribunal administratif de Lille d'ordonner, sur le fondement des dispositions de l'article L. 521-1 du code de justice administrative, la suspension de la décision du 31 octobre 2014 par laquelle le président du conseil général du Nord a " refusé de l'admettre à l'aide sociale à l'enfance " et d'enjoindre à cette autorité de " l'admettre à l'aide sociale à l'enfance " dans un délai de quinze jours.<br/>
<br/>
              Par une ordonnance n° 1408440 du 16 décembre 2014, le juge des référés du tribunal administratif de Lille a suspendu la décision du président du conseil général du Nord et lui a enjoint de procéder à nouvel examen de la demande de Mme A...dans un délai de quinze jours.<br/>
<br/>
            Procédure devant le Conseil d'Etat  <br/>
<br/>
              Par un pourvoi, enregistré le 29 décembre 2014 au secrétariat du contentieux du Conseil d'Etat, le département du Nord demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance du juge des référés du tribunal administratif de Lille du 16 décembre 2014 ;<br/>
<br/>
              2°) de rejeter la demande présentée à ce juge par MmeA....<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
Vu :<br/>
- le code civil ;<br/>
- le code de l'action sociale et des familles ;<br/>
- la loi n° 91-647 du 10 juillet 1991 ;<br/>
- le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Laurent Cytermann, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Rémi Decout-Paolini, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Manuel Delamarre, avocat du département du Nord, et à la SCP Monod, Colin, Stoclet, avocat de Mme A... ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces de la procédure devant le juge des référés du tribunal administratif de Lille que le département du Nord soutenait que la demande de suspension de sa décision présentée par Mme A...n'était pas recevable, en raison de l'existence d'une autre voie de recours, devant le juge des enfants, en application de l'article 375 du code civil ; que si le juge des référés s'est prononcé sur la compétence de la juridiction administrative, il n'a, en revanche, pas répondu à cette fin de non-recevoir opérante ; que, par suite, le département du Nord est fondé à soutenir que l'ordonnance qu'il attaque est insuffisamment motivée ;<br/>
<br/>
              2. Considérant qu'il résulte de ce qui précède que, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, l'ordonnance du juge des référés du tribunal administratif de Lille doit être annulée ; <br/>
<br/>
              3. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de statuer sur la demande de suspension en application des dispositions de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              4. Considérant que selon l'article L. 223-2 du code de l'action sociale et des familles : " Sauf si un enfant est confié au service par décision judiciaire ou s'il s'agit de prestations en espèces, aucune décision sur le principe ou les modalités de l'admission dans le service de l'aide sociale à l'enfance ne peut être prise sans l'accord écrit des représentants légaux ou du représentant légal du mineur ou du bénéficiaire lui-même s'il est mineur émancipé. / En cas d'urgence et lorsque le représentant légal du mineur est dans l'impossibilité de donner son accord, l'enfant est recueilli provisoirement par le service qui en avise immédiatement le procureur de la République. / (...) / Si, dans le cas prévu au deuxième alinéa du présent article, l'enfant n'a pas pu être remis à sa famille ou le représentant légal n'a pas pu ou a refusé de donner son accord dans un délai de cinq jours, le service saisit également l'autorité judiciaire en vue de l'application de l'article 375-5 du code civil (...) " ; qu'aux termes de l'article 375 du code civil : " Si la santé, la sécurité ou la moralité d'un mineur non émancipé sont en danger, ou si les conditions de son éducation ou de son développement physique, affectif, intellectuel et social sont gravement compromises, des mesures d'assistance éducative peuvent être ordonnées par justice à la requête des père et mère conjointement, ou de l'un d'eux, de la personne ou du service à qui l'enfant a été confié ou du tuteur, du mineur lui-même ou du ministère public (...) " ; qu'aux termes de l'article 375-3 du même code : " Si la protection de l'enfant l'exige, le juge des enfants peut décider de le confier : (...) 3° A un service départemental de l'aide sociale à l'enfance (...) " ; que selon l'article 375-1 du même code : " Le juge des enfants est compétent, à charge d'appel, pour tout ce qui concerne l'assistance éducative (...) " ; que l'article 375-5 dispose que : " A titre provisoire mais à charge d'appel, le juge peut, pendant l'instance, soit ordonner la remise provisoire du mineur à un centre d'accueil ou d'observation, soit prendre l'une des mesures prévues aux articles 375-3 et 375-4. / En cas d'urgence, le procureur de la République du lieu où le mineur a été trouvé a le même pouvoir, à charge de saisir dans les huit jours le juge compétent, qui maintiendra, modifiera ou rapportera la mesure (...) " ;<br/>
<br/>
              5. Considérant qu'il résulte de ces dispositions que lorsqu'il est saisi par un mineur d'une demande d'admission à l'aide sociale à l'enfance et que le ou les représentants légaux de celui-ci ne sont pas en mesure, notamment en raison de leur éloignement géographique, de donner leur accord à cette admission, le président du conseil général peut seulement, au-delà de la période d'accueil provisoire de cinq jours prévue par l'article L. 223-2 du code de l'action sociale et des familles, décider de saisir l'autorité judiciaire, mais ne peut en aucun cas décider d'admettre le mineur à l'aide sociale à l'enfance sans que l'autorité judiciaire ne l'ait ordonné ; que si le président du conseil général refuse de saisir l'autorité judiciaire, notamment lorsqu'il estime que le jeune a atteint la majorité, celui-ci peut saisir le juge des enfants en application de l'article 375 du code civil ; que l'existence de cette voie de recours, par laquelle un mineur peut obtenir du juge qu'il ordonne son admission à l'aide sociale à l'enfance, y compris à titre provisoire pendant l'instance, sans que son incapacité à agir en justice ne puisse lui être opposée, rend irrecevable le recours pour excès de pouvoir devant le juge administratif contre la décision du président du conseil général de refuser de saisir l'autorité judiciaire et la demande de suspension dont ce recours peut être assorti ; <br/>
<br/>
              6. Considérant que, saisi d'une demande d'admission à l'aide sociale présentée par MmeA..., qui indiquait être née le 1er juin 1999 à Kinshasa et ne pas avoir de famille en France, le président du conseil général du Nord, par une décision du 31 octobre 2014, a refusé de saisir l'autorité judiciaire de sa situation, en contestant tant sa minorité que son isolement ; qu'il résulte de ce qui a été dit ci-dessus que l'existence de la voie de recours dont l'intéressée disposait devant le juge des enfants s'opposait à ce qu'elle forme devant le tribunal administratif un recours tendant à l'annulation pour excès de pouvoir de la décision de refus du président du conseil général et en demande la suspension au juge des référés ; que, par suite, la demande présentée par Mme A...sur le fondement de l'article L. 521-1 du code de justice administrative doit être rejetée ; <br/>
<br/>
              7. Considérant que les dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 font obstacle à ce qu'il soit fait droit aux conclusions présentées à ce titre par  Mme A...; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'ordonnance du juge des référés du tribunal administratif de Lille du 16 décembre 2014 est annulée.<br/>
Article 2 : La demande présentée par Mme A...devant le juge des référés du tribunal administratif de Lille est rejetée.<br/>
Article 3 : Les conclusions présentées devant le Conseil d'Etat par Mme A...au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 sont rejetées.<br/>
Article 4 : La présente décision sera notifiée au département du Nord et à MmeB....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">04-02-02 AIDE SOCIALE. DIFFÉRENTES FORMES D'AIDE SOCIALE. AIDE SOCIALE À L'ENFANCE. - 1) ADMISSION - CAS OÙ LES REPRÉSENTANTS LÉGAUX DU MINEUR NE SONT PAS EN MESURE DE DONNER LEUR ACCORD À L'ADMISSION - POUVOIR DU PCG DE SAISIR L'AUTORITÉ JUDICIAIRE - EXISTENCE - POSSIBILITÉ D'ADMETTRE LE MINEUR SANS DÉCISION DE JUSTICE - ABSENCE - 2) CAS OÙ LE PCG REFUSE DE SAISIR L'AUTORITÉ JUDICIAIRE - VOIE DE RECOURS SPÉCIFIQUE, POUR LE MINEUR, DEVANT LE JUGE DES ENFANTS - CONSÉQUENCE - IRRECEVABILITÉ DU REP ET DU RÉFÉRÉ-SUSPENSION.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">17-03-01-02-05 COMPÉTENCE. RÉPARTITION DES COMPÉTENCES ENTRE LES DEUX ORDRES DE JURIDICTION. COMPÉTENCE DÉTERMINÉE PAR DES TEXTES SPÉCIAUX. ATTRIBUTIONS LÉGALES DE COMPÉTENCE AU PROFIT DES JURIDICTIONS JUDICIAIRES. DIVERS CAS D`ATTRIBUTIONS LÉGALES DE COMPÉTENCE AU PROFIT DES JURIDICTIONS JUDICIAIRES. - AIDE SOCIALE À L'ENFANCE - ADMISSION - 1) CAS OÙ LES REPRÉSENTANTS LÉGAUX DU MINEUR NE SONT PAS EN MESURE DE DONNER LEUR ACCORD À L'ADMISSION - POUVOIR DU PCG DE SAISIR L'AUTORITÉ JUDICIAIRE - EXISTENCE - POSSIBILITÉ D'ADMETTRE LE MINEUR SANS DÉCISION DE JUSTICE - ABSENCE - 2) CAS OÙ LE PCG REFUSE DE SAISIR L'AUTORITÉ JUDICIAIRE - VOIE DE RECOURS SPÉCIFIQUE, POUR LE MINEUR, DEVANT LE JUGE DES ENFANTS - CONSÉQUENCE - IRRECEVABILITÉ DU REP ET DU RÉFÉRÉ-SUSPENSION.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">54-01-03 PROCÉDURE. INTRODUCTION DE L'INSTANCE. EXCEPTION DE RECOURS PARALLÈLE. - AIDE SOCIALE À L'ENFANCE - ADMISSION - 1) CAS OÙ LES REPRÉSENTANTS LÉGAUX DU MINEUR NE SONT PAS EN MESURE DE DONNER LEUR ACCORD À L'ADMISSION - POUVOIR DU PCG DE SAISIR L'AUTORITÉ JUDICIAIRE - EXISTENCE - POSSIBILITÉ D'ADMETTRE LE MINEUR SANS DÉCISION DE JUSTICE - ABSENCE - 2) CAS OÙ LE PCG REFUSE DE SAISIR L'AUTORITÉ JUDICIAIRE - VOIE DE RECOURS SPÉCIFIQUE, POUR LE MINEUR, DEVANT LE JUGE DES ENFANTS - CONSÉQUENCE - IRRECEVABILITÉ DU REP ET DU RÉFÉRÉ-SUSPENSION.
</SCT>
<ANA ID="9A"> 04-02-02 1) Il résulte des dispositions de l'article L. 223-2 du code de l'action sociale et des familles (CASF) et des articles 375, 375-1, 375-3 et 375-5 du code civil  que lorsqu'il est saisi par un mineur d'une demande d'admission à l'aide sociale à l'enfance et que le ou les représentants légaux de celui-ci ne sont pas en mesure, notamment en raison de leur éloignement géographique, de donner leur accord à cette admission, le président du conseil général (PCG) peut seulement, au-delà de la période d'accueil provisoire de cinq jours prévue par l'article L. 223-2 du CASF, décider de saisir l'autorité judiciaire, mais ne peut en aucun cas décider d'admettre le mineur à l'aide sociale à l'enfance sans que l'autorité judiciaire ne l'ait ordonné.,,,2) Si le PCG refuse de saisir l'autorité judiciaire, notamment lorsqu'il estime que le jeune a atteint la majorité, celui-ci peut saisir le juge des enfants en application de l'article 375 du code civil. L'existence de cette voie de recours, par laquelle un mineur peut obtenir du juge qu'il ordonne son admission à l'aide sociale à l'enfance, y compris à titre provisoire pendant l'instance, sans que son incapacité à agir en justice ne puisse lui être opposée, rend irrecevable le recours pour excès de pouvoir (REP) devant le juge administratif contre la décision du président du conseil général de refuser de saisir l'autorité judiciaire et la demande de suspension dont ce recours peut être assorti.</ANA>
<ANA ID="9B"> 17-03-01-02-05 1) Il résulte des dispositions de l'article L. 223-2 du code de l'action sociale et des familles (CASF) et des articles 375, 375-1, 375-3 et 375-5 du code civil  que lorsqu'il est saisi par un mineur d'une demande d'admission à l'aide sociale à l'enfance et que le ou les représentants légaux de celui-ci ne sont pas en mesure, notamment en raison de leur éloignement géographique, de donner leur accord à cette admission, le président du conseil général (PCG) peut seulement, au-delà de la période d'accueil provisoire de cinq jours prévue par l'article L. 223-2 du CASF, décider de saisir l'autorité judiciaire, mais ne peut en aucun cas décider d'admettre le mineur à l'aide sociale à l'enfance sans que l'autorité judiciaire ne l'ait ordonné.,,,2) Si le PCG refuse de saisir l'autorité judiciaire, notamment lorsqu'il estime que le jeune a atteint la majorité, celui-ci peut saisir le juge des enfants en application de l'article 375 du code civil. L'existence de cette voie de recours, par laquelle un mineur peut obtenir du juge qu'il ordonne son admission à l'aide sociale à l'enfance, y compris à titre provisoire pendant l'instance, sans que son incapacité à agir en justice ne puisse lui être opposée, rend irrecevable le recours pour excès de pouvoir (REP) devant le juge administratif contre la décision du président du conseil général de refuser de saisir l'autorité judiciaire et la demande de suspension dont ce recours peut être assorti.</ANA>
<ANA ID="9C"> 54-01-03 1) Il résulte des dispositions de l'article L. 223-2 du code de l'action sociale et des familles (CASF) et des articles 375, 375-1, 375-3 et 375-5 du code civil  que lorsqu'il est saisi par un mineur d'une demande d'admission à l'aide sociale à l'enfance et que le ou les représentants légaux de celui-ci ne sont pas en mesure, notamment en raison de leur éloignement géographique, de donner leur accord à cette admission, le président du conseil général (PCG) peut seulement, au-delà de la période d'accueil provisoire de cinq jours prévue par l'article L. 223-2 du CASF, décider de saisir l'autorité judiciaire, mais ne peut en aucun cas décider d'admettre le mineur à l'aide sociale à l'enfance sans que l'autorité judiciaire ne l'ait ordonné.,,,2) Si le PCG refuse de saisir l'autorité judiciaire, notamment lorsqu'il estime que le jeune a atteint la majorité, celui-ci peut saisir le juge des enfants en application de l'article 375 du code civil. L'existence de cette voie de recours, par laquelle un mineur peut obtenir du juge qu'il ordonne son admission à l'aide sociale à l'enfance, y compris à titre provisoire pendant l'instance, sans que son incapacité à agir en justice ne puisse lui être opposée, rend irrecevable le recours pour excès de pouvoir (REP) devant le juge administratif contre la décision du président du conseil général de refuser de saisir l'autorité judiciaire et la demande de suspension dont ce recours peut être assorti.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
