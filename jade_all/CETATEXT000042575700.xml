<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042575700</ID>
<ANCIEN_ID>JG_L_2020_11_000000431985</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/57/57/CETATEXT000042575700.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème chambre, 27/11/2020, 431985, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-11-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>431985</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Bertrand Mathieu</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Sophie Roussel</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2020:431985.20201127</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés les 24 juin et 21 octobre 2019 au secrétariat du contentieux du Conseil d'Etat, Mme B... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret du 27 février 2019 rapportant le décret du 12 juillet 2016 en ce qu'il lui avait accordé la nationalité française ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 1 200 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la convention internationale relative aux droits de l'enfant ;<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - le code civil ;<br/>
              - le code justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - Le rapport de M. Bertrand Mathieu, conseiller d'Etat en service extraordinaire, <br/>
<br/>
              - Les conclusions de Mme Sophie Roussel, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
               Considérant ce qui suit : <br/>
<br/>
              1.	Aux termes de l'article 27-2 du code civil : " Les décrets portant acquisition, naturalisation ou réintégration peuvent être rapportés sur avis conforme du Conseil d'Etat dans le délai de deux ans à compter de leur publication au Journal officiel si le requérant ne satisfait pas aux conditions légales ; si la décision a été obtenue par mensonge ou fraude, ces décrets peuvent être rapportés dans le délai de deux ans à partir de la découverte de la fraude ".<br/>
<br/>
              2.	Mme A... B..., ressortissante burkinabè, a déposé une demande de naturalisation, le 28 mars 2015, dans laquelle elle a indiqué être célibataire et mère d'un enfant mineur. Elle s'est engagée sur l'honneur à signaler tout changement dans sa situation personnelle et familiale. Au vu de ses déclarations, elle été naturalisée par décret du 12 juillet 2016. Toutefois, par bordereau reçu le 28 février 2017, le ministre des affaires étrangères et du développement international a informé le ministre chargé des naturalisations que <br/>
Mme B... avait épousé à Ouagadougou (Burkina Faso) le 27 décembre 2014 un ressortissant burkinabè résidant habituellement à l'étranger. Par décret du 27 février 2019, publié au Journal officiel le 28 février 2019, le Premier ministre a rapporté le décret de naturalisation de Mme B... au motif qu'il avait été pris au vu d'informations mensongères délivrées par l'intéressée quant à sa situation familiale. Mme B... demande l'annulation pour excès de pouvoir de ce décret.<br/>
<br/>
              3.	En premier lieu, le délai de deux ans imparti par l'article 27-2 du code civil pour rapporter le décret de Mme B... a commencé à courir à la date à laquelle la réalité de la situation familiale de l'intéressée a été portée à la connaissance du ministre chargé des naturalisations.<br/>
<br/>
              4.	A cet égard, il ressort des pièces du dossier que les services du ministre chargé des naturalisations n'ont été informés de la réalité de la situation familiale de la requérante que le 28 février 2017, date à laquelle ils ont reçu les documents relatifs au mariage de l'intéressée transmis par bordereau du ministre des affaires étrangères et du développement international. La circonstance que les actes d'état civil relatifs à ses enfants mentionnent le nom de son époux n'est pas de nature à établir que son mariage aurait été porté à la connaissance des services du ministre chargé des naturalisations à une date antérieure au 28 février 2017. En outre, si la requérante soutient avoir indiqué lors de son entretien d'assimilation à l'agent de la préfecture être mariée, elle n'apporte aucun élément au soutien de cette allégation. Dans ces conditions, le décret attaqué, qui a été signé le 27 février 2019, a été pris avant l'expiration du délai de deux ans prévu par les dispositions de l'article 27-2 du code civil.<br/>
<br/>
              5.	En deuxième lieu, l'article 21-16 du code civil dispose que : " Nul ne peut être naturalisé s'il n'a en France sa résidence au moment de la signature du décret de naturalisation ". Il résulte de ces dispositions que la demande de naturalisation n'est pas recevable lorsque l'intéressé n'a pas fixé en France de manière durable le centre de ses intérêts. Pour apprécier si cette condition est remplie, l'autorité administrative peut notamment prendre en compte, sous le contrôle du juge de l'excès de pouvoir, la situation familiale en France de l'intéressé à la date du décret lui accordant la nationalité française. Par suite, alors même qu'elle remplirait les autres conditions requises à l'obtention de la nationalité française, la circonstance que l'intéressée ait dissimulé s'être mariée à l'étranger avec un ressortissant burkinabè avant l'instruction de sa demande de naturalisation était de nature à modifier l'appréciation qui a été portée par l'autorité administrative sur la fixation du centre de ses intérêts.<br/>
<br/>
              6.	Il ressort des pièces du dossier que Mme B... s'est mariée le 27 décembre 2014 à Ouagadougou (Burkina Faso) avec un ressortissant burkinabè résidant habituellement à l'étranger. Ce mariage a constitué un changement de sa situation familiale que l'intéressée aurait dû porter à la connaissance des services instruisant sa demande de naturalisation, comme elle s'y était engagée en déposant sa demande de naturalisation, ce qu'elle n'a pas fait avant que lui soit accordée la nationalité française. Si Mme B... soutient qu'elle était de bonne foi et qu'elle a informé l'administration de l'existence de son mariage lors de l'établissement des actes d'état civil relatifs à ses enfants, elle ne fait état d'aucune circonstance qui l'aurait mise dans l'impossibilité de faire part de sa situation familiale avant l'intervention du décret lui accordant la nationalité française. L'intéressée, qui maîtrise la langue française, ainsi qu'il ressort du compte-rendu d'assimilation du 16 juin 2015, ne pouvait se méprendre ni sur la teneur des indications devant être portées à la connaissance de l'administration chargée d'instruire sa demande, ni sur la portée de la déclaration sur l'honneur qu'elle a signée. Dans ces conditions, Mme B... doit être regardée comme ayant volontairement dissimulé le changement de sa situation familiale. Par suite, en rapportant sa naturalisation, le ministre de l'intérieur n'a pas fait une inexacte application des dispositions de l'article 27-2 du code civil.<br/>
<br/>
              7.	En troisième lieu, un décret qui rapporte pour fraude un décret de naturalisation est, par lui-même, dépourvu d'effet sur la présence sur le territoire français de celui qu'il vise, comme sur ses liens avec les membres de sa famille. Ainsi, les stipulations de l'article 3-1 de la convention internationale relative aux droits de l'enfant ne peuvent être utilement invoquées à l'appui des conclusions dirigées contre le décret attaqué. En revanche, un tel décret affecte un élément constitutif de l'identité de la personne concernée et est ainsi susceptible de porter atteinte au droit au respect de sa vie privée. En l'espèce, toutefois, eu égard à la date à laquelle il est intervenu et aux motifs qui le fondent, le décret attaqué ne peut être regardé comme ayant porté une atteinte disproportionnée au droit au respect de la vie privée de<br/>
 Mme B... garanti par l'article 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales.<br/>
<br/>
              8.	Il résulte de ce qui précède, sans qu'il soit besoin de statuer sur la recevabilité de la requête, que Mme B... n'est pas fondée à demander l'annulation pour excès de pouvoir du décret du 27 février 2019 par lequel le premier ministre a rapporté le décret du 12 juillet 2016 qui lui avait accordé la nationalité française. Ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative ne peuvent, en conséquence, qu'être rejetées.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : La requête de Mme B... est rejetée.<br/>
<br/>
Article 2 : La présente décision sera notifiée à Mme A... B... et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
