<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037080611</ID>
<ANCIEN_ID>JG_L_2018_06_000000420739</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/08/06/CETATEXT000037080611.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Formation spécialisée, 18/06/2018, 420739</TITRE>
<DATE_DEC>2018-06-18</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>420739</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Formation spécialisée</FORMATION>
<TYPE_REC>Appréciation de la légalité</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Emmanuelle Prada Bordenave</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Gilles Pellissier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CEFSP:2018:420739.20180618</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête du 15 mai 2018, enregistrée le 18 mai 2018 au secrétariat du contentieux du Conseil d'Etat, le procureur de la République près le tribunal de grande instance de X... demande au Conseil d'Etat de vérifier la régularité de la mise en oeuvre d'une technique de renseignement.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de procédure pénale ;<br/>
              - le code de la sécurité intérieure ; <br/>
              - la loi n° 2015-912 du 24 juillet 2015 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir convoqué à une séance à huis-clos, le Premier ministre et la Commission nationale de contrôle des techniques de renseignement, qui ont été mis à même de prendre la parole avant les conclusions ;<br/>
<br/>
              Et après avoir entendu en séance :<br/>
              - le rapport de Mme Emmanuelle Prada Bordenave, conseillère d'Etat, <br/>
              - et, hors la présence des parties, les conclusions de M. Gilles Pellissier, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes du premier alinéa de l'article L. 821-1 du code de la sécurité intérieure : " La mise en oeuvre sur le territoire national des techniques de recueil de renseignement mentionnées au titre V du présent livre est soumise à autorisation préalable du Premier ministre, délivrée après avis de la Commission nationale de contrôle des techniques de renseignement ". Aux termes de l'article L. 833-1 du même code : " La Commission nationale de contrôle des techniques de renseignement veille à ce que les techniques de recueil de renseignement soient mises en oeuvre sur le territoire national conformément au présent livre ". Elle exerce sa mission dans les conditions prévues aux articles L. 833-2 à L. 833-11 du même code et peut, notamment, en vertu de l'article L. 833-4, procéder " de sa propre initiative (...) au contrôle de la ou des techniques invoquées en vue de vérifier qu'elles ont été ou sont mises en oeuvre dans le respect du présent livre ".<br/>
<br/>
              2. L'article L. 841-1 du code de la sécurité intérieure dispose que : " Sous réserve des dispositions particulières prévues à l'article L. 854-9 du présent code, le Conseil d'Etat est compétent pour connaître, dans les conditions prévues au chapitre III bis du titre VII du livre VII du code de justice administrative, des requêtes concernant la mise en oeuvre des techniques de renseignement mentionnées au titre V du présent livre. / (...) / Lorsqu'une juridiction administrative ou une autorité judiciaire est saisie d'une procédure ou d'un litige dont la solution dépend de l'examen de la régularité d'une ou de plusieurs techniques de recueil de renseignement, elle peut, d'office ou sur demande de l'une des parties, saisir le Conseil d'Etat à titre préjudiciel. Il statue dans le délai d'un mois à compter de sa saisine ". Ces dispositions s'appliquent aux techniques de renseignement mises en oeuvre à compter de la date de leur entrée en vigueur, y compris celles qui, initiées avant cette date, ont continué à être mises en oeuvre après.<br/>
<br/>
              3. Selon l'article L. 773-2 du code de justice administrative, les renvois présentés sur le fondement de l'article L. 841-1 du code de la sécurité intérieure sont portés, sous les réserves prévues à cet article, devant une formation spécialisée du Conseil d'Etat, dont les membres et le rapporteur public sont habilités ès qualités au secret de la défense nationale. Aux termes du dernier alinéa de cet article : " Dans le cadre de l'instruction de la requête, les membres de la formation de jugement et le rapporteur public sont autorisés à connaître de l'ensemble des pièces en possession de la Commission nationale de contrôle des techniques de renseignement ou des services mentionnés à l'article L. 811-2 du code de la sécurité intérieure et ceux désignés par le décret en Conseil d'Etat mentionné à l'article L. 811-4 du même code et utiles à l'exercice de leur office, y compris celles protégées au titre de l'article 413-9 du code pénal ". Aux termes de l'article L. 773-6 du même code : " Lorsque la formation de jugement constate l'absence d'illégalité dans la mise en oeuvre d'une technique de recueil de renseignement, la décision indique (...) à la juridiction de renvoi qu'aucune illégalité n'a été commise, sans confirmer ni infirmer la mise en oeuvre d'une technique ". Enfin, aux termes de son article L. 773-7 : " Lorsque la formation de jugement constate qu'une technique de recueil de renseignement est ou a été mise en oeuvre illégalement ou qu'un renseignement a été conservé illégalement, elle peut annuler l'autorisation et ordonner la destruction des renseignements irrégulièrement collectés. / Sans faire état d'aucun élément protégé par le secret de la défense nationale, elle informe (...) la juridiction de renvoi qu'une illégalité a été commise (...) ". <br/>
<br/>
              4. Saisi d'une plainte pour atteinte à la vie privée, le procureur de la République près le tribunal de grande instance de X... demande au Conseil d'Etat de vérifier la régularité de la technique de renseignement mentionnée dans cette plainte.<br/>
<br/>
              5. La formation spécialisée a examiné les éléments fournis par la Commission nationale de contrôle des techniques de renseignement, qui a précisé l'ensemble des vérifications auxquelles elle avait procédé, et par le Premier ministre. A l'issue de cet examen, il y a lieu de répondre au procureur de la République près le tribunal de grande instance de X... que la vérification qu'il a sollicitée a été effectuée et que l'examen de la technique de renseignement sur laquelle il a saisi le Conseil d'Etat n'a révélé aucune illégalité. <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La vérification à laquelle il a été procédé à la demande du procureur de la République près le tribunal de grande instance de X... n'a révélé aucune illégalité.<br/>
Article 2 : La présente décision sera notifiée au procureur de la République près le tribunal de grande instance de X..., au Premier ministre et à la Commission nationale de contrôle des techniques de renseignement.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">17-04 COMPÉTENCE. COMPÉTENCES CONCURRENTES DES DEUX ORDRES DE JURIDICTION. - SAISINE DE LA FORMATION SPÉCIALISÉE DU CONSEIL D'ETAT À TITRE PRÉJUDICIEL (ART. L. 841-1 DU CSI) - 1) POSSIBILITÉ POUR UN PROCUREUR DE LA RÉPUBLIQUE DE SAISIR CETTE FORMATION À TITRE PRÉJUDICIEL DÈS LE STADE DE L'ENQUÊTE PRÉLIMINAIRE - EXISTENCE (SOL. IMPL.) - 2) OFFICE DE LA FORMATION SPÉCIALISÉE SAISIE D'UNE TELLE DEMANDE - VÉRIFICATION À ACCOMPLIR - MENTIONS DEVANT FIGURER DANS LES MOTIFS ET DISPOSITIF DE SA DÉCISION [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">49-03 POLICE. ÉTENDUE DES POUVOIRS DE POLICE. - SAISINE DE LA FORMATION SPÉCIALISÉE DU CONSEIL D'ETAT À TITRE PRÉJUDICIEL (ART. L. 841-1 DU CSI) - 1) POSSIBILITÉ POUR UN PROCUREUR DE LA RÉPUBLIQUE DE SAISIR CETTE FORMATION À TITRE PRÉJUDICIEL DÈS LE STADE DE L'ENQUÊTE PRÉLIMINAIRE - EXISTENCE (SOL. IMPL.) - 2) OFFICE DE LA FORMATION SPÉCIALISÉE SAISIE D'UNE TELLE DEMANDE - VÉRIFICATION À ACCOMPLIR - MENTIONS DEVANT FIGURER DANS LES MOTIFS ET DISPOSITIF DE SA DÉCISION [RJ1].
</SCT>
<ANA ID="9A"> 17-04 1) Un procureur de la République peut, sur le fondement de l'article L. 841-1 du code de la sécurité intérieure (CSI), saisir la formation spécialisée à titre préjudiciel dès le stade de l'enquête préliminaire.... ,,2) Saisine de la formation spécialisée du Conseil d'Etat à titre préjudiciel par un procureur de la République, sur le fondement de l'article L. 841-1 du CSI, dans le cadre d'une plainte pour atteinte à la vie privée, d'une demande tendant à la vérification de la régularité de la technique de renseignement mentionnée dans cette plainte.... ,,Après examen des éléments fournis par la Commission nationale de contrôle des techniques de renseignement, qui a précisé l'ensemble des vérifications auxquelles elle avait procédé, et par le Premier ministre, il y a lieu pour la formation spécialisée de répondre au procureur de la République, dans les motifs et le dispositif de sa décision, que la vérification qu'il a sollicitée a été effectuée et, le cas échéant, que l'examen de la technique de renseignement sur laquelle il a saisi le Conseil d'Etat n'a révélé aucune illégalité, sans autre précision.</ANA>
<ANA ID="9B"> 49-03 1) Un procureur de la République peut, sur le fondement de l'article L. 841-1 du code de la sécurité intérieure (CSI), saisir la formation spécialisée à titre préjudiciel dès le stade de l'enquête préliminaire.... ,,2) Saisine de la formation spécialisée du Conseil d'Etat à titre préjudiciel par un procureur de la République, sur le fondement de l'article L. 841-1 du CSI, dans le cadre d'une plainte pour atteinte à la vie privée, d'une demande tendant à la vérification de la régularité de la technique de renseignement mentionnée dans cette plainte.... ,,Après examen des éléments fournis par la Commission nationale de contrôle des techniques de renseignement, qui a précisé l'ensemble des vérifications auxquelles elle avait procédé, et par le Premier ministre, il y a lieu pour la formation spécialisée de répondre au procureur de la République, dans les motifs et le dispositif de sa décision, que la vérification qu'il a sollicitée a été effectuée et, le cas échéant, que l'examen de la technique de renseignement sur laquelle il a saisi le Conseil d'Etat n'a révélé aucune illégalité, sans autre précision.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., CE, 19 octobre 2016,,, n° 396958, p. 433.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
