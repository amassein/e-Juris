<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034209366</ID>
<ANCIEN_ID>JG_L_2017_03_000000394046</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/20/93/CETATEXT000034209366.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème chambre, 17/03/2017, 394046, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-03-17</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>394046</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DE CHAISEMARTIN, COURJON ; SCP GATINEAU, FATTACCINI</AVOCATS>
<RAPPORTEUR>Mme Ophélie Champeaux</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emilie Bokdam-Tognetti</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2017:394046.20170317</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société par actions simplifiée (SAS) Socoter a demandé au tribunal administratif de la Polynésie française de prononcer la décharge des impositions supplémentaires et des pénalités qui lui ont été réclamées, pour un montant total de 120 727 898 F CFP, par des avis d'imposition émis le 10 octobre 2011. Par un jugement n° 1300036 du 11 juin 2013, le tribunal administratif de la Polynésie Française l'a déchargée des cotisations supplémentaires d'impôt sur les sociétés ainsi que des pénalités pour manoeuvres frauduleuses et intérêts auxquels elle a été assujettie au titre de l'année 2006 et a rejeté le surplus des conclusions de sa demande.<br/>
<br/>
              Par un arrêt n° 13PA03709 du 10 juillet 2015, la cour administrative d'appel de Paris a, d'une part, sur appel de la Polynésie française, annulé l'article 1er de ce jugement et remis à la charge de la SAS Socoter les cotisations supplémentaires d'impôt sur les sociétés ainsi que les pénalités pour manoeuvres frauduleuses et intérêts auxquelles elle avait été assujettie au titre de l'année 2006 et, d'autre part, rejeté les conclusions de l'appel incident de la SAS Socoter.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 13 octobre 2015 et 13 janvier 2016 au secrétariat du contentieux du Conseil d'Etat, la SAS Socoter demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de la Polynésie française et de faire droit à son appel incident ;<br/>
<br/>
              3°) de mettre à la charge de la Polynésie française la somme de 4 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la loi organique n° 2004-192 du 27 février 2004 portant statut d'autonomie de la Polynésie française ;<br/>
              - le code des impôts de la Polynésie française ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Ophélie Champeaux, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Emilie Bokdam-Tognetti, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Gatineau, Fattaccini, avocat de la société Socoter et à la SCP de Chaisemartin, Courjon, avocat de la Présidence de la Polynésie française ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond qu'à l'issue d'une vérification de la comptabilité de la société Socoter, l'administration fiscale a remis en cause le crédit d'impôt dont cette société avait bénéficié, en application des articles 374-1 et 375-1 du code des impôts de Polynésie française, au titre du financement d'un projet de construction d'une résidence hôtelière à Papeete. En conséquence, des cotisations supplémentaires d'impôt sur les sociétés, assorties d'une majoration de 80 % pour manoeuvres frauduleuses, ont été mises à sa charge au titre des années 2006, 2007 et 2008. Par un jugement du 11 juin 2013, le tribunal administratif de la Polynésie française a déchargé la société des impositions et pénalités mises à sa charge au titre de l'année 2006 et a rejeté le surplus des conclusions de sa demande. Saisie en appel par la Polynésie française et par un appel incident de la société Socoter, la cour administrative d'appel de Paris a, par l'arrêt attaqué du 10 juillet 2015, d'une part, annulé l'article 1er du jugement du tribunal administratif et remis à la charge de la société Socoter les impositions et pénalités dont le tribunal avait prononcé la décharge et, d'autre part, rejeté les conclusions de l'appel incident de la société. Par un moyen qu'elle a relevé d'office, la cour a estimé que la demande présentée par la société Socoter devant le tribunal administratif était irrecevable faute d'avoir été précédée, conformément aux dispositions de l'article 611-2 du code des impôts de la Polynésie française, d'une réclamation adressée au président de la Polynésie française.<br/>
<br/>
              2. Aux termes du second alinéa de l'article 4 de la loi du 12 avril 2000 relative aux droits des citoyens dans leurs relations avec les administrations, repris à l'article L. 212-1 du code des relations entre le public et l'administration : " Toute décision prise par l'une des autorités administratives mentionnées à l'article 1er comporte, outre la signature de son auteur, la mention, en caractères lisibles, du prénom, du nom et de la qualité de celui-ci ". S'agissant d'une autorité administrative de caractère collégial, il est satisfait aux exigences découlant de ces dispositions dès lors que la décision que prend cette autorité porte la signature de son président, accompagnée des mentions, en caractères lisibles, prévues par cet article. <br/>
<br/>
              3. Il ressort des pièces du dossier soumis aux juges du fond que la décision de la Polynésie française de relever appel du jugement du tribunal a été formalisée par un arrêté du 1er octobre 2013 du vice-président, ministre de l'économie, des finances et du budget, de la fonction publique,  signée " Pour le vice-président, ministre de l'économie, des finances et du budget, de la fonction publique ", par le ministre de l'éducation, de l'enseignement supérieur, de la jeunesse et des sports, M. A...B.... Par un courrier du 25 mars 2015, le greffe de la cour a demandé à l'avocat de la Polynésie française de produire l'arrêté de délégation de signature au profit de M. A...B...lui donnant compétence pour signer l'arrêté le désignant pour interjeter appel du jugement. En réponse, la Polynésie française s'est bornée à communiquer l'arrêté n° 390/PR du 17 mai 2013 relatif aux attributions du vice-président, l'arrêté n° 3956/VP du 21 mai 2013 portant délégation du pouvoir de l'ordonnateur, et l'arrêté n° 750/CM du 23 mai 2013 portant délégation de pouvoir du conseil des ministres.<br/>
<br/>
              4. En relevant, pour rejeter la fin de non-recevoir, tirée de l'incompétence de l'auteur de l'arrêté du 1er octobre 2013, soulevée par la société Socoter, que cet arrêté émane du vice-président de la Polynésie française alors même qu'il a été signé par un ministre, sans tirer les conséquences du défaut de justification de l'existence d'une délégation de signature à la date de l'acte, la cour a insuffisamment motivé son arrêt et commis une erreur de droit. <br/>
<br/>
              5. Le motif retenu au point 4 ci-dessus suffisant à entraîner l'annulation de l'arrêt attaqué, il n'est pas nécessaire de statuer sur les autres moyens du pourvoi.<br/>
<br/>
              6. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la Polynésie française une somme de 3 000 euros à verser à la société Socoter au titre de l'article L. 761-1 du code de justice administrative. Ces dispositions font, en revanche, obstacle à ce qu'une somme soit mise, à ce titre, à la charge de la société Socoter qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 10 juillet 2015 de la cour administrative d'appel de Paris est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Paris. <br/>
Article 3 : La Polynésie française versera à la société Socoter une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Les conclusions présentées par la Polynésie française sur le fondement de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée à la société Socoter et à la Polynésie française.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
