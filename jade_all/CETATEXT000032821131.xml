<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032821131</ID>
<ANCIEN_ID>JG_L_2016_06_000000385606</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/82/11/CETATEXT000032821131.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème - 10ème chambres réunies, 30/06/2016, 385606</TITRE>
<DATE_DEC>2016-06-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>385606</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème - 10ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GATINEAU, FATTACCINI</AVOCATS>
<RAPPORTEUR>Mme Marie-Gabrielle Merloz</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emilie Bokdam-Tognetti</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2016:385606.20160630</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés les 7 novembre 2014 et 13 mars 2015 au secrétariat du contentieux du Conseil d'Etat, la caisse régionale de crédit agricole mutuel de la Réunion demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler la lettre du 8 septembre 2014 du secrétaire général de l'Autorité de contrôle prudentiel et de résolution (ACPR) relative à la désignation de M. Karl Techer, président de son conseil d'administration, en qualité de " dirigeant effectif " ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code monétaire et financier ;<br/>
              - l'ordonnance n° 2014-158 du 20 février 2014 ;<br/>
              - le règlement n° 96-16 du 20 décembre 1996 modifié du comité de la réglementation bancaire et financière ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Marie-Gabrielle Merloz, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Emilie Bokdam-Tognetti, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Gatineau, Fattaccini, avocat de l'Autorité de contrôle prudentiel et de résolution ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 16 juin 2016, présentée par la caisse régionale de crédit agricole mutuel de la Réunion ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes du deuxième alinéa de l'article L. 511-13 du code monétaire et financier, dans sa rédaction applicable au litige : " La direction effective de l'activité des établissements de crédit ou des sociétés de financement doit être assurée par deux personnes au moins " ; qu'aux termes de l'article 9 du règlement du 20 décembre 1996 du comité de la réglementation bancaire et financière (CRBF) relatif aux modifications de situation des établissements de crédit, des sociétés de financement et des entreprises d'investissement autres que les sociétés de gestion des portefeuilles modifié : " La désignation de toute nouvelle personne appelée, en application de l'article L. 511-13 ou L. 532-2 du code monétaire et financier, à assurer la direction effective de l'activité d'une entreprise assujettie doit être immédiatement déclarée à l'Autorité de contrôle prudentiel et de résolution. Cette déclaration est accompagnée de tous éléments permettant d'apprécier l'honorabilité et l'expérience de la personne concernée./(...) lorsque l'entreprise assujettie est un établissement de crédit, l'Autorité de contrôle prudentiel et de résolution dispose également d'un délai d'un mois, à compter soit de la déclaration qui lui est faite en application de l'alinéa premier du présent article (...) pour faire savoir au déclarant que la désignation n'est pas compatible avec l'agrément précédemment délivré " ; qu'en vertu de l'article L. 511-15 du code monétaire et financier, l'ACPR peut prononcer d'office le retrait d'agrément notamment si l'entreprise ne remplit plus les conditions auxquelles était subordonné son agrément ;<br/>
<br/>
              2. Considérant qu'il ressort des pièces du dossier qu'en application des dispositions précitées de l'article 9 du règlement du 20 décembre 1996 du CRBF, la caisse régionale de crédit agricole mutuel de la Réunion a, le 17 juillet 2014, déclaré à l'ACPR avoir désigné M. Karl Techer, président de son conseil d'administration, en tant que " dirigeant effectif " à compter du 27 mars 2014 ; que la caisse régionale de crédit agricole mutuel de la Réunion demande l'annulation de la lettre du 8 septembre 2014 par lequel le secrétaire général de l'ACPR a répondu à cette demande ;<br/>
<br/>
              3. Considérant qu'il résulte des termes mêmes de la lettre du 8 septembre 2014 que le secrétaire général de l'ACPR a répondu à la caisse régionale que, dans sa " position " 2014-P-07 du 20 juin 2014, l'Autorité rappelé que, compte tenu du nouveau cadre juridique issu de la transposition de la directive 2013/36/UE du 26 juin 2013 par l'ordonnance du 20 février 2014, la fonction de " dirigeant effectif " est exercée, au sein des établissements de crédit constitués sous la forme d'une société anonyme à conseil d'administration, d'une part, par le directeur général et, d'autre part, par le directeur général délégué ou, si la situation particulière de l'établissement fait obstacle à la désignation d'un directeur général délégué pour des raisons exposées à l'ACPR, par un cadre dirigeant disposant des pouvoirs nécessaires, attribués par le conseil d'administration, à l'exercice d'une direction effective de l'activité de l'établissement, et que le président du conseil d'administration ne peut être désigné comme " dirigeant effectif " de l'établissement, sauf dans les cas où il est expressément autorisé par l'Autorité à cumuler ses fonctions avec celles de directeur général ; que le secrétaire général de l'ACPR en a déduit que la direction effective de la caisse régionale n'était actuellement assurée que par une seule personne, son directeur général, et a invité cette caisse à communiquer à l'Autorité le dossier de la personne qu'elle entendait désigner comme deuxième " dirigeant effectif ", soit, en principe, le directeur général délégué ou, en cas de situation particulière le justifiant, un cadre dirigeant disposant des pouvoirs nécessaires, attribués par le conseil d'administration, à l'exercice d'une direction effective de l'établissement ; que cette lettre ne présente pas le caractère d'une décision prise sur le fondement de l'article 9 précité du règlement du 20 décembre 1996 du CRBF, en vertu duquel l'ACPR dispose d'un délai d'un mois pour faire savoir au déclarant que la désignation n'est pas compatible avec l'agrément précédemment délivré, ni sur celui des pouvoirs de police administrative que détient l'ACPR, tel celui d'adresser la mise en demeure prévue à l'article L. 612-31 du code monétaire et financier ; que, dès lors, l'invitation adressée à la caisse régionale par le secrétaire général de l'ACPR est, par elle-même, dépourvue de caractère contraignant ; qu'il suit de là que la lettre du 8 septembre 2014 ne peut être regardée comme une décision faisant grief susceptible de faire l'objet d'un recours pour excès de pouvoir ; que la requête de la caisse régionale de crédit agricole de la Réunion est, par suite, irrecevable ;<br/>
<br/>
              4. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat (ACPR), qui n'est pas, dans la présente instance, la partie perdante ; que, dans les circonstances de l'espèce, il n'y a pas lieu de faire droit aux conclusions présentées par l'Etat (ACPR) à ce même titre ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
Article 1er : La requête de la caisse régionale de crédit agricole mutuel de la Réunion est rejetée. <br/>
Article 2 : Les conclusions de l'Etat (ACPR) présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La présente décision sera notifiée à la caisse régionale de crédit agricole mutuel de la Réunion et à l'Autorité de contrôle prudentiel et de résolution.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">13-027 CAPITAUX, MONNAIE, BANQUES. - LETTRE DU SG DE L'ACPR RAPPELANT UNE POSITION DE L'ACPR ET INVITANT UN ÉTABLISSEMENT DE CRÉDIT À AGIR - DÉCISION SUSCEPTIBLE DE FAIRE L'OBJET D'UN REP - ABSENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-01-01-02 PROCÉDURE. INTRODUCTION DE L'INSTANCE. DÉCISIONS POUVANT OU NON FAIRE L'OBJET D'UN RECOURS. ACTES NE CONSTITUANT PAS DES DÉCISIONS SUSCEPTIBLES DE RECOURS. - INCLUSION - LETTRE DU SG DE L'ACPR RAPPELANT UNE POSITION DE L'ACPR ET INVITANT UN ÉTABLISSEMENT DE CRÉDIT À AGIR.
</SCT>
<ANA ID="9A"> 13-027 Lettre du secrétaire général (SG) de l'Autorité de contrôle prudentiel et de résolution (ACPR) rappelant une position par laquelle l'ACPR expose son interprétation de la notion de direction effective de l'établissement au sens de l'article L. 511-13 du code monétaire et financier et invitant un établissement de crédit à communiquer à l'ACPR le dossier de la personne qu'elle entend désigner comme deuxième  dirigeant effectif .... ,,Cette lettre ne présente pas le caractère d'une décision prise sur le fondement de l'article 9 précité du règlement n° 96-16 du 20 décembre 1996 modifié du comité de la réglementation bancaire et financière  (CRBF), en vertu duquel l'ACPR dispose d'un délai d'un mois pour faire savoir au déclarant que la désignation n'est pas compatible avec l'agrément précédemment délivré, ni sur celui des pouvoirs de police administrative que détient l'ACPR, tel celui d'adresser la mise en demeure prévue à l'article L. 612-31 du code monétaire et financier. Dès lors, l'invitation adressée à l'établissement de crédit par le secrétaire général de l'ACPR est, par elle-même, dépourvue de caractère contraignant.... ,,Il suit de là que la lettre du secrétaire général de l'ACPR ne peut être regardée comme une décision faisant grief susceptible de faire l'objet d'un recours pour excès de pouvoir.</ANA>
<ANA ID="9B"> 54-01-01-02 Lettre du secrétaire général (SG) de l'Autorité de contrôle prudentiel et de résolution (ACPR) rappelant une position par laquelle l'ACPR expose son interprétation de la notion de direction effective de l'établissement au sens de l'article L. 511-13 du code monétaire et financier et invitant un établissement de crédit à communiquer à l'ACPR le dossier de la personne qu'elle entend désigner comme deuxième  dirigeant effectif .... ,,Cette lettre ne présente pas le caractère d'une décision prise sur le fondement de l'article 9 précité du règlement n° 96-16 du 20 décembre 1996 modifié du comité de la réglementation bancaire et financière  (CRBF), en vertu duquel l'ACPR dispose d'un délai d'un mois pour faire savoir au déclarant que la désignation n'est pas compatible avec l'agrément précédemment délivré, ni sur celui des pouvoirs de police administrative que détient l'ACPR, tel celui d'adresser la mise en demeure prévue à l'article L. 612-31 du code monétaire et financier. Dès lors, l'invitation adressée à l'établissement de crédit par le secrétaire général de l'ACPR est, par elle-même, dépourvue de caractère contraignant.... ,,Il suit de là que la lettre du secrétaire général de l'ACPR ne peut être regardée comme une décision faisant grief susceptible de faire l'objet d'un recours pour excès de pouvoir.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
