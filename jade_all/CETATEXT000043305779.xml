<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043305779</ID>
<ANCIEN_ID>JG_L_2021_03_000000450375</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/30/57/CETATEXT000043305779.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 22/03/2021, 450375, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-03-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>450375</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2021:450375.20210322</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme D... A... et M. E... C... B... ont demandé au juge des référés du tribunal administratif de Nantes, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, d'annuler la décision de refus de visa opposée par l'autorité consulaire française à Dakar à la demande de M. C... B... et d'enjoindre au ministre de l'intérieur de délivrer à celui-ci un visa lui permettant de célébrer son mariage en France avec Mme A... et d'y réaliser son projet d'études ou, à titre subsidiaire, de procéder à un réexamen de sa demande. Par une ordonnance n° 2101676 du 18 février 2021, le juge des référés du tribunal administratif de Nantes a rejeté leur demande.<br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés les 4 et 13 mars 2021 au secrétariat du contentieux du Conseil d'Etat, Mme A... et M. C... B... demandent au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) d'enjoindre au ministre de l'intérieur de délivrer à M. C... B... un visa lui permettant d'entrer sur le territoire français et d'y séjourner jusqu'à la célébration de son mariage, dans un délai de 24 heures à compter de la notification de l'ordonnance à venir, sous astreinte de 500 euros par jour de retard ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat le versement de la somme de 5 000 euros en application de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Ils soutiennent que :<br/>
              - Mme A... justifie d'un intérêt lui donnant qualité pour agir en ce que la décision de refus de délivrance d'un visa à M. C... B... lui porte préjudice en sa qualité de fiancée et de future épouse de l'intéressé ;<br/>
              - la condition d'urgence est satisfaite dès lors que la décision de refus d'un visa en qualité d'étudiant à M. C... B... ferait obstacle à la célébration de leur mariage prévu le 27 mars 2001, lequel ne pourra être ni différé ni célébré au Cap-Vert compte tenu des préparatifs engagés de longue date en France et de la situation sanitaire actuelle ;<br/>
              - le juge des référés du tribunal administratif de Nantes a entaché son ordonnance d'une erreur manifeste d'appréciation en estimant, en dépit de la proximité de la date à laquelle doit avoir lieu la célébration de leur mariage, que la condition d'urgence n'était pas remplie ;<br/>
              - cette condition d'urgence doit également être appréciée au regard de la situation de Mme A..., qui se trouve dans une situation psychologique fragile à la suite d'une interruption de grossesse et dont l'état de santé se dégrade ;<br/>
              - le délai qui a séparé la décision de refus de délivrance d'un visa de la saisine du juge des référés du tribunal administratif de Nantes s'explique par les nouvelles démarches entreprises auprès des autorités consulaires pour compléter le dossier de demande de visa et obtenir un réexamen de leur situation ;  <br/>
              - la décision de refus de visa est entachée d'irrégularité, dès lors qu'elle ne comporte pas le nom de l'agent qui en est l'auteur, en méconnaissance des dispositions de l'article L. 212-1 du code des relations entre le public et l'administration ; <br/>
              - la décision de refus de visa est entachée d'une illégalité en raison de l'atteinte disproportionnée à la liberté matrimoniale et au droit de fonder une famille et de mener une vie familiale normale, alors que le caractère sérieux de leur projet de vie commune est attesté par de nombreux témoignages circonstanciés et concordants. <br/>
<br/>
              Par un mémoire en défense, enregistré le 11 mars 2021, le ministre de l'intérieur conclut au rejet de la requête. Il fait valoir que la condition d'urgence n'est pas remplie et qu'il n'est porté aucune atteinte grave et manifestement illégale aux libertés fondamentales invoquées. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Les parties ont été informées, sur le fondement de l'article 3 de l'ordonnance n° 2020-1402 du 18 novembre 2020 portant adaptation des règles applicables aux juridictions administratives, de ce qu'aucune audience ne se tiendrait et de ce que la clôture de l'instruction serait fixée le lundi 15 mars à 12 heures. <br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. Le juge des référés se prononce dans un délai de quarante-huit heures ".<br/>
<br/>
              2. M. E... C... B..., ressortissant cap verdien né le 24 décembre 1991, a fait la connaissance de Mme D... A..., ressortissante française, lors d'un séjour qu'elle effectué au Cap-Vert en 2018 et s'est fiancé avec elle en décembre 2019. Les intéressés ont entamé en octobre 2020 des démarches auprès de la mairie de Chamalières en vue de la célébration de leur mariage, prévu le 27 mars 2021. Le 23 novembre 2020, M. C... B... a déposé une demande de visa de long séjour en qualité d'étudiant en langue française à l'université de Clermont-Auvergne, rejetée par décision de l'autorité consulaire française à Dakar du 16 décembre 2020. Mme A... et M. C... B... relèvent appel de l'ordonnance du 18 février 2021 par laquelle le juge des référés du tribunal administratif de Nantes, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, a rejeté leur demande tendant à l'annulation de la décision de refus de délivrance d'un visa et à ce qu'il soit enjoint au ministre de l'intérieur de délivrer à M. C... B... un visa lui permettant d'entrer sur le territoire français en vue de la célébration de son mariage. <br/>
<br/>
              3. En distinguant les procédures prévues par les articles L. 521-1 et L. 521-2 du code de justice administrative, le législateur a entendu répondre à des situations différentes. Les conditions auxquelles est subordonnée l'application de ces dispositions ne sont pas les mêmes, non plus que les pouvoirs dont dispose le juge des référés. En particulier, le requérant qui saisit le juge des référés sur le fondement des dispositions de l'article L. 521-2 du code de justice administrative doit justifier des circonstances particulières caractérisant la nécessité pour lui de bénéficier à très bref délai d'une mesure de la nature de celles qui peuvent être ordonnées sur le fondement de cet article. Sauf circonstances particulières, le refus des autorités consulaires de délivrer un visa d'entrée en France ne constitue pas une situation d'urgence caractérisée rendant nécessaire l'intervention dans les quarante-huit heures du juge des référés.<br/>
<br/>
              4. Pour établir l'existence d'une situation d'urgence justifiant que le juge des référés fasse usage des pouvoirs qu'il tient de l'article L. 521-2 du code de justice administrative, les requérants font valoir que la décision de refus de délivrance d'un visa prise par les autorités consulaires porterait une atteinte grave à leur liberté de se marier et de fonder une famille et à leur droit à mener une vie familiale normale, alors que les témoignages circonstanciés versés au dossier établissent la réalité et la stabilité de leur relation affective et le caractère sérieux de leur projet de vie commune. <br/>
<br/>
              5. Si Mme A... et M. C... B... soutiennent, d'une part, que les nouvelles démarches en vue de l'obtention d'un visa ou d'un laisser-passer consulaire entreprises postérieurement à la décision de refus qui leur a été notifiée n'ont pu aboutir en raison de la fermeture des frontières liée à la situation actuelle crise sanitaire, d'autre part, que la séparation prolongée du couple dégrade l'état de santé physique et mental de Mme A..., ces circonstances, pour regrettables qu'elles soient, ne sont pas, à elles seules, de nature à caractériser une situation d'urgence pouvant justifier qu'une mesure visant à sauvegarder une liberté fondamentale doive être prise dans les 48 heures. Au demeurant, il résulte de l'instruction que la demande de visa formée auprès de l'autorité consulaire française à Dakar tendait à l'obtention d'un visa de long séjour en qualité d'étudiant et mentionnait pour unique motif le projet de M. C... B... de suivre des études en langue française à l'université de Clermont-Auvergne, sans faire état de son mariage prévu avec Mme A... le 27 mars 2021.<br/>
<br/>
              6. Il résulte de ce qui précède que, sans qu'il soit besoin de se prononcer sur la condition tenant à l'existence d'une atteinte grave et manifestement illégale à une liberté fondamentale, Mme A... et M. C... B... ne sont pas fondés à soutenir que c'est à tort que, par l'ordonnance attaquée, le juge des référés du tribunal administratif de Nantes a rejeté leur demande présentée sur le fondement de l'article L. 521-2 du code de justice administrative. Il y a lieu, par suite, de rejeter la requête de Mme A... et de M. C... B..., y compris leurs conclusions tendant à l'application de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
              ------------------<br/>
<br/>
Article 1er : La requête de Mme A... et de M. C... B... est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à Mme D... A..., à M. E... C... B... et au ministre de l'intérieur. <br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
