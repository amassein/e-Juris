<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033843803</ID>
<ANCIEN_ID>JG_L_2017_01_000000406034</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/84/38/CETATEXT000033843803.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 04/01/2017, 406034, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-01-04</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>406034</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP SEVAUX, MATHONNET</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2017:406034.20170104</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et un mémoire complémentaire, enregistrés les 16 et 28 décembre 2016 au secrétariat du contentieux du Conseil d'Etat, Mme B...A...et le syndicat de la magistrature demandent au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative :<br/>
<br/>
              1°) de suspendre l'exécution de la décision implicite du garde des sceaux, ministre de la justice, de ne pas présenter la candidature de Mme A...à l'avis conforme du Conseil supérieur de la magistrature en vue de sa nomination à une fonction hors hiérarchie ; <br/>
<br/>
              2°) d'enjoindre au ministre de la justice de présenter, dans un délai d'un mois, la candidature de Mme A...à l'avis conforme du Conseil supérieur de la magistrature en vue de sa nomination à une fonction hors hiérarchie ;<br/>
<br/>
              3°) subsidiairement, d'enjoindre au ministre de la justice de procéder à un nouvel examen, dans un délai d'un mois, de la candidature de Mme A...à une fonction hors hiérarchie ;<br/>
<br/>
              4°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Ils soutiennent que : <br/>
              - la requête est recevable dès lors que la décision litigieuse lui fait grief ; <br/>
              - la condition d'urgence est remplie, dès lors que l'exécution de la décision contestée porte une atteinte grave et immédiate, d'une part, aux intérêts privés de Mme A... dès lors qu'elle impacte sa carrière alors qu'elle pourra prétendre à un départ à la retraite à la fin de l'année 2019, que le prochain mouvement des magistrats devrait avoir lieu en mars 2017 et qu'elle réitère la discrimination syndicale dont elle fait l'objet et, d'autre part, à l'intérêt général tenant au principe de continuité et de bon fonctionnement du service public ;<br/>
              - il existe un doute sérieux quant à la légalité de la décision attaquée ;<br/>
              - elle est entachée d'une erreur de droit en ce qu'elle constitue une discrimination syndicale ;<br/>
              - elle est entachée d'une erreur de droit en ce qu'elle se fonde sur des motifs étrangers à l'intérêt du service ;<br/>
              - elle méconnaît le principe de la présomption d'innocence ; <br/>
              - elle est entachée d'une erreur d'appréciation du ministre dès lors qu'il n'a pas pris en compte les aptitudes de Mme A...et leur adéquation aux exigences du bon fonctionnement de l'institution judiciaire ;<br/>
              - le Conseil supérieur de la magistrature estime sa candidature digne d'intérêt ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
- l'ordonnance organique n° 58-1270 du 22 décembre 1958 ;<br/>
- le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>Sans qu'il soit besoin de statuer sur la recevabilité de la requête en tant qu'elle émane du syndicat de la magistrature ;<br/>
<br/>
              1. Considérant qu'aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision " ; qu'il résulte de ces dispositions que le prononcé de la suspension d'un acte administratif est subordonné notamment à une condition d'urgence ; que l'urgence justifie la suspension de l'exécution d'un acte administratif lorsque celui-ci porte atteinte de manière suffisamment grave et immédiate à un intérêt public, à la situation du requérant ou aux intérêts qu'il entend défendre ; qu'en vertu de l'article L. 522-3 du même code, le juge des référés peut rejeter une requête par une ordonnance motivée, sans instruction contradictoire ni audience publique, lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable ou qu'elle est mal fondée ;<br/>
<br/>
              2. Considérant que Mme B...A..., conseillère à la cour d'appel d'Agen, s'est portée candidate, à plusieurs reprises depuis la fin de l'année 2015, à différents emplois " hors hiérarchie " ; que le ministre de la justice n'a pas proposé la requérante à l'avis conforme du Conseil supérieur de la magistrature au Conseil supérieur de la magistrature ; <br/>
<br/>
              3. Considérant que pour demander la suspension de la décision du garde des sceaux, ministre de la justice, de ne pas présenter la candidature de Mme A... à l'avis conforme du Conseil supérieur de la magistrature, fait notamment valoir, au titre de l'urgence, que la décision litigieuse porte atteinte aux intérêts de Mme A...du fait de ses conséquences  sur sa carrière, de la proximité de son départ à la retraite, prévu pour la fin de l'année 2019 et de l'échéance probable de la prochaine procédure nationale de mouvement des magistrats, en mars 2017 ; que, toutefois, le retard apporté à une éventuelle promotion, la discrimination alléguée, le trouble éventuel qui résulterait pour le bon fonctionnement du service public de la justice du refus litigieux, la situation de sous-effectif de la juridiction concernée qui résulterait de l'absence de nomination de Mme A...aux postes de président de chambre à la cour d'appel de Bordeaux et de premier vice-président du tribunal de grande instance de Toulouse, ne caractérisent pas en eux-mêmes une situation d'urgence au sens des dispositions de l'article L. 521-1 du code de justice administrative ; <br/>
<br/>
              4. Considérant qu'il résulte de ce qui précède que, faute de remplir l'une des conditions exigées par l'article L. 521-1 du code de justice administrative, la demande de suspension doit être rejetée selon la procédure prévue par l'article L. 522-3 du même code, y compris ses conclusions tendant à l'application de l'article L. 761-1 de ce code ;<br/>
<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de Mme A...et du syndicat de la magistrature est rejetée.<br/>
<br/>
Article 2 : La présente ordonnance sera notifiée à Mme B...A..., au syndicat de la magistrature et au garde des sceaux, ministre de la justice.<br/>
Copie en sera adressée à la SCP Sevaux et Mathonnet.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
