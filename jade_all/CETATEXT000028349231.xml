<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028349231</ID>
<ANCIEN_ID>JG_L_2013_12_000000369304</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/34/92/CETATEXT000028349231.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème et 2ème sous-sections réunies, 16/12/2013, 369304, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-12-16</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>369304</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème et 2ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SPINOSI</AVOCATS>
<RAPPORTEUR>M. Jean-Dominique Nuttens</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Bertrand Dacosta</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:369304.20131216</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu 1°, sous le n° 369304, la requête et le mémoire complémentaire, enregistrés les 12 et 25 juin 2013 au secrétariat du contentieux du Conseil d'Etat, présentés par la société des autoroutes Esterel, Côte d'Azur, Provence, Alpes (ESCOTA), dont le siège est 432 route de Cannes à Mandelieu (06210) ; la société ESCOTA demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret n° 2013-436 du 28 mai 2013 modifiant la redevance due par les sociétés concessionnaires d'autoroutes pour occupation du domaine public ; <br/>
<br/>
              2°) d'enjoindre à l'administration de produire dans un délai de 10 jours l'étude d'impact au vu de laquelle le décret n° 2013-436 a été pris ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu 2°, sous le n° 369384, la requête, enregistrée le 14 juin 2013 au secrétariat du contentieux du Conseil d'Etat, présentée par la société Arcour, dont le siège est 1 cours Ferdinand de Lesseps à Rueil-Malmaison (92851) ; la société Arcour demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le même décret ; <br/>
<br/>
              2°) d'enjoindre, avant dire droit, à l'Etat de produire les documents justifiant la majoration contestée, notamment l'étude d'impact, telle qu'elle a été présentée au Conseil d'Etat, ainsi que le rapport de présentation au Premier ministre, tel qu'il a été remis à ce dernier ;<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales et le premier protocole additionnel à cette convention ;<br/>
<br/>
              Vu la directive 1999/62/CE du Parlement européen et du Conseil du 17 juin 1999 ;<br/>
<br/>
              Vu le code général de la propriété des personnes publiques ;<br/>
<br/>
              Vu le code de la voirie routière ;<br/>
<br/>
              Vu la loi n° 93-122 du 29 janvier 1993 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Dominique Nuttens, Maître des Requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Bertrand Dacosta, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Spinosi, avocat de l'association des sociétés françaises d'autoroutes ;<br/>
<br/>
<br/>
<br/>1. Considérant que les requêtes des sociétés des autoroutes Esterel, Côte d'Azur, Provence, Alpes et Arcour sont dirigées contre un même décret ; qu'il y a lieu de les joindre pour statuer par une seule décision ; <br/>
<br/>
              2. Considérant que l'association des sociétés françaises d'autoroutes a intérêt à l'annulation du décret attaqué ; qu'ainsi, son intervention est recevable ; qu'elle ne peut toutefois se plaindre de ce que le mémoire de l'une des parties ne lui a pas été communiqué, dès lors que son intervention n'a pas pour effet de lui donner la qualité de partie à l'instance, ni utilement critiquer l'absence de communication de l'avis d'audience à l'une des parties ; que le moyen tiré de ce que l'avis d'audience ne lui aurait pas été communiqué manque en fait ;<br/>
<br/>
              3. Considérant qu'aux termes de l'article L. 2122-1 du code général de la propriété des personnes publiques : " Nul ne peut, sans disposer d'un titre l'y habilitant, occuper une dépendance du domaine public d'une personne publique mentionnée à l'article L. 1 ou l'utiliser dans des limites dépassant le droit d'usage qui appartient à tous " ; qu'aux termes de l'article L. 2125-1 du même code : " Toute occupation ou utilisation du domaine public d'une personne publique mentionnée à l'article L. 1 donne lieu au paiement d'une redevance (...) " ; qu'aux termes de l'article L. 2125-3 du même code: " La redevance due pour l'occupation ou l'utilisation du domaine public tient compte des avantages de toute nature procurés au titulaire de l'autorisation " ;<br/>
<br/>
              4. Considérant qu'il résulte de l'article R. 122-27 du code de la voirie routière que chaque société concessionnaire d'autoroutes verse annuellement à l'Etat une redevance pour occupation du domaine public, dont le montant est assis, d'une part, sur la valeur locative des voies autoroutières exploitées par le concessionnaire, d'autre part, sur le montant du chiffre d'affaires réalisé par celui-ci au titre de son activité de concessionnaire d'autoroutes sur le domaine public national ; que le décret contesté du 28 mai 2013 porte de 0,015 à 0,055 le coefficient multiplicateur du chiffre d'affaires permettant de calculer le montant de la redevance domaniale ; <br/>
<br/>
              5. Considérant, en premier lieu, que ce décret n'étant pas une mesure d'application de l'article R. 122-27 qu'il modifie sur ce seul point, les sociétés requérantes et l'association intervenante ne peuvent utilement contester, par la voie de l'exception, les autres dispositions de cet article ; que doit ainsi être écarté le moyen tiré de ce que la nature de l'occupation du domaine public par les sociétés concessionnaires d'autoroutes interdirait l'institution d'une redevance domaniale ; <br/>
<br/>
              6. Considérant, en deuxième lieu, que la société ESCOTA ne saurait sérieusement soutenir que les dispositions contestées sont contraires au principe de clarté et d'intelligibilité de la norme du fait de la hausse importante du montant de redevance résultant de la modification contestée de la formule de calcul de la redevance par l'adoption d'un coefficient multiplicateur du chiffre d'affaires plus élevé ;<br/>
<br/>
              7. Considérant, en troisième lieu, que les sociétés concessionnaires d'autoroutes occupent, pour l'exercice de la concession, le domaine public routier national que constitue l'autoroute et en retirent des avantages ; que la volonté d'assurer une meilleure exploitation du domaine public fait partie des motifs pouvant justifier une modification du montant de la redevance dans le respect des dispositions citées ci-dessus du code général de la propriété des personnes publiques ; qu'il ressort des pièces du dossier que, pour modifier la formule de calcul de la redevance d'occupation due par les sociétés concessionnaires d'autoroutes, basée sur la longueur de voie qui leur est concédée et leur chiffre d'affaires, le Premier ministre a pris en compte l'augmentation de ce dernier, plus rapide que celle de la redevance, constatée depuis son institution, en 1997 ; qu'il ne ressort pas des pièces du dossier qu'il ait, par la modification contestée, commis une erreur de droit ou une erreur manifeste dans l'appréciation qu'il doit porter, en fixant ce montant, sur l'intérêt du domaine public ainsi que sur les avantages retirés de son occupation ; qu'il a ainsi pu légalement modifier les règles permettant de déterminer le montant de la redevance en cause sans que celle-ci présente, du seul fait de sa révision, le caractère d'une imposition ;   <br/>
<br/>
              8. Considérant, en quatrième lieu, que si les sociétés requérantes soutiennent que le décret attaqué porte atteinte au principe d'égalité en n'opérant aucune distinction entre les différentes sociétés concessionnaires, ce principe n'implique pas que des entreprises se trouvant dans des situations différentes doivent être soumises à des régimes différents ; que l'élément contesté de la formule de calcul de la redevance permet au demeurant de prendre en considération les différences entre sociétés ; qu'enfin, les conditions dans lesquelles les tarifs des péages pourraient tenir compte de l'augmentation de la redevance d'occupation du domaine public sont sans incidence sur la légalité du décret attaqué ;<br/>
<br/>
              9. Considérant, en cinquième lieu, que le moyen tiré de la violation des dispositions de la directive du 17 juin 1999 relative à la taxation des poids lourds pour l'utilisation de certaines infrastructures, en ce qu'elle prohiberait l'inclusion dans les péages d'une redevance du type de celle ici en cause, est sans influence sur la légalité du décret attaqué qui n'a, en tout état de cause, pas cet objet ;<br/>
<br/>
              10. Considérant, en sixième lieu, que le décret attaqué, dépourvu de portée rétroactive, procède à une hausse de la redevance domaniale acquittée par les sociétés concessionnaires d'autoroutes destinée à mieux prendre en considération les avantages de toute nature retirés par celles-ci de l'occupation du domaine public ; qu'il ne ressort pas des pièces du dossier que l'application immédiate du décret attaqué porterait une atteinte excessive aux intérêts des sociétés concessionnaires et méconnaîtrait par suite le principe de sécurité juridique ; que le principe de confiance légitime, qui fait partie des principes généraux du droit de l'Union européenne, ne trouve à s'appliquer dans l'ordre juridique national que dans le cas où la situation juridique dont a à connaître le juge administratif français est régie par le droit de l'Union européenne ; que tel n'est pas le cas en l'espèce, nonobstant l'existence de règles de droit de l'Union relatives notamment aux péages autoroutiers ; <br/>
<br/>
              11. Considérant, en septième lieu, que les dispositions citées ci-dessus du code général de la propriété des personnes publiques prévoient les conditions dans lesquelles l'occupation du domaine public donne lieu au paiement d'une redevance tenant compte des avantages retirés de l'occupation ; que la société requérante ne peut dès lors se prévaloir d'une espérance légitime à la stabilité de la redevance d'occupation du domaine public qu'elle acquitte ; que, par suite, les stipulations de l'article 1er du premier protocole additionnel à la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ne peuvent être utilement invoquées ; <br/>
<br/>
              12. Considérant, en huitième lieu, que si les sociétés requérantes soutiennent que le décret attaqué méconnaît les dispositions de l'article 40 de la loi du 29 janvier 1993 relative à la prévention de la corruption et à la transparence de la vie économique et des procédures publiques, aux termes desquelles les montants et les modes de calcul des redevances versées par un délégataire de service public doivent être justifiés dans la convention de délégation, ces dispositions ne privent pas l'Etat, en sa qualité de propriétaire du domaine, du pouvoir de définir et modifier par voie réglementaire les montants de redevance dus en application des dispositions citées ci-dessus du code général de la propriété des personnes publiques ; que si les sociétés requérantes soutiennent que l'intervention du décret attaqué modifie l'équilibre de la concession de service public dont elles sont titulaires, cette circonstance, à la supposer établie, ne serait de nature qu'à leur permettre de saisir le juge du contrat et serait en tout état de cause sans influence sur la légalité du décret attaqué ; <br/>
<br/>
              13. Considérant, enfin, que le détournement de pouvoir ou de procédure allégué n'est pas établi ; <br/>
<br/>
              14. Considérant qu'il résulte de tout ce qui précède que les sociétés ESCOTA et Arcour ne sont pas fondées à demander l'annulation du décret attaqué ; que les conclusions qu'elles présentent à cet effet doivent, par suite, être rejetées, sans qu'il soit besoin d'ordonner les mesures d'instruction demandées ; que les conclusions présentées par la société ESCOTA au titre des dispositions de l'article L. 761-1 du code de justice administrative doivent également être rejetées ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'intervention de l'association des sociétés françaises d'autoroutes est admise. <br/>
Article 2 : Les requêtes des sociétés ESCOTA et Arcour sont rejetées. <br/>
Article 3 : La présente décision sera notifiée à la société des autoroutes Esterel, Côte d'Azur, Provence, Alpes (ESCOTA), à la société Arcour, au ministre de l'écologie, du développement durable et de l'énergie, au ministre de l'économie et des finances, au Premier ministre et à l'association des sociétés françaises d'autoroutes.<br/>
<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
