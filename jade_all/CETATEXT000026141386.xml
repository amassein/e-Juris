<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026141386</ID>
<ANCIEN_ID>JG_L_2012_07_000000352714</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/14/13/CETATEXT000026141386.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème et 2ème sous-sections réunies, 04/07/2012, 352714, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2012-07-04</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>352714</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème et 2ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Edmond Honorat</PRESIDENT>
<AVOCATS>SCP GASCHIGNARD</AVOCATS>
<RAPPORTEUR>M. Frédéric Dieu</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Boulouis</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2012:352714.20120704</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi, enregistré le 16 septembre 2011 au secrétariat du contentieux du Conseil d'Etat, du MINISTRE DE LA DEFENSE ET DES ANCIENS COMBATTANTS ; <br/>
<br/>
              le MINISTRE DE LA DEFENSE ET DES ANCIENS COMBATTANTS demande au Conseil d'Etat d'annuler l'arrêt n° 10NT00987 du 1er juillet 2011 par lequel la cour administrative d'appel de Nantes a rejeté son recours tendant à l'annulation du jugement n° 0803888 du 12 mars 2010 du tribunal administratif d'Orléans en tant que ce jugement, d'une part, a accueilli les conclusions indemnitaires des sociétés ICEC, Philippe-Tardits, Bergeret, Plée Constructions et Boussiquet en réparation du préjudice subi par ces dernières à la suite de leur éviction irrégulière de la procédure de l'attribution d'un marché relatif au lot n° 1 des travaux de construction de bureaux de la direction des ressources humaines de l'armée de l'air sur la base aérienne 705 de Tours et, d'autre part, avant de statuer sur les conclusions indemnitaires de ces sociétés, a décidé de faire procéder à une expertise ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code des marchés publics ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
<br/>
              - le rapport de M. Frédéric Dieu, Maître des Requêtes,  <br/>
<br/>
              - les observations de la SCP Gaschignard, avocat de la société Boussiquet, de la société Plée Constructions, de la société Selarl Philippe-Tardits, de la société Bergeret et de la société ICEC,<br/>
<br/>
              - les conclusions de M. Nicolas Boulouis, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Gaschignard, avocat de la société Boussiquet, de la société Plée Constructions, de la société Selarl Philippe-Tardits, de la société Bergeret et de la société ICEC ;<br/>
<br/>
<br/>
<br/>Considérant qu'il ressort des pièces du dossier soumis au juge du fond que, par un avis d'appel public à la concurrence publié le 30 novembre 2007, le MINISTRE DE LA DEFENSE ET DES ANCIENS COMBATTANTS a lancé une procédure pour l'attribution d'un marché relatif au lot n° 1 des travaux de construction de bureaux destinés à accueillir le personnel de la direction des ressources humaines de l'armée de l'air sur la base aérienne 705 de Tours ; que, par un courrier du 21 mai 2008, le MINISTRE a informé la société ICEC du rejet de l'offre présentée par le groupement d'entreprises dont elle était mandataire ; que, le 23 juin 2008, le marché a été attribué au groupement ayant pour mandataire la société Savoie Frères ; que, saisi par les sociétés ICEC, Philippe-Tardits, Bergeret, Plée Constructions et Boussiquet, le tribunal administratif d'Orléans a, par un jugement du 12 mars 2010, rejeté comme irrecevables, d'une part, les conclusions tendant à l'annulation pour excès de pouvoir de la décision du 21 mai 2008 informant la société ICEC du rejet de l'offre présentée par le groupement d'entreprises dont elle était mandataire ainsi que de la décision attribuant le marché en cause au groupement d'entreprises dont la société Savoie Frères était le mandataire et, d'autre part, à raison de leur tardiveté, celles tendant à l'annulation du contrat ; que, par le même jugement, le tribunal, après avoir jugé que les sociétés ICEC, Philippe-Tardits, Bergeret, Plée Constructions et Boussiquet avaient été irrégulièrement évincées du marché et qu'elles avaient droit à l'indemnisation de l'intégralité du manque à gagner qu'elles avait subi du fait de cette éviction, a ordonné une mesure d'expertise aux fins de déterminer le bénéfice net que chacune des sociétés aurait retiré de l'exécution du marché si le groupement dont elles étaient membres l'avait obtenu ; que, par l'arrêt attaqué, la cour administrative d'appel de Nantes a rejeté le recours du MINISTRE DE LA DEFENSE ET DES ANCIENS COMBATTANTS tendant à l'annulation de ce jugement ;<br/>
<br/>
              Considérant, en premier lieu, qu'indépendamment du recours en annulation de l'acte détachable du contrat ou du recours de pleine juridiction contestant la validité de ce contrat ou de certaines de ses clauses qui en sont divisibles, afin d'en obtenir la résiliation ou l'annulation, le concurrent évincé a toujours la faculté de présenter des conclusions indemnitaires tendant à la réparation du préjudice que lui a causé son éviction irrégulière de la procédure d'attribution du contrat ; que de telles conclusions peuvent faire l'objet d'un recours propre ou être présentées dans le cadre d'un recours tendant également à l'annulation de l'acte détachable du contrat ou à la contestation de sa validité ; que, dans ce dernier cas, la recevabilité de ces conclusions, qui est soumise, selon les modalités du droit commun, à l'intervention d'une décision préalable de l'administration de nature à lier le contentieux, le cas échéant en cours d'instance, sauf en matière de travaux publics, doit être appréciée indépendamment de la recevabilité des conclusions tendant à l'annulation de l'acte détachable du contrat ou à la contestation de sa validité ; que, par suite, la cour administrative d'appel de Nantes a pu, sans commettre d'erreur de droit, juger recevables les conclusions indemnitaires des sociétés ICEC, Philippe-Tardits, Bergeret, Plée Constructions et Boussiquet, lesquelles tendaient à la réparation du préjudice que leur avait causé leur éviction irrégulière du marché, après avoir jugé irrecevables les conclusions, présentées dans le cadre du même recours, tendant à l'annulation des actes détachables du contrat et à l'annulation de ce contrat lui-même ;<br/>
<br/>
              Considérant, en deuxième lieu, qu'aux termes de l'article 50 du code des marchés publics, dans sa rédaction alors en vigueur : " Lorsque le pouvoir adjudicateur se fonde sur plusieurs critères pour attribuer le marché, il peut autoriser les candidats à présenter des variantes. / Le pouvoir adjudicateur indique dans l'avis d'appel public à la concurrence ou dans les documents de la consultation s'il autorise ou non les variantes ; à défaut d'indication, les variantes ne sont pas admises. / Les documents de la consultation mentionnent les exigences minimales que les variantes doivent respecter ainsi que les modalités de leur présentation. Seules les variantes répondant à ces exigences minimales peuvent être prises en considération. / Les variantes sont proposées avec l'offre de base " ; que, pour l'application de ces dispositions, les variantes constituent des modifications, à l'initiative des candidats, de spécifications prévues dans la solution de base décrite dans les documents de la consultation ; <br/>
<br/>
              Considérant qu'après avoir souverainement estimé que les documents de la consultation ne permettaient pas aux candidats de présenter des variantes ne comportant pas de verrières pour couvrir les atriums créés entre les deux barres du bâtiment, la cour administrative d'appel de Nantes a inexactement qualifié les faits en jugeant que l'autorisation donnée aux candidats, au cours de la procédure et avant le dépôt des offres, de présenter de telles variantes, constituait une modification substantielle des conditions initiales du marché, qui devait donner lieu à la publication d'un avis d'appel public à la concurrence rectificatif assorti d'un nouveau délai de réception des offres de cinquante-deux jours à compter de son envoi à la publication ;<br/>
<br/>
              Considérant toutefois, en dernier lieu, qu'aux termes de l'article 53 du code des marchés publics : " I. Pour attribuer le marché au candidat qui a présenté l'offre économiquement la plus avantageuse, le pouvoir adjudicateur se fonde : / 1°) Soit sur une pluralité de critères non discriminatoires et liés à l'objet du marché, notamment la qualité, le prix, la valeur technique, le caractère esthétique et fonctionnel, les performances en matière de protection de l'environnement, les performances en matière d'insertion professionnelle des publics en difficulté, le coût global d'utilisation, la rentabilité, le caractère innovant, le service après-vente et l'assistance technique, la date de livraison, le délai de livraison ou d'exécution. D'autres critères peuvent être pris en compte s'ils sont justifiés par l'objet du marché (...) / II. Pour les marchés passés selon une procédure formalisée autre que le concours et lorsque plusieurs critères sont prévus, le pouvoir adjudicateur précise leur pondération (...) / Les critères ainsi que leur pondération ou leur hiérarchisation sont indiqués dans l'avis d'appel public à la concurrence ou dans les documents de la consultation (...) " ; que ces dispositions imposent au pouvoir adjudicateur d'informer les candidats des critères de sélection des offres ainsi que de leur pondération ou hiérarchisation ; que si le pouvoir adjudicateur décide, pour mettre en oeuvre ces critères de sélection des offres, de faire usage de sous-critères également pondérés ou hiérarchisés, il doit porter à la connaissance des candidats la pondération ou la hiérarchisation de ces sous-critères dès lors que, eu égard à leur nature et à l'importance de cette pondération ou hiérarchisation, ils sont susceptibles d'exercer une influence sur la présentation des offres par les candidats ainsi que sur leur sélection et doivent en conséquence être eux-mêmes regardés comme des critères de sélection ;<br/>
<br/>
              Considérant qu'il résulte de ce qui précède qu'après avoir souverainement relevé que chacun des six éléments composant le critère de la valeur technique était affecté d'une note variant de 5 à 15 points, et après avoir ainsi, implicitement mais nécessairement, regardé ces éléments comme des sous-critères pondérés de ce critère susceptibles d'exercer une influence sur la présentation des offres par les candidats ainsi que sur leur sélection, la cour administrative d'appel de Nantes n'a pas commis d'erreur de droit en jugeant que l'absence de communication aux candidats de la modification de cette pondération, opérée par le ministre dans le rapport d'analyse des offres et destinée à procéder à la comparaison entre les offres de base comportant des verrières et les variantes n'en comportant pas, avait été susceptible d'exercer une influence sur la présentation des offres par les candidats et avait méconnu les dispositions précitées de l'article 53 du code des marchés publics ; que, pour ce seul motif, la cour a ainsi pu juger que les sociétés ICEC, Philippe-Tardits, Bergeret, Plée Constructions et Boussiquet avaient été irrégulièrement évincées du marché et privées, au vu du faible écart de points séparant leur groupement de celui auquel le marché a été attribué à l'issue de l'opération de notation des offres, d'une chance sérieuse de l'emporter ;<br/>
<br/>
              Considérant qu'il résulte de tout ce qui précède que le pourvoi du MINISTRE DE LA DEFENSE ET DES ANCIENS COMBATTANTS doit être rejeté ; qu'il y a lieu de mettre à la charge de l'Etat, au titre de l'article L. 761-1 du code de justice administrative, le versement aux sociétés ICEC, Philippe-Tardits, Bergeret, Plée Constructions et Boussiquet d'une somme de 700 euros chacune ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi du MINISTRE DE LA DEFENSE ET DES ANCIENS COMBATTANTS est rejeté.<br/>
Article 2 : L'Etat versera aux sociétés ICEC, Philippe-Tardits, Bergeret, Plée Constructions et Boussiquet une somme de 700 euros chacune.<br/>
Article 3 : La présente décision sera notifiée au MINISTRE DE LA DEFENSE et aux sociétés SAS ICEC, Selarl Philippe-Tardits, SAS Bergeret, SA Plée Constructions et Sarl Boussiquet.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
