<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037509200</ID>
<ANCIEN_ID>JG_L_2018_09_000000419320</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/50/92/CETATEXT000037509200.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème chambre, 26/09/2018, 419320, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-09-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>419320</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP NICOLAY, DE LANOUVELLE, HANNOTIN</AVOCATS>
<RAPPORTEUR>M. Richard Senghor</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Edouard Crépey</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:419320.20180926</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              L'association " Horizons croisés "  a demandé au juge des référés du tribunal administratif de Limoges, sur le fondement de l'article L. 521-1 du code de justice administrative, de suspendre l'exécution de l'arrêté du préfet de la région Nouvelle-Aquitaine du 12 décembre 2017 qui a prononcé le retrait de ses licences d'entrepreneur de spectacles. Par une ordonnance n° 1800234 du 14 mars 2018, le juge des référés du tribunal administratif de Limoges a suspendu l'exécution de cette décision.  <br/>
<br/>
              Par un pourvoi sommaire et des mémoires, enregistrés les 28 mars, 10 avril 2018 et 17 août 2018 au secrétariat du contentieux du Conseil d'Etat, la ministre de la culture demande au Conseil d'Etat d'annuler cette ordonnance.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code des relations entre le public et l'administration ;<br/>
              - le code du travail ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Richard Senghor, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Edouard Crépey, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Nicolaÿ, de Lanouvelle, Hannotin, avocat de l'association Horizons croisés ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1.	Il ressort des pièces du dossier soumis au juge des référés que, par un arrêté du 12 décembre 2017, le préfet de la région Nouvelle-Aquitaine a, après avoir recueilli l'avis de la commission consultative régionale des licences d'entrepreneurs de spectacles, prononcé le retrait des deux licences dont bénéficiait l'association " Horizons croisés " depuis le 28 octobre 2015. Après avoir exercé le 22 janvier 2018 un recours gracieux contre cette décision, l'association a demandé au juge des référés du tribunal administratif de Limoges de suspendre l'exécution de cette décision du préfet, sur le fondement de l'article L. 521-1 du code de justice administrative. Par une ordonnance n° 1800234 du 14 mars 2018, contre laquelle la ministre de la culture se pourvoit en cassation, le juge des référés du tribunal administratif de Limoges a suspendu l'exécution de l'arrêté du préfet.<br/>
<br/>
              2.	En premier lieu, contrairement à ce qui est soutenu, il ressort des pièces de la procédure que la demande aux fins d'annulation, dont au demeurant les moyens étaient strictement identiques à ceux soulevés dans la demande aux fins de suspension, a été communiquée à la préfecture de la Nouvelle-Aquitaine qui en a accusé réception le 21 février 2018 à 15 heures 26.  Le moyen tiré de ce que l'ordonnance attaquée a été rendue au terme d'une procédure irrégulière doit donc être écarté. <br/>
<br/>
              3.	Aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision ". <br/>
<br/>
              4.	L'urgence justifie que soit prononcée la suspension d'un acte administratif lorsque l'exécution de celui-ci porte atteinte, de manière suffisamment grave et immédiate, à un intérêt public, à la situation du requérant ou aux intérêts qu'il entend défendre. Il appartient au juge des référés d'apprécier concrètement, compte tenu des justifications fournies par le requérant, si les effets de l'acte litigieux sont de nature à caractériser une urgence justifiant que, sans attendre le jugement de la requête au fond, l'exécution de la décision soit suspendue. Il lui appartient également, l'urgence s'appréciant objectivement et compte tenu de l'ensemble des circonstances de chaque espèce, de faire apparaître dans sa décision tous les éléments qui, eu égard notamment à l'argumentation des parties, l'ont conduit à considérer que la suspension demandée revêtait un caractère d'urgence.<br/>
<br/>
              5.	En deuxième lieu, en estimant que la condition d'urgence devait être regardée comme remplie dans la mesure où l'association requérante employait quatre personnes et que, au regard de son objet social, il n'était pas contesté que le retrait des licences d'entrepreneur de spectacles avait pour effet de l'empêcher de poursuivre son activité, le juge des référés du tribunal administratif de Limoges, qui a suffisamment motivé son ordonnance sur ce point, s'est livré à une appréciation souveraine des circonstances de l'espèce exempte de dénaturation et n'a pas commis d'erreur de droit.<br/>
<br/>
              6.	En troisième lieu,  le juge des référés n'a ni, eu égard à son office, commis d'erreur de droit ni dénaturé les pièces du dossier, en retenant, en l'état de l'instruction, comme de nature à créer un doute sérieux sur la légalité de l'arrêté préfectoral litigieux, le moyen tiré de la méconnaissance des dispositions de l'article R. 7122-17 du code du travail relatives à la procédure préalable de retrait de la licence d'entrepreneur de spectacles après avoir estimé que le courrier adressé à l'association, le 6 octobre 2017, ne l'avait pas mise à même de produire utilement ses observations sur la perspective du retrait de ses licences.<br/>
<br/>
              7.	Il résulte de ce qui précède que la ministre de la culture n'est pas fondée à demander l'annulation de l'ordonnance qu'elle attaque.<br/>
<br/>
              8.	Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 2 000 euros à verser à l'association " Horizons croisés ", au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de la ministre de la culture est rejeté.<br/>
Article 2  : L'Etat versera à l'association " Horizons croisés " une somme de 2000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3  : La présente décision sera notifiée à la ministre de la culture, à l'association " Horizons croisés " et au  ministre d'Etat, ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
