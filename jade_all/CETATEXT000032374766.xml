<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032374766</ID>
<ANCIEN_ID>JG_L_2016_04_000000381552</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/37/47/CETATEXT000032374766.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème / 1ère SSR, 06/04/2016, 381552</TITRE>
<DATE_DEC>2016-04-06</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>381552</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème / 1ère SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MATUCHANSKY, VEXLIARD, POUPOT ; SCP LYON-CAEN, THIRIEZ</AVOCATS>
<RAPPORTEUR>M. Jean-Baptiste de Froment</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Suzanne von Coester</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2016:381552.20160406</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La commune de Brix a demandé au tribunal administratif de Caen d'annuler l'arrêté du 10 octobre 2011 du préfet de la Manche autorisant la société Carrières Leroux-Philippe à exploiter une installation de stockage de déchets inertes dans le secteur Nc du plan local d'urbanisme de la commune. Par un jugement n° 12-618 du 4 décembre 2012 le tribunal administratif de Caen a fait droit à cette demande.<br/>
<br/>
              Par un arrêt n° 13NT00394 du 18 avril 2014, la cour administrative d'appel de Nantes a rejeté l'appel formé par la société Carrières Leroux-Philippe contre ce jugement.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 20 juin et 22 septembre 2014 au secrétariat du contentieux du Conseil d'Etat, la société Carrières Leroux-Philippe demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de la commune de Brix la somme de 4 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              -	le code de l'environnement ;<br/>
              -	le code de l'urbanisme ;<br/>
              -	le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Baptiste de Froment, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Suzanne von Coester, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Matuchansky, Vexliard, Poupot, avocat de la société Carrières Leroux-Philippe et à la SCP Lyon-Caen, Thiriez, avocat de la commune de Brix ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, par un arrêté du 10 octobre 2011, le préfet de la Manche a fait droit à la demande de la société Carrières Leroux-Philippe de renouvellement, pour une durée de deux ans, de l'autorisation d'exploiter une installation de stockage de déchets inertes dans le secteur Nc du plan local d'urbanisme de la commune de Brix ; que, par un jugement du 4 décembre 2012, le tribunal administratif de Caen a annulé cet arrêté à la demande de la commune de Brix ; que la société Carrières Leroux-Philippe se pourvoit en cassation contre l'arrêt du 18 avril 2014 par lequel la cour administrative d'appel de Nantes a rejeté son appel dirigé contre ce jugement ;  <br/>
<br/>
              2. Considérant qu'aux termes de l'article L. 123-5 du code de l'urbanisme relatif au plan local d'urbanisme, dans sa rédaction applicable au litige : " Le règlement et ses documents graphiques sont opposables à toute personne publique ou privée pour l'exécution de tous travaux, constructions, plantations, affouillements ou exhaussements des sols, (...) " ; qu'aux termes du I de l'article R. 541-70 du code de l'environnement alors applicable : " L'autorisation [d'exploitation d'une installation de stockage de déchets inertes] peut être refusée, par décision motivée, si l'exploitation de l'installation est de nature à porter atteinte : / 1° A la salubrité, à la sécurité ou à la tranquillité publiques ; / 2° Au caractère ou à l'intérêt des lieux avoisinants ; / 3° Aux sites, aux paysages, à la conservation des perspectives monumentales ; / 4° A l'exercice des activités agricoles et forestières ou à la conservation des milieux naturels, de la faune ou de la flore. " ; <br/>
<br/>
              3. Considérant qu'il résulte des dispositions de l'article L. 123-5 du code de l'urbanisme que le règlement d'un plan local d'urbanisme est opposable à l'exécution de tous travaux ayant pour objet ou pour effet un exhaussement des sols, y compris lorsque ces travaux relèvent du régime d'autorisation prévu par l'article R. 541-70 du code de l'environnement, alors en vigueur ; qu'ainsi, et alors même que le I de l'article R. 541-70 cité au point précédent ne mentionne pas la méconnaissance du règlement du plan local d'urbanisme au nombre des motifs susceptibles de justifier le refus d'autorisation d'exploitation d'une installation de stockage de déchets inertes, les dispositions du plan local d'urbanisme peuvent être légalement opposées à une installation de stockage de déchets inertes qui donne lieu à un exhaussement des sols ;  <br/>
<br/>
              4. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que l'article N1 du règlement du plan local d'urbanisme de la commune de Brix interdit les utilisations du sol non prévues à l'article N 2 de ce même règlement, aux termes duquel : " Occupations et utilisations du sol admises sous conditions (...) Secteur Nc : l'exploitation de carrières ayant fait l'objet d'une autorisation préfectorale " ;<br/>
<br/>
              5. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que l'installation de stockage de déchets inertes litigieuse devait notamment permettre, en vue de la remise en état des lieux rendue nécessaire par la fermeture de la carrière jusqu'alors exploitée sur le site en application d'une autorisation préfectorale, le remblaiement des terrains ; que la cour a estimé que cette installation avait pour effet d'entraîner un exhaussement des sols et en a déduit à bon droit qu'elle était soumise aux prescriptions du plan local d'urbanisme en application de l'article L. 123-5 ; que, toutefois, en jugeant que les dispositions citées ci-dessus de l'article N2 du règlement du plan local d'urbanisme interdisaient toute exploitation d'une installation de stockage de déchets inertes au seul motif qu'elles ne mentionnaient pas expressément une telle exploitation, alors même qu'elle avait relevé que, dans les circonstances de l'espèce qui lui était soumise, l'installation litigieuse visait à remblayer une carrière de grès dont la société requérante achevait l'exploitation, la cour a entaché son arrêt d'erreur de droit ; que, dès lors, et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, son arrêt doit être annulé ; <br/>
<br/>
              6. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à la charge de la société Carrières Leroux-Philippe qui n'est pas, dans la présente instance, la partie perdante ; qu'il y a lieu, en revanche, dans les circonstances de l'espèce, de mettre à la charge de la commune de Brix une somme de 3 000 euros à verser à la société Carrières Leroux-Philippe au titre de ces mêmes dispositions ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Nantes du 18 avril 2014 est annulé.<br/>
<br/>
Article 2 : L'affaire est renvoyée devant la cour administrative d'appel de Nantes.<br/>
<br/>
Article 3 : La commune de Brix versera à la société Carrières Leroux-Philippe une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : Les conclusions présentées par la commune de Brix au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 5 : La présente décision sera notifiée à la société Carrières Leroux-Philippe et à la commune de Brix.<br/>
Copie en sera adressée pour information à la ministre de l'environnement, de l'énergie et de la mer, chargée des relations internationales sur le climat.  <br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">44-035-04 Nature et environnement.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">68-01-01-02-015 Urbanisme et aménagement du territoire. Plans d'aménagement et d'urbanisme. Plans d`occupation des sols (POS) et plans locaux d'urbanisme (PLU). Application des règles fixées par les POS ou les PLU. Opposabilité du plan.
</SCT>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
