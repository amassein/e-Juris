<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037034080</ID>
<ANCIEN_ID>JG_L_2018_06_000000409805</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/03/40/CETATEXT000037034080.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème - 2ème chambres réunies, 07/06/2018, 409805, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-06-07</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>409805</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème - 2ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP POTIER DE LA VARDE, BUK LAMENT, ROBILLOT</AVOCATS>
<RAPPORTEUR>M. Thomas Pez-Lavergne</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Olivier Henrard</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:409805.20180607</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société La Coccinelle a demandé au tribunal administratif de Paris de condamner l'Etat à lui verser la somme de 4 031 630,36 euros en réparation du préjudice qu'elle aurait subi du fait du dysfonctionnement du service public de la juridiction administrative. Par un jugement n° 1410584 du 2 juillet 2015, le tribunal administratif de Paris a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 15PA03243 du 9 février 2017, la cour administrative d'appel de Paris a rejeté l'appel formé par la société La Coccinelle contre ce jugement.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 14 avril et 17 juillet 2017 au secrétariat du contentieux du Conseil d'Etat, la société La Coccinelle demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 9 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Thomas Pez-Lavergne, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Olivier Henrard, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Potier de la Varde, Buk Lament, Robillot, avocat de la société La Coccinelle.<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des énonciations de l'arrêt attaqué, en premier lieu, que, par un marché notifié le 25 juin 2002, le syndicat interdépartemental pour l'assainissement de l'agglomération parisienne a confié à un groupement dont la société La Coccinelle était mandataire la réalisation des travaux de la deuxième tranche de l'opération dite de " la décharge du Pantin La Briche " ; qu'à la suite de l'interruption des travaux, M. A...a été désigné en qualité d'expert par ordonnance du 15 septembre 2005 du président du tribunal administratif de Cergy-Pontoise pour notamment évaluer les prestations réalisées par chacun des membres du groupement, établir les comptes entre les parties et recueillir tous éléments permettant de déterminer les responsabilités et préjudices dans le cadre d'une éventuelle instance au fond ; que, par ordonnance du 12 octobre 2006, les frais et honoraires de l'expert ont été taxés et liquidés à hauteur de 93 225,36 euros ; que, par un arrêt du 8 décembre 2011, la cour administrative d'appel de Versailles a rejeté l'appel de cet expert dirigé contre le jugement du 3 juin 2009 par lequel le tribunal administratif de Cergy-Pontoise avait rejeté sa demande tendant à la réformation de l'ordonnance de taxation du 12 octobre 2006 ;<br/>
<br/>
              2. Considérant, en deuxième lieu, que, par un jugement du 3 juin 2009, le tribunal administratif de Cergy-Pontoise a condamné le syndicat interdépartemental pour l'assainissement de l'agglomération parisienne à verser à la société La Coccinelle une indemnité de 100 000 euros, assortie des intérêts légaux et de la capitalisation de ceux-ci, en règlement du solde du marché public de travaux ; que, par un arrêt du 18 avril 2013, la cour administrative d'appel de Versailles a partiellement annulé le jugement du tribunal administratif de Cergy-Pontoise du 3 juin 2009, puis rejeté les conclusions indemnitaires de la société La Coccinelle et mis à sa charge les frais de l'expertise réalisée par M. A...; que, par une décision du 18 décembre 2013, le Conseil d'Etat, statuant au contentieux, n'a pas admis le pourvoi formé par la société La Coccinelle contre l'arrêt du 18 avril 2013 ; <br/>
<br/>
              3. Considérant, en troisième lieu, que la société La Coccinelle a demandé au ministre de la justice de lui verser une indemnité de 4 031 630,36 euros en réparation des préjudices qu'elle estime avoir subis du fait des dysfonctionnements de la justice administrative à l'occasion des procédures rappelées aux points 1 et 2 ; qu'elle se pourvoit en cassation contre l'arrêt du 9 février 2017 par lequel la cour administrative d'appel de Paris a rejeté son appel dirigé contre le jugement du 2 juillet 2015 par lequel le tribunal administratif de Paris a rejeté sa demande tendant à la condamnation de l'Etat pour faute lourde ;<br/>
<br/>
              4. Considérant qu'en vertu des principes généraux régissant la responsabilité de la puissance publique, une faute lourde commise dans l'exercice de la fonction juridictionnelle par une juridiction administrative est susceptible d'ouvrir droit à indemnité ; que si l'autorité qui s'attache à la chose jugée s'oppose à la mise en jeu de cette responsabilité dans les cas où la faute lourde alléguée résulterait du contenu même de la décision juridictionnelle et où cette décision serait devenue définitive, la responsabilité de l'Etat peut cependant être engagée dans le cas où le contenu de la décision juridictionnelle est entachée d'une violation manifeste du droit de l'Union européenne ayant pour objet de conférer des droits aux particuliers ;<br/>
<br/>
              5. Considérant qu'il ressort des énonciations de l'arrêt attaqué que la société La Coccinelle soutenait que les stipulations de l'article 6 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, qui garantissent le droit à un procès équitable, avaient été méconnues faute pour la cour administrative d'appel de Versailles d'avoir, dans son arrêt du 18 avril 2013, ordonné une mesure d'instruction aux fins de désignation d'un nouvel expert alors même qu'elle avait écarté comme irrégulière l'expertise à laquelle avait procédé l'expert désigné par le président du tribunal administratif de Cergy-Pontoise ; qu'en écartant ce moyen au motif, d'une part, que la faute alléguée commise par la juridiction administrative résultait d'une décision juridictionnelle devenue définitive, et non d'un acte détachable de celle-ci, et, d'autre part, que la requérante n'invoquait pas de violation manifeste du droit de l'Union européenne ayant pour objet de conférer des droits aux particuliers, la cour administrative d'appel de Paris n'a pas entaché son arrêt d'insuffisance de motivation ni d'erreur de droit ; <br/>
<br/>
              6. Considérant qu'il résulte de ce qui précède que le pourvoi de la société La Coccinelle doit être rejeté ;<br/>
<br/>
              7. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise, à ce titre, à la charge de l'Etat, qui n'est pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de la société La Coccinelle est rejeté.<br/>
Article 2 : La présente décision sera notifiée à la société La Coccinelle et à la garde des sceaux, ministre de la justice.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
