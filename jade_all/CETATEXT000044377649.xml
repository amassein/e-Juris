<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044377649</ID>
<ANCIEN_ID>JG_L_2021_11_000000457053</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/37/76/CETATEXT000044377649.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 24/11/2021, 457053, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-11-24</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>457053</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2021:457053.20211124</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu les procédures suivantes :<br/>
<br/>
              I. Sous le n° 457053, par une requête et un nouveau mémoire, enregistrés les 27 septembre et 21 octobre 2021 au secrétariat du contentieux du Conseil d'Etat, le syndicat Action et Démocratie demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative :<br/>
<br/>
              1°) d'ordonner la suspension de l'exécution de l'arrêté du 27 juillet 2021 portant adaptation des modalités d'organisation du baccalauréat général et au baccalauréat technologique à compter de la session 2022 ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 1 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Il soutient que :<br/>
              - sa requête est recevable dès lors, d'une part, qu'il justifie d'un intérêt à agir et, d'autre part, que son président a qualité pour agir en justice ;<br/>
              - la condition d'urgence est satisfaite dès lors, d'une part, que les dispositions de l'arrêté contesté sont applicables à compter de la session 2022 du baccalauréat général et du baccalauréat technologique et, d'autre part, que l'impact de l'extension du contrôle continu a produit des effets préjudiciables sur l'exercice des missions des enseignants dès la fin du mois de septembre 2021 ; <br/>
              - il existe un doute sérieux quant à la légalité de l'arrêté contesté ; <br/>
              - les dispositions de l'arrêté du 27 juillet 2021 sont entachées d'illégalité dès lors qu'elles ont pour effet d'étendre et de rendre pérennes les mesures prises dans le cadre de circonstances exceptionnelles liées à l'épidémie de Covid-19, ayant entraîné la fermeture de classes, désormais révolues ; <br/>
              - elles méconnaissent le principe de sécurité juridique dès lors, d'une part, qu'elles revêtent un caractère rétroactif, en ce qu'elles prévoient que les notes moyennes annuelles obtenues au cours de l'année scolaire 2020-2021 seront prises en compte pour la session 2022 du baccalauréat et, d'autre part, que les conditions d'obtention du baccalauréat ont été modifiées à trois reprises en quatre ans ; <br/>
              - l'arrêté contesté, en ce qu'il prévoit l'obligation de prendre en compte les notes de l'année scolaire pour la délivrance du baccalauréat, porte atteinte à la liberté pédagogique prévue par l'article L. 912-1-1 du code de l'éducation, en ce qu'il dénature, la relation pédagogique entre l'enseignant et l'élève et l'objet de l'évaluation, tels que définis par les articles L. 311-1, L. 311-7 et D. 334-9 du code de l'éducation ; <br/>
              - il méconnaît le principe d'égalité dès lors qu'en l'absence de normes précises encadrant les modalités de notation dans le cadre du contrôle continu, ces dernières varient considérablement selon le type d'établissement et le niveau d'exigence des enseignants ;<br/>
              - l'arrêté contesté méconnaît le principe mentionné à l'article D. 334-9 du code de l'éducation selon lequel la notation d'un candidat à un examen ne peut être réalisée par l'enseignant ayant eu le candidat comme élève au cours de l'année scolaire.<br/>
<br/>
              Par un mémoire en défense et un nouveau mémoire, enregistrés les 15 et 22 octobre 2021, le ministre de l'éducation nationale, de la jeunesse et des sports conclut au rejet de la requête. Il soutient que le requérant ne justifie pas d'un intérêt à agir, que la condition d'urgence n'est pas satisfaite, et que les moyens soulevés ne sont pas fondés. <br/>
<br/>
<br/>
              II. Sous le n° 457054, par une requête et un nouveau mémoire enregistrés les 27 septembre et 21 octobre 2021 au secrétariat du contentieux du Conseil d'Etat, le syndicat Action et Démocratie demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative :<br/>
<br/>
              1°) d'ordonner la suspension de l'exécution du décret n° 2021-983 du 27 juillet 2021 modifiant les dispositions du code de l'éducation relatives au baccalauréat général et technologique ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 1 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Il soutient que :<br/>
              - sa requête est recevable dès lors, d'une part, qu'il justifie d'un intérêt à agir et, d'autre part, que son président a qualité pour agir en justice ;<br/>
              - la condition d'urgence est satisfaite dès lors, d'une part, que les dispositions du décret contesté sont applicables à compter de la session 2022 du baccalauréat général et du baccalauréat technologique et, d'autre part, que l'impact de l'extension du contrôle continu a produit des effets préjudiciables sur l'exercice des missions des enseignants dès la fin du mois de septembre 2021 ; <br/>
              - il existe un doute sérieux quant à la légalité du décret contesté ; <br/>
              - les dispositions de ce décret sont entachées d'illégalité dès lors qu'elles ont pour effet d'étendre et de rendre pérennes les mesures prises dans le cadre de circonstances exceptionnelles liées à l'épidémie de Covid-19, ayant entraîné la fermeture de classes, désormais révolues ; <br/>
              - elles méconnaissent le principe de sécurité juridique dès lors, d'une part, qu'elles revêtent un caractère rétroactif, en ce qu'elles prévoient que les notes moyennes annuelles obtenues au cours de l'année scolaire 2020-2021 seront prises en compte pour la session 2022 du baccalauréat et, d'autre part, que les conditions d'obtention du baccalauréat ont été modifiées à trois reprises en quatre ans ; <br/>
              - le décret contesté, en ce qu'il prévoit l'obligation de prendre en compte les notes de l'année scolaire pour la délivrance du baccalauréat, porte atteinte à la liberté pédagogique prévue par l'article L. 912-1-1 du code de l'éducation, en ce qu'il dénature, la relation pédagogique entre l'enseignant et l'élève et l'objet de l'évaluation, tels que définis par les articles L. 311-1, L. 311-7 et D. 334-9 du code de l'éducation ; <br/>
              - il méconnaît le principe d'égalité dès lors qu'en l'absence de normes précises encadrant les modalités de notation dans le cadre du contrôle continu, ces dernières varient considérablement selon le type d'établissement et le niveau d'exigence des enseignants ;<br/>
              - le décret contesté méconnaît le principe mentionné à l'article D. 334-9 du code de l'éducation selon lequel la notation d'un candidat à un examen ne peut être réalisée par l'enseignant ayant eu le candidat comme élève au cours de l'année scolaire.<br/>
<br/>
              Par un mémoire en défense et un nouveau mémoire, enregistrés les 15 et 22 octobre 2021, le ministre de l'éducation nationale, de la jeunesse et des sports conclut au rejet de la requête. Il soutient que le requérant ne justifie pas d'un intérêt à agir, que la condition d'urgence n'est pas satisfaite, et que les moyens soulevés ne sont pas fondés. <br/>
<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu : <br/>
              - le code de l'éducation ;  <br/>
              - le décret n° 2018-614 du 16 juillet 2018 ;<br/>
              - le décret n° 2021-209 du 25 février 2021 ;<br/>
              - le décret n° 2021-983 du 27 juillet 2021 ;<br/>
              - l'arrêté du 16 juillet 2018 relatif aux modalités d'organisation du contrôle continu pour l'évaluation des enseignements dispensés dans les classes conduisant au baccalauréat général et au baccalauréat technologique ;<br/>
              - l'arrêté du 27 juillet 2021 portant adaptation des modalités d'organisation du baccalauréat général et au baccalauréat technologique à compter de la session 2022 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, le syndicat Action et Démocratie, et d'autre part, le ministre de l'éducation nationale, de la jeunesse et des sports ;<br/>
<br/>
              Ont été entendus lors de l'audience publique du 19 octobre 2021, à 10 heures : <br/>
<br/>
              - les représentants du syndicat requérant ; <br/>
<br/>
              - les représentants du ministre de l'éducation nationale, de la jeunesse et des sports ;<br/>
<br/>
              à l'issue de laquelle le juge des référés a reporté la clôture de l'instruction au 22 octobre 2021, à 18 heures ; <br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Les requêtes visées ci-dessus tendent à la suspension de l'exécution du décret du 27 juillet 2021 modifiant les dispositions du code de l'éducation relatives au baccalauréat général et technologique et de l'arrêté du 27 juillet 2021 portant adaptation des modalités d'organisation du baccalauréat général et technologique à compter de la session 2022. Elles présentent à juger les mêmes questions. Il y a lieu de les joindre pour statuer par une seule ordonnance. <br/>
<br/>
              2. Aux termes de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision ". <br/>
<br/>
              Sur le cadre juridique du litige : <br/>
<br/>
              3. L'article 20 du décret du 16 juillet 2018 relatif aux enseignements conduisant au baccalauréat général et aux formations technologiques conduisant au baccalauréat technologique a modifié le premier alinéa de l'article D. 334-4 du code de l'éducation en disposant, pour ce qui est du baccalauréat général, que : " L'évaluation des enseignements obligatoires repose sur des épreuves terminales et sur des évaluations de contrôle continu tout au long du cycle terminal ". <br/>
<br/>
              4. L'article 2 du décret du 27 juillet 2021 modifiant les dispositions du code de l'éducation relatives au baccalauréat général et au baccalauréat technologique a modifié le 6ème alinéa l'article D. 334-4 en disposant qu'un arrêté du ministre chargé de l'éducation nationale définit les modalités de prise en compte des notes de contrôle continu pour le baccalauréat général pour les candidats au baccalauréat. En application de ces dispositions, l'arrêté du 27 juillet 2021 portant adaptation des modalités d'organisation du baccalauréat général et au baccalauréat technologique à compter de la session 2022 a modifié l'arrêté du 16 juillet 2018 relatif aux modalités d'organisation du contrôle continu pour l'évaluation des enseignements dispensés dans les classes conduisant au baccalauréat général et au baccalauréat technologique en disposant que les candidats aux baccalauréats général et technologique scolarisés dans les établissements publics d'enseignement, dans les établissements d'enseignement privés ayant passé avec l'Etat le contrat prévu par l'article L. 442-5 du code de l'éducation ainsi que dans les établissements d'enseignement privés hors contrat font l'objet d'évaluations au cours du cycle terminal mentionné à l'article D. 333-2 du même code, qui se traduisent par une note de contrôle continu, comptant pour 40 % de la note moyenne obtenue à l'examen par le candidat. Cette note se compose, à concurrence de 30 %, des évaluations obtenues en cours d'année au titre des enseignements scientifique, d'histoire-géographie, de langues vivantes A et B suivies au cours du cycle terminal et de spécialité suivi en classe de première et, à hauteur de 10 %, de l'évaluation des bulletins scolaires des classes de première et de terminale. <br/>
<br/>
              5. L'article 4 du décret du 27 juillet 2021 a également modifié les dispositions de l'article D. 334-4-1 du code de l'éducation pour tirer les conséquences de la suppression des évaluations communes et remplacer la commission d'harmonisation des notes obtenues dans le cadre de ces épreuves par une commission chargée d'harmoniser les notes de contrôle continu. Aux termes de ces dispositions, cette instance prend connaissance des notes des évaluations ponctuelles et des notes figurant dans les livrets scolaires des candidats, s'assure qu'il n'existe pas de discordance manifeste entre ces notes et procède si nécessaire à leur harmonisation. <br/>
<br/>
              Sur la demande en référé :  <br/>
<br/>
              6. En premier lieu, si les dispositions contestées modifient les modalités d'organisation de l'examen du baccalauréat à compter de la session 2022 en donnant un poids accru aux notes obtenues en cours d'année dans le cadre du contrôle continu, elles n'ont ni pour objet, ni pour effet d'instituer de nouvelles épreuves ou modalités d'évaluation impliquant une préparation spécifique de la part des élèves de terminale. Eu égard à la date de publication du décret et de l'arrêté ministériel contestés, elles étaient connues des futurs candidats au baccalauréat et des enseignants chargés de les évaluer avant la date de la rentrée scolaire. Ces modalités d'évaluation sont, au surplus, identiques à celles qui ont été retenues à titre exceptionnel pour la session 2021 du baccalauréat dans le contexte de l'épidémie de Covid-19, afin de limiter le nombre d'épreuves en présentiel auxquelles les candidats au baccalauréat devaient se soumettre. Par suite, le moyen tiré de ce qu'en substituant, en cours d'année scolaire, le contrôle continu à certaines épreuves jusque-là prévues par les textes pour les candidats qui se présenteront au baccalauréat en 2022, le décret et l'arrêté contestés priveraient ces derniers de la possibilité de disposer d'un délai raisonnable pour s'adapter aux nouvelles modalités d'organisation de cet examen et porteraient ainsi atteinte au principe de sécurité juridique n'est pas propre à créer un doute sérieux quant à sa légalité. <br/>
<br/>
              7. En deuxième lieu, s'il est soutenu que les dispositions contestées porteraient atteinte à la liberté pédagogique des enseignants en ce que, d'une part, le poids accru du contrôle continu dans la délivrance du baccalauréat exposerait ces derniers à un risque de pressions de la part des élèves et de leurs parents, et, d'autre part, les empêcherait de mener à bien leur mission de formation des élèves, il résulte des dispositions de l'article L. 912-1-1 du code l'éducation que la liberté pédagogique de l'enseignant, qui s'exerce dans le respect des programmes et des instructions du ministre chargé de l'éducation nationale et dans le cadre du projet d'école ou d'établissement, concerne à titre principal la manière dont l'enseignement est délivré, et ne fait pas obstacle à ce que les pouvoirs publics modifient le déroulement ou les modalités d'évaluations des épreuves du baccalauréat en accroissant la part du contrôle continu. Il s'ensuit que le moyen tiré de ce que les dispositions contestées méconnaîtraient ce principe n'est pas de nature à faire naître, en l'état de l'instruction, un doute sérieux quant à sa légalité. <br/>
<br/>
              8. En troisième lieu, il résulte de l'instruction que l'harmonisation des notes du contrôle continu est réalisée, d'une part, par l'intermédiaire de la commission instituée par les dispositions de l'article D. 334-4-1 citées au point 5, présidée par le recteur d'académie et composée d'inspecteurs d'académie-inspecteurs pédagogiques régionaux et d'enseignants sur la base d'éléments statistiques relatifs aux résultats obtenus dans l'établissement d'inscription des candidats au cours des deux dernières sessions du baccalauréat, mais également au niveau de chaque établissement d'enseignement, tenu en vertu de l'article 2 de l'arrêté du 27 juillet 2021 de mettre en place un projet d'évaluation du contrôle continu élaboré en conseil d'enseignement et validé en conseil pédagogique, puis soumis au conseil d'administration dans les établissements publics d'enseignement. La note de service du ministère de de l'éducation nationale, de la jeunesse et des sports du 28 juillet 2021, publiée au bulletin officiel n° 30 du 29 juillet 2021, précise à cet égard que ce travail collégial doit aboutir à la définition de principes communs, garants de l'égalité entre les candidats. Enfin, aux termes des dispositions de l'article 6 du décret du 25 février 2021, le jury du baccalauréat doit s'assurer qu'il n'existe pas de discordances manifestes entre les notes issues des moyennes annuelles des livrets scolaires et les résultats obtenus dans le cadre des épreuves du baccalauréat et peut, le cas échéant, en vertu de son pouvoir souverain d'appréciation, lui-même procéder à une harmonisation des notes. Il s'ensuit qu'au vu des différentes dispositions qui ont été prises en vue d'harmoniser les notes entre les candidats et garantir ainsi l'égalité dans l'évaluation de ces derniers, les moyens tirés, d'une part, de la méconnaissance par les textes contestés du principe d'égalité entre les candidats, en ce qu'il prévoit la prise en compte au titre des épreuves du baccalauréat de notations issues du contrôle continu dont le niveau peut varier selon l'établissement et les exigences des professeurs et, d'autre part, du caractère arbitraire du processus d'harmonisation des notes qu'il instituerait ne peuvent être regardés, en l'état de l'instruction, comme de nature à faire naître un doute sérieux quant à la légalité des dispositions contestées. Il en va de même du moyen tiré de ce que le poids accru donné aux notes obtenues par les élèves en cours d'année scolaire méconnaîtrait la règle fixée à l'article D. 334-9 du code de l'éducation selon laquelle les examinateurs ne peuvent évaluer leurs élèves de l'année en cours, dès lors qu'aucun principe n'interdit la prise en compte des évaluations issues du contrôle continu dans le cadre d'un examen tel que le baccalauréat.<br/>
<br/>
              9. Il résulte de tout ce qui précède que, sans qu'il soit besoin de se prononcer, d'une part, sur la fin de non-recevoir soulevée en défense par le ministre de l'éducation nationale, de la jeunesse et des sports et, d'autre part, sur la condition d'urgence, la requête du syndicat Action et Démocratie doit être rejetée, y compris ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
              ------------------<br/>
<br/>
Article 1er : La requête du syndicat Action et Démocratie est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée au syndicat Action et Démocratie et au ministre de l'éducation nationale, de la jeunesse et des sports.<br/>
Fait à Paris, le 24 novembre 2021<br/>
Signé : Benoît Bohnert <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
