<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044124871</ID>
<ANCIEN_ID>JG_L_2021_09_000000432579</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/12/48/CETATEXT000044124871.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème - 8ème chambres réunies, 28/09/2021, 432579, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-09-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>432579</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème - 8ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Cécile Isidoro</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Laurent Cytermann</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:432579.20210928</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              M. B... D... a demandé au tribunal administratif de Nancy de prononcer la décharge de la contribution sociale généralisée, de la contribution pour le remboursement de la dette sociale, du prélèvement social, de la contribution additionnelle au prélèvement social au taux de 0,3 % et de la contribution additionnelle au prélèvement social de 2 % auxquels il a été assujetti au titre de l'année 2013 à raison de la réalisation de plus-values immobilières. <br/>
<br/>
              Par un jugement nos 1401385, 140387, 1401388, 1401389, 1401495 du 5 novembre 2015, le tribunal administratif de Nancy a déchargé M. D... des impositions litigieuses. <br/>
<br/>
              Par un arrêt n° 15NC02416 du 26 janvier 2017, C... administrative d'appel de Nancy a rejeté l'appel formé par le ministre de l'économie et des finances contre ce jugement, à l'exception du prélèvement de solidarité au taux de 2 % qu'elle a remis à la charge de M. D.... <br/>
<br/>
              Par une décision n° 409153 du 19 décembre 2018, le Conseil d'Etat, saisi par le ministre, a annulé les articles 3 et 4 de cet arrêt et a renvoyé l'affaire dans cette mesure devant C... administrative d'appel de Nancy. <br/>
<br/>
              Par un nouvel arrêt n° 18NC03448 du 14 mai 2019, C... administrative d'appel de Nancy a, d'une part, remis à la charge de M. D... la contribution sociale généralisée et le prélèvement social dus au titre de l'année 2013 pour un montant de 53 856 euros, d'autre part, rejeté le surplus des conclusions de l'appel du ministre. <br/>
<br/>
              Par un pourvoi, enregistré le 12 juillet 2019 au secrétariat du contentieux du Conseil d'Etat, le ministre de l'action et des comptes publics demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler les articles 2 et 4 de cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de remettre à la charge de M. D..., au titre de l'année 2013, la contribution au règlement de la dette sociale et la contribution additionnelle au prélèvement social pour un montant de 3 391 euros. <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le traité sur le fonctionnement de l'Union européenne ;<br/>
              - le règlement (CE) n° 883/2004 du Parlement européen et du Conseil du 29 avril 2004 ;<br/>
              - le code de l'action sociale et des familles ;<br/>
              - le code général des impôts ;<br/>
              - l'ordonnance n° 96-50 du 24 janvier 1996 ; <br/>
              - l'arrêt n° C-372/18 du 14 mars 2019 de C... de justice de l'Union européenne ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Cécile Isidoro, conseillère d'Etat,  <br/>
<br/>
              - les conclusions de M. Laurent Cytermann, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que M. D..., résident au Luxembourg percevant des pensions de retraite de source française, est associé de deux sociétés civiles immobilières, dont le siège se trouve en France et qui ont réalisé des plus-values immobilières au cours de l'année 2013. Au titre de ces plus-values, il a été assujetti, à due proportion de ses parts dans ces sociétés, à la contribution sociale généralisée prévue par l'article 1600-0 C du code général des impôts au taux de 8,2 %, à la contribution au remboursement de la dette sociale prévue par l'article 1600-0 H du même code au taux de 0,5 %, au prélèvement social prévu par l'article 1600-0-F bis du même code au taux de 4,5 %, à la contribution additionnelle à ce prélèvement prévue au 2° de l'article L. 14-10-4 du code de l'action sociale et des familles au taux de 0,3 % et au prélèvement de solidarité prévu par l'article 1600-0 S du code général des impôts au taux de 2 %, pour un montant global de 65 728 euros. Par un jugement du 5 novembre 2015, le tribunal administratif de Nancy l'a déchargé de ces cinq impositions. Par un arrêt du 26 janvier 2017, C... administrative d'appel de Nancy a rejeté l'appel formé contre ce jugement par le ministre de l'économie et des finances à l'exception du prélèvement de solidarité au taux de 2 % qu'elle a remis à la charge de M. D.... Par une décision du 19 décembre 2018, le Conseil d'Etat, statuant au contentieux, saisi par le ministre, a annulé les articles 3 et 4 de cet arrêt et renvoyé, dans cette mesure, l'affaire devant C.... Par un arrêt du 14 mai 2019, C... administrative d'appel de Nancy a, d'une part, remis à la charge de M. D... la contribution sociale généralisée et le prélèvement social au titre de l'année 2013 pour un montant de 53 856 euros, rejeté le surplus des conclusions du recours du ministre et réformé le jugement en ce qu'il avait de contraire à son arrêt. Le ministre de l'action et des comptes publics se pourvoit en cassation contre les articles 2 et 4 de cet arrêt en tant qu'il rejette ses conclusions tendant à la décharge de la contribution au remboursement de la dette sociale et de la contribution additionnelle au prélèvement social et met à sa charge les frais exposés et non compris dans les dépens. <br/>
<br/>
              Sur le cadre juridique du litige : <br/>
<br/>
              2. D'une part, aux termes de l'article 2 du règlement du Parlement européen et du Conseil du 29 avril 2004 : " 1. Le présent règlement s'applique aux ressortissants de l'un des États membres, aux apatrides et aux réfugiés résidant dans un État membre qui sont ou ont été soumis à la législation d'un ou de plusieurs Etats membres, ainsi qu'aux membres de leur famille et à leurs survivants (...) ". Aux termes de l'article 3 du même règlement : " 1. Le présent règlement s'applique à toutes les législations relatives aux branches de sécurité sociale qui concernent : a) les prestations de maladie ; b) les prestations de maternité et de paternité assimilées ; c) les prestations d'invalidité ; d) les prestations de vieillesse ; e) les prestations de survivant ; f) les prestations en cas d'accidents du travail et de maladies professionnelles ; g) les allocations de décès ; h) les prestations de chômage ; i) les prestations de préretraite ; j) les prestations familiales (...) / 3. Le présent règlement s'applique également aux prestations spéciales en espèces à caractère non contributif visées à l'article 70 ". Aux termes de cet article 70 : " 1. Le présent article s'applique aux prestations spéciales en espèces à caractère non contributif relevant d'une législation qui, de par son champ d'application personnel, ses objectifs et/ou ses conditions d'éligibilité, possède les caractéristiques à la fois de la législation en matière de sécurité sociale visée à l'article 3, paragraphe 1, et d'une assistance sociale. / 2. Aux fins du présent chapitre, on entend par "prestations spéciales en espèces à caractère non contributif" les prestations a) qui sont destinées : i) soit à couvrir à titre complémentaire, subsidiaire ou de remplacement, les risques correspondant aux branches de sécurité sociale visées à l'article 3, paragraphe 1, et à garantir aux intéressés un revenu minimum de subsistance eu égard à l'environnement économique et social dans l'État membre concerné ; ii) soit uniquement à assurer la protection spécifique des personnes handicapées, étroitement liées à l'environnement social de ces personnes dans l'Etat membre concerné ; et b) qui sont financées exclusivement par des contributions fiscales obligatoires destinées à couvrir des dépenses publiques générales et dont les conditions d'attribution et modalités de calcul ne sont pas fonction d'une quelconque contribution pour ce qui concerne leurs bénéficiaires. Les prestations versées à titre de complément d'une prestation contributive ne sont toutefois pas considérées, pour ce seul motif, comme des prestations contributives ; et c) qui sont énumérées à l'annexe X ". Aux termes de l'article 11 de ce même règlement : " 1. Les personnes auxquelles le présent règlement est applicable ne sont soumises qu'à la législation d'un seul État membre. Cette législation est déterminée conformément au présent titre ". Enfin, aux termes de son article 30 : " 1. L'institution d'un Etat membre qui applique une législation prévoyant des retenues de cotisations pour la couverture des prestations de maladie, de maternité et de paternité assimilées, ne peut procéder à l'appel et au recouvrement de ces cotisations, calculées selon la législation qu'elle applique, que dans la mesure où les dépenses liées aux prestations servies en vertu des articles 23 à 26 sont à la charge d'une institution dudit Etat membre. 2. Lorsque, dans les cas visés à l'article 25, le titulaire de pension doit verser des cotisations, ou lorsque le montant correspondant doit être retenu, pour la couverture des prestations de maladie, de maternité et de paternité assimilées, selon la législation de l'Etat membre dans lequel il réside, ces cotisations ne peuvent pas être recouvrées du fait de son lieu de résidence. ". Il résulte de ces dispositions qu'une prestation non contributive relevant de l'assistance sociale n'entre dans le champ d'application du règlement que lorsqu'elle possède également les caractéristiques de la législation en matière de sécurité sociale visée au paragraphe 1 de l'article 3 et à la condition, notamment, qu'elle soit mentionnée à l'annexe à laquelle ces dispositions renvoient.<br/>
<br/>
              3. D'autre part, aux termes du 1er paragraphe de l'article 24 du même règlement : " La personne qui perçoit une pension ou des pensions en vertu de la législation d'un ou plusieurs Etats membres, et qui ne bénéficie pas des prestations en nature selon la législation de l'Etat membre de résidence, a toutefois droit, pour elle-même et pour les membres de sa famille, à de telles prestations, pour autant qu'elle y aurait droit selon la législation de l'Etat membre ou d'au moins un des Etats membres auxquels il incombe de servir une pension, si elle résidait dans l'Etat membre concerné. Les prestations en nature sont servies pour le compte de l'institution visée au paragraphe 2 par l'institution du lieu de résidence, comme si l'intéressé bénéficiait de la pension et des prestations en nature selon la législation de cet Etat membre. " Il en résulte que les personnes qui perçoivent une pension de retraite en vertu de la législation française et résident dans un autre Etat membre selon la législation duquel elles ne bénéficient pas de prestations en nature peuvent bénéficier des prestations en nature auxquelles elles auraient droit si elles résidaient en France, ces prestations étant servies par l'institution de l'Etat membre où elles résident selon la législation qui y est applicable pour le compte et à la charge des caisses de sécurité sociale françaises. Ces personnes peuvent, par ailleurs, être soumises en France aux retenues de cotisations instituées pour la couverture des prestations de maladie, de maternité et de paternité assimilées en cause. Les personnes qui perçoivent une pension de retraite en vertu de la législation française et résident dans un autre Etat membre doivent, lorsqu'elles contestent le principe de leur assujettissement à de tels prélèvements, justifier non seulement de ce qu'elles sont affilées au régime de sécurité sociale de leur Etat de résidence mais aussi que c'est en vertu de la législation de cet Etat qu'elles bénéficient de leurs prestations et non pour le compte des caisses de sécurité sociale françaises. <br/>
<br/>
              Sur la contribution additionnelle au prélèvement social :<br/>
<br/>
              4. Par un arrêt C-372/18 du 14 mars 2019, Ministre de l'action et des comptes publics c. / M. et Mme A..., C... de justice de l'Union européenne, saisie par C... administrative d'appel de Nancy d'une question préjudicielle relative au prélèvement social et à la contribution additionnelle à ce prélèvement affectés au financement de la Caisse nationale de solidarité pour l'autonomie, a dit pour droit que l'article 3 du règlement du Parlement européen et du Conseil du 29 avril 2004 portant sur la coordination des systèmes de sécurité sociale doit être interprété en ce sens que des prestations, telles que l'allocation personnalisée d'autonomie et la prestation compensatoire du handicap, doivent, aux fins de leur qualification de " prestations de sécurité sociale " au sens de cette disposition, être considérées comme étant octroyées en dehors de toute appréciation individuelle des besoins personnels du bénéficiaire, dès lors que les ressources de ce dernier sont prises en compte aux seules fins du calcul du montant effectif de ces prestations sur la base de critères objectifs et légalement définis.<br/>
<br/>
              5. Par ailleurs, l'allocation personnalisée d'autonomie et la prestation compensatoire du handicap, qui sont des prestations de sécurité sociale portant sur le risque de dépendance et qui visent à améliorer l'état de santé et la vie des personnes dépendantes, tout en présentant des caractéristiques qui leur sont propres, doivent être assimilées à des " prestations de maladie ", au sens du a) de l'article 3, paragraphe 1, du règlement du 29 avril 2004. Il en résulte que le prélèvement social et la contribution additionnelle à ce prélèvement affectés au financement de la Caisse nationale de solidarité pour l'autonomie, qui doivent, pour l'application de l'article 3 du règlement du 29 avril 2004 portant sur la coordination des systèmes de sécurité sociale, être regardés comme affectés de manière spécifique et directe au financement du régime de sécurité sociale français et relèvent par conséquent du champ d'application de ce règlement, doivent être qualifiés de cotisations instituées pour la couverture des prestations de maladie, de maternité et de paternité assimilées au sens de l'article 30 du même règlement.<br/>
<br/>
              6. Il résulte de ce qui précède qu'en jugeant que la contribution additionnelle au prélèvement social n'avait pas pour objet d'assurer la couverture des prestations de maladie, de maternité et de paternité ou assimilées pour lesquelles l'article 30 précité du règlement du 29 avril 2004 prévoit une dérogation au principe d'unicité de la législation en matière de sécurité sociale et que l'administration ne pouvait assujettir M. D... à cette contribution à raison des revenus perçus du fait de la plus-value réalisée en France en 2013, C... administrative d'appel de Nancy a commis une erreur de droit. Son arrêt doit, par suite, être annulé dans cette mesure. <br/>
<br/>
              Sur la contribution pour le remboursement de la dette sociale :<br/>
<br/>
              7. D'une part, l'article 1600-0 H du code général des impôts, dans sa rédaction alors applicable, dispose que : " La contribution pour le remboursement de la dette sociale assise sur les produits de placement est établie, contrôlée et recouvrée conformément à l'article 16 de l'ordonnance n° 96-50 du 24 janvier 1996 relative au remboursement de la dette sociale. " L'article 16 de cette ordonnance, dans sa rédaction alors applicable, prévoit que : " I. - Il est institué, à compter du 1er février 1996 une contribution prélevée sur les produits de placement désignés aux I et I bis de l'article L. 136-7 du code de la sécurité sociale. Cette contribution est assise, recouvrée et contrôlée selon les modalités prévues aux V et VI du même article (...) ". Aux termes de l'article L. 136-7 du code de la sécurité sociale, dans sa rédaction alors applicable : " (...) Sont également assujettis à cette contribution : (...). 2° Les plus-values mentionnées aux articles 150 U à 150 UC du code général des impôts. (...) ".<br/>
<br/>
              8. D'autre part, aux termes de l'article 2 de l'ordonnance n° 96-50 du 24 janvier 1996 relative au remboursement de la dette sociale : " La Caisse d'amortissement de la dette sociale a pour mission, d'une part, d'apurer la dette mentionnée au I de l'article 4 et, d'autre part, d'effectuer les versements prévus au II et III du même article. " L'article 4 du même texte dispose que : " I. La dette d'un montant de 137 milliards de francs de l'Agence centrale des organismes de sécurité sociale à l'égard de la Caisse des dépôts et consignations constatée au 31 décembre 1995, correspondant au financement des déficits accumulés au 31 décembre 1995 par le régime général de sécurité sociale et à celui de son déficit prévisionnel de l'exercice 1996, est transférée à la Caisse d'amortissement de la dette sociale à compter du 1er janvier 1996. (...). II bis- La couverture des déficits cumulés de la branche mentionnée au 1° de l'article L. 200-2 du code de la sécurité sociale arrêtés au 31 décembre 2003 et celui du déficit prévisionnel au titre de l'exercice 2004 est assurée par des transferts de la Caisse d'amortissement de la dette sociale à l'Agence centrale des organismes de sécurité sociale à hauteur de 10 milliards d'euros le 1er septembre 2004 et dans la limite de 25 milliards d'euros au plus tard le 31 décembre 2004 (...) ".<br/>
<br/>
              9. Il résulte de ces dispositions que la contribution au remboursement de la dette sociale, perçue au taux de 0,5 %, est affectée à la Caisse d'amortissement de la dette sociale et destinée à contribuer à l'apurement des déficits de la sécurité sociale et à la reprise de la dette du fonds de solidarité vieillesse. Elle doit, pour l'application de l'article 3 du règlement du 29 avril 2004 portant sur la coordination des systèmes de sécurité sociale, être regardée comme affectée de manière spécifique et directe au financement du régime de sécurité sociale français. Par ailleurs, compte tenu de la nature des déficits de la sécurité sociale à l'apurement desquels la contribution au remboursement de la dette sociale a pour objet de contribuer, elle doit être regardée comme étant au nombre des cotisations instituées pour la couverture des prestations de maladie, de maternité et de paternité ou assimilées au sens de l'article 30 du même règlement.<br/>
<br/>
              10. Il résulte de ce qui précède qu'en jugeant que la contribution au remboursement de la dette sociale n'a pas pour objet d'assurer la couverture des prestations de maladie, de maternité et de paternité ou assimilées pour lesquelles l'article 30 du règlement du 29 avril 2004 prévoit une dérogation au principe d'unicité de la législation en matière de sécurité sociale et que l'administration ne pouvait assujettir M. D... à ces contributions à raison des revenus perçus du fait de la plus-value réalisée en France en 2013, C... administrative d'appel de Nancy a commis une erreur de droit. Son arrêt doit, par suite, être annulé dans cette mesure.<br/>
<br/>
              11. Aux termes du second alinéa de l'article L. 821-2 du code de justice administrative : " Lorsque l'affaire fait l'objet d'un second pourvoi en cassation, le Conseil d'Etat statue définitivement sur cette affaire. ". Il y a lieu de procéder au règlement de l'affaire au fond dans la mesure de la cassation prononcée.<br/>
<br/>
              12. D'une part, M. D..., qui réside au Luxembourg et perçoit des pensions de retraite de source française, a été assujetti aux prélèvements indiqués au point 1. S'il justifie, notamment par la production d'une copie de carte de sécurité sociale et de remboursements de soins intervenus en 2011 et 2015, de son affiliation au régime de sécurité sociale luxembourgeois, ces seuls éléments sont insuffisants pour établir que les prestations en nature qui lui ont été remboursées par la caisse nationale de santé luxembourgeoise l'ont été en vertu de la législation luxembourgeoise et non pour le compte d'un organisme de sécurité sociale français, alors qu'il résulte de l'instruction qu'il a sollicité le formulaire " S 1 " qui atteste, avec effet à compter du 1er janvier 2012, de ses droits aux prestations de maladie en vertu de la législation française débitrice de ses pensions. <br/>
<br/>
              13. D'autre part, ainsi qu'il a été dit plus haut, la contribution additionnelle au prélèvement social et la contribution au remboursement de la dette sociale entrent dans le champ du règlement du 29 avril 2004 portant sur la coordination des systèmes de sécurité sociale et ont pour objet d'assurer la couverture des prestations de maladie, de maternité et de paternité ou assimilées pour lesquelles l'article 30 du règlement du 29 avril 2004 prévoit une dérogation au principe d'unicité de la législation en matière de sécurité sociale. Par suite, c'est à bon droit que l'administration a assujetti M. D... à ces contributions à raison de la plus-value qu'il a réalisée en 2013. <br/>
<br/>
              14. Il résulte de ce qui précède que le ministre de l'action et des comptes publics est fondé à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Nancy a déchargé M. D... du paiement de la contribution au remboursement de la dette sociale et de la contribution additionnelle au prélèvement social au titre de l'année 2013. Il s'ensuit que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que l'Etat verse à M. D... la somme que ce dernier a demandé en appel au titre des frais exposés et non compris dans les dépens.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : Les articles 2 et 4 de l'arrêt de C... administrative d'appel de Nancy du 14 mai 2019 sont annulés.<br/>
Article 2 : La contribution au remboursement de la dette sociale et la contribution additionnelle au prélèvement social sont remises à la charge de M. D... au titre de l'année 2013 pour un montant de 3 391 euros.<br/>
Article 3 : Le jugement du tribunal administratif de Nancy du 5 novembre 2015 est réformé en ce qu'il a de contraire à l'article 2.<br/>
Article 4 : Les conclusions présentées par M. D... devant C... administrative d'appel au titre de l'article L. 761-1 du code de justice sont rejetées.<br/>
Article 5 : La présente décision sera notifiée au ministre de l'action et des comptes publics et à M. B... D....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
