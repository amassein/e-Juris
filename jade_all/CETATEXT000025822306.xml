<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000025822306</ID>
<ANCIEN_ID>JG_L_2012_05_000000336462</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/25/82/23/CETATEXT000025822306.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème et 8ème sous-sections réunies, 04/05/2012, 336462, Publié au recueil Lebon</TITRE>
<DATE_DEC>2012-05-04</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>336462</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème et 8ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>LE PRADO ; SCP BARTHELEMY, MATUCHANSKY, VEXLIARD ; SCP GASCHIGNARD ; SCP BOUTET</AVOCATS>
<RAPPORTEUR>Mme Emilie Bokdam-Tognetti</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emmanuelle Cortot-Boucher</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2012:336462.20120504</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 10 février et 10 mai 2010 au secrétariat du contentieux du Conseil d'Etat, présentés pour la FEDERATION DE LA LIBRE PENSEE ET D'ACTION SOCIALE DU RHONE, dont le siège est 7 rue Major Martin à Lyon (69001), représentée par son président ; la FEDERATION DE LA LIBRE PENSEE ET D'ACTION SOCIALE DU RHONE demande au Conseil d'Etat d'annuler l'arrêt n° 07LY01079-07LY01113 du 21 avril 2009 par lequel la cour administrative d'appel de Lyon, faisant droit aux requêtes de l'association Communauté Sant'Egidio France et de la commune de Lyon, a d'une part annulé le jugement n° 0506481 du 22 mars 2007 par lequel le tribunal administratif de Lyon a annulé la délibération du 20 juin 2005 du conseil municipal de Lyon attribuant à l'association Communauté Sant'Egidio France une subvention pour l'organisation de la 19ème rencontre internationale pour la paix du 11 au 13 septembre 2005 à Lyon, et d'autre part, rejeté la demande présentée par elle et par M. Picquier devant ce tribunal et tendant à l'annulation de cette délibération ; <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code général des collectivités territoriales ;<br/>
<br/>
              Vu la loi du 9 décembre 1905 concernant la séparation des Eglises et de l'Etat ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Emilie Bokdam-Tognetti, Auditeur,<br/>
<br/>
              - les observations de la SCP Boutet, avocat de la FEDERATION DE LA LIBRE PENSEE ET D'ACTION SOCIALE DU RHONE, de la SCP Barthélemy, Matuchansky, Vexliard, avocat de l'association Communauté Sant'Egidio France et de la SCP Gaschignard, avocat de la commune de Lyon, <br/>
<br/>
              - les conclusions de Mme Emmanuelle Cortot-Boucher, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Boutet, avocat de la FEDERATION DE LA LIBRE PENSEE ET D'ACTION SOCIALE DU RHONE, à la SCP Barthélemy, Matuchansky, Vexliard, avocat de l'association Communauté Sant'Egidio France et à la SCP Gaschignard, avocat de la commune de Lyon ; <br/>
<br/>
<br/>
<br/>
              Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, par une délibération du 20 juin 2005, le conseil municipal de Lyon a attribué à l'association Communauté Sant'Egidio France une subvention pour l'organisation à Lyon, du 11 au 13 septembre 2005, de la 19ème rencontre internationale pour la paix ; que la FEDERATION DE LA LIBRE PENSEE ET D'ACTION SOCIALE DU RHONE et M. Picquier ont demandé au tribunal administratif de Lyon l'annulation de cette délibération ; que, par un jugement du 22 mars 2007, le tribunal a fait droit à cette demande ; que, par un arrêt du 21 avril 2009, contre lequel la FEDERATION DE LA LIBRE PENSEE ET D'ACTION SOCIALE DU RHONE se pourvoit en cassation, la cour administrative d'appel de Lyon a, sur l'appel de l'association Communauté Sant'Egidio France et de la commune de Lyon, annulé ce jugement et rejeté la demande présentée devant le tribunal administratif de Lyon ;<br/>
<br/>
              Considérant, en premier lieu, qu'aux termes de l'article 1er de la loi du 9 décembre 1905 concernant la séparation des Eglises et de l'Etat : " La République assure la liberté de conscience. Elle garantit le libre exercice des cultes sous les seules restrictions édictées ci-après dans l'intérêt de l'ordre public " ; que l'article 2 de cette loi dispose : " La République ne reconnaît, ne salarie ni ne subventionne aucun culte. En conséquence, à partir du 1er janvier qui suivra la promulgation de la présente loi, seront supprimées des budgets de l'Etat, des départements et des communes, toutes dépenses relatives à l'exercice des cultes. " ; qu'enfin, aux termes du dernier alinéa de l'article 19 de cette même loi, les associations formées pour subvenir aux frais, à l'entretien et à l'exercice d'un culte en vertu du titre IV de cette loi " ne pourront, sous quelque forme que ce soit, recevoir des subventions de l'Etat, des départements et des communes. Ne sont pas considérées comme subventions les sommes allouées pour réparations aux édifices affectés au culte public, qu'ils soient ou non classés monuments historiques. " ; <br/>
<br/>
              Considérant qu'il résulte des dispositions précitées de la loi du 9 décembre 1905 que les collectivités territoriales ne peuvent accorder aucune subvention, à l'exception des concours pour des travaux de réparation d'édifices cultuels, aux associations cultuelles au sens du titre IV de cette loi ; qu'il leur est également interdit d'apporter une aide quelconque à une manifestation qui participe de l'exercice d'un culte ; qu'elles ne peuvent accorder une subvention à une association qui, sans constituer une association cultuelle au sens du titre IV de la même loi, a des activités cultuelles, qu'en vue de la réalisation d'un projet, d'une manifestation ou d'une activité qui ne présente pas un caractère cultuel et n'est pas destiné au culte et à la condition, en premier lieu, que ce projet, cette manifestation ou cette activité présente un intérêt public local et, en second lieu, que soit garanti, notamment par voie contractuelle, que la subvention est exclusivement affectée au financement de ce projet, de cette manifestation ou de cette activité et n'est pas utilisée pour financer les activités cultuelles de l'association ; <br/>
<br/>
              Considérant, d'une part, qu'une association dont l'une des activités consiste en l'organisation de prières collectives de ses membres, ouvertes ou non au public, doit être regardée, même si elle n'est pas une " association cultuelle " au sens du titre IV de la loi du 9 décembre 1905, comme ayant, dans cette mesure, une activité cultuelle ; que tel n'est pas le cas, en revanche, d'une association dont des membres, à l'occasion d'activités associatives sans lien avec le culte, décident de se réunir, entre eux, pour prier ; que, dès lors, en jugeant que les seules circonstances qu'une association se réclame d'une confession particulière ou que certains de ses membres se réunissent, entre eux, en marge d'activités organisées par elle, pour prier, ne suffisent pas à établir que cette association a des activités cultuelles, la cour n'a pas commis d'erreur de droit ; <br/>
<br/>
              Considérant, d'autre part, qu'il ressort des pièces du dossier soumis à la cour que la 19ème rencontre internationale pour la paix a donné lieu à un ensemble de tables rondes et de conférences consacrées, dans l'esprit des rencontres d'Assise du 27 octobre 1986, au " courage d'un humanisme de paix " et a réuni plusieurs centaines d'invités et plusieurs milliers de participants ; qu'après avoir relevé que cette manifestation ne comportait la célébration d'aucune cérémonie cultuelle et que l'association organisatrice s'était bornée à prévoir un horaire libre, afin que les fidèles des différentes confessions puissent, s'ils le souhaitaient, participer, dans des édifices cultuels de leur choix, à des prières, la cour a jugé que, alors même que des personnalités religieuses figuraient parmi les participants et que certaines conférences portaient sur des thèmes en rapport avec les différentes religions représentées, la manifestation ne présentait pas un caractère cultuel et que la commune de Lyon avait pu, sans méconnaître les dispositions précitées de la loi du 9 décembre 1905, apporter un concours financier pour son organisation ; qu'en statuant ainsi, la cour a exactement qualifié les faits qui lui étaient soumis et n'a pas commis d'erreur de droit ;<br/>
<br/>
              Considérant, en deuxième lieu, que l'article L. 2121-13 du code général des collectivités territoriales dispose " Tout membre du conseil municipal a le droit, dans le cadre de sa fonction, d'être informé des affaires de la commune qui font l'objet d'une délibération " ; <br/>
<br/>
              Considérant qu'en jugeant que ces dispositions n'imposaient pas, en l'absence de demande de leur part, la communication des comptes et des statuts de l'association Communauté Sant'Egidio France aux conseillers municipaux préalablement à l'adoption de la délibération décidant d'octroyer une subvention à cette association pour l'organisation de la 19ème rencontre internationale pour la paix et en estimant que l'information délivrée aux membres du conseil municipal avait été suffisante pour leur permettre d'exercer leurs attributions et avait satisfait aux exigences de l'article L. 2121-13 du code général des collectivités territoriales, la cour n'a pas commis d'erreur de droit ni dénaturé les faits qui lui étaient soumis ; <br/>
<br/>
              Considérant, en troisième lieu, qu'aux termes du premier alinéa de l'article L. 2121-29 du même code : " Le conseil municipal règle par ses délibérations les affaires de la commune " ;<br/>
<br/>
              Considérant qu'en jugeant que la tenue à Lyon de la 19ème rencontre internationale pour la paix, qui respectait le principe de neutralité à l'égard des cultes, était, eu égard au nombre important des participants, notamment étrangers, et à l'intervention au cours des tables rondes de nombreuses personnalités nationales et internationales, positive pour " l'image de marque " et le rayonnement de la commune de Lyon et qu'elle était de nature à contribuer utilement à la vie économique de son territoire, et en en déduisant que l'octroi de la subvention en litige présentait un caractère d'intérêt public communal et que la délibération du conseil municipal du 20 juin 2005 trouvait dès lors un fondement légal dans les dispositions précitées de l'article L. 2121-29 du code général des collectivités territoriales, la cour a exactement qualifié les faits qui lui étaient soumis et n'a entaché son arrêt ni de  contradiction de motifs  ni d'erreur de droit ;<br/>
<br/>
              Considérant qu'il résulte de tout ce qui précède que la FEDERATION DE LA LIBRE PENSEE ET D'ACTION SOCIALE DU RHONE n'est pas fondée à demander l'annulation de l'arrêt qu'elle attaque ;<br/>
<br/>
              Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la FEDERATION DE LA LIBRE PENSEE ET D'ACTION SOCIALE DU RHONE le versement à la commune de Lyon d'une somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de la FEDERATION DE LA LIBRE PENSEE ET D'ACTION SOCIALE DU RHONE est rejeté.<br/>
Article 2 : La FEDERATION DE LA LIBRE PENSEE ET D'ACTION SOCIALE DU RHONE versera à la commune de Lyon une somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative. <br/>
Article 3 : La présente décision sera notifiée à la FEDERATION DE LA LIBRE PENSEE ET D'ACTION SOCIALE DU RHONE, à l'association Communauté Sant'Egidio France et à la commune de Lyon.<br/>
Copie en sera adressée pour information au ministre de l'intérieur, de l'outre-mer, des collectivités territoriales et de l'immigration.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">135-01 COLLECTIVITÉS TERRITORIALES. DISPOSITIONS GÉNÉRALES. - CONCOURS FINANCIERS DES COLLECTIVITÉS TERRITORIALES AUX CULTES - LOI DU 9 DÉCEMBRE 1905 - CADRE GÉNÉRAL - POSSIBILITÉ D'ACCORDER UNE SUBVENTION À UNE ASSOCIATION QUI, SANS CONSTITUER UNE ASSOCIATION CULTUELLE AU SENS DU TITRE IV DE CETTE LOI, A DES ACTIVITÉS CULTUELLES - EXISTENCE - CONDITIONS [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">135-02-01-02-01-02-02 COLLECTIVITÉS TERRITORIALES. COMMUNE. ORGANISATION DE LA COMMUNE. ORGANES DE LA COMMUNE. CONSEIL MUNICIPAL. ATTRIBUTIONS. DÉCISIONS RELEVANT DE LA COMPÉTENCE DU CONSEIL MUNICIPAL. - 1) CONCOURS FINANCIERS AUX CULTES - LOI DU 9 DÉCEMBRE 1905 - CADRE GÉNÉRAL - POSSIBILITÉ D'ACCORDER UNE SUBVENTION À UNE ASSOCIATION QUI, SANS CONSTITUER UNE ASSOCIATION CULTUELLE AU SENS DU TITRE IV DE CETTE LOI, A DES ACTIVITÉS CULTUELLES - EXISTENCE - CONDITIONS [RJ1] - 2) DÉLIBÉRATION OCTROYANT UNE SUBVENTION POUR L'ORGANISATION DANS LA COMMUNE D'UNE MANIFESTATION RESPECTANT LE PRINCIPE DE NEUTRALITÉ À L'ÉGARD DES CULTES, POSITIVE POUR L'IMAGE DE MARQUE ET LE RAYONNEMENT DE LA COMMUNE ET DE NATURE À CONTRIBUER UTILEMENT À LA VIE ÉCONOMIQUE DE SON TERRITOIRE - INTÉRÊT PUBLIC COMMUNAL - EXISTENCE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">135-02-04-02 COLLECTIVITÉS TERRITORIALES. COMMUNE. FINANCES COMMUNALES. DÉPENSES. - 1) CONCOURS FINANCIERS AUX CULTES - LOI DU 9 DÉCEMBRE 1905 - CADRE GÉNÉRAL - POSSIBILITÉ D'ACCORDER UNE SUBVENTION À UNE ASSOCIATION QUI, SANS CONSTITUER UNE ASSOCIATION CULTUELLE AU SENS DU TITRE IV DE CETTE LOI, A DES ACTIVITÉS CULTUELLES - EXISTENCE - CONDITIONS [RJ1] - 2) SUBVENTION POUR L'ORGANISATION DANS LA COMMUNE D'UNE MANIFESTATION RESPECTANT LE PRINCIPE DE NEUTRALITÉ À L'ÉGARD DES CULTES, POSITIVE POUR L'IMAGE DE MARQUE ET LE RAYONNEMENT DE LA COMMUNE ET DE NATURE À CONTRIBUER UTILEMENT À LA VIE ÉCONOMIQUE DE SON TERRITOIRE - INTÉRÊT PUBLIC COMMUNAL - EXISTENCE.
</SCT>
<SCT ID="8D" TYPE="PRINCIPAL">21 COLLECTIVITÉS TERRITORIALES. COMMUNE. FINANCES COMMUNALES. DÉPENSES. - 1) CONCOURS FINANCIERS DES COLLECTIVITÉS TERRITORIALES AUX CULTES - LOI DU 9 DÉCEMBRE 1905 - CADRE GÉNÉRAL - POSSIBILITÉ D'ACCORDER UNE SUBVENTION À UNE ASSOCIATION QUI, SANS CONSTITUER UNE ASSOCIATION CULTUELLE AU SENS DU TITRE IV DE CETTE LOI, A DES ACTIVITÉS CULTUELLES - EXISTENCE - CONDITIONS [RJ1] - 2) MANIFESTATION CULTUELLE - EXCLUSION - ENSEMBLE DE TABLES RONDES ET DE COLLOQUES NE COMPORTANT LA CÉLÉBRATION D'AUCUNE CÉRÉMONIE CULTUELLE - CIRCONSTANCE QUE LE PROGRAMME COMPORTE UN HORAIRE LIBRE POUR CEUX QUI SOUHAITERAIENT PARTICIPER À DES PRIÈRES DANS DES ÉDIFICES CULTUELS DE LEUR CHOIX - INCIDENCE - ABSENCE.
</SCT>
<ANA ID="9A"> 135-01 Il résulte des dispositions de la loi du 9 décembre 1905 concernant la séparation des Eglises et de l'Etat que les collectivités territoriales ne peuvent accorder aucune subvention, à l'exception des concours pour des travaux de réparation d'édifices cultuels, aux associations cultuelles au sens du titre IV de cette loi. Il leur est également interdit d'apporter une aide quelconque à une manifestation qui participe de l'exercice d'un culte. Elles ne peuvent accorder une subvention à une association qui, sans constituer une association cultuelle au sens du titre IV de la même loi, a des activités cultuelles, qu'en vue de la réalisation d'un projet, d'une manifestation ou d'une activité qui ne présente pas un caractère cultuel et n'est pas destiné au culte et à la condition, en premier lieu, que ce projet, cette manifestation ou cette activité présente un intérêt public local et, en second lieu, que soit garanti, notamment par voie contractuelle, que la subvention est exclusivement affectée au financement de ce projet, de cette manifestation ou de cette activité et n'est pas utilisée pour financer les activités cultuelles de l'association.</ANA>
<ANA ID="9B"> 135-02-01-02-01-02-02 1) Il résulte des dispositions de la loi du 9 décembre 1905 concernant la séparation des Eglises et de l'Etat que les collectivités territoriales ne peuvent accorder aucune subvention, à l'exception des concours pour des travaux de réparation d'édifices cultuels, aux associations cultuelles au sens du titre IV de cette loi. Il leur est également interdit d'apporter une aide quelconque à une manifestation qui participe de l'exercice d'un culte. Elles ne peuvent accorder une subvention à une association qui, sans constituer une association cultuelle au sens du titre IV de la même loi, a des activités cultuelles, qu'en vue de la réalisation d'un projet, d'une manifestation ou d'une activité qui ne présente pas un caractère cultuel et n'est pas destiné au culte et à la condition, en premier lieu, que ce projet, cette manifestation ou cette activité présente un intérêt public local et, en second lieu, que soit garanti, notamment par voie contractuelle, que la subvention est exclusivement affectée au financement de ce projet, de cette manifestation ou de cette activité et n'est pas utilisée pour financer les activités cultuelles de l'association.,,2) Présente un caractère d'intérêt public communal l'octroi d'une subvention pour l'organisation dans une commune d'une manifestation qui respecte le principe de neutralité à l'égard des cultes, est positive pour  l'image de marque  et le rayonnement de la commune, eu égard au nombre important des participants, notamment étrangers, et à l'intervention au cours des tables rondes de nombreuses personnalités nationales et internationales, et est de nature à contribuer utilement à la vie économique de son territoire.</ANA>
<ANA ID="9C"> 135-02-04-02 1) Il résulte des dispositions de la loi du 9 décembre 1905 concernant la séparation des Eglises et de l'Etat que les collectivités territoriales ne peuvent accorder aucune subvention, à l'exception des concours pour des travaux de réparation d'édifices cultuels, aux associations cultuelles au sens du titre IV de cette loi. Il leur est également interdit d'apporter une aide quelconque à une manifestation qui participe de l'exercice d'un culte. Elles ne peuvent accorder une subvention à une association qui, sans constituer une association cultuelle au sens du titre IV de la même loi, a des activités cultuelles, qu'en vue de la réalisation d'un projet, d'une manifestation ou d'une activité qui ne présente pas un caractère cultuel et n'est pas destiné au culte et à la condition, en premier lieu, que ce projet, cette manifestation ou cette activité présente un intérêt public local et, en second lieu, que soit garanti, notamment par voie contractuelle, que la subvention est exclusivement affectée au financement de ce projet, de cette manifestation ou de cette activité et n'est pas utilisée pour financer les activités cultuelles de l'association.,,2) Présente un caractère d'intérêt public communal l'octroi d'une subvention pour l'organisation dans une commune d'une manifestation qui respecte le principe de neutralité à l'égard des cultes, est positive pour  l'image de marque  et le rayonnement de la commune, eu égard au nombre important des participants, notamment étrangers, et à l'intervention au cours des tables rondes de nombreuses personnalités nationales et internationales, et est de nature à contribuer utilement à la vie économique de son territoire.</ANA>
<ANA ID="9D"> 21 1) Il résulte des dispositions de la loi du 9 décembre 1905 concernant la séparation des Eglises et de l'Etat que les collectivités territoriales ne peuvent accorder aucune subvention, à l'exception des concours pour des travaux de réparation d'édifices cultuels, aux associations cultuelles au sens du titre IV de cette loi. Il leur est également interdit d'apporter une aide quelconque à une manifestation qui participe de l'exercice d'un culte. Elles ne peuvent accorder une subvention à une association qui, sans constituer une association cultuelle au sens du titre IV de la même loi, a des activités cultuelles, qu'en vue de la réalisation d'un projet, d'une manifestation ou d'une activité qui ne présente pas un caractère cultuel et n'est pas destiné au culte et à la condition, en premier lieu, que ce projet, cette manifestation ou cette activité présente un intérêt public local et, en second lieu, que soit garanti, notamment par voie contractuelle, que la subvention est exclusivement affectée au financement de ce projet, de cette manifestation ou de cette activité et n'est pas utilisée pour financer les activités cultuelles de l'association.,,2) Une manifestation consistant dans un ensemble de tables rondes et de conférences consacrées au « courage d'un humanisme de paix » et ne comportant la célébration d'aucune cérémonie cultuelle ne revêt pas un caractère cultuel au motif que l'association organisatrice a prévu dans le programme une plage horaire libre, afin que les fidèles des différentes confessions puissent, s'ils le souhaitent, participer, dans des édifices cultuels de leur choix, à des prières.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, Assemblée, 19 juillet 2011, Fédération de la libre pensée et de l'action sociale du Rhône et Picquier, n° 308817, à publier au Recueil. Comp. CE, Section, 9 octobre 1992, Commune de Saint-Louis c/ Association Siva Soupramanien de Saint-Louis, n° 94455, p. 358.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
