<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038279168</ID>
<ANCIEN_ID>JG_L_2019_03_000000426200</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/27/91/CETATEXT000038279168.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème chambre, 27/03/2019, 426200, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-03-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>426200</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LYON-CAEN, THIRIEZ ; SCP DE NERVO, POUPET ; SCP GATINEAU, FATTACCINI</AVOCATS>
<RAPPORTEUR>M. Thomas Pez-Lavergne</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Gilles Pellissier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2019:426200.20190327</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
 Procédure contentieuse antérieure <br/>
<br/>
              La société Pentax France Lifecare a demandé au juge du référé précontractuel du tribunal administratif de Lyon, sur le fondement de l'article L. 551-1 du code de justice administrative, en premier lieu, d'enjoindre au groupement de coopération sanitaire UniHA de produire tout document permettant de constater que le produit présenté par la société Fujifilm dans son offre de base et lors des essais était conforme aux exigences du cahier des clauses techniques particulières applicable au marché relatif à la fourniture d'équipements de vidéo-chirurgie et de vidéo-endoscopie souple, maintenance, formation et services associés qu'il entendait conclure, en deuxième lieu, d'annuler la consultation lancée par le groupement UniHA pour l'attribution du lot n° 9, en troisième lieu, d'annuler la décision par laquelle le groupement UniHA a attribué le marché et a rejeté son offre, en dernier lieu, d'enjoindre au groupement UniHA de reprendre la procédure de passation du marché public dans son intégralité en se conformant aux dispositions législatives et réglementaires en vigueur et aux principes régissant la publicité et la mise en concurrence.<br/>
<br/>
              Par une ordonnance n° 1808081 du 28 novembre 2018, le juge des référés du tribunal administratif de Lyon a annulé la procédure de passation du lot n° 9 et enjoint au groupement UniHA, s'il entendait conclure le marché afférent à ce lot, de reprendre la procédure au stade de l'examen des offres.<br/>
<br/>
<br/>
Procédures devant le Conseil d'Etat <br/>
<br/>
              1° Sous le n° 426200, par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 12 et 27 décembre 2018 et le 4 mars 2019 au secrétariat du contentieux du Conseil d'Etat, la société Fujifilm France demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) réglant l'affaire au titre de la procédure de référé engagée, de rejeter la requête de la société Pentax France Lifecare ;<br/>
<br/>
              3°) de mettre à la charge de la société Pentax France Lifecare la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              2° Sous le n° 426265, par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 14 et 24 décembre 2018 et le 8 mars 2019 au secrétariat du contentieux du Conseil d'Etat, le groupement de coopération sanitaire UniHA demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) réglant l'affaire au titre de la procédure de référé engagée, de rejeter la requête de  la société Pentax France Lifecare ;<br/>
<br/>
              3°) de mettre à la charge de la société Pentax France Lifecare la somme de 4 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu : <br/>
              - l'ordonnance n° 2015-899 du 23 juillet 2015 ;<br/>
              - le décret n° 2016-360 du 25 mars 2016 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Thomas Pez-Lavergne, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Gilles Pellissier, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Lyon Caen, Thiriez, avocat de la société Fujifilm France, à la SCP de Nervo, Poupet, avocat de la société Pentax France Lifecare et à la SCP Gatineau, Fattaccini, avocat du groupement de coopération sanitaire UniHA.<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Les pourvois de la société Fujifilm France et du groupement de coopération sanitaire UniHA étant dirigés contre la même ordonnance, il y a lieu de les joindre pour statuer par une seule décision.<br/>
<br/>
              2. Aux termes de l'article L. 551-1 du code de justice administrative : " Le président du tribunal administratif, ou le magistrat qu'il délègue, peut être saisi en cas de manquement aux obligations de publicité et de mise en concurrence auxquelles est soumise la passation par les pouvoirs adjudicateurs de contrats administratifs ayant pour objet l'exécution de travaux, la livraison de fournitures ou la prestation de services, avec une contrepartie économique constituée par un prix ou un droit d'exploitation (...) ".<br/>
<br/>
              3. Il ressort des pièces du dossier soumis au juge des référés du tribunal administratif de Lyon que le groupement de coopération sanitaire UniHA a lancé, en mai 2018, une procédure de passation d'un accord-cadre avec bons de commande, comportant douze lots, ayant pour objet la fourniture, l'installation et la mise en service d'équipements de vidéo-chirurgie et de vidéo-endoscopie souple, avec les services associés de maintenance et de formation. Le lot n° 9 portait sur la fourniture de " colonnes de vidéo-endoscopie souples gastrologie et pneumologie garanties deux ans ". La société Pentax France Lifecare a été informée, le 25 octobre 2018, du rejet de son offre et de l'attribution du lot n° 9 à la société Fujifilm France. Par une ordonnance du 28 novembre 2018, le juge du référé précontractuel du tribunal administratif de Lyon a, à la demande de la société Pentax France Lifecare, annulé la procédure de passation du lot n° 9 du marché et enjoint au groupement UniHA, s'il entendait conclure le marché afférent à ce lot, de reprendre la procédure au stade de l'examen des offres. La société Fujifilm France et le groupement UniHA se pourvoient en cassation contre cette ordonnance.<br/>
<br/>
              4. En premier lieu, aux termes de l'article R. 742-5 du code de justice administrative : " La minute de l'ordonnance est signée du seul magistrat qui l'a rendue. (...) ". Il ressort de la minute de l'ordonnance attaquée que celle-ci porte la signature du juge des référés qui l'a rendue. Ainsi, le moyen soulevé par le groupement UniHA tiré de ce que les dispositions précitées de l'article R. 742-5 du code de justice administrative auraient été méconnues manque en fait et doit être écarté.<br/>
<br/>
              5. En deuxième lieu, il ressort des énonciations de l'ordonnance attaquée que, selon l'article 11 du chapitre 5 du cahier des clauses techniques particulières, intitulé " définition des besoins ", l'offre de base relative au lot n° 9 du marché devait présenter " l'intégralité des items (...) 1, 20, 32, 63 ", l'item 32 correspondant, selon l'article 1er de ce chapitre, à une " source de lumière froide Xénon ". Par ailleurs, selon le même article 11 du chapitre 5, les candidats pouvaient, en " fonctionnalité supplémentaire éventuelle facultative ", proposer les items énumérés en annexe du cahier des clauses techniques particulières, parmi lesquels figurait l'item 28 correspondant à une " source de lumière froide Led ". Il ressort, en outre, du glossaire figurant au chapitre 2 du cahier des clauses techniques particulières, soumis au juge des référés que l'offre de base, définie comme un " contenu technique minimal imposé par le pouvoir adjudicateur pour que l'offre soit jugée conforme techniquement " pouvait " être complétée ou diminuée de certains éléments lors de la commande grâce aux prestations supplémentaires éventuelles obligatoires ou facultatives ". Par suite le juge des référés, dont l'ordonnance est suffisamment motivée, n'a pas dénaturé la portée des clauses du cahier des clauses techniques particulières en estimant qu'elles imposaient que l'offre de base comprenne une " source lumineuse Xénon " et en relevant que l'offre de la société Fujifilm France, qui comprenait uniquement une source lumineuse Led, ne respectait pas ces exigences et était, par voie de conséquence, irrégulière.<br/>
<br/>
              6. En dernier lieu, aux termes de l'article 59 du décret du 25 mars 2016 relatif aux marchés publics : " I. - L'acheteur vérifie que les offres qui n'ont pas été éliminées en application du IV de l'article 43 sont régulières, acceptables et appropriées. / Une offre irrégulière est une offre qui ne respecte pas les exigences formulées dans les documents de la consultation notamment parce qu'elle est incomplète, ou qui méconnaît la législation applicable notamment en matière sociale et environnementale. (...) / II. - Dans les procédures d'appel d'offres et les procédures adaptées sans négociation, les offres irrégulières, inappropriées ou inacceptables sont éliminées. Toutefois, l'acheteur peut autoriser tous les soumissionnaires concernés à régulariser les offres irrégulières dans un délai approprié, à condition qu'elles ne soient pas anormalement basses. (...) / IV. - La régularisation des offres irrégulières ne peut avoir pour effet de modifier des caractéristiques substantielles des offres ". Il résulte de ces dispositions que l'acheteur doit éliminer les offres qui ne respectent pas les exigences formulées dans les documents de la consultation, sauf, le cas échéant, s'il a autorisé leur régularisation.<br/>
<br/>
              7. Il ressort des pièces du dossier soumis au juge des référés que, le juge des référés du tribunal administratif de Lyon n'a pas commis d'erreur de droit en estimant que le pouvoir adjudicateur devait éliminer son offre, dès lors que celle-ci proposait uniquement une source lumineuse Led alors que, comme il a été dit précédemment, le cahier des clauses techniques particulières imposait que l'offre des candidats comporte nécessairement " une source de lumière froide Xénon " et éventuellement, en fonctionnalité facultative supplémentaire, une " source de lumière froide Led ".<br/>
<br/>
              8. Il résulte de ce qui précède que les pourvois de la société Fujifilm France et du groupement UniHA doivent être rejetés.<br/>
<br/>
              9. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de la société Pentax France Lifecare qui n'est pas, dans la présente instance, la partie perdante. En revanche, il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la société Fujifilm France et du groupement de coopération sanitaire UniHA la somme de 2 000 euros, chacun, à verser à la société Pentax France Lifecare, au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Les pourvois de la société Fujifilm France et du groupement de coopération sanitaire UniHA sont rejetés.<br/>
Article 2 : La société Fujifilm France et le groupement de coopération sanitaire UniHA verseront chacun à la société Pentax France Lifecare une somme de 2 000 euros, au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée à la société Fujifilm France, au groupement de coopération sanitaire UniHA et à la société Pentax France Lifecare.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
