<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030538088</ID>
<ANCIEN_ID>JG_L_2015_04_000000388069</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/53/80/CETATEXT000030538088.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème et 8ème sous-sections réunies, 29/04/2015, 388069, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-04-29</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>388069</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème et 8ème sous-sections réunies</FORMATION>
<TYPE_REC>Autres</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR>M. Pierre Lombard</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Vincent Daumas</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:388069.20150429</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société Icade, à l'appui de sa demande tendant à la décharge des suppléments d'impôt sur les sociétés, majorations et pénalités mises à sa charge au titre des exercices clos au cours des années 2007 à 2010, a produit un mémoire, enregistré le 19 décembre 2014 au greffe du tribunal administratif de Montreuil, en application de l'article 23-1 de l'ordonnance n° 58-1067 du 7 novembre 1958, par lequel elle soulève une question prioritaire de constitutionnalité.<br/>
<br/>
              Par une ordonnance n° 1411845 du 16 février 2015, enregistrée le 18 février 2015 au secrétariat du contentieux du Conseil d'Etat, le président de la première chambre de ce tribunal, avant qu'il soit statué sur la demande de la société Icade, a décidé, par application des dispositions de l'article 23-2 de l'ordonnance n° 58-1067 du 7 novembre 1958, de transmettre au Conseil d'Etat la question de la conformité aux droits et libertés garantis par la Constitution de l'article 208 C ter du code général des impôts.<br/>
<br/>
              Dans la question prioritaire de constitutionnalité transmise, et dans un mémoire enregistré le 23 mars 2015 au secrétariat du contentieux du Conseil d'Etat, la société Icade soutient que l'article 208 C ter du code général des impôts, applicable au litige, méconnaît les principes constitutionnels d'égalité devant la loi et d'égalité devant les charges publiques, garantis par les articles 6 et 13 de la Déclaration des droits de l'homme et du citoyen, ainsi que la garantie des droits proclamée par l'article 16 de cette même Déclaration.<br/>
<br/>
              Par un mémoire, enregistré le 3 mars 2015, le ministre des finances et des comptes publics soutient que les conditions posées par l'article 23-4 de l'ordonnance du 7 novembre 1958 ne sont pas remplies. <br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la Constitution, notamment son Préambule et son article 61-1 ;<br/>
              - l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
              - le code général des impôts ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Pierre Lombard, auditeur,<br/>
<br/>
              - les conclusions de M. Vincent Daumas, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Piwnica, Molinié, avocat de la société Icade ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il résulte des dispositions de l'article 23-4 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel que, lorsqu'une juridiction relevant du Conseil d'Etat a transmis à ce dernier, en application de l'article 23-2 de cette même ordonnance, la question de la conformité à la Constitution d'une disposition législative, le Conseil constitutionnel est saisi de cette question de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux ;<br/>
<br/>
              2. Considérant qu'en vertu des dispositions de l'article 1663 du code général des impôts, les sociétés d'investissements immobiliers cotées visées au I de l'article 208 C du même code qui ont opté, en application du II de cet article, pour l'exonération d'impôt sur les sociétés pour la fraction de leur bénéfice provenant de la location des immeubles et droits réels qu'il mentionne et des plus-values sur la cession de tels biens acquittent l'impôt au titre des plus-values latentes au cours de l'année d'option pour le quart de son montant, le solde étant versé par fractions égales au cours des trois années suivantes ; qu'aux termes de l'article 208 C ter du même code : " Lorsque, postérieurement à l'exercice de l'option prévue au premier alinéa du II de l'article 208 C, des immeubles, des droits réels mentionnés au sixième alinéa du II de ce même article, des droits afférents à un contrat de crédit-bail portant sur un immeuble, des droits portant sur un immeuble dont la jouissance a été conférée à titre temporaire par l'Etat, une collectivité territoriale ou un de leurs établissements publics ou des participations dans des personnes visées à l'article 8 deviennent éligibles à l'exonération mentionnée à cet alinéa, la société doit réintégrer à son résultat fiscal soumis à l'impôt sur les sociétés une somme correspondant à la plus-value calculée par différence entre la valeur réelle de ces biens à cette date et leur valeur fiscale. Cette réintégration est effectuée par parts égales sur une période de quatre ans. La cession des biens concernés entraîne l'imposition immédiate de la plus-value qui n'a pas encore été réintégrée. " ;<br/>
<br/>
              3. Considérant que la société Icade soutient que ces dispositions méconnaissent le principe constitutionnel d'égalité devant la loi et devant les charges publiques, garanti par les articles 6 et 13 de la Déclaration des droits de l'homme et du citoyen, dès lors que, à la différence du régime applicable aux sociétés ayant exercé l'option prévue à l'article 208 C du code général des impôts, l'article 208 C ter du même code ne garantit pas le maintien des règles en vigueur l'année de la fusion, en particulier le taux d'imposition, pour l'imposition des plus-values latentes réintégrées dans le résultat fiscal de la société mère déjà soumise au régime des sociétés d'investissements immobilières cotées ; que, selon elle, les dispositions contestées méconnaissent également la garantie des droits proclamée par l'article 16 de la Déclaration des droits de l'homme et du citoyen ;<br/>
<br/>
              4. Considérant que les dispositions de l'article 208 C ter du code général des impôts sont applicables au litige dont est saisi le tribunal administratif de Montreuil ; qu'elles n'ont pas déjà été déclarées conformes à la Constitution par le Conseil constitutionnel ; que le moyen tiré de ce que ces dispositions portent atteinte aux droits et libertés garantis par la Constitution, notamment au principe d'égalité devant la loi, soulève une question présentant un caractère sérieux ; qu'ainsi, il y a lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité invoquée ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La question de la conformité à la Constitution de l'article 208 C ter du code général des impôts est renvoyée au Conseil constitutionnel.<br/>
<br/>
Article 2 : La présente décision sera notifiée à la société Icade et au ministre des finances et des comptes publics.<br/>
Copie en sera adressée au Premier ministre et au tribunal administratif de Montreuil.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
