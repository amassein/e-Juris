<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031464484</ID>
<ANCIEN_ID>JG_L_2015_11_000000388890</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/46/44/CETATEXT000031464484.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème / 7ème SSR, 09/11/2015, 388890, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-11-09</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>388890</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème / 7ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DELAPORTE, BRIARD, TRICHET</AVOCATS>
<RAPPORTEUR>Mme Sophie-Caroline de Margerie</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Xavier Domino</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:388890.20151109</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête sommaire et un mémoire complémentaire, enregistrés au secrétariat du contentieux du Conseil d'Etat les 23 mars et 4 mai 2015, M. A...B...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret du 28 novembre 2014 par lequel le Premier ministre a accordé son extradition aux autorités britanniques ;<br/>
<br/>
              2°) à titre subsidiaire, de renvoyer à la Cour de justice de l'Union européenne la question de l'applicabilité de l'article 8 de la convention européenne d'extradition du 27 septembre 1996 à des faits entièrement prescrits selon la législation de l'Etat requis à la date d'entrée en vigueur de cette convention dans l'ordre juridique de l'Etat requis ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 500 au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la convention européenne d'extradition du 13 décembre 1957 ;<br/>
              - la convention établie sur la base de l'article K 3 du traité sur l'Union européenne, relative à l'extradition entre les Etats membres de l'Union européenne, signée à Dublin le 27 septembre 1996 ;<br/>
              - le code pénal ;<br/>
              - le code de procédure pénale ;<br/>
              - la loi n° 79-587 du 11 juillet 1979 ;<br/>
              - la loi n° 2000-321 du 12 avril 2000 ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Sophie-Caroline de Margerie, conseiller d'Etat, <br/>
<br/>
              - les conclusions de M. Xavier Domino, rapporteur public,<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Delaporte, Briard, Trichet, avocat de M. B...; 		<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Sur la légalité externe du décret :<br/>
<br/>
              1.	Considérant, en premier lieu, que le décret attaqué mentionne les stipulations conventionnelles et les dispositions législatives sur lesquelles il se fonde ; que, par ailleurs, si les stipulations du paragraphe 2 de l'article 8 de la convention du 27 septembre 1996 relative à l'extradition entre les Etats membres de l'Union européenne permettent dans certains cas à l'Etat requis de refuser une extradition au motif que l'action serait prescrite selon sa propre législation, il ne résulte d'aucune disposition qu'un décret accordant une extradition devrait énoncer les motifs pour lesquels le Gouvernement renonce à faire usage de cette faculté ; qu'il résulte de ce qui précède que M. B...n'est pas fondé à soutenir que le décret attaqué serait insuffisamment motivé ; <br/>
<br/>
              2.	Considérant, en second lieu, qu'aux termes de l'article 24 de la loi du 12 avril 2000 relative aux droits des citoyens dans leurs relations avec les administrations : " Les décisions individuelles qui doivent être motivées en application des articles 1er et 2 de la loi du 11 juillet 1979 relative à la motivation des actes administratifs et à l'amélioration des relations entre l'administration et le public, n'interviennent qu'après que la personne intéressée a été mise à même de présenter des observations écrites et, le cas échéant, sur sa demande, des observations orales... - Les dispositions de l'alinéa précédent ne sont pas applicables... aux décisions pour lesquelles des dispositions législatives ont instauré une procédure contradictoire particulière " ; qu'en l'absence de stipulations sur ce point dans la convention du 27 septembre 1996, les articles 696-8 et suivants du code de procédure pénale régissent la procédure préalable à l'extradition en prévoyant une procédure contradictoire particulière ; qu'ainsi les prescriptions du premier alinéa de l'article 24 ne peuvent être utilement invoquées par M. B...pour soutenir que le décret qu'il attaque aurait été pris à l'issue d'une procédure irrégulière ; <br/>
<br/>
              Sur la légalité interne du décret :<br/>
<br/>
              3.	Considérant que le décret attaqué a été pris sur le fondement de la convention relative à l'extradition entre les Etats membres de l'Union européenne, signée à Dublin le 27 septembre 1996, dont les stipulations sont applicables en vertu du paragraphe 5 de son article 18 aux demandes d'extradition présentées postérieurement à son entrée en vigueur et qui s'est substituée, entre la France et le Royaume Uni, à compter du 1er juillet 2005, date de l'entrée en vigueur dans chacun de ces pays, aux stipulations contraires de la convention européenne d'extradition du 13 décembre 1957 ;<br/>
<br/>
              4.	Considérant, en premier lieu, que les conventions d'extradition sont des lois de procédure qui, sauf stipulation contraire, sont applicables immédiatement aux faits survenus avant leur entrée en vigueur ; qu'il en est ainsi, notamment, des conditions qu'elles fixent pour la prescription de l'action publique ou de la peine ; que les stipulations du paragraphe 1 de l'article 8 de la convention du 27 septembre 1996 selon lesquelles " L'extradition ne peut être refusée au motif qu'il y a prescription de l'action ou de la peine selon la législation de l'Etat requis " étaient applicables à la demande d'extradition de M. B...présentée par les autorités britanniques ; que, par suite, et sans qu'il y ait lieu de saisir la Cour de justice de l'Union européenne d'une question préjudicielle, le moyen tiré de ce que l'action publique à l'égard des faits commis par M. B...était prescrite en droit français à la date de la demande d'extradition ne peut qu'être écarté ;<br/>
<br/>
              5.	Considérant, en second lieu, que les dispositions de l'article 696-6 du code de procédure pénale, aux termes desquelles : " Sous réserve des exceptions prévues à l'article 696-34, l'extradition n'est accordée qu'à la condition que la personne extradée ne sera ni poursuivie, ni condamnée pour une infraction autre que celle ayant motivé l'extradition et antérieure à la remise ", qui ont un caractère supplétif en vertu des prescriptions de l'article 696 du même code, ne sont pas applicables à la demande d'extradition formulée contre M. B...; que sont applicables, en revanche, les stipulations de l'article 10 de la convention de Dublin relatives à la règle de la spécialité ; qu'il ne résulte pas du dossier et n'est d'ailleurs pas allégué par M. B...que les autorités britanniques n'entendraient pas respecter l'engagement résultant pour elles de cet article ;<br/>
<br/>
              6.	Considérant qu'il résulte de tout ce qui précède que M. B...n'est pas fondé à demander l'annulation du décret du 28 novembre 2014 accordant son extradition aux autorités britanniques ; que ses conclusions tendant à ce qu'une somme soit mise à la charge de l'Etat au titre de l'article L. 761-1 du code de justice administrative ne peuvent, par suite, qu'être rejetées ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de M. B...est rejetée.<br/>
<br/>
Article 2 : La présente décision sera notifiée à M. A...B...et à la garde des sceaux, ministre de la justice.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
