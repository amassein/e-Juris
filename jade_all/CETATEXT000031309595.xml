<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031309595</ID>
<ANCIEN_ID>JG_L_2015_10_000000370710</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/30/95/CETATEXT000031309595.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  6ème sous-section jugeant seule, 09/10/2015, 370710, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-10-09</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>370710</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 6ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BARADUC, DUHAMEL, RAMEIX</AVOCATS>
<RAPPORTEUR>Mme Clémence Olsina</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Xavier de Lesquen</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2015:370710.20151009</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A...B...a demandé au tribunal administratif de Paris, en premier lieu, d'annuler pour excès de pouvoir l'arrêté du ministre de l'éducation nationale du 23 décembre 2010 portant inscription au tableau d'avancement au grade hors classe des inspecteurs d'académie et inspecteurs pédagogiques régionaux au titre de l'année 2011, la décision du 20 juillet 2011 rejetant son recours gracieux contre cet arrêté, l'arrêté du 13 janvier 2011 du même ministre portant nomination au grade hors classe des inspecteurs d'académie et inspecteurs pédagogiques régionaux au titre de l'année 2011, l'arrêté du 12 janvier 2012 portant inscription au tableau d'avancement au grade hors classe des inspecteurs d'académie et inspecteurs pédagogiques régionaux au titre de l'année 2012, en deuxième lieu, d'enjoindre au ministre de l'éducation nationale de le nommer au grade hors classe au titre de l'année 2011 ou, à défaut de dresser un nouveau tableau d'avancement dans un délai de quatre mois sous astreinte de 150 euros par jour de retard et, en troisième lieu, de condamner l'Etat à lui verser la somme de 6 000 euros ainsi que les sommes qu'il aurait perçues s'il avait été nommé au grade hors classe, en réparation du préjudice subi du fait de son éviction illégale du tableau d'avancement ainsi que les intérêts et la capitalisation des intérêts.<br/>
<br/>
              Par un jugement n° 1203786/5-4 du 28 mai 2013, le tribunal administratif de Paris a annulé l'arrêté du 13 janvier 2011, la décision du 20 juillet 2011 et l'arrêté du 12 janvier 2012, a fait partiellement droit aux conclusions de M. B...aux fins d'indemnisation, et a rejeté ses conclusions aux fins d'injonction.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 30 juillet et 29 octobre 2013 au secrétariat du contentieux du Conseil d'Etat, M. B...demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler le jugement n° 1203786/5-4 du 28 mai 2013 du tribunal administratif de Paris en tant qu'il a rejeté ses conclusions aux fins d'injonction et n'a fait que partiellement droit à sa demande d'indemnisation ;<br/>
<br/>
              2°) réglant l'affaire au fond, d'enjoindre au ministre de l'éducation nationale de le nommer au grade hors classe pour les années 2011 et 2012, ou, subsidiairement, de procéder à un nouvel examen de sa candidature au titre d'un avancement au grade hors classe pour les années 2011 et 2012, dans un délai de deux mois à compter de la décision à intervenir et sous astreinte de 150 euros par jour de retard, et de condamner l'Etat à lui verser la différence entre les sommes qu'il aurait dû percevoir s'il avait été inscrit au tableau d'avancement pour les années 2011 et 2012 et celles qui lui ont été allouées ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
              Vu :<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Clémence Olsina, auditeur,  <br/>
<br/>
              - les conclusions de M. Xavier de Lesquen, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Baraduc, Duhamel, Rameix, avocat de M. B...;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant que M.B..., membre du corps des inspecteurs d'académie et inspecteurs pédagogiques régionaux, a demandé au tribunal administratif de Paris, en premier lieu, d'annuler pour excès de pouvoir, notamment, l'arrêté du ministre de l'éducation nationale du 23 décembre 2010 portant inscription des inspecteurs d'académie et inspecteurs pédagogiques régionaux (IA-IPR) sur le tableau d'avancement au grade hors classe de ce corps au titre de l'année 2011 et l'arrêté du 13 janvier 2011 du même ministre portant nomination au grade hors classe de ce corps, en deuxième lieu, d'enjoindre au ministre de le nommer au grade hors classe au titre de l'année 2011 ou à défaut de dresser un nouveau tableau d'avancement pour l'année 2011 et, en troisième lieu, de condamner l'Etat à lui verser la somme de 6 000 euros en raison du préjudice subi, les sommes qu'il aurait perçues s'il n'avait pas été illégalement évincé du tableau d'avancement, et les intérêts et la capitalisation des intérêts ; que, par jugement du 28 mai 2013, le tribunal administratif de Paris a annulé, notamment, l'arrêté du 13 janvier 2011, a fait partiellement droit à ses conclusions indemnitaires et a rejeté ses conclusions aux fins d'injonction ; que M. B...se pourvoit en cassation contre ce jugement, en tant qu'il a rejeté ses conclusions aux fins d'injonction et n'a fait que partiellement droit à sa demande d'indemnisation ;  que le ministre de l'éducation nationale, de l'enseignement supérieur et de la recherche forme un pourvoi incident contre ce jugement en tant qu'il a annulé son arrêté du 13 janvier 2011 et sa décision du 20 juillet 2011 rejetant le recours gracieux de M. B...;<br/>
<br/>
              Sur le pourvoi incident du ministre de l'éducation nationale, de l'enseignement supérieur et de la recherche :<br/>
<br/>
              2. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que le recours gracieux présenté par M. B...le 4 avril 2011, qui ne mentionne pas l'arrêté du 13 janvier 2011 et se borne à répondre à la lettre du ministre du 16 février 2011 consécutive au refus de l'inscrire au tableau d'avancement pour l'accès au grade hors classe des IA-IPR au titre de l'année 2011, ne peut être regardé que comme étant exclusivement dirigé contre l'arrêté du 23 décembre 2010 portant tableau d'avancement au grade hors classe au titre de l'année 2011 ; que, par suite, en estimant, pour écarter la fin de non-recevoir soulevée par le ministre, que le recours du 4 avril 2011 portait indifféremment sur le tableau d'avancement adopté par arrêté du 23 décembre 2010 et sur l'arrêté du 13 janvier 2011, et en déduire que ce recours avait pu par suite conserver le délai de recours contentieux contre l'arrêté du 13 janvier 2011 et la décision du 20 juillet 2011, le tribunal administratif de Paris a dénaturé les pièces du dossier ; que, par suite, le ministre de l'éducation nationale, de l'enseignement supérieur et de la recherche est fondé à demander l'annulation du jugement attaqué en tant qu'il a annulé l'arrêté du ministre de l'éducation nationale du 13 janvier 2011 et la décision du 20 juillet 2011 ;<br/>
<br/>
              4. Considérant qu'il y a lieu, dans les circonstances de l'espèce et dans la mesure de la cassation ainsi prononcée, de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative ; <br/>
<br/>
              5. Considérant que, vis-à-vis des tiers, le délai de recours contentieux contre les tableaux d'avancement court à compter de leur publication ; qu'il ressort des pièces du dossier que le tableau d'avancement du 23 décembre 2010 a été publié au bulletin officiel de l'éducation nationale n° 4 du 27 janvier 2011, l'arrêté du 13 janvier 2011 nommant les intéressés ayant été publié pour sa part, au bulletin officiel de l'éducation nationale du 24 février 2011 ; que le recours gracieux présenté par M. B...le 4 avril 2011, qui contestait, ainsi qu'il a été dit, exclusivement le tableau d'avancement du 23 décembre 2010, n'a pu conserver le délai du recours contentieux contre l'arrêté du 13 janvier 2011 ;<br/>
<br/>
              6. Considérant qu'il résulte de ce qui précède que, la demande de M. B...ayant été enregistrée au greffe du tribunal administratif  le 5 mars 2012, les conclusions dirigées contre l'arrêté du 13 janvier 2011 et, par voie de conséquence, celles dirigées contre la décision du 20 juillet 2011 rejetant le recours gracieux de M.B..., sont tardives et, par suite, irrecevables ; que, dès lors, les conclusions de M. B...aux fins d'injonction et d'indemnisation ne peuvent, dans cette mesure, qu'être rejetées ; <br/>
<br/>
              Sur le pourvoi principal :<br/>
<br/>
              7. Considérant que l'annulation du jugement du 28 mai 2013 en tant qu'il annule l'arrêté du 13 janvier 2011 et la décision du 20 juillet 2011, puis le rejet des conclusions de M. B...contre ces décisions, prononcé au point 6, privent d'objet, dans cette mesure, le pourvoi formé par M. B...contre ce même jugement ; que le pourvoi de M. B...conserve en revanche son objet en ce qui concerne le rejet partiel des conclusions aux fins d'injonction et des conclusions indemnitaires liées à l'annulation de l'arrêté du 12 janvier 2012, non contestée par le pourvoi incident du ministre ;<br/>
<br/>
              8. Considérant, en premier lieu, que le tribunal administratif a estimé que l'annulation du tableau d'avancement au grade hors classe des inspecteurs d'académie et des inspecteurs pédagogiques régionaux au titre de 2012 n'impliquait pas l'inscription du requérant sur ce tableau d'avancement ni le réexamen de sa candidature ; qu'en se fondant sur la circonstance que M. B...n'avait pas contesté les nominations prises en application de ce tableau et qu'il ne ressortait pas des pièces du dossier que ces décisions n'étaient pas devenues définitives, le tribunal administratif de Paris n'a pas commis d'erreur de droit ;<br/>
<br/>
              9. Considérant, en deuxième lieu, que le tribunal administratif a pu, sans entacher son jugement d'erreur de droit, déduire de ce que M. B...ne pouvait se prévaloir d'un droit à être inscrit au tableau d'avancement litigieux qu'il ne pouvait prétendre à une indemnisation du préjudice de carrière allégué ; <br/>
<br/>
              10. Considérant, enfin, que le tribunal, en relevant que le ministère d'avocat n'était pas obligatoire pour le contentieux introduit par M.B..., et en estimant que sa décision d'engager des frais d'avocat n'était pas en lien direct avec les fautes constatées, n'a entaché son jugement ni d'erreur de droit ni de dénaturation ; <br/>
<br/>
              Sur les conclusions de M. B...présentées au titre de l'article L. 761-1 du code de justice administrative : <br/>
<br/>
              11. Considérant qu'il résulte de ce qui précède que M.B..., dans la mesure où son pourvoi conserve son objet, n'est pas fondé à demander l'annulation du jugement qu'il attaque ;<br/>
<br/>
              12. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante ; <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du tribunal administratif de Paris du 28 mai 2013 est annulé en tant qu'il annule l'arrêté du ministre de l'éducation nationale du 13 janvier 2011 portant nomination au grade hors classe des inspecteurs d'académie et inspecteurs pédagogiques régionaux au titre de l'année 2011 et la décision du même ministre du 20 juillet 2011 rejetant le recours gracieux de M. B...contre cet arrêté. <br/>
Article 2 : Les conclusions présentées par M. B...devant le tribunal administratif de Paris contre l'arrêté du 13 janvier 2011 et la décision du 20 juillet 2011, ainsi que ses conclusions aux fins d'injonction et d'indemnisation sont rejetées.<br/>
Article 3 : Il n'y a pas lieu de statuer sur les conclusions du pourvoi de M. B...relatives aux conclusions à fin d'injonction et d'indemnisation relatives à l'arrêté du 13 janvier 2011 et à la décision du 20 juillet 2011.<br/>
Article 4 : Le surplus des conclusions du pourvoi de M. B...est rejeté.<br/>
Article 5 : La présente décision sera notifiée à M. A...B...et à la ministre de l'éducation nationale, de l'enseignement supérieur et de la recherche.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
