<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026845854</ID>
<ANCIEN_ID>JG_L_2012_12_000000349070</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/84/58/CETATEXT000026845854.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  9ème sous-section jugeant seule, 26/12/2012, 349070, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2012-12-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>349070</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 9ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP TIFFREAU, CORLAY, MARLANGE</AVOCATS>
<RAPPORTEUR>M. Olivier Gariazzo</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Aladjidi</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2012:349070.20121226</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi, enregistré le 6 mai 2011 au secrétariat du contentieux du Conseil d'Etat, présenté par le ministre du budget, des comptes publics et de la réforme de l'État, porte-parole du Gouvernement ; le ministre demande au Conseil d'État d'annuler l'arrêt n° 09PA03148 du 31 mars 2011 par lequel la cour administrative d'appel de Paris a, sur l'appel de la société BNP Paribas, annulé le jugement n° 0417809/1-2 du 24 mars 2009 par lequel le tribunal administratif de Paris a rejeté la demande de cette société tendant à la décharge des cotisations supplémentaires d'impôt sur les sociétés et de contributions additionnelles sur cet impôt auxquelles elle a été assujettie au titre de l'année 1998, ainsi que des pénalités correspondantes ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code général des impôts et le livre des procédures fiscales ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Olivier Gariazzo, Maître des Requêtes en service extraordinaire,  <br/>
<br/>
              - les observations de la SCP Tiffreau, Corlay, Marlange, avocat de la société BNP Paribas,<br/>
<br/>
              - les conclusions de M. Frédéric Aladjidi, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Tiffreau, Corlay, Marlange, avocat de la société BNP Paribas ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces des dossiers soumis aux juges du fond qu'à l'issue d'une vérification de comptabilité, la société BNP a été assujettie, sur le fondement de l'article 209 B du code général des impôts, au titre de l'exercice 1998, à l'impôt sur les sociétés et à la contribution additionnelle sur cet impôt, ainsi qu'aux pénalités correspondantes, à raison de l'inclusion dans son bénéfice taxable des bénéfices réalisés par la société BNP Private Bank and Trust Ltd, établie aux Bahamas ; qu'après avoir vainement réclamé auprès de l'administration, elle a saisi le tribunal administratif de Paris qui, par un jugement du 24 mars 2009, a rejeté sa demande ; que, sur l'appel de la société BNP Paribas, venant aux droits de la société BNP, la cour administrative d'appel de Paris a, par un arrêt du 31 mars 2011, annulé ce jugement et prononcé la décharge des impositions en litige ; que le ministre du budget, des comptes publics et de la réforme de l'État, porte-parole du Gouvernement, se pourvoit en cassation contre cet arrêt ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article 209 B du code général des impôts, dans sa rédaction applicable aux années d'imposition en litige : " I. Lorsqu'une entreprise passible de l'impôt sur les sociétés détient directement ou indirectement 25 % au moins des actions ou parts d'une société établie dans un État étranger ou un territoire situé hors de France dont le régime fiscal est privilégié au sens mentionné à l'article 238 A, cette entreprise est soumise à l'impôt sur les sociétés sur les résultats bénéficiaires de la société étrangère dans la proportion des droits sociaux qu'elle y détient. / Ces bénéfices font l'objet d'une imposition séparée. (...) / II. Les dispositions du I ne s'appliquent pas si l'entreprise établit que les opérations de la société étrangère n'ont pas principalement pour effet de permettre la localisation de bénéfices dans un Etat ou territoire où elle est soumise à un régime fiscal privilégié. Cette condition est réputée remplie notamment : / - lorsque la société étrangère a principalement une activité industrielle ou commerciale effective ; / et qu'elle réalise ses opérations de façon prépondérante sur le marché local " ; qu'aux termes de l'article 238 A du même code alors applicable : " (...) les personnes sont regardées comme soumises à un régime fiscal privilégié dans l'État ou le territoire considéré si elles n'y sont pas imposables ou si elles y sont assujetties à des impôts sur les bénéfices ou les revenus notablement moins élevés qu'en France " ;<br/>
<br/>
              3. Considérant qu'il résulte de ces dispositions, éclairées par leurs travaux préparatoires, que le législateur a entendu dissuader les entreprises passibles en France de l'impôt sur les sociétés de localiser, pour des raisons principalement fiscales, une partie de leurs bénéfices, au travers de filiales, créées par elles ou par une de leurs filiales dans des pays ou territoires à régime fiscal privilégié au sens de l'article 238 A du même code ; qu'à cette fin, le premier alinéa du I de l'article 209B du code général des impôts prévoit, dans sa rédaction applicable aux années d'imposition en litige, que dans le cas où une entreprise passible en France de l'impôt sur les sociétés détient, directement ou indirectement, au moins 25 % des actions ou parts d'une société implantée dans un Etat ou territoire à régime fiscal privilégié, elle est normalement soumise à l'impôt sur les sociétés sur les bénéfices de cette société, à proportion des droits qu'elle y détient ; qu'il n'en va autrement, de manière dérogatoire, que si l'entreprise démontre, ainsi que le prévoit le premier alinéa du II de l'article 209 B, que l'implantation de la filiale, détenue directement ou indirectement, dans un pays à régime fiscal privilégié n'a pas, pour la société mère, principalement pour objet d'échapper à l'impôt français ; que si une telle situation peut être présumée, c'est à la double condition, prévue aux deuxième et troisième alinéas du II de l'article 209 B, qu'il soit démontré, d'une part, que la société étrangère exerce à titre principal une activité industrielle ou commerciale effective, et d'autre part, que les opérations qu'elle réalise dans le cadre de cette activité sont effectuées de manière prépondérante sur le marché local, cette dernière notion devant, en tant qu'elle autorise une dérogation à la règle de droit commun, être interprétée strictement ;<br/>
<br/>
              4. Considérant, en premier lieu, qu'après avoir relevé que l'administration établissait que la société BNP Paribas entrait dans le champ d'application de l'article 209 B du code général des impôts à raison de sa détention de la totalité des parts de la société BNP Private Bank and Trust Ltd, la cour administrative d'appel de Paris, en jugeant qu'il résultait de l'instruction que le service ne pouvait pas faire usage des dispositions du I de cet article pour soumettre la société BNP Paribas aux impositions litigieuses à raison des bénéfices réalisés par la filiale bahamienne, n'a pas entaché son arrêt de contradiction de motifs ;<br/>
<br/>
              5. Considérant, en deuxième lieu, que la cour a relevé, par une appréciation souveraine des faits non arguée de dénaturation, que l'activité de banque privée internationale de la société BNP Private Bank and Trust Ltd consistait en la collecte de fonds de clients particuliers nord-américains intéressés par le placement de leurs avoirs aux Bahamas et que son implantation sur ce territoire avait permis à la société BNP Paribas d'acquérir une clientèle spécifique qui n'aurait pas réalisé ses placements en France, pour en déduire qu'ainsi la société requérante apportait la preuve qui lui incombait en vertu du premier alinéa du II de l'article 209 B du code général des impôts que l'implantation dans un pays à régime fiscal privilégié de la filiale qu'elle détenait indirectement, n'avait pas, pour elle, principalement pour objet d'échapper à l'impôt français ; que dès lors, le ministre ne peut utilement soutenir que la cour aurait ainsi méconnu la portée des deuxième et troisième alinéas du même II ; qu'il ne peut soutenir, par les seuls moyens qu'il invoque, que la cour aurait commis une erreur de droit au regard de l'article 209 B du code général des impôts ;<br/>
<br/>
              6. Considérant qu'il résulte de ce qui précède que le pourvoi du ministre du budget, des comptes publics, de la fonction publique et de la réforme de l'État, porte-parole du Gouvernement, doit être rejeté ;<br/>
<br/>
              7. Considérant qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par la société requérante au titre l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : Le pourvoi du ministre du budget, des comptes publics, de la fonction publique et de la réforme de l'État, porte-parole du Gouvernement est rejeté.<br/>
Article 2 : Le surplus des conclusions de la société BNP Paribas est rejeté.<br/>
Article 3 : La présente décision sera notifiée au ministre de l'économie et des finances et à la société BNP Paribas.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
