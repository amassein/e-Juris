<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042065826</ID>
<ANCIEN_ID>JG_L_2020_06_000000436335</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/06/58/CETATEXT000042065826.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème chambre, 29/06/2020, 436335, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-06-29</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>436335</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Yves Doutriaux</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Sophie Roussel</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2020:436335.20200629</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La Commission nationale des comptes de campagne et des financements politiques a, les 29 novembre 2019 et 17 janvier 2020, saisi le Conseil d'Etat en application de l'article L. 5215 du code électoral, sur le fondement de sa décision en date du 26 novembre 2019 par laquelle elle a constaté l'absence de dépôt d'un compte de campagne par M. B... A..., candidat tête de liste " Les Oubliés de l'Europe " à l'élection des représentants français au Parlement européen, qui a eu lieu le 26 mai 2019.<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu :<br/>
              - le code électoral ;<br/>
              - la loi n° 77-729 du 7 juillet 1977 ;<br/>
              - le code de justice administrative et l'ordonnance n° 2020-305 du 25 mars 2020 ; <br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Yves Doutriaux, conseiller d'Etat,<br/>
<br/>
              - les conclusions de Mme Sophie Roussel, rapporteur public,<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 52-12 du code électoral, rendu applicable comme les autres dispositions du titre Ier du livre Ier de ce code, à l'élection des représentants au Parlement européen par l'article 2 de la loi du 7 juillet 1977 relative à l'élection des représentants au Parlement européen : " Chaque candidat ou candidat tête de liste soumis au plafonnement prévu à l'article L. 52-11 et qui a obtenu au moins 1 % des suffrages exprimés est tenu d'établir un compte de campagne retraçant, selon leur origine, l'ensemble des recettes perçues et, selon leur nature, l'ensemble des dépenses engagées ou effectuées en vue de l'élection, hors celles de la campagne officielle par lui-même ou pour son compte, au cours de la période mentionnée à l'article L. 52-4. La même obligation incombe au candidat ou au candidat tête de liste dès lors qu'il a bénéficié de dons de personnes physiques conformément à l'article L. 52-8 du présent code selon les modalités prévues à l'article 200 du code général des impôts. (...) / Au plus tard avant 18 heures le dixième vendredi suivant le premier tour de scrutin, chaque candidat ou candidat tête de liste présent au premier tour dépose à la Commission nationale des comptes de campagne et des financements politiques son compte de campagne et ses annexes accompagné des justificatifs de ses recettes (...). Cette présentation n'est pas nécessaire lorsque aucune dépense ou recette ne figure au compte de campagne. Dans ce cas, le mandataire établit une attestation d'absence de dépense et de recette. Cette présentation n'est pas non plus nécessaire lorsque le candidat ou la liste dont il est tête de liste a obtenu moins de 1 % des suffrages exprimés et qu'il n'a pas bénéficié de dons de personnes physiques selon les modalités prévues à l'article 200 du code général des impôts. ".<br/>
<br/>
              2. Aux termes de l'article L. 52-15 du même code : " La Commission nationale des comptes de campagne et des financements politiques approuve et, après procédure contradictoire, rejette ou réforme les comptes de campagne. (...) / Hors le cas prévu à l'article L. 118-2, elle se prononce dans les six mois du dépôt des comptes. Passé ce délai, les comptes sont réputés approuvés. / Lorsque la commission a constaté que le compte de campagne n'a pas été déposé dans le délai prescrit, si le compte a été rejeté ou si, le cas échéant après réformation, il fait apparaître un dépassement du plafond des dépenses électorales, la commission saisit le juge de l'élection. (...) ".<br/>
<br/>
              3. Enfin, l'article L. 118-3 du même code dispose : " Saisi par la commission instituée par l'article L. 52-14, le juge de l'élection peut prononcer l'inéligibilité du candidat dont le compte de campagne, le cas échéant après réformation, fait apparaître un dépassement du plafond des dépenses électorales. (...) / Saisi dans les mêmes conditions, le juge de l'élection peut prononcer l'inéligibilité du candidat ou des membres du binôme de candidats qui n'a pas déposé son compte de campagne dans les conditions et le délai prescrits à l'article L. 52-12. (...) / L'inéligibilité (...) est prononcée pour une durée maximale de trois ans et s'applique à toutes les élections (...) ".<br/>
<br/>
              4. Il appartient au juge de l'élection, pour apprécier s'il y a lieu de faire usage de la faculté donnée par ces dispositions de déclarer inéligible un candidat qui n'a pas déposé son compte de campagne dans les conditions et délai prescrits à l'article L. 52-12 du code électoral, de tenir compte de la nature de la règle méconnue, du caractère délibéré ou non du manquement, de l'existence éventuelle d'autres motifs d'irrégularité du compte, du montant des sommes en cause ainsi que de l'ensemble des circonstances de l'espèce. <br/>
<br/>
              5. Il résulte de l'instruction que M. A..., tête de liste " Les Oubliés de l'Europe ", qui a obtenu moins de 1 % des suffrages, n'a pas déposé de compte de campagne. S'il fait valoir qu'il n'a perçu aucun don de personnes physiques et n'était dès lors pas soumis à l'obligation de dépôt de compte de campagne mentionnée à l'article L. 52-12 précité du code électoral, il résulte toutefois de l'instruction que, sur 100 reçus-dons qui lui ont été délivrés par la commission, M. A... n'en a restitué que 97. L'absence de restitution de carnets de reçus-dons délivrés par la commission étant de nature à faire présumer la perception de dons de personnes physiques, M. A... doit être regardé comme ayant méconnu l'exigence de dépôt du compte de campagne. Toutefois, dans les circonstances de l'espèce, il n'y a pas lieu, eu égard au faible nombre de reçus-dons manquants et aux montants en cause, de prononcer l'inéligibilité de M. A..., qui a produit devant le Conseil d'Etat des pièces de nature à établir que les manquements constatés ne procèdent ni d'une fraude ni d'une volonté de dissimulation.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : Il n'y a pas lieu de prononcer l'inéligibilité de M. A....<br/>
<br/>
Article 2 : La présente décision sera notifiée à la Commission nationale des comptes de campagne et des financements politiques et à M. B... A....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
