<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043648138</ID>
<ANCIEN_ID>JG_L_2021_04_000000448062</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/64/81/CETATEXT000043648138.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème chambre, 30/04/2021, 448062, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-04-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>448062</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR>Mme Dorothée Pradines</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Dieu</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:448062.20210430</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu les procédures suivantes :<br/>
<br/>
              La médecin-conseil, cheffe de service de l'échelon local du service médical de l'Oise, a porté plainte le 4 mai 2017 contre M. D... A... B... devant la section des assurances sociales de la chambre disciplinaire de première instance de Picardie de l'ordre des médecins. Par une décision du 23 octobre 2020, la section des assurances sociales du Conseil national de l'ordre des médecins a, après dessaisissement de la section des assurances sociales de la chambre disciplinaire de première instance de Picardie de l'ordre des médecins en application de l'article R. 145-19 du code de la sécurité sociale, infligé à M. A... B... la sanction de l'interdiction de donner des soins aux assurés sociaux pendant une durée d'un an, dont six mois assortis du sursis, avec publication pendant une période de six mois.<br/>
<br/>
              1° Sous le n° 448062, par un pourvoi, enregistré le 22 décembre 2020 au secrétariat du contentieux du Conseil d'État, M. A... B... demande au Conseil d'État :<br/>
<br/>
              1°) d'annuler cette décision ;<br/>
<br/>
              2°) de mettre solidairement à la charge de la caisse primaire d'assurance maladie de l'Oise et du service du contrôle médical de l'Oise la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              2° Sous le n° 448064, par une requête, enregistrée le 22 décembre 2020 au secrétariat du contentieux du Conseil d'État, M. A... B... demande au Conseil d'État :<br/>
<br/>
              1°) d'ordonner qu'il soit sursis à l'exécution de la même décision du 23 octobre 2020 de la section des assurances sociales du Conseil national de l'ordre des médecins ;<br/>
<br/>
              2°) de mettre à la charge solidaire de la caisse primaire d'assurance maladie de l'Oise et du service du contrôle médical de l'Oise la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu : <br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - le code de santé publique ; <br/>
              - le code de la sécurité sociale ;<br/>
              - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ; <br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Dorothée Pradines, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Frédéric Dieu, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Piwnica et Molinié, avocat de M. A... B... ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Le pourvoi par lequel M. A... B... demande l'annulation de la décision du 23 octobre 2020 de la section des assurances sociales du Conseil national de l'ordre des médecins et sa requête tendant à ce qu'il soit sursis à l'exécution de cette même décision présentent à juger les mêmes questions. Il y a lieu de les joindre pour y statuer par une seule décision.<br/>
<br/>
              2. Aux termes de l'article L. 822-1 du code de justice administrative : " Le pourvoi en cassation devant le Conseil d'État fait l'objet d'une procédure préalable d'admission. L'admission est refusée par décision juridictionnelle si le pourvoi est irrecevable ou n'est fondé sur aucun moyen sérieux ".<br/>
<br/>
              3. Pour demander l'annulation de la décision de la section des assurances sociales du Conseil national de l'ordre des médecins qu'il attaque, M. A... B... soutient qu'elle est entachée :<br/>
              - d'irrégularité en ce qu'elle a été prise à l'issue d'une procédure méconnaissant le principe d'impartialité garanti par l'article 6, paragraphe 1, de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - d'erreur de droit en ce qu'elle juge qu'il a méconnu les obligations de la nomenclature générale des actes professionnels (NGAP) relatives à l'envoi des bilans-diagnostics kinésithérapiques au médecin prescripteur ;<br/>
              - d'erreur de droit en ce qu'elle juge qu'il a méconnu les obligations de la NGAP imposant que le praticien se consacre exclusivement à son patient.<br/>
<br/>
              M. A... B... soutient enfin que cette décision prononce des sanctions hors de proportion avec les fautes qui lui sont reprochées.<br/>
<br/>
              4. Aucun de ces moyens n'est de nature à permettre l'admission du pourvoi.<br/>
<br/>
              5. Le pourvoi formé par M. A... B... contre la décision 23 octobre 2020 de la section des assurances sociales du Conseil national de l'ordre des médecins n'étant pas admis, les conclusions qu'il présente aux fins de sursis à exécution de cette décision sont devenues sans objet. Il n'y a donc pas lieu d'y statuer.<br/>
<br/>
              6. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'il soit fait droit aux conclusions présentées par M. A... B... à ce titre. Dans les circonstances de l'espèce, il y a lieu, au même titre, de mettre à la charge de M. A... B... le versement à la médecin-conseil, cheffe de service de l'échelon local du service médical de l'Oise, de la somme de 3 000 euros.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de M. A... B... n'est pas admis.<br/>
Article 2 : Il n'y a pas lieu de statuer sur la requête de M. A... B... à fin de sursis à exécution de la décision du 23 octobre 2020 de la section des assurances sociales du Conseil national de l'ordre des médecins.<br/>
Article 3 : Les conclusions présentées par M. A... B... au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : M. A... B... versera la somme de 3 000 euros à la médecin-conseil, cheffe de service de l'échelon local du service médical de l'Oise, au titre de l'article L. 761-1 du code de justice administrative. <br/>
Article 5 : La présente décision sera notifiée à M. D... A... B... et à la médecin-conseil, cheffe de service de l'échelon local du service médical de l'Oise.<br/>
Copie en sera adressée au Conseil national de l'ordre des médecins.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
