<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028349218</ID>
<ANCIEN_ID>JG_L_2013_12_000000366791</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/34/92/CETATEXT000028349218.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème et 5ème sous-sections réunies, 16/12/2013, 366791, Publié au recueil Lebon</TITRE>
<DATE_DEC>2013-12-16</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>366791</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème et 5ème sous-sections réunies</FORMATION>
<TYPE_REC>Autres</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GARREAU, BAUER-VIOLAS, FESCHOTTE-DESBOIS</AVOCATS>
<RAPPORTEUR>M. Benjamin de Maillard</RAPPORTEUR>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CESSR:2013:366791.20131216</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu le jugement n° 1102066 du 28 février 2013, enregistré le 14 mars 2013 au secrétariat du contentieux du Conseil d'Etat, par lequel le tribunal administratif de Lyon, avant de statuer sur la demande de M. B...C...et de Mme A...C...tendant d'une part, à l'annulation de la décision du 13 décembre 2010 de l'inspectrice d'académie, directrice des services départementaux de l'éducation nationale du Rhône, rejetant le recours gracieux présenté, à leur demande, par l'Association de défense des droits de l'homme, à l'encontre de la décision du 22 octobre 2010 leur opposant un avis défavorable à l'inscription réglementée de leur fille Saphya, au Centre national d'enseignement à distance (CNED), d'autre part, à ce qu'il soit enjoint à l'inspectrice d'académie de leur accorder l'inscription réglementée de leur fille à compter de la rentrée de septembre 2011, a décidé, par application des dispositions de l'article L. 113-1 du code de justice administrative, de transmettre le dossier de cette demande au Conseil d'Etat, en soumettant à son examen la question suivante :<br/>
<br/>
              dès lors que l'article R. 426-2-1 du code de l'éducation, issu du décret n° 2009-238 du 27 février 2009 relatif au service public de l'enseignement à distance, prévoit désormais que la décision d'inscription des élèves au CNED est prise par le directeur général de cet établissement public, et qu'elle est précédée, dans le cas des élèves relevant de l'instruction obligatoire, de l'avis de l'inspecteur d'académie, directeur des services départementaux de l'éducation nationale, l'avis défavorable émis, compte tenu des motifs de la demande d'inscription au CNED d'un tel élève, par l'inspecteur d'académie, peut-il être regardé, notamment eu égard aux modalités de constitution du dossier d'inscription fixées par l'arrêté du 27 juillet 2009, comme un refus de proposition faisant grief, voire comme une véritable décision négative, susceptibles comme tels de faire l'objet d'un recours pour excès de pouvoir, ou au contraire, comme un avis insusceptible de recours quand bien même le directeur du CNED, lié en tout état de cause par l'avis défavorable déjà notifié aux parents, ne serait-il pas saisi d'une demande d'inscription '<br/>
<br/>
              Vu les observations, enregistrées le 28 juin 2013, présentées pour M. et Mme C... ;<br/>
<br/>
              Vu les observations, enregistrées le 17 septembre 2013, présentées par le ministre de l'éducation nationale ; <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de l'éducation ;<br/>
<br/>
              Vu l'arrêté du ministre de l'éducation nationale du 27 juillet 2009 relatif au dossier d'inscription au Centre national d'enseignement à distance ;<br/>
<br/>
              Vu le code de justice administrative, notamment son article L. 113-1 ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
- le rapport de M. Benjamin de Maillard, Auditeur, <br/>
- les conclusions de Mme Gaëlle Dumortier, rapporteur public ;<br/>
              - La parole ayant été donnée, avant et après les conclusions, à la SCP Garreau, Bauer-Violas, Feschotte-Desbois, avocat de M. et Mme C...;<br/>
REND L'AVIS SUIVANT : <br/>
<br/>
<br/>
              1. En vertu des dispositions des articles L. 131-1 et L. 131-1-1 du code de l'éducation, l'instruction obligatoire est assurée prioritairement dans les établissements d'enseignement. Toutefois, l'article R. 426-2 du même code prévoit que le Centre national d'enseignement à distance, service public de l'enseignement à distance, dispense, pour le compte de l'Etat, un service d'enseignement à destination des élèves, notamment, selon le quatrième alinéa de cet article, ceux qui relèvent de l'instruction obligatoire, ayant vocation à être accueillis dans un établissement d'enseignement et qui ne peuvent y être scolarisés totalement ou partiellement. Aux termes de l'article R. 426-2-1, dans sa rédaction alors en vigueur : " La décision d'inscription des élèves mentionnés au quatrième alinéa de l'article R. 426-2 est prise par le directeur général du centre au vu d'un dossier défini par arrêté du ministre chargé de l'éducation nationale et, en ce qui concerne les élèves relevant de l'instruction obligatoire, sur avis favorable de l'inspecteur d'académie, directeur des services départementaux de l'éducation nationale du département de résidence de l'élève. (...) ". L'article 2 de l'arrêté du 27 juillet 2009 relatif au dossier d'inscription au Centre national d'enseignement à distance, pris en application de ces dispositions, dispose que : " Le dossier d'inscription des élèves relevant de l'instruction obligatoire à une formation complète ou à une unité d'enseignement doit comporter un avis favorable de l'inspecteur d'académie, directeur des services départementaux de l'éducation nationale du département de résidence de l'élève précisant les motifs de l'inscription (...) ". <br/>
<br/>
              2. La publication de cet arrêté au seul bulletin officiel du ministère de l'éducation nationale, ainsi que le prévoyait son article 6, n'a pu suffire à rendre ses dispositions opposables aux administrés. Toutefois, l'absence de publication au Journal officiel de la République française n'a pas fait obstacle à l'entrée en vigueur des dispositions du code de l'éducation relatives à ce service d'enseignement à distance, notamment à celles de l'article R. 426-2-1, dès lors que leur application n'est pas manifestement impossible en l'absence de publication de l'arrêté auquel cet article renvoie afin de préciser la composition du dossier de demande d'inscription. <br/>
<br/>
              3. Il résulte de ce qui précède que cette inscription est subordonnée à un avis favorable de l'inspecteur d'académie. Il ressort par ailleurs des pièces du dossier que les documents relatifs aux demandes d'inscription, établis par les services compétents et mis à la disposition des familles, prescrivent effectivement à celles-ci de joindre à leur demande cet avis favorable, ainsi que le prévoit l'arrêté du 27 juillet 2009. Il en résulte qu'un avis défavorable recueilli par les demandeurs rend impossible la constitution d'un dossier susceptible d'aboutir à une décision favorable, mettant ainsi un terme à la procédure, sauf pour les intéressés à présenter néanmoins au directeur général du Centre national d'enseignement à distance une demande nécessairement vouée au rejet, dans le seul but de faire naître une décision susceptible d'un recours à l'occasion duquel l'avis défavorable pourrait être contesté. Dans ces conditions, l'avis défavorable de l'inspecteur d'académie, désormais dénommé directeur académique des services de l'éducation nationale, doit être regardé comme faisant grief et comme étant, par suite, susceptible d'être déféré au juge de l'excès de pouvoir.<br/>
<br/>
<br/>
              Le présent avis sera notifié au tribunal administratif de Lyon, à M. B... C... et à Mme A...C...ainsi qu'au ministre de l'éducation nationale. Il sera publié au Journal officiel de la République française.<br/>
<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-08-01-02 ACTES LÉGISLATIFS ET ADMINISTRATIFS. APPLICATION DANS LE TEMPS. ENTRÉE EN VIGUEUR. ENTRÉE EN VIGUEUR SUBORDONNÉE À L'INTERVENTION DE MESURES D'APPLICATION. - ABSENCE - DISPOSITIONS DU CODE DE L'ÉDUCATION RELATIVES AUX INSCRIPTIONS AU CNED.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">30-02 ENSEIGNEMENT ET RECHERCHE. QUESTIONS PROPRES AUX DIFFÉRENTES CATÉGORIES D'ENSEIGNEMENT. - ENSEIGNEMENT À DISTANCE - 1) ENTRÉE EN VIGUEUR IMMÉDIATE DES DISPOSITIONS DU CODE DE L'ÉDUCATION RELATIVES AU SERVICE D'ENSEIGNEMENT À DISTANCE DU CNED - EXISTENCE, MALGRÉ L'ABSENCE D'OPPOSABILITÉ AUX ADMINISTRÉS DE L'ARRÊTÉ FIXANT LE DOSSIER D'INSCRIPTION - 2) AVIS DU DIRECTEUR DES SERVICES ACADÉMIQUES REQUIS POUR L'INSCRIPTION D'UN ÉLÈVE RELEVANT DE L'INSTRUCTION OBLIGATOIRE - AVIS DÉFAVORABLE - ACTE SUSCEPTIBLE DE RECOURS - EXISTENCE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">54-01-01-01 PROCÉDURE. INTRODUCTION DE L'INSTANCE. DÉCISIONS POUVANT OU NON FAIRE L'OBJET D'UN RECOURS. ACTES CONSTITUANT DES DÉCISIONS SUSCEPTIBLES DE RECOURS. - AVIS DÉFAVORABLE DU DIRECTEUR ACADÉMIQUE DES SERVICES DE L'ÉDUCATION NATIONALE À L'INSCRIPTION D'UN ÉLÈVE AU SERVICE D'ENSEIGNEMENT À DISTANCE DU CNED.
</SCT>
<ANA ID="9A"> 01-08-01-02 L'application des dispositions du code de l'éducation relatives au service d'enseignement à distance du Centre national d'enseignement à distance (CNED) n'est pas manifestement impossible en l'absence de l'arrêté du ministre chargé de l'éducation nationale, prévu à l'article R. 426-2-1 de ce code, fixant le dossier de demande d'inscription. Par suite, si la publication de cet arrêté au seul bulletin officiel du ministère de l'éducation nationale n'a pu suffire à le rendre opposable aux administrés, cette circonstance n'a pas fait obstacle à l'entrée en vigueur de ces dispositions.</ANA>
<ANA ID="9B"> 30-02 1) L'application des dispositions du code de l'éducation relatives au service d'enseignement à distance du Centre national d'enseignement à distance (CNED) n'est pas manifestement impossible en l'absence de l'arrêté du ministre chargé de l'éducation nationale, prévu à l'article R. 426-2-1 de ce code, fixant le dossier de demande d'inscription. Par suite, si la publication de cet arrêté au seul bulletin officiel du ministère de l'éducation nationale n'a pu suffire à le rendre opposable aux administrés, cette circonstance n'a pas fait obstacle à l'entrée en vigueur de ces dispositions.,,,2) Dès lors qu'il résulte de l'article R. 426-2-1 que l'inscription au CNED d'un élève relevant de l'instruction obligatoire est subordonnée à un avis favorable de l'inspecteur d'académie, désormais dénommé directeur académique des services de l'éducation nationale, et que les familles doivent joindre cet avis favorable à leur demande d'instruction, un avis défavorable recueilli par les demandeurs rend impossible la constitution d'un dossier susceptible d'aboutir à une décision favorable, mettant ainsi un terme à la procédure, sauf pour les intéressés à présenter néanmoins au directeur général du CNED une demande vouée au rejet. Dans ces conditions, l'avis défavorable du directeur académique doit être regardé comme faisant grief et comme étant, par suite, susceptible d'être déféré au juge de l'excès de pouvoir.</ANA>
<ANA ID="9C"> 54-01-01-01 Dès lors qu'il résulte de l'article R. 426-2-1 du code de l'éducation que l'inscription au CNED d'un élève relevant de l'instruction obligatoire est subordonnée à un avis favorable de l'inspecteur d'académie, désormais dénommé directeur académique des services de l'éducation nationale, et que les familles doivent joindre cet avis favorable à leur demande d'instruction, un avis défavorable recueilli par les demandeurs rend impossible la constitution d'un dossier susceptible d'aboutir à une décision favorable, mettant ainsi un terme à la procédure, sauf pour les intéressés à présenter néanmoins au directeur général du CNED une demande vouée au rejet. Dans ces conditions, l'avis défavorable du directeur académique doit être regardé comme faisant grief et comme étant, par suite, susceptible d'être déféré au juge de l'excès de pouvoir.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
