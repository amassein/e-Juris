<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027031729</ID>
<ANCIEN_ID>JG_L_2013_02_000000349169</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/03/17/CETATEXT000027031729.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème et 8ème sous-sections réunies, 04/02/2013, 349169, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-02-04</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>349169</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème et 8ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GHESTIN</AVOCATS>
<RAPPORTEUR>M. Romain Victor</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Vincent Daumas</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:349169.20130204</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 11 mai et 29 juillet 2011 au secrétariat du contentieux du Conseil d'Etat, présentés pour la société par actions simplifiée Sodigar 2, dont le siège est Fraixinet, Centre commercial Roques à Roques-sur-Garonne (31120), représentée par son président ; la société demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 10BX00136 du 10 mars 2011 par lequel la cour administrative d'appel de Bordeaux a rejeté l'appel qu'elle a interjeté du jugement n° 0501664 du 17 novembre 2009 par lequel le tribunal administratif de Toulouse a rejeté ses demandes tendant, à titre principal, à la décharge des suppléments d'impôt sur les sociétés et de contributions additionnelles à cet impôt ainsi que des pénalités correspondantes auxquels elle a été assujettie au titre de l'exercice clos en 2001 et, à titre subsidiaire, à la réduction de son résultat imposable à l'impôt sur les sociétés à hauteur de la somme de 517 340 euros ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code général des impôts et le livre des procédures fiscales ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Romain Victor, Maître des Requêtes en service extraordinaire, <br/>
<br/>
              - les observations de la SCP Ghestin, avocat de la société Sodigar 2,<br/>
<br/>
              - les conclusions de M. Vincent Daumas, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Ghestin, avocat de la société Sodigar 2 ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que la société Sodigar 2 a exploité en location-gérance, jusqu'au 31 janvier 2000, un fonds de commerce à usage d'hypermarché situé à Roques-sur-Garonne (Haute-Garonne) ; qu'à cette date, elle a transféré l'exploitation du fonds à sa filiale, la société Sodigar ; qu'étant restée titulaire du contrat de crédit-bail immobilier des locaux abritant le centre commercial, elle a poursuivi, à compter du 1er février 2000, une activité de sous-location d'immeuble et de location de matériels ; qu'à l'issue de la vérification de comptabilité dont elle a fait l'objet au titre de la période du 1er février 2000 au 31 janvier 2002, la société Sodigar 2 s'est vu notifier un redressement au titre de l'exercice clos le 31 janvier 2001, correspondant à la remise en cause de l'imputation sur le bénéfice de l'exercice d'un déficit né à la clôture de l'exercice clos le 31 janvier 1996 ; que la société Sodigar 2 se pourvoit en cassation contre l'arrêt du 10 mars 2011 par lequel la cour administrative d'appel de Bordeaux a rejeté l'appel qu'elle a interjeté du jugement du 17 novembre 2009 par lequel le tribunal administratif de Toulouse a rejeté sa demande tendant à la décharge des suppléments d'impôt sur les sociétés et de contribution additionnelle à cet impôt qui ont été mis à sa charge en conséquence de ce redressement ;<br/>
<br/>
              2. Considérant, en premier lieu, que contrairement à ce que soutient la société requérante, la cour administrative d'appel de Bordeaux a répondu au moyen tiré de ce qu'elle avait été créée, dès l'origine, pour exercer une activité de holding et de ce qu'elle n'a exploité un hypermarché en location-gérance qu'à titre temporaire, afin de financer l'opération de rachat des titres de sa filiale ; que le moyen tiré par la société Sodigar 2 d'un défaut de réponse de la cour à ce moyen manque donc en fait ;<br/>
<br/>
              3. Considérant, en deuxième lieu, qu'aux termes du I de l'article 209 du code général des impôts relatif à la détermination de la base de l'impôt sur les sociétés, dans sa rédaction applicable aux années d'imposition en litige : " (...) en cas de déficit subi pendant un exercice, ce déficit est considéré comme une charge de l'exercice suivant et déduit du bénéfice réalisé pendant ledit exercice. Si ce bénéfice n'est pas suffisant pour que la déduction puisse être intégralement opérée, l'excédent du déficit est reporté successivement sur les exercices suivants jusqu'au cinquième exercice qui suit l'exercice déficitaire (...) " ; qu'aux termes de l'article 221 du même code : " (...) 5. Le changement de l'objet social ou de l'activité réelle d'une société emporte cessation d'entreprise (...) " ; qu'il résulte de la combinaison de ces dispositions que la mise en oeuvre du droit au report déficitaire est subordonnée notamment à la condition qu'une société n'ait pas subi, dans son activité, des transformations telles qu'elle n'est plus, en réalité, la même ; que de telles transformations dans l'activité d'une société, qui doivent être regardées comme emportant cessation d'entreprise, font obstacle à ce que celle-ci puisse reporter un déficit antérieur à son changement d'activité sur le bénéfice d'un exercice postérieur à ce changement, fût-ce à hauteur seulement des profits comptabilisés au cours de cet exercice mais provenant de l'ancienne activité ; que, dès lors, en jugeant, après avoir constaté, par une appréciation souveraine qui n'est pas arguée de dénaturation, que la société Sodigar 2 avait subi, à compter du 1er février 2000, un changement profond dans son activité devant être regardé comme emportant cessation d'entreprise, que cette société ne pouvait pas procéder, au titre de l'exercice clos le 31 janvier 2001, au report du déficit provenant de son ancienne activité, et ceci, même à concurrence des profits comptabilisés au titre de cet exercice provenant de cette activité, la cour n'a commis aucune erreur de droit ;<br/>
<br/>
              4. Considérant, en troisième lieu, qu'en jugeant que la société requérante ne pouvait, sur le fondement des dispositions de l'article L. 80 A du livre des procédures fiscales, tirer des énonciations de la documentation de base 4-H-2211 selon lesquelles " les déficits éventuellement subis jusqu'à la date du changement ne peuvent pas être reportés sur les bénéfices réalisés ultérieurement dans le cadre de l'activité nouvelle ", la conclusion qu'il était possible de reporter les déficits antérieurs sur des profits comptabilisés après le changement d'activité mais provenant de l'ancienne activité, la cour n'a pas commis d'erreur de droit ;<br/>
<br/>
              5. Considérant qu'il résulte de ce qui précède que la société Sodigar 2 n'est pas fondée à demander l'annulation de l'arrêt qu'elle attaque ; que par suite, les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de la société Sodigar 2 est rejeté.<br/>
Article 2 : La présente décision sera notifiée à la société Sodigar 2 et au ministre de l'économie et des finances.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
