<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032409018</ID>
<ANCIEN_ID>JG_L_2016_04_000000390759</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/40/90/CETATEXT000032409018.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème / 7ème SSR, 15/04/2016, 390759</TITRE>
<DATE_DEC>2016-04-15</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>390759</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème / 7ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR>M. Clément Malverti</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Xavier Domino</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2016:390759.20160415</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu les procédures suivantes :<br/>
<br/>
              1° Sous le n° 390759, par une requête, enregistrée le 5 juin 2015 au secrétariat du contentieux du Conseil d'Etat, la société Guadeloupe Téléphone Mobile demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler la décision n° 2015-0592-FR du 19 mai 2015 par laquelle la formation restreinte de l'Autorité de régulation des communications électroniques et des postes lui a retiré l'ensemble des droits d'utilisation des fréquences qui lui avaient été attribués en 2008 en vue de l'exploitation d'un réseau mobile de deuxième et de troisième génération dans le département de la Guadeloupe ainsi que la décision de publier cette sanction sur la page d'accueil du site Internet de l'Autorité ;<br/>
<br/>
              2°) de mettre à la charge de l'Autorité la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              2° Sous le n° 390761, par une requête, enregistrée le 5 juin 2015 au secrétariat du contentieux du Conseil d'Etat, la société Guyane Téléphone Mobile demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler la décision n° 2015-0594-FR du 19 mai 2015 par laquelle la formation restreinte de l'Autorité de régulation des communications électroniques et des postes lui a retiré l'ensemble des droits d'utilisation des fréquences qui lui avaient été attribués en 2008 en vue de l'exploitation d'un réseau mobile de deuxième et de troisième génération dans le département de la Guyane ainsi que la décision de publier cette sanction sur la page d'accueil du site Internet de l'Autorité ;<br/>
<br/>
              2°) de mettre à la charge de l'Autorité la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
<br/>
<br/>
              3° Sous le n° 390762, par une requête, enregistrée le 5 juin 2015 au secrétariat du contentieux du Conseil d'Etat, la société Martinique Téléphone Mobile demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler la décision n° 2015-0593-FR du 19 mai 2015 par laquelle la formation restreinte de l'Autorité de régulation des communications électroniques et des postes lui a retiré l'ensemble des droits d'utilisation des fréquences qui lui avaient été attribués en 2008 en vue de l'exploitation d'un réseau mobile de deuxième et de troisième génération dans le département de la Martinique ainsi que la décision de publier cette sanction sur la page d'accueil du site Internet de l'Autorité ;<br/>
<br/>
              2°) de mettre à la charge de l'Autorité la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu : <br/>
              - le code des postes et des communications électroniques ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Clément Malverti, auditeur,<br/>
<br/>
              - les conclusions de M. Xavier Domino, rapporteur public,<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Piwnica, Molinié, avocat de la société Guadeloupe Téléphone Mobile, de la société Guyane Téléphone Mobile et de la société Martinique Téléphone Mobile ; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1.	Considérant que les requêtes visées ci-dessus présentent à juger les mêmes questions ; qu'il y a lieu de les joindre pour statuer par une seule décision ;<br/>
<br/>
              Sur la légalité externe des décisions attaquées :<br/>
<br/>
              2.	Considérant, en premier lieu, qu'aux termes du troisième alinéa de l'article D. 598 du code des postes et des communications électroniques : " La décision de la formation restreinte est signée par le président et mentionne les noms des membres qui ont siégé. Elle est motivée, notifiée et comporte les voies et délais de recours " ; que les exigences de notification et de mention des voies et délais de recours prévues par ces dispositions ont pour objet d'informer la personne concernée par la décision de l'intervention de celle-ci et des modalités d'exercice de son droit au recours juridictionnel ; qu'elles sont ainsi relatives à des formalités postérieures à la décision, dont l'exécution est sans influence sur la légalité de celle-ci ; qu'en tout état de cause, en l'espèce, les exigences qu'elles édictent n'ont pas été méconnues dans la mesure où les courriers de notification des décisions attaquées indiquaient expressément aux sociétés requérantes les voies et délais de recours ;<br/>
<br/>
              3.	Considérant, en second lieu, que si la décision par laquelle la formation restreinte de l'Autorité de régulation des communications électroniques et des postes rend publique la sanction prononcée a le caractère d'une sanction complémentaire, elle n'a pas à faire l'objet d'une motivation spécifique, distincte de la motivation d'ensemble ; qu'en l'espèce, la motivation d'ensemble ne saurait être regardée comme insuffisante ;<br/>
<br/>
              Sur la légalité interne des décisions attaquées :<br/>
<br/>
              4.	Considérant qu'il résulte de l'instruction que des autorisations d'utilisation des fréquences en vue de l'exploitation d'un réseau mobile de deuxième et troisième génération ont été délivrées en 2008 aux sociétés requérantes ; que par trois décisions du 7 octobre 2014, l'Autorité de régulation des communications électroniques et des postes a mis en demeure les sociétés requérantes de respecter, selon un calendrier fixant trois échéances, les obligations en matière de couverture de la population et de fourniture de services mobiles figurant aux cahiers des charges annexés aux autorisations d'utilisation des fréquences attribuées en 2008 ; que par trois décisions du 19 mai 2015, l'Autorité, estimant les manquements établis à l'issue du contrôle de la première échéance de la mise en demeure, a décidé de retirer aux sociétés requérantes les autorisations d'utilisation des fréquences et de rendre publiques ces décisions sur son site internet ; <br/>
<br/>
              5.	Considérant, en premier lieu, qu'aux termes du I de l'article L. 36-11 du code des postes et des télécommunications électroniques : " En cas de manquement par un exploitant de réseau ou un fournisseur de services de communications électroniques (...) aux prescriptions d'une décision d'attribution ou d'assignation de fréquence prise par l'Autorité en application de l'article 26 de la loi n° 86-1067 du 30 septembre 1986 relative à la liberté de communication, l'exploitant ou le fournisseur est mis en demeure par l'Autorité de régulation des communications électroniques et des postes de s'y conformer dans un délai qu'elle détermine. / La mise en demeure peut être assortie d'obligations de se conformer à des étapes intermédiaires dans le même délai (...) " ; que le II de cet article dispose que : " Lorsqu'un exploitant de réseau ou un fournisseur de services de communications électroniques ne se conforme pas dans les délais fixés à la mise en demeure prévue au I ou aux obligations intermédiaires dont elle est assortie, l'Autorité de régulation des communications électroniques et des postes peut, après instruction conduite par ses services, notifier les griefs à la personne en cause. Elle transmet alors le dossier d'instruction et la notification des griefs à la formation restreinte " ; qu'aux termes du troisième alinéa du III du même article : " La formation restreinte peut prononcer à l'encontre de l'exploitant de réseau ou du fournisseur de services en cause une des sanctions suivantes :/ (...) - la suspension totale ou partielle, pour un mois au plus, la réduction de la durée, dans la limite d'une année, ou le retrait de la décision d'attribution ou d'assignation prise en application des articles L. 42-1 ou L. 44. La formation restreinte peut notamment retirer les droits d'utilisation sur une partie de la zone géographique sur laquelle porte la décision, une partie des fréquences ou bandes de fréquences préfixes, numéros ou blocs de numéros attribués ou assignés, ou une partie de la durée restant à courir de la décision (...) " ;<br/>
<br/>
              6.	Considérant que ces dispositions n'ont pas pour effet d'interdire à la formation restreinte de l'Autorité de régulation des communications électroniques et des postes de sanctionner un manquement à une obligation intermédiaire fixée dans une mise en demeure, et notamment, compte tenu des circonstances de l'espèce et de la gravité du manquement, de prononcer sans attendre l'expiration de la dernière échéance prévue dans la mise en demeure initiale, la sanction de retrait des droits d'utilisation de fréquences ; que, par suite, les sociétés requérantes ne sont pas fondées à soutenir que l'Autorité aurait fait une inexacte application de ces dispositions en prononçant à leur encontre une sanction qui a eu pour effet de les priver de la possibilité de se mettre en conformité avec la mise en demeure dont elles avaient fait l'objet ;<br/>
<br/>
              7.	Considérant, en second lieu, que les sociétés requérantes avaient, dès la date de délivrance des autorisations d'utilisation de fréquences, connaissance de l'étendue de leurs obligations de déploiement des réseaux mobile de deuxième et troisième générations ; que les sociétés requérantes n'apportent aucun élément permettant d'établir que, malgré l'ancienneté des autorisations et la mise en demeure dont elles ont été l'objet en octobre 2014, elles auraient commencé à déployer des équipements nécessaires à l'exploitation des réseaux mobiles de deuxième et troisième générations ; qu'il résulte de l'instruction que les fréquences disponibles dans les bandes 900 MHz, 1800 MHz et 2,1 GHz se raréfient dans les départements de la Guadeloupe, de la Guyane et de la Martinique ; qu'ainsi, compte tenue de la demande accrue d'attributions de telles fréquences, de la nécessité d'améliorer le déploiement de réseaux mobiles dans les départements en cause et de l'intérêt général qui s'attache à la gestion efficace du spectre radioélectrique, les sociétés ne sont pas fondées à soutenir que les sanctions de retrait des autorisations d'utilisation des fréquences prononcées à leur encontre revêtent un caractère disproportionné ;<br/>
<br/>
              8.	Considérant que les décisions de publication de ces sanctions ne présentent pas, en l'espèce, un caractère disproportionné ;<br/>
<br/>
              9.	Considérant qu'il résulte de tout ce qui précède que les conclusions dirigées contre les sanctions ne peuvent qu'être rejetées ;<br/>
<br/>
              Sur les conclusions tendant à l'application des dispositions de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              10.	Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Autorité de régulation des communications électroniques et des postes, qui n'est pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : Les requêtes de la société Guadeloupe Téléphone Mobile, de la société Guyane Téléphone Mobile et de la société Martinique Téléphone Mobile sont rejetées.<br/>
<br/>
Article 2 : La présente décision sera notifiée à la société Guadeloupe Téléphone Mobile, à la société Guyane Téléphone Mobile, à la société Martinique Téléphone Mobile et à l'Autorité de régulation des communications électroniques et des postes.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">51-005 Postes et communications électroniques.
</SCT>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
