<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030491208</ID>
<ANCIEN_ID>JG_L_2015_04_000000359304</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/49/12/CETATEXT000030491208.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème SSJS, 16/04/2015, 359304, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-04-16</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>359304</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème SSJS</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP CAPRON</AVOCATS>
<RAPPORTEUR>Mme Maïlys Lange</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Marie-Astrid Nicolazo de Barmon</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2015:359304.20150416</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Me C...B..., agissant en qualité de liquidateur de la SARL LP Diffusion, et M. et Mme A...D...ont demandé au tribunal administratif de Nice la condamnation de l'Etat à les indemniser du préjudice subi en raison de l'adoption illégale, par le ministre de l'économie, des finances et de l'industrie, de l'arrêté du 20 août 2001 portant suspension de la mise sur le marché d'une embarcation nautique et ordonnant son retrait. Par un jugement n° 0501586 du 27 janvier 2009, le tribunal administratif de Nice a rejeté leur demande.<br/>
<br/>
              Par un arrêt n° 09MA01122 du 13 mars 2012, la cour administrative d'appel de Marseille a rejeté l'appel formé par Me B...et M. et Mme D...contre ce jugement. <br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 11 mai et 8 août 2012 et le 23 janvier 2014 au secrétariat du contentieux du Conseil d'Etat, Me B...et M. et Mme D...demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à leur appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de la consommation ; <br/>
              - le code de justice administrative.<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Maïlys Lange, maître des requêtes,<br/>
<br/>
              - les conclusions de Mme Marie-Astrid Nicolazo de Barmon, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Capron, avocat de Me C...B...et autres ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que la SARL LP Diffusion, placée en liquidation judiciaire le 25 novembre 2002, exerçait une activité d'importation et de commercialisation d'une embarcation nautique de la marque " Surfbike ". A la suite d'un accident mortel survenu le 30 juillet 2001, l'importation et la commercialisation de cette embarcation nautique ont été suspendues pour une durée d'un an par arrêté du 20 août 2001, pris sur le fondement des articles L. 221-5 et L. 221-9 du code de la consommation. L'exécution de cet arrêté, en tant qu'il s'appliquait à toutes les embarcations de la marque " Surfbike " et non pas aux seules embarcations dont le guidon a une forme semi-circulaire, a été suspendue par une ordonnance du juge des référés du Conseil d'Etat du 30 novembre 2001. L'arrêté du 20 août 2001 a alors été abrogé par un arrêté du 28 décembre 2001, qui a également prévu la suspension pour une durée d'un an de la mise sur le marché des seules embarcations nautiques de la marque " Surfbike " munies d'un guidon semi-circulaire. Ce dernier arrêté a fait l'objet d'un recours pour excès de pouvoir, qui a été rejeté par une décision du 2 juillet 2003 du Conseil d'Etat statuant au contentieux. MeB..., agissant en qualité de liquidateur de la SARL LP Diffusion, et M. et MmeD..., actionnaires de cette société, se pourvoient en cassation contre l'arrêt du 13 mars 2012 de la cour administrative d'appel de Marseille ayant rejeté leur appel dirigé contre le jugement du 27 janvier 2009 du tribunal administratif de Nice rejetant leur demande de réparation du préjudice qu'ils estiment avoir subi en raison de l'illégalité qui aurait entaché, selon eux, l'arrêté du 20 août 2001.<br/>
<br/>
              2. En premier lieu, pour rejeter la demande des requérants tendant à l'indemnisation d'un préjudice de 96 839 euros lié, selon ces derniers, à des annulations de commandes et à un refus de paiement consécutifs à l'arrêté du 20 août 2001, la cour administrative d'appel de Marseille a jugé que les documents versés aux débats ne permettaient pas de savoir si les annulations et le refus en question concernaient des " Surfbikes " à guidon droit ou des " Surfbikes " à guidon semi-circulaire, ni par suite d'établir si le préjudice allégué était susceptible d'être la conséquence de l'illégalité éventuelle de cet arrêté. En statuant ainsi la cour, qui a suffisamment motivé son arrêt, n'a pas commis d'erreur de droit, dès lors que n'étaient pas contestés en appel les motifs du jugement du tribunal administratif de Nice selon lesquels seule la suspension de la commercialisation des " Surfbikes " à guidon droit était de nature à entraîner la responsabilité de l'Etat et, qu'ainsi, seules les conséquences de cette dernière suspension pouvaient, le cas échéant, se traduire par un préjudice indemnisable.<br/>
<br/>
              3. En deuxième lieu, les frais de justice, s'ils ont été exposés en conséquence directe d'une faute de l'administration, sont susceptibles d'être pris en compte dans le préjudice résultant de l'illégalité fautive imputable à l'administration. Toutefois, lorsque l'intéressé a fait valoir devant le juge une demande fondée sur l'article L. 761-1 du code de justice administrative, le préjudice est intégralement réparé par la décision que prend le juge sur ce fondement. A supposer même que l'arrêté du 20 août 2001 ait été illégal, la cour administrative d'appel n'a donc pas commis d'erreur de droit ni méconnu le principe de la réparation intégrale du préjudice en jugeant, pour écarter la demande d'indemnisation des frais d'avocat engagés par la SARL LP Diffusion à l'occasion de ses recours dirigés contre cet arrêté, que ces frais avaient été pris en compte par le Conseil d'Etat, tant dans le cadre de la procédure de référé qu'au fond, au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              4. En troisième lieu, la cour n'a pas dénaturé les faits qui lui étaient soumis en jugeant que la dépense de 311 euros engagée par la SARL LP Diffusion pour notifier à ses clients, par courrier en recommandé, l'arrêté du 20 août 2001, résultait de sa propre initiative et que cet arrêté n'imposait pas ces modalités d'information des clients de la société.<br/>
<br/>
              5. En quatrième lieu, c'est par une appréciation souveraine, qui n'est pas entachée de dénaturation, que la cour administrative d'appel a jugé que la SARL LP Diffusion n'établissait pas que sa mise en liquidation judiciaire, et les pertes qui l'accompagnaient au titre du fonds de commerce, du stock et de l'interruption de toute activité commerciale, découlaient de l'illégalité qui aurait, selon elle, entaché l'arrêté du 20 août 2001. La cour, en mentionnant d'une part " la brièveté de la période pendant laquelle l'illégalité supposée a pu produire des effets ", d'autre part, " la circonstance que ladite liquidation judiciaire soit intervenue près d'un an après le prononcé de la suspension de l'arrêté du 20 août 2001 ", ne s'est pas, en tout état de cause, contrairement à ce qui est soutenu par les requérants, fondée sur des motifs inopérants pour statuer comme elle l'a fait.<br/>
<br/>
              6. En dernier lieu, dès lors que la cour administrative d'appel de Marseille n'a reconnu ni l'existence d'une illégalité fautive de nature à engager la responsabilité de l'Etat, ni l'engagement de la responsabilité sans faute de l'Etat pour rupture d'égalité devant les charges publiques ni même l'existence du préjudice allégué par la SARL LP Diffusion par sa non-participation au salon nautique de Paris, elle n'a pas méconnu son office en jugeant que les pièces du dossier ne permettaient pas de chiffrer ce préjudice, en ne le chiffrant pas elle-même et, en tout état de cause, en ne faisant pas usage, pour le faire, de ses pouvoirs d'instruction. <br/>
<br/>
              7. Il résulte de tout ce qui précède, sans qu'il soit besoin de statuer sur la fin de non-recevoir opposée par le ministre de l'économie et des finances, que le pourvoi de Me B...et de M. et Mme D...doit, être rejeté, y compris leurs conclusions tendant à l'application des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
Article 1er : Le pourvoi de Me B...et de M. et Mme D...est rejeté.<br/>
Article 2 : La présente décision sera notifiée à Me C...B..., en sa qualité de liquidateur de la SARL L.P. Diffusion, à M. et Mme A...D...et au ministre de l'économie, du redressement productif et du numérique.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
