<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036673246</ID>
<ANCIEN_ID>JG_L_2018_03_000000397881</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/67/32/CETATEXT000036673246.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème - 3ème chambres réunies, 05/03/2018, 397881, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-03-05</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>397881</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème - 3ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Karin Ciavaldini</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Romain Victor</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:397881.20180305</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une décision du 25 janvier 2017, le Conseil d'Etat, statuant au contentieux sur la requête de M. A...tendant à l'annulation, pour excès de pouvoir, des décisions contenues dans les communiqués de presse du 20 octobre 2015 n° 486 et n° 487, respectivement du secrétaire d'Etat auprès du ministre des finances et des comptes publics, chargé du budget et de la direction générale des finances publiques, en tant qu'ils excluent du champ de la restitution des prélèvements sociaux qu'ils mentionnent, d'une part, les ressortissants fiscaux des pays tiers à l'Espace économique européen et, d'autre part, le prélèvement social de 2 %, a rejeté les conclusions de la requête tendant à l'annulation des communiqués de presse en tant qu'ils excluent du champ de la restitution " le prélèvement social de 2 % ", ainsi que les conclusions aux fins d'injonction correspondantes, et sursis à statuer sur le surplus de la requête jusqu'à ce que la Cour de justice de l'Union européenne se soit prononcée sur les questions de savoir si les articles 63, 64 et 65 du traité sur le fonctionnement de l'Union européenne doivent être interprétés en ce sens que :<br/>
<br/>
              1°) La circonstance qu'une personne affiliée à un régime de sécurité sociale d'un Etat tiers à l'Union européenne, autre que les Etats membres de l'Espace économique européen ou la Suisse soit soumise, de même que les personnes affiliées à la sécurité sociale en France, aux prélèvements sur les revenus du capital prévus par la législation française entrant dans le champ du règlement du 29 avril 2004, alors qu'une personne relevant d'un régime de sécurité sociale d'un Etat membre autre que la France ne peut, compte tenu des dispositions de ce règlement, y être soumise, constitue une restriction aux mouvements de capitaux en provenance ou à destination des pays tiers en principe interdite par l'article 63 du traité sur le fonctionnement de l'Union européenne ;<br/>
<br/>
              2°) En cas de réponse positive à la première question, une telle restriction aux mouvements de capitaux, qui découle de la combinaison d'une législation française, qui soumet aux prélèvements en litige l'ensemble des titulaires de certains revenus du capital sans opérer par elle-même aucune distinction selon le lieu de leur affiliation à un régime de sécurité sociale, et d'un acte de droit dérivé de l'Union européenne, peut être regardée comme compatible avec les stipulations dudit article du traité sur le fonctionnement de l'Union européenne, notamment :<br/>
              - au regard du 1 de l'article 64 du traité, pour les mouvements de capitaux qui entrent dans son champ, au motif que la restriction découlerait de l'application du principe d'unicité de législation prévu à l'article 11 du règlement du 29 avril 2004, introduit dans le droit de l'Union par l'article 13 du règlement du 14 juin 1971, soit à une date antérieure au 31 décembre 1993, alors même que les prélèvements sur les revenus du capital en cause ont été institués ou rendus applicables après le 31 décembre 1993 ;<br/>
              - au regard du 1 de l'article 65 du traité, au motif que la législation fiscale française, appliquée de manière conforme au règlement du 29 avril 2004, établirait une distinction entre des contribuables ne se trouvant pas dans la même situation au regard du critère tiré de l'affiliation à un régime de sécurité sociale ;<br/>
              - au regard de l'existence de raisons impérieuses d'intérêt général susceptibles de justifier une restriction à la libre circulation des capitaux, tirées de ce que les dispositions qui seraient regardées comme constitutives d'une restriction aux mouvements de capitaux en provenance ou à destination des pays tiers répondent à l'objectif, poursuivi par le règlement du 29 avril 2004, de libre circulation des travailleurs au sein de l'Union européenne.<br/>
<br/>
              Par un arrêt n° C-45/17 du 18 janvier 2018, la Cour de justice de l'Union européenne s'est prononcée sur ces questions.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le traité sur le fonctionnement de l'Union européenne ;<br/>
              - le règlement (CEE) n° 1408/71 du Conseil du 14 juin 1971 ;<br/>
              - le règlement (CE) n° 883/2004 du Parlement européen et du Conseil du 29 avril 2004 ;<br/>
              - le code général des impôts ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Karin Ciavaldini, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Romain Victor, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Dans l'arrêt du 18 janvier 2018 par lequel elle s'est prononcée sur les questions dont le Conseil d'Etat, statuant au contentieux, l'avait saisie à titre préjudiciel, après avoir écarté les autres moyens de la requête dirigés contre les communiqués de presse attaqués en tant qu'ils excluent du champ du remboursement des prélèvements sur les revenus du patrimoine qu'ils mentionnent les redevables affiliés à la sécurité sociale dans un Etat autre que la Suisse ou que ceux qui font partie de l'Union européenne ou de l'Espace économique européen, la Cour de justice de l'Union européenne a dit pour droit que " les articles 63 et 65 du traité sur le fonctionnement de l'Union européenne doivent être interprétés en ce sens qu'ils ne s'opposent pas à la législation d'un Etat membre, telle que la législation française, en vertu de laquelle un ressortissant de cet Etat membre, qui réside dans un Etat tiers autre qu'un Etat membre de l'Espace économique européen ou la Suisse, et qui y est affilié à un régime de sécurité sociale, est soumis, dans cet Etat membre, à des prélèvements sur les revenus du capital au titre d'une cotisation au régime de sécurité sociale instauré par celui-ci, alors qu'un ressortissant de l'Union relevant d'un régime de sécurité sociale d'un autre Etat membre en est exonéré en raison du principe de l'unicité de la législation applicable en matière de sécurité sociale en vertu de l'article 11 du règlement (CE) n° 883/2004 du Parlement européen et du Conseil, du 29 avril 2004, portant sur la coordination des systèmes de sécurité sociale ".<br/>
<br/>
              2. Il résulte de ce qu'a ainsi jugé la Cour de justice de l'Union européenne que le moyen de la requête tiré de ce qu'en tant qu'ils excluent du champ du remboursement des prélèvements sur les revenus du patrimoine qu'ils mentionnent les redevables affiliés à la sécurité sociale dans un Etat autre que la Suisse ou que ceux qui font partie de l'Union européenne ou de l'Espace économique européen, les communiqués attaqués, qui rappellent la règle posée par la loi fiscale française, telle qu'interprétée par la jurisprudence du Conseil d'Etat, méconnaîtraient le principe de liberté de circulation des capitaux garanti par l'article 63 du traité sur le fonctionnement de l'Union européenne ne peut qu'être écarté. <br/>
<br/>
              3. En conséquence, les conclusions de M. A...tendant à l'annulation des deux communiqués de presse en tant qu'ils excluent du champ du remboursement des prélèvements sur les revenus du patrimoine qu'ils mentionnent les redevables affiliés à la sécurité sociale dans un Etat autre que la Suisse ou que ceux qui font partie de l'Union européenne ou de l'Espace économique européen doivent être rejetées, ainsi que, par suite, ses conclusions correspondantes aux fins d'injonction.<br/>
<br/>
              4. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise, à ce titre, à la charge de l'Etat, qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>			D E C I D E :<br/>
              			--------------<br/>
Article 1er : Les conclusions de la requête tendant à l'annulation pour excès de pouvoir des communiqués de presse attaqués, en tant qu'ils excluent du champ du remboursement qu'ils prévoient les redevables affiliés à la sécurité sociale dans un Etat autre que les Etats membres de l'Union européenne, les Etats membres de l'Espace économique européen ou la Suisse, sont rejetées, ainsi que les conclusions aux fins d'injonction correspondantes.<br/>
Article 2 : Les conclusions de M. A...tendant à l'application de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La présente décision sera notifiée à M. B...A..., au ministre de l'action et des comptes publics et à la ministre des solidarités et de la santé.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
