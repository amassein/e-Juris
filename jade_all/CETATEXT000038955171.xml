<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038955171</ID>
<ANCIEN_ID>JG_L_2019_08_000000420626</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/95/51/CETATEXT000038955171.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  6ème chambre jugeant seule, 21/08/2019, 420626, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-08-21</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>420626</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 6ème chambre jugeant seule</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GASCHIGNARD ; BROUCHOT</AVOCATS>
<RAPPORTEUR>M. Didier Ribes</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Louis Dutheillet de Lamothe</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2019:420626.20190821</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              Par un mémoire, enregistré le 31 mai 2019 au secrétariat du contentieux du Conseil d'État, présenté en application de l'article 23-5 de l'ordonnance n° 58-1067 du 7 novembre 1958, M. B... A... demande au Conseil d'Etat, à l'appui de sa requête tendant à l'annulation des trois décisions en date du 3 mars 2015 par lesquelles le ministre du budget a refusé de faire droit à ses demandes tendant à ce qu'il lui soit accordé remise des sommes que la Cour des comptes a mises à sa charge par trois arrêts des 21 juin 2004, 26 mai et 4 octobre 2011, de renvoyer au Conseil constitutionnel la question de la conformité aux droits et libertés garantis par la Constitution des VI et IX de l'article 60 de la loi du 23 février 1963. <br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la Constitution, notamment son Préambule et son article 61-1 ; <br/>
              - l'ordonnance n° 58-1067 du 7 novembre 1958 ; <br/>
              - la loi n° 63-156 du 23 février 1963 ;<br/>
              - le code de justice administrative ; <br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Didier Ribes, maître des requêtes, <br/>
<br/>
              - les conclusions de M. Louis Dutheillet de Lamothe, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Gaschignard, avocat de M. A... ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1.	Aux termes du premier alinéa de l'article 23-5 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel : " Le moyen tiré de ce qu'une disposition législative porte atteinte aux droits et libertés garantis par la Constitution peut être soulevé (...) à l'occasion d'une instance devant le Conseil d'Etat (...) ". Il résulte des dispositions de ce même article que le Conseil constitutionnel est saisi de la question prioritaire de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux.<br/>
<br/>
              2.	Aux termes du VI de l'article 60 de la loi du 23 février 1963, portant loi de finances pour 1963, dans sa rédaction antérieure à la loi du 28 décembre 2011 de finances rectificative pour 2011 : " Le comptable public dont la responsabilité pécuniaire est mise en jeu par le ministre dont il relève, le ministre chargé du budget ou le juge des comptes a l'obligation de verser immédiatement de ses deniers personnels une somme égale, soit au montant de la perte de recette subie, de la dépense irrégulièrement payée, de l'indemnité versée, de son fait, à un autre organisme public ou à un tiers, de la rétribution d'un commis d'office par l'organisme public intéressé, soit, dans le cas où il en tient la comptabilité matière, à la valeur du bien manquant. / Toutefois, le comptable public peut obtenir le sursis de versement de la somme fixée à l'alinéa précédent. / Lorsque le ministère public près le juge des comptes requiert l'instruction d'une charge à l'égard du comptable public, ce dernier a la faculté de verser immédiatement de ses deniers personnels une somme égale soit au montant de la perte de recette subie, de la dépense irrégulièrement payée, de l'indemnité versée de son fait à un autre organisme public ou à un tiers, de la rétribution d'un commis d'office par l'organisme public intéressé, soit, dans le cas où il en tient la comptabilité matière, à la valeur du bien manquant. " Aux termes du IX du même article : " Dans les conditions fixées par l'un des décrets prévus au XII, les comptables publics dont la responsabilité personnelle et pécuniaire a été mise en jeu peuvent obtenir la remise gracieuse des sommes laissées à leur charge. / En cas de remise gracieuse, les débets des comptables publics sont supportés par le budget de l'organisme intéressé. Toutefois, ils font l'objet d'une prise en charge par le budget de l'Etat dans les cas et conditions fixés par l'un des décrets prévus au XII. L'Etat est subrogé dans tous les droits des organismes publics à concurrence des sommes qu'il a prises en charge ".<br/>
<br/>
              3.	Il résulte des dispositions citées au point précédent que l'autorité administrative peut accorder à titre gracieux la remise de sommes dues par un comptable dont la responsabilité personnelle et pécuniaire a été mise en jeu. D'une part, eu égard à la finalité du régime de responsabilité des comptables publics, dont l'engagement est soumis à des conditions, précisément définies par la loi, qui ne sont pas critiquées en elles-mêmes, le législateur pouvait prévoir cette faculté de remise gracieuse sans méconnaître le principe d'égalité résultant de la Déclaration des droits de l'homme et du citoyen. D'autre part, il résulte de la jurisprudence du Conseil d'Etat que le refus d'accorder une telle remise peut faire l'objet d'un recours pour excès de pouvoir. Par suite, les VI et IX de l'article 60 de la loi du 23 février 1963 ne portent pas atteinte au droit à un recours juridictionnel effectif garanti par l'article 16 de la même Déclaration.<br/>
<br/>
              4.	Il résulte de ce qui précède que la question prioritaire de constitutionnalité soulevée, qui n'est pas nouvelle, est dépourvue de caractère sérieux. Il n'y a, dès lors, pas lieu de la renvoyer au Conseil constitutionnel.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée par M. A.... <br/>
Article 2 : La présente décision sera notifiée à M. B... A....<br/>
Copie en sera adressée au Premier ministre, au ministre de l'action et des comptes publics, à la ministre de l'enseignement supérieur et de la recherche et au Conseil constitutionnel.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
