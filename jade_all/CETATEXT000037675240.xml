<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037675240</ID>
<ANCIEN_ID>JG_L_2018_11_000000414377</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/67/52/CETATEXT000037675240.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème - 2ème chambres réunies, 30/11/2018, 414377</TITRE>
<DATE_DEC>2018-11-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>414377</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème - 2ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP SPINOSI, SUREAU ; SCP MATUCHANSKY, POUPOT, VALDELIEVRE ; SCP POTIER DE LA VARDE, BUK LAMENT, ROBILLOT ; SCP FOUSSARD, FROGER</AVOCATS>
<RAPPORTEUR>M. Marc Firoud</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Gilles Pellissier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:414377.20181130</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société Philippe Vediaud Publicité a demandé au tribunal administratif de Bordeaux, d'une part, d'annuler le contrat conclu le 14 janvier 2010 entre la commune de Bègles et la société Communication et Développement Atlantique (CDA), ayant pour objet " la mise à disposition de modules d'affichage destinés à l'information municipale et à la publicité ", d'autre part, de condamner la commune de Bègles à l'indemniser des préjudices subis à raison de son éviction irrégulière de ce contrat. Par un jugement n° 1000736 du 25 avril 2013, le tribunal administratif de Bordeaux a annulé le contrat et condamné la commune de Bègles à verser à la société Philippe Vediaud Publicité la somme de 2 000 euros en réparation du préjudice tenant aux frais de présentation de son offre. <br/>
<br/>
              Par un arrêt n°s 13BX01692, 13BX01693, 13BX1745 du 2 juin 2015, la cour administrative d'appel de Bordeaux a annulé ce jugement et rejeté la demande présentée par la société Philippe Védiaud Publicité devant le tribunal administratif de Bordeaux.<br/>
<br/>
              Par une décision du 21 octobre 2016, n° 392355, le Conseil d'Etat, statuant au contentieux, a annulé l'arrêt de la cour et lui a renvoyé ces affaires. <br/>
<br/>
              Par un arrêt n° 16BX03518 du 17 juillet 2017, la cour administrative d'appel de Bordeaux, après avoir décidé qu'il n'y avait pas lieu de statuer sur la requête de la commune de Bègles n° 13BX01693 tendant au sursis à exécution du jugement du tribunal administratif de Bordeaux du 25 avril 2013, a annulé le jugement du tribunal administratif de Bordeaux du 25 avril 2013 en tant qu'il statue sur les conclusions indemnitaires de la société Philippe Vediaud Publicité, et a rejeté ces conclusions indemnitaires ainsi que le surplus des conclusions des parties.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et deux nouveaux mémoires, enregistrés les 18 septembre et 18 décembre 2017 ainsi que les 23 mai et 3 septembre 2018 au secrétariat du contentieux du Conseil d'Etat, la société CDA Publimedia, venant aux droits de la société CDA, demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt en tant qu'il a rejeté ses conclusions d'appel incident tendant à l'annulation du jugement du 25 avril 2013 du tribunal administratif de Bordeaux en tant qu'il a annulé le contrat qu'elle avait conclu avec la commune de Bègles le 14 janvier 2010 ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à ses conclusions d'appel incident ;<br/>
<br/>
              3°) de mettre à la charge de la société Philippe Vediaud Publicité la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code général des collectivités territoriales ; <br/>
              - le code des marchés publics ; <br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Marc Firoud, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Gilles Pellissier, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Spinosi, Sureau, avocat de la société CDA Publimedia, à la SCP Matuchansky, Poupot, Valdelièvre, avocat de la société Philippe Vediaud Publicité, à la SCP Potier de la Varde, Buk Lament, Robillot, avocat de la commune de Bègles et à la SCP Foussard, Froger, avocat de Bordeaux Métropole.<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. La société Philippe Vediaud Publicité a demandé au tribunal administratif de Bordeaux, d'une part, d'annuler le contrat conclu le 14 janvier 2010 entre la commune de Bègles et la société Communication et Développement Atlantique (CDA), ayant pour objet " la mise à disposition de modules d'affichage destinés à l'information municipale et à la publicité ", d'autre part, de condamner la commune de Bègles à l'indemniser des préjudices subis à raison de son éviction irrégulière de ce contrat. Par un jugement du 25 avril 2013, le tribunal administratif de Bordeaux a annulé ce contrat et condamné la commune de Bègles à verser à la société Philippe Vediaud Publicité la somme de 2 000 euros. Sur appel de la commune de Bègles et de la société Philippe Vediaud Publicité, la cour administrative d'appel de Bordeaux a, par un arrêt du 2 juin 2015, annulé ce jugement et rejeté la demande présentée par la société Philippe Vediaud Publicité. Par une décision du 21 octobre 2016, le Conseil d'Etat, statuant au contentieux, a annulé l'arrêt de la cour et lui a renvoyé l'affaire. Par un arrêt du 17 juillet 2017, contre lequel la société CDA Publimedia, venant aux droits de la société CDA, se pourvoit en cassation, la cour administrative d'appel de Bordeaux a annulé le jugement du 25 avril 2013 en tant qu'il a statué sur les conclusions indemnitaires de la société Philippe Vediaud Publicité, rejeté les conclusions indemnitaires présentées par cette même société et, enfin, rejeté le surplus des conclusions des parties.<br/>
<br/>
              Sur l'intervention de Bordeaux Métropole :<br/>
<br/>
              2. Bordeaux Métropole justifie, eu égard à la nature et à l'objet du litige, d'un intérêt suffisant à l'annulation de l'arrêt attaqué. Par suite, son intervention est recevable. <br/>
<br/>
              Sur le pourvoi en cassation de la société CDA Publimedia :<br/>
<br/>
              3. Il ressort des énonciations de l'arrêt attaqué que la cour administrative d'appel de Bordeaux a estimé que le contrat attribué par la commune de Bègles, qui, à la date à laquelle il a été conclu, était membre de la communauté urbaine de Bordeaux, devenue Bordeaux Métropole, avait un contenu illicite faute pour la commune de disposer du pouvoir de concéder à son cocontractant, en contrepartie des prestations obtenues, le droit d'exploiter commercialement les mobiliers urbains, dès lors qu'ils étaient installés sur le domaine public routier dont la communauté urbaine était devenue seule gestionnaire en vertu de l'article L. 5215-20-1 du code général des collectivités territoriales, qui, dans sa rédaction alors applicable, avait transféré aux communautés urbaines les pouvoirs des communes membres en matière de gestion du domaine public routier, et que l'installation de ces mobiliers impliquait une emprise au sol. <br/>
<br/>
              4. Toutefois, il ressort également des énonciations de l'arrêt attaqué que le marché de mobilier urbain passé par la commune avait pour objet de permettre la réalisation et la fourniture de prestations de service en matière d'information municipale par voie d'affichage. Ce contrat répondait aux besoins de la commune. En contrepartie des prestations assurées, le cocontractant se rémunérait par l'exploitation, à titre exclusif, d'une partie des mobiliers urbains à des fins publicitaires. Un tel contrat ne constituait ainsi ni une simple convention domaniale, ni une convention se rapportant à la gestion de la voirie. <br/>
<br/>
              5. Par suite, si l'installation sur le domaine public routier des dispositifs de mobilier urbain nécessitait la délivrance d'une autorisation de la part du gestionnaire du domaine public, celui-ci n'était compétent ni pour prendre la décision de recourir à ce mode d'affichage, ni pour l'exploiter. La cour administrative d'appel de Bordeaux a, dès lors, commis une erreur de droit en déduisant de la circonstance que l'implantation des mobiliers urbains sur le domaine public routier nécessitait la délivrance d'une permission de voirie par la communauté urbaine de Bordeaux l'incompétence de la commune pour passer un tel contrat.<br/>
<br/>
              6. Il résulte de ce qui précède, et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, que la société CDA Publimedia est fondée à demander l'annulation de l'arrêt attaqué.<br/>
<br/>
              7. Aux termes du second alinéa de l'article L. 821-2 du code de justice administrative : " Lorsque l'affaire fait l'objet d'un second pourvoi en cassation, le Conseil d'Etat statue définitivement sur cette affaire ". Le Conseil d'Etat étant saisi, en l'espèce, d'un second pourvoi en cassation, il lui incombe de régler l'affaire au fond.<br/>
<br/>
              Sur la requête d'appel de la commune de Bègles :<br/>
<br/>
              8. Aux termes des dispositions du II de l'article 59 du code des marchés publics, alors en vigueur : " Après classement des offres finales conformément au III de l'article 53, l'offre économiquement la plus avantageuse est choisie en application du ou des critères annoncés dans l'avis d'appel public à la concurrence ou dans le règlement de la consultation. Ce choix est effectué par la commission d'appel d'offres pour les collectivités territoriales (...) ". <br/>
<br/>
              9. Pour annuler le contrat de mobilier urbain conclu entre la commune de Bègles et la société CDA, le tribunal administratif de Bordeaux s'est fondé sur la circonstance que le choix de l'attributaire avait été effectué par l'autorité municipale et non par la commission d'appel d'offres, en méconnaissance des dispositions citées au point 8. Il résulte toutefois de l'instruction que c'est la commission d'appel d'offres, et non l'autorité municipale, qui a procédé au choix du candidat ayant proposé l'offre économiquement la plus avantageuse, ainsi qu'il ressort notamment du compte rendu de la réunion du 9 décembre 2009, signé par les membres de la commission d'appel d'offres. Par suite, la commune de Bègles est fondée à soutenir que c'est à tort que le tribunal administratif de Bordeaux s'est fondé sur ce motif pour faire droit à la demande d'annulation présentée par la société Philippe Vediaud Publicité.<br/>
<br/>
              10. Il appartient au Conseil d'Etat, saisi de l'ensemble du litige par l'effet dévolutif de l'appel, d'examiner les autres moyens soulevés par la société Philippe Vediaud Publicité devant le tribunal administratif de Bordeaux à l'appui de sa demande d'annulation du contrat.<br/>
<br/>
              11. Eu égard à la date d'engagement de la procédure de passation et à la date de signature du contrat, le concurrent évincé peut se prévaloir, en l'espèce, de tout vice à l'encontre du contrat. <br/>
<br/>
              En ce qui concerne l'incompétence de l'autorité exécutive pour signer le contrat :<br/>
<br/>
              12. Il résulte de l'instruction que la délibération du 14 décembre 2009 par laquelle le conseil municipal de Bègles a autorisé le maire à signer le contrat a été reçue par les services de la préfecture le 17 décembre 2009, avant la signature du contrat par le maire de la commune de Bègles le 14 janvier 2010. Par suite, le moyen tiré de ce que le contrat serait entaché de nullité faute pour sa signature d'avoir été précédée de la transmission au contrôle de légalité de la délibération du conseil municipal autorisant cette signature ne peut qu'être écarté. <br/>
<br/>
              13. Il ressort par ailleurs des termes mêmes de la délibération précitée qu'elle décrit l'objet ainsi que le contenu du contrat et qu'elle indique que le candidat se rémunèrera par les recettes publicitaires versées par les annonceurs, de sorte que la commune de Bègles n'ait à engager aucune dépense. La délibération précise également le nom de l'entreprise avec laquelle le contrat doit être passé, après avoir rappelé qu'elle a été retenue par la commission d'appel d'offres. Dans ces conditions, le moyen tiré de l'insuffisante information des membres du conseil municipal sur le contrat dont ils ont autorisé la signature doit être écarté.<br/>
<br/>
              En ce qui concerne l'illégalité des critères d'attribution du contrat litigieux :<br/>
<br/>
              14. Aux termes des dispositions du II de l'article 53 du code des marchés publics, alors en vigueur : " Pour les marchés passés selon une procédure formalisée autre que le concours et lorsque plusieurs critères sont prévus, le pouvoir adjudicateur précise leur pondération. / Le poids de chaque critère peut être exprimé par une fourchette dont l'écart maximal est approprié. / Le pouvoir adjudicateur qui estime pouvoir démontrer que la pondération n'est pas possible notamment du fait de la complexité du marché, indique les critères par ordre décroissant d'importance. / Les critères ainsi que leur pondération ou leur hiérarchisation sont indiqués dans l'avis d'appel public à la concurrence ou dans les documents de la consultation ".<br/>
<br/>
              15. Les dispositions citées au point 14 font obligation au pouvoir adjudicateur d'informer les candidats à des contrats passés selon une procédure formalisée, autre que le concours, des critères de sélection des offres ainsi que de leur pondération ou de leur hiérarchisation. Si, lorsque le pouvoir adjudicateur décide, pour mettre en oeuvre ces critères de sélection, de faire usage de sous-critères pondérés ou hiérarchisés, il est tenu de porter à la connaissance des candidats la pondération ou la hiérarchisation de ces sous-critères lorsque, eu égard à leur nature et à l'importance de cette pondération ou hiérarchisation, ils sont susceptibles d'exercer une influence sur la présentation des offres par les candidats ainsi que sur leur sélection, et doivent, en conséquence, être eux-mêmes regardés comme des critères de sélection, il ne résulte pas de l'instruction que la commune de Bègles, qui a indiqué dans le règlement de consultation que le 3ème critère de sélection des offres serait " les qualités esthétiques et fonctionnelles ", pondéré à hauteur de 20 % , et que le 4ème critère serait " le service après-vente et l'assistance technique ", pondéré à hauteur de 10% , ait entendu, pour apprécier chacun de ces critères, faire usage de sous-critères pondérés ou hiérarchisés. Par suite, le moyen tiré de ce que la commune aurait méconnu son obligation d'informer les candidats, en ne leur indiquant pas l'importance particulière accordée au sous-critère relatif aux qualités esthétiques par rapport au sous-critère relatif aux qualités fonctionnelles ainsi qu'au sous-critère relatif au service après-vente par rapport au sous-critère relatif à l'assistance technique, doit être écarté.<br/>
<br/>
              16. S'il est également soutenu que le contrat serait entaché d'irrégularité au motif que les sous-critères " qualité des matériaux " et " sécurité des vitrages " ont à tort été indiqués par la commune de Bègles pour apprécier le critère des qualités esthétiques et de l'intégration dans l'environnement urbain, il résulte de l'instruction que de tels sous-critères ne figurent ni dans le règlement de la consultation ni dans les autres documents soumis à la consultation.<br/>
<br/>
              17. La société Philippe Vediaud Publicité soutient, enfin, que si le critère de la valeur technique de l'offre a été décomposé en cinq sous-critères, trois de ces sous-critères, respectivement relatifs aux matériaux utilisés dans le cadre d'achats responsables, à la consommation électrique et aux modalités de mise en oeuvre de la clause d'insertion sociale, sont sans rapport avec le critère de la valeur technique et doivent être regardés comme étant rattachés soit au critère des performances en matière de protection de l'environnement, soit au critère des performances en matière d'insertion professionnelle des publics en difficulté. Toutefois, la qualité des matériaux utilisés et la consommation électrique des panneaux d'affichage sont des sous-critères qui ne sont pas sans lien avec la valeur technique de l'offre. Par ailleurs, le critère de la mise en oeuvre de la clause d'insertion sociale peut être regardé comme étant en rapport avec les conditions d'exécution du contrat. Le moyen de l'irrégularité de ces sous-critères doit, par suite, être écarté.<br/>
<br/>
              En ce qui concerne l'abandon de l'option obligatoire en cours de procédure :<br/>
<br/>
              18. La société Philippe Vediaud Publicité soutient que l'abandon de l'option obligatoire en cours de procédure a eu pour effet de fausser les règles de mise en concurrence car elle a faussé la note finale. Toutefois, d'une part, s'il résulte de l'instruction que le règlement de consultation invitait les candidats à présenter une option en complément de l'offre de base sous la forme décrite à l'article 6 du cahier des clauses techniques particulières, c'est-à-dire de cinq écrans plats installés dans différents endroits de la ville et pilotés par l'entreprise, ce même règlement indiquait également que la commune se réservait le droit de ne pas retenir l'option. Tous les candidats étaient donc informés de l'abandon possible de cette option. D'autre part, il ressort du compte rendu de la commission d'appel d'offres du 9 décembre 2009 et du rapport d'analyse qui y était joint que l'appréciation des offres n'a été faite qu'au regard des éléments proposés dans chacune des offres de base, l'option n'étant pas examinée. Dès lors, le moyen tiré de ce que la commune de Bègles s'est illégalement abstenue de noter l'offre dans sa globalité et que l'abandon de l'option obligatoire a faussé la notation finale doit être écarté.<br/>
<br/>
              En ce qui concerne les variantes pouvant être proposées avec l'offre de base :<br/>
<br/>
              19. Aux termes des dispositions de l'article 50 du code des marchés publics, alors en vigueur : " I. - Pour les marchés passés selon une procédure formalisée, lorsque le pouvoir adjudicateur se fonde sur plusieurs critères pour attribuer le marché, il peut autoriser les candidats à présenter des variantes. Le pouvoir adjudicateur indique dans l''avis d'appel public à la concurrence ou dans les documents de la consultation s'il autorise ou non les variantes ; à défaut d'indication, les variantes ne sont pas admises. / Les documents de la consultation mentionnent les exigences minimales que les variantes doivent respecter ainsi que les modalités de leur présentation. Seules les variantes répondant à ces exigences minimales peuvent être prises en considération ". <br/>
<br/>
              20. Le règlement de la consultation prévoyait que les candidats pouvaient proposer des variantes avec l'offre de base. L'article 6 du cahier des clauses techniques particulières stipulait que les variantes consistaient en " deux modèles supplémentaires de son mobilier, aux dimensions identiques et caractéristiques techniques équivalentes ". L'article 7 du même cahier énonçait les dimensions et les caractéristiques techniques et esthétiques précises de ce mobilier consistant en des modules d'affichage. Dans ces conditions, le moyen tiré de ce que la commune de Bègles s'est abstenue de définir les exigences minimales que les variantes devaient respecter doit être écarté. <br/>
<br/>
              21. Il ressort par ailleurs du compte rendu de la commission d'appel d'offres du 9 décembre 2009 et de l'analyse des offres qui y est jointe que la commission a examiné les variantes proposées par les soumissionnaires et a apprécié les offres en les prenant en compte. Aucune disposition du code des marchés publics alors en vigueur n'imposait à la commission d'examiner d'abord l'offre de base puis les variantes. Par suite, le moyen tiré de ce que la commune n'aurait pas procédé à une analyse objective des différentes offres proposées, de base et en variantes, et méconnu ce faisant le principe d'égalité de traitement des candidats doit être écarté.<br/>
<br/>
              En ce qui concerne le caractère illicite de l'objet du contrat :<br/>
<br/>
              22. Si la société Philippe Vediaud Publicité soutient que le contrat est entaché de nullité en raison de l'illicéité de son objet, dès lors que les mobiliers urbains visés par le contrat sont destinés à être installés sur le domaine public communautaire dont la communauté urbaine de Bordeaux est le propriétaire et le gestionnaire exclusif, il résulte de ce qui a été dit aux points 3 à 5 que la commune de Bègles était compétente pour conclure le contrat de mobilier urbain litigieux, qui vise à satisfaire aux besoins de la commune en matière d'information municipale. Par suite, le moyen tiré de l'illégalité de l'objet du contrat doit être écarté.<br/>
<br/>
              En ce qui concerne l'inexactitude ou l'incomplétude des informations fournies par la société CDA :<br/>
<br/>
              23. La société Philippe Vediaud Publicité soutient que, pour obtenir le contrat, la société CDA Publimedia a produit de fausses références ou des références incomplètes. Toutefois, il résulte de l'instruction, d'une part, que si des références produites à l'appui de la candidature de la société CDA Publimedia peuvent être regardées comme inexactes ou incomplètes, leur nombre a été peu élevé au regard des très nombreuses références non contestées présentées par la CDA Publimedia. Il résulte également de l'instruction, d'autre part, que nonobstant la présence de ces références inexactes ou incomplètes, la commission d'appel d'offres ne s'est pas livrée à une appréciation manifestement erronée en estimant que la société CDA Publimedia présentait les garanties et capacités techniques requises pour exécuter le contrat. Le moyen tiré de ce que la société CDA Publimedia se serait, en produisant de fausses références, livrée à des manoeuvres dolosives de nature à vicier le consentement du pourvoir adjudicateur ne peut, par suite, qu'être écarté.<br/>
<br/>
              24. Il résulte de tout ce qui précède que la commune de Bègles est fondée à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Bordeaux a annulé le contrat qu'elle a conclu avec la société CDA et l'a condamnée à verser la somme de 2 000 euros, tous intérêts compris, à la société Philippe Vediaud Publicité en réparation du préjudice subi par cette société à raison de son éviction irrégulière de l'attribution du contrat.<br/>
<br/>
              Sur les autres requêtes :<br/>
<br/>
              25. La présente décision annulant le jugement du 25 avril 2013 du tribunal administratif de Bordeaux, la requête de la commune de Bègles tendant au sursis à exécution de ce jugement devient sans objet. Il en va de même de la requête de la société Philippe Vediaud Publicité tendant à l'annulation de ce jugement en tant qu'il a limité à 2 000 euros l'indemnisation du préjudice résultant de son éviction de l'attribution du contrat. Il n'y a, par suite, pas non plus lieu de statuer sur les conclusions incidentes présentées à l'appui de cette dernière requête par la société CDA Publimedia et par la commune de Bègles.<br/>
<br/>
              Sur les conclusions tendant à l'application de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              26. Il y a lieu, dans les circonstances de l'espèce, de faire application de l'article L. 761-1 du code de justice administrative et de mettre à la charge de la société Philippe Vediaud Publicité la somme de 3 000 euros au titre des frais exposés par la commune de Bègles et non compris dans les dépens pour l'ensemble de la procédure, ainsi que la somme de 3 000 euros au titre des mêmes frais exposés par la société CDA Publimedia.<br/>
<br/>
              27. Les dispositions de l'article L. 761-1 du code de justice administrative font, en revanche, obstacle à ce que soit mise à la charge de la commune de Bègles, qui n'est pas la partie perdante dans les présentes instances, une somme au titre des frais exposés par la société Philippe Vediaud Publicité et non compris dans les dépens.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'intervention de Bordeaux Métropole est admise. <br/>
Article 2 : L'arrêt du 17 juillet 2017 de la cour administrative d'appel de Bordeaux et le jugement du tribunal administratif de Bordeaux du 25 avril 2013 sont annulés. <br/>
Article 3 : La demande présentée par la société Philippe Vediaud Publicité devant le tribunal administratif de Bordeaux est rejetée. <br/>
Article 4 : Il n'y a pas lieu de statuer sur la requête de la commune de Bègles tendant au sursis à exécution du jugement du 25 avril 2013 du tribunal administratif de Bordeaux ni sur la requête d'appel de la société Philippe Vediaud Publicité. <br/>
Article 5 : Le surplus des conclusions présentées par la société CDA Publimédia et la commune de Bègles est rejeté.<br/>
Article 6 : La société Philippe Vediaud Publicité versera la somme de 3 000 euros à la commune de Bègles ainsi que la somme de 3 000 euros à la société CDA Publimedia au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 7: Le présent arrêt sera notifié à la société CDA Publimedia, à la commune de Bègles, à la société Philippe Vediaud Publicité et à Bordeaux Métropole.<br/>
Copie en sera adressée au ministre de l'économie et des finances et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">135-02-03-01 COLLECTIVITÉS TERRITORIALES. COMMUNE. ATTRIBUTIONS. COMPÉTENCES TRANSFÉRÉES. - COMPÉTENCE RELATIVE À LA GESTION DU DOMAINE PUBLIC - COMPÉTENCE TRANSFÉRÉE AUX COMMUNAUTÉS URBAINES (ART. L. 5215-20-1 DU CGCT ALORS EN VIGUEUR) - MARCHÉ DE MOBILIER URBAIN POUR UNE COMMUNE - COMPÉTENCE DE CETTE COMMUNE POUR PASSER UN TEL CONTRAT - EXISTENCE, NONOBSTANT CE TRANSFERT [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">135-05-01-07 COLLECTIVITÉS TERRITORIALES. COOPÉRATION. ÉTABLISSEMENTS PUBLICS DE COOPÉRATION INTERCOMMUNALE - QUESTIONS GÉNÉRALES. COMMUNAUTÉS URBAINES. - COMPÉTENCE RELATIVE À LA GESTION DU DOMAINE PUBLIC - COMPÉTENCE TRANSFÉRÉE AUX COMMUNAUTÉS URBAINES (ART. L. 5215-20-1 DU CGCT ALORS EN VIGUEUR) - MARCHÉ DE MOBILIER URBAIN POUR UNE COMMUNE - COMPÉTENCE DE CETTE COMMUNE POUR PASSER UN TEL CONTRAT - EXISTENCE, NONOBSTANT CE TRANSFERT [RJ1].
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">39-02-01 MARCHÉS ET CONTRATS ADMINISTRATIFS. FORMATION DES CONTRATS ET MARCHÉS. QUALITÉ POUR CONTRACTER. - MARCHÉ DE MOBILIER URBAIN POUR UNE COMMUNE - COMPÉTENCE DE CETTE COMMUNE POUR PASSER UN TEL CONTRAT - EXISTENCE, QUAND BIEN MÊME LA COMPÉTENCE RELATIVE À LA GESTION DU DOMAINE PUBLIC RELÈVE D'UNE AUTRE COLLECTIVITÉ TERRITORIALE [RJ1].
</SCT>
<ANA ID="9A"> 135-02-03-01 Le marché de mobilier urbain passé par la commune a pour objet de permettre la réalisation et la fourniture de prestations de service en matière d'information municipale par voie d'affichage. Ce contrat répond aux besoins de la commune. En contrepartie des prestations assurées, le cocontractant se rémunère par l'exploitation, à titre exclusif, d'une partie des mobiliers urbains à des fins publicitaires. Un tel contrat ne constitue ainsi ni une simple convention domaniale, ni une convention se rapportant à la gestion de la voirie.,,,Par suite, si l'installation sur le domaine public routier des dispositifs de mobilier urbain nécessite la délivrance d'une autorisation de la part de la communautaire urbaine, seule gestionnaire du domaine public en vertu de l'article L. 5215-20-1 du code général des collectivités territoriales (CGCT) alors en vigueur, celle-ci n'est compétente ni pour prendre la décision de recourir à ce mode d'affichage, ni pour l'exploiter. Dès lors, commet une erreur de droit une cour administrative d'appel qui déduit de la circonstance que l'implantation des mobiliers urbains sur le domaine public routier nécessite la délivrance d'une permission de voirie par la communauté urbaine l'incompétence de la commune pour passer un tel contrat.</ANA>
<ANA ID="9B"> 135-05-01-07 Le marché de mobilier urbain passé par la commune a pour objet de permettre la réalisation et la fourniture de prestations de service en matière d'information municipale par voie d'affichage. Ce contrat répond aux besoins de la commune. En contrepartie des prestations assurées, le cocontractant se rémunère par l'exploitation, à titre exclusif, d'une partie des mobiliers urbains à des fins publicitaires. Un tel contrat ne constitue ainsi ni une simple convention domaniale, ni une convention se rapportant à la gestion de la voirie.,,,Par suite, si l'installation sur le domaine public routier des dispositifs de mobilier urbain nécessite la délivrance d'une autorisation de la part de la communautaire urbaine, seule gestionnaire du domaine public en vertu de l'article L. 5215-20-1 du code général des collectivités territoriales (CGCT) alors en vigueur, celle-ci n'est compétente ni pour prendre la décision de recourir à ce mode d'affichage, ni pour l'exploiter. Dès lors, commet une erreur de droit une cour administrative d'appel qui déduit de la circonstance que l'implantation des mobiliers urbains sur le domaine public routier nécessite la délivrance d'une permission de voirie par la communauté urbaine l'incompétence de la commune pour passer un tel contrat.</ANA>
<ANA ID="9C"> 39-02-01 Le marché de mobilier urbain passé par la commune a pour objet de permettre la réalisation et la fourniture de prestations de service en matière d'information municipale par voie d'affichage. Ce contrat répond aux besoins de la commune. En contrepartie des prestations assurées, le cocontractant se rémunère par l'exploitation, à titre exclusif, d'une partie des mobiliers urbains à des fins publicitaires. Un tel contrat ne constitue ainsi ni une simple convention domaniale, ni une convention se rapportant à la gestion de la voirie.,,,Par suite, si l'installation sur le domaine public routier des dispositifs de mobilier urbain nécessite la délivrance d'une autorisation de la part de la communautaire urbaine, seule gestionnaire du domaine public en vertu de l'article L. 5215-20-1 du code général des collectivités territoriales (CGCT), celle-ci n'est compétente ni pour prendre la décision de recourir à ce mode d'affichage, ni pour l'exploiter. Dès lors, commet une erreur de droit une cour administrative d'appel qui déduit de la circonstance que l'implantation des mobiliers urbains sur le domaine public routier nécessite la délivrance d'une permission de voirie par la communauté urbaine l'incompétence de la commune pour passer un tel contrat.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., CE, Assemblée, 4 novembre 2005, Société Jean-Claude,, n°s 247298 247299, p. 476.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
