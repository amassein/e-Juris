<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044287027</ID>
<ANCIEN_ID>JG_L_2020_12_000000447063</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/28/70/CETATEXT000044287027.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 24/12/2020, 447063, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-12-24</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>447063</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP SEVAUX, MATHONNET</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2020:447063.20201224</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
      I. Sous le n°447063, par une requête et un nouveau mémoire, enregistrés les 30 novembre et 17 décembre 2020 au secrétariat du contentieux du Conseil d'Etat, le Syndicat des avocats de France, l'association Avocats pour la défense des droits des étrangers (ADDE) et l'association ELENA France demandent au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative : <br/>
      1°) d'ordonner la suspension de l'exécution des articles 2 et 3 de l'ordonnance n°2020-1402 du 18 novembre 2020 portant adaptation des règles applicables aux juridictions de l'ordre administratif ;<br/>
      2°) de mettre à la charge de l'Etat la somme de 2 000 euros à verser à chacune des requérantes au titre des dispositions de l'article L. 761-1 du code de justice administrative<br/>
      Ils soutiennent que :<br/>
      - ils disposent d'un intérêt pour agir eu égard à leur objet social ;<br/>
      - leur requête est recevable dès lors que l'ordonnance contestée n'a pas fait l'objet d'une ratification par le Parlement et que ce délai n'a pas encore expiré ;<br/>
      - la condition d'urgence est satisfaite dès lors que l'ordonnance litigieuse, d'une part, autorise dès son entrée en vigueur la possibilité pour le juge des référés de statuer sans audience préalable ainsi que la tenue de l'audience par visioconférence pour l'ensemble du contentieux porté devant les juridictions administratives, et, d'autre part, méconnaît le droit des justiciables de bénéficier d'un traitement égal et d'un procès juste et équitable, et, en dernier lieu, peut être présumée satisfaite en cas de contestation de mesures prises dans le cadre de l'état d'urgence sanitaire ;<br/>
      - il existe un doute sérieux quant à la légalité des dispositions contestée ;<br/>
      - le dispositif introduit par les dispositions de l'article 2 de l'ordonnance contestée n'est pas adapté, proportionné et nécessaire à l'objectif de lutte contre la propagation du virus de la covid 19 et au principe de continuité du service public dès lors que, en premier lieu, il n'est pas justifié par un sous-effectif au sein des juridictions administratives, en deuxième lieu, il n'était pas prévu lors du premier confinement, en troisième lieu la situation sanitaire s'améliore ; <br/>
      - les dispositions de l'ordonnance contestées méconnaissent le droit à un recours effectif et les droits de la défense, qui comprennent le droit à un procès équitable et le droit de comparaître à une audience, dès lors qu'elles permettent la tenue d'une audience de façon dématérialisée, en utilisant un moyen de télécommunication audiovisuelle, alors que la possibilité du recours aux audiences par visioconférence n'est pas assortie de suffisamment de garanties pour assurer, notamment, le respect du consentement des parties ou la qualité de la retranscription dans le procès-verbal ;  <br/>
      - les dispositions de l'article 3 de l'ordonnance contestée sont entachées d'illégalités et méconnaissent l'article 6 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales dès lors, d'une part, qu'elles instaurent la possibilité pour le juge des référés de statuer sans audience préalable alors que des observations orales peuvent être ajoutées aux écritures et, d'autre part, qu'elles sont inadaptées et disproportionnées à l'objectif de lutte contre l'épidémie de covid-19 eu égard à l'amélioration de la situation sanitaire et à la réouverture des commerces non-essentiels.<br/>
      Par un mémoire en défense, enregistré le 11 décembre 2020, le ministre de la justice conclut au rejet de la requête. Il soutient que la condition d'urgence n'est pas satisfaite et qu'aucun moyen de la requête n'est propre à créer un doute sérieux quant à la légalité de l'ordonnance contestée.<br/>
      La requête a été communiquée au Premier ministre qui n'a pas présenté d'observations.<br/>
      II. Sous le n°447066, par une requête et un nouveau mémoire, enregistrés le 30 novembre et 17 décembre 2020 au secrétariat du contentieux du Conseil d'Etat, le Conseil national des barreaux et la Conférence des Bâtonniers demandent au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative : <br/>
      1°) d'ordonner la suspension de l'exécution de l'ordonnance n°2020-1402 du 18 novembre 2020 portant adaptation des règles applicables aux juridictions de l'ordre administratif ;<br/>
      2°) de mettre à la charge de l'Etat la somme de 1 000 euros à verser à chacune des requérantes au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
      Ils soutiennent que :<br/>
      - ils disposent d'un intérêt pour agir eu égard à leur objet social ;<br/>
      - leur requête est recevable dès lors que l'ordonnance contestée n'a pas fait l'objet d'une ratification par le Parlement et que ce délai n'a pas encore expiré ;<br/>
      - la condition d'urgence est satisfaite dès lors que l'ordonnance litigieuse, d'une part, autorise dès son entrée en vigueur la possibilité pour le juge des référés de statuer sans audience préalable ainsi que la tenue de l'audience par visioconférence pour l'ensemble du contentieux porté devant les juridictions administratives, et, d'autre part, méconnaît le droit des justiciables de bénéficier d'un traitement égal et d'un procès juste et équitable, et, en dernier lieu, peut être présumée satisfaite en cas de contestation de mesures prises dans le cadre de l'état d'urgence sanitaire ;<br/>
      - il existe un doute sérieux quant à la légalité des dispositions contestée ;<br/>
      - le dispositif introduit par les dispositions de l'article 2 de l'ordonnance contestée n'est pas adapté, proportionné et nécessaire à l'objectif lutte contre la propagation du virus de la covid-19 et au principe de continuité du service public dès lors que, en premier lieu, il n'est pas justifié par un sous-effectif au sein des juridictions administratives, en deuxième lieu, il n'était pas prévu lors du premier confinement, en troisième lieu la situation sanitaire s'améliore ; <br/>
      - les dispositions de l'ordonnance contestées méconnaissent le droit à un recours effectif et les droits de la défense, qui comprennent le droit à un procès équitable et le droit de comparaître à une audience, dès lors qu'elles permettent la tenue d'une audience de façon dématérialisée, en utilisant un moyen de télécommunication audiovisuelle, alors que la possibilité du recours aux audiences par visioconférence n'est pas assortie de suffisamment de garanties pour assurer, notamment, le respect du consentement des parties ou la qualité de la retranscription dans le procès-verbal ;  <br/>
      - les dispositions de l'article 3 de l'ordonnance contestée sont entachées d'illégalités et méconnaissent l'article 6 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales dès lors, d'une part, qu'elles instaurent la possibilité pour le juge des référés de statuer sans audience préalable alors que des observations orales peuvent être ajoutées aux écritures et, d'autre part, qu'elles sont inadaptées et disproportionnées à l'objectif de lutte contre l'épidémie de covid-19 eu égard à l'amélioration de la situation sanitaire et à la réouverture des commerces non-essentiels.<br/>
      Par un mémoire en défense, enregistré le 11 décembre 2020, le ministre de la justice conclut au rejet de la requête. Il soutient que la condition d'urgence n'est pas satisfaite et qu'aucun moyen de la requête n'est propre à créer un doute sérieux quant à la légalité de l'ordonnance contestée.<br/>
      La requête a été communiquée au Premier ministre qui n'a pas présenté d'observations.<br/>
      Vu les autres pièces du dossier ;<br/>
      Vu : <br/>
      - la Constitution, et notamment son préambule ;<br/>
      - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ; <br/>
      - l'ordonnance n°2020-1402 du 18 novembre 2020 ; <br/>
      - le code de la santé publique ; <br/>
      - le décret n° 2020-1310 du 29 octobre 2020 ; <br/>
      - le décret 2020-1454 du 27 novembre 2020 ; <br/>
      - le code de justice administrative ;<br/>
      Après avoir convoqué à une audience publique, d'une part, le syndicat des avocats de France, le Conseil national des barreaux et les autres requérants et, d'autre part, le garde des sceaux, ministre de la justice <br/>
      Ont été entendus lors de l'audience publique du 18 décembre 2020, à 11 heures : <br/>
      - Me Mathonnet, avocat au Conseil d'Etat et à la Cour de cassation, avocat du Syndicat des avocats de France, du Conseil national des barreaux et des autres requérants ; <br/>
      - la représentante de l'ADDE ; <br/>
      - le représentant de la Conférence des Bâtonniers ; <br/>
      - les représentants du ministre de la justice ;  <br/>
      à l'issue de laquelle le juge des référés a clos l'instruction._<br/>
(CONSIDERANT)<br/>(/VISAS)<br/>
      Considérant ce qui suit :<br/>
      1. Les requêtes visées ci-dessus, présentées sur le fondement de l'article L. 521 1 du code de justice administrative, sont dirigées contre les mêmes dispositions de la même ordonnance. Il y a lieu de les joindre pour statuer par une seule ordonnance. <br/>
      2. Aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision ".<br/>
      Sur les circonstances et le cadre juridique du litige : <br/>
      3. Aux termes de l'article L. 3131-12 du code de la santé publique, issu de la loi du 23 mars 2020 d'urgence pour faire face à l'épidémie de covid-19: " L'état d'urgence sanitaire peut être déclaré sur tout ou partie du territoire métropolitain ainsi que du territoire des collectivités régies par les articles 73 et 74 de la Constitution et de la Nouvelle-Calédonie en cas de catastrophe sanitaire mettant en péril, par sa nature et sa gravité, la santé de la population ". L'article L. 3131-13 du même code précise que " L'état d'urgence sanitaire est déclaré par décret en conseil des ministres pris sur le rapport du ministre chargé de la santé. (...) / La prorogation de l'état d'urgence sanitaire au-delà d'un mois ne peut être autorisée que par la loi, après avis du comité de scientifiques prévu à l'article L. 3131-19 ". En vertu de l'article L. 3131-15 du même code, " dans les circonscriptions territoriales où l'état d'urgence sanitaire est déclaré, le Premier ministre peut, par décret réglementaire pris sur le rapport du ministre chargé de la santé, aux seules fins de garantir la santé publique " prendre un certain nombre de mesures de restriction ou d'interdiction des déplacements, rassemblements sur la voie publique et réunions " strictement proportionnées aux risques sanitaires encourus et appropriées aux circonstances de temps et de lieu ". <br/>
      4. L'émergence d'un nouveau coronavirus, responsable de la maladie à coronavirus 2019 ou covid-19 et particulièrement contagieux, a été qualifiée d'urgence de santé publique de portée internationale par l'Organisation mondiale de la santé le 30 janvier 2020, puis de pandémie le 11 mars 2020. La propagation du virus sur le territoire français a conduit le ministre des solidarités et de la santé puis le Premier ministre à prendre, à compter du 4 mars 2020, des mesures de plus en plus strictes destinées à réduire les risques de contagion. Pour faire face à l'aggravation de l'épidémie, la loi du 23 mars 2020 mentionnée ci-dessus a créé un régime d'état d'urgence sanitaire aux articles L. 3131-12 à L. 3131-20 du code de la santé publique et déclaré l'état d'urgence sanitaire pour une durée de deux mois à compter du 24 mars 2020. La loi du 11 mai 2020 prorogeant l'état d'urgence sanitaire et complétant ces dispositions, a prorogé cet état d'urgence sanitaire jusqu'au 10 juillet 2020 inclus. L'évolution de la situation sanitaire a conduit à un assouplissement des mesures prises et la loi du 9 juillet 2020 a organisé un régime de sortie de cet état d'urgence. <br/>
      5. Une nouvelle progression de l'épidémie a conduit le Président de la République à prendre, sur le fondement des articles L. 3131-12 et L. 3131-13 du code de la santé publique, le décret du 14 octobre 2020 déclarant l'état d'urgence à compter du 17 octobre sur l'ensemble du territoire national. Le 29 octobre 2020, le Premier ministre a pris, sur le fondement de l'article L. 3131-15 du code de la santé publique, un décret prescrivant les mesures générales nécessaires pour faire face à l'épidémie de covid-19 dans le cadre de l'état d'urgence sanitaire. Le législateur, par l'article 1er de la loi du 14 novembre 2020 autorisant la prorogation de l'état d'urgence sanitaire et portant diverses mesures de gestion de la crise sanitaire, a prorogé cet état d'urgence sanitaire jusqu'au 16 février 2021 inclus. <br/>
      6. Dans ce cadre, l'ordonnance en litige, en date du 18 novembre 2020, prise sur le fondement de l'habilitation prévue par les dispositions combinées de l'article 11 de la loi du 23 mars 2020 et de l'article 10 de la loi du 14 novembre 2020, a prévu diverses adaptations des règles applicables aux juridictions de l'ordre administratif.<br/>
Sur la demande en référés : <br/>
      7. Les requérants, sur le fondement de l'article L. 521-1 du code de justice administrative, demandent au juge des référés du Conseil d'Etat de suspendre l'exécution de l'ordonnance du 18 novembre 2020. Ils soutiennent que ses dispositions sont entachées d'un doute sérieux quant à leur légalité dès lors que les mesures mises en place sont disproportionnées à l'objectif de protection de la santé publique et qu'elles méconnaissent le droit à un procès équitable ainsi que le droit au recours effectif. <br/>
      8. Eu égard aux moyens qu'elles soulèvent, les requêtes formées par le syndicat des avocats de France, le Conseil national des barreaux et les autres requérants doivent être regardées comme tendant seulement à la suspension des articles 2 et 3 de l'ordonnance contestée. <br/>
      En ce qui concerne l'article 2 de l'ordonnance : <br/>
      9. L'article 2 de l'ordonnance en cause permet de recourir à des moyens de télécommunication audiovisuelle pour la tenue de certaines audiences des juridictions de l'ordre administratif. Ainsi, les dispositions du I de cet article prévoient que le président de la formation de jugement peut, par une décision non susceptible de recours, décider que l'audience se tiendra en utilisant un moyen de télécommunication audiovisuelle permettant de s'assurer de l'identité des personnes y participant et garantissant la qualité de la transmission et la confidentialité des échanges entre les parties et leurs avocats, le deuxième alinéa ajoutant qu'en cas d'impossibilité technique ou matérielle de recourir à un tel moyen, le juge peut, à leur demande, décider d'entendre les parties et leurs avocats par tout moyen de communication électronique, y compris téléphonique, permettant de s'assurer de leur identité et de garantir la qualité de la transmission et la confidentialité des échanges. En outre, les dispositions du II du même article 2 de la même ordonnance prévoient que, dans les mêmes cas, le président de la formation de jugement peut autoriser des membres de la juridiction à participer à l'audience depuis un lieu distinct de la salle d'audience en utilisant un moyen de télécommunication audiovisuelle permettant de s'assurer de leur identité et garantissant la qualité de la transmission. Enfin, elles permettent au président de la juridiction de tenir lui-même ou d'autoriser un magistrat statuant seul à tenir une audience par un moyen de télécommunication audiovisuelle depuis un lieu distinct de la salle d'audience. Le greffier est, dans tous les cas, présent dans la salle d'audience et dresse le procès-verbal des opérations. <br/>
      10. Il résulte de l'instruction, que ces dispositions, applicables pour un temps limité, visent, dans le contexte sanitaire particulier résultant de l'épidémie de covid-19, à assurer une continuité de l'activité des juridictions administratives en permettant aux juridictions de l'ordre administratif de s'adapter aux contraintes qu'exige la lutte contre l'épidémie de covid-19, qui impose de limiter les occasions de contacts entre les personnes. Elles se bornent à offrir une faculté au président de la formation de jugement, auquel il appartient de n'y recourir que pour autant que certaines parties ou leurs conseils ou encore certains membres de la formation de jugement ou le rapporteur public sont dans l'incapacité, pour des motifs liés à la crise sanitaire, d'être physiquement présents dans la salle de l'audience et que la nature et les enjeux de l'affaire n'y font pas obstacle. Le recours à des moyens téléphoniques n'est quant à lui envisageable qu'en cas d'impossibilité technique ou matérielle de recourir à des moyens de télécommunication audiovisuelle et uniquement à la demande des parties elles-mêmes. Lorsqu'il décide de recourir à ces mesures, il incombe au juge de s'assurer que l'audience se déroule dans des conditions propres à satisfaire les exigences du caractère contradictoire de la procédure et le respect des droits de la défense, notamment en veillant à ce que le moyen de communication utilisé permet de certifier l'identité des personnes et garantit la qualité de la transmission. Il doit également s'assurer du bon déroulement des échanges entre les parties, du respect des droits de la défense et du caractère contradictoire des débats, un procès-verbal des opérations effectuées devant être dressé par le greffe. Le respect des droits de la défense implique, en particulier, que le dispositif retenu permette d'assurer la confidentialité des échanges entre les parties et leurs avocats. Enfin, il incombe aux juridictions concernées de s'assurer que les moyens de télécommunication utilisés par les membres de la formation de jugement garantissent effectivement le secret du délibéré. <br/>
En ce qui concerne l'article 3 de l'ordonnance : <br/>
      11. L'article 3 de l'ordonnance contestée prévoit que, outre les cas prévus à l'article L. 522-3 du code de justice administrative, il peut être statué sans audience, par ordonnance motivée, sur les requêtes présentées en référé. Le juge des référés doit, lorsqu'il y a recours, en informer les parties et fixer la date à partir de laquelle l'instruction sera close. Cette procédure donne lieu à une ordonnance motivée, susceptible d'appel lorsqu'elle est prise sur le fondement de l'article L. 521-2 du code de justice et que les conditions prévues à l'article L. 522-3 du code de justice administrative ne sont pas remplies. <br/>
      12. Si ces dispositions étendent, à titre temporaire, le champ des affaires pouvant être jugées sans audience, elles ne sont susceptibles de s'appliquer qu'aux affaires de référé, pour lesquelles l'article L. 511-1 du code de justice administrative prévoit que ne sont prises que des mesures qui présentent un caractère provisoire, lorsque le juge des référés estime que la nature et les enjeux de l'affaire n'y font pas obstacle. En outre, elles ne dérogent pas au principe du caractère contradictoire de la procédure. Enfin, dans le contexte particulier résultant de l'épidémie de covid-19, imposant de limiter les occasions de contacts entre les personnes, elles contribuent au jugement à bref délai de ces affaires, qui exigent une célérité particulière. <br/>
      13. Il résulte de tout ce qui précède que les moyens tirés du défaut de proportionnalité des mesures en cause, de l'atteinte au droit à un procès équitable et au droit au recours effectif ne sont pas, en l'état de l'instruction eu égard à la situation sanitaire actuelle, de nature à créer un doute sérieux quant à la légalité des dispositions de l'article 2 du décret contesté.<br/>
      14. Par suite, sans qu'il soit besoin de statuer sur l'urgence, les conclusions tendant à ce que soit ordonnée leur suspension doivent être rejetées, y compris les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
