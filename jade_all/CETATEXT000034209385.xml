<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034209385</ID>
<ANCIEN_ID>JG_L_2017_03_000000397152</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/20/93/CETATEXT000034209385.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère - 6ème chambres réunies, 17/03/2017, 397152, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-03-17</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>397152</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère - 6ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BOUTET-HOURDEAUX</AVOCATS>
<RAPPORTEUR>Mme Dorothée Pradines</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Rémi Decout-Paolini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:397152.20170317</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 19 février 2016, 18 mai 2016 et 1er février 2017 au secrétariat du contentieux du Conseil d'Etat, la Fédération française des sociétés d'assurances (FFSA) demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler pour excès de pouvoir l'arrêté du 11 décembre 2015 du ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social portant extension d'accords conclus dans le cadre de la convention collective nationale des vins, cidres, jus de fruits, sirops, spiritueux et liqueurs de France (n°493) ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 7 500 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de la sécurité sociale ;<br/>
              - le code du travail ;<br/>
              - la loi n° 2013-1203 du 23 décembre 2013 ;<br/>
              - le décret n° 2014-1498 du 11 décembre 2014 ;<br/>
              - le décret n° 2015-13 du 8 janvier 2015 ;<br/>
              - le décret n° 2017-162 du 9 février 2017 ; <br/>
              - le code de justice administrative, notamment son article R. 771-2 ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Dorothée Pradines, auditeur,  <br/>
<br/>
              - les conclusions de M. Rémi Decout-Paolini, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Boutet-Hourdeaux, avocat du Conseil national des industries et commerces en gros des vins, cidres, spiritueux, sirops, jus de fruits et boissons diverses.<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. L'article L. 911-1 du code de la sécurité sociale prévoit qu'à moins qu'elles ne soient instituées par des dispositions législatives ou réglementaires, les garanties collectives dont bénéficient les salariés, anciens salariés et ayants droit en complément de celles qui résultent de l'organisation de la sécurité sociale sont déterminées, notamment, par voie de conventions ou d'accords collectifs. Le I de l'article L. 912-1 du même code, dans sa rédaction issue de la loi du 23 décembre 2013, dispose que : " Les accords professionnels ou interprofessionnels mentionnés à l'article L. 911-1 peuvent, dans des conditions fixées par décret en Conseil d'Etat, prévoir l'institution de garanties collectives présentant un degré élevé de solidarité et comprenant à ce titre des prestations à caractère non directement contributif, pouvant notamment prendre la forme d'une prise en charge partielle ou totale de la cotisation pour certains salariés ou anciens salariés, d'une politique de prévention ou de prestations d'action sociale. / Dans ce cas, les accords peuvent organiser la couverture des risques concernés en recommandant un ou plusieurs organismes mentionnés à l'article 1er de la loi n° 89-1009 du 31 décembre 1989 renforçant les garanties offertes aux personnes assurées contre certains risques ou une ou plusieurs institutions mentionnées à l'article L. 370-1 du code des assurances (...) ", sous réserve du respect des conditions de mise en concurrence et d'égalité définies au II du même article.<br/>
<br/>
              2. En application du premier alinéa de l'article L. 2261-15 du code du travail, auquel renvoie l'article L. 911-3 du code de la sécurité sociale et aux termes duquel : " Les stipulations d'une convention de branche ou d'un accord professionnel ou interprofessionnel (...) peuvent être rendues obligatoires pour tous les salariés et employeurs compris dans le champ d'application de cette convention ou de cet accord, par arrêté du ministre chargé du travail, après avis motivé de la Commission nationale de la négociation collective ", le ministre chargé du travail a, par un arrêté du 11 décembre 2015, étendu les stipulations de l'accord du 1er avril 2015 instituant un régime complémentaire en matière de frais de santé, conclu dans le cadre de la convention collective nationale des vins, cidres, jus de fruits, sirops, spiritueux et liqueurs de France. L'accord ainsi étendu procède notamment à la recommandation des institutions de prévoyance Klesia Prévoyance et Malakoff Médéric Prévoyance pour la couverture du régime complémentaire en matière de frais de santé et pour procéder, y compris auprès des entreprises non adhérentes au régime conventionnel, à l'appel des cotisations relatives au fonds d'action sociale de la branche qu'il institue. La Fédération française des sociétés d'assurance (FFSA) demande au Conseil d'Etat d'annuler pour excès de pouvoir cet arrêté d'extension.<br/>
<br/>
              Sur les fins de non-recevoir opposées en défense :<br/>
<br/>
              3. La FFSA est une organisation professionnelle dont l'objet, défini à l'article 3 de ses statuts, est notamment de représenter et de défendre les intérêts des entreprises ou organismes du secteur de l'assurance. Elle a ainsi pour objet la défense des intérêts des entreprises régies par le code des assurances, qui sont concurrentes des organismes recommandés par l'accord faisant l'objet de l'arrêté d'extension pour la mise en oeuvre du régime de complémentaire frais de santé et proposent des prestations concurrentes de celles couvertes par le fonds d'action sociale de la branche. Dès lors, le CNVS et le ministre chargé du travail ne sont pas fondés à soutenir que la fédération requérante ne justifie pas d'un intérêt lui donnant qualité pour demander l'annulation de l'arrêté qu'elle attaque.<br/>
<br/>
              Sur la légalité externe de l'arrêté attaqué :<br/>
<br/>
              4. Le II de l'article L. 912-1 du code de la sécurité sociale prévoit que la recommandation d'un organisme ou d'une institution, permise par le I du même article cité au point 1, " doit être précédée d'une procédure de mise en concurrence des organismes ou institutions concernés, dans des conditions de transparence, d'impartialité et d'égalité de traitement entre candidats et selon des modalités prévues par décret ". L'article D. 912-13 du code de la sécurité sociale, dans sa rédaction issue du décret 8 janvier 2015, dispose que : " Lorsque les organisations syndicales de salariés et les organisations professionnelles d'employeurs demandent l'extension d'une convention ou d'un accord collectif comportant une clause de recommandation (...), ils joignent à leur demande les pièces afférentes à la procédure de mise en concurrence dont la liste est fixée par arrêté du ministre chargé de la sécurité sociale, du ministre chargé du budget et du ministre chargé du travail. " Les dispositions de l'article D. 912-13 du code de la sécurité sociale ont pour seul objet de permettre au ministre chargé du travail, saisi d'une demande d'extension d'un accord comportant une clause de recommandation, de vérifier que celle-ci a été adoptée au terme d'une procédure de mise en concurrence régulière, en l'absence de laquelle il ne peut légalement l'étendre. Si les parties à l'accord doivent pouvoir justifier de l'organisation d'une procédure de mise en concurrence conforme au II de l'article L. 912-1 du code de la sécurité sociale et si le ministre est en droit de refuser l'extension d'un accord lorsque les pièces mentionnées à l'article D. 912-13 ne sont pas jointes à la demande d'extension, la seule circonstance que le ministre se serait prononcé sans disposer de ces pièces n'est pas, par elle-même, de nature à entacher d'illégalité l'arrêté pris sur cette demande. Le moyen tiré par la requérante de ce que l'arrêté attaqué serait illégal de ce seul fait ne peut dès lors qu'être écarté.<br/>
<br/>
              Sur la légalité interne de l'arrêté attaqué :<br/>
<br/>
              5. En premier lieu, si l'article 1er de l'accord stipule que : " Les entreprises ayant instauré un régime de frais de soins de santé obligatoire avant l'entrée en vigueur du présent accord ont la possibilité de ne pas adhérer au régime recommandé par la branche dès lors que les garanties assurées couvrent les mêmes prestations à un niveau supérieur ou égal à celles définies dans le présent accord " et si l'article 2 stipule que : " La négociation a été menée avec la volonté d'instaurer un régime obligatoire, collectif, responsable et solidaire ", le dernier alinéa de l'article 1er de l'accord précise que : " En tout état de cause, le choix de l'organisme assureur incombe à l'employeur, qui recueille préalablement l'avis des représentants du personnel lorsqu'ils existent. L'organisme choisi par l'entreprise assure la collecte des cotisations et le versement des prestations correspondant au moins aux dispositions prévues par le présent accord ". Il résulte dès lors clairement de l'ensemble de ces stipulations que seul le niveau minimum de couverture complémentaire des frais de santé du régime instauré par l'accord étendu revêt un caractère obligatoire, et non l'adhésion à l'un des organismes recommandés. Par suite, le moyen tiré de ce que le ministre n'aurait pu légalement étendre cet accord au motif qu'il créerait une obligation d'adhésion à l'un des organismes recommandés, en méconnaissance des dispositions du I de l'article L. 912-1 du code de la sécurité sociale, ne soulève pas une contestation sérieuse et doit être écarté.<br/>
<br/>
              6. En deuxième lieu, le III de l'article L. 912-1 du code de la sécurité sociale prévoit que : " Les accords mentionnés au I comportent une clause fixant dans quelles conditions et selon quelle périodicité, qui ne peut excéder cinq ans, les modalités d'organisation de la recommandation sont réexaminées ". L'article 3.1 de l'accord stipule que : " Conformément à l'article L. 912-1 du code de la sécurité sociale, les partenaires sociaux réexamineront les modalités d'organisation de la recommandation dans un délai qui ne pourra excéder 5 ans à compter de l'entrée en vigueur du présent accord. À cette fin, les partenaires sociaux se réuniront au moins 6 mois avant le terme de la recommandation ". Il résulte clairement de cette stipulation que le moyen de la requérante selon lequel l'accord ne comporterait pas une clause de réexamen conforme à celle que prescrit le III de l'article L. 912-1 du code de la sécurité sociale manque en fait. Par suite, le moyen tiré de ce que le ministre n'aurait pu, faute d'une telle clause, légalement étendre cet accord, qui ne soulève pas de contestation sérieuse, doit être écarté.<br/>
<br/>
              7. En troisième lieu, le IV de l'article L. 912-1 du code de la sécurité sociale dispose que : " Les accords mentionnés au I peuvent prévoir que certaines des prestations nécessitant la prise en compte d'éléments relatifs à la situation des salariés ou sans lien direct avec le contrat de travail les liant à leur employeur sont financées et gérées de façon mutualisée, selon des modalités fixées par décret en Conseil d'Etat, pour l'ensemble des entreprises entrant dans leur champ d'application ". <br/>
<br/>
              8. Le décret du 11 décembre 2014 relatif aux garanties collectives présentant le degré élevé de solidarité mentionné à l'article L. 912-1 du code de la sécurité sociale a, d'une part, inséré dans ce code, l'article R. 912-1, aux termes duquel : " Les accords professionnels ou interprofessionnels mentionnés au premier alinéa du I de l'article L. 912-1 prévoient la part de la prime ou de la cotisation acquittée qui sera affectée au financement de prestations mentionnées aux 1°, 2° et 3° de l'article R. 912-2 ainsi que, le cas échéant, à d'autres actions équivalentes procédant d'un objectif de solidarité qu'ils stipulent. / Sont regardés comme présentant un degré élevé de solidarité au sens des dispositions du premier alinéa de l'article L. 912-1 les accords pour lesquels la part de ce financement est au moins égale à 2 % de la prime ou de la cotisation ". Il a inséré dans ce même code, d'autre part, l'article R. 912-2, aux termes duquel : " Les accords professionnels ou interprofessionnels mentionnés au premier alinéa du I de l'article L. 912-1 peuvent prévoir, en vue de comporter des garanties présentant un degré élevé de solidarité au sens des dispositions de cet alinéa : / 1° Une prise en charge, totale ou partielle, de la cotisation de tout ou partie des salariés ou apprentis pouvant bénéficier des dispenses d'adhésion prévues au b du 2° de l'article R. 242-1-6, ainsi que de la cotisation de tout ou partie des salariés, apprentis ou anciens salariés dont la cotisation représente au moins 10 % de leurs revenus bruts ; / 2° Le financement d'actions de prévention concernant les risques professionnels ou d'autres objectifs de la politique de santé, relatifs notamment aux comportements en matière de consommation médicale. (...) / 3° La prise en charge de prestations d'action sociale, comprenant notamment : / a) Soit à titre individuel : l'attribution, lorsque la situation matérielle des intéressés le justifie, d'aides et de secours individuels aux salariés, anciens salarié et ayants droit ; / b) Soit à titre collectif, pour les salariés, les anciens salariés ou leurs ayants droit : l'attribution suivant des critères définis par l'accord d'aides leur permettant de faire face à la perte d'autonomie, y compris au titre des dépenses résultant de l'hébergement d'un adulte handicapé dans un établissement médico-social, aux dépenses liées à la prise en charge d'un enfant handicapé ou à celles qui sont nécessaires au soutien apporté à des aidants familiaux. / Les orientations des actions de prévention ainsi que les règles de fonctionnement et les modalités d'attribution des prestations d'action sociale sont déterminées par la commission paritaire de branche, en prenant en compte, le cas échéant, les objectifs d'amélioration de la santé définis dans le cadre de la politique de santé à la mise en oeuvre desquels ces orientations contribuent dans le champ professionnel ou interprofessionnel qu'elles couvrent. / La commission paritaire de branche contrôle la mise en oeuvre de ces orientations par les organismes auprès desquels les entreprises organisent la couverture de leurs salariés ".<br/>
<br/>
              9. Il résulte des dispositions mêmes de ce décret qu'elles ont été édictées, de façon d'ailleurs conforme à son titre, pour préciser le " degré élevé de solidarité " que doivent présenter les garanties collectives instituées en vertu du I de l'article L. 912-1 du code de la sécurité sociale et qu'elles ne sauraient tenir lieu des dispositions également renvoyées à un décret en Conseil d'Etat pour l'application du IV de ce même article afin de préciser les modalités selon lesquelles les accords conclus au titre du I pourraient prévoir le financement et la gestion mutualisés de certaines prestations. Ces modalités ont d'ailleurs été fixées par le décret du 9 février 2017 relatif au financement et à la gestion de façon mutualisée des prestations mentionnées au IV de l'article L. 912-1 du code de la sécurité sociale.<br/>
<br/>
              10. Il suit de là que l'article R. 912-2 du code de la sécurité sociale n'est pas applicable à la gestion et au financement mutualisés de certaines prestations de solidarité instaurés par l'accord et que la fédération requérante ne peut utilement invoquer leur méconnaissance par les clauses de l'accord étendu relatives à cette gestion et à ce financement mutualisés.  <br/>
<br/>
              11. Toutefois, il est constant que ni à la date de la conclusion de l'accord litigieux, ni même à celle de l'arrêté l'étendant, aucun décret n'était intervenu pour prévoir les modalités de gestion et de financement mutualisés de certaines prestations, qui étaient nécessaires à l'entrée en vigueur des dispositions du IV de l'article L. 912-1 du code de la sécurité sociale. Par suite, en l'absence d'un tel décret, ces dispositions n'étaient pas entrées en vigueur.<br/>
<br/>
              12. Dès lors, l'appréciation du bien-fondé des moyens tirés de ce que l'arrêté d'extension attaqué ne pouvait dans ces conditions légalement étendre les clauses de l'accord en cause instituant un fonds de solidarité de branche en prévoyant les prestations qui seraient financées et gérées de façon mutualisée et en fixant lui-même les modalités de leur financement, par un prélèvement à la charge de toutes les entreprises, et de leur gestion mutualisés dépend notamment de la question de savoir si l'exercice par les parties à l'accord du 1er avril 2015 de leur liberté contractuelle leur permettait, en l'absence de disposition législative, de prévoir la mutualisation du financement et de la gestion de certaines prestations et notamment leur financement par un prélèvement de 2 % sur les cotisations versées à l'organisme recommandé, ou un prélèvement équivalent à cette somme exigible auprès des entreprises qui n'adhèrent pas à l'organisme recommandé. Cette question présente à juger une difficulté sérieuse, qu'il n'appartient qu'au juge judiciaire de trancher et qui ne peut être résolue au vu d'une jurisprudence établie.<br/>
<br/>
              13. Par suite, il y a lieu pour le Conseil d'Etat de surseoir à statuer sur la requête de la Fédération française des sociétés d'assurance en ce qu'elle tend à l'annulation de l'arrêté en tant qu'il étend les stipulations de l'article 2 de l'accord du 1er avril 2015 instituant un régime complémentaire en matière de frais de santé relatives au fonds d'action sociale de la branche, qui sont divisibles des autres clauses de l'accord, jusqu'à ce que la juridiction compétente se soit prononcée sur cette question préjudicielle. Il y a lieu, par suite, de surseoir également à statuer sur les conclusions des parties présentées au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Il est sursis à statuer sur les conclusions de la requête de la FFSA tendant à l'annulation de l'arrêté du 11 décembre 2015 en tant qu'il étend les stipulations de l'article 2 de l'accord du 1er avril 2015 instituant un régime complémentaire en matière de frais de santé relatives au fonds d'action sociale de la branche, jusqu'à ce que le tribunal de grande instance de Paris se soit prononcé sur le point de savoir si l'exercice par les parties à l'accord de leur liberté contractuelle leur permettait, en l'absence de disposition législative, de prévoir la mutualisation du financement et de la gestion de certaines prestations et notamment leur financement par un prélèvement de 2 % sur les cotisations versées à l'organisme recommandé, ou un prélèvement équivalent à cette somme exigible auprès des entreprises qui n'adhèrent pas à l'organisme recommandé.<br/>
Article 2 : Il est sursis à statuer sur les conclusions des parties  présentées au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : Le surplus des conclusions de la requête de la FFSA est rejeté.<br/>
Article 4 : La présente décision sera notifiée à la Fédération française des sociétés d'assurance, au Conseil national des industries et commerces en gros des vins, cidres, spiritueux, sirops, jus de fruits et boissons diverses, à la ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social et au président du tribunal de grande instance de Paris.<br/>
Copie en sera adressée à la CFE-CGC, à la FGTA FO, à la FNAF CGT, à Klesia Prévoyance et à Malakoff Médéric Prévoyance.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
