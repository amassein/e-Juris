<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043897176</ID>
<ANCIEN_ID>JG_L_2021_08_000000448466</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/89/71/CETATEXT000043897176.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème chambre, 03/08/2021, 448466, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-08-03</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>448466</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BAUER-VIOLAS, FESCHOTTE-DESBOIS, SEBAGH ; SCP RICARD, BENDEL-VASSEUR, GHNASSIA ; SCP FABIANI, LUC-THALER, PINATEL</AVOCATS>
<RAPPORTEUR>Mme Carine Chevrier</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Stéphane Hoynck</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:448466.20210803</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
               M. F... B... a demandé au juge des référés du tribunal administratif de Montpellier, statuant sur le fondement de l'article L. 521-1 du code de justice administrative, d'ordonner la suspension de l'exécution du permis de construire délivré le 30 septembre 2020 par le maire de Ventenac-Cabardès (Aude) à M. E.... Par une ordonnance n° 2005504 du 24 décembre 2020, le juge des référés du tribunal administratif de Montpellier a rejeté sa demande.<br/>
<br/>
               Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 7 janvier et 22 janvier 2021 au secrétariat du contentieux du Conseil d'Etat, M. B... demande au Conseil d'Etat :<br/>
<br/>
               1°) d'annuler cette ordonnance ;<br/>
<br/>
               2°) statuant en référé, de faire droit à sa demande de suspension du permis de construire ;<br/>
<br/>
               3°) de mettre à la charge de la commune de Ventenac-Cabardès et de M. E..., chacun, la somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
               Vu les autres pièces du dossier ;<br/>
<br/>
               Vu : <br/>
               - le code de l'urbanisme ;<br/>
               - le code de justice administrative ;<br/>
<br/>
               Après avoir entendu en séance publique :<br/>
<br/>
               - le rapport de Mme A... C..., conseillère d'Etat, <br/>
<br/>
               - les conclusions de M. Stéphane Hoynck, rapporteur public ;<br/>
<br/>
               La parole ayant été donnée, après les conclusions, à la SCP Bauer-Violas, Feschotte-Desbois, Sebagh, avocat de M. B..., à la SCP Fabiani, Luc-Thaler, Pinatel, avocat de la commune de Ventenac-Cabardès et à la SCP Ricard, Bendel-Vasseur, Ghnassia, avocat de M. E... ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
               Considérant ce qui suit :<br/>
<br/>
               1. Aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision ". Aux termes de l'article L. 600-3 du code de l'urbanisme : " Un recours dirigé contre une décision de non-opposition à déclaration préalable ou contre un permis de construire, d'aménager ou de démolir ne peut être assorti d'une requête en référé suspension que jusqu'à l'expiration du délai fixé pour la cristallisation des moyens soulevés devant le juge saisi en premier ressort. La condition d'urgence prévue à l'article L. 521-1 du code de justice administrative est présumée satisfaite ".<br/>
<br/>
               2. Il ressort des pièces du dossier soumis au juge des référés que, par un arrêté du 30 septembre 2020, le maire de Ventenac-Cabardès a accordé à M. E... un permis de construire pour la réalisation de deux hangars agricoles à toiture photovoltaïque ainsi que des locaux onduleurs sur un terrain situé en zone A du règlement du plan local d'urbanisme de la commune. Par une ordonnance du 24 décembre 2020, le juge des référés du tribunal administratif de Montpellier a rejeté la demande présentée par M. B..., voisin immédiat, tendant à la suspension de ce permis de construire sur le fondement de l'article L. 521-1 du code de justice administrative. M. B... se pourvoit en cassation contre cette ordonnance. <br/>
<br/>
               3. Aux termes de l'article A4 du règlement du plan local d'urbanisme de la commune de Ventenac-Cabardès : " Toute construction doit soit être raccordée au réseau public d'eau potable, soit posséder une desserte autonome réglementaire ". Il ressort des pièces du dossier soumis au juge des référés que le projet de réalisation de hangars autorisé par le permis délivré à M. E..., qui constituait bien une " construction " au sens de cet article, ne disposait ni d'un raccordement au réseau d'eau, ni d'une desserte autonome réglementaire. Il s'ensuit qu'en jugeant que le moyen tiré de la méconnaissance de l'article A4 du règlement du plan local d'urbanisme n'était pas de nature à créer un doute sérieux sur la légalité du permis, le juge des référés a commis une dénaturation.<br/>
<br/>
               4. Il résulte de ce qui précède que M. B... est fondé à demander l'annulation de l'ordonnance qu'il attaque.<br/>
<br/>
               5. Il y a eu lieu, dans les circonstances de l'espèce, de statuer sur la demande de suspension en application des dispositions de l'article L. 821-2 du code de justice administrative.<br/>
<br/>
               6. D'une part, il ressort des pièces du dossier qu'aucune circonstance particulière de l'espèce ne justifie d'écarter la présomption résultant des dispositions de l'article L. 600-3 du code de l'urbanisme. La condition d'urgence prévue à l'article L. 521-1 du code de justice administrative doit, par suite, être regardée comme remplie.<br/>
<br/>
               7. D'autre part, pour demander la suspension de l'exécution du permis de construire délivré à M. E..., M. B... soutient que ce permis méconnaît les articles A3.1, A4 et A11 du règlement de plan local d'urbanisme de la commune de Ventenac-Cabardès et qu'il est entaché d'une fraude en ce qui concerne le raccordement au réseau d'eau. <br/>
<br/>
               8. En l'état de l'instruction, le moyen tiré de ce que le permis de construire méconnaît l'article A4 du règlement du plan local d'urbanisme paraît, pour les motifs indiqués au point 3, de nature à faire naître un doute sérieux quant à la légalité de l'arrêté en litige. <br/>
<br/>
               9. En revanche, pour l'application des dispositions de l'article L. 600-4-1 du code de l'urbanisme, les moyens tirés de ce que la décision contestée méconnaîtrait les dispositions des articles A3.1 et A11 du règlement de plan local d'urbanisme ou qu'elle aurait été délivrée par fraude ne sont pas, en l'état de l'instruction, de nature à faire naître un tel doute. <br/>
<br/>
               10. Il résulte de ce qui précède que le requérant est fondé à demander la suspension de l'exécution de l'arrêté du maire de Ventenac-Cabardès du 30 septembre 2020.<br/>
<br/>
               11. La commune de Ventenac-Cabardès et M. E... verseront, chacun, la somme de 1 000 euros à M. B... au titre des dispositions de l'article L. 761-1 du code de justice administrative. Ces mêmes dispositions font obstacle à ce qu'il soit fait droit aux conclusions présentées, à ce titre, par la commune de Ventenac-Cabardès et par M. E.... <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'ordonnance n° 2005504 du juge des référés du tribunal administratif de Montpellier du 24 décembre 2020 est annulée. <br/>
Article 2 : L'exécution du permis de construire délivré le 30 septembre 2020 par le maire de Ventenac-Cabardès à M. E... est suspendue. <br/>
Article 3 : La commune de Ventenac-Cabardès et M. E... verseront, chacun, la somme de 1 000 euros à M. B... au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Les conclusions de la commune de Ventenac-Cabardès et de M. E... au titre de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
Article 5 : La présente décision sera notifiée à M. F... B..., à la commune de Ventenac-Cabardès et à M. D... E....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
