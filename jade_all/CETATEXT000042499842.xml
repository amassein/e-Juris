<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042499842</ID>
<ANCIEN_ID>JG_L_2020_11_000000428744</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/49/98/CETATEXT000042499842.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème - 1ère chambres réunies, 04/11/2020, 428744, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-11-04</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>428744</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème - 1ère chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP NICOLAY, DE LANOUVELLE, HANNOTIN</AVOCATS>
<RAPPORTEUR>Mme Yaël Treille</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Dieu</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:428744.20201104</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société Financière Mag, venant aux droits de la société Novopac, a demandé au tribunal administratif de Lyon de condamner l'Etat à l'indemniser des préjudices qu'elle estime avoir subis en raison de l'illégalité de la décision du 18 décembre 2009 par laquelle l'inspectrice du travail de la 13ème section du Rhône l'a autorisée à licencier M. A... B.... Par un jugement n° 1505531 du 13 juin 2017, le tribunal administratif a condamné l'Etat à lui verser une somme de 10 015,21 euros, assortie des intérêts au taux légal et de la capitalisation des intérêts, et rejeté le surplus de ses conclusions.<br/>
<br/>
              Par un arrêt n° 17LY03028 du 7 janvier 2019, la cour administrative d'appel de Lyon a rejeté l'appel formé contre ce jugement par la société Financière Mag.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 11 mars et 12 juin 2019 au secrétariat du contentieux du Conseil d'Etat, la société Financière Mag demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code du travail ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Yaël Treille, auditeur,  <br/>
<br/>
              - les conclusions de M. Frédéric Dieu, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Nicolaÿ, de Lanouvelle, Hannotin, avocat de la société Financière Mag ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que la société Novopac, aux droits de laquelle vient la société Financière Mag, a sollicité l'autorisation de licencier pour motif économique M. B..., salarié protégé. Par une décision du 18 décembre 2009, l'inspectrice du travail de la 13ème section du Rhône a autorisé ce licenciement. Toutefois par un arrêt du 6 juin 2013 devenu définitif, la cour administrative d'appel de Lyon a confirmé le jugement du 3 avril 2012 du tribunal administratif de Lyon ayant annulé cette décision, au motif que l'administration n'avait pas vérifié la réalité du motif économique au niveau du secteur d'activité dont relevait l'entreprise au sein du groupe auquel elle appartenait. La société Novopac, ayant, à la suite de cette annulation, été condamnée par le juge judiciaire à verser à M. B... une indemnité de 20 030,42 euros en application de l'article L. 2422-4 du code du travail, une indemnité de 22 000 euros en application de l'article L. 1235-3 du code du travail ainsi qu'une indemnité au titre du préjudice moral du salarié, a recherché la responsabilité de l'Etat afin d'obtenir réparation du préjudice subi par elle à raison du versement de ces indemnités, et demandé au tribunal administratif de Lyon de condamner l'État à lui verser la somme de 45 530,42 euros. Par un jugement du 13 juin 2017, le tribunal administratif de Lyon a condamné l'Etat à verser à la société Financière Mag une somme de 10 015,21 euros, assortie des intérêts au taux légal et capitalisation des intérêts, et rejeté le surplus de la demande de la société. La société Novopac se pourvoit en cassation contre l'arrêt du 7 janvier 2019 par lequel la cour administrative d'appel de Lyon a rejeté son appel contre ce jugement.  <br/>
<br/>
              2. En application des dispositions du code du travail, le licenciement d'un salarié protégé ne peut intervenir que sur autorisation de l'autorité administrative. L'illégalité de la décision autorisant un tel licenciement constitue une faute de nature à engager la responsabilité de la puissance publique à l'égard de l'employeur, pour autant qu'il en soit résulté pour celui-ci un préjudice direct et certain.  <br/>
<br/>
              Sur la responsabilité de l'Etat au titre du versement par l'employeur au salarié de l'indemnité prévue par l'article L. 2422-4 du code du travail :<br/>
<br/>
              3. Aux termes de l'article L. 2422-4 du code du travail : " Lorsque l'annulation d'une décision d'autorisation est devenue définitive, le salarié investi d'un des mandats mentionnés à l'article L. 2422-1 a droit au paiement d'une indemnité correspondant à la totalité du préjudice subi au cours de la période écoulée entre son licenciement et sa réintégration, s'il en a formulé la demande dans le délai de deux mois à compter de la notification de la décision./ L'indemnité correspond à la totalité du préjudice subi au cours de la période écoulée entre son licenciement et l'expiration du délai de deux mois s'il n'a pas demandé sa réintégration.(...) " .  4. En application des principes généraux de la responsabilité de la puissance publique, il peut le cas échéant être tenu compte, pour déterminer l'étendue de la responsabilité de l'Etat à l'égard de l'employeur à raison de la délivrance d'une autorisation de licenciement entachée d'illégalité, au titre du versement par l'employeur au salarié de l'indemnité prévue par l'article L. 2422-4 du code du travail, de la faute également commise par l'employeur en sollicitant la délivrance d'une telle autorisation. En l'espèce toutefois, la cour administrative d'appel de Lyon, en se fondant, pour juger que la société Novopac avait commis une faute de nature à exonérer l'Etat de la moitié de la responsabilité encourue, sur le seul fait que la société avait pris acte devant le conseil des prud'hommes que, selon son arrêt du 6 juin 2013, l'autorisation était illégale pour un motif de fond, et qu'elle n'entendait pas se pourvoir en cassation contre cet arrêt, la cour a commis une erreur de droit et, par suite, entaché son arrêt d'inexacte qualification juridique des faits.<br/>
<br/>
              Sur la responsabilité de l'Etat au titre du versement par l'employeur au salarié de l'indemnité prévue par l'article L. 1235-3 du code du travail :<br/>
<br/>
              4. Aux termes de l'article L. 1235-3 du code du travail, dans sa rédaction applicable au litige : " Si le licenciement d'un salarié survient pour une cause qui n'est pas réelle et sérieuse, le juge peut proposer la réintégration du salarié dans l'entreprise, avec maintien de ses avantages acquis. /Si l'une ou l'autre des parties refuse, le juge octroie une indemnité au salarié. Cette indemnité, à la charge de l'employeur, ne peut être inférieure aux salaires des six derniers mois. Elle est due sans préjudice, le cas échéant, de l'indemnité de licenciement prévue à l'article L. 1234-9. ". <br/>
<br/>
              5. En jugeant que le versement à M. B... de l'indemnité prévue par l'article L. 1235-3 du code du travail ne présentait pas de lien direct avec l'illégalité de l'autorisation administrative au seul motif que la condamnation trouvait son fondement dans un jugement d'un conseil de prud'hommes constatant l'absence de cause réelle et sérieuse de ce licenciement, sans rechercher notamment si le conseil des prud'hommes avait déduit cette absence de cause réelle et sérieuse des motifs de l'annulation de l'autorisation administrative par le juge administratif, la cour administrative d'appel de Lyon a commis une erreur de droit. <br/>
<br/>
              6. Il résulte de tout ce qui précède que, sans qu'il soit besoin d'examiner les autres moyens de son pourvoi, la société Financière Mag est fondée à demander l'annulation de l'arrêt qu'elle attaque. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat une somme de 3 000 euros à verser à la société Financière Mag au titre de l'article <br/>
L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt n° 17LY03028 de la cour administrative d'appel de Lyon du 7 janvier 2019 est annulé. <br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Lyon.<br/>
Article 3 : L'Etat versera une somme de 3 000 euros à la société Novopac au titre de l'article <br/>
L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à la société Financière Mag et à la ministre du travail, de l'emploi et de l'insertion.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
