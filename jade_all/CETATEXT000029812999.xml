<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029812999</ID>
<ANCIEN_ID>JG_L_2014_11_000000384324</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/81/29/CETATEXT000029812999.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème et 4ème sous-sections réunies, 28/11/2014, 384324, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-11-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>384324</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème et 4ème sous-sections réunies</FORMATION>
<TYPE_REC>Autres</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Manon Perrière</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:384324.20141128</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu l'ordonnance n°14000602 du 27 août 2014, enregistrée le 9 septembre 2014 au secrétariat du contentieux du Conseil d'Etat, par laquelle le tribunal administratif de Saint-Denis, avant de statuer sur la demande du syndicat réunionnais des exploitants de stations-service tendant à l'annulation de l'article 8 de l'arrêté du préfet de la Réunion du 23 avril 2014 règlementant les ventes de boissons alcoolisées dans les stations-service, a décidé, en application de l'article 23-2 de l'ordonnance n° 58-1067 du 7 novembre 1958, de transmettre au Conseil d'Etat la question de la conformité aux droits et libertés garantis par la Constitution des dispositions des 4ème et 5ème alinéas de l'article L. 3322-9 du code de la santé publique ;<br/>
<br/>
              Vu le mémoire, enregistré le 16 juin 2014 au greffe du tribunal administratif de Saint-Denis, présenté par le syndicat réunionnais des exploitants de stations-service, qui demande que soit renvoyée au Conseil constitutionnel la question de la conformité aux droits et libertés garantis par la Constitution des dispositions des 4ème et 5ème alinéas de l'article L. 3322-9 du code de la santé publique ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la Constitution, notamment son Préambule et son article 61-1 ; <br/>
<br/>
              Vu l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
<br/>
              Vu le code de la santé publique, notamment son article L. 3322-9 ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Manon Perrière, auditeur,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il résulte des dispositions de l'article 23-4 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel que, lorsqu'une juridiction relevant du Conseil d'Etat a transmis à ce dernier, en application de l'article 23-2 de cette même ordonnance, la question de la conformité à la Constitution d'une disposition législative, le Conseil constitutionnel est saisi de cette question de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux ;<br/>
<br/>
              2. Considérant qu'aux termes des 4ème et 5ème alinéas de l'article L. 3322-9 du code de la santé publique, dans leur rédaction issue de la loi du 21 juillet 2009  portant réforme de l'hôpital et relative aux patients, à la santé et aux territoires: " Il est interdit de vendre des boissons alcooliques à emporter, entre 18 heures et 8 heures, dans les points de vente de carburant. / Il est interdit de vendre des boissons alcooliques réfrigérées dans les points de vente de carburant " ;<br/>
<br/>
              3. Considérant que le syndicat réunionnais des exploitants de stations-service conteste la conformité aux droits et libertés garantis par la Constitution de ces dispositions ; qu'il soutient qu'en instituant les interdictions qu'elles prévoient, le législateur a méconnu le principe d'égalité et porté atteinte à la liberté d'entreprendre ;<br/>
<br/>
              4. Considérant que l'article 6 de la Déclaration des droits de l'homme et du citoyen de 1789 dispose que la loi " doit être la même pour tous, soit qu'elle protège, soit qu'elle punisse " ; que le principe d'égalité ne s'oppose ni à ce que le législateur règle de façon différente des situations différentes, ni à ce qu'il déroge à l'égalité pour des raisons d'intérêt général, pourvu que, dans l'un et l'autre cas, la différence de traitement qui en résulte soit en rapport direct avec l'objet de la loi qui l'établit ;<br/>
<br/>
              5. Considérant, par ailleurs, qu'il est loisible au législateur d'apporter à la liberté d'entreprendre, qui découle de l'article 4 de la Déclaration des droits de l'homme et du citoyen, des limitations justifiées par l'intérêt général, à la condition qu'il n'en résulte pas d'atteintes disproportionnées au regard de l'objectif poursuivi ; <br/>
<br/>
              6. Considérant qu'en interdisant de manière permanente la vente dans les points de vente de carburant des boissons alcooliques réfrigérées, qui sont normalement acquises en vue d'une consommation immédiate, et en y interdisant entre 18 heures et 8 heures la vente de toute boisson alcoolique, le législateur a entendu améliorer, notamment la nuit, la prévention des accidents liés à la consommation d'alcool par les conducteurs, laquelle constitue le premier facteur d'accidents de la route ; que ces mesures, édictées dans l'intérêt général, ne portent pas une atteinte disproportionnée à la liberté d'entreprendre ;  que la différence de traitement qui en résulte au détriment des exploitants de points de vente de carburant visant à la protection de la vie et de la santé des personnes, objectif de valeur constitutionnelle découlant du onzième alinéa du Préambule de la Constitution de 1946, et étant en rapport direct avec l'objet ainsi poursuivi, la mesure que le législateur a édictée dans l'intérêt général ne méconnaît pas le principe d'égalité ; <br/>
<br/>
              7. Considérant qu'il résulte de ce qui précède que la question prioritaire de constitutionnalité soulevée, qui n'est pas nouvelle, ne présente pas un caractère sérieux ; que, par suite, il n'y a pas lieu de la renvoyer au Conseil constitutionnel ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée par le syndicat réunionnais des exploitants de stations-service.<br/>
<br/>
Article 2 : La présente décision sera notifiée au syndicat réunionnais des exploitants de stations-service ainsi qu'au Premier ministre.<br/>
Copie en sera adressée pour information au Conseil constitutionnel, à la ministre des affaires sociales, de la santé et des droits des femmes ainsi qu'au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
