<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031502285</ID>
<ANCIEN_ID>JG_L_2015_11_000000381812</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/50/22/CETATEXT000031502285.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème SSJS, 20/11/2015, 381812, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-11-20</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>381812</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème SSJS</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MASSE-DESSEN, THOUVENIN, COUDRAY</AVOCATS>
<RAPPORTEUR>Mme Manon Perrière</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Laurence Marion</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2015:381812.20151120</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              M. A...B...a demandé au tribunal administratif de Versailles d'annuler pour excès de pouvoir l'arrêté du 7 mai 2012 du préfet des Hauts-de-Seine prononçant la suspension de son permis de conduire, la décision verbale du commissaire de police de Limay lui ordonnant de restituer ce permis et la décision implicite rejetant son recours gracieux formulé le 30 août 2012 tendant à la restitution du permis. Par un jugement n° 1206908 du 24 avril 2014, le tribunal administratif a rejeté sa demande. <br/>
<br/>
              Par une requête sommaire et un mémoire complémentaire, enregistrés les 25 juin et 25 septembre 2014 au secrétariat du contentieux du Conseil d'Etat, M. B...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
<br/>
              - le code de la route ;<br/>
<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Manon Perrière, auditeur,  <br/>
<br/>
              - les conclusions de Mme Laurence Marion, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Masse-Dessen, Thouvenin, Coudray, avocat de M. B...;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond qu'à la suite d'un accident de la circulation ayant entraîné le décès d'un piéton, le préfet des Hauts-de-Seine a pris le 7 mai 2012 un arrêté suspendant pour un an le permis de conduire de M. B... en application des dispositions de l'article L. 224-2 du code de la route ; que, le 30 mai 2012, le procureur de la République a classé sans suite la procédure pénale engagée contre l'intéressé ; que, le 22 juin 2012, les services préfectoraux lui ont restitué son permis de conduire ; que M. B...s'est rendu le 30 août 2012 au commissariat de police de Limay où il avait été convoqué et où son titre de conduite lui a été à nouveau retiré ; que son avocat a adressé le jour même au commissaire de police un courrier relatif à ce retrait ; que M. B... a demandé au tribunal administratif de Versailles d'annuler l'arrêté préfectoral du 7 mai 2012 suspendant son permis de conduire, la décision verbale de lui retirer à nouveau ce permis et la décision implicite de rejet résultant du silence gardé par l'administration à la suite du courrier du 30 août 2012 qui présentait, selon lui, le caractère d'un recours gracieux ; qu'il se pourvoit en cassation contre le jugement du 24 avril 2014 par lequel le tribunal administratif a rejeté l'ensemble de ces conclusions ; <br/>
<br/>
              En ce qui concerne l'arrêté préfectoral du 7 mai 2012 suspendant le permis de conduire de M.B... :<br/>
<br/>
              2. Considérant qu'aux termes du dernier alinéa de l'article L. 224-1 du code de la route : " En cas d'accident de la circulation ayant entraîné la mort d'une personne, les officiers et agents de police judiciaire retiennent également à titre conservatoire le permis de conduire du conducteur à l'égard duquel il existe une ou plusieurs raisons plausibles de le soupçonner d'avoir commis une infraction en matière de respect des vitesses maximales autorisées ou des règles de croisement, de dépassement, d'intersection et de priorités de passage " ; que l'article L. 224-2 du même code prévoit que le préfet peut, dans les soixante-douze heures de la rétention du permis de conduire, prononcer, dans certaines hypothèses, la suspension du permis de conduire ; qu'en vertu des deux derniers alinéas du même article, ces dispositions sont notamment applicables " lorsque le permis a été retenu à la suite d'un accident de la circulation ayant entraîné la mort d'une personne, en application du dernier alinéa de l'article L. 224-1, en cas de procès-verbal constatant que le conducteur a commis une infraction en matière de respect des vitesses maximales autorisées ou des règles de croisement, de dépassement, d'intersection et de priorités de passage ", la durée maximale de la suspension, normalement fixée à six mois, étant alors portée à un an ; qu'aux termes de l'article L. 224-9 du même code : " Quelle que soit sa durée, la suspension du permis de conduire ou l'interdiction de sa délivrance ordonnée par le représentant de l'Etat dans le département en application des articles L. 224-2 et L. 224-7 cesse d'avoir effet lorsque est exécutoire une décision judiciaire prononçant une mesure restrictive du droit de conduire prévue au présent titre. / Les mesures administratives prévues aux articles L. 224-1 à L. 224-3 et L. 224-7 sont considérées comme non avenues en cas d'ordonnance de non-lieu ou de jugement de relaxe ou si la juridiction ne prononce pas effectivement de mesure restrictive du droit de conduire. /(...) " ;<br/>
<br/>
              3. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond qu'à l'appui de ses conclusions tendant à l'annulation de l'arrêté du 7 mai 2012 suspendant pour un an la validité de son permis de conduire, M. B...soutenait, en mentionnant la décision de classement sans suite prise par le ministère public le 30 mai suivant, qu'il n'avait commis aucune infraction  ; qu'en se bornant à rappeler qu'une décision de classement sans suite est par elle-même dépourvue d'incidence sur la légalité d'une mesure de suspension du permis de conduire, sans rechercher si la condition, relative à l'existence d'une infraction, constatée par procès-verbal, en matière de respect des vitesses maximales autorisées ou des règles de croisement, de dépassement, d'intersection et de priorités de passage, à laquelle les dispositions de l'avant-dernier alinéa de l'article L. 224-2 subordonnent la suspension du permis de conduire, était remplie en l'espèce, le tribunal administratif n'a pas légalement justifié le rejet des conclusions dont il était saisi ;  <br/>
<br/>
              En ce qui concerne la décision verbale exigeant la restitution du permis de conduire :<br/>
<br/>
              4. Considérant qu'il résulte des pièces du dossier soumis aux juges du fond que M.B..., qui, dans sa requête introductive d'instance devant le tribunal administratif, avait demandé l'annulation d'une décision verbale du commissaire de police de Limay du 22 août 2012 lui enjoignant de restituer son permis de conduire, a précisé dans un mémoire présenté le 20 décembre 2013, avant la clôture de l'instruction qui avait été rouverte par une ordonnance du 3 décembre 2013, que ses conclusions devaient être regardées comme dirigées contre une décision verbale prise le 30 août 2012 ; qu'en énonçant dans son jugement qu'il n'avait pas été saisi avant la clôture de l'instruction de conclusions dirigées contre une décision du 30 août 2012, et en se bornant à rejeter des conclusions dirigées contre une décision du 22 août 2012 au motif qu'aucune décision n'avait été prise à cette date, le tribunal s'est mépris sur la portée des écritures de l'intéressé ; <br/>
<br/>
              En ce qui concerne la décision implicite de rejet qui serait née à la suite du courrier du 30 août 2012 :<br/>
<br/>
              5. Considérant qu'en estimant, après avoir relevé que le courrier adressé le 30 août 2012 par l'avocat du requérant au commissaire de police de Limay se bornait à solliciter des explications sur le retrait de son permis de conduire, sans formuler aucune demande, que ce courrier ne pouvait être regardé comme un recours gracieux, et en en déduisant que le silence gardé par l'administration n'avait pas fait naître une décision implicite de rejet susceptible de faire l'objet d'un recours pour excès de pouvoir, le tribunal administratif a souverainement apprécié la portée de ce courrier, sans entacher son jugement de dénaturation ; <br/>
<br/>
              6. Considérant qu'il résulte de tout ce qui précède que M. B...est fondé à demander l'annulation du jugement du 24 avril 2014 du tribunal administratif de Versailles en tant, d'une part, qu'il rejette ses conclusions dirigées contre l'arrêté du 7 mai 2012 du préfet des Hauts-de-Seine et, d'autre part, qu'il s'abstient de statuer sur ses conclusions dirigées contre la décision verbale du 30 août 2012 ;<br/>
<br/>
              7. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat, au titre des dispositions de l'article L. 761-1 du code de justice administrative, le versement à M. B...de la somme de 3 000 euros ; <br/>
<br/>
<br/>
<br/>
<br/>			D E C I D E :<br/>
              			--------------<br/>
<br/>
Article 1er : Le jugement du 24 avril 2014 du tribunal administratif de Versailles est annulé en tant, d'une part, qu'il rejette les conclusions de M. B...dirigées contre l'arrêté du 7 mai 2012 du préfet des Hauts-de-Seine et, d'autre part, qu'il s'abstient de statuer sur ses conclusions dirigées contre une décision verbale du 30 août 2012.<br/>
Article 2 : L'affaire est renvoyée, dans la mesure de la cassation ainsi prononcée, au tribunal administratif de Versailles. <br/>
Article 3 : L'Etat versera à M. B...la somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Le surplus des conclusions de M. B...est rejeté.<br/>
Article 5 : La présente décision sera notifiée à M. A... B...et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
