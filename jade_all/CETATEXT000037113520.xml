<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037113520</ID>
<ANCIEN_ID>JG_L_2018_06_000000415764</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/11/35/CETATEXT000037113520.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème - 3ème chambres réunies, 27/06/2018, 415764, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-06-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>415764</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème - 3ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BRIARD</AVOCATS>
<RAPPORTEUR>M. Etienne de Lageneste</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Romain Victor</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:415764.20180627</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société par actions simplifiée l'Européenne d'embouteillage a demandé au tribunal administratif d'Orléans de prononcer la réduction des cotisations supplémentaires de taxe foncière sur les propriétés bâties auxquelles elle a été assujettie au titre des années 2011 et 2012 et de la cotisation supplémentaire de cotisation foncière des entreprises à laquelle elle a été assujettie au titre de l'année 2012 à raison d'un établissement situé à Donnery (Loiret). Par un jugement nos 1403577, 1403589 du 23 juin 2015, le tribunal administratif a rejeté ces demandes.<br/>
<br/>
              Par un arrêt n° 15NT02563 du 16 novembre 2017, enregistré le 17 novembre 2017 au secrétariat du contentieux du Conseil d'Etat, la cour administrative d'appel de Nantes a transmis au Conseil d'Etat, en application de l'article R. 351-2 du code de justice administrative, les conclusions, enregistrées le 13 août 2015 au greffe de cette cour, présentées par la société l'Européenne d'embouteillage contre ce jugement en tant qu'il concerne la taxe foncière sur les propriétés bâties.<br/>
<br/>
              Par ces conclusions, un nouveau mémoire et un mémoire en réplique, enregistrés les 10 janvier et 4 juin 2018 au secrétariat du contentieux du Conseil d'Etat, la société l'Européenne d'embouteillage demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement en tant qu'il statue sur la taxe foncière sur les propriétés bâties ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 1 680 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Etienne de Lageneste, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Romain Victor, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Briard, avocat de la société l'Europeenne d'embouteillage.<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond qu'à l'issue de la vérification de comptabilité dont  la société l'Européenne d'embouteillage a fait l'objet, l'administration a procédé au rehaussement de la valeur locative foncière de son établissement industriel situé à Donnery (Loiret), dont la valeur locative est évaluée selon la méthode comptable, et lui a en conséquence notifié des cotisations supplémentaires de taxe foncière sur les propriétés bâties au titre des années 2011 et 2012. La société se pourvoit en cassation contre le jugement du 23 juin 2015 du tribunal administratif d'Orléans en tant qu'il a rejeté sa demande en décharge de ces cotisations supplémentaires.<br/>
<br/>
              2. Aux termes de l'article 1415 du code général des impôts : " La taxe foncière sur les propriétés bâties, la taxe foncière sur les propriétés non bâties et la taxe d'habitation sont établies pour l'année entière d'après les faits existants au 1er janvier de l'année de l'imposition ". Aux termes du 1 du I de l'article 1517 du même code, dans sa rédaction applicable au litige : " Il est procédé, annuellement, à la constatation des constructions nouvelles et des changements de consistance ou d'affectation des propriétés bâties et non bâties. Il en va de même pour les changements de caractéristiques physiques ou d'environnement quand ils entraînent une modification de plus d'un dixième de la valeur locative ". Il résulte de ces dispositions que l'administration est en droit, pour l'établissement de la taxe foncière sur les propriétés bâties, de la taxe professionnelle ou de la cotisation foncière des entreprises, de constater annuellement les changements de caractéristiques physiques ou les changements d'environnement des biens passibles de taxe foncière lorsque ces changements entraînent une modification de plus d'un dixième de la valeur locative. Ce seuil s'apprécie par référence à la valeur locative du bien à la date du fait générateur de l'imposition de l'année précédente, indépendamment de toute autre cause de modification apparue depuis cette date. Il en résulte que des changements de consistance ou d'affectation postérieurs à cette date sont sans incidence sur l'appréciation à porter au regard du seuil précité. En revanche, il incombe à l'administration, pour déterminer le pourcentage de variation de la valeur locative due aux changements de caractéristiques physiques ou d'environnement, de corriger la valeur locative résultant du rôle général de l'année précédente des changements de consistance ou d'affectation qui n'auraient pas été pris en compte pour l'établissement de l'imposition correspondante, que cette omission ait été mise en évidence par l'administration ou établie par le contribuable. <br/>
<br/>
              3. Il en découle que le tribunal administratif d'Orléans a entaché son jugement d'erreur de droit en jugeant qu'il résultait des dispositions de l'article 1517 du code général des impôts qu'en cas de constatation, à l'occasion d'un contrôle, de changements de caractéristiques physiques ou d'environnement, le dépassement du seuil de dix pour cent qu'elles prévoient devait s'apprécier au regard de la valeur locative du bien retenue dans le rôle général de l'année précédant celle de cette constatation et qu'il n'y avait pas lieu de corriger cette valeur locative des changements de consistance ou d'affectation identifiés lors du contrôle, sans distinguer selon que ces derniers sont antérieurs ou postérieurs au fait générateur de l'imposition due au titre de l'année précédente.<br/>
<br/>
              4. Il résulte de ce qui précède, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, que la société requérante est fondée à demander l'annulation du jugement qu'elle attaque en tant qu'il statue sur les conclusions relatives à la taxe foncière sur les propriétés bâties.<br/>
<br/>
              5. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 1 000 euros à verser à la société l'Européenne d'embouteillage, au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
                                 D E C I D E :<br/>
                                --------------<br/>
<br/>
Article 1er : Le jugement du tribunal administratif d'Orléans du 23 juin 2015 est annulé en tant qu'il statue sur les conclusions relatives à la taxe foncière sur les propriétés bâties.<br/>
<br/>
Article 2 : L'affaire est renvoyée, dans cette mesure, au tribunal administratif d'Orléans.<br/>
<br/>
Article 3 : L'Etat versera à la société l'Européenne d'embouteillage une somme de 1 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : La présente décision sera notifiée à la société par actions simplifiée l'Européenne d'embouteillage et au ministre de l'action et des comptes publics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
