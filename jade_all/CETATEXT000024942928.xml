<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000024942928</ID>
<ANCIEN_ID>JG_L_2011_12_000000337255</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/24/94/29/CETATEXT000024942928.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Section du Contentieux, 09/12/2011, 337255, Publié au recueil Lebon</TITRE>
<DATE_DEC>2011-12-09</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>337255</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Section du Contentieux</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Marie Picard</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Rémi Keller</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESEC:2011:337255.20111209</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu l'ordonnance n° 0912657/5-3 du 24 février 2010, enregistrée le 4 mars 2010 au secrétariat du contentieux du Conseil d'Etat, par laquelle le président du tribunal administratif de Paris a transmis au Conseil d'Etat, en application de l'article R. 351-2 du code de justice administrative, la requête présentée à ce tribunal par M. A... B... ;<br/>
<br/>
              Vu la requête, enregistrée le 30 juillet 2009 au greffe du tribunal administratif de Paris, présentée par M. A... B..., demeurant..., ; M.  B...demande au Conseil d'État :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision lui retirant sa prime de recherche et d'enseignement supérieur (PRES) pour le  premier semestre de l'année universitaire 2008-2009, par retenue d'un montant de 609,58 euros figurant sur son bulletin de paie du 23 février 2009, et la décision implicite née du silence gardé par le président de l'université Paris I sur son recours gracieux du 1er avril 2009 ;<br/>
<br/>
              2°) d'enjoindre au président de l'université Paris I d'ordonner le versement de sa prime de recherche et d'enseignement supérieur, augmentée des intérêts légaux à compter du 1er avril 2009, et ce sous astreinte ;<br/>
<br/>
              3°) de mettre à la charge de l'université Paris I le versement d'une somme au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code civil, notamment son article 1153 ;<br/>
<br/>
              Vu le code de l'éducation ;<br/>
<br/>
              Vu le décret n° 85-618 du 13 juin 1985 ;<br/>
<br/>
              Vu le décret n° 89-775 du 23 octobre 1989 ;<br/>
<br/>
              Vu l'arrêté du 23 octobre 1989 fixant la liste des personnels de l'enseignement supérieur pouvant bénéficier de la prime de recherche et d'enseignement supérieur instituée par le décret n° 89-775 du 23 octobre 1989 relatif à la prime de recherche et d'enseignement supérieur des personnels de l'enseignement supérieur relevant du ministère chargé de l'enseignement supérieur ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Marie Picard, Maître des Requêtes,<br/>
<br/>
              - les conclusions de M. Rémi Keller, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant que M. B..., qui, en sa qualité de professeur des universités à l'université Paris I, bénéficie de la prime de recherche et d'enseignement supérieur (PRES) instituée par le décret du 23 octobre 1989, a obtenu, par une décision du 5 novembre 2008 du président de cette université, une rémunération en contrepartie de sa participation à l'exécution d'un contrat de recherche en application des dispositions du décret du 13 juin 1985 ; que l'autorité administrative, ainsi que le révèle le bulletin de paie de l'intéressé du 23 février 2009, lui a retiré le bénéfice de la PRES pour le premier semestre de l'année universitaire 2008-2009, au motif que cette prime ne serait pas cumulable avec la rémunération dont l'intéressé a bénéficié pour la même période au titre du décret du 13 juin 1985 ; que M.  B...demande, d'une part, l'annulation de la décision du président de l'université Paris I lui supprimant cette prime ainsi que du rejet implicite de son recours gracieux présenté le 1er avril 2009 et, d'autre part, qu'il soit enjoint à l'université de lui verser la prime supprimée, assortie des intérêts au taux légal ; que le requérant a déclaré, en cours d'instance, se désister de ses " conclusions pécuniaires " ;<br/>
<br/>
              Sur la fin de non-recevoir opposée par l'université Paris I :<br/>
<br/>
              Considérant que le bulletin de paie de M.  B...pour le mois de février 2009 fait apparaître une retenue de 609,58 euros qui a été justifiée par un trop-perçu au titre de la prime de recherche et d'enseignement supérieur versée pour le premier semestre de l'année universitaire 2008-2009 ; que ce document révèle ainsi l'existence d'une décision  de priver l'intéressé de cette prime pour la période considérée ; que, par suite, la fin de non-recevoir opposée par l'université Paris I et tirée de ce que la requête ne serait pas dirigée contre une décision ne peut qu'être écartée ;<br/>
<br/>
              Sur la portée du désistement relatif aux " conclusions pécuniaires " :<br/>
<br/>
              Considérant, en premier lieu, que lorsque sont présentées dans la même instance des conclusions tendant à l'annulation pour excès de pouvoir d'une décision et des conclusions relevant du plein contentieux tendant au versement d'une indemnité pour réparation du préjudice causé par l'illégalité fautive que le requérant estime constituée par cette même décision, cette circonstance n'a pas pour effet de donner à l'ensemble des conclusions le caractère d'une demande de plein contentieux  ; que, par suite, et en tout état de cause, le désistement présenté par M.  B...de ses " conclusions pécuniaires " ne pourrait avoir d'effet ni sur le maintien, ni sur la recevabilité de ses conclusions à fin d'annulation pour excès de pouvoir ;<br/>
<br/>
              Considérant, en second lieu, qu'à l'occasion d'un litige portant sur le versement d'une somme d'argent, les conclusions ayant trait au principal et celles ayant trait aux intérêts sont de même nature ; qu'il en résulte que, lorsqu'un requérant est recevable à demander, par la voie du recours pour excès de pouvoir,  l'annulation de la décision administrative qui l'a privé de cette somme, il est également recevable à demander, par la même voie, l'annulation de la décision qui l'a privé des intérêts qui y sont attachés ; que, lorsque le principal est dû, les intérêts sont dus de plein droit, à condition d'être demandés ; qu'il en résulte que, dans l'hypothèse où le requérant demande l'annulation pour excès de pouvoir de la décision qui l'a privé d'une somme, il est recevable, sur le fondement de l'article L. 911-1 du code de justice administrative, à demander que soit enjoint, pour l'exécution de cette annulation, le versement des intérêts dus à compter de la réception de sa demande préalable à l'administration ou, à défaut, de l'enregistrement de sa requête introductive d'instance ; que de telles conclusions à fin d'injonction, bien qu'ayant un objet pécuniaire, ne devant pas, à peine d'irrecevabilité, être présentées par le ministère d'un avocat, aucune des conclusions à fin d'injonction de M.  B...devant le juge administratif ne nécessitait un tel ministère ; qu'il résulte des termes du mémoire à fin de désistement de M.  B...que celui-ci n'a entendu se désister que des conclusions pécuniaires nécessitant le ministère d'un avocat au Conseil d'Etat en vertu de l'article R. 432-1 du code de justice administrative ; qu'il n'y a donc pas lieu de donner acte d'un désistement relatif à de telles conclusions ;<br/>
<br/>
              Sur la légalité des décisions attaquées :<br/>
<br/>
              Considérant, en premier lieu, qu'en vertu du décret du 13 juin 1985, les personnels des établissements publics d'enseignement supérieur et de recherche  peuvent être rétribués pour les essais, recherches, études ou analyses auxquels ils participent dans le cadre de certains contrats ou conventions signés par leur établissement ; qu'en vertu de l'article 3 de ce décret, le bénéfice des rémunérations qu'il prévoit est incompatible avec la perception de la prime de recherche, instituée par le décret du 6 juillet 1957, ou de la prime de participation à la recherche ;<br/>
<br/>
              Considérant, en second lieu, que la prime de recherche et d'enseignement supérieur (PRES), créée par le décret du 23 octobre 1989, est attribuée aux personnels qui participent à l'élaboration et à la transmission des connaissances ainsi qu'au développement de la recherche ; qu'ayant pour objet de remplacer à la fois l'indemnité forfaitaire spéciale en faveur des personnels enseignants et la prime de recherche, ce même décret a abrogé par son article 4 les décrets du 26 mai 1954 et du 6 juillet 1957 qui instituaient ces primes ; que si, en son article 1er, le décret du 23 octobre 1989 prévoit que le versement de la prime d'enseignement supérieur et de recherche est exclusif de la prime d'enseignement supérieur instaurée par le décret n° 89-776 du même jour, il n'instaure aucune incompatibilité en ce qui concerne les activités de recherche, dès lors qu'elles ne procèdent pas d'un cumul prohibé par le second alinéa de son article 3 ; que le décret du 13 juin 1985 ne comporte aucun régime général d'incompatibilité ; qu'ainsi, en assimilant la PRES, qui a un objet différent de la prime de recherche, à cette dernière, pour en refuser, sur le fondement de l'article 3 du décret du 13 juin 1985, le versement aux enseignants-chercheurs qui bénéficieraient de la rétribution pour leurs travaux de recherche dans le cadre de contrats ou conventions signés par leur établissement, le président de l'université Paris I a fait une inexacte application des textes analysés ci-dessus ; que, par suite, M.  B...est fondé à demander l'annulation des décisions qu'il attaque ;<br/>
<br/>
              Sur les conclusions à fins d'injonction et d'astreinte :<br/>
<br/>
              Considérant que l'exécution de la présente décision implique que M.  B...soit, au titre du premier semestre de l'année universitaire 2008-2009, rétabli dans ses droits à percevoir la prime de recherche et d'enseignement supérieur, qui a un caractère statutaire ; qu'il y a donc lieu d'enjoindre à l'université Paris I, sur le fondement de l'article L. 911-1 du code de justice administrative, de verser la somme de 609,58 euros, assortie des intérêts au taux légal à compter de la réception par ses services du recours gracieux formé par M.  B...le 1er avril 2009, dans le délai d'un mois à compter de la notification de la présente décision ; qu'il n'y a pas lieu, en l'espèce, de faire droit aux conclusions à fin d'astreinte ;<br/>
<br/>
              Sur les conclusions tendant à l'application de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              Considérant que, faute d'être chiffrées, les conclusions de M.  B...tendant à l'application de ces dispositions ne peuvent qu'être rejetées ;<br/>
<br/>
<br/>
<br/>
<br/>
      D E C I D E :<br/>
      --------------<br/>
Article 1er : La décision  supprimant la prime de recherche et d'enseignement supérieur (PRES) de M.  B...au titre du premier semestre de l'année universitaire 2008-2009 pour un montant de 609,58 euros et la décision implicite née du silence gardé par le président de l'université Paris I sur le recours gracieux formé le 1er avril 2009 par M.  B...sont annulées.<br/>
Article 2 : Il est enjoint à l'université Paris I de verser à M.  B...la prime de recherche et d'enseignement supérieur due à celui-ci au titre du premier semestre de l'année universitaire 2008-2009 dans le délai d'un mois à compter de la notification de la présente décision, pour un montant de 609,58 euros, assortie des intérêts au taux légal à compter de la réception de son recours gracieux du 1er avril 2009.<br/>
Article 3 : Le surplus des conclusions de M.  B...est rejeté.<br/>
Article 4 : La présente décision sera notifiée à M. A... B..., à l'université Paris I et au ministre de l'enseignement supérieur et de la recherche.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">30-01-02-01 ENSEIGNEMENT ET RECHERCHE. QUESTIONS GÉNÉRALES. QUESTIONS GÉNÉRALES RELATIVES AU PERSONNEL. QUESTIONS GÉNÉRALES RELATIVES AU PERSONNEL ENSEIGNANT. - ENSEIGNANTS-CHERCHEURS - POSSIBILITÉ DE CUMUL DE LA PRIME DE RECHERCHE ET D'ENSEIGNEMENT SUPÉRIEUR (DÉCRET DU 23 OCTOBRE 1989) ET DE LA RÉTRIBUTION POUR TRAVAUX DE RECHERCHE DANS LE CADRE DE CONTRATS OU CONVENTIONS SIGNÉS PAR LEUR ÉTABLISSEMENT (DÉCRET DU 13 JUIN 1985) - EXISTENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">36-08-03 FONCTIONNAIRES ET AGENTS PUBLICS. RÉMUNÉRATION. INDEMNITÉS ET AVANTAGES DIVERS. - ENSEIGNANTS-CHERCHEURS - POSSIBILITÉ DE CUMUL DE LA PRIME DE RECHERCHE ET D'ENSEIGNEMENT SUPÉRIEUR (DÉCRET DU 23 OCTOBRE 1989) ET DE LA RÉTRIBUTION POUR TRAVAUX DE RECHERCHE DANS LE CADRE DE CONTRATS OU CONVENTIONS SIGNÉS PAR LEUR ÉTABLISSEMENT (DÉCRET DU 13 JUIN 1985) - EXISTENCE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">54-01-08-02-02 PROCÉDURE. INTRODUCTION DE L'INSTANCE. FORMES DE LA REQUÊTE. MINISTÈRE D'AVOCAT. ABSENCE D'OBLIGATION. - LITIGE PORTANT SUR LE VERSEMENT D'UNE SOMME D'ARGENT - CONCLUSIONS AYANT TRAIT AU PRINCIPAL ET CONCLUSIONS AYANT TRAIT AUX INTÉRÊTS - RECOURS DE MÊME NATURE - CONSÉQUENCE - REQUÉRANT RECEVABLE À DEMANDER, PAR LA VOIE DU RECOURS POUR EXCÈS DE POUVOIR, L'ANNULATION DE LA DÉCISION ADMINISTRATIVE QUI L'A PRIVÉ DE CETTE SOMME ÉGALEMENT RECEVABLE À DEMANDER, PAR LA MÊME VOIE, L'ANNULATION DE LA DÉCISION QUI L'A PRIVÉ DES INTÉRÊTS [RJ2].
</SCT>
<SCT ID="8D" TYPE="PRINCIPAL">54-02-01-01 PROCÉDURE. DIVERSES SORTES DE RECOURS. RECOURS POUR EXCÈS DE POUVOIR. RECOURS AYANT CE CARACTÈRE. - 1) CONCLUSIONS TENDANT À L'ANNULATION POUR EXCÈS DE POUVOIR D'UNE DÉCISION ET CONCLUSIONS RELEVANT DU PLEIN CONTENTIEUX PRÉSENTÉES DANS LA MÊME INSTANCE - CONSÉQUENCE - CARACTÈRE DE RECOURS DE PLEIN CONTENTIEUX DE L'ENSEMBLE DE LA DEMANDE - ABSENCE [RJ1] - 2) LITIGE PORTANT SUR LE VERSEMENT D'UNE SOMME D'ARGENT - CONCLUSIONS AYANT TRAIT AU PRINCIPAL ET CONCLUSIONS AYANT TRAIT AUX INTÉRÊTS - RECOURS DE MÊME NATURE - CONSÉQUENCE - REQUÉRANT RECEVABLE À DEMANDER, PAR LA VOIE DU RECOURS POUR EXCÈS DE POUVOIR, L'ANNULATION DE LA DÉCISION ADMINISTRATIVE QUI L'A PRIVÉ DE CETTE SOMME ÉGALEMENT RECEVABLE À DEMANDER, PAR LA MÊME VOIE, L'ANNULATION DE LA DÉCISION QUI L'A PRIVÉ DES INTÉRÊTS [RJ2].
</SCT>
<SCT ID="8E" TYPE="PRINCIPAL">54-02-02 PROCÉDURE. DIVERSES SORTES DE RECOURS. RECOURS DE PLEIN CONTENTIEUX. - CONCLUSIONS TENDANT À L'ANNULATION POUR EXCÈS DE POUVOIR D'UNE DÉCISION ET CONCLUSIONS RELEVANT DU PLEIN CONTENTIEUX PRÉSENTÉES DANS LA MÊME INSTANCE - CONSÉQUENCE - CARACTÈRE DE RECOURS DE PLEIN CONTENTIEUX DE L'ENSEMBLE DE LA DEMANDE - ABSENCE [RJ1].
</SCT>
<ANA ID="9A"> 30-01-02-01 En vertu du décret n° 85-618 du 13 juin 1985, les personnels des établissements publics d'enseignement supérieur et de recherche peuvent être rétribués pour les essais, recherches, études ou analyses auxquels ils participent dans le cadre de certains contrats ou conventions signés par leur établissement. En vertu de l'article 3 de ce décret, le bénéfice des rémunérations qu'il prévoit est incompatible avec la perception de la prime de recherche, instituée par le décret du 6 juillet 1957, ou de la prime de participation à la recherche. La prime de recherche et d'enseignement supérieur (PRES), créée par le décret n° 89-775 du 23 octobre 1989, est attribuée aux personnels qui participent à l'élaboration et à la transmission des connaissances ainsi qu'au développement de la recherche. Ayant pour objet de remplacer à la fois l'indemnité forfaitaire spéciale en faveur des personnels enseignants et la prime de recherche, ce même décret a abrogé par son article 4 les décrets du 26 mai 1954 et du 6 juillet 1957 qui instituaient ces primes. Si, en son article 1er, le décret du 23 octobre 1989 prévoit que le versement de la prime d'enseignement supérieur et de recherche est exclusif de la prime d'enseignement supérieur instaurée par le décret n° 89-776 du même jour, il n'instaure aucune incompatibilité en ce qui concerne les activités de recherche, dès lors qu'elles ne procèdent pas d'un cumul prohibé par le second alinéa de son article 3. Le décret du 13 juin 1985 ne comporte aucun régime général d'incompatibilité. Ainsi, en assimilant la PRES, qui a un objet différent de la prime de recherche, à cette dernière, pour en refuser, sur le fondement de l'article 3 du décret du 13 juin 1985, le versement aux enseignants-chercheurs qui bénéficieraient de la rétribution pour leurs travaux de recherche dans le cadre de contrats ou conventions signés par leur établissement, un président d'université fait une inexacte application des textes analysés ci-dessus.</ANA>
<ANA ID="9B"> 36-08-03 En vertu du décret n° 85-618 du 13 juin 1985, les personnels des établissements publics d'enseignement supérieur et de recherche peuvent être rétribués pour les essais, recherches, études ou analyses auxquels ils participent dans le cadre de certains contrats ou conventions signés par leur établissement. En vertu de l'article 3 de ce décret, le bénéfice des rémunérations qu'il prévoit est incompatible avec la perception de la prime de recherche, instituée par le décret du 6 juillet 1957, ou de la prime de participation à la recherche. La prime de recherche et d'enseignement supérieur (PRES), créée par le décret n° 89-775 du 23 octobre 1989, est attribuée aux personnels qui participent à l'élaboration et à la transmission des connaissances ainsi qu'au développement de la recherche. Ayant pour objet de remplacer à la fois l'indemnité forfaitaire spéciale en faveur des personnels enseignants et la prime de recherche, ce même décret a abrogé par son article 4 les décrets du 26 mai 1954 et du 6 juillet 1957 qui instituaient ces primes. Si, en son article 1er, le décret du 23 octobre 1989 prévoit que le versement de la prime d'enseignement supérieur et de recherche est exclusif de la prime d'enseignement supérieur instaurée par le décret n° 89-776 du même jour, il n'instaure aucune incompatibilité en ce qui concerne les activités de recherche, dès lors qu'elles ne procèdent pas d'un cumul prohibé par le second alinéa de son article 3. Le décret du 13 juin 1985 ne comporte aucun régime général d'incompatibilité. Ainsi, en assimilant la PRES, qui a un objet différent de la prime de recherche, à cette dernière, pour en refuser, sur le fondement de l'article 3 du décret du 13 juin 1985, le versement aux enseignants-chercheurs qui bénéficieraient de la rétribution pour leurs travaux de recherche dans le cadre de contrats ou conventions signés par leur établissement, un président d'université fait une inexacte application des textes analysés ci-dessus.</ANA>
<ANA ID="9C"> 54-01-08-02-02 A l'occasion d'un litige portant sur le versement d'une somme d'argent, les conclusions ayant trait au principal et celles ayant trait aux intérêts sont de même nature. Il en résulte que, lorsqu'un requérant est recevable à demander, par la voie du recours pour excès de pouvoir, l'annulation de la décision administrative qui l'a privé de cette somme, il est également recevable à demander, par la même voie, l'annulation de la décision qui l'a privé des intérêts qui y sont attachés. Lorsque le principal est dû, les intérêts sont dus de plein droit, à condition d'être demandés. Ainsi, dans l'hypothèse où le requérant demande l'annulation pour excès de pouvoir de la décision qui l'a privé d'une somme, il est recevable, sur le fondement de l'article L. 911-1 du code de justice administrative, à demander que soit enjoint, pour l'exécution de cette annulation, le versement des intérêts dus à compter de la réception de sa demande préalable à l'administration ou, à défaut, de l'enregistrement de sa requête introductive d'instance. De telles conclusions à fin d'injonction, bien qu'ayant un objet pécuniaire, ne doivent pas, à peine d'irrecevabilité, être présentées par le ministère d'un avocat.</ANA>
<ANA ID="9D"> 54-02-01-01 1) Lorsque sont présentées dans la même instance des conclusions tendant à l'annulation pour excès de pouvoir d'une décision et des conclusions relevant du plein contentieux tendant au versement d'une indemnité pour réparation du préjudice causé par l'illégalité fautive que le requérant estime constituée par cette même décision, cette circonstance n'a pas pour effet de donner à l'ensemble des conclusions le caractère d'une demande de plein contentieux.... ...2) A l'occasion d'un litige portant sur le versement d'une somme d'argent, les conclusions ayant trait au principal et celles ayant trait aux intérêts sont de même nature. Il en résulte que, lorsqu'un requérant est recevable à demander, par la voie du recours pour excès de pouvoir, l'annulation de la décision administrative qui l'a privé de cette somme, il est également recevable à demander, par la même voie, l'annulation de la décision qui l'a privé des intérêts qui y sont attachés. Lorsque le principal est dû, les intérêts sont dus de plein droit, à condition d'être demandés. Ainsi, dans l'hypothèse où le requérant demande l'annulation pour excès de pouvoir de la décision qui l'a privé d'une somme, il est recevable, sur le fondement de l'article L. 911-1 du code de justice administrative, à demander que soit enjoint, pour l'exécution de cette annulation, le versement des intérêts dus à compter de la réception de sa demande préalable à l'administration ou, à défaut, de l'enregistrement de sa requête introductive d'instance. De telles conclusions à fin d'injonction, bien qu'ayant un objet pécuniaire, ne doivent pas, à peine d'irrecevabilité, être présentées par le ministère d'un avocat.</ANA>
<ANA ID="9E"> 54-02-02 Lorsque sont présentées dans la même instance des conclusions tendant à l'annulation pour excès de pouvoir d'une décision et des conclusions relevant du plein contentieux tendant au versement d'une indemnité pour réparation du préjudice causé par l'illégalité fautive que le requérant estime constituée par cette même décision, cette circonstance n'a pas pour effet de donner à l'ensemble des conclusions le caractère d'une demande de plein contentieux.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Ab. Jur. CE, 7 novembre 1990, Ministre de la défense c/ Mme Delfau, n° 113217, T. p. 649.,,[RJ2] Ab. Jur. CE, 11 juillet 1991, Crégut, n° 91758, T. p. 1120.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
