<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000007627463</ID>
<ANCIEN_ID>JFXLX1989X03X0000058361</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/07/62/74/CETATEXT000007627463.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'Etat, 9 / 8 SSR, du 20 mars 1989, 58361, inédit au recueil Lebon</TITRE>
<DATE_DEC>1989-03-20</DATE_DEC>
<JURIDICTION>Conseil d'Etat</JURIDICTION>
<NUMERO>58361</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9 / 8 SSR</FORMATION>
<TYPE_REC>Plein contentieux fiscal</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mallet</RAPPORTEUR>
<COMMISSAIRE_GVT>Ph. Martin</COMMISSAIRE_GVT>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>     Vu, 1°, sous le n° 58 361, la requête sommaire et le mémoire complémentaire enregistrés les 11 avril 1984 et 7 août 1984 au secrétariat du Contentieux du Conseil d'Etat, présentés pour M. Pierre X..., demeurant ..., et tendant à ce que le Conseil d'Etat :<br/>    1° annule le jugement du 10 janvier 1984 par lequel le tribunal administratif de Paris a rejeté sa demande en décharge des suppléments d'impôt sur le revenu auquel il a été assujetti au titre des années 1976, 1977, 1978 et 1979 ;<br/>    2° lui accorde la décharge de ces impositions,<br/>     Vu, 2°, sous le n° 82 063, la requête et le mémoire, enregistrés le 16 septembre 1986 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. X..., demeurant ... et tendant à ce que le Conseil d'Etat :<br/>    1° annule le jugement du 5 juin 1986 par lequel le tribunal administratif de Paris a rejeté sa demande en décharge des suppléments d'impôt sur le revenu auxquels il a été assujetti au titre des années 1980 et 1981 ;<br/>    2° lui accorde la décharge de ces impositions ;<br/>     Vu les autres pièces des dossiers ;<br/>    Vu le code général des impôts ;<br/>    Vu le code du travail ;<br/>    Vu le code des tribunaux administratifs et des cours administratives d'appel ;<br/>    Vu l'ordonnance n° 45-1708 du 31 juillet 1945, le décret n° 53-934 du 30 septembre 1953 et la loi n° 87-1127 du 31 décembre 1987 ;<br/>    Vu la loi n° 77-1468 du 30 décembre 1977 ;<br/>    Après avoir entendu :<br/>    - le rapport de M. Mallet, Maître des requêtes,<br/>    - les observations de Me Le Griel, avocat de M. Pierre X...,<br/>    - les conclusions de M. Ph Martin, Commissaire du gouvernement ;<br/>
<br/>     Considérant que les requêtes de M. X... ont trait à l'impôt sur le revenu auquel il a été assujetti au titre, d'une part, des années 1976 à 1979, d'autre part, des années 1980 et 1981 ; quelles présentent à juger des questions semblables ; qu'il y a lieu de les joindre pour statuer par une seule décision ;<br/>    Considérant qu'en vertu de l'article 5 de l'annexe IV du code général des impôts en application de l'article 83 du même code, les voyageurs, représentants et placiers de commerce et d'industrie (V.R.P.) ont droit, pour la détermination des salaires à retenir en vue du calcul de l'impôt sur le revenu, à une déduction supplémentaire pour frais professionnels de 30 % ; que dans le cas des personnes qui exercent à la fois à une activité de V.R.P. et des activités d'une autre nature, la déduction supplémentaire ne s'applique à l'ensemble des rémunérations perçues que lorsque les activités étrangères à la profession de V.R.P. ne présentent qu'un caractère accessoire ;<br/>    Considérant que, pour soutenir que M. X... n'était pas en droit d'appliquer la déduction supplémentaire pour frais professionnels de 30 % à l'ensemble des rémunérations qui lui ont été versées de 1976 à 1981 par la société "Office de diffusion de matériel pédagogique" (O.D.M.P.), l'administration fit valoir qu'il avait pour principale fonction d'animer et de diriger le réseau commercial de cette société, qui disposait d'une équipe de V.R.P. dont chacun était responsable d'un secteur géographique déterminé ; qu'elle fait, en outre, observer que les salaires étaient calculés en pourcentage du montant global des commandes enregistrées ; que, si, de son côté, M. X... expose qu'il était personnellement chargé du démarchage de la clientèle dans deux départements de la région parisienne ainsi que dans plusieurs autres villes de province et qu'il assurait le remplacement des V.R.P. défaillants, il ne fait état que d'éléments imprécis quant à l'importance réelle de ces activités et du montant des rémunérations particulières qui en auraient été la contrepartie ; que ne justifiant pas que lesdites activités n'auraient eu qu'un caractère accessoire , comparé à celles qu'il exerçait à la direction de la société, il ne saurait prétendre qu'il était en droit de pratiquer la déduction supplémentaire de 30 % sur l'ensemble de ses salaires ; <br/>
<br/>    Considérant qu'il résulte de tout ce qui précède que M. X... n'est pas fondé à soutenir que c'est à tort que, par les jugements attaqués, qui ne sont entachés d'aucune irrégularité, le tribunal administratif de Paris a rejeté ses demandes en décharge des suppléments d'impôt sur le revenu qui lui ont été assignés ;<br/>Article 1er : Les requêtes de M. X... sont rejetées.<br/>Article 2 : La présente décision sera notifiée à M. X... et au ministre délégué auprès du ministre d'Etat, ministre de l'économie, des finances et du budget, chargé du budget.<br/>
<br/>
</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8AA" TYPE="PRINCIPAL">19-04-01-02 CONTRIBUTIONS ET TAXES - IMPOTS SUR LES REVENUS ET BENEFICES - REGLES GENERALES PROPRES AUX DIVERS IMPOTS - IMPOT SUR LE REVENU</SCT>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS>
<LIEN cidtexte="" datesignatexte="" id="" naturetexte="" nortexte="" num="" numtexte="" sens="source" typelien="CITATION">. CGI 83</LIEN>
<LIEN cidtexte="" datesignatexte="" id="" naturetexte="" nortexte="" num="" numtexte="" sens="source" typelien="CITATION">CGIAN4 5</LIEN>
</LIENS>
</TEXTE_JURI_ADMIN>
