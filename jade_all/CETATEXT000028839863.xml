<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028839863</ID>
<ANCIEN_ID>JG_L_2014_04_000000369462</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/83/98/CETATEXT000028839863.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  9ème sous-section jugeant seule, 09/04/2014, 369462, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-04-09</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>369462</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 9ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR>Mme Maïlys Lange</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Aladjidi</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2014:369462.20140409</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>VU LA PROCEDURE SUIVANTE :<br/>
<br/>
Procédure contentieuse antérieure<br/>
<br/>
              La société Office Dépôt Participations France a demandé au tribunal administratif de Montreuil, d'une part, le rétablissement d'une fraction des déficits constatés au titre de son résultat d'ensemble pour les exercices clos en 2004 et 2005 et, d'autre part, la décharge du prélèvement exceptionnel de 25 % ainsi que des pénalités correspondantes auxquels elle a été assujettie au titre de l'année 2005. Par un jugement n° 1106767 du 18 octobre 2012, le tribunal administratif de Montreuil a rejeté sa demande.<br/>
<br/>
              Par une ordonnance n° 12VE04253 du 18 avril 2013, le président de la première chambre de la cour administrative d'appel de Versailles a rejeté l'appel formé par la société Office Dépôt Participations France contre ce jugement. <br/>
<br/>
Procédure devant le Conseil d'Etat<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 18 juin et 18 septembre 2013 au secrétariat du contentieux du Conseil d'Etat, et un mémoire en réplique, enregistré le 5 mars 2014, la société Office Dépôt Participations France demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'ordonnance n° 12VE04253 du 18 avril 2013 du président de la première chambre de la cour administrative d'appel de Versailles ;<br/>
<br/>
              2°) de renvoyer l'examen de l'affaire à la cour administrative d'appel de Versailles ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Par un mémoire en défense, enregistré le 26 décembre 2013, le ministre de l'économie et des finances déclare s'en remettre à la sagesse du Conseil d'Etat sur le pourvoi de la société Office Dépôt Participations France et conclut en outre, dans l'hypothèse où le Conseil réglerait l'affaire au fond, au rejet de la requête présentée par cette société devant la cour administrative d'appel de Versailles. <br/>
<br/>
<br/>
              Vu :<br/>
              - les autres pièces du dossier ;<br/>
<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Maïlys Lange, Auditeur,  <br/>
<br/>
              - les conclusions de M. Frédéric Aladjidi, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Piwnica, Molinié, avocat de la société Office Dépôt Participations France ;<br/>
<br/>
<br/>
<br/>CONSIDERANT CE QUI SUIT :<br/>
<br/>
              1. En vertu des dispositions du 4° de l'article R. 222-1 du code de justice administrative, les présidents de formations de jugement des cours administratives d'appel peuvent, par ordonnance, rejeter les requêtes entachées d'une irrecevabilité manifeste insusceptible d'être couverte en cours d'instance, sans être tenus d'inviter leur auteur à les régulariser.<br/>
<br/>
              2. En application de ces dispositions, le président de la première chambre de la cour administrative d'appel de Versailles a rejeté, comme entachée d'une irrecevabilité manifeste insusceptible d'être couverte en cours d'instance, la requête présentée par la société Office Dépôt Participations France, au motif que cette requête n'avait été enregistrée que le 26 décembre 2012, soit au-delà de la date d'expiration du délai de recours contre le jugement du 18 octobre 2012 du tribunal administratif de Montreuil, notifié à l'intéressée le 23 octobre 2012.<br/>
<br/>
              3. Il ressort des pièces du dossier de la cour administrative d'appel de Versailles que la lettre contenant la requête de la société Office Dépôt Participations France a été postée à Paris, le vendredi 21 décembre 2012, soit en temps utile pour parvenir au greffe de la cour avant l'expiration, le lundi 24 décembre 2012 à minuit, du délai d'appel fixé par l'article R. 811-2 du code de justice administrative.<br/>
<br/>
              4. En se référant à la date d'enregistrement au greffe de la cour de la requête de la société Office Dépôt Participations France pour juger celle-ci tardive et, comme telle, manifestement irrecevable, sans avoir recherché si elle avait été expédiée en temps utile, le président de la première chambre de la cour d'appel de Versailles a entaché son ordonnance d'erreur de droit.<br/>
<br/>
              5. Il résulte de ce qui précède que, sans qu'il soit besoin d'examiner l'autre moyen du pourvoi, l'ordonnance attaquée doit être annulée.<br/>
<br/>
              6. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat le versement au bénéfice de la société Office Dépôt Participations France de la somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : L'ordonnance du 18 avril 2013 du président de la première chambre de la cour administrative d'appel de Versailles est annulée.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Versailles.<br/>
Article 3 : L'Etat versera à la société Office Dépôt Participations France la somme de 3 000 euros en application de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à la société Office Dépôt Participations France et au ministre des finances et des comptes publics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
