<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029490945</ID>
<ANCIEN_ID>JG_L_2014_09_000000358888</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/49/09/CETATEXT000029490945.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème et 10ème sous-sections réunies, 22/09/2014, 358888, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-09-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>358888</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème et 10ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DELAPORTE, BRIARD, TRICHET</AVOCATS>
<RAPPORTEUR>M. Olivier Japiot</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Marie-Astrid Nicolazo de Barmon</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:358888.20140922</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi, enregistré le 26 avril 2012 au secrétariat du contentieux du Conseil d'Etat, du ministre du budget, des comptes publics et de la réforme de l'Etat ; il demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 10NC01589 du 22 février 2012 par lequel la cour administrative d'appel de Nancy a, d'une part, annulé le jugement n° 0702047 du 30 juillet 2010 du tribunal administratif de Châlons-en-Champagne rejetant la demande de M. B...A...tendant à la décharge des cotisations supplémentaires d'impôt sur le revenu auxquelles M. et Mme A... ont été assujettis au titre de l'année 2000, d'autre part, les a déchargés de cette imposition d'un montant de 50 383 euros ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rétablir M. A...au rôle de l'impôt sur le revenu pour l'année 2000 ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code général des impôts et le livre des procédures fiscales ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Olivier Japiot, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Marie-Astrid Nicolazo de Barmon, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Delaporte, Briard, Trichet, avocat de M. A...;<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes du I de l'article 163 tervicies du code général des impôts, alors applicable : " Les contribuables peuvent déduire de leur revenu net global une somme égale au montant hors taxes des investissements productifs (...) qu'ils réalisent dans les départements et territoires d'outre-mer (...), dans le cadre d'une entreprise exerçant une activité dans les secteurs (...) de l'agriculture (...)./ Les dispositions du premier alinéa s'appliquent également aux investissements réalisés par une société soumise au régime d'imposition prévu à l'article 8 (...). En ce cas, la déduction est pratiquée par les associés ou membres dans une proportion correspondant à leurs droits dans la société ou le groupement./ La déduction prévue au premier alinéa est opérée au titre de l'année au cours de laquelle l'investissement est réalisé. (...) " ; qu'aux termes de l'article 518 du code civil : " Les fonds de terre et les bâtiments sont immeubles par leur nature " ; qu'aux termes de l'article 520 du même code : " Les récoltes pendantes par les racines et les fruits des arbres non encore recueillis sont pareillement immeubles./ Dès que les grains sont coupés et les fruits détachés, quoique non enlevés, ils sont meubles. (...) " ; qu'enfin, l'article L. 411-1 du code rural et de la pêche maritime soumet au statut du fermage ou du métayage toute mise à disposition à titre onéreux d'un immeuble à usage agricole en vue de l'exploiter pour y exercer une activité agricole ;<br/>
<br/>
              2. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que M. A... était l'un des associés de la société en nom collectif (SNC) Billy ; que celle-ci a effectué, en 2000, un investissement à Basse-Pointe (Martinique), pour un montant de 288 862,03 euros hors taxe, consistant à faire réaliser par l'EURL Leyritz-Béranger des travaux de plantation de bananiers ; que le contrat entre les deux sociétés prévoyait qu'une fois ces travaux achevés, les plantations seraient données en location pour cinq ans à l'EURL, pour qu'elle les exploite, avant de les racheter au terme de ce délai ; que M. A...a déduit de son revenu imposable pour l'année 2000, au titre de cet investissement, en application des dispositions citées au point 1 de l'article 163 tervicies du code général des impôts, la somme de 76 218 euros (499 957 F), proportionnelle à ses droits sociaux dans la SNC Billy ; que l'administration a remis en cause cette déduction ; que, par un jugement du 30 juillet 2010, le tribunal administratif de Châlons-en-Champagne a rejeté sa demande de décharge ; que le ministre du budget, des comptes publics et de la réforme de l'Etat se pourvoit en cassation contre l'arrêt du 22 février 2012 par lequel la cour administrative d'appel de Nancy a annulé ce jugement et déchargé M. A...du complément d'impôt sur le revenu d'un montant de 50 383 euros auquel il a été assujetti au titre de l'année 2000 ;<br/>
<br/>
              3. Considérant, en premier lieu, d'une part, qu'aux termes du deuxième alinéa de l'article R. 200-2 du livre des procédures fiscales : " Le demandeur ne peut contester devant le tribunal administratif des impositions différentes de celles qu'il a visées dans sa réclamation à l'administration " ; qu'il ressort des pièces du dossier soumis aux juges du fond que tant la réclamation que M. A...a formée le 15 janvier 2007 que sa requête devant le tribunal administratif de Châlons-en-Champagne portaient sur l'ensemble des cotisations supplémentaires d'impôt sur le revenu mises en recouvrement le 30 novembre 2006 ; que le ministre n'est, dès lors, pas fondé à soutenir que la cour administrative d'appel aurait statué ultra petita en prononçant la décharge de la totalité de ces cotisations supplémentaires ; que, d'autre part, si le ministre soutient, à titre subsidiaire, que la cour n'a pas suffisamment motivé son arrêt en accordant cette décharge totale sans se prononcer sur le chef de redressement fondé sur l'interdiction d'imputer le déficit de la SNC Billy sur d'autres revenus que les bénéfices industriels et commerciaux, elle a suffisamment motivé cette décharge dès lors qu'elle a relevé qu'il résultait des dispositions de l'article 163 tervicies du code général des impôts que l'investissement mentionné au point 2 de la présente décision était déductible du revenu global du contribuable, dans une proportion correspondant à ses droits dans la SNC Billy ; <br/>
<br/>
              4. Considérant, en deuxième lieu, d'une part, que les dispositions citées au point 1 du I de l'article 163 tervicies du code général des impôts, dès lors qu'elles se réfèrent sans restriction au secteur d'activité de l'agriculture, ne sauraient être interprétées, en l'absence d'indication de la volonté du législateur dans les travaux parlementaires ayant précédé leur adoption, comme limitant le bénéfice de l'avantage fiscal qu'elles prévoient aux investissements réalisés dans les entreprises appartenant au secteur de la production agricole et, par suite, aux seuls exploitants agricoles entrant dans le champ d'application des articles 63 et 1450 du code général des impôts ; que, d'autre part, la circonstance que l'investissement productif réalisé, à caractère immobilier, donne lieu à location ou sous-location est sans incidence sur le droit au bénéfice de la déduction au titre de l'impôt sur le revenu prévu par ces dispositions ; qu'enfin, la circonstance, à la supposer établie, que la SNC Billy aurait exercé une activité civile de location d'immeubles par nature, en application des dispositions combinées des articles 518 et 520 du code civil et de l'article L. 411-1 du code rural et de la pêche maritime, est dépourvue d'incidence sur le droit au bénéfice des dispositions de l'article 163 tervicies du code général des impôts ; qu'il suit de là que c'est sans commettre d'erreur de droit que la cour administrative d'appel de Nancy, qui a suffisamment motivé son arrêt sur ce point, a jugé que les immobilisations réalisées par la SNC Billy au bénéfice d'une entreprise exerçant son activité dans le secteur de l'agriculture et affectées à cette activité, entraient dans le champ d'application du I de l'article 163 tervicies du code général des impôts, sans que s'y oppose la circonstance que la SNC Billy a donné cet investissement en location à l'entreprise agricole chargée d'en assurer l'exploitation ;<br/>
<br/>
              5. Considérant, enfin, que la cour a suffisamment répondu au moyen invoqué par le ministre, tiré de ce que certaines dépenses incluses dans l'investissement constituaient en réalité des charges d'exploitation courante insusceptibles de bénéficier de la déduction prévue par les dispositions citées au point 1 de l'article 163 tervicies du code général des impôts, en relevant qu'il résultait de l'instruction que l'ensemble des dépenses mentionnées par la facture établie par l'EURL Leyritz-Béranger avaient pour objet la fourniture des plants, leur traitement et la préparation du terrain, afin de permettre la réalisation de l'investissement neuf constitué par la bananeraie et que, dès lors, l'intégralité des dépenses de cette facture pouvait bénéficier de ces dispositions  ;<br/>
<br/>
              6. Considérant qu'il résulte de tout ce qui précède que le ministre du budget, des comptes publics et de la réforme de l'Etat n'est pas fondé à demander l'annulation de l'arrêt qu'il attaque ; <br/>
<br/>
              7. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 3 500 à verser à M.A..., au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : Le pourvoi du ministre du budget, des comptes publics et de la réforme de l'Etat est rejeté.<br/>
Article 2 : L'Etat versera à M. A...une somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée au ministre des finances et des comptes publics et à M. B... A....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
