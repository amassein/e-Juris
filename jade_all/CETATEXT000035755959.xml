<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000035755959</ID>
<ANCIEN_ID>JG_L_2017_10_000000399153</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/35/75/59/CETATEXT000035755959.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème - 1ère chambres réunies, 09/10/2017, 399153, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-10-09</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>399153</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème - 1ère chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP NICOLAY, DE LANOUVELLE, HANNOTIN</AVOCATS>
<RAPPORTEUR>Mme Laurence Franceschini</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Suzanne von Coester</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:399153.20171009</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              1° Sous le n° 399153, par une requête sommaire, un mémoire complémentaire et un nouveau mémoire, enregistrés les 26 avril et 25 juillet 2016 et le 8 août 2017 au secrétariat du contentieux du Conseil d'Etat, l'Ordre des avocats au barreau de Toulon demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret n° 2016-217 du 26 février 2016 fixant la liste et le ressort des tribunaux de commerce spécialisés ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              2° Sous le n° 399154, par une requête sommaire, un mémoire complémentaire et un nouveau mémoire, enregistrés les 26 avril et 20 juillet 2016 et le 26 juillet 2017 au secrétariat du contentieux du Conseil d'Etat, l'Ordre des avocats au barreau de Versailles demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret n° 2016-217 du 26 février 2016 fixant la liste et le ressort des tribunaux de commerce spécialisés ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
              3° Sous le n° 399342, par une requête sommaire, un mémoire complémentaire et un nouveau mémoire, enregistrés les 29 avril et 29 juillet 2016 et le 2 juin 2017 au secrétariat du contentieux du Conseil d'Etat, l'Ordre des avocats du barreau de la Rochelle-Rochefort demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir l'annexe 1 du décret n° 2016-217 du 26 février 2016 fixant la liste et le ressort des tribunaux de commerce spécialisés, en tant qu'elle désigne comme tribunal de commerce spécialisé pour le ressort des tribunaux de commerce de Brive-la-Gaillarde, de Guéret, de la Roche-sur-Yon, de La Rochelle, de Limoges, de Niort, de Poitiers et de Saintes, le tribunal de commerce de Poitiers ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu : <br/>
              - le code de commerce ;<br/>
              - le décret n° 2011-184 du 15 février 2011 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Laurence Franceschini, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Suzanne von Coester, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Nicolaÿ, de Lanouvelle, Hannotin, avocat de l'Ordre des avocats au barreau de Toulon et autres et à la SCP Fabiani, Luc-Thaler, Pinatel, avocat de l'ordre des avocats du barreau de la Rochelle-Rochefort.<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'en vertu des dispositions de l'article L. 721-8 du code de commerce, dans sa rédaction issue de la loi du 6 août 2015 pour la croissance, l'activité et l'égalité des chances économiques, certains contentieux ou procédures sont attribués à des tribunaux de commerce spécialement désignés ; que cet article prévoit qu'un décret, pris après avis du Conseil national des tribunaux de commerce, fixe la liste de ces tribunaux et détermine leur ressort, en tenant compte des bassins d'emplois et des bassins d'activité économique ; que, sous respectivement les nos 399153 et 399154, les Ordres des avocats aux barreaux de Toulon et Versailles demandent l'annulation pour excès de pouvoir du décret du 26 février 2016 fixant la liste et le ressort des tribunaux de commerce spécialisés ; que, sous le n° 399342, l'Ordre des avocats au barreau de La Rochelle-Rochefort demande l'annulation pour excès de pouvoir de l'annexe 1 de ce décret, en tant qu'elle désigne le tribunal de commerce de Poitiers comme tribunal de commerce spécialisé pour le ressort des tribunaux de commerce de Brive-la-Gaillarde, de Guéret, de la Roche-sur-Yon, de La Rochelle, de Limoges, de Niort, de Poitiers et de Saintes ; que ces trois requêtes sont dirigées contre le même décret ; qu'il y a lieu de les joindre pour y statuer par une seule décision ;<br/>
<br/>
              2. Considérant qu'eu égard à la nature et à l'objet du présent litige, l'Ordre des avocats au barreau de Draguignan justifie d'un intérêt suffisant à demander l'annulation du décret attaqué ; qu'ainsi, son intervention est recevable ;<br/>
<br/>
              3. Considérant que la spécialisation de certains tribunaux de commerce décidée par le législateur vise, dans un but d'intérêt général, à attribuer à un nombre réduit de juridictions les contentieux et procédures les plus complexes et sensibles, en renforçant ainsi la professionnalisation des juges consulaires et en limitant les risques de conflits d'intérêts liés à une trop grande proximité entre les acteurs économiques locaux et les tribunaux de commerce ; que, ainsi que l'a prévu la loi, le décret attaqué a désigné parmi les 134 tribunaux de commerce existants, les tribunaux de commerce qui seraient ainsi spécialisés, au nombre de 18 ;<br/>
<br/>
              Sur la légalité externe du décret attaqué :<br/>
<br/>
              4. Considérant que l'Ordre des avocats au barreau de Toulon et l'Ordre des avocats au barreau de Versailles soutiennent qu'il n'est pas établi que la consultation du comité spécial de service placé auprès du directeur des services judiciaires a été effectuée conformément aux dispositions du décret du 15 février 2011 relatif aux comités techniques dans les administrations et les établissements publics de l'Etat et que la consultation du conseil national des tribunaux de commerce est intervenue conformément aux dispositions des articles R. 721-7 et suivants et A. 721-1 et suivants du code commerce ; que ce moyen n'est pas assorti des précisions permettant d'en apprécier le bien fondé et ne peut, par suite, qu'être écarté ;  <br/>
<br/>
              5. Considérant qu'aux termes de l'article 22 de la Constitution du 4 octobre 1958, " Les actes du Premier ministre sont contresignés, le cas échéant, par les ministres chargés de leur exécution " ; que, s'agissant d'un acte de nature réglementaire, les ministres chargés de son exécution sont ceux qui ont compétence pour signer ou contresigner les mesures réglementaires ou individuelles que comporte nécessairement l'exécution du décret ; qu'en l'espèce, ni le ministre des finances et des comptes publics, ni le ministre de l'économie, de l'industrie et du numérique, ne sont appelés à prendre de telles mesures pour l'exécution du décret attaqué ; que, par suite, le moyen tiré de ce que le décret serait illégal, faute d'avoir été contresigné par ces ministres, ne peut qu'être écarté ; <br/>
<br/>
              Sur la légalité interne du décret attaqué :<br/>
<br/>
              6. Considérant, en premier lieu, que l'article L. 721-8 du code de commerce prévoit que le décret fixant la liste des tribunaux de commerce spécialisés détermine le ressort de ces juridictions en tenant compte des bassins d'emplois et des bassins d'activité économique ; qu'il en résulte que le pouvoir réglementaire pouvait légalement prendre en compte, pour fixer la liste des tribunaux de commerce spécialisés, des critères pertinents au regard des objectifs poursuivis, tels que l'activité économique des territoires concernés, le nombre d'entreprises qui y sont implantées, l'activité et les effectifs des tribunaux concernés, l'accessibilité du service public de la justice ainsi que la cohérence du maillage des tribunaux de commerce spécialisés avec le découpage administratif et judiciaire ; que, par suite, le moyen tiré de ce que l'auteur du décret attaqué se serait illégalement fondé sur des critères qui n'ont pas été prévus par le législateur doit être écarté ;<br/>
<br/>
              7. Considérant, en deuxième lieu, que, compte tenu des éléments mentionnés aux points 3 et 6, malgré l'éloignement et les difficultés pratiques pouvant en résulter pour certains justiciables et professionnels, le fait de n'avoir prévu que 18 tribunaux de commerce spécialisés, sans retenir ceux de Toulon et Versailles, ne constitue pas, eu égard aux motifs d'intérêt général présidant à cette réforme de l'organisation de ces juridictions, une atteinte disproportionnée au principe d'égalité entre les usagers du service public de la justice et ne procède pas d'une erreur manifeste d'appréciation ; <br/>
<br/>
              8. Considérant, en troisième et dernier lieu, que l'Ordre des avocats au barreau de La  Rochelle-Rochefort conteste le choix de Poitiers, au lieu de La Rochelle, comme tribunal de commerce spécialisé du ressort incluant, outre ces deux juridictions, les tribunaux de commerce de Brive-la-Gaillarde, Guéret, La Roche-sur-Yon, Limoges, Niort, et Saintes ; qu'il fait notamment valoir que l'activité du tribunal de commerce de La Rochelle est plus importante et son effectif de juges consulaires supérieur, et que la population et l'activité économique de son département, la Charente-Maritime, excèdent ceux du département de la Vienne ; que, toutefois, les éléments chiffrés fournis, qui sont du même ordre de grandeur, ne révèlent pas de différence déterminante en faveur du tribunal de commerce de La Rochelle ; qu'il ressort des pièces du dossier qu'en désignant comme tribunal de commerce spécialisé celui de Poitiers, pour des motifs d'intérêt général tenant en particulier à la nécessité d'assurer la cohérence de la carte des juridictions commerciales spécialisées, le pouvoir réglementaire n'a pas commis d'erreur manifeste d'appréciation ;<br/>
<br/>
              9. Considérant qu'il résulte de tout ce qui précède que les requérants ne sont pas fondés à demander l'annulation du décret qu'ils attaquent ; que leurs conclusions présentées au titre de l'article L. 761-1 du code de justice administrative doivent, en conséquence, être rejetées ;  <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'intervention de l'Ordre des avocats au barreau de Draguignan est admise.<br/>
<br/>
Article 2 : Les requêtes de l'Ordre des avocats au barreau de Toulon, de l'Ordre des avocats au barreau de Versailles et de l'Ordre des avocats au barreau de La Rochelle sont rejetées.<br/>
<br/>
Article 3 : La présente décision sera notifiée à l'Ordre des avocats au barreau de Toulon, à l'Ordre des avocats au barreau de Versailles, à l'Ordre des avocats au barreau de La Rochelle-Rochefort, à l'Ordre des avocats au barreau de Draguignan, au Premier ministre et à la garde des sceaux, ministre de la justice. <br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
