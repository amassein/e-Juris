<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043456931</ID>
<ANCIEN_ID>JG_L_2021_04_000000432377</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/45/69/CETATEXT000043456931.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème chambre, 27/04/2021, 432377, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-04-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>432377</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP THOUIN-PALAT, BOUCARD</AVOCATS>
<RAPPORTEUR>M. François Charmont</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:432377.20210427</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B... A... a demandé au tribunal administratif de Dijon d'annuler la décision du 18 janvier 2019 par laquelle le ministre de l'intérieur a constaté la perte de validité de son permis de conduire pour solde de points nul ainsi que les décisions de retraits de points qui y étaient récapitulées, et d'enjoindre au ministre de l'intérieur de lui restituer son permis de conduire. Par une ordonnance n° 1900420 du 30 avril 2019, le président du tribunal administratif, statuant sur le fondement du 7° de l'article R. 222-1 du code de justice administrative, a rejeté sa demande.<br/>
<br/>
              Par un pourvoi, un mémoire complémentaire et un mémoire en réplique, enregistrés les 8 juillet, 8 octobre et 26 novembre 2019 au secrétariat du contentieux du Conseil d'Etat, M. A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance ;   <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
              Vu : <br/>
              - le code de la route ;<br/>
              - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. François Charmont, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Thouin-Palat, Boucard, avocat de M. A... ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis au juge du fond que M. A... a demandé au tribunal administratif de Dijon d'annuler la décision référencée " 48 SI " du 18 janvier 2019 par laquelle le ministre de l'intérieur a constaté l'invalidité de son permis de conduire pour solde de points nul ainsi que les décisions du ministre de l'intérieur portant retraits de points de son permis de conduire consécutives aux infractions des 9 mars 2016, 8 novembre 2017, 17 novembre 2017, 4 mai 2018 et 11 juillet 2018 et d'enjoindre au ministre de rétablir son capital de points et de lui restituer son permis de conduire. M. A... se pourvoit en cassation contre l'ordonnance du 30 avril 2019 par laquelle le président du tribunal administratif de Dijon, statuant sur le fondement du 7° de l'article R. 222-1 du code de justice administrative, a rejeté sa demande.<br/>
<br/>
              Sur la régularité de l'ordonnance attaquée :<br/>
<br/>
              2. Aux termes de l'article R. 222-1 du code de justice administrative : " Les présidents de tribunal administratif (...) peuvent, par ordonnance:/ (...) 7° Rejeter, après l'expiration du délai de recours ou, lorsqu'un mémoire complémentaire a été annoncé, après la production de ce mémoire, les requêtes ne comportant que des moyens de légalité externe manifestement infondés, des moyens irrecevables, des moyens inopérants ou des moyens qui ne sont assortis que de faits manifestement insusceptibles de venir à leur soutien ou ne sont manifestement pas assortis des précisions permettant d'en apprécier le bien-fondé (...) ".<br/>
<br/>
              3. Eu égard aux motifs retenus par l'ordonnance attaquée ainsi qu'à la teneur des moyens qui étaient soulevés, les moyens tirés de ce que son auteur aurait, en faisant application des dispositions du 7° de l'article R  222-1 du code de justice administrative pour rejeter la demande de M. A..., entaché son ordonnance d'erreur de droit et d'insuffisance de motivation ne peuvent qu'être écartés. <br/>
<br/>
              Sur le bien-fondé de l'ordonnance attaquée :<br/>
<br/>
              4. Aux termes de l'article L. 223-1 du code de la route : " Le permis de conduire est affecté d'un nombre de points. Celui-ci est réduit de plein droit si le titulaire du permis a commis une infraction pour laquelle cette réduction est prévue. / (...) La réalité d'une infraction entraînant retrait de points est établie par le paiement d'une amende forfaitaire ou l'émission du titre exécutoire de l'amende forfaitaire majorée, l'exécution d'une composition pénale ou par une condamnation définitive. (...) ". La délivrance au titulaire du permis de conduire à l'encontre duquel est relevée une infraction donnant lieu à retrait de points de l'information prévue aux articles L. 223-3 et R. 223-3 du code de la route constitue une garantie essentielle donnée à l'auteur de l'infraction pour lui permettre, avant d'en reconnaître la réalité par le paiement d'une amende forfaitaire ou l'exécution d'une composition pénale, d'en mesurer les conséquences sur la validité de son permis et éventuellement d'en contester la réalité devant le juge pénal. Son accomplissement conditionne dès lors la régularité de la procédure au terme de laquelle le retrait de points est décidé.<br/>
<br/>
              5. Pour écarter le moyen tiré de ce M. A... avait, en ce qui concerne le retrait de points consécutif à l'infraction du 8 novembre 2017, été privé de la garantie prévue par les articles L. 223-3 et R. 223-3 du code de la route, l'auteur de l'ordonnance attaquée a estimé que l'administration apportait, par la production du procès-verbal électronique signé par M. A..., la preuve qui lui incombait de la communication de l'information prévue par ces dispositions. En statuant ainsi, alors qu'il ressort des pièces du dossier qui lui était soumis que M. A... n'a pas apposé sa signature sur le procès-verbal électronique en cause, l'auteur de l'ordonnance attaquée a entaché son appréciation d'une dénaturation des pièces du dossier.<br/>
<br/>
              6. Il résulte de ce qui précède que M. A..., dont le pourvoi ne comporte aucun moyen relatif aux autres retraits de points dont son permis de conduire a fait l'objet, est fondé à demander l'annulation de l'ordonnance qu'il attaque en tant qu'elle rejette ses conclusions tendant à l'annulation du retrait de points consécutif à l'infraction du 8 novembre 2017.<br/>
<br/>
              7. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 3 000 euros à verser à M. A..., au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'ordonnance du président du tribunal administratif de Dijon du 30 avril 2019 est annulée en tant qu'elle rejette les conclusions de M. A... tendant à l'annulation du retrait de points consécutif à l'infraction du 8 novembre 2017.<br/>
Article 2 : L'affaire est renvoyée au tribunal administratif de Dijon.<br/>
Article 3 : L'Etat versera à M. A... la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à M. B... A... et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
