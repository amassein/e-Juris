<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043121081</ID>
<ANCIEN_ID>JG_L_2021_02_000000443128</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/12/10/CETATEXT000043121081.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère chambre, 10/02/2021, 443128, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-02-10</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>443128</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SARL DIDIER, PINET</AVOCATS>
<RAPPORTEUR>M. Arnaud Skzryerbak</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Vincent Villette</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:443128.20210210</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Le préfet de la région Bourgogne - Franche-Comté, préfet de la Côte-d'Or, a déféré au tribunal administratif de Dijon l'élection, le 23 mai 2020, de Mme D... A... en tant que maire de Nogent-lès-Montbard et a demandé au tribunal administratif de proclamer M. C... B... élu en cette qualité. Par un jugement n° 2001353 du 21 juillet 2020, le tribunal administratif de Dijon a fait droit à ce déféré.<br/>
<br/>
              Par une requête et un mémoire, enregistrés le 20 août 2020 et le 21 janvier 2021 au secrétariat du contentieux du Conseil d'Etat, Mme A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) de rejeter le déféré du préfet de la région Bourgogne - Franche-Comté, préfet de la Côte-d'Or ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code électoral ;<br/>
              - le code général des collectivités territoriales ;<br/>
              - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Arnaud Skzryerbak, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Vincent Villette, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SARL Didier, Pinet, avocat de Mme A... ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. À l'issue des opérations électorales qui se sont déroulées le 15 mars 2020 dans la commune de Nogent-lès-Montbard, les onze sièges de conseillers municipaux ont été pourvus et les conseillers municipaux ont procédé à l'élection du maire de la commune et de ses adjoints le 23 mai 2020. Il résulte des mentions du procès-verbal que Mme D... A... a été proclamée maire à l'issue du second tour de scrutin. Mme A... relève appel du jugement du 21 juillet 2020 par lequel le tribunal administratif de Dijon a, sur déféré du préfet de la région Bourgogne Franche-Comté, préfet de la Côte-d'Or, annulé son élection et proclamé M. C... B... maire de Nogent-lès-Montbard.<br/>
<br/>
              2. En premier lieu, si aux termes de l'article R. 119 du code électoral, rendu applicable à l'élection du maire par l'article L. 2122-13 du code général des collectivités territoriales, " (...) la notification est faite, dans les trois jours de l'enregistrement de la protestation, aux conseillers dont l'élection est contestée (...) ", la circonstance que Mme A... n'ait reçu notification du déféré du préfet, enregistré le 5 juin 2020 au greffe du tribunal administratif de Dijon, que le 10 juin 2020 n'est pas de nature à entacher le jugement d'irrégularité dès lors qu'elle a disposé d'un délai suffisant pour se défendre utilement, ce qu'elle a au demeurant fait en l'espèce en déposant quatre mémoires en défense.<br/>
<br/>
              3. En deuxième lieu, aux termes de l'article L. 2122-7 du code général des collectivités territoriales : " Le maire est élu au scrutin secret et à la majorité absolue. Si, après deux tours de scrutin, aucun candidat n'a obtenu la majorité absolue, il est procédé à un troisième tour de scrutin et l'élection a lieu à la majorité relative ". La majorité absolue requise pour être élu maire au premier tour de scrutin et, si elle n'a pas été atteinte, au deuxième tour qui est alors organisé, se calcule, non par rapport à l'effectif légal du conseil municipal, mais en fonction du nombre des suffrages exprimés, les bulletins blancs ou nuls n'étant pas pris en compte pour la détermination des suffrages exprimés.<br/>
<br/>
              4. Il résulte de l'instruction qu'avant l'ouverture de la réunion du 23 mai 2020 au cours de laquelle les conseillers municipaux devaient élire le maire, Mme A..., maire sortante, a été prise à partie par un conseiller municipal au sujet de son absence lors d'une réunion portant sur la réouverture des écoles à la suite de la période de confinement. Mme A... s'est abstenue de présenter sa candidature lors du premier tour de scrutin et M. B..., seul candidat, a recueilli cinq voix sur huit suffrages exprimés et trois bulletins blancs, soit la majorité absolue des suffrages exprimés, ainsi que l'a jugé à bon droit le tribunal administratif sans que Mme A... le conteste d'ailleurs en appel. D'une part, contrairement à ce que soutient Mme A..., s'il résulte des mentions du procès-verbal que le conseil municipal a commis une erreur dans le calcul du nombre de suffrages nécessaires pour atteindre la majorité absolue, cette erreur n'a pu porter atteinte à la sincérité du scrutin. D'autre part, il ne résulte pas de l'instruction que le retentissement, pour Mme A... et pour les autres conseillers municipaux, de l'altercation survenue avant l'ouverture de la réunion, dont il n'est pas soutenu et dont il ne résulte pas de l'instruction qu'elle aurait été constitutive d'une manoeuvre, ait été tel qu'il ait entaché le scrutin d'insincérité.<br/>
<br/>
              5. En dernier lieu, il ne résulte pas de l'instruction que M. B... n'aurait pas satisfait aux obligations imposées par le service national et qu'il aurait été par suite atteint par l'inéligibilité édictée par l'article L. 45 du code électoral.<br/>
<br/>
              6. Par suite, Mme A... n'est pas fondée à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Dijon a annulé son élection en tant que maire de Nogent-lès-Montbard et proclamé M. B... élu à sa place.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : La requête de Mme A... est rejetée.<br/>
Article 2 : La présente décision sera notifiée à Mme D... A..., à M. C... B... et au ministre de l'intérieur.<br/>
Copie en sera adressée au préfet de la région Bourgogne - Franche-Comté, préfet de la Côte-d'Or.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
