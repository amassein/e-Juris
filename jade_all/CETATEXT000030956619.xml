<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030956619</ID>
<ANCIEN_ID>JG_L_2015_07_000000374687</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/95/66/CETATEXT000030956619.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère / 6ème SSR, 27/07/2015, 374687, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-07-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>374687</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère / 6ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Yannick Faure</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Alexandre Lallet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:374687.20150727</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              1° Par une requête, un nouveau mémoire et un mémoire en réplique, enregistrés les 16 et 17 janvier 2014 et le 4 mai 2015 au secrétariat du contentieux du Conseil d'Etat, la confédération des praticiens des hôpitaux demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir l'article 3 de l'arrêté du 8 novembre 2013 du ministre de l'économie et des finances, du ministre des affaires sociales et de la santé, du ministre de l'enseignement supérieur et de la recherche et du ministre délégué auprès du ministre de l'économie et des finances, chargé du budget, modifiant l'arrêté du 30 avril 2003 relatif à l'organisation et à l'indemnisation de la continuité des soins et de la permanence pharmaceutique dans les établissements publics de santé et les établissements publics d'hébergement pour personnes âgées dépendantes ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 2 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              2° Par une requête et un nouveau mémoire, enregistrés les 17 janvier et 26 février 2014 au secrétariat du contentieux du Conseil d'Etat, le syndicat national des praticiens hospitaliers anesthésistes réanimateurs élargi demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir l'arrêté du 8 novembre 2013 modifiant l'arrêté du 30 avril 2003 relatif à l'organisation et à l'indemnisation de la continuité des soins et de la permanence pharmaceutique dans les établissements publics de santé et les établissements publics d'hébergement pour personnes âgées dépendantes ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu :<br/>
              - la directive 2003/88/CE du Parlement européen et du Conseil du 4 novembre 2003 concernant certains aspects de l'aménagement du temps de travail ;<br/>
              - le code de la santé publique ;<br/>
              - le décret n° 84-135 du 24 février 1984 ;<br/>
              - le décret n° 95-569 du 6 mai 1995 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Yannick Faure, auditeur,  <br/>
<br/>
              - les conclusions de M. Alexandre Lallet, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant que la requête de la confédération des praticiens des hôpitaux et celle du syndicat national des praticiens hospitaliers anesthésistes réanimateurs élargi sont dirigées contre le même arrêté ; qu'il y a lieu de les joindre pour statuer par une seule décision ;<br/>
<br/>
              Sur la recevabilité des requêtes :<br/>
<br/>
              2. Considérant que l'arrêté attaqué, en tant que, par son article 2, il prévoit la possibilité de périodes de temps de travail additionnel, fixe la périodicité et les modalités de leur décompte, ouvre aux praticiens le choix entre rémunération, récupération ou versement au compte épargne-temps et précise la structure dans laquelle ces périodes s'effectuent, en rappelant au demeurant certaines des dispositions des statuts des différentes catégories de praticiens concernées, se borne à reprendre les termes de l'article 4 de l'arrêté du 30 avril 2003 modifié par arrêté du 18 novembre 2003, dont il remplace les dispositions ; que, dans cette mesure, il est purement confirmatif de dispositions qui, publiées au Journal officiel de la République française les 2 mai et 3 décembre 2003, sont devenues définitives ; qu'il en résulte que les conclusions dirigées contre l'arrêté attaqué sont, dans cette mesure, tardives et, par suite, irrecevables ; <br/>
<br/>
              Sur la compétence des auteurs de l'acte :<br/>
<br/>
              3. Considérant qu'aux termes du dernier alinéa de l'article 30 du décret du 24 février 1984 portant statut des personnels enseignants et hospitaliers des centres hospitaliers et universitaires, qui prévoit notamment le versement d'indemnités de participation à la permanence des soins pour les praticiens hospitaliers universitaires : " Le montant, les conditions d'attribution et les modalités de versement des indemnités mentionnées au présent article sont fixées par arrêté des ministres chargé de l'enseignement supérieur, de la santé et du budget " ; qu'en vertu des articles D. 6152-23-1, D. 6152-220-1 et D. 6152-417 du code de la santé publique, le montant, les conditions d'attribution et les modalités de versement des indemnités de sujétion correspondant au temps de travail accompli, dans le cadre des obligations de service hebdomadaires, la nuit, le samedi après-midi, le dimanche et les jours fériés, des indemnités forfaitaires pour tout temps de travail additionnel accompli, sur la base du volontariat, au-delà des obligations de service hebdomadaires et des indemnités correspondant aux astreintes et aux déplacements, prévues au bénéfice respectivement des praticiens hospitaliers à temps plein, des praticiens des hôpitaux à temps partiel et des praticiens contractuels, sont fixés par arrêté des ministres chargés du budget et de la santé ; qu'en vertu des articles D. 6152-514, D. 6152-539-4, D. 6152-612-1 et D. 6152-633-1 du même code et de l'article 23 du décret du 6 mai 1995, le montant et les modalités de versement des mêmes indemnités ou des indemnités de sujétion et des indemnités forfaitaires, prévues au bénéfice respectivement des assistants des hôpitaux, des assistants associés, des praticiens attachés, des praticiens attachés associés et des praticiens adjoints contractuels, sont fixés par arrêté des ministres chargés du budget, de la santé et de la sécurité sociale ; <br/>
<br/>
              4. Considérant qu'il résulte de ces dispositions que le ministre des affaires sociales et de la santé, le ministre de l'économie et des finances et le ministre de l'enseignement supérieur et de la recherche, auteurs de l'arrêté attaqué, avaient compétence pour modifier les règles d'indemnisation des activités des différentes catégories de praticiens mentionnées à l'article 9 de l'arrêté du 30 avril 2003 et participant à la permanence des soins dans les établissements publics de santé et les établissements publics d'hébergement pour personnes âgées dépendantes, établies par le même arrêté du 30 avril 2003 ; qu'à ce titre, ils avaient également compétence pour modifier les dispositions relatives à l'organisation du service indissociables des conditions d'attribution des indemnités considérées, notamment pour prévoir la conclusion d'un contrat préalablement à l'accomplissement d'un temps de travail additionnel, l'inscription des besoins prévisionnels de tels contrats dans le contrat de pôle, ainsi que les modalités de décompte de certains temps de travail en vue de leur indemnisation ;<br/>
<br/>
              5. Considérant, en revanche, que seul un décret en Conseil d'Etat, pris sur le fondement de l'article L. 6152-6 du code de la santé publique, pouvait prévoir des dispositions à caractère statutaire et, notamment, définir le temps de travail effectif et les repos auxquels ont droit les praticiens ; que sont ainsi entachées d'incompétence les dispositions du 2° de l'article 1er de l'arrêté attaqué qui prévoient que le temps d'intervention sur place et le temps de trajet réalisés au cours d'une astreinte constituent du temps de travail effectif et sont pris en compte pour l'attribution du repos quotidien, les dispositions de son article 2 qui prévoient la même règle pour les astreintes à domicile et garantissent au praticien le repos quotidien après la fin du dernier déplacement, celles de ses articles 3 et 4 qui prévoient cette règle tant pour les déplacements exceptionnels que pour les astreintes à domicile et garantissent également le repos quotidien après la fin du dernier déplacement ;<br/>
<br/>
              6. Considérant, en outre, que les auteurs de l'arrêté attaqué ne tenaient ni des dispositions mentionnées au point 2 ni d'aucune autre disposition le pouvoir d'imposer aux établissements publics de santé et aux établissements publics d'hébergement pour personnes âgées dépendantes, sur lesquels il ne leur appartient pas d'exercer les pouvoirs relevant d'un chef de service, des règles relatives à l'organisation des activités médicales et pharmaceutiques non plus que le respect d'obligations destinées à permettre le contrôle de la durée effective du travail qui ne seraient pas nécessaires au contrôle des conditions d'attribution des indemnités en cause ; que sont ainsi entachées d'incompétence les dispositions de l'article 1er de l'arrêté attaqué qui modifient la liste des activités organisées en temps médical continu ; qu'ont également été prises par des autorités incompétentes les dispositions de l'article 2 de l'arrêté qui prévoient que les registres de temps travaillé sont portés à la connaissance des services de santé au travail, confient un rôle de suivi du temps de travail additionnel à la commission relative à l'organisation de la permanence des soins et à la commission médicale d'établissement et imposent qu'un bilan annuel soit transmis à l'agence régionale de santé et que le bilan social de l'établissement comporte des données relatives au temps de travail additionnel ;<br/>
<br/>
              Sur les autres moyens :<br/>
<br/>
              7. Considérant, en premier lieu, que si ses articles 2 et 3 indiquent modifier un arrêté du " 30 avril 2013 " au lieu du " 30 avril 2003 ", cette erreur purement matérielle est sans influence sur la légalité de l'arrêté attaqué ;<br/>
<br/>
              8. Considérant, en deuxième lieu, que l'article 2 de la directive  2003/88/CE du Parlement européen et du Conseil du 4 novembre 2003 concernant certains aspects de l'aménagement du temps de travail définit le " temps de travail " comme " toute période durant laquelle le travailleur est au travail, à la disposition de l'employeur et dans l'exercice de son activité ou de ses fonctions " ; que l'article 6 de cette directive impose aux Etats membres de prendre les mesures nécessaires pour que " la durée moyenne de travail pour chaque période de sept jours n'excède pas quarante-huit heures, y compris les heures supplémentaires " ; que le 1 de son article 22 prévoit toutefois qu'" un État membre a la faculté de ne pas appliquer l'article 6 tout en respectant les principes généraux de la protection de la sécurité et de la santé des travailleurs et à condition qu'il assure, par les mesures nécessaires prises à cet effet, que : / a) aucun employeur ne demande à un travailleur de travailler plus de quarante-huit heures au cours d'une période de sept jours (...), à moins qu'il ait obtenu l'accord du travailleur pour effectuer un tel travail (...) " et qu'un certain nombre d'autres conditions soient remplies, destinées notamment à assurer la protection de la sécurité et de la santé des travailleurs ; qu'aux termes de l'article R. 6152-27 du code de la santé publique : " Le service hebdomadaire est fixé à dix demi-journées, sans que la durée du travail puisse excéder quarante-huit heures par semaine, cette durée étant calculé en moyenne sur une période de quatre mois. (...) Le praticien peut accomplir, sur la base du volontariat au-delà de ses obligations de service hebdomadaires, un temps de travail additionnel donnant lieu soit à récupération, soit au versement d'indemnités de participation à la continuité des soins et, le cas échéant, d'indemnités de temps de travail additionnel (...) " ; <br/>
<br/>
              9. Considérant que l'article 2 de l'arrêté attaqué prévoit que " lorsque, dans le cadre de la réalisation de ses obligations de service, le praticien a été conduit à dépasser le seuil maximal de quarante-huit heures hebdomadaires en moyenne lissée sur le quadrimestre, le temps de travail effectué au-delà est décompté en heures de temps de travail additionnel qui, cumulées par plages de cinq heures, sont converties en une demi-période de temps de travail additionnel " ; que ces dispositions, qui ont pour seul objet de prévoir les modalités de décompte en périodes de temps de travail additionnel du temps de travail effectué par un praticien au-delà du seuil maximal de quarante-huit heures hebdomadaires pour calculer les indemnités de temps de travail additionnel qui lui sont dues, n'ont pas pour objet et ne sauraient avoir pour effet de permettre l'emploi des praticiens hospitaliers dans des conditions méconnaissant les dispositions de l'article R. 6152-27 du code de la santé publique ; que, pour les mêmes motifs, ces dispositions ne sont pas incompatibles avec les objectifs de la directive 2003/88/CE ;<br/>
<br/>
              10. Considérant, en dernier lieu, que les dispositions de l'article 3 de l'arrêté attaqué définissent les conditions d'indemnisation des astreintes et des déplacements exceptionnels réalisés sans que le praticien ne soit d'astreinte à domicile, établissent, pour le calcul de l'indemnisation, les modalités de prise en compte des déplacements pendant les astreintes et prévoient un dispositif dérogatoire d'indemnisation forfaitaire de l'astreinte opérationnelle ou de l'astreinte de sécurité dans le cadre d'un contrat conclu entre le responsable de la structure concernée et le directeur de l'établissement ; que ces dispositions, qui se bornent à fixer les règles d'indemnisation des interventions et activités des praticiens, n'ont pas pour objet et ne sauraient avoir pour effet de déterminer ou de restreindre les conditions de prise en compte de ces périodes dans le temps de  travail effectif et ainsi de permettre l'emploi des praticiens dans des conditions méconnaissant les dispositions de l'article R. 6152-27 du code de la santé publique ; que, pour les mêmes motifs, elles ne sont pas incompatibles avec les objectifs des articles 2 et 6 de la directive 2003/88/CE, desquels il ressort clairement qu'ils ne concernent pas la rémunération des travailleurs ;<br/>
<br/>
              11. Considérant qu'il résulte de tout ce qui précède que la confédération des praticiens des hôpitaux et le syndicat national des praticiens hospitaliers anesthésistes réanimateurs élargi ne sont fondés à demander l'annulation que des dispositions des 1° et 2° de l'article 1er de l'arrêté qu'ils attaquent et des huitième, neuvième, treizième, vingtième et dernier alinéas de l'article 4, de la première phrase du troisième alinéa du II, des deuxième et sixième alinéas du III et du dernier alinéa du IV de l'article 14 et des quatrième et huitième alinéas de l'article 14 bis de l'arrêté du 30 avril 2003 dans la rédaction que leur donne l'arrêté attaqué, qui sont divisibles des autres dispositions de cet arrêté ;<br/>
<br/>
              Sur les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              12. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat une somme de 1 500 euros à verser au syndicat national des praticiens hospitaliers anesthésistes réanimateurs élargi au titre de ces dispositions ; qu'en revanche, il n'y a pas lieu de faire droit aux conclusions présentées au même titre par la confédération des praticiens des hôpitaux ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Les 1° et 2° de l'article 1er de l'arrêté du 8 novembre 2013 modifiant l'arrêté du 30 avril 2003 relatif à l'organisation et à l'indemnisation de la continuité des soins et de la permanence pharmaceutique dans les établissements publics de santé et les établissements publics d'hébergement pour personnes âgées dépendantes et les huitième, neuvième, treizième, vingtième et dernier alinéas de l'article 4, la première phrase du troisième alinéa du II, les deuxième et sixième alinéas du III et le dernier alinéa du IV de l'article 14 et les quatrième et huitième alinéas de l'article 14 bis de l'arrêté du 30 avril 2003 dans la rédaction que leur donne l'arrêté du 8 novembre 2013 sont annulés.<br/>
Article 2 : L'Etat versera une somme de 1 500 euros au syndicat national des praticiens hospitaliers anesthésistes réanimateurs élargi.<br/>
Article 3 : Le surplus des conclusions des requêtes de la confédération des praticiens des hôpitaux et du syndicat national des praticiens hospitaliers anesthésistes réanimateurs élargi est rejeté.<br/>
Article 4 : La présente décision sera notifiée à la confédération des praticiens des hôpitaux, au syndicat national des praticiens hospitaliers anesthésistes réanimateurs élargi, à la ministre des affaires sociales, de la santé et des droits des femmes, au ministre des finances et des comptes publics et à la ministre de l'éducation nationale, de l'enseignement supérieur et de la recherche. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
