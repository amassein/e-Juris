<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032659092</ID>
<ANCIEN_ID>JG_L_2015_09_000000389181</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/65/90/CETATEXT000032659092.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème SSJS, 23/09/2015, 389181, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-09-23</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>389181</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème SSJS</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>OCCHIPINTI</AVOCATS>
<RAPPORTEUR>M. Arno Klarsfeld</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Bertrand Dacosta</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2015:389181.20150923</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              M. B...A...a demandé au tribunal administratif de Montreuil, d'une part, d'annuler l'arrêté du 5 mars 2013 par lequel le préfet de la Seine-Saint-Denis a refusé de l'admettre au séjour, lui a fait obligation de quitter le territoire français dans un délai de trente jours et a fixé le pays à destination duquel il pourrait être reconduit d'office à l'expiration de ce délai et, d'autre part, à ce qu'il soit enjoint au préfet de la Seine-Saint-Denis de lui délivrer une autorisation provisoire de séjour l'autorisant à travailler ou un titre de séjour, dans un délai de deux mois à compter de la notification de l'arrêt à intervenir, sous astreinte de 150 euros par jour de retard, sur le fondement des articles L. 911-1 et L. 911-2 du code de justice administrative. <br/>
<br/>
              Par un jugement n° 1303883 du 23 septembre 2013, le tribunal administratif de Montreuil a rejeté sa requête.<br/>
<br/>
              Par une ordonnance n° 14VE03033 du 9 décembre 2014, la cour administrative d'appel de Versailles a rejeté l'appel formé par M.A.... <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 2 avril et 1er juillet 2015 au secrétariat du contentieux du Conseil d'Etat, M. A...demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat le versement de la somme de 3 500 euros au profit de M. A...qui déclare renoncer à l'aide de l'Etat en application des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ; <br/>
<br/>
              Vu le code de l'entrée et du séjour des étrangers et du droit d'asile ; <br/>
<br/>
              Vu la loi n° 91-647 du 10 juillet 1991 relative à l'aide juridique, modifiée ;<br/>
<br/>
              Vu le décret n° 91-1266 du 19 décembre 1991 portant application de la loi n° 91-647 du 10 juillet 1991 relative à l'aide juridique, modifiée ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Arno Klarsfeld, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Bertrand Dacosta, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Occhipinti, avocat de M. A...;<br/>
<br/>
<br/>
<br/>1. Considérant, d'une part, qu'aux termes de l'article R. 811-2 du code de justice administrative : " Sauf disposition contraire, le délai d'appel est de deux mois. Il court contre toute partie à l'instance à compter du jour où la notification a été faite à cette partie dans les conditions prévues aux articles R. 751-3 et R. 751-4 " ; qu'aux termes de l'article 39 du décret du 19 décembre 1991 susvisé : " Lorsqu'une demande d'aide juridictionnelle en vue de se pourvoir en matière civile devant la Cour de cassation est adressée au bureau d'aide juridictionnelle établi près cette juridiction avant l'expiration du délai imparti pour le dépôt du pourvoi ou des mémoires, ce délai est interrompu. Un nouveau délai court à compter du jour de la réception par l'intéressé de la notification de la décision du bureau d'aide juridictionnelle ou, si elle est plus tardive, de la date à laquelle un auxiliaire de justice a été désigné. " ;<br/>
<br/>
              2. Considérant, d'autre part, que le 4° de l'article R. 222-1 du code de justice administrative permet aux présidents de tribunal administratif et de cour administrative d'appel, aux vice-présidents du tribunal administratif de Paris et aux présidents des formations de jugement des tribunaux administratifs et des cours administratives d'appel de rejeter par ordonnance les requêtes entachées d'une irrecevabilité manifeste non susceptible d'être couverte en cours d'instance, au nombre desquelles sont les requêtes tardives ;<br/>
<br/>
              3. Considérant que, par une ordonnance prise sur le fondement de l'article R. 222-1 du code de justice administrative, le président de la 4ème chambre de la cour administrative d'appel de Versailles a rejeté comme entachée d'une irrecevabilité manifeste insusceptible d'être couverte en cours d'instance la requête présentée par M.A..., par le motif, relevé d'office, que M. A...a sollicité l'aide juridictionnelle le 13 décembre 2013, soit après l'expiration du délai d'un mois qui lui était imparti à compter du 26 septembre 2013, date à laquelle le jugement attaqué lui a été notifié et qu'ainsi la requête de M. A...enregistrée au greffe de la cour le 30 octobre 2013 ne pouvait qu'être rejetée comme tardive ; que, toutefois, il ressort des pièces du dossier d'appel, en particulier de l'attestation de dépôt de sa demande d'aide juridictionnelle, que M. A...avait saisi le bureau d'aide juridictionnelle le 7 octobre 2013 ; que cette demande d'aide juridictionnelle présentée dans le délai imparti a eu pour effet d'interrompre le délai d'appel ; que dans ces conditions, l'appel de M. A...enregistré le 30 octobre 2014 ne pouvait être regardé comme tardif ; que, dès lors, l'ordonnance attaquée doit être annulée ;<br/>
<br/>
              4. Considérant que, dans les circonstances de l'espèce, il n'y a pas lieu de faire droit à la demande présentée sur le fondement des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'ordonnance de la cour administrative d'appel de Versailles du 9 décembre 2014 est annulée.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Versailles.<br/>
Article 3 : Le surplus des conclusions de M. A...est rejeté.<br/>
Article 4 : La présente décision sera notifiée à M. B...A....<br/>
Copie en sera adressée pour information au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
