<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033285510</ID>
<ANCIEN_ID>JG_L_2016_10_000000403546</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/28/55/CETATEXT000033285510.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 13/10/2016, 403546, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-10-13</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>403546</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP SEVAUX, MATHONNET</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2016:403546.20161013</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête enregistrée le 15 septembre 2016 au secrétariat du contentieux du Conseil d'Etat, l'association Avenir Haute Durance, les communes de Puy-Saint-Eusèbe, de Réallon, de Châteauroux-Les-Alpes, de Puy-Sanières, de la Bâtie-Neuve, les associations Société alpine de protection de la nature, France nature environnement PACA, Hautes-Alpes nature environnement, " Les Haut des Granes " et Curl'air Parapente, Jennif'AIR - école de parapente, M. B...A..., Mme C...E...et M. B... -G... D...demandent au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative :<br/>
<br/>
              1°) d'ordonner la suspension de l'exécution de l'arrêté du 6 octobre 2014 par lequel la ministre de l'écologie, du développement durable et de l'énergie et la ministre du logement, de l'égalité des territoires et de la ruralité ont déclaré d'utilité publique, en vue de l'institution de servitudes, les travaux de construction d'une ligne électrique aérienne à 225  000 volts entre les postes de Grisolles et de Pralong, sur le territoire des communes de Chorges, Embrun, La Bâtie-Neuve, La Rochette, Prunières, Puy-Saint-Eusèbe, Puy-Sanières, Saint-Apollinaire, Savines-le-Lac dans le département des Hautes-Alpes ; <br/>
<br/>
              2°) d'adresser à Réseau Transports Electricité Sud-Est (RTE) une injonction précisant les obligations qu'impose la suspension prononcée, sous astreinte d'un montant de 200 000 euros par jour de retard à compter de la notification de l'ordonnance ;<br/>
<br/>
              3°) de mettre à la charge de RTE la somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Les requérants soutiennent que : <br/>
              - la condition d'urgence est remplie dès lors que les travaux ont commencé sur le secteur du tracé des lignes à très haute tension P4, P6 et P3, que l'ensemble des travaux est accéléré du fait de la dispense du permis de construire, que le défrichement sur la commune de Savines pourrait rendre la situation irréversible en ce que les arbres mettent plus d'une cinquantaine d'années à repousser et que les pylônes sont sur le point d'être installés ;<br/>
              - il existe un doute sérieux sur la légalité de l'arrêté attaqué ;<br/>
              - le dossier d'enquête publique méconnaît les dispositions des 3° et 4° de l'article R. 123-8 du code de l'environnement ; <br/>
              - l'étude d'impact est insuffisante au regard des dispositions de l'article R. 122-5 du même code en ce qu'elle ne tient pas compte de l'implantation et des caractéristiques des pylônes, en ce qu'elle ne comporte aucune évaluation des conséquences sur les nappes phréatiques et l'alimentation en eau potable et en ce qu'elle ne comporte pas d'étude des solutions de substitution envisagées par le maître d'ouvrage ; <br/>
              - eu égard au caractère indissociable des six projets de rénovation du réseau électrique de la Haute-Durance P1 à P6, ceux-ci ne pouvaient faire l'objet de déclarations d'utilité publique séparées, ni d'enquêtes publiques ou d'études d'impact distinctes ;<br/>
              - la déclaration d'utilité publique contestée accorde à RTE des droits en méconnaissance de l'article 17 de la Déclaration des droits de l'homme et du citoyen, de l'article 1er du premier protocole additionnel à la Convention européenne des droits de l'homme et de sauvegarde des libertés fondamentales et aux articles 6 et 13 de cette convention ; <br/>
              - le projet et le dimensionnement du réseau procèdent d'une erreur manifeste d'appréciation ; <br/>
              - le projet porte une atteinte excessive à l'environnement.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la Constitution, et notamment son préambule ;<br/>
              - le code de l'énergie ;<br/>
              - le décret n° 70-492 du 11 juin 1970 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision " ; qu'en vertu de l'article L. 522-3 du même code, le juge des référés peut, par une ordonnance motivée, rejeter une requête sans instruction ni audience lorsque la condition d'urgence n'est pas remplie ou lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable ou qu'elle est mal fondée ; <br/>
<br/>
              2. Considér	ant que le prononcé de la suspension d'un acte administratif est subordonné notamment à une condition d'urgence ; que l'urgence justifie la suspension de l'exécution d'un acte administratif lorsque celui-ci porte atteinte de manière suffisamment grave et immédiate, à un intérêt public, à la situation du requérant ou aux intérêts qu'il entend défendre ; qu'il appartient au juge des référés d'apprécier concrètement, compte tenu des justifications fournies par le requérant, si les effets de l'acte contesté sont de nature à caractériser une urgence justifiant que, sans attendre le jugement de la requête au fond, l'exécution de la décision soit suspendue ; que l'urgence doit être appréciée objectivement et compte tenu de l'ensemble des circonstances de l'affaire ;<br/>
<br/>
              3. Considérant que la procédure conduisant à l'établissement de servitudes pour le passage de lignes électriques comporte plusieurs étapes ; que la déclaration d'utilité publique d'ouvrages d'électricité en vue de l'établissement de servitudes ne permet pas à elle seule l'institution de celles-ci ; que le décret du 11 juin 1970, pris pour l'application de l'article 35 de la loi du 8 avril 1946 concernant la procédure de déclaration d'utilité publique des travaux d'électricité et de gaz qui ne nécessitent que l'établissement de servitudes ainsi que les conditions d'établissement desdites servitudes, qui les régissait et dont les dispositions ont été reprises dans la partie réglementaire du code de l'énergie, prévoit que, postérieurement à la déclaration d'utilité publique, les servitudes sont instituées par un arrêté préfectoral dont l'intervention est précédée d'une enquête publique de type parcellaire ; qu'ainsi, dès lors que l'arrêté interministériel du 6 octobre 2014 contesté se borne à déclarer d'utilité publique, en vue de l'établissement de servitudes, les travaux de construction de la ligne électrique aérienne à 225 000 volts entre les postes de Grisolles et de Pralong, sur le territoire des communes de Chorges, Embrun, La Bâtie-Neuve, La Rochette, Prunières, Puy-Saint-Eusèbe, Puy-Sanières, Saint-Apollinaire, Savines-le-Lac dans le département des Hautes-Alpes, les demandes tendant à la suspension de cet arrêté, ne présentent pas, par elles-mêmes, un caractère d'urgence au sens des dispositions précitées de l'article L. 521-1 du code de justice administrative ;<br/>
<br/>
              4. Considérant qu'il résulte de ce qui précède que les requérants ne sont pas fondés à demander la suspension de l'exécution de l'arrêté contesté ; que la requête, y compris les conclusions à fin d'injonction et celles tendant à l'application de l'article L. 761-1 du code de justice administrative, ne peut, par suite, qu'être rejetée selon la procédure prévue par l'article L. 522-3 de ce code ;<br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de l'association Avenir Haute Durance et autres est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à l'association Avenir Haute Durance, les communes de Puy-Saint-Eusèbe, de Réallon, de Châteauroux-Les-Alpes, de Puy-Sanières, de la Bâtie-Neuve, les associations société alpine de protection de la nature, France nature environnement PACA, Hautes-Alpes nature environnement, " Les Haut des Granes " et Curl'air Parapente, Jennif'AIR - école de parapente, M. B...A..., Mme C...E...et M. F... D....<br/>
Copie en sera adressée à la ministre de l'environnement, de l'énergie et de la mer, chargée des relations internationales sur le climat et à la ministre du logement et de l'habitat durable.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
