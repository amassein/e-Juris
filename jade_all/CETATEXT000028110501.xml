<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028110501</ID>
<ANCIEN_ID>JG_L_2013_10_000000362715</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/11/05/CETATEXT000028110501.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème et 5ème sous-sections réunies, 23/10/2013, 362715</TITRE>
<DATE_DEC>2013-10-23</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>362715</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème et 5ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Bruno Bachini</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Gaëlle Dumortier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:362715.20131023</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi, enregistré le 12 septembre 2012 au secrétariat du contentieux du Conseil d'Etat, présenté par le ministre de l'éducation nationale ; le ministre demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 11BX01431 du 10 juillet 2012 par lequel la cour administrative d'appel de Bordeaux a rejeté son recours tendant, d'une part, à l'annulation du jugement n° 1002419 du 7 avril 2011 par lequel le tribunal administratif de Pau a annulé pour excès de pouvoir la décision de l'inspecteur d'académie des Pyrénées-Atlantiques du 5 novembre 2010 attribuant des crédits pour créer un emploi de vie scolaire au sein de l'école primaire de Saint-Palais où est scolarisé BixenteB..., en exécution de la décision de la commission des droits et de l'autonomie des personnes handicapées du 1er octobre 2010 accordant à ce dernier le concours d'un auxiliaire de vie scolaire individuel, et, d'autre part, au rejet de la demande de première instance de M. et Mme B...; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de l'éducation ;<br/>
<br/>
              Vu le décret n° 2003-484 du 6 juin 2003 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Bruno Bachini, Maître des Requêtes,  <br/>
<br/>
              - les conclusions de Mme Gaëlle Dumortier, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'aux termes de l'article L. 351-3 du code de l'éducation, relatif à la scolarité des enfants handicapés, dans sa version alors en vigueur : " Lorsque la commission mentionnée à l'article L. 146-9 du code de l'action sociale et des familles constate qu'un enfant peut être scolarisé (...) à condition de bénéficier d'une aide individuelle dont elle détermine la quotité horaire, cette aide peut être apportée par un assistant d'éducation recruté conformément au sixième alinéa de l'article L. 916-1. / Les assistants d'éducation affectés aux missions d'aide à l'accueil et à l'intégration scolaires des enfants handicapés sont recrutés par l'inspecteur d'académie, directeur des services départementaux de l'éducation nationale. Si l'aide individuelle nécessaire à l'enfant handicapé ne comporte pas de soutien pédagogique, ces assistants peuvent être recrutés sans condition de diplôme. Ils reçoivent une formation adaptée. (...) " ; qu'aux termes du sixième alinéa de l'article L. 916-1 du même code, dans sa version alors en vigueur : " (...) des assistants d'éducation peuvent être recrutés par l'Etat pour exercer des fonctions d'aide à l'accueil et à l'intégration des élèves handicapés dans les conditions prévues à l'article L. 351-3 (...) " ; qu'aux termes du premier alinéa de l'article 3 du décret du 6 juin 2003 fixant les conditions de recrutement et d'emploi des assistants d'éducation : " Les candidats aux fonctions d'assistant d'éducation doivent être titulaires du baccalauréat, ou d'un titre ou diplôme de niveau IV au sens de l'article L. 335-6 du code de l'éducation susvisé, ou d'un titre ou diplôme de niveau égal ou supérieur. Les candidats recrutés en application du sixième alinéa de l'article L. 916-1 du code de l'éducation qui justifient d'une expérience de trois ans de service dans le domaine de l'aide à l'intégration scolaire des élèves handicapés (...) sont dispensés de cette condition. " ; <br/>
<br/>
              2. Considérant que, s'il résulte de la combinaison de ces dispositions que l'aide individuelle accordée aux élèves handicapés par la commission des droits et de l'autonomie des personnes handicapées peut être confiée aux assistants d'éducation, sans qu'aucune condition de diplôme puisse leur être opposée lorsque leur mission n'inclut pas un soutien pédagogique ou lorsqu'ils justifient d'au moins trois années d'expérience dans ce domaine, elles ne font pas obstacle à ce que cette aide soit également assurée, dans tous les cas, par d'autres catégories de personnels recrutés à cet effet par l'Etat ; que ces personnels doivent justifier de conditions de formation ou d'expérience adaptées à l'exercice des tâches qui leur sont confiées, en particulier lorsqu'elles comportent un soutien pédagogique à l'élève concerné ;    <br/>
<br/>
              3. Considérant que, pour annuler la décision de l'inspecteur d'académie des Pyrénées-Atlantiques du 5 novembre 2010 créant un emploi de vie scolaire en exécution de la décision de la commission des droits et de l'autonomie des personnes handicapées accordant à M. et Mme B...le concours, en faveur de leur fils, d'un auxiliaire de vie scolaire individuel pendant les cours à raison de douze heures par semaine, la cour administrative d'appel de Bordeaux s'est fondée sur ce que, dans le cas où l'aide apportée est de nature pédagogique, l'auxiliaire de vie scolaire chargé d'accompagner l'élève ne peut qu'être un assistant d'éducation remplissant la condition de diplôme ou d'expérience professionnelle requise par les dispositions du décret du 6 juin 2003 précédemment citées ; qu'il résulte de ce qui a été dit plus haut qu'elle a, ce faisant, commis une erreur de droit ; que le ministre de l'éducation nationale est, dès lors, fondé à demander l'annulation de l'arrêt attaqué ; <br/>
<br/>
<br/>
<br/>
<br/>			D E C I D E :<br/>
              			--------------<br/>
<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Bordeaux du 10 juillet 2012 est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Bordeaux. <br/>
Article 3 : La présente décision sera notifiée au ministre de l'éducation nationale et à M. et Mme A...B....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">04-02-04 AIDE SOCIALE. DIFFÉRENTES FORMES D'AIDE SOCIALE. AIDE SOCIALE AUX PERSONNES HANDICAPÉES. - AIDE INDIVIDUELLE ACCORDÉE AUX ÉLÈVES HANDICAPÉS PAR LA COMMISSION DES DROITS ET DE L'AUTONOMIE DES PERSONNES HANDICAPÉES - 1) POSSIBILITÉ POUR DES PERSONNELS AUTRES QUE LES ASSISTANTS D'ÉDUCATION D'ASSURER CETTE AIDE - EXISTENCE - 2) CONDITIONS - JUSTIFICATION PAR CES PERSONNELS D'UNE FORMATION OU D'UNE EXPÉRIENCE ADAPTÉES À L'EXERCICE DES TÂCHES QUI LEUR SONT CONFIÉES.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">30-01-02 ENSEIGNEMENT ET RECHERCHE. QUESTIONS GÉNÉRALES. QUESTIONS GÉNÉRALES RELATIVES AU PERSONNEL. - FOURNITURE DE L'AIDE INDIVIDUELLE AUX ÉLÈVES HANDICAPÉS DÉCIDÉE PAR LA COMMISSION DES DROITS ET DE L'AUTONOMIE DES PERSONNES HANDICAPÉES - 1) POSSIBILITÉ POUR DES PERSONNELS AUTRES QUE LES ASSISTANTS D'ÉDUCATION D'ASSURER CETTE AIDE - EXISTENCE - 2) CONDITIONS - JUSTIFICATION PAR CES PERSONNELS D'UNE FORMATION OU D'UNE EXPÉRIENCE ADAPTÉES À L'EXERCICE DES TÂCHES QUI LEUR SONT CONFIÉES.
</SCT>
<ANA ID="9A"> 04-02-04 1) S'il résulte de la combinaison des articles L. 351-3 et L. 916-1 du code de l'éducation, dans leur rédaction antérieure à l'entrée en vigueur de la loi n° 2011-1977 du 28 décembre 2011 de finances pour 2012, et du premier alinéa de l'article 3 du décret n° 2003-484 du 6 juin 2003 fixant les conditions de recrutement et d'emploi des assistants d'éducation, que l'aide individuelle accordée aux élèves handicapés par la commission des droits et de l'autonomie des personnes handicapées peut être confiée aux assistants d'éducation, sans qu'aucune condition de diplôme puisse leur être opposée lorsque leur mission n'inclut pas un soutien pédagogique ou lorsqu'ils justifient d'au moins trois années d'expérience dans ce domaine, ces mêmes dispositions ne font pas obstacle à ce que cette aide soit également assurée, dans tous les cas, par d'autres catégories de personnels recrutés à cet effet par l'Etat.... ,,2) Ces personnels autres que les assistants d'éducation doivent justifier de conditions de formation ou d'expérience adaptées à l'exercice des tâches qui leur sont confiées, en particulier lorsqu'elles comportent un soutien pédagogique à l'élève concerné.</ANA>
<ANA ID="9B"> 30-01-02 1) S'il résulte de la combinaison des articles L. 351-3 et L. 916-1 du code de l'éducation, dans leur rédaction antérieure à l'entrée en vigueur de la loi n° 2011-1977 du 28 décembre 2011 de finances pour 2012, et du premier alinéa de l'article 3 du décret n° 2003-484 du 6 juin 2003 fixant les conditions de recrutement et d'emploi des assistants d'éducation, que l'aide individuelle accordée aux élèves handicapés par la commission des droits et de l'autonomie des personnes handicapées peut être confiée aux assistants d'éducation, sans qu'aucune condition de diplôme puisse leur être opposée lorsque leur mission n'inclut pas un soutien pédagogique ou lorsqu'ils justifient d'au moins trois années d'expérience dans ce domaine, ces mêmes dispositions ne font pas obstacle à ce que cette aide soit également assurée, dans tous les cas, par d'autres catégories de personnels recrutés à cet effet par l'Etat.... ,,2) Ces personnels autres que les assistants d'éducation doivent justifier de conditions de formation ou d'expérience adaptées à l'exercice des tâches qui leur sont confiées, en particulier lorsqu'elles comportent un soutien pédagogique à l'élève concerné.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
