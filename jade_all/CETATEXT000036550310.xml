<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036550310</ID>
<ANCIEN_ID>JG_L_2018_01_000000408256</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/55/03/CETATEXT000036550310.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème - 9ème chambres réunies, 26/01/2018, 408256, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-01-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>408256</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème - 9ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP SPINOSI, SUREAU</AVOCATS>
<RAPPORTEUR>Mme Marie Gautier-Melleray</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Aurélie Bretonneau</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:408256.20180126</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme B...A...a demandé à la Cour nationale du droit d'asile d'annuler la décision du 31 mai 2016 par laquelle l'Office français de protection des réfugiés et apatrides (OFPRA) a rejeté sa demande d'asile. Par une décision n° 16023924 du 19 décembre 2016, la Cour nationale du droit d'asile a rejeté cette demande. <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 22 février et 22 mai 2017 au secrétariat du contentieux du Conseil d'Etat, Mme A...demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cette décision ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à la demande qu'elle avait présentée devant la Cour ; <br/>
<br/>
              3) de mettre à la charge de l'OFPRA la somme de 4 000 euros en application de l'article L.761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la convention de Genève du 28 juillet 1951 relative au statut des réfugiés et le protocole signé à New York le 31 janvier 1967 relatif au statut des réfugiés ;<br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              - le code civil ; <br/>
              - le code de justice administrative ; <br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Marie Gautier-Melleray, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Aurélie Bretonneau, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Spinosi, Sureau, avocat de Mme B...A...;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis à la Cour nationale du droit d'asile que Mme A..., ressortissante du Kosovo, s'est vu refuser la qualité de réfugié et le bénéfice de la protection subsidiaire par une décision du 31 mai 2016 de l'OFPRA. Elle se pourvoit en cassation contre la décision du 19 décembre 2016 par laquelle la Cour nationale du droit d'asile a rejeté sa demande d'annulation de cette décision.<br/>
<br/>
              2. Aux termes des stipulations du paragraphe A, 2° de l'article 1er de la convention de Genève du 28 juillet 1951 et du protocole signé à New York le 31 janvier 1967, doit être considérée comme réfugiée toute personne qui " craignant avec raison d'être persécutée du fait de sa race, de sa religion, de sa nationalité, de son appartenance à un certain groupe social ou de ses opinions politiques, se trouve hors du pays dont elle a la nationalité et qui ne peut, ou, du fait de cette crainte, ne veut se réclamer de la protection de ce pays ". Aux termes de l'article L. 712-1 du code de l'entrée et du séjour des étrangers et du droit d'asile " Le bénéfice de la protection subsidiaire est accordé à toute personne qui ne remplit pas les conditions pour se voir reconnaître la qualité de réfugié et pour laquelle il existe des motifs sérieux et avérés de croire qu'elle courrait dans son pays un risque réel de subir l'une des atteintes graves suivantes : (...) / b) La torture ou des peines ou traitements inhumains ou dégradants / (...) ". <br/>
<br/>
              3. Il appartient à la Cour nationale du droit d'asile, qui statue comme juge de plein contentieux sur le recours d'un demandeur d'asile dont la demande a été rejetée par l'OFPRA, de se prononcer elle-même sur le droit de l'intéressé à la qualité de réfugié ou au bénéfice de la protection subsidiaire au vu de l'ensemble des circonstances de fait dont elle a connaissance au moment où elle statue. A ce titre, il lui revient, pour apprécier la réalité des risques invoqués par le demandeur ou l'application du principe d'unité de la famille, de prendre en compte l'ensemble des pièces que celui-ci produit à l'appui de ses prétentions. En particulier, lorsque le demandeur produit devant elle un acte de l'état civil dressé en France, il lui incombe de tirer les conséquences du caractère authentique de cet acte, sauf fraude l'entachant ou, en cas de difficulté sérieuse soulevée par une question d'état des personnes et notamment en cas de fraude seulement soupçonnée, à renvoyer cette question au juge judiciaire.  <br/>
<br/>
              4. Pour rejeter la demande de MmeA..., la Cour nationale du droit d'asile s'est notamment fondée sur la circonstance que les déclarations de l'intéressée quant au lien de filiation entre sa fille et son compagnon seraient peu crédibles eu égard aux incohérences relevées entre les dates de rencontres avec son compagnon indiquées par l'intéressée à l'audience et celle de la naissance de sa fille sans même mentionner l'acte de naissance dont la copie intégrale figurait au dossier et qui établissait cette filiation. En remettant en cause les énonciations de cet acte de naissance sans, soit relever une fraude établie par les pièces du dosser, soit renvoyer à l'autorité judiciaire le soin de se prononcer sur le lien de filiation qu'établissait cet acte, la Cour a méconnu son office et entaché sa décision d'une erreur de droit.<br/>
<br/>
              5. Il résulte de ce qui précède, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, que Mme A...est fondée à demander l'annulation de la décision du 19 décembre 2016 de la Cour nationale du droit d'asile. <br/>
<br/>
              6. Il y a lieu, dans les circonstances de l'espèce de mettre à la charge de l'OFPRA la somme de 3 000 euros à verser à MmeA..., au titre de l'article L. 761-1 du code de justice administrative.  <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
       --------------<br/>
Article 1er : La décision du 19 décembre 2016 de la Cour nationale du droit d'asile est annulée.<br/>
<br/>
Article 2 : L'affaire est renvoyée à la Cour nationale du droit d'asile.<br/>
<br/>
Article 3 : L'Office français de protection des réfugiés et apatrides versera une somme de 3 000 euros à Mme A...au titre de l'article L. 761-1 du code de justice administrative. <br/>
Article 4 : La présente décision sera notifiée à Mme B...A...et à l'Office français de protection des réfugiés et apatrides. <br/>
Copie en sera adressée au ministre d'Etat, ministre de l'intérieur.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
