<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000025744409</ID>
<ANCIEN_ID>JG_L_2012_04_000000324918</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/25/74/44/CETATEXT000025744409.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème et 3ème sous-sections réunies, 24/04/2012, 324918, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2012-04-24</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>324918</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème et 3ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Christian Vigouroux</PRESIDENT>
<AVOCATS>SCP BARADUC, DUHAMEL</AVOCATS>
<RAPPORTEUR>M. Jean-Marc Anton</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Laurent Olléon</COMMISSAIRE_GVT>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 9 février et 7 mai 2009 au secrétariat du contentieux du Conseil d'Etat, présentés pour la SA EUROGRAM, dont le siège est 267 rue Lecourbe à Paris (75015), représentée par son président-directeur général en exercice ; elle demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'article 3 de l'arrêt n° 07PA001606, 07PA01603 du 3 décembre 2008 par lequel la cour administrative d'appel de Paris a rejeté le surplus des conclusions de ses requêtes tendant à l'annulation, d'une part, du jugement n° 0010617 du 5 mars 2007 du tribunal administratif de Paris rejetant sa demande tendant à la décharge des cotisations supplémentaires d'impôt sur les sociétés auxquelles elle a été assujettie au titre des années 1993 et 1994, à la restitution de précomptes mobiliers acquittés au titre des années 1993 à 1997 et à la réduction des cotisations primitives d'impôt sur les sociétés auxquelles elle a été assujettie au titre des années 1993 à 1996, et, d'autre part, du jugement n° 0011894 du 5 mars 2007 du tribunal administratif de Paris rejetant sa demande tendant à la décharge des rappels de taxe sur la valeur ajoutée mis à sa charge au titre des périodes couvrant les années 1993 et 1995 et à leur décharge, leur restitution ou leur réduction ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit intégralement à son appel ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat le versement de la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code général des impôts et le livre des procédures fiscales ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Marc Anton, Maître des Requêtes, <br/>
<br/>
              - les observations de la SCP Baraduc, Duhamel, avocat de la SA EUROGRAM, <br/>
<br/>
              - les conclusions de M. Laurent Olléon, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Baraduc, Duhamel, avocat de la SA EUROGRAM ;<br/>
<br/>
<br/>
<br/>
              Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que la SA EUROGRAM exerce son activité dans le secteur de la recherche scientifique ; qu'elle a fait l'objet d'une vérification de comptabilité portant sur les exercices clos au cours des années 1993 à 1995 à l'issue de laquelle l'administration a réintégré aux résultats des produits de participation ainsi que diverses charges et a procédé à des rappels de taxe sur la valeur ajoutée ; qu'à l'issue de ce contrôle, des cotisations supplémentaires d'impôt sur les sociétés ont été établies au titre des années 1993 à 1995 et des rappels de taxe sur la valeur ajoutée ont été mis en recouvrement le 30 septembre 1997 ; que, par deux réclamations en date du 23 novembre 1999 et du 27 décembre 1999, la société a sollicité la décharge de ces impositions ainsi que la réduction des cotisations primitives d'impôt sur les sociétés payées au titre des années 1993 à 1996 et la restitution du précompte mobilier, prévu par l'article 223 sexies du code général des impôts alors en vigueur, qu'elle avait acquitté ; qu'à la suite du rejet de ses demandes, elle a porté le litige devant le tribunal administratif de Paris qui a rejeté l'ensemble de ses conclusions ; qu'elle se pourvoit en cassation contre l'arrêt en date du 3 décembre 2008 par lequel la cour administrative d'appel de Paris a rejeté le surplus des conclusions de sa requête tendant à la décharge de ces impositions supplémentaires, à la réduction des cotisations primitives et à la restitution du précompte mobilier ;<br/>
<br/>
              Sur le pourvoi en tant qu'il porte sur les rappels de taxe sur la valeur ajoutée :<br/>
<br/>
              Considérant que le moyen que soulève la SA EUROGRAM à l'appui de ses conclusions tendant à l'annulation de l'arrêt attaqué en ce qu'il statue sur les rappels de taxe sur la valeur ajoutée est tiré de ce que la cour a omis de répondre au moyen tiré de ce que l'administration n'avait pas déduit du rappel de taxe sur la valeur ajoutée mis à sa charge au titre de l'année 1995 un crédit de taxe, d'un montant de 133 268 F (20 317 euros), dont elle disposait au 31 décembre 1995 ; que, par un avis en date du 17 février 2011, l'administration a prononcé un dégrèvement à hauteur de ce montant et, par suite, a fait droit au fond à la contestation de la société ; que, dès lors et dans cette mesure, il n'y a plus lieu de statuer sur les conclusions du pourvoi de la SA EUROGRAM ; <br/>
<br/>
              Sur les motifs de l'arrêt relatifs au précompte mobilier :<br/>
<br/>
              Considérant, d'une part, qu'aux termes du I de l'article 158 bis du code général des impôts alors en vigueur : " Les personnes qui perçoivent des dividendes distribués par des sociétés françaises disposent à ce titre d'un revenu constitué : / a) par les sommes qu'elles reçoivent de la société ; / b) par un avoir fiscal représenté par un crédit ouvert sur le Trésor. / Ce crédit d'impôt est égal à la moitié des sommes effectivement versées par la société. / Il ne peut être utilisé que dans la mesure où le revenu est compris dans la base de l'impôt sur le revenu dû par le bénéficiaire / Il est reçu en paiement de l'impôt (...) " ; qu'aux termes du 1 de l'article 223 sexies du même code alors en vigueur : " (...) lorsque les produits distribués par une société sont prélevés sur des sommes à raison desquelles elle n'a pas été soumise à l'impôt sur les sociétés au taux normal (...), cette société est tenu d'acquitter un précompte égal au montant du crédit prévu au I de l'article 158 bis (...) " ; qu'il résulte de ces dispositions qu'une société est tenue d'acquitter un précompte mobilier égal au montant du crédit prévu au I de l'article 158 bis lorsque les produits distribués sont prélevés sur des sommes à raison desquelles elle n'a pas été soumise à l'impôt sur les sociétés au taux normal ; que ce précompte constitue ainsi une imposition distincte de l'impôt sur les sociétés ; <br/>
<br/>
              Considérant, d'autre part, qu'aux termes de l'article R. 196-1 du livre des procédures fiscales : " Pour être recevables, les réclamations relatives aux impôts autres que les impôts directs locaux et les taxes annexes à ces impôts, doivent être présentées à l'administration au plus tard le 31 décembre de la deuxième année suivant celle, selon le cas : a) De la mise en recouvrement du rôle ou de la notification d'un avis de mise en recouvrement ; / b) Du versement de l'impôt contesté lorsque cet impôt n'a pas donné lieu à l'établissement d'un rôle ou à la notification d'un avis de mise en recouvrement ; / c) De la réalisation de l'événement qui motive la réclamation " ; qu'aux termes de l'article R. 196-3 du même livre dans sa rédaction applicable aux années d'imposition en litige : " Dans le cas où un contribuable fait l'objet d'une procédure de reprise ou de redressement de la part de l'administration des impôts, il dispose d'un délai égal à celui de l'administration pour présenter ses propres réclamations " ; <br/>
<br/>
              Considérant que, dès lors que le précompte mobilier constitue une imposition distincte de l'impôt sur les sociétés, la circonstance qu'un redressement ait été notifié au titre de ce dernier impôt n'est pas de nature à faire courir, pour une réclamation tendant à la restitution du précompte spontanément acquitté par une société, le délai spécial de réclamation prévu à l'article R. 196-3 du livre des procédures fiscales ; que, par suite, la cour administrative d'appel de Paris n'a pas commis d'erreur de droit en jugeant, pour ce motif, que la société requérante ne pouvait se prévaloir de ce délai spécial ; <br/>
<br/>
              	Sur les motifs de l'arrêt relatifs aux cotisations supplémentaires d'impôt sur les sociétés : <br/>
<br/>
              Considérant, en premier lieu, qu'en jugeant, pour écarter le moyen de la société selon lequel elle n'avait reçu l'avis de vérification de comptabilité, qui avait été envoyé à une adresse erronée, qui n'était pas celle de son principal établissement, que le 9 septembre 1996, soit la veille du début des opérations et qu'en conséquence elle n'avait pas disposé d'un délai suffisant pour pouvoir se faire assister d'un conseil, que ses allégations quant aux dates de réception de cet avis et quant à l'adresse de son principal établissement n'étaient assorties d'aucune justification alors qu'il résultait de l'instruction que l'adresse du 267 rue Lecourbe Paris 15ème, à laquelle a été expédié l'avis, est celle du siège social et du principal établissement de la société et que l'administration faisait valoir, sans qu'aucune pièce du dossier ne permette de contredire cette affirmation, que l'avis avait été envoyé le 2 août 1996, qu'il avait été reçu par le représentant légal de la société le 8 août 1996, soit plus d'un mois avant le début des opérations et qu'elle disposait de l'accusé de réception daté du 8 août 1996, la cour s'est livrée à une appréciation souveraine des faits et n'a pas commis d'erreur de droit au regard des dispositions de l'article L. 47 du livre des procédures fiscales et des règles régissant la charge de la preuve de la régularité de la procédure d'imposition ;<br/>
<br/>
              Considérant, en deuxième lieu, qu'aux termes de l'article L. 59 du livre des procédures fiscales : " Lorsque le désaccord persiste sur les redressements notifiés, l'administration, si le contribuable le demande, soumet le litige à l'avis soit de la commission départementale des impôts directs et des taxes sur le chiffre d'affaires prévue à l'article 1651 du code général des impôts (...) " ; qu'aux termes de l'article R. 57-1 du même livre : " La notification de redressement prévue par l'article L. 57 fait connaître au contribuable la nature et les motifs du redressement envisagé. L'administration invite, en même temps, le contribuable à faire parvenir son acceptation ou ses observations dans un délai de trente jours à compter de la réception de la notification " ; qu'il résulte de ces dispositions que l'expression du désaccord du contribuable sur les redressements qui lui sont notifiés doit être formulée par écrit dans le délai précité et qu'en cas de réponse orale, le contribuable est regardé comme ayant tacitement accepté les redressements ; qu'en l'absence d'un désaccord exprimé dans ce délai, l'administration n'est pas tenue de donner suite à une demande de saisine de la commission départementale des impôts directs et des taxes sur le chiffre d'affaires présentée par le contribuable ; <br/>
<br/>
              Considérant que la cour a relevé que la société n'avait exprimé son désaccord sur les redressements relatifs à l'exercice clos en 1993 notifiés le 4 décembre 1996 que le 9 janvier 1997 soit après l'expiration du délai de trente jours qui lui était imparti ; qu'elle en a déduit que l'administration n'était pas tenue de donner suite à la demande de saisine de la commission départementale des impôts directs et des taxes sur le chiffre d'affaires ; qu'en statuant ainsi, la cour, qui n'a pas dénaturé les pièces du dossier, a implicitement mais nécessairement écarté le moyen de la société tiré de ce qu'elle avait refusé oralement ces redressements le 12 décembre 1996 ; que son arrêt, qui est par suite suffisamment motivé, n'est pas entaché d'erreur de droit ; <br/>
<br/>
              Considérant, en troisième lieu, qu'en jugeant que la société n'avait pas présenté de conclusions tendant à la décharge des cotisations supplémentaires d'impôt sur les sociétés au titre de l'année 1995, la cour n'a pas dénaturé ses écritures d'appel dès lors qu'il ressort des termes mêmes de la dernière page de sa requête d'appel, qu'elle a seulement demandé " le dégrèvement des impositions mises en recouvrement au titre des années 1993 et 1994 " ; que, par suite, la société n'est pas fondée à soutenir que la cour aurait commis une erreur de droit ;  <br/>
<br/>
              Sur les motifs de l'arrêt relatifs aux cotisations primitives d'impôt sur les sociétés :<br/>
              En ce qui concerne les années 1993 à 1995 :<br/>
<br/>
              Considérant qu'aux termes de l'article R. 197-3 du livre des procédures fiscales dispose : " Toute réclamation doit, à peine d'irrecevabilité : (...) d) Etre accompagnée soit de l'avis d'imposition, d'une copie de cet avis ou d'un extrait du rôle, soit de l'avis de mise en recouvrement ou d'une copie de cet avis (...) / La réclamation peut être régularisée à tout moment par la production de l'une des pièces énumérées au d. " ; <br/>
<br/>
              Considérant que la cour a relevé que le ministre soutenait sans être contredit que ces prescriptions n'avaient pas été respectées, s'agissant des conclusions tendant à la réduction des cotisations primitives d'impôt sur les sociétés auxquelles la SA EUROGRAM a été assujettie au titre des années 1993 à 1995 et qu'il ne résultait pas des pièces du dossier qu'elle aurait régularisé sa réclamation par la production des avis d'imposition concernant les impositions primitives de ces années ; qu'en jugeant que les conclusions tendant à la décharge de ces impositions étaient irrecevables, la cour a suffisamment motivé son arrêt, n'a pas dénaturé les pièces de son dossier et n'a pas commis d'erreur de droit ; <br/>
<br/>
              En ce qui concerne l'année 1996 :<br/>
<br/>
              Considérant que la cour n'a pas répondu au moyen tiré de ce que la réclamation en date du 27 décembre 1999, en ce qu'elle portait sur la cotisation primitive d'impôt sur les sociétés au titre de l'année 1996, était motivée par le litige qui l'opposait à son ancien dirigeant et qui avait donné lieu à une assignation devant le tribunal de commerce de Paris en date du 23 juillet 1998 et que cet élément constituait un événement motivant cette réclamation au sens des dispositions du c) de l'article R. 196-1 du livre des procédures fiscales de sorte que cette réclamation était recevable dès lors qu'elle avait été présentée avant le 31 décembre 2000 ; que ce moyen n'était pas inopérant ;<br/>
<br/>
              Considérant qu'il résulte de ce qui précède que la SA EUROGRAM est fondée, dans la mesure de ses conclusions relatives aux cotisations primitives d'impôt sur les sociétés portant sur l'année 1996, à demander l'annulation de l'arrêt attaqué ;<br/>
<br/>
              Considérant que, dans les circonstances de l'espèce, il y a lieu de faire application des dispositions de l'article L. 821-2 du code de justice administrative et de régler l'affaire au fond, dans cette même mesure ;<br/>
<br/>
              Considérant que, d'une part, la société ne conteste pas que sa réclamation en date du 27 décembre 1999 était tardive au regard des dispositions du b) de l'article R. 196-1 du livre des procédures fiscales ; que, d'autre part, l'assignation, par la société requérante, de son ancien dirigeant devant le tribunal de commerce de Paris en date du 23 juillet 1998 ne constitue pas un événement motivant la réclamation au sens du c) de l'article R. 196-1 du même livre ; que, par suite, le délai de réclamation était expiré en ce qui concerne la cotisation primitive d'impôt sur les sociétés établie au titre de l'année 1996 lorsqu'elle a présenté cette réclamation ; <br/>
<br/>
              Considérant qu'il résulte de ce qui précède que la SA EUROGRAM n'est pas fondée à soutenir que c'est à tort que par le jugement attaqué le tribunal administratif a rejeté sa demande ;<br/>
<br/>
              Sur les conclusions tendant à l'application des dispositions de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 1 500 euros à verser à la SA EUROGRAM au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>		D E C I D E :<br/>
              		--------------<br/>
<br/>
Article 1er : Il n'y a pas lieu de statuer sur les conclusions du pourvoi de la SA EUROGRAM relatives aux rappels de taxe sur la valeur ajoutée. <br/>
<br/>
Article 2 : L'arrêt du 3 décembre 2008 de la cour administrative d'appel de Paris est annulé en tant qu'il statue sur les conclusions relatives à la cotisation primitive d'impôt sur les sociétés relative à l'année 1996.<br/>
<br/>
Article 3 : Les conclusions de la requête de la SA EUROGRAM tendant à la réduction de la cotisation primitive d'impôt sur les sociétés relative à l'année 1996 sont rejetées.<br/>
<br/>
Article 4 : L'Etat versera à la SA EUROGRAM la somme de 1 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 5 : Le surplus des conclusions du pourvoi de la SA EUROGRAM est rejeté.<br/>
<br/>
Article 6 : La présente décision sera notifiée à la SA EUROGRAM et à la ministre du budget, des comptes publics et de la réforme de l'Etat, porte-parole du Gouvernement.<br/>
<br/>
</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
