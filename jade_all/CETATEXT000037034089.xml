<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037034089</ID>
<ANCIEN_ID>JG_L_2018_06_000000414708</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/03/40/CETATEXT000037034089.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème chambre, 07/06/2018, 414708, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-06-07</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>414708</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GADIOU, CHEVALLIER</AVOCATS>
<RAPPORTEUR>M. Pierre Ramain</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Edouard Crépey</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:414708.20180607</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B...A...a demandé à la Cour nationale du droit d'asile d'annuler la décision du 22 décembre 2016 de l'Office français de protection des réfugiés et apatrides (OFPRA) qui a rejeté sa demande de réexamen et a refusé de lui reconnaitre la qualité de réfugié ou à défaut de lui accorder le bénéfice de la protection subsidiaire. Par une décision n° 17003699 du 10 mai 2017, la Cour nationale du droit d'asile a rejeté sa demande.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 28 septembre et 28 décembre 2017 au secrétariat du contentieux du Conseil d'Etat, M. A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette décision ;<br/>
<br/>
              2°) réglant l'affaire au fond, de lui reconnaitre la qualité de réfugié ou à défaut lui accorder le bénéfice de la protection subsidiaire.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la convention de Genève du 28 juillet 1951 relative au statut des réfugiés et le protocole signé à New York le 31 janvier 1967 relatif au statut des réfugiés ;<br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Pierre Ramain, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Edouard Crépey, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Gadiou, Chevallier, avocat de M. B...A...;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes des stipulations du paragraphe A, 2° de l'article 1er de la convention de Genève du 28 juillet 1951 et du protocole signé à New York le 31 janvier 1967, doit être considérée comme réfugiée toute personne qui " craignant avec raison d'être persécutée du fait de sa race, de sa religion, de sa nationalité, de son appartenance à un certain groupe social ou de ses opinions politiques, se trouve hors du pays dont elle a la nationalité et qui ne peut, ou, du fait de cette crainte, ne veut se réclamer de la protection de ce pays ". <br/>
<br/>
              2. Il ressort des pièces du dossier soumis aux juges du fond que M.A..., né en 1978 en Arménie, a sollicité le réexamen de sa demande d'asile à la suite de l'incendie de son domicile en Russie en juillet 2014. Il se pourvoit en cassation contre la décision de la Cour nationale du droit d'asile du 10 mai 2017 rejetant son recours contre la décision de l'Office français de protection des réfugiés et apatrides en date du 22 décembre 2016 rejetant cette demande. <br/>
<br/>
              3. En premier lieu, aux termes de l'article L. 733-5 du code de l'entrée et du séjour des étrangers et du droit d'asile : " Saisie d'un recours contre une décision du directeur général de l'Office français de protection des réfugiés et apatrides, la Cour nationale du droit d'asile statue, en qualité de juge de plein contentieux, sur le droit du requérant à une protection au titre de l'asile au vu des circonstances de fait dont elle a connaissance au moment où elle se prononce. / La cour ne peut annuler une décision du directeur général de l'office et lui renvoyer l'examen de la demande d'asile que lorsqu'elle juge que l'office a pris cette décision sans procéder à un examen individuel de la demande ou en se dispensant, en dehors des cas prévus par la loi, d'un entretien personnel avec le demandeur et qu'elle n'est pas en mesure de prendre immédiatement une décision positive sur la demande de protection au vu des éléments établis devant elle ". Le moyen tiré de ce que l'entretien personnel du demandeur d'asile à l'Office se serait déroulé dans de mauvaises conditions n'est pas de nature à justifier que la Cour nationale du droit d'asile annule une décision de l'Office et lui renvoie l'examen de la demande d'asile. En revanche, il revient à la Cour de procéder à cette annulation et à ce renvoi si elle juge que le demandeur a été dans l'impossibilité de se faire comprendre lors de cet entretien, faute d'avoir pu bénéficier du concours d'un interprète dans la langue qu'il a choisie dans sa demande d'asile ou dans une autre langue dont il a une connaissance suffisante, et que ce défaut d'interprétariat est imputable à l'Office. <br/>
<br/>
              4. Il ressort des pièces du dossier soumis aux juges du fond que si le requérant se prévalait devant la cour de l'irrégularité de sa convocation à l'entretien devant l'OFPRA, faute de mention de la possibilité d'être assisté par un avocat, il ne soutenait pas que cet entretien n'avait pas eu lieu ni qu'il n'avait pas pu s'y faire comprendre. Il suit de là qu'en écartant comme inopérant le moyen tiré de l'irrégularité de sa convocation à l'entretien à l'OFPRA, la cour n'a pas commis d'erreur de droit. <br/>
<br/>
              5. En deuxième lieu, pour juger que M. A...ne justifiait pas être de nationalité russe, la cour s'est fondée sur le caractère lacunaire de ses explications sur les conditions d'obtention de cette nationalité et a considéré que la production des copies de l'acte de naissance de sa fille et de son acte de mariage ne suffisaient pas à l'établir alors même que ces documents mentionnaient qu'il était de nationalité russe. En se prononçant de la sorte sur la valeur probante des éléments fournis par le requérant, la cour n'a pas commis d'erreur de droit et a procédé à une appréciation souveraine des pièces du dossier exempte de dénaturation. <br/>
<br/>
              6. En dernier lieu, en écartant comme insuffisamment probantes les allégations de M. A...sur l'incendie criminel qui aurait eu lieu à son domicile en Russie en juillet 2014, la cour n'a pas dénaturé les pièces du dossier. <br/>
<br/>
              7. Il résulte de tout ce qui précède que M. A...n'est pas fondé à demander l'annulation de la décision qu'il attaque. <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : Le pourvoi de M. A...est rejeté. <br/>
Article 2 : La présente décision sera notifiée à M. A...et à l'Office français de protection des réfugiés et apatrides.<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
