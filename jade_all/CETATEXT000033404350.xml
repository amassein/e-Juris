<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033404350</ID>
<ANCIEN_ID>JG_L_2016_11_000000394033</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/40/43/CETATEXT000033404350.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème chambre, 16/11/2016, 394033, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-11-16</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>394033</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP POTIER DE LA VARDE, BUK LAMENT ; HAAS</AVOCATS>
<RAPPORTEUR>Mme Mireille Le Corre</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Xavier de Lesquen</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2016:394033.20161116</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              Par une requête, enregistrée le 12 octobre 2015 au secrétariat du contentieux du Conseil d'Etat, l'association de défense des riverains, usagers et propriétaires du chemin de Mogador, Mme A...E..., M. C... D...et Mme G...F...demandent au Conseil d'Etat : <br/>
<br/>
              1°) de condamner la SCP Gaschignard à les indemniser des préjudices qu'ils estiment avoir subis en raison de l'omission des notifications de leur pourvoi en cassation devant le Conseil d'Etat, exigées par l'article R. 600-1 du code de l'urbanisme, qui a conduit à la non-admission de ce pourvoi, et à leur verser la somme de 29 365 euros à titre de dommages, avec intérêts au taux légal à compter du 18 octobre 2013, date de la saisine du conseil de l'ordre des avocats au Conseil d'Etat et à la Cour de cassation ;<br/>
<br/>
              2°) de mettre à la charge de la SCP Gaschignard la somme de 2 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code civil ;<br/>
              - l'ordonnance du 10 septembre 1817, notamment son article 13 modifié par le décret n° 2002-76 du 11 janvier 2002 ; <br/>
              - l'avis du 14 mai 2014 du conseil de l'ordre des avocats au Conseil d'Etat et à la Cour de cassation ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Mireille Le Corre, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Xavier de Lesquen, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Potier de la Varde, Buk Lament, avocat de l'association de défense des riverains, usagers et propriétaires du chemin de Mogador et autres et à Me Haas, avocat de la SCP Gaschignard ;<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'aux termes du deuxième alinéa de l'article 13 de l'ordonnance du 10 septembre 1817 qui regroupe, sous la dénomination d'Ordre des avocats au Conseil d'Etat et à la Cour de cassation, l'ordre des avocats aux conseils et le collège des avocats à la Cour de cassation, fixe irrévocablement, le nombre des titulaires de l'Ordre, et contient des dispositions pour sa discipline intérieure, dans sa rédaction issue du décret du 11 janvier 2002 relatif à la discipline des avocats au Conseil d'Etat et à la Cour de cassation : " (...) Les actions en responsabilité civile professionnelle engagées à l'encontre d'un avocat au Conseil d'Etat et à la Cour de cassation sont portées, après avis du conseil de l'ordre, devant le Conseil d'Etat, quand les faits ont trait aux fonctions exercées devant le tribunal des conflits et les juridictions de l'ordre administratif, et devant la Cour de cassation dans les autres cas. (...). " ;<br/>
<br/>
              2. Considérant que, par un jugement du 25 juin 2009, le tribunal administratif de Nice a, à la demande de l'association de défense des riverains, usagers et propriétaires du chemin de Mogador et autres, annulé l'arrêté du 2 février 2006 par lequel le maire de Toulon a délivré à M. B...un permis de construire un ensemble immobilier ; que, par un arrêt du 5 mai 2011, la cour administrative d'appel de Marseille a, sur la requête de M. B...et de la commune de Toulon, annulé ce jugement et rejeté la requête ; que l'association de défense des riverains, usagers et propriétaires du chemin de Mogador et autres soutiennent qu'en omettant de procéder aux notifications exigées par l'article R. 600-1 du code de l'urbanisme, ce qui a conduit à la non-admission de leur pourvoi en cassation, la SCP Gaschignard a commis une faute qui leur a fait perdre une chance sérieuse d'obtenir l'annulation de cet arrêt ; qu'ils recherchent, sur le fondement des dispositions citées au point 1, la responsabilité civile de leur avocat aux fins de réparation du préjudice qu'ils estiment avoir subi ; <br/>
<br/>
              3. Considérant que, pour apprécier si l'avocat a commis une faute, il y a lieu de déterminer s'il a normalement accompli avec les diligences suffisantes les devoirs de sa charge, à la condition que son client l'ait mis en mesure de le faire ; <br/>
<br/>
              4. Considérant qu'il résulte de l'instruction que la SCP Gaschignard a commis une faute en omettant de procéder aux notifications exigées par l'article R. 600-1 du code de l'urbanisme ; <br/>
<br/>
              5. Considérant, toutefois, que les requérants ne sont fondés à demander à la SCP Gaschignard, réparation du préjudice qu'ils estiment avoir subi en raison de cette faute que dans la mesure où celle-ci leur aurait fait perdre une chance sérieuse d'obtenir tant la cassation de l'arrêt du 5 mai 2011 de la cour administrative d'appel de Marseille que l'annulation du jugement du 25 juin 2009 et de l'arrêté du 2 février 2006 ;<br/>
<br/>
              6. Considérant qu'il ne résulte pas de l'instruction que les requérants auraient perdu une chance sérieuse d'obtenir la cassation de cet arrêt, au vu des moyens soulevés dans la présente instance, et tirés, en premier lieu, de ce que la cour administrative d'appel de Marseille aurait entaché son arrêt d'une erreur de droit en regardant comme inopérantes, en tant que constitutives de caractéristiques générales de la circulation sans effet sur la légalité du permis de construire, les circonstances relatives aux difficultés particulières d'accès au terrain d'assiette auxquelles s'était référé le tribunal administratif, en deuxième lieu, d'une erreur de droit et d'un défaut de motifs, d'une part, pour s'être fondé sur le motif selon lequel les autorisations d'urbanisme sont accordées sous réserve des droits des tiers pour écarter le moyen tiré de ce que le pétitionnaire n'avait pas justifié qu'il disposait des titres nécessaires à la réalisation du raccordement du projet aux réseaux publics d'eau potable et d'assainissement, ni même de la possibilité qu'il aurait eue d'obtenir de tels titres et, d'autre part, pour avoir écarté le moyen tiré de ce que le pétitionnaire n'avait pas justifié de la possibilité de réaliser un poteau incendie, et, en troisième lieu, d'une dénaturation dans l'appréciation du caractère contigu des différentes constructions composant le projet, au sens de l'article UG8 du règlement du plan d'occupation des sols ; <br/>
<br/>
              7. Considérant qu'il en résulte que la requête de l'association de défense des riverains, usagers et propriétaires du chemin de Mogador et autres en vue de la réparation d'un préjudice que leur a causé la faute commise par la SCP Gaschignard ne peut qu'être rejetée ; <br/>
<br/>
              8. Considérant qu'il résulte de tout ce qui précède que la requête doit être rejetée ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : La requête de l'association de défense des riverains, usagers et propriétaires du chemin de Mogador et autres est rejetée.<br/>
<br/>
Article 2 : La présente décision sera notifiée à l'association de défense des riverains, usagers et propriétaires du chemin de Mogador, premier requérant dénommé, et à la SCP Gaschignard. Les autres requérants seront informés de la présente décision par la la SCP Potier de la Varde, Buk Lament, avocat au Conseil d'Etat et à la Cour de cassation, qui les représente devant le Conseil d'Etat.<br/>
Copie en sera adressée à l'ordre des avocats au Conseil d'Etat et à la Cour de cassation.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
