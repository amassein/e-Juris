<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027800636</ID>
<ANCIEN_ID>JG_L_2013_08_000000355261</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/80/06/CETATEXT000027800636.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  4ème sous-section jugeant seule, 01/08/2013, 355261, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-08-01</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>355261</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 4ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Contentieux des pensions</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LESOURD</AVOCATS>
<RAPPORTEUR>M. Bruno Bachini</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Gaëlle Dumortier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2013:355261.20130801</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu le pourvoi, enregistré le 27 décembre 2011 au secrétariat du contentieux du Conseil d'Etat, présenté par le ministre de la défense et des anciens combattants ; le ministre demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 10/03321 du 3 novembre 2011 par lequel la cour régionale des pensions de Pau a confirmé le jugement du tribunal départemental des pensions des Pyrénées-Atlantiques du 1er juillet 2010 accordant à M. A...B...la revalorisation de sa pension militaire d'invalidité, calculée initialement au grade de maréchal des logis-chef de la gendarmerie nationale, en fonction de l'indice afférent au grade équivalent dans la marine nationale ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
<br/>
              Vu le code des pensions militaires d'invalidité et des victimes de la guerre ;<br/>
<br/>
              Vu le décret n° 56-913 du 5 septembre 1956 ;<br/>
<br/>
              Vu le décret n° 59-327 du 20 février 1959 ;<br/>
<br/>
              Vu le décret n° 65-29 du 11 janvier 1965 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Bruno Bachini, Maître des Requêtes,  <br/>
<br/>
              - les conclusions de Mme Gaëlle Dumortier, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Lesourd, avocat de M. B...;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'aux termes de l'article L. 24 du code des pensions militaires d'invalidité et des victimes de la guerre, alors en vigueur : " Les pensions militaires prévues par le présent code sont liquidées et concédées, sous réserve de la confirmation ou modification prévues à l'alinéa ci-après, par le ministre des anciens combattants et des victimes de guerre ou par les fonctionnaires qu'il délègue à cet effet (...). / Les concessions ainsi établies sont confirmées ou modifiées par un arrêté conjoint du ministre des anciens combattants et victimes de guerre et du ministre de l'économie et des finances. La concession ne devient définitive qu'après intervention dudit arrêté. / (...) / Les dispositions qui précèdent ne sont pas applicables aux militaires et marins de carrière (...), pour lesquels la pension est liquidée (...) par le ministre d'Etat chargé de la défense nationale (...), la constatation de leurs droits incombant au ministre des anciens combattants et victimes de la guerre. Ces pensions sont concédées par arrêté signé du ministre de l'économie et des finances. " ; que, d'une part, en vertu de l'article 5 du décret du 20 février 1959 relatif aux juridictions des pensions, dans sa rédaction alors en vigueur, l'intéressé dispose d'un délai de six mois pour contester, devant le tribunal départemental des pensions, les décisions prises en vertu du premier ou du dernier alinéa de l'article L. 24 précité ainsi que la décision prise en vertu du deuxième alinéa du même article, sauf si celle-ci a simplement confirmé la décision primitive prise en vertu du premier alinéa ; que, d'autre part, aux termes de l'article L. 78 du même code : " Les pensions définitives ou temporaires attribuées au titre du présent code peuvent être révisées dans les cas suivants : / 1° Lorsqu'une erreur matérielle de liquidation a été commise. / 2° Lorsque les énonciations des actes ou des pièces sur le vu desquels l'arrêté de concession a été rendu sont reconnues inexactes soit en ce qui concerne le grade, le décès ou le genre de mort, soit en ce qui concerne l'état des services, soit en ce qui concerne l'état civil ou la situation de famille, soit en ce qui concerne le droit au bénéfice d'un statut légal générateur de droits. / Dans tous les cas, la révision a lieu sans condition de délai (...). " ;<br/>
<br/>
              2. Considérant que le décalage défavorable entre l'indice de la pension servie à un ancien sous-officier de l'armée de terre, de l'armée de l'air ou de la gendarmerie et l'indice afférent au grade équivalent dans la marine nationale, lequel ne résulte ni d'une erreur matérielle dans la liquidation de la pension, ni d'une inexactitude entachant les informations relatives à la personne du pensionné, notamment quant au grade qu'il détenait ou au statut générateur de droit auquel il pouvait légalement prétendre, ne figure pas au nombre des cas permettant la révision, sans condition de délai, d'une pension militaire d'invalidité sur le fondement de l'article L. 78 du code des pensions militaires d'invalidité et des victimes de la guerre ; qu'ainsi, la demande présentée par le titulaire d'une pension militaire d'invalidité, concédée à titre temporaire ou définitif sur la base du grade que l'intéressé détenait dans l'armée de terre, l'armée de l'air ou la gendarmerie, tendant à la revalorisation de cette pension en fonction de l'indice afférent au grade équivalent dans la marine nationale, doit être formée dans le délai de six mois fixé par l'article 5 du décret du 20 février 1959 ; qu'en instituant un tel délai, les auteurs du décret n'ont ni méconnu le principe d'égalité, ni les dispositions de l'article 14 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ; que, passé ce délai de six mois ouvert au pensionné pour contester l'arrêté lui concédant sa pension, l'intéressé ne peut demander sa révision que pour l'un des motifs limitativement énumérés aux 1° et 2° de cet article L. 78 ;<br/>
<br/>
              3. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que M. B... a demandé le 27 juin 2006 datedemandeau ministre de la défense et des anciens combattants de recalculer la pension militaire d'invalidité qui lui avait été concédée à titre définitif par arrêté du 12 septembre 1968 en fonction de l'indice, plus favorable, afférent au grade équivalent dans la marine nationale ; que, par lettre du 17 juillet 2006, le ministre lui a répondu que l'administration recherchait les moyens de donner une suite à sa demande et qu'il en serait tenu informé dès que possible ; qu'en l'absence d'autre réponse, M. B...a présenté un recours contre ce qu'il a estimé être un rejet implicite de cette demande préalable, devant le tribunal départemental des pensions des Pyrénées-Atlantiques qui, par jugement du 1er juillet 2010, a fait droit à sa demande ; que, sur appel du ministre, la cour régionale des pensions de Pau a confirmé ce jugement et accordé à M. B... la revalorisation de sa pension à compter du 27 juin 2003 ;<br/>
<br/>
              4. Considérant que, pour écarter la fin de non-recevoir opposée devant elle par le commissaire du gouvernement et tirée de la forclusion de la demande de M. B..., la cour s'est notamment fondée sur la circonstance que la notification de l'arrêté du 12 septembre 1968 ne mentionnait pas les voies et délais de recours ouverts contre cette décision, de sorte que le délai de recours contentieux prévu par l'article 5 du décret du 20 février 1959 n'avait pu courir ; que, pour ce faire, la cour s'est fondée sur les dispositions de l'article R. 421-5 du code de justice administrative, codifiant celles du dernier alinéa de l'article 1er du décret du 11 janvier 1965 aux termes desquelles : " Les délais de recours ne sont opposables qu'à la condition d'avoir été mentionnés, ainsi que les voies de recours, dans la notification de la décision. " ; que, cependant, ces dispositions, qui ont été ajoutées à l'article 1er du décret du 11 janvier 1965 par le décret n° 83-1025 du 28 novembre 1983, ne sont entrées en vigueur que six mois après la date de publication de ce décret, soit le 4 juin 1984 ; qu'ainsi, en en faisant application à une notification diligentée avant cette date, la cour régionale des pensions de Pau a méconnu le champ de leur application dans le temps ; <br/>
<br/>
              5. Considérant qu'il résulte de ce qui précède que le ministre de la défense et des anciens combattants est fondé à demander l'annulation de l'arrêt attaqué ;<br/>
<br/>
              6. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              7. Considérant qu'aux termes du dernier alinéa de l'article L. 25 du code des pensions militaires d'invalidité et des victimes de la guerre : " La notification des décisions prises en vertu de l'article L. 24, premier alinéa, du présent code, doit mentionner que le délai de recours contentieux court à partir de cette notification et que les décisions confirmatives à intervenir n'ouvrent pas de nouveau délai de recours " ; qu'ainsi, le délai de recours contentieux de six mois prévu à l'article 5 du décret du 20 février 1959 ne commence à courir que du jour où la décision primitive, prise en application du premier alinéa de l'article L. 24 du code des pensions militaires d'invalidité et des victimes de la guerre, a été notifiée au pensionné dans les formes prévues à l'article L. 25 du même code ou, à défaut, du jour où l'arrêté par lequel cette pension a été concédée à titre définitif, en application du deuxième alinéa du même article L. 24, a été régulièrement notifié à l'intéressé ; <br/>
<br/>
              8. Considérant qu'il ne résulte pas de l'instruction, et n'est d'ailleurs pas allégué, que la décision primitive de concession de la pension d'invalidité de M.B..., prise en vertu du premier alinéa de l'article L. 24 du code des pensions militaires d'invalidité et des victimes de la guerre, ait été notifiée à l'intéressé dans les formes prescrites par l'article L. 25 du même code ; que, cependant, il résulte de l'instruction que l'arrêté du 12 septembre 1968 portant concession définitive de cette pension a, quant à lui, été régulièrement notifié à M. B... au regard des dispositions alors en vigueur qui, comme il a été dit ci-dessus, n'imposaient pas encore que la notification de toute décision administrative mentionne les voies et délais de recours ouverts contre cette décision ; que, par suite, et à supposer même que l'arrêté du 12 septembre 1968 ait été purement confirmatif de la décision primitive contre laquelle le délai de recours contentieux n'avait pu commencer à courir, ce délai a couru, en tout état de cause, au plus tard à compter de la notification, le 9 décembre 1968, de ce même arrêté ; que le courrier que M. B... a adressé à l'administration le 27 juin 2006 en vue d'obtenir la revalorisation de sa pension, et qui devait être regardé comme un recours gracieux contre l'arrêté du 12  septembre 1968, a été présenté après l'expiration du délai de six mois fixé par l'article 5 du décret du 20 février 1959 ; que, par suite, le recours contentieux que l'intéressé a formé devant le tribunal départemental des pensions des Pyrénées-Atlantiques, le 9 janvier 2009, en vue, d'une part, de contester le refus implicite opposé à sa demande de revalorisation et, d'autre part, d'obtenir la réformation de l'arrêté du 12 septembre 1968 portant concession de sa pension à titre définitif était tardif, sans que le requérant puisse utilement soutenir que le décret du 5 septembre 1956 violerait le principe d'égalité ; qu'il en résulte que le ministre de la défense et des anciens combattants est fondé à soutenir que c'est à tort que, par le jugement attaqué, le tribunal départemental des pensions a fait droit à la demande de M. B...;<br/>
<br/>
<br/>
<br/>
<br/>			D E C I D E :<br/>
              			--------------<br/>
<br/>
Article 1er : L'arrêt de la cour régionale des pensions de Pau du 3 novembre 2011 et le jugement du tribunal départemental des pensions des Pyrénées-Atlantiques du 1er juillet 2010 sont annulés.<br/>
Article 2 : La requête présentée par M. B...devant le tribunal départemental des pensions des Pyrénées-Atlantiques est rejetée.<br/>
Article 3 : La présente décision sera notifiée au ministre de la défense et à M. A... B....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
