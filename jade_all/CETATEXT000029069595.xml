<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029069595</ID>
<ANCIEN_ID>JG_L_2014_06_000000367158</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/06/95/CETATEXT000029069595.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  4ème sous-section jugeant seule, 11/06/2014, 367158, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-06-11</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>367158</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 4ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Bruno Bachini</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Gaëlle Dumortier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2014:367158.20140611</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 25 mars 2013 au secrétariat du contentieux du Conseil d'Etat, présentée pour la société Bricorama France, dont le siège est situé rue du Moulin Paillasson à Roanne (42300), représentée par son président-directeur général en exercice, et la société maison du treizième, dont le siège est situé 154, boulevard Vincent Auriol à Paris (75013) ; les sociétés requérantes demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision n° 1570 T et 1571 T du 12 décembre 2012 par laquelle la Commission nationale d'aménagement commercial leur a refusé l'autorisation préalable requise en vue de créer, à Saint-Sulpice-sur-Risle (Orne), un magasin de 5 000 m² de surface de vente spécialisée dans la distribution d'articles de bricolage, de jardinage, d'équipement du foyer et de décoration intérieure ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de commerce ;<br/>
<br/>
              Vu la loi n° 73-1193 du 27 décembre 1973 ;<br/>
<br/>
              Vu la loi n° 2008-776 du 4 août 2008 ;<br/>
<br/>
              Vu le décret n° 2008-1212 du 24 novembre 2008 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Bruno Bachini, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Gaëlle Dumortier, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>Sur la légalité de la décision attaquée :<br/>
<br/>
              En ce qui concerne la motivation :<br/>
<br/>
              1. Considérant que si, eu égard à la nature, à la composition et aux attributions de la Commission nationale d'aménagement commercial, les décisions qu'elle prend doivent être motivées, cette obligation n'implique pas que la commission soit tenue de prendre explicitement parti sur le respect, par le projet qui lui est soumis, de chacun des objectifs et critères d'appréciation fixés par les dispositions législatives applicables ; qu'en l'espèce, en se référant à la décision n° 353387 du Conseil d'Etat du 1er août 2012 au motif que la demande soumise à son examen présentait de nombreuses similitudes avec celle, déjà présentée par la société Bricorama France, ayant donné lieu à une décision de rejet de sa part sur laquelle le Conseil d'Etat avait précédemment statué en rejetant la requête aux fins d'annulation formée contre elle, la commission nationale a satisfait à cette obligation ; que, par suite, le moyen tiré de l'insuffisance de la motivation doit être écarté ;<br/>
<br/>
              En ce qui concerne l'appréciation de la commission nationale :<br/>
<br/>
              2. Considérant qu'il appartient aux commissions d'aménagement commercial, lorsqu'elles se prononcent sur un projet d'exploitation commerciale soumis à autorisation en application de l'article L. 752-1 du code de commerce, d'apprécier la conformité de ce projet aux objectifs prévus à l'article 1er de la loi du 27 décembre 1973 et à l'article L. 750-1 du code de commerce, au vu des critères d'évaluation mentionnés à l'article L. 752-6 du même code ; que l'autorisation ne peut être refusée que si, eu égard à ses effets, le projet compromet la réalisation de ces objectifs ;<br/>
<br/>
              3. Considérant que si les sociétés requérantes soutiennent que la commission nationale aurait commis une erreur d'appréciation sur le critère relatif à l'aménagement du territoire, il ressort des pièces du dossier que leur projet, bien que peu éloigné d'une zone d'activité commerciale, en est séparé par la rocade de contournement de la ville de l'Aigle à l'extérieur de laquelle il est implanté, et qu'il se traduirait ainsi par une avancée de l'urbanisation dans un environnement rural, à proximité d'un habitat n'ayant pas vocation à se développer ; que, par sa localisation, un tel projet serait de nature à contribuer à l'étalement urbain sans avoir d'effet positif sur l'animation de la vie locale ; <br/>
<br/>
              4. Considérant que, s'agissant de l'objectif de développement durable, il ressort des pièces du dossier, et notamment des rapports des services instructeurs, que, d'une part, le site n'est pas directement desservi par les transports en commun et la fréquence de la desserte est inadaptée, d'autre part, que les accès pour les cyclistes et les piétons ne sont pas sécurisés ; qu'en outre, et contrairement à ce que soutiennent les sociétés requérantes, aucun effort sérieux n'a été accompli pour améliorer la qualité de l'architecture et l'insertion de l'équipement projeté dans son environnement ; qu'ainsi, au vu de l'ensemble de ces éléments, il ne ressort pas des pièces du dossier que la commission nationale ait fait une inexacte application des dispositions législatives précitées en estimant que le projet méconnaissait les objectifs fixés par le législateur en matière d'aménagement du territoire et de développement durable ;<br/>
<br/>
              5. Considérant qu'il résulte de tout ce qui précède que la société Bricorama France et la société Maison du treizième ne sont pas fondées à demander l'annulation de la décision attaquée ;<br/>
<br/>
              Sur les conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              6. Considérant que ces dispositions font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante ; qu'il y a lieu, en revanche, dans les circonstances de l'espèce, de mettre à la charge de la société Bricorama France et de la société Maison du treizième les sommes de 1 000 euros chacune à verser respectivement à la société Pascalyne et à la société Risldis au titre de ces mêmes dispositions ;<br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er : La requête des sociétés Bricorama France et Maison du treizième est rejetée.<br/>
<br/>
Article 2 : Les sociétés Bricorama France et Maison du treizième verseront aux sociétés Pascalyne et Risldis la somme de 1 000 euros chacune au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
Article 3 : La présente décision sera notifiée à la société Bricorama France, à la société Maison du treizième, à la société Pascalyne, à la société Risldis et à la Commission nationale d'aménagement commercial. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
