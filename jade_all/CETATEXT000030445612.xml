<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030445612</ID>
<ANCIEN_ID>JG_L_2015_03_000000370628</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/44/56/CETATEXT000030445612.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème / 8ème SSR, 20/03/2015, 370628</TITRE>
<DATE_DEC>2015-03-20</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>370628</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème / 8ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP WAQUET, FARGE, HAZAN ; SCP ROCHETEAU, UZAN-SARANO</AVOCATS>
<RAPPORTEUR>M. Christian Fournier</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emmanuelle Cortot-Boucher</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:370628.20150320</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
VU LA PROCEDURE SUIVANTE<br/>
<br/>
              M. A... B...a demandé au tribunal administratif de Grenoble d'annuler pour excès de pouvoir la décision du 12 février 2010 par laquelle le président du conseil d'administration de la régie électrique de Montvalezan-La Rosière et le maire de la commune de Montvalezan ont prononcé son licenciement pour insuffisance professionnelle. Par un jugement n° 1001637 du 22 mai 2012, le tribunal administratif de Grenoble a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 12LY01959 du 28 mai 2013, la cour administrative d'appel de Lyon a rejeté l'appel formé par M. B... contre ce jugement.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 29 juillet et 29 octobre 2013 et 15 juillet 2014 au secrétariat du contentieux du Conseil d'Etat, M. B... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 12LY01959 du 28 mai 2013 de la cour administrative d'appel de Lyon et de saisir le Tribunal des conflits sur le fondement de l'article 34 ou de l'article 35 du décret du 26 octobre 1849 ; <br/>
<br/>
              2°) de mettre à la charge de la régie électrique de Montvalezan-La Rosière et de la commune de Montvalezan la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
- la loi n° 46-628 du 8 avril 1946 ; <br/>
- la loi n° 49-1090 du 2 août 1949 ;<br/>
- le décret du 26 octobre 1849 ;<br/>
- le décret n° 46-1541 du 2 juin 1946 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Christian Fournier, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Emmanuelle Cortot-Boucher, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Waquet, Farge, Hazan, avocat de M. B...et à la SCP Rocheteau, Uzan-Sarano, avocat de la régie électrique de Montvalezan-la-Rosière et de la commune de Montvalezan ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, par une décision du 12 février 2010, le président du conseil d'administration de la régie électrique de Montvalezan-La Rosière, créée en 1938, et le maire de la commune de Montvalezan ont licencié M. B..., directeur de la régie électrique de Montvalezan-La Rosière, pour insuffisance professionnelle ; que, par un jugement en date du 22 mai 2012, le tribunal administratif de Grenoble a rejeté la demande de M. B... tendant à l'annulation de cette décision ; que M. B... se pourvoit en cassation contre l'arrêt du 28 mai 2013 par lequel la cour administrative d'appel de Lyon a rejeté l'appel qu'il a formé contre ce jugement ;<br/>
<br/>
              2. Considérant que l'article 6 de la loi du 2 août 1949, modifiant notamment l'article 23 de la loi du 8 avril 1946 sur la nationalisation de l'électricité et du gaz, désormais repris à l'article L. 111-54 du code de l'énergie, a prévu que les régies municipales de distribution de gaz et d'électricité seront maintenues à titre définitif dans leur situation particulière, telle qu'elle résulte de la réglementation en vigueur ; que cette modification législative a eu pour conséquence d'exclure ces régies de la nationalisation et de permettre, dès sa publication au Journal officiel du 6 août 1949, l'entrée en vigueur dans ces régies municipales du statut national du personnel des industries électriques et gazières approuvé par décret du 22 juin 1946 ; que l'article 47 de la loi du 8 avril 1946 dispose que " (...) Ce statut s'applique à tout le personnel de l'industrie électrique et gazière (...) " et qu'aucune autre disposition législative ne crée d'exception pour les agents de direction des régies municipales ; <br/>
<br/>
              3. Considérant, d'une part, qu'en raison de l'application par le législateur de l'ensemble des dispositions de ce statut, les agents de direction des régies municipales de gaz et d'électricité ont acquis la qualité de salariés de droit privé ; qu'il en résulte que les litiges relatifs à leur situation individuelle ressortissent à la compétence des tribunaux judiciaires ; <br/>
<br/>
              4. Considérant, d'autre part, que si, par un arrêt du 28 juin 2005 devenu définitif, la cour d'appel de Chambéry a jugé que M. B... ne pouvait saisir la juridiction prud'homale du litige qui l'opposait à la régie d'électricité de Montvalezan-La Rosière au sujet d'une demande en réparation du préjudice résultant de faits de harcèlement moral, le litige tranché par cette cour n'est pas identique à celui qui fait l'objet du présent pourvoi et qui est relatif à la légalité de la décision du 12 février 2010 du président du conseil d'administration de la régie électrique de Montvalezan-La Rosière et du maire de la commune de Montvalezan prononçant le licenciement de M. B... pour insuffisance professionnelle ; <br/>
<br/>
              5. Considérant qu'il résulte de ce qui précède, sans qu'il y ait lieu de renvoyer la question de compétence au tribunal des conflits en application des dispositions de l'article 34 du décret du 26 octobre 1849, repris à l'article R. 771-1 du code de justice administrative ou de l'article 35 du même décret, repris à l'article R. 771-2 du même code, que la cour administrative d'appel a commis une erreur de droit en estimant que la juridiction administrative était compétente pour connaître du litige relatif à la situation personnelle de M. B..., directeur de la régie électrique de Montvalezan-La Rosière ; que, dès lors, M. B... est fondé, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, à demander l'annulation de l'arrêt qu'il attaque ;<br/>
<br/>
              6. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              7. Considérant, ainsi qu'il vient d'être dit, qu'il n'appartient pas à la juridiction administrative de connaître de litiges relatifs à la situation personnelle d'un directeur de régie municipale électrique, qui a la qualité de salarié de droit privé ; qu'il suit de là que la juridiction administrative n'est pas compétente pour connaître des conclusions de M. B... tendant à l'annulation de la décision du 12 février 2010 par laquelle le président du conseil d'administration de la régie électrique de Montvalezan-La Rosière et le maire de Montvalezan ont prononcé son licenciement pour insuffisance professionnelle ; que, par suite, il y a lieu d'annuler le jugement du tribunal administratif de Grenoble et de rejeter ces conclusions comme portées devant un ordre de juridiction incompétent pour en connaître ; <br/>
<br/>
              8. Considérant qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire application des dispositions de l'article L. 761-1 du code de justice administrative et de mettre à la charge de M. B... la somme que demandent la régie d'électricité de Montvalezan-La Rosière et la commune de Montvalezan au titre des  frais exposés par elles et non compris dans les dépens ; que ces dispositions font obstacle à ce que la régie d'électricité de Montvalezan-La Rosière et la commune de Montvalezan, qui ne sont pas, dans la présente instance, la partie perdante, versent à M. B... la somme qu'il demande au titre des frais exposés par lui et non compris dans les dépens ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 28 mai 2013 de la cour administrative d'appel de Lyon et le jugement du 22 mai 2012 du tribunal administratif de Grenoble sont annulés.<br/>
Article 2 : Les conclusions de la demande de M. B... devant le tribunal administratif de Grenoble tendant à l'annulation de la décision du 12 février 2010 par laquelle le président du conseil d'administration de la régie électrique de Montvalezan-La Rosière et le maire de Montvalezan ont prononcé son licenciement pour insuffisance professionnelle sont rejetées comme portées devant un ordre de juridiction incompétent pour en connaître.<br/>
Article 3 : Les conclusions de M. B... ainsi que celles de la régie d'électricité de Montvalezan-La Rosière et de la commune de Montvalezan tendant à l'application de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée à M. A... B..., à la régie électrique de Montvalezan-La Rosière et à la commune de Montvalezan.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">135-02-06 COLLECTIVITÉS TERRITORIALES. COMMUNE. AGENTS COMMUNAUX (VOIR : FONCTIONNAIRES ET AGENTS PUBLICS). - AGENTS DE DIRECTION DES RÉGIES MUNICIPALES DE GAZ ET D'ÉLECTRICITÉ - STATUT DE DROIT PRIVÉ - EXISTENCE [RJ1].
</SCT>
<ANA ID="9A"> 135-02-06 En raison de l'application par le législateur de l'ensemble des dispositions du statut national du personnel des industries électriques et gazières à leur situation, les agents de direction des régies municipales de gaz et d'électricité ont acquis la qualité de salariés de droit privé. Il en résulte que les litiges relatifs à leur situation individuelle ressortissent à la compétence des tribunaux judiciaires.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, Section, 22 janvier 1954, Sieur Wittwer, p. 42.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
