<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034205970</ID>
<ANCIEN_ID>JG_L_2017_03_000000395286</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/20/59/CETATEXT000034205970.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème - 4ème chambres réunies, 15/03/2017, 395286</TITRE>
<DATE_DEC>2017-03-15</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>395286</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème - 4ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP SPINOSI, SUREAU</AVOCATS>
<RAPPORTEUR>M. Marc Lambron</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Laurence Marion</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:395286.20170315</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A...B...a demandé au tribunal administratif de Caen, d'une part,  d'annuler la décision du 23 janvier 2015 par laquelle le ministre de l'intérieur a retiré six points de son permis de conduire à raison d'une infraction au code de la route commise le 19 décembre 2014, récapitulé onze retraits de points antérieurs et constaté la perte de validité de son titre de conduite pour solde de points nul ainsi que la décision du 31 mars 2015 rejetant son recours gracieux, et, d'autre part, d'enjoindre au ministre de l'intérieur de rétablir les points illégalement retirés. Par un jugement n° 1500832 du 15 octobre 2015, le tribunal administratif a annulé la décision du 23 janvier 2015 en tant qu'elle constatait  la perte de validité du permis de conduire de M. B..., annulé dans la même mesure la décision du 31 mars 2015 et enjoint au ministre de procéder à la reconstitution du capital de points du permis de l'intéressé.<br/>
<br/>
              Par un pourvoi, enregistré le 15 décembre 2015 au secrétariat du contentieux du Conseil d'Etat, le ministre de l'intérieur demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter les conclusions présentées par M. B... devant les juges du fond.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
<br/>
              - le code de la route ; <br/>
<br/>
              - le code de procédure pénale ;<br/>
<br/>
              - le décret n° 2003-293 du 31 mars 2003 ; <br/>
<br/>
              -le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Marc Lambron, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Laurence Marion, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Spinosi, Sureau, avocat de M. B....<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que le ministre de l'intérieur a, par une décision du 23 juin 2015, retiré six points du permis de conduire de M. B...en raison d'une infraction au code de la route commise le 19 décembre 2014, récapitulé onze retraits de points antérieurs et informé M. B...de la perte de validité de son permis de conduire pour solde de points nul ; qu'après rejet de son recours gracieux par une décision du 31 mars 2015, M. B...a saisi le tribunal administratif de Caen, qui, par un jugement du 15 octobre 2015, a annulé la décision du 23 janvier 2015 en tant qu'elle constatait la perte de validité du permis de conduire ainsi que la décision du 31 mars 2015 dans la même mesure, enjoint au ministre de l'intérieur de procéder à la reconstitution du capital de points du permis de conduire en tenant compte de l'illégalité du retrait d'un point consécutif à une infraction relevée le 22 janvier 2010 et de la nécessité de réattribuer à l'intéressé, en application du dernier alinéa de l'article L. 223-6 du code de la route,  quatre points retirés à la suite d'une infraction commise le 17 décembre 2002 et rejeté le surplus des conclusions du demandeur ; que le ministre de l'intérieur se pourvoit en cassation contre ce jugement ; <br/>
<br/>
              2. Considérant qu'il appartient au juge administratif, saisi d'une contestation portant sur un retrait de points du permis de conduire, lequel constitue une sanction que l'administration inflige à un administré, de se prononcer  sur cette sanction comme juge de plein contentieux ; que, compte tenu des pouvoirs dont il dispose ainsi, il lui appartient, le cas échéant, de faire application d'une loi nouvelle plus douce entrée en vigueur entre la date à laquelle la réalité de l'infraction à l'origine du retrait de points a été établie et celle à laquelle il statue ; <br/>
<br/>
              3. Considérant qu'en vertu des dispositions de l'article L. 223-6 du code de la route, dans sa rédaction applicable à l'infraction commise le 17 décembre 2002 et ayant donné lieu à une condamnation par jugement du 20 mars 2003 : " Sans préjudice de l'application des deux premiers alinéas du présent article, les points retirés du fait de contraventions passibles d'une amende forfaitaire sont réattribués au titulaire du permis de conduire à l'expiration d'un délai de dix ans à compter de la date à laquelle la condamnation est devenue définitive ou du paiement de l'amende forfaitaire correspondante. " ;  qu'aux termes de l'article 48-1 du code de procédure pénale, dans sa version alors applicable : " les contraventions des quatre premières classes pour lesquelles l'action publique est éteinte par le paiement d'une amende forfaitaire sont les suivantes : 1°) Contraventions réprimées par le code de la route punies uniquement d'une peine d'amende, à l'exclusion de toute peine complémentaire, qu'elles entraînent ou non un retrait de points affectés au permis de conduire sous réserve des dispositions de l'article R. 49-8-5 relatives à l'amende forfaitaire minorée " ; qu'aux termes de l'article R. 412-30 du code de la route : " Tout conducteur doit marquer l'arrêt absolu devant un feu de signalisation rouge, fixe ou clignotant/ (...)/. Le fait, pour tout conducteur, de contrevenir aux dispositions du présent article est puni de l'amende prévue pour les contraventions de la quatrième classe./ Toute personne coupable de cette infraction encourt également la peine complémentaire de suspension, pour une durée de trois ans au plus, du permis de conduire, cette suspension pouvant être limitée à la conduite en dehors de l'activité professionnelle./ Cette contravention donne lieu de plein droit à la réduction de quatre points du permis de conduire " ; qu'il résulte de la combinaison de ces dispositions qu'à la date du 17 décembre 2002, l'infraction consistant à ne pas marquer l'arrêt devant un feu rouge était passible d'une peine complémentaire et ne pouvait, par suite, donner lieu au paiement d'une amende forfaitaire ; qu'il en résulte que les points retirés du fait de la commission, à cette date, d'une telle infraction n'étaient pas réattribués à l'expiration du délai de dix ans prévu par les dispositions de l'article L. 223-6 du code de la route ; <br/>
<br/>
              4. Considérant, toutefois, que, dans sa rédaction résultant de l'article 1er du décret du 31 mars 2003 relatif à la sécurité routière et modifiant le code de procédure pénale et le code de la route, l'article R. 48-1 du code de procédure pénale dispose que : " Les contraventions des quatre premières classes pour lesquelles l'action publique est éteinte par le paiement d'une amende forfaitaire sont les suivantes :/ 1° Contraventions réprimées par le code de la route qu'elles entraînent ou non un retrait des points affectés au permis de conduire sous réserve des dispositions de l'article R. 49-8-5 relatives à l'amende forfaitaire minorée (...) " ; qu'il résulte de ces dispositions que, depuis leur entrée en vigueur, l'infraction de non-respect de l'obligation d'arrêt à un feu rouge est passible de l'amende forfaitaire ; qu'il en résulte que les points retirés à raison de cette infraction sont, désormais, réattribués de plein droit à l'expiration du délai de dix ans prévu par les dispositions de l'article L. 223-6 du code de la route ; qu'il suit de là que les dispositions combinées de l'article L 223-6 du code de la route et de l'article R 48-1 du code de procédure pénale dans  sa rédaction résultant du décret du 31 mars 2003 doivent être regardées comme constituant une loi pénale plus douce ; qu'il en résulte que le tribunal administratif de Caen n'a pas commis d'erreur de droit en jugeant que les quatre points perdus par M. B...du fait de l'infraction de non-respect de l'arrêt à un feu rouge qu'il avait commise le 17 décembre 2002 et qui avait donné lieu à un jugement du tribunal de police de Caen devenu définitif du 20 mars 2003 devaient lui être réattribués le 20 mars 2013 et en en déduisant que le ministre avait à tort, pour prendre la décision attaquée, comptabilisé ces quatre points en retrait ;  que le pourvoi du ministre doit donc être rejeté ;   <br/>
<br/>
              5. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat une somme de 3 000 euros à verser à M. B...au titre des dispositions de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
		Article 1er : Le pourvoi du ministre de l'intérieur est rejeté.<br/>
<br/>
Article 2 : L'Etat versera à M. B...une somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 3 : La présente décision sera notifiée au ministre de l'intérieur et à M. A... B....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-08-03 ACTES LÉGISLATIFS ET ADMINISTRATIFS. APPLICATION DANS LE TEMPS. TEXTE APPLICABLE. - APPLICATION IMMÉDIATE DE LA LOI RÉPRESSIVE NOUVELLE PLUS DOUCE (RÉTROACTIVITÉ IN MITIUS) - RESTITUTION DES POINTS RETIRÉS AU TITULAIRE D'UN PERMIS DE CONDUIRE DU FAIT DE CONTRAVENTIONS PASSIBLES D'UNE AMENDE FORFAITAIRE (ART. L. 223-6 DU CODE DE LA ROUTE) - MODIFICATION, POUR L'ÉLARGIR, DU CHAMP DES CONTRAVENTIONS PASSIBLES D'UNE AMENDE FORFAITAIRE (ART. R. 48-1 DU CPP) - EXISTENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">49-04-01-04-04 POLICE. POLICE GÉNÉRALE. CIRCULATION ET STATIONNEMENT. PERMIS DE CONDUIRE. RESTITUTION DE POINTS. - RÉATTRIBUTION DES POINTS RETIRÉS DU FAIT DE CONTRAVENTIONS PASSIBLES D'UNE AMENDE FORFAITAIRE (ART. L. 223-6 DU CODE DE LA ROUTE) - ELARGISSEMENT DU CHAMP DES CONTRAVENTIONS PASSIBLES D'UNE AMENDE FORFAITAIRE (ART. R. 48-1 DU CPP) - CONSÉQUENCE - LOI PÉNALE PLUS DOUCE - EXISTENCE.
</SCT>
<ANA ID="9A"> 01-08-03 Les points retirés du fait de contraventions passibles d'une amende forfaitaire sont réattribués au titulaire du permis de conduire à l'expiration d'un délai de dix ans à compter de la date à laquelle la condamnation est devenue définitive ou du paiement de l'amende forfaitaire correspondante (art. L. 223-6 du code de la route). L'élargissement du champ des contraventions passibles d'une amende forfaitaire, du fait de la modification de l'article R. 48-1 du code de procédure pénale (CPP) postérieurement à la commission de l'infraction, a pour effet de permettre, sur le fondement de l'article L. 223-6 du code de la route, la réattribution des points retirés du fait de contraventions jusqu'alors exclues du dispositif de réattribution de points. Les dispositions combinées des articles L. 223-6 et R. 48-1 du CPP doivent dans cette hypothèse être regardées comme constituant une loi pénale plus douce.</ANA>
<ANA ID="9B"> 49-04-01-04-04 Les points retirés du fait de contraventions passibles d'une amende forfaitaire sont réattribués au titulaire du permis de conduire à l'expiration d'un délai de dix ans à compter de la date à laquelle la condamnation est devenue définitive ou du paiement de l'amende forfaitaire correspondante (art. L. 223-6 du code de la route). L'élargissement du champ des contraventions passibles d'une amende forfaitaire, du fait de la modification de l'article R. 48-1 du code de procédure pénale (CPP) postérieurement à la commission de l'infraction, a pour effet de permettre, sur le fondement de l'article L. 223-6 du code de la route, la réattribution des points retirés du fait de contraventions jusqu'alors exclues du dispositif de réattribution de points. Les dispositions combinées des articles L. 223-6 et R. 48-1 du CPP doivent dans cette hypothèse être regardées comme constituant une loi pénale plus douce.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
