<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042992811</ID>
<ANCIEN_ID>JG_L_2020_12_000000447900</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/99/28/CETATEXT000042992811.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 24/12/2020, 447900, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-12-24</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>447900</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP HEMERY, THOMAS-RAQUIN, LE GUERER</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2020:447900.20201224</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu les procédures suivantes :<br/>
<br/>
              I. Sous le n° 447900, par une requête et un mémoire rectificatif, enregistrés les 16 et 21 décembre 2020 au secrétariat du contentieux du Conseil d'Etat, la Société des auteurs dans les arts graphiques et plastiques (ADAGP), la Fédération des professionnels de l'art contemporain (le CIPAC) et la Fédération des réseaux et associations d'artistes plasticiens (FRAAP) demandent au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative : <br/>
<br/>
              1°) d'ordonner la suspension de l'exécution des dispositions de l'article 45 du décret n° 2020-1310 du 29 octobre 2020, en ce qu'elles interdisent aux établissements de type T et Y d'accueillir du public ; <br/>
<br/>
              2°) d'enjoindre au Premier ministre de modifier les dispositions en vigueur, en prenant les mesures strictement proportionnées aux risques sanitaires encourus et appropriées aux circonstances de temps et de lieu, afin de permettre la réouverture immédiate des lieux d'expositions d'oeuvres artistiques exploités dans des établissements de type T et Y dans le strict respect des protocoles sanitaires établis ; <br/>
<br/>
              3°) de prononcer toute autre mesure qu'il estimerait utile pour mettre fin aux atteintes graves et manifestement illégales que ces dispositions portent aux libertés fondamentales invoquées ; <br/>
<br/>
              4°) de mettre à la charge de l'Etat la somme de 5 000 euros sur le fondement de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Les requérantes soutiennent que : <br/>
              - la condition d'urgence est satisfaite dès lors que le maintien de la fermeture des lieux d'exposition, décidé sans concertation avec les acteurs concernés, a un impact sur l'ensemble du secteur des arts visuels et sur le réseau des salles d'exposition ; <br/>
              - il est porté une atteinte grave et manifestement illégale à plusieurs libertés fondamentales ; <br/>
              - le décret contesté méconnaît la liberté d'expression et de communication ainsi que la liberté de création dès lors que, d'une part, il fait obstacle à ce que les salles d'exposition puissent présenter au public les oeuvres des artistes, ce qui a un impact à long terme non seulement économique, mais humain et psychologique, et, d'autre part, il ne permet pas aux professionnels du secteur des arts visuels d'avoir une visibilité sur la date à laquelle une décision de réouverture des lieux d'exposition est susceptible d'intervenir ; <br/>
              - il méconnaît la liberté du commerce et de l'industrie ainsi que la liberté d'entreprendre dès lors que, d'une part, il prive les entreprises qui exploitent les lieux d'exposition de toute possibilité d'exercer leurs activités et met en péril la rentabilité des investissements artistiques, humains et financiers que ces entreprises mettent en oeuvre pour la diffusion des oeuvres des arts visuels ; <br/>
              - il est constitutif d'une discrimination à l'encontre des lieux d'exposition dès lors que, au contraire et sans justification au regard des risques sanitaires encourus, les galeries d'art et les salles de ventes peuvent quant à elles ouvrir de nouveau ; <br/>
              - la mesure est manifestement disproportionnée au regard de l'objectif de préservation de la santé publique dès lors que, en premier lieu, des mesures moins sévères peuvent être mises en oeuvre en application de protocoles sanitaires stricts, en deuxième lieu, elle est constitutive d'une interdiction générale et absolue qui n'est pas justifiée par l'existence de risques sanitaires qui seraient propres à l'activité des lieux d'exposition, et, en dernier lieu, les risques de contamination au sein des lieux d'exposition sont moindres qu'au sein des magasins, des centres commerciaux et des lieux de culte, dont l'accès est de nouveau autorisé.<br/>
<br/>
              Par un mémoire en défense, enregistré le 21 décembre 2020, le ministre des solidarités et de la santé conclut au rejet de la requête. Il soutient qu'aucune atteinte grave et manifestement illégale n'est portée aux libertés fondamentales invoquées.   <br/>
<br/>
              La requête a été communiquée au Premier ministre et au ministre de la culture qui n'ont pas présenté d'observations.<br/>
<br/>
<br/>
              II. Sous le n° 447935, par une requête, enregistrée le 18 décembre 2020 au secrétariat du contentieux du Conseil d'Etat, la société Culturespaces SA demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, d'enjoindre au Premier ministre de modifier, dans un délai de trois jours à compter de la notification de l'ordonnance, en application de l'article L. 3131-15 du code de la santé publique, les dispositions du I de l'article 45 du décret n° 2020-1310 du 29 octobre 2020, en prenant les mesures strictement proportionnées à l'ouverture des établissements de type Y et L, ainsi que celles du I de l'article 42 concernant les établissements de type PA.<br/>
<br/>
<br/>
              Elle soutient que :<br/>
              - la condition d'urgence est satisfaite eu égard aux conséquences financières et à la perte de la clientèle que la mesure engendre sur le long terme pour les lieux de culture ;<br/>
              - il est porté une atteinte grave et manifestement illégale à liberté d'expression, à la libre communication des idées et des opinions, à la liberté d'entreprendre et au principe d'égalité ;<br/>
              - le décret contestée instaure une situation de discrimination entre, d'une part, les magasins de vente de type M, les galeries d'art et les salles de vente et, d'autre part, les lieux de culture qui ne sont pas autorisés recevoir du public ;<br/>
              - il est manifestement disproportionné, non-nécessaire et inapproprié à l'objectif de lutte contre l'épidémie covid-19 dès lors que, en premier lieu, aucune étude scientifique ne démontre l'existence de risques sanitaires qui seraient propres à l'activité des lieux de culture alors que des protocoles sanitaires stricts sont mis en place, en deuxième lieu, il impose une interdiction générale et absolue aux professionnels de la culture d'exercer leurs activités tout en poussant au maintien de l'activité économique et, en dernier lieu, il crée des dommages irréversibles. <br/>
<br/>
              Par un mémoire en défense et un nouveau mémoire, enregistrés le 21 décembre 2020, le ministre des solidarités et de la santé conclut au rejet de la requête. Il soutient qu'aucune atteinte grave et manifestement illégale n'est portée aux libertés fondamentales invoquées. <br/>
<br/>
              La requête a été communiquée au Premier ministre et au ministre de la culture qui n'ont pas présenté d'observations.  <br/>
<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu :<br/>
              - la Constitution, et notamment son préambule ;<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - le code de la construction et de l'habitation ;<br/>
              - le code de la santé publique ;<br/>
              - la loi n° 2020-1379 du 14 novembre 2020 ;<br/>
              - le décret n° 2020-1257 du 14 octobre 2020 ;<br/>
              - le décret n° 2020-1310 du 29 octobre 2020 ;<br/>
              - le décret n° 2020-1582 du 14 décembre 2020 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, l'ADACP, le CIPAC, la FRAAP et la société Culturespaces SA, d'autre part, le Premier ministre, le ministre de la culture ainsi que le ministre des solidarités et de la santé ; <br/>
<br/>
              Ont été entendus lors de l'audience publique du 22 décembre 2020 à 10 heures ;<br/>
<br/>
              - Me Le Guerer, avocat au Conseil d'Etat et à la Cour de cassation, avocat de l'ADAGP et de la société Culturespaces SA ; <br/>
<br/>
              - les représentants de l'ADAGP ; <br/>
<br/>
              - les représentants de la société Culturespaces SA ; <br/>
<br/>
              - les représentants du ministre des solidarités et de la santé ; <br/>
<br/>
              à l'issue de laquelle le juge des référés a clôt l'instruction.<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. L'article L. 511-1 du code de justice administrative dispose que : " Le juge des référés statue par des mesures qui présentent un caractère provisoire. Il n'est pas saisi du principal et se prononce dans les meilleurs délais ". Aux termes de l'article L. 521-2 du même code : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. Le juge des référés se prononce dans un délai de quarante-huit heures ". <br/>
<br/>
              2. Les requêtes visées ci-dessus, qui sont présentées en application de l'article L. 521-1 du code de justice administrative, sont dirigées contre les dispositions des articles 39, 42 et 45 du décret du 29 octobre 2020, dans leur rédaction issue du décret du 27 novembre 2020, en tant qu'elles interdisent l'accueil du public dans les salles d'exposition à vocation artistique relevant du type T défini par le règlement pris en application de l'article R. 123-12 du code de la construction et de l'habitation, dans les établissements de plein air à vocation culturelle relevant du type PA, dans les salles de projection relevant du type L et dans les musées et salles destinées à recevoir des expositions à vocation culturelle ayant un caractère temporaire, relevant du type Y. Elles présentent à juger les mêmes questions ; il y a lieu de les joindre pour statuer par une seule ordonnance. <br/>
<br/>
              Sur l'office du juge des référés :<br/>
<br/>
              3. Il résulte de la combinaison des dispositions des articles L. 511-1, L. 521-2 et L. 521-4 du code de justice administrative qu'il appartient au juge des référés, lorsqu'il est saisi sur le fondement de l'article L. 521-2 précité et qu'il constate une atteinte grave et manifestement illégale portée par une personne morale de droit public à une liberté fondamentale, de prendre les mesures qui sont de nature à faire disparaître les effets de cette atteinte. Ces mesures doivent en principe présenter un caractère provisoire, sauf lorsqu'aucune mesure de cette nature n'est susceptible de sauvegarder l'exercice effectif de la liberté fondamentale à laquelle il est porté atteinte. Le juge des référés peut, sur le fondement de l'article L. 521-2 du code de justice administrative, ordonner à l'autorité compétente de prendre, à titre provisoire, une mesure d'organisation des services placés sous son autorité lorsqu'une telle mesure est nécessaire à la sauvegarde d'une liberté fondamentale. Toutefois, le juge des référés ne peut, au titre de la procédure particulière prévue par l'article L. 521-2 précité, qu'ordonner les mesures d'urgence qui lui apparaissent de nature à sauvegarder, dans un délai de quarante-huit heures, la liberté fondamentale à laquelle il est porté une atteinte grave et manifestement illégale. Eu égard à son office, il peut également, le cas échéant, décider de déterminer dans une décision ultérieure prise à brève échéance les mesures complémentaires qui s'imposent et qui peuvent également être très rapidement mises en oeuvre. Dans tous les cas, l'intervention du juge des référés dans les conditions d'urgence particulière prévues par l'article L. 521-2 précité est subordonnée au constat que la situation litigieuse permette de prendre utilement et à très bref délai les mesures de sauvegarde nécessaires. Compte tenu du cadre temporel dans lequel se prononce le juge des référés saisi sur le fondement de l'article L. 521-2, les mesures qu'il peut ordonner doivent s'apprécier en tenant compte des moyens dont dispose l'autorité administrative compétente et des mesures qu'elle a déjà prises.<br/>
<br/>
              Sur les circonstances et le cadre du litige : <br/>
<br/>
              4. Aux termes de l'article L. 3131-12 du code de la santé publique, issu de la loi du 23 mars 2020 d'urgence pour faire face à l'épidémie de Covid-19 : " L'état d'urgence sanitaire peut être déclaré sur tout ou partie du territoire métropolitain ainsi que du territoire des collectivités régies par les articles 73 et 74 de la Constitution et de la Nouvelle-Calédonie en cas de catastrophe sanitaire mettant en péril, par sa nature et sa gravité, la santé de la population ". L'article L. 3131-13 du même code dispose que : " L'état d'urgence sanitaire est déclaré par décret en conseil des ministres pris sur le rapport du ministre chargé de la santé. Ce décret motivé détermine la ou les circonscriptions territoriales à l'intérieur desquelles il entre en vigueur et reçoit application. Les données scientifiques disponibles sur la situation sanitaire qui ont motivé la décision sont rendues publiques. / (...) / La prorogation de l'état d'urgence sanitaire au-delà d'un mois ne peut être autorisée que par la loi, après avis du comité de scientifiques prévu à l'article L. 3131-19. " Aux termes du I de l'article L. 3131-15 du même code : " Dans les circonscriptions territoriales où l'état d'urgence sanitaire est déclaré, le Premier ministre peut, par décret réglementaire pris sur le rapport du ministre chargé de la santé, aux seules fins de garantir la santé publique : (...) 5° Ordonner la fermeture provisoire et réglementer l'ouverture, y compris les conditions d'accès et de présence, d'une ou plusieurs catégories d'établissements recevant du public ainsi que des lieux de réunion, en garantissant l'accès des personnes aux biens et services de première nécessité. " Ce même article précise à son III que les mesures prises en application de ses dispositions " sont strictement proportionnées aux risques sanitaires encourus et appropriées aux circonstances de temps et de lieu " et " qu'il y est mis fin sans délai lorsqu'elles ne sont plus nécessaires ".<br/>
<br/>
              5. L'émergence d'un nouveau coronavirus, responsable de la maladie à coronavirus 2019 ou Covid-19 et particulièrement contagieux, a été qualifiée d'urgence de santé publique de portée internationale par l'Organisation mondiale de la santé le 30 janvier 2020, puis de pandémie le 11 mars 2020. La propagation du virus sur le territoire français a conduit le ministre chargé de la santé puis le Premier ministre à prendre, à compter du 4 mars 2020, des mesures de plus en plus strictes destinées à réduire les risques de contagion. Pour faire face à l'aggravation de l'épidémie, la loi du 23 mars 2020 a créé un régime d'état d'urgence sanitaire, défini aux articles L. 3131-12 à L. 3131-20 du code de la santé publique, et a déclaré l'état d'urgence sanitaire pour une durée de deux mois à compter du 24 mars 2020. La loi du 11 mai 2020 prorogeant l'état d'urgence sanitaire et complétant ces dispositions a prorogé cet état d'urgence sanitaire jusqu'au 10 juillet 2020.<br/>
<br/>
              6. Une nouvelle progression de l'épidémie a conduit le Président de la République à prendre le 14 octobre dernier, sur le fondement des articles L. 3131-12 et L. 3131-13 du code de la santé publique, un décret déclarant l'état d'urgence sanitaire à compter du 17 octobre 2020 sur l'ensemble du territoire national. L'article 1er de la loi du 14 novembre 2020 autorisant la prorogation de l'état d'urgence sanitaire et portant diverses mesures de gestion de la crise sanitaire a prorogé l'état d'urgence sanitaire jusqu'au 16 février 2021 inclus. Le Premier ministre a pris, sur le fondement de l'article L. 3131-15 du code de la santé publique, le décret du 29 octobre 2020 prescrivant les mesures générales nécessaires pour faire face à l'épidémie de covid-19 dans le cadre de l'urgence sanitaire, modifié en dernier lieu par un décret du 20 décembre 2020. Il résulte des dispositions des articles 39, 42 et 45 de ce décret que les salles d'exposition relevant du type T, les établissements relevant du type PA, " établissements de plein air, à l'exception de ceux au sein desquels est pratiquée la pêche en eau douce " et du type L, comprenant notamment les salles de spectacles ou à usage multiple ainsi que les musées et salles destinées à recevoir des expositions temporaires à vocation culturelle, relevant du type Y, ne peuvent accueillir du public. Le Premier ministre a annoncé, le 10 décembre 2020, que ces lieux, tout comme les cinémas, théâtres et salles de spectacle, resteraient fermés au moins trois semaines supplémentaires après la fin de la période de confinement, fixée au 15 décembre 2020.<br/>
<br/>
              Sur la demande en référé : <br/>
<br/>
              7. Ainsi que le relèvent les requérants, la fermeture au public des musées, des salles d'exposition à vocation culturelle, des salles de spectacles et des monuments historiques en plein air porte une atteinte grave aux libertés fondamentales que constituent la liberté d'expression et la libre communication des idées, la liberté de création artistique, la liberté d'accès aux oeuvres culturelles, la liberté d'entreprendre et la liberté du commerce et de l'industrie ainsi que le droit au libre exercice d'une profession. La seule circonstance qu'une partie des activités concernées pourrait demeurer accessible au public à travers d'autres supports ou de manière dématérialisée ne saurait faire disparaître cette atteinte. <br/>
<br/>
              8. S'agissant des musées et autres lieux d'expositions à vocation culturelle, l'administration fait valoir, pour justifier le maintien de la fermeture de ces établissements, qu'il s'agit de lieux clos, à forte densité d'occupation, dans lesquels les personnes se retrouvent en contact prolongé, avec des regroupements en différents points, notamment à l'entrée et à la sortie. <br/>
              S'agissant des établissements en plein air, l'administration fait valoir que si les espaces extérieurs font courir un moindre risque de contamination, ces lieux attirent un public plus nombreux, venant souvent de plus loin, occasionnant ainsi un brassage de population de nature à favoriser la dissémination du virus. Ces caractéristiques sont associées, selon elle, à un risque important de contamination, accru par le risque de relâchement dans le respect des gestes barrières inhérent à toute activité de loisir.<br/>
<br/>
              9. Il résulte toutefois de l'instruction que les exploitants des établissements concernés ont conçu et mis en oeuvre, entre les mois de mai et octobre 2020, en lien avec les services de l'Etat, des aménagements des pratiques professionnelles et des protocoles sanitaires particulièrement stricts qui sont de nature à diminuer de manière significative le risque lié à l'existence de rassemblements dans un espace clos. Ces protocoles, outre le port obligatoire du masque, prévoient, en particulier, des limitations du nombre de visiteurs, des nettoyages réguliers, l'obligation d'utiliser des solutions hydro-alcooliques avant d'entrer dans la salle, l'organisation de la circulation des visiteurs propres à éviter les concentrations de personnes, des systèmes de réservation exclusivement en ligne, la fermeture de tous les services annexes, notamment de vente de boissons et de restauration et une information des spectateurs sur l'ensemble des contraintes à respecter. Ces établissements disposent également de la présence de gardiens pour veiller au respect de ces règles par tous les visiteurs.<br/>
<br/>
              10. L'administration ne conteste pas que ces protocoles sont de nature à réduire sensiblement les risques de contamination dans ces lieux et ne soutient pas que les visiteurs de ces établissements seraient exposés, lorsque ces protocoles sont respectés, à un risque plus important de contamination que dans d'autres établissements autorisés à accueillir du public. <br/>
<br/>
              11. Au vu de l'ensemble de ces circonstances, et en l'absence de perspective d'éradication du virus dans un avenir proche, le maintien d'une interdiction générale et absolue d'ouverture au public des musées, salles d'exposition à vocation culturelle, salles de spectacles et établissements culturels de plein air serait de nature à porter une atteinte excessive, et donc manifestement illégale, aux libertés mentionnées au point 7 si elle était justifiée par la seule persistance d'un risque de contamination de spectateurs par le virus SARS-CoV-2. Le maintien d'une telle interdiction, sur l'ensemble du territoire national ou sur une partie de celui-ci, ne peut être regardé comme une mesure nécessaire et adaptée, et, ce faisant, proportionnée à l'objectif de préservation de la santé publique qu'elle poursuit qu'en présence d'un contexte sanitaire marqué par un niveau particulièrement élevé de diffusion du virus au sein de la population susceptible de compromettre à court terme la prise en charge, notamment hospitalière, des personnes contaminées et des patients atteints d'autres affections. <br/>
<br/>
              12. En l'espèce, il résulte des données scientifiques disponibles qu'à la date du 22 décembre 2020, 2 490 946 cas ont été confirmés positifs au virus covid-19, en augmentation de 11 795 dans les dernières vingt-quatre heures, le taux de positivité des tests se situe à 4,4 % et 61 702 décès liés à l'épidémie sont à déplorer, en hausse de 386 personnes dans les dernières vingt-quatre heures. Le taux de reproduction du virus est de 1,03 et le taux d'incidence de 139,62. Le taux d'occupation des lits en réanimation par des patients atteints de la covid-19 demeure à un niveau élevé avec une moyenne nationale de 54,1 %, mettant sous tension l'ensemble du système de santé. Ces données qui, montrent une dégradation de la situation sanitaire au cours de la période récente à partir d'un plateau épidémique déjà très élevé, pourraient se révéler encore plus préoccupantes au début du mois de janvier. En outre, la détection d'un nouveau variant du SARS-CoV-2 au Royaume-Uni, avec un taux de transmission plus important, qui a conduit à fermer provisoirement les frontières avec ce pays, est de nature à accroître l'incertitude. Dans ces conditions, compte tenu du caractère très évolutif d'une situation, avec un risque d'augmentation de l'épidémie à court terme, la décision du Premier ministre, à la date de la présente ordonnance, ne porte pas une atteinte manifestement illégale aux libertés fondamentales dont se prévalent les requérants.<br/>
<br/>
              13. Il résulte de tout ce qui précède, et sans qu'il y ait lieu de se prononcer sur la condition d'urgence, que les requêtes présentées par la Société des auteurs dans les arts graphiques et plastiques, la Fédération des professionnels de l'art contemporain, la Fédération des réseaux et associations d'artistes plasticiens et la société Culturespaces doivent être rejetées, y compris les conclusions tendant à l'application de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
              ------------------<br/>
<br/>
Article 1er : Les requêtes présentées, sous le n° 447900, par la Société des auteurs dans les arts graphiques et plastiques, la Fédération des professionnels de l'art contemporain et la Fédération des réseaux et associations d'artistes plasticiens et sous le n° 447935 par la société Culturespaces, sont rejetées.<br/>
Article 2 : La présente ordonnance sera notifiée à la Société des auteurs dans les arts graphiques et plastiques, première dénommée, à la société Culturespaces et au ministre des solidarités et de la santé.<br/>
Copie en sera adressée au Premier ministre et au ministre de la culture.  <br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
