<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044361780</ID>
<ANCIEN_ID>J3_L_2021_11_00019BX01759</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/36/17/CETATEXT000044361780.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>CAA de BORDEAUX, 4ème chambre, 23/11/2021, 19BX01759, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-11-23</DATE_DEC>
<JURIDICTION>CAA de BORDEAUX</JURIDICTION>
<NUMERO>19BX01759</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème chambre</FORMATION>
<TYPE_REC>plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>Mme BALZAMO</PRESIDENT>
<AVOCATS>CABINET BLOCH O'MAHONY TISSIER AARPI</AVOCATS>
<RAPPORTEUR>M. Nicolas  NORMAND</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme CABANNE</COMMISSAIRE_GVT>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
       Procédure contentieuse antérieure :<br/>
<br/>
       La société Sodipam a demandé au tribunal administratif de la Martinique de prononcer la décharge des rappels de taxe sur la valeur ajoutée qui lui ont été réclamés au titre de la période du 1er janvier 2013 au 28 février 2016, pour un montant total de 914 795 euros et, à titre subsidiaire, de prononcer la réduction de ces impositions à concurrence de 715 552 euros.<br/>
<br/>
       Par un jugement n° 1700328 du 10 janvier 2019, le tribunal administratif de la Martinique a rejeté sa demande.<br/>
<br/>
       Procédure devant la cour :<br/>
       Par une requête et un mémoire enregistrés les 29 avril 2019 et 27 novembre 2020, la société Sodipam, représentée par Me Bersay, demande à la cour :<br/>
<br/>
       1°) d'annuler ce jugement du tribunal administratif de la Martinique du 10 janvier 2019 ;<br/>
<br/>
       2°) à titre principal, de prononcer la décharge des rappels de taxe sur la valeur ajoutée qui lui ont été réclamés au titre de la période du 1er janvier 2013 au 28 février 2016, pour un montant total de 915 096 euros incluant les intérêts de retard ;<br/>
<br/>
       3°) à titre subsidiaire, sur la base du calcul de la TVA " en dedans " et en excluant le droit de consommation de l'assiette de la TVA, de prononcer la décharge en matière de taxe sur la valeur ajoutée à hauteur de 731 181 euros et d'en tirer les conséquences en matière d'impôt sur les sociétés en application de la cascade simple ;<br/>
<br/>
       4°) à titre extrêmement subsidiaire, sur la base uniquement du calcul de la TVA " en dedans ", de prononcer la décharge en matière de taxe sur la valeur ajoutée à hauteur de 71 687 euros et d'en tirer les conséquences en matière d'impôt sur les sociétés en application de la cascade simple ;<br/>
<br/>
       5°) de condamner l'Etat à lui verser des intérêts moratoires au taux annuel de 4,80 % sur le fondement de l'article L. 208 du livre des procédures fiscales ;<br/>
<br/>
       6°) de mettre à la charge de l'Etat la somme de 6 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
       Elle soutient que :<br/>
       - le jugement est à la fois insuffisamment motivé et mal fondé ; le tribunal a ignoré l'absence de toute définition de la notion de " marges commerciales " postérieures à la fabrication ou à l'importation ; les ventes des tabacs postérieurement à l'importation ne peuvent valablement être soumises à la TVA puisque l'article 298 sexdecies du code général des impôts est inapplicable ; les travaux parlementaires ne vont pas dans le sens d'un assujettissement à la TVA des opérations de reventes en gros de tabacs manufacturés préalablement importés ; en se basant uniquement sur les documents d'importation, l'administration fiscale n'a pas pu prendre en compte les variations de stocks pourtant nécessaire au calcul éventuel d'une marge commerciale puisqu'elle n'a pas pris en compte les ventes réelles de tabacs réalisées par Sodipam ;<br/>
       - sauf à méconnaître le principe de neutralité fiscale inhérent au système de la TVA, l'assiette de la TVA lors des opérations de revente des tabacs manufacturés qu'elle avait importés, ne pouvait comprendre le droit de consommation acquitté lors de l'importation ; l'imprécision de l'article 298 sexdecies du code général des impôts et l'absence de définition de la base d'imposition par cet article contraste clairement avec l'article 292 du code général des impôts qui fixe précisément la base d'imposition de la TVA à l'importation ; à supposer qu'une TVA soit applicable sur les opérations de revente de tabacs, le droit de consommation ne peut être inclus dans l'assiette de la taxe dès lors que, conformément à la jurisprudence de la Cour de justice de l'Union Européenne (CJUE), il ne peut être regardé comme présentant un lien direct avec l'opération de vente ; en l'absence de précision concernant les impôts, droits, prélèvements et taxes à inclure dans la base d'imposition à la TVA des opérations de vente de tabac postérieurement à l'importation, il convient de se conformer aux principes généraux posés par la CJUE, selon lesquels une taxe dont le fait générateur et/ou l'exigibilité sont différents du fait générateur et/ou de l'exigibilité de la TVA sur les opérations de vente de tabac, ne saurait être incluse dans la base imposable ; <br/>
       - les dispositions prévues au II de l'article 298 quaterdecies du code général des impôts visent spécifiquement les ventes dans les départements de France métropolitaine et ne s'appliquent donc pas aux ventes en Martinique postérieures à l'importation ; <br/>
       - en omettant, à l'article 298 sexdecies du code général des impôts, de préciser les modalités de détermination de ce qui constituerait la " marge commerciale " à exclure de la base imposable, et sans qu'il soit précisé si le droit de consommation doit être inclus dans cette assiette dérogatoire du droit commun, le législateur rend impossible le calcul de la base d'imposition d'une quelconque TVA sur les ventes de tabacs postérieurement à l'importation nécessaire à l'application de cette TVA et en tout état de cause, ne permet pas d'inclure le droit de consommation dans cette assiette ; le droit de consommation ne peut être regardé comme présentant un lien direct avec l'opération de vente et, par suite, comme pouvant être inclus dans l'assiette de la taxe sur la valeur ajoutée ;<br/>
       - elle se prévaut de la documentation de base 3G-261 du 1er septembre 1998 ;<br/>
       - le calcul en dehors de l'assiette de la TVA porte atteinte au principe selon lequel la TVA est une taxe sur la consommation, qui doit être supportée, à la fin du circuit économique, par le consommateur final ; la base d'imposition retenue par l'administration fiscale est erronée ; dans l'hypothèse où les rappels de TVA seraient maintenus, il faudrait par application de la cascade que les suppléments de TVA soient déduits des bases de l'impôt sur les sociétés ;<br/>
       - une exclusion du droit de consommation de l'assiette de la TVA est justifiée par l'économie générale de la loi du 24 mai 1976 ; le droit de douane et le droit de consommation sont de nature différente, quand bien même leur exigibilité se situe au moment de l'importation, pour les produits importés.<br/>
<br/>
       Par des mémoires en défense, enregistrés les 27 novembre 2019 et 4 février 2021, le ministre de l'économie et des finances conclut au rejet de la requête. Il fait valoir que les moyens soulevés par la société requérante ne sont pas fondés.<br/>
<br/>
<br/>
       Vu les autres pièces du dossier.<br/>
<br/>
       Vu :<br/>
       - le code général des impôts et le livre des procédures fiscales ; <br/>
       - le code des douanes ;<br/>
       - le code de justice administrative.<br/>
       Les parties ont été régulièrement averties du jour de l'audience.<br/>
<br/>
       Ont été entendus au cours de l'audience publique :<br/>
       - le rapport de M. Nicolas Normand, <br/>
       - les conclusions de Mme Cécile Cabanne, rapporteure publique,<br/>
       - et les observations de Me Bitton, représentant la SARL Sodipam.<br/>
<br/>
<br/>
       Considérant ce qui suit :<br/>
<br/>
       1. La SARL Sodipam exploite une activité de commerce de gros de produits à base de tabacs et à ce titre importe du tabac en Martinique et le revend à des distributeurs locaux. Elle a fait l'objet d'une vérification de comptabilité en matière de taxe sur la valeur ajoutée au titre de la période du 1er janvier 2013 au 28 février 2016 à l'issue de laquelle le service vérificateur a considéré, eu égard aux droits de TVA déductible sur l'importation des tabacs, que les opérations de vente réalisées au profit des distributeurs locaux étaient passibles de la TVA sur le fondement des dispositions combinées des articles 298 quaterdecies et 298 sexdecies du code général des impôts. La société Sodipam relève appel du jugement du 10 janvier 2019 par lequel le tribunal administratif de la Martinique a rejeté sa demande tendant à la décharge des rappels de taxe sur la valeur ajoutée qui lui ont été réclamés au titre de la période du 1er janvier 2013 au 28 février 2016, pour un montant total de 915 096 euros incluant les intérêts de retard.<br/>
<br/>
<br/>
       Sur la régularité du jugement attaqué :<br/>
       2. Pour écarter le moyen tiré de ce que la société Sodipam ne devait pas collecter la taxe sur la valeur ajoutée sur les ventes postérieures à l'importation, le tribunal administratif a notamment répondu que " S'il résulte des dispositions précitées de l'article 298 sexdecies du code général des impôts que demeurent toutefois exclues de l'assiette de la TVA à laquelle étaient soumises les ventes de tabacs manufacturés réalisées dans les départements d'outre-mer, les marges commerciales prises postérieurement à la fabrication ou à l'importation de ces tabacs, aucune disposition législative ou réglementaire n'a prévu une exonération totale du prix de vente hors marge de ces tabacs manufacturés ". Le tribunal qui n'était pas tenu d'apporter une réponse à tous les arguments soulevés au soutien d'un tel moyen a suffisamment répondu à ce moyen.<br/>
<br/>
<br/>
       Sur le bien-fondé des impositions : <br/>
       3. Aux termes, d'une part, de l'article 298 quaterdecies du code général des impôts, dans sa rédaction applicable au présent litige : " I. Les opérations portant sur les tabacs manufacturés sont soumises à la taxe sur la valeur ajoutée dans les conditions de droit commun, sous réserve des dispositions ci-après. / II. Le fait générateur de la taxe sur la valeur ajoutée applicable aux ventes dans les départements de France continentale de tabacs manufacturés est celui qui est prévu à l'article 575 C (relatif à la mise à la consommation). / La taxe est assise sur le prix de vente au détail, à l'exclusion de la taxe sur la valeur ajoutée elle-même. / Elle est acquittée par le fournisseur dans le même délai que le droit de consommation ". Aux termes de l'article 298 sexdecies du même code : " Dans les départements de la Réunion, de la Martinique et de la Guadeloupe, les marges commerciales postérieures à la fabrication ou à l'importation demeurent exclues de la taxe sur la valeur ajoutée ". En vertu de ces dispositions, éclairées par les travaux préparatoires de l'article 15 de la loi du 24 mai 1976 dont elles sont issues, en Corse et dans les départements d'outre-mer, les opérations portant sur les tabacs manufacturés sont, à chacun des stades du circuit de commercialisation, soumises à la taxe sur la valeur ajoutée à laquelle sont soumises les ventes de tabacs manufacturés réalisées dans les conditions du droit commun. Demeurent toutefois exclues de l'assiette de la taxe sur la valeur ajoutée à laquelle sont soumises les ventes de tabacs manufacturés réalisées dans les départements d'outre-mer, les marges commerciales prises postérieurement à la fabrication ou à l'importation de ces tabacs, comme elles l'avaient été avant l'entrée en vigueur de la loi du 24 mai 1976.<br/>
       4. Aux termes d'autre part, de l'article 292 du code général des impôts dans sa rédaction alors applicable " La base d'imposition est constituée par la valeur définie par la législation douanière conformément aux règlements communautaires en vigueur. Toutefois, sont à comprendre dans la base d'imposition : 1° Les impôts, droits, prélèvements et autres taxes qui sont dus en raison de l'importation, à l'exception de la taxe sur la valeur ajoutée elle-même ; 2° Les frais accessoires, tels que les frais de commission, d'emballage, de transport et d'assurance intervenant jusqu'au premier lieu de destination des biens à l'intérieur du pays ; par premier lieu de destination, il faut entendre le lieu mentionné sur la lettre de voiture ou tout autre document de transport sous le couvert duquel les biens sont importés ; à défaut de cette mention, le premier lieu de destination est celui de la première rupture de charge. [...] ; ". Aux termes de l'article 268 du code de douanes dans sa rédaction applicable " 1. Les cigarettes, les cigares, cigarillos, les tabacs à mâcher, les tabacs à priser, les tabacs fine coupe destinés à rouler les cigarettes et les autres tabacs à fumer, destinés à être consommés dans les départements de la Guadeloupe, de la Guyane, de la Martinique et de La Réunion, sont passibles d'un droit de consommation (...] 2. Le droit de consommation est exigible soit à l'importation, soit à l'issue de la fabrication par les usines locales. ". Aux termes de 575 E du code général des impôts dans sa rédaction alors applicable " Dans les départements d'outre-mer, le droit de consommation est exigible, soit à l'importation, soit à l'issue de la fabrication par les usines locales. Il est liquidé et perçu selon les règles et garanties applicables en matière douanière ". Enfin, l'article 302 D du même code dans sa rédaction applicable dispose " L'impôt est exigible : 1° Lors de la mise à la consommation. Le produit est mis à la consommation [...] b. Lorsqu'il est importé, à l'exclusion des cas où il est placé, au moment de l'importation ".<br/>
<br/>
<br/>
       En ce qui concerne les conclusions principales :<br/>
<br/>
       Sur le terrain de la loi fiscale : <br/>
<br/>
       5.  Il résulte des dispositions précitées des articles 298 quaterdecies et 298 sexdecies du code général des impôts que la société Sodipam est légalement redevable de la taxe sur la valeur ajoutée à raison de ses opérations de vente à des détaillants des tabacs manufacturés qu'elle a importés. Les travaux parlementaires dont se prévaut la société requérante ne vont pas dans le sens d'un non assujettissement à la TVA des opérations de reventes en gros de tabacs manufacturés préalablement importés. Ainsi, en l'absence de facturation et de collecte de la taxe sur la valeur ajoutée sur son prix de vente diminué des marges commerciales réalisées à la revente, l'administration a pu sans excéder la base d'imposition prévue par les dispositions précitées, établir la taxe litigieuse sur le seul prix de revient des tabacs revendus, dès lors d'une part que les dispositions de l'article 292 précité du code général des impôts permettent de déterminer l'assiette de la taxe des biens importés à partir de sa valeur douanière et que ce prix de revient ainsi calculé n'a pas, par construction, inclus la marge commerciale qui doit être exclue de la base taxable selon les dispositions précitées. C'est à cet égard, sans portée utile que la requérante soutient que l'article 298 sexdecies du code général des impôts ne précise pas les modalités de détermination de ce qui constituerait la " marge commerciale " à exclure de la base imposable. Le principe de neutralité du régime de la taxe sur la valeur ajoutée est préservé puisque l'administration, en opérant le rappel de la taxe non collectée, n'a nullement remis en cause le droit de déduction de la taxe ayant grevé les importations des tabacs ensuite revendus dont la société était fondée à se prévaloir sur le fondement de l'article 271 du code général des impôts dès lors que ces opérations étaient soumises à la TVA. <br/>
            6.  Il résulte de ce qui précède, qu'au regard de la loi fiscale, la société n'est pas fondée à contester le bien-fondé des rappels de taxe sur la valeur ajoutée mis à sa charge qui procèdent d'une exacte application de la loi fiscale précitée.<br/>
       Sur le terrain de la doctrine administrative : <br/>
            7.    Aux termes de l'article L. 80 A du livre des procédures fiscales : " (...) Lorsque le redevable a appliqué un texte fiscal selon l'interprétation que l'administration avait fait connaître par ses instructions ou circulaires publiées et qu'elle n'avait pas rapportée à la date des opérations en cause, elle ne peut poursuivre aucun rehaussement en soutenant une interprétation différente. (...)". Si ces dispositions instituent une garantie contre les changements de doctrine de l'administration, qui permet aux contribuables de se prévaloir des énonciations contenues dans les notes ou instructions publiées, qui ajoutent à la loi ou la contredisent, c'est à la condition que les intéressés entrent dans les prévisions de la doctrine, appliquée littéralement, résultant de ces énonciations. Les contribuables ne peuvent se prévaloir utilement, sur le fondement de l'article L. 80 A précité, d'une partie seulement d'une telle doctrine dont les éléments, bien qu'énoncés successivement, sont indissociables.<br/>
       8. Sur le fondement de ces dispositions, la SARL Sodipam se prévaut de l'instruction 3G-261 du 1er septembre 1998 publiée au paragraphe 3-G-26 du bulletin officiel de la direction générale des impôts selon laquelle : " 1. Dans les départements de la Guadeloupe, de la Martinique et de la Réunion où la législation sur les taxes sur le chiffre d'affaires a été introduite, les tabacs manufacturés sont soumis à la TVA dans les conditions ci-après. 10. Dans ces départements, les marges commerciales postérieures à la fabrication ou à l'importation sont, aux termes de l'article 298 sexdecies du CGI, exclues de la TVA. Il en résulte que les négociants grossistes, dépositaires, détaillants ou débitants qui opèrent la distribution des tabacs ne doivent pas être recherchés en paiement de la taxe. 20. La TVA est exigible soit à l'importation, soit à l'issue de la fabrication, c'est-à-dire à la sortie des tabacs manufacturés des établissements de production.30. Le taux applicable aux tabacs est le taux normal applicable dans les DOM.40 Les fabricants exercent leurs droits à déduction par imputation sur la taxe due à l'issue des opérations de fabrication des tabacs et, le cas échéant, la fraction non imputable de ces droits peut faire l'objet d'un remboursement dans les conditions fixées aux articles 242 OA et suivants de l'annexe II au CGI. Dans la mesure où ils sont dispensés du paiement de la TVA, les négociants qui opèrent la distribution des tabacs dans les départements d'outre-mer ne peuvent prétendre à aucun droit à déduction ".<br/>
       9. Si cette instruction retient une interprétation différente de la loi fiscale, en ce qu'elle exclut les ventes de tabacs importés du champ d'application de la TVA, les contribuables ne peuvent se prévaloir utilement, sur le fondement de l'article L. 80 A du livre des procédures fiscales, d'une partie seulement d'une telle doctrine dont les éléments, bien qu'énoncés successivement, sont indissociables. La doctrine précitée permet aux négociants grossistes, dépositaires, détaillants ou débitants de ne pas être recherchés en paiement de la taxe collectée en contrepartie de la renonciation à leur droit de déduction de la taxe acquittée. La société requérante qui se prévaut à la fois de sa qualité d'importateur pour déduire la taxe sur la valeur ajoutée qu'elle a supportée en douane et de sa qualité de négociant grossiste pour ne pas placer ses ventes dans le champ d'application de la taxe sur la valeur ajoutée ne satisfait pas aux conditions posées par cette doctrine.<br/>
En ce qui concerne les conclusions subsidiaires :<br/>
       10. En premier lieu, en application des dispositions précitées et notamment l'article 268 du code de douanes et l'article 575 E du code général des impôts, la société Sodipam a supporté le règlement du droit de consommation sur les tabacs manufacturés qu'elle a importés. Cet impôt fait partie du prix de revient sur la base duquel a été déterminé, en l'espèce par l'administration fiscale, la taxe sur la valeur ajoutée collectée par cette société. Conformément aux dispositions précitées de l'article 292 du code général des impôts, qui transposent les dispositions des articles 85 et 86 de la directive du Conseil n° 2006/112/CE du 28 novembre 2006, modifiée, relative au système commun de la TVA auquel est soumis la société requérante, les impôts, droits, taxes et prélèvements dus à raison de l'importation de biens sont à comprendre dans la base d'imposition à la taxe sur la valeur ajoutée. Ces dispositions n'excluent d'ailleurs que la taxe sur la valeur ajoutée elle-même. C'est à cet égard, à tort, que la requérante soutient que le droit de consommation ne peut être inclus dans l'assiette de la taxe en ce que selon la jurisprudence de la Cour de justice de l'Union Européenne, il ne pourrait être regardé comme présentant un lien direct avec l'opération de vente. Il suit de là que le droit de consommation fait partie du prix de revient supporté par l'importateur sur la base duquel la taxe sur la valeur ajoutée est appliquée. En outre, contrairement à ce que soutient la requérante, il ne peut être déduit ni de l'article 298 sexdecies précité du code général des impôts ni de travaux parlementaires que le législateur a entendu exclure de l'assiette de la taxe sur la valeur ajoutée le droit de consommation. Il résulte de ce qui précède, qu'alors même que les faits générateurs du droit de consommation d'une part, et de la taxe sur la valeur ajoutée d'autre part, sont comme le soutient la requérante, différents, que l'assiette du droit de consommation, par opposition à un droit de douane calculé sur le prix d'achat du bien importé, ne correspond pas au coût d'achat des produits mais au prix de vente au détail des tabacs dont est redevable l'importateur, que le droit de consommation perçu à l'importation n'est pas la contrepartie directe d'une livraison de bien, que les services de recouvrement de ces deux taxes ne sont pas les mêmes et que les règles de comptabilisation de ces impositions sont distinctes, la société requérante n'est pas fondée à soutenir que le droit de consommation acquitté à l'importation n'est pas compris dans la base d'imposition de la TVA due à l'importation.<br/>
            11.  En second lieu, la société requérante soutient que la base d'imposition retenue par l'administration est erronée en ce qu'elle avait l'obligation de calculer la TVA " en dedans " par rapport au prix convenu. <br/>
       12. Il est constant que la taxe sur la valeur ajoutée dont est redevable un vendeur est un élément qui grève le prix convenu avec le client et non un accessoire de ce prix. De ce fait, l'assiette de la taxe sur la valeur ajoutée est égale au prix convenu entre les parties, diminué notamment de la taxe exigible sur cette opération. Par suite, lorsqu'un assujetti réalise une opération moyennant un prix convenu qui ne mentionne aucune TVA dans des conditions qui ne font pas apparaître que les parties seraient convenues d'ajouter, au prix stipulé, un supplément de prix égal à la TVA applicable à l'opération, la taxe due au titre de cette opération doit être assise sur une somme égale à ce prix diminué notamment du montant de cette même taxe.<br/>
       13. En l'espèce, il résulte de ce qui précède que le vérificateur avait, en l'absence de toute TVA collectée par la société requérante, le droit de reconstituer celle-ci à partir du coût de revient du tabac importé lequel d'ailleurs ne comporte, à ce stade du circuit de commercialisation, aucune marge pour l'importateur. Il suit de là que la requérante n'est pas fondée à soutenir que la base d'imposition à la TVA est pour les motifs allégués, excessive.<br/>
       Sur le bénéfice de la cascade :<br/>
       14. Aux termes de l'article L. 77 du livre des procédures fiscales " En cas de vérification simultanée des taxes sur le chiffre d'affaires et taxes assimilées, de l'impôt sur le revenu ou de l'impôt sur les sociétés, le supplément de taxes sur le chiffre d'affaires et taxes assimilées afférent à un exercice donné est déduit, pour l'assiette de l'impôt sur le revenu ou de l'impôt sur les sociétés, des résultats du même exercice, sauf demande expresse des contribuables, formulée dans le délai qui leur est imparti pour répondre à la proposition de rectification (...). ".<br/>
       15. En l'absence des rehaussements apportés à l'impôt sur les sociétés, le moyen tendant au bénéfice de la cascade simple est inopérant.<br/>
       Sur les conclusions tendant au versement des intérêts moratoires :<br/>
            16.  Aux termes de l'article L. 208 du livre des procédures fiscales : " Quand l'Etat est condamné à un dégrèvement d'impôt par un tribunal ou quand un dégrèvement est prononcé par l'administration à la suite d'une réclamation tendant à la réparation d'une erreur commise dans l'assiette ou le calcul des impositions, les sommes déjà perçues sont remboursées au contribuable et donnent lieu au paiement d'intérêts moratoires dont le taux est celui de l'intérêt de retard prévu à l'article 1727 du code général des impôts. Les intérêts courent du jour du paiement. Ils ne sont pas capitalisés (...) ".<br/>
       17. En l'absence, tant devant le tribunal administratif qu'en appel, de litige né et actuel relatif à un refus de paiement des intérêts moratoires dus au contribuable au titre de l'article L. 208 du livre des procédures fiscales, les conclusions de la société Sodipam tendant au paiement de ces intérêts sont sans objet, et, par suite, irrecevables.<br/>
       18. Il résulte de tout ce qui précède que la société Sodipam n'est pas fondée à soutenir que c'est à tort que par le jugement attaqué, le tribunal administratif de la Martinique a rejeté sa demande.<br/>
       Sur les frais liés au litige : <br/>
       19. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que l'Etat, qui n'est pas la partie perdante dans la présente instance, soit condamné à verser à la société Sodipam la somme qu'elle demande au titre des frais exposés par elle et non compris dans les dépens.<br/>
DECIDE :<br/>
Article 1er : La requête de la société Sodipam est rejetée.<br/>
<br/>
Article 2 : Le présent arrêt sera notifié à la société Sodipam et au ministre de l'économie, des finances et de la relance.<br/>
Copie en sera adressée à la direction spécialisée de contrôle fiscal sud-ouest.<br/>
Délibéré après l'audience du 19 octobre 2021 à laquelle siégeaient :<br/>
Mme Evelyne Balzamo, présidente,<br/>
M. Dominique Ferrari, président-assesseur,<br/>
M. Nicolas Normand, premier conseiller,<br/>
<br/>
Rendu public par mise à disposition au greffe le 23 novembre 2021.<br/>
<br/>
<br/>
       Le rapporteur,<br/>
<br/>
<br/>
<br/>
       Nicolas Normand<br/>
               La présidente,<br/>
<br/>
<br/>
<br/>
       Evelyne Balzamo<br/>
               La greffière,<br/>
<br/>
<br/>
<br/>
       Camille Péan       <br/>
      La République mande et ordonne au ministre de l'économie, des finances et de la relance en ce qui le concerne, et à tous huissiers de justice à ce requis en ce qui concerne les voies de droit commun, contre les parties privées, de pourvoir à l'exécution du présent arrêt.<br/>
3<br/>
N° 19BX01759<br/>
<br/>
<br/>
<br/>
</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">19-06-02 Contributions et taxes. - Taxes sur le chiffre d'affaires et assimilées. - Taxe sur la valeur ajoutée.
</SCT>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
