<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037271444</ID>
<ANCIEN_ID>JG_L_2018_07_000000422226</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/27/14/CETATEXT000037271444.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 19/07/2018, 422226, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-07-19</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>422226</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP POTIER DE LA VARDE, BUK LAMENT, ROBILLOT</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2018:422226.20180719</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A...B...a demandé au juge des référés du tribunal administratif de Nancy, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, d'enjoindre au président du conseil départemental de Meurthe-et-Moselle de lui assurer, dans le délai de trois heures à compter de l'ordonnance à intervenir, l'hébergement, la vêture et la nourriture et de pourvoir à ses besoins, y compris médicaux, sous astreinte de 200 euros par jour de retard, jusqu'à la décision du juge des enfants. Par une ordonnance n° 1801664 du 22 juin 2018, le juge des référés du tribunal administratif de Nancy a rejeté cette demande. <br/>
<br/>
              Par une requête, enregistrée le 13 juillet 2016 au secrétariat du contentieux du Conseil d'Etat, M. B...demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) de faire droit à sa demande présentée devant le juge des référés du tribunal administratif de Nancy ;<br/>
<br/>
              3°) de mettre à la charge du département de Meurthe-et-Moselle la somme de 3 000 euros à verser à la SCP Potier de la Varde, Buk Lament, Robillot au titre des articles 37 de la loi du 10 juillet 1991 et L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Il soutient que : <br/>
              - la condition d'urgence est remplie dès lors que la décision mettant fin à sa prise en charge au titre de l'aide sociale à l'enfance en qualité de mineur étranger isolé le place dans une situation de danger et de détresse ;<br/>
              - le doute quant à sa minorité devait lui profiter, tant s'agissant de l'authenticité des documents d'identification qu'il détenait que des examens radiologiques osseux pratiqués sur sa personne, dont les conclusions étaient nuancées et n'excluaient pas sa minorité, de sorte que c'est à tort que le juge des référés du tribunal administratif de Nancy a estimé que le département n'avait pas porté une atteinte grave et manifestement illégale au droit à l'hébergement et à la prise en charge éducative d'un enfant mineur en mettant fin à sa prise en charge sans que soit intervenue de saisine du juge des enfants ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la Constitution, notamment son Préambule ;<br/>
              - le code de l'action sociale et des familles ;<br/>
              - le code civil ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. Le juge des référés se prononce dans un délai de quarante-huit heures ". En vertu de l'article L. 522-3 du même code, le juge des référés peut rejeter une requête par une ordonnance motivée, sans instruction contradictoire ni audience publique, lorsque la condition d'urgence n'est pas remplie ou lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable ou qu'elle est mal fondée. A cet égard, il appartient au juge d'appel de prendre en considération les éléments recueillis par le juge du premier degré dans le cadre de la procédure écrite et orale qu'il a diligentée.<br/>
<br/>
              2. Il résulte de l'instruction menée par le juge des référés du tribunal administratif de Nancy que M.B..., qui indique être un ressortissant guinéen né le 15 mai 2001, a été pris en charge le 23 octobre 2017 par le service d'aide sociale à l'enfance du conseil départemental de Meurthe-et-Moselle en qualité de mineur étranger isolé au titre de l'accueil provisoire d'urgence. A l'issue de l'évaluation de sa situation et au vu d'un courrier du 13 juin 2018 du parquet indiquant au département que les éléments recueillis lors de l'évaluation n'avaient pas permis d'établir que l'intéressé était mineur et isolé, que l'expertise documentaire des pièces d'état civil produites conduisait à les écarter comme irrecevables et que l'expertise médicale diligentée concluait à un âge supérieur à 18 ans et invitant en conséquence le département à notifier à M. B...un refus de prise en charge, le président du conseil départemental a, le 19 juin 2018, mis fin à sa prise en charge à compter du lendemain. Par courrier du 20 juin 2018, M. B...a saisi de sa situation le juge des enfants du tribunal de grande instance de Nancy. M. B... relève appel de l'ordonnance du 22 juin 2018 par laquelle le juge des référés du tribunal administratif de Nancy, saisi sur le fondement de l'article L. 521-2 du code de justice administrative, a rejeté sa demande tendant à ce qu'il soit enjoint au président du conseil départemental de Meurthe-et-Moselle de lui assurer, dans le délai de trois heures à compter de l'ordonnance à intervenir, l'hébergement, la vêture et la nourriture et de pourvoir à ses besoins, y compris médicaux, sous astreinte de 200 euros par jour de retard, jusqu'à la décision du juge des enfants.<br/>
<br/>
              3. Aux termes du deuxième et du quatrième alinéa de l'article L. 223-2 du code de l'action sociale et des familles : " En cas d'urgence et lorsque le représentant légal du mineur est dans l'impossibilité de donner son accord, l'enfant est recueilli provisoirement par le service qui en avise immédiatement le procureur de la République. / Si, dans le cas prévu au deuxième alinéa du présent article, l'enfant n'a pas pu être remis à sa famille ou le représentant légal n'a pas pu ou a refusé de donner son accord dans un délai de cinq jours, le service saisit également l'autorité judiciaire en vue de l'application de l'article 375-5 du code civil. ". L'article 375-5 du code civil dispose que dans cette situation, le procureur de la République ou le juge des enfants auquel la situation d'un mineur isolé a été signalée décide de l'orientation du mineur concerné, laquelle peut consister en application de l'article 375-3 du même code en son admission à l'aide sociale à l'enfance. En revanche, si le département qui a recueilli la personne refuse de saisir l'autorité judiciaire, notamment parce qu'il estime que cette personne a atteint la majorité, cette personne peut saisir elle-même le juge des enfants en application de l'article 375 du code civil afin qu'il soit décidé de son orientation.<br/>
<br/>
              4. Aux termes de l'article R. 221-11 du code de l'action sociale et des familles : " I.- Le président du conseil départemental du lieu où se trouve une personne se déclarant mineure et privée temporairement ou définitivement de la protection de sa famille met en place un accueil provisoire d'urgence d'une durée de cinq jours, à compter du premier jour de sa prise en charge, selon les conditions prévues aux deuxième et quatrième alinéas de l'article L. 223-2. / II.- Au cours de la période d'accueil provisoire d'urgence, le président du conseil départemental procède aux investigations nécessaires en vue d'évaluer la situation de cette personne au regard notamment de ses déclarations sur son identité, son âge, sa famille d'origine, sa nationalité et son état d'isolement. Cette évaluation s'appuie essentiellement sur : 1° Des entretiens conduits par des professionnels justifiant d'une formation ou d'une expérience définies par un arrêté des ministres mentionnés au III dans le cadre d'une approche pluridisciplinaire et se déroulant dans une langue comprise par l'intéressé / 2° Le concours du préfet de département sur demande du président du conseil départemental pour vérifier l'authenticité des documents d'identification détenus par la personne ; / 3° Le concours de l'autorité judiciaire, s'il y a lieu, dans le cadre du second alinéa de l'article 388 du code civil. / III.- L'évaluation est réalisée par les services du département, ou par toute structure du secteur public ou du secteur associatif à laquelle la mission d'évaluation a été déléguée par le président du conseil départemental. L'évaluation est conduite selon les modalités précisées dans un référentiel national fixé par arrêté interministériel du ministre de la justice, du ministre de l'intérieur, du ministre chargé de la famille et du ministre chargé de l'outre-mer. IV.- Au terme du délai mentionné au I, ou avant l'expiration de ce délai si l'évaluation a été conduite avant son terme, le président du conseil départemental saisit le procureur de la République en vertu du quatrième alinéa de l'article L. 223-2 et du second alinéa de l'article 375-5 du code civil. En ce cas, l'accueil provisoire d'urgence mentionné au I se prolonge tant que n'intervient pas une décision de l'autorité judiciaire. / S'il estime que la situation de la personne mentionnée au présent article ne justifie pas la saisine de l'autorité judiciaire, il notifie à cette personne une décision de refus de prise en charge (...). En ce cas, l'accueil provisoire d'urgence mentionné au I prend fin ". L'article 388 du code civil régit les conditions dans lesquelles un individu est regardé comme mineur et son deuxième alinéa prévoit que " les examens radiologiques osseux aux fins de détermination de l'âge, en l'absence de documents d'identité valables et lorsque l'âge allégué n'est pas vraisemblable, ne peuvent être réalisés que sur décision de l'autorité judiciaire et après recueil de l'accord de l'intéressé. ".<br/>
<br/>
              5. En l'espèce, ainsi qu'il a été dit au point 2 et qu'il n'est d'ailleurs pas contesté, M.B..., s'étant déclaré mineur et isolé, a été pris en charge à compter du 23 octobre 2017 par le département de Meurthe-et-Moselle au titre de l'accueil provisoire d'urgence prévu par les dispositions mentionnées aux points précédents. Dans ce cadre, il a fait l'objet de l'évaluation prévue par l'article R. 221-11 du code de l'action sociale et des familles par les services du département, avec lesquels il a bénéficié de deux entretiens les 18 et 22 décembre 2017 et lors de laquelle le département a, au vu des doutes suscités par ces entretiens quant à la minorité de l'intéressé, sollicité d'abord le concours du préfet puis celui de l'autorité judiciaire dans les conditions permises par le II de cet article. Le 13 juin 2018, le parquet a fait connaître au département, qui ne pouvait admettre M. B...à l'aide sociale à l'enfance au-delà de la période de l'accueil provisoire d'urgence sans que l'autorité judiciaire ne l'ait ordonné, que les éléments recueillis lors de l'évaluation n'avaient pas permis d'établir que l'intéressé était mineur et isolé, que l'expertise documentaire des pièces d'état civil produites conduisait à les écarter comme irrecevables et que l'expertise médicale diligentée concluait à un âge supérieur à dix-huit ans et a invité en conséquence le département à notifier à M. B...un refus de prise en charge. La saisine par M. B...du juge des enfants du tribunal de grande instance de Nancy le 20 juin 2018 n'a, à ce jour, conduit à aucune mesure de placement par ce dernier, même à titre provisoire ainsi que l'article 375-5 du code civil le lui permet au cours de l'instance devant lui.<br/>
<br/>
              6. Dans ces conditions, le refus du département de Meurthe-et-Moselle de poursuivre la prise en charge de l'intéressé au titre de l'accueil provisoire d'urgence ne révèle aucune atteinte grave et manifestement illégale au droit à l'hébergement et à la prise en charge éducative d'un enfant mineur. M.B..., qui n'apporte, en appel, aucun élément de nature à infirmer l'appréciation ainsi portée par le juge des référés du tribunal administratif de Nancy, n'est donc manifestement pas fondé à soutenir que c'est à tort que celui-ci a rejeté sa demande tendant à ce qu'il soit enjoint au président du conseil départemental de Meurthe-et-Moselle de lui assurer l'hébergement, la vêture et la nourriture et de pourvoir à ses besoins, y compris médicaux, jusqu'à la décision du juge des enfants. Son appel, y compris les conclusions tendant à l'application des articles 37 de la loi du 10 juillet 1991 et L. 761-1 du code de justice administrative, ne peut en conséquence qu'être rejeté selon la procédure prévue par l'article L. 522-3 du même code.<br/>
<br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de M. B...est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à M. A...B....<br/>
Copie en sera adressée pour information au département de Meurthe-et-Moselle. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
