<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030200587</ID>
<ANCIEN_ID>JG_L_2015_01_000000382824</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/20/05/CETATEXT000030200587.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème / 3ème SSR, 21/01/2015, 382824, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-01-21</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>382824</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème / 3ème SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Esther de Moustier</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Nathalie Escaut</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:382824.20150121</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>		VU LA PROCEDURE SUIVANTE :<br/>
<br/>
	Procédure contentieuse antérieure <br/>
<br/>
              Mme J...A..., a saisi le tribunal administratif de Châlons-en-Champagne d'une protestation tendant à l'annulation des opérations électorales qui se sont déroulées le 23 mars 2014 pour l'élection des conseillers municipaux de la commune de Montcy-Notre-Dame. <br/>
<br/>
              Par un jugement n° 1400639 du 19 juin 2014, le tribunal administratif de Châlons-en-Champagne a annulé les opérations électorales qui se sont déroulées le 23 mars 2014 pour l'élection des conseillers municipaux de la commune de Montcy-Notre-Dame et des conseillers communautaires délégués de la commune de Montcy-Notre-Dame à la communauté d'agglomération de Charleville-Mézières / Sedan, ainsi que l'élection du maire et des adjoints de Montcy-Notre-Dame. <br/>
<br/>
		Procédure devant le Conseil d'Etat <br/>
<br/>
              Par une requête d'appel et un mémoire en réplique, enregistrés les 18 juillet et 22 septembre 2014 au secrétariat du contentieux du Conseil d'Etat, M. F...C...et M. D... E..., demandent au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler ce jugement du 19 juin 2014 du tribunal administratif de Châlons-en-Champagne ; <br/>
<br/>
              2°) de rejeter la protestation de Mme A...contre ces opérations électorales ; <br/>
<br/>
              3°) de mettre à la charge de Mme A...la somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu :<br/>
              - les autres pièces du dossier ;<br/>
              - le code électoral ;<br/>
              - le code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Esther de Moustier, auditeur,  <br/>
<br/>
              - les conclusions de Mme Nathalie Escaut, rapporteur public.<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'à l'issue du premier tour des opérations électorales qui se sont déroulées le 23 mars 2014 dans la commune de Montcy-Notre-Dame (Ardennes) en vue de l'élection des membres du conseil municipal et des conseillers communautaires, la liste conduite par M. C...a recueilli 52,16% des suffrages exprimés et obtenu 15 sièges au conseil municipal et la liste adverse conduite par M. G...4 sièges ; que MM. C...et E...relèvent appel du jugement du 19 juin 2014 par lequel le tribunal administratif de Châlons-en-Champagne a annulé, sur la protestation de MmeA..., les opérations électorales ; <br/>
<br/>
              2. Considérant qu'aux termes du second alinéa de l'article L. 52-1 du code électoral : " A compter du premier jour du sixième mois précédant le mois au cours duquel il doit être procédé à des élections générales, aucune campagne de promotion publicitaire des réalisations ou de la gestion d'une collectivité ne peut être organisée sur le territoire des collectivités intéressées par le scrutin. Sans préjudice des dispositions du présent chapitre, cette interdiction ne s'applique pas à la présentation, par un candidat ou pour son compte, dans le cadre de l'organisation de sa campagne, du bilan de la gestion des mandats qu'il détient ou qu'il a détenus. Les dépenses afférentes sont soumises aux dispositions relatives au financement et au plafonnement des dépenses électorales contenues au chapitre V bis du présent titre. " ; que l'article L. 52-8 du même code dispose que : " (...) Les personnes morales, à l'exception des partis ou groupements politiques, ne peuvent participer au financement de la  campagne électorale d'un candidat, ni en lui consentant des dons sous quelque forme que ce soit, ni en lui fournissant des biens, services ou autres avantages directs ou indirects à des prix inférieurs à ceux qui  sont habituellement pratiqués (...) " ;<br/>
<br/>
              3. Considérant qu'il résulte de l'instruction que la commune de Montcy-Notre-Dame a publié un nouveau numéro du journal de la municipalité comprenant, d'une part, des informations pratiques relatives à l'état civil, aux associations et commerces du village ainsi qu'à diverses manifestations organisées sur son territoire, d'autre part, l'analyse détaillée des réalisations de la commune au cours des dernières années dans l'ensemble des domaines de compétence de celle-ci, illustrée par de nombreuses photos, où figurait fréquemment le maire ; que cette publication était rédigée en des termes mesurés et reprenait le format d'éditions antérieures ; que, toutefois, bien que datée de décembre 2013, elle a été diffusée seulement en février 2014, après une longue période d'interruption de plus de trois ans et demi, la précédente édition étant parue en mai 2010 ; que, compte tenu de son caractère exceptionnel et du moment choisi pour sa diffusion, à l'approche des élections municipales, pendant la période mentionnée par les dispositions du second alinéa de l'article L. 52-1 du code électoral, cette publication doit être regardée comme ayant eu pour objet de valoriser l'action de la municipalité sortante et a eu un effet de propagande électorale ; qu'elle doit, par suite, être regardée comme revêtant le caractère d'une campagne de promotion publicitaire au sens du second alinéa de cet article ; qu'il n'est en outre pas contesté que les frais de conception, d'impression et de distribution de l'édition litigieuse du journal de la municipalité ont été pris en charge par la commune ; que dès lors que cette publication revêt le caractère d'une campagne de promotion publicitaire au sens du second alinéa de l'article L. 52-1 du code électoral, sa prise en charge par la collectivité constitue un avantage consenti par la municipalité en faveur de la liste conduite par le maire sortant, prohibé par les dispositions de l'article L. 52-8 du code électoral ; <br/>
<br/>
              4. Considérant, toutefois, qu'eu égard à l'écart de voix séparant les deux listes à l'issue du scrutin, de 39 voix sur 899 suffrages exprimés, soit 4,3 % des suffrages exprimés, la méconnaissance, par la municipalité sortante des articles L. 52-1 et L. 52-8 du code électoral n'a pas été de nature à altérer la sincérité du scrutin ; que, par suite, les requérants sont fondés à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Châlons-en-Champagne s'est fondé sur ce motif pour annuler les opérations électorales qui se sont déroulées dans la commune de Montcy-Notre-Dame le 23 mars 2014 ; <br/>
<br/>
              5. Considérant qu'il appartient au Conseil d'Etat, saisi de l'ensemble du litige par l'effet dévolutif de l'appel, d'examiner les autres griefs soulevés par Mme A...dans sa protestation devant le tribunal ;<br/>
<br/>
              6. Considérant qu'il résulte de l'instruction que l'octroi par le maire de la commune de bons d'achats d'un montant de 50 euros à trois employés communaux en contrat aidé, dont au demeurant un seul était électeur à Montcy-Notre-Dame, constitue une pratique habituelle en période de fêtes de Noël et ne saurait être regardé comme un don ou une libéralité consentis en méconnaissance de l'article L. 106 du code électoral ; <br/>
<br/>
              7. Considérant que si Mme A...soutient que des travaux ont été entrepris à l'initiative du maire le 20 mars 2014 dans une cour privée, il résulte de l'instruction que ces travaux, d'importance mineure, ont été entrepris à titre gracieux et sans intervention du maire, à la seule initiative de riverains et de l'entreprise de construction Colas qui disposait de gravas excédentaires issus d'un chantier qu'elle exploitait à proximité ; <br/>
<br/>
              8. Considérant qu'il résulte de l'instruction que les tracts dont Mme A...soutient qu'ils ont été distribués dans la nuit du vendredi 21 au samedi 22 mars 2014, en méconnaissance des dispositions de l'article L. 49 du code électoral, ont été distribués dans la soirée du jeudi 20 mars 2014 ; <br/>
<br/>
              9. Considérant que si Mme A...soutient que le maire sortant a poursuivi sa campagne électorale dans les bureaux de vote le jour du scrutin, les deux attestations qu'elle produit à l'appui de cette allégation, émanant respectivement de M. et Mme H...qui se bornent à affirmer qu'ils ont entendu M. C...et MmeB..., conseillère municipale sortante, poursuivre ouvertement leur campagne électorale sans autre précision, ne suffisent pas à établir la réalité de ces affirmations ; <br/>
<br/>
              10. Considérant que la circonstance que des isoloirs aient été placés à proximité de fenêtres du bureau de vote n'a pas été de nature à entacher le secret du scrutin ; <br/>
<br/>
              11. Considérant, enfin, que la circonstance que le procès-verbal des opérations de vote a recensé 951 enveloppes pour 950 signatures est en tout état de cause sans incidence sur le résultat du scrutin, eu égard notamment à l'écart entre les deux listes à l'issue du scrutin qui s'établissait à 39 voix ; <br/>
<br/>
              12. Considérant qu'il résulte de ce qui précède, sans qu'il soit besoin de répondre aux fins de non-recevoir opposées aux griefs soulevés par MmeA..., que celle-ci n'est pas fondée à demander l'annulation des opérations électorales ; <br/>
<br/>
              13. Considérant qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par M. C...et M. E...au titre de l'article L. 761-1 du code de justice administrative ; que ces dispositions font obstacle à ce qu'une somme soit mise à ce titre à la charge des requérants qui ne sont pas la partie perdante en la présente instance ; <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du tribunal administratif de Châlons-en-Champagne du 19 juin 2014 est annulé.<br/>
Article 2 : Les opérations électorales qui se sont déroulées le 23 mars 2014 dans la commune de Montcy-Notre-Dame sont validées.<br/>
Article 3 : La protestation de Mme A...et ses conclusions présentées sur le fondement de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
Article 4 : Les conclusions présentées par MM. C...et E...sur le fondement de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
Article 5 : La présente décision sera notifiée à M. F... C..., à M. D... E..., à Mme J...A..., à M. I... G...et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
