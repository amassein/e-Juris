<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037492968</ID>
<ANCIEN_ID>JG_L_2018_10_000000401292</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/49/29/CETATEXT000037492968.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème - 10ème chambres réunies, 12/10/2018, 401292, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-10-12</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>401292</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème - 10ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP NICOLAY, DE LANOUVELLE, HANNOTIN</AVOCATS>
<RAPPORTEUR>M. Matias de Sainte Lorette</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Marie-Astrid Nicolazo de Barmon</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:401292.20181012</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. et Mme B...A...ont demandé au tribunal administratif de Montpellier de prononcer la réduction des cotisations supplémentaires de contributions sociales auxquelles ils ont été assujettis au titre de l'année 2008, ainsi que des pénalités correspondantes. Par un jugement n° 1202753 du 13 juin 2013, le tribunal administratif de Montpellier a rejeté leur demande.<br/>
<br/>
              Par un arrêt n° 13MA02809 du 9 mai 2016, la cour administrative d'appel de Marseille a rejeté l'appel formé par M. et Mme A... contre ce jugement. <br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 8 juillet et 10 octobre 2016 et le 4 juin 2018 au secrétariat du contentieux du Conseil d'Etat, M. et Mme A... demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à leur appel ;<br/>
<br/>
              3°) de mettre à la charge de L'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - le code de l'action sociale et des familles ;<br/>
              - le code de la sécurité sociale ;<br/>
              - la loi n° 90-1168 du 28 décembre 1990 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Matias de Sainte Lorette, maître des requêtes en service extraordinaire,<br/>
<br/>
              - les conclusions de Mme Marie-Astrid Nicolazo de Barmon, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Nicolaÿ, de Lanouvelle, Hannotin, avocat de M. et MmeA....<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que Mme A...a fait apport à la société anonyme Editions du voyage, le 16 novembre 1990, d'un fonds de commerce d'édition et transactions publicitaires qui lui appartenait, et a opté, en application des dispositions de l'article 151 octies du code général des impôts, pour le report de l'imposition de la plus-value professionnelle constatée à l'occasion de cet apport. Mme A... a cédé le 2 décembre 2008 les titres reçus en contrepartie de l'apport. La plus-value en report d'imposition a été exonérée d'impôt sur le revenu sur le fondement des dispositions de l'article 151 septies A du code général des impôts. Par proposition de rectification en date du 18 mai 2011, l'administration a informé M. et Mme A...de son intention de soumettre la plus-value en report d'imposition aux prélèvements sociaux. M. et Mme A...se pourvoient en cassation contre l'arrêt du 9 mai 2016 par lequel la cour administrative d'appel de Marseille a rejeté leur appel contre le jugement du 13 juin 2013 par lequel le tribunal administratif de Montpellier a rejeté leur demande tendant à la décharge des cotisations supplémentaires de contributions sociales, ainsi que des pénalités correspondantes, auxquelles ils ont été assujettis au titre de l'année 2008.<br/>
<br/>
              2. Aux termes de l'article 151 octies du code général des impôts, dans sa rédaction applicable au litige : " I. Les plus-values soumises au régime des articles 39 duodecies à 39 quindecies et réalisées par une personne physique à l'occasion de l'apport à une société soumise à un régime réel d'imposition d'une entreprise individuelle ou d'une branche complète d'activité peuvent bénéficier des dispositions suivantes : / a. L'imposition des plus-values afférentes aux immobilisations non amortissables fait l'objet d'un report jusqu'à la date de la cession, du rachat ou de l'annulation des droits sociaux reçus en rémunération de l'apport de l'entreprise ou jusqu'à la cession de ces immobilisations par la société si elle est antérieure. (...) II. Le régime défini au I s'applique : / a. Sur simple option exercée dans l'acte constatant la constitution de la société, lorsque l'apport de l'entreprise est effectué à une société en nom collectif, une société en commandite simple, une société à responsabilité limitée dans laquelle la gérance est majoritaire ou à une société civile exerçant une activité professionnelle ; / (...) L'option est exercée dans l'acte d'apport conjointement par l'apporteur et la société ; elle entraîne l'obligation de respecter les règles prévues au présent article. ". Aux termes du IV bis de l'article 151 septies A du même code, dans sa rédaction applicable : " En cas de cession à titre onéreux de parts ou d'actions de sociétés passibles de l'impôt sur les sociétés ou d'un impôt équivalent ou soumises sur option à cet impôt, rendant imposable une plus-value en report d'imposition sur le fondement (...) du a du I de l'article 151 octies, (...) cette plus-value en report est exonérée, lorsque les conditions suivantes sont réunies : / 1° Le cédant : / a) Doit avoir exercé, de manière continue pendant les cinq années précédant la cession, l'une des fonctions énumérées au 1° de l'article 885 O bis et dans les conditions prévues au même 1° dans la société dont les titres sont cédés ; / b) Cesse toute fonction dans la société dont les titres sont cédés et fait valoir ses droits à la retraite, soit dans l'année suivant la cession, soit dans l'année précédant celle-ci si ces événements sont postérieurs au 31 décembre 2005 ; (...)  ". Aux termes de l'article L. 136-6 du code de la sécurité sociale, dans sa rédaction applicable : " I.-Les personnes physiques fiscalement domiciliées en France au sens de l'article 4 B du code général des impôts sont assujetties à une contribution sur les revenus du patrimoine assise sur le montant net retenu pour l'établissement de l'impôt sur le revenu, à l'exception de ceux ayant déjà supporté la contribution au titre des articles L. 136-3 et L. 136-7 : / e) Des plus-values, gains en capital et profits réalisés sur les marchés à terme d'instruments financiers et de marchandises  (...) soumis à l'impôt sur le revenu à un taux proportionnel (...) / II. bis.-Les personnes physiques fiscalement domiciliées en France au sens de l'article 4 B du code général des impôts sont également assujetties à la contribution mentionnée au I à raison des plus-values exonérées d'impôt sur le revenu en application du 7 du III de l'article 150-0 A dudit code. Il en est de même pour les plus-values à long terme exonérées en application de l'article 151 septies A du code général des impôts (...). / III.-La contribution portant sur les revenus mentionnés aux I et II ci-dessus est assise, contrôlée et recouvrée selon les mêmes règles et sous les mêmes sûretés, privilèges et sanctions que l'impôt sur le revenu. (...)Il en est de même pour la contribution mentionnée au II bis dont l'assiette est calculée conformément aux dispositions de l'article 150-0 D du code général des impôts. ". <br/>
<br/>
              3. Aux termes de l'article 1600-0 G du code général des impôts, dans sa rédaction applicable : " I. Les personnes physiques désignées à l'article L. 136-1 du code de la sécurité sociale sont assujetties à une contribution perçue à compter de 1996 et assise sur les revenus du patrimoine définis au I de l'article L. 136-6 du même code. / Cette contribution est établie chaque année sur les revenus de l'année précédente. Toutefois, la contribution due sur les revenus de la première année d'imposition est assise sur les onze douzièmes des revenus de l'année 1995. / Elle est établie, recouvrée et contrôlée dans les conditions et selon les modalités prévues au III de l'article L. 136-6 du code de la sécurité sociale, à l'exception du troisième alinéa. ". Aux termes de l'article 1600-0 H du même code, dans sa rédaction applicable : " Sont également assujettis à la contribution mentionnée à l'article 1600-0 G, dans les conditions et selon les modalités prévues aux I et II de cet article : / (...) 5. Les plus-values à long terme exonérées d'impôt en application de l'article 151 septies A. ".  Aux termes de l'article L. 245-14 du code de la sécurité sociale, dans sa rédaction applicable : " Les personnes physiques fiscalement domiciliées en France au sens de l'article 4 B du code général des impôts sont assujetties à un prélèvement sur les revenus et les sommes visés à l'article L. 136-6. Les dispositions du III de l'article L. 136-6 sont applicables à ce prélèvement. ". Aux termes de l'article L. 14-10-4 du code de l'action sociale et des familles : " Les produits affectés à la Caisse nationale de solidarité pour l'autonome sont constitués par : / (...) 2° Une contribution additionnelle au prélèvement social mentionné à l'article L. 245-14 du code de la sécurité sociale et une contribution additionnelle au prélèvement social mentionné à l'article L. 245-15 du même code. Ces contributions additionnelles sont assises, contrôlées, recouvrées et exigibles dans les mêmes conditions et sous les mêmes sanctions que celles applicables à ces prélèvements sociaux. Leur taux est fixé à 0,3 % ; ".<br/>
<br/>
              4. Il résulte de la combinaison des dispositions citées ci-dessus que les plus-values exonérées d'impôt sur le revenu sur le fondement de l'article 151 septies A du code général des impôts sont expressément soumises à la contribution sociale généralisée. Il en va de même lorsque ces plus-values avaient été placées en report en application des dispositions de l'article 151 octies du même code, dès lors que ces dernières dispositions n'ont pas pour effet de différer le paiement d'une imposition qui aurait été établie au titre de l'année de réalisation de la plus-value, mais seulement de permettre, par dérogation à la règle suivant laquelle le fait générateur de l'imposition d'une plus-value est constitué au cours de l'année de sa réalisation, de la rattacher à l'année au cours de laquelle intervient l'événement qui met fin au report d'imposition. <br/>
<br/>
              5. En premier lieu, la contribution sociale généralisée mentionnée à l'article L. 136-6 du code de la sécurité sociale, la contribution au remboursement de la dette sociale mentionné aux articles 1600-0 G et 1600-0 H du code général des impôts, le prélèvement mentionné à l'article L. 245-14 du code de la sécurité sociale et la contribution additionnelle mentionnée à l'article L. 14-10-4 du code de l'action sociale et des familles étaient applicables aux plus-values imposables au titre de l'année 2008 ou exonérées au titre de cette même année sur le fondement de l'article 151 septies A du code général des impôts. Par suite, il résulte de ce qui a été dit au point 4 ci-dessus que la cour, qui n'était pas tenue de rechercher si la plus-value litigieuse avait été placée en report au cours d'une année au titre de laquelle les contributions en litige étaient déjà en vigueur, n'a pas méconnu le champ d'application de la loi fiscale.<br/>
<br/>
              6. En deuxième lieu, aux termes de l'article 13 du code général des impôts : " (...) 2. Le revenu global net annuel servant de base à l'impôt sur le revenu est déterminé en totalisant les bénéfices ou revenus nets visés aux I à VII bis de la 1re sous-section de la présente section, compte tenu, le cas échéant, du montant des déficits visés aux I et I bis de l'article 156 (...) / 3. Le bénéfice ou revenu net de chacune des catégories de revenus visées au 2 est déterminé distinctement suivant les règles propres à chacune d'elles. ". Aux termes de l'article 39 quindecies du code général des impôts, le montant net des plus-values à long terme " s'entend de l'excédent de ces plus-values sur les moins-values de même nature constatées au cours du même exercice. ". D'une part, ces dispositions ne prévoient pas que le montant imposable des plus-values à long terme réalisées dans le cadre d'une activité industrielle ou commerciale et relevant par suite de la catégorie des bénéfices industriels et commerciaux soit déterminé après compensation avec des moins-values constatées relevant d'une autre catégorie d'imposition, telle la catégorie des plus-values de cessions de valeurs mobilières, droits sociaux et titres assimilés. D'autre part, pour l'imposition à la contribution sociale généralisée, ces dispositions sont applicables aux plus-values professionnelles exonérées sur le fondement de l'article 151 septies A du code général des impôts qui auraient été imposées sur le fondement des dispositions de l'article 39 quindecies du même code en l'absence d'exonération. Par suite, la cour n'a pas commis d'erreur de droit en jugeant que les dispositions de l'article L. 136-6 du code de la sécurité sociale ne permettaient pas d'imputer une moins-value née de la cession de valeurs mobilières sur la plus-value nette à long terme professionnelle dont le report avait pris fin. <br/>
<br/>
              7. Il résulte de tout ce qui précède que les requérants ne sont pas fondés à demander l'annulation de l'arrêt qu'ils attaquent. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à la charge de l'Etat, lequel n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de M et Mme A...est rejeté.<br/>
Article 2 : La présente décision sera notifiée à M. et Mme B... A...et au ministre de l'action et des comptes publics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
