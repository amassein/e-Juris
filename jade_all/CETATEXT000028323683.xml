<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028323683</ID>
<ANCIEN_ID>JG_L_2013_12_000000357747</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/32/36/CETATEXT000028323683.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème et 1ère sous-sections réunies, 06/12/2013, 357747, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-12-06</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>357747</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème et 1ère sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Jean-Baptiste de Froment</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Suzanne von Coester</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:357747.20131206</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 20 mars 2012 au secrétariat du contentieux du Conseil d'Etat, présentée pour M. K...M..., demeurant..., M. O...V..., demeurant..., M. D...E..., demeurant..., M. Z... -C...G..., demeurant..., M. K...H..., demeurant..., M. T...R..., demeurant..., M. Z...-L...F..., demeurant..., M. C...Y..., demeurant..., M. W...N..., demeurant..., M. P...I..., demeurant  ..., M. A...J..., demeurant..., M. S...B..., demeurant..., M. L... X..., demeurant..., M. T...U..., demeurant..., M. T... Q..., demeurant ... et le syndicat des vérificateurs des monuments historiques, faisant élection de domicile chez M. T...Q..., 19, rue du Petit Musc, à Paris (75004) ; M. M...et autres demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir les articles 19 et 26 du décret du 16 février 2012 portant statut particulier du corps des techniciens des services culturels et des bâtiments de France, ainsi que les décisions individuelles prises en application de ce décret ;<br/>
<br/>
              2°) d'enjoindre à l'administration de reprendre le processus de régularisation qu'impose l'exécution de la décision du 18 février 2011 du Conseil d'Etat, statuant au contentieux ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat, pour chaque requérant, une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la loi n° 83-634 du 13 juillet 1983 ;<br/>
<br/>
              Vu la loi n° 84-16 du 11 janvier 1984 ;<br/>
<br/>
              Vu le décret du 22 mars 1908 relatif à l'organisation du service d'architecture des bâtiments civils et des palais nationaux ;<br/>
<br/>
              Vu le décret n° 82-452 du 28 mai 1982 ;<br/>
<br/>
              Vu le décret n° 85-986 du 16 septembre 1985 ;<br/>
              Vu la décision n° 330349-330350 du 18 février 2011 du Conseil d'Etat, statuant au contentieux ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Baptiste de Froment, Maître des Requêtes,  <br/>
<br/>
              - les conclusions de Mme Suzanne von Coester, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant que le désistement de M.G..., M. J...et M. X...est pur et simple ; que rien ne s'oppose à ce qu'il en soit donné acte ;<br/>
<br/>
              2. Considérant qu'il résulte de l'article 9 du décret du 22 mars 1908 relatif à l'organisation du service d'architecture des bâtiments civils et des palais nationaux, aujourd'hui abrogé, que les vérificateurs du service d'architecture des bâtiments civils et des palais nationaux, dits " vérificateurs des monuments historiques ", étaient recrutés par concours, sans condition de diplôme, qu'ils étaient chargés de collaborer, sous l'autorité des architectes en chef, à l'établissement des devis estimatifs et étaient rétribués par des honoraires ; que les intéressés pouvaient, en outre, cumuler cette mission publique avec une activité privée lucrative exercée à titre libéral ; que, par une décision du 18 février 2011, le Conseil d'Etat, statuant au contentieux a annulé le refus du ministre de la culture et de la communication de régulariser la situation des vérificateurs des monuments historiques et de retirer la décision de mettre fin à leur régime ; que cette décision relève notamment que, en dépit de la forme particulière de la rémunération des vérificateurs des monuments historiques, empruntée aux usages de la profession, et de la circonstance qu'ils avaient la faculté, en dehors de leurs fonctions publiques, d'avoir une clientèle privée, le décret du 22 mars 1908 leur a conféré la qualité de fonctionnaires ; qu'elle juge également que le pouvoir réglementaire était tenu de régulariser la situation des intéressés au regard de la loi du 11 janvier 1984 soit, s'il entendait maintenir ce corps de fonctionnaires, en édictant un statut particulier, soit, s'il entendait supprimer ce corps, en le fusionnant avec un autre corps de fonctionnaires ou en le mettant en extinction ; que, par le décret du 16 février 2012 portant statut particulier du corps des techniciens des services culturels et des Bâtiments de France, le Premier ministre a, pour l'exécution de la décision précitée, procédé à la fusion du corps des vérificateurs des monuments historiques avec un autre corps, en classant le corps issu de cette fusion en catégorie B  ; que la requête de M. M...et autres est dirigée contre les articles 19 et 26 de ce décret, en ce qu'ils prévoient l'intégration des intéressés dans le nouveau corps en fixant leurs modalités de reclassement et l'abrogation des dispositions du décret du 22 mars 1908 les concernant ;<br/>
<br/>
              3. Considérant que le décret attaqué ne comporte aucune disposition relative aux garanties fondamentales accordées aux fonctionnaires, dont la définition, en vertu de l'article 34 de la Constitution, relève de la loi ; que le moyen tiré de ce qu'il serait entaché d'incompétence pour ce motif  doit dès lors être écarté ; <br/>
<br/>
              4. Considérant qu'il ressort des pièces du dossier que l'intervention du décret attaqué a été précédée d'une consultation du comité technique paritaire du ministère de la culture, qui est commun à l'ensemble des fonctionnaires relevant de l'administration centrale de ce ministère, y compris  les membres du corps des vérificateurs des monuments historiques, et qui était compétent, en vertu de l'article 12 du décret du 28 mai 1982 relatif aux comités techniques paritaires, alors en vigueur, pour se prononcer sur un projet de statut particulier relatif à ces personnels ; que la composition de ce comité a été fixée en tenant compte des résultats des élections générales des représentants du personnel aux commissions administratives paritaires du ministère intervenues en 2010, soit avant que le Conseil d'Etat ne juge, par la décision précitée, que le décret du 22 mars 1908 a conféré aux vérificateurs la qualité de fonctionnaire ; que, compte-tenu notamment de l'effectif très réduit du corps des vérificateurs, qui n'excédait pas, à l'époque, une quinzaine de membres, le ministre n'était pas tenu,  dans la seule perspective de la régularisation de la situation des intéressés, de procéder à de nouvelles élections des représentants du personnel aux commissions administratives paritaires du ministère de la culture, auxquelles les vérificateurs auraient participé ; que, dans les circonstances de l'espèce, en consultant le comité technique paritaire du ministère de la culture dans sa composition résultant des élections de 2010,  le ministre n'a pas méconnu les exigences relatives à la participation des fonctionnaires aux décisions les concernant ; qu'aucune disposition législative ou réglementaire, notamment pas l'article 9 de la loi du 13 juillet 1983, ni aucun principe ne lui imposaient par ailleurs de consulter les délégués des vérificateurs ou le groupement professionnel qu'ils avaient constitué ; <br/>
<br/>
              5. Considérant qu'il appartenait au Premier ministre, comme le relèvent les motifs de la décision précitée du Conseil d'Etat, dès lors que, ainsi qu'il lui était loisible de le faire, il ne décidait ni de doter le corps des vérificateurs des monuments historiques d'un statut particulier ni de le mettre en extinction, de procéder à sa suppression en le fusionnant avec un autre corps,  sans être tenu d'édicter, préalablement à la fusion, un nouveau statut particulier du corps des vérificateurs ; que, par suite,  le moyen tiré de ce que le décret attaqué serait entaché d'erreur de droit ou d'erreur manifeste d'appréciation et aurait méconnu l'autorité de chose jugée qui s'attache à la décision du 18 février 2011 du Conseil d'Etat, faute qu'ait été d'abord édicté un nouveau statut particulier du corps des vérificateurs  ne peut qu'être écarté ;<br/>
<br/>
              6. Considérant qu'aux termes de l'article 29 de la loi du 11 janvier 1984 : " Les fonctionnaires appartiennent à des corps qui comprennent un ou plusieurs grades et sont classés, selon leur niveau de recrutement, en catégories. / Ces corps groupent les fonctionnaires soumis au même statut particulier et ayant vocation aux mêmes grades. / Ils sont répartis en quatre catégories désignées dans l'ordre hiérarchique décroissant par les lettres A, B, C et D. Les statuts particuliers fixent le classement de chaque corps dans l'une de ces catégories. " ; qu'ainsi qu'il a été dit ci-dessus, il résulte de l'article 9 du décret du 22 mars 1908 que les vérificateurs des monuments historiques avaient pour mission, sous l'autorité de l'architecte en chef, de vérifier les devis des travaux et des programmes et d'établir les marchés ; que ces missions sont de nature comparable à celles des techniciens des services culturels et des Bâtiments de France, corps de catégorie B, auquel les vérificateurs sont intégrés en vertu du décret attaqué ; que s'ils devaient justifier d'une expérience professionnelle de quatre ans dans le domaine de l'économie de la construction, les candidats au concours d'accès au corps des vérificateurs n'étaient soumis à aucune condition de diplôme ; que, par ailleurs, les requérants ne peuvent utilement se prévaloir des indications contenues dans des documents tels que leur carte professionnelle ou un guide du concours, pour soutenir qu'ils relevaient de la catégorie A ; que, dès lors, en ce qu'il prévoit que les intéressés seront intégrés dans un corps de techniciens relevant de la catégorie B, le décret attaqué n'est entaché d'aucune erreur manifeste d'appréciation ; <br/>
<br/>
              7. Considérant que, pour contester les dispositions du décret attaqué, M. M... et autres ne peuvent en tout état de cause utilement exciper de la méconnaissance ni des dispositions des articles 13 bis de la loi du 13 juillet 1983 portant droits et obligations des fonctionnaires, qui concernent le détachement et l'intégration directe des fonctionnaires dans un corps, ni de celles de 63 de la loi du 11 janvier 1984 portant dispositions statutaires relatives à la fonction publique de l'Etat, qui concernent le reclassement de fonctionnaires reconnus inaptes à l'exercice de leurs fonctions, ni de celles des articles 39-1 et 39-2 du décret du 16 septembre 1985 relatif au régime particulier de certaines positions des fonctionnaires de l'Etat, à la mise à disposition, à l'intégration et à la cessation définitive de fonctions, qui concernent l'intégration directe des fonctionnaires dans un corps ; que, par ailleurs, le moyen tiré de ce que les vérificateurs des monuments historiques devraient bénéficier d'un reclassement dans un grade dont l'indice sommital serait le plus proche de leur " niveau de rémunération " antérieur n'est, en tout état de cause, pas assorti des précisions permettant d'en apprécier le bien-fondé ; <br/>
<br/>
              8. Considérant qu'il résulte de tout ce qui précède que M. M...et autres ne sont pas fondés à demander l'annulation des articles 19 et 26 du décret du 16 février 2012 attaqué ; que leurs conclusions aux fins d'injonction ne peuvent, par suite, qu'être rejetées ; qu'il en va de même,  en tout état de cause, de leurs conclusions tendant à ce que des décisions individuelles soient annulées par voie de conséquence de l'annulation des articles 19 et 26 du décret ; qu'enfin, les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mise à la charge de l'Etat, qui n'est pas dans la présente instance la partie perdante, la somme que demandent M. M...et autres au titre des frais exposés par eux et leur syndicat professionnel et non compris dans les dépens ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article  1er : Il est donné acte du désistement de M. Z...-C...G..., M. A...J...et M. L... X.... <br/>
<br/>
      Article 2 : La requête de M. M...et autres est rejetée.<br/>
<br/>
Article 3 : La présente décision sera notifiée à M. K...M..., à M. O...V..., à M. D... E..., à M. K...H..., à M. T...R..., à M. Z...-L...F..., à M. C...Y..., à M. W...N..., à M. P...I..., à M. S...B..., à M. T...U..., à M. T...Q..., à M. Z...-C...G..., à M. A...J..., à M. L...X..., au syndicat des vérificateurs, à la ministre de la culture, et au Premier ministre. Copie en sera adressée pour information à la ministre de la réforme de l'Etat, de la décentralisation et de la fonction publique.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
