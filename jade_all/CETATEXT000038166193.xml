<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038166193</ID>
<ANCIEN_ID>JG_L_2019_02_000000423993</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/16/61/CETATEXT000038166193.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème chambre, 25/02/2019, 423993, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-02-25</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>423993</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP THOUIN-PALAT, BOUCARD</AVOCATS>
<RAPPORTEUR>M. Guillaume Leforestier</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Cécile Barrois de Sarigny</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2019:423993.20190225</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B...A...a demandé au tribunal administratif d'Amiens d'annuler la décision du 1er septembre 2017 du ministre de l'intérieur constatant la perte de validité de son permis de conduire pour solde de points nul ainsi que les décisions de retrait de points qui y étaient récapitulées et d'enjoindre au ministre de lui restituer les points illégalement retirés. Par un jugement n° 1800014 du 9 juillet 2018, le tribunal administratif a rejeté sa demande.    <br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés au secrétariat du contentieux du Conseil d'Etat les 10 septembre et 10 décembre 2018 et 22 janvier 2019, M. A...demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler ce jugement ;   <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 4 500 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de procédure pénale ; <br/>
              - le code de la route ;<br/>
              - l'arrêté du 29 juin 1992 fixant les supports techniques de la communication par le ministère public au ministre de l'intérieur des informations prévues à l'article L. 30 (4°, 5°, 6° et 7°) du code de la route ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Guillaume Leforestier, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Cécile Barrois de Sarigny, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Thouin-Palat, Boucard, avocat de M.A....<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis au juge du fond que M. A...a demandé au tribunal administratif d'Amiens d'annuler la décision du 1er septembre 2017 par laquelle le ministre de l'intérieur a constaté la perte de validité de son permis de conduire pour solde de points nul, ainsi que les retraits de points qui y étaient récapitulés, et d'enjoindre au ministre de rétablir son capital de points. M. A...se pourvoit en cassation contre le jugement du 9 juillet 2018 qui a rejeté sa demande. <br/>
<br/>
              2. Aux termes de l'article L. 223-1 du code de la route : " Le permis de conduire est affecté d'un nombre de points. Celui-ci est réduit de  plein droit si le titulaire du permis a commis une infraction pour laquelle cette réduction est prévue. / (...) La réalité d'une infraction entraînant retrait de points est établie par le paiement d'une amende forfaitaire ou l'émission du titre exécutoire de l'amende forfaitaire majorée, l'exécution d'une composition pénale ou par une condamnation définitive (...) ". La délivrance, au titulaire du permis de conduire à l'encontre duquel est relevée une infraction donnant lieu à retrait de points, de l'information prévue aux articles L. 223-3 et R. 223-3 du code de la route constitue une garantie essentielle donnée à l'auteur de l'infraction pour lui permettre, avant d'en reconnaître la réalité par le paiement d'une amende forfaitaire ou l'exécution d'une composition pénale, d'en mesurer les conséquences sur la validité de son permis et éventuellement d'en contester la réalité devant le juge pénal. Elle revêt le caractère d'une formalité substantielle et conditionne la régularité de la procédure au terme de laquelle le retrait de points est décidé. <br/>
<br/>
              Sur les retraits de points consécutifs aux infractions constatées les 28 mars 2008, 24 mars 2009, 21 septembre 2015, 26 avril 2016 et 4 juin 2017 : <br/>
<br/>
              3. Il résulte des articles L. 225-1 du code de la route, 529, 529-2 et 530 du code de procédure pénale et de l'arrêté du 29 juin 1992 visé ci-dessus que le mode d'enregistrement et de contrôle des informations relatives aux infractions au code de la route conduit à considérer que la mention au relevé d'information intégral relatif à un permis de conduire du paiement de l'amende forfaitaire établit, sauf preuve contraire, la réalité de ce paiement. Dès lors, en retenant au vu de cette mention que M. A...avait acquitté les amendes forfaitaires afférentes aux infractions relevées les 28 mars 2008, 24 mars 2009, 21 septembre 2015, 26 avril 2016 et 4 juin 2017, pour en déduire qu'il avait nécessairement été mis en possession d'un document, nécessaire à ce paiement, comportant les informations requises par les articles L. 223-3 et R. 223-3 du code de la route, le tribunal administratif, dont le jugement est suffisamment motivé sur ce point, n'a pas commis d'erreur de droit et n'a pas dénaturé les pièces du dossier qui lui était soumis. <br/>
<br/>
              Sur le retrait de points consécutif à l'infraction constatée le 18 décembre 2008 : <br/>
<br/>
              4. Il ressort des pièces du dossier soumis au juge du fond que le relevé d'information intégral mentionne que l'infraction constatée le 18 décembre 2008, avec interception du véhicule, a donné lieu à la même date au paiement d'une amende forfaitaire. Toutefois, lorsque le véhicule a été intercepté et que le paiement de l'amende forfaitaire a eu lieu le jour de l'infraction, ce paiement, qui a pu être effectué entre les mains de l'agent verbalisateur, n'établit pas que le conducteur se soit vu remettre un document comportant les informations requises par les articles L. 223-3 et R. 223-3 du code de la route. Par ailleurs, dès lors que l'intéressé n'avait pas apposé sa signature sur le procès-verbal, qui ne comportait pas non plus la mention " refuse de signer ", cette pièce n'était pas de nature à attester qu'il s'était vu remettre un document contenant ces informations. Par suite, en se fondant sur le procès-verbal pour regarder comme établi que l'administration s'était acquittée de son obligation d'information, le tribunal a commis une erreur de droit. <br/>
<br/>
              5. Il résulte de ce qui précède que M. A...n'est fondé à demander l'annulation du jugement attaqué du 9 juillet 2018 qu'en tant qu'il rejette ses conclusions tendant à l'annulation du retrait de points consécutif à l'infraction du 18 décembre 2008 et, par voie de conséquence, de la décision du 1er septembre 2017 constatant la perte de validité de son permis de conduire pour solde de points nul ainsi que ses conclusions tendant au prononcé d'une injonction et au remboursement des frais exposés.<br/>
<br/>
              6. Il y a lieu dans les circonstances de l'espèce, de mettre à la charge de l'Etat le versement à M. A...de la somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du tribunal administratif d'Amiens du 9 juillet 2018 est annulé en tant qu'il rejette les conclusions de M. A...tendant à l'annulation du retrait de points consécutif à l'infraction du 18 décembre 2008 et, par voie de conséquence, de la décision du 1er septembre 2017 constatant la perte de validité de son permis de conduire pour solde de points nul, ainsi que ses conclusions tendant au prononcé d'une injonction et au remboursement des frais. <br/>
Article 2 : L'affaire est renvoyée au tribunal administratif d'Amiens dans la limite de la cassation prononcée.<br/>
Article 3 : L'Etat versera à M. A...la somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Le surplus des conclusions du pourvoi est rejeté.<br/>
Article 5 : La présente décision sera notifiée à M. B...A...et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
