<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000020936107</ID>
<ANCIEN_ID>JG_L_2009_07_000000295805</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/20/93/61/CETATEXT000020936107.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème et 8ème sous-sections réunies, 27/07/2009, 295805, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2009-07-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>295805</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème et 8ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Martin</PRESIDENT>
<AVOCATS>SCP CELICE, BLANCPAIN, SOLTNER</AVOCATS>
<RAPPORTEUR>Mme Anne  Egerszegi</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Glaser Emmanuel</COMMISSAIRE_GVT>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 24 juillet et 16 novembre 2006 au secrétariat du contentieux du Conseil d'Etat, présentés pour la SOCIETE CONFORAMA HOLDING, dont le siège est 80 boulevard du Mandinet (Lognes) à Marne La Vallée Cedex 2 (77432) ; la SOCIETE CONFORAMA HOLDING demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt du 22 mai 2006 par lequel la cour administrative d'appel de Paris, confirmant le jugement du tribunal administratif de Melun en date du 30 mai 2002, a rejeté sa requête tendant à la décharge du complément d'impôt sur les sociétés et des pénalités correspondantes auxquels elle a été assujettie au titre des exercices clos les 31 décembre 1990 et 1991 ; <br/>
<br/>
              2°) réglant l'affaire au fond, de prononcer la décharge des impositions litigieuses ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code général des impôts et le livre des procédures fiscales; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Anne Egerszegi, Maître des Requêtes,<br/>
<br/>
              - les observations de la SCP Célice, Blancpain, Soltner, avocat de la SOCIETE CONFORAMA HOLDING,<br/>
<br/>
              - les conclusions de M. Emmanuel Glaser, Rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Célice, Blancpain, Soltner, avocat de la SOCIETE CONFORAMA HOLDING ; <br/>
<br/>
<br/>
<br/>
<br/>Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que la société requérante a acquis, en mars 1989, 6,66% du capital de la holding de droit luxembourgeois Europarticipations pour un montant de 155 millions de francs ; qu'en 1990 et 1991, elle a perçu des dividendes de cette holding qu'elle a retranchés de son bénéfice net imposable, déduction faite d'une quote-part de frais et charges, en se prévalant du régime de faveur prévu aux articles 145 et 216 du code général des impôts en faveur des sociétés mères et filiales ; qu'à l'issue de deux vérifications de comptabilité portant sur l'exercice clos en 1990 et sur la période du 1er janvier 1991 au 31 décembre 1992, l'administration fiscale a réintégré, sur le fondement de l'article L. 64 du livre des procédures fiscales, dans la base imposable à l'impôt sur les sociétés des exercices vérifiés, le montant des dividendes en cause, au motif que la société avait participé à un montage délibéré ayant pour seul but la défiscalisation de ces dividendes ; que la société CONFORAMA HOLDING se pourvoit en cassation contre l'arrêt du 22 mai 2006 par lequel la cour administrative d'appel de Paris, confirmant le jugement du tribunal administratif de Melun en date du 30 mai 2002, a rejeté sa requête tendant à la décharge des suppléments d'impôt  sur les sociétés résultant de ce redressement ainsi que de la remise en cause, d'une part, du régime des plus-values à long terme sous lequel la société requérante avait placé l'imposition du produit de la cession, en 1991, de sa participation dans la société Europarticipations et, d'autre part, d'un crédit d'impôt-recherche en 1990, qui lui ont été assignés au titre des exercices clos en 1990 et 1991 ; <br/>
<br/>
              Sur la régularité de la procédure d'imposition :<br/>
<br/>
              Considérant, en premier lieu, que l'administration n'est tenue de soumettre à un débat oral et contradictoire avec le contribuable les documents qu'elle a pu obtenir dans le cadre de son droit de communication que s'ils présentent le caractère de pièces comptables de l'entreprise vérifiée et s'ils ont été effectivement utilisés pour procéder aux redressements contestés ; qu'ainsi, en jugeant que l'administration fiscale n'était pas tenue de soumettre à un débat oral et contradictoire les éléments recueillis auprès des co-associés de la holding luxembourgeoise Europarticipations dont la société requérante détenait une partie du capital, au motif qu'il ne s'agissait pas d'éléments de la comptabilité de la société vérifiée, la cour n'a pas commis d'erreur de droit ; que par ailleurs, en estimant  surabondantes  les informations sur l'absence de la société requérante aux assemblées statutaires, obtenues par l'administration dans l'exercice de son droit de communication et dont elle a fait connaître l'origine et la teneur à la SOCIETE CONFORAMA HOLDING, la cour n'a pas entaché sa décision d'une contradiction de motifs ; <br/>
<br/>
              Considérant, en second lieu, que la cour, qui a estimé que la réponse aux observations de la contribuable du 21 janvier 1994 n'était, sur le chef de redressement relatif au crédit d'impôt-recherche, ni imprécise ni peu détaillée, n'a pas dénaturé ce document dès lors qu'il ressort des pièces du dossier qu'après avoir résumé les observations de la société, le vérificateur y a répondu par une réponse motivée, bien que concise, permettant à la contribuable de contester utilement la position de l'administration fiscale, par ailleurs précisément détaillée dans la notification de redressements du 21 décembre 1993 ; <br/>
<br/>
              Sur le bien-fondé des redressements : <br/>
<br/>
              En ce qui concerne l'abus de droit :<br/>
<br/>
              Considérant qu'aux termes de l'article L. 64 du livre des procédures fiscales, dans sa rédaction applicable aux années d'imposition en litige :  Ne peuvent être opposés à l'administration des impôts les actes qui dissimulent la portée véritable d'un contrat ou d'une convention à l'aide de clauses : (...) / (...) b. qui déguisent soit une réalisation, soit un transfert de bénéfices ou de revenus (...) / L'administration est en droit de restituer son véritable caractère à l'opération litigieuse. En cas de désaccord sur les redressements notifiés sur le fondement du présent article, le litige est soumis, à la demande du contribuable, à l'avis du comité consultatif pour la répression des abus de droit. L'administration peut également soumettre le litige à l'avis du comité dont les avis rendus font l'objet d'un rapport annuel. / Si l'administration ne s'est pas conformée à l'avis du comité, elle doit apporter la preuve du bien-fondé du redressement  ; qu'il résulte de ces dispositions que, lorsque l'administration use de la faculté qu'elles lui confèrent dans des conditions telles que la charge de la preuve lui incombe, elle est fondée à écarter comme ne lui étant pas opposables certains actes passés par le contribuable, dès lors qu'elle établit que ces actes ont un caractère fictif, ou, que, recherchant le bénéfice d'une application littérale des textes à l'encontre des objectifs poursuivis par leurs auteurs, ils n'ont pu être inspirés par aucun autre motif que celui d'éluder ou d'atténuer les charges fiscales que l'intéressé, s'il n'avait pas passé ces actes, auraient normalement supportées, eu égard à sa situation ou à ses activités réelles ; <br/>
<br/>
              Considérant qu'en estimant insuffisante, par une appréciation souveraine, la valeur probante des éléments apportés par la société pour contredire l'argumentation de l'administration fiscale selon laquelle le montage auquel avait participé la société requérante avait pour but exclusif d'éluder l'impôt, la cour, qui a relevé que le ministre soutenait, sans être sérieusement contredit, que la société Europarticipations était  restée au cours de sa période d'existence sous l'entière dépendance de la Banque Internationale du Luxembourg, à l'origine de sa création, en ce qui concerne tant sa gestion que ses investissements, qu'elle ne constituait qu'une structure dépourvue de substance dès lors que son conseil d'administration n'était composé que de membres dirigeants de cette banque et que les actionnaires n'exerçaient aucune influence sur la gestion des actifs, que la recherche d'un levier fiscal maximal était révélé d'une part par l'absence quasi-totale de toute imposition au Luxembourg des bénéfices des holdings relevant de la loi du 31 juillet 1929 et, d'autre part, par un taux de participation au capital permettant de bénéficier du régime d'exonération des dividendes prévu en faveur des sociétés mères aux articles 145 et 216 du code général des impôts, tout en évitant l'imposition prévue à l'article 209 B de ce code sur les bénéfices des filiales détenues dans la proportion d'au moins 25 % et établies dans un Etat à fiscalité privilégiée, et, enfin, que la contribuable n'avait pu valablement justifier d'aucun intérêt autre que l'avantage fiscal qu'elle retirait de cette opération, a pu, sans commettre d'erreur de droit ni d'erreur de qualification juridique, par une décision suffisamment motivée, juger que l'administration avait apporté la preuve qui lui incombait de ce que l'opération litigieuse était constitutive d'un abus de droit ; <br/>
<br/>
              En ce qui concerne la cession des titres Europarticipations :<br/>
<br/>
              Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, en 1991, la société requérante a cédé la participation qu'elle avait acquise en mars 1989 dans le capital de la société Europarticipations et déclaré une plus-value de 1,203 MF au titre des plus-value à long terme ; que l'administration fiscale a imposé la plus value en cause comme plus-value à court terme au motif que la date de cession des titres n'était pas établie ; <br/>
<br/>
              Considérant, en premier lieu, que le moyen tiré de ce que la cour aurait omis de répondre au moyen portant sur la charge de la preuve manque en fait ; <br/>
<br/>
              Considérant, en deuxième lieu, qu'aux termes de l'article 219 du code général des impôts relatif au calcul de l'impôt sur les sociétés, dans sa rédaction applicable en 1991:  I. (...) Le taux normal de l'impôt est fixé à 34 %. / Toutefois : / a. Le montant net des plus-values à long terme (...) fait l'objet d'une imposition séparée au taux de 15 % (...) ; qu'aux termes de l'article 39 duodecies du même code, applicable en matière de détermination des bénéfices passibles de l'impôt sur les sociétés en vertu de l'article 209 :  1. Par dérogation aux dispositions de l'article 38, les plus-values provenant de la cession d'éléments de l'actif immobilisé sont soumises à des régimes distincts suivant qu'elles sont réalisées à court ou à long terme. / 2. Le régime des plus-values à court terme est applicable : / a. Aux plus-values provenant de la cession d'éléments acquis ou créés depuis moins de deux ans (...) / 3. Le régime des plus-values à long terme est applicable aux plus-values autres que celles définies au 2 (...)  ;<br/>
<br/>
              Considérant que la date à laquelle la cession de titres nominatifs d'une société, à l'origine d'une plus-value imposable, doit être regardée comme réalisée est, indépendamment de ses modalités de paiement, celle à laquelle s'opère entre les parties le transfert de propriété ; que ce transfert de propriété a lieu, sauf stipulations contractuelles contraires, à la date où un accord intervient sur la chose et le prix, même si ce transfert n'est opposable aux tiers qu'à compter de sa date d'inscription au registre de la société émettrice ou du jour auquel ils ont été informés de la cession, s'il est antérieur à cette date ;<br/>
<br/>
              Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que le courrier du 29 novembre 1991, produit pour la première fois devant la cour, par lequel la société requérante confirmait sa demande de retrait du capital de la holding Europarticipations le 16 décembre 1991, ne saurait être regardé comme établissant la date du transfert de propriété des titres ni, par suite, la durée de détention de ceux-ci ; qu'ainsi, en jugeant que la société n'avait produit aucun élément nouveau de nature à conférer une date certaine à la cession des titres litigieuse et en en déduisant que la plus-value réalisée devait être imposée selon le régime des plus-values à court terme, la cour, qui n'a pas dénaturé les pièces du dossier, a, par une décision suffisamment motivée, souverainement apprécié les faits et exactement qualifié la nature de la plus-value en cause ; <br/>
<br/>
              Considérant, en troisième lieu, que la société requérante soutient qu'à supposer qu'elle ait conservé ses titres jusqu'à la date de liquidation, le 23 décembre 1991, elle n'a pas réalisé une plus-value mais perçu sa part du boni de liquidation, exonéré d'imposition par application du régime des sociétés mères ; que ce moyen est sans incidence sur le bien-fondé de l'arrêt attaqué ; <br/>
<br/>
              En ce qui concerne le crédit d'impôt-recherche :<br/>
<br/>
              Considérant qu'aux termes du premier alinéa du I de l'article 244 quater B du code général des impôts, dans sa rédaction applicable à l'année 1990 :  Les entreprises industrielles et commerciales imposées d'après leur bénéfice réel peuvent bénéficier d'un crédit d'impôt égal à 25 % de l'excédent des dépenses de recherche exposées au cours d'une année par rapport à la moyenne des dépenses de même nature, revalorisées de la hausse des prix à la consommation, exposées au cours des deux années précédentes ; qu'aux termes de l'article 49 septies F de l'annexe III à ce code :  Pour l'application des dispositions de l'article 244 quater B du code général des impôts, sont considérées comme opérations de recherche scientifique ou technique : (...) / c. Les activités ayant le caractère d'opérations de développement expérimental effectuées, au moyen de prototypes ou d'installations pilotes, dans le but de réunir toutes les informations nécessaires pour fournir les éléments techniques des décisions, en vue de la production de nouveaux matériaux, dispositifs, produits, procédés, systèmes, services ou en vue de leur amélioration substantielle. Par amélioration substantielle, on entend les modifications qui ne découlent pas d'une simple utilisation de l'état des techniques existantes et qui présentent un caractère de nouveauté  ;<br/>
<br/>
              Considérant que la cour n'a pas commis d'erreur de droit en jugeant, par une décision suffisamment motivée, qu'il résultait de ces dispositions que ne pouvaient être prises en compte pour le bénéfice du crédit d'impôt-recherche que les dépenses exposées pour le développement de logiciels dont la conception ne pouvait être envisagée, eu égard à l'état des connaissances techniques à l'époque considérée, par un professionnel averti, par simple développement ou adaptation desdites techniques ; <br/>
<br/>
              Considérant qu'il résulte de tout ce qui précède que la SOCIETE CONFORAMA HOLDING n'est pas fondée à demander l'annulation de l'arrêt qu'elle attaque ; qu'il y a lieu par suite, en application des dispositions de l'article L. 761-1 du code de justice administrative, de rejeter la demande qu'elle présente au titre des frais exposés par elle et non compris dans les dépens ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de la SOCIETE CONFORAMA HOLDING est rejeté.<br/>
Article 2 : La présente décision sera notifiée à la SOCIETE CONFORAMA HOLDING et au ministre du budget, des comptes publics, de la fonction publique et de la réforme de l'Etat.<br/>
<br/>
</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
