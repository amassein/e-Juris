<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034808209</ID>
<ANCIEN_ID>JG_L_2017_05_000000389785</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/80/82/CETATEXT000034808209.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème - 4ème chambres réunies, 24/05/2017, 389785, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-05-24</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>389785</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème - 4ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR>M. Jean-Dominique Langlais</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:389785.20170524</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme A...B...a demandé au tribunal administratif de Paris d'annuler l'arrêté du 26 juillet 2011 par lequel le ministre de l'intérieur l'a suspendue de ses fonctions d'adjointe de sécurité pour une durée de dix-huit mois, dont douze avec sursis, et de condamner l'Etat à lui verser une somme de 30 000 euros en réparation des préjudices ayant résulté de cette sanction. Par un jugement n° 1113351/5-1 du 6 décembre 2012, le tribunal administratif a rejeté sa demande. <br/>
<br/>
              Par un arrêt n° 13PA00914 du 31 décembre 2014, la cour administrative d'appel de Paris a rejeté l'appel formé par Mme B...contre ce jugement.  <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 27 avril et 27 juillet 2015 au secrétariat du contentieux du Conseil d'Etat, Mme B...demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ; <br/>
<br/>
              - la loi n° 83-634 du 13 juillet 1983 ;<br/>
<br/>
              - la loi n° 84-16 du 11 janvier 1984 ;<br/>
<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Dominique Langlais, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Piwnica, Molinié, avocat de MmeB... ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que Mme A...B..., alors adjointe de sécurité à la police de l'air et des frontières, a co-signé, sous le titre " Omerta dans la police - abus de pouvoir, homophobie, racisme, sexisme ", un ouvrage publié en 2010 et imputant à cette institution un certain nombre d'abus ; que, par un arrêté du 26 juillet 2011, le ministre de l'intérieur s'est fondé sur ce comportement, qu'il a qualifié de manquement à l'obligation de réserve, pour suspendre l'intéressée de ses fonctions pour une durée de dix-huit mois, dont douze assortis du sursis ; que le tribunal administratif de Paris a rejeté, par un jugement du 6 décembre 2012, le recours pour excès de pouvoir formé par Mme B... contre cette décision ainsi que sa demande d'indemnité ; que Mme B...se pourvoit en cassation contre l'arrêt du 31 décembre 2014 par lequel la cour administrative d'appel de Paris a rejeté son appel dirigé contre ce jugement ; <br/>
<br/>
              2. Considérant que, pour rejeter l'appel de MmeB..., la cour administrative d'appel de Paris a jugé que celle-ci avait délibérément manqué à son obligation de réserve en co-signant et en promouvant dans les médias un ouvrage portant de graves accusations contre le service de police au sein duquel elle était affectée et contre la politique gouvernementale en matière de police, que ces accusations, souvent formulées de manière outrancière, étaient de nature à jeter le discrédit sur l'institution policière dans son ensemble, que Mme B...n'avait saisi le procureur de la République, sur le fondement de l'article 40 du code de procédure pénale, que d'une partie seulement des faits qu'elle prétendait dénoncer dans cet ouvrage, qu'elle avait publié cet ouvrage avant que la Haute autorité de lutte contre les discriminations et pour l'égalité n'émette un avis sur ceux de ces faits dont elle l'avait saisie et qu'enfin, elle avait agi dans une intention délibérément polémique ; qu'elle a déduit de l'ensemble de ces circonstances que Mme B... ne pouvait se prévaloir d'aucun " devoir d'alerte " pour justifier la publication de cet ouvrage ; qu'en se prononçant par ces motifs, la cour, qui a suffisamment motivé son arrêt et n'a pas omis de répondre au moyen tiré de ce que l'intérêt général aurait exigé la divulgation des informations contenues dans l'ouvrage en cause, n'a pas commis l'erreur de droit alléguée dans l'application de l'article 10 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
<br/>
              3. Considérant qu'il résulte de ce qui précède que le pourvoi de Mme B...doit être rejeté ;<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
		Article 1er : Le pourvoi de Mme B...est rejeté.<br/>
<br/>
Article 2 : La présente décision sera notifiée à Mme A...B...et au ministre d'Etat, ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
