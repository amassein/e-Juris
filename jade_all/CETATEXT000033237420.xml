<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033237420</ID>
<ANCIEN_ID>JG_L_2016_10_000000398544</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/23/74/CETATEXT000033237420.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème - 7ème chambres réunies, 12/10/2016, 398544</TITRE>
<DATE_DEC>2016-10-12</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>398544</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème - 7ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Sophie-Caroline de Margerie</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Béatrice Bourgeois-Machureau</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2016:398544.20161012</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A...B...a saisi le tribunal administratif de Paris d'une demande contestant les conditions de son reclassement dans le corps des conseillers des affaires étrangères. Par un jugement n° 1309758/5-2 du 2 octobre 2014, le tribunal  a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 14PA04886 du 30 mars 2016, la cour administrative d'appel de Paris a, d'une part, annulé le jugement du tribunal administratif de Paris du 2 octobre 2014 et, d'autre part, transmis au Conseil d'Etat, en application de l'article R. 311-1 du code de justice administrative, la requête, enregistrée le 8 juillet 2013 au greffe du tribunal administratif de Paris. Par cette requête, un mémoire en réplique et des nouveaux mémoires enregistrés les 11 mars, 3 juillet et 24 septembre 2014 au greffe du tribunal administratif de Paris, et les 2 décembre 2014, 15 janvier 2015 et 11 août 2015 au greffe de la cour administrative d'appel de Paris, M. A...B...demande :<br/>
<br/>
              1°) l'annulation  pour excès de pouvoir de l'arrêté du ministre des affaires étrangères du 23 janvier 2013 en tant qu'il fixe son classement au troisième échelon du grade de conseiller des affaires étrangères, ensemble la décision implicite rejetant son recours gracieux du 11 mars 2013 ;<br/>
<br/>
              2°) d'enjoindre à l'administration de réexaminer sa situation dans un délai d'un mois à compter de la notification du jugement à intervenir ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la loi n° 83-634 du 13 juillet 1983  ;<br/>
              - la loi n° 84-16 du 11 janvier 1984 ; <br/>
              - la loi n° 84-53 du 26 janvier 1984 ;<br/>
              - le décret n° 69-222 du 6 mars 1969 ;<br/>
              - le décret n° 87-1004 du 16 décembre 1987 ;<br/>
              - le code de justice administrative ; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Sophie-Caroline de Margerie, conseiller d'Etat, <br/>
<br/>
              - les conclusions de Mme Béatrice Bourgeois-Machureau, rapporteur public.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1.	Considérant qu'aux termes de l'article 10 du décret du 6 mars 1969 relatif au statut particulier des agents diplomatiques et consulaires : " Les conseillers des affaires étrangères du cadre général sont recrutés parmi les anciens élèves de l'Ecole nationale d'administration ; ils sont nommés et titularisés en cette qualité à compter du lendemain du dernier jour de leur scolarité à l'école. Pour tenir compte de leur scolarité à l'Ecole nationale d'administration, quelle qu'en soit la durée, ils sont nommés directement au 3e échelon. Toutefois, si l'indice qu'ils détiennent dans leur corps, cadre d'emplois ou emploi d'origine est supérieur à celui correspondant au 3e échelon, les conseillers des affaires étrangères du cadre général recrutés par la voie des concours interne et externe de cette école sont placés à l'échelon du grade de conseiller des affaires étrangères comportant un traitement égal ou, à défaut, immédiatement supérieur à celui dont ils bénéficiaient antérieurement dans leur corps, cadre d'emplois ou emploi d'origine pour les fonctionnaires ou dans leur emploi pour les agents non titulaires (...) " ; <br/>
<br/>
              2.	Considérant qu'aux termes de l'article 110 de la loi du 26 janvier 1984 portant dispositions statutaires relatives à la fonction publique territoriale : " l'autorité territoriale peut, pour former son cabinet librement, recruter un ou plusieurs collaborateurs et mettre librement fin à leur fonction. La nomination de non-fonctionnaires à ces emplois ne leur donne aucun droit à être titularisés dans un grade de la fonction publique territoriale. (...)/ Ces collaborateurs ne rendent compte qu'à l'autorité territoriale auprès de laquelle ils sont placés et qui décide des conditions et des modalités d'exécution du service qu'ils accomplissent auprès d'elle (...) " ; qu'aux termes de l'article 7 du décret du 16 décembre 1987 relatif aux collaborateurs de cabinet des autorités territoriales : " La rémunération individuelle de chaque collaborateur de cabinet est fixée par l'autorité territoriale. Elle comprend un traitement indiciaire, l'indemnité de résidence et le supplément familial de traitement y afférents ainsi que, le cas échéant, des indemnités. Le traitement indiciaire ne peut en aucun cas être supérieur à 90 % du traitement correspondant soit à l'indice terminal de l'emploi administratif fonctionnel de direction le plus élevé de la collectivité ou de l'établissement occupé par un fonctionnaire, soit à l'indice terminal du grade administratif le plus élevé détenu par un fonctionnaire en activité dans la collectivité ou l'établissement. " ; <br/>
<br/>
              3.	Considérant qu'il ressort des pièces du dossier qu'à sa sortie de l'Ecole nationale d'administration, qu'il avait intégrée en 2011 par la voie du concours interne, M. B... a été nommé et titularisé dans le corps de conseiller des affaires étrangères par un décret du président de la République du 31 janvier 2013 ; que, par arrêté du ministre des affaires étrangères du 23 janvier 2013, il a été classé au troisième échelon du grade de conseiller des affaires étrangères sans ancienneté conservée, avec un indice brut 528 ; que M. B...demande l'annulation de cet arrêté dès lors qu'il n'a pas été tenu compte des fonctions de collaborateur de cabinet qu'il a exercées auprès du président du conseil général de l'Essonne du 16 juin 2008 au 31 décembre 2010 et pour lesquelles il percevait un traitement indiciaire correspondant à l'indice majoré 1004 ;<br/>
<br/>
              4.	Considérant que les dispositions précitées de l'article 10 du décret du 6 mars 1969 permettent, en ce qui concerne les agents non titulaires, nommés et titularisés en qualité de conseiller des affaires étrangères du cadre général à l'issue de leur scolarité à l'Ecole nationale d'administration, leur reclassement à l'échelon du grade de conseiller des affaires étrangères comportant un traitement égal ou, à défaut, immédiatement supérieur à celui dont ils bénéficiaient dans l'emploi qu'ils occupaient antérieurement à leur scolarité ; qu'il ne ressort ni de ces dispositions ni d'aucune autre que les agents non titulaires ayant occupé des fonctions de collaborateur de cabinet au sens de l'article 110 précité de la loi du 26 janvier 1984 seraient exclus de leur bénéfice ; qu'ainsi, en refusant de tenir compte, pour son reclassement dans le grade de conseiller des affaires étrangères, des services effectués par M. B...dans l'emploi de collaborateur de cabinet qu'il a occupé du 16 juin 2008 au 31 décembre 2010 auprès du président du conseil général de l'Essonne, avant son entrée à l'Ecole nationale d'administration, le ministre des affaires étrangères a méconnu les dispositions de l'article 10 du décret du 6 mars 1969 et commis une erreur de droit ; que, par suite, M. B...est fondé à demander l'annulation de l'arrêté du 23 janvier 2013 fixant son reclassement au troisième échelon du grade de conseiller des affaires étrangères ;<br/>
<br/>
              5.	Considérant qu'il y a lieu, statuant sur les conclusions à fin d'injonction présentées par M.B..., d'enjoindre au ministre des affaires étrangères de procéder à un réexamen de la situation de l'intéressé, dans un délai de deux mois à compter de la notification de la présente décision ;<br/>
<br/>
              6.	Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 2 000 euros à verser à M.B..., au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : L'arrêté du 23 janvier 2013 du ministre des affaires étrangères fixant le reclassement de M. B...au troisième échelon du grade de conseiller des affaires étrangères est annulé.<br/>
<br/>
Article 2 : Il est enjoint au ministre des affaires étrangères de procéder à un nouvel examen de la situation de M. B...dans un délai de deux mois à compter de la notification de la présente décision.<br/>
<br/>
Article 3 : L'Etat versera à M. B...une somme de 2 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : Le présent jugement sera notifié à M. A...B...et au ministre des affaires étrangères. <br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
