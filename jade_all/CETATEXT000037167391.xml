<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037167391</ID>
<ANCIEN_ID>JG_L_2018_07_000000404265</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/16/73/CETATEXT000037167391.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème et 5ème chambres réunies, 09/07/2018, 404265, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-07-09</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>404265</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème et 5ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MONOD, COLIN, STOCLET</AVOCATS>
<RAPPORTEUR>Mme Laure Durand-Viel</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Louis Dutheillet de Lamothe</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:404265.20180709</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A...B...a demandé au tribunal administratif de Paris l'annulation pour excès de pouvoir des décisions du ministre de l'intérieur des 1er octobre et 20 novembre 2013 rejetant sa demande tendant à l'autoriser à se maintenir provisoirement sur le territoire français en l'assignant à résidence en application de l'article L. 561-1 du code de l'entrée et du séjour des étrangers et du droit d'asile. Par un jugement n° 1402798/7-3 du 26 mars 2015, le tribunal a rejeté sa demande. <br/>
<br/>
              Par un arrêt n° 15PA02104 du 14 juin 2016, la cour administrative d'appel de Paris a rejeté l'appel formé  par M. B...contre ce jugement.  <br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et deux mémoires en réplique, enregistrés le 10 octobre 2016 et les 10 janvier, 16 novembre et 18 décembre 2017 au secrétariat du contentieux du Conseil d'Etat, M. B...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ; <br/>
              - le code pénal ;<br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile, notamment son article L. 561-1 ;<br/>
              - la loi n° 2011-672 du 16 juin 2011 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Laure Durand-Viel, auditeur,  <br/>
<br/>
              - les conclusions de M. Louis Dutheillet de Lamothe, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Monod, Colin, Stoclet, avocat de M.B....<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant que M.B..., de nationalité tunisienne, a été condamné à une peine d'interdiction définitive du territoire français par un jugement du 17 décembre 1990 du tribunal correctionnel de Paris, confirmé par un arrêt du 20 juin 1991 de la cour d'appel de Paris ; que, par un arrêt du 11 octobre 1993, la cour d'appel de Paris a rejeté sa demande tendant au relèvement de cette peine ; que le 29 juillet 2013, l'intéressé a demandé au ministre de l'intérieur le bénéfice des dispositions de l'article L. 541-2 du code de l'entrée et du séjour des étrangers et du droit d'asile permettant son assignation à résidence en raison de l'impossibilité de quitter le territoire français ; que, par une décision du 1er octobre 2013, le ministre a rejeté cette demande puis, par décision du 20 décembre 2013, le recours gracieux formé par l'intéressé ; que le tribunal administratif de Paris a rejeté le recours introduit par M. B...tendant à l'annulation de ces deux décisions ; que la cour administrative d'appel de Paris a rejeté l'appel formé par M. B... contre ce jugement, par un arrêt du 14 juin 2016 contre lequel il se pourvoit régulièrement en cassation ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article L. 561-1 du code de l'entrée et du séjour des étrangers et du droit d'asile, dans sa rédaction applicable au litige, issue de la loi du 16 juin 2011 relative à l'immigration, à l'intégration et à la nationalité : " Lorsque l'étranger justifie être dans l'impossibilité de quitter le territoire français ou ne peut ni regagner son pays d'origine ni se rendre dans aucun autre pays, l'autorité administrative peut, jusqu'à ce qu'existe une perspective raisonnable d'exécution de son obligation, l'autoriser à se maintenir provisoirement sur le territoire français en l'assignant à résidence, par dérogation à l'article L. 551-1, dans les cas suivants : / (...) 5° Si l'étranger doit être reconduit à la frontière en exécution d'une interdiction du territoire prévue au deuxième alinéa de l'article 131-30 du code pénal (...) " ; <br/>
<br/>
              3. Considérant, en premier lieu, qu'il résulte des dispositions de l'article L. 561-1 précité qu'il appartient à l'étranger qui, pour l'exécution d'une décision judiciaire d'interdiction du territoire ou pour en obtenir le relèvement, demande à être assigné à résidence en application de ces dispositions, de justifier soit qu'il se trouve dans l'impossibilité matérielle ou juridique de quitter le territoire français, soit que sa vie ou sa liberté sont menacées dans le pays de destination qui lui est assigné ou qu'il est exposé dans ce pays à des traitements contraires à l'article 3 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ; que, dès lors, en jugeant que l'intéressé ne justifiait pas, par l'invocation de considérations relatives à sa vie personnelle et familiale et notamment ses attaches en France, être dans l'impossibilité de quitter le territoire français, au sens de l'article L. 561-1 du code de l'entrée et du séjour des étrangers et du droit d'asile, la cour administrative d'appel de Paris n'a pas commis d'erreur de droit ; que, pour le même motif, le moyen tiré de ce que la cour aurait commis une erreur de droit et dénaturé les pièces du dossier en jugeant que le ministre n'avait pas commis d'erreur de droit ni d'erreur d'appréciation dans l'application de ces mêmes dispositions doit être écarté ;<br/>
<br/>
              4. Considérant, en deuxième lieu, que les conséquences d'un éloignement du territoire sur la vie privée et familiale de la personne qui en fait l'objet résultent des décisions judiciaires d'interdiction du territoire prononcées à son encontre et non de la décision par laquelle le ministre de l'intérieur se borne à prendre les mesures qu'implique l'exécution des décisions de l'autorité judiciaire ; qu'il résulte par ailleurs de ce qui a été dit au point 3 que ces conséquences ne sont pas au nombre des motifs de nature à justifier l'application des dispositions de l'article L. 561-1 précité ; que, dès lors, la cour administrative d'appel de Paris n'a pas commis d'erreur de droit en jugeant que le requérant ne pouvait utilement se prévaloir des stipulations de l'article 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales à l'encontre de la décision refusant de l'assigner à résidence ; <br/>
<br/>
              5. Considérant, en dernier lieu, qu'aux termes de l'article L. 541-2 du code de l'entrée et du séjour des étrangers et du droit d'asile : " Il ne peut être fait droit à une demande de relèvement d'une interdiction du territoire que si le ressortissant étranger réside hors de France. / Toutefois, cette disposition ne s'applique pas : (...) / 2° Lorsque l'étranger fait l'objet d'un arrêté d'assignation à résidence pris en application des articles L. 523-3, L. 523 4, L. 523-5 ou L. 561-1 " ; qu'en jugeant que le refus du ministre de faire droit à la demande d'assignation à résidence de l'intéressé sur le fondement de l'article L. 561-1 du même code, qui ne le privait pas de la faculté de présenter, depuis un pays étranger, une demande en relèvement de l'interdiction définitive du territoire français prononcée à son encontre, n'était pas entaché d'une erreur dans l'appréciation des conséquences de sa décision sur la situation personnelle du requérant, la cour n'a pas dénaturé les pièces du dossier ; <br/>
<br/>
              6. Considérant qu'il résulte de tout ce qui précède que le pourvoi de M. B...doit être rejeté, y compris ses conclusions présentées sur le fondement de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
      Article 1er : Le pourvoi de M. B...est rejeté.<br/>
<br/>
Article 2 : La présente décision sera notifiée à M. A...B...et au ministre d'Etat, ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
