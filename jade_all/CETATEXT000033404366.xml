<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033404366</ID>
<ANCIEN_ID>JG_L_2016_11_000000402744</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/40/43/CETATEXT000033404366.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème - 1ère chambres réunies, 16/11/2016, 402744</TITRE>
<DATE_DEC>2016-11-16</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>402744</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème - 1ère chambres réunies</FORMATION>
<TYPE_REC>Autres</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Marie-Françoise Guilhemsans</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Suzanne von Coester</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2016:402744.20161116</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              MM. E...F..., B...A...et C...D...ont, dans le cadre de l'instance introduite devant la chambre territoriale des comptes de Nouvelle-Calédonie comme suite au réquisitoire n° 2015-004-NC du 26 mai 2015 par lequel le procureur financier a requis cette chambre de statuer sur des faits susceptibles d'être qualifiés de gestion de fait des deniers de la province Sud, produit un mémoire, enregistré le 18 mai 2016 au greffe de cette chambre, en application de l'article 23-1 de l'ordonnance n° 58-1067 du 7 novembre 1958, par lequel ils soulèvent une question prioritaire de constitutionnalité. <br/>
<br/>
              Par un jugement n° 2016-QPC du 12 août 2016, enregistré au secrétariat du contentieux du Conseil d'Etat le 23 août 2016, la chambre territoriale des comptes de Nouvelle-Calédonie, avant de statuer, a décidé, par application des dispositions de l'article 23-2 de l'ordonnance n° 58-1067 du 7 novembre 1958, de transmettre au Conseil d'Etat la question de la conformité aux droits et libertés garantis par la Constitution de l'article 66 de la loi n° 90-1169 du 29 décembre 1990 de finances rectificative pour 1990 et de l'article 146-I-11° de la loi n° 2006-1771 du 30 décembre 2006 de finances pour 2006.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la Constitution, notamment son Préambule et ses articles 47 et 61-1 ; <br/>
              - l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
              - l'article 66 de la loi n° 90-1169 du 29 décembre 1990 ;<br/>
              - l'article 146-I-11 de la loi n° 2006-1771 du 30 décembre 2006 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Marie-Françoise Guilhemsans, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Suzanne von Coester, rapporteur public.<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il résulte des dispositions de l'article 23-4 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel que, lorsqu'une juridiction relevant du Conseil d'Etat a transmis à ce dernier, en application de l'article 23-2 de cette même ordonnance, la question de la conformité à la Constitution d'une disposition législative, le Conseil constitutionnel est saisi de cette question de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux ; <br/>
<br/>
              2. Considérant que, par la question prioritaire de constitutionnalité transmise, MM.F..., A...etD..., qui font l'objet d'une procédure de déclaration de gestion de fait devant la chambre territoriale des comptes de Nouvelle-Calédonie sur le fondement du XI de l'article 60 de la loi du 23 février 1963 de finances pour 1963, soulèvent la question de la conformité à la Constitution d'une part, de l'article 66 de la loi du 29 décembre 1990 de finances rectificative pour 1990, prévoyant l'application dans les territoires d'outre-mer de la rédaction alors en vigueur de cet article 60, d'autre part du 11° du I de l'article 146 de la loi du 30 décembre 2006 de finances pour 2006, qui insère à cet article 60, une disposition d'application à la Nouvelle-Calédonie et a ainsi pour effet d'étendre à cette collectivité les modifications intervenues depuis l'entrée en vigueur de l'article 66 de la loi du 29 décembre 1990 ; que cette question prioritaire de constitutionnalité est soulevée au cours de la première phase de la procédure de gestion de fait ; que cette première phase, qui constitue une instance autonome, se conclut par une décision du juge des comptes visant uniquement à reconnaître l'existence d'obligations constitutives de gestion de fait et à assujettir le comptable de fait aux obligations incombant aux comptables publics, notamment l'obligation de rendre un compte ; que ce n'est que dans une phase ultérieure de la procédure, lors du jugement du compte, que la chambre territoriale des comptes sera amenée à se prononcer sur les justifications fournies et sur l'opportunité de sanctionner le comptable de fait ; que cette première phase est régie par les dispositions des trois premiers alinéas du XI de l'article 60 de la loi du 23 février 1963 ; que les dispositions applicables à cette première phase ont été étendues à la Nouvelle-Calédonie par les dispositions précédemment mentionnées de l'article 66 de la loi du 29 décembre 1990 et n'ont été modifiées depuis lors ni par les dispositions du 11° du I de l'article 146 de la loi du 30 décembre 2006 ni par aucun autre texte ; que, par suite, ces dispositions de la loi du 30 décembre 2006  ne sont pas applicables au litige ; que les dispositions de l'article 66 de la loi du 29 décembre 1990 ne sont applicables au litige qu'en tant qu'elles auraient étendu à la Nouvelle-Calédonie les dispositions des trois premiers alinéas du XI de l'article 60 de la loi du 23 février 1963 ; <br/>
<br/>
              3. Considérant que lorsque le juge des comptes, en application des trois premiers alinéas du XI de l'article 60 de la loi du 23 février 1963, déclare une personne comptable de fait et lui demande de rendre un compte, il ne prononce pas une sanction ayant le caractère d'une punition ; que dès lors, MM.F..., A...et D...ne sauraient utilement soutenir que l'article 66 de la loi du 29 décembre 1990, précédemment mentionné, qui prévoit leur application en Nouvelle-Calédonie, méconnaîtrait les articles 7 et 8 de la Déclaration des droits de l'homme et du citoyen ;<br/>
<br/>
              4. Considérant que les requérants soutiennent également que l'article 66 de la loi n° 90-1169 du 29 décembre 1990 de finances rectificative pour 1990 méconnaît, dans la mesure où son adoption n'a pas été précédée de la consultation de l'assemblée délibérante de la Nouvelle-Calédonie, les droits des populations de cette collectivité, garantis par la Constitution ; que, toutefois, le grief tiré de la méconnaissance de la procédure d'adoption d'une loi ne peut être invoqué à l'appui d'une question prioritaire de constitutionnalité sur le fondement de l'article 61-1 de la Constitution ; <br/>
<br/>
              5. Considérant, enfin, que les requérants soutiennent que l'article 66 de la loi du 29 décembre 1990 méconnaît les dispositions de l'article 16 de la Déclaration des droits de l'homme et du citoyen, dans la mesure où il rendrait applicables en Nouvelle-Calédonie les dispositions relatives à la responsabilité des comptables publics, telle qu'elle est prévue par l'article 60 de la loi du 23 février 1960, sans prévoir une date différée d'entrée en vigueur ; que, toutefois, la mise en oeuvre de ces dispositions, qui ne portent atteinte à aucune situation légalement acquise, ne nécessitait en tout état de cause aucune mesure transitoire ; que dès lors les requérants ne sont  pas fondés à soutenir que cet article méconnaîtrait la garantie des droits proclamée par l'article 16 de la déclaration des droits de l'homme et du citoyen ; <br/>
<br/>
              6. Considérant qu'il résulte de ce qui précède que la question posée, qui n'est pas nouvelle, ne présente pas un caractère sérieux ; qu'il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité invoquée ; <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée par M. F...et autres.<br/>
<br/>
Article 2 : La présente décision sera notifiée à MM. E...F..., B...A...et C...D...et au ministre de l'économie et des finances.<br/>
Copie en sera adressée au Premier ministre, au garde des sceaux, ministre de la justice, au ministre des outre-mer, au Gouvernement de Nouvelle-Calédonie, à la Province Sud de Nouvelle-Calédonie, à la chambre territoriale des comptes de Nouvelle-Calédonie et au Conseil constitutionnel.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-10-01-02 PROCÉDURE. - RÈGLES DE PROCÉDURE D'ADOPTION D'UNE LOI - EXCLUSION.
</SCT>
<ANA ID="9A"> 54-10-01-02 Le grief tiré de la méconnaissance de la procédure d'adoption d'une loi ne peut être invoqué à l'appui d'une question prioritaire de constitutionnalité sur le fondement de l'article 61-1 de la Constitution.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
