<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034130203</ID>
<ANCIEN_ID>JG_L_2017_03_000000406024</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/13/02/CETATEXT000034130203.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème - 3ème chambres réunies, 01/03/2017, 406024, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-03-01</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>406024</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème - 3ème chambres réunies</FORMATION>
<TYPE_REC>Autres</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Vincent Uher</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Romain Victor</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:406024.20170301</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société à responsabilité limitée FB Finance, à l'appui de sa demande tendant à la décharge des droits supplémentaires de cotisation sur la valeur ajoutée des entreprises auxquels elle a été assujettie au titre des années 2011 à 2013, a produit un mémoire, enregistré le 30 décembre 2015 au greffe du tribunal administratif de Lille, en application de l'article 23-1 de l'ordonnance n° 58-1067 du 7 novembre 1958, par lequel elle soulève une question prioritaire de constitutionnalité.<br/>
<br/>
              Par une ordonnance n° 1508105 du 13 décembre 2016, enregistrée le 16 décembre 2016 au secrétariat du contentieux du Conseil d'Etat, le président de la 4ème chambre du tribunal administratif de Lille, avant qu'il soit statué sur la demande de la société FB Finance, a décidé, par application des dispositions de l'article 23-2 de l'ordonnance n° 58-1067 du 7 novembre 1958, de transmettre au Conseil d'Etat la question de la conformité aux droits et libertés garantis par la Constitution du I bis de l'article 1586 quater du code général des impôts.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la Constitution, notamment son Préambule et son article 61-1 ;<br/>
              - l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
              - le code général des impôts, notamment son article 1586 quater ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Vincent Uher, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Romain Victor, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il résulte des dispositions de l'article 23-4 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel que, lorsqu'une juridiction relevant du Conseil d'Etat a transmis à ce dernier, en application de l'article 23-2 de cette même ordonnance, la question de la conformité à la Constitution d'une disposition législative, le Conseil constitutionnel est saisi de cette question de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux.<br/>
<br/>
              2. En vertu du I de l'article 1586 quater du code général des impôts, les entreprises redevables de la cotisation sur la valeur ajoutée des entreprises bénéficient d'un dégrèvement dont le montant est égal à une fraction de cette cotisation. Cette fraction décroît en fonction de leur chiffre d'affaires. Aux termes du premier alinéa du I bis de cet article : " Lorsqu'une société est membre d'un groupe mentionné à l'article 223 A ou à l'article 223 A bis, le chiffre d'affaires à retenir pour l'application du I s'entend de la somme des chiffres d'affaires de chacune des sociétés membres du groupe. ".<br/>
<br/>
              3. La société FB Finance soutient, à l'appui de sa demande tendant à la décharge des droits supplémentaires de cotisation sur la valeur ajoutée des entreprises auxquels elle a été assujettie au titre des années 2011 à 2013, que ces dispositions, d'une part, en réservant un traitement différent, au regard de la cotisation sur la valeur ajoutée des entreprises, des sociétés membres d'un groupe fiscalement intégré et des sociétés qui ne sont pas membres d'un tel groupe, méconnaissent les principes d'égalité devant la loi et d'égalité devant les charges publiques garantis respectivement par les articles 6 et 13 de la Déclaration des droits de l'homme et du citoyen et, d'autre part, en s'appliquant aux sociétés ayant opté pour le régime d'intégration fiscale antérieurement à leur entrée en vigueur et en ne produisant pas d'effets pour les demandes de dégrèvement transitoire prévu à l'article 1647 C quinquies B du code général des impôts présentées, au titre de l'année 2010, postérieurement à leur entrée en vigueur, méconnaissent le principe de garantie des droits énoncé par l'article 16 de la Déclaration des droits de l'homme et du citoyen. <br/>
<br/>
              4. Le premier alinéa du I bis de l'article 1586 quater précité est applicable au présent litige. Cette disposition n'a pas déjà été déclarée conforme à la Constitution par le Conseil constitutionnel. Le moyen tiré de ce qu'elle porte atteinte aux droits et libertés garantis par la Constitution et notamment au principe d'égalité devant la loi soulève une question présentant un caractère sérieux. Ainsi, il y a lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité invoquée.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La question de la conformité à la Constitution du premier alinéa du I bis de l'article 1586 quater du code général des impôts est renvoyée au Conseil constitutionnel.<br/>
Article 2 : La présente décision sera notifiée à la société à responsabilité limitée FB Finance et au ministre de l'économie et des finances.<br/>
Copie en sera adressée au Premier ministre, ainsi qu'au tribunal administratif de Lille.<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
