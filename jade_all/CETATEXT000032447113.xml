<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032447113</ID>
<ANCIEN_ID>JG_L_2016_04_000000398597</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/44/71/CETATEXT000032447113.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 19/04/2016, 398597, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-04-19</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>398597</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FABIANI, LUC-THALER, PINATEL ; SCP MONOD, COLIN, STOCLET</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2016:398597.20160419</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B... A...a demandé au juge des référés du tribunal administratif de Melun, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, d'enjoindre, en premier lieu, au directeur territorial de l'Office français de l'immigration et de l'intégration de Créteil, d'une part, de lui verser l'allocation pour demandeur d'asile pour la période du 23 décembre 2015 à la date de l'ordonnance à intervenir dans un délai de 48 heures sous astreinte de 100 euros par jour de retard et, d'autre part, d'examiner sa demande dans le délai de 24 heures suivant la notification de l'ordonnance à intervenir et de lui indiquer le centre d'accueil pour demandeurs d'asile ou le lieu d'hébergement susceptible de l'accueillir dans un délai de huit jours, en deuxième lieu, au directeur de l'association France terre d'asile de lui délivrer sans délai un certificat de domiciliation conformément à l'article R. 744-3 du code de l'entrée et du séjour des étrangers et de droit d'asile et de lui fournir une aide matérielle, en dernier lieu, au préfet du Val-de-Marne de lui indiquer sans délai un lieu susceptible de l'accueillir au titre de l'article L. 345-2-2 du code de l'action sociale et des familles. <br/>
<br/>
              Par une ordonnance n° 1602505 du 23 mars 2016, le juge des référés du tribunal administratif de Melun a enjoint, d'une part, à l'Office français de l'immigration et de l'intégration (direction territoriale de Créteil) d'accorder à M. A... l'allocation aux demandeurs d'asile dans le délai de 15 jours à compter de la notification de l'ordonnance à effet du 8 janvier 2016 et, dans ce même délai, de lui proposer un hébergement, éventuellement en dehors de la région Île-de-France, dans les conditions prévues par le code de l'entrée et du séjour des étrangers et du droit d'asile, d'autre part, à l'association France terre d'asile de délivrer à M. A... un certificat d'hébergement dans le délai de 8 jours à compter de la notification de l'ordonnance et de lui accorder une aide d'urgence d'un montant de 300 euros dans le délai de 5 jours.<br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés les 7 et 14 avril 2016 au secrétariat du contentieux du Conseil d'Etat, l'association France terre d'asile, représentée par son président, demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, d'annuler cette ordonnance en tant qu'elle lui adresse des injonctions. <br/>
<br/>
<br/>
              Elle soutient que : <br/>
              - l'ordonnance contestée est irrégulière en ce qu'elle méconnaît, d'une part, les dispositions de l'article L. 522-1 du code de justice administrative et, d'autre part, le principe du contradictoire, dès lors qu'elle n'a pas été convoquée à l'audience ;<br/>
              - elle est entachée d'une erreur d'appréciation en ce qu'elle méconnaît les stipulations du cahier des clauses particulières de la convention conclue avec l'Office français de l'immigration et de l'intégration dès lors qu'elle lui enjoint, d'une part, d'accorder une somme d'argent et, d'autre part, de délivrer un certificat d'hébergement.<br/>
<br/>
              Par un mémoire, enregistré le 14 avril 2016, l'Office français de l'immigration et de l'intégration conclut aux mêmes fins que la requête de l'association France terre d'asile, par les mêmes moyens. <br/>
<br/>
              Par un mémoire en défense, enregistré le 14 avril 2016, M. B... A...conclut à un non-lieu partiel et au rejet de la requête. Il soutient qu'il n'y a plus lieu de délivrer un certificat d'hébergement et que, pour le surplus, les moyens invoqués par l'association requérante ne sont pas fondés.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              - le code de l'action sociale et des familles ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, l'association France terre d'asile, d'autre part, M.A..., l'Office français de l'immigration et de l'intégration et le ministre de l'intérieur ; <br/>
<br/>
              Vu le procès-verbal de l'audience publique du 15 avril 2016 à 10 heures au cours de laquelle ont été entendus :<br/>
<br/>
              - Me Stoclet, avocat au Conseil d'Etat et à la Cour de cassation, avocat de l'association France terre d'asile ;<br/>
<br/>
              - Me Pinatel, avocat au Conseil d'Etat et à la Cour de cassation, avocat de M. A... ;<br/>
<br/>
              - les représentants de l'Office française de l'immigration et de l'intégration ;<br/>
              et à l'issue de laquelle le juge des référés a clos l'instruction ;<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. En vertu de l'article L. 521-2 du code de justice administrative, le juge des référés,  saisi d'une demande en ce sens justifiée par l'urgence, peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté  une atteinte grave et manifestement illégale. <br/>
<br/>
              2. Ressortissant du Burundi, M. B...A..., qui est né en 1983,  est entré en France le 16 septembre 2013, sur le fondement d'un visa de long séjour pour y poursuivre des études. A l'expiration de ce visa, il a engagé, en décembre 2015, des démarches en vue de se voir reconnaître la qualité de réfugié. N'obtenant ni un hébergement ni le versement d'une allocation, il a saisi le juge des référés du tribunal administratif de Melun sur le fondement de l'article L. 521-2 du code de justice administrative. Par une ordonnance du 23 mars 2016, ce juge a fait droit à sa demande et adressé des injonctions à l'Office français de l'immigration et de l'intégration et à l'association France terre d'asile. L'association fait appel de cette ordonnance dans la mesure où elle prononce des injonctions à son égard.  <br/>
<br/>
              En ce qui concerne la régularité de l'ordonnance contestée :<br/>
<br/>
              3. L'association France terre d'asile soutient qu'elle n'a pas été convoquée à l'audience et que l'ordonnance attaquée a été en conséquence rendue dans des conditions irrégulières.<br/>
<br/>
              4. L'article L. 522-1 du code de justice administrative prévoit que le juge des référés, saisi sur le fondement des articles L. 521-1 ou L. 521-2, qui décide de tenir une audience publique " informe sans délai les parties de la date et de l'heure " de cette audience. Si l'article L. 5 de ce code dispose que " les exigences de la contradiction sont adaptées à celles de l'urgence ", il résulte tant du principe du contradictoire que des termes de l'article L. 522-1 qui en assurent la mise en oeuvre qu'il incombe au juge des référés d'informer, par tout moyen approprié, et en tenant compte de l'urgence, les parties de la date et de l'heure de l'audience.<br/>
<br/>
              5. Il résulte en l'espèce de l'instruction que le greffe du tribunal administratif de Melun a adressé la veille de l'audience par courriel aux parties un avis qui indiquait la date et l'heure de l'audience. Si, s'agissant de France terre d'asile, ce courriel a été envoyé non au siège central de l'association mais à l'adresse électronique de son antenne du Val-de-Marne, une telle communication doit être regardée comme ayant régulièrement avisé l'association. L'adresse électronique utilisée était en effet celle de la plate-forme du Val-de-Marne de l'association. Or il ressortait des documents soumis au tribunal que cette plate-forme avait elle-même conclu avec l'Office français de l'immigration et de l'intégration l'acte d'engagement du marché de prestations de premier accueil et d'accompagnement des demandeurs d'asile, dont M. A...demandait l'application. L'avis d'audience pouvait donc lui être régulièrement adressé, à charge pour elle d'informer les responsables nationaux de l'association. Dans ces conditions, l'ordonnance attaquée n'a pas été rendue dans des conditions irrégulières. <br/>
<br/>
              En ce qui concerne les conclusions relatives à l'hébergement :<br/>
<br/>
              6. Il résulte de l'instruction, et il a été confirmé à l'audience publique, que, postérieurement à l'introduction de l'appel, M. A...a reçu un hébergement assuré par l'Office français de l'immigration et de l'intégration. L'hébergement dont l'intéressé bénéfice désormais n'a pas été décidé en exécution de l'injonction contestée adressée à France terre d'asile. Les conclusions de la requête relatives à l'hébergement sont en conséquence devenues sans objet.<br/>
<br/>
              En ce qui concerne le versement d'une allocation d'urgence : <br/>
<br/>
              7. L'article L. 744-1 du code de l'entrée et du séjour des étrangers et du droit d'asile prévoit que l'Office français de l'immigration et de l'intégration " peut déléguer à des personnes morales, par convention, la possibilité d'assurer certaines prestations d'accueil et d'accompagnement social et administratif des demandeurs d'asile pendant la période d'instruction de leur demande ".  <br/>
<br/>
              8. Le marché de prestations de premier accueil et d'accompagnement des demandeurs d'asile conclu entre l'Office français de l'immigration et de l'intégration et l'association France terre d'asile sur le fondement de ces dispositions législatives stipule que l'association propose, à titre exceptionnel, aux demandeurs d'asile se trouvant dans une situation de grande précarité des aides de secours d'urgence, sous forme de bons ou de colis alimentaires. Il indique également que l'association oriente les intéressés vers des structures qui dispensent des aides alimentaires ou vestimentaires. Il ne met, en revanche, pas à la charge de l'association, le versement de sommes en argent. Le juge des référés a néanmoins ordonné à l'association de verser à M. A...la somme de 300 euros. Eu égard à ses termes, cette injonction ne peut être requalifiée comme tendant en réalité à l'attribution d'une aide en nature d'un montant équivalent. Elle met ainsi à la charge de l'association une obligation qui ne résulte pas de la convention conclue avec l'Office français de l'immigration et de l'intégration. L'association France terre d'asile est donc en tout état de cause fondée à demander l'annulation sur ce point de l'ordonnance attaquée.<br/>
<br/>
              9. Il résulte au demeurant de l'instruction que la prise en charge de M. A... par l'Office français de l'immigration et de l'intégration comprend l'attribution de l'allocation pour demandeurs d'asile, dont il a été confirmé au cours de l'audience publique que l'intéressé percevrait le versement, à compter de la date d'ouverture de ses droits, dans les prochains jours. <br/>
<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : Il n'y pas lieu de statuer sur les conclusions de la requête de l'association France terre d'asile relatives à l'hébergement de M.A....<br/>
Article 2 : L'article 2 de l'ordonnance du juge des référés du tribunal administratif de Melun du 23 mars 2016 est annulé en tant qu'il enjoint à l'association France terre d'asile de verser à M. A... une somme de 300 euros. Les conclusions présentées sur ce point par M.  A... au juge des référés du tribunal administratif de Melun sont rejetées. <br/>
Article 3 : La présente ordonnance sera notifiée à l'association France terre d'asile, à M. B... A...et à l'Office français de l'immigration et de l'intégration.<br/>
Copie en sera adressée au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
