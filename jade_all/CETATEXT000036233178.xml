<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036233178</ID>
<ANCIEN_ID>JG_L_2017_12_000000405093</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/23/31/CETATEXT000036233178.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème - 5ème chambres réunies, 18/12/2017, 405093, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-12-18</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>405093</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème - 5ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MASSE-DESSEN, THOUVENIN, COUDRAY ; SCP BARADUC, DUHAMEL, RAMEIX</AVOCATS>
<RAPPORTEUR>Mme Françoise Tomé</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Dieu</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:405093.20171218</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              La société Richard Pascal a demandé au tribunal administratif de Nancy d'annuler pour excès de pouvoir la décision de l'inspecteur du travail de la 1ère section de l'unité territoriale des Vosges du 21 septembre 2012 déclarant Mme A...B...inapte à reprendre son poste de travail au sein de l'entreprise et la décision du 21 décembre 2012 par laquelle le ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social a rejeté son recours contre cette décision. Par un jugement n° 1300370 du 14 avril 2015, le tribunal administratif a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 15NC01290 du 5 juillet 2016, la cour administrative d'appel de Nancy a, sur appel de la société Richard Pascal, annulé ce jugement et ces deux décisions. <br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés le 16 novembre 2016 et les 10 février et 28 août 2017 au secrétariat du contentieux du Conseil d'Etat, Mme B...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de la société Richard Pascal une somme de 3 500 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative et des articles 35 et 75 de la loi du 10 juillet 1991.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code du travail ;<br/>
              - le code des relations entre le public et l'administration ;<br/>
              - la loi n° 79-587 du 11 juillet 1979 ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Françoise Tomé, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Frédéric Dieu, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Masse-Dessen, Thouvenin, Coudray, avocat de Mme B...et à la SCP Baraduc, Duhamel, Rameix, avocat de la société Richard Pascal ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des énonciations de l'arrêt attaqué qu'à la suite d'un congé de maladie, MmeB..., salariée de la société Richard Pascal, a été déclarée apte à la reprise de ses fonctions par le médecin du travail ; que, saisi d'un recours par MmeB..., l'inspecteur du travail de la 1ère section de l'unité territoriale des Vosges a, par une décision du 21 septembre 2012, annulé cet avis d'aptitude et déclaré Mme B...inapte à reprendre toute activité au sein de la société Richard Pascal ; que, par une décision du 21 décembre 2012, le ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social, a rejeté le recours hiérarchique formé par la société Richard Pascal contre la décision de l'inspecteur du travail ; que Mme B...se pourvoit en cassation contre l'arrêt du 5 juillet 2016 par lequel la cour administrative d'appel de Nancy, saisie en appel d'un jugement du 14 avril 2015 du tribunal administratif de Nancy qui avait rejeté la demande d'annulation de la société Richard Pascal, a annulé ces deux décisions ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article L. 1226-2 du code du travail, dans sa rédaction alors applicable : " Lorsque, à l'issue des périodes de suspension du contrat de travail (...) le salarié est déclaré inapte par le médecin du travail à reprendre l'emploi qu'il occupait précédemment, l'employeur lui propose un autre emploi approprié à ses capacités. / Cette proposition prend en compte les conclusions écrites du médecin du travail et les indications qu'il formule sur l'aptitude du salarié à exercer l'une des tâches existantes dans l'entreprise " ; qu'aux termes de l'article L. 4624-1 du même code, dans sa rédaction alors applicable : " Le médecin du travail est habilité à proposer des mesures individuelles telles que mutations ou transformations de postes, justifiées par des considérations relatives notamment à l'âge, à la résistance physique ou à l'état de santé physique et mentale des travailleurs. / L'employeur est tenu de prendre en considération ces propositions et, en cas de refus, de faire connaître les motifs qui s'opposent à ce qu'il y soit donné suite. / En cas de difficulté ou de désaccord, l'employeur ou le salarié peut exercer un recours devant l'inspecteur du travail. Ce dernier prend sa décision après avis du médecin inspecteur du travail / " ; que ces dispositions, qui fixent le contenu des avis du médecin du travail en matière d'aptitude au poste de travail, définissent entièrement les règles de motivation applicables, tant à ces décisions qu'aux décisions prises, sur recours contre ces avis, par l'inspecteur du travail ou, sur recours hiérarchique, par le ministre chargé du travail, à l'exclusion des dispositions de l'article 1er de la loi du 11 juillet 1979 sur la motivation des actes administratifs, devenues l'article L. 122-1 du code des relations du public avec l'administration ;<br/>
<br/>
              3. Considérant qu'il résulte de ce qui précède qu'en se fondant, pour annuler la décision du 21 septembre 2012 de l'inspecteur du travail et la décision du 21 décembre 2012 du ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social, sur ce que, faute de préciser les motifs pour lesquels les conditions de travail de Mme B...s'étaient dégradées au sein de l'entreprise, ces décisions ne mentionnaient pas les éléments de fait qui en constituaient le fondement et méconnaissaient, par suite, les dispositions de l'article 1er de la loi du 11 juillet 1979, la cour administrative d'appel a entaché son arrêt d'une erreur de droit ;<br/>
<br/>
              4. Considérant qu'il résulte de ce qui précède, que, sans qu'il soit besoin d'examiner l'autre moyen du pourvoi, Mme B...est fondée à demander l'annulation de l'arrêt qu'elle attaque ;<br/>
<br/>
              5. Considérant que Mme B...a obtenu le bénéfice de l'aide juridictionnelle ; que, par suite, son avocat peut se prévaloir des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 ; qu'il y a lieu, dans les circonstances de l'espèce, et sous réserve que la SCP Masse-Dessen, Thouvenin, Coudray, avocat de Mme B..., renonce à percevoir la somme correspondant à la part contributive de l'Etat, de mettre à la charge de la société Richard Pascal une somme de 3 000 euros à verser à la SCP Masse-Dessen, Thouvenin Coudray ; qu'en revanche, ces mêmes dispositions font obstacle à ce qu'une somme soit mise à ce titre à la charge de Mme B...qui n'est pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Nancy du 5 juillet 2016 est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Nancy.<br/>
Article 3 : La société Richard Pascal versera à la SCP Masse-Dessen, Thouvenin, Coudray une somme de 3 000 euros en application du deuxième alinéa de l'article 37 de la loi du 10 juillet 1991, sous réserve que cette société renonce à percevoir la somme correspondant à la part contributive de l'Etat.<br/>
Article 4 : Les conclusions présentées par la société Richard Pascal au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée à Mme A...B...et à la société Richard Pascal.<br/>
Copie en sera adressée à la ministre du travail. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
