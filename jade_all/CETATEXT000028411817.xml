<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028411817</ID>
<ANCIEN_ID>JG_L_2013_12_000000354881</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/41/18/CETATEXT000028411817.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère et 6ème sous-sections réunies, 30/12/2013, 354881</TITRE>
<DATE_DEC>2013-12-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>354881</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère et 6ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Gaël Raimbault</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Alexandre Lallet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:354881.20131230</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<p>Vu la requête sommaire et le mémoire complémentaire, enregistrés les 14 décembre 2011 et 14 mars 2012 au secrétariat du contentieux du Conseil d'Etat, présentés par le Syndicat national des exploitants de parcours aventures (SNEPA), dont le siège est Le Bourg aux Estables (43150), représenté par son président ; le SNEPA demande au Conseil d'Etat :<br clear="none"/>
<br clear="none"/>
1°) d'annuler pour excès de pouvoir l'article 8 de l'arrêté du ministre du travail, de l'emploi et de la santé du 7 octobre 2011 portant extension d'accords et d'avenants examinés en sous-commission des conventions et accords du 30 septembre 2011, étendant l'avis interprétatif du 9 juillet 2010 relatif à l'avenant n° 26 ter, conclu dans le cadre de la convention collective nationale des espaces de loisirs, d'attractions et culturels du 5 janvier 1994 ;<br clear="none"/>
<br clear="none"/>
2°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br clear="none"/>
<br clear="none"/>
<br clear="none"/>
Vu les autres pièces du dossier ;<br clear="none"/>
<br clear="none"/>
Vu le code du travail ;<br clear="none"/>
<br clear="none"/>
Vu le code de justice administrative ;<br clear="none"/>
<br clear="none"/>
Après avoir entendu en séance publique :<br clear="none"/>
<br clear="none"/>
- le rapport de M. Gaël Raimbault, Maître des Requêtes en service extraordinaire,<br clear="none"/>
<br clear="none"/>
- les conclusions de M. Alexandre Lallet, rapporteur public ;<br clear="none"/>
<br clear="none"/>
<br clear="none"/>
<br clear="none"/>
<br clear="none"/>
<br clear="none"/>
1. Considérant que, par l'article 8 de l'arrêté du 7 octobre 2011 portant extension d'accords et d'avenants examinés en sous-commission des conventions et accords du 30 septembre 2011, le ministre du travail, de l'emploi et de la santé a rendu obligatoires, pour tous les employeurs et tous les salariés compris dans le champ d'application de la convention collective nationale des espaces de loisirs, d'attractions et culturels du 5 janvier 1994, les stipulations de l'avis interprétatif du 9 juillet 2010 relatif à l'avenant n° 26 ter du 13 novembre 2009 qui avait révisé le champ d'application de cette convention ;<br clear="none"/>
<br clear="none"/>
2. Considérant, en premier lieu, qu'il résulte des dispositions du décret du 27 juillet 2005 relatif aux délégations de signature des membres du Gouvernement que le directeur général du travail, dont le décret de nomination a été publié au Journal officiel de la République française le 26 août 2006, avait du fait de cette nomination compétence pour signer l'arrêté attaqué au nom du ministre ;<br clear="none"/>
<br clear="none"/>
3. Considérant, en deuxième lieu, qu'il résulte des dispositions des articles L. 2261-15, L. 2261-16 et L. 2261-24 du code du travail que le ministre chargé du travail peut rendre obligatoires, par arrêté, les avenants ou annexes à une convention ou à un accord étendu, après avis motivé de la commission nationale de la négociation collective ; qu'en vertu des dispositions des articles L. 2271-1 et R. 2272-10, la sous-commission des conventions et accords peut exercer les missions de la commission relatives à l'extension et à l'élargissement des conventions et accords collectifs ; qu'en l'espèce, l'avis de la sous-commission, consultée lors de sa séance du 30 septembre 2011, rappelle la procédure suivie et les conditions de négociation et indique que l'extension a recueilli l'avis favorable de l'ensemble des organisations représentées ; qu'une telle motivation répond aux exigences de l'article L. 2261-15 du code du travail ; que, contrairement à ce que soutient le syndicat requérant, il n'avait pas à être consulté ou convoqué à une réunion de la commission nationale de la négociation collective, dont la composition est fixée par les articles R. 2272-1 et suivants du code du travail, et cette commission n'avait pas à rendre un avis préalablement à la négociation et à la conclusion de l'accord ;<br clear="none"/>
<br clear="none"/>
4. Considérant, en troisième lieu, qu'en dehors de l'hypothèse mentionnée à l'article L. 2261-27 du code du travail où le ministre chargé du travail décide l'extension d'une convention, d'un accord ou d'un avenant malgré l'opposition écrite et motivée de deux organisations d'employeurs ou de deux organisations de salariés représentées à la commission nationale de la négociation collective, aucun texte ni aucun principe n'impose la motivation de l'arrêté, à caractère réglementaire, par lequel le ministre étend une convention, un accord ou un avenant ;<br clear="none"/>
<br clear="none"/>
5. Considérant, en quatrième lieu, qu'aux termes de l'article L. 2261-19 du code du travail : " Pour pouvoir être étendus, la convention de branche ou l'accord professionnel ou interprofessionnel, leurs avenants ou annexes, doivent avoir été négociés et conclus en commission paritaire. / Cette commission est composée de représentants des organisations syndicales d'employeurs et de salariés représentatives dans le champ d'application considéré " ; que, sauf lorsque ce champ recouvre plusieurs branches d'activités distinctes, cette représentativité doit être appréciée par le ministre chargé du travail au niveau du champ d'application de la convention ou de l'accord considéré ; qu'il en est ainsi même si l'avenant conclu ne couvre qu'une partie des entreprises entrant dans le champ de la convention ou de l'accord qu'il complète ou modifie ; qu'en l'espèce, et alors même qu'existe une organisation représentant distinctement les entreprises de ce secteur, l'activité de parcours acrobatique en hauteur, qui a été incluse dans le champ de la convention collective nationale des espaces de loisirs, d'attractions et culturels par un avenant du 13 novembre 2009, ne présente pas des particularités telles qu'elle doive être regardée comme constituant à elle seule une branche d'activité ; que si le Syndicat national des exploitants de parcours aventures regroupe une proportion notable des entreprises gérant des parcs acrobatiques en hauteur, il ne ressort pas des pièces du dossier qu'il serait représentatif au niveau du champ d'application de la convention collective nationale des espaces de loisirs, d'attractions et culturels ; que, par suite, il n'est pas fondé à soutenir qu'il aurait dû être invité à désigner des représentants à la commission paritaire au sein de laquelle a été négocié et conclu l'avis interprétatif étendu par l'arrêté litigieux ;<br clear="none"/>
<br clear="none"/>
6. Considérant, toutefois, qu'il résulte des dispositions des articles L. 2261-15 et L. 2261-16 du code du travail que l'extension d'un avenant ou d'une annexe à une convention ou à un accord étendu a pour objet d'en rendre les stipulations obligatoires pour tous les salariés et employeurs compris dans le champ d'application de la convention ou de l'accord de référence ou dans le champ d'application différent qu'il détermine ; qu'en vertu de l'article L. 2222-1 du même code, les conventions et accords collectifs de travail " déterminent leur champ d'application territorial et professionnel ", le champ d'application professionnel étant " défini en termes d'activités économiques " ; qu'aux termes du premier alinéa de l'article L. 2261-2 du code du travail : " La convention collective applicable est celle dont relève l'activité principale exercée par l'employeur " ;<br clear="none"/>
<br clear="none"/>
7. Considérant que l'avis interprétatif du 9 juillet 2010 prévoit qu'une entreprise exerçant l'activité de parcours acrobatique en hauteur ne relève du champ d'application de la convention collective nationale des espaces de loisirs, d'attractions et culturels que si l'exploitant met en oeuvre tout moyen qui garantit la mise en sécurité du visiteur et si l'organisme de contrôle agréé des installations fournit à l'entreprise une attestation qui assure que ces moyens satisfont aux conditions de sécurité requises et sont bien mis en oeuvre ; que le syndicat requérant soutient que le ministre a commis une erreur de droit et une erreur manifeste d'appréciation en étendant un accord qui exclut ainsi du champ d'application de la convention collective les parcs d'aventures qui ne satisferaient pas à certaines obligations de sécurité ; que l'appréciation du bien-fondé de ces moyens dépend du point de savoir si une convention collective peut valablement définir son champ d'application en fonction du respect par l'employeur d'obligations de sécurité et de leur contrôle par un organisme agréé ; que cette question, qui ne peut être résolue au vu d'une jurisprudence établie, soulève une difficulté sérieuse qu'il n'appartient qu'à l'autorité judiciaire de trancher ; que, par suite, il y a lieu pour le Conseil d'Etat de surseoir à statuer sur la requête du Syndicat national des exploitants de parcours aventures tendant à l'annulation de l'article 8 de l'arrêté du ministre du travail, de l'emploi et de la santé du 7 octobre 2011 jusqu'à ce que la juridiction compétente se soit prononcée sur cette question préjudicielle ;<br clear="none"/>
<br clear="none"/>
<br clear="none"/>
<br clear="none"/>
D E C I D E :<br clear="none"/>
--------------<br clear="none"/>
Article 1er : Il est sursis à statuer sur la requête du Syndicat national des exploitants de parcours aventures dirigée contre l'article 8 de l'arrêté du 7 octobre 2011 portant extension d'accords et d'avenants examinés en sous-commission des conventions et accords du 30 septembre 2011 jusqu'à ce que l'autorité judiciaire se soit prononcée sur la question de savoir si une convention collective peut valablement définir son champ d'application en fonction du respect par l'employeur d'obligations de sécurité et de leur contrôle par un organisme agréé.<br clear="none"/>
Article 2 : Le Syndicat national des exploitants de parcours aventures devra justifier devant le Conseil d'Etat, dans un délai de deux mois à compter de la notification de la présente décision, de sa diligence à saisir de cette question la juridiction compétente.<br clear="none"/>
Article 3 : La présente décision sera notifiée au Syndicat national des exploitants de parcours aventures, au ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social, au Syndicat national des espaces de loisirs, d'attractions et culturels et au Syndicat national des discothèques.<br clear="none"/>
Copie en sera adressée à la Confédération générale du travail - Force ouvrière, à la Confédération française des travailleurs chrétiens, à la Fédération des services CFDT, à la Fédération du commerce et des services CGT et à la CFE-CGC INOVA.</p>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">17-04-02-01 COMPÉTENCE. COMPÉTENCES CONCURRENTES DES DEUX ORDRES DE JURIDICTION. CONTENTIEUX DE L'APPRÉCIATION DE LA LÉGALITÉ. CAS OÙ UNE QUESTION PRÉJUDICIELLE S'IMPOSE. - ARRÊTÉ D'EXTENSION D'UNE CONVENTION OU D'UN ACCORD COLLECTIF - QUESTION DE SAVOIR SI UNE CONVENTION OU UN ACCORD COLLECTIF PEUT VALABLEMENT DÉLIMITER SON CHAMP D'APPLICATION EN FONCTION DU RESPECT PAR LES EMPLOYEURS D'OBLIGATIONS DE SÉCURITÉ OU DE LEUR CONTRÔLE PAR UN ORGANISME AGRÉÉ - ABSENCE DE JURISPRUDENCE ÉTABLIE DES JURIDICTIONS JUDICIAIRES SUR CE POINT [RJ1] - DIFFICULTÉ SÉRIEUSE - CONSÉQUENCE - QUESTION PRÉJUDICIELLE AU JUGE JUDICIAIRE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">66-02-02-035 TRAVAIL ET EMPLOI. CONVENTIONS COLLECTIVES. EXTENSION DES CONVENTIONS COLLECTIVES. CONDITION DE LÉGALITÉ DE L'EXTENSION TENANT À LA VALIDITÉ DE LA CONVENTION. - POSSIBILITÉ POUR UNE CONVENTION OU UN ACCORD COLLECTIF DE DÉLIMITER VALABLEMENT SON CHAMP D'APPLICATION EN FONCTION DU RESPECT PAR LES EMPLOYEURS D'OBLIGATIONS DE SÉCURITÉ OU DE LEUR CONTRÔLE PAR UN ORGANISME AGRÉÉ - ABSENCE DE JURISPRUDENCE ÉTABLIE DES JURIDICTIONS JUDICIAIRES SUR CE POINT [RJ1] - QUESTION SOULEVANT UNE DIFFICULTÉ SÉRIEUSE - CONSÉQUENCE - QUESTION PRÉJUDICIELLE AU JUGE JUDICIAIRE.
</SCT>
<ANA ID="9A"> 17-04-02-01 Avis interprétatif du 9 juillet 2010 prévoyant qu'une entreprise exerçant l'activité de parcours acrobatique en hauteur ne relève du champ d'application de la convention collective nationale des espaces de loisirs, d'attractions et culturels du 5 janvier 1994 que si l'exploitant met en oeuvre tout moyen qui garantit la mise en sécurité du visiteur et si l'organisme de contrôle agréé des installations fournit à l'entreprise une attestation qui assure que ces moyens satisfont aux conditions de sécurité requises et sont bien mis en oeuvre.,,,La réponse au moyen tiré de ce que le ministre ne pouvait légalement procéder à l'extension de cet avis, qui exclut du champ d'application de la convention les parcs d'aventures qui ne satisferaient pas à certaines obligations de sécurité dépend du point de savoir si une convention collective peut valablement définir son champ d'application en fonction du respect par l'employeur d'obligations de sécurité et de leur contrôle par un organisme agréé, alors qu'il résulte des dispositions de l'article L. 2222-1 du code du travail que le champ d'application des conventions et accords collectifs de travail est en principe défini en termes d'activités économiques.... ,,Cette question, qui ne peut être résolue au vu d'une jurisprudence établie, soulève une difficulté sérieuse qu'il n'appartient qu'à l'autorité judiciaire de trancher.</ANA>
<ANA ID="9B"> 66-02-02-035 Avis interprétatif du 9 juillet 2010 prévoyant qu'une entreprise exerçant l'activité de parcours acrobatique en hauteur ne relève du champ d'application de la convention collective nationale des espaces de loisirs, d'attractions et culturels du 5 janvier 1994 que si l'exploitant met en oeuvre tout moyen qui garantit la mise en sécurité du visiteur et si l'organisme de contrôle agréé des installations fournit à l'entreprise une attestation qui assure que ces moyens satisfont aux conditions de sécurité requises et sont bien mis en oeuvre.,,,La réponse au moyen tiré de ce que le ministre ne pouvait légalement procéder à l'extension de cet avis, qui exclut du champ d'application de la convention les parcs d'aventures qui ne satisferaient pas à certaines obligations de sécurité dépend du point de savoir si une convention collective peut valablement définir son champ d'application en fonction du respect par l'employeur d'obligations de sécurité et de leur contrôle par un organisme agréé, alors qu'il résulte des dispositions de l'article L. 2222-1 du code du travail que le champ d'application des conventions et accords collectifs de travail est en principe défini en termes d'activités économiques.,,,Cette question, qui ne peut être résolue au vu d'une jurisprudence établie, soulève une difficulté sérieuse qu'il n'appartient qu'à l'autorité judiciaire de trancher.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, Section, 23 mars 2012, Fédération Sud Santé Sociaux, n° 331805, p. 102.


</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
