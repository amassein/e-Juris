<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039728711</ID>
<ANCIEN_ID>JG_L_2019_12_000000423958</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/72/87/CETATEXT000039728711.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère - 4ème chambres réunies, 31/12/2019, 423958, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-12-31</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>423958</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère - 4ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Bénédicte Fauvarque-Cosson</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Marie Sirinelli</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:423958.20191231</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu les procédures suivantes :<br/>
<br/>
              1° Sous le n° 423958, par une requête, un nouveau mémoire et deux mémoires en réplique, enregistrés le 7 septembre 2018, le 5 mars 2019, le 26 mars 2019 et le 19 décembre 2019 au secrétariat du contentieux du Conseil d'Etat, la société Sanofi Aventis France demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision implicite par laquelle la ministre des solidarités et de la santé et le ministre de l'action et des comptes publics ont rejeté sa demande tendant à l'abrogation de l'instruction de ces ministres n°DSS/1C/DGOS/PF2/2018/42 du 19 février 2018 relative à l'incitation à la prescription hospitalière de médicaments biologiques similaires lorsqu'ils sont délivrés en ville et l'arrêté des mêmes ministres du 3 août 2018 relatif à l'expérimentation pour l'incitation à la prescription hospitalière de médicaments biologiques similaires délivrés en ville, en tant qu'ils s'appliquent à la spécialité Toujeo ; <br/>
<br/>
              2°) d'enjoindre aux ministres chargés de la santé et de la sécurité sociale de modifier cette instruction et cet arrêté pour exclure Toujeo du groupe de l'insuline glargine ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 6 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              2° Sous le n° 429693, par une requête et un mémoire en réplique, enregistrés les 12 avril et 8 octobre 2019, la société Sanofi Aventis France demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler pour excès de pouvoir l'arrêté de la ministre des solidarités et de la santé et du ministre de l'action et des comptes publics du 12 février 2019 relatif à l'expérimentation pour l'incitation à la prescription hospitalière de médicaments biologiques similaires délivrés en ville, en tant qu'il s'applique à la spécialité Toujeo ;<br/>
<br/>
               2°) de mettre à la charge de l'Etat la somme de 6 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
              3° Sous le n° 430318, par une requête et un mémoire en réplique, enregistrés les 30 avril et 8 octobre 2019, la société Sanofi Aventis France demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler pour excès de pouvoir l'arrêté de la ministre des solidarités et de la santé et du ministre de l'action et des comptes publics du 19 mars 2019 relatif à l'efficience et la pertinence de la prescription hospitalière de médicaments biologiques similaires délivrés en ville, en tant qu'il s'applique à la spécialité Toujeo ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 6 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
              4° Sous le n° 433185, par une requête, enregistrée le 1er août 2019, la société Sanofi Aventis France demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler pour excès de pouvoir l'arrêté de la ministre des solidarités et de la santé et du ministre de l'action et des comptes publics du 29 mai 2019 relatif à l'efficience et la pertinence de la prescription hospitalière de médicaments biologiques similaires délivrés en ville, en tant qu'il s'applique à la spécialité Toujeo ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 6 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu : <br/>
              - le code de la santé publique ;<br/>
              - le code de la sécurité sociale ;<br/>
              - la loi n° 2018-1203 du 22 décembre 2018 ;<br/>
              - le code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Bénédicte Fauvarque-Cosson, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Marie Sirinelli, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Par quatre requêtes qu'il y a lieu de joindre, la société Sanofi Aventis France demande l'annulation pour excès de pouvoir de la décision implicite par laquelle la ministre des solidarités et de la santé et le ministre de l'action et des comptes publics ont rejeté sa demande tendant à l'abrogation de leur instruction du 19 février 2018 relative à l'incitation à la prescription hospitalière de médicaments biologiques similaires lorsqu'ils sont délivrés en ville, des arrêtés des mêmes ministres des 3 août 2018 et 12 février 2019 relatifs à l'expérimentation pour l'incitation à la prescription hospitalière de médicaments biologiques similaires délivrés en ville et des arrêtés de ces ministres des 19 mars 2019 et 29 mai 2019 relatifs à l'efficience et la pertinence de la prescription hospitalière de médicaments biologiques similaires délivrés en ville, en tant qu'ils s'appliquent à la spécialité Toujeo 300 unités par millilitre, solution injectable en stylo prérempli, indiquée dans le traitement du diabète sucré de l'adulte, qu'elle commercialise en vertu d'une autorisation de mise sur le marché délivrée le 24 avril 2015. <br/>
<br/>
              Sur l'instruction du 19 février 2018 : <br/>
<br/>
              2. L'instruction du 19 février 2018 relative à l'incitation à la prescription hospitalière des médicaments biologiques similaires lorsqu'ils sont délivrés en ville prévoit, d'une part, un dispositif d'intéressement à cette prescription au profit des établissements ayant signé un contrat d'amélioration de la qualité et de l'efficience des soins, faisant l'objet d'une rémunération versée en 2019 au titre du recours aux médicaments biosimilaires des groupes étanercept et insuline glargine durant l'année 2018, et annonce, d'autre part, l'instauration d'un dispositif expérimental comportant un intéressement plus important des services hospitaliers de certains établissements, sur le fondement de l'article 51 de la loi du 22 décembre 2018 de financement de la sécurité sociale pour 2019.<br/>
<br/>
              3. Par l'arrêté du 19 mars 2019 relatif à l'efficience et la pertinence de la prescription hospitalière de médicaments biologiques similaires délivrés en ville, la ministre des solidarités et de la santé et le ministre de l'action et des comptes publics ont prévu que les établissements de santé qui exercent les activités de médecine, de chirurgie, de gynécologie-obstétrique et d'odontologie et ont signé un contrat d'amélioration de la qualité et de l'efficience des soins perçoivent une dotation pour l'efficience et la pertinence de leur prescription, fixée, au titre de l'année 2018, sur la base d'indicateurs relatifs à la prescription hospitalière de médicaments biologiques similaires délivrés en ville, pour les groupes étanercept et insuline glargine. Ces dispositions se sont substituées à celles de l'instruction du 19 février 2018 relatives au dispositif d'intéressement à la prescription de médicaments biologiques similaires au profit des établissements ayant signé un tel contrat, seules à faire grief et à être critiquées par la société requérante, en tant qu'elles s'appliquent à la spécialité Toujeo. Au surplus, par une nouvelle instruction du 26 mars 2019 relative à l'incitation à la prescription hospitalière de médicaments biologiques similaires, lorsqu'ils sont délivrés en ville, la ministre des solidarités et de la santé et le ministre de l'action et des comptes publics, faisant état de l'intervention de l'arrêté du 19 mars 2019, ont déclaré abroger leur instruction du 19 février 2018.<br/>
<br/>
              4. Dans ces conditions, et dès lors, en tout état de cause, que la société Sanofi Aventis France demande également l'annulation pour excès de pouvoir de l'arrêté du 19 mars 2019, les conclusions de sa requête n° 423958 tendant à l'annulation du refus d'abroger l'instruction du 19 février 2018 ont perdu leur objet en cours d'instance. Il n'y a, ainsi, pas lieu d'y statuer.<br/>
<br/>
              Sur la notion de médicament biologique similaire et le statut de la spécialité Toujeo :<br/>
<br/>
              5. Aux termes de l'article  L. 5121-1 du  code  de  la  santé   publique  : " On entend par : (...) 15° a) (...) médicament biologique similaire, tout médicament biologique de même composition qualitative et quantitative en substance active et de même forme pharmaceutique qu'un médicament biologique de référence mais qui ne remplit pas les conditions prévues au a du 5° du présent article pour être regardé comme une spécialité générique en raison de différences liées notamment à la variabilité de la matière première ou aux procédés de fabrication et nécessitant que soient produites des données précliniques et cliniques supplémentaires dans des conditions déterminées par voie réglementaire (...) ; / b) Groupe biologique similaire, le regroupement d'un médicament biologique de référence et de ses médicaments biologiques similaires, tels que définis au a du présent 15°. Ils sont regroupés au sein de la liste de référence des groupes biologiques similaires établie par l'Agence nationale de sécurité du médicament et des produits de santé (...) ". Les dispositions des articles L. 5125-23-2 et L. 5125-23-3 du même code précisent les précautions que le prescripteur doit respecter lorsqu'il initie un traitement par un médicament biologique et les conditions qui doivent être réunies pour que le pharmacien puisse délivrer un médicament biologique similaire par substitution au médicament biologique prescrit. <br/>
<br/>
              6. Il résulte du a du 15° de l'article L. 5121-1 du code de la santé publique qu'un médicament biologique ne peut être qualifié de " médicament biologique similaire " que s'il a la même composition qualitative et quantitative en substance active que le médicament biologique de référence, ainsi que la même forme pharmaceutique. Il ressort des pièces des dossiers que les spécialités Lantus, insuline glargine 100 unités par millilitre, et Toujeo, insuline glargine 300 unités par millilitre, sont deux analogues lents de l'insuline, à base d'insuline glargine, pour le traitement symptomatique de l'hyperglycémie et constituent tous deux, chez l'adulte, des traitements de première intention dans la prise en charge du diabète de type 1 et des traitements de seconde intention du diabète de type 2. Toutefois, Toujeo est une formulation à libération prolongée d'insuline glargine trois fois plus concentrée que la spécialité Lantus. Au demeurant, elle ne figure pas au sein du groupe biologique similaire " insuline glargine ", ayant Lantus pour médicament biologique de référence, inscrit par l'Agence nationale de sécurité du médicament et des produits de santé sur la liste de référence des groupes biologiques similaires, et les spécialités Lantus et Toujeo ne sont pas directement interchangeables mais ont une cinétique différente. Lantus et Toujeo diffèrent ainsi par leur composition quantitative en substance active, ce qui fait obstacle à ce que l'un puisse être regardé comme un médicament biologique similaire de l'autre au sens du 15° de l'article L. 5121-1 du code de la santé publique.<br/>
<br/>
              Sur la légalité des arrêtés attaqués :<br/>
<br/>
              7. D'une part, l'article L. 162-31-1 du code de la sécurité sociale, dans sa rédaction issue de la loi du 30 décembre 2017 de financement de la sécurité sociale pour 2018, prévoit à son I que des expérimentations dérogatoires à certaines dispositions qu'il énumère, relatives notamment aux règles de facturation, de tarification et de remboursement applicables aux établissements de santé, " peuvent être mises en oeuvre, pour une durée qui ne peut excéder cinq ans. / Ces expérimentations ont l'un ou l'autre des buts suivants : / (...) / 2° Améliorer la pertinence de la prise en charge par l'assurance maladie des médicaments (...) et la qualité des prescriptions, en modifiant : / (...) / b) Les modalités de rémunération, les dispositions prévoyant des mesures incitatives ou de modulation concernant les professionnels de santé ou les établissements de santé, ainsi que des mesures d'organisation dans l'objectif de promouvoir un recours pertinent aux médicaments (...) ". Le III du même article précise que : " Les expérimentations à dimension nationale sont autorisées, le cas échéant après avis de la Haute Autorité de santé, par arrêté des ministres chargés de la sécurité sociale et de la santé. (...) / Les catégories d'expérimentations (...) sont précisées par décret en Conseil d'Etat ". Aux termes du II de l'article R. 162-50-1 du même code : " Les expérimentations mentionnées au 2° du I de l'article L. 162-31-1 regroupent les catégories d'expérimentation visant à améliorer l'efficience ou la qualité : / (...) / 2° De la prescription des médicaments (...), notamment par le développement de nouvelles modalités de rémunération et d'incitations financières (...) ". <br/>
<br/>
              8. D'autre part, l'article L. 162-22-7-4 inséré dans le code de la sécurité sociale par la loi du 22 décembre 2018 de financement de la sécurité sociale pour 2019 prévoit que les établissements de santé exerçant les activités de médecine, de chirurgie, de gynécologie-obstétrique et d'odontologie peuvent bénéficier d'une dotation du fonds d'intervention régional mentionné à l'article L. 1435-8 du code de la santé publique " lorsqu'ils atteignent des résultats évalués à l'aide d'indicateurs relatifs à la pertinence et à l'efficience de leurs prescriptions de produits de santé, mesurés tous les ans par établissement. / Un arrêté des ministres chargés de la santé et de la sécurité sociale dresse la liste des indicateurs relatifs à la pertinence et à l'efficience des prescriptions et précise, pour chaque indicateur, les modalités de calcul du montant de la dotation par établissement ". Aux termes du III de l'article 66 de la loi du 22 décembre 2018 : " Les modalités de détermination de la dotation mentionnées à l'article L. 162-22-7-4 du code de la sécurité sociale peuvent se fonder sur l'analyse des prescriptions effectuées à compter du 1er janvier 2018 ".<br/>
<br/>
              9. Sur le fondement de ces dispositions, les ministres chargés de la santé et de la sécurité sociale peuvent, d'une part, autoriser des expérimentations à caractère national reposant sur des mesures incitatives pour les établissements de santé et, d'autre part, prévoir que ces mêmes établissements de santé peuvent bénéficier d'une dotation du fonds d'intervention régional, dans le but, dans l'un et l'autre cas, notamment, d'améliorer la pertinence et l'efficience de la prescription de médicaments. Ces mesures peuvent, en particulier, concerner la prescription de médicaments ayant, en tout ou partie, les mêmes indications thérapeutiques, alors même qu'ils n'appartiendraient pas au même groupe générique ou au même groupe biologique similaire, dès lors qu'elles visent à en améliorer la pertinence et l'efficience.<br/>
<br/>
              10. En revanche, les ministres chargés de la santé et de la sécurité sociale ne peuvent sans commettre d'erreur de droit, pour définir le champ de l'expérimentation qu'ils autorisent ou déterminer les indicateurs qui fondent l'évaluation des résultats des établissements de santé ouvrant le bénéfice d'une dotation du fonds d'intervention régional, inciter à la prescription de certains médicaments biologiques de préférence à d'autres en utilisant les notions de " médicaments biologiques similaires " ou de " médicaments biosimilaires " dans un sens différent de celui qui résulte des dispositions de l'article L. 5121-1 du code de la santé publique citées au point 5. <br/>
<br/>
              11. Par l'arrêté du 3 août 2018 relatif à l'expérimentation pour l'incitation à la prescription hospitalière de médicaments biologiques similaires délivrés en ville, puis par l'arrêté du 12 février 2019 ayant le même objet qui l'a abrogé et remplacé, les ministres chargés de la santé et de la sécurité sociale ont autorisé, du 1er octobre 2018 au 1er octobre 2021, une expérimentation " pour l'incitation à la prescription hospitalière de médicaments biologiques similaires " lorsqu'ils sont délivrés en ville, définie dans le cahier des charges annexé. Celui-ci précise que le nouveau mécanisme d'intéressement porte sur les groupes de médicaments pour lesquels des médicaments biosimilaires ont récemment été admis au remboursement, notamment le " groupe insuline glargine ", et que le niveau de la rémunération incitative dépend de la proportion de patients suivant un traitement biosimilaire par rapport aux traitements de référence correspondants. Toutefois, le cahier des charges annexé à l'arrêté du 3 août 2018 précise à son point 8 que le calcul de rémunération repose sur l'écart de prix existant entre un médicament biosimilaire et le ou les médicaments biologiques de référence " au sens de l'instruction " du 19 février 2018, laquelle inclut la spécialité Toujeo parmi les " médicaments référents " du " groupe insuline glargine ". De même, le cahier des charges annexé à l'arrêté du 12 février 2019 précise que ce calcul repose sur l'écart de prix existant entre un médicament biosimilaire et le ou les médicaments biologiques de référence " au sens de la présente expérimentation ", qui inclut Toujeo parmi les " médicaments référents " du " groupe insuline glargine ". Si le cahier des charges annexé à l'arrêté du 3 août 2018 comporte à son point 1 une note de bas de page et celui annexé à l'arrêté du 12 février 2019 une mention au point 1 selon laquelle les groupes de médicaments sélectionnés " sont comparables mais ne correspondent pas nécessairement aux groupes de médicaments biologiques similaires tels que définis par l'article L. 5121-1 " du code de la santé publique, ils ne précisent aucunement les conditions dans lesquelles la prescription d'une spécialité biologique similaire à Lantus devrait être regardée comme plus pertinente et plus efficiente que celle de Toujeo mais présentent les médicaments biosimilaires de Lantus comme étant également des médicaments biosimilaires de Toujeo. <br/>
<br/>
              12. Par les arrêtés des 19 mars et 29 mai 2019 relatifs à l'efficience et la pertinence de la prescription hospitalière de médicaments biologiques similaires délivrés en ville, les ministres chargés de la santé et de la sécurité sociale ont prévu, respectivement au titre de 2018 et de 2019, le versement, aux établissements de santé qui exercent les activités de médecine, de chirurgie, de gynécologie-obstétrique et d'odontologie et ont signé un contrat d'amélioration de la qualité et de l'efficience des soins, d'une dotation pour l'efficience et la pertinence de leur prescription, fixée sur la base d'indicateurs relatifs à la " prescription hospitalière de médicaments biologiques similaires délivrés en ville ", en particulier pour le groupe insuline glargine. Ils précisent que pour chaque groupe de médicaments concerné, sont définis les médicaments " biosimilaires " du groupe et les médicaments " référents " correspondants et que la dotation versée à l'établissement de santé dépend, pour chaque groupe considéré, du taux de recours aux médicaments biosimilaires et du volume de prescription de l'établissement. S'ils mentionnent que la liste des médicaments " référents " peut contenir des médicaments comparables au médicament biologique de référence du ou des médicaments biosimilaires considérés, ils font figurer Toujeo au nombre des médicaments " référents " du " groupe insuline glargine ", sans préciser aucunement les conditions dans lesquelles la prescription d'une spécialité biologique similaire à Lantus devrait être regardée comme plus pertinente et plus efficiente que celle de Toujeo, et présentent les médicaments biosimilaires de Lantus comme étant également des médicaments biosimilaires de Toujeo.<br/>
<br/>
              13. Il résulte de ce qui a été dit au point 10 que les ministres chargés de la santé et de la sécurité sociale ont commis une erreur de droit en qualifiant la spécialité Toujeo de médicament " référent " d'un groupe insuline glargine pour les besoins des mesures d'incitation des établissements de santé à la prescription de médicaments biologiques similaires délivrés en ville qu'ils adoptaient sur le fondement des articles L. 162-31-1 et L. 162-22-7-4 du code de la sécurité sociale. Par suite, et sans qu'il soit besoin d'examiner les autres moyens des requêtes, la société Sanofi Aventis France est fondée à demander l'annulation pour excès de pouvoir des arrêtés du 3 août 2018 et des 12 février, 19 mars et 29 mai 2019 en tant qu'ils s'appliquent à la spécialité pharmaceutique Toujeo.<br/>
<br/>
              14. La présente décision n'appelant aucune mesure d'exécution, les conclusions de la société Sanofi Aventis France à fin d'injonction ne peuvent qu'être rejetées.<br/>
<br/>
              15. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat une somme de 5 000 euros à verser à la société Sanofi Aventis France au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : Il n'y a pas lieu de statuer sur les conclusions de la requête n° 423958 de la société Sanofi Aventis France tendant à l'annulation de la décision implicite rejetant sa demande d'abrogation de l'instruction du 19 février 2018 relative à l'incitation à la prescription hospitalière de médicaments biologiques similaires lorsqu'ils sont délivrés en ville.<br/>
Article 2 : Les arrêtés de la ministre des solidarités et de la santé et du ministre de l'action et de comptes publics des 3 août 2018, 12 février 2019, 19 mars 2019 et 29 mai 2019 sont annulés en tant qu'ils s'appliquent à la spécialité Toujeo.<br/>
<br/>
Article 3 : L'Etat versera à la société Sanofi Aventis France une somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Le surplus des conclusions de la requête n° 423958 est rejeté.<br/>
Article 5 : La présente décision sera notifiée à la société Sanofi Aventis France et à la ministre des solidarités et de la santé.<br/>
Copie en sera adressée au ministre de l'action et des comptes publics, à la Haute Autorité de santé et à l'Agence nationale de sécurité du médicament et des produits de santé.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
