<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032659074</ID>
<ANCIEN_ID>JG_L_2014_12_000000378887</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/65/90/CETATEXT000032659074.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème SSJS, 23/12/2014, 378887, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-12-23</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>378887</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème SSJS</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP SPINOSI, SUREAU</AVOCATS>
<RAPPORTEUR>M. Timothée Paris</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Aurélie Bretonneau</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2014:378887.20141223</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête sommaire et un mémoire complémentaire enregistrés les 28 avril  et 20 mai 2014 au secrétariat du contentieux du Conseil d'Etat, les communes de Ghisonaccia, Ghisoni, Lugo-di-Nazza et Poggio-di-Nazza demandent au Conseil d'Etat : <br/>
              1°) d'annuler pour excès de pouvoir le décret n° 2014-255 du 26 février 2014 portant délimitation des cantons dans le département de la Haute-Corse ;<br/>
              2°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu :<br/>
              - les autres pièces du dossier ;<br/>
              - la Constitution ;<br/>
              - le code électoral ;<br/>
              - le code général des collectivités territoriales ;<br/>
              - la loi n° 86-825 du 11 juillet 1986 ;<br/>
              - la loi n° 2013-403 du 17 mai 2013 ;<br/>
              - le décret n° 2013-938 du 18 octobre 2013, modifié par le décret n° 2014-112 du 6 février 2014 ;<br/>
              - la décision du Conseil constitutionnel n° 2013-667 DC du 16 mai 2013 ;<br/>
              - le code de justice administrative.<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Timothée Paris, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Aurélie Bretonneau, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Spinosi, Sureau, avocat des communes de Ghisonaccia, de Ghisoni, de Lugo Di Nazza et de Poggio Di Nazza ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant que les communes de Ghisonaccia, Ghisoni, Lugo-di-Nazza et Poggio-di-Nazza demandent au Conseil d'Etat d'annuler pour excès de pouvoir le décret n° 2014-255 du 26 février 2014 portant délimitation des cantons dans le département de la Haute-Corse ; <br/>
<br/>
              2. Considérant qu'aux termes de l'article L. 191-1 du code électoral, résultant de la loi du 17 mai 2013 relative à l'élection des conseillers départementaux, des conseillers municipaux et des conseillers communautaires et modifiant le calendrier électoral : " Le nombre de cantons dans lesquels seront élus les conseillers départementaux est égal, pour chaque département, à la moitié du nombre de cantons existant au 1er janvier 2013, arrondi à l'unité impaire supérieure si ce nombre n'est pas entier impair. / Le nombre de cantons dans chaque département comptant plus de 500 000 habitants ne peut être inférieur à dix-sept. Il ne peut être inférieur à treize dans chaque département comptant entre 150 000 et 500 000 habitants " ; que ces dispositions impliquent qu'il soit procédé à une nouvelle délimitation de l'ensemble des circonscriptions cantonales, qui sera applicable à compter du prochain renouvellement général des conseils généraux fixé au mois de mars 2015 ; qu'aux termes de l'article L. 3113-2 du code général des collectivités territoriales, dans sa version issue de la loi du 17 mai 2013 applicable à la date du décret attaqué : " I. - Les modifications des limites territoriales des cantons, les créations et suppressions de cantons et le transfert du siège de leur chef-lieu sont décidés par décret en Conseil d'Etat après consultation du conseil départemental qui se prononce dans un délai de six semaines à compter de sa saisine (...) III. - La modification des limites territoriales des cantons effectuée en application du I est conforme aux règles suivantes : a) Le territoire de chaque canton est défini sur des bases essentiellement démographiques ; / b) Le territoire de chaque canton est continu ; / c) Est entièrement comprise dans le même canton toute commune de moins de 3 500 habitants ; / IV. - Il n'est apporté aux règles énoncées au III que des exceptions de portée limitée, spécialement justifiées, au cas par cas, par des considérations géographiques ou par d'autres impératifs d'intérêt général " ; <br/>
<br/>
              3. Considérant que le décret attaqué a, en application de l'article L. 3113-2 du code général des collectivités territoriales, procédé à une nouvelle délimitation des cantons du département de la Haute-Corse, compte tenu de l'exigence de réduction du nombre des cantons de ce département de trente à quinze qui résulte de l'application de l'article L. 191-1 du code électoral ;<br/>
<br/>
              Sur la légalité externe du décret :<br/>
<br/>
              En ce qui concerne l'avis du Conseil d'Etat :<br/>
<br/>
              4. Considérant qu'il ressort de la copie de la minute de la section de l'intérieur du Conseil d'Etat, telle qu'elle a été versée au dossier par le Premier ministre, que le texte publié ne contient pas de dispositions qui différeraient à la fois du projet initial du Gouvernement et du texte adopté par le Conseil d'Etat ; que les requérants ne sont, dès lors, pas fondés à soutenir que le décret attaqué aurait, pour ce motif, été pris selon une procédure irrégulière ; <br/>
<br/>
              En ce qui concerne la consultation du conseil général :<br/>
<br/>
              5. Considérant, en premier lieu, qu'il ressort des pièces du dossier qu'à l'appui de la demande d'avis adressée au conseil général de la Haute-Corse sur le projet de décret, celui-ci a été accompagné, notamment, d'une note décrivant la méthode retenue pour procéder à la nouvelle délimitation des cantons, d'un état statistique comparatif des populations par canton et d'une série de cartes ; que l'assemblée départementale, disposant d'informations suffisantes, a ainsi été mise à même d'émettre un avis sur la nouvelle délimitation des cantons prévue par le législateur et de faire des propositions spécifiques pour le département de la Haute-Corse ; que, par suite, les communes requérantes ne sont pas fondées à soutenir que la consultation du conseil général aurait été irrégulière ; <br/>
<br/>
              6. Considérant, en second lieu, que l'organisme dont une disposition législative ou réglementaire prévoit la consultation avant l'intervention d'un texte doit être mis à même d'exprimer son avis sur l'ensemble des questions soulevées par ce texte ; que, par suite, dans le cas où, après avoir recueilli son avis, l'autorité compétente pour prendre ce texte envisage d'apporter à son projet des modifications qui posent des questions nouvelles, elle doit le consulter à nouveau ; que si les communes requérantes soutiennent que le décret qu'elles attaquent ne serait pas conforme à celui qui a été soumis pour avis au conseil général de la Haute-Corse, il ne ressort pas des pièces du dossier que le décret adopté aurait comporté, par rapport au projet soumis au conseil général, des modifications soulevant des questions nouvelles imposant une nouvelle consultation ;<br/>
<br/>
              En ce qui concerne la consultation de l'Assemblée de Corse :<br/>
<br/>
              7. Considérant qu'aux termes du V de l'article L. 4422-16 du code général des collectivités territoriales : " L'Assemblée de Corse est consultée sur les projets et les propositions de loi ou de décret comportant des dispositions spécifiques à la Corse " ; que, par ces dispositions, le législateur n'a pas entendu rendre obligatoire la consultation de l'Assemblée de Corse sur tous les textes législatifs ou réglementaires dont le champ d'application territoriale couvre tout ou partie du territoire de la collectivité, mais seulement sur ceux d'entre eux qui, adaptés aux particularités de cette dernière ou ne concernant qu'elle, sont spécifiques à cette collectivité ; que le décret attaqué, qui se borne à appliquer sur le territoire du département de la Haute-Corse les règles de délimitation des circonscriptions cantonales résultant de la loi du 17 mai 2013, ne comporte aucune règle spécifique à la Corse qui aurait rendu nécessaire une consultation préalable de l'Assemblée de Corse au titre de l'article L. 4422-16 ; que le moyen tiré de la méconnaissance de cet article ne peut donc qu'être écarté ;<br/>
<br/>
              Sur la légalité interne du décret : <br/>
<br/>
              8. Considérant qu'il résulte des dispositions de l'article L. 3113-2 du code général des collectivités territoriales que le territoire de chaque canton doit être établi sur des bases essentiellement démographiques, qu'il doit être continu et que toute commune de moins de 3 500 habitants doit être entièrement comprise dans un même canton, seules des exceptions de portée limitée et spécialement justifiées pouvant être apportées à ces règles ;<br/>
<br/>
              En ce qui concerne les données démographiques retenues :<br/>
<br/>
              9. Considérant qu'il appartient au Gouvernement de retenir, pour procéder à un découpage électoral, les chiffres de population les plus récents auxquels il est susceptible de se référer en tenant compte de la date des prochaines échéances électorales ainsi que des exigences d'une bonne administration, au nombre desquelles figure notamment le respect des contraintes et délais de consultation inhérents au processus d'élaboration et d'adoption des nouvelles délimitations ; que, pour répondre à ces exigences, l'article 71 du décret du 18 octobre 2013, dans sa rédaction applicable en l'espèce issue de l'article 8 du décret n° 2014-112 du 6 février 2014 prévoit que : " (...) Pour la première délimitation générale des cantons opérée en application de l'article L. 3113-2 du code général des collectivités territoriales, dans sa rédaction résultant de l'article 46 de la loi n° 2013-403 du 17 mai 2013 relative à l'élection des conseillers départementaux, (...), le chiffre de la population municipale auquel il convient de se référer est celui authentifié par le décret n° 2012-1479 du 27 décembre 2012 authentifiant les chiffres des populations (...) " ;<br/>
<br/>
              10. Considérant qu'il est constant que les nouveaux cantons du département de la Haute-Corse ont été délimités sur la base des données authentifiées par le décret du 27 décembre 2012 ; que, par suite, le moyen tiré de ce que le décret attaqué est entaché d'erreur de droit pour avoir retenu des données qui ne correspondraient pas à la réalité démographique ne peut qu'être écarté ;<br/>
<br/>
              En ce qui concerne le principe d'égalité des citoyens devant le suffrage :<br/>
<br/>
              11. Considérant que les dispositions du III de l'article L. 3113-2 du code général des collectivités territoriales qui, afin de respecter le principe constitutionnel d'égalité des citoyens devant le suffrage, prévoient que le territoire de chaque canton est défini sur des bases essentiellement démographiques, n'imposent pas que, dans un même département, la répartition des sièges doive être proportionnelle à la population et permettent de regarder comme admissible un écart de l'ordre de plus ou moins 20 % par rapport à la moyenne de la population par canton au sein du département, notamment afin de respecter les exigences du b) et du c) du III de cet article, et à condition qu'un écart de cet ordre repose sur des considérations dénuées d'arbitraire ; qu'en vertu du IV du même article, des exceptions limitées peuvent être apportées au caractère essentiellement démographique de la délimitation d'un canton, lorsque des considérations géographiques, au nombre desquelles figurent, ainsi que l'a jugé le Conseil constitutionnel dans sa décision du 16 mai 2013, l'insularité, le relief, l'enclavement ou la superficie, ainsi que d'autres impératifs d'intérêt général, imposent de s'écarter de la ligne directrice qui limite à plus ou moins 20 % l'écart par rapport à la moyenne de la population par canton au sein du département ; <br/>
<br/>
              12. Considérant que l'écart de 22,79 % entre la population du canton n° 12 (Fiumorbo-Castello) et la moyenne de la population cantonale dans le département de la Haute-Corse dépasse les disparités démographiques admissibles sur le fondement du III de l'article L. 3113-2 du code général des collectivités territoriales, dès lors qu'il s'écarte de la ligne directrice rappelée au point précédent ; qu'il ressort des pièces du dossier que le Gouvernement a procédé à la délimitation de ce canton en se fondant, à titre principal, sur les nécessités résultant des contraintes géographiques du territoire pris dans ses limites administratives, tirées de ce que ce canton, dont la superficie ne peut être étendue vers le Nord sans déséquilibrer la répartition des cantons, est entièrement situé en zone de montagne et bordé à l'Ouest et au Sud par le département de la Corse-du-Sud et à l'Est par la mer Méditerranée ; que le décret attaqué, qui se fonde sur ces considérations dépourvues de caractère arbitraire, ne méconnaît pas l'obligation, énoncée au IV de l'article L. 3113-2 du code général des collectivités territoriales, de n'apporter au caractère essentiellement démographique de la délimitation des cantons opérée dans le département de la Haute-Corse que des exceptions de portée limitée spécialement justifiées par la prise en compte des contraintes géographiques de ce territoire ;<br/>
<br/>
              En ce qui concerne les autres critères de délimitation de l'ensemble des cantons :<br/>
<br/>
              13. Considérant que ni les dispositions de l'article L. 3113-2 du code général des collectivités territoriales, ni aucun autre texte non plus qu'aucun principe n'imposent au Gouvernement de prévoir que les limites des cantons, qui sont des circonscriptions électorales, coïncident avec les limites des cartes des établissements publics de coopération intercommunale, ou celles de la carte scolaire, ni de tenir compte de la proximité géographique des communes, de leurs liens historiques, économiques, sociaux, administratifs ou domaniaux ; que, par suite, alors même qu'elle seraient propriétaires indivis d'un terrain, les communes requérantes ne sont pas fondées à soutenir que le décret qu'elles attaquent est entaché d'erreur de droit ou d'erreur manifeste d'appréciation pour les avoir  réparties entre les cantons n°12 (Fiumorbo-Castello) et n°13 (Ghisonaccia) ; <br/>
<br/>
              14. Considérant qu'il résulte de tout ce qui précède que les communes requérantes ne sont pas fondées à demander l'annulation du décret qu'elles attaquent ; que, par suite, leurs conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative ne peuvent qu'être rejetées ;<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Les requêtes des communes de Ghisonaccia, Ghisoni, Lugo-di-Nazza et Poggio-di-Nazza sont rejetées.<br/>
Article 2 : La présente décision sera notifiée aux communes de Ghisonaccia, Ghisoni, Lugo-di-Nazza et Poggio-di-Nazza et au ministre de l'intérieur.<br/>
Copie en sera adressée au Premier ministre.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
