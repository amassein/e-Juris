<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042115681</ID>
<ANCIEN_ID>JG_L_2020_07_000000440700</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/11/56/CETATEXT000042115681.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème - 9ème chambres réunies, 10/07/2020, 440700, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-07-10</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>440700</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème - 9ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP SPINOSI, SUREAU</AVOCATS>
<RAPPORTEUR>Mme Myriam Benlolo Carabot</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Anne Iljic</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:440700.20200710</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A... B... a demandé au juge des référés du tribunal administratif de Melun, sur le fondement de l'article L. 521-1 du code de justice administrative, de suspendre l'exécution de la décision du 10 avril 2020 par laquelle la garde des sceaux, ministre de la justice, a prolongé son placement à l'isolement du 15 avril au 15 juillet 2020. Par une ordonnance n° 2003151 du 21 avril 2020, le juge des référés du tribunal administratif a rejeté sa demande.<br/>
<br/>
              Par un pourvoi et un mémoire enregistrés les 18 mai et 6 juillet 2020 au secrétariat du contentieux du Conseil d'Etat, M. B... demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) statuant en référé, de faire droit à sa demande ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
                  Vu les autres pièces du dossier ;<br/>
<br/>
                  Vu :<br/>
<br/>
                  - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
                  - le code de procédure pénale ;<br/>
                  - le code de justice administrative et l'ordonnance n° 2020-305 du 25 mars 2020 ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Myriam Benlolo Carabot, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Anne Iljic, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Spinosi, Sureau, avocat de M. A... B... ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis au juge des référés du tribunal administratif de Melun que, par une décision du 10 avril 2020, la garde des sceaux, ministre de la justice, a décidé de prolonger, à compter du 15 avril 2020 et jusqu'au 15 juillet 2020, le placement à l'isolement de M. B..., actuellement détenu au centre pénitentiaire de Meaux-Chauconin-Neufmontiers. M. B... a sollicité la suspension de l'exécution de cette décision sur le fondement de l'article L. 521-1 du code de justice administrative. Statuant par application de l'article L. 522-3 du code de justice administrative, le juge des référés a rejeté sa demande par une ordonnance du 21 avril 2020 contre laquelle M. B... se pourvoit en cassation. <br/>
<br/>
              2. La section française de l'Observatoire international des prisons justifie, eu égard à la nature et à l'objet du litige, d'un intérêt suffisant pour intervenir dans la présente instance au soutien du pourvoi. Son intervention est, par suite, recevable.<br/>
<br/>
              3. Aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision ". Aux termes de l'article L. 522-3 du même code : " Lorsque la demande ne présente pas un caractère d'urgence ou lorsqu'il apparaît manifeste, au vu de la demande, (...) qu'elle est mal fondée, le juge des référés peut la rejeter par une ordonnance motivée sans qu'il y ait lieu d'appliquer les deux premiers alinéas de l'article L. 522-1 ", lesquels prévoient que le juge des référés statue au terme d'une procédure contradictoire écrite ou orale et après une audience publique.<br/>
<br/>
              4. Aux termes du premier alinéa de l'article 726-1 du code de procédure pénale : " Toute personne détenue, sauf si elle est mineure, peut être placée par l'autorité administrative, pour une durée maximale de trois mois, à l'isolement par mesure de protection ou de sécurité soit à sa demande, soit d'office. Cette mesure ne peut être renouvelée pour la même durée qu'après un débat contradictoire, au cours duquel la personne concernée, qui peut être assistée de son avocat, présente ses observations orales ou écrites. L'isolement ne peut être prolongé au-delà d'un an qu'après avis de l'autorité judiciaire ".<br/>
<br/>
              5. Pour juger que le moyen tiré de la méconnaissance de l'article 3 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales n'était pas de nature à créer un doute sérieux sur la légalité de la décision du 10 avril 2020 prolongeant le placement à l'isolement de M. B..., alors qu'il relevait que ce dernier était maintenu à l'isolement depuis plus de trois ans, le juge des référés s'est uniquement fondé sur des impératifs de sécurité et sur le comportement de l'intéressé. En statuant ainsi pour apprécier l'existence alléguée de traitements inhumains ou dégradants, sans prendre en compte les effets de la mesure sur les conditions de la détention de M. B... et sur son état de santé physique et psychique, le juge des référés a commis une erreur de droit. <br/>
<br/>
              6. Si le juge des référés a, en outre, estimé que la condition d'urgence n'était en l'espèce pas remplie, il ressort des termes mêmes de son ordonnance que cet autre motif présente un caractère surabondant. Il résulte, par suite, de ce qui a été dit au point 5, sans qu'il soit besoin de se prononcer sur les autres moyens du pourvoi, que M. B... est fondé à demander l'annulation de l'ordonnance qu'il attaque.<br/>
<br/>
              7. Il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au titre de la procédure de référé engagée, en application de l'article L. 821-2 du code de justice administrative.<br/>
<br/>
              8. Pour demander la suspension de l'exécution de la décision du 10 avril 2020 qui a prolongé jusqu'au 15 juillet 2020 son maintien à l'isolement au centre pénitentiaire de Meaux-Chauconin-Neufmontiers, laquelle a pour effet de porter le temps passé à l'isolement à trois ans, cinq mois et neuf jours, M. B... soutient que cette décision est insuffisamment motivée, qu'elle est entachée d'erreur de droit en ce qu'elle est fondée sur des évaluations anciennes et que son caractère strictement nécessaire n'est pas justifié, qu'elle repose sur des faits matériellement inexacts et qu'elle est entachée d'une erreur d'appréciation. Toutefois, au vu de l'ensemble des circonstances de l'espèce, aucun de ces moyens n'est, en l'état de l'instruction, de nature à faire naître un doute sérieux quant à la légalité de la décision litigieuse.<br/>
<br/>
              9. M. B... soutient, en outre, que la décision contestée méconnaîtrait les stipulations de l'article 3 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales. Toutefois, au vu de l'avis médical du 23 mars 2020 n'émettant aucune contre-indication au maintien à l'isolement de M. B... et en l'absence d'éléments de nature à établir que cette mesure serait en l'espèce susceptible, eu égard aux conditions de détention de M. B... et à la gravité des effets de la mesure sur son état de santé physique et psychique, d'exposer celui-ci à des traitements inhumains et dégradants, alors qu'il ressort des pièces versées au dossier dans le cadre de l'instruction contradictoire devant le Conseil d'Etat qu'il a des contacts réguliers avec sa famille dans le cadre de parloirs individuels, qu'il bénéficie de la possibilité de faire du sport et qu'il fait seulement état d'un sentiment de solitude et d'angoisse, d'une mauvaise qualité de sommeil, d'une baisse de vision et de l'aggravation de rhumatismes, ce dernier moyen n'est pas davantage, en l'état de l'instruction, de nature à faire naître un doute sérieux sur la légalité de la décision litigieuse. <br/>
<br/>
              10. Il résulte de ce qui précède que M. B... n'est pas fondé à demander la suspension de la décision du 10 avril 2020 par laquelle la garde des sceaux, ministre de la justice, a prolongé son placement à l'isolement du 15 avril au 15 juillet 2020. Ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative ne peuvent, par suite, qu'être rejetées. <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
Article 1er : L'intervention de la section française de l'Observatoire international des prisons est admise.<br/>
<br/>
Article 2 : L'ordonnance du 21 avril 2020 du juge des référés du tribunal administratif de Melun est annulée.<br/>
<br/>
Article 3 : La demande présentée par M. B... devant le juge des référés du tribunal administratif de Melun et ses conclusions présentées devant le Conseil d'Etat au titre de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
<br/>
Article 4 : La présente décision sera notifiée à M. A... B..., au garde des sceaux, ministre de la justice et à la section française de l'Observatoire international des prisons.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
