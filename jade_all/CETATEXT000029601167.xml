<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029601167</ID>
<ANCIEN_ID>JG_L_2014_10_000000367504</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/60/11/CETATEXT000029601167.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème / 4ème SSR, 01/10/2014, 367504, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-10-01</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>367504</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème / 4ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GASCHIGNARD ; SCP POTIER DE LA VARDE, BUK LAMENT</AVOCATS>
<RAPPORTEUR>M. Jean-Dominique Langlais</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Fabienne Lambolez</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:367504.20141001</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 8 avril et 9 juillet 2013 au secrétariat du contentieux du Conseil d'Etat, présentés pour Mme A... B...demeurant... ; Mme B...demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler le jugement n° 1101782 du 5 février 2013 par lequel le tribunal administratif de Pau a rejeté sa requête tendant, d'une part, à l'annulation pour excès de pouvoir de la décision du 11 avril 2011 du directeur du centre hospitalier de Bigorre refusant de reconnaître l'imputabilité au service de son état dépressif ayant justifié l'octroi de congés pour maladie du 6 mars au 31 juillet 2006 ainsi que de la décision implicite de rejet née du silence gardé par l'administration sur son recours gracieux dirigé contre cette décision et, d'autre part, à ce qu'il soit enjoint au centre hospitalier de Bigorre de reconnaître l'imputabilité au service de ces arrêts de travail ; <br/>
<br/>
              2°) de mettre à la charge du centre hospitalier de Bigorre la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la loi n° 86-33 du 9 janvier 1986 ; <br/>
<br/>
              Vu le décret n° 2001-1375 du 31 décembre 2001 ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Dominique Langlais, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Fabienne Lambolez, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Gaschignard, avocat de Mme B...et à la SCP Potier de la Varde, Buk Lament, avocat du centre hospitalier de Bigorre ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'aux termes du deuxième alinéa du 2° de l'article 41 de la loi du 9 janvier 1986 portant dispositions statutaires relatives à la fonction publique hospitalière : "  (...) si la maladie provient (...) d'un accident survenu dans l'exercice ou à l'occasion de l'exercice de ses fonctions, le fonctionnaire conserve l'intégralité de son traitement jusqu'à ce qu'il soit en état de reprendre son service ou jusqu'à sa mise à la retraite. Il a droit, en outre, au remboursement des honoraires médicaux et des frais directement entraînés par la maladie ou l'accident (...) " ; <br/>
<br/>
              2. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que Mme B... a été recrutée par le centre hospitalier de Bigorre et nommée infirmière titulaire le 6 octobre 2004, puis cadre de santé stagiaire le 1er mars 2005 ; qu'elle a ensuite fait l'objet de diverses mesures défavorables ; qu'une décision de changement d'affectation, prise d'office à son égard le 8 février 2006, a été annulée par le juge de l'excès de pouvoir ; que la commission administrative paritaire locale a émis le 6 mars 2006 un avis défavorable à sa titularisation ; que Mme B...a présenté, à compter de cette date, un état dépressif qui a justifié l'octroi d'arrêts de travail du 6 mars au 31 juillet 2006 ; qu'elle a demandé que l'imputabilité au service de ces arrêts de travail soit reconnue ; que le directeur du centre hospitalier de Bigorre a rejeté sa demande par une première décision du 27 mars 2009, annulée par un jugement du tribunal administratif de Pau du 16 novembre 2010 ; qu'il a opposé un nouveau refus à sa demande le 11 avril 2011 ; que l'intéressée se pourvoit en cassation contre le jugement du 5 février 2013 par lequel le tribunal administratif a rejeté son recours pour excès de pouvoir dirigé contre cette décision ;  <br/>
<br/>
              3. Considérant que, pour obtenir le bénéfice de ces dispositions, Mme B...invoquait un avis du 29 mars 2011 de la commission de réforme reconnaissant l'imputabilité de son état à ses conditions de service ; qu'elle invoquait également plusieurs faits précis, de nature à établir, selon elle, qu'elle avait fait l'objet de vexations répétées, notamment des retraits injustifiés de missions et de projets, et que ses supérieurs s'étaient délibérément abstenus de lui apporter un soutien ; qu'elle se référait par ailleurs à des attestations médicales dont il ressortait qu'elle ne présentait pas d'antécédents dépressifs avant le 6 mars 2006, date de l'avis défavorable de la commission administrative paritaire locale, et que l'état dépressif qui s'était manifesté à compter de cette date pouvait être imputé à ces faits ; qu'en se bornant à énoncer, pour écarter cette argumentation, qu'il ne ressortait pas des pièces du dossier que le refus de titularisation du 6 mars 2006 ait constitué la cause exclusive ou même déterminante de son état, le tribunal administratif de Pau a, au regard de l'argumentation dont il était ainsi saisi, insuffisamment motivé son jugement ; que, par suite, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, il y a lieu d'en prononcer l'annulation ;  <br/>
<br/>
              4. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              Sur la fin de non-recevoir opposée par le centre hospitalier de Bigorre : <br/>
<br/>
              5. Considérant qu'il ressort des pièces du dossier que Mme B...a saisi le directeur du centre hospitalier de Bigorre d'un recours gracieux le 21 avril 2011, dans le délai de recours contentieux ouvert à l'encontre de la décision du 11 avril 2011 ; que le silence gardé sur ce recours gracieux a fait naître le 21 juin 2011 une décision implicite de rejet ; que Mme B...a saisi le tribunal administratif de Pau d'une demande tendant à l'annulation de ces décisions le 2 août 2011, soit avant l'expiration du délai de recours contentieux ; que, dès lors, le centre hospitalier de Bigorre n'est pas fondé à soutenir que sa demande serait tardive et, par suite, irrecevable ; <br/>
<br/>
              Sur la légalité de la décision attaquée :<br/>
<br/>
              6. Considérant qu'il ressort également des pièces du dossier qu'à la suite d'un conflit avec la directrice des soins de l'établissement, Mme B...a fait l'objet, de la part de ses supérieurs, de mesures qui l'ont conduite à exercer ses fonctions dans des conditions particulièrement difficiles ; qu'elle a, à plusieurs reprises, sollicité sans succès l'intervention du chef d'établissement ; que, dans ce contexte, l'annonce que la décision de ne pas la titulariser a pu affecter son équilibre personnel ; qu'un certificat médical produit par Mme B...fait état d'une absence d'antécédents et d'une " anxiété réactionnelle directement liée à des conflits professionnels et qui ne serait pas apparue sans ces derniers " ; que l'expertise établie à la demande de la commission de réforme conclut à l'imputabilité au service de ses arrêts de travail ; qu'il ne ressort pas des pièces du dossier que son état dépressif résulterait d'une cause étrangère au service ; que cet état doit, dans les circonstances de l'espèce, être regardé comme imputable au service ; que la décision du 11 avril 2011 par laquelle le directeur du centre hospitalier de Bigorre a refusé de reconnaître l'imputabilité au service des arrêts de travail de l'intéressée du 6 mars au 31 juillet 2006 ainsi que la décision implicite de rejet de son recours gracieux doivent, par suite, être annulées ; <br/>
<br/>
              Sur les conclusions à fin d'injonction : <br/>
<br/>
              7. Considérant qu'aux termes de l'article L. 911-1 du code de justice administrative : " Lorsque sa décision implique nécessairement qu'une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public prenne une mesure d'exécution dans un sens déterminé, la juridiction, saisie de conclusions en ce sens, prescrit, par la même décision, cette mesure assortie, le cas échéant, d'un délai d'exécution " ; que l'annulation des décisions attaquées implique nécessairement que le centre hospitalier de Bigorre reconnaisse l'imputabilité au service de l'état dépressif ayant justifié les arrêts de travail de Mme B...du 6 mars au 31 juillet 2006 ; qu'il y a, dès lors, lieu d'enjoindre au directeur de l'établissement de procéder à cette reconnaissance dans un délai d'un mois à compter de la notification de la présente décision ; qu'il n'y a pas lieu d'assortir cette injonction d'une astreinte ; <br/>
<br/>
              Sur les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative : <br/>
<br/>
              8. Considérant que ces dispositions font obstacle à ce qu'une somme soit mise à ce titre à la charge de MmeB..., qui n'est pas, dans la présente instance, la partie perdante ; qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge du centre hospitalier de Bigorre la somme de 4 000 euros, à verser à Mme B... au titre des frais engagés par elle devant le tribunal administratif et le Conseil d'Etat ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du 5 février 2013 du tribunal administratif de Pau est annulé. <br/>
<br/>
Article 2 : La décision du 11 avril 2011 du directeur du centre hospitalier de Bigorre refusant de reconnaître l'imputabilité au service des arrêts de travail de Mme B...du 6 mars au 31 juillet 2006 ainsi que la décision implicite de rejet née du silence gardé par l'administration sur son recours gracieux sont annulées.<br/>
<br/>
Article 3 : Il est enjoint au directeur du centre hospitalier de Bigorre de prendre une décision reconnaissant l'imputabilité au service de l'état dépressif ayant justifié les arrêts de travail de Mme B...du 6 mars au 31 juillet 2006 dans un délai d'un mois à compter de la notification de la présente décision.  <br/>
<br/>
Article 4 : Le centre hospitalier de Bigorre versera à Mme B...la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
Article 5 : Les conclusions présentées par le centre hospitalier de Bigorre au titre de l'article L. 761-1 du code de justice administrative devant le tribunal administratif de Pau sont rejetées.<br/>
<br/>
Article 6 : La présente décision sera notifiée à Mme A... B...et au centre hospitalier de Bigorre.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
