<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027410905</ID>
<ANCIEN_ID>JG_L_2013_05_000000346795</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/41/09/CETATEXT000027410905.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  3ème sous-section jugeant seule, 15/05/2013, 346795, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-05-15</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>346795</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 3ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BARADUC, DUHAMEL</AVOCATS>
<RAPPORTEUR>M. Christian Fournier</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emmanuelle Cortot-Boucher</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2013:346795.20130515</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
<br/>
              Vu le pourvoi, enregistré le 17 février 2011 au secrétariat du contentieux du Conseil d'Etat, présenté par le ministre du budget, des comptes publics, de la fonction publique et de la réforme de l'Etat ; le ministre demande au Conseil d'État d'annuler l'arrêt n° 09PA00673 du 16 décembre 2010 par lequel la cour administrative d'appel de Paris, faisant droit à l'appel de M. B...A..., a annulé le jugement n° 0213395/2 du 15 décembre 2008 du tribunal administratif de Paris rejetant la demande de l'intéressé tendant à la décharge des cotisations supplémentaires d'impôt sur le revenu et de contributions sociales auxquelles il a été assujetti au titre de l'année 1997, et a prononcé la décharge de ces suppléments d'impôt ; <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 25 avril 2013, présentée pour M.A... ;<br/>
<br/>
              Vu le code civil ;<br/>
<br/>
              Vu le code général des impôts et le livre des procédures fiscales ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Christian Fournier, Maître des Requêtes,  <br/>
<br/>
              - les conclusions de Mme Emmanuelle Cortot-Boucher, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée avant et après les conclusions à la SCP Baraduc, Duhamel, avocat de M. A...;<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, par acte sous seing privé du 24 janvier 1997, M. A...a cédé à la société Cap Gemini les droits sociaux qu'il détenait dans la société Partnership for International Consulting (PIC) moyennant un prix de 1 432 francs par action, payable à la signature de l'acte de vente, auquel s'ajoutaient quatre compléments de prix payables respectivement le 1er juillet 1997, le 1er juillet 1998, le 1er juillet 1999 et le 31 mars 2000 ; qu'à l'issue de la vérification de comptabilité de M.A..., l'administration a réintégré au montant de la plus-value, correspondant au prix payé en 1997, que le contribuable avait déclarée au titre de cette même année, les compléments de prix payables en 1998, 1999 et 2000 ; que, par jugement du 15 décembre 2008, le tribunal administratif de Paris a rejeté la demande de M. A... tendant à la décharge des cotisations supplémentaires d'impôt sur le revenu et de contributions sociales auxquelles il a été assujetti au titre de l'année 1997 en conséquence de ce redressement ; que le ministre du budget, des comptes publics, de la fonction publique et de la réforme de l'Etat se pourvoit en cassation contre l'arrêt du 16 décembre 2010 par lequel la cour administrative d'appel de Paris, sur appel de M.A..., a annulé ce jugement et a prononcé la décharge de ces suppléments d'impôt ; <br/>
<br/>
              2. Considérant qu'aux termes du premier alinéa du I de l'article 92 B du code général des impôts, applicable à l'année d'imposition en litige : " I. Sont considérés comme des bénéfices non commerciaux, les gains nets retirés des cessions à titre onéreux (...) de valeurs mobilières (...), lorsque le montant de ces cessions excède, par foyer fiscal, 150.000 F par an " ; qu'aux termes du 2 de l'article 200 A du même code, dans sa rédaction applicable au litige : " 2. Les gains nets obtenus dans les conditions prévues aux articles 92 B et 92 F sont imposés au taux forfaitaire de 16 % " ; qu'aux termes de l'article 1583 du code civil, la vente " est parfaite entre les parties, et la propriété est acquise de droit à l'acheteur à l'égard du vendeur, dès qu'on est convenu de la chose et du prix, quoique la chose n'ait pas encore été livrée ni le prix payé " ; qu'aux termes de l'article 1591 du même code : " le prix de la vente doit être déterminé et désigné par les parties " ; qu'il résulte de ces dispositions que la date à laquelle la cession des titres d'une société doit être regardée comme réalisée est celle à laquelle s'opère entre les parties, indépendamment des modalités de paiement, le transfert de propriété ; que ce transfert a lieu, sauf stipulations contractuelles contraires, à la date où un accord intervient sur la chose et le prix ; que la plus-value éventuellement constatée lors de la cession des titres est imposable entre les mains du cédant au titre de l'année au cours de laquelle l'opération est intervenue ; que, lorsque l'acte de vente prévoit le versement au cédant d'un complément de prix, tout en subordonnant le paiement à la réalisation d'une condition définie contractuellement, la plus-value de cession constatée au titre de ce complément de prix est imposable entre les mains du cédant au titre de l'année civile au cours de laquelle les titres ont été cédés, indépendamment de son versement effectif, dès lors que le complément de prix est déterminé ou déterminable, qu'il ne dépend pas de la volonté de l'une des parties ou de la réalisation d'accords ultérieurs et qu'il ne ressort pas des stipulations de l'acte de vente que les parties auraient eu la commune intention de subordonner le transfert de la propriété des titres à la réalisation de la condition mise au versement du complément de prix ;<br/>
<br/>
              3. Considérant, dès lors, qu'en jugeant, après avoir relevé que le versement des deuxième, troisième et quatrième compléments de prix stipulés dans l'acte de vente du 24 janvier 1997, était subordonné, selon des modalités définies contractuellement, à la présence du cédant en qualité de salarié au sein du Groupe Bossard, que ces compléments de prix ne pouvaient être pris en compte pour la détermination de la plus-value imposable au titre de l'année 1997 au motif que leur versement effectif dépendait de conditions dont la réalisation ne pouvait être constatée avant le 31 décembre 1997 et, dans certaines hypothèses, ne dépendaient pas de la volonté des parties au contrat, la cour, qui n'a examiné que les modalités de paiement des compléments de prix sans rechercher, alors qu'il ressort de l'arrêt attaqué que leurs montants étaient complètement déterminés dans l'acte de vente et que la clause prise en compte n'était suspensive que de leur paiement, si les parties avaient entendu subordonner le transfert de la propriété des titres à la réalisation d'une condition suspensive affectant ce transfert lui-même, a entaché son arrêt d'erreur de droit ; que, par suite, le ministre est fondé à en demander l'annulation ;<br/>
<br/>
              4. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Paris du 16 décembre 2010 est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Paris.<br/>
Article 3 : Les conclusions de M. A...présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée au ministre de l'économie et des finances et à M. B... A....<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
