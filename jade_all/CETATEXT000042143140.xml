<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042143140</ID>
<ANCIEN_ID>JG_L_2020_07_000000437336</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/14/31/CETATEXT000042143140.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème chambre, 22/07/2020, 437336, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-07-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>437336</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Paul Bernard</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Sophie Roussel</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2020:437336.20200722</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, enregistrée le 3 janvier 2020 au secrétariat du contentieux du Conseil d'Etat, M. C... A... demande au Conseil d'Etat d'annuler pour excès de pouvoir le décret du 13 septembre 2019 rapportant le décret du 21 octobre 2010 lui ayant accordé la nationalité française.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ; <br/>
              - le traité sur le fonctionnement de l'Union européenne ;<br/>
              - le code civil ;<br/>
              - le décret n° 93-1362 du 30 décembre 1993 ;<br/>
              - le code de justice administrative et l'ordonnance n° 2020-305 du 25 mars 2020 ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Paul Bernard, maître des requêtes, <br/>
<br/>
              - les conclusions de Mme Sophie Roussel, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1.	Aux termes de l'article 27-2 du code civil : " Les décrets portant acquisition, naturalisation ou réintégration peuvent être rapportés sur avis conforme du Conseil d'Etat dans le délai de deux ans à compter de leur publication au Journal officiel si le requérant ne satisfait pas aux conditions légales ; si la décision a été obtenue par mensonge ou fraude, ces décrets peuvent être rapportés dans le délai de deux ans à partir de la découverte de la fraude ". Aux termes de l'article 63 du décret du 30 décembre 1993 relatif aux déclarations de nationalité, aux décisions de naturalisation, de réintégration, de perte, de déchéance et de retrait de la nationalité française : " Les décrets portant perte ou déchéance de la nationalité française et les décrets rapportant un décret de naturalisation ou de réintégration dans la nationalité française sont publiés au Journal officiel de la République française. Ils prennent effet à la date de leur signature (...) ".<br/>
<br/>
              2.	Il ressort des pièces du dossier que M. A..., ressortissant comorien, a déposé une demande de naturalisation le 13 juin 2008, dans laquelle il indiquait être célibataire et père de deux enfants mineurs et s'est engagé sur l'honneur à signaler tout changement dans sa situation personnelle et familiale. Au vu de ses déclarations, l'intéressé a été naturalisé par décret du 21 octobre 2010. Toutefois, par bordereau reçu le 14 septembre 2017, le ministre de l'Europe et des affaires étrangères a informé le ministre chargé des naturalisations que M. A... avait épousé aux Comores, le 20 décembre 2009, Mme B..., et qu'il était en outre père de quatre autres enfants, nés en 1998, 1999 et 2009 à Dzoidjou, aux Comores. Par décret du 13 septembre 2019, le Premier ministre a rapporté le décret du 21 octobre 2010 lui accordant la nationalité française, au motif qu'il avait été pris au vu d'informations mensongères délivrées par l'intéressé quant à sa situation familiale. M. A... demande l'annulation pour excès de pouvoir de ce décret.  <br/>
<br/>
              3.	En premier lieu, le délai de deux ans imparti par l'article 27-2 du code civil pour rapporter le décret de M. A... commence à courir à la date à laquelle la réalité de la situation familiale de l'intéressé est portée à la connaissance du ministre chargé des naturalisations. Il ressort des pièces du dossier que les services du ministre chargé des naturalisations n'ont été informés de la réalité de la situation familiale du requérant que le 14 septembre 2017, date à laquelle ils ont reçu les documents relatifs au mariage de l'intéressé et à ses autres enfants transmis par bordereau du ministre de l'Europe et des affaires étrangères. Dans ces conditions, le décret attaqué, qui a été signé le 13 septembre 2019, a été pris avant l'expiration du délai de deux ans prévu par les dispositions de l'article 27-2 du code civil. <br/>
<br/>
              4.	En deuxième lieu, l'article 21-16 du code civil dispose que : " Nul ne peut être naturalisé s'il n'a en France sa résidence au moment de la signature du décret de naturalisation ". Il résulte de ces dispositions que la demande de naturalisation n'est pas recevable lorsque l'intéressé n'a pas fixé en France de manière durable le centre de ses intérêts. Pour apprécier si cette condition est remplie, l'autorité administrative peut notamment prendre en compte, sous le contrôle du juge de l'excès de pouvoir, la situation familiale en France de l'intéressé à la date du décret lui accordant la nationalité française. Par suite, alors même qu'il remplirait les autres conditions requises à l'obtention de la nationalité française, la circonstance que l'intéressé soit marié avec une ressortissante comorienne et père de quatre autres enfants, était de nature à modifier l'appréciation qui a été portée par l'autorité administrative sur la fixation du centre de ses intérêts.<br/>
<br/>
              5.	Il ressort des pièces du dossier que M. A... a contracté mariage le 20 décembre 2009 aux Comores avec une ressortissante comorienne résidant habituellement à l'étranger et que quatre autres enfants étaient nés avant cette union. Ce mariage a constitué un changement de sa situation familiale que l'intéressé aurait dû porter à la connaissance des services instruisant sa demande de naturalisation, comme il s'y était engagé en déposant sa demande de naturalisation, ce qu'il n'a pas fait avant que lui soit accordée la nationalité française. <br/>
<br/>
              6.	M. A... soutient que son illettrisme ne lui a pas permis de comprendre le sens et la portée des renseignements qui lui étaient demandés par l'administration. Toutefois, l'intéressé, qui maîtrise la langue française, ainsi qu'il ressort du procès-verbal d'assimilation du 28 mai 2009, ne pouvait se méprendre sur le sens de la déclaration sur l'honneur qu'il a signée le 13 juin 2008 en déposant sa demande et par laquelle il certifiait les actes et complétait les indications données sur sa situation personnelle et familiale. En outre, M. A... n'a pas davantage informé les services chargés de sa demande lors de son entretien d'assimilation, où il s'est présenté en personne, de la réalité de sa situation familiale lorsque lui ont été demandés les liens qui le rattachaient encore à son pays d'origine. Dans ces conditions, M. A... doit être regardé comme ayant volontairement dissimulé sa situation familiale. Par suite, en rapportant sa naturalisation, dans le délai de deux ans à compter de la découverte de la fraude, le ministre de l'intérieur n'a pas méconnu les dispositions de l'article 27-2 du code civil.<br/>
<br/>
              7.	En troisième lieu, la définition des conditions et de la perte de la nationalité relève de la compétence de chaque Etat membre de l'Union européenne. Toutefois, dans la mesure où la perte de nationalité d'un Etat membre a pour conséquence la perte du statut de citoyen de l'Union, la perte de la nationalité d'un Etat membre doit, pour être conforme au droit de l'Union, répondre à des motifs d'intérêt général et être proportionnée à la gravité des faits qui la fondent, au délai écoulé depuis l'acquisition de la nationalité et à la possibilité pour l'intéressé de recouvrer une autre nationalité. Il résulte des dispositions de l'article 27-2 du code civil, qui s'appliquent également au cas de retrait pour un enfant mineur de l'effet collectif attaché à l'acquisition de la nationalité française par un de ses parents, qu'un décret ayant conféré la nationalité française peut être rapporté dans un délai de deux ans à compter de la découverte de la fraude au motif que l'intéressé a obtenu la nationalité française par mensonge ou fraude. Ces dispositions, qui ne sont pas incompatibles avec le droit de l'Union européenne, permettaient en l'espèce, eu égard à la date à laquelle il est intervenu et aux motifs qui le fondent, de rapporter légalement le décret accordant à M. A... et à ses enfants mineurs la nationalité française. Si M. A..., qui ne peut utilement invoquer la Déclaration universelle des droits de l'homme et du citoyen, la convention sur la réduction des cas d'apatridie, signée à New York le 30 août 1961, ainsi que la convention européenne sur la nationalité du 6 novembre 1997 soutient, en invoquant la loi comorienne, qu'il aurait perdu la nationalité comorienne en conséquence du décret du 21 octobre 2010 lui ayant conféré la nationalité française, il n'apporte aucun élément de nature à établir qu'il aurait effectivement perdu sa nationalité d'origine ou ne pourrait la recouvrer. Par suite, le Premier ministre a pu légalement prendre le décret attaqué en faisant application en l'espèce des dispositions de l'article 27-2 du code civil.<br/>
<br/>
              8.	En dernier lieu, si un décret qui rapporte un décret ayant conféré la nationalité française est, par lui-même, dépourvu d'effet sur la présence sur le territoire français de celui qu'il vise, comme sur ses liens avec les membres de sa famille, et n'affecte pas, dès lors, le droit au respect de sa vie familiale, il affecte néanmoins un élément constitutif de l'identité de la personne concernée et est ainsi susceptible de porter atteinte au droit au respect de sa vie privée. En l'espèce, toutefois, eu égard à la date à laquelle il est intervenu, aux motifs qui le fondent et à la situation de M. A... et de ses enfants, le décret attaqué ne peut être regardé comme ayant porté une atteinte disproportionnée au droit au respect de leur vie privée.<br/>
<br/>
              9.	Il résulte de ce qui précède que M. A... n'est pas fondé à demander l'annulation pour excès de pouvoir du décret du décret du 13 septembre 2019 par lequel le Premier ministre a rapporté le décret du 21 octobre 2010 qui lui avait accordé la nationalité française. <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : La requête de M. A... est rejetée.<br/>
<br/>
Article 2 : La présente décision sera notifiée à M. C... A... et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
