<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039426804</ID>
<ANCIEN_ID>JG_L_2019_11_000000432996</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/42/68/CETATEXT000039426804.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème - 2ème chambres réunies, 27/11/2019, 432996, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-11-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>432996</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème - 2ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FOUSSARD, FROGER ; SCP COUTARD, MUNIER-APAIRE</AVOCATS>
<RAPPORTEUR>M. François Lelièvre</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Gilles Pellissier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:432996.20191127</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société Pompes Funèbres de l'Avesnois et la société La Compagnie des crématoriums ont demandé, sur le fondement de l'article L. 551-1 du code de justice administrative, au juge des référés du tribunal administratif de Lille d'annuler la procédure de passation de la convention de concession de service public portant sur la mise aux normes et l'exploitation du crématorium communal lancée par la commune d'Hautmont, ainsi que l'exécution de toute décision qui s'y rapporte et d'enjoindre à la commune, si elle entendait conclure une concession ayant le même objet, de reprendre la procédure de passation en se conformant à ses obligations de publicité et de mise en concurrence. <br/>
<br/>
              Par une ordonnance n° 1905164 du 12 juillet 2019, le juge des référés du tribunal administratif de Lille a annulé la convention de passation et a enjoint à la commune d'Hautmont de reprendre, le cas échéant, la procédure de passation en se conformant à ses obligations de publicité et de mise en concurrence.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire, et un mémoire en réplique, enregistrés les 26 juillet, 9 août et 1er octobre 2019 au secrétariat du contentieux du Conseil d'Etat, la commune d'Hautmont demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) réglant l'affaire au titre de la procédure de référé, de rejeter la demande des sociétés Pompes Funèbres de l'Avesnois et La Compagnie des crématoriums ;<br/>
<br/>
              3°) de mettre à la charge de ces sociétés la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code général des collectivités territoriales ;<br/>
              - l'ordonnance n° 2016-65 du 29 janvier 2016 ;<br/>
              - le décret n° 2016-86 du 1er février 2016 ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. François Lelièvre, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Gilles Pellissier, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Foussard, Froger, avocat de la commune d'Hautmont et à la SCP Coutard, Munier-Apaire, avocat de la société Pompes Funèbres de l'Avesnois et de la société La Compagnie des crématoriums ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 19 novembre 2019, présentée par la société Pompes Funèbres de l'Avesnois et la société La Compagnie des crématoriums ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 551-1 du code de justice administrative : " Le président du tribunal administratif, ou le magistrat qu'il délègue, peut être saisi en cas de manquement aux obligations de publicité et de mise en concurrence auxquelles est soumise la passation par les pouvoirs adjudicateurs de contrats administratifs ayant pour objet l'exécution de travaux, la livraison de fournitures ou la prestation de services, avec une contrepartie économique constituée par un prix ou un droit d'exploitation, la délégation d'un service public ou la sélection d'un actionnaire opérateur économique d'une société d'économie mixte à opération unique (...) / Le juge est saisi avant la conclusion du contrat ".<br/>
<br/>
              2. Il ressort des pièces du dossier soumis au juge des référés du tribunal administratif de Lille que, par une délibération du 6 février 2019, le conseil municipal d'Hautmont a approuvé le principe du recours à une délégation de service public pour l'exploitation sur une durée de 15 ans du crématorium communal, jusque-là exploité par la SARL des Etablissements Frères sur le fondement d'une convention d'exploitation d'un complexe funéraire conclue le 23 novembre 1989. Le 25 mars 2019, un avis d'appel public à la concurrence a été publié au Bulletin officiel des annonces des marchés publics (BOAMP) en vue de conclure un contrat de concession de service public portant sur la mise aux normes et l'exploitation du crématorium communal. Saisi, sur le fondement de l'article L. 551-1 du code de justice, par la société Pompes funèbres de l'Avesnois et la société La Compagnie des crématoriums, le juge des référés du tribunal administratif de Lille a, par une ordonnance du 12 juillet 2019 contre laquelle la commune d'Hautmont se pourvoit en cassation, annulé la procédure de passation de la convention de concession de service public du crématorium au motif que le délai supplémentaire de neuf jours laissé aux candidat, afin de tenir compte d'une modification apportée par la commune au dossier de la consultation, pour déposer leur dossier de candidature et leur offre était insuffisant et a enjoint à la commune, si elle entendait conclure une concession ayant le même objet, de reprendre la procédure de passation en se conformant aux obligations de publicité et de mise en concurrence.<br/>
<br/>
              3. En vertu des dispositions de l'article L. 551-10 du code de justice administrative, les personnes habilitées à engager le recours prévu à l'article L. 551-1 en cas de manquement du pouvoir adjudicateur à ses obligations de publicité et de mise en concurrence sont celles qui ont un intérêt à conclure le contrat et qui sont susceptibles d'être lésées par le manquement invoqué. Il appartient dès lors au juge du référé précontractuel de rechercher si l'entreprise qui le saisit se prévaut de manquements qui, eu égard à leur portée et au stade de la procédure auquel ils se rapportent, sont susceptibles de l'avoir lésée ou risquent de la léser, fût-ce de façon indirecte en avantageant une entreprise concurrente. La commune d'Hautmont soutenait devant le juge des référés qu'aucun des manquements invoqués par la société Pompes Funèbres de l'Avesnois et la société La Compagnie des crématoriums n'était susceptible de les avoir lésées dès lors qu'elles n'avaient pas fait acte de candidature et que si elles l'avaient fait, leurs candidatures n'auraient pu qu'être écartées, s'agissant de la société Les Pompes Funèbres de l'Avesnois, parce qu'elle ne disposait pas des capacités techniques et financières pour assurer la mise aux normes et l'exploitation du crématorium et, s'agissant de la société La Compagnie des crématoriums, parce qu'elle ne disposait pas de l'habilitation prévue par l'article L. 2223-23 du code général des collectivités territoriales. Le juge des référés a écarté cette argumentation comme inopérante au motif que les sociétés avaient été dissuadées de présenter leur candidature en raison du délai supplémentaire insuffisant laissé aux participants à la consultation pour compléter leur offre à l'issue de la modification apportée par la commune aux conditions de la consultation. En se prononçant ainsi, le juge des référés s'est abstenu de rechercher si, comme le soutenait la commune, les sociétés ne disposaient manifestement pas des capacités techniques et financières suffisantes ou des pièces nécessaires pour constituer un dossier conforme aux exigences du règlement de consultation, et si, de ce fait, les manquements dénoncés n'étaient pas insusceptibles de les avoir lésées. Il a, ce faisant, commis une erreur de droit. Par suite, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, la commune d'Hautmont est fondée à demander l'annulation de l'ordonnance qu'elle attaque.<br/>
<br/>
              4. Dans les circonstances de l'espèce, il y a lieu, en application des dispositions de l'article L. 821-2 du code de justice administrative, de régler l'affaire au titre de la procédure de référé engagée.<br/>
<br/>
              5. En premier lieu, aux termes de l'article 4 du décret du 1er février 2016 relatif aux contrats de concession : " I. - Les documents de la consultation sont constitués de l'ensemble des documents fournis par l'autorité concédante ou auxquels elle se réfère, pour définir l'objet, les spécifications techniques et fonctionnelles, les conditions de passation et d'exécution du contrat de concession, ainsi que le délai de remise des candidatures ou des offres et, s'il y a lieu, les conditions de tarification du service rendu à l'usager. Ils comprennent notamment l'avis de concession, le cahier des charges de la concession et, le cas échéant, l'invitation à présenter une offre. / Toute modification des documents de la consultation est communiquée à l'ensemble des opérateurs économiques, aux candidats admis à présenter une offre ou à tous les soumissionnaires, dans des conditions garantissant leur égalité et leur permettant de disposer d'un délai suffisant pour remettre leurs candidatures ou leurs offres. / II. L'autorité concédante communique, au plus tard six jours avant la date limite fixée pour la réception des candidatures ou des offres, les renseignements complémentaires sur les documents de la consultation sollicités en temps utile par les candidats ou soumissionnaires ".<br/>
<br/>
              6. Il ressort des pièces du dossier qu'à la suite de la visite du site qui s'est déroulée le 15 avril 2019, la société La Compagnie des crématoriums a adressé une liste de questions à la commune dont la dernière portait sur les modalités de déplacement des cercueils vers la partie technique du crématorium, qui devaient, d'après les informations alors fournies par la commune, suivre un chemin situé à l'extérieur des bâtiments sur environ 60 mètres. La commune a, d'une part, répondu aux premières questions dès le 19 avril en produisant notamment les documents financiers et techniques sollicités et, d'autre part, réservé sa réponse à la dernière question dans l'attente d'une réponse de la préfecture sur la conformité du dispositif de déplacement des cercueils envisagé aux dispositions de l'article D. 2223-103 du code général des collectivités territoriales, qui prévoient que les pièces de la partie technique d'un crématorium communiquent entre elles pour permettre la circulation du personnel hors de la vue du public et que l'accès des cercueils au crématorium doit s'effectuer, en position horizontale, par la partie technique. Bien que les modalités de transit des cercueils initialement envisagées aient été validées par les services préfectoraux, la commune a choisi de modifier le circuit d'acheminement des cercueils vers la partie technique pour le rendre plus court et faciliter l'activité du délégataire, grâce à la création d'une servitude de passage accordée par le propriétaire du terrain au complexe funéraire. La commune a porté ces éléments le 20 mai 2019 à la connaissance de l'ensemble des participants, en repoussant par ailleurs au 29 mai la date limite de dépôt des candidatures et des offres. <br/>
<br/>
              7. La modification ainsi apportée par la commune au dossier de consultation, qui a porté uniquement sur les modalités de cheminement des cercueils au sein de l'établissement, ne peut être regardée comme une modification substantielle des conditions de consultation. Dans ces conditions, la commune, en prolongeant de neuf jours le délai de remise des offres, a laissé un délai suffisant, compte tenu de la nature et de la portée de cette modification d'ordre matériel, pour permettre aux participants d'en prendre connaissance et d'adapter leur offre. Par suite les sociétés requérantes ne sont pas fondées à soutenir que la commune aurait méconnu les dispositions de l'article 4 du décret du 1er février 2016 en ne prolongeant pas suffisamment le délai de remise des offres.<br/>
<br/>
              8. En deuxième lieu, il ne résulte pas de l'instruction que les pièces relatives aux données techniques et financières figurant au dossier de consultation, complétées par les documents fournis en réponse aux questions posées par les sociétés ayant participé à la consultation et en particulier par la société La Compagnie des crématoriums, auraient été insuffisantes pour permettre aux candidats de formuler leur offre dans des conditions conformes aux principes de libre accès à la commande publique, de transparence et d'égalité de traitement. Par suite, les sociétés requérantes ne sont pas fondées à soutenir que les informations données aux candidats étaient insuffisantes pour leur permettre d'élaborer une offre satisfaisante.<br/>
<br/>
<br/>
              9. En dernier lieu, s'il est soutenu que la commune aurait imposé des conditions d'exécution illégales en ce qui concerne le cheminement des cercueils, la propriété des terrains et la détermination des biens de retour en fin de concession et, ce faisant, favorisé le délégataire sortant, les sociétés requérantes se bornent sur ce point à de simples allégations insusceptibles d'établir les manquements qu'elles invoquent.<br/>
<br/>
              10. Il résulte de tout ce qui précède, sans qu'il soit besoin de se prononcer sur la fin de non recevoir opposée par la commune et tirée du défaut d'intérêt pour agir des sociétés, que la demande présentée par les sociétés Pompes Funèbres de l'Avesnois et La Compagnie des crématoriums ne peut qu'être rejetée.<br/>
<br/>
              11. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge des sociétés Pompes Funèbres de l'Avesnois et La Compagnie des crématoriums une somme globale de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. Ces dispositions font, en revanche, obstacle à ce qu'une somme soit mise à la charge de la commune d'Hautmont qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'ordonnance du 12 juillet 2019 du juge des référés du tribunal administratif de Lille est annulée.<br/>
Article 2 : La demande des sociétés Pompes Funèbres de l'Avesnois et La Compagnie des crématoriums et leurs conclusions présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : Les sociétés Pompes Funèbres de l'Avesnois et La Compagnie des crématoriums verseront à la commune d'Hautmont une somme globale de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à la commune d'Hautmont, à la société Pompes funèbres de l'Avesnois, à la société La Compagnie des crématoriums et à la SARL Etablissements Frères.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
