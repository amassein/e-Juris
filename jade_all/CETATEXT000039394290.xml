<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039394290</ID>
<ANCIEN_ID>JG_L_2019_11_000000422222</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/39/42/CETATEXT000039394290.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème chambre, 13/11/2019, 422222, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-11-13</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>422222</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème chambre</FORMATION>
<TYPE_REC>Contentieux des pensions</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BENABENT</AVOCATS>
<RAPPORTEUR>M. Marc Firoud</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Gilles Pellissier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2019:422222.20191113</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme C... A..., épouse B..., a demandé au tribunal administratif de Marseille, à titre principal, d'annuler la décision du 30 janvier 2012 par laquelle le centre de gestion des retraites de la direction régionale des finances publiques de Provence-Alpes-Côte d'Azur et du département des Bouches-du-Rhône a rejeté sa réclamation préalable tendant à la décharge de l'obligation de payer la somme de 60 255 euros au titre d'un trop-perçu de pension de réversion, à titre subsidiaire, de condamner l'Etat à raison de son inaction fautive à ne pas avoir recouvré le trop-perçu qui lui est réclamé et de ramener le montant de la créance de l'Etat à la somme de 17 446 euros. Par un jugement n° 1202199 du 22 février 2016, le tribunal administratif de Marseille a rejeté cette demande.<br/>
<br/>
              Par un arrêt n° 16MA01671 du 10 juillet 2018, enregistré au secrétariat du contentieux du Conseil d'Etat le 13 juillet 2018, la cour administrative d'appel de Marseille a, en application des dispositions combinées du 7° de l'article R. 811-1 et du 3° de l'article R. 222-13 du code de justice administrative, transmis au Conseil d'Etat le pourvoi et les quatre mémoires, enregistrés les 22 avril et 6 octobre 2016, les 18 avril et 3 mai 2017 et le 9 avril 2018 au greffe de cette cour, présentés par Mme A....<br/>
<br/>
              Par ce pourvoi, ces mémoires et quatre autres mémoires, enregistrés les 13 et 19 novembre 2018 et les 4 et 9 juillet 2019 au secrétariat du contentieux du Conseil d'Etat, Mme A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
<br/>
<br/>
              2°) réglant l'affaire au fond :<br/>
              - à titre principal, d'annuler la décision du 30 janvier 2012 et de la décharger de l'obligation de payer la somme de 60 255 euros ;<br/>
              - à titre subsidiaire, de ramener la créance de l'Etat à 17 446 euros ;<br/>
              - à titre très subsidiaire, de lui accorder un délai de paiement de 24 mois et de l'autoriser à payer sa dette par des versements échelonnés.<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code civil ;<br/>
              - le code des pensions civiles et militaires de retraite ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Marc Firoud, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Gilles Pellissier, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Bénabent, avocat de Mme A... ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que Mme A..., petite-nièce et légataire universelle de M. A..., décédé en 1994, a reçu un commandement de payer la somme de 60 255 euros, établi le 5 octobre 2011 par le directeur régional des finances publiques (DRFIP) de Provence-Alpes-Côte d'Azur et du département des Bouches-du-Rhône, en raison du versement indu à M. A... d'une pension de réversion entre le 1er juin 1981 et le 31 mai 1994. Par un courrier du 30 janvier 2012, la responsable du centre de gestion des retraites de la DRFIP a rejeté sa réclamation préalable tendant à la décharge de l'obligation de payer la somme en cause. Par un jugement du 22 février 2016, le tribunal administratif de Marseille a rejeté la demande de Mme A... tendant, notamment, à l'annulation de la décision du 30 janvier 2012. Par un arrêt du 10 juillet 2018, la cour administrative d'appel de Marseille a transmis au Conseil d'Etat le pourvoi formé par Mme A... contre ce jugement.<br/>
<br/>
              2. Il résulte des dispositions de l'article L. 46 du code des pensions civiles et militaires de retraite que le conjoint survivant ou le conjoint divorcé d'un pensionné perd son droit au bénéfice d'une pension de réversion s'il contracte un nouveau mariage ou vit en état de concubinage. Aux termes de l'article L. 93 du même code : " Sauf le cas de fraude, omission, déclaration inexacte ou de mauvaise foi de la part du bénéficiaire, la restitution des sommes payées indûment au titre des pensions, de leurs accessoires ou d'avances provisoires sur pensions, attribués en application des dispositions du présent code, ne peut être exigée que pour celles de ces sommes correspondant aux arrérages afférents à l'année au cours de laquelle le trop-perçu a été constaté et aux trois années antérieures ".<br/>
<br/>
              3. Il résulte des dispositions mentionnées au point 2 que lorsque le bénéficiaire d'une pension de réversion omet de déclarer auprès de l'administration un changement de sa situation matrimoniale, cette omission, alors même qu'elle ne révèle aucune intention frauduleuse ou mauvaise foi, fait obstacle à l'application de la prescription prévue par l'article L. 93 du code des pensions civiles et militaires de retraite. <br/>
<br/>
              4. Toutefois, l'article 2277 du code civil, dans sa rédaction applicable avant l'entrée en vigueur de la loi du 17 juin 2008, instituait une prescription par cinq ans des actions relatives aux créances périodiques, sans qu'il y ait lieu de distinguer selon qu'il s'agissait d'une action en paiement ou en restitution de ce paiement. Cette prescription ne courait pas lorsque la créance, même périodique, dépendait d'éléments qui n'étaient pas connus du créancier et devaient résulter de déclarations que le débiteur était tenu de faire. En revanche, l'article 2277 du code civil permettait au débiteur d'opposer cette prescription au créancier qui s'était abstenu de faire valoir sa créance depuis plus de cinq ans à compter de la date à laquelle il disposait des éléments nécessaires à son recouvrement. Désormais, en vertu de l'article 2224 du code civil, dans sa rédaction issue de la loi du 17 juin 2008, les actions personnelles ou mobilières se prescrivent par cinq ans à compter du jour où le titulaire d'un droit a connu ou aurait dû connaître les faits lui permettant de l'exercer. Le débiteur qui ne peut se prévaloir de la prescription de l'article L. 93 du code des pensions civiles et militaires de retraite peut invoquer le bénéfice de la prescription de l'article 2277 du code civil pour la période antérieure à l'entrée en vigueur de la loi du 17 juin 2008 et de l'article 2224 du même code pour la période postérieure.<br/>
<br/>
              5. Dès lors, en jugeant que la requérante ne pouvait utilement invoquer la prescription de l'article 2277 du code civil au soutien de sa demande, au motif que sa situation était régie par les dispositions de l'article L. 93 du code des pensions civiles et militaires de retraite, le tribunal administratif de Marseille a commis une erreur de droit. <br/>
<br/>
              6. Il résulte de tout ce qui précède que Mme A... est fondée, sans qu'il soit besoin d'examiner les autres moyens de son pourvoi, à demander l'annulation du jugement qu'elle attaque.<br/>
<br/>
              7. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 3 000 euros à verser à Mme A..., au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du 22 février 2016 du tribunal administratif de Marseille est annulé.<br/>
Article 2 : L'affaire est renvoyée au tribunal administratif de Marseille. <br/>
Article 3 : L'Etat versera à Mme A... une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à Mme C... A... et au ministre de l'action et des comptes publics. <br/>
Copie en sera adressée à la Caisse des dépôts et consignations. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
