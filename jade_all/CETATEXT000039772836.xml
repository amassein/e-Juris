<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039772836</ID>
<ANCIEN_ID>JG_L_2019_12_000000419836</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/77/28/CETATEXT000039772836.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème chambre, 27/12/2019, 419836, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-12-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>419836</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GATINEAU, FATTACCINI ; SCP BARADUC, DUHAMEL, RAMEIX</AVOCATS>
<RAPPORTEUR>M. Florian Roussel</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Cécile Barrois de Sarigny</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2019:419836.20191227</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme B... A... a demandé au tribunal administratif de Cergy-Pontoise de la décharger de l'obligation de payer la somme de 22 239,06 euros mise à sa charge par une contrainte de la caisse d'allocations familiales du Val d'Oise 19 août 2016. Par un jugement n° 1608710 du 13 février 2018, le tribunal l'a déchargée de l'obligation de payer la somme de 14 929,87 euros, correspondant à l'indu d'aide personnalisée au logement dont le remboursement lui était réclamé et a rejeté le surplus de ses conclusions.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 13 avril et 13 juillet 2018 au secrétariat du contentieux du Conseil d'Etat, la caisse d'allocations familiales (CAF) du Val d'Oise demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement en tant qu'il fait partiellement droit à la demande de Mme A... ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter la demande de Mme A... ;<br/>
<br/>
              3°) de mettre à la charge de Mme A... la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de la construction et de l'habitation ;<br/>
              - la loi n°91-647 du 10 juillet 1991 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Florian Roussel, maître des requêtes, <br/>
<br/>
              - les conclusions de Mme Cécile Barrois de Sarigny, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Gatineau, Fattaccini, avocat de la Caisse D'allocations Familiales Du Val D'oise et à la SCP Baraduc, Duhamel, Rameix, avocat de Mme B... A....<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que, par une contrainte en date du 19 août 2016, le directeur de la caisse d'allocations familiales du Val d'Oise a exigé de Mme A... le paiement d'une somme de 22 239,06 euros, correspondant à des indus d'aide personnalisée au logement, de revenu de solidarité active et de prime exceptionnelle de fin d'année. Par un jugement du 13 février 2018, le tribunal administratif de Cergy-Pontoise a déchargé Mme A... de l'obligation de payer la somme de 14 929,87 euros réclamée au titre de l'aide personnalisée au logement. Le ministre de la cohésion des territoires, régularisant un pourvoi initialement introduit par la caisse d'allocations familiales du Val d'Oise, demande l'annulation de ce jugement en tant qu'il fait partiellement droit à la demande de Mme A....<br/>
<br/>
              2. Aux termes de l'article L. 351-2 du code de la construction et de l'habitation, " l'aide personnalisée au logement est accordée au titre de la résidence principale, quel que soit le lieu de son implantation sur le territoire national. Son domaine d'application comprend : 1° Les logements occupés par leurs propriétaires, construits, acquis ou améliorés, à compter du 5 janvier 1977, au moyen de formes spécifiques d'aides de l'Etat ou de prêts dont les caractéristiques et les conditions d'octroi sont fixées par décret ". Aux termes de l'article L.351-2du même code : " l'aide personnalisée est accordée au propriétaire qui est titulaire de l'un des prêts définis par les articles R. 331-32 et suivants et qui supporte les charges afférentes à ce prêt ". Pour décharger Mme A... de l'obligation de payer l'indu d'aide personnalisée au logement, le tribunal administratif s'est fondé sur ce que, contrairement à ce que soutenait devant lui l'administration, l'intéressée avait droit au versement de cette aide sur le fondement des dispositions, citées ci-dessus, de l'article R.351-2 du code de la construction et de l'habitation, en raison de ce qu'elle supportait le remboursement du prêt immobilier au titre duquel l'aide lui avait été versée.<br/>
<br/>
              3. Toutefois, en se fondant sur ce motif alors qu'il ressortait des pièces du dossier qui lui était soumis que le reversement de l'indu était également motivé par les inexactitudes figurant dans les déclarations de l'intéressée en ce qui concerne ses charges familiales et le montant de ses ressources, le tribunal administratif a commis une erreur de droit. Par suite, son jugement doit être annulé dans la mesure demandée par le ministre, à savoir en tant qu'il statue sur les conclusions tendant à la décharge de la somme réclamée au titre de l'indu d'aide personnalisée au logement.<br/>
<br/>
              4. Les dispositions de l'article L. 761-1 du code de justice font obstacle à ce que soit mise à la charge de l'Etat, qui n'est pas la partie perdante dans la présente instance, la somme que demande, à ce titre, Mme A.... La caisse d'allocations familiales n'étant pas recevable à se pourvoir en cassation contre le jugement du tribunal administratif de Cergy-Pontoise, ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative ne peuvent qu'être rejetées.<br/>
<br/>
<br/>
<br/>
<br/>
              D E C I D E:<br/>
              --------------<br/>
<br/>
Article 1er : Le jugement du tribunal administratif de Cergy-Pontoise est annulé en tant qu'il statue sur les conclusions de Mme A... tendant à la décharge de la somme mise à sa charge au titre de l'indu d'aide personnalisée au logement.<br/>
<br/>
            Article 2 : L'affaire est renvoyée au tribunal administratif de Cergy-Pontoise.<br/>
<br/>
Article 3 : Les conclusions des parties présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 4 : La présente décision sera notifiée à la ministre de la cohésion des territoires et des relations avec les collectivités territoriales et à Mme B... A.... <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
