<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033172381</ID>
<ANCIEN_ID>JG_L_2016_09_000000395319</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/17/23/CETATEXT000033172381.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème chambre, 30/09/2016, 395319, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-09-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>395319</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MEIER-BOURDEAU, LECUYER</AVOCATS>
<RAPPORTEUR>M. Charles Touboul</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2016:395319.20160930</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B...A...a demandé au tribunal administratif de Lyon, d'une part, d'annuler les décisions par lesquelles le ministre de l'intérieur a successivement retiré du capital de son permis de conduire deux points pour une infraction au code de la route commise le 24 janvier 2011, trois points pour une infraction commise le 28 février 2011, deux points pour une infraction commise le 30 août 2011, un point pour une infraction commise le 7 décembre 2011 et six points pour une infraction commise le 3 juin 2012 et, d'autre part, d'enjoindre au ministre de rétablir ces points. Par une ordonnance n° 1305526 du 12 octobre 2015, le président de la 4ème chambre du tribunal administratif a rejeté sa demande.<br/>
<br/>
              Par un pourvoi, enregistré le 15 décembre 2015 au secrétariat du contentieux du Conseil d'Etat, M. A...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat une somme de 2 000 euros a titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Charles Touboul, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP  Meier-Bourdeau, Lecuyer, avocat de M. A...;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'aux termes de l'article R. 421-2 du code de justice administrative : " Sauf disposition législative ou règlementaire contraire, le silence gardé pendant plus de deux mois sur une réclamation par l'autorité compétente vaut décision de rejet. / Les intéressés disposent, pour se pourvoir contre cette décision implicite, d'un délai de deux mois à compter du jour de l'expiration de la période mentionnée au premier alinéa (...) ; que, toutefois, aux termes de l'article R. 421-3 du même code : " (...) l'intéressé n'est forclos qu'après un délai de deux mois à compter du jour de la notification d'une décision expresse de rejet : / 1° En matière de plein contentieux (...) " ;<br/>
<br/>
              2. Considérant que, pour rejeter comme tardive la demande de M. A...tendant à l'annulation de décisions par lesquelles le ministre de l'intérieur avait retiré des points de son permis de conduire, l'ordonnance attaquée relève que l'intéressé s'est vu notifier le 22 février 2013 une décision récapitulant ces retraits et constatant la perte de validité de son permis et que, s'il a présenté le 11 mars 2013 un recours gracieux qui a prorogé le délai de recours contentieux, il n'a pas saisi le tribunal administratif dans les deux mois suivant la naissance, le 11 mai 2013, d'une décision implicite rejetant ce recours gracieux ; <br/>
<br/>
              3. Considérant, toutefois, que la demande présentée par M.A..., étant dirigée contre des décisions présentant le caractère de sanctions prises par une autorité administrative contre un administré, relevait du plein contentieux ; que, par suite, en application des dispositions citées ci-dessus du 1° de l'article R. 421-3 du code de justice administrative, le délai de deux mois imparti à l'intéressé pour saisir le tribunal administratif, qui avait été interrompu par la présentation d'un recours gracieux, ne pouvait courir à nouveau qu'à compter de la notification d'une décision expresse rejetant ce recours ; qu'en estimant que le délai avait recommencé à courir à compter de la naissance d'une décision implicite de rejet, l'ordonnance attaquée est entachée d'une erreur de droit ;<br/>
<br/>
              4. Considérant qu'il résulte de ce qui précède que l'ordonnance du 12 octobre 2015 doit être annulée ; qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat le versement à M. A...de la somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>			D E C I D E :<br/>
              			--------------<br/>
<br/>
Article 1er : L'ordonnance du 12 octobre 2015 du président de la 4ème chambre du tribunal administratif de Lyon est annulée.<br/>
Article 2 : L'affaire est renvoyée au tribunal administratif de Lyon.<br/>
Article 3 : L'Etat versera à M. A...la somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à M. B...A...et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
