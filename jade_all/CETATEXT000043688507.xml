<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043688507</ID>
<ANCIEN_ID>JG_L_2021_06_000000437768</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/68/85/CETATEXT000043688507.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème - 8ème chambres réunies, 21/06/2021, 437768</TITRE>
<DATE_DEC>2021-06-21</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>437768</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème - 8ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP THOUVENIN, COUDRAY, GREVY ; SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR>M. Laurent-Xavier Simonel</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Laurent Cytermann</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:437768.20210621</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Le syndicat CGT des ouvriers et employés de la ville de Saint-Martin-d'Hères et du CCAS et Mme B... A... ont demandé au tribunal administratif de Grenoble d'annuler la délibération du 9 septembre 2014 par laquelle le conseil d'administration du centre communal d'action sociale (CCAS) de Saint-Martin-d'Hères a approuvé le règlement intérieur des agents du service d'aide à domicile et les plannings hebdomadaires pris en application de ce règlement, ainsi que les rejets de leurs recours gracieux.<br/>
<br/>
              Par un jugement n° 1500786 du 18 juillet 2017, le tribunal administratif de Grenoble a annulé la délibération du 9 septembre 2014 en tant qu'elle approuve les dispositions de ce règlement intérieur permettant une annualisation du temps de travail des postes à temps non complet, annulé, dans la même mesure, les décisions rejetant les recours gracieux du syndicat CGT des ouvriers et employés de la ville de Saint-Martin-d'Hères et du CCAS et de Mme A... et rejeté le surplus de leurs conclusions.<br/>
<br/>
              Par un arrêt n° 17LY03522 du 18 novembre 2019, la cour administrative d'appel de Lyon a annulé la délibération du 9 septembre 2014 en tant que le règlement qu'elle approuve n'a pas défini les modalités de pause et de repos des agents du service d'aide à domicile, annulé le jugement du tribunal administratif de Grenoble en tant qu'il n'a pas fait droit, dans cette mesure, aux demandes du syndicat CGT des ouvriers et employés de la ville de Saint-Martin-d'Hères et de Mme A..., annulé, dans la même mesure, les décisions de rejet nées du silence conservé par le CCAS de Saint-Martin-d'Hères sur les recours gracieux du syndicat CGT des ouvriers et employés de la ville de Saint-Martin-d'Hères et du CCAS et de Mme A..., enjoint au CCAS de Saint-Martin-d'Hères de fixer, par une nouvelle délibération, les modalités de repos et de pause des agents de ce service dans un délai d'un mois à compter de la notification de son arrêt et rejeté le surplus des conclusions du syndicat CGT des ouvriers et employés de la ville de Saint-Martin-d'Hères et de Mme A.... <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 20 janvier et 21 avril 2020 au secrétariat du contentieux du Conseil d'Etat, le syndicat CGT des ouvriers et employés de la ville de Saint-Martin-d'Hères et du CCAS et Mme A... demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt en tant qu'il n'a pas intégralement fait droit à leur appel ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à leurs conclusions d'appel et, ce faisant, d'annuler le jugement du tribunal administratif de Grenoble du 18 juillet 2017, la délibération du 9 septembre 2014 et les décisions attaquées ;<br/>
<br/>
              3°) de mettre à la charge du CCAS de Saint-Martin-d'Hères la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la convention de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - la charte des droits fondamentaux de l'Union européenne ;<br/>
              - la loi n° 84-53 du 26 janvier 1984 ;<br/>
              - le décret n° 2000-815 du 25 août 2000 ;<br/>
              - le décret n° 2001-623 du 12 juillet 2001 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Laurent-Xavier Simonel, conseiller d'Etat en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Laurent Cytermann, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Thouvenin, Coudray, Grevy, avocat du syndicat CGT des ouvriers et employés de la ville de Saint-Martin-d'Hères et du CCAS et de Mme A... et à la SCP Piwnica, Molinié, avocat du centre communal d'action sociale de Saint-Martin-d'Hères ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Le syndicat CGT des ouvriers et employés de la ville de Saint-Martin-d'Hères et du CCAS et Mme B... A..., aide à domicile au centre communal d'action sociale de Saint-Martin-d'Hères (CCAS), ont demandé au tribunal administratif de Grenoble d'annuler le règlement intérieur des agents du service d'aide à domicile, approuvé par une délibération du conseil d'administration du CCAS du 9 septembre 2014, ainsi que les décisions de rejet nées du silence gardé sur leurs recours gracieux et les plannings individuels des agents de ce service. Par un jugement du 18 juillet 2017, le tribunal administratif de Grenoble a annulé la délibération du 9 septembre 2014 en tant qu'elle approuve les dispositions de ce règlement intérieur permettant une annualisation du temps de travail des postes à temps non complet, annulé, dans la même mesure, les décisions rejetant les recours gracieux des requérants et rejeté le surplus de leurs conclusions. Le syndicat CGT des ouvriers et employés de la ville de Saint-Martin-d'Hères et du CCAS et Mme A... se pourvoient contre l'arrêt du 18 novembre 2019 par lequel la cour administrative d'appel de Lyon a annulé la délibération du 9 septembre 2014 en tant que le règlement intérieur qu'elle approuve n'a pas défini les modalités de pause et de repos des agents du service d'aide à domicile, annulé le jugement du tribunal administratif de Grenoble en tant qu'il n'a pas fait droit, dans cette mesure, aux demandes des requérants, annulé, dans la même mesure, les décisions rejetant les recours gracieux des requérants, enjoint au CCAS de Saint-Martin-d'Hères de fixer, par une nouvelle délibération, les modalités de repos et de pause des agents de ce service, dans un délai d'un mois à compter de la notification de son arrêt, et rejeté le surplus des conclusions des requérants.<br/>
<br/>
              2. Aux termes de l'article 1er du décret du 12 juillet 2001 pris pour l'application de l'article 7-1 de la loi n° 84-53 du 26 janvier 1984 et relatif à l'aménagement et à la réduction du temps de travail dans la fonction publique territoriale : " Les règles relatives à la définition, à la durée et à l'aménagement du temps de travail applicables aux agents des collectivités territoriales en relevant sont déterminées dans les conditions prévues par le décret du 25 août 2000 (...) sous réserve des dispositions suivantes (...) ". Aux termes de l'article 4 du même décret : " L'organe délibérant de la collectivité ou de l'établissement détermine, après avis du comité technique compétent, les conditions de mise en place des cycles de travail prévus par l'article 4 du décret du 25 août 2000 (...) ".<br/>
<br/>
              3. Aux termes de l'article 1er du décret du 25 août 2000 relatif à l'aménagement et à la réduction du temps de travail dans la fonction publique de l'Etat et dans la magistrature : " La durée du travail effectif est fixée à trente-cinq heures par semaine dans les services et établissements publics de l'Etat ainsi que dans les établissements publics locaux d'enseignement. / Le décompte du temps de travail est réalisé sur la base d'une durée annuelle de travail effectif de 1607 heures maximum, sans préjudice des heures supplémentaires. / (...) ". Aux termes de l'article 3 du même décret : " I.- L'organisation du travail doit respecter les garanties minimales ci-après définies. / La durée hebdomadaire du travail effectif, heures supplémentaires comprises, ne peut excéder ni quarante-huit heures au cours d'une même semaine, ni quarante-quatre heures en moyenne sur une période quelconque de douze semaines consécutives et le repos hebdomadaire, comprenant en principe le dimanche, ne peut être inférieur à trente-cinq heures. / La durée quotidienne du travail ne peut excéder dix heures. / Les agents bénéficient d'un repos minimum quotidien de onze heures. / L'amplitude maximale de la journée de travail est fixée à douze heures. / Le travail de nuit comprend au moins la période comprise entre 22 heures et 5 heures ou une autre période de sept heures consécutives comprise entre 22 heures et 7 heures. / Aucun temps de travail quotidien ne peut atteindre six heures sans que les agents bénéficient d'un temps de pause d'une durée minimale de vingt minutes. / (...) ". Aux termes de l'article 4 du même décret : " Le travail est organisé selon des périodes de référence dénommées cycles de travail. Les horaires de travail sont définis à l'intérieur du cycle, qui peut varier entre le cycle hebdomadaire et le cycle annuel de manière que la durée du travail soit conforme sur l'année au décompte prévu à l'article 1er. / Des arrêtés ministériels pris après avis des comités techniques ministériels compétents définissent les cycles de travail auxquels peuvent avoir recours les services. Ces arrêtés déterminent notamment la durée des cycles, les bornes quotidiennes et hebdomadaires, les modalités de repos et de pause. / Ces cycles peuvent être définis par service ou par nature de fonction. / Les conditions de mise en oeuvre de ces cycles et les horaires de travail en résultant sont définies pour chaque service ou établissement, après consultation du comité technique. / (...) ". <br/>
<br/>
              4. Il résulte des dispositions citées aux points 2 et 3 que, lorsqu'elle décide de mettre en place un cycle de travail annuel à l'intérieur duquel sont définis les horaires de travail des agents de l'un de ses services, une collectivité territoriale est soumise à l'obligation de respecter les durées maximales et minimales du temps de travail et de repos figurant aux articles 1er et 3 du décret du 25 août 2000, mais n'est pas tenue de définir, de manière uniforme, à l'intérieur de ces limites, le temps de travail de l'ensemble des agents du service, ni même de ceux qui exercent les mêmes fonctions. Ces dispositions ne font, par suite, pas obstacle à ce que soient élaborés, dans le cadre des cycles de travail ainsi définis, des plannings individuels mensuels fixant les horaires des agents, ni à ce que soient déterminées des bornes quotidiennes et hebdomadaires entre lesquelles les horaires de chaque agent sont susceptibles de varier. <br/>
<br/>
              5. En l'espèce, le règlement intérieur des agents du service d'aide à domicile, approuvé par la délibération du conseil d'administration du CCAS du 9 septembre 2014, fixe le cycle de travail annuel à 1 536,50 heures, définit les horaires de travail au sein de ce cycle par référence aux horaires d'ouverture du service, soit de 7h30 à 19h30 et détermine, en particulier, les amplitudes maximales hebdomadaires, comprises entre 28 heures et 42 heures, les limites quotidiennes fixées à 9 heures à l'intérieur d'une amplitude horaire journalière maximum de travail de 12 heures, ainsi que la durée minimale du repos hebdomadaire, égale à 35 heures, comprenant en principe le dimanche.<br/>
<br/>
              6. Il résulte de ce qui précède que les requérants, qui ne soutiennent pas que les limitations du temps de travail mentionnées au point 5 méconnaîtraient les articles 1er et 3 du décret du 25 août 2000, ne sont pas fondés à soutenir que la cour administrative d'appel a commis une erreur de droit en jugeant que le règlement intérieur a suffisamment défini le cycle de travail applicable aux agents du service d'aide à domicile du CCAS, alors même que celui-ci n'a pas procédé, à l'intérieur de ce cycle, à une définition collective de l'organisation du travail pour l'ensemble des agents du service ou à tout le moins pour les agents exerçant les mêmes fonctions et que, pour chaque agent, l'organisation du travail n'est pas structurée autour de plannings se répétant à l'identique d'une période à l'autre. Ils ne sont pas davantage fondés à soutenir que la cour aurait commis une erreur de droit et une erreur de qualification juridique des faits en jugeant que le règlement intérieur a effectivement déterminé les bornes quotidiennes et minimales, lesquelles ne doivent pas être comprises comme correspondant aux limites réelles du temps de travail des agents du service.<br/>
<br/>
              7. Les moyens tirés, par voie de conséquence, de ce que la cour aurait commis une erreur de droit en jugeant que le règlement intérieur ne méconnaissait pas les prescriptions en matière de décompte des heures supplémentaires et en jugeant régulière la mise en place d'un régime d'annualisation pour les agents autorisés à exercer leur service à temps partiel ne peuvent, par suite, qu'être également écartés.<br/>
<br/>
              8. Enfin, les requérants soutiennent que, du fait des variations considérables des plannings individuels qu'ils reçoivent chaque mois, les agents sont dans l'impossibilité d'organiser à l'avance leurs journées et semaines de travail pour le mois suivant, ce qui aurait un impact important sur leur vie personnelle. Toutefois, la cour, qui a relevé que le règlement intérieur contesté prévoit que les plannings individuels mensuels doivent être remis aux intéressés avec un préavis d'au moins sept jours, réduit à quatre jours en cas de modification justifiée par l'urgence, avant le début de leur exécution et confère à chaque agent le droit de refuser à quatre reprises les modifications de son planning, n'a pas entaché son arrêt d'erreur de qualification juridique en estimant qu'en dépit des contraintes, justifiées par les nécessités du service, qu'il fait peser sur les agents, ce règlement intérieur, qui respecte toutes les règles de limitation du temps de travail prévues par le décret du 25 août 2000, ne porte pas, dans ces conditions, une atteinte disproportionnée au droit au respect de la vie privée et familiale des agents, garanti par l'article 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales et l'article 7 de la charte des droits fondamentaux de l'Union européenne.<br/>
<br/>
              9. Il résulte de ce qui précède que le pourvoi du syndicat CGT des ouvriers et employés de la ville de Saint-Martin-d'Hères et du CCAS et de Mme A... doit être rejeté, y compris ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative. Dans les circonstances de l'espèce, il n'y a pas lieu de faire droit aux conclusions présentées au même titre par le CCAS de Saint-Martin-d'Hères.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : Le pourvoi du syndicat CGT des ouvriers et employés de la ville de Saint-Martin-d'Hères et du CCAS et de Mme A... est rejeté.<br/>
Article 2 : Les conclusions présentées par le CCAS de la commune de Saint-Martin-d'Hères au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La présente décision sera notifiée au syndicat CGT des ouvriers et employés de la ville de Saint-Martin-d'Hères et du CCAS, premier requérant dénommé, et au centre communal d'action sociale de Saint-Martin-d'Hères.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">36-07-01-03 FONCTIONNAIRES ET AGENTS PUBLICS. STATUTS, DROITS, OBLIGATIONS ET GARANTIES. STATUT GÉNÉRAL DES FONCTIONNAIRES DE L'ÉTAT ET DES COLLECTIVITÉS LOCALES. DISPOSITIONS STATUTAIRES RELATIVES À LA FONCTION PUBLIQUE TERRITORIALE (LOI DU 26 JANVIER 1984). - DÉFINITION PAR L'EMPLOYEUR D'UN CYCLE ANNUEL DE TRAVAIL (DÉCRET DU 25 AOÛT 2000) - POSSIBILITÉ DE DÉFINIR, DANS LE CADRE DES CYCLES DE TRAVAIL, DES PLANNINGS INDIVIDUELS - EXISTENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">36-07-11 FONCTIONNAIRES ET AGENTS PUBLICS. STATUTS, DROITS, OBLIGATIONS ET GARANTIES. OBLIGATIONS DES FONCTIONNAIRES. - DÉFINITION PAR L'EMPLOYEUR D'UN CYCLE ANNUEL DE TRAVAIL (DÉCRET DU 25 AOÛT 2000) - POSSIBILITÉ DE DÉFINIR, DANS LE CADRE DES CYCLES DE TRAVAIL, DES PLANNINGS INDIVIDUELS - EXISTENCE.
</SCT>
<ANA ID="9A"> 36-07-01-03 Il résulte des articles 1er et 4 du décret n° 2001-623 du 12 juillet 2001 et des articles 1er, 3 et 4 du décret n° 2000-815 du 25 août 2000 que, lorsqu'elle décide de mettre en place un cycle de travail annuel à l'intérieur duquel sont définis les horaires de travail des agents de l'un de ses services, une collectivité territoriale est soumise à l'obligation de respecter les durées maximales et minimales du temps de travail et de repos figurant aux articles 1er et 3 du décret du 25 août 2000, mais n'est pas tenue de définir, de manière uniforme, à l'intérieur de ces limites, le temps de travail de l'ensemble des agents du service, ni même de ceux qui exercent les mêmes fonctions.,,,Ces dispositions ne font, par suite, pas obstacle à ce que soient élaborés, dans le cadre des cycles de travail ainsi définis, des plannings individuels mensuels fixant les horaires des agents, ni à ce que soient déterminées des bornes quotidiennes et hebdomadaires entre lesquelles les horaires de chaque agent sont susceptibles de varier.</ANA>
<ANA ID="9B"> 36-07-11 Il résulte des articles 1er et 4 du décret n° 2001-623 du 12 juillet 2001 et des articles 1er, 3 et 4 du décret n° 2000-815 du 25 août 2000 que, lorsqu'elle décide de mettre en place un cycle de travail annuel à l'intérieur duquel sont définis les horaires de travail des agents de l'un de ses services, une collectivité territoriale est soumise à l'obligation de respecter les durées maximales et minimales du temps de travail et de repos figurant aux articles 1er et 3 du décret du 25 août 2000, mais n'est pas tenue de définir, de manière uniforme, à l'intérieur de ces limites, le temps de travail de l'ensemble des agents du service, ni même de ceux qui exercent les mêmes fonctions.,,,Ces dispositions ne font, par suite, pas obstacle à ce que soient élaborés, dans le cadre des cycles de travail ainsi définis, des plannings individuels mensuels fixant les horaires des agents, ni à ce que soient déterminées des bornes quotidiennes et hebdomadaires entre lesquelles les horaires de chaque agent sont susceptibles de varier.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
