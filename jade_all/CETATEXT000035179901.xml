<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000035179901</ID>
<ANCIEN_ID>JG_L_2017_07_000000410832</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/35/17/99/CETATEXT000035179901.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème - 2ème chambres réunies, 12/07/2017, 410832, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-07-12</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>410832</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème - 2ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROCHETEAU, UZAN-SARANO ; SCP GARREAU, BAUER-VIOLAS, FESCHOTTE-DESBOIS</AVOCATS>
<RAPPORTEUR>M. Jean Sirinelli</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Olivier Henrard</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:410832.20170712</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société Etudes Créations et Informatique (ECI) a saisi le juge des référés du tribunal administratif de La Réunion d'une demande tendant à l'annulation, sur le fondement de l'article L. 551-1 du code de justice administrative, de la procédure de passation de marché public menée par le syndicat mixte de transports de La Réunion (SMTR) pour les lots n° 1, n° 2 et n° 3 de l'opération " système de transport intelligent pour La Réunion ". Par une ordonnance n° 1700293 du 10 mai 2017, le juge des référés a rejeté cette demande.<br/>
<br/>
              Par un pourvoi, enregistré le 24 mai 2017 au secrétariat du contentieux du Conseil d'Etat, la société Etudes Créations et Informatique demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) réglant l'affaire en référé, de faire droit à ses conclusions ;<br/>
<br/>
              3°) de mettre à la charge du syndicat mixte de transports de La Réunion la somme de 5 500 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - l'ordonnance n° 2015-899 du 23 juillet 2015 ;<br/>
              - le décret n° 2016-360 du 25 mars 2016 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean Sirinelli, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Olivier Henrard, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Rocheteau, Uzan-Sarano, avocat de la société Etudes Créations et Informatique et à la SCP Garreau, Bauer-Violas, Feschotte-Desbois, avocat du syndicat mixte de transports de La Réunion.<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis au juge des référés du tribunal administratif de La Réunion que la société ECI a déposé un dossier de candidature pour les lots n° 1, n° 2 et n° 3 du marché public intitulé " système de transport intelligent pour La Réunion ", qui a fait l'objet d'un avis publié au Bulletin officiel des annonces de marchés publics et au Journal officiel de l'Union européenne par le SMTR le 21 juin 2016 ; que sa candidature pour le lot n° 3 a été rejetée par une décision notifiée le 14 septembre 2016 ; que, pour les lots n° 1 et n° 2, sa candidature a été admise et la société ECI s'est vue communiquer le dossier de consultation des entreprises le 20 septembre 2016 et a été invitée à remettre ses offres à la date du 15 novembre 2016 ; qu'estimant que le règlement de la consultation l'empêchait de présenter utilement ses offres, la société ECI a renoncé à remettre celles-ci ; que, le 8 avril 2017, elle a introduit devant le juge des référés du tribunal administratif de La Réunion une demande tendant à l'annulation, sur le fondement de l'article L. 551-1 du code de justice administrative, de la procédure de passation pour les lots n° 1, n° 2 et n° 3 ; que par l'ordonnance attaquée du 10 mai 2017, le juge des référés a rejeté sa demande comme tardive ;<br/>
<br/>
              2 Considérant qu'aux termes de l'article L. 551-1 du code de justice administrative : " Le président du tribunal administratif, ou le magistrat qu'il délègue, peut être saisi en cas de manquement aux obligations de publicité et de mise en concurrence auxquelles est soumise la passation par les pouvoirs adjudicateurs de contrats administratifs ayant pour objet l'exécution de travaux, la livraison de fournitures ou la prestation de services, avec une contrepartie économique constituée par un prix ou un droit d'exploitation ... / Le juge est saisi avant la conclusion du contrat " ; que l'article L. 551-5 du même code prévoit des dispositions identiques pour les contrats passés par les entités adjudicatrices ; qu'aux termes de l'article L. 551-10 du même code : " Les personnes habilitées à engager les recours prévus aux articles L. 551-1 et L. 551-5 sont celles qui ont un intérêt à conclure le contrat (...) et qui sont susceptibles d'être lésées par le manquement invoqué (...) " ;<br/>
<br/>
              3. Considérant que ni ces dispositions ni aucune autre disposition législative ou réglementaire n'impliquent que les personnes ayant intérêt à conclure le contrat et qui s'estiment susceptibles d'être lésées par des manquements aux règles de publicité et de mise en concurrence soient tenues de saisir le juge du référé précontractuel dans un délai déterminé à compter du moment où elles ont connaissance de ces manquements ; qu'une telle absence de délai ne conduit pas à ce que ces manquements puissent être contestés indéfiniment devant le juge du référé précontractuel, dès lors que la signature du contrat met fin à la possibilité de saisir ce juge ; qu'au demeurant, la possibilité ainsi offerte aux personnes intéressées de former un référé précontractuel à tout moment de la procédure, en permettant que ces manquements soient, le cas échéant, corrigés avant la conclusion du contrat, tend à prévenir l'introduction de recours remettant en cause le contrat lui-même après sa signature et alors qu'il est en cours d'exécution ; que, par suite, le juge des référés du tribunal administratif de La Réunion a commis une erreur de droit en jugeant qu'il découlait du principe de sécurité juridique une obligation de former un référé précontractuel dans un délai raisonnable, en fixant celui-ci, sous réserve de circonstances particulières, à trois mois à compter de la date à laquelle le requérant a eu connaissance du manquement allégué et en rejetant comme tardive, pour ce motif, la demande présentée par la société ECI ;<br/>
<br/>
              4. Considérant qu'il résulte de ce qui précède, et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, que la société ECI est fondée à demander l'annulation de l'ordonnance qu'elle attaque ;<br/>
<br/>
              5. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge du syndicat mixte de transports de La Réunion la somme de 3 500 euros à verser à la société ECI au titre de l'article L. 761-1 du code de justice administrative ; que ces dispositions font obstacle à ce qu'une somme soit mise, à ce titre, à la charge de la société ECI, qui n'est pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'ordonnance du juge des référés du tribunal administratif de La Réunion du 10 mai 2017 est annulée. <br/>
Article 2 : L'affaire est renvoyée au tribunal administratif de La Réunion. <br/>
Article 3 : Le syndicat mixte de transports de La Réunion versera à la société Etudes Créations et Informatique une somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Les conclusions du syndicat mixte de transports de La Réunion présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée à la société Etudes Créations et Informatique et au syndicat mixte de transports de La Réunion.<br/>
Copie en sera adressée au ministre de l'économie et des finances.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
