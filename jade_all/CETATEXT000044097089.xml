<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044097089</ID>
<ANCIEN_ID>JG_L_2021_09_000000449250</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/09/70/CETATEXT000044097089.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème - 2ème chambres réunies, 23/09/2021, 449250</TITRE>
<DATE_DEC>2021-09-23</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>449250</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème - 2ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP PIWNICA, MOLINIE ; SCP FOUSSARD, FROGER</AVOCATS>
<RAPPORTEUR>M. Alexis Goin</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Marc Pichon de Vendeuil</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:449250.20210923</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société Alstom-Aptis a demandé au juge des référés du tribunal administratif de Paris, sur le fondement de l'article L. 551-5 du code de justice administrative, d'annuler les décisions se rapportant à la procédure de passation de l'accord-cadre relatif à la fourniture d'autobus électriques standards de douze mètres, d'ordonner à la Régie autonome des transports parisiens (RATP) de se conformer à ses obligations de publicité et de mise en concurrence et d'enjoindre à la RATP, si elle entendait conclure le marché, de reprendre la procédure de passation au stade de l'analyse des offres.<br/>
<br/>
              Par une ordonnance n° 2021714 du 15 janvier 2021, le juge des référés du tribunal administratif de Paris a enjoint à la RATP de suspendre l'exécution de la décision d'attribution de l'accord-cadre et de la décision de rejet de l'offre de la société Alstom-Aptis et, si elle entendait poursuivre la procédure de passation du marché, de la reprendre au stade de l'analyse des offres en intégrant son offre.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 1er et 16 février et le 8 juillet 2021 au secrétariat du contentieux du Conseil d'Etat, la RATP demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) réglant l'affaire au titre de la procédure de référé engagée, de rejeter la demande de la société Alstom-Aptis ;<br/>
<br/>
              3°) de mettre à la charge de la société Alstom-Aptis la somme de 6 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de la commande publique ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Alexis Goin, auditeur,  <br/>
<br/>
              - les conclusions de M. Marc Pichon de Vendeuil, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Piwnica, Molinié, avocat de la Régie autonome des transports parisiens et à la SCP Foussard, Froger, avocat de la société Alstom-Aptis ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 9 septembre 2021, présentée par la Régie autonome des transports parisiens ; <br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis au juge des référés du tribunal administratif de Paris que, par un avis d'appel public à la concurrence publié le 25 novembre 2019, la Régie autonome des transports parisiens (RATP) a lancé une procédure négociée de passation d'un accord-cadre multi attributaire à marchés subséquents relatif à la " fourniture d'autobus électriques standards (12 m) ", sans montant minimum et avec un montant maximum de 825 000 000 euros. Par un courrier en date du 17 décembre 2020, la RATP a rejeté l'offre de la société Alstom-Aptis au motif de sa tardiveté. Par une ordonnance du 15 janvier 2021, le juge des référés a suspendu cette décision et enjoint à la RATP, si elle entendait poursuivre la procédure de passation du marché, de la reprendre au stade de l'analyse des offres en intégrant l'offre de cette société.<br/>
<br/>
              2. D'une part, aux termes de l'article R. 2151-5 du code de la commande publique : " Les offres reçues hors délai sont éliminées ". D'autre part, selon l'article R. 2132-7 : " Les communications et les échanges d'informations lors de la passation d'un marché en application du présent livre ont lieu par voie électronique ". Aux termes de l'article R. 2132-9 : " L'acheteur assure la confidentialité et la sécurité des transactions sur un réseau informatique accessible selon des modalités figurant dans un arrêté du ministre chargé de l'économie figurant en annexe au présent code ". Selon l'article R. 2132-11 : " Les candidats et soumissionnaires qui transmettent leurs documents par voie électronique peuvent adresser à l'acheteur, sur support papier ou sur support physique électronique, une copie de sauvegarde de ces documents ".<br/>
<br/>
              3. Si l'article R. 2151-5 du code de la commande publique prévoit que les offres reçues hors délai sont éliminées, l'acheteur public ne saurait toutefois rejeter une offre remise par voie électronique comme tardive lorsque le soumissionnaire, qui n'a pu déposer celle-ci dans le délai sur le réseau informatique mentionné à l'article R. 2132-9 du même code, établit, d'une part, qu'il a accompli en temps utile les diligences normales attendues d'un candidat pour le téléchargement de son offre et, d'autre part, que le fonctionnement de son équipement informatique était normal. <br/>
<br/>
              4. En premier lieu, en constatant, par une appréciation souveraine exempte de dénaturation, d'une part, que l'impossibilité pour la société Alstom-Aptis de transmettre son offre dématérialisée dans le délai imparti n'était imputable ni à son équipement informatique, ni à une faute ou une négligence de sa part dans le téléchargement des documents constituant son offre et, d'autre part, que la RATP n'établissait pas le bon fonctionnement de sa plateforme de dépôt et en déduisant de ce constat que la tardiveté de la remise de l'offre de la société Alstom-Aptis était imputable à un dysfonctionnement de cette plateforme qui faisait obstacle à ce que la RATP écarte cette offre comme tardive, le juge des référés n'a commis aucune erreur de droit.<br/>
<br/>
              5. En second lieu, le  juge des référés n'a pas commis d'erreur de droit en ne tenant pas compte, dans son appréciation d'une éventuelle négligence de la société Alstom-Aptis, de l'absence de dépôt par cette société d'une copie de sauvegarde des documents transmis, dès lors que la transmission d'une copie de sauvegarde des documents transmis par voie électronique est une simple faculté ouverte aux candidats et soumissionnaires en application de l'article R. 2132-11 du code de la commande publique, et que l'absence d'un tel dépôt n'était pas à elle seule de nature à établir l'existence d'une négligence de la société.<br/>
<br/>
              6. Il résulte de tout ce qui précède que le pourvoi de la RATP doit être rejeté.<br/>
<br/>
              7. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la RATP la somme de 3 000 euros à verser à la société Alstom-Aptis au titre de l'article L. 761-1 du code de justice administrative. En revanche, les dispositions de cet article font obstacle à ce qu'une somme soit mise à la charge de la société Alstom-Aptis, qui n'est pas la partie perdante dans la présente instance, sur son fondement.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de la RATP est rejeté.<br/>
Article 2 : La RATP versera la somme de 3 000 euros à la société Alstom-Aptis au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée à la Régie autonome des transports parisiens et à la société Alstom-Aptis. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">39-02-005 MARCHÉS ET CONTRATS ADMINISTRATIFS. - FORMATION DES CONTRATS ET MARCHÉS. - FORMALITÉS DE PUBLICITÉ ET DE MISE EN CONCURRENCE. - UTILISATION D'UNE PLATEFORME ÉLECTRONIQUE POUR LE DÉPÔT DES OFFRES - REJET D'UNE OFFRE POUR TARDIVETÉ - CONDITIONS - CHARGE DE LA PREUVE [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-04-04 PROCÉDURE. - INSTRUCTION. - PREUVE. - FORMALITÉS DE PUBLICITÉ ET DE MISE EN CONCURRENCE LORS DE LA FORMATION DES CONTRATS ET MARCHÉS - UTILISATION D'UNE PLATEFORME ÉLECTRONIQUE POUR LE DÉPÔT DES OFFRES - REJET D'UNE OFFRE POUR TARDIVETÉ - CONDITIONS - CHARGE DE LA PREUVE [RJ1].
</SCT>
<ANA ID="9A"> 39-02-005 Si l'article R. 2151-5 du code de la commande publique (CCP) prévoit que les offres reçues hors délai sont éliminées, l'acheteur public ne saurait toutefois rejeter une offre remise par voie électronique comme tardive lorsque le soumissionnaire, qui n'a pu déposer celle-ci dans le délai sur le réseau informatique mentionné à l'article R. 2132-9 du même code, établit, d'une part, qu'il a accompli en temps utile les diligences normales attendues d'un candidat pour le téléchargement de son offre et, d'autre part, que le fonctionnement de son équipement informatique était normal.......Dans un cas où, d'une part, l'impossibilité pour un candidat de transmettre son offre dématérialisée dans le délai imparti n'est imputable ni à son équipement informatique, ni à une faute ou une négligence de sa part dans le téléchargement des documents constituant son offre et où, d'autre part, l'acheteur public n'établit pas le bon fonctionnement de sa plateforme de dépôt, la tardiveté de la remise de l'offre doit être regardée comme imputable à un dysfonctionnement de cette plateforme faisant obstacle à ce que l'acheteur public écarte cette offre comme tardive.</ANA>
<ANA ID="9B"> 54-04-04 Si l'article R. 2151-5 du code de la commande publique (CCP) prévoit que les offres reçues hors délai sont éliminées, l'acheteur public ne saurait toutefois rejeter une offre remise par voie électronique comme tardive lorsque le soumissionnaire, qui n'a pu déposer celle-ci dans le délai sur le réseau informatique mentionné à l'article R. 2132-9 du même code, établit, d'une part, qu'il a accompli en temps utile les diligences normales attendues d'un candidat pour le téléchargement de son offre et, d'autre part, que le fonctionnement de son équipement informatique était normal. ......Dans un cas où, d'une part, l'impossibilité pour un candidat de transmettre son offre dématérialisée dans le délai imparti n'est imputable ni à son équipement informatique, ni à une faute ou une négligence de sa part dans le téléchargement des documents constituant son offre et où, d'autre part, l'acheteur public n'établit pas le bon fonctionnement de sa plateforme de dépôt, la tardiveté de la remise de l'offre doit être regardée comme imputable à un dysfonctionnement de cette plateforme faisant obstacle à ce que l'acheteur public écarte cette offre comme tardive.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., pour l'appréciation de la preuve de la régularité d'une signature électronique, CE, 17 octobre 2016, Ministre de la défense c/ Société Tribord, n°s 400791 400794, aux tables sur un autre point.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
