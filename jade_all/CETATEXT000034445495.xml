<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034445495</ID>
<ANCIEN_ID>JG_L_2017_04_000000393585</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/44/54/CETATEXT000034445495.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème chambre, 19/04/2017, 393585, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-04-19</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>393585</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème chambre</FORMATION>
<TYPE_REC>Recours en révision</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>DELAMARRE</AVOCATS>
<RAPPORTEUR>M. Jean-Luc Matt</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Marie-Astrid Nicolazo de Barmon</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2017:393585.20170419</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme B...A...a demandé au tribunal administratif de Toulouse d'annuler les avis à tiers détenteur des 16 octobre et 12 décembre 2013 et du 31 janvier 2014 d'un montant de 8 658 euros ainsi que le courrier du directeur départemental des finances publiques de l'Ariège du 16 octobre 2013, de confirmer les décisions prises par ce même directeur dans son courrier du 25 septembre 2013, d'ordonner au centre des finances publiques de l'Ariège de lui accorder l'exonération de taxe foncière pour la part correspondant aux logements sociaux situés 15, bis, rue de Mounic à Verniolle, pour une durée de vingt-cinq ans à compter de l'année 2007, de rectifier la taxe foncière pour les années 2009 à 2013, en la ramenant au montant dû pour son domicile, de la dispenser du paiement de la totalité de la taxe foncière due au titre des années 2012 à 2013, d'annuler toutes les poursuites à son encontre relatives à ces taxes foncières ainsi qu'aux majorations et frais relatifs aux poursuites abusives, de confirmer le sursis de paiement tant que ses recours successifs ne sont pas traités et de lui accorder le sursis de paiement tant qu'elle n'est pas réintégrée en Ariège comme fonctionnaire de catégorie A. Par une ordonnance n° 1400787 du 7 avril 2014, le président du tribunal administratif de Toulouse a rejeté ces demandes.<br/>
<br/>
              Par une décision n° 381164 du 3 juin 2015, le Conseil d'Etat statuant au contentieux a jugé que le pourvoi formé par Mme A...contre cette ordonnance était irrecevable faute d'avoir été présenté dans le délai de deux mois imparti par l'article R. 821-1 du code de justice administrative et a refusé de l'admettre.<br/>
<br/>
              Par une requête et un mémoire complémentaire, enregistrés le 19 août 2015 et le 6 avril 2016 au secrétariat du contentieux du Conseil d'Etat, Mme B... A...demande au Conseil d'Etat :<br/>
<br/>
              1°) de rectifier pour erreur matérielle la décision n° 381164 du 3 juin 2015 ;<br/>
<br/>
              2°) statuant à nouveau sur le pourvoi n° 381164, d'annuler l'ordonnance du président du tribunal administratif de Toulouse du 7 avril 2014 et de faire droit à ses demandes ;<br/>
<br/>
              3°) de condamner l'Etat à lui verser une somme de 9 780 euros en réparation du préjudice subi ;<br/>
<br/>
              4°) de mettre à la charge de l'Etat la somme de 3 500 euros en application de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Luc Matt, maître des requêtes, <br/>
<br/>
              - les conclusions de Mme Marie-Astrid Nicolazo de Barmon, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Delamarre, avocat de Mme B...A...;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article R. 833-1 du code de justice administrative : " Lorsqu'une décision d'une cour administrative d'appel ou du Conseil d'Etat est entachée d'une erreur matérielle susceptible d'avoir exercé une influence sur le jugement de l'affaire, la partie intéressée peut introduire devant la juridiction qui a rendu la décision un recours en rectification. / Ce recours doit être présenté dans les mêmes formes que celles dans lesquelles devait être introduite la requête initiale. Il doit être introduit dans un délai de deux mois qui court du jour de la notification ou de la signification de la décision dont la rectification est demandée. / (...) ". <br/>
<br/>
              2. Par une décision n° 381164 du 3 juin 2015, dont Mme A...demande la rectification pour erreur matérielle, le Conseil d'Etat statuant au contentieux a rejeté le pourvoi qu'elle avait formé contre une ordonnance du 7 avril 2014 du président du tribunal administratif de Toulouse au motif que ce pourvoi avait été introduit après l'expiration du délai de deux mois fixé par l'article R. 821-1 du code de justice administrative et n'était pas, dès lors, recevable. Il résulte de l'instruction, et notamment de l'accusé de réception de la notification de cette décision, versé au dossier n° 381164, que Mme A...a accusé réception de la notification de cette décision le 8 juin 2015. Le délai de recours de deux mois, qui lui était applicable en vertu de l'article R. 833-1 du code de justice administrative cité au point 1 ci-dessus, expirait par suite le 10 août 2015. Son recours, expédié par voie postale le 18 août 2015, a été enregistré au secrétariat du contentieux du Conseil d'Etat le 19 août 2015, soit après l'expiration de ce délai.<br/>
<br/>
              3. Il résulte de ce qui précède que le recours en rectification d'erreur matérielle présenté par Mme A...est irrecevable en raison de sa tardiveté et que sa requête doit être rejetée, y compris les conclusions tendant à ce que l'Etat soit condamné à lui verser une somme en réparation du préjudice subi du fait de l'erreur matérielle alléguée. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mise à la charge de l'Etat, qui n'est pas la partie perdante dans la présente instance, le versement de la somme que demande, à ce titre, MmeA....<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de Mme A...est rejetée.<br/>
Article 2 : La présente décision sera notifiée à Mme A...et au ministre de l'économie et des finances.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
