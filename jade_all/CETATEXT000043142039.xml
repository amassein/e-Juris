<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043142039</ID>
<ANCIEN_ID>JG_L_2021_02_000000427187</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/14/20/CETATEXT000043142039.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème chambre, 12/02/2021, 427187, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-02-12</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>427187</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BARADUC, DUHAMEL, RAMEIX</AVOCATS>
<RAPPORTEUR>M. François Charmont</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:427187.20210212</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A... B... a demandé au tribunal administratif de Dijon d'annuler l'arrêté du 17 septembre 2018 par lequel le préfet de la Côte-d'Or a suspendu son permis de conduire pour une durée de six mois et d'enjoindre au préfet de lui restituer son permis. Par un jugement n° 1802502 du 16 novembre 2018, le tribunal administratif a rejeté sa demande.<br/>
<br/>
              Par un pourvoi enregistré le 18 janvier 2019 au secrétariat du contentieux du Conseil d'Etat, M. B... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;   <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 2 500 euros au titre de l'article L.761-1 du code de justice administrative.<br/>
<br/>
              M. B... soutient que le jugement attaqué est entaché d'erreur de droit et de dénaturation des pièces du dossier en ce qu'il juge qu'il lui appartenait d'apporter la preuve de ce que les éléments constitutifs de l'infraction n'étaient pas réunis.<br/>
<br/>
              Par un mémoire en défense, enregistré le 30 avril 2019, le ministre de l'intérieur conclut au rejet du pourvoi. Il soutient que ses moyens ne sont pas fondés.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de la route ;<br/>
              - le code de procédure pénale ;<br/>
              - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. François Charmont, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Baraduc, Duhamel, Rameix, avocat de M. B... ;<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que M. B... a fait l'objet le 15 septembre 2018 d'un contrôle routier au cours duquel a été mis en évidence un taux d'alcoolémie qui a conduit à la rétention de son permis de conduire. Par un arrêté du 17 septembre 2018, le préfet de la Côte-d'Or a ensuite suspendu son permis de conduire pour une durée de six mois. M. B... se pourvoit en cassation contre le jugement du 16 novembre 2018 par lequel le tribunal administratif de Dijon a rejeté sa demande tendant à l'annulation de cette dernière décision.<br/>
<br/>
              2. Aux termes de l'article L. 224-1 du code de la route, dans sa version alors applicable : " Lorsque les épreuves de dépistage de l'imprégnation alcoolique et le comportement du conducteur permettent de présumer que celui-ci conduisait sous l'empire de l'état alcoolique défini à l'article L.234-1 ou lorsque les mesures faites au moyen de l'appareil homologué mentionné à l'article L.234-4 ont établi cet état, les officiers et agents de police judiciaire retiennent à titre conservatoire le permis de conduire de l'intéressé (...) ". Aux termes de l'article L. 224-2 du même code : " Lorsque l'état alcoolique est établi au moyen d'un appareil homologué, comme il est dit au premier alinéa de l'article L.224-1 ou lorsque les vérifications mentionnées aux articles L.234-4 et L.234-5 apportent la preuve de cet état, le représentant de l'Etat dans le département peut, dans les soixante-douze heures de la rétention du permis, prononcer la suspension du permis de conduire pour une durée qui ne peut excéder six mois (...) ". Enfin, aux termes de l'article R. 224-1 du même code : " Dans les cas prévus à l'article L. 224-1, la décision de rétention du permis de conduire, qu'elle soit ou non accompagnée de la remise matérielle de ce titre, donne lieu à l'établissement d'un avis de rétention dont un exemplaire est immédiatement remis au conducteur (...) ".<br/>
<br/>
              3. En estimant, au vu des pièces du dossier qui lui était soumis, que M. B... était le conducteur du véhicule dont l'immobilisation, sur le bas-côté de la route, avait conduit au contrôle effectué par la gendarmerie dans la nuit du 15 septembre 2018, alors même que l'autorité administrative n'avait pas établi de procès-verbal à la suite de ce contrôle routier et que la procédure avait, pour ce motif, fait l'objet d'un classement par le procureur de la République, le tribunal administratif s'est livré à une appréciation souveraine, exempte de dénaturation et n'a pas commis d'erreur de droit.<br/>
<br/>
              4. Il résulte de ce qui précède que le pourvoi de M. B... doit être rejeté, y compris, par voie de conséquence, ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>			D E C I D E :<br/>
              			--------------<br/>
	Article 1er : Le pourvoi de M. B... est rejeté.<br/>
	Article 2 : La présente décision sera notifiée à M. A... B... et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
