<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043240966</ID>
<ANCIEN_ID>JG_L_2021_03_000000449861</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/24/09/CETATEXT000043240966.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 08/03/2021, 449861, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-03-08</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>449861</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROCHETEAU, UZAN-SARANO</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2021:449861.20210308</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés les 18 et 25 février 2021 au secrétariat du contentieux du Conseil d'Etat, le Rassemblement des opticiens de France demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative :<br/>
<br/>
              1°) de suspendre l'exécution du décret n° 2021-99 du 30 janvier 2021 modifiant les décrets n° 2020-1262 du 16 octobre 2020 et n° 2020-1310 du 29 octobre 2020 prescrivant les mesures générales nécessaires pour faire face à l'épidémie de covid-19 dans le cadre de l'état d'urgence sanitaire, en tant qu'il n'autorise pas l'ouverture des commerces de détail de produits d'optique en magasin spécialisé au sein des centres commerciaux et magasins de vente comportant un ou plusieurs bâtiments dont la surface commerciale utile cumulée est supérieure ou égale à vingt mille mètres carrés, y compris pour les retraits de commandes, jusqu'à ce qu'il soit statué au fond sur sa légalité ;<br/>
<br/>
              2°) d'ordonner au Premier ministre de modifier le décret du 30 janvier 2021 afin d'autoriser l'ouverture de ces commerces dans un délai d'une semaine à compter de la notification de la décision à intervenir et sous astreinte de cinq cents euros par jour de retard ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 10 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Il soutient que :<br/>
              - la condition d'urgence est remplie dès lors que l'exécution du décret contesté, en tant qu'il exclut toute possibilité pour les commerces de détail de produits d'optique en magasin spécialisé d'ouvrir au sein des grands centres commerciaux, y compris pour y exercer une activité de retrait de commandes, porte une atteinte grave et immédiate à la situation de ces commerces et que la mesure contestée produit des effets anticoncurrentiels importants ;<br/>
              - aucune urgence ne s'attache à l'exécution de la mesure contestée dès lors qu'elle a pris effet à un moment où le taux de reproduction effectif du virus de la covid-19 était inférieur à 1, que plusieurs activités peuvent continuer à être exercées au sein des centres commerciaux concernés, telles que les commerces alimentaires ou les pharmacies, que les opticiens-lunetiers ont mis en place des protocoles sanitaires permettant de lutter efficacement contre la diffusion du virus et que la fermeture des magasins d'optique dans les grands centres commerciaux conduit à un report de la clientèle vers un nombre réduit de magasins, en méconnaissance de l'objectif de lutte contre la propagation du virus ;<br/>
              - il existe un doute sérieux quant à la légalité des dispositions contestées ; <br/>
              - la mesure contestée crée une différence injustifiée de traitement, d'une part, avec les pharmacies implantées dans les grands centres commerciaux et autorisées à rester ouvertes alors que l'activité des opticiens, qui sont des professionnels de santé, présente un caractère essentiel et, d'autre part, entre les commerces de détail de produits d'optique selon leur localisation, en particulier avec ceux appartenant à une enseigne de la grande distribution et qui, alors même qu'ils sont implantés dans des grands centres commerciaux, demeurent ouverts ;<br/>
              - la mesure contestée n'est pas proportionnée dès lors que les magasins d'optique ont mis en place des protocoles sanitaires extrêmement stricts, que la fermeture des magasins concernés contraint les patients à se diriger vers un nombre réduit de magasins situés dans d'autres centres commerciaux de surface plus modeste ou en centre-ville, ce qui conduit à un brassage de population exacerbé et à un allongement des délais de rendez-vous ; <br/>
              - la mesure contestée méconnaît le principe fondamental du libre choix, par le patient, de son praticien de santé dès lors qu'il a pour effet d'empêcher de nombreux patients de consulter leurs opticiens-lunetiers habituels et de restreindre leur choix entre plusieurs opticiens ;<br/>
              - elle méconnaît la liberté d'entreprendre, la liberté du commerce et de l'industrie et le principe de libre concurrence dès lors que la fermeture des magasins d'optique dans les grands centres commerciaux n'est pas suffisamment justifiée et est disproportionnée au regard de l'objectif de protection de la santé publique, qu'il est interdit à ces magasins de poursuivre une activité sous forme d'un retrait de commandes et que la vente en ligne est impossible en raison du coût de cette modalité de vente et du caractère personnalisé des services fournis par les opticiens-lunetiers ; <br/>
              - elle induit un phénomène de concurrence déloyale ;<br/>
              - eu égard à ses effets anticoncurrentiels, le décret contesté est entaché d'illégalité faute pour l'Autorité de la concurrence d'avoir été consultée, en méconnaissance de l'article L. 462-1 du code de commerce ; <br/>
              - la mesure contestée méconnaît le principe de sécurité juridique et le droit à la protection des biens dès lors que l'interdiction litigieuse était totalement imprévisible, les magasins d'optique ayant jusqu'à présent été autorisés à accueillir du public, y compris en période de confinement, sans condition de délai ni mesure transitoire. <br/>
<br/>
              Par un mémoire en défense, enregistré le 24 février 2021, le ministre des solidarités et de la santé conclut au rejet de la requête. Il soutient que la mesure contestée est adaptée, nécessaire et proportionnée et qu'aucun des moyens soulevés n'est de nature à faire naître un doute sérieux quant à sa légalité.<br/>
<br/>
              La requête a été communiquée au Premier ministre qui n'a pas formulé d'observations.<br/>
              Par un nouveau mémoire, enregistré le 1er mars 2021, le ministre des solidarités et de la santé reprend les conclusions de son précédent mémoire et les mêmes moyens. <br/>
<br/>
              Par un nouveau mémoire, enregistré le 1er mars 2021, le Rassemblement des opticiens de France reprend les conclusions de sa requête et les mêmes moyens. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - le code de commerce ;<br/>
              - le code de la santé publique ;<br/>
              - la loi n° 2021-160 du 15 février 2021 ;<br/>
              - le décret n° 2020-1257 du 14 octobre 2020 ;<br/>
              - le décret n° 2020-1310 du 29 octobre 2020 modifié par le décret n° 2021-99 du 30 janvier 2021 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, le Rassemblement des opticiens de France et, d'autre part, le Premier ministre et le ministre des solidarités et de la santé ;<br/>
<br/>
              Ont été entendus à l'audience publique du 26 février 2021, à 10 heures :<br/>
<br/>
              - Me Uzan-Sarano, avocat au Conseil d'Etat et à la Cour de cassation, avocat du Rassemblement des opticiens de France ;<br/>
<br/>
              - les représentants du Rassemblement des opticiens de France ;<br/>
<br/>
              - le représentant du ministre des solidarités et de la santé ;<br/>
<br/>
              à l'issue de laquelle le juge des référés a différé la clôture de l'instruction au 1er mars 2021, à 17 heures. <br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision ". <br/>
<br/>
              Sur le cadre juridique du litige :<br/>
<br/>
              2. La nouvelle progression de l'épidémie de covid-19 à l'automne 2020 en France a conduit le Président de la République à déclarer, par décret du 14 octobre 2020 pris sur le fondement des articles L. 3131-12 et L. 3131-13 du code de la santé publique, l'état d'urgence sanitaire à compter du 17 octobre 2020 sur l'ensemble du territoire national. L'article 1er de la loi du 14 novembre 2020 et l'article 2 de la loi du 15 février 2021 ont prorogé cet état d'urgence respectivement jusqu'au 16 février 2021, puis jusqu'au 1er juin 2021. Le Premier ministre a pris, sur le fondement de l'article L. 3131-15 du code de la santé publique, le décret du 29 octobre 2020 prescrivant les mesures générales nécessaires pour faire face à l'épidémie de covid-19 dans le cadre de l'urgence sanitaire, modifié en dernier lieu par un décret du 30 janvier 2021.<br/>
<br/>
              3. Le II de l'article 37 du décret du 29 octobre 2020 dans sa rédaction issue de l'article 2 du décret du 30 janvier 2021 prévoit ainsi que " les magasins de vente et centres commerciaux, comportant un ou plusieurs bâtiments dont la surface commerciale utile cumulée (...) est supérieure ou égale à vingt mille mètres carrés, ne peuvent accueillir du public. L'activité de retrait de commandes (...) y est également interdite. / Les interdictions résultant de l'alinéa précédent ne font pas obstacle à l'ouverture des magasins de vente relevant des catégories suivantes, y compris au sein des centres commerciaux : /- Commerce de détail de produits surgelés ; / - Commerce d'alimentation générale ; / - Supérettes ; / - Supermarchés ; / - Magasins multi-commerces dont l'activité principale est la vente alimentaire ; - Hypermarchés ; / - Commerce de détail de fruits et légumes en magasin spécialisé ; / - Commerce de détail de viandes et de produits à base de viande en magasin spécialisé ; / - Commerce de détail de poissons, crustacés et mollusques en magasin spécialisé ; / - Commerce de détail de pain, pâtisserie et confiserie en magasin spécialisé ; / - Boulangerie et boulangerie-pâtisserie ; / - Autres commerces de détail alimentaires en magasin spécialisé ;/ - Commerce de détail de produits pharmaceutiques en magasin spécialisé ".<br/>
<br/>
              Sur la requête en référé : <br/>
<br/>
              4. Le Rassemblement des opticiens de France demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative, de suspendre l'exécution des dispositions citées au point 3 ci-dessus en tant qu'elles n'autorisent pas l'ouverture des commerces de détail de produits d'optique en magasin spécialisé au sein des centres commerciaux et magasins de vente comportant un ou plusieurs bâtiments dont la surface commerciale utile cumulée est supérieure ou égale à vingt mille mètres carrés, y compris pour les retraits de commandes. <br/>
<br/>
              5. En premier lieu, aux termes du second alinéa de l'article L. 462-1 du code de commerce, l'Autorité de la concurrence " donne son avis sur toute question de concurrence à la demande du Gouvernement ". Ces dispositions n'imposaient pas au gouvernement de consulter l'Autorité de la concurrence préalablement à l'édiction de la mesure contestée qui, au demeurant, ne créée pas un régime nouveau ayant directement pour effet de soumettre l'accès à un marché à des restrictions quantitatives au sens de l'article L. 462-2 du même code.  <br/>
<br/>
              6. En deuxième lieu, il résulte de l'instruction que la fermeture des commerces implantés dans les grands centres commerciaux, prévue au II de l'article 37 du décret du 29 octobre 2020 dans sa rédaction issue de l'article 2 du décret du 30 janvier 2021, constitue une mesure complémentaire de freinage de la propagation de l'épidémie de covid-19, ciblée sur des lieux de consommation caractérisés par la concentration de nombreux commerces dans un espace clos, attirant des populations importantes et, par là-même, de nature à favoriser la dissémination du virus par la multiplication des interactions entre les personnes, notamment dans les moyens de transports permettant d'y accéder et les zones de circulation. En ne prévoyant, au titre des exceptions au principe de fermeture ainsi institué, que les seuls commerces alimentaires et à prédominance alimentaire ainsi que les pharmacies, à raison de la participation des officines à la lutte contre l'épidémie notamment en matière de dépistage et de la nécessité d'assurer la distribution des médicaments, l'administration indique qu'elle a entendu faire produire à cette mesure de contrainte limitée ses pleins effets.  <br/>
<br/>
              7. D'une part, si le Rassemblement des opticiens de France soutient, à l'appui de sa demande tendant à l'inscription des commerces de détail de produits d'optique en magasin spécialisé implantés dans les grands centres commerciaux, sur la liste des commerces autorisés à rester ouverts, que la profession a adopté un protocole sanitaire extrêmement strict dans les magasins et que la clientèle est accueillie uniquement sur rendez-vous, il résulte de l'instruction que de nombreux autres commerces non-alimentaires situés dans ces mêmes centres pourraient se prévaloir de dispositifs identiques pour demander le bénéfice d'un traitement équivalent, ce qui priverait, pour une large part, la mesure contestée de ses effets attendus pour lutter contre la propagation de l'épidémie.  <br/>
<br/>
              8. D'autre part, il résulte de l'instruction que les prescriptions médicales en matière d'optique ont une durée de validité particulièrement longue et que la fidélité de la clientèle à leur opticien-lunetier est très élevée. Par suite, il n'est pas établi que la clientèle des magasins d'optique concernés par la mesure de fermeture contestée, qui conserve, en tout état de cause, le libre choix de son praticien, se reportera systématiquement sur les commerces de centre-ville ou sur ceux implantés dans les centres commerciaux de plus petite taille et ne différera pas ses achats, dans l'attente de la levée de l'interdiction. <br/>
<br/>
              9. Dans ces conditions, compte tenu de l'objectif de santé publique poursuivi et des dispositifs de soutien financier mis en place pour assurer la viabilité des commerces contraints à la fermeture, alors même que la vente en ligne n'apparaît pas adaptée à la vente des produits et des services d'optique et que le retrait de commandes n'est pas autorisé, la mesure contestée n'apparaît pas, en l'état de l'instruction, disproportionnée et porter une atteinte excessive à la liberté du commerce et de l'industrie, à la liberté d'entreprendre, à la libre concurrence et au principe d'égalité, les magasins d'optique appartenant aux enseignes de la grande distribution implantés dans les grands centres commerciaux n'étant pas, non plus, au nombre des commerces autorisés à ouvrir.<br/>
<br/>
              10. En troisième lieu, compte tenu de la différence de nature entre une mesure de confinement, qui repose sur le principe de la fermeture de l'ensemble des commerces sous réserve de ceux permettant à la population d'avoir accès aux biens et services de première nécessité, au titre desquels figurent les produits et services d'optique, et la mesure contestée, limitée à la fermeture des commerces non-alimentaires implantés dans les grands centres commerciaux, à l'exception des pharmacies, le moyen tiré de ce que la fermeture des magasins d'optique implantés dans de tels centres était imprévisible dès lors qu'ils avaient été autorisés à rester ouverts durant les périodes de confinement, et que, par suite, elle porterait atteinte au principe de sécurité juridique et méconnaîtrait les stipulations de l'article 1er du protocole additionnel à la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales n'est pas de nature à créer, en l'état de l'instruction, un doute sérieux sur sa légalité. <br/>
<br/>
              11. Il résulte de tout ce qui précède que, sans qu'il soit besoin de se prononcer sur la condition d'urgence, la requête présentée par le Rassemblement des opticiens de France sur le fondement de l'article L. 521-1 du code de justice administrative doit être rejetée, y compris les conclusions à fin d'injonction et celles présentées au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
              ------------------<br/>
<br/>
Article 1er : La requête du Rassemblement des opticiens de France est rejetée. <br/>
Article 2 : La présente ordonnance sera notifiée au Rassemblement des opticiens de France, au Premier ministre et au ministre des solidarités et de la santé.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
