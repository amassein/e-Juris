<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037882254</ID>
<ANCIEN_ID>JG_L_2018_12_000000410347</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/88/22/CETATEXT000037882254.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème - 7ème chambres réunies, 28/12/2018, 410347</TITRE>
<DATE_DEC>2018-12-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>410347</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème - 7ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Clément Malverti</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Guillaume Odinet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:410347.20181228</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, un mémoire en réplique et un nouveau mémoire, enregistrés les 9 mai et 17 novembre 2017 et le 14 février 2018 au secrétariat du contentieux du Conseil d'Etat, l'association La Cimade demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler les décisions implicites par lesquelles le ministre de l'intérieur et le directeur général de l'Office français de l'immigration et de l'intégration ont rejeté ses demandes tendant à l'édiction des mesures réglementaires nécessaires au respect des délais d'enregistrement des demandes d'asile et d'accès aux conditions matérielles d'accueil prescrites par l'article L. 741-1 du code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
<br/>
              2°) d'enjoindre au ministre de l'intérieur, d'une part, dans l'attente d'une modification de l'organisation des services des préfectures de nature à garantir le respect du délai d'enregistrement des demandes d'asile prescrit par l'article L. 741-1 du code de l'entrée et du séjour des étrangers et du droit d'asile, de prévoir que la convocation au guichet unique dans un délai supérieur à dix jours ouvrés vaudra attestation de demande d'asile au titre de l'article L. 741-1 du même code, d'autre part, de réexaminer sa décision de ne pas modifier l'arrêté du 9 octobre 2015 pris en application de cet article afin de porter à deux mois la durée de validité des attestations de demande d'asile ;<br/>
<br/>
              3°) d'enjoindre au directeur général de l'Office français de l'immigration et de l'intégration, d'une part, de réexaminer sa décision refusant d'augmenter les moyens alloués aux organismes conventionnés en charge des structures de premier accueil et, d'autre part, de réexaminer sa décision d'organisation de ses services afin que l'offre de prise en charge prévue à l'article L. 744-1 du code de l'entrée et du séjour des étrangers et du droit d'asile et l'évaluation de la vulnérabilité des demandeurs d'asile prévue par l'article L. 744-6 du même code soient effectuées dès la présentation de la demande d'asile ;<br/>
<br/>
              4°) de mettre à la charge de l'Etat et de l'Office français de l'immigration et de l'intégration la somme de 3 000 euros chacun au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
                          Vu :<br/>
               - la directive 2013/32/UE du Parlement européen et du Conseil du 26 juin 2013 ;<br/>
              - la directive 2013/33/UE du Parlement européen et du Conseil du 26 juin 2013 ;<br/>
                          - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
                          - la loi n° 2015-925 du 29 juillet 2015 ;<br/>
                          - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Clément Malverti, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Guillaume Odinet, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
              1.	Considérant que l'article 6 de la directive 2013/32/UE du Parlement européen et du Conseil du 26 juin 2013 relative à des procédures communes pour l'octroi et le retrait de la protection internationale prévoit que : " 1. Lorsqu'une personne présente une demande de protection internationale à une autorité compétente en vertu du droit national pour enregistrer de telles demandes, l'enregistrement a lieu au plus tard trois jours ouvrables après la présentation de la demande. / Si la demande de protection internationale est présentée à d'autres autorités qui sont susceptibles de recevoir de telles demandes, mais qui ne sont pas, en vertu du droit national, compétentes pour les enregistrer, les États membres veillent à ce que l'enregistrement ait lieu au plus tard six jours ouvrables après la présentation de la demande./ (...) 5. Lorsque, en raison du nombre élevé de ressortissants de pays tiers ou d'apatrides qui demandent simultanément une protection internationale, il est dans la pratique très difficile de respecter le délai prévu au paragraphe 1, les États membres peuvent prévoir de porter ce délai à dix jours ouvrables " ; qu'aux termes de l'article 6 de la directive 2013/33/UE du Parlement européen et du Conseil du 26 juin 2013 établissant des normes pour l'accueil des personnes demandant la protection internationale : " Les États membres font en sorte que les demandeurs reçoivent, dans un délai de trois jours à compter de l'introduction de leur demande de protection internationale, un document délivré à leur nom attestant leur statut de demandeur ou attestant qu'ils sont autorisés à demeurer sur le territoire de l'État membre pendant que leur demande est en attente ou en cours d'examen (...) " ; que, selon l'article 17 de la même directive, " Les États membres font en sorte que les demandeurs aient accès aux conditions matérielles d'accueil lorsqu'ils présentent leur demande de protection internationale " ; que l'article L. 741-1 du code de l'entrée et du séjour des étrangers et du droit d'asile dispose que : " L'enregistrement [des demandes d'asile] a lieu au plus tard trois jours ouvrés après la présentation de la demande à l'autorité administrative compétente, sans condition préalable de domiciliation. Toutefois, ce délai peut être porté à dix jours ouvrés lorsqu'un nombre élevé d'étrangers demandent l'asile simultanément (...) " ; que l'article L. 744-1 du même code  prévoit que " les conditions matérielles d'accueil du demandeur d'asile, au sens de la directive 2013/33/UE (...), sont proposées à chaque demandeur d'asile par l'Office français de l'immigration et de l'intégration après l'enregistrement de la demande d'asile par l'autorité administrative compétente (...) " ;<br/>
<br/>
              2.	Considérant que les dispositions précédemment citées de l'article L. 741-1 du code de l'entrée et du séjour des étrangers et du droit d'asile, issues de la loi du 29 juillet 2015 relative à la réforme du droit d'asile, transposant les objectifs de la directive 2013/32/UE du 26 juin 2013, font peser sur l'Etat une obligation de résultat s'agissant des délais dans lesquels les demandes d'asile doivent être enregistrées ; qu'il incombe en conséquence aux autorités compétentes de prendre les mesures nécessaires au respect de ces délais ; que le refus de prendre de telles mesures constitue une décision susceptible de faire l'objet d'un recours pour excès de pouvoir ; <br/>
<br/>
              3.	Considérant qu'il ressort des pièces du dossier que, par un courrier du 28 février 2017, le secrétaire général de l'association La Cimade a demandé au ministre de l'intérieur et au directeur général de l'Office français de l'immigration et de l'intégration de prendre toutes mesures utiles afin de garantir le respect, sur l'ensemble du territoire national, des délais d'enregistrement des demandes d'asile fixés à l'article L. 741-1 du code de l'entrée et du séjour des étrangers et du droit d'asile ; que, par un courrier du 31 mars 2017, le directeur général des étrangers en France, après avoir admis que les délais moyens d'enregistrement des demandes d'asile se situaient au-dessus des délais prescrits par ces dispositions, s'est borné à porter à la connaissance du secrétaire général de l'association La Cimade les efforts entrepris pour améliorer ces délais ; qu'eu égard à la portée de l'obligation résultant de l'article L. 741-1 du code de l'entrée et du séjour des étrangers et du droit d'asile, transposant les objectifs de la directive du 26 juin 2013, ce courrier doit être regardé comme un refus de prendre les mesures sollicitées par l'association La Cimade ; que, par suite, l'association la Cimade est, contrairement à ce que soutient le ministre de l'intérieur, recevable à demander l'annulation pour excès de pouvoir de cette décision de refus ainsi que de la décision de refus qui résulte du silence conservé par le directeur général de l'Office français de l'immigration et de l'intégration sur la demande qui lui a été adressée ;<br/>
<br/>
              4.	Considérant que si, conformément au principe du caractère contradictoire de l'instruction, le juge administratif est tenu de ne statuer qu'au vu des seules pièces du dossier qui ont été communiquées aux parties, il lui appartient, dans l'exercice de ses pouvoirs généraux de direction de la procédure, de prendre toutes mesures propres à lui procurer, par les voies de droit, les éléments de nature à lui permettre de former sa conviction sur les points en litige ;<br/>
<br/>
              5.	Considérant qu'il ressort des pièces du dossier et qu'il n'est pas contesté qu'à la date de la décision attaquée, les délais moyens d'enregistrement des demandes d'asile se situaient au-dessus des délais prescrits par les dispositions de l'article L. 741-1 du code de l'entrée et du séjour des étrangers et du droit d'asile dans la plupart des guichets uniques pour demandeurs d'asile ; que dans ces conditions, eu égard au caractère généralisé du non-respect de l'obligation de résultat prévue par le législateur, le ministre de l'intérieur, dont les services sont chargés de l'enregistrement des demandes d'asile, ne pouvait légalement refuser de faire usage de ses pouvoirs en vue d'assurer le respect effectif du délai prescrit par l'article L. 741-1 du code ; <br/>
<br/>
              6.	Considérant, toutefois, que, depuis la date de la décision attaquée, est entrée en vigueur la loi du 10 septembre 2018 qui a modifié l'organisation et entendu améliorer le fonctionnement du dispositif d'accueil et de prise en charge des demandeurs d'asile et ont été édictées par le ministre de l'intérieur une information du 4 décembre 2017 relative à l'évolution du parc d'hébergement des demandeurs d'asile et des réfugiés et une instruction du 12 janvier 2018 relative à la réduction des délais d'enregistrement des demandes d'asile aux guichets uniques ; que, compte tenu de ces évolutions, il y a lieu d'ordonner, avant de statuer sur les conclusions de la requête, au ministre de l'intérieur et au directeur général de l'Office français de l'immigration et de l'intégration de produire tous éléments susceptibles de permettre au Conseil d'Etat, d'une part, d'apprécier si les délais fixés à l'article L. 741-1 du code de l'entrée et du séjour des étrangers et du droit d'asile sont désormais respectés, d'autre part, de connaître l'ensemble des mesures prises depuis la décision attaquée afin de garantir le respect de ces délais ;<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Avant dire droit sur les conclusions de la requête de l'association La Cimade, il est ordonné au ministre de l'intérieur et au directeur général de l'Office français de l'immigration et de l'intégration de communiquer au Conseil d'Etat, dans un délai de deux mois à compter de la notification de la présente décision, les éléments définis par les motifs de la présente décision.<br/>
Article 2 : La présente décision sera notifiée à l'association La Cimade, au ministre de l'intérieur et à l'Office français de l'immigration et de l'intégration.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
