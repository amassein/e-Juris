<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030514558</ID>
<ANCIEN_ID>JG_L_2015_04_000000389161</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/51/45/CETATEXT000030514558.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 13/04/2015, 389161, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-04-13</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>389161</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FABIANI, LUC-THALER, PINATEL</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2015:389161.20150413</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme A...B...a demandé au juge des référés du tribunal administratif de Paris, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, d'enjoindre au préfet de police de suspendre sa tentative d'éloignement vers le Gabon. Par une ordonnance n° 1503635 du 16 mars 2015, le juge des référés du tribunal administratif de Paris a enjoint au préfet de police d'organiser, dans les meilleurs délais et aux frais de l'Etat, le retour de Mme B...en France. <br/>
<br/>
              Par un recours enregistré le 1er avril 2015 au secrétariat du contentieux du Conseil d'Etat, le ministre de l'intérieur demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative :<br/>
<br/>
              1°) d'annuler cette ordonnance ; <br/>
<br/>
              2°) de rejeter la demande de première instance présentée par Mme B....<br/>
<br/>
<br/>
              Il soutient que : <br/>
              - l'ordonnance attaquée est entachée d'une erreur de droit en ce que le juge des référés n'a pas soulevé d'office l'irrecevabilité de la requête de Mme B... pour tardiveté et qu'il ne pouvait faire droit aux conclusions de la requérante sans excéder ses pouvoirs ; <br/>
              - le juge des référés du tribunal administratif de Paris ne pouvait ordonner à l'administration d'organiser le retour de Mme B...en l'absence d'une nouvelle décision régularisant sa situation administrative en France ;<br/>
              - c'est à tort que le juge des référés du tribunal administratif de Paris a considéré que la condition d'urgence était remplie ; <br/>
              - c'est à tort qu'il a estimé que l'arrêté préfectoral portait une atteinte grave et manifestement illégale au droit de l'intéressée. <br/>
<br/>
<br/>
              Par un mémoire en défense, enregistré le 7 avril 2015, et un mémoire complémentaire, enregistré le 8 avril 2015, Mme B... conclut au rejet du recours et à ce qu'il soit enjoint au préfet de police d'organiser, sans délai et aux frais de l'Etat français, son retour en France. Elle soutient que les moyens soulevés par le ministre ne sont pas fondés.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ; <br/>
<br/>
              - le code de l'entrée et du séjour des étrangers ;<br/>
<br/>
              - le code de justice administrative ; <br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, le ministre de l'intérieur, d'autre part, Mme B...; <br/>
<br/>
              Vu le procès-verbal de l'audience publique du 8 avril 2015 à 10 heures au cours de laquelle ont été entendus : <br/>
<br/>
              - les représentants du ministre de l'intérieur ;<br/>
<br/>
              - Me Pinatel, avocat au Conseil d'Etat et à la Cour de Cassation, avocat de MmeB... ;<br/>
<br/>
              - les représentants de Mme B...;<br/>
              et à l'issue de laquelle le juge des référés a clos l'instruction ; <br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. Le juge des référés se prononce dans un délai de quarante-huit heures " <br/>
<br/>
              2. Considérant qu'en vertu de l'article L. 521-2 du code de justice administrative, il appartient au juge administratif des référés d'ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une autorité administrative aurait porté une atteinte grave et manifestement illégale ; que l'usage par le juge des référés des pouvoirs qu'il tient de cet article est ainsi subordonné au caractère grave et manifeste de l'illégalité à l'origine d'une atteinte à une liberté fondamentale ;<br/>
<br/>
              3. Considérant que MmeB..., de nationalité gabonaise, entrée en France en février 2010, a fait l'objet d'une obligation de quitter le territoire français par le préfet de police le 10 février 2015 et a été placée en rétention ; que, le 12 février 2015, dans le délai de recours, elle a saisi le tribunal administratif de Paris d'une requête tendant à l'annulation de l'obligation de quitter le territoire ; que, toutefois, en raison d'un dysfonctionnement du greffe du tribunal administratif, cette requête n'a pas été notifiée à l'administration qui n'a pas eu connaissance de ce que l'intéressée avait formé un recours ; que l'administration a, dans la matinée du 6 mars 2015, engagé l'exécution de l'éloignement de l'intéressée qui, parallèlement, a informé son avocat de sa situation afin qu'il introduise en urgence un recours contre son éloignement ; que, saisi sur le fondement de l'article L. 521-2 du code de justice administrative, le juge des référés du tribunal administratif de Paris a enjoint au préfet de police d'organiser, dans les meilleurs délais et aux frais de l'Etat, le retour de Mme B... en France, par une ordonnance du 16 mars 2015 dont le ministre de l'intérieur relève appel ;<br/>
<br/>
              4. Considérant que le ministre de l'intérieur soutient que la requête présentée pour Mme B...était tardive ; qu'il résulte toutefois de l'instruction, et notamment des échanges au cours de l'audience, que Mme B...a été informée le 6 mars vers 6 heures du matin, alors qu'elle se trouvait en rétention administrative, que l'obligation de quitter le territoire français dont elle faisait l'objet depuis le 10 février 2015 allait être mise à exécution dans les prochaines heures par son embarquement dans un avion à destination de Libreville (Gabon) ; que l'intéressée, qui pensait que le recours qu'elle avait formé devant le tribunal administratif de Paris contre l'obligation de quitter le territoire français empêchait l'exécution de cette mesure tant que le tribunal ne s'était pas prononcé, en raison de l'effet suspensif qui s'attache à ce recours, s'est efforcée de joindre en urgence son avocat afin qu'il dépose un recours contre l'éloignement dont elle faisait l'objet ; que ce recours a finalement été enregistré au greffe du tribunal administratif ce même jour à 11 heures 43 ; que si, dans le même temps, Mme B...avait été embarquée vers 10 heures 45 dans un avion à destination de Libreville, le ministre n'est en tout état de cause pas fondé à soutenir que le recours dont a été saisi le tribunal administratif était tardif, eu égard à l'enchainement très rapide des faits et à l'incertitude qui en résultait quant à la situation précise dans laquelle se trouvait concrètement Mme B...au regard de l'exécution de la mesure d'éloignement lorsque le tribunal a été saisi ; <br/>
<br/>
              5. Considérant qu'aux termes de l'article L. 512-3 du code de l'entrée et du séjour des étrangers et du droit d'asile : " (...) L'obligation de quitter le territoire français ne peut faire l'objet d'une exécution d'office ni avant l'expiration du délai de départ volontaire ou, si aucun délai n'a été accordé, avant l'expiration d'un délai de quarante-huit heures suivant sa notification par voie administrative, ni avant que le tribunal administratif n'ait statué s'il a été saisi. " ; que l'introduction d'un recours contre l'obligation de quitter le territoire français a par elle-même pour effet de suspendre l'exécution de la mesure d'éloignement jusqu'à ce que le tribunal administratif se soit prononcé, afin d'assurer à ce recours son caractère effectif ; <br/>
<br/>
              6. Considérant qu'en exécutant l'obligation de quitter le territoire français dont faisait l'objet Mme B...alors même que le recours qu'elle avait formé devant le tribunal administratif contre cette mesure était encore pendant, l'administration a porté une atteinte grave et manifestement illégale au droit de l'intéressée de disposer d'un recours effectif ; que la circonstance que l'administration ignorait que Mme B...avait formé un recours contre l'obligation de quitter le territoire français lorsqu'elle a procédé à l'exécution de cette mesure en raison de l'erreur commise par le greffe du tribunal administratif est sans incidence ; <br/>
<br/>
              7. Considérant que si le ministre soutient que c'est à tort que le juge des référés de première instance a regardé la condition d'urgence comme remplie, alors que l'audience sur le recours formé par Mme B...devant le tribunal administratif de Paris contre la mesure l'obligeant à quitter le territoire français s'est tenue le 2 avril et que le tribunal a mis son jugement en délibéré jusqu'au 16 avril, l'éloignement de MmeB..., en dépit de l'effet suspensif qui s'attache à son recours, porte à son droit à un recours effectif, qui implique notamment son droit à ne pas être éloignée jusqu'au prononcé du jugement statuant sur son recours, une atteinte grave et immédiate à laquelle il doit être mis fin de manière urgente ; <br/>
<br/>
              8. Considérant qu'il résulte de ce qui précède qu'en enjoignant à l'administration de prendre toutes les mesures nécessaires pour organiser dans les meilleurs délais et aux frais de l'Etat le retour de Mme B...en France, le ministre de l'intérieur, qui n'a pas fait valoir d'élément tenant à ce qu'il serait matériellement impossible à l'Etat de prendre les mesures propres à assurer l'exécution de cette injonction, n'est pas fondé à soutenir que le juge des référés du tribunal administratif de Paris aurait excédé son office ;<br/>
<br/>
              9. Considérant qu'il résulte de ce qui précède que le ministre de l'intérieur n'est pas fondé à soutenir que c'est à tort que, par l'ordonnance attaquée, le juge des référés du tribunal administratif de Paris a enjoint au préfet de police d'organiser le retour de MmeB... ;<br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
------------------<br/>
Article 1er : Le recours du ministre de l'intérieur est rejeté.<br/>
Article 2 : La présente ordonnance sera notifiée au ministre de l'intérieur et à Mme A...B....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
