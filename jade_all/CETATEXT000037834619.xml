<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037834619</ID>
<ANCIEN_ID>JG_L_2018_12_000000419774</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/83/46/CETATEXT000037834619.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème - 8ème chambres réunies, 19/12/2018, 419774, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-12-19</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>419774</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème - 8ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROUSSEAU, TAPIE ; SCP WAQUET, FARGE, HAZAN</AVOCATS>
<RAPPORTEUR>M. Christian Fournier</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emmanuelle Cortot-Boucher</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:419774.20181219</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              M. B... A...a demandé au juge des référés du tribunal administratif de Paris, d'une part, d'ordonner, sur le fondement des dispositions de l'article L. 521-1 du code de justice administrative, la suspension de l'exécution de la décision du 29 janvier 2018 par laquelle le directeur rabbinique de la cacherout de l'association consistoriale israélite de Paris a révoqué son autorisation de sacrificateur rituel et, d'autre part, d'enjoindre à la commission rabbinique intercommunautaire de l'abattage rituel d'informer, au plus tard dans les huit jours, les préfets, à l'exception de ceux d'Alsace-Moselle, et le ministre chargé de l'agriculture de ce qu'il est habilité en qualité sacrificateur rituel, sous astreinte de 1 500 euros par jour de retard.<br/>
<br/>
              Par une ordonnance n° 1804591 du 23 mars 2018, le juge des référés du tribunal administratif de Paris a rejeté cette demande.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 11 et 26 avril au secrétariat du contentieux du Conseil d'Etat, M. A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) de mettre à la charge de l'association consistoriale israélite de Paris la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code rural et de la pêche maritime ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Christian Fournier, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Emmanuelle Cortot-Boucher, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Rousseau, Tapie, avocat de M. A...et à la SCP Waquet, Farge, Hazan, avocat de l'association consistoriale israélite de Paris ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision (...) ".<br/>
<br/>
              2. Il ressort des pièces du dossier soumis au juge des référés que, par une décision du 29 janvier 2018, le directeur rabbinique de la cacherout de l'association consistoriale israélite de Paris n'a pas renouvelé l'habilitation délivrée à M. A... le 14 mars 2016 l'autorisant à procéder à l'abattage rituel en France. Par une ordonnance du 23 mars 2018, le juge des référés du tribunal administratif de Paris a rejeté la demande de M. A... tendant, d'une part, à ce qu'il suspende, sur le fondement des dispositions de l'article L. 521-1 du code de justice administrative, l'exécution de cette décision et, d'autre part, à ce qu'il enjoigne à la commission rabbinique intercommunautaire de l'abattage rituel d'informer, au plus tard dans les huit jours, les préfets, à l'exception de ceux d'Alsace-Moselle, et le ministre chargé de l'agriculture de ce qu'il est habilité sacrificateur rituel, sous astreinte de 1 500 euros par jour de retard.<br/>
<br/>
              3. Indépendamment des cas dans lesquels le législateur a lui-même entendu reconnaître ou, à l'inverse, exclure l'existence d'un service public, une personne privée qui assure une mission d'intérêt général sous le contrôle de l'administration et qui est dotée à cette fin de prérogatives de puissance publique est chargée de l'exécution d'un service public. Même en l'absence de telles prérogatives, une personne privée doit également être regardée, dans le silence de la loi, comme assurant une mission de service public lorsque, eu égard à l'intérêt général de son activité, aux conditions de sa création, de son organisation ou de son fonctionnement, aux obligations qui lui sont imposées ainsi qu'aux mesures prises pour vérifier que les objectifs qui lui sont assignés sont atteints, il apparaît que l'administration a entendu lui confier une telle mission.<br/>
<br/>
              4. Aux termes de l'article L. 214-3 du code rural et de la pêche maritime : " Il est interdit d'exercer des mauvais traitements envers les animaux domestiques ainsi qu'envers les animaux sauvages apprivoisés ou tenus en captivité. / Des décrets en Conseil d'Etat déterminent les mesures propres à assurer la protection de ces animaux contre les mauvais traitements ou les utilisations abusives et à leur éviter des souffrances lors des manipulations inhérentes aux diverses techniques d'élevage, de parcage, de transport et d'abattage des animaux (...) ". Aux termes de l'article R. 214-70 du même code : " I. - L'étourdissement des animaux est obligatoire avant l'abattage ou la mise à mort, à l'exception des cas suivants : / 1° Si cet étourdissement n'est pas compatible avec la pratique de l'abattage rituel ; (...) ". Aux termes de l'article R. 217-75 du même code : " Sous réserve des dispositions du troisième alinéa du présent article, l'abattage rituel ne peut être effectué que par des sacrificateurs habilités par les organismes religieux agréés, sur proposition du ministre de l'intérieur, par le ministre chargé de l'agriculture. / Les organismes agréés mentionnés à l'alinéa précédent doivent faire connaître au ministre chargé de l'agriculture le nom des personnes habilitées et de celles auxquelles l'habilitation a été retirée. / Si aucun organisme religieux n'a été agréé, le préfet du département dans lequel est situé l'abattoir utilisé pour l'abattage rituel peut accorder des autorisations individuelles sur demande motivée des intéressés (...) ".<br/>
<br/>
              5. D'une part, il ressort des pièces du dossier soumis au juge des référés que l'habilitation accordée par les organismes religieux agréés aux sacrificateurs afin qu'ils pratiquent l'abattage rituel dans des abattoirs ayant reçu un agrément des services vétérinaires est accordée uniquement en fonction de critères religieux et que la carte qui leur est délivrée mentionne les établissements où ils interviennent. La seule obligation qui s'impose aux organismes agréés à l'égard de l'administration est de transmettre la liste des sacrificateurs habilités aux préfets des départements où ils interviennent. Les sacrificateurs, qui sont tenus de justifier de cette habilitation aux agents chargés du contrôle des abattoirs, compte tenu de ce que leur pratique rituelle déroge à l'obligation d'étourdissement, doivent par ailleurs détenir un certificat de compétence en protection des animaux et avoir reçu une formation en matière de sécurité sanitaire des aliments. D'autre part, il ne résulte pas des dispositions précitées qu'en confiant aux organismes religieux agréés par le ministre chargé de l'agriculture, sur proposition du ministre de l'intérieur, la mission d'habiliter les sacrificateurs à procéder à l'abattage des animaux sans étourdissement préalable selon la pratique de l'abattage rituel, le Premier ministre ait entendu reconnaître que l'habilitation revête le caractère d'un service public. Ni ces dispositions ni aucune autre disposition n'attribuent l'exercice de prérogatives de puissance publique à ces organismes. En tout état de cause, les conditions de création, d'organisation, de fonctionnement et de financement des organismes religieux agréés ne permettent pas de les regarder comme étant chargés d'une mission de service public. Par suite, alors même que l'agrément des organismes religieux pour accorder cette habilitation est placé sous le contrôle du juge administratif, les décisions d'habilitation des sacrificateurs rituels ne présentent pas le caractère d'actes administratifs. Dès lors, le juge des référés n'a pas commis d'erreur de droit en jugeant que les circonstances qu'en application de la réglementation, les sacrificateurs sont habilités par des organismes religieux agréés et que le préfet peut accorder des autorisations individuelles en l'absence d'organisme religieux agréé ne sauraient avoir pour effet de conférer aux décisions prises par les organismes religieux agréés pour l'habilitation ou le retrait de l'habilitation d'un sacrificateur rituel le caractère de décisions administratives soumises au contrôle du juge administratif. En conséquence, M. A... n'est pas fondé à demander l'annulation de l'ordonnance qu'il attaque.<br/>
<br/>
              6. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'association consistoriale israélite de Paris, qui n'est pas, dans la présente instance, la partie perdante. Il n'y a pas lieu, dans les circonstances de l'espèce, de mettre une somme à la charge de M. A... au titre de ces mêmes dispositions.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de M. A... est rejeté.<br/>
Article 2 : Les conclusions de l'association consistoriale israélite de Paris présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La présente décision sera notifiée à M. B... A...et à l'association consistoriale israélite de Paris.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
