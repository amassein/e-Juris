<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032928903</ID>
<ANCIEN_ID>JG_L_2016_07_000000398032</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/92/89/CETATEXT000032928903.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème chambre, 22/07/2016, 398032, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-07-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>398032</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>BALAT</AVOCATS>
<RAPPORTEUR>Mme Emmanuelle Petitdemange</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Benoît Bohnert</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2016:398032.20160722</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La Commission nationale des comptes de campagne et des financements politiques a saisi le tribunal administratif de Nancy, en application des dispositions de l'article L. 52-15 du code électoral, de sa décision du 6 novembre 2014 constatant, d'une part, l'absence de dépôt, par un expert-comptable, du compte de campagne de M. D... et de MmeB..., candidats aux élections départementales qui se sont déroulées les 22 et 29 mars 2015 dans le canton de Gérardmer (Vosges) et, d'autre part, un don d'une personne morale.<br/>
<br/>
              Par un jugement n° 1503323 et 1503324 du 18 février 2016, le tribunal administratif a confirmé le rejet du compte de campagne de M. D...et Mme B...et les a déclarés inéligibles pour une durée d'un an.<br/>
<br/>
              Par une requête et un mémoire complémentaire, enregistrés les 17 mars et le 15 avril 2016 au secrétariat du contentieux du Conseil d'Etat, M. D...et Mme B...demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) de ne pas donner suite à la saisine de la Commission nationale des comptes de campagne et des financements politiques ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 000 euros en application des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code électoral ;<br/>
              - la loi n° 2011-412 du 14 avril 2011 portant simplification de dispositions du code électoral et relative à la transparence financière de la vie politique ;<br/>
                          - le code de justice administrative ; 		<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Emmanuelle Petitdemange, auditeur,  <br/>
<br/>
              - les conclusions de M. Benoît Bohnert, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Balat, avocat de M. D...et de Mme B...;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'aux termes du deuxième alinéa de l'article L. 52-8 du code électoral : " Les personnes morales, à l'exception des partis ou groupements politiques, ne peuvent participer au financement de la campagne électorale d'un candidat, ni en lui consentant des dons sous quelque forme que ce soit, ni en lui fournissant des biens, services ou autres avantages directs ou indirects à des prix inférieurs à ceux qui sont habituellement pratiqués " ; qu'aux termes de l'article L. 52-12 du même code : " Chaque candidat ou candidat tête de liste soumis au plafonnement prévu à l'article L. 52-11 et qui a obtenu au moins 1 % des suffrages exprimés est tenu d'établir un compte de campagne (...) / Le compte de campagne est présenté par un membre de l'ordre des experts-comptables et des comptables agréés ; celui-ci met le compte de campagne en état d'examen et s'assure de la présence des pièces justificatives requises. Cette présentation n'est pas nécessaire lorsqu'aucune dépense ou recette ne figure au compte de campagne. Dans ce cas, le mandataire établit une attestation d'absence de dépense et de recette. Cette présentation n'est pas non plus nécessaire lorsque le candidat ou la liste dont il est tête de liste a obtenu moins de 1 % des suffrages exprimés et qu'il n'a pas bénéficié de dons de personnes physiques selon les modalités prévues à l'article 200 du code général des impôts (...) " ;  qu'aux termes de l'article L. 52-15 du ce code : " La Commission nationale des comptes de campagne et des financements politiques approuve et, après procédure contradictoire, rejette ou réforme les comptes de campagne. / (...) Lorsque la commission a constaté que le compte de campagne n'a pas été déposé dans le délai prescrit, (...) la commission saisit le juge de l'élection (...) " ; qu'enfin, aux termes du deuxième alinéa de l'article L. 118-3 du même code, dans sa rédaction issue de la loi du 14 avril 2011 : " Saisi par la commission instituée par l'article L. 52-14, (...) le juge de l'élection peut déclarer inéligible le candidat qui n'a pas déposé son compte de campagne dans les conditions et le délai prescrits à l'article L. 52-12. / " ;   <br/>
<br/>
              2. Considérant que, par une décision du 16 novembre 2015, la Commission nationale des comptes de campagne et des financements politiques a constaté que le compte de campagne de M. D...et MmeB..., candidats aux élections qui se sont déroulées les 22 et 29 mars 2015 en vue de la désignation des conseillers départementaux du canton de Gérardmer (Vosges), n'avait pas été déposé par un expert-comptable, contrairement aux exigences de l'article L. 52-12 du code électoral, et faisait apparaître un don d'une personne morale, en méconnaissance  de l'article L. 52-8 du même code ; que, saisi en application de l'article L. 52-15 de ce code, le tribunal administratif de Nancy, par un jugement du 18 février 2016, a confirmé le rejet du compte de campagne et déclaré M. D...et Mme B... inéligibles pour une durée d'un an ; que M. D...et Mme B...relèvent appel de ce jugement ; <br/>
<br/>
              Sur la régularité du jugement attaqué :<br/>
<br/>
              3. Considérant qu'il ressort des pièces du dossier de première instance, en particulier de la minute du jugement attaqué, que le tribunal administratif de Nancy a visé et analysé le mémoire en défense produit par les requérants et enregistré au greffe du tribunal le 17 janvier 2016 ; qu'ainsi, le moyen tiré de ce que le tribunal aurait entaché son jugement d'irrégularité en ne visant pas ce mémoire doit être écarté ;<br/>
<br/>
              4. Considérant qu'il résulte des dispositions précitées de l'article L. 118-3 du code électoral, éclairées par les travaux préparatoires de la loi du 14 avril 2011 dont elles sont issues, que la déclaration d'inéligibilité d'un candidat qui n'a pas déposé son compte de campagne dans les conditions et le délai prescrits à l'article L. 52-12 du même code est une faculté dont dispose le juge de l'élection qui doit prendre en compte, pour déterminer s'il y a lieu d'en faire usage, la nature de la règle méconnue, le caractère délibéré ou non du manquement, l'existence éventuelle d'autres motifs d'irrégularité du compte, le montant des sommes en cause ainsi que l'ensemble des circonstances de l'espèce ; qu'ainsi, contrairement à ce que font valoir M. D...et MmeB..., il relève de l'office du juge de l'élection, saisi sur le fondement de l'article L. 52-15 du code électoral, même en l'absence de conclusions de la Commission nationale des comptes de campagne et de financement de la vie politique en ce sens, de déclarer, le cas échéant, un candidat inéligible ; <br/>
<br/>
              5. Considérant que M. D...et MmeB..., dont il ressort des pièces du dossier de première instance, au demeurant, qu'ils avaient présenté devant les premiers juges des observations tendant à ce que ces derniers ne prononcent pas leur inéligibilité, soutiennent en appel que le tribunal administratif ne pouvait prononcer celle-ci, en tout état de cause, en l'absence de conclusions en ce sens de la Commission nationale des comptes de campagne et de financement de la vie politique, qu'après les avoir informés qu'il entendait relever un moyen d'office et les avoir invités à présenter leurs observations sur ce moyen dans les formes prévues par l'article R. 611-7 du code de justice administrative, aux termes duquel " lorsque la décision lui paraît susceptible d'être fondée sur un moyen relevé d'office, le président de la formation de jugement ou, au Conseil d'Etat, la sous-section chargée de l'instruction en informe les parties avant la séance de jugement et fixe le délai dans lequel elles peuvent, sans qu'y fasse obstacle la clôture éventuelle de l'instruction, présenter leurs observations sur le moyen communiqué (...) " ; que, toutefois, ces dispositions ne s'appliquent pas aux sanctions d'inéligibilité susceptibles d'être prononcées d'office par le juge sur le fondement des dispositions précitées de l'article L. 118-3 du code électoral ; qu'ainsi, le moyen tiré de ce que le tribunal administratif aurait entaché son jugement d'irrégularité en s'abstenant d'informer les requérants, avant la séance, de son intention de les déclarer inéligibles doit être écarté ;<br/>
<br/>
              Sur le bien-fondé du rejet du compte de campagne et de l'inéligibilité prononcée :<br/>
<br/>
              6. Considérant que l'omission de la présentation du compte de campagne par un membre de l'ordre des experts-comptables, qui prive la Commission nationale des comptes de campagne et du financement de la vie politique de toute possibilité de s'assurer de l'exactitude et de la sincérité des comptes soumis à son examen, constitue un manquement à une obligation substantielle, qui justifie le rejet du compte ; qu'il résulte de l'instruction que le compte de campagne du binôme électoral constitué de M. D...et de MmeB..., élu au second tour, n'a pas été présenté par un membre de l'ordre des experts-comptables et que ce défaut n'a pas été régularisé devant la Commission nationale des comptes de campagne et du financement de la vie politique ; que M. D...et Mme B...ne sont donc pas fondés à soutenir que c'est à tort que le tribunal administratif de Nancy a confirmé le rejet de leur compte de campagne ;  <br/>
<br/>
              7. Considérant qu'il résulte de l'instruction que ce manquement à la règle instituée par l'article L. 52-12 du code électoral, eu égard à la circonstance que M. D...avait déjà été candidat ou élu à d'autres élections et en l'absence d'ambiguïté des règles applicables, doit être regardé comme présentant un caractère délibéré ; que, dans ces circonstances, et alors que, en outre, il résulte également de l'instruction que les candidats ont perçu, en méconnaissance des dispositions de l'article L 52-8 du même code, une somme de 650 euros en provenance de l'association Daermanau, dont M. D...est le président, M. D... et Mme B...ne sont pas fondés à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Nancy les a déclarés démissionnaires d'office et a prononcé leur inéligibilité pour une durée d'un an à compter de la date de son jugement ; <br/>
<br/>
              Sur les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              8. Considérant que ces dispositions font obstacle à ce qu'une somme soit mise à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>               D E C I D E :<br/>
                             --------------<br/>
<br/>
Article 1er : La requête de M. D...et Mme B...est rejetée.<br/>
<br/>
Article 2 : La présente décision sera notifiée à M. A...D..., Mme C...B...et à la Commission nationale des comptes de campagne et des financements politiques.<br/>
Copie en sera adressée, pour information, au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
