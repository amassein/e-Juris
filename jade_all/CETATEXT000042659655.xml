<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042659655</ID>
<ANCIEN_ID>JG_L_2020_12_000000436388</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/65/96/CETATEXT000042659655.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème - 8ème chambres réunies, 11/12/2020, 436388</TITRE>
<DATE_DEC>2020-12-11</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>436388</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème - 8ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GASCHIGNARD ; SCP CELICE, TEXIDOR, PERIER</AVOCATS>
<RAPPORTEUR>M. Laurent-Xavier Simonel</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Laurent Cytermann</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:436388.20201211</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              La Société générale a demandé au juge des référés du tribunal administratif d'Orléans de condamner la commune de Thiron-Gardais à lui verser, à titre provisionnel en application de l'article R. 541-1 du code de justice administrative, la somme de 206 298,96 euros, majorée des intérêts au taux légal, en paiement de sa créance pour des redevances d'occupation dues par cette commune en vertu d'une convention de mise à disposition de locaux destinés à la Gendarmerie nationale et édifiés sur un terrain appartenant à la commune.<br/>
<br/>
              Par une ordonnance n° 1801578 du 8 novembre 2018, le juge des référés du tribunal administratif d'Orléans a condamné la commune de Thiron-Gardais à verser à la Société générale, à titre de provision, la somme de 206 298,96 euros, assortie d'intérêts au taux légal, soit à compter du 24 avril 2018 pour la somme de 146 856,80 euros correspondant aux redevances impayées dues aux 1er mars, 1er juin, 1er septembre et 1er décembre 2017 ainsi qu'au 1er mars 2018, échues avant cette date du 24 avril 2018 à laquelle la commune a reçu la mise en demeure de les payer et à compter respectivement du 1er juin 2018 et du 1er septembre 2018 pour les deux sommes de 29 721,08 euros correspondant aux redevances impayées échues à ces deux dates.<br/>
<br/>
              Par un arrêt n° 18NT04092 du 15 novembre 2019, la cour administrative d'appel de Nantes a rejeté l'appel formé par la commune de Thiron-Gardais contre cette ordonnance et, sur l'appel incident de la Société générale, a porté le montant de la provision en faveur de la banque à laquelle la commune est condamnée à la somme de 266 929,96 euros assortie d'intérêts au taux légal, soit à compter du 24 avril 2018 pour la somme de 146 856,80 euros correspondant aux redevances impayées dues pour les échéances antérieures à cette date, d'une part et, d'autre part, à compter respectivement du 1er juin 2018 et du 1er septembre 2018 pour les deux sommes de 29 721,08 euros correspondant aux redevances impayées échues à ces deux dates et à compter respectivement du 1er décembre 2018 et du 1er mars 2019 pour les deux sommes de 30 315,50 euros correspondant aux échéances impayées échues à ces deux dates.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un nouveau mémoire, enregistrés au secrétariat de la section du contentieux les 2 et 18 décembre 2019 et le 28 janvier 2020, la commune de Thiron-Gardais demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) de mettre à la charge de la Société générale la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code civil ;<br/>
              - le code général des collectivités territoriales ; <br/>
              - le code monétaire et financier ;<br/>
              - la loi n° 80-539 du 16 juillet 1980 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Laurent-Xavier Simonel, conseiller d'Etat en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Laurent Cytermann, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Gaschignard, avocat de la commune de Thiron-Gardais et à la SCP Célice, Texidor, Perier, avocat de la Société générale ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que, par une lettre datée du 9 janvier 2013, la Société générale a, conformément à l'article L. 313-28 du code monétaire et financier, notifié au comptable public de la commune de Thiron-Gardais la cession, réalisée à son profit par la société SNC Thiron Gend, de la créance dont cette commune était débitrice et qui portait sur des redevances trimestrielles dues par celle-ci, dans le cadre d'un bail emphytéotique administratif relatif à la construction d'une caserne de gendarmerie. Par délibération du 3 février 2017, le conseil municipal a décidé de cesser le versement à la Société générale de la redevance en cause à compter de l'échéance du 1er mars 2017. Après avoir adressé, sans succès, une mise demeure à la commune, la Société générale a demandé au juge des référés du tribunal administratif d'Orléans de condamner la commune à lui verser, à titre provisionnel en application de l'article L. 541-1 du code de justice administrative, le montant des échéances échues depuis le 1er mars 2017 jusqu'au 1er mars 2018, puis a étendu, le 17 octobre 2018, sa demande aux échéances également restées impayées des 1er juin et 1er septembre 2018. Par une ordonnance du 8 novembre 2018, la commune a été condamnée à verser à la Société générale une somme de 206 298,96 euros, assortie d'intérêts au taux légal. La commune de Thiron-Gardais se pourvoit en cassation contre l'arrêt du 15 novembre 2019 par lequel la cour administrative d'appel de Nantes a rejeté son appel et a réformé l'ordonnance, sur appel incident de la Société générale, en portant le montant de la provision à une somme de 266 929,96 euros assortie d'intérêts au taux légal. <br/>
<br/>
              2. Aux termes du premier alinéa de l'article L. 313-23 du code monétaire et financier : " Tout crédit qu'un établissement de crédit (...) consent à une personne morale de droit privé ou de droit public, ou à une personne physique dans l'exercice par celle-ci de son activité professionnelle, peut donner lieu au profit de cet établissement (...) par la seule remise d'un bordereau, à la cession ou au nantissement par le bénéficiaire du crédit, de toute créance que celui-ci peut détenir sur un tiers, personne morale de droit public ou de droit privé ou personne physique dans l'exercice par celle-ci de son activité professionnelle. / (...) ". Aux termes de l'article L. 313-25 du même code : " Le bordereau est signé par le cédant. (...). La date est apposée par le cessionnaire ". Aux termes de l'article L. 313-27 du même code : " La cession ou le nantissement prend effet entre les parties et devient opposable aux tiers à la date apposée sur le bordereau lors de sa remise, quelle que soit la date de naissance, d'échéance ou d'exigibilité des créances, sans qu'il soit besoin d'autre formalité, et ce quelle que soit la loi applicable aux créances et la loi du pays de résidence des débiteurs. / (...) ". Aux termes de l'article L. 313-28 du même code : " L'établissement de crédit (...) peut, à tout moment, interdire au débiteur de la créance cédée ou nantie de payer entre les mains du signataire du bordereau. A compter de cette notification, dont les formes sont fixées par le décret en Conseil d'Etat prévu à l'article L. 313-35, le débiteur ne se libère valablement qu'auprès de l'établissement de crédit (...) ". Enfin, aux termes de l'article L. 313-29 du même code : " Sur la demande du bénéficiaire du bordereau, le débiteur peut s'engager à le payer directement : cet engagement est constaté, à peine de nullité, par un écrit intitulé : " Acte d'acceptation de la cession ou du nantissement d'une créance professionnelle ". / Dans ce cas, le débiteur ne peut opposer à l'établissement de crédit (...) les exceptions fondées sur ses rapports personnels avec le signataire du bordereau, à moins que l'établissement de crédit (...), en acquérant ou en recevant la créance, n'ait agi sciemment au détriment du débiteur ".<br/>
<br/>
              3. Il résulte de ces dispositions, qui sont applicables aux créances détenues sur des personnes morales de droit public, que la souscription par le débiteur d'une créance cédée, à la demande de l'établissement de crédit cessionnaire, de l'acte d'acceptation prévu par l'article L. 313-29 du code monétaire et financier a pour effet de créer à l'encontre de ce débiteur une obligation de paiement entre les mains du bénéficiaire du bordereau, détachée de la créance initiale de l'entreprise et contre laquelle il ne peut faire valoir des exceptions tirées de ses rapports avec l'entreprise cédante. <br/>
<br/>
              4. Il ressort des pièces du dossier soumis aux juges du fond que la commune de Thiron-Gardais a fait valoir, en première instance, des exceptions, reprises en appel, résultant de ses rapports avec la société SNC Thiron Gend et tirées, en particulier, de la modification des conditions de financement de la caserne de gendarmerie. Alors que ces exceptions avaient été écartées par le juge des référés du tribunal administratif au motif que la commune avait accepté, par un acte du 28 août 2007, la cession de créance litigieuse en application de l'article L. 313-29 du code monétaire et financier, la cour a commis une erreur de droit, en se bornant à juger que l'irrégularité de l'acte précité n'avait pas d'incidence sur les obligations financières de la commune et sans se prononcer sur ces exceptions. Dès lors et sans qu'il soit besoin de se prononcer sur les autres moyens du pourvoi, la commune est fondée à demander l'annulation de l'arrêt attaqué.<br/>
<br/>
              5. Il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application de l'article L. 821-1 du code de justice administrative.  <br/>
<br/>
              Sur l'appel principal de la commune de Thiron-Gardais :<br/>
<br/>
              6. Aux termes de l'article R. 541-1 du code de justice administrative : " Le juge des référés peut, même en l'absence d'une demande au fond, accorder une provision au créancier qui l'a saisi lorsque l'existence de l'obligation n'est pas sérieusement contestable. (...) ".<br/>
<br/>
              7. En premier lieu, il résulte de l'instruction que, dans le cadre de la convention d'escompte, conclue le 29 août 2007, entre la Société générale et la société SNC Thiron Gend, la seconde a remis à la première, après l'avoir signé, le bordereau mentionné à l'article L. 313-23 du code monétaire et financier, portant sur la créance en litige, sur lequel la banque a apposé la date du 7 février 2008 et que cette dernière a notifié la cession de créance intervenue au comptable public de la commune de Thiron-Gardais, ainsi qu'il a été dit, le 9 janvier 2013. Par suite, le moyen tiré de ce que, faute de bordereau, les dispositions de l'article L. 313-28 du code précité sont inapplicables, manque en fait.<br/>
<br/>
              8. En deuxième lieu, eu égard à ses conséquences pour le débiteur cédé rappelées au point 3, l'acceptation d'une cession de créance effectuée dans les conditions prévues par l'article L. 313-23 du code monétaire et financier ne peut intervenir avant que cette cession ait pris effet et doit résulter d'un acte postérieur à la date apposée par le cessionnaire sur le bordereau après qu'il lui a été remis. <br/>
<br/>
              9. Il est constant que l'acte d'acceptation dont la Société générale se prévaut pour écarter comme inopérantes à son encontre les exceptions tirées par la commune de Thiron-Gardais de ses rapports avec la société SNC Thiron Gend a été signé dès le 28 août 2007, alors que la date apposée sur le bordereau de la cession de créance litigieuse est le 7 février 2008. Par suite, cette acceptation est irrégulière et il y a lieu pour le Conseil d'Etat, saisi de l'ensemble du litige par l'effet dévolutif de l'appel, de se prononcer sur les exceptions précitées et d'examiner les autres moyens soulevés par la Société générale devant le juge des référés et devant la cour.<br/>
<br/>
              10. Il résulte de l'instruction, tout d'abord, que la substitution de la SNC Thiron Gend dans les droits d'emphytéote de la société Solefim qui avait initialement conclu le bail mentionné au point 1 et dans les droits que celle-ci tenait de la convention de mise à disposition du terrain d'assiette de la gendarmerie conclue le 4 avril 2006, n'a pas donné lieu, comme le soutient la commune, à une simple lettre recommandée mais résulte de l'agrément stipulé par l'avenant n° 1 au bail précité, conclu le 28 août 2007, conformément à l'article 1.4.2 de ce bail et à l'article 1.8 de la convention. <br/>
<br/>
              11. Il est, ensuite, constant que les évolutions de la répartition des titres de la SNC Thiron Gend ayant abouti à la transmission de leur quasi-intégralité à la société Levaux et associés après qu'elle a absorbé la société Solefim en 2011, n'ont entraîné aucune modification de la personnalité juridique de l'emphytéote et ne relevaient pas des transferts de restructuration capitalistique, au demeurant libres à l'intérieur du groupe de l'emphytéote, prévus à l'article 1.4.3 du bail et à l'article 1.9 de la convention de mise à disposition.<br/>
<br/>
              12. Enfin, il résulte de l'instruction que les paramètres détaillés de l'évolution de la charge constituée pour la commune de Thiron-Gardais par la redevance en litige, résultant du passage d'un taux variable à un taux fixe annuel de 5,07 % assorti d'une progressivité des annuités, lui ont été communiqués à temps et avec un détail suffisant pour lui permettre d'apprécier l'écart pouvant survenir entre cette charge et la ressource reçue de l'Etat au titre de l'utilisation finale de l'ouvrage. Dès lors le moyen tiré par la commune de l'existence de manoeuvres intentionnellement commises à son détriment par la Société générale manque en fait.<br/>
<br/>
              13. Il résulte de ce qui précède que la créance dont la Société générale poursuit le recouvrement à l'encontre de la commune de Thiron-Gardais doit être regardée, en l'état de l'instruction, comme n'étant pas sérieusement contestable. La commune n'est, par suite, pas fondée à se plaindre de ce que, par l'ordonnance attaquée, le juge des référés du tribunal administratif d'Orléans l'a condamnée à payer à la Société générale une provision de 206 298,96 euros en principal. <br/>
<br/>
              Sur l'appel incident de la Société générale :<br/>
<br/>
              14. Il est constant, notamment au regard des pièces produites pour la première fois en appel par la Société générale, que la commune de Thiron-Gardais ne lui a pas payé la redevance due le 1er décembre 2018 et le 1er mars 2019, à chaque fois pour un montant de 30 315,50 euros. Si ces créances sont apparues postérieurement à l'ordonnance attaquée, elles sont rattachables au même fait générateur que celui qui était initialement invoqué. Par suite, la Société générale est fondée à demander que le montant de la provision soit porté à 266 929, 96 euros.<br/>
<br/>
              Sur les intérêts moratoires :<br/>
<br/>
              15. Lorsqu'ils ont été demandés, et quelle que soit la date de cette demande, les intérêts moratoires courent à compter du jour où la demande de paiement du principal est parvenue au débiteur ou, en l'absence d'une telle demande préalablement à la saisine du juge, à compter du jour de cette saisine.<br/>
<br/>
              16. Par suite et sans qu'ait d'incidence à cet égard la délibération du 3 février 2017 par laquelle le conseil municipal de Thiron-Gardais a décidé de consigner les émoluments de la créance cédé en ne les payant plus à la banque cessionnaire à compter de l'échéance du 1er mars 2017, la Société générale est fondée à demander à ce que les intérêts au taux légal courent à compter :<br/>
              - du 24 avril 2018, date de la réception par la commune de sa mise en demeure de les payer, sur la somme de 146 856,80 euros correspondant aux échéances dues au 1er mars, 1er juin, 1er septembre et 1er décembre 2017 ainsi qu'au 1er mars 2018 ; <br/>
              - du 17 octobre 2018, date de sa demande au juge des référés les concernant, sur la somme de 59 442,16 euros correspondant aux échéances dues au 1er juin et 1er septembre 2018 ; <br/>
              - du 27 février 2019, date de sa demande à la cour la concernant, sur le montant de 30 315,50 euros correspondant à l'échéance due au 1er décembre 2018 ;<br/>
              - et du 6 juin 2019, date de sa demande à la cour la concernant, sur le montant de 30 315,50 euros correspondant à l'échéance due au 1er mars 2019.<br/>
<br/>
              Sur les conclusions aux fins d'injonction sous astreinte :<br/>
<br/>
              17. En cas d'inexécution de la présente décision, les dispositions du II de l'article 1er de la loi du 16 juillet 1980 relative aux astreintes prononcées en matière administrative et à l'exécution des jugements par les personnes morales de droit public, reproduites au I de l'article L. 911-9 du code de justice administrative, permettent à la Société générale d'en obtenir le mandatement d'office, dans les conditions qui y sont prévues. Dès lors, il n'y a pas lieu de faire droit à ses conclusions accessoires à fin d'injonction sous astreinte.<br/>
<br/>
              Sur les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              18. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la commune de Thiron-Gardais la somme de 3 000 euros à verser à la Société générale au titre des dispositions de l'article L. 761-1 du code de justice administrative. Ces dispositions font obstacle à ce que soit mis à la charge de cette dernière, qui n'est pas la partie perdante dans la présente instance, le versement de la somme que demande, à ce titre, la commune de Thiron-Gardais. <br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative de Nantes du 15 novembre 2019 est annulé.<br/>
Article 2 : La requête de la commune de Thiron-Gardais présentée devant la cour est rejetée.<br/>
Article 3 : La provision que la commune de Thiron-Gardais est condamnée à verser à la Société Générale est portée à la somme de 266 929,96 euros. <br/>
Article 4 : La somme de 146 856,80 euros est assortie des intérêts au taux légal à compter du 24 avril 2018, celle de 59 442,16 euros à compter du 17 octobre 2018, une première somme de 30 315,50 euros à compter du 27 février 2019 et une seconde somme de 30 315,50 euros à compter du 6 juin 2019.<br/>
Article 5 : L'ordonnance du juge des référés du tribunal administratif d'Orléans du 8 novembre 2018 est réformée en ce qu'elle a de contraire à la présente décision.<br/>
Article 6 : Le surplus des conclusions des parties est rejeté.<br/>
Article 7 : La présente décision sera notifiée à la commune de Thiron-Gardais et à la Société générale.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">13-06 CAPITAUX, MONNAIE, BANQUES. RÉGLEMENTATION DU CRÉDIT. - CESSION DE CRÉANCE PROFESSIONNELLE (ART. L. 313-23 À L. 313-34 DU CMF) [RJ1] - ACCEPTATION ANTÉRIEURE À LA DATE APPOSÉE SUR LE BORDEREAU DAILLY - ACCEPTATION SANS PORTÉE [RJ2].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">18-05 COMPTABILITÉ PUBLIQUE ET BUDGET. DETTES DES COLLECTIVITÉS PUBLIQUES - QUESTIONS DIVERSES. - CESSION DE CRÉANCE PROFESSIONNELLE (ART. L. 313-23 À L. 313-34 DU CMF) [RJ1] - ACCEPTATION ANTÉRIEURE À LA DATE APPOSÉE SUR LE BORDEREAU DAILLY - ACCEPTATION SANS PORTÉE [RJ2].
</SCT>
<ANA ID="9A"> 13-06 Eu égard à ses conséquences pour le débiteur cédé, l'acceptation d'une cession de créance effectuée dans les conditions prévues par l'article L. 313-23 du code monétaire et financier (CMF) ne peut intervenir avant que cette cession ait pris effet et doit résulter d'un acte postérieur à la date apposée par le cessionnaire sur le bordereau après qu'il lui a été remis.</ANA>
<ANA ID="9B"> 18-05 Eu égard à ses conséquences pour le débiteur cédé, l'acceptation d'une cession de créance effectuée dans les conditions prévues par l'article L. 313-23 du code monétaire et financier (CMF) ne peut intervenir avant que cette cession ait pris effet et doit résulter d'un acte postérieur à la date apposée par le cessionnaire sur le bordereau après qu'il lui a été remis.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf., sur l'applicabilité de ces dispositions aux créances détenues sur des personnes morales de droit public, CE, 25 juin 2003, Caisse centrale de crédit mutuel du Nord de la France, n° 240679, p. 285.,,[RJ2] Rappr. Cass. Com., 3 novembre 2015, Société Dumez Méditerranée, n° 14-14.373, Bull. 2015, IV, n° 151.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
