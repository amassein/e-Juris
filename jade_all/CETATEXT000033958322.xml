<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033958322</ID>
<ANCIEN_ID>JG_L_2017_01_000000384076</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/95/83/CETATEXT000033958322.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème chambre, 30/01/2017, 384076, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-01-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>384076</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DIDIER, PINET</AVOCATS>
<RAPPORTEUR>Mme Déborah Coricon</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emmanuelle Cortot-Boucher</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2017:384076.20170130</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Par une requête, enregistrée au secrétariat du contentieux du Conseil d'Etat le 29 août 2014, MM. A...et C...B...demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret n° 2014-708 du 25 juin 2014 relatif à l'appellation d'origine contrôlée (AOC) " Bergerac ", le décret n° 2014-709 du 25 juin 2014 relatif à l'AOC " Côtes de Bergerac ", le décret n° 2014-718 du 26 juin 2014 relatif à l'AOC  " Haut-Montravel ", ainsi que le décret n° 2014-719 du 26 juin 2014 relatif à l'AOC " Côtes de Montravel " ;<br/>
<br/>
              2°) d'apprécier la légalité du décret n° 2011-1277 du 11 octobre 2011 relatif à l'AOC " Montravel " et de vérifier la légalité du futur cahier des charges de cette appellation lors de sa prochaine demande d'homologation. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le traité sur le fonctionnement de l'Union européenne ;<br/>
              - le règlement (UE) n° 1308/2013 du 17 décembre 2013 ;<br/>
              - le code rural et de la pêche maritime ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Déborah Coricon, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Emmanuelle Cortot-Boucher, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Didier, Pinet, avocat de l'Institut national de l'origine et de la qualité ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Aux termes du premier alinéa de l'article L. 641-7 du code rural et de la pêche maritime : " La reconnaissance d'une appellation d'origine contrôlée est prononcée par un décret qui homologue un cahier des charges où figurent notamment la délimitation de l'aire géographique de production de cette appellation ainsi que ses conditions de production ".<br/>
<br/>
              Sur les conclusions dirigées contre les décrets n° 2014-708 et 2014-709 du 25 juin 2014 relatifs aux appellations d'origine contrôlée (AOC)  " Bergerac " et " Côtes de Bergerac " :<br/>
<br/>
              2. Les décrets n° 2014-708 et 2014-709 du 25 juin 2014 relatifs, respectivement, aux AOC " Bergerac " et " Côtes de Bergerac " ont été publiés au Journal officiel de la République française le 27 juin 2014. Cette publication a fait courir le délai de recours de deux mois ouvert à l'encontre de ces décrets, qui a expiré le 28 août 2014. Dès lors que la requête de MM. B...n'a été enregistrée au secrétariat du contentieux du Conseil d'Etat que le 29 août 2014, soit après l'expiration du délai de recours, leurs conclusions dirigées contre ces décrets sont tardives et par suite irrecevables. <br/>
<br/>
              Sur les conclusions dirigées contre les décrets n° 2014-718 et 2014-719 du 26 juin 2014 relatifs aux AOC " Côtes de Montravel " et " Haut-Montravel " :<br/>
<br/>
              3. En premier lieu, les règles relatives à la densité de plantation et à l'écartement entre les rangs de vigne, qui relèvent des conditions de production susceptibles d'être fixées par le cahier des charges d'une AOC en application des dispositions précitées de l'article L. 641-7 du code rural et de la pêche maritime, sont fixées par référence aux normes communes à toutes les AOC définies par les travaux du comité national compétent de l'Institut national de l'origine et de la qualité (INAO) et de sa commission d'experts, aux fins de réguler le développement de la vigne et d'en maîtriser le niveau de production. Contrairement à ce que soutiennent les requérants, les exploitants ne sauraient se prévaloir de droits acquis au maintien de règles précédemment fixées par des cahiers des charges antérieurs.  <br/>
<br/>
              4. En deuxième lieu, les cahiers des charges homologués par les deux décrets attaqués ont porté de 4 000 à 5 000 pieds par hectare le niveau minimal de densité de la vigne. Ils ont également réduit de 2,50 à 2 mètres l'écartement entre les rangs de vigne. Il a toutefois été prévu que les parcelles plantées en vigne au 31 juillet 2009 qui ne respecteraient pas les nouvelles dispositions relatives à la densité pourraient être maintenues en l'état jusqu'à leur arrachage, sous réserve, d'une part, que la hauteur de feuillage permette de disposer de 1,40 mètre carré de surface externe de couvert végétal pour la production d'un kilogramme de raisin et, d'autre part, qu'elles satisfassent aux autres exigences du cahier des charges. Il ressort des pièces du dossier que l'augmentation de la densité des ceps, d'une part, induit un enracinement plus profond qui augmente la typicité des vins en obligeant chaque pied à optimiser l'espace qui lui est laissé et, d'autre part, améliore la qualité des tanins. Ainsi, en édictant des règles nouvelles de densité et d'écartement des rangs, les décrets attaqués ont pour objet d'améliorer la qualité et la typicité des vins produits, compte-tenu des caractéristiques des sols en cause. Dès lors le moyen tiré de ce qu'en modifiant les règles de densité de plantation et d'écartement entre les rangs comme il l'a fait, le pouvoir réglementaire aurait commis une erreur manifeste d'appréciation doit être écarté.<br/>
<br/>
              5. En troisième lieu et contrairement à ce que soutiennent les requérants, les règles nouvelles de densité et d'écartement des rangs ne présentent pas un caractère discriminatoire et non objectif. En édictant de ces règles, les décrets attaqués ne méconnaissent pas les dispositions de l'article 118 quater du règlement (CE) n° 1234/2007 du 22 octobre 2007 reprises dans le règlement (UE) n° 1308/2013 du 17 décembre 2013. <br/>
<br/>
              6. Enfin, aux termes du dernier alinéa de l'article L. 641-7 du code rural et de la pêche maritime, dans sa rédaction applicable au litige : " Des conditions de production et de contrôle communes à plusieurs produits peuvent être définies par décret, sur proposition de l'Institut national de l'origine et de la qualité après avis des organismes de défense et de gestion intéressés ". Si cet article prévoit la possibilité de fixer par décret des dispositions communes à plusieurs produits, il n'interdit pas, contrairement à ce que soutiennent les requérants, que différents produits partagent des conditions communes de production. Le comité national compétent de l'INAO a donc pu régulièrement fixer des normes communes à différentes AOC, sans que l'intervention d'un décret autorisant ces règles communes ne s'impose. <br/>
<br/>
              Sur les autres conclusions de la requête :<br/>
<br/>
              7. MM. A...et C...B...demandent en outre au Conseil d'Etat, d'une part, d'apprécier la légalité du décret n° 2011-1277 relatif à l'AOC " Montravel ". Toutefois il n'appartient pas au juge de l'excès de pouvoir d'apprécier la légalité d'un décret en l'absence de tout litige dans lequel ce décret est en cause. Les requérants, d'autre part, ne sont en tout état de cause pas recevables à demander à ce que le Conseil d'Etat, le moment venu, " vérifie la légalité du futur cahier des charges de cette appellation lors de sa prochaine demande d'homologation ". Par suite, les autres conclusions de la requête de MM. A...et C...B...ne sauraient être accueillies.<br/>
<br/>
              8. Il résulte de tout ce qui précède que la requête de MM. A...et C...B...doit être rejetée. <br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de MM. A...et C...B...est rejetée.  <br/>
Article 2 : La présente décision sera notifiée à MM. A...et C...B..., au ministre de l'agriculture, de l'agroalimentaire et de la forêt et à l'Institut national de l'origine et de la qualité. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
