<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042575661</ID>
<ANCIEN_ID>JG_L_2020_11_000000422678</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/57/56/CETATEXT000042575661.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème chambre, 27/11/2020, 422678, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-11-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>422678</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>HAAS ; SCP BAUER-VIOLAS, FESCHOTTE-DESBOIS, SEBAGH</AVOCATS>
<RAPPORTEUR>M. François Weil</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Sophie Roussel</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2020:422678.20201127</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Procédures devant les tribunaux administratifs<br/>
              M. A... B... a demandé au tribunal administratif de Paris d'annuler la décision du 19 janvier 2011 du directeur du support et de la maintenance de La Poste le plaçant d'office en congé de longue durée pour une période de six mois à compter du 1er décembre 2010, les décisions des 20 mai et 17 juin 2011 prolongeant ce congé pour les mois de juin et <br/>
juillet 2011 et la décision du 25 juillet 2011 prolongeant ce congé pour une durée de quatre mois à compter du 1er août 2011.<br/>
<br/>
              Par des ordonnances du 31 janvier 2012, ces requêtes ont été transférées par le tribunal administratif de Paris au tribunal administratif de Toulouse. <br/>
<br/>
              M. A... B... a également demandé au tribunal administratif de Toulouse d'annuler la décision du directeur du support et de la maintenance de La Poste du 19 décembre 2011 prolongeant son congé de longue durée pour une période de six mois à compter du 1er décembre 2011, la décision du 19 juillet 2013 prononçant la prolongation du congé de longue durée pour une durée de dix-huit mois du 1er juin 2012 au 30 novembre 2013, ainsi que la décision de La Poste rejetant implicitement sa demande tendant à sa réintégration et au bénéfice d'un congé ordinaire de maladie à plein traitement.<br/>
<br/>
              Par un jugement n° 1500529, 1200530, 1200561, 1200704, 1203505, 1304186 du 17 mars 2016, le tribunal administratif de Toulouse a rejeté ses demandes.<br/>
<br/>
              Procédure devant la cour administrative d'appel<br/>
<br/>
              Par un arrêt n° 16BX01476 du 28 mai 2018, la cour administrative d'appel de Bordeaux a réformé le jugement du tribunal administratif de Toulouse, annulé la décision du <br/>
19 janvier 2011 et, par voie de conséquence, les décisions des 25 juillet 2011, 19 décembre 2011 et 19 juillet 2013, enjoint à La Poste de réintégrer juridiquement M. B... à la date <br/>
du 1er décembre 2010, de prendre rétroactivement les mesures nécessaires pour le placer dans une situation régulière et de reconstituer sa carrière sur la période du 1er décembre 2010 au <br/>
30 novembre 2013 dans un délai de trois mois et condamné La Poste à lui verser la somme de 15 000 euros en réparation de son préjudice moral et des troubles dans ses conditions d'existence. <br/>
<br/>
              Procédure devant le Conseil d'État<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 30 juillet 2018, 30 octobre 2018 et 23 septembre 2020 au secrétariat du contentieux du Conseil d'État, la société La Poste demande au Conseil d'État : <br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) de rejeter l'appel formé par M. B... ;<br/>
<br/>
              3°) de mettre à la charge de M. B... le versement à La Poste de la somme de 3 000 euros en application de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Elle soutient que la cour administrative d'appel de Bordeaux : <br/>
              - a entaché son arrêt d'insuffisance de motivation et d'omission à statuer en n'explicitant pas les raisons pour lesquelles elle jugeait que la société ne pouvait utilement se prévaloir du secret médical pour ne pas produire des éléments médicaux détaillés concernant <br/>
M. B... et en ne répondant pas à sa demande tendant à ce que, si la cour jugeait insuffisants les documents médicaux versés au débat, elle fasse usage de ses pouvoirs d'instruction ;<br/>
              - a inexactement qualifié les faits de l'espèce, commis des erreurs de droit et dénaturé les faits et pièces du dossier en jugeant que la lettre du 25 février 2011 de M. B... présentait le caractère d'un recours gracieux préservant le délai de recours contentieux ;<br/>
              - a commis une erreur de droit et inexactement qualifié les faits de l'espèce en annulant la décision du 19 janvier 2011 plaçant d'office M. B...  en congé de longue durée pour une durée de six mois à compter du 1er décembre 2010 au motif que la société n'établissait pas que l'intéressé souffrait d'une maladie mentale, au sens du 4° de l'article 34 de la loi <br/>
du 11 janvier 1984, le mettant dans l'impossibilité d'exercer ses fonctions, alors que les pièces du dossier caractérisaient l'existence de cette pathologie et que la cour, en appuyant son raisonnement sur une ordonnance de non-lieu du 11 décembre 2013 du juge d'instruction du tribunal de grande instance d'Aurillac, a méconnu les règles gouvernant son office et dénaturé les termes de cette ordonnance ;<br/>
              - a commis une erreur de droit en jugeant que les documents médicaux que la société avait produits étaient insuffisants pour caractériser une maladie mentale mettant <br/>
M. B... dans l'impossibilité d'exercer ses fonctions sans tenir compte des contraintes que faisait peser le secret médical sur la société La Poste ;<br/>
              - a commis une erreur de droit en annulant les décisions de renouvellement du congé de longue durée sur la période comprise entre le 1er juin 2011 et le 30 novembre 2013 par voie de conséquence de l'annulation de la décision du 19 janvier 2011 alors que, parce qu'elles supposaient de la société pour chaque période de renouvellement une nouvelle appréciation de l'état de santé de l'agent, elles ne procédaient pas directement et exclusivement de la décision du 19 janvier 2011 ;<br/>
              - a commis une erreur de droit et dénaturé les faits et les pièces du dossier en condamnant la société La Poste à verser à M. B... une somme de 15 000 euros en raison du préjudice moral et des troubles dans ses conditions d'existence en lien direct avec son éviction du service pendant la période de congé de longue durée, alors que la société n'avait commis aucune illégalité fautive en le plaçant en congé de longue durée à compter du 1er décembre 2010 en raison des troubles psychologiques dont il souffrait et en prolongeant ce congé de longue durée du 1er juin 2011 au 30 novembre 2013.<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la loi n° 83-634 du 13 juillet 1983 ;<br/>
              - la loi n° 84-16 du 11 janvier 1984 ;<br/>
              - la loi n° 90-568 du 2 juillet 1990 ;<br/>
              - le décret n° 86-442 du 14 mars 1986 ;<br/>
              - le décret n° 86-442 du 14 mars 1986 ;<br/>
              -le décret n° 2010-191 du 26 février 2010 ;<br/>
<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. François Weil, Conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme C... D..., rapporteure publique,<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Haas, avocat de La Poste et à la SCP Bauer-Violas, Feschotte-Desbois, Sebagh, avocat de M. A... B... ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que M. A... B..., cadre de premier niveau à La Poste, affecté à Aurillac, s'est porté candidat en 2008 à un poste situé à Toulouse, sur lequel il a été nommé. Jugeant ensuite que ce poste était trop éloigné de son domicile familial situé à Trizac (Cantal), il a demandé sa réaffectation à Aurillac et, ne l'obtenant pas, a commencé une grève de la faim en avril 2010 et tenu, en novembre 2010, des propos inquiétants sur son état de santé. À la suite d'une alerte sur son état de santé lancée par l'assistante sociale de La Poste Midi-Pyrénées, la société La Poste a engagé une procédure destinée à le placer en congé de longue durée sur le fondement des dispositions des articles 34 et 35 du décret du 14 mars 1986 relatif à la désignation des médecins agréés, à l'organisation des comités médicaux et des commissions de réforme, aux conditions d'aptitude physique pour l'admission aux emplois publics et au régime de congés de maladie des fonctionnaires. Après avoir été examiné par un médecin et après avis favorable du comité médical, M. B... a été placé, par une décision du 19 janvier 2011, en congé de longue durée pour six mois à compter du <br/>
1er décembre 2010. Ce congé de longue durée a été prolongé par plusieurs décisions ultérieures après des avis favorables successifs du comité médical. M. B... a contesté l'ensemble de ces décisions devant le tribunal administratif de Toulouse et lui a demandé d'enjoindre à La Poste de lui accorder un congé de maladie ordinaire à plein traitement ou de régulariser sa situation par des mesures rétroactives administratives et financières. Il lui a en outre demandé de condamner La Poste à lui verser une somme de 2 000 euros par mois pour congé de longue durée abusif et de 500 000 euros à titre indemnitaire. Par un jugement du 17 mars 2016, le tribunal administratif de Toulouse a rejeté ces demandes en prononçant un non-lieu à statuer sur les conclusions de <br/>
M. B... aux fins d'annulation des décisions le plaçant en congé de longue durée et en rejetant le surplus de ses requêtes. Par un arrêt du 28 mai 2018, la cour administrative d'appel de Bordeaux a annulé la décision de La Poste du 19 janvier 2011 plaçant M. B... en congé de longue durée et, par voie de conséquence, les décisions ultérieures prolongeant ce congé, enjoint à La Poste de le réintégrer à la date du 1er décembre 2010 et de prendre rétroactivement les mesures nécessaires pour le placer dans une situation régulière et reconstituer sa carrière sur la période du 1er décembre 2010 au 30 novembre 2013, condamné La Poste à verser à l'intéressé 15 000 euros en réparation de son préjudice moral et de ses troubles dans ses conditions d'existence et réformé le jugement du tribunal de Toulouse en ce qu'il avait de contraire. La société La Poste se pourvoit en cassation contre cet arrêt.<br/>
<br/>
              2. Aux termes de l'article 29 de la loi du 2 juillet 1990 : " Les personnels de La Poste et de France Télécom sont régis par des statuts particuliers, pris en application de la loi n° 83-634 du 13 juillet 1983 portant droits et obligations des fonctionnaires et de la loi <br/>
n° 84-16 du 11 janvier 1984 portant dispositions statutaires relatives à la fonction publique de l'Etat, qui comportent des dispositions spécifiques dans les conditions prévues aux alinéas <br/>
ci-après, ainsi qu'à l'article 29-1. (...) ". L'article 34 de la loi du 11 janvier 1984, dans sa rédaction applicable à la date de la décision contestée, dispose : " Le fonctionnaire en activité a droit : (...) / 4° A un congé de longue durée, en cas de tuberculose, maladie mentale, affection cancéreuse, poliomyélite ou déficit immunitaire grave et acquis, de trois ans à plein traitement et de deux ans à demi-traitement. Le fonctionnaire conserve ses droits à la totalité du supplément familial de traitement et de l'indemnité de résidence. / (...) Sauf dans le cas où le fonctionnaire ne peut être placé en congé de longue maladie à plein traitement, le congé de longue durée n'est attribué qu'à l'issue de la période rémunérée à plein traitement d'un congé de longue maladie. (...) ". Aux termes de l'article 34 du décret du 14 mars 1986 : <br/>
" Lorsqu'un chef de service estime, au vu d'une attestation médicale ou sur le rapport des supérieurs hiérarchiques, que l'état de santé d'un fonctionnaire pourrait justifier qu'il lui soit fait application des dispositions de l'article 34 (3° ou 4°) de la loi du 11 janvier 1984 susvisée, il peut provoquer l'examen médical de l'intéressé dans les conditions prévues aux alinéas 3 et suivants de l'article 35 ci-dessous. Un rapport écrit du médecin chargé de la prévention attachée au service auquel appartient le fonctionnaire concerné doit figurer au dossier soumis au comité médical. ". L'article 35 de ce même décret dispose : " Pour obtenir un congé de longue maladie ou de longue durée, les fonctionnaires en position d'activité ou leurs représentants légaux doivent adresser à leur chef de service une demande appuyée d'un certificat de leur médecin traitant spécifiant qu'ils sont susceptibles de bénéficier des dispositions de l'article 34 (3° ou 4°) de la loi du 11 janvier 1984 susvisée. / (...) Sur le vu de ces pièces, le secrétaire du comité médical fait procéder à la contre-visite du demandeur par un médecin agréé compétent pour l'affection en cause. / Le dossier est ensuite soumis au comité médical compétent (...) ".<br/>
<br/>
              3. Il ressort des pièces du dossier soumis aux juges du fond que, au cours du dernier trimestre 2010, la hiérarchie de M. B..., l'assistante sociale chargée d'assurer son suivi et le médecin de prévention ont été alertés à plusieurs reprises par l'intéressé sur son état et sur sa détermination à se mettre lui-même en danger. Par des courriers des 19 novembre <br/>
et 6 décembre 2010 et l'assistante sociale et le médecin de prévention ont fait part de leurs inquiétudes à son sujet. Le certificat médical établi le 7 décembre 2010 par le médecin psychiatre désigné par La Poste qui a examiné M. B... estimait que son état de santé justifiait une mise en congé de longue maladie pour une durée de six mois et l'avis du comité médical, lors de sa séance du 12 janvier 2011, était favorable au placement de M. B... dans cette position statutaire. En jugeant, dans ces conditions, que M. B... ne pouvait être regardé comme souffrant d'une maladie mentale au sens du 4° de l'article 34 de la loi du <br/>
11 janvier 1984 le mettant dans l'impossibilité d'exercer ses fonctions et justifiant qu'il soit placé d'office en congé de longue durée pour six mois à compter du 1er décembre 2010 et en annulant pour ce motif la décision du 19 janvier 2011 et, par voie de conséquence, les décisions des 25 juillet, 19 décembre 2011 et 19 juillet 2013, la cour a inexactement qualifié les faits de l'espèce.<br/>
<br/>
              4. Par suite, et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, la société La Poste est fondée à demander l'annulation de l'arrêt attaqué.<br/>
<br/>
              5. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mise à la charge de la société La Poste, qui n'est pas, dans la présente instance, la partie perdante, la somme que M. B... demande à ce titre. Il n'y a pas lieu, dans les circonstances particulières de l'espèce, de mettre à la charge de M. B... la somme que La Poste demande au titre de ces mêmes dispositions.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 28 mai 2018 de la cour administrative d'appel de Bordeaux est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Bordeaux.<br/>
Article 3 : Le surplus des conclusions de la requête et les conclusions de M. B... tendant à l'application de l'article L. 761-1 du code de justice administrative sont rejetés.<br/>
Article 4 : La présente décision sera notifiée à la société La Poste et à M. A... B....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
