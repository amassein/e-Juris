<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030860177</ID>
<ANCIEN_ID>JG_L_2015_07_000000365850</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/86/01/CETATEXT000030860177.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème / 10ème SSR, 08/07/2015, 365850, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-07-08</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>365850</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème / 10ème SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GATINEAU, FATTACCINI</AVOCATS>
<RAPPORTEUR>M. Olivier Japiot</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Aladjidi</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:365850.20150708</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu les procédures suivantes :<br/>
<br/>
              1° Sous le n° 365850 :<br/>
<br/>
              La société Peugeot a demandé au tribunal administratif de Cergy-Pontoise, d'une part, la décharge des cotisations supplémentaires d'impôt sur les sociétés et de contributions additionnelles à cet impôt, ainsi que des pénalités correspondantes, auxquelles elle a été assujettie au titre de l'exercice clos en 2001 pour un montant de 3 037 297 euros et, d'autre part, la restitution d'une somme de 787 943 euros qu'elle avait acquittée au titre du même impôt, majorée des intérêts moratoires. Par un jugement n° 0511303 du 29 juillet 2010, le tribunal administratif de Cergy-Pontoise a fait droit à ces demandes.<br/>
<br/>
              Par un arrêt n° 10VE03850 du 22 novembre 2012, la cour administrative d'appel de Versailles a rejeté l'appel formé par le ministre du budget, des comptes publics et de la réforme de l'Etat contre ce jugement. <br/>
<br/>
              Par un pourvoi et deux mémoires en réplique, enregistrés les 7 février 2013, 25 mars 2015 et 10 avril 2015 au secrétariat du contentieux du Conseil d'Etat, le ministre délégué, chargé du budget, demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel.<br/>
<br/>
<br/>
              2° Sous le n° 370317 :<br/>
<br/>
              La société Peugeot a demandé au tribunal administratif de Cergy-Pontoise la décharge des cotisations supplémentaires d'impôt sur les sociétés et de contributions additionnelles à cet impôt, ainsi que des pénalités correspondantes, auxquelles elle a été assujettie au titre de l'exercice clos en 2002. Par un jugement n° 0606311 du 30 août 2011, le tribunal administratif de Cergy-Pontoise a fait partiellement droit à cette demande à hauteur d'un montant de 11 149 647 euros.<br/>
<br/>
              Par un arrêt n° 11VE03788 du 27 juin 2013, la cour administrative d'appel de Versailles a rejeté l'appel formé par le ministre du budget, des comptes publics et de la réforme de l'Etat contre ce jugement en tant qu'il prononçait cette décharge partielle. <br/>
<br/>
              Par un pourvoi et deux mémoires en réplique, enregistrés les 18 juillet 2013, 24 mars 2015 et 10 avril 2015 au secrétariat du contentieux du Conseil d'Etat, le ministre délégué, chargé du budget, demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu :<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Olivier Japiot, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Frédéric Aladjidi, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Gatineau, Fattaccini, avocat de la société Peugeot et SA Geparcia et SA Financière Pergolèse ;<br/>
<br/>
<br/>
<br/>1. Considérant que les pourvois visés ci-dessus présentent à juger les mêmes questions ; qu'il y a lieu de les joindre pour statuer par une seule décision ;<br/>
<br/>
              2. Considérant, d'une part, qu'aux termes de l'article 223 D du code général des impôts, dans sa rédaction alors en vigueur : " La plus-value nette ou la moins-value nette à long terme d'ensemble est déterminée par la société mère en faisant la somme algébrique des plus-values ou des moins-values nettes à long terme de chacune des sociétés du groupe (...) / La plus-value nette à long terme d'ensemble fait l'objet d'une imposition séparée dans les conditions prévues au a bis du I de l'article 219./ (...)En cas de cession entre sociétés du groupe de titres éligibles au régime des plus ou moins-values à long terme, les dotations aux provisions pour dépréciation de ces titres effectuées postérieurement à la cession sont également ajoutées à la plus-value nette à long terme d'ensemble ou retranchées de la moins-value nette à long terme d'ensemble, à hauteur de l'excédent des plus-values ou profits sur les moins-values ou pertes afférent à ces mêmes titres, qui n'a pas été pris en compte, en application du premier alinéa de l'article 223 F, pour le calcul du résultat ou de la plus ou moins-value nette à long terme d'ensemble. (...) " ; qu'aux termes de l'article 223 F du même code, dans rédaction applicable en l'espèce : " La fraction de la plus-value ou de la moins-value afférente à la cession entre sociétés du groupe d'un élément d'actif immobilisé, acquise depuis sa date d'inscription au bilan de la société du groupe qui a effectué la première cession, n'est pas retenue pour le calcul du résultat ou de la plus-value ou de la moins-value nette à long terme d'ensemble au titre de l'exercice de cette cession./ (...)Lors de la cession hors du groupe du bien ou de la sortie du groupe d'une société qui l'a cédé ou de celle qui en est propriétaire, la société mère doit comprendre dans le résultat ou plus-value ou moins-value nette à long terme d'ensemble, le résultat ou la plus-value ou la moins-value qui n'a pas été retenu lors de sa réalisation. " ;<br/>
<br/>
              3. Considérant, d'autre part, qu'aux termes de l'article L 64 du livre des procédures fiscales, dans sa rédaction alors applicable : " Ne peuvent être opposés à l'administration des impôts les actes qui dissimulent la portée véritable d'un contrat ou d'une convention à l'aide de clauses : (...) b) (...) qui déguisent soit une réalisation, soit un transfert de bénéfices ou de revenus ; (...) L'administration est en droit de restituer son véritable caractère à l'opération litigieuse. En cas de désaccord sur les redressements notifiés sur le fondement du présent article, le litige est soumis, à la demande du contribuable, à l'avis du comité consultatif pour la répression des abus de droit. L'administration peut également soumettre le litige à l'avis du comité dont les avis rendus feront l'objet d'un rapport annuel. Si l'administration ne s'est pas conformée à l'avis du comité, elle doit apporter la preuve du bien-fondé du redressement. " ; qu'il résulte de ces dispositions que, lorsque l'administration use de la faculté qu'elles lui confèrent dans des conditions telles que la charge de la preuve lui incombe, elle est fondée à écarter comme ne lui étant pas opposables certains actes passés par le contribuable, dès lors que ces actes ont un caractère fictif, ou, que, recherchant le bénéfice d'une application littérale des textes à l'encontre des objectifs poursuivis par leurs auteurs, ils n'ont pu être inspirés par aucun autre motif que celui d'éluder ou d'atténuer les charges fiscales que l'intéressé, s'il n'avait pas passé ces actes, aurait normalement supportées, eu égard à sa situation ou à ses activités réelles ;<br/>
<br/>
              4. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que la société Peugeot, tête du groupe fiscalement intégré PSA, a cédé, le 30 novembre 2000, la participation à 100 % qu'elle détenait dans le capital social de la société Geparcia, membre du groupe intégré, à une autre de ses filiales, la société Financière Pergolèse, également membre de ce groupe, pour un montant de 31 556 852 euros ; que, par un traité de fusion approuvé le 20 décembre 2001, la société Financière Pergolèse a ensuite absorbé la société Geparcia avec effet rétroactif au 1er janvier 2001 dans le cadre d'une fusion simplifiée ; que l'administration, sur le fondement des dispositions de l'article L. 64 du livre des procédures fiscales, a remis en cause la cession de ces titres à la société Financière Pergolèse, préalablement à la fusion des deux sociétés ; que, par jugements des 29 juillet 2010 et 30 juillet 2011, le tribunal administratif de Cergy-Pontoise a déchargé la société Peugeot des cotisations supplémentaires d'impôt sur les sociétés auxquelles elle a été assujettie, pour ce motif, au titre des exercices clos en 2001 et 2002 ; que le ministre se pourvoit en cassation contre les arrêts des 22 novembre 2012 et 27 juin 2013 par lesquels la cour administrative d'appel de Versailles a rejeté ses recours dirigés contre ces deux jugements ;<br/>
<br/>
              5. Considérant qu'il ressort des énonciations des arrêts attaqués que la société Peugeot a enregistré, à la suite de la cession des titres de la société Geparcia, une moins-value d'un montant de 51 008 406 euros, soumise au régime des plus ou moins-values à long terme prévu par les dispositions du I de l'article 219 du code général des impôts ; que cette moins-value a été neutralisée pour la détermination du résultat d'ensemble à long terme en application des dispositions précitées de l'article 223 F du même code ; qu'il a en a été de même, en vertu des dispositions de l'article 223 D du même code, de la reprise de la provision pour dépréciation des titres en cause, comptabilisée pour un montant de 30 565 213 euros par la société Peugeot ; que la dissolution de la société Geparcia ayant entraîné sa sortie du groupe fiscal intégré au titre de l'exercice clos en 2001, la société Peugeot a déduit de son résultat à long terme la moins-value de cession des titres, d'un montant de 51 008 406 euros, en application des dispositions précitées de l'article 223 F ; <br/>
<br/>
              6. Considérant que, s'agissant de la cession de titres de la société Geparcia auparavant intervenue, le 30 novembre 2000, au profit de la société Financière Pergolèse, la cour a relevé que la société Peugeot faisait valoir, sans être contredite par le ministre, qu'elle devait, avant la fin de l'année 2000 ou au début de l'année 2001, anticiper un fort besoin de trésorerie, estimé à 1 400 millions d'euros, lié à l'acquisition des activités d'équipement automobile du groupe Sommer-Allibert, alors qu'elle ne disposait que de 304 millions d'euros de disponibilités ; que la cession immédiate des titres de la société Geparcia lui a permis de limiter son recours à l'endettement, tout en faisant disparaître de son bilan les titres d'une société fortement dépréciée ; que la cour a également relevé que la société Peugeot, dans le cadre de la restructuration d'ensemble du groupe PSA engagée en 1998, n'avait vocation qu'à détenir des titres de sociétés elles-mêmes têtes de sous-groupes, alors que la société Financière Pergolèse était destinée à détenir les filiales du groupe sans activité ou en sommeil comme l'était alors la société Geparcia ; que si le ministre a soutenu devant la cour que les mêmes objectifs de renforcement de la trésorerie et de lisibilité du bilan auraient pu être atteints par la société Peugeot en absorbant directement la société Geparcia, la cour administrative d'appel de Versailles n'a pas inexactement qualifié les faits qui lui étaient soumis en jugeant que l'administration ne pouvait, compte tenu de ce qui vient d'être dit, être regardée comme établissant que la cession des titres de la société Geparcia à la société Financière Pergolèse, suivie de l'absorption par celle-ci de sa filiale, aurait été exclusivement inspirée par le motif d'éluder ou d'atténuer l'impôt et en en déduisant qu'elle n'était, dès lors, pas fondée à se prévaloir de l'existence d'un abus de droit pour procéder aux redressements contestés ;<br/>
<br/>
              7. Considérant qu'il résulte de ce qui précède que le ministre délégué, chargé du budget, n'est pas fondé à demander l'annulation des arrêts qu'il attaque ;<br/>
<br/>
              8. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 3 500 euros à verser à la société Peugeot, au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : Les pourvois du ministre délégué, chargé du budget, sont rejetés.<br/>
Article 2 : L'Etat versera à la société Peugeot une somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée au ministre des finances et des comptes publics et à la société Peugeot.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
