<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038601880</ID>
<ANCIEN_ID>JG_L_2019_06_000000414109</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/60/18/CETATEXT000038601880.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème chambre, 12/06/2019, 414109, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-06-12</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>414109</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP CELICE, SOLTNER, TEXIDOR, PERIER</AVOCATS>
<RAPPORTEUR>Mme Cécile Isidoro</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emmanuelle Cortot-Boucher</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2019:414109.20190612</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              La société d'exercice libéral par actions à responsabilité limitée (Selarl) cabinet dentaire A...Patrick a demandé au tribunal administratif de Strasbourg de prononcer la décharge des cotisations supplémentaires d'impôt sur les sociétés auxquelles elle a été assujettie au titre des exercices clos en 2007 et 2008 ainsi que des majorations correspondantes.<br/>
<br/>
              Par un jugement n° 1202152 du 6 juillet 2017, le tribunal administratif de Strasbourg a rejeté sa demande. <br/>
<br/>
              Par un arrêt n° 16NC00594 du 6 juillet 2017, la cour administrative d'appel de Nancy a rejeté l'appel formé par la Selarl cabinet dentaire A...Patrick contre ce jugement. <br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 7 septembre et 7 décembre 2017 et le 21 novembre 2018 au secrétariat du contentieux du Conseil d'Etat, la Selarl cabinet dentaire A...Patrick demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - le code de justice administrative ; <br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Cécile Isidoro, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Emmanuelle Cortot-Boucher, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Célice, Soltner, Texidor, Perier, avocat du cabinet dentaire A...Patrick ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que M. A..., qui est chirurgien-dentiste a exercé son activité sous la forme d'une entreprise individuelle jusqu'à la création de la Selarl cabinet dentaire A...Patrick à laquelle il a cédé son fonds libéral le 5 janvier 2007. A l'issue d'une vérification de comptabilité de cette société, l'administration a estimé qu'elle avait acquis sa patientèle à un prix indûment majoré et que les intérêts de l'emprunt contracté pour financer cette acquisition devaient être réintégrés au résultat de la société à hauteur de la fraction prétendument excessive du prix de cession. La société a été assujettie à des cotisations supplémentaires d'impôt sur les sociétés au titre des exercices clos en 2007 et 2008 ainsi qu'à des pénalités, dont elle a demandé la décharge. Le tribunal administratif de Strasbourg a rejeté sa demande par un jugement du 4 février 2016. La société se pourvoit en cassation contre l'arrêt du 6 juillet 2017 par lequel la cour administrative d'appel de Nancy a rejeté l'appel qu'elle a formé contre ce jugement.  <br/>
<br/>
              Sur l'arrêt en ce qui concerne la régularité de la procédure d'imposition :<br/>
<br/>
              2. Aux termes de l'article L. 64 du livre des procédures fiscales, alors applicable : " Afin d'en restituer le véritable caractère, l'administration est en droit d'écarter, comme ne lui étant pas opposables, les actes constitutifs d'un abus de droit, soit que ces actes ont un caractère fictif, soit que, recherchant le bénéfice d'une application littérale des textes ou de décisions à l'encontre des objectifs poursuivis par leurs auteurs, ils n'ont pu être inspirés par aucun autre motif que celui d'éluder ou d'atténuer les charges fiscales que l'intéressé, si ces actes n'avaient pas été passés ou réalisés, aurait normalement supportées eu égard à sa situation ou à ses activités réelles./ En cas de désaccord sur les rectifications notifiées sur le fondement du présent article, le litige est soumis, à la demande du contribuable, à l'avis du comité de l'abus de droit fiscal. L'administration peut également soumettre le litige à l'avis du comité. (...)". Ces dispositions ne sont pas applicables, alors même qu'une de ces conditions serait remplie, lorsque le redressement est justifié par l'existence d'un acte anormal de gestion.<br/>
<br/>
              3. Par suite, après avoir estimé, sans dénaturer les pièces du dossier qui lui étaient soumises, que l'administration s'était bornée à considérer qu'en procédant à l'acquisition de sa patientèle à un prix majoré, la société avait procédé à un acte anormal de gestion, la cour a pu, sans commettre d'erreur de droit, en déduire que la société ne pouvait utilement soutenir qu'elle aurait été imposée en méconnaissance des dispositions précitées de l'article L. 64 du livre des procédures fiscales.<br/>
<br/>
              Sur l'arrêt en ce qui concerne le bien-fondé des impositions :<br/>
<br/>
              4. Aux termes du I de l'article L. 141-1 du code de commerce : " Dans tout acte constatant une cession amiable de fonds de commerce, consentie même sous condition et sous la forme d'un autre contrat ou l'apport en société d'un fonds de commerce, sauf si l'apport est fait à une société détenue en totalité par le vendeur, le vendeur est tenu d'énoncer : (...) / 3° Le chiffre d'affaires qu'il a réalisé durant les trois exercices comptables précédant celui de la vente, ce nombre étant réduit à la durée de la possession du fonds si elle a été inférieure à trois ans ".<br/>
<br/>
              5. Aux termes de l'article R. 613-4 du code de justice administrative : "  (...) La réouverture de l'instruction peut également résulter d'un jugement ou d'une mesure d'investigation ordonnant le supplément d'instruction. / Les mémoires qui auraient été produits pendant la période comprise entre la clôture et la réouverture de l'instruction sont communiqués aux parties ". <br/>
<br/>
              6. Il ressort des pièces du dossiers soumis aux juges du fond que, par un mémoire enregistré le 9 juin 2017, qui ne peut être regardé comme produit après la clôture de l'instruction, dès lors que si celle-ci avait été close au 6 juin 2017, elle a été réouverte par une mesure supplémentaire d'instruction du 16 juin 2017, la société requérante a soutenu que la mention dans l'acte de cession du 5 janvier 2007, de son chiffre d'affaires de 2003 à 2005 visait uniquement à satisfaire aux exigences des dispositions précitées du 3° du I de l'article L. 141-1 du code de commerce, à une date où ses résultats au titre de 2006 n'étaient pas encore connus mais qu'il convenait de tenir compte du chiffre d'affaires au titre de l'année 2006 dont l'administration disposait et de celui des deux qui l'ont précédée, pour calculer la valeur de sa patientèle suivant la méthode usuelle reposant sur le chiffre d'affaire moyens des trois dernières années. Dès lors, en estimant que c'était à bon droit que l'administration avait fondé son estimation sur les chiffres d'affaires de 2003 à 2005 et en jugeant qu'il n'était pas contesté qu'elle s'était ainsi appuyée sur les données fournies par la société, la cour a dénaturé les pièces du dossier et s'est méprise sur le sens des écritures qui lui étaient soumises en ce qui concerne la déductibilité des intérêts d'emprunt. La société ne présentant aucun moyen portant sur les rectifications opérées en matière d'agios bancaires et de frais kilométriques ainsi que sur les pénalités dont les impositions correspondantes ont été assorties, elle est seulement fondée à demander l'annulation de l'arrêt qu'elle attaque en ce qui concerne la fraction de l'imposition résultant de la réintégration des intérêts d'emprunt et les pénalités correspondantes. <br/>
<br/>
              7. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 1 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Nancy du 6 juillet 2017 est annulé en ce qui concerne la fraction de l'imposition résultant de la réintégration des intérêts d'emprunt et les pénalités correspondantes.<br/>
<br/>
Article 2 : L'affaire est renvoyée, dans cette mesure, à la cour administrative d'appel de Nancy. <br/>
Article 3 : Le surplus des conclusions du pourvoi de la Selarl cabinet dentaire A...Patrick est rejeté. <br/>
Article 4 : L'Etat versera la somme de 1 000 euros à la Selarl cabinet dentaire A...Patrick au titre de l'article L. 761-1 du code de justice administrative.  <br/>
Article 5 : La présente décision sera notifiée à la Selarl cabinet dentaire A...Patrick et au ministre de l'action et des comptes publics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
