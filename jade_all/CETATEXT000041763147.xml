<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000041763147</ID>
<ANCIEN_ID>JG_L_2020_03_000000426291</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/41/76/31/CETATEXT000041763147.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème - 2ème chambres réunies, 27/03/2020, 426291, Publié au recueil Lebon</TITRE>
<DATE_DEC>2020-03-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>426291</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème - 2ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BAUER-VIOLAS, FESCHOTTE-DESBOIS, SEBAGH ; SCP COUTARD, MUNIER-APAIRE ; SCP BOULLOCHE ; SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR>M. Marc Pichon de Vendeuil</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Mireille Le Corre</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:426291.20200327</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              MM. A... I... H..., B... F..., D... K..., J... E... et G... C... ont demandé au tribunal administratif de Nancy d'annuler ou de résilier l'avenant n° 1 au contrat de concession pour le service public du développement et de l'exploitation du réseau de distribution d'électricité et de fourniture d'énergie électrique aux tarifs réglementés de vente que la communauté urbaine du Grand Nancy (CUGN) a signé le 18 avril 2011 avec les sociétés EDF et ERDF, subsidiairement, d'annuler l'article 1er de cet avenant et la délibération n° 27 du conseil communautaire du 14 novembre 2014 et d'enjoindre à la communauté urbaine du Grand Nancy de résilier l'avenant litigieux ou de saisir le juge du contrat pour qu'il en constate la nullité, et, enfin, d'annuler les décisions du 16 mars 2015 ayant rejeté leurs recours gracieux. Par un jugement n° 1501422 du 2 mai 2017, ce tribunal a rejeté leurs demandes.<br/>
<br/>
              Par un arrêt n° 17NC01597 du 16 octobre 2018, la cour administrative d'appel de Nancy a rejeté l'appel formé par M. I... H... et autres contre ce jugement.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire, un mémoire en réplique et trois nouveaux mémoires, enregistrés les 17 décembre 2018, 18 mars et 29 octobre 2019 et 12 février, 4 et 9 mars 2020 au secrétariat du contentieux du Conseil d'Etat, M. I... H... et autres demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à leur appel ;<br/>
<br/>
              3°) de mettre à la charge de la métropole du Grand Nancy, de la société EDF et de la société Enedis la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.  <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'énergie ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Marc Pichon de Vendeuil, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Mireille Le Corre, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Bauer-Violas, Feschotte-Desbois, Sebagh, avocat de M. I... H... et autres, à la SCP Boulloche, avocat de la métropole du Grand Nancy, à la SCP Piwnica, Molinié, avocat de la société EDF, et à la SCP Coutard, Munier-Apaire, avocat de la société Enedis ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que, par une délibération du 15 avril 2011, le conseil de la communauté urbaine du Grand Nancy, devenue depuis la métropole du Grand Nancy, a autorisé son président à signer avec les sociétés ERDF, devenue Enedis, et EDF, un contrat de concession du service public du développement et de l'exploitation du réseau de distribution et de fourniture d'énergie électrique aux tarifs réglementés. Par un arrêt devenu définitif du 12 mai 2014, la cour administrative d'appel de Nancy a annulé cette délibération ainsi que la décision du président de la communauté urbaine de signer cette convention en tant que figuraient à son cahier des charges les articles 2 et 19 relatifs à la propriété des compteurs électriques, et l'article 31 concernant l'indemnité de fin de contrat en cas de résiliation anticipée, qui comportaient des clauses illégales. Tirant les conséquences de cet arrêt, la communauté urbaine du Grand Nancy a, le 25 février 2015, signé avec les sociétés EDF et ERDF un avenant modifiant les clauses des articles 2, 19 et 31 du contrat. Se prévalant de leur qualité d'usagers du service public et de contribuables locaux, M. I... H... et autres ont demandé l'annulation de cet avenant devant le tribunal administratif de Nancy qui a rejeté leur demande par un jugement en date du 2 mai 2017. Ils se pourvoient en cassation contre l'arrêt du 16 octobre 2018 par lequel la cour administrative d'appel de Nancy a rejeté l'appel qu'ils ont formé contre ce jugement.<br/>
<br/>
              Sur les conclusions à fins de non-lieu présentées par la métropole du Grand Nancy :<br/>
<br/>
              2. La métropole du Grand Nancy fait valoir que par une délibération du conseil métropolitain du 20 décembre 2019, le contrat de concession litigieux a été résilié à compter du 31 décembre 2019 et que le recours des demandeurs est par voie de conséquence privé d'objet. Toutefois, la circonstance que le contrat de concession ait été résilié n'est pas de nature à priver d'objet le présent pourvoi, qui tend à l'annulation de l'avenant adopté le 25 février 2015 et qui a été en vigueur à compter de cette date. Ses conclusions à fins de non-lieu doivent par suite être rejetées.<br/>
<br/>
              Sur le pourvoi :<br/>
<br/>
              3. Indépendamment des actions dont disposent les parties à un contrat administratif et des actions ouvertes devant le juge de l'excès de pouvoir contre les clauses réglementaires d'un contrat ou devant le juge du référé contractuel sur le fondement des articles L. 551-13 et suivants du code de justice administrative, tout tiers à un contrat administratif susceptible d'être lésé dans ses intérêts de façon suffisamment directe et certaine par sa passation ou ses clauses est recevable à former devant le juge du contrat un recours de pleine juridiction contestant la validité du contrat ou de certaines de ses clauses non réglementaires qui en sont divisibles. Ce recours doit être exercé dans un délai de deux mois à compter de l'accomplissement des mesures de publicité appropriées, notamment au moyen d'un avis mentionnant à la fois la conclusion du contrat et les modalités de sa consultation dans le respect des secrets protégés par la loi. <br/>
<br/>
              4. Saisi par un tiers dans les conditions définies ci-dessus de conclusions contestant la validité d'un contrat ou de certaines de ses clauses, il appartient au juge du contrat de vérifier que l'auteur du recours autre que le représentant de l'Etat dans le département ou qu'un membre de l'organe délibérant de la collectivité territoriale ou du groupement de collectivités territoriales concerné se prévaut d'un intérêt susceptible d'être lésé de façon suffisamment directe et certaine. Lorsque l'auteur du recours se prévaut de sa qualité de contribuable local, il lui revient d'établir que la convention ou les clauses dont il conteste la validité sont susceptibles d'emporter des conséquences significatives sur les finances ou le patrimoine de la collectivité.<br/>
<br/>
              5. Il ressort des motifs de l'arrêt attaqué que M. I... H... et autres se prévalaient notamment de leur qualité de contribuables locaux pour contester, d'une part, la validité des clauses de l'avenant relatives à la délimitation du périmètre des ouvrages concédés, dont ils estimaient qu'elles n'incluaient pas dans les biens de retour qui, en principe, reviennent gratuitement à l'autorité concédante à l'expiration de la concession, les dispositifs de suivi intelligent, de contrôle, de coordination et de stockage des flux électriques mentionnés à l'article 2 du cahier des charges modifié, alors, selon eux, que ces équipements étaient nécessaires à l'exploitation des compteurs Linky et, partant, au fonctionnement du service public. Ils contestaient, d'autre part, la validité des clauses relatives à l'indemnité susceptible d'être versée au concessionnaire en cas de rupture anticipée du contrat, dont ils estimaient que l'application pouvait excéder le montant du préjudice réellement subi par ce dernier et constituer de ce fait une libéralité prohibée. <br/>
<br/>
              6. Pour écarter l'intérêt à agir des requérants en tant que contribuables locaux, la cour s'est en premier lieu fondée sur le caractère aléatoire du déploiement des dispositifs exclus de la liste des ouvrages concédés par l'article 2 du cahier des charges et sur le caractère incertain de la mise en oeuvre de la clause relative à la rupture anticipée du contrat. Elle a ce faisant commis une erreur de droit, le caractère éventuel ou incertain de la mise en oeuvre de clauses étant par lui-même dépourvu d'incidence sur l'appréciation de leur répercussion possible sur les finances ou le patrimoine de l'autorité concédante. En second lieu, en se fondant sur la spécificité des dispositions du code de l'énergie, dont l'article L. 111-52 fixe des zones de desserte exclusives pour les gestionnaires de réseaux publics et attribue de ce fait un monopole légal à la société Enedis, et sur la durée de la convention litigieuse, qui a été conclue pour trente ans, pour estimer que la mise en oeuvre de l'indemnité pour rupture anticipée du contrat était trop hypothétique pour suffire à établir que les finances ou le patrimoine de la métropole s'en trouveraient affectés de façon significative, alors qu'au vu des évolutions scientifiques, techniques, économiques et juridiques propres au secteur de l'énergie, des modifications d'une telle concession sont probables au cours de la période couverte par le contrat et pourraient notamment nécessiter la mise en oeuvre des clauses critiquées, la cour a commis une autre erreur de droit. Par suite, et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, son arrêt doit être annulé.<br/>
<br/>
              7. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge respective de la métropole du Grand Nancy et des société Enedis et EDF la somme de 1 000 euros à verser chacune aux requérants, au titre des dispositions de l'article L. 761-1 du code de justice administrative. Les mêmes dispositions font en revanche obstacle à ce qu'une somme soit mise à ce titre à la charge des requérants qui ne sont pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 16 octobre 2018 de la cour administrative d'appel de Nancy est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Nancy. <br/>
Article 3 : La métropole du Grand Nancy, la société Enedis et la société EDF verseront chacune une somme totale de 1 000 euros aux requérants, au titre des dispositions de l'article L. 761-1 du code de justice administrative. Les conclusions de la métropole du Grand Nancy, de la société Enedis et de la société EDF présentées au titre des mêmes dispositions sont rejetées.<br/>
Article 4 : La présente décision sera notifiée à M. A... I... H..., représentant unique ainsi qu'à la métropole du Grand Nancy, à la société Enedis et à la société EDF.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">39-08 MARCHÉS ET CONTRATS ADMINISTRATIFS. RÈGLES DE PROCÉDURE CONTENTIEUSE SPÉCIALES. - POURVOI TENDANT À L'ANNULATION D'UN CONTRAT DE CONCESSION AYANT ÉTÉ RÉSILIÉ - NON-LIEU - ABSENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">39-08-01-03 MARCHÉS ET CONTRATS ADMINISTRATIFS. RÈGLES DE PROCÉDURE CONTENTIEUSE SPÉCIALES. RECEVABILITÉ. - INTÉRÊT À FORMER UN RECOURS TARN-ET-GARONNE [RJ1] - REQUÉRANT SE PRÉVALANT DE SA QUALITÉ DE CONTRIBUABLE LOCAL - 1) EXISTENCE, À CONDITION D'ÉTABLIR QUE LE CONTRAT CONTESTÉ EST SUSCEPTIBLE D'EMPORTER DES CONSÉQUENCES SIGNIFICATIVES SUR LES FINANCES OU LE PATRIMOINE DE LA COLLECTIVITÉ - 2) ILLUSTRATION.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">54-01-04-02-01 PROCÉDURE. INTRODUCTION DE L'INSTANCE. INTÉRÊT POUR AGIR. EXISTENCE D'UN INTÉRÊT. INTÉRÊT LIÉ À UNE QUALITÉ PARTICULIÈRE. - REQUÉRANT SE PRÉVALANT DE SA QUALITÉ DE CONTRIBUABLE LOCAL - INTÉRÊT À FORMER UN RECOURS TARN-ET-GARONNE [RJ1] - 1) EXISTENCE, À CONDITION D'ÉTABLIR QUE LE CONTRAT CONTESTÉ EST SUSCEPTIBLE D'EMPORTER DES CONSÉQUENCES SIGNIFICATIVES SUR LES FINANCES OU LE PATRIMOINE DE LA COLLECTIVITÉ - 2) ILLUSTRATION.
</SCT>
<SCT ID="8D" TYPE="PRINCIPAL">54-05-05-01 PROCÉDURE. INCIDENTS. NON-LIEU. ABSENCE. - POURVOI TENDANT À L'ANNULATION D'UN CONTRAT DE CONCESSION AYANT ÉTÉ RÉSILIÉ - ABSENCE.
</SCT>
<ANA ID="9A"> 39-08 La circonstance qu'un contrat de concession ait été résilié n'est pas de nature à priver d'objet le pourvoi tendant à son annulation.</ANA>
<ANA ID="9B"> 39-08-01-03 1) Saisi par un tiers de conclusions contestant la validité d'un contrat ou de certaines de ses clauses, il appartient au juge du contrat de vérifier que l'auteur du recours autre que le représentant de l'Etat dans le département ou qu'un membre de l'organe délibérant de la collectivité territoriale ou du groupement de collectivités territoriales concerné se prévaut d'un intérêt susceptible d'être lésé de façon suffisamment directe et certaine. Lorsque l'auteur du recours se prévaut de sa qualité de contribuable local, il lui revient d'établir que la convention ou les clauses dont il conteste la validité sont susceptibles d'emporter des conséquences significatives sur les finances ou le patrimoine de la collectivité.,,,2) Recours contre un contrat de concession du service public du développement et de l'exploitation du réseau de distribution et de fourniture d'énergie électrique aux tarifs réglementés, attribué à la société Enedis.,,,Requérants se prévalant de leur qualité de contribuables locaux pour contester, d'une part, la validité des clauses relatives à la délimitation du périmètre des ouvrages concédés, dont ils estimaient qu'elles n'incluaient pas certains dispositifs dans les biens de retour, d'autre part, la validité des clauses relatives à l'indemnité susceptible d'être versée au concessionnaire en cas de rupture anticipée du contrat, dont ils estimaient que l'application pouvait excéder le montant du préjudice réellement subi par ce dernier et constituer de ce fait une libéralité prohibée.,,,L'intérêt à agir des requérants en tant que contribuables locaux ne peut être écarté en se fondant sur le caractère aléatoire du déploiement des dispositifs exclus de la liste des ouvrages concédés et sur le caractère incertain de la mise en oeuvre de la clause relative à la rupture anticipée du contrat : d'une part, le caractère éventuel ou incertain de la mise en oeuvre de clauses est par lui-même dépourvu d'incidence sur l'appréciation de leur répercussion possible sur les finances ou le patrimoine de l'autorité concédante ; d'autre part, bien que l'article L. 111-52 du code de l'énergie fixe des zones de desserte exclusives pour les gestionnaires de réseaux publics et attribue de ce fait un monopole légal à la société Enedis et que la convention litigieuse a été conclue pour 30 ans, au vu des évolutions scientifiques, techniques, économiques et juridiques propres au secteur de l'énergie, des modifications d'une telle concession sont probables au cours de la période couverte par le contrat et pourraient notamment nécessiter la mise en oeuvre des clauses critiquées.</ANA>
<ANA ID="9C"> 54-01-04-02-01 1) Saisi par un tiers de conclusions contestant la validité d'un contrat ou de certaines de ses clauses, il appartient au juge du contrat de vérifier que l'auteur du recours autre que le représentant de l'Etat dans le département ou qu'un membre de l'organe délibérant de la collectivité territoriale ou du groupement de collectivités territoriales concerné se prévaut d'un intérêt susceptible d'être lésé de façon suffisamment directe et certaine. Lorsque l'auteur du recours se prévaut de sa qualité de contribuable local, il lui revient d'établir que la convention ou les clauses dont il conteste la validité sont susceptibles d'emporter des conséquences significatives sur les finances ou le patrimoine de la collectivité.,,,2) Recours contre un contrat de concession du service public du développement et de l'exploitation du réseau de distribution et de fourniture d'énergie électrique aux tarifs réglementés, attribué à la société Enedis.,,,Requérants se prévalant de leur qualité de contribuables locaux pour contester, d'une part, la validité des clauses relatives à la délimitation du périmètre des ouvrages concédés, dont ils estimaient qu'elles n'incluaient pas certains dispositifs dans les biens de retour, d'autre part, la validité des clauses relatives à l'indemnité susceptible d'être versée au concessionnaire en cas de rupture anticipée du contrat, dont ils estimaient que l'application pouvait excéder le montant du préjudice réellement subi par ce dernier et constituer de ce fait une libéralité prohibée.,,,L'intérêt à agir des requérants en tant que contribuables locaux ne peut être écarté en se fondant sur le caractère aléatoire du déploiement des dispositifs exclus de la liste des ouvrages concédés et sur le caractère incertain de la mise en oeuvre de la clause relative à la rupture anticipée du contrat : d'une part, le caractère éventuel ou incertain de la mise en oeuvre de clauses est par lui-même dépourvu d'incidence sur l'appréciation de leur répercussion possible sur les finances ou le patrimoine de l'autorité concédante ; d'autre part, bien que l'article L. 111-52 du code de l'énergie fixe des zones de desserte exclusives pour les gestionnaires de réseaux publics et attribue de ce fait un monopole légal à la société Enedis et que la convention litigieuse a été conclue pour 30 ans, au vu des évolutions scientifiques, techniques, économiques et juridiques propres au secteur de l'énergie, des modifications d'une telle concession sont probables au cours de la période couverte par le contrat et pourraient notamment nécessiter la mise en oeuvre des clauses critiquées.</ANA>
<ANA ID="9D"> 54-05-05-01 La circonstance qu'un contrat de concession ait été résilié n'est pas de nature à priver d'objet le pourvoi tendant à son annulation.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, Assemblée, 4 avril 2014, Département de Tarn-et-Garonne, n° 358994, p. 70.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
