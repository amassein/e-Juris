<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042417973</ID>
<ANCIEN_ID>JG_L_2020_10_000000428434</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/41/79/CETATEXT000042417973.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème - 1ère chambres réunies, 09/10/2020, 428434, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-10-09</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>428434</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème - 1ère chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP CELICE, TEXIDOR, PERIER ; SCP SEVAUX, MATHONNET</AVOCATS>
<RAPPORTEUR>Mme Catherine Brouard-Gallet</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Raphaël Chambon</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:428434.20201009</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              M. A... B... a demandé au tribunal administratif de Nantes d'annuler pour excès de pouvoir la décision du 11 décembre 2015 par laquelle la ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social a, d'une part, annulé la décision du 22 avril 2015 de l'inspecteur du travail de la 36ème section de l'unité territoriale de Loire-Atlantique refusant à la Société nationale d'exploitation industrielle des tabacs et allumettes (SEITA) l'autorisation de le licencier et, d'autre part, accordé cette autorisation. Par un jugement n°s 1510256, 1601056 du 27 juin 2017, le tribunal administratif a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 17NT02562 du 26 décembre 2018, la cour administrative d'appel de Nantes a, sur appel de M. B..., annulé ce jugement et la décision de la ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 26 février et 27 mai 2019 et le 23 janvier 2020 au secrétariat du contentieux du Conseil d'Etat, la SEITA demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) de mettre à la charge de M. B... la somme de 2 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code du travail ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Catherine Brouard-Gallet, conseiller d'Etat en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Raphaël Chambon, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Célice, Texidor, Perier, avocat de la Société nationale d'exploitation industrielle des tabacs et des allumettes et à la SCP Sevaux, Mathonnet, avocat de M. B... ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que la Société nationale d'exploitation industrielle des tabacs et allumettes (SEITA), filiale française du groupe Imperial Tobacco, a sollicité l'autorisation de licencier pour motif économique M. B..., salarié protégé de la société, dans le cadre du projet de fermeture de son site de production situé à Carquefou. Par une décision du 22 avril 2015, l'inspecteur du travail de la 36ème section de l'unité territoriale de Loire-Atlantique a refusé d'autoriser la SEITA à licencier M. B.... Toutefois, par une décision du 15 décembre 2015, la ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social a annulé cette décision de l'inspecteur du travail et autorisé la SEITA à licencier le salarié. Par un jugement du 27 juin 2017, le tribunal administratif de Nantes a rejeté la demande de M. B... tendant à l'annulation de la décision de la ministre. Par un arrêt du 26 décembre 2018, la cour administrative d'appel de Nantes, saisie par M. B..., a annulé le jugement et la décision de la ministre du travail. La SEITA se pourvoit en cassation contre cet arrêt.<br/>
<br/>
              2. En vertu des dispositions du code du travail, le licenciement des salariés légalement investis de fonctions représentatives, qui bénéficient, dans l'intérêt de l'ensemble des travailleurs qu'ils représentent, d'une protection exceptionnelle, est subordonné à une autorisation de l'inspecteur du travail dont dépend l'établissement. Lorsque le licenciement d'un de ces salariés est envisagé, ce licenciement ne doit pas être en rapport avec les fonctions représentatives normalement exercées ou l'appartenance syndicale de l'intéressé. Dans le cas où la demande d'autorisation de licenciement est fondée sur un motif de caractère économique, il appartient à l'inspecteur du travail et, le cas échéant, au ministre de rechercher, sous le contrôle du juge de l'excès de pouvoir, si la situation de l'entreprise justifie le licenciement, en tenant compte notamment de la nécessité des réductions envisagées d'effectifs et de la possibilité d'assurer le reclassement du salarié dans l'entreprise ou au sein du groupe auquel appartient cette dernière. En outre, pour refuser l'autorisation sollicitée, l'autorité administrative a la faculté de retenir des motifs d'intérêt général relevant de son pouvoir d'appréciation de l'opportunité, sous réserve qu'une atteinte excessive ne soit pas portée à l'un ou l'autre des intérêts en présence.<br/>
<br/>
              3. Aux termes de l'article L. 1233-3 du code du travail, dans sa rédaction applicable au litige : " Constitue un licenciement pour motif économique le licenciement effectué par un employeur pour un ou plusieurs motifs non inhérents à la personne du salarié résultant d'une suppression ou transformation d'emploi ou d'une modification, refusée par le salarié, d'un élément essentiel du contrat de travail, consécutives notamment à des difficultés économiques ou à des mutations technologiques ". La sauvegarde de la compétitivité de l'entreprise peut constituer un motif économique, à la condition que soit établie une menace pour la compétitivité de l'entreprise, laquelle s'apprécie, lorsque l'entreprise appartient à un groupe, au niveau du secteur d'activité dont relève l'entreprise en cause au sein du groupe. <br/>
<br/>
              4. Il ressort tant des pièces du dossier soumis aux juges du fond que des constatations effectuées par la cour administrative d'appel de Nantes dans l'arrêt attaqué que le groupe Imperial Tobacco réalise plus de 70 % du chiffre d'affaires net de son secteur tabac en Europe, que la production et la consommation de tabac ont diminué en volume de plus de 30 % en Europe entre 2002 et 2013 et qu'entre 2009 et 2013, le groupe a connu une perte de 1,5 point de part de marché. Si le chiffre d'affaires de l'activité tabac du groupe avait augmenté de 2 % en 2014 et 3 % en 2015, cette inversion de tendance récente n'était pas de nature à remettre en cause ces tendances structurelles, caractérisées par une forte et régulière rétractation du marché européen du tabac et par la réduction constante de la consommation de tabac sur ce marché et notamment sur le marché français, sur lesquels ce secteur d'activité réalise l'essentiel de son chiffre d'affaires. Dans ces conditions, en retenant qu'il n'existait pas de menace sérieuse pesant sur la compétitivité du groupe Imperial Tobacco au niveau de son secteur d'activité tabac de nature à justifier la réorganisation du groupe, pour en déduire que la réalité du motif économique allégué à l'appui de la demande d'autorisation de licenciement n'était pas établie, la cour administrative d'appel de Nantes a inexactement qualifié les faits qui lui étaient soumis. Par suite, la SEITA est fondée à demander l'annulation de son arrêt, sans qu'il soit besoin de se prononcer sur les autres moyens qu'elle soulève à l'appui de son pourvoi.<br/>
<br/>
              5. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à la charge de la SEITA, qui n'est pas, dans la présente instance, la partie perdante. Il n'y a par ailleurs pas lieu, dans les circonstances de l'espèce, de mettre à la charge de M. B... la somme demandée par la SEITA au titre des mêmes dispositions.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt n° 17NT02562 de la cour administrative d'appel de Nantes du 26 décembre 2018 est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Nantes.<br/>
Article 3 : Les conclusions présentées par les parties au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée à la Société nationale d'exploitation industrielle des tabacs et allumettes et à M. A... B...<br/>
Copie en sera adressée à la ministre du travail, de l'emploi et de l'insertion.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
