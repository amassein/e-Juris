<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026230121</ID>
<ANCIEN_ID>JG_L_2012_07_000000349824</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/23/01/CETATEXT000026230121.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème et 9ème sous-sections réunies, 27/07/2012, 349824, Publié au recueil Lebon</TITRE>
<DATE_DEC>2012-07-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>349824</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème et 9ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DE CHAISEMARTIN, COURJON ; FOUSSARD</AVOCATS>
<RAPPORTEUR>M. Thierry Carriol</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Edouard Crépey</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2012:349824.20120727</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 3 juin et 31 août 2011 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. A...B..., demeurant..., ; M.  B...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler la décision n° 09000998 du 19 novembre 2010 de la Cour nationale du droit d'asile en tant que celle-ci, annulant la décision du 22 décembre 2008 du directeur général de l'Office français de protection des réfugiés et apatrides rejetant sa demande d'asile, et lui accordant le bénéfice de la protection subsidiaire, ne lui a pas reconnu le statut de réfugié ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Office français de protection des réfugiés et apatrides la somme de 2 500 euros à verser la SCP de Chaisemartin-Courjon, son avocat, au titre des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la convention de Genève relative au statut des réfugiés et le protocole de New York ;<br/>
<br/>
              Vu la directive 2004/83/CE du Conseil du 29 avril 2004 ;<br/>
<br/>
              Vu le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
<br/>
              Vu la loi n° 91-647 du 10 juillet 1991 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Thierry Carriol, Maître des Requêtes en service extraordinaire,  <br/>
<br/>
              - les observations de la SCP de Chaisemartin, Courjon, avocat de M. B...et de Me Foussard, avocat de l'Office français de protection des réfugiés et apatrides,<br/>
<br/>
              - les conclusions de M. Edouard Crépey, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP de Chaisemartin, Courjon, avocat de M. B...et à Me Foussard, avocat de l'Office français de protection des réfugiés et apatrides ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes du 2° du paragraphe A de l'article 1er de la convention de Genève du 28 juillet 1951, la qualité de réfugié est reconnue à : " toute personne qui (...), craignant avec raison d'être persécutée du fait de sa race, de sa religion, de sa nationalité, de son appartenance à un certain groupe social ou de ses opinions politiques, se trouve hors du pays dont elle a la nationalité et qui ne peut ou, du fait de cette crainte, ne veut se réclamer de la protection de ce pays ; " ; qu'aux termes de l'article 10, paragraphe 1 d) de la directive 2004/83/CE du Conseil du 29 avril 2004 : " Un groupe est considéré comme un certain groupe social lorsque, en particulier : / - ses membres partagent (...) une caractéristique ou une croyance à ce point essentielle pour l'identité ou la conscience qu'il ne devrait pas être exigé d'une personne qu'elle y renonce, et / - ce groupe a son identité propre dans le pays en question parce qu'il est perçu comme étant différent par la société environnante. / En fonction des conditions qui prévalent dans le pays d'origine, un groupe social spécifique peut être un groupe dont les membres ont pour caractéristique commune une orientation sexuelle. " ;<br/>
<br/>
              2. Considérant qu'un groupe social est, au sens de ces dispositions, constitué de personnes partageant un caractère inné, une histoire commune ou une caractéristique essentielle à leur identité et à leur conscience, auxquels il ne peut leur être demandé de renoncer, et une identité propre perçue comme étant différente par la société environnante ou par les institutions ; qu'en fonction des conditions qui prévalent dans un pays, des personnes peuvent, à raison de leur orientation sexuelle, constituer un groupe social au sens de ces dispositions ; qu'il convient dès lors, dans l'hypothèse où une personne sollicite le bénéfice du statut de réfugié à raison de son orientation sexuelle, d'apprécier si les conditions existant dans le pays dont elle a la nationalité permettent d'assimiler les personnes se revendiquant de la même orientation sexuelle à un groupe social du fait du regard que portent sur ces personnes la société environnante ou les institutions et dont les membres peuvent craindre avec raison d'être persécutés du fait même de leur appartenance à ce groupe ;<br/>
<br/>
              3. Considérant qu'il résulte de ce qui précède que l'octroi du statut de réfugié du fait de persécutions liées à l'appartenance à un groupe social fondé sur des orientations sexuelles communes ne saurait être subordonné à la manifestation publique de cette orientation sexuelle par la personne qui sollicite le bénéfice du statut de réfugié dès lors que le groupe social, au sens des dispositions précitées, n'est pas institué par ceux qui le composent, ni même du fait de l'existence objective de caractéristiques qu'on leur prête mais par le regard que portent sur ces personnes la société environnante ou les institutions ; que la circonstance que l'appartenance au groupe social ne fasse l'objet d'aucune disposition pénale répressive spécifique est sans incidence sur l'appréciation de la réalité des persécutions à raison de cette appartenance qui peut, en l'absence de toute disposition pénale spécifique, reposer soit sur des dispositions de droit commun abusivement appliquées au groupe social considéré, soit sur des comportements émanant des autorités, encouragés ou favorisés par ces autorités ou même simplement tolérés par elles ;<br/>
<br/>
              4. Considérant, dès lors, qu'en refusant à M.  B...le statut de réfugié au motif, d'une part, que l'intéressé n'établissait pas qu'il aurait manifesté son orientation sexuelle et, d'autre part, que l'homosexualité n'est pas réprimée par le code pénal de la République démocratique du Congo, la Cour nationale du droit d'asile a commis une double erreur de droit ; que, par suite et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, sa décision doit être annulée ;<br/>
<br/>
              5. Considérant que M. B...a obtenu le bénéfice de l'aide juridictionnelle ; que, par suite, son avocat peut se prévaloir des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 ; qu'il y a lieu, dans les circonstances de l'espèce, et sous réserve que la SCP de Chaisemartin - Courjon, avocat de M.B..., renonce à percevoir la somme correspondant à la part contributive de l'Etat, de mettre à la charge de  l'Office français de protection des réfugiés et apatrides la somme de 2 500 euros à verser à la SCP de Chaisemartin - Courjon ;<br/>
<br/>
<br/>
<br/>				D E C I D E  :<br/>
              				--------------<br/>
<br/>
Article 1er : La décision de la Cour nationale du droit d'asile du 19 novembre 2010 est annulée.<br/>
<br/>
Article 2 : L'affaire est renvoyée à la Cour nationale du droit d'asile.<br/>
Article 3 : L'Office français de protection des réfugiés et apatrides versera à la SCP de Chaisemartin - Courjon, avocat de M.B..., une somme de 2 500 euros en application des dispositions du deuxième alinéa de l'article 37 de la loi du 10 juillet 1991, sous réserve que cette société renonce à percevoir la somme correspondant à la part contributive de l'Etat.<br/>
Article 4 : La présente décision sera notifiée à M. A... B...et à l'Office français de protection des réfugiés et apatrides.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">095-03-01-02-03-05 - 1) NOTION DE GROUPE SOCIAL - DÉFINITION - 2) CAS DE L'ORIENTATION SEXUELLE - A) CONDITIONS D'APPRÉCIATION - B) EXIGENCE D'UNE MANIFESTATION DE L'ORIENTATION SEXUELLE - ABSENCE - C) EXIGENCE D'UNE LÉGISLATION PÉNALE RÉPRESSIVE - ABSENCE.
</SCT>
<ANA ID="9A"> 095-03-01-02-03-05 1) Un groupe social est constitué de personnes partageant un caractère inné, une histoire commune ou une caractéristique essentielle à leur identité et à leur conscience, auxquels il ne peut leur être demandé de renoncer, et une identité propre perçue comme étant différente par la société environnante ou par les institutions.... ...2) En fonction des conditions qui prévalent dans un pays, des personnes peuvent, à raison de leur orientation sexuelle, constituer un groupe social.... ...a) Dès lors, dans l'hypothèse où une personne sollicite le bénéfice du statut de réfugié à raison de son orientation sexuelle, il convient d'apprécier si les conditions existant dans le pays dont elle a la nationalité permettent d'assimiler les personnes se revendiquant de la même orientation sexuelle à un groupe social du fait du regard que portent sur ces personnes la société environnante ou les institutions et dont les membres peuvent craindre avec raison d'être persécutés du fait même de leur appartenance à ce groupe.,,b) L'octroi du statut de réfugié du fait de persécutions liées à l'appartenance à un groupe social fondé sur des orientations sexuelles communes ne saurait être subordonné à la manifestation publique de cette orientation sexuelle par la personne qui sollicite le bénéfice du statut de réfugié dès lors que le groupe social n'est pas institué par ceux qui le composent, ni même du fait de l'existence objective de caractéristiques qu'on leur prête mais par le regard que portent sur ces personnes la société environnante ou les institutions.,,c) La circonstance que l'appartenance au groupe social ne fasse l'objet d'aucune disposition pénale répressive spécifique est sans incidence sur l'appréciation de la réalité des persécutions à raison de cette appartenance qui peut, en l'absence de toute disposition pénale spécifique, reposer soit sur des dispositions de droit commun abusivement appliquées au groupe social considéré, soit sur des comportements émanant des autorités, encouragés ou favorisés par ces autorités ou même simplement tolérés par elles.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
