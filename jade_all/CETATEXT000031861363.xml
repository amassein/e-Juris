<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031861363</ID>
<ANCIEN_ID>JG_L_2015_12_000000389759</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/86/13/CETATEXT000031861363.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème SSJS, 23/12/2015, 389759, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-12-23</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>389759</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème SSJS</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP THOUIN-PALAT, BOUCARD ; SCP BARTHELEMY, MATUCHANSKY, VEXLIARD, POUPOT</AVOCATS>
<RAPPORTEUR>Mme Florence Chaltiel-Terral</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Sophie-Justine Lieber</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2015:389759.20151223</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu les procédures suivantes :<br/>
<br/>
              Le conseil départemental de la Ville de Paris de l'ordre des médecins a décidé le 12 novembre 2014 de saisir le conseil régional d'Ile-de-France de l'ordre des médecins d'une demande d'expertise de Mme A...B...au titre de l'article R. 4124-3 du code de la santé publique. Le conseil régional n'ayant pas statué dans le délai de deux mois a transmis la demande au Conseil national de l'ordre des médecins. Par une décision du 11 février 2015, le Conseil national de l'ordre des médecins, statuant en formation restreinte, a suspendu Mme A...B...du droit d'exercer la médecine pendant une durée d'un an et décidé que la reprise de son activité professionnelle sera subordonnée aux résultats d'une nouvelle expertise diligentée par le conseil régional d'Ile-de-France de l'ordre des médecins.<br/>
<br/>
              1° Sous le n° 389759, par une requête sommaire et un mémoire complémentaire, enregistrés les 24 avril et 16 juin 2015 au secrétariat du contentieux du Conseil d'Etat, Mme B...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir cette décision ; <br/>
<br/>
              2°) de mettre à la charge du Conseil national de l'ordre des médecins la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              2° Sous le n° 391039, par une requête enregistrée le 15 juin 2015 au secrétariat du contentieux du Conseil d'Etat, Mme B...demande au Conseil d'Etat d'ordonner, sur le fondement de l'article R. 821-5 du code de justice administrative, le sursis à exécution de la décision du 11 février 2015 de la formation restreinte du Conseil national de l'ordre des médecins.<br/>
<br/>
              Vu la décision attaquée et dont le sursis est demandé ;<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu :<br/>
              - le code de la santé publique ;<br/>
              - la loi n° 2000-321 du 12 avril 2000 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Florence Chaltiel-Terral, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Sophie-Justine Lieber, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Thouin-Palat, Boucard, avocat de Mme B...et à la SCP Barthélemy, Matuchansky, Vexliard, Poupot, avocat du Conseil national de l'ordre des médecins ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il y a lieu de joindre les requêtes de Mme B...tendant, d'une part à l'annulation de la décision du Conseil national de l'ordre des médecins du 11 février 2015, d'autre part à ce qu'il soit sursis à l'exécution de cette décision ; <br/>
<br/>
              Sur la requête tendant à l'annulation de la décision du 11 février 2015 :<br/>
<br/>
              2. Considérant que les décisions prononçant, sur le fondement de l'article R. 4124-3 du code de la santé publique, la suspension temporaire du droit d'exercer d'un médecin sont des décisions à caractère administratif ; qu'en application de l'article 4 de la loi du 12 avril 2000 relative aux droits des citoyens dans leurs relations avec les administrations, toute décision administrative doit être signée par son auteur ; que la signature par le président d'une autorité administrative collégiale satisfait à cette obligation ; qu'en l'espèce, il ressort des pièces du dossier que la décision attaquée prise par le conseil national de l'ordre siégeant en formation restreinte est signée par son président ; qu'aucune disposition législative ou réglementaire ni aucun principe n'imposent que cette décision soit signée par un secrétaire de séance ;<br/>
<br/>
              3. Considérant qu'il résulte de l'article R. 4124-3 du code de la santé publique que la suspension temporaire du droit d'exercer la médecine, qui peut intervenir " dans le cas d'infirmité ou d'état pathologique rendant dangereux l'exercice de la profession ", " ne peut être ordonnée que sur un rapport motivé établi à la demande du conseil par trois médecins spécialistes désignés comme experts " ; que si cet article prévoit que ce rapport doit être déposé dans le délai de six semaines à compter de la saisine du conseil, ce délai, qui en l'espèce a été respecté eu égard à la date de saisine du conseil national, n'a en tout état de cause pas été prescrit à peine de nullité ; qu'en l'espèce, le rapport remis au conseil national a été transmis par télécopie au conseil de Mme B...dès le lendemain de sa réception et une semaine avant l'audience ; que la requérante, qui avait déjà préalablement été informée de la date d'audience, avait pu avoir accès à toutes les autres pièces du dossier, et a pu, ainsi que son conseil, être entendue par la formation restreinte du Conseil national de l'ordre des médecins, n'est pas fondée à soutenir que la décision aurait été rendue au terme d'une procédure irrégulière faute de respecter les droits de la défense ; <br/>
<br/>
              4. Considérant qu'il ressort des termes mêmes de la décision attaquée qu'elle est fondée, outre sur le rapport d'expertise, qui est suffisamment motivé, sur les éléments de fait signalés par l'entourage professionnel de MmeB..., sur les observations écrites et orales qu'elle a présentées au cours de la procédure et sur l'appréciation propre qu'a portée la formation restreinte du Conseil national de l'ordre des médecins ; qu'ainsi, la requérante n'est pas fondée à soutenir que cette dernière se serait à tort crue liée par l'avis du rapport d'expertise ; <br/>
<br/>
              5. Considérant qu'en estimant, au vu de l'ensemble des pièces du dossier et de la procédure devant elle, que MmeB..., pédiatre, présentait un état pathologique qui rendait dangereux l'exercice de la profession médicale et en suspendant son droit d'exercer pendant un an, la formation restreinte du Conseil national de l'ordre des médecins, qui a suffisamment motivé sa décision, n'a pas commis d'erreur manifeste d'appréciation ; <br/>
<br/>
              6. Considérant qu'il résulte de tout ce qui précède que la requête de Mme B... doit être rejetée ; que le Conseil national de l'ordre des médecins n'étant pas, dans la présente instance, la partie perdante, les conclusions de Mme B...tendant à l'application des dispositions de l'article L. 761-1 du code de justice administrative ne peuvent qu'être rejetées ; <br/>
<br/>
              Sur la requête tendant au sursis à exécution de cette décision :<br/>
<br/>
              7. Considérant qu'ainsi qu'il a été dit au point 2, la décision en cause ne présente pas de caractère juridictionnel ; qu'ainsi les conclusions présentées sur le fondement de l'article R. 821-5 du code de justice administrative sont manifestement irrecevables et ne peuvent, par suite, qu'être rejetées ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Les requêtes  de Mme B...sont rejetées.<br/>
Article 2 : La présente décision sera notifiée à Mme A...B...et au Conseil national de l'ordre des médecins.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
