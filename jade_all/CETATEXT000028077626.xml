<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028077626</ID>
<ANCIEN_ID>JG_L_2013_10_000000357444</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/07/76/CETATEXT000028077626.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  6ème sous-section jugeant seule, 16/10/2013, 357444, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-10-16</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>357444</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 6ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FABIANI, LUC-THALER ; SCP DIDIER, PINET</AVOCATS>
<RAPPORTEUR>M. Didier Ribes</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Suzanne von Coester</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2013:357444.20131016</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu 1°, sous le n° 357444, le pourvoi sommaire et le mémoire complémentaire, enregistrés les 8 mars 2012 et 8 juin 2012 au secrétariat du contentieux du Conseil d'État, présentés pour le Groupement foncier agricole (GFA) du  Château du Villard, dont le siège est au Château du Villard à Saint-Arcons-de-Barges (43420), représenté par son gérant en exercice ; le GFA Château du Villard demande au Conseil d'État :<br/>
<br/>
              1°) d'annuler l'arrêt n° 10LY02340 du 2 février 2012 par lequel la cour administrative d'appel de Lyon, à la demande de M. C...B...et Mme A...B..., a annulé, d'une part, le jugement du tribunal administratif de Lyon du 16 juillet 2010, en tant qu'il a rejeté les conclusions de M. et Mme B...contre la décision du 8 décembre 2007 du maire de Saint-Arcons-de-Barges ne faisant pas opposition à la déclaration préalable relative au projet de construction d'une fumière du groupement foncier agricole (GFA) du Château du Villard, d'autre part, la décision du maire de Saint-Arcons-de-Barges du 8 décembre 2007, dans la mesure où elle ne s'oppose pas à cette déclaration préalable et la décision implicite rejetant le recours gracieux dirigé contre cette décision ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter les conclusions d'appel de M. et Mme B... ; <br/>
<br/>
              3°) de mettre à la charge de M. et Mme B...la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu 2°, sous le n° 358425, le pourvoi, enregistré le 10 avril 2012 au secrétariat du contentieux du Conseil d'État, présenté par le ministre de l'écologie, du développement durable, des transports et du logement, qui demande au Conseil d'État d'annuler le même arrêt ;<br/>
<br/>
<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu les autres pièces des dossiers ; <br/>
<br/>
              Vu le code de l'environnement ; <br/>
<br/>
              Vu le code de l'urbanisme ; <br/>
<br/>
              Vu l'arrêté du 7 février 2005 du ministre de l'écologie et du développement durable ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Didier Ribes, Maître des Requêtes, <br/>
<br/>
              - les conclusions de Mme Suzanne von Coester, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Didier, Pinet, avocat du GFA du Château du Villard, et à la SCP Fabiani, Luc-Thaler, avocat de M. et Mme B...;<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que par deux décisions des 8 décembre 2007 et 3 novembre 2008, le maire de Saint-Arcons-de-Barges, agissant au nom de l'État, d'une part, n'a pas fait opposition à la déclaration de travaux déposée par le groupement foncier agricole (GFA) du Château du Villard en vue de la construction d'une fumière et d'une plateforme pour silo et, d'autre part, lui a accordé un permis de construire pour la construction d'un hangar de stockage de matériel agricole ; que M. et Mme B..., qui sont propriétaires d'un ensemble immobilier situé au voisinage de ces projets, ont contesté ces deux décisions devant le tribunal administratif de Clermont-Ferrand ainsi que la décision du maire rejetant implicitement le recours gracieux formé contre ces décisions ; que par un jugement du 16 juillet 2010, le tribunal a rejeté leur demande ; que par un arrêt du 2 février 2012, la cour administrative d'appel de Lyon a, en premier lieu, annulé le jugement du tribunal administratif en tant qu'il a rejeté les conclusions de M. et Mme B...contre la décision du 8 décembre 2007 ne faisant pas opposition au projet de fumière, en deuxième lieu, annulé cette même décision et le rejet implicite du recours dirigé contre elle et, en troisième lieu, rejeté les conclusions présentées par les intéressés à fin d'annulation du permis de construire un hangar de stockage et de la décision de non opposition à la déclaration de travaux en tant qu'elle concerne la plateforme pour silo ; que le GFA du Château du Villard et le ministre de l'écologie, du développement durable, des transports et du logement se pourvoient en cassation contre cet arrêt en tant qu'il a partiellement annulé le jugement du tribunal administratif et annulé la décision du maire de Saint-Arcons-de-Barges ; qu'il y a lieu de joindre les deux pourvois pour statuer par une seule décision ;<br/>
<br/>
              Sur les conclusions à fin d'annulation de l'arrêt attaqué :<br/>
<br/>
              2. Considérant qu'il résulte des dispositions du deuxième alinéa de l'article    R. 811-1 du code de justice administrative, combinées avec celles du 1° de l'article R. 222-13 du même code, que le tribunal administratif statue en premier et dernier ressort dans les litiges relatifs aux déclarations préalables prévues par l'article L. 421-4 du code de l'urbanisme ; que la contestation par les époux B...de l'arrêté du 8 décembre 2007 concerne une décision de non opposition à une déclaration préalable prévue à cet article ; qu'elle est donc au nombre des litiges sur lesquels le tribunal administratif statue en premier et dernier ressort ; que cette demande, relevant d'une voie de recours distincte de celle concernant la délivrance du permis de construire précité, ne saurait présenter avec elle un lien de connexité ; que la cour administrative d'appel de Lyon était, par suite, incompétente pour statuer par la voie de l'appel sur le jugement du tribunal administratif en tant qu'il a statué sur les conclusions relatives à la déclaration de travaux déposée en vue de la construction d'une fumière et d'une plateforme pour silo ; qu'ainsi, sans qu'il soit besoin d'examiner les autres moyens des pourvois, le GFA du Château du Villard et le ministre de l'écologie, du développement durable, des transports et du logement sont fondés à demander, dans cette mesure, l'annulation de l'arrêt attaqué ; <br/>
<br/>
              Sur les conclusions à fin d'annulation du jugement du 16 juillet 2010 : <br/>
<br/>
              3. Considérant qu'il appartient au Conseil d'Etat de statuer, en tant que juge de cassation, sur les conclusions présentées par M. et Mme B...devant la cour administrative d'appel de Lyon en tant qu'elles sont dirigées contre le jugement du 16 juillet 2010 du tribunal administratif de Clermont-Ferrand, en tant qu'il a rejeté leurs conclusions tendant à l'annulation de la décision du maire de Saint-Arcons-de-Barges du 8 décembre 2007 et du rejet implicite du recours contre cette décision ;<br/>
<br/>
              4. Considérant, en premier lieu, qu'aux termes de l'article 155.1 de l'arrêté du préfet de la Haute-Loire du 20 juin 1979, relatif à " l'implantation des dépôts à caractère permanent " : " ... Ces dépôts doivent être [...] établis à une distance d'au moins 50 mètres des immeubles habités ou habituellement occupés par des tiers, des zones de loisirs ou de tout établissement recevant du public... " ; que l'article 157-3 du même arrêté dispose : " Silos non aménagés / il peut être dérogé aux règles d'étanchéité des radiers et des parois lorsqu'elles existent, sous réserve que de tels silos soient implantés : / à plus de 50 mètres des immeubles habités ou occupés habituellement par des tiers, des zones de loisirs et des établissements recevant du public, / à plus de 100 mètres des divers points d'eau énumérés au paragraphe 157.2 " ; <br/>
<br/>
              5. Considérant que si M. et Mme B...soutiennent que les distances qui séparent la fumière et le silo projetés de la dépendance affectée à l'usage de salle de jeux, de remise, d'atelier et de garage sont inférieures à 50 mètres, c'est par une appréciation souveraine des faits, exempte de dénaturation et sans erreur de droit, que le tribunal administratif a estimé que ce bâtiment n'avait pas un usage d'habitation au sens des dispositions précitées du règlement sanitaire départemental ; qu'il ressort de l'ensemble des pièces du dossier que le tribunal administratif n'a pas dénaturé les faits de l'espèce en estimant que le bâtiment principal à usage d'habitation est situé à plus de 50 mètres de la fumière et du silo ; <br/>
<br/>
              6. Considérant qu'il résulte de ce qui précède que le moyen tiré de ce que le tribunal administratif aurait méconnu les dispositions précitées du règlement sanitaire départemental doit être écarté ; <br/>
<br/>
              7. Considérant, en deuxième lieu, qu'aux termes de l'article R. 111-2 du code de l'urbanisme : " Le projet peut être refusé ou n'être accepté que sous réserve de l'observation de prescriptions spéciales s'il est de nature à porter atteinte à la salubrité ou à la sécurité publique du fait de sa situation, de ses caractéristiques, de son importance ou de son implantation à proximité d'autres installations " ; qu'aux termes de l'article R. 111-3 du même code : " Le projet peut être refusé ou n'être accepté que sous réserve de l'observation de prescriptions spéciales s'il est susceptible, en raison de sa localisation, d'être exposé à des nuisances graves, dues notamment au bruit. " ; qu'en estimant qu'eu égard, d'une part, à l'endroit d'implantation des constructions en litige et, d'autre part, à l'absence d'éléments suffisamment probants exposés par les requérants pour établir, dans les circonstances de l'espèce, les atteintes aux intérêts visés à l'article R. 111-2, les nuisances mentionnées à l'article R. 111-3 ainsi que les atteintes portées au site, l'arrêté en litige n'avait pas méconnu ces dispositions, le tribunal administratif a porté sur les faits de l'espèce une appréciation souveraine, qui n'est pas entachée de dénaturation, et n'a pas commis d'erreur de droit ; <br/>
<br/>
              8. Considérant, en dernier lieu, que l'arrêté du ministre de l'écologie et du développement durable du 7 février 2005 fixant les règles techniques auxquelles doivent satisfaire les élevages de bovins, de volailles et/ou de gibiers à plumes et de porcs soumis à autorisation au titre du livre V du code de l'environnement a été pris en application de la législation relative aux installations classées pour la protection de l'environnement, en particulier des articles L. 511-1 et L. 512-10 du code de l'environnement ; que la vérification du respect des prescriptions contenues dans cet arrêté ne s'impose pas à l'autorité délivrant des autorisations d'urbanisme ; qu'ainsi, c'est sans erreur de droit que le tribunal administratif  a jugé que le moyen tiré de la méconnaissance des règles techniques fixées par ces prescriptions, notamment de distance et d'insertion dans le paysage, ne pouvait être utilement invoqué à l'encontre de l'arrêté en litige ; <br/>
<br/>
              9. Considérant qu'il résulte de tout ce qui précède que M. et Mme B...ne sont pas fondés à demander l'annulation du jugement du tribunal administratif de Clermont-Ferrand en tant qu'il a rejeté leurs conclusions dirigées contre la décision du maire de Saint-Arcons-de-Barges du 8 décembre 2007 de ne pas faire opposition à la déclaration de travaux présentée par le GFA du Château du Villard ; <br/>
<br/>
              Sur les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative : <br/>
<br/>
              10. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mis à la charge de l'État et du GFA du Château du Villard, qui ne sont pas les parties perdantes dans la présente instance, le versement de la somme que M. et Mme B... demandent au titre des frais exposés par eux et non compris dans les dépens ; qu'il y a lieu, en revanche, de mettre à la charge de M. et Mme B...la somme de 3 000 euros que le GFA du Château du Villard demande au titre des mêmes dispositions ; <br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Les articles 1 et 2 de l'arrêt du 2 février 2012 de la cour administrative d'appel de Lyon, ainsi que l'article 3 en tant qu'il a statué sur les conclusions de M. et Mme B...tendant à l'annulation du jugement du tribunal administratif de Clermont-Ferrand en tant qu'il a statué sur les conclusions dirigées contre l'arrêté du 8 décembre 2007 du maire de Saint-Arcons-de-Barges de ne pas s'opposer au projet de construction d'un silo du GFA du Château du Villard, sont annulés. <br/>
<br/>
Article 2 : Le pourvoi présenté par M. et Mme B...contre le jugement du tribunal administratif de Clermont-Ferrand, en tant que ce jugement a rejeté les conclusions des intéressés contre l'arrêté du 8 décembre 2007 du maire de Saint-Arcons-de-Barges, est rejeté. <br/>
<br/>
Article 3 : M. et Mme B...verseront au GFA du Château du Villard la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
      Article 4 : Le surplus des conclusions du GFA du Château du Villard est rejeté. <br/>
<br/>
Article 5 : La présente décision sera notifiée au Groupement foncier agricole du Château du Villard, à M. et Mme C...B..., à la ministre de l'égalité des territoires et du logement et au ministre de l'écologie, du développement durable et de l'énergie. Copie en sera adressée à la commune de Saint-Arcons-de-Bages. <br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
