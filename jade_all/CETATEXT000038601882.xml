<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038601882</ID>
<ANCIEN_ID>JG_L_2019_06_000000416808</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/60/18/CETATEXT000038601882.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème chambre, 12/06/2019, 416808, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-06-12</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>416808</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MONOD, COLIN, STOCLET</AVOCATS>
<RAPPORTEUR>M. Laurent Domingo</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Karin Ciavaldini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2019:416808.20190612</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B...A...a demandé au tribunal administratif de Pau de prononcer la décharge des cotisations de taxe foncière sur les propriétés bâties auxquelles il a été assujetti au titre des années 2014 à 2016. Par une ordonnance n° 1700659 du 24 octobre 2017, le vice-président de ce tribunal a rejeté sa demande.  <br/>
<br/>
              Par une ordonnance n° 17BX03898 du 20 décembre 2017, enregistrée le 27 décembre 2017 au secrétariat du contentieux du Conseil d'Etat, le président de la cour administrative d'appel de Bordeaux a transmis au Conseil d'Etat, en application de l'article R. 351-2 du code de justice administrative, le pourvoi, enregistré le 11 décembre 2017 au greffe de cette cour, présenté par M.A.... Par ce pourvoi et deux nouveaux mémoires, enregistrés les 27 février et 6 avril 2018 au secrétariat du contentieux du Conseil d'Etat, M. A...demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Laurent Domingo, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Karin Ciavaldini, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Monod, Colin, Stoclet, avocat de M. A...;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis au juge du fond que, par des réclamations formées, respectivement, les 1er octobre 2014 et 29 septembre 2016, le 4 juillet 2016 et le 13 septembre 2016, M. A...a demandé à l'administration de prononcer la décharge des cotisations de taxe foncière sur les propriétés bâties auxquelles il a été assujetti au titre des années 2014, 2015 et 2016. Par une décision du 13 décembre 2016, l'administration a rejeté certaines de ces réclamations. M. A...se pourvoit en cassation contre l'ordonnance du 24 octobre 2017 par laquelle le vice-président du tribunal administratif de Pau a, sur le fondement de l'article R. 222-1 du code de justice administrative, rejeté comme tardive sa demande tendant à la décharge de l'ensemble des cotisations litigieuses. <br/>
<br/>
              En ce qui concerne la cotisation établie au titre de l'année 2016 :<br/>
<br/>
              2. Il ressort des pièces du dossier soumis au juge du fond que la décision de rejet de l'administration en date du 13 décembre 2016 répond à la réclamation " concernant la taxe foncière 2015 " déposée " le 04 juillet 2016 " et à la réclamation " concernant la taxe foncière 2014 " déposée le " 08 octobre 2016 ". En jugeant qu'il ressortait de cette décision que l'administration rejetait ainsi les réclamations formulées contre les cotisations de taxe foncière émises au titre des années 2014, 2015 et 2016 alors qu'elle ne concernait pas l'année 2016, le vice-président du tribunal administratif a dénaturé les pièces du dossier. Son ordonnance doit dès lors être annulée en tant qu'elle se prononce sur la cotisation de taxe foncière sur les propriétés bâties à laquelle M. A...a été assujetti au titre de l'année 2016.<br/>
<br/>
              En ce qui concerne les cotisations établies au titre des années 2014 et 2015 :<br/>
<br/>
              3. Aux termes de l'article R. 222-1 du code de justice administrative : " Les présidents de tribunal administratif et de cour administrative d'appel, les premiers vice-présidents des tribunaux et des cours (...), les présidents de formation de jugement des tribunaux et des cours (...) peuvent, par ordonnance : / (...) 4° Rejeter les requêtes manifestement irrecevables, lorsque la juridiction n'est pas tenue d'inviter leur auteur à les régulariser ou qu'elles n'ont pas été régularisées à l'expiration du délai imparti par une demande en ce sens (...) ". <br/>
<br/>
              4. Il ressort des pièces de la procédure devant le tribunal administratif de Pau que, saisi de la demande de M.A..., son greffe l'a communiquée au directeur départemental des finances publiques des Pyrénées-Atlantiques. A l'appui de son mémoire en défense, ce dernier a soulevé une fin de non-recevoir tirée du caractère tardif de cette demande et produit à cette fin la décision mentionnée ci-dessus, datée du 13 décembre 2016, rejetant certaines des réclamations formées par le contribuable. Ce mémoire et cette pièce ont été communiqués le vendredi 6 octobre 2017 en fin d'après-midi à l'avocat de M.A.... Le courrier accompagnant cette communication ne fixait pas de délai de réponse mais indiquait " Afin de ne pas retarder la mise en état d'être jugé de votre dossier, vous avez tout intérêt, si vous l'estimez utile, à produire ces observations aussi rapidement que possible ". L'avocat de M. A...en a accusé réception le lundi 9 octobre 2017 au matin. Le 24 octobre 2017, sans qu'aucun acte de procédure ne soit intervenu entre-temps, le vice-président du tribunal administratif de Pau a rejeté la demande de M. A... comme tardive, sur le fondement des dispositions précitées du 4° de l'article R. 222-1 du code de justice administrative. <br/>
<br/>
              5. Si l'instruction ainsi conduite ne faisait pas obstacle à ce que le vice-président du tribunal administratif de Pau fasse usage des pouvoirs définis par les dispositions citées au point 3, il lui appartenait de fixer à M. A...un délai pour produire ses observations sur le mémoire en défense communiqué et d'attendre, pour statuer, que ce délai soit écoulé. M. A... est par suite fondé à soutenir que l'ordonnance attaquée, par laquelle le vice-président du tribunal a, en se fondant sur le mémoire en défense et la décision de rejet de réclamation produits par l'administration, rejeté comme tardive sa demande tendant à la décharge des cotisations de taxe foncière auxquelles il a été assujetti au titre des années 2014, 2015 et 2016, a été rendue en méconnaissance du caractère contradictoire de la procédure. Cette ordonnance doit donc, pour ce motif, être annulée en tant qu'elle statue sur les cotisations de taxe foncière sur les propriétés bâties auxquelles M. A...a été assujetti au titre des années 2014 et 2015.<br/>
<br/>
              6. Il résulte de tout ce qui précède, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, que l'ordonnance du 24 octobre 2017 attaquée doit être annulée.<br/>
<br/>
              7. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat une somme de 3 000 euros à verser à M.A..., au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'ordonnance du 24 octobre 2017 du vice-président du tribunal administratif de Pau est annulée.<br/>
Article 2 : L'affaire est renvoyée au tribunal administratif de Pau.<br/>
Article 3 : L'Etat versera une somme de 3 000 euros à M. A...au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à M. B...A...et au ministre de l'action et des comptes publics.  <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
