<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042375615</ID>
<ANCIEN_ID>JG_L_2020_09_000000424192</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/37/56/CETATEXT000042375615.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème - 5ème chambres réunies, 28/09/2020, 424192, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-09-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>424192</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème - 5ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BORE, SALVE DE BRUNETON, MEGRET ; SCP DIDIER, PINET</AVOCATS>
<RAPPORTEUR>M. Didier Ribes</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Olivier  Fuchs</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:424192.20200928</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société Socardel a demandé au tribunal administratif de Nantes, d'une part, d'annuler l'arrêté du préfet de la Mayenne du 7 décembre 2010 renouvelant, pour une durée de quarante ans, le règlement d'eau applicable aux ouvrages hydroélectriques exploités par la Société hydraulique d'études et de missions d'assistance (SHEMA) sur la Mayenne et reconduit, pour la même durée, l'autorisation d'exploiter ces installations dont bénéficiait cette société et, d'autre part, de la rétablir dans sa priorité de débit pour les ouvrages hydroélectriques qu'elle exploite au niveau des barrages de la Fourmondière supérieure et de la Fourmondière inférieure, situés sur la rivière Mayenne, sur le territoire de la commune d'Andouillé. Par un jugement n° 1500933 du 17 novembre 2016, le tribunal administratif de Nantes a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 17NT00477 du 13 juillet 2018, la cour administrative d'appel de Nantes a rejeté l'appel de la société Socardel contre ce jugement.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 14 septembre et 17 décembre 2018 et le 18 décembre 2019 au secrétariat du contentieux du Conseil d'État, la société Socardel demande au Conseil d'État :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'État et de la SHEMA la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              -   le code de l'énergie ;<br/>
              -   le code de l'environnement ;<br/>
              - le code de justice administrative et l'ordonnance n° 2020-305 du 25 mars 2020 modifiée ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Didier Ribes, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Olivier Fuchs, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Boré, Salve de Bruneton, Mégret, avocat de la société Socardel et à la SCP Didier, Pinet, avocat de la Société hydraulique d'études et de missions d'assistance ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que, par un décret du 4 juillet 1959, le ministre de l'industrie et du commerce a déclaré d'utilité publique et a concédé à Électricité de France (EDF), pour une durée de trente ans, l'aménagement et l'exploitation de seize microcentrales hydroélectriques situées en rive gauche de la Mayenne, dans le département de la Mayenne, dont celles de la Fourmondière supérieure et de la Fourmondière inférieure. Le 25 mars 2004, le préfet de la Mayenne a pris acte du transfert de ces microcentrales par EDF au profit de sa filiale, la Société hydraulique d'études et de missions d'assistance. Par un arrêté du 7 décembre 2010, le même préfet a, d'une part, renouvelé, pour une durée de quarante ans, le règlement d'eau applicable aux ouvrages hydroélectriques exploités par la Société hydraulique d'études et de missions d'assistance sur la Mayenne et, d'autre part, prolongé, pour la même durée, l'autorisation d'exploiter ces installations. Par un jugement du 17 novembre 2016, le tribunal administratif de Nantes a rejeté la demande de la société Socardel tendant à l'annulation de l'arrêté du 7 décembre 2010. Par un arrêt du 13 juillet 2018 contre lequel la société Socardel se pourvoit en cassation, la cour administrative d'appel de Nantes a rejeté son appel formé contre le jugement du tribunal administratif de Nantes.<br/>
<br/>
              2. D'une part, en vertu du I de l'article L. 211-1 du code de l'environnement, les dispositions des chapitres Ier à VII du titre " Eau et milieux aquatiques et marins " de ce code ont pour objet une gestion équilibrée et durable de la ressource en eau, qui prend en compte les adaptations nécessaires au changement climatique et vise à assurer les huit objectifs qui sont énumérés, notamment : " 5° La valorisation de l'eau comme ressource économique et, en particulier, pour le développement de la production d'électricité d'origine renouvelable ainsi que la répartition de cette ressource ". En vertu de l'article L. 214-1 du même code : " Sont soumis aux dispositions des articles L. 214-2 à L. 214-6 les installations, les ouvrages, travaux et activités réalisés à des fins non domestiques par toute personne physique ou morale, publique ou privée, et entraînant des prélèvements sur les eaux superficielles ou souterraines, restitués ou non, une modification du niveau ou du mode d'écoulement des eaux, la destruction de frayères, de zones de croissance ou d'alimentation de la faune piscicole ou des déversements, écoulements, rejets ou dépôts directs ou indirects, chroniques ou épisodiques, même non polluants ".<br/>
<br/>
              3. D'autre part, en vertu des dispositions de l'article L. 214-10 du code de l'environnement, dans leur rédaction alors applicable, les décisions prises en application des articles L. 214-1 à L. 214-6 et L. 214-8 peuvent être déférées à la juridiction administrative dans les conditions prévues à l'article L. 514-6 du même code. Ce dernier, relatif aux installations classées pour la protection de l'environnement, soumet certaines décisions prises au titre de cette législation, dont les autorisations d'exploitation, à un contentieux de pleine juridiction et précise qu'elles " peuvent être déférées à la juridiction administrative : / 1° Par les demandeurs ou exploitants, dans un délai de deux mois qui commence à courir du jour où lesdits actes leur ont été notifiés ; / 2° Par les tiers, personnes physiques ou morales, les communes intéressées ou leurs groupements, en raison des inconvénients ou des dangers que le fonctionnement de l'installation présente pour les intérêts visés à l'article L. 511-1, dans un délai de quatre ans à compter de la publication ou de l'affichage desdits actes, ce délai étant, le cas échéant, prolongé jusqu'à la fin d'une période de deux années suivant la mise en activité de l'installation. (...) ".<br/>
<br/>
              4. Il en résulte qu'en application des dispositions combinées des articles L. 214-10 et L. 514-6 du code de l'environnement, un établissement industriel ou commercial ne peut se voir reconnaître la qualité de tiers recevable à contester devant le juge une autorisation délivrée en application de l'article L. 214-1 à une entreprise, fût-elle concurrente, que dans les cas où les effets que les installations, ouvrages, travaux et activités autorisés comportent sur les objectifs protégés par la police de l'eau sont de nature à affecter par eux-mêmes les conditions d'exploitation de cet établissement industriel ou commercial. Il appartient à ce titre au juge administratif de vérifier si ce dernier justifie d'un intérêt suffisamment direct lui donnant qualité pour demander l'annulation de l'autorisation en cause, compte tenu des conséquences directes que présentent pour lui les installations, ouvrages, travaux et activités autorisés, appréciés notamment en fonction de leurs conditions de fonctionnement, de la configuration des lieux ainsi que de l'état de la ressource en eau.<br/>
<br/>
              5. Il ressort des pièces du dossier soumis aux juges du fond que la société Socardel exploite deux microcentrales hydroélectriques en rive droite de la rivière Mayenne, au droit des microcentrales hydroélectriques implantées en rive gauche et exploitées par la Société hydraulique d'études et de missions d'assistance. Chacune des installations hydroélectriques se trouve à l'extrémité d'un barrage commun nécessaire à l'utilisation de l'énergie hydraulique, de telle façon que leur utilisation de la force du cours d'eau pour la production d'électricité d'origine renouvelable affecte nécessairement les conditions de leurs exploitations respectives. Il s'ensuit qu'en estimant que la société Socardel ne disposait pas d'un intérêt lui donnant qualité pour demander l'annulation de l'arrêté du 7 décembre 2010, qui prévoit au demeurant, au bénéfice de la Société hydraulique d'études et de missions d'assistance, une augmentation du débit autorisé de 4 m3/s, passant de 9 à 13 m3/s, la cour administrative d'appel de Nantes a commis une erreur de qualification juridique. Il en résulte que, sans qu'il soit besoin de se prononcer sur les autres moyens du pourvoi, la société Socardel est fondée à demander l'annulation de l'arrêt qu'elle attaque.<br/>
<br/>
              6. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat et de la Société hydraulique d'études et de missions d'assistance la somme de 1 500 euros chacun à verser à la société Socardel au titre de l'article L. 761-1 du code de justice administrative. Ces mêmes dispositions font obstacle à ce qu'une somme soit mise à ce titre à la charge de la société Socardel qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 13 juillet 2018 de la cour administrative d'appel de Nantes est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Nantes.<br/>
Article 3 : L'Etat et la Société hydraulique d'études et de missions d'assistance verseront chacun la somme de 1 500 euros à la société Socardel au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Les conclusions présentées par la Société hydraulique d'études et de missions d'assistance au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée à la société Socardel, à la Société hydraulique d'études et de missions d'assistance et à la ministre de la transition écologique.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
