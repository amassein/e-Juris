<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036739792</ID>
<ANCIEN_ID>JG_L_2018_03_000000412029</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/73/97/CETATEXT000036739792.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème chambre, 23/03/2018, 412029, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-03-23</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>412029</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BRIARD ; SCP SPINOSI, SUREAU</AVOCATS>
<RAPPORTEUR>Mme Louise Bréhier</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Xavier Domino</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:412029.20180323</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société Free Mobile a demandé au juge des référés du tribunal administratif de Marseille, sur le fondement de l'article L. 521-1 du code de justice administrative, de suspendre l'exécution de l'arrêté du 10 mars 2017 par lequel le maire de Marseille s'est opposé à la déclaration préalable qu'elle avait déposée en vue de la réalisation d'un relais de radiotéléphonie mobile sur un bâtiment situé 181, avenue de la Capelette, et d'enjoindre au maire de Marseille de réexaminer sa déclaration dans un délai d'un mois.<br/>
<br/>
              Par une ordonnance n° 1703894 du 16 juin 2017, le juge des référés du tribunal administratif de Marseille a rejeté sa demande. <br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire, un mémoire en réplique et un nouveau mémoire, enregistrés les 30 juin, 11 juillet, 1er décembre 2017 et 23 février 2018 au secrétariat du contentieux du Conseil d'Etat, la société Free Mobile demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) statuant en référé, de faire droit à sa demande ; <br/>
<br/>
              3°) de mettre à la charge de la commune de Marseille la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de l'urbanisme ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Louise Bréhier, auditrice, <br/>
<br/>
              - les conclusions de M. Xavier Domino, rapporteur public,<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Spinosi, Sureau, avocat de la société Free mobile et à la SCP Briard, avocat de la commune de Marseille ; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1.	Considérant qu'aux termes de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité  de la décision " ;<br/>
<br/>
              2.	Considérant que l'urgence justifie la suspension de l'exécution d'un acte administratif lorsque celle-ci porte atteinte, de manière suffisamment grave et immédiate, à un intérêt public, à la situation du requérant ou aux intérêts qu'il entend défendre ; qu'il appartient au juge des référés d'apprécier concrètement, compte tenu des justifications fournies par le requérant, si les effets de l'acte contesté sont de nature à caractériser une urgence justifiant que, sans attendre le jugement de la requête au fond, l'exécution de la décision soit suspendue ;<br/>
<br/>
              3.	Considérant que, pour rejeter la demande de suspension dont il était saisi, le juge des référés du tribunal administratif, après avoir relevé que la partie du territoire de la commune de Marseille sur laquelle le relais de téléphonie de la société Free mobile devait être implanté n'était pas couverte par les réseaux 3G et 4G de cette société, s'est fondé sur la circonstance que la société aurait déjà atteint ses objectifs de couverture et respecté ses engagements vis-à-vis de l'Etat en matière de couverture du territoire national, pour en déduire que la condition d'urgence n'était pas remplie ; <br/>
<br/>
              4.	Considérant qu'en se fondant ainsi sur la seule circonstance que la société aurait, au niveau national, atteint ses objectifs de couverture pour refuser de tenir la condition d'urgence comme remplie, tout en relevant que la partie du territoire de la commune de Marseille concernée par le projet d'antenne n'était pas couverte par les réseaux de la société Free Mobile, le juge des référés a commis une erreur de droit ;  que, par suite, et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, la société Free Mobile est fondée à demander l'annulation de l'ordonnance qu'elle attaque ;<br/>
<br/>
              5.	Considérant qu'il y a lieu, par application de l'article L. 821-2 du code de justice administrative, de régler l'affaire au titre de la procédure de référé engagée ;<br/>
<br/>
              6.	Considérant, en premier lieu, qu'eu égard à l'intérêt public qui s'attache à la couverture du territoire national par le réseau de téléphonie mobile tant 3G que 4G et aux intérêts propres de la Société Free Mobile, et en particulier à la circonstance que le territoire de la commune de Marseille, dans la zone concernée, n'est que partiellement couvert par les réseaux de téléphonie mobile de la société requérante, la condition d'urgence doit, dans les circonstances de l'espèce, être regardée comme remplie ; <br/>
<br/>
              7.	Considérant, en second lieu, qu'en l'état de l'instruction, le moyen tiré de ce qu'en prenant la décision litigieuse le maire de Marseille a fait une inexacte application des dispositions de l'article UB11 du règlement du plan local d'urbanisme est de nature à faire naître un doute sérieux quant à la légalité de la décision contestée ;<br/>
<br/>
              8.	Considérant que, pour l'application de l'article L. 600-4-1 du code de l'urbanisme, en l'état du dossier soumis au Conseil d'Etat, aucun autre moyen n'est susceptible d'entraîner la suspension de la décision attaquée ;<br/>
<br/>
              9.	Considérant qu'il résulte de ce qui précède que la Société Free Mobile est fondée à demander la suspension de l'exécution de l'arrêté contesté du 10 mars 2017 ;<br/>
<br/>
              10.	Considérant qu'il y a lieu d'enjoindre au maire de Marseille de procéder à une nouvelle instruction de la déclaration préalable de travaux de la Société Free Mobile dans un délai d'un mois à compter de la notification de la présente décision ;<br/>
<br/>
              11.	Considérant qu'il y a lieu de mettre à la charge de la commune de Marseille le versement à la société Free Mobile de la somme de 4 000 euros au titre des frais exposés devant le Conseil d'Etat et devant le tribunal administratif de Marseille et non compris dans les dépens ; qu'en revanche, les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mise à la charge de la société Free Mobile, qui n'est pas, dans la présente instance, la partie perdante, la somme demandée à ce même titre par la commune de Marseille ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : L'ordonnance du juge des référés du tribunal administratif de Marseille du 16 juin 2017 est annulée.<br/>
<br/>
Article 2 : L'exécution de l'arrêté du 10 mars 2017 par lequel le maire de la commune de Marseille s'est opposé à la déclaration préalable présentée par la société Free Mobile est suspendue.<br/>
<br/>
Article 3 : Il est enjoint au maire de Marseille de procéder à un nouvel examen de la déclaration préalable déposée par la société Free Mobile dans un délai d'un mois à compter de la notification de la présente décision.<br/>
<br/>
Article 4 : La commune de Marseille versera à la société Free Mobile la somme de 4 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 5 : Les conclusions de la commune de Marseille tendant à l'application de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
<br/>
Article 6 : La présente décision sera notifiée à la société Free Mobile et à la commune de Marseille. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
