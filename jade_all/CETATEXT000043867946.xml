<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043867946</ID>
<ANCIEN_ID>JG_L_2021_07_000000453249</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/86/79/CETATEXT000043867946.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème chambre, 12/07/2021, 453249, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-07-12</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>453249</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GADIOU, CHEVALLIER ; SCP BOULLOCHE</AVOCATS>
<RAPPORTEUR>Mme Thalia Breton</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Dieu</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:453249.20210712</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              L'association PASS LAS 21 BORDEAUX a demandé au juge des référés du tribunal administratif de Bordeaux d'ordonner, sur le fondement des dispositions de l'article L. 521-1 du code de justice administrative, la suspension de l'exécution de la délibération du 15 mars 2021 par laquelle le conseil d'administration de l'université de Bordeaux a fixé les capacités d'accueil en deuxième année du premier cycle des formations de santé pour l'année 2021-2022. Par une ordonnance n° 2102181 du 21 mai 2021, rectifiée par une ordonnance du même jour, le juge des référés du tribunal administratif de Bordeaux a rejeté sa demande.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 3, 8 et 23 juin 2021 au secrétariat du contentieux du Conseil d'État, l'association PASS LAS 21 BORDEAUX demande au Conseil d'État :<br/>
<br/>
              1°) d'annuler l'article 2 de cette ordonnance ;<br/>
<br/>
              2°) statuant en référé, de faire droit à sa demande ; <br/>
<br/>
              3°) d'enjoindre à l'université de Bordeaux de prendre, dans un délai de quinze jours, une nouvelle délibération fixant les capacités d'accueil et, subsidiairement, d'autoriser les étudiants inscrits en parcours accès santé spécifique (PASS) et en licence accès santé (LAS) reçus à leurs examens et non admis en filière santé à redoubler dans la même formation ;<br/>
<br/>
              4°) d'enjoindre aux ministres chargés de la santé et de l'enseignement supérieur et à l'université de Bordeaux d'ajouter les places réservées aux étudiants inscrits en première année commune aux études de santé (PACES) au lieu de les retrancher de l'ensemble des places offertes aux étudiants inscrits en PASS et en LAS ;<br/>
<br/>
              5°) de mettre à la charge de l'université de Bordeaux la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la Constitution ;<br/>
              - le code de l'éducation ;<br/>
              - la loi n° 2019-774 du 24 juillet 2019 relative à l'organisation et à la transformation du système de santé ;<br/>
              - le décret n° 2019-1125 du 4 novembre 2019 relatif à l'accès aux formations de médecine, de pharmacie, d'odontologie et de maïeutique ;<br/>
              - l'arrêté du 4 novembre 2019 relatif à l'accès aux formations de médecine, de pharmacie, d'odontologie et de maïeutique ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme B... A..., auditrice,  <br/>
<br/>
              - les conclusions de M. Frédéric Dieu, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Gadiou, Chevallier, avocat de l'association Pass Las 21 Bordeaux et à la SCP Boulloche, avocat de l'université de Bordeaux ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              Sur le cadre juridique :<br/>
<br/>
              En ce qui concerne l'économie générale de la réforme du premier cycle des études de santé :<br/>
<br/>
              1. L'article 1er de la loi du 24 juillet 2019 relative à l'organisation et à la transformation du système de santé a réformé l'accès aux formations de médecine, de pharmacie, d'odontologie et de maïeutique, en supprimant le numerus clausus déterminant le nombre d'étudiants en première année commune aux études de santé (PACES) pouvant poursuivre en deuxième année de ces formations. Comme le prévoit le décret du 4 novembre 2019 pris pour son application, l'accès en deuxième année est désormais ouvert, à compter de l'année universitaire 2020-2021, aux étudiants relevant principalement de trois types de parcours - les étudiants en parcours accès santé spécifique (PASS) ; les étudiants inscrits en licence accès santé (LAS) ; les étudiants titulaires d'un titre ou d'un diplôme d'Etat d'auxiliaire médical -, dans la mesure des capacités d'accueil de ces formations, déterminées annuellement par les universités, en considération de leurs capacités de formation et des besoins de santé. Ces étudiants doivent, en vertu de l'article L. 631-1 du code de l'éducation, avoir validé leur parcours de formation antérieur dans l'enseignement supérieur et réussi des épreuves, qui sont déterminées par un décret en Conseil d'Etat. Le décret du 4 novembre 2019 prévoit que les étudiants peuvent, sous certaines conditions, présenter deux fois leur candidature à cette admission en deuxième année de ces formations, étant toutefois relevé qu'ainsi qu'il résulte des dispositions combinées de ce décret et de l'arrêté du 4 novembre 2019 de la ministre des armées, de la ministre des solidarités et de la santé et de la ministre de l'enseignement supérieur, de la recherche et de l'innovation, ils ne peuvent être inscrits qu'une fois en PASS et que la seule inscription en PASS vaut utilisation d'une des deux possibilités de candidature.<br/>
<br/>
              En ce qui concerne la détermination des capacités d'accueil en deuxième année des études de santé :<br/>
<br/>
              2. Aux termes du deuxième alinéa du I de l'article L. 631-1 du code de l'éducation : " Les capacités d'accueil des formations en deuxième et troisième années de premier cycle sont déterminées annuellement par les universités. Pour déterminer ces capacités d'accueil, chaque université prend en compte les objectifs pluriannuels d'admission en première année du deuxième cycle de ces formations. Ces objectifs pluriannuels, qui tiennent compte des capacités de formation et des besoins de santé du territoire, sont arrêtés par l'université sur avis conforme de l'agence régionale de santé ou des agences régionales de santé concernées. L'agence régionale de santé ou les agences régionales de santé consultent, au préalable, la conférence régionale de la santé et de l'autonomie ou les conférences régionales de la santé et de l'autonomie concernées. Les objectifs pluriannuels d'admission en première année du deuxième cycle sont définis au regard d'objectifs nationaux pluriannuels relatifs au nombre de professionnels à former établis par l'Etat pour répondre aux besoins du système de santé, réduire les inégalités territoriales d'accès aux soins et permettre l'insertion professionnelle des étudiants ". Aux termes des dispositions du III de l'article R. 631-1-1 du code de l'éducation : " Pour chaque formation de médecine, de pharmacie, d'odontologie et de maïeutique, le nombre de places en deuxième ou troisième année du premier cycle est réparti entre les parcours de formation mentionnés à l'article R. 631-1 de façon à garantir la diversification des voies d'accès. Cette répartition est effectuée en précisant le nombre de places proposées pour chaque parcours, ou pour des groupes de parcours. Un arrêté des ministres en charge de la santé et de l'enseignement supérieur fixe le nombre de places proposées pour chaque formation de médecine, de pharmacie, d'odontologie et de maïeutique, pour un parcours ou un groupe de parcours qui ne peut excéder 50 % du nombre total de places proposées. / Le nombre de places ainsi réparti est porté à la connaissance des candidats dans le cadre de la procédure nationale de préinscription prévue à l'article L. 612-3 ". Le IV de l'article 6 du décret du 4 novembre 2019 prévoit que pendant une durée de deux ans à compter de la rentrée universitaire 2020, un arrêté des ministres chargés de la santé et de l'enseignement supérieur peut autoriser certaines universités à déroger au pourcentage précité, dans la limite de 70% du nombre total de places proposées. Enfin, l'article 18 de l'arrêté du 4 novembre 2019 prévoit en son III que le nombre de places en deuxième année, pour l'année universitaire 2021-2022, est arrêté par chaque université au plus tard le 31 mars 2020 et porté à la connaissance des étudiants sur le site internet des universités.<br/>
<br/>
              En ce qui concerne les dispositions transitoires relatives aux étudiants ayant déjà suivi une année d'études de santé avant l'intervention de la réforme :<br/>
<br/>
              3. Les dispositions du VII de l'article 1er de la loi du 24 juillet 2019 prévoient que les étudiants ayant suivi une première année d'études de santé avant l'intervention de la réforme, notamment ceux ayant suivi une PACES, et qui auraient eu, en application des dispositions antérieures, la possibilité de présenter une nouvelle candidature à l'admission en deuxième année des études médicales, pharmaceutiques, odontologiques ou maïeutiques, " conservent cette possibilité selon des modalités fixées par décret en Conseil d'Etat ". A ce titre, les dispositions du III de l'article 6 du décret du 4 novembre 2019 disposent que ces étudiants " peuvent s'inscrire une nouvelle et dernière fois en première année commune aux études de santé (...) que les universités qui la proposaient sont tenues de maintenir au cours de la première année universitaire pendant laquelle elles mettent en oeuvre les dispositions du présent décret " et que " pour chaque université concernée par les dispositions transitoires, les ministres chargés de l'enseignement supérieur et de la santé fixent par arrêté le nombre de places attribuées au titre de cette première année commune aux études de santé ". Enfin, les dispositions du II de l'article 18 de l'arrêté du 4 novembre 2019 prévoient que les effectifs d'étudiants admis à la rentrée universitaire 2021 en deuxième année des formations de médecine, pharmacie, odontologie et maïeutique sont, pour les universités ayant maintenu une PACES au cours de l'année universitaire 2020-2021, " constitués (...) du nombre spécifique d'étudiants ayant suivi cette première année commune et autorisés à poursuivre en deuxième année (...) leurs études en médecine, pharmacie, odontologie et maïeutique, ainsi que des capacités d'accueil en deuxième (...) année déterminées conformément aux dispositions du présent arrêté ". <br/>
<br/>
              Sur le pourvoi :<br/>
<br/>
              4. Il ressort des pièces du dossier soumis au juge des référés que, par une délibération de son conseil d'administration du 15 mars 2021, l'université de Bordeaux a arrêté les capacités d'accueil en deuxième année du premier cycle des formations de médecine, de pharmacie, d'odontologie et de maïeutique pour l'année université 2021-2022. Cette délibération fixe à 189 le nombre de places en filière de médecine allouées aux étudiants inscrits en PASS ou en LAS, à 6 le nombre de places en filière de maïeutique, à 20 le nombre de places en filière d'odontologie et à 43 le nombre de places en filière de pharmacie. L'association PASS LAS 21 BORDEAUX a demandé au juge des référés du tribunal administratif de Bordeaux de suspendre, sur le fondement des dispositions de l'article L. 521-1 du code de justice administrative, l'exécution de cette délibération. Par une ordonnance du 21 mai 2021, rectifiée par une ordonnance du même jour, contre laquelle l'association PASS LAS 21 BORDEAUX se pourvoit en cassation en tant qu'elle lui fait grief, le juge des référés a rejeté sa demande. <br/>
<br/>
              5. Il ressort également des pièces du dossier soumis au juge des référés que, s'agissant de l'année de transition entre les anciennes études de santé et les nouvelles que constitue l'année universitaire 2020-2021, les pouvoirs publics entendaient garantir non seulement aux étudiants en PACES des chances de réussite équivalentes à celles constatées les années antérieures mais aussi, aux étudiants qui s'engageaient dans les nouvelles études de santé, que la préservation des chances de réussite des étudiants en PACES ne se ferait pas à leur détriment. A cet égard, l'étude d'impact de la loi du 24 juillet 2019 faisait état de ce que la mise en oeuvre de l'article 1er de cette loi devrait se traduire, durant l'année de transition, par une augmentation temporaire, de l'ordre de 20%, du nombre de places offertes en deuxième année des études de santé " afin de garantir l'égalité des chances " entre les deux cohortes d'étudiants, et l'exposé des motifs de cette même loi, indiquait que, pour cette année de transition, une part de l'augmentation du nombre d'étudiants admis en deuxième année serait allouée à l'admission des étudiants en PACES afin de ne pas créer d'inégalités au détriment des nouveaux étudiants en PASS et en LAS. De même, il avait été prévu, comme il a été dit au point 2, de façon à permettre aux étudiants s'inscrivant en LAS ou en PASS de connaître les possibilités effectives d'accès en deuxième année des études de santé lors de l'année de transition, que les places ouvertes en deuxième année des études de santé en 2021-2022 seraient rendues publiques dès le mois de mars 2020. <br/>
<br/>
              6. Il ressort aussi des pièces du dossier soumis au juge des référés que le nombre de places offertes en deuxième année pour les étudiants en PACES, PASS et LAS à l'université de Bordeaux pour l'année universitaire 2021-2022 s'élève à 652, dont 394 places pour les étudiants en PACES, ce nombre ayant été fixé par l'arrêté du 25 mai 2021 du ministre de la santé et des solidarités et de la ministre de l'enseignement supérieur, de la recherche et de l'innovation, dont la décision du Conseil d'Etat, statuant au contentieux, du 8 juillet 2021 n°452731, a jugé qu'il devait partiellement être annulé mais que cette annulation ne prendrait effet que le 30 septembre 2021, et 258 places pour les étudiants en PASS et LAS, ce nombre ayant été fixé par la délibération contestée. Il en résulte que le nombre de places en deuxième année n'a été accru que de 16 % par rapport aux 562 places résultant du numerus clausus en vigueur l'année précédente. Or, si l'université de Bordeaux avait arrêté des capacités d'accueil permettant une augmentation d'au moins 20 % des places offerts en deuxième année, elle aurait créé au moins 22 places supplémentaires au bénéfice des étudiants en PASS et LAS. Il s'ensuit qu'en jugeant que le moyen tiré de ce que la délibération du 15 mars est entachée d'erreur manifeste d'appréciation, en tant qu'elle ne fixe pas un nombre suffisant de places allouées aux étudiants inscrits en PASS et en LAS, n'était pas de nature à faire naître un doute sérieux quant à la légalité de la décision contestée, le juge des référés a entaché son ordonnance de dénaturation des pièces du dossier. <br/>
<br/>
              7. Par suite, et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, l'association requérante est fondée à demander l'annulation de l'article 2 de l'ordonnance qu'elle attaque.<br/>
<br/>
              8. Il y a lieu, dans les circonstances de l'espèce, de régler, dans la mesure de la cassation prononcée, l'affaire au titre de la procédure de référé engagée par l'association PASS LAS 21 BORDEAUX, en application des dispositions de l'article L. 821-2 du code de justice administrative.<br/>
<br/>
              Sur la demande de suspension de la délibération du 15 mars 2021 de l'université de Bordeaux en tant qu'elle n'alloue qu'un nombre insuffisant de places aux étudiants en LAS et en PASS ;<br/>
<br/>
              9. En premier lieu, si l'université de Bordeaux soutient que la demande de suspension  est irrecevable, faute pour l'association requérante de justifier d'un intérêt à agir contre la décision attaquée, son objet statutaire, qui est de représenter les intérêts des étudiants inscrits en PASS et en LAS à l'université de Bordeaux et de défendre et promouvoir l'égalité des chances à l'accès aux études de santé et notamment l'accès en deuxième année, est suffisamment précis et en rapport avec les conclusions de leur demande. Par suite, l'université de Bordeaux n'est pas fondée à soutenir que l'association requérante ne serait pas recevable à demander la suspension de l'exécution de la délibération contestée.<br/>
<br/>
              10. En deuxième lieu, la condition d'urgence à laquelle est subordonné le prononcé d'une mesure de suspension doit être regardée comme remplie lorsque la décision administrative contestée préjudicie de manière suffisamment grave et immédiate à un intérêt public, à la situation du requérant ou aux intérêts qu'il entend défendre. Il appartient au juge des référés d'apprécier concrètement, compte tenu des justifications fournies par le requérant, si les effets de l'acte litigieux sont de nature à caractériser une urgence justifiant que, sans attendre le jugement de la requête au fond, l'exécution de la décision soit suspendue. L'urgence doit être appréciée objectivement et compte tenu de l'ensemble des circonstances de l'espèce.<br/>
<br/>
              11. Il résulte de l'instruction qu'eu égard à la circonstance que le nombre de places en deuxième année auxquelles peuvent prétendre les étudiants inscrits en PASS et en LAS à l'université de Bordeaux au cours de l'année universitaire 2020-2021, dépend directement des capacités d'accueil objet du présent litige, la délibération contestée est de nature à préjudicier de manière suffisamment grave aux intérêts de ces étudiants, que l'association PASS LAS 21 BORDEAUX s'est donnée pour objet de défendre, sans que les difficultés susceptibles d'être causées par une éventuelle mesure de suspension de cette délibération alléguées par l'université ne soient établies. Dans ces conditions, la condition d'urgence, prévue à l'article L. 521-1 du code de justice administrative, doit être regardée comme remplie.<br/>
<br/>
              12. En troisième lieu, compte tenu de ce qui a été dit aux points 5 et 6, le moyen tiré de ce que la délibération du 15 mars 2021, en tant qu'elle ne fixe pas un nombre suffisant de places en deuxième année allouées aux étudiants de PASS et de LAS, est entachée d'erreur manifeste d'appréciation, est de nature, en l'état de l'instruction, à faire naître un doute sérieux quant à la légalité de la délibération contestée.<br/>
<br/>
              13. Les conditions fixées par l'article L. 521-1 du code de justice administrative étant réunies, sans qu'il soit besoin de se prononcer sur les autres moyens de la demande, l'association PASS LAS 21 BORDEAUX est fondée à demander la suspension de l'exécution de la délibération du 15 mars 2021 en tant qu'elle ne fixe pas un nombre suffisant de places en deuxième année allouées aux étudiants de PASS et de LAS.<br/>
<br/>
              14. Il y a lieu, dans l'attente du jugement au fond, d'enjoindre l'université de Bordeaux d'adopter, dans un délai de quinze jours à compter de la présente décision, une nouvelle délibération créant en deuxième année des études de santé de nouvelles places allouées aux étudiants de PASS et de LAS de façon à permettre une augmentation du nombre total de places en deuxième année des études de santé d'au moins 20% par rapport au nombre de places ouvertes à la rentrée universitaire 2020. Il n'y a pas lieu, en revanche, de faire droit aux autres conclusions à fins d'injonction. <br/>
<br/>
              15. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'université de Bordeaux la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. Ces mêmes dispositions font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'association PASS LAS 21 BORDEAUX qui n'est pas, dans la présente instance, la partie perdante.<br/>
              [0]<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'article 2 de l'ordonnance du 21 mai 2021 du juge des référés du tribunal administratif de Bordeaux est annulé.<br/>
Article 2 : L'exécution de la délibération du 15 mars 2021 de l'université de Bordeaux est suspendue en tant qu'elle ne fixe pas un nombre suffisant de places en deuxième année allouées aux étudiants de PASS et de LAS.<br/>
Article 3 : Il est enjoint à l'université de Bordeaux, dans l'attente du jugement au fond, d'adopter, dans un délai de quinze jours à compter de la notification de la présente ordonnance, une délibération conforme aux motifs de la présente décision.<br/>
Article 4 : L'université de Bordeaux versera à l'association PASS LAS 21 BORDEAUX la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 5 : Les conclusions de l'université de Bordeaux présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 6 : La présente décision sera notifiée à l'association PASS LAS 21 BORDEAUX et à l'université de Bordeaux.<br/>
Copie en sera adressée à la ministre de l'enseignement supérieur, de la recherche et de l'innovation.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
