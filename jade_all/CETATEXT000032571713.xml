<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032571713</ID>
<ANCIEN_ID>JG_L_2016_05_000000384404</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/57/17/CETATEXT000032571713.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère - 6ème chambres réunies, 20/05/2016, 384404</TITRE>
<DATE_DEC>2016-05-20</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>384404</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère - 6ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MATUCHANSKY, VEXLIARD, POUPOT ; SCP GATINEAU, FATTACCINI</AVOCATS>
<RAPPORTEUR>M. Yannick Faure</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Jean Lessi</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2016:384404.20160520</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              M. A...B...a demandé au tribunal administratif de Montreuil de condamner la caisse d'assurance vieillesse, invalidité et maladie des cultes (CAVIMAC) à lui verser la somme de 8 691,60 euros en réparation du préjudice qu'il estime avoir subi du fait de l'illégalité entachant l'article 1.23 du règlement intérieur de cette caisse. Par un jugement n° 1305415 du 10 juillet 2014, le tribunal administratif de Montreuil a rejeté sa demande.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 10 septembre 2014, 10 décembre 2014 et 2 juin 2015 au secrétariat du contentieux du Conseil d'Etat, M. B...demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler le jugement du tribunal administratif de Montreuil du 10 juillet 2014 ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande ;<br/>
<br/>
              3°) de mettre à la charge de la caisse d'assurance vieillesse, invalidité et maladie des cultes la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de la sécurité sociale ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Yannick Faure, auditeur,  <br/>
<br/>
              - les conclusions de M. Jean Lessi, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Gatineau, Fattaccini, avocat de M.B..., et à la SCP Matuchansky, Vexliard, Poupot, avocat de la caisse d'assurance vieillesse, invalidité et maladie des cultes ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que M. B...a demandé au tribunal administratif de Montreuil de condamner la caisse d'assurance vieillesse, invalidité et maladie des cultes (CAVIMAC) à réparer le préjudice qu'il estime avoir subi, tenant à l'absence de prise en compte, lors de la liquidation en 2007 de sa pension de retraite, de la période comprise entre son admission au grand séminaire de Toulouse, au mois d'octobre 1961, et sa tonsure, le 27 juin 1966, du fait de l'illégalité entachant l'article 1.23 du règlement intérieur des prestations de la caisse mutuelle d'assurance vieillesse des cultes, à laquelle s'est substituée la CAVIMAC ; que, par un jugement du 10 juillet 2014, contre lequel M. B... se pourvoit en cassation, le tribunal administratif de Montreuil a rejeté sa demande ;<br/>
<br/>
              Sur la compétence des juridictions de l'ordre administratif :<br/>
<br/>
              2. Considérant qu'aux termes de l'article L. 142-1 du code de la sécurité sociale, rendu applicable aux différends relatifs à l'assurance vieillesse des ministres des cultes et membres des congrégations et collectivités religieuses par l'article L. 382-20 du même code : " Il est institué une organisation du contentieux général de la sécurité sociale. / Cette organisation règle les différends auxquels donnent lieu l'application des législations et réglementations de sécurité sociale et de mutualité sociale agricole, et qui ne relèvent pas, par leur nature, d'un autre contentieux (...) " ; <br/>
<br/>
              3. Considérant, d'une part, que l'action en responsabilité engagée par un assuré contre l'organisme gestionnaire d'un régime de sécurité sociale auquel il est affilié, au motif que les droits qu'il tient de ce régime auraient été méconnus, ne relève pas, par nature et quel qu'en soit le fondement, d'un contentieux autre que celui de la sécurité sociale ;<br/>
<br/>
              4. Considérant, d'autre part, que l'action en responsabilité portant, non sur les droits que cet assuré tient de ce régime, mais sur la faute commise par une personne publique ou une personne privée chargée d'une mission de service public dans l'exercice de son pouvoir réglementaire, relève par nature de la juridiction administrative ;<br/>
<br/>
              5. Considérant que le litige soulevé par M. B...tend à la réparation, par la caisse d'assurance vieillesse, invalidité et maladie des cultes, du préjudice qu'il estime avoir subi du fait de l'illégalité entachant l'article 1.23 du règlement intérieur des prestations de la caisse mutuelle d'assurance vieillesse des cultes ; qu'il tend ainsi à la condamnation de la CAVIMAC du fait d'une faute commise dans l'exercice de son pouvoir réglementaire par la caisse à laquelle elle s'est substituée ; que, par suite, le litige relève, par nature, de la compétence des juridictions de l'ordre administratif ; <br/>
<br/>
              Sur le jugement attaqué, en tant qu'il statue sur le préjudice matériel :<br/>
<br/>
              6. Considérant que si M. B...demandait au tribunal administratif de Montreuil la condamnation de la caisse d'assurance vieillesse, invalidité et maladie des cultes à lui verser une indemnité compensant la minoration de la pension servie par ce régime en raison de l'absence de prise en compte de la période comprise entre octobre 1961 et juin 1966, du fait de l'article 1.23 du règlement intérieur des prestations, ces conclusions avaient, en réalité, le même objet que des conclusions tendant à la contestation du montant de la pension servie par ce régime ; que M. B...a d'ailleurs saisi de ce litige le tribunal des affaires de sécurité sociale d'Ille-et-Vilaine puis la cour d'appel de Rennes, laquelle a rejeté sa demande par un arrêt du 9 février 2011 ; que l'existence de la voie de recours dont disposait M. B...devant les juridictions du contentieux général de la sécurité sociale, en application des dispositions précitées de l'article L. 142-1 du code de la sécurité sociale, en vue du règlement d'un tel litige, s'opposait à ce qu'il engage devant le tribunal administratif une action mettant en cause la responsabilité de la caisse d'assurance vieillesse, invalidité et maladie des cultes en raison de l'illégalité du règlement intérieur des prestations dont elle lui a fait application ; que ce motif, qui est d'ordre public et n'appelle l'appréciation d'aucune circonstance de fait, doit être substitué aux motifs retenus par le jugement attaqué, dont il justifie le dispositif sur ce point ;<br/>
<br/>
              Sur le jugement attaqué, en tant qu'il statue sur le préjudice moral :<br/>
<br/>
              7. Considérant que le tribunal administratif de Monteuil a jugé que le préjudice moral allégué par M. B...ne résultait pas directement de l'illégalité des dispositions de l'article 1.23 du règlement intérieur des prestations, entachées d'incompétence, mais découlait de la décision par laquelle la caisse d'assurance vieillesse, invalidité et maladie des cultes, le 22 février 2007, avait liquidé sa pension de retraite ; qu'en écartant ainsi l'existence d'un lien direct de causalité entre la faute commise par la caisse mutuelle d'assurance vieillesse des cultes en adoptant des dispositions qu'elle n'avait pas compétence pour prendre et le préjudice allégué par M.B..., tenant au niveau de sa pension de retraite, le tribunal administratif de Montreuil, dont le jugement est suffisamment motivé, n'a pas commis d'erreur de droit et a exactement qualifié les faits de l'espèce ; <br/>
<br/>
              8. Considérant qu'il résulte de ce qui précède que M. B...n'est pas fondé à demander l'annulation du jugement qu'il attaque ;<br/>
<br/>
              Sur les conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              9. Considérant que les dispositions de l'article L. 761-1 font obstacle à ce qu'une somme soit mise à ce titre à la charge de la caisse d'assurance vieillesse, invalidité et maladie des cultes, qui n'est pas, dans la présente instance, la partie perdante ; qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par cette même caisse au titre des mêmes dispositions ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de M. B...est rejeté.<br/>
Article 2 : Les conclusions de la caisse d'assurance vieillesse, invalidité et maladie des cultes présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La présente décision sera notifiée à M. A...B...et à la caisse d'assurance vieillesse, invalidité et maladie des cultes.<br/>
Copie en sera adressée à la ministre des affaires sociales et de la santé.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">17-03-01-02-04 COMPÉTENCE. RÉPARTITION DES COMPÉTENCES ENTRE LES DEUX ORDRES DE JURIDICTION. COMPÉTENCE DÉTERMINÉE PAR DES TEXTES SPÉCIAUX. ATTRIBUTIONS LÉGALES DE COMPÉTENCE AU PROFIT DES JURIDICTIONS JUDICIAIRES. COMPÉTENCE DES JURIDICTIONS JUDICIAIRES EN MATIÈRE DE PRESTATIONS DE SÉCURITÉ SOCIALE. - LITIGES EN MATIÈRE DE SÉCURITÉ SOCIALE (ART. L. 142-1 S. DU CSS) - CHAMP D'APPLICATION - ACTION EN RESPONSABILITÉ POUR MÉCONNAISSANCE DU RÉGIME DE SÉCURITÉ SOCIALE APPLICABLE - INCLUSION - ACTION EN RESPONSABILITÉ FONDÉE SUR UNE FAUTE DANS L'EXERCICE DU POUVOIR RÉGLEMENTAIRE EN MATIÈRE DE SÉCURITÉ SOCIALE - EXCLUSION [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">17-03-02-05-01-01 COMPÉTENCE. RÉPARTITION DES COMPÉTENCES ENTRE LES DEUX ORDRES DE JURIDICTION. COMPÉTENCE DÉTERMINÉE PAR UN CRITÈRE JURISPRUDENTIEL. RESPONSABILITÉ. RESPONSABILITÉ EXTRA-CONTRACTUELLE. COMPÉTENCE ADMINISTRATIVE. - ACTION EN RESPONSABILITÉ FONDÉE SUR UNE FAUTE DANS L'EXERCICE DU POUVOIR RÉGLEMENTAIRE EN MATIÈRE DE SÉCURITÉ SOCIALE - COMPÉTENCE DE LA JURIDICTION ADMINISTRATIVE [RJ1].
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">54-01-03 PROCÉDURE. INTRODUCTION DE L'INSTANCE. EXCEPTION DE RECOURS PARALLÈLE. - ACTION INDEMNITAIRE FONDÉE SUR UNE CONTESTATION DU MONTANT D'UNE PENSION DE RETRAITE ET ACTION INDEMNITAIRE METTANT EN CAUSE L'IRRÉGULARITÉ DE LA DISPOSITION DU RÈGLEMENT ADMINISTRATIF À L'ORIGINE DE LA MINORATION DE PENSION [RJ2].
</SCT>
<SCT ID="8D" TYPE="PRINCIPAL">62-05-01 SÉCURITÉ SOCIALE. CONTENTIEUX ET RÈGLES DE PROCÉDURE CONTENTIEUSE SPÉCIALES. RÈGLES DE COMPÉTENCE. - COMPÉTENCE DES JURIDICTIONS JUDICIAIRES EN MATIÈRE DE SÉCURITÉ SOCIALE (ART. L. 142-1 S. DU CSS) - CHAMP D'APPLICATION - ACTION EN RESPONSABILITÉ POUR MÉCONNAISSANCE DU RÉGIME DE SÉCURITÉ SOCIALE APPLICABLE - INCLUSION - ACTION EN RESPONSABILITÉ FONDÉE SUR UNE FAUTE DANS L'EXERCICE DU POUVOIR RÉGLEMENTAIRE EN MATIÈRE DE SÉCURITÉ SOCIALE - EXCLUSION [RJ1].
</SCT>
<ANA ID="9A"> 17-03-01-02-04 Si l'action en responsabilité engagée par un assuré contre l'organisme gestionnaire d'un régime de sécurité sociale auquel il est affilié, au motif que les droits qu'il tient de ce régime auraient été méconnus, ne relève pas, par nature et quel qu'en soit le fondement, d'un contentieux autre que celui de la sécurité sociale, et relève donc des juridictions judiciaires en application des articles L. 142-1 et suivants du code de la sécurité sociale (CSS), l'action en responsabilité portant, non sur les droits que cet assuré tient de ce régime, mais sur la faute commise par une personne publique ou une personne privée chargée d'une mission de service public dans l'exercice de son pouvoir réglementaire, relève par nature de la juridiction administrative.</ANA>
<ANA ID="9B"> 17-03-02-05-01-01 Si l'action en responsabilité engagée par un assuré contre l'organisme gestionnaire d'un régime de sécurité sociale auquel il est affilié, au motif que les droits qu'il tient de ce régime auraient été méconnus, ne relève pas, par nature et quel qu'en soit le fondement, d'un contentieux autre que celui de la sécurité sociale, et relève donc des juridictions judiciaires en application des articles L. 142-1 et suivants du code de la sécurité sociale (CSS), l'action en responsabilité portant, non sur les droits que cet assuré tient de ce régime, mais sur la faute commise par une personne publique ou une personne privée chargée d'une mission de service public dans l'exercice de son pouvoir réglementaire, relève par nature de la juridiction administrative.</ANA>
<ANA ID="9C"> 54-01-03 Si le requérant demande la condamnation d'une caisse de retraite à lui verser une indemnité compensant la minoration de sa pension en raison de l'absence de prise en compte d'une certaine période du fait de l'illégalité d'un article du règlement intérieur des prestations, ces conclusions ont, en réalité, le même objet que des conclusions tendant à la contestation du montant de la pension servie par ce régime. L'existence de la voie de recours dont dispose le requérant en vue du règlement d'un tel litige devant les juridictions du contentieux général de la sécurité sociale, qu'il a d'ailleurs saisies antérieurement, s'oppose à ce qu'il engage devant le tribunal administratif une action mettant en cause la responsabilité de la caisse de retraite en raison de l'illégalité du règlement intérieur des prestations dont elle lui a fait application.</ANA>
<ANA ID="9D"> 62-05-01 Si l'action en responsabilité engagée par un assuré contre l'organisme gestionnaire d'un régime de sécurité sociale auquel il est affilié, au motif que les droits qu'il tient de ce régime auraient été méconnus, ne relève pas, par nature et quel qu'en soit le fondement, d'un contentieux autre que celui de la sécurité sociale, et relève donc des juridictions judiciaires en application des articles L. 142-1 et suivants du code de la sécurité sociale (CSS), l'action en responsabilité portant, non sur les droits que cet assuré tient de ce régime, mais sur la faute commise par une personne publique ou une personne privée chargée d'une mission de service public dans l'exercice de son pouvoir réglementaire, relève par nature de la juridiction administrative.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr. TC, 31 mars 2008, Société Boiron c/ Direction générale des douanes et droits indirects, n° 3631, p. 553 ; CE, 15 février 2016, Département de la Guadeloupe, n° 378625, à mentionner aux Tables., ,[RJ2] Rappr., s'agissant d'une action en décharge fiscale et d'un recours indemnitaire fondé sur la mauvaise transposition d'une directive fiscale, CE, Assemblée, 30 octobre 1996, Ministre du budget c/ S.A. Jacques Dangeville, n° 141043, p. 399 ; s'agissant d'une action en révision de pension et de l'action indemnitaire fondée sur la méconnaissance du droit communautaire par le droit interne des pensions, CE, 19 mai 2004, Santalucia, n° 253425, T. pp. 622-789. Comp., s'agissant d'une action en décharge fiscale et d'une action indemnitaire tendant à la réparation d'un préjudice commercial et financier distinct causé par des fautes des services fiscaux, CE, 5 juillet 1996, SCI Saint-Michel, n° 150398, p. 269.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
