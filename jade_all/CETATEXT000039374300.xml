<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039374300</ID>
<ANCIEN_ID>JG_L_2019_11_000000421299</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/37/43/CETATEXT000039374300.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème - 5ème chambres réunies, 13/11/2019, 421299</TITRE>
<DATE_DEC>2019-11-13</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>421299</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème - 5ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GASCHIGNARD</AVOCATS>
<RAPPORTEUR>Mme Coralie Albumazard</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Louis Dutheillet de Lamothe</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:XX:2019:421299.20191113</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu les procédures suivantes :<br/>
<br/>
              Par un arrêt n° S2018-0986 du 10 avril 2018, la Cour des comptes a, notamment, déclaré Mme B... D... et M. C... A..., agents comptables de l'Office national de l'eau et des milieux aquatiques (ONEMA), débiteurs envers cet établissement, respectivement, d'une somme de 148 146,22 euros augmentée des intérêts de droit au titre de l'exercice 2015, et d'une somme de 560 076,20 euros augmentée des intérêts de droit au titre des exercices 2014 et 2015.  <br/>
<br/>
              1° Sous le n° 421299, par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 7 juin et 10 septembre 2018 au secrétariat du contentieux du Conseil d'Etat, Mme B... D... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt en tant qu'il a retenu sa responsabilité au titre des charges n° 2 et n° 3 ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              2° Sous le n° 421306, par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 7 juin et 10 septembre 2018 au secrétariat du contentieux du Conseil d'Etat, M. C... A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt en tant qu'il a retenu sa responsabilité au titre des charges n° 2 et n° 3 ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu : <br/>
              - le code des juridictions financières ;<br/>
              - la loi n° 63-156 du 23 février 1963 ;<br/>
              - le décret n° 48-1108 du 10 juillet 1948 ;<br/>
              - le décret n° 2000-239 du 13 mars 2000 ;<br/>
              - le décret n° 2001-1273 du 21 décembre 2001 ;<br/>
              - le décret n° 2012-1246 du 7 novembre 2012 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Coralie Albumazard, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Louis Dutheillet de Lamothe, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Gaschignard, avocat de Mme D... et de M. A... ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 23 octobre 2019, présentée par Mme D... ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Par un arrêt du 10 avril 2018, la Cour des comptes a constitué M. A... et Mme D..., qui se sont succédé en qualité de comptable public de l'Office national de l'eau et des milieux aquatiques (ONEMA), débiteurs envers cet établissement public de l'Etat, au titre de la charge n° 2 et de la charge n° 3, respectivement, d'une somme totale de 560 076,20 euros augmentée des intérêts de droit au titre des exercices 2014 et 2015 et d'une somme totale de 148 146,22 euros augmentée des intérêts de droit au titre de l'exercice 2015. Par deux pourvois qu'il y a lieu de joindre pour statuer par une même décision, Mme D... et M. A... se pourvoient en cassation contre cet arrêt en tant qu'il les constitue débiteurs de ces sommes envers l'ONEMA.<br/>
<br/>
              2. En vertu de l'article 60 de la loi de finances du 23 février 1963, la responsabilité personnelle et pécuniaire des comptables publics se trouve engagée dès lors notamment qu'une dépense a été irrégulièrement payée. Selon le VI de cet article 60, le comptable public dont la responsabilité pécuniaire est ainsi engagée ou mise en jeu a l'obligation de verser immédiatement de ses deniers personnels une somme égale à la dépense payée à tort. S'il n'a pas versé cette somme, il peut être, selon le VII de cet article 60, constitué en débet par le juge des comptes. Les contrôles mis à la charge des comptables publics en matière de dépenses sont fixés par les articles 19 et 20 du décret du 7 novembre 2012 relatif à la gestion budgétaire et comptable publique. Le 4° de l'article 20 de ce décret, dans sa version applicable, prévoit que le comptable public, au titre de son contrôle sur la validité de la dette, contrôle la production des pièces justificatives. L'article 50 du même décret dispose que " les opérations de recettes, de dépenses et de trésorerie doivent être justifiées par des pièces prévues dans des nomenclatures établies, pour chaque catégorie de personnes morales mentionnées à l'article 1er, par arrêté du ministre chargé du budget. (...) / Lorsqu'une opération de dépense n'a pas été prévue par une nomenclature mentionnée ci-dessus, doivent être produites des pièces justificatives permettant au comptable d'opérer les contrôles mentionnés aux articles 19 et 20. " La nomenclature des pièces justificatives des dépenses des établissements publics de l'Etat n'a été arrêtée que le 31 janvier 2018, soit postérieurement aux faits litigieux. <br/>
<br/>
              3. Il résulte des dispositions citées au point précédent que, pour apprécier la validité des créances, les comptables doivent notamment exercer leur contrôle sur la production des justifications. A ce titre, il leur revient d'apprécier si les pièces fournies présentent un caractère suffisant pour justifier la dépense engagée. Pour établir ce caractère suffisant, il leur appartient de vérifier, en premier lieu, si l'ensemble des pièces requises au titre de la nomenclature comptable applicable lorsqu'elle existe, ou à défaut, l'ensemble des pièces leur permettant d'opérer les contrôles qui leur incombent, leur ont été fournies et, en deuxième lieu, si ces pièces sont, d'une part, complètes et précises, d'autre part, cohérentes au regard de la catégorie de la dépense définie dans la nomenclature applicable et de la nature et de l'objet de la dépense telle qu'elle a été ordonnancée. Si ce contrôle peut conduire les comptables à porter une appréciation juridique sur les actes administratifs à l'origine de la créance et s'il leur appartient alors d'en donner une interprétation conforme à la réglementation en vigueur, ils n'ont pas le pouvoir de se faire juges de leur légalité. Enfin, lorsque les pièces justificatives fournies sont insuffisantes pour établir la validité de la créance, il appartient aux comptables de suspendre le paiement jusqu'à ce que l'ordonnateur leur ait produit les justifications nécessaires.<br/>
<br/>
              4. En premier lieu, il résulte des dispositions de l'article 4 du décret du 10 juillet 1948 portant classement hiérarchique des grades et emplois des personnels de l'Etat relevant du régime général des retraites, dans sa version applicable, que ces personnels ne peuvent bénéficier d'aucune indemnité autre que celles qui sont attribuées par décret. L'article 6 du décret du 21 décembre 2001 relatif aux primes et indemnités allouées aux fonctionnaires des corps d'agents techniques et de techniciens de l'environnement prévoit que " lorsqu'ils sont affectés dans les brigades mobiles d'intervention, les agents techniques et les techniciens de l'environnement commissionnés et assermentés perçoivent une indemnité de mobilité à titre de compensation des sujétions imposées par des déplacements fréquents ". Il ressort des énonciations de l'arrêt attaqué que, par deux décisions des 18 septembre 2009 et 1er décembre 2010, le directeur de l'Office national de l'eau et des milieux aquatiques a fixé la liste des services assimilés à des brigades mobiles d'intervention, et que, par deux lettres en date des 23 décembre 2011 et 6 juin 2012, le ministre chargé de la tutelle de l'Office a établi une liste des services ou fonctions ouvrant droit au bénéfice de l'indemnité de mobilité et a indiqué que la condition d'affectation en brigade mobile d'intervention posée par l'article 6 du décret du 21 décembre 2011 doit s'interpréter, non comme un critère organique, mais par référence aux missions exercées. <br/>
<br/>
              5. Au titre de la charge n° 2, la Cour des comptes a constitué M. A... et Mme D..., qui se sont succédé comme comptables publics de l'Office national de l'eau et des milieux aquatiques, débiteurs respectivement des sommes de 328 617,40 euros et de 39 171,96 euros correspondant au paiement, en 2014 et 2015, des indemnités de mobilité à des agents de l'Office qui n'étaient pas affectés en brigades mobiles d'intervention. Pour se prononcer ainsi, la Cour s'est fondée sur la seule circonstance que ces comptables n'avaient pas suspendu les paiements concernés alors que les décisions à caractère réglementaire du directeur général de l'Office et les lettres du ministre prévoyant l'attribution de la prime de mobilité à certains agents non affectés en brigades mobiles d'intervention étaient contraires aux dispositions de l'article 6 du décret du 21 décembre 2001. En statuant ainsi, alors qu'elle devait seulement rechercher, d'abord, sur quelles pièces justificatives les comptables auraient dû se fonder pour régulièrement apprécier la validité de ces dépenses, le cas échéant, compte tenu de l'absence de nomenclature applicable à l'établissement public en cause, en se référant à la nomenclature applicable à l'Etat, laquelle prévoit que figure parmi les pièces justificatives le texte institutif de l'indemnité, et, ensuite, si ces pièces justificatives ne présentaient pas d'incohérence au regard de la nature et de l'objet de la dépense engagée, elle a exigé des comptables qu'ils exercent un contrôle de légalité sur les pièces fournies par l'ordonnateur, alors que, en présence des pièces justificatives requises, ceux-ci étaient tenus de procéder aux paiements litigieux, et entaché ainsi son arrêt d'une erreur de droit. Par suite, son arrêt doit être annulé en tant qu'il porte sur la charge n° 2.<br/>
<br/>
              6. En deuxième lieu, le décret du 13 mars 2000 instituant une prime spéciale en faveur de certains personnels du ministère chargé de l'agriculture prévoit à son article 1er que celle-ci peut être attribuée aux fonctionnaires de certains corps ou emplois de ce ministère, notamment lorsqu'ils sont en position normale d'activité dans des établissements publics dont la liste est fixée par arrêté. L'arrêté du 11 août 2004 pris pour l'application de ce décret fixe cette liste, dans laquelle ne figure pas l'ONEMA.  <br/>
<br/>
              7. Au titre de la charge n° 3, la Cour des comptes a constitué M. A... et Mme D... débiteurs respectivement des sommes de 231 458,80 euros et de 108 974,26 euros correspondant au paiement, en 2014 et 2015, de la prime spéciale établie par ce décret à des ingénieurs de l'agriculture en fonctions à l'Office national de l'eau et des milieux aquatiques, au motif que cet établissement ne figurait pas sur la liste des organismes ouvrant droit au bénéfice de cette prime résultant de l'arrêté du 11 août 2004, dans sa version alors applicable. En jugeant ainsi que les comptables ne pouvaient procéder au paiement de ces indemnités sans que soit produit, au nombre des pièces justificatives, de texte rendant la prime applicable aux agents de l'Office, la Cour des comptes n'a pas commis d'erreur de droit.<br/>
<br/>
              8. Il résulte de tout ce qui précède que M. A... et Mme D... ne sont fondés à demander l'annulation de l'arrêt attaqué qu'en tant que, par ses articles 2, 5 et 8, il les a constitués débiteurs, respectivement, d'une somme totale de 328 617,40 euros augmentée des intérêts de droit au titre des exercices 2014 et 2015, et d'une somme de 39 171,96 euros augmentée des intérêts de droit au titre de l'exercice 2015.<br/>
<br/>
              9. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 3 000 euros à verser, d'une part, à Mme D..., et d'autre part, à M. A..., au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Les articles 2, 5 et 8 de l'arrêt de la Cour des comptes du 10 avril 2018 sont annulés. <br/>
Article 2 : Les affaires sont renvoyées, dans cette mesure, à la Cour des comptes.<br/>
Article 3 : L'Etat versera, d'une part, à Mme D..., et, d'autre part, à M. A..., une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Le surplus des conclusions des pourvois de Mme D... et de M. A... est rejeté.<br/>
Article 5 : La présente décision sera notifiée à Mme B... D..., à M. C... A..., à la procureure générale près la Cour des comptes, à la ministre de la transition écologique et solidaire et au ministre de l'action et des comptes publics. Copie en sera adressée à l'Office national de l'eau et des milieux aquatiques. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">18-01-03 COMPTABILITÉ PUBLIQUE ET BUDGET. RÉGIME JURIDIQUE DES ORDONNATEURS ET DES COMPTABLES. RESPONSABILITÉ. - RESPONSABILITÉ DES COMPTABLES - DÉPENSES - CONTRÔLE DE LA VALIDITÉ DE LA CRÉANCE [RJ1] - ILLUSTRATIONS - 1) DÉCISION RÉGLEMENTAIRE DU DIRECTEUR DE L'ONEMA PRÉVOYANT LE VERSEMENT D'UNE PRIME - MANQUEMENT DU COMPTABLE FAUTE D'AVOIR SUSPENDU LE PAIEMENT EN RAISON DE L'ILLÉGALITÉ DE CETTE DÉCISION RÉGLEMENTAIRE - ABSENCE - 2) DÉCRET INSTITUANT UNE PRIME SPÉCIALE ATTRIBUÉE À DES FONCTIONNAIRES SELON UNE LISTE FIXÉE PAR ARRÊTÉ - ONEMA NE FIGURANT PAS DANS CET ARRÊTÉ - MANQUEMENT DU COMPTABLE FAUTE D'AVOIR SUSPENDU LE PAIEMENT EN RAISON DE L'INCOHÉRENCE ENTRE L'ORDRE DE VERSEMENT ET L'ARRÊTÉ PRODUIT À SON SOUTIEN - EXISTENCE.
</SCT>
<ANA ID="9A"> 18-01-03 1) Pour retenir que les comptables de l'Office national de l'eau et des milieux aquatiques (ONEMA) avaient manqué à leurs obligations de contrôle, la Cour des comptes s'est fondée sur la seule circonstance que ces comptables n'avaient pas suspendu les paiements de la prime de mobilité à certains agents alors que les décisions à caractère réglementaire du directeur général de l'office et les lettres du ministre prévoyant l'attribution de la prime de mobilité à ces agents étaient contraires aux conditions d'attribution prévues par l'article 6 du décret n° 2001-1273 du 21 décembre 2001.... ,,En statuant ainsi, alors qu'elle devait seulement rechercher, d'abord, sur quelles pièces justificatives les comptables auraient dû se fonder pour régulièrement apprécier la validité de ces dépenses, le cas échéant, compte tenu de l'absence de nomenclature applicable à l'établissement public en cause, en se référant à la nomenclature applicable à l'Etat, laquelle prévoit que figure parmi les pièces justificatives le texte institutif de l'indemnité, et, ensuite, si ces pièces justificatives ne présentaient pas d'incohérence au regard de la nature et de l'objet de la dépense engagée, la Cour des comptes, qui a exigé des comptables qu'ils exercent un contrôle de légalité sur les pièces fournies par l'ordonnateur, alors que, en présence des pièces justificatives requises, ceux-ci étaient tenus de procéder aux paiements litigieux, a entaché son arrêt d'une erreur de droit.,,,2) Décret n° 2000-239 du 13 mars 2000 instituant une prime spéciale en faveur de certains personnels du ministère chargé de l'agriculture selon une liste fixée par arrêté ministériel.,,,La Cour des comptes a constitué les comptables de l'ONEMA débiteurs de sommes correspondant au paiement cette prime spéciale à des ingénieurs de l'agriculture en fonctions à l'ONEMA, au motif que cet établissement ne figurait pas sur la liste des organismes ouvrant droit au bénéfice de cette prime résultant de l'arrêté du 11 août 2004, dans sa version alors applicable. En jugeant ainsi que les comptables ne pouvaient procéder au paiement de ces indemnités sans que soit produit, au nombre des pièces justificatives, de texte rendant la prime applicable aux agents de l'Office, la Cour n'a pas commis d'erreur de droit.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 8 février 2012, Ministre du budget, des comptes publics et de la réforme de l'Etat, n° 340698, p. 34 et CE, Section, 8 février 2012, Ministre du budget, des comptes publics et de la réforme de l'Etat, n° 342825, p. 37.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
