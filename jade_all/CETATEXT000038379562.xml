<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038379562</ID>
<ANCIEN_ID>JG_L_2019_04_000000429247</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/37/95/CETATEXT000038379562.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 07/04/2019, 429247, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-04-07</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>429247</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2019:429247.20190407</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Par une requête, enregistrée le 29 mars 2019 au secrétariat du contentieux du Conseil d'Etat, M. B...A...demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, d'ordonner son rétablissement dans sa situation statuaire en donnant acte de l'inexistence de toutes les atteintes portées à sa situation depuis le décret du 26 août 1981.<br/>
<br/>
<br/>
<br/>
              Il soutient que : <br/>
              - la condition d'urgence est remplie eu égard aux troubles manifestement illicites générés, en premier lieu, par le décret du 26 août 1981 qui procède à sa nomination au tribunal de grande instance de Pontoise et, en second lieu, par le décret du 24 juillet 1987 qui procède à sa radiation ;<br/>
              - il est portée une atteinte grave et manifestement illégale au principe constitutionnel d'inamovibilité des magistrats du siège ;<br/>
              - les décrets précités des 26 août 1981 et 24 juillet 1987 sont illégaux.<br/>
<br/>
<br/>
              Par un mémoire distinct, enregistré le 29 mars 2019, présenté en application de l'article 23-5 de l'ordonnance n° 58-1067 du 7 novembre 1958, M. A...demande au juge des référés du Conseil d'Etat de renvoyer au Conseil constitutionnel la question de la conformité aux droits et libertés garantis par la Constitution de l'article L. 521-2 du code de justice administrative. Il soutient que l'application jurisprudentielle qui est faite par le juge administratif de l'article L. 521-2 précité du code de justice administrative en ce qu'elle s'éloigne du texte de la loi n° 2000-597 du 30 juin 2000 pour faire prévaloir la notion " d'urgence rendant nécessaire l'intervention dans les 48 heures d'une mesure de sauvegarde d'une liberté fondamentale ", est incompatible avec l'article 16 de la Déclaration des droits de l'Homme et du citoyen du 26 août 1789.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. Le juge des référés se prononce dans un délai de quarante-huit heures ". En vertu de l'article L. 522-3 du même code, le juge des référés peut, par une ordonnance motivée, rejeter une requête sans instruction ni audience lorsque la condition d'urgence n'est pas remplie ou lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable ou qu'elle est mal fondée. <br/>
<br/>
              2. Aux termes du premier alinéa de l'article 23-5 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel, dans la rédaction que lui a donnée la loi organique du 10 décembre 2009 : " Le moyen tiré de ce qu'une disposition législative porte atteinte aux droits et libertés garantis par la Constitution peut être soulevé (...) à l'occasion d'une instance devant le Conseil d'Etat (...) ". Aux termes des dispositions du même article, le Conseil constitutionnel est saisi de la question prioritaire de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et qu'elle soit nouvelle ou présente un caractère sérieux. <br/>
<br/>
              3. Il résulte de la combinaison de ces dispositions organiques avec celles du livre V du code de justice administrative qu'une question prioritaire de constitutionnalité peut être soulevée devant le juge administratif des référés statuant, en première instance ou en appel, sur le fondement de l'article L. 521-2 de ce code. Le juge des référés peut en toute hypothèse, y compris lorsqu'une question prioritaire de constitutionnalité est soulevée devant lui, rejeter une requête qui lui est soumise pour incompétence de la juridiction administrative, irrecevabilité ou défaut d'urgence.<br/>
<br/>
              4. L'usage par le juge des référés des pouvoirs qu'il tient des dispositions de l'article L. 521-2 du code de justice administrative est subordonné à la condition qu'une urgence particulière rende nécessaire l'intervention dans les quarante-huit heures d'une mesure de sauvegarde d'une liberté fondamentale. Il résulte de l'instruction, et notamment de la requête, que M.A..., qui évoque lui-même les conditions de recevabilité de son action, ne justifie pas d'une situation d'urgence qui rendrait nécessaire l'intervention, dans de brefs délais, d'une mesure de la nature de celles qui peuvent être ordonnées sur le fondement de l'article L. 521-2 du code de justice administrative. <br/>
<br/>
              5. Par suite, il y a lieu de rejeter l'ensemble des conclusions de la requête selon la procédure prévue par l'article L. 522-3 du code de justice administrative, sans qu'il soit besoin de se prononcer sur la transmission au Conseil constitutionnel de la question prioritaire de constitutionnalité soulevée.<br/>
<br/>
<br/>
              6. Aux termes de l'article R. 741-12 du code de justice administrative : " Le juge peut infliger à l'auteur d'une requête qu'il estime abusive une amende dont le montant ne peut excéder 10 000 euros ". La requête de M. A...présente un caractère abusif. Dès lors, il y a lieu de lui infliger, en application de ces dispositions, une amende de 2 000 euros.<br/>
<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de M. A...est rejetée.<br/>
Article 2 : M. A...est condamné à verser une amende pour recours abusif de 2 000 euros.<br/>
Article 3 : La présente ordonnance sera notifiée à M. B...A...et au directeur départemental des finances publiques des Yvelines.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
