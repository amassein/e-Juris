<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031281294</ID>
<ANCIEN_ID>JG_L_2015_10_000000387899</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/28/12/CETATEXT000031281294.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème - 2ème SSR, 05/10/2015, 387899</TITRE>
<DATE_DEC>2015-10-05</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>387899</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème - 2ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. François Lelièvre</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Bertrand Dacosta</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:XX:2015:387899.20151005</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu les procédures suivantes :<br/>
<br/>
              1° Sous le n° 387899, par une requête enregistrée le 12 février 2015 au secrétariat du contentieux du Conseil d'Etat, le comité d'entreprise du siège de l'Ifremer, le syndicat CGT-Ifremer et la fédération de l'éducation, de la recherche et de la culture (FERC-CGT) demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision du Premier ministre du 18 décembre 2014 de transférer le siège de l'Ifremer à Brest-Plouzané (Finistère) ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat le versement des sommes de 3 000 euros au comité d'entreprise de l'Ifremer et de 500 euros chacun au syndicat CGT-Ifremer et à la FERC-CGT au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              2° Sous le n° 388524, par une requête et un mémoire complémentaire, enregistrés les 6 mars et 8 juin 2015 au secrétariat du contentieux du Conseil d'Etat, la fédération générale des mines et de la métallurgie (FGMM-CFDT) demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la même décision ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat le versement de la somme de 3 500 euros à la FGMM CFDT au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu :<br/>
              - le décret n° 84-428 du 5 juin 1984 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. François Lelièvre, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Bertrand Dacosta, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Masse-Dessen, Thouvenin, Coudray, avocat de la fédération générale des mines et de la métallurgie (FGMM-CFDT) ;<br/>
<br/>
<br/>
<br/>1. Considérant que les requêtes du comité d'entreprise du siège de l'Ifremer, de la CGT Ifremer et de la FERC-CGT, d'une part, et de la FGMM-CFDT, d'autre part, sont dirigées contre le même acte ; qu'il y a lieu de les joindre pour statuer par une seule décision ;<br/>
<br/>
              2. Considérant que si, dans un discours prononcé à Brest le 18 décembre 2014, le Premier ministre a entendu confirmer la " décision " de " transfert du siège social d'Ifremer " dans cette ville que son prédécesseur avait déjà annoncée le 13 décembre 2013, il y est spécifié qu'il s'agit d'un engagement à concrétiser ; que d'ailleurs, par un courrier daté du 17 décembre 2014, les ministres de tutelle de cet établissement public, après avoir rappelé l'annonce du " principe d'un transfert du siège de l'Ifremer sur le pôle brestois " ont demandé à son directeur général de " préparer le transfert sur le campus Ifremer de Brest-Plouzané du siège " ; que ces annonces, qui sont dépourvues par elles-mêmes de tout effet juridique direct, ne révèlent pas l'existence d'une décision susceptible d'être attaquée par la voie du recours en excès de pouvoir ; qu'il suit de là que la ministre de l'éducation nationale, de l'enseignement supérieur et de la recherche est fondée à soutenir que les requêtes sont irrecevables ; qu'elles doivent, dès lors, être rejetées ;<br/>
<br/>
              3. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mis à la charge de l'Etat, qui n'est pas la partie perdante, le versement des sommes que demandent les requérants ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Les requêtes du comité d'entreprise du siège de l'Ifremer, du syndicat CGT Ifremer et de la FERC-CGT et de la FGMM-CFD sont rejetées.<br/>
Article 2 : La présente décision sera notifiée au comité d'entreprise du siège de l'Ifremer, au syndicat CGT-Ifremer, à la fédération de l'éducation, de la recherche et de la culture (FERC-CGT), à la fédération générale des mines et de la métallurgie (FGMM-CFDT), au Premier ministre et à la ministre de l'éducation nationale, de l'enseignement supérieur et de la recherche.<br/>
Copie en sera adressée à la ministre de l'écologie, du développement durable et de l'énergie.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-01-05-02-02 ACTES LÉGISLATIFS ET ADMINISTRATIFS. DIFFÉRENTES CATÉGORIES D'ACTES. ACTES ADMINISTRATIFS - NOTION. ACTES À CARACTÈRE DE DÉCISION. ACTES NE PRÉSENTANT PAS CE CARACTÈRE. - ANNONCE DU TRANSFERT D'UN ÉTABLISSEMENT PUBLIC N'EMPORTANT PAR ELLE-MÊME AUCUN EFFET JURIDIQUE [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-01-01-02-05 PROCÉDURE. INTRODUCTION DE L'INSTANCE. DÉCISIONS POUVANT OU NON FAIRE L'OBJET D'UN RECOURS. ACTES NE CONSTITUANT PAS DES DÉCISIONS SUSCEPTIBLES DE RECOURS. ACTES DÉCLARATIFS. - ANNONCE DU TRANSFERT D'UN ÉTABLISSEMENT PUBLIC N'EMPORTANT PAR ELLE-MÊME AUCUN EFFET JURIDIQUE [RJ1].
</SCT>
<ANA ID="9A"> 01-01-05-02-02 Si, dans un discours prononcé à Brest le 18 décembre 2014, le Premier ministre a entendu confirmer la décision de transfert du siège social d'Ifremer dans cette ville que son prédécesseur avait déjà annoncée le 13 décembre 2013, il y est spécifié qu'il s'agit d'un engagement à concrétiser. Par un courrier daté du 17 décembre 2014, les ministres de tutelle de cet établissement public, après avoir rappelé l'annonce du principe d'un transfert du siège de l'Ifremer sur le pôle brestois ont d'ailleurs demandé à son directeur général de préparer le transfert sur le campus Ifremer de Brest-Plouzané du siège. Ces annonces, qui sont dépourvues par elles-mêmes de tout effet juridique direct, ne révèlent pas l'existence d'une décision susceptible d'être attaquée par la voie du recours en excès de pouvoir.</ANA>
<ANA ID="9B"> 54-01-01-02-05 Si, dans un discours prononcé à Brest le 18 décembre 2014, le Premier ministre a entendu confirmer la décision de transfert du siège social d'Ifremer dans cette ville que son prédécesseur avait déjà annoncée le 13 décembre 2013, il y est spécifié qu'il s'agit d'un engagement à concrétiser. Par un courrier daté du 17 décembre 2014, les ministres de tutelle de cet établissement public, après avoir rappelé l'annonce du principe d'un transfert du siège de l'Ifremer sur le pôle brestois ont d'ailleurs demandé à son directeur général de préparer le transfert sur le campus Ifremer de Brest-Plouzané du siège. Ces annonces, qui sont dépourvues par elles-mêmes de tout effet juridique direct, ne révèlent pas l'existence d'une décision susceptible d'être attaquée par la voie du recours en excès de pouvoir.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1]Comp. CE, Assemblée, 3 mars 1993, Comité d'entreprise de la SEITA, n° 132993, p. 41 ; CE, Assemblée, 4 juin 1993, Association des anciens élèves de l'ENA, n°s 138672 138878 138952, p. 168.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
