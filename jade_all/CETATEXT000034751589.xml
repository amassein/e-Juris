<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034751589</ID>
<ANCIEN_ID>JG_L_2017_05_000000396034</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/75/15/CETATEXT000034751589.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème - 2ème chambres réunies, 17/05/2017, 396034, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-05-17</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>396034</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème - 2ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FABIANI, LUC-THALER, PINATEL ; SCP DIDIER, PINET</AVOCATS>
<RAPPORTEUR>M. François Lelièvre</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Olivier Henrard</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:396034.20170517</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Le conseil régional de l'ordre des architectes du Languedoc-Roussillon a demandé au tribunal administratif de Nîmes d'annuler la décision d'attribution du marché de maîtrise d'oeuvre de l'école intercommunale de musique de la communauté de communes de Petite Camargue au cabinet Stéphan Hermet architecture. Par un jugement n° 1303338 du 12 mars 2015, le tribunal administratif a rejeté cette demande.<br/>
<br/>
              Par un arrêt n° 15MA01938 du 9 novembre 2015, la cour administrative d'appel de Marseille, sur appel du conseil régional de l'ordre des architectes du Languedoc-Roussillon, a annulé ce jugement et renvoyé l'affaire devant le tribunal administratif de Nîmes.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 11 janvier et 11 avril 2016 au secrétariat du contentieux du Conseil d'Etat, la communauté de communes de Petite Camargue demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ; <br/>
<br/>
              2°) de mettre à la charge du conseil régional de l'ordre des architectes du Languedoc-Roussillon la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code des marchés publics ;<br/>
              - la loi n° 77-2 du 3 janvier 1977 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. François Lelièvre, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Olivier Henrard, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Didier, Pinet, avocat de la communauté de communes de Petite Camargue et à la SCP Fabiani, Luc-Thaler, Pinatel, avocat du conseil régional de l'ordre des architectes du Languedoc-Roussillon. <br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que le conseil régional de l'ordre des architectes du Languedoc-Roussillon a demandé au tribunal administratif de Nîmes d'annuler la décision d'attribution par la communauté de communes de Petite Camargue du marché de maîtrise d'oeuvre de l'école intercommunale de musique ; que par un jugement du 12 mars 2015, le tribunal administratif a rejeté cette demande comme irrecevable au motif que le conseil régional de l'ordre des architectes du Languedoc-Roussillon ne justifiait pas d'un intérêt lui donnant qualité pour demander l'annulation de cette décision ; que la communauté de communes de Petite Camargue se pourvoit en cassation contre l'arrêt du 9 novembre 2015 par lequel la cour administrative d'appel de Marseille a annulé ce jugement et renvoyé l'affaire devant le tribunal ;<br/>
<br/>
              2. Considérant, en premier lieu, que le conseil régional de l'ordre des architectes du Languedoc-Roussillon a contesté dans sa requête d'appel la fin de non-recevoir opposée à sa demande par les premiers juges ; qu'en tenant compte, pour se prononcer sur ce moyen d'appel, de l'ensemble des pièces du dossier qui lui était soumis, y compris les éléments produits en première instance par le conseil régional de l'ordre des architectes du Languedoc-Roussillon pour justifier de son intérêt à agir, la cour administrative d'appel n'a pas entaché son arrêt d'erreur de droit ni méconnu la portée des écritures de l'appelant ;<br/>
<br/>
              3. Considérant, en deuxième lieu, qu'aux termes de l'article 26 de la loi du 3 janvier 1977 sur l'architecture : " Le conseil national et le conseil régional de l'ordre des architectes concourent à la représentation de la profession auprès des pouvoirs publics. / Ils ont qualité pour agir en justice en vue notamment de la protection du titre d'architecte et du respect des droits conférés et des obligations imposées aux architectes par les lois et règlements. En particulier, ils ont qualité pour agir sur toute question relative aux modalités d'exercice de la profession ainsi que pour assurer le respect de l'obligation de recourir à un architecte. / (...) " ; qu'aux termes du second alinéa du II de l'article 74 du code des marchés publics, alors en vigueur : " Dans le cas de marchés de maîtrise d'oeuvre passés en procédure adaptée, toute remise de prestations donne lieu au versement d'une prime (...) " ;<br/>
<br/>
              4. Considérant qu'il ressort des énonciations de l'arrêt attaqué que la cour administrative d'appel de Marseille, sans rechercher si, en l'espèce, l'investissement que devait consentir les architectes candidats pour établir leur offre était significatif, a jugé que l'absence, dans l'avis d'appel à concurrence du marché litigieux, de dispositions prévoyant l'allocation de primes pour les candidats non retenus était de nature à limiter l'accès des architectes à ce marché et qu'elle était ainsi susceptible d'affecter les modalités d'exercice de cette profession ; qu'elle en a déduit que le conseil régional de l'ordre des architectes justifiait d'un intérêt lui donnant qualité pour demander l'annulation de l'attribution du marché de maîtrise d'oeuvre litigieux ; qu'en statuant ainsi, la cour n'a entaché son arrêt ni d'erreur de droit ni d'insuffisance de motivation ;<br/>
<br/>
              5. Considérant, en dernier lieu, que lorsque le juge d'appel estime que le tribunal administratif a opposé à tort une fin de non-recevoir à une demande, il annule le jugement pour irrégularité et peut soit évoquer l'affaire, soit la renvoyer devant ce tribunal ; que, dans cette seconde hypothèse, il n'a pas à statuer sur les autres fins de non-recevoir opposées à la demande de première instance qu'il appartiendra au tribunal administratif d'examiner ; que, par suite, la cour administrative d'appel de Marseille n'a pas commis d'erreur de droit en décidant d'annuler le jugement du tribunal administratif de Nîmes sans se prononcer au préalable sur l'autre fin de non-recevoir, tirée de la tardiveté de la demande, qui avait été opposée en première instance par la communauté de communes et auquel le tribunal n'avait pas répondu ;<br/>
<br/>
              6. Considérant qu'il résulte de ce qui précède que le pourvoi de la communauté de communes de Petite Camargue doit être rejeté, y compris ses conclusions tendant à l'application de l'article L. 761-1 du code de justice administrative ; qu'en revanche, il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la communauté de communes de Petite Camargue le versement d'une somme de 3 000 euros au conseil régional de l'ordre des architectes du Languedoc-Roussillon au titre des mêmes dispositions ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de la communauté de communes de Petite Camargue est rejeté.<br/>
Article 2 : La communauté de communes de Petite Camargue versera la somme de 3 000 euros au conseil régional de l'ordre des architectes du Languedoc-Roussillon au titre de l'article L.  761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée à la communauté de communes de Petite Camargue et au conseil régional de l'ordre des architectes du Languedoc-Roussillon.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
