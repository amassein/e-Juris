<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037834594</ID>
<ANCIEN_ID>JG_L_2018_12_000000414247</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/83/45/CETATEXT000037834594.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème chambre, 19/12/2018, 414247, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-12-19</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>414247</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP THOUVENIN, COUDRAY, GREVY ; SCP LEVIS</AVOCATS>
<RAPPORTEUR>M. Olivier  Fuchs</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Dieu</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:414247.20181219</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme A...B...a demandé au tribunal administratif d'Orléans d'annuler pour excès de pouvoir la décision du 17 septembre 2013 par laquelle l'inspecteur du travail de la 5ème section de l'unité territoriale du Loiret a autorisé la société Compagnie de gestion et de prêts (CGP) à la licencier et celle du ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social du 18 mars 2014 ayant rejeté son recours hiérarchique. Par un jugement n° 1402027 du 23 avril 2015, le tribunal administratif a annulé ces deux décisions.<br/>
<br/>
              Par un arrêt n° 15NT01925 du 12 juillet 2017, la cour administrative d'appel de Nantes a, sur appel de la société CGP, annulé ce jugement et rejeté la demande présentée par MmeB....<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 12 septembre et 12 décembre 2017 et le 15 juin 2018 au secrétariat du contentieux du Conseil d'Etat, Mme B...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de la société CGDP ;<br/>
<br/>
              3°) de mettre solidairement à la charge de la société BNP Paribas Personal Finance, venant aux droits de la société CGP, et de l'Etat la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code du travail ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Olivier Fuchs, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Frédéric Dieu, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Thouvenin, Coudray, Grevy, avocat de Mme B...et à la SCP Lévis, avocat de la société BNP Paribas Personal Finance venant aux droits de la société Compagnie de gestion et de prêts ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, par une décision du 17 septembre 2013, l'inspecteur du travail de la 5e section de l'unité territoriale du Loiret a autorisé la société Compagnie de gestion et de prêts (CGP), aux droits de laquelle est ultérieurement venue la société BNP Paribas Personal Finance, à licencier Mme B..., salariée protégée, pour motif économique ; que le recours hiérarchique formé par Mme B...contre cette décision a été rejeté par le ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social par une décision du 18 mars 2014 ; que par un jugement du 23 avril 2015, le tribunal administratif d'Orléans a annulé ces décisions ; que Mme B...se pourvoit en cassation contre l'arrêt du 12 juillet 2017 par lequel la cour administrative d'appel de Nantes a annulé ce jugement et rejeté sa demande de première instance ;<br/>
<br/>
              2. Considérant qu'en vertu des dispositions du code du travail, le licenciement des salariés qui bénéficient d'une protection exceptionnelle dans l'intérêt de l'ensemble des travailleurs qu'ils représentent, ne peut intervenir que sur autorisation de l'inspecteur du travail ; que, lorsque le licenciement d'un de ces salariés est envisagé, ce licenciement ne doit pas être en rapport avec les fonctions représentatives normalement exercées ou l'appartenance syndicale de l'intéressé ; que, dans le cas où la demande de licenciement est fondée sur un motif de caractère économique, il appartient à l'inspecteur du travail et, le cas échéant, au ministre, de rechercher, sous le contrôle du juge de l'excès de pouvoir, si la situation de l'entreprise justifie le licenciement du salarié, en tenant compte notamment de la nécessité des réductions envisagées d'effectifs et de la possibilité d'assurer le reclassement du salarié dans l'entreprise ou au sein du groupe auquel appartient cette dernière ;<br/>
<br/>
              3. Considérant, en premier lieu, que lorsque le ministre rejette le recours hiérarchique qui lui est présenté contre la décision de l'inspecteur du travail statuant sur la demande d'autorisation de licenciement formée par l'employeur, sa décision ne se substitue pas à celle de l'inspecteur ; que, par suite, s'il appartient au juge administratif, saisi d'un recours contre ces deux décisions, d'annuler, le cas échéant, celle du ministre par voie de conséquence de l'annulation de celle de l'inspecteur, des moyens critiquant les vices propres dont serait entachée la décision du ministre ne peuvent être utilement invoqués, au soutien des conclusions dirigées contre cette décision ; qu'ainsi, le moyen tiré de l'insuffisante motivation de la décision du ministre, dont la cour administrative d'appel était saisie, était inopérant ; que la cour n'a, par suite, pas entaché son arrêté d'irrégularité en s'abstenant d'y répondre : <br/>
<br/>
              4. Considérant, en deuxième lieu, qu'il ressort des énonciations non contestées de l'arrêt attaqué que la société CGP a été fragilisée par la cessation de ses rapports commerciaux avec son partenaire historique, son positionnement commercial et certaines modifications législatives intervenues dans le secteur du crédit à la consommation ; qu'il ressort également des énonciations non contestées de cet arrêt que les autres entreprises du groupe relevant du même secteur d'activité connaissaient d'importantes difficultés économiques ; que, par suite, en en déduisant que la société CGP justifiait, à l'appui de sa demande de licenciement de MmeB..., de la réalité des difficultés économiques qu'elle alléguait, la cour administrative d'appel de Nantes, qui a exactement qualifié les faits dont elle était saisie et a suffisamment motivé son arrêt, n'a pas commis d'erreur de droit ;<br/>
<br/>
              5. Considérant, en troisième lieu, qu'il ressort des pièces du dossier soumis aux juges du fond que Mme B...n'avait soulevé ni en première instance ni en appel de moyen tiré de ce que l'employeur aurait arrêté ses recherches de reclassement, non à la date du licenciement, mais à la date de la réunion du comité d'entreprise ; qu'elle ne peut, par suite, utilement soutenir que l'arrêt est entaché d'insuffisance de motivation et d'erreur de droit faute d'avoir répondu à un tel moyen ; qu'en outre, il ressort des termes mêmes de l'arrêt attaqué que la cour a recherché si l'inspecteur du travail avait apprécié les possibilités de reclassement de la salariée jusqu'à la date à laquelle il a statué sur la demande de l'employeur ; que la requérante ne saurait par suite utilement soutenir que la cour a, à tort, jugé que l'obligation de reclassement cessait à la date de la réunion du comité d'entreprise ;<br/>
<br/>
              6. Considérant, en dernier lieu, que la cour a pu, sans entacher son arrêt de dénaturation ni commettre d'erreur de droit, juger que la convention de mise à disposition de Mme B...auprès de l'Union régionale interprofessionnelle Centre CFDT n'était clairement plus applicable à la date de la décision litigieuse du 17 septembre 2013  ; qu'elle n'a, par suite, pas commis d'erreur de droit en écartant comme inopérant le moyen tiré de ce que cette convention était de nature à faire obstacle au licenciement de l'intéressée ;<br/>
<br/>
              7. Considérant qu'il résulte de tout ce qui précède que le pourvoi de Mme B...doit être rejeté, y compris ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative ; qu'il n'y a pas lieu, dans les circonstances de l'espèce, de mettre à la charge de Mme B...la somme que demande, au même titre, la société BNP Paribas Personal Finance ;<br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				--------------<br/>
Article 1er : Le pourvoi de Mme B...est rejeté.<br/>
Article 2 : Le surplus des conclusions de la société BNP Paribas Personal Finance, présenté au titre de l'article L.761-1 du code de justice administrative, est rejeté.<br/>
Article 3 : La présente décision sera notifiée à Mme A...B...et à la société BNP Paribas Personal Finance.<br/>
Copie en sera adressée à la ministre du travail.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
