<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042451968</ID>
<ANCIEN_ID>JG_L_2020_10_000000430901</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/45/19/CETATEXT000042451968.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème chambre, 20/10/2020, 430901, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-10-20</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>430901</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MARLANGE, DE LA BURGADE</AVOCATS>
<RAPPORTEUR>M. Joachim Bendavid</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2020:430901.20201020</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme E... D... et Mme A... B... ont demandé au juge des référés du tribunal administratif de Nantes, sur le fondement de l'article L. 521-1 du code de justice administrative, de prononcer la suspension de l'exécution de l'arrêté du 12 décembre 2018 par lequel le maire de Treillères (Loire-Atlantique) a accordé à Mme C... F... un permis d'aménager un terrain situé rue de la Chédorgère. Par une ordonnance n° 1904700 du 9 mai 2019, le juge des référés a rejeté leur demande.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et deux autres mémoires, enregistrés au secrétariat du contentieux du Conseil d'Etat les 21 et 29 mai, 23 octobre et 2 décembre 2019, Mme D... et Mme B... demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) statuant en référé, de faire droit à leur demande ;<br/>
<br/>
              3°) de mettre à la charge de la commune de Treillères la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
              Vu : <br/>
              - le code de l'urbanisme ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Joachim Bendavid, auditeur,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Marlange, de la Burgade, avocat de Mme D... et autre.<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis au juge des référés que, par un arrêté du 12 décembre 2018, le maire de Treillières (Loire-Atlantique) a accordé à Mme F... un permis d'aménager un lotissement de cinq lots sur un terrain situé rue de la Chédorgère. Mme D... et Mme B... ont demandé au juge des référés du tribunal administratif de Nantes de suspendre l'exécution de cet arrêté. Elles se pourvoient en cassation contre l'ordonnance du 9 mai 2019 par laquelle le juge des référés a rejeté leur demande.<br/>
<br/>
              2. Aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, faite l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision ".<br/>
<br/>
              3. Il résulte des termes mêmes de l'ordonnance attaquée que, pour rejeter la demande de suspension présentée par Mme D... et Mme B..., le juge des référés a jugé qu'aucun des moyens soulevés n'était de nature à créer un doute sérieux sur la légalité l'arrêté contesté. Toutefois, faute d'avoir analysé, dans les visas ou les motifs de son ordonnance, les moyens que soulevaient les requérantes, il a entaché son ordonnance d'une insuffisance de motivation qui, sans qu'il soit besoin de se prononcer sur les autres moyens du pourvoi, en justifie l'annulation. <br/>
<br/>
              4. Il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au titre de la procédure de référé engagée, en application de l'article L. 821-2 du code de justice administrative.<br/>
<br/>
              5. Pour demander la suspension de l'exécution de l'arrêté accordant le permis d'aménager contesté, Mme D... et Mme B... soutiennent :<br/>
              - qu'il a été délivré à une personne qui n'avait pas qualité pour en faire la demande au nom de l'indivision propriétaire du terrain et qui s'est faussement présentée comme sa représentante ;<br/>
               - que, accordé à l'épouse de l'adjoint au maire chargé de l'urbanisme, il est entaché de détournement de pouvoir et de prise illégale d'intérêts de la part de cet adjoint et du maire de la commune ;<br/>
              - qu'il méconnaît la règle selon laquelle la délivrance d'une autorisation d'urbanisme fait obstacle au dépôt par un tiers d'une autre demande d'autorisation visant le même terrain ;<br/>
              - qu'il faut tirer toutes les conséquences de la procédure en nullité de donation concernant la parcelle en cause, actuellement pendante devant le juge judiciaire. <br/>
<br/>
              6. Aucun de ces moyens n'est de nature à créer un doute sérieux quant à la légalité de l'arrêté contesté.<br/>
<br/>
              7. Il résulte de tout ce qui précède que la demande de Mme D... et de Mme B... doit être rejetée, y compris les conclusions qu'elles présentent au titre de l'article L. 761-1 du code de justice administrative ainsi que, sans se prononcer sur leur recevabilité, celles par lesquelles elles demandent que soit prorogée, par voie de conséquence, la validité de leur propre permis d'aménager pour le même terrain.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : L'ordonnance du 9 mai 2019 du juge des référés du tribunal administratif de Nantes est annulée.<br/>
<br/>
Article 2 : La demande présentée par Mme D... et Mme B... devant le juge des référés du tribunal administratif de Nantes et le surplus de leurs conclusions présentées devant le Conseil d'Etat au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 3 : La présente décision sera notifiée à Mme E... D..., à Mme A... B..., à la commune de Treillères et à Mme C... F.... <br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
