<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037253950</ID>
<ANCIEN_ID>JG_L_2018_07_000000404237</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/25/39/CETATEXT000037253950.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème chambre, 26/07/2018, 404237, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-07-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>404237</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP JEAN-PHILIPPE CASTON</AVOCATS>
<RAPPORTEUR>M. Alexandre  Koutchouk</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Benoît Bohnert</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:404237.20180726</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société à responsabilité limitée (SARL) Somabir a demandé au tribunal administratif de La Réunion de réduire les cotisations de taxe foncière sur les propriétés bâties auxquelles elle a été assujettie au titre des années 2011 et 2012 dans les rôles de la commune de Saint-Denis (La Réunion) à raison de deux résidences étudiantes. Par un jugement n° 1400569 du 8 juillet 2016, ce tribunal a rejeté cette demande.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 10 octobre 2016 et 10 janvier 2017 au secrétariat du contentieux du Conseil d'Etat, la société Somabir demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat de la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Alexandre Koutchouk, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Benoît Bohnert, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Jean-Philippe Caston, avocat de la société Somabir.<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis au juge du fond que la société Somabir, qui est propriétaire de deux résidences étudiantes à Saint-Denis (La Réunion), a sollicité, sur le fondement de l'article 1389 du code général des impôts, un dégrèvement partiel des cotisations de taxe foncière sur les propriétés bâties auxquelles elle a été assujettie au titre des années 2011 et 2012, à raison de la vacance d'une partie des logements de ces résidences. L'administration a rejeté sa réclamation. La société se pourvoit en cassation contre le jugement du tribunal administratif de la Réunion du 8 juillet 2016 qui a rejeté sa demande.<br/>
<br/>
              2. Aux termes du I de l'article 1389 du code général des impôts : " Les contribuables peuvent obtenir le dégrèvement de la taxe foncière en cas de vacance d'une maison normalement destinée à la location ou d'inexploitation d'un immeuble utilisé par le contribuable lui-même à usage commercial ou industriel, à partir du premier jour du mois suivant celui du début de la vacance ou de l'inexploitation jusqu'au dernier jour du mois au cours duquel la vacance ou l'inexploitation a pris fin. / Le dégrèvement est subordonné à la triple condition que la vacance ou l'inexploitation soit indépendante de la volonté du contribuable, qu'elle ait une durée de trois mois au moins et qu'elle affecte soit la totalité de l'immeuble, soit une partie susceptible de location ou d'exploitation séparée ". Ces dispositions subordonnent le dégrèvement de la taxe foncière sur les propriétés bâties à la condition, notamment, que la vacance de l'immeuble normalement destiné à la location ou l'inexploitation de l'immeuble utilisé par le contribuable lui-même à usage commercial ou industriel soit indépendante de la volonté du propriétaire. Le caractère involontaire de la vacance s'apprécie eu égard aux circonstances dans lesquelles cette vacance est intervenue et aux démarches accomplies par le propriétaire, selon les possibilités qui lui étaient offertes, en fait comme en droit, pour la prévenir ou y mettre fin.<br/>
<br/>
              3. Il ne ressort pas des pièces du dossier soumis au juge du fond, et l'administration ne le soutenait d'ailleurs pas, que les résidences étudiantes souffraient d'un défaut d'entretien de nature à faire, par lui-même, obstacle à la location des logements. Dès lors, en jugeant que la vacance des logements était imputable à l'abstention de la société Somabir à réaliser les travaux nécessaires à l'entretien régulier des locaux, le tribunal administratif a entaché son jugement de dénaturation. Il en résulte, sans qu'il soit besoin d'examiner l'autre moyen du pourvoi, que la requérante est fondée à demander l'annulation du jugement qu'elle attaque.<br/>
<br/>
              4. Il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application de l'article L. 821-2 du code de justice administrative.<br/>
<br/>
              5. Pour l'application de l'article 1389 du code général des impôts, les studios meublés que loue la société Somabir au sein de ses résidences étudiantes doivent être regardés, nonobstant la circonstance que ces résidences proposent, à titre de services annexes, l'accès à une laverie et la présence d'un concierge, comme des maisons normalement destinées à la location. Le bénéfice du dégrèvement de taxe foncière prévu par ces dispositions est par suite subordonné à la triple condition que la vacance soit indépendante de la volonté du contribuable, qu'elle ait une durée de trois mois au moins et qu'elle affecte soit la totalité de l'immeuble, soit une partie susceptible de location séparée. <br/>
<br/>
              6. Si la société Somabir a, ainsi qu'elle le soutient, mis en oeuvre des actions de communication en vue de faire connaître son offre de logements étudiants et de résorber la vacance de ses résidences, il ne résulte pas de l'instruction que les loyers qu'elle pratiquait étaient adaptés aux prestations offertes, compte tenu notamment de la concurrence d'autres résidences étudiantes récemment construites. Dans ces conditions, la requérante n'établit pas que la vacance des logements en cause serait indépendante de sa volonté. Par suite, sa demande ne peut qu'être rejetée.<br/>
<br/>
              7. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat, qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>
<br/>                D E C I D E :<br/>
                            --------------<br/>
<br/>
Article 1er : Le jugement du tribunal administratif de La Réunion du 8 juillet 2016 est annulé.<br/>
<br/>
Article 2 : La demande présentée par la société Somabir devant le tribunal administratif de La Réunion est rejetée.<br/>
<br/>
Article 3 : Le surplus des conclusions du pourvoi est rejeté.<br/>
<br/>
Article 4 : La présente décision sera notifiée à la société à responsabilité limitée Somabir et au ministre de l'action et des comptes publics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
