<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000035245541</ID>
<ANCIEN_ID>JG_L_2017_07_000000400954</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/35/24/55/CETATEXT000035245541.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème chambre, 19/07/2017, 400954, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-07-19</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>400954</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MASSE-DESSEN, THOUVENIN, COUDRAY ; SCP DE NERVO, POUPET</AVOCATS>
<RAPPORTEUR>M. Yves Doutriaux</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Xavier Domino</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2017:400954.20170719</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A...B...a demandé au tribunal administratif de Pau d'annuler les certificats d'urbanisme négatifs n° CU B06413011B0010 et n° CU B06413011B009 qui lui ont été délivrés le 31 mai 2011 par le maire de Biriatou (Pyrénées-Atlantiques). Par un jugement n°s 1101669 et 1101670 du 30 mai 2013, le tribunal administratif a annulé ces certificats.<br/>
<br/>
              Par un arrêt n° 13BX02168 du 26 avril 2016, la cour administrative d'appel de Bordeaux, sur appel de la commune de Biriatou, a annulé ce jugement en tant qu'il avait annulé le certificat d'urbanisme n° CU B06413011B010 et a rejeté, dans cette mesure, la demande présentée par M. B...devant le tribunal administratif.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés au secrétariat du contentieux du Conseil d'Etat les 27 juin et 27 septembre 2016, M. B...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de la commune de Biriatou ;<br/>
<br/>
              3°) de mettre à la charge de la commune de Biriatou la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'urbanisme ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique : <br/>
<br/>
              - le rapport de M. Yves Doutriaux, conseiller d'Etat,<br/>
<br/>
              - les conclusions de M. Xavier Domino, rapporteur public,<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Masse-Dessen, Thouvenin, Coudray, avocat de M.B..., et à la SCP de Nervo, Poupet, avocat de la commune de Biriatou ; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1.	Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, par une décision du 31 mai 2011, le maire de Biriatou a délivré à M. B...un certificat d'urbanisme négatif indiquant que ses parcelles ne pouvaient être utilisées pour y construire une maison individuelle, dès lors qu'elles se situent en dehors des parties actuellement urbanisées de la commune, que le projet est de nature à favoriser une urbanisation dispersée incompatible avec la vocation des espaces naturels environnants et que la desserte en électricité du projet nécessiterait une extension du réseau public ;<br/>
<br/>
              2.	Considérant qu'aux termes de l'article L. 600-4-1 du code de l'urbanisme : " Lorsqu'elle annule pour excès de pouvoir un acte intervenu en matière d'urbanisme ou en ordonne la suspension, la juridiction administrative se prononce sur l'ensemble des moyens de la requête qu'elle estime susceptibles de fonder l'annulation ou la suspension, en l'état du dossier " ; qu'en vertu de ces dispositions, il appartient au juge d'appel, saisi d'un jugement par lequel un tribunal administratif a prononcé l'annulation d'une décision d'urbanisme en retenant plusieurs moyens, de se prononcer sur le bien fondé de tous les moyens d'annulation retenus au soutien de leur décision par les premiers juges et d'apprécier si l'un au moins de ces moyens justifie la solution d'annulation ; que, dans ce cas, le juge d'appel n'a pas à examiner les autres moyens de première instance ; que dans le cas où il estime en revanche qu'aucun des moyens retenus par le tribunal administratif n'est fondé, le juge d'appel, saisi par l'effet dévolutif des autres moyens de première instance, examine ces moyens ; qu'il lui appartient de les écarter si aucun d'entre eux n'est fondé et, à l'inverse, en application des dispositions précitées de l'article L. 600-4-1 du code de l'urbanisme, de se prononcer, si un ou plusieurs d'entre eux lui paraissent fondés, sur l'ensemble de ceux qu'il estime, en l'état du dossier, de nature à confirmer, par d'autres motifs, l'annulation prononcée par les premiers juges ;<br/>
<br/>
              3.	Considérant que, pour annuler le certificat d'urbanisme délivré le 31 mai 2011 à M.B..., le tribunal administratif de Pau s'est fondé sur trois motifs, les deux premiers résultant de ce que, contrairement à ce qu'avait retenu le maire de Biriatou, le projet envisagé se situait dans les parties urbanisées de la commune et le dernier de ce que la parcelle en cause était desservie par le réseau public d'électricité ; que, par l'arrêt attaqué, la cour administrative d'appel de Bordeaux s'est bornée à juger que les deux premiers motifs retenus par le tribunal administratif de Pau n'étaient pas fondés, sans se prononcer sur le troisième motif relatif au raccordement de la parcelle au réseau public d'électricité ; qu'en statuant ainsi, alors que les dispositions de l'article L. 600-4-1 ont pour objet de permettre que les parties à un litige mettant en cause un acte intervenu en matière d'urbanisme soient éclairées aussi complètement que possible sur les vices susceptibles d'entacher la légalité de cet acte, la cour a commis une erreur de droit ; que, par suite et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, M. B...est fondé à demander l'annulation de l'arrêt qu'il attaque ;<br/>
<br/>
              4.	Considérant que les dispositions de l'article L. 761-1 du code de justice administrative  font  obstacle à ce qu'une somme soit mise à ce titre à la charge de M. B...qui n'est pas, dans la présente instance, la partie perdante ; qu'il n'y a pas lieu, dans les circonstances de l'espèce, de mettre à la charge de la commune de Biriatou la somme de 3 000 euros que M. B...demande au même titre ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Bordeaux du 26 avril 2016 est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Bordeaux.<br/>
<br/>
Article 3 : Les conclusions de M. B...présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 4 : Les conclusions de la commune de Biriatou présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
<br/>
Article 5 : La présente décision sera notifiée à M. A...B...et à la commune de Biriatou.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
