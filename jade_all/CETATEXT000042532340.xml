<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042532340</ID>
<ANCIEN_ID>JG_L_2020_11_000000445957</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/53/23/CETATEXT000042532340.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 10/11/2020, 445957, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-11-10</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>445957</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2020:445957.20201110</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Par une requête, enregistrée le 4 novembre 2020 au secrétariat du contentieux du Conseil d'Etat, l'association Pour réussir On s'unit Fièrement et Solidairement (P.R.O.F.S) demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-3 du code de justice administrative :<br/>
<br/>
              1°) d'ordonner toutes mesures qu'il estimera utiles afin que le Premier ministre, le ministre de l'éducation nationale, de la jeunesse et des sports, ainsi que le ministre des solidarités et de la santé, assurent le déroulé des enseignements dans des conditions sécurisées pour les élèves, les enseignants et les personnels ; <br/>
<br/>
              2°) plus précisément, d'enjoindre au Premier ministre, au ministre de l'éducation nationale, de la jeunesse et des sports, ainsi qu'au ministre des solidarités et de la santé, en premier lieu, de limiter le nombre d'élèves dans les classes, notamment en réduisant le nombre d'élèves en présentiel à la moitié des effectifs, avec une alternance par demi-journée entre les groupes reçus pour éviter les brassages entre élèves et limiter les risques de contamination, en deuxième lieu, de mettre à disposition les locaux non occupés des mairies et autres collectivités publiques aux fins de permettre le suivi des enseignements par groupes réduits avec des enseignants supplémentaires, en troisième lieu, de mettre en place le ravitaillement et l'approvisionnement, quitte à ordonner une mesure de réquisition, de masques et solutions hydroalcooliques au sein de tous les établissements, et, en dernier lieu, d'assurer la mise à l'abri des personnels et publics accueillis vulnérables. <br/>
<br/>
<br/>
<br/>
              Elle soutient que : <br/>
              - la condition d'urgence est satisfaite dès lors que, en premier lieu, elle est inhérente à la situation même " d'urgence sanitaire ", en deuxième lieu, les nombres de contaminations et de cas-contacts augmentent de façon exponentielle au sein des établissements scolaires et, en dernier lieu, les conditions d'accueil et de vie au sein de ces établissements ne permettent pas le suivi des enseignements dans le respect les règles d'hygiène et de distanciation sociale, alors que de nombreux personnels tombent malades ; <br/>
              - la condition tenant à l'absence de contestation sérieuse est satisfaite dès lors qu'aucune décision administrative n'est contestée ; <br/>
              - la condition d'utilité de la mesure sollicitée est satisfaite dès lors que, d'une part, le respect du principe de dignité humaine et le droit à la vie n'est plus assuré lorsque les personnels, enseignants et élèves sont tenus de prendre des risques pour leur santé alors que l'effectivité des protocoles sanitaires en vigueur n'est pas garantie et, d'autre part, que le gouvernement avait lui-même envisagé de prendre de telles mesures en juin 2020 pour préparer la rentrée de septembre ; <br/>
              - les mesures sollicitées ne font par elles-mêmes obstacle à l'exécution d'aucune décision administrative ; <br/>
              - les mesures sollicitées sont provisoires dès lors qu'elles n'ont vocation à durer que tant que l'épidémie n'est pas endiguée et qu'elles peuvent être amenées à évoluer en fonction de l'état sanitaire français. <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de la santé publique ;<br/>
              - le code de justice administrative ; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. Aux termes de l'article L. 521-3 du code de justice administrative : " En cas d'urgence et sur simple requête qui sera recevable même en l'absence de décision administrative préalable, le juge des référés peut ordonner toutes autres mesures utiles sans faire obstacle à l'exécution d'aucune décision administrative ". En vertu de l'article L. 522-3 du même code, le juge des référés peut, par une ordonnance motivée, rejeter une requête sans instruction ni audience lorsque la condition d'urgence n'est pas remplie ou lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable ou qu'elle est mal fondée.<br/>
<br/>
              Sur le cadre juridique : <br/>
<br/>
              2. Aux termes de l'article L. 3131-12 du code de la santé publique, issu de la loi du 23 mars 2020 d'urgence pour faire face à l'épidémie de covid-19 : " L'état d'urgence sanitaire peut être déclaré sur tout ou partie du territoire métropolitain ainsi que du territoire des collectivités régies par les articles 73 et 74 de la Constitution et de la Nouvelle-Calédonie en cas de catastrophe sanitaire mettant en péril, par sa nature et sa gravité, la santé de la population ". L'article L. 3131-13 du même code, précise que " L'état d'urgence sanitaire est déclaré par décret en conseil des ministres pris sur le rapport du ministre chargé de la santé. (...) /La prorogation de l'état d'urgence sanitaire au-delà d'un mois ne peut être autorisée que par la loi, après avis du comité de scientifiques prévu à l'article L. 3131-19 ". Enfin, il résulte de l'article L. 3131-15 du même code que " dans les circonscriptions territoriales où l'état d'urgence sanitaire est déclaré, le Premier ministre peut, par décret réglementaire pris sur le rapport du ministre chargé de la santé, aux seules fins de garantir la santé publique " prendre un certain nombre de mesures de restriction ou d'interdiction des déplacements, activités et réunions " strictement proportionnées aux risques sanitaires encourus et appropriées aux circonstances de temps et de lieu ". <br/>
<br/>
              3. L'émergence d'un nouveau coronavirus, responsable de la maladie à coronavirus 2019 ou covid-19 et particulièrement contagieux, a été qualifiée d'urgence de santé publique de portée internationale par l'Organisation mondiale de la santé le 30 janvier 2020, puis de pandémie le 11 mars 2020. La propagation du virus sur le territoire français a conduit le ministre des solidarités et de la santé puis le Premier ministre à prendre, à compter du 4 mars 2020, des mesures de plus en plus strictes destinées à réduire les risques de contagion. Pour faire face à l'aggravation de l'épidémie, la loi du 23 mars 2020 mentionnée ci-dessus a créé un régime d'état d'urgence sanitaire aux articles L. 3131-12 à L. 3131-20 du code de la santé publique et déclaré l'état d'urgence sanitaire pour une durée de deux mois à compter du 24 mars 2020. La loi du 11 mai 2020 prorogeant l'état d'urgence sanitaire et complétant ces dispositions, a prorogé cet état d'urgence sanitaire jusqu'au 10 juillet 2020 inclus. L'évolution de la situation sanitaire a conduit à un assouplissement des mesures prises et la loi du 9 juillet 2020, a organisé un régime de sortie de cet état d'urgence. <br/>
<br/>
              4. Une nouvelle progression de l'épidémie a conduit le Président de la République à prendre, sur le fondement des articles L. 3131-12 et L. 3131-13 du code de la santé publique, le décret du 14 octobre 2020 déclarant l'état d'urgence à compter du 17 octobre à 00 heure sur l'ensemble du territoire national. Le 29 octobre 2020, le Premier ministre a pris, sur le fondement de l'article L. 3131-15 du code de la santé publique, le décret contesté, prescrivant les mesures générales nécessaires pour faire face à l'épidémie de covid-19 dans le cadre de l'état d'urgence sanitaire.<br/>
<br/>
              Sur la demande en référé : <br/>
<br/>
              5. L'association P.R.O.F.S demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-3 du code de justice administrative, d'enjoindre au Premier ministre, au ministre de l'éducation nationale, de la jeunesse et des sports, ainsi qu'au ministre des solidarités et de la santé d'édicter toutes mesures utiles afin de maintenir l'accueil des élèves dans les établissements scolaires dans le respect des règles d'hygiènes et de distanciation physique qui s'imposent pour lutter contre la propagation du virus de la covid-19 dans ces établissements. La requérante demande donc au juge des référés, statuant sur le fondement de l'article L. 521-3, d'enjoindre au Premier ministre, au ministre de l'éducation nationale, de la jeunesse et des sports, ainsi qu'au ministre des solidarités et de la santé de prendre des mesures réglementaires en matière de santé et de sécurité au travail dans les établissements scolaires, en vue d'assurer la continuité du service public national de l'éducation dans le cadre de l'état d'urgence sanitaire. <br/>
<br/>
              6. Toutefois, si le juge des référés, saisi sur le fondement de l'article L. 521-3 du code de justice administrative, peut prescrire, à des fins conservatoires ou à titre provisoire, toutes mesures autres que celles régies par les articles L. 521-1 et L. 521-2 du code de justice administrative, notamment sous forme d'injonctions adressées tant à de personnes privées que, le cas échéant, à l'administration, à condition que ces mesures soient utiles et ne se heurtent à aucune contestation sérieuse, une demande tendant à ce qu'il soit ordonné à l'autorité compétente de prendre des mesures réglementaires, y compris d'organisation des services placés sous son autorité, n'est pas au nombre de celles qui peuvent être présentées au juge des référés sur le fondement de l'article L. 521-3 du code de justice administrative, eu égard à l'objet de ces dispositions et aux pouvoirs que le juge des référés tient des articles L. 521-1 et L. 521-2 du même code. <br/>
<br/>
              7. Il s'ensuit que les conclusions de l'association P.R.O.F.S tendant à ce que le juge des référés de l'article L. 521-3 du code de justice administrative enjoigne au Premier ministre, au ministre de l'éducation nationale, de la jeunesse et des sports, ainsi qu'au ministre des solidarités et de la santé l'édiction de mesures à caractère réglementaire sont irrecevables. <br/>
<br/>
              8. Il résulte de tout ce qui précède que la requête de l'association P.R.O.F.S doit être rejetée selon la procédure prévue à l'article L. 522-3 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de l'association P.R.O.F.S est rejetée. <br/>
Article 2 : La présente ordonnance sera notifiée à l'association Pour réussir On s'unit Fièrement et Solidairement ainsi qu'au ministre de l'éducation nationale, de la jeunesse et des sports et au ministre des solidarités et de la santé. <br/>
Copie en sera adressée au Premier ministre. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
