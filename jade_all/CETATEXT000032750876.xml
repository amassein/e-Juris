<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032750876</ID>
<ANCIEN_ID>JG_L_2016_06_000000395913</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/75/08/CETATEXT000032750876.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème chambre, 22/06/2016, 395913, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-06-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>395913</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROCHETEAU, UZAN-SARANO ; SCP GASCHIGNARD</AVOCATS>
<RAPPORTEUR>Mme Marie-Anne Lévêque</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Olivier Henrard</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2016:395913.20160622</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              Par un mémoire, enregistré le 6 avril 2016 au secrétariat du contentieux du Conseil d'Etat, présenté en application de l'article 23-5 de l'ordonnance n° 58-1067 du 7 novembre 1958, Mme A...B...demande au Conseil d'Etat, à l'appui de son pourvoi tendant à l'annulation de l'arrêt de la cour administrative de Lyon n° 14LY00354 du 3 novembre 2015, de renvoyer au Conseil constitutionnel la question de la conformité aux droits et libertés garantis par la Constitution de l'article 7-1, alinéa 2, de la loi n° 84-53 du 26 janvier 1984, dans sa rédaction issue de la loi n° 2007-209 du 19 février 2007 ; <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la Constitution, notamment son préambule et son article 61-1 ; <br/>
              - l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
              - la loi n° 84-53 du 26 janvier 1984, dans sa rédaction issue de la loi n° 2007-209 du 19 février 2007, notamment son article 7-1 ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Marie-Anne Lévêque, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Olivier Henrard, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Gaschignard, avocat de MmeB..., et à la SCP Rocheteau, Uzan-Sarano, avocat du département de l'Ardèche ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes du premier alinéa de l'article 23-5 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel : " Le moyen tiré de ce qu'une disposition législative porte atteinte aux droits et libertés garantis par la Constitution peut être soulevé, y compris pour la première fois en cassation, à l'occasion d'une instance devant le Conseil d'Etat (...) " ; qu'il résulte des dispositions de ce même article que le Conseil constitutionnel est saisi de la question prioritaire de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux ;<br/>
<br/>
              2. Considérant qu'aux termes des deux premiers alinéas de l'article 7-1 de la loi du 26 janvier 1984 portant dispositions statutaires relatives à la fonction publique territoriale : " Les règles relatives à la définition, à la durée et à l'aménagement du temps de travail des agents des collectivités territoriales et des établissements publics mentionnés au premier alinéa de l'article 2 sont fixées par la collectivité ou l'établissement, dans les limites applicables aux agents de l'Etat, en tenant compte de la spécificité des missions exercées par ces collectivités ou établissements. / Un décret en Conseil d'Etat détermine les conditions d'application du premier alinéa. Ce décret prévoit les conditions dans lesquelles la collectivité ou l'établissement peut, par délibération, proposer une compensation financière à ses agents, d'un montant identique à celle dont peuvent bénéficier les agents de l'Etat, en contrepartie des jours inscrits à leur compte épargne-temps /... " ; <br/>
<br/>
              3. Considérant que Mme B...soutient que les dispositions du deuxième alinéa de l'article 7-1 méconnaissent le droit de propriété et le principe d'égalité devant la loi, garantis respectivement par les articles 17 et 6 de la Déclaration des droits de l'homme du citoyen, en ce qu'elles renvoient à un décret en Conseil d'Etat le soin de prévoir les conditions dans lesquelles les collectivités territoriales peuvent, par délibération, proposer une compensation financière en contrepartie des jours inscrits aux comptes épargne-temps de leurs agents, sans prévoir l'indemnisation des jours accumulés sur le compte épargne-temps lorsqu'ils n'ont pu être pris, et en ce qu'elles conduisent à ce que soient traités de manière différente, d'une part, les agents des collectivités territoriales selon que la collectivité dont ils relèvent a ou non délibéré pour prévoir une compensation financière des jours inscrits au compte épargne-temps et, d'autre part, les agents des collectivités locales et les agents de l'Etat auxquels le pouvoir réglementaire a offert la possibilité d'une compensation financière des jours inscrits au compte épargne-temps ; <br/>
<br/>
              4. Considérant, en premier lieu, que le compte épargne-temps a pour finalité de permettre aux agents de différer dans le temps la prise d'une partie de leurs congés annuels et de leurs journées de repos instituées en contrepartie de la réduction du temps de travail ; que la possibilité ouverte aux agents qui ne souhaitent pas utiliser ces jours conformément à leur finalité d'obtenir, sous certaines conditions, une contrepartie financière, constitue un régime indemnitaire spécifique ; que par les dispositions contestées, le législateur, dans le respect du principe de libre administration des collectivités territoriales posé par l'article 72 de la Constitution, a laissé à l'appréciation des organes délibérants des collectivités territoriales la possibilité de prévoir une compensation financière des jours inscrits sur le compte épargne-temps ; qu'en ouvrant cette possibilité, ces dispositions, alors même qu'elles ne réservent pas le cas des agents des collectivités territoriales qui, en raison de circonstances particulières telles qu'une invalidité, ne pourraient utiliser les jours inscrits sur leur compte épargne-temps sous forme de jours de repos, n'ont ni pour objet ni pour effet d'affecter les droits constitués par les agents ayant inscrit des jours de congé ou des jours de réduction du temps de travail sur leur compte épargne-temps ; que, par suite, les dispositions litigieuses ne peuvent, en tout état de cause, être regardées comme portant atteinte au droit de propriété ; qu'il ne peut dès lors être soutenu, à l'appui de cette question prioritaire de constitutionnalité, que le législateur aurait, en édictant de telles dispositions, méconnu l'étendue de la compétence qu'il tient de l'article 34 de la Constitution ;<br/>
<br/>
              5. Considérant, en second lieu, que le principe d'égalité ne s'oppose ni à ce que le législateur règle de façon différente des situations différentes, ni à ce qu'il déroge à l'égalité pour des raisons d'intérêt général, pourvu que, dans l'un et l'autre cas, la différence de traitement qui en résulte soit en rapport direct avec l'objet de la loi qui l'établit ; qu'en subordonnant à l'intervention d'une délibération de l'organe délibérant des collectivités territoriales la possibilité pour les agents des collectivités locales d'obtenir une compensation financière des jours inscrits sur leur compte épargne-temps, le législateur n'a pas méconnu le principe d'égalité qui ne fait pas obstacle à ce que les agents soient soumis à un traitement différent selon la collectivité auprès de laquelle ils exercent leurs fonctions ; qu'il ne résulte, par suite, de ces dispositions aucune atteinte au principe d'égalité devant la loi entre agents de la fonction publique territoriale, ni entre ces agents et les agents de la fonction publique de l'Etat ;  <br/>
<br/>
              6. Considérant qu'il résulte de ce qui précède que la question prioritaire de constitutionnalité soulevée, qui n'est pas nouvelle, ne présente pas un caractère sérieux ; que, par suite, il n'y a pas lieu de la renvoyer au Conseil constitutionnel ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée par MmeB.... <br/>
Article 2 : La présente décision sera notifiée à Mme A...B...et au département de l'Ardèche.<br/>
Copie en sera adressée au Conseil constitutionnel, au Premier ministre et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
