<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030588361</ID>
<ANCIEN_ID>JG_L_2015_05_000000373155</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/58/83/CETATEXT000030588361.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  9ème sous-section jugeant seule, 12/05/2015, 373155, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-05-12</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>373155</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 9ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP THOUIN-PALAT, BOUCARD</AVOCATS>
<RAPPORTEUR>M. Olivier Japiot</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Aladjidi</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2015:373155.20150512</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La SAS Chanel parfums beauté a demandé au tribunal administratif de Montreuil la décharge des cotisations supplémentaires de taxe professionnelle auxquelles elle a été assujettie au titre des années 2005 et 2006 à raison de son établissement de Le Meux (60880). Par un jugement n° 1003619 du 21 juin 2011, le tribunal administratif de Montreuil a fait droit à sa demande. <br/>
<br/>
              Par un arrêt n° 11VE03272 du 9 juillet 2013, la cour administrative d'appel de Versailles a rejeté le recours formé par le ministre du budget, des comptes publics et de la réforme de l'Etat contre ce jugement. <br/>
<br/>
              Par un pourvoi et un mémoire en réplique, enregistrés les 6 novembre 2013 et 27 octobre 2014 au secrétariat du contentieux du Conseil d'Etat, le ministre de l'économie et des finances demande au Conseil d'Etat d'annuler cet arrêt.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code général des impôts ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Olivier Japiot, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Frédéric Aladjidi, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Thouin-Palat, Boucard, avocat de la SAS Chanel parfums beauté ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que, par un traité d'apport partiel d'actif conclu le 30 septembre 2004 et son avenant en date du 25 novembre 2004, la SAS Chanel parfums beauté a reçu de la SAS Bourgeois, dont elle est la filiale à 100 %, la branche complète et autonome d'activité de fabrication de produits cosmétiques, de stockage et de logistique de ces produits ainsi que l'activité de recherche et développement correspondante. La SAS Chanel parfums beauté a retenu les quatre cinquième de la valeur des immobilisations reçues dans le cadre de cet apport, en application des dispositions de l'article 1518 B du code général des impôts, pour la déclaration de taxe professionnelle du 30 décembre 2004 concernant son établissement situé rue du Bois Barbier à Le Meux (Oise). A l'issue d'une vérification de comptabilité, l'administration a estimé qu'en vertu des dispositions du 3° quater de l'article 1469 du même code, ces immobilisations devaient être retenues pour 100 % de leur valeur. Elle a ainsi rehaussé, dans cette mesure, les bases d'imposition de la société requérante. Le ministre se pourvoit contre l'arrêt du 9 juillet 2013 par lequel la cour administrative d'appel de Versailles a confirmé le jugement du 21 juin 2011 du tribunal administratif de Montreuil, lequel a déchargé la SAS Chanel parfums beauté des cotisations supplémentaires de taxe professionnelle auxquelles elle avait été assujettie au titre de l'année 2006 pour l'établissement qu'elle exploite à Le Meux. <br/>
<br/>
              2. Aux termes de l'article 1467 du code général des impôts, dans sa rédaction applicable aux impositions en litige : " La taxe professionnelle a pour base : I. 1° Dans le cas des contribuables autres que ceux visés au 2° : a. la valeur locative, telle qu'elle est définie aux articles 1469, 1518 A et 1518 B, des immobilisations corporelles dont le redevable a disposé pour les besoins de son activité professionnelle pendant la période de référence définie aux articles 1467 A et 1478, à l'exception de celles qui ont été détruites ou cédées au cours de la même période (...) ". Aux termes du 3° quater de l'article 1469 du même code, alors applicable : " Le prix de revient d'un bien cédé n'est pas modifié lorsque ce bien est rattaché au même établissement avant et après la cession et lorsque, directement ou indirectement : a. l'entreprise cessionnaire contrôle l'entreprise cédante ou est contrôlée par elle ; b. ou ces deux entreprises sont contrôlées par la même entreprise ". <br/>
<br/>
              3. Il résulte des termes mêmes des dispositions précitées du 3° quater de l'article 1469 du code général des impôts que les cessions de biens qu'elles visent s'entendent des seuls transferts de propriété consentis entre un cédant et un cessionnaire. Ces dispositions, dont les termes renvoient à une opération définie et régie par le droit civil, ne sauraient s'entendre comme incluant toutes autres opérations qui, sans constituer des " cessions " proprement dites, ont pour conséquence une mutation patrimoniale. <br/>
<br/>
              4. Cependant, la notion de cession au sens du droit civil recouvre tous les transferts de propriété consentis entre un cédant et un cessionnaire, effectués à titre gratuit ou à titre onéreux. Par suite, en jugeant que l'opération par laquelle une société apporte une branche complète et autonome d'activité à une autre société et reçoit en contrepartie des droits sociaux de la société bénéficiaire de l'apport, ne pouvait, eu égard à la nature de cette contrepartie, laquelle associe l'apporteur aux aléas de la société bénéficiaire et ne constitue dès lors pas un prix, être regardée comme une cession au sens du droit civil et en en déduisant que les apports partiels d'actifs n'entraient pas dans les prévisions du 3° quater de l'article 1469 du code général des impôts, la cour administrative d'appel de Versailles a entaché son arrêt d'une erreur de droit. Le ministre des finances et des comptes publics est, par suite, sans qu'il soit besoin d'examiner les autres moyens de son pourvoi, fondé à demander, pour ce motif, l'annulation de l'arrêt qu'il attaque.<br/>
<br/>
              5. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante.  <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : L'arrêt du 9 juillet 2013 de la cour administrative d'appel de Versailles est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Versailles.<br/>
<br/>
Article 3 : Les conclusions de la SAS Chanel parfums beauté présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée au ministre des finances et des comptes publics et à la SAS Chanel parfums beauté.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
