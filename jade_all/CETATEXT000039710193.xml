<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039710193</ID>
<ANCIEN_ID>JG_L_2019_11_000000424900</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/71/01/CETATEXT000039710193.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère chambre, 15/11/2019, 424900, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-11-15</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>424900</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DELAMARRE, JEHANNIN</AVOCATS>
<RAPPORTEUR>M. Arnaud Skzryerbak</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Marie Sirinelli</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2019:424900.20191115</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une décision du 13 mars 2019, le Conseil d'Etat, statuant au contentieux a prononcé l'admission des conclusions du pourvoi de Mme B... C... épouse A... dirigées contre le jugement du tribunal administratif de Cergy-Pontoise n° 1705255 du 28 juin 2018 en tant seulement que ce jugement s'est prononcé sur les décisions de récupération d'indus d'aides exceptionnelles de fin d'année au titre de 2014 et 2015 prises le 10 août 2016 par la caisse d'allocations familiales des Hauts-de-Seine. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'action sociale et des familles ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ; <br/>
              - le décret n° 2014-1709 du 30 décembre 2014 ;<br/>
              - le décret n° 2015-1870 du 30 décembre 2015 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Arnaud Skzryerbak, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Marie Sirinelli, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Delamarre, Jéhannin, avocat de Mme A... ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis au juge du fond que, par deux courriers du 10 août 2016, la caisse d'allocations familiales des Hauts-de-Seine a informé Mme A... de sa décision de recouvrer auprès d'elle deux indus d'un montant de 152,45 euros chacun, au motif qu'elle ne pouvait prétendre au bénéfice de l'aide exceptionnelle de fin d'année qu'elle avait perçue au titre des années 2014 et 2015. Mme A... a demandé au tribunal administratif de Cergy-Pontoise d'annuler, outre des décisions relatives à son droit au revenu de solidarité active, ces décisions, ainsi que de la décharger des sommes correspondantes et d'enjoindre à la caisse d'allocations familiales de lui restituer les sommes déjà recouvrées. Par un jugement du 28 juin 2018, le tribunal a rejeté ces conclusions. Par une décision du 13 mars 2019, le Conseil d'Etat, statuant au contentieux, a prononcé l'admission des conclusions du pourvoi formé Mme A... contre ce jugement en tant qu'il se prononce sur les décisions de récupération d'indus d'aides exceptionnelles de fin d'année au titre de 2014 et 2015. <br/>
<br/>
              2. Lorsque le juge administratif est saisi d'un recours dirigé contre une décision qui, remettant en cause des paiements déjà effectués, ordonne la récupération d'un indu d'aide exceptionnelle de fin d'année, il entre dans son office d'apprécier, au regard de l'argumentation du requérant, le cas échéant, de celle développée par le défendeur et, enfin, des moyens d'ordre public, en tenant compte de l'ensemble des circonstances de fait qui résultent de l'instruction, la régularité comme le bien-fondé de la décision de récupération d'indu. Il lui appartient, s'il y a lieu, d'annuler ou de réformer la décision ainsi attaquée, pour le motif qui lui paraît, compte tenu des éléments qui lui sont soumis, le mieux à même, dans l'exercice de son office, de régler le litige. L'organisme débiteur du revenu de solidarité active, qui doit apprécier si le bénéficiaire satisfaisait aux conditions d'ouverture du droit à cette aide prévues par la réglementation applicable et vérifier si les délais de prescription de l'action tendant à la répétition de l'aide indûment perçue ne font pas obstacle à la récupération, ne peut être regardé comme placé en situation de compétence liée, du seul fait qu'il estime à bon droit que le bénéficiaire ne pouvait prétendre au revenu de solidarité active, lorsqu'il décide de récupérer un indu d'aide exceptionnelle de fin d'année.<br/>
<br/>
              3. Aux termes de l'article 3 du décret du 30 décembre 2014 portant attribution d'une aide exceptionnelle de fin d'année à certains allocataires du revenu de solidarité active et aux bénéficiaires de l'allocation de solidarité spécifique, de l'allocation équivalent retraite et de l'allocation transitoire de solidarité : " Une aide exceptionnelle est attribuée aux allocataires du revenu de solidarité active qui ont droit à cette allocation au titre du mois de novembre 2014 ou, à défaut, du mois de décembre 2014, sous réserve que le montant dû au titre de ces périodes ne soit pas nul et à condition que les ressources du foyer (...) n'excèdent pas le montant forfaitaire mentionné au 2° de l'article L. 262-2 du même code. / Une seule aide est due par foyer ". Aux termes de l'article 6 du même décret : " Tout paiement indu d'une aide exceptionnelle attribuée en application du présent décret est récupéré pour le compte de l'Etat par l'organisme chargé du service de celle-ci (...) ". Le décret du 30 décembre 2015 portant attribution d'une aide exceptionnelle de fin d'année à certains allocataires du revenu de solidarité active et aux bénéficiaires de l'allocation de solidarité spécifique, de la prime forfaitaire pour reprise d'activité et de l'allocation équivalent retraite prévoit des dispositions similaires pour 2015.<br/>
<br/>
              4. Le tribunal administratif de Cergy-Pontoise a jugé que Mme A... n'avait pas droit au revenu de solidarité active en 2014 et en 2015. S'il résultait, dès lors, des dispositions des décrets des 30 décembre 2014 et 30 décembre 2015 qu'elle ne pouvait prétendre à l'aide exceptionnelle de fin d'année au titre des années 2014 et 2015, le tribunal ne pouvait toutefois rejeter ses conclusions tendant à l'annulation des décisions de la caisse d'allocations familiales des Hauts-de-Seine de récupérer les sommes versées à ce titre sans examiner les moyens tirés de l'incompétence de leur auteur et de leur insuffisante motivation qu'elle soulevait dans sa demande présentée devant le tribunal. En jugeant ces moyens inopérants, alors qu'il était saisi de conclusions dirigées contre une décision qui, remettant en cause des paiements déjà effectués, ordonnait la récupération d'un indu, le tribunal a commis une erreur de droit.  <br/>
<br/>
              5. Par suite, Mme A... est fondée à demander l'annulation du jugement du tribunal administratif de Cergy-Pontoise en tant qu'il statue sur les décisions de récupération d'indus d'aides exceptionnelles de fin d'année au titre de 2014 et 2015. <br/>
<br/>
              6. Il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions du pourvoi présentées au titre des articles 37 de la loi du 10 juillet 1991 et L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : Le jugement du tribunal administratif de Cergy-Pontoise du 28 juin 2018 est annulé en tant qu'il se prononce sur les décisions de récupération d'indus d'aides exceptionnelles de fin d'année au titre de 2014 et 2015 prises par la caisse d'allocations familiales des Hauts-de-Seine.<br/>
Article 2 : L'affaire est renvoyée, dans la mesure de la cassation prononcée, au tribunal administratif de Cergy-Pontoise.<br/>
Article 3 :  Les conclusions du pourvoi présentées au titre des articles 37 de la loi du 10 juillet 1991 et L. 761-1 du code de justice administrative sont rejetées. <br/>
Article 4 : La présente décision sera notifiée à Mme B... C... épouse A... et à la ministre des solidarités et de la santé.<br/>
Copie en sera adressée à la caisse d'allocations familiales des Hauts-de-Seine.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
