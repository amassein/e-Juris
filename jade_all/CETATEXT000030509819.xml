<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030509819</ID>
<ANCIEN_ID>JG_L_2015_04_000000380514</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/50/98/CETATEXT000030509819.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème SSJS, 17/04/2015, 380514, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-04-17</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>380514</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème SSJS</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FABIANI, LUC-THALER, PINATEL ; SCP DIDIER, PINET</AVOCATS>
<RAPPORTEUR>Mme Florence Chaltiel-Terral</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Maud Vialettes</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2015:380514.20150417</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme B...A...a demandé au tribunal administratif de Nancy d'annuler pour excès de pouvoir de la décision du 11 février 2011 de l'inspectrice du travail d'Epinal autorisant l'association départementale des amis et parents d'enfants inadaptés (ADAPEI) des Vosges à la licencier ainsi que de la décision du ministre du travail, de l'emploi et de la santé du 21 juillet 2011 rejetant son recours hiérarchique. Par un jugement n° 1101808 du 4 juin 2013, le tribunal administratif de Nancy a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 13NC01265 du 24 mars 2014, la cour administrative d'appel de Nancy a, sur appel de MmeA..., annulé ce jugement et ces décisions.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 21 mai et 25 juillet 2014 au secrétariat du contentieux du Conseil d'Etat, l'ADAPEI des Vosges demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) de mettre à la charge de Mme A...la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code du travail ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Florence Chaltiel-Terral, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Maud Vialettes, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Fabiani, Luc-Thaler, Pinatel, avocat de l'association départementale des amis et parents d'enfants inadaptés (ADAPEI) et à la SCP Didier, Pinet, avocat de Mme A...;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il résulte des termes de l'arrêt attaqué que, pour annuler le jugement du tribunal administratif de Nancy du 4 juin 2013 et la décision de l'inspectrice du travail de la 4ème section des Vosges du 11 février 2011 autorisant le licenciement de Mme A... par l'association départementale des amis et parents d'enfants inadaptés (ADAPEI) des Vosges, la cour administrative d'appel de Nancy s'est fondée sur ce qu'il " n'était pas contesté " que l'institut médico-éducatif de Châtenois dans lequel travaillait Mme A... constituait un établissement autonome situé à l'extérieur de la circonscription de l'inspectrice du travail, et que, par suite, celle-ci n'était pas compétente pour prendre la décision en litige ; <br/>
<br/>
              2. Mais considérant qu'il ressort des pièces du dossier soumis à la cour administrative d'appel que, lors de l'instance devant elle, le ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social a, en renvoyant à un précédent mémoire produit par lui devant le tribunal administratif de Nancy dont il joignait une copie, expressément contesté devant la cour le caractère autonome de l'institut médico-éducatif de Châtenois et affirmé, par suite, la compétence territoriale de l'inspectrice du travail ayant pris la décision attaquée ;<br/>
<br/>
              3. Considérant qu'il résulte de ce qui précède que l'ADAPEI des Vosges est fondée à soutenir que les juges du fond se sont mépris sur l'interprétation des mémoires qui leur étaient soumis ; que l'arrêt du 24 mars 2014 doit, par suite, et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, être annulé ; <br/>
<br/>
              4. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'ADAPEI des Vosges qui n'est pas, dans la présente instance, la partie perdante ; que, dans les circonstances de l'espèce, il n'y a pas lieu de mettre à la charge de Mme A...la somme que demande l'ADAPEI au titre des mêmes dispositions ; <br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Nancy du 24 mars 2014 est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Nancy. <br/>
Article 3 : Le surplus des conclusions de l'ADAPEI des Vosges et les conclusions de Mme A... présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetés. <br/>
Article 4 : La présente décision sera notifiée à l'association départementale des amis et parents d'enfants inadaptés des Vosges, à Mme B...A...et au ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
