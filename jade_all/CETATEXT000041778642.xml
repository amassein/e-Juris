<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000041778642</ID>
<ANCIEN_ID>J4_L_2020_03_000001803553</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/41/77/86/CETATEXT000041778642.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>CAA de NANTES, 1ère chambre, 31/03/2020, 18NT03553, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-03-31</DATE_DEC>
<JURIDICTION>CAA de NANTES</JURIDICTION>
<NUMERO>18NT03553</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère chambre</FORMATION>
<TYPE_REC>plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. BATAILLE</PRESIDENT>
<AVOCATS>SELARL SAVARIN AVOCAT FISCALISTE</AVOCATS>
<RAPPORTEUR>Mme Fanny  MALINGUE</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme CHOLLET</COMMISSAIRE_GVT>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
       Procédure contentieuse antérieure :<br/>
<br/>
       La société à responsabilité limitée (SARL) Broyage Plastique de l'Ouest a demandé au tribunal administratif de Nantes de prononcer la décharge des suppléments de cotisation foncière des entreprises auxquels elle a été assujettie au titre des années 2013 à 2016 dans les rôles de la commune de Puceul et de transmettre au Conseil d'Etat une question prioritaire de constitutionnalité portant sur les dispositions de l'article 1500 du code général des impôts.<br/>
<br/>
       Par un jugement avant dire droit n° 1709641 du 2 février 2018, le tribunal administratif de Nantes a décidé qu'il n'y avait pas lieu de transmettre au Conseil d'Etat la question prioritaire de constitutionnalité soulevée par la société Broyage Plastique de l'Ouest.<br/>
<br/>
       Par un jugement n°1709641 du 20 juillet 2018, le tribunal administratif de Nantes a rejeté cette demande.<br/>
<br/>
       Procédure devant la cour :<br/>
<br/>
       I. Par une requête, enregistrée le 21 septembre 2018 sous le n° 18NT03553, la SARL Broyage Plastique de l'Ouest, représentée par Me A..., demande à la cour :<br/>
<br/>
       1°) d'annuler le jugement du 20 juillet 2018 ;<br/>
<br/>
       2°) de prononcer cette décharge ;<br/>
<br/>
       3°) de mettre à la charge de l'Etat une somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
       Elle soutient que :<br/>
       - les dispositions de l'article 1500 du code général des impôts induisent une différence de détermination de la base taxable à la cotisation foncière des entreprises selon le régime fiscal du propriétaire de l'immeuble exploité alors que ce régime n'a aucune influence sur ses capacités contributives ou sur la valeur du bien qu'elle exploite ; cette différence est constitutive d'une inégalité devant la charge fiscale contraire à l'article 13 de la Déclaration des droits de l'homme et du citoyen ;<br/>
       - le dispositif prévu par les articles 1499 et 1500 du code général des impôts induit une restriction à la liberté d'établissement et une discrimination selon que l'actif à l'origine de la taxation est détenu par une société française soumise aux obligations de l'article 53 A du code général des impôts ou par toute autre personne morale établie en France ou dans un autre Etat membre ; aucun motif impérieux d'intérêt général ne justifie cette restriction et cette discrimination, contraires à l'article 49 du traité sur le fonctionnement de l'Union Européenne ;<br/>
       - elle peut se prévaloir, sur le fondement des articles L. 80 A et L. 80 B du livre des procédures fiscales, d'une prise de position formelle de l'administration fiscale définie par courrier du 28 novembre 2012 et établie lors du précédent contrôle dont elle a fait l'objet ;<br/>
       - la valeur locative doit être réduite du prix d'acquisition des équipements spécialisés permettant de contrebalancer les installations énergivores nécessaires à la transformation du plastique, de rafraîchir les machines et d'éliminer les poussières en application de l'article 1382 du code général des impôts et du point 50 de l'instruction BOI-IF-TFB-10-10-10 ; elle en fournit la liste et la copie des factures d'achat.<br/>
       Par un mémoire en défense, enregistré le 1er avril 2019, le ministre de l'action et des comptes publics conclut au rejet de la requête.<br/>
<br/>
       Il fait valoir que :<br/>
       - les conclusions relatives à l'exonération de la facture Eurofeu services du 19 octobre 2012 pour un montant de 2 700,06 euros sont sans objet dès lors qu'un dégrèvement a été prononcé à ce titre le 5 septembre 2017 ;<br/>
       - le moyen tiré de la rupture d'égalité devant les charges publiques est inopérant à l'encontre d'une imposition légalement établie ; <br/>
       - le moyen tiré de la méconnaissance des principes généraux du droit communautaire est inopérant à l'encontre de cotisations uniquement régies par la loi fiscale interne ;<br/>
       - les autres moyens présentés par la société Broyage Plastique de l'Ouest ne sont pas fondés.<br/>
<br/>
       II. Par une requête et un mémoire, enregistrés les 21 septembre 2018 et 21 mars 2019 sous le n° 18NT03567, la société Broyage Plastique de l'Ouest, représentée par Me A..., demande à la cour :<br/>
<br/>
       1°) d'annuler le jugement avant dire droit du 2 février 2018 ;<br/>
<br/>
       2°) de transmettre au Conseil d'Etat la question prioritaire de constitutionnalité soulevée.<br/>
<br/>
       Elle soutient que :<br/>
       - l'article 1500 du code général des impôts induit une différence de traitement entre les redevables de la cotisation foncière des entreprises placés dans une situation identique, qui n'est pas justifiée par des raisons d'intérêt général et n'est pas en adéquation avec l'objet de la loi, laquelle a établi les règles relatives à la détermination des valeurs locatives soumises aux impôts locaux, ce qui porte ainsi atteinte aux principes constitutionnels d'égalité devant la loi et devant les charges publiques découlant des articles 6 et 13 de la Déclaration des droits de l'homme et du citoyen de 1789 ; <br/>
       - la question prioritaire de constitutionnalité qu'elle soulève est d'une application directe dans le litige qui l'oppose à l'administration fiscale dans sa requête en appel formé contre le rejet de sa demande de décharge des suppléments de cotisation foncière des entreprises auxquels elle a été assujettie au titre des années 2013 à 2016 ;<br/>
       - l'article 1500 du code général des impôts n'a pas déjà été déclaré conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel et la question prioritaire de constitutionnalité qu'elle soulève présente un caractère nouveau ;<br/>
       - la question prioritaire de constitutionnalité présente un caractère sérieux.<br/>
<br/>
       Par un mémoire en défense, enregistré le 22 février 2019, le ministre de l'action et des comptes publics conclut au rejet de la requête.<br/>
<br/>
       Il fait valoir que, la question prioritaire de constitutionnalité soulevée ne présentant pas un caractère sérieux, il n'y a pas lieu de la transmettre au Conseil d'Etat.<br/>
<br/>
       Vu les autres pièces des dossiers.<br/>
<br/>
       Vu : <br/>
       - la Constitution, notamment son Préambule et son article 61-1 ;<br/>
       - l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
       - le code général des impôts et le livre des procédures fiscales ;<br/>
       - la loi n° 2008-1443 du 30 décembre 2008 ;<br/>
       - le décret n° 2009-389 du 7 avril 2009 ;<br/>
       - le code de justice administrative.<br/>
<br/>
       Les parties ont été régulièrement averties du jour de l'audience. <br/>
<br/>
       Ont été entendus au cours de l'audience publique : <br/>
       - le rapport de Mme B...,<br/>
       - et les conclusions de Mme Chollet, rapporteur public.<br/>
<br/>
       Considérant ce qui suit :<br/>
<br/>
       1. La société à responsabilité limitée (SARL) Broyage Plastique de l'Ouest (BPO), qui exerce, dans son établissement de Puceul, une activité de récupération et de transformation de déchets plastiques rigides, chutes de production ou produits " fin de vie ", a fait l'objet d'une vérification de comptabilité à l'issue de laquelle, par courrier du 25 octobre 2016, l'administration fiscale a rectifié les bases imposables en matière de cotisation foncière des entreprises au titre des années 2013 à 2016, au motif que la valeur locative du bâtiment exploité par la SARL BPO devait être évaluée selon la méthode comptable, prévue à l'article 1499 du code général des impôts au lieu de la méthode par appréciation directe prévue à l'article 1498 du code général des impôts. Les impositions supplémentaires résultant de cette rectification ont été mises en recouvrement le 30 avril 2017 pour un montant total, en droits et intérêts de retard, de 46 402 euros. Après le rejet partiel de sa réclamation préalable, la société BPO a demandé au tribunal administratif de Nantes de prononcer la décharge des suppléments de cotisation foncière des entreprises auxquels elle a été assujettie au titre des années 2013 à 2016 dans les rôles de la commune de Puceul et de transmettre au Conseil d'Etat une question prioritaire de constitutionnalité portant sur les dispositions de l'article 1500 du code général des impôts. Par un jugement avant dire droit du 2 février 2018, dont la société BPO relève appel par une requête enregistrée sous le n° 18NT03567, ce tribunal a décidé qu'il n'y avait pas lieu de transmettre au Conseil d'Etat la question prioritaire de constitutionnalité soulevée par la société Broyage Plastique de l'Ouest. Par un jugement du 20 juillet 2018, dont la société relève appel par une requête enregistrée sous le n°18NT03553, ce même tribunal a rejeté sa demande.<br/>
<br/>
       2. Les requêtes n°18NT03553 et n°18NT03567 présentent à juger des questions connexes. Il y a lieu de les joindre pour y statuer par un même arrêt.<br/>
<br/>
       Sur la contestation du refus de transmission de la question prioritaire de constitutionnalité :<br/>
<br/>
       3. D'une part, aux termes de l'article 61-1 de la Constitution : " Lorsque à l'occasion d'une instance en cours devant une juridiction, il est soutenu qu'une disposition législative porte atteinte aux droits et libertés que la Constitution garantit, le Conseil constitutionnel peut être saisi de cette question sur renvoi du Conseil d'Etat ou de la Cour de cassation qui se prononce dans un délai déterminé " et aux termes de l'article 23-2 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel : " La juridiction statue sans délai par une décision motivée sur la transmission de la question prioritaire de constitutionnalité au Conseil d'Etat ou  à la Cour de cassation. Il est procédé à cette transmission si les conditions suivantes sont remplies : / 1° La disposition contestée est applicable au litige ou à la procédure ou constitue le fondement des poursuites ; 2° Elle n'a pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances ; 3° La question n'est pas dépourvue de caractère sérieux ".<br/>
<br/>
       4. D'autre part, aux termes de l'article 1500 du code général des impôts, dans sa rédaction applicable au litige : " Les bâtiments et terrains industriels sont évalués : / - 1° selon les règles fixées à l'article 1499 lorsqu'ils figurent à l'actif du bilan de leur propriétaire ou de leur exploitant, et que celui-ci est soumis aux obligations définies à l'article 53 A ; / - 2° selon les règles fixées à l'article 1498 lorsque les conditions prévues au 1° ne sont pas satisfaites. ". Aux termes de l'article 53 A du même code : " Sous réserve des dispositions de l'article 302 septies A bis, les contribuables, autres que ceux soumis au régime défini à l'article 50-0 (1), sont tenus de souscrire chaque année, dans les conditions et délais prévus aux articles 172 et 175, une déclaration permettant de déterminer et de contrôler le résultat imposable de l'année ou de l'exercice précédent. (...) ".<br/>
<br/>
       5. Aux termes de l'article 6 de la Déclaration des droits de l'homme et du citoyen de 1789 : " La loi (...) doit être la même pour tous, soit qu'elle protège, soit qu'elle punisse ". Le principe d'égalité ne s'oppose ni à ce que le législateur règle de façon différente des situations différentes, ni à ce qu'il déroge à l'égalité pour des raisons d'intérêt général, pourvu que, dans l'un et l'autre cas, la différence de traitement qui en résulte soit en rapport direct avec l'objet de la loi qui l'établit. Aux termes de l'article 13 de cette Déclaration : " Pour l'entretien de la force publique, et pour les dépenses d'administration, une contribution commune est indispensable : elle doit être également répartie entre tous les citoyens, en raison de leurs facultés ". En vertu de l'article 34 de la Constitution, il appartient au législateur de déterminer, dans le respect des principes constitutionnels et compte tenu des caractéristiques de chaque impôt, les règles selon lesquelles doivent être appréciées les facultés contributives. En particulier, pour assurer le respect du principe d'égalité, il doit fonder son appréciation sur des critères objectifs et rationnels en fonction des buts qu'il se propose. Cette appréciation ne doit cependant pas entraîner de rupture caractérisée de l'égalité devant les charges publiques.<br/>
<br/>
       6. La société BPO soutient que l'article 1500 du code général des impôts induit une différence de traitement entre les redevables de la cotisation foncière des entreprises placés dans une situation identique, qui n'est pas justifiée par des raisons d'intérêt général et n'est pas en adéquation avec l'objet de la loi, laquelle a établi les règles relatives à la détermination des valeurs locatives soumises aux impôts locaux, portant ainsi atteinte aux principes constitutionnels d'égalité devant la loi et devant les charges publiques découlant des articles 6 et 13 de la Déclaration des droits de l'homme et du citoyen de 1789.<br/>
<br/>
       7. L'article 1500 du code général des impôts prévoit que la méthode dite comptable, prévue à l'article 1499 du code général des impôts, est appliquée aux bâtiments et terrains figurant à l'actif du bilan de leur propriétaire ou exploitant astreint aux obligations définies à l'article 53 A du code général des impôts, au nombre desquelles figure la tenue d'une comptabilité complète incluant la tenue d'un bilan dans lequel sont inscrits les immobilisations corporelles tandis que la méthode prévue à l'article 1498 du code général des impôts est appliquée dans les autres cas. Ces dispositions prennent en compte, au regard de l'objet de la loi qui est d'organiser une évaluation fiable de la valeur locative des bâtiments et terrains industriels, la différence de situation du propriétaire ou de l'exploitant de ces immeubles selon que ces biens font l'objet ou non d'une valorisation comptable. Par suite, elles ne portent pas atteinte aux principes d'égalité devant la loi et devant les charges publiques garantis par les articles 6 et 13 de la Déclaration des droits de l'homme et du citoyen.<br/>
<br/>
       8. Par ailleurs, s'agissant plus particulièrement des sociétés civiles immobilières, aucune disposition ne fait obligation à une société civile immobilière relevant du 1° de l'article 8 du code général des impôts, dont les parts ne sont pas inscrites à l'actif d'une personne morale passible de l'impôt sur les sociétés dans les conditions de droit commun ou d'une entreprise industrielle, commerciale, artisanale ou agricole imposable à l'impôt sur le revenu de plein droit selon un régime de bénéfice réel, de tenir elle-même une comptabilité d'engagement impliquant, en particulier, l'établissement d'un bilan annuel lorsque son activité consiste en la location de biens immobiliers dans des conditions non constitutives d'une exploitation commerciale. A défaut d'exercice par cette société de l'option pour son assujettissement à l'impôt sur les sociétés, prévue par le b du 3 de l'article 206 du code général des impôts, la valeur locative des bâtiments et terrains industriels dont elle est propriétaire est déterminée, en application des dispositions du 2° de l'article 1500 du code général des impôts, selon les règles fixées à l'article 1498 du même code. Une telle société civile immobilière, lorsqu'elle exerce l'option pour son assujettissement à l'impôt sur les sociétés, se voit astreinte, de ce fait, aux obligations déclaratives de l'article 53 A du même code et soumise à l'obligation de tenir une comptabilité d'engagement impliquant d'inscrire à l'actif de son bilan les biens durablement productifs de revenus. Cette société remplit alors les conditions prévues par le 1° de l'article 1500 du même code et la valeur locative des immeubles industriels dont elle est propriétaire doit être déterminée selon les règles fixées à l'article 1499 de ce code. La différence de traitement ainsi relevée par la société requérante procède toutefois non de la loi elle-même, mais de l'exercice par le contribuable de l'option pour son assujettissement à l'impôt sur les sociétés. Il en résulte que l'article 1500 du code général des impôts ne saurait être regardé, en ce qu'il conduit, pour les sociétés civiles immobilières ayant exercé cette option, à déterminer la valeur locative des immeubles assujettis à la taxe foncière sur les propriétés bâties selon les règles fixées à l'article 1499 de ce code, comme portant atteinte aux principes d'égalité devant la loi et les charges publiques.<br/>
<br/>
       9. Il résulte de ce qui a été dit aux points 6 et 7 que la question qu'elle soulève ne présentant pas un caractère sérieux, la société BPO n'est pas fondée à soutenir que c'est à tort que, par le jugement avant dire droit attaqué, le tribunal administratif de Nantes a refusé de transmettre cette question au Conseil d'Etat.<br/>
<br/>
<br/>
       Sur les conclusions aux fins de décharge :<br/>
<br/>
       En ce qui concerne l'application de la méthode dite comptable :<br/>
<br/>
       10. En premier lieu, la société BPO ne peut utilement invoquer une méconnaissance du droit de l'Union européenne par l'administration fiscale à l'encontre de rappels de cotisation foncière des entreprises uniquement régis par la loi fiscale interne.<br/>
<br/>
       11. En deuxième lieu, aux termes de l'article L. 80 A du livre des procédures fiscales dans sa rédaction applicable : " Il ne sera procédé à aucun rehaussement d'impositions antérieures si la cause du rehaussement poursuivi par l'administration est un différend sur l'interprétation par le redevable de bonne foi du texte fiscal et s'il est démontré que l'interprétation sur laquelle est fondée la première décision a été, à l'époque, formellement admise par l'administration. Lorsque le redevable a appliqué un texte fiscal selon l'interprétation que l'administration avait fait connaître par ses instructions ou circulaires publiées et qu'elle n'avait pas rapportée à la date des opérations en cause, elle ne peut poursuivre aucun rehaussement en soutenant une interprétation différente. Sont également opposables à l'administration, dans les mêmes conditions, les instructions ou circulaires publiées relatives au recouvrement de l'impôt et aux pénalités fiscales. ". Aux termes de l'article L. 80 B du même livre : " La garantie prévue au premier alinéa de l'article L. 80 A est applicable : / 1° Lorsque l'administration a formellement pris position sur l'appréciation d'une situation de fait au regard d'un texte fiscal ; elle se prononce dans un délai de trois mois lorsqu'elle est saisie d'une demande écrite, précise et complète par un redevable de bonne foi. (...) ".<br/>
<br/>
       12. Le courrier du 28 novembre 2012, confirmant l'évaluation des biens immobiliers utilisés par la société BPO selon la méthode d'appréciation directe prévue à l'article 1498 du code général des impôts pour l'établissement de la taxe professionnelle due au titre de l'année 2009 et la cotisation foncière des entreprises au titre des années 2010 à 2012, ne fait aucune référence à l'article 1499 du code général des impôts et ne comporte aucune analyse des conséquences de la cession, par acte du 31 janvier 2012, de ces biens à la société civile immobilière (SCI) Maline Immobiliers, qui est soumise, contrairement au précédent propriétaire, aux obligations définies à l'article 53 A du code général des impôts. Dès lors que cette cession a induit une modification de la situation de la société BPO au regard des dispositions de l'article 1500 du code général des impôts alors en vigueur, la garantie prévue par l'article L. 80 B du livre des procédures fiscales a cessé de produire ses effets. Par suite, la société requérante n'est pas fondée à se prévaloir, sur le fondement des dispositions citées au point 10, d'une prise de position formelle faisant obstacle à l'évaluation de la valeur locative des biens selon la méthode comptable prévue à l'article 1499 du code général des impôts.<br/>
<br/>
       En ce qui concerne les biens d'équipements spécialisés :<br/>
<br/>
       S'agissant de l'application de la loi fiscale :<br/>
<br/>
       13. Aux termes de l'article 1382 du code général des impôts : " Sont exonérés de la taxe foncière sur les propriétés bâties : (...) 11° Les outillages et autres installations et moyens matériels d'exploitation des établissements industriels à l'exclusion de ceux visés aux 1° et 2° de l'article 1381 (...) ". Pour bénéficier de l'exonération prévue par ces dispositions, les outillages et autres installations et moyens matériels d'exploitation des établissements industriels doivent, d'une part, participer directement à l'activité industrielle de l'établissement, d'autre part, être dissociables des immeubles et ne pas faire corps avec eux.<br/>
<br/>
       14. En premier lieu, il résulte de l'instruction que l'administration fiscale a admis, au stade de la décision prise sur la réclamation préalable, l'exonération demandée au titre de la facture Eurofeu services de 2 700,06 euros.<br/>
<br/>
       15. En deuxième lieu, les installations électriques peuvent être utilisées en cas d'affectation des locaux à d'autres activités et n'ont pas vocation à être dissociées de l'immeuble auxquelles elles ont été incorporées sauf preuve contraire qui n'est pas rapportée en l'espèce par les précisions et factures produites. Ainsi, elles ne constituent pas des outillages, installations et moyens matériels d'exploitation au sens des dispositions du 11° de l'article 1382 du code général des impôts. Par suite, l'administration a pu retenir, pour le calcul de la valeur locative ayant servi de base à l'établissement des impositions en litige sur le fondement du 1° de l'article 1381 du code général des impôts, les biens correspondant aux factures Laurent de 13 038,43 euros, Monnier de 1 298,30 euros, Edf de 1 532,11 euros et Juret de 1360,27 euros.<br/>
<br/>
       16. En troisième lieu, les seuls libellés et factures produits par la société requérante, qui a, par ailleurs, enregistré ces biens en compte 213500 " installations générales, agencements, aménagements des constructions ", sont insuffisants pour justifier de la participation directe à l'activité industrielle de l'établissement et du caractère dissociable des biens correspondant aux factures BHR Industrie de 1 040 euros et 450 euros, MAT de 6 360,30 euros et Alizeol de 8 500 euros ainsi qu'à la facture Juret de 5 068,83 euros.<br/>
<br/>
       S'agissant de l'interprétation administrative de la loi fiscale :<br/>
<br/>
       17. La société BPO n'est pas fondée à se prévaloir du paragraphe 50 de l'instruction BOI-IF-TFB-20-10-50-30, et non BOI-IF-TFB-10-10-10 comme elle le mentionne à tort, qui ne comporte pas de la loi fiscale une interprétation différente de celle dont il a été fait application.<br/>
<br/>
       18. Il résulte de ce qui a été dit aux points 9 à 16 que la société BPO n'est pas fondée à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Nantes a rejeté sa demande.<br/>
<br/>
       19. Il résulte de tout ce qui précède que les requêtes de la société BPO, y compris les conclusions relatives aux frais liés au litige, doivent être rejetées.<br/>
<br/>
DECIDE :<br/>
Article 1er : Les requêtes n°18NT03553 et n°18NT03567 de la SARL Broyage Plastique de l'Ouest sont rejetées.<br/>
Article 2 : Le présent arrêt sera notifié à la société à responsabilité limitée Broyage Plastique de l'Ouest et au ministre de l'action et des comptes publics.<br/>
      Délibéré après l'audience du 5 mars 2020, à laquelle siégeaient :<br/>
<br/>
       - M. Bataille, président de chambre,<br/>
       - M. Geffray, président assesseur,<br/>
       - Mme B..., premier conseiller.<br/>
<br/>
<br/>
      Rendu public par mise à disposition au greffe de la juridiction le 31 mars 2020.<br/>
<br/>
Le rapporteur,<br/>
F. B...Le président,<br/>
F. Bataille<br/>
Le greffier,<br/>
A. Rivoal<br/>
       La République mande et ordonne au ministre de l'action et des comptes publics en ce qui le concerne, et à tous huissiers de justice à ce requis en ce qui concerne les voies de droit commun contre les parties privées, de pourvoir à l'exécution de la présente décision.<br/>
<br/>
1<br/>
2<br/>
N°s18NT03553-18NT03567<br/>
1<br/>
<br/>
<br/>
<br/>
<br/>
</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
