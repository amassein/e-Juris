<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027666365</ID>
<ANCIEN_ID>JG_L_2013_07_000000361364</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/66/63/CETATEXT000027666365.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  3ème sous-section jugeant seule, 05/07/2013, 361364, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-07-05</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>361364</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 3ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DIDIER, PINET</AVOCATS>
<RAPPORTEUR>Mme Anne Egerszegi</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Vincent Daumas</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2013:361364.20130705</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi, enregistré le 26 juillet 2012 au secrétariat du contentieux du Conseil d'Etat, présenté par le ministre de l'intérieur ; le ministre demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler le jugement n° 0903902 du 27 juin 2012, par lequel le tribunal administratif de Nantes a annulé pour excès de pouvoir la décision du 16 janvier 2009 par laquelle le préfet de la Loire-Atlantique a rejeté la demande de Mme A...B...tendant au versement, sur son compte épargne-temps, de dix jours acquis au titre de l'aménagement et de la réduction du temps de travail pour l'année 2008 ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter la demande de MmeB... ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la loi n° 83-634 du 13 juillet 1983 ;<br/>
<br/>
              Vu la loi n° 84-16 du 11 janvier 1984 ;<br/>
<br/>
              Vu la loi n° 2010-1657 du 29 décembre 2011, notamment son article 115 ; <br/>
<br/>
              Vu le décret n° 2000-815 du 25 août 2000 ;<br/>
<br/>
              Vu le décret n° 2002-634 du 29 avril 2002 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Anne Egerszegi, Maître des Requêtes,  <br/>
<br/>
              - les conclusions de M. Vincent Daumas, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Didier, Pinet, avocat de Mme A...B...;<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que MmeB..., fonctionnaire du ministère de l'intérieur affectée à la préfecture de la Loire-Atlantique, a présenté, par un courrier du 8 janvier 2009, une demande de versement, sur son compte épargne-temps, de dix jours acquis pour l'année 2008, au titre de l'aménagement et de la réduction du temps de travail ; que par une note du 16 janvier 2009, le secrétaire général de la préfecture a informé Mme B... que sa demande ne pouvait être accueillie dans la mesure où elle ne disposait, compte-tenu de la minoration effectuée au regard de la durée de ses congés de maladie et des autorisations exceptionnelles d'absence dont elle avait bénéficié, que d'un reliquat de quatre jours de récupération ; que Mme B...a présenté, le 5 mars 2009, un recours hiérarchique auprès du préfet de la Loire-Atlantique à l'encontre de la note du secrétaire général ; que ce recours a été rejeté par un courrier du 28 avril 2009 ; que par une requête, enregistrée le 1er juillet 2009 au greffe du tribunal administratif de Nantes, Mme B...a demandé l'annulation de ces décisions et contesté, par voie d'exception, la légalité du règlement intérieur de la préfecture ; que par un jugement du 27 juin 2012, le tribunal a fait droit à sa demande ; que le ministre de l'intérieur se pourvoit en cassation contre ce jugement ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article 1er du décret du 25 août 2000 relatif à l'aménagement et à la réduction du temps de travail dans la fonction publique de l'Etat : " La durée du travail effectif est fixée à 35 heures par semaine dans les services et établissements publics administratifs de l'Etat ainsi que dans les établissements locaux d'enseignement. / Le décompte du temps de travail est réalisé sur la base d'une durée annuelle de travail effectif de 1607 heures maximum, sans préjudice des heures supplémentaires susceptibles d'être effectuées " ; que l'article 2 de ce décret dispose que : " La durée du travail effectif s'entend comme le temps pendant lequel les agents sont à la disposition de leur employeur et doivent se conformer à ses directives sans pouvoir vaquer à des occupations personnelles " ; que pour l'application de ces dispositions, les agents placés en congés de maladie, de longue maladie ou de longue durée en vertu de l'article 34 de la loi du 11 janvier 1984 portant dispositions statutaires relatives à la fonction publique de l'Etat, s'ils se trouvent dans une position statutaire d'activité qui leur permet de satisfaire aux obligations relatives à la durée légale du temps de travail, ne peuvent être regardés ni comme exerçant effectivement leurs fonctions ni comme se trouvant à la disposition de leur employeur et en situation de devoir se conformer à ses directives sans pouvoir vaquer à des occupations personnelles ; que, dès lors, le temps pendant lequel les agents sont placés en congés de maladie, de longue maladie ou de longue durée ne peut être pris en compte pour le calcul de la durée annuelle du travail effectif ni donner lieu à récupération du temps correspondant ;<br/>
<br/>
              3. Considérant qu'en énonçant, d'une part, que le droit aux congés de maladie, de longue maladie ou de longue durée, prévu par l'article 34 de la loi du 11 janvier 1984 susvisée, fait obligation de prendre en compte, pour le calcul des durées annuelles de travail effectif, le temps pendant lequel les agents sont placés en congés de maladie, de longue maladie ou de longue durée et, d'autre part, que la définition de la durée du travail effectif donnée par l'article 2 du décret du 25 août 2000 pour la fonction publique de l'Etat, d'ailleurs similaire à celle figurant dans les décrets applicables aux autres fonctions publiques, n'a pas pour objet et ne saurait avoir légalement pour effet d'exclure du temps de travail effectif le temps des congés de maladie, de longue maladie ou de longue durée, le tribunal administratif de Nantes a commis une erreur de droit ; que, dès lors, le ministre de l'intérieur est fondé à demander l'annulation du jugement qu'il attaque ;<br/>
<br/>
              4. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              5. Considérant qu'ainsi qu'il a été dit ci-dessus, il résulte des dispositions des articles 1er et 2 du décret du 25 août 2000 susmentionné que les agents placés en congés de maladie, de longue maladie ou de longue durée en vertu de l'article 34 de la loi du 11 janvier 1984 susvisée, s'ils se trouvent dans une position d'activité qui leur permet de satisfaire aux obligations relatives à la durée légale du temps de travail, ne peuvent être regardés ni comme exerçant effectivement leurs fonctions ni comme se trouvant à la disposition de leur employeur et en situation de devoir se conformer à ses directives sans pouvoir vaquer à des occupations personnelles ; que dès lors, Mme B...n'est pas fondée à demander l'annulation de la décision du 28 avril 2009 par laquelle le préfet de la Loire-Atlantique a rejeté sa demande de versement, sur son compte épargne-temps, de dix jours au titre de l'année 2008 ni à contester, par voie d'exception, la légalité de l'article II-3.3 du règlement intérieur de la préfecture de la Loire-Atlantique sur lequel s'est fondé le préfet pour prendre sa décision ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du tribunal  administratif de Nantes du 27 juin 2012 est annulé.<br/>
Article 2 : La requête formée par Mme B...devant le tribunal administratif de Nantes est rejetée.<br/>
Article 3 : La présente décision sera notifiée au ministre de l'intérieur et à MmeB....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
