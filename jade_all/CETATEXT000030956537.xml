<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030956537</ID>
<ANCIEN_ID>JG_L_2015_07_000000367864</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/95/65/CETATEXT000030956537.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème SSJS, 27/07/2015, 367864, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-07-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>367864</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème SSJS</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROCHETEAU, UZAN-SARANO ; SCP ODENT, POULET</AVOCATS>
<RAPPORTEUR>Mme Anne Egerszegi</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Vincent Daumas</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2015:367864.20150727</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La mutuelle assurance des instituteurs de France (MAIF) a saisi le tribunal administratif de Nancy d'une demande tendant à la condamnation de la commune de Vaxoncourt à lui verser la somme de 2 000 euros, assortie des intérêts au taux légal à compter de la demande préalable, en remboursement du total des indemnités versées à Mlle B...A..., victime de l'accident causé par l'explosion d'un bûcher de la Saint-Jean le 30 juin 2007.<br/>
<br/>
              Par un jugement n° 1100661 du 15 janvier 2013, le tribunal administratif de Nancy a rejeté sa demande.<br/>
<br/>
              Par une requête enregistrée au greffe de la cour administrative d'appel de Nancy, le 18 mars 2013, la MAIF a demandé cette cour d'annuler le jugement du 15 janvier 2013 du tribunal administratif de Nancy et de faire droit à sa demande.<br/>
<br/>
              Par une ordonnance n° 13NC00479 du 11 avril 2013, enregistrée le 19 avril 2013 au secrétariat du contentieux du Conseil d'Etat, le président de la cour administrative d'appel de Nancy a transmis au Conseil d'Etat, en application de l'article R. 351-2 du code de justice administrative, la requête présentée à cette cour par la MAIF.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 12 juillet et 4 septembre 2013 au secrétariat du contentieux du Conseil d'Etat, la MAIF demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler le jugement du 15 janvier 2013 du tribunal administratif de Nancy ;<br/>
<br/>
              2°) réglant l'affaire au fond de faire droit, de faire droit à sa demande ; <br/>
<br/>
              3°) de mettre à la charge de la commune de Vaxoncourt la somme de 450 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code général des collectivités territoriales ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Anne Egerszegi, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Vincent Daumas, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Rocheteau, Uzan-Sarano, avocat de la mutuelle assurance des instituteurs de France (MAIF) et à la SCP Odent, Poulet, avocat de la commune de Vaxoncourt ;<br/>
<br/>
<br/>
<br/>
<br/>Sur la régularité du jugement attaqué :<br/>
<br/>
              1. Il ressort des écritures soumises au tribunal administratif de Nancy que la mutuelle assurance des instituteurs de France (MAIF) s'est prévalue de la méconnaissance des dispositions du code de la construction et de l'habitation relatives aux établissements recevant du public au soutien de son moyen tiré de ce que la responsabilité de la commune de Vaxoncourt était engagée en raison du fonctionnement de l'ouvrage public que constituait, selon elle, le bûcher de la Saint-Jean. Le tribunal administratif qui a écarté la responsabilité de la commune sur ce terrain n'était pas tenu de répondre à l'ensemble de l'argumentation de la requérante. Par suite, le moyen tiré de ce que le jugement attaqué serait insuffisamment motivé, faute pour le tribunal administratif d'avoir statué sur ce fondement de responsabilité invoqué par la MAIF ne peut qu'être écarté. <br/>
<br/>
              Sur la responsabilité du fait d'une activité de service public :<br/>
<br/>
              2. Il ressort des pièces du dossier soumis aux juges du fond que la mise à feu du bûcher de la Saint-Jean est une manifestation de caractère traditionnel organisée à l'occasion de la kermesse annuelle des associations de la commune de Vaxoncourt. Cette manifestation pour laquelle la commune n'a versé aucune subvention et a seulement mis à disposition à titre gratuit un terrain communal, a été organisée le 30 juin 2007 par l'association " Foyer rural de Vaxoncourt " qui a souscrit, à ce titre, un contrat d'assurances avec la MAIF couvrant notamment les risques liés aux " feux de la Saint-Jean ". Si l'adjoint à la vie associative de la commune, au demeurant membre de l'association " Foyer rural de Vaxoncourt ", a participé aux réunions de préparation de cet événement, cette circonstance ne permet pas, à elle seule, d'établir que la manifestation aurait été organisée sous l'étroit contrôle de la commune. Par suite, le tribunal administratif n'a pas commis d'erreur de droit ni inexactement qualifié les faits de l'espèce en jugeant que la mise à feu du bûcher de la Saint-Jean ne présentait pas le caractère d'une activité de service public. <br/>
<br/>
              Sur la responsabilité du fait de la carence du maire dans l'exercice de ses pouvoirs de police :<br/>
<br/>
              3. Il ressort des pièces du dossier soumis aux juges du fond que la commune avait établi sur l'enceinte du terrain de sport communal un périmètre de sécurité interdisant l'accès du public au bûcher de la Saint-Jean dans un rayon de 40 mètres. Trois extincteurs étaient installés au niveau de la cour de l'école. Lors de la mise à feu du bûcher, deux pompiers volontaires étaient en position de sécurité à une dizaine de mètres du bûcher, munis d'une lance incendie de gros calibre branchée à un poteau incendie communal. Dans ces conditions, le tribunal administratif a pu, sans commettre d'erreur de droit ni inexactement qualifier les faits qu'il n'a pas dénaturés, écarter toute faute du maire dans l'usage de ses pouvoirs de police. <br/>
<br/>
              4. Les moyens tirés de ce que le tribunal administratif aurait dénaturé les faits et commis une erreur de droit en jugeant que n'était pas établi le lien de causalité entre le dommage et la carence alléguée de l'autorité de police, sont dirigés contre un motif surabondant du jugement et par suite sont en tout état de cause inopérants.<br/>
<br/>
              Sur la responsabilité du fait du fonctionnement d'un ouvrage public : <br/>
<br/>
              5. Il ressort des termes du jugement attaqué que pour écarter le fondement de responsabilité de la commune invoqué par la MAIF et tiré du défaut de conception ou d'entretien d'un ouvrage public, le tribunal administratif a jugé que le bûcher de la Saint-Jean n'avait le caractère ni d'une dépendance du domaine public, ni d'un ouvrage public, en dépit du fait qu'il avait été érigé sur le terrain de sport communal. En statuant ainsi, le tribunal administratif n'a pas commis d'erreur de qualification juridique des faits.<br/>
<br/>
              6. Il résulte de tout ce qui précède que la MAIF n'est pas fondée à demander l'annulation du jugement attaqué.<br/>
<br/>
              7. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la MAIF le versement d'une somme de 250 euros au titre de l'article L. 761-1 du code de justice administrative. En revanche, ces mêmes dispositions font obstacle à ce qu'une somme soit mise à la charge de la commune de Vaxoncourt qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de la MAIF est rejeté.  <br/>
<br/>
Article 2 : La MAIF versera la somme de 250 euros à la commune de Vaxoncourt au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
Article 3 : La présente décision sera notifiée à la mutuelle assurance des instituteurs de France et à la commune de Vaxoncourt.<br/>
Copie en sera adressée à Mlle B...A....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
