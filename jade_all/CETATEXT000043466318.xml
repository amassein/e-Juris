<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043466318</ID>
<ANCIEN_ID>JG_L_2021_04_000000451517</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/46/63/CETATEXT000043466318.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 23/04/2021, 451517, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-04-23</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>451517</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FABIANI, LUC-THALER, PINATEL</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2021:451517.20210423</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A... B... a demandé au juge des référés du tribunal administratif de Pau, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, en premier lieu de l'admettre à titre provisoire au bénéfice de l'aide juridictionnelle, en deuxième lieu d'enjoindre au préfet des Pyrénées-Atlantiques de suspendre l'exécution de la décision du 15 mars 2021 portant reconduite d'office a` la frontière et fixation du pays de destination jusqu'a` ce que la Cour nationale du droit d'asile statue définitivement sur sa demande d'asile et, en dernier lieu, d'enjoindre à ce préfet de mettre immédiatement fin a` sa rétention. Par une ordonnance n° 2100781 du 6 avril 2021, le juge des référés a admis M. Kelekci au bénéfice de l'aide juridictionnelle provisoire et rejeté le surplus de ses conclusions.<br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés les 8 et 16 avril 2021 au secrétariat du contentieux du Conseil d'Etat, M. Kelekci demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative:<br/>
<br/>
              1°) de l'admettre au bénéfice de l'aide juridictionnelle provisoire ; <br/>
<br/>
              2°) d'annuler l'ordonnance du 6 avril 2021 ;<br/>
<br/>
              3°) de faire droit à ses demandes ;<br/>
<br/>
              4°) de mettre à la charge de l'Etat la somme de 2 000 euros au titre des articles 37 de la loi n° 91-647 du 10 juillet 1991 et L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Il soutient que :<br/>
              - sa requête est recevable ;<br/>
              - le premier juge a entaché son ordonnance d'irrégularité dès lors qu'il a retenu qu'il n'appartenait pas au juge des référés, saisi sur le fondement de l'article L. 521- 2 du code de justice administrative, de priver d'effet l'ordonnance rendue par le juge judiciaire, le juge des référés étant pourtant compétent pour ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle l'administration porte une atteinte grave et manifestement illégale ;<br/>
              - il a commis une erreur de droit en jugeant que sa demande d'asile pouvait être qualifiée de frauduleuse, alors que, d'une part, si une enquête pénale a été ouverte a` l'encontre de deux agents de la préfecture auprès de laquelle il a enregistré sa demande d'asile, il n'est pas mis en cause dans cette procédure, que, d'autre part, la préfecture lui a remis une attestation de demande d'asile en procédure normale, enfin, que le seul fait qu'il présente une nouvelle demande d'asile en France après s'être vu opposer un rejet en Allemagne ne saurait permettre d'en établir le caractère frauduleux ; <br/>
              - il a commis une erreur de droit, aucune disposition législative ou règlementaire ne prévoyant la fin du droit au maintien sur le territoire d'un demandeur d'asile au seul motif que sa demande d'asile serait considérée comme frauduleuse ; <br/>
              - la condition d'urgence est satisfaite dès lors qu'il est placé en centre de rétention administrative depuis le 15 mars 2021 et que l'office français de protection des réfugiés et apatrides (OFPRA) ayant rejeté sa demande d'asile, la mesure d'éloignement dont il fait l'objet est exécutoire, l'administration effectuant actuellement les diligences nécessaires a` son éloignement vers la Turquie ;<br/>
              - il est porté atteinte à son droit d'asile, qui a pour corollaire le droit de se maintenir sur le territoire jusqu'a` ce qu'une décision définitive intervienne sur le bien-fondé de sa demande d'asile ; <br/>
              - il est porté atteinte à sa liberté d'aller et venir en raison de son placement en rétention par un arrêté dénué de base légale, la procédure de l'article L. 531-3 du code de l'entrée et du séjour des étrangers et du droit d'asile ne trouvant pas à s'appliquer dans ces circonstances. <br/>
<br/>
              Par un mémoire en défense, enregistré le 16 avril 2021, le ministre de l'intérieur conclut à ce que la requête de M. Kelekci soit rejetée pour défaut d'urgence. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, M. Kelekci et, d'autre part, le ministre de l'intérieur ;<br/>
<br/>
              Ont été entendus lors de l'audience publique du 19 avril 2021, à 14 heures : <br/>
<br/>
              - Me Pinatel, avocat au Conseil d'Etat et à la Cour de cassation, avocat de M. Kelekci ;<br/>
<br/>
              - les représentants du ministre de l'intérieur ;<br/>
<br/>
              à l'issue de laquelle le juge des référés a prononcé la clôture de l'instruction.  <br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Selon l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. (...) ". <br/>
<br/>
              2. Eu égard à son office, qui consiste à assurer la sauvegarde des libertés fondamentales, il appartient au juge des référés, saisi sur le fondement de l'article L. 521-2 du code de justice administrative, de prendre, en cas d'urgence, toutes les mesures qui sont de nature à remédier aux effets résultant d'une atteinte grave et manifestement illégale portée, par une autorité administrative, à une liberté fondamentale.<br/>
<br/>
              3. Cependant, il ressort des dispositions du III de l'article L. 512-1 du code de l'entrée et du séjour des étrangers et du droit d'asile que le législateur a entendu organiser une procédure spéciale afin que le juge administratif statue rapidement sur la légalité des mesures relatives à l'éloignement des étrangers, hors la décision refusant le séjour et les mesures d'expulsion, lorsque ces derniers sont placés en rétention ou assignés à résidence. Il appartient ainsi à l'étranger qui entend contester une mesure d'éloignement accompagnée d'un placement en rétention administrative de saisir le juge administratif sur le fondement des dispositions du III de l'article L. 512-1 d'une demande tendant à leur annulation. Cette procédure particulière est exclusive de celles prévues par le livre V du code de justice administrative. Il n'en va autrement que dans le cas où les modalités selon lesquelles il est procédé à l'exécution d'une telle mesure relative à l'éloignement forcé d'un étranger emportent des effets qui, en raison de changements dans les circonstances de droit ou de fait survenus depuis l'intervention de cette mesure et après que le juge, saisi sur le fondement de l'article L. 512-1, a statué ou que le délai prévu pour le saisir a expiré, excèdent ceux qui s'attachent normalement à sa mise à exécution. <br/>
<br/>
              4. Il résulte de l'instruction que M. Kelekci, ressortissant turc, a fait l'objet d'une décision du 15 mars 2021 du préfet des Pyrénées-Atlantiques le reconduisant d'office vers son pays d'origine ou vers un pays où il est légalement admissible, et a été placé en rétention au centre d'Hendaye pour une durée de quarante-huit heures. Le magistrat délégué par la présidente du tribunal administratif de Pau, saisi le 17 mars en application de l'article L. 512-1, a rejeté la requête de l'intéressé. Ce dernier a été maintenu en rétention, une première fois par une ordonnance du 17 mars 2021 du juge des libertés et de la détention du tribunal judiciaire de Bayonne, confirmée en appel par une ordonnance du 19 mars 2021 du président de la cour d'appel de Pau, puis, au-delà du délai de vingt-huit jours, une seconde fois par une ordonnance du même juge du 14 avril 2021.<br/>
<br/>
              5. L'article L. 531-3 du code de l'entrée et du séjour des étrangers et du droit d'asile prévoit que : " Lorsqu'un étranger non ressortissant d'un Etat membre de l'Union européenne a fait l'objet d'un signalement aux fins de non-admission en vertu d'une décision exécutoire prise par l'un des autres Etats parties à la convention signée à Schengen le 19 juin 1990 et qu'il se trouve irrégulièrement sur le territoire métropolitain, l'autorité administrative peut décider qu'il sera d'office reconduit à la frontière. / Il en est de même lorsqu'un étranger non ressortissant d'un Etat membre de l'Union européenne, qui se trouve en France, a fait l'objet d'une décision d'éloignement exécutoire prise par l'un des autres Etats membres de l'Union européenne ". Si, au regard des circonstances particulières de son dépôt à la préfecture des Bouches-du-Rhône, la demande d'asile introduite par M. Kelekci en procédure normale préalablement à son placement en rétention pouvait le cas échéant être requalifiée et traitée selon une autre procédure, elle ne pouvait être regardée comme inexistante, de sorte que l'intéressé disposait du droit au maintien sur le territoire métropolitain garanti par l'article L. 743-2 du code de l'entrée et du séjour des étrangers et du droit d'asile. Le ministre ne conteste pas ce point. Par suite, le préfet n'a pu légalement fonder sa décision du 15 mars 2021 sur l'article L. 531-3. <br/>
<br/>
              6. Cependant, d'une part, il résulte de la défense du ministre de l'intérieur et des engagements pris par ses représentants lors de l'audience que l'exécution de la décision du 15 mars 2021 reconduisant M. Kelekci d'office à destination du pays dont il a la nationalité ne sera pas exécutée avant que la Cour nationale du droit d'asile ait statué, d'une part, sur la demande d'avis qu'il a introduite sur le fondement de l'article L. 731-3 du code de l'entrée et du séjour des étrangers et du droit d'asile, et d'autre part, sur le recours qu'il a formé contre la décision par laquelle l'OFPRA a rejeté le 18 mars 2021 sa demande d'asile. Il revient à la seule Cour de statuer sur la contestation du bien-fondé de la procédure d'urgence, fondée sur l'article L. 556-1 du même code, choisie par l'OFPRA. Dans ces conditions, M. Kelekci bénéficie du droit au maintien sur le territoire français de nature à garantir l'instruction complète de sa demande d'asile. Ainsi, les effets de l'exécution de la mesure d'éloignement forcée litigieuse ne peuvent être regardés comment excédant ceux qui s'attachent normalement à un telle mise à exécution et portant une atteinte au droit d'asile justifiant que le juge des référés, saisi sur le fondement de l'article L. 521-2 du code de justice administrative, intervienne après que le juge compétent a statué selon la procédure particulière de l'article L. 512-1.<br/>
<br/>
              7. D'autre part, il revient au seul juge judiciaire de statuer sur le maintien du requérant en rétention et sur l'atteinte alléguée à sa liberté d'aller et venir, les effets de son placement initial décidé par le préfet le 15 mars 2021 ayant épuisé ses effets à compter de la première période de quarante-huit heures. <br/>
<br/>
              8. Il résulte de ce qui précède que M. Kelekci n'est pas fondé à demander l'annulation de l'ordonnance attaquée, ses conclusions présentées sur le fondement des articles 37 de la loi n° 91-647 du 10 juillet 1991 et L. 761-1 du code de justice administrative ne pouvant qu'être rejetées, sans qu'il y ait lieu de l'admettre au bénéfice de l'aide juridictionnelle provisoire. <br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
              ------------------<br/>
<br/>
Article 1er : La requête de M. Kelekci est rejetée. <br/>
Article 2 : La présente ordonnance sera notifiée à M. Latif Kelekci et au ministre de l'intérieur. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
