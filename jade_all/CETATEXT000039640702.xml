<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039640702</ID>
<ANCIEN_ID>JG_L_2019_12_000000419655</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/64/07/CETATEXT000039640702.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème chambre, 18/12/2019, 419655, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-12-18</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>419655</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BORE, SALVE DE BRUNETON, MEGRET</AVOCATS>
<RAPPORTEUR>M. Aurélien Caron</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emilie Bokdam-Tognetti</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2019:419655.20191218</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société à responsabilité limitée (SARL) Sylvie Brossard a demandé au tribunal administratif de Paris de prononcer la décharge de l'obligation de payer résultant de deux avis à tiers détenteur émis à son encontre par le comptable du service des impôts des entreprises de Paris 17ème Batignolles les 26 novembre et 16 décembre 2014 pour un montant total, en droits et majorations, de 26 509 euros en vue du paiement de la cotisation foncière des entreprises due au titre des années 2011 et 2012 et de la taxe sur la valeur ajoutée due au titre des mois de janvier et août 2013, d'ordonner la restitution d'une somme de 14 763 euros correspondant à la cotisation foncière des entreprises assortie du versement des intérêts légaux à compter de la date des avis à tiers détenteur et de constater la compensation de la taxe sur la valeur ajoutée réclamée avec les créances qu'elle détient sur le Trésor public. Par un jugement n° 1513143 du 8 mars 2017 le tribunal administratif de Paris a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 17PA01570 du 8 février 2018, la cour administrative d'appel de Paris a prononcé un non-lieu à statuer à hauteur de la somme de 14 763 euros et a rejeté le surplus des conclusions de la requête d'appel formée par la SARL Sylvie Brossard contre ce jugement.<br/>
<br/>
              Par un pourvoi, enregistré le 9 avril 2018 au secrétariat du contentieux du Conseil d'Etat, la SARL Sylvie Brossard demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la Constitution, notamment son Préambule et son article 61-1 ;<br/>
              - l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
              - le code général des impôts et le livre des procédures fiscales, notamment son article L. 257 B ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Aurélien Caron, Auditeur,  <br/>
<br/>
              - les conclusions de Mme Emilie Bokdam-Tognetti, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Boré, Salve de Bruneton, Mégret, avocat de la société Sylvie Brossard ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 822-1 du code de justice administrative : " Le pourvoi en cassation devant le Conseil d'Etat fait l'objet d'une procédure préalable d'admission. L'admission est refusée par décision juridictionnelle si le pourvoi est irrecevable ou n'est fondé sur aucun moyen sérieux ".<br/>
<br/>
              Sur la contestation du refus de transmission au Conseil d'Etat de la question prioritaire de constitutionnalité soulevée devant la cour administrative d'appel :<br/>
<br/>
              2. Les dispositions de l'article 23-2 de l'ordonnance portant loi organique du 7 novembre 1958 prévoient que, lorsqu'une juridiction relevant du Conseil d'Etat est saisie de moyens contestant la conformité d'une disposition législative aux droits et libertés garantis par la Constitution, elle transmet au Conseil d'Etat la question de constitutionnalité ainsi posée à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et qu'elle ne soit pas dépourvue de caractère sérieux.<br/>
<br/>
              3. L'article L. 257 B du livre des procédures fiscales dispose que " le comptable public compétent peut affecter au paiement des impôts, droits, taxes, pénalités ou intérêts de retard dus par un redevable les remboursements, dégrèvements ou restitutions d'impôts, droits, taxes, pénalités ou intérêts de retard constatés au bénéfice de celui-ci (...) ". La SARL Sylvie Brossard soutient que ces dispositions, en tant qu'elles font obstacle à ce que les contribuables puissent opposer à l'administration fiscale la compensation entre leurs dettes fiscales et les créances qu'ils détiennent sur le Trésor public, méconnaissent le principe de la garantie des droits qui découle de l'article 16 de la Déclaration des droits de l'homme et du citoyen de 1789, le droit de propriété garanti par les articles 2 et 17 de cette Déclaration ainsi que le principe d'égalité devant la loi garanti par l'article 6 de la même Déclaration.<br/>
<br/>
              4. Il ressort, toutefois, des pièces du dossier que le litige dont ont été saisis les juges du fond tend à la décharge de l'obligation de payer résultant de deux avis à tiers détenteur émis à l'encontre de la société par le comptable du service des impôts des entreprises de Paris 17ème Batignolles les 26 novembre et 16 décembre 2014 et que les dispositions précitées de l'article L. 257 B n'ont été ni appliquées par l'administration, ni invoquées par la société requérante à l'appui de son opposition à contrainte. En outre, la cour ne s'est pas fondée sur cet article pour écarter la requête de la société. Par suite, c'est sans erreur de droit que la cour a jugé que les dispositions de l'article L. 257 B n'étaient pas applicables au litige au sens et pour l'application de l'article 23-5 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel. <br/>
<br/>
              5. Par suite, la société requérante n'est pas fondée à contester le refus de transmission de sa question prioritaire de constitutionnalité qui lui a été opposé par la cour administrative d'appel de Paris.<br/>
<br/>
              Sur les autres moyens du pourvoi :<br/>
<br/>
              6. Pour demander l'annulation de l'arrêt qu'elle attaque, la SARL Sylvie Brossard soutient, en outre, que la cour administrative d'appel de Paris :<br/>
              - a commis une erreur de droit en écartant le moyen tiré de ce que le jugement du tribunal administratif était entaché d'une omission à statuer ;<br/>
              - a commis une erreur de droit en jugeant qu'elle ne pouvait utilement invoquer la compensation entre ses dettes et ses créances d'impôt et en déduisant qu'elle n'était pas fondée à demander la décharge de l'obligation de payer la taxe sur la valeur ajoutée résultant des avis à tiers détenteurs litigieux.<br/>
<br/>
              7. Aucun de ces moyens n'est de nature à permettre l'admission du pourvoi.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La contestation du refus de transmission de la question prioritaire de constitutionnalité portant sur les dispositions de l'article L. 257 B du livre des procédures fiscales est écartée.<br/>
Article 2 : Le surplus des conclusions du pourvoi de la SARL Sylvie Brossard n'est pas admis.<br/>
Article 3 : La présente décision sera notifiée à la SARL Sylvie Brossard.<br/>
Copie en sera adressée au Conseil constitutionnel, au Premier ministre et au ministre de l'action et des comptes publics. <br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
