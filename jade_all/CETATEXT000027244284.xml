<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027244284</ID>
<ANCIEN_ID>JG_L_2013_03_000000349933</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/24/42/CETATEXT000027244284.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  9ème sous-section jugeant seule, 28/03/2013, 349933, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-03-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>349933</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 9ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP COUTARD, MUNIER-APAIRE ; SCP FABIANI, LUC-THALER</AVOCATS>
<RAPPORTEUR>M. Matthieu Schlesinger</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Aladjidi</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2013:349933.20130328</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 7 juin et 7 septembre 2011 au secrétariat du contentieux du Conseil d'Etat, présentés pour Me Raphaël Petavy, agissant en qualité de mandataire judiciaire de la société Electric Industrie, dont le siège est Les Barbières BP 26 à Montfaucon-en-Velay (43290) ; Me Petavy demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 09LY01892 du 7 avril 2011 par lequel la cour administrative d'appel de Lyon a rejeté sa requête tendant à l'annulation du jugement n° 0800579 du 5 juin 2009 par lequel le tribunal administratif de Clermont-Ferrand a rejeté sa demande tendant à ce que la société Electricité de France (EDF) soit condamnée à verser à la société Electric Industrie la somme de 4 070 836 euros en réparation des conséquences dommageables de la méconnaissance par la société EDF de l'obligation d'achat d'énergie électrique mise à sa charge ainsi que les intérêts au taux légal à compter du 28 décembre 2007 ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de la société EDF la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la loi n° 46-628 du 8 avril 1946 ;<br/>
<br/>
              Vu le décret n° 55-662 du 20 mai 1955 ;<br/>
<br/>
              Vu le décret n° 65-29 du 11 janvier 1965 ;<br/>
<br/>
              Vu l'arrêté du 23 janvier 1995 du ministre de l'industrie, des postes et télécommunications et du commerce extérieur relatif à la suspension de l'obligation de passer des contrats d'achat pour la production autonome ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Matthieu Schlesinger, Auditeur,  <br/>
<br/>
              - les observations de la SCP Fabiani, Luc-Thaler, avocat de Me Petavy et de la SCP Coutard, Munier-Apaire, avocat de l'Electricité de France,<br/>
<br/>
              - les conclusions de M. Frédéric Aladjidi, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Fabiani, Luc-Thaler, avocat de Me Petavy et à la SCP Coutard, Munier-Apaire, avocat de l'Electricité de France ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que la société Electric Industrie, dont Me Petavy est le mandataire judiciaire, a pour activité la production d'énergie électrique ; que cette société a présenté, le 19 janvier 1994, à EDF une demande de raccordement au réseau et de contrat d'achat de cette énergie, en application de l'article 1er du décret du 20 mai 1955 réglant les rapports entre les établissements visés par les articles 2 et 23 de la loi du 8 avril 1946 et les producteurs autonomes d'énergie électrique, pour un projet de centrale thermique située sur le plateau de Saint-Sigolène en Haute-Loire ; que le Gouvernement, après avoir modifié, à l'article 1er du décret du 20 mai 1955, par un décret du 20 décembre 1994, les conditions de suspension de l'obligation d'achat, a publié le 23 janvier 1995 un arrêté de suspension pour trois ans, réservant les cas des projets les plus avancés ; que toutefois, par un courrier du 6 février 1995, EDF, après avoir constaté que le projet de la société Electric Industrie ne remplissait pas les conditions permettant de conserver le bénéfice de l'obligation d'achat, a informé cette société qu'elle ne poursuivrait pas l'instruction de sa demande ; que la société, estimant que le retard pris dans l'examen de son projet était la conséquence de carences fautives, a adressé à EDF, le 27 décembre 2007, une demande préalable d'indemnisation de son préjudice subi à raison de l'abandon du projet et de la perte des gains que ce dernier lui apporterait ; qu'EDF ayant rejeté cette demande, la société a saisi le tribunal administratif de Clermont-Ferrand qui a rejeté sa demande d'indemnisation par un jugement du 5 juin 2009 ; que la société se pourvoit en cassation contre l'arrêt de la cour administrative d'appel de Lyon du 7 avril 2011 qui a confirmé ce rejet ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article 8 de la loi du 8 avril 1946 : " 5° Les aménagements de production d'énergie de tout établissement, entreprise ou de tout particulier, lorsque la puissance installée des appareils de production n'excède pas 8000 kVA (puissance maximum des machines tournantes susceptibles de marcher simultanément). Il ne sera pas tenu compte, pour le calcul de la puissance installée, des installations de récupération d'énergie résiduaire visées au paragraphe 4° précédent. " ; qu'aux termes du premier alinéa de l'article 1er du décret du 20 mai 1955 dans sa rédaction en vigueur à la date de la demande : " Electricité de France sera tenue de recevoir sur les réseaux qu'elle exploite, sous réserve qu'il n'en résulte aucune entrave au bon fonctionnement de la distribution, et ce dans les conditions fixées ci-après, l'énergie produite dans les installations visées aux 3e alinéa (paragraphes 3 à 6), et 4e alinéa de l'article 8 de la loi du 8 avril 1946 modifiée par la loi du 2 août 1949. Elle sera tenue également de passer un contrat pour l'achat de l'énergie produite dans ces installations. " ; qu'aux termes du 3e alinéa de l'article 1er du décret du 20 mai 1955, duquel sont issues les dispositions du décret du 20 décembre 1994 : " L'obligation de passer un contrat d'achat peut être suspendue, par arrêté du ministre chargé de l'énergie, pour une durée déterminée, sur tout ou partie du territoire national, pour l'ensemble des installations de production ou pour celles répondant à une demande d'électricité de caractéristiques définies (base, semi-base ou pointe), lorsqu'il est constaté que les moyens de production existants sont suffisants pour faire face, à tout instant, à la demande correspondante dans des conditions économiques satisfaisantes de production, transport et distribution. " ;<br/>
<br/>
              3. Considérant que si l'instruction des demandes présentées au titre de l'article 1er du décret du 20 mai 1955 n'est pas enserrée dans un délai fixé par des textes, EDF est néanmoins tenu de les examiner dans un délai raisonnable ; qu'un retard dans l'instruction de la demande est susceptible de constituer une faute engageant la responsabilité du concessionnaire public ; qu'ainsi, la cour, en se bornant à relever qu'aucun délai n'était imparti par un texte au concessionnaire public pour juger que la société ne pouvait soutenir qu'EDF avait commis une faute en ne statuant pas sur sa demande avant l'entrée en vigueur de la suspension de l'obligation d'achat, sans rechercher si EDF avait instruit le dossier dans un délai raisonnable, a entaché son arrêt d'une erreur de droit ; que, par suite, et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, son arrêt doit être annulé ;<br/>
<br/>
              4. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la société EDF la somme de 3 000 euros à verser à la société Electric Industrie, au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : L'arrêt du 7 avril 2011 de la cour administrative d'appel de Lyon est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Lyon.<br/>
Article 3 : La société EDF versera à la société Electric Industrie, représentée par Me Petavy, une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à Me Raphaël Petavy et à la société EDF.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
