<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038860074</ID>
<ANCIEN_ID>JG_L_2019_07_000000422741</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/86/00/CETATEXT000038860074.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère chambre, 31/07/2019, 422741, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-07-31</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>422741</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROUSSEAU, TAPIE</AVOCATS>
<RAPPORTEUR>M. Arnaud Skzryerbak</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Rémi Decout-Paolini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2019:422741.20190731</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme A...B...a demandé au tribunal administratif de Montpellier :<br/>
              - d'annuler la décision de la caisse d'allocations familiales de l'Hérault du 17 novembre 2015 de récupérer un indu de revenu de solidarité active de 1 298,96 euros après compensation et retenue ainsi que la décision de la même autorité du 14 janvier 2016 recalculant ses droits ;<br/>
              - d'annuler la décision du 30 mai 2016 par laquelle le président du conseil départemental de l'Hérault, saisi d'un recours administratif préalable, lui a accordé à titre gracieux une remise de dette d'un montant de 365,69 euros et a confirmé l'indu pour le surplus ;<br/>
              - d'ordonner le reversement des sommes prélevées sur ses allocations, avec les intérêts et la capitalisation des intérêts. <br/>
<br/>
              Par un jugement n° 1603731 du 31 mai 2018, le tribunal administratif de Montpellier a rejeté sa demande.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire rectificatif et un mémoire complémentaire, enregistrés les 31 juillet, 30 août et 31 octobre 2018 au secrétariat du contentieux du Conseil d'Etat, Mme B...demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) de mettre à la charge du département de l'Hérault la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de l'action sociale et des familles ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Arnaud Skzryerbak, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Rémi Decout-Paolini, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Rousseau, Tapie, avocat de Mme B...;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis au juge du fond qu'à la suite d'un contrôle de la situation de Mme A...B..., la caisse d'allocations familiales de l'Hérault a décidé le 17 novembre 2015 de récupérer à son encontre un indu de revenu de solidarité active et d'allocation de logement familiale puis a recalculé les droits de l'intéressée le 14 janvier 2016. Saisi d'un recours administratif préalable à l'encontre de la décision de récupération de l'indu de revenu de solidarité active, portant sur la période du 1er janvier au 31 octobre 2015, le président du conseil départemental de l'Hérault a accordé le 30 mai 2016 à Mme B...une remise de dette d'un montant de 365,69 euros et confirmé pour le surplus la récupération de l'indu, dont le solde s'élevait à 853,27 euros. Par un jugement du 31 mai 2018 contre lequel Mme B...se pourvoit en cassation, le tribunal administratif de Montpellier a rejeté sa demande tendant à l'annulation des décisions du 17 novembre 2015 et des 14 janvier et 30 mai 2016 en tant qu'elles portent sur le revenu de solidarité active.<br/>
<br/>
              Sur le jugement attaqué, en tant qu'il statue sur les décisions des 17 novembre 2015 et 14 janvier 2016 :<br/>
<br/>
              2. Aux termes du premier alinéa de l'article L. 262-47 du code de l'action sociale et des familles : " Toute réclamation dirigée contre une décision relative au revenu de solidarité active fait l'objet, préalablement à l'exercice d'un recours contentieux, d'un recours administratif auprès du président du conseil départemental (...) ". Aux termes du premier alinéa de l'article R. 262-88 du même code : " Le recours administratif préalable mentionné à l'article L. 262-47 est adressé par le bénéficiaire au président du conseil départemental dans un délai de deux mois à compter de la notification de la décision contestée (...) ". L'institution par ces dispositions d'un recours administratif, préalable obligatoire à la saisine du juge, a pour effet de laisser à l'autorité compétente pour en connaître le soin d'arrêter définitivement la position de l'administration. Il s'ensuit que la décision prise à la suite du recours se substitue en principe à la décision initiale, et qu'elle est seule susceptible d'être déférée au juge. Tel est le cas, en particulier, d'une décision de récupération d'indu, dont l'annulation implique, s'il y a lieu, que l'administration rembourse la somme déjà recouvrée, sauf à régulariser sa décision de récupération si celle-ci n'a été annulée que pour un vice de forme ou de procédure.<br/>
<br/>
              3. La décision du 30 mai 2016 par laquelle le président du conseil départemental de l'Hérault a accordé à Mme B...une remise de dette partielle et confirmé, pour le surplus, la récupération de l'indu de revenu de solidarité active pour la période du 1er janvier au 31 octobre 2015 s'est substituée aux décisions de la caisse d'allocations familiales de l'Hérault des 17 novembre 2015 et 14 janvier 2016 en tant qu'elles concernent la récupération de cet indu. Elle était, par suite, seule susceptible d'être déférée au juge et le tribunal n'a pas commis d'erreur de droit en rejetant comme irrecevables les conclusions de Mme B...dirigées contre les décisions des 17 novembre 2015 et 14 janvier 2016.  <br/>
<br/>
              Sur le jugement attaqué, en tant qu'il statue sur la décision du 30 mai 2016 et sur les conclusions accessoires de la requête :<br/>
<br/>
              4. A l'appui de sa demande de décharge de l'indu de revenu de solidarité active qui lui était réclamé, Mme B...soutenait notamment qu'elle avait toujours régulièrement déclaré sa situation financière et que le département de l'Hérault ne justifiait ni des sommes réintégrées dans ses ressources au titre de salaires prétendument non déclarés ni des calculs opérés pour en tirer les conséquences sur son droit au revenu de solidarité active. Le tribunal a rejeté sa demande sans se prononcer sur ce moyen, qui n'était pas inopérant. Par, suite Mme B... est fondée à soutenir qu'il a insuffisamment motivé son jugement.<br/>
<br/>
              5. Il résulte de ce qui précède que le jugement attaqué doit être annulé en tant qu'il statue sur les conclusions de la demande de Mme B...dirigées contre la décision du 30 mai 2016 ainsi que sur ses conclusions à fin d'injonction et ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative. Le moyen retenu suffisant à entraîner cette annulation, il n'est pas nécessaire de se prononcer sur les autres moyens du pourvoi ayant la même portée.<br/>
<br/>
              Sur les frais liés au litige :<br/>
<br/>
              6. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge du département de l'Hérault le versement à Mme B...d'une somme de 1 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du tribunal administratif de Montpellier du 31 mai 2018 est annulé en tant qu'il statue sur les conclusions de la demande de Mme B...dirigées contre la décision du 30 mai 2016 ainsi que sur ses conclusions à fin d'injonction et ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 2 : L'affaire est renvoyée au tribunal administratif de Montpellier dans la mesure de la cassation prononcée.<br/>
Article 3 : Le département de l'Hérault versera à Mme B...une somme de 1 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Le surplus des conclusions du pourvoi est rejeté.<br/>
Article 5 : La présente décision sera notifiée à Mme A...B...et au département de l'Hérault.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
