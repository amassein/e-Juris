<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026010977</ID>
<ANCIEN_ID>J1_L_2012_06_000001104175</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/01/09/CETATEXT000026010977.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Cour administrative d'appel de Paris, 3 ème chambre , 07/06/2012, 11PA04175, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2012-06-07</DATE_DEC>
<JURIDICTION>Cour administrative d'appel de Paris</JURIDICTION>
<NUMERO>11PA04175</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3 ème chambre </FORMATION>
<TYPE_REC>plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>Mme FOLSCHEID</PRESIDENT>
<AVOCATS>CERVERA-KHELIFI</AVOCATS>
<RAPPORTEUR>Mme Mathilde  RENAUDIN</RAPPORTEUR>
<COMMISSAIRE_GVT>M. JARRIGE</COMMISSAIRE_GVT>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 16 septembre 2011, présentée pour Mlle Sonia A, demeurant ..., par Me Cervera-Khelifi ; Mlle A demande à la Cour :<br/>
<br/>
       1°) de confirmer le jugement n° 0909071/6-1 du 15 juillet 2011 du Tribunal administratif de Paris en ce qu'il a retenu la responsabilité du centre hospitalier Sainte-Anne dans sa prise en charge défectueuse à compter de l'intervention chirurgicale qu'elle y a subie le 21 janvier 2003 et de le réformer en ce qu'il n'a fait que partiellement droit à ses demandes indemnitaires s'agissant des préjudices qui n'ont pas été indemnisés par l'office national d'indemnisation des accidents médicaux, des affections iatrogènes et des infections nosocomiales (ONIAM) ;<br/>
<br/>
       2°) en conséquence, de condamner solidairement le centre hospitalier Sainte-Anne et son assureur, la société hospitalière d'assurances mutuelles (SHAM), à lui verser la somme de 929 629, 70 euros, assortie des intérêts au taux légal à compter du 13 mars 2009, date de sa demande préalable, en réparation de ses préjudices ;<br/>
<br/>
       3°) de mettre à la charge du centre hospitalier Sainte-Anne et de la SHAM une somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
....................................................................................................................<br/>
<br/>
       Vu les autres pièces du dossier ; <br/>
<br/>
       Vu le code de la famille et de l'aide sociale ;<br/>
<br/>
       Vu le code de la sécurité sociale ;<br/>
<br/>
       Vu le code de la santé publique ;<br/>
<br/>
       Vu le code de justice administrative ;<br/>
<br/>
       Les parties ayant été régulièrement averties du jour de l'audience ;<br/>
<br/>
       Après avoir entendu au cours de l'audience publique du 10 mai 2012 :<br/>
<br/>
       - le rapport de Mme Renaudin, rapporteur,<br/>
<br/>
       - les conclusions de M. Jarrige, rapporteur public,<br/>
<br/>
      - et les observations de Me Cervera-Khelifi, pour Mlle A ;<br/>
<br/>
       Considérant que Mlle A, née en 1983, était atteinte depuis son enfance d'une hypertension intracrânienne qui a été traitée à l'hôpital des Enfants malades par la mise en place, en décembre 1996, d'une dérivation lombo-péritonéale, laquelle faisait l'objet de révisions régulières lors des à coups d'hyperpression intracrânienne ressenties par la patiente ; qu'au début de l'année 2003, connaissant à nouveau des troubles de types céphalées, raideur de la nuque et nausées, Mlle A a été hospitalisée dans le service de neurologie du centre hospitalier Sainte-Anne où elle était désormais prise en charge, ne pouvant plus l'être, en raison de son âge, à l'hôpital des Enfants malades ; que l'équipe médicale a alors posé le diagnostic d'hypotension intracrânienne et a réalisé le 21 janvier 2003 une intervention en vue de l'ablation de la dérivation lombo-péritonéale qu'elle portait ; qu'il résulte de l'instruction que dans les suites immédiates de l'intervention sont apparues d'importantes céphalées, suivies d'une baisse de l'acuité visuelle ; que la pose d'une nouvelle dérivation lombo-péritonéale ne sera réalisée que le 11 mars 2003, les gestes médicaux pratiqués jusqu'alors pour drainer le liquide céphalo-rachidien, soit des ponctions lombaires répétées et la pose d'une valve de dérivation ventriculo-péritonéale le 5 février 2003, étant insuffisants ; que du fait de ce retard thérapeutique Mlle A demeure atteinte d'une cécité totale par destruction des nerfs optiques due à l'hyperpression intracrânienne et de troubles de la sensibilité au niveau de son bras droit ; qu'elle a saisi la commission régionale de conciliation et d'indemnisation des accidents médicaux (CRCI) d'Ile-de-France, qui, par avis du 14 décembre 2005, a conclu que la réparation de ses préjudices incombait au centre hospitalier Sainte-Anne du fait des fautes commises par l'équipe médicale ; qu'estimant n'avoir pas reçu de proposition indemnitaire de la part de l'assureur de l'établissement de santé dans le délai de quatre mois à compter de la réception de l'avis de la CRCI, Mlle A a saisi l'ONIAM d'une demande de substitution sur le fondement de l'article L. 1142-15 du code de la santé publique ; que l'ONIAM lui a adressé une offre transactionnelle partielle d'un montant de 440 714 euros, portant sur ses postes de préjudices à caractère extra-patrimonial ainsi que sur celui tenant à la perte d'une année scolaire, acceptée par la signature d'un protocole d'accord le 24 avril 2007 et réglée le 15 mai 2007 ; que Mlle A n'a, en revanche, pas donné suite à l'offre complémentaire d'un montant de 242 356, 37 euros, que l'ONIAM lui a proposée le 29 avril 2008, en réparation du reste de ses préjudices à caractère patrimonial ; qu'après avoir obtenu en référé la condamnation du centre hospitalier Sainte-Anne à lui verser une indemnité provisionnelle d'un montant de 150 000 euros par ordonnance en date du 12 mars 2010 du Tribunal administratif de Paris, Mlle A, par une requête au fond, a contesté le montant de l'offre complémentaire proposée par l'ONIAM et demandé la condamnation solidaire du centre hospitalier Sainte-Anne et de son assureur, la société hospitalière d'assurances mutuelles (SHAM), à lui verser la somme de 1 110 489, 33 euros, assortie des intérêts au taux légal à compter du 13 mars 2009, en réparation de ses préjudices ; que par jugement du 15 juillet 2011, le Tribunal administratif de Paris a retenu la responsabilité du centre hospitalier Sainte-Anne, à raison, d'une part, d'une erreur dans le diagnostic porté d'hypotension intracrânienne, Mlle A souffrant d'une hypertension intracrânienne correctement traitée jusqu'alors par une dérivation lombo-péritonéale, d'autre part, d'une erreur dans l'indication de l'intervention chirurgicale consistant dans le retrait de la dérivation lombo-péritonéale et effectuée le 21 janvier 2003, et enfin d'une défaillance dans le suivi postopératoire, les gestes médicaux qui s'imposaient devant les symptômes qu'elle présentait à la suite de l'intervention n'ayant pas été réalisés ou trop tardivement pour éviter les séquelles survenues ; que les premiers juges ont estimé que Mlle A avait perdu toute chance de se soustraire aux dommages intervenus et ont donc condamné solidairement le centre hospitalier Sainte-Anne et la SHAM à réparer l'intégralité de ses préjudices ; que s'agissant des préjudices indemnisés par l'ONIAM en vertu du protocole d'accord intervenu le 24 avril 2007 pour un montant total de 440 714 euros, les premiers juges ont rejeté la demande d'indemnisation de la perte d'une année scolaire ; qu'ils ont évalué les troubles de toute nature dans les conditions d'existence subis par l'intéressée à la somme de 409 514 euros, son pretium doloris à celle de 12 000 euros et son préjudice esthétique à 7 000 euros ; que l'ONIAM ayant de plus justifié du paiement, pour un montant de 600 euros, de frais relatifs à l'expertise ordonnée par la CRCI, les premiers juges ont considéré que l'ONIAM devait se voir allouer, en sa qualité de subrogé dans les droits de la victime, la somme de 429 114 euros mise à la charge du centre hospitalier Sainte-Anne ; que, s'agissant des préjudices patrimoniaux subis par l'intéressée non indemnisés par l'ONIAM, ils ont mis à la charge solidairement du centre hospitalier Sainte-Anne et de la SHAM une somme de 196 600 euros ; que Mlle A relève régulièrement appel du jugement du 15 juillet 2011 du Tribunal administratif de Paris en ce qu'il n'a fait que partiellement droit à ses demandes indemnitaires s'agissant de ces derniers préjudices, pour lesquels elle demande l'octroi d'une somme totale de 929 629, 70 euros ;<br/>
<br/>
       Sur les conclusions en indemnisation dirigées contre l'assureur du centre hospitalier Sainte-Anne :<br/>
<br/>
       Considérant que l'action ouverte à la victime d'un accident par l'article L. 121-12 du code des assurances contre l'assureur de l'auteur responsable du dommage est distincte de son action en responsabilité envers ce dernier ; que si ces deux actions sont fondées l'une et l'autre sur le droit de la victime à la réparation du préjudice qu'elle a subi, l'action directe ne poursuit que l'obligation de l'assureur à cette réparation, laquelle est une obligation de droit privé ; qu'il s'ensuit qu'elle relève des tribunaux judiciaires ; que, dès lors, les conclusions de Mlle A dirigées contre la SHAM, assureur du centre hospitalier Sainte-Anne, sont portées devant une juridiction incompétente pour en connaître ; que, par suite, c'est à tort que le Tribunal administratif de Paris n'a pas rejeté comme portées devant une juridiction incompétente pour en connaître les conclusions de Mlle A dirigées contre la SHAM et a condamné cette dernière à verser, solidairement avec le centre hospitalier Sainte-Anne, la somme de 196 600 euros à Mlle A ; que le jugement attaqué doit, dans cette mesure, être annulé ;<br/>
<br/>
       Considérant qu'il y a lieu d'évoquer l'affaire sur ce point et, pour le motif exposé ci-dessus, de rejeter comme portées devant une juridiction incompétente pour en connaître les conclusions de la demande de Mlle A dirigées contre la SHAM ;<br/>
<br/>
       Sur les conclusions en indemnisation dirigées contre le centre hospitalier <br/>
Sainte-Anne s'agissant des préjudices de Mlle A n'ayant pas été indemnisés par l'ONIAM :<br/>
<br/>
       En ce qui concerne les frais liés au handicap :<br/>
<br/>
       Considérant que si Mlle A fait valoir qu'elle justifie au dossier de frais d'acquisition du matériel adapté à son état de santé, notamment de logiciels informatique de braille, elle produit un ensemble de devis de différentes sociétés, se recoupant dans l'offre de matériel, qui ne permettent pas d'identifier les dépenses qu'elle a réellement exposées ; que, dans ces conditions et compte tenu des éléments partiels d'information qui y figurent, elle n'est pas fondée à soutenir que les premiers juges ont fait une évaluation insuffisante de l'indemnité qui lui est due au titre de ces équipements en la fixant à la somme de 100 000 euros pour les dépenses passées et futures afférentes à l'achat et au renouvellement du matériel lié à son handicap, compte tenu de sa durée d'utilisation, laquelle au demeurant ne peut être limitée à trois ans comme le prétend la requérante ;<br/>
<br/>
       Considérant que Mlle A peut prétendre, compte tenu de l'important déficit fonctionnel permanent dont elle est atteinte fixé à 85% par l'expert, à l'indemnisation des frais d'entretien d'un chien d'aveugle comme l'ont jugé à bon droit les premiers juges, nonobstant les conclusions de l'expert sur le caractère éventuel de cette aide ; que Mlle A justifie au dossier de frais exposés pour l'entretien d'un chien qu'elle a acquis au cours de l'année 2006 et s'élevant à la somme de 870, 83 euros au titre de cette même année ; qu'il y a lieu, compte tenu des éléments versés du dossier, de retenir pour l'entretien de ce chien un montant annuel de 1 000 euros, prenant en compte les dépenses de nourriture, de vétérinaire, d'assurances et de petit matériel, auquel il convient d'appliquer le barème de capitalisation de 2011 publié à la Gazette du Palais ; que les frais futurs d'entretien d'un chien d'aveugle s'élèvent par conséquent à la somme de 32 556 euros ; qu'il y a lieu de réformer le jugement attaqué en ce qu'il a évalué à la somme de 20 000 euros les dépenses passées et à venir nécessaires à l'entretien du chien et de porter ladite somme à 33 426, 83 euros ; <br/>
<br/>
       Considérant qu'il résulte de l'instruction et en particulier du rapport d'expertise que, comme l'ont estimé à bon droit les premiers juges, si l'état de santé de Mlle A a nécessité en 2003 l'aide d'une tierce personne durant trois heures par jour, puis en 2004 pendant quatre heures par semaine, à compter de 2005, soit à la date de l'expertise où l'état de l'intéressée est considéré comme consolidé, cet état ne justifie plus qu'un besoin résiduel d'une heure par jour tous les deux jours pour permettre à Mlle A une adaptation à sa vie quotidienne, selon les termes de l'expert ; que les premiers juges n'ont pas fait, contrairement à ce que soutient la requérante, une insuffisante appréciation du préjudice qu'elle a subi au titre de l'assistance d'une tierce personne, compte tenu du taux horaire fixé par la convention collective des aides à domicile et de l'application d'un barème de capitalisation, en l'évaluant à la somme de 95 000 euros ;<br/>
<br/>
       Considérant que si Mlle A soutient que c'est à tort que les premiers juges ont imputé sur cette somme celles qu'elle perçoit du conseil général au titre de l'allocation de compensation qui lui a été octroyée pour l'assistance d'une tierce personne, il appartient au juge, nonobstant la circonstance que la prestation en cause n'ouvre pas droit à un recours contre la personne tenue à réparation aux termes de la loi n° 85-677 du 5 juillet 1985 tendant à l'amélioration de la situation des victimes d'accidents de la circulation et à l'accélération des procédures d'indemnisation, de prendre les mesures nécessaires pour éviter une double indemnisation de la victime ; que compte tenu de ce qu'il résultait de l'instruction que Mlle A perçoit, depuis le 8 avril 2008, une allocation compensatrice pour l'aide d'une tierce personne dont l'objet est le même que l'indemnisation qui lui a été octroyée par le jugement attaqué, c'est donc à bon droit que les premiers juges ont imputé le montant de cette allocation sur l'indemnisation du préjudice de Mlle A lié à la nécessité de l'assistance d'une tierce personne ; qu'en l'absence de déclaration par la requérante dans ses écritures des sommes perçues, au titre de l'allocation de compensation, les premiers juges ont fait une juste appréciation du montant capitalisé de cette prestation, en l'évaluant à 250 000 euros ; qu'ils ont apprécié à juste titre la part de l'indemnisation allouée à Mlle A pour la période à compter de laquelle elle a perçu l'allocation de compensation, soit à partir d'avril 2008, en l'évaluant à 70 000 euros sur les 95 000 euros qui lui ont été octroyés au total ; que par conséquent, ils ont jugé à bon droit que son préjudice d'assistance d'une tierce personne à hauteur de 70 000 euros se trouvait intégralement compensé par l'allocation de compensation, seule restant à sa charge la somme de 25 000 euros au titre des frais d'assistance d'une tierce personne pour la période antérieure à avril 2008 ;<br/>
<br/>
       Considérant que si Mlle A soutient que les études d'histoire qu'elle poursuit sont impossibles sans l'assistance d'une lectrice, elle ne l'établit par aucune pièce au dossier ; que c'est donc à bon droit que les premiers juges ont rejeté ce chef de préjudice ;<br/>
<br/>
       En ce qui concerne l'incidence professionnelle :<br/>
<br/>
       Considérant qu'il résulte de l'instruction, en particulier du rapport d'expertise, que la cécité de la requérante réduit le nombre de métiers auxquels elle est susceptible de prétendre et fait obstacle à ce qu'elle envisage d'être journaliste comme elle le souhaitait ; que, compte tenu de son jeune âge et par conséquent de l'incidence de son état de santé sur l'ensemble de sa vie professionnelle, les premiers juges ont fait une juste appréciation de ce préjudice en l'évaluant à 50 000 euros ;<br/>
<br/>
       En ce qui concerne l'assistance d'un médecin-conseil :<br/>
<br/>
       Considérant que Mlle A ayant justifié avoir engagé des frais d'un montant de 1 600 euros pour être assistée par un médecin-conseil dans le présent litige, c'est à bon droit que les premiers juges ont indemnisé ce chef de préjudice ;<br/>
<br/>
       Considérant qu'il résulte de ce qui précède que les premiers juges ont fait une insuffisante appréciation du préjudice patrimonial de Mlle A non indemnisé par l'ONIAM en le fixant à la somme de 196 600 euros, somme assortie des intérêts au taux légal à compter du 13 mars 2009 ; que cette somme doit être portée à 210 026, 83 euros et être assortie des mêmes intérêts ; que Mlle A est fondée à demander la réformation du jugement attaqué dans cette mesure ; qu'en revanche le centre hospitalier Sainte-Anne n'est pas fondé à demander, par la voie de l'appel incident, que les indemnités octroyées à la requérante soient ramenées à de plus justes proportions ;<br/>
<br/>
       Sur les conclusions tendant à l'application de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
       Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge du centre hospitalier Sainte-Anne la somme de 2 000 euros au titre des frais exposés par Mlle A et non compris dans les dépens ;<br/>
<br/>
<br/>
       D E C I D E :<br/>
<br/>
Article 1er : Le jugement du 15 juillet 2011 du Tribunal administratif de Paris est annulé en ce qu'il a, par son article 1er, condamné la SHAM solidairement avec le centre hospitalier <br/>
Sainte-Anne à verser à Mlle A la somme de 196 000 euros, augmentée des intérêts au taux légal à compter du 13 mars 2009.<br/>
<br/>
Article 2 : Les conclusions de Mlle A présentées devant le Tribunal administratif de Paris et devant la Cour dirigées contre la SHAM sont rejetées comme portées devant une juridiction incompétente pour en connaître.<br/>
<br/>
Article 3 : La somme de 196 000 euros augmentée des intérêts au taux légal à compter du 13 mars 2009, que le centre hospitalier Sainte-Anne a été condamné à verser à Mlle A sous déduction des provisions déjà versées, par l'article 1er du jugement susvisé du Tribunal administratif de Paris, est portée à 210 026, 83 euros, assortie des mêmes intérêts.<br/>
<br/>
Article 4 : Le jugement du 15 juillet 2011 susmentionné du Tribunal administratif de Paris est réformé en ce qu'il est contraire au présent arrêt.<br/>
<br/>
Article 5 : Le centre hospitalier Sainte-Anne versera la somme de 2 000 euros à Mlle A au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 6 : Le surplus de la requête de Mlle A est rejeté.<br/>
''<br/>
''<br/>
''<br/>
''<br/>
5<br/>
N° 10PA03855<br/>
2<br/>
N° 11PA04175<br/>
<br/>
<br/>
<br/>
</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
