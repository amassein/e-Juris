<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030509793</ID>
<ANCIEN_ID>JG_L_2015_04_000000371671</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/50/97/CETATEXT000030509793.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème / 1ère SSR, 17/04/2015, 371671, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-04-17</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>371671</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème / 1ère SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP WAQUET, FARGE, HAZAN</AVOCATS>
<RAPPORTEUR>Mme Clémence Olsina</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Suzanne von Coester</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:371671.20150417</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 27 août et 27 novembre 2013 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. B...A..., demeurant ... ; M. A...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 12PA03927 du 27 juin 2013 par lequel la cour administrative d'appel de Paris a rejeté sa requête tendant à l'annulation du jugement nos 0908235/6, 0908237/6 du 21 juin 2012 par lequel le tribunal administratif de Melun a rejeté ses demandes tendant, en premier lieu, à l'annulation de la décision du 4 décembre 2007 par laquelle le procureur de la République près le tribunal de grande instance de Créteil et le secrétaire général du même tribunal ont mis fin à ses fonctions d'expert-traducteur, en deuxième lieu, à l'annulation de la décision du 23 octobre 2009 par laquelle le garde des sceaux, ministre de la justice, a rejeté sa demande d'indemnisation à raison des préjudices résultant de la décision du 4 décembre 2007 et de la conservation de ses fichiers de travail, et, en troisième lieu, à la condamnation de l'Etat à lui verser la somme de 343 442 euros en réparation des préjudices subis ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 600 euros au titre de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de procédure pénale ; <br/>
<br/>
              Vu le décret n° 86-83 du 17 janvier 1986 ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Clémence Olsina, auditeur,  <br/>
<br/>
              - les conclusions de Mme Suzanne von Coester, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Waquet, Farge, Hazan, avocat de M. A...;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond qu'à compter de 1988, M.A..., inscrit sur la liste des experts près la cour d'appel de Paris en qualité de traducteur-interprète en langues anglaise et arabe, a assuré, auprès du tribunal de grande instance de Créteil, des permanences d'interprétariat ; que, par une convention du 1er février 2002 conclue avec le vice-président du tribunal de grande instance de Créteil et le procureur de la République près ce tribunal, M. A...s'est vu confier l'organisation d'un service de permanence d'interprétariat au sein de la juridiction ; que, le 4 décembre 2007, M.A..., récemment titularisé en tant que professeur d'anglais, a été informé par le procureur de la République et le secrétaire général du tribunal qu'il était mis un terme immédiat à ses fonctions ; que, par un courrier du 9 juillet 2009, M. A...a demandé au garde des sceaux, ministre de la justice, de lui allouer la somme de 343 442 euros en réparation des préjudices qu'il estime avoir subis du fait de la décision fautive de l'Etat de le priver de la possibilité de poursuivre son activité d'interprète et de conserver ses fichiers de traduction ; que, par un jugement du 21 juin 2012, le tribunal administratif de Melun a rejeté sa demande tendant, en premier lieu, à l'annulation de la décision du 4 décembre 2007, en deuxième lieu, à l'annulation de la décision implicite de rejet résultant du silence gardé par le garde des sceaux, ministre de la justice, sur sa demande d'indemnisation, et, en troisième lieu, à la condamnation de l'Etat à lui verser la somme demandée ; que M. A...se pourvoit en cassation contre l'arrêt du 27 juin 2013 par lequel la cour administrative d'appel de Paris a rejeté son appel ; <br/>
<br/>
              2. Considérant qu'en vertu du 1er alinéa de l'article 1er du décret du 17 janvier 1986 relatif aux dispositions générales applicables aux agents contractuels de l'Etat pris pour l'application de l'article 7 de la loi du 11 janvier 1984 portant dispositions statutaires relatives à la fonction publique de l'Etat, dans sa version alors applicable : " Les dispositions du présent décret s'appliquent aux agents non titulaires de droit public de l'Etat et de ses établissements publics à caractère administratif ou à caractère scientifique, culturel et professionnel, recrutés ou employés dans les conditions définies aux articles 3 (2e, 3e et 6e alinéa), 4, 5, 6, et 82 de la loi du 11 janvier 1984 susvisée et au I de l'article 34 de la loi n° 2000-321 du 12 avril 2000 relative aux droits des citoyens dans leurs relations avec les administrations, à l'exception des agents en service à l'étranger et des agents engagés pour exécuter un acte déterminé " ; qu'un agent de droit public employé par l'Etat ne peut pas prétendre au bénéfice des dispositions prévues par ce décret en faveur de ses agents non titulaires mais doit être regardé comme ayant été engagé pour exécuter un acte déterminé lorsqu'il a été recruté pour répondre ponctuellement à un besoin de l'administration ; que la circonstance que cet agent a été recruté plusieurs fois, au cours de différentes années, pour exécuter des actes déterminés n'a pas pour effet, à elle seule, de lui conférer la qualité d'agent contractuel ;<br/>
<br/>
              3. Considérant que pour juger que M. A...ne pouvait être regardé comme un agent de droit public non titulaire de l'Etat entrant dans le champ des dispositions du décret du 17 janvier 1986, la cour a relevé qu'il était inscrit sur la liste des experts-traducteurs, au titre de ses prestations d'interprétariat, que sa rémunération était fondée sur les tarifs prévus à l'article R. 122 du code de procédure pénale et qu'il était ainsi réputé " exercer des actes déterminés relevant, au surplus, d'une activité libérale indépendante " ; qu'il ressortait toutefois des pièces du dossier qui lui était soumis, notamment de la convention conclue le 1er février 2002, que M. A... était chargé non seulement de réaliser des prestations d'interprétariat, mais aussi de mettre en place, au sein de la juridiction, un service d'interprétariat répondant à un besoin permanent de la juridiction et qu'il exerçait sa mission dans des conditions caractérisant un lien de subordination à l'égard du président du tribunal de grande instance de Créteil et du parquet près ce tribunal ; que, par suite, M. A...est fondé à soutenir que la cour a inexactement qualifié les faits qui lui étaient soumis ; <br/>
<br/>
              4. Considérant qu'il résulte de ce qui précède que, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, M. A...est fondé à demander l'annulation de l'arrêt qu'il attaque ; qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat une somme de 3 000 euros à verser à M. A...au titre de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 27 juin 2013 de la cour administrative d'appel de Paris est annulé. <br/>
<br/>
      Article 2 : L'affaire est renvoyée à la cour administrative de Paris. <br/>
<br/>
Article 3 : L'Etat versera à M. A...la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
      Article 4 : La présente décision sera notifiée à M. B...A.... <br/>
Copie en sera adressée à la garde des sceaux, ministre de la justice.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
