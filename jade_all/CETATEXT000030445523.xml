<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030445523</ID>
<ANCIEN_ID>JG_L_2015_03_000000348261</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/44/55/CETATEXT000030445523.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème - 9ème SSR, 23/03/2015, 348261, Publié au recueil Lebon</TITRE>
<DATE_DEC>2015-03-23</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>348261</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème - 9ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MASSE-DESSEN, THOUVENIN, COUDRAY ; SCP ROCHETEAU, UZAN-SARANO</AVOCATS>
<RAPPORTEUR>M. Romain Godet</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Edouard Crépey</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:XX:2015:348261.20150323</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi et le mémoire complémentaire, enregistrés les 8 avril et 8 juillet 2011 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. et Mme A...B..., demeurant ... ; M. et Mme B...demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler le jugement n° 1001903 du 28 janvier 2011 par lequel le tribunal administratif de Nice a rejeté leur demande tendant à l'annulation de l'arrêté du 2 mars 2010 par lequel le maire d'Aspremont a fait opposition à la déclaration préalable qu'ils avaient déposée en vue d'édifier une clôture ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à leur demande ;<br/>
<br/>
              3°) de mettre à la charge de la commune d'Aspremont la somme de 3 600 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code civil ; <br/>
<br/>
              Vu le code de l'urbanisme ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Romain Godet, maître des requêtes en service extraordinaire,<br/>
<br/>
              - les conclusions de M. Edouard Crépey, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Masse-Dessen, Thouvenin, Coudray, avocat de M. et Mme B...et à la SCP Rocheteau, Uzan-Sarano, avocat de la commune d'Aspremont ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il résulte du a) de l'article R. 423-1 du code de l'urbanisme que les demandes de permis de construire et les déclarations préalables sont adressées à la mairie de la commune dans laquelle les travaux sont envisagés, notamment, " par le ou les propriétaires du ou des terrains, leur mandataire ou par une ou plusieurs personnes attestant être autorisées par eux à exécuter les travaux " ; qu'aux termes de l'article R. 431-35 du même code : " La déclaration préalable précise : a) L'identité du ou des déclarants ; (...) La déclaration comporte également l'attestation du ou des déclarants qu'ils remplissent les conditions définies à l'article R. 423-1 pour déposer une déclaration préalable. " ; <br/>
<br/>
              2. Considérant qu'il résulte de ces dispositions que les déclarations préalables doivent seulement comporter, comme les demandes de permis de construire en vertu de l'article R. 431-5 du code de l'urbanisme, l'attestation du pétitionnaire qu'il remplit les conditions définies à l'article R. 423-1 précité ; que les autorisations d'utilisation du sol, qui ont pour seul objet de s'assurer de la conformité des travaux qu'elles autorisent avec la législation et la réglementation d'urbanisme, étant accordées sous réserve du droit des tiers, il n'appartient pas à l'autorité compétente de vérifier, dans le cadre de l'instruction d'une déclaration ou d'une demande de permis, la validité de l'attestation établie par le demandeur ; que les tiers ne sauraient donc utilement, pour contester une décision accordant une telle autorisation au vu de l'attestation requise, faire grief à l'administration de ne pas en avoir vérifié l'exactitude ;<br/>
<br/>
              3. Considérant, toutefois, que lorsque l'autorité saisie d'une telle déclaration ou d'une demande de permis de construire vient à disposer au moment où elle statue, sans avoir à procéder à une instruction lui permettant de les recueillir, d'informations de nature à établir son caractère frauduleux ou faisant apparaître, sans que cela puisse donner lieu à une contestation sérieuse, que le pétitionnaire ne dispose, contrairement à ce qu'implique l'article R. 423-1 du code de l'urbanisme, d'aucun droit à la déposer, il lui revient de s'opposer à la déclaration ou de refuser la demande de permis pour ce motif ; <br/>
<br/>
              4. Considérant que, pour rejeter la demande de M. et Mme B...tendant à l'annulation de l'opposition du maire d'Aspremont à la déclaration préalable qu'ils avaient déposée en vue de l'édification d'une clôture en limite du tracé, établi après bornage judiciaire, du chemin rural bordant leur propriété, le tribunal administratif de Nice a relevé que les requérants n'avaient pas qualité, au sens de l'article R. 423-1 précité, pour déposer une déclaration préalable pour ce projet ; que, pour ce faire, il s'est fondé, d'une part, sur une décision judiciaire rendue dans le cadre d'une action en bornage, laquelle ne tranche pas la question de la propriété d'un fonds mais en détermine seulement la délimitation matérielle, et, d'autre part, sur les motifs d'une décision judiciaire, postérieure à l'arrêté contesté et rendue dans le cadre d'une action en revendication de propriété, relatifs au tracé du chemin rural revendiqué par des voisins des requérants aux fins d'obtenir la reconnaissance d'une servitude par destination du père de famille en application de l'article 692 du code civil ;<br/>
<br/>
              5. Considérant qu'en confirmant ainsi l'un des motifs retenus par le maire pour s'opposer à la déclaration de M. et MmeB..., alors, d'une part, qu'il ne ressort pas des pièces du dossier qui lui était soumis et n'est d'ailleurs pas allégué que les requérants n'auraient pas fourni l'attestation prévue par l'article R. 431-35 du code de l'urbanisme ou qu'ils auraient procédé à une manoeuvre en vue d'obtenir par fraude que le maire d'Aspremont ne s'oppose pas à leur projet et, d'autre part, qu'il ne résultait pas des décisions judiciaires précitées, eu égard à leur portée, que les déclarants ne disposaient pas du droit à déposer cette déclaration en application de l'article R. 423-1 du même code, le tribunal administratif a commis une erreur de droit ; que, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, le jugement attaqué doit, en conséquence, être annulé ;<br/>
<br/>
              6. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              7. Considérant, en premier lieu, que l'article R. 600-1 du code de l'urbanisme n'impose, à peine d'irrecevabilité, la notification d'un recours contentieux qu'à l'encontre d'une décision de non-opposition à une déclaration préalable ; que l'arrêté du maire d'Aspremont du 2 mars 2010 constituant une décision d'opposition, la fin de non-recevoir opposée par la commune d'Aspremont et tirée du défaut de notification de celle-ci ne peut qu'être écartée ; <br/>
<br/>
              8. Considérant, en second lieu, qu'aux termes des articles R. 421-2 et R. 421-12 du code de l'urbanisme, l'édification de clôtures est dispensée de toute formalité, sous réserve que les prescriptions du plan local d'urbanisme n'imposent pas, dans un secteur déterminé, le dépôt d'une déclaration préalable ; qu'il ne ressort pas des pièces du dossier que le plan d'occupation des sols de la commune d'Aspremont, dans sa version applicable au litige, imposait une telle formalité dans le secteur dans lequel est situé le terrain d'assiette du projet contesté ; que, dès lors qu'aucune déclaration préalable n'était imposée pour la réalisation de ce projet, le maire d'Aspremont ne pouvait légalement s'opposer à la déclaration déposée par M. et Mme B...; qu'il en résulte que ceux-ci sont fondés à demander l'annulation de l'arrêté qu'ils attaquent ;<br/>
<br/>
              9. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la commune d'Aspremont la somme de 5 000 euros à verser à M. et Mme B...en application des dispositions de l'article L. 761-1 du code de justice administrative pour l'ensemble de la procédure engagée ; que ces mêmes dispositions font obstacle à ce qu'une somme soit versée à ce titre à la commune d'Aspremont qui  n'est pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du tribunal administratif de Nice du 28 janvier 2011 est annulé.<br/>
Article 2 : L'arrêté du maire de la commune d'Aspremont en date du 2 mars 2010 est annulé.<br/>
Article 3 : La commune d'Aspremont versera à M. et Mme B...une somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Les conclusions de la commune d'Aspremont présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée à M. et Mme A...B...et à la commune d'Aspremont. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">68-03-02-02 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. PERMIS DE CONSTRUIRE. PROCÉDURE D'ATTRIBUTION. INSTRUCTION DE LA DEMANDE. - FOURNITURE PAR LE DEMANDEUR DE L'ATTESTATION SUIVANT LAQUELLE IL REMPLIT LES CONDITIONS DÉFINIES À L'ARTICLE R. 423-1 DU CODE DE L'URBANISME - 1) PRINCIPE - OBLIGATION POUR LE SERVICE INSTRUCTEUR DE VÉRIFIER LA VALIDITÉ DE CETTE ATTESTATION - ABSENCE - POSSIBILITÉ POUR LES TIERS DE FAIRE GRIEF À L'ADMINISTRATION DE NE PAS EN AVOIR VÉRIFIÉ L'EXACTITUDE - ABSENCE - 2) RÉSERVE - CAS OÙ L'ADMINISTRATION DISPOSE, SANS AVOIR À LES RECHERCHER, D'INFORMATIONS DE NATURE À ÉTABLIR LE CARACTÈRE FRAUDULEUX DE LA DEMANDE OU FAISANT APPARAÎTRE QUE LE PÉTITIONNAIRE NE DISPOSE D'AUCUN DROIT À LA DÉPOSER - OBLIGATION POUR L'ADMINISTRATION DE REFUSER LE PERMIS - EXISTENCE [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">68-04-045-02 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. AUTORISATIONS D`UTILISATION DES SOLS DIVERSES. RÉGIMES DE DÉCLARATION PRÉALABLE. DÉCLARATION DE TRAVAUX EXEMPTÉS DE PERMIS DE CONSTRUIRE. - FOURNITURE PAR LE DÉCLARANT DE L'ATTESTATION SUIVANT LAQUELLE IL REMPLIT LES CONDITIONS DÉFINIES À L'ARTICLE R. 423-1 DU CODE DE L'URBANISME - 1) PRINCIPE - OBLIGATION POUR LE SERVICE INSTRUCTEUR DE VÉRIFIER LA VALIDITÉ DE CETTE ATTESTATION - ABSENCE - POSSIBILITÉ POUR LES TIERS DE FAIRE GRIEF À L'ADMINISTRATION DE NE PAS EN AVOIR VÉRIFIÉ L'EXACTITUDE - ABSENCE - 2) RÉSERVE - CAS OÙ L'ADMINISTRATION DISPOSE, SANS AVOIR À LES RECHERCHER, D'INFORMATIONS DE NATURE À ÉTABLIR LE CARACTÈRE FRAUDULEUX DE LA DÉCLARATION OU FAISANT APPARAÎTRE QUE LE PÉTITIONNAIRE NE DISPOSE D'AUCUN DROIT À LA DÉPOSER - OBLIGATION POUR L'ADMINISTRATION DE S'OPPOSER À LA DÉCLARATION - EXISTENCE [RJ1].
</SCT>
<ANA ID="9A"> 68-03-02-02 Il résulte des dispositions des articles R. 423-1 et R. 431-35 du code de l'urbanisme que les déclarations préalables doivent seulement comporter, comme les demandes de permis de construire en vertu de l'article R. 431-5 du code de l'urbanisme, l'attestation du pétitionnaire qu'il remplit les conditions définies à l'article R. 423-1.... ,,1) Les autorisations d'utilisation du sol, qui ont pour seul objet de s'assurer de la conformité des travaux qu'elles autorisent avec la législation et la réglementation d'urbanisme, étant accordées sous réserve du droit des tiers, il n'appartient pas à l'autorité compétente de vérifier, dans le cadre de l'instruction d'une déclaration ou d'une demande de permis, la validité de l'attestation établie par le demandeur. Les tiers ne sauraient donc soutenir utilement, pour contester une décision accordant une telle autorisation au vu de l'attestation requise, faire grief à l'administration de ne pas en avoir vérifié l'exactitude.,,,2) Toutefois, lorsque l'autorité saisie d'une telle déclaration ou d'une demande de permis de construire vient à disposer au moment où elle statue, sans avoir à procéder à une instruction lui permettant de les recueillir, d'informations de nature à établir son caractère frauduleux ou faisant apparaître, sans que cela puisse donner lieu à une contestation sérieuse, que le pétitionnaire ne dispose, contrairement à ce qu'implique l'article R. 423-1 du code de l'urbanisme, d'aucun droit à la déposer, il lui revient de s'opposer à la déclaration ou de refuser la demande de permis pour ce motif.</ANA>
<ANA ID="9B"> 68-04-045-02 Il résulte des dispositions des articles R. 423-1 et R. 431-35 du code de l'urbanisme que les déclarations préalables doivent seulement comporter, comme les demandes de permis de construire en vertu de l'article R. 431-5 du code de l'urbanisme, l'attestation du pétitionnaire qu'il remplit les conditions définies à l'article R. 423-1.... ,,1) Les autorisations d'utilisation du sol, qui ont pour seul objet de s'assurer de la conformité des travaux qu'elles autorisent avec la législation et la réglementation d'urbanisme, étant accordées sous réserve du droit des tiers, il n'appartient pas à l'autorité compétente de vérifier, dans le cadre de l'instruction d'une déclaration ou d'une demande de permis, la validité de l'attestation établie par le demandeur. Les tiers ne sauraient donc soutenir utilement, pour contester une décision accordant une telle autorisation au vu de l'attestation requise, faire grief à l'administration de ne pas en avoir vérifié l'exactitude.,,,2) Toutefois, lorsque l'autorité saisie d'une telle déclaration ou d'une demande de permis de construire vient à disposer au moment où elle statue, sans avoir à procéder à une instruction lui permettant de les recueillir, d'informations de nature à établir son caractère frauduleux ou faisant apparaître, sans que cela puisse donner lieu à une contestation sérieuse, que le pétitionnaire ne dispose, contrairement à ce qu'implique l'article R. 423-1 du code de l'urbanisme, d'aucun droit à la déposer, il lui revient de s'opposer à la déclaration ou de refuser la demande de permis pour ce motif.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr. CE, 17 octobre 2014, Commune de Jouars-Pontchartrain, n° 360968, T. p. 908.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
