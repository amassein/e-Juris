<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000025822343</ID>
<ANCIEN_ID>JG_L_2012_05_000000353536</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/25/82/23/CETATEXT000025822343.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème et 7ème sous-sections réunies, 07/05/2012, 353536, Publié au recueil Lebon</TITRE>
<DATE_DEC>2012-05-07</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>353536</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème et 7ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR>Mme Isabelle de Silva</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Damien Botteghi</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2012:353536.20120507</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 21 octobre 2011 au secrétariat du contentieux du Conseil d'Etat, présentée pour M. Eric B, demeurant au ... ; M. B demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler le jugement n° 1102504 du 22 septembre 2011 par lequel le tribunal administratif de Cergy-Pontoise a, sur la protestation de M. Paul C, annulé les opérations électorales organisées les 20 et 27 mars 2011 dans le canton de Saint-Cloud pour l'élection du conseiller général de ce canton ; <br/>
<br/>
              2°) de rejeter la protestation de M. C contre ces opérations électorales ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code électoral ;<br/>
<br/>
              Vu le code général des collectivités territoriales ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Isabelle de Silva, Conseiller d'Etat,<br/>
<br/>
              - les observations de la SCP Piwnica, Molinié, avocat de M. B,<br/>
<br/>
              - les conclusions de M. Damien Botteghi, rapporteur public,<br/>
<br/>
              - la parole ayant à nouveau été donnée à la SCP Piwnica, Molinié, avocat de M. B ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant qu'à l'issue des opérations électorales qui se sont déroulées les 20 et 27 mars 2011 dans le canton de Saint-Cloud en vue de la désignation du conseiller général de ce canton, le décompte des suffrages a attribué, au premier tour, 3394 voix, soit 46,81 % des suffrages exprimés, à M. B, 1266 voix, soit 17,46 %, à Mme D, et 1205 voix, soit 16,62 % à Mme E et, au deuxième tour, 5421 voix, soit 77,09 % des suffrages exprimés, à M. Berdoati et 1611 voix, soit 22,91 %, à Mme D ; que, par le jugement attaqué, le tribunal administratif de Cergy-Pontoise a, sur la protestation de M. C, électeur, annulé l'ensemble des opérations électorales ;<br/>
<br/>
              Considérant que, d'une part, aux termes de l'article L. 2121-27-1 du code général des collectivités territoriales : " Dans les communes de 3500 habitants et plus, lorsque la commune diffuse, sous quelque forme que ce soit, un bulletin d'information générale sur les réalisations et la gestion du conseil municipal, un espace est réservé à l'expression des conseillers n'appartenant pas à la majorité municipale. Les modalités d'application de cette disposition sont définies par le règlement intérieur " ; que, d'autre part, selon le deuxième alinéa de l'article L. 52-8 du code électoral : " Les personnes morales, à l'exception des partis ou groupements politiques, ne peuvent participer au financement de la campagne électorale d'un candidat, ni en lui consentant des dons sous quelque forme que ce soit, ni en lui fournissant des biens, services ou autres avantages directs ou indirects à des prix inférieurs à ceux qui sont habituellement pratiqués. " ;<br/>
<br/>
              Considérant qu'il résulte des dispositions de l'article L. 2121-27-1 du code général des collectivités territoriales que la commune est tenue de réserver dans son bulletin d'information municipale, lorsqu'elle diffuse un tel bulletin, un espace d'expression réservé à l'opposition municipale ; que la commune ne saurait contrôler le contenu des articles publiés dans ce cadre, qui n'engagent que la responsabilité de leurs auteurs ; que dans ces conditions, si de tels articles sont susceptibles d'être regardés, en fonction de leur contenu et de leur date de parution, comme des éléments de propagande électorale de leurs auteurs, ils ne sauraient être assimilés à des dons émanant de la commune, personne morale, au sens des dispositions de l'article L. 52-8 du code électoral ; <br/>
<br/>
              Considérant qu'il résulte de l'instruction que la commune de Saint-Cloud a fait paraître dans le numéro de février 2011 du bulletin d'information municipale " Saint-Cloud Magazine ", dans la rubrique " tribunes " réservée, conformément aux dispositions de l'article L. 2121-27-1 du code général des collectivités territoriales, à l'opposition municipale, trois articles dont un, qui émanait de Mme D, conseillère municipale d'opposition, intitulé " cantonales 2011 : le Front national sera présent " ; que ce texte, consacré pour l'essentiel à un rappel de la portée des élections cantonales et à l'annonce de la candidature de Mme D à cette élection, ne traduit, dans les circonstances de l'espèce, aucune irrégularité susceptible d'avoir altéré les résultats du scrutin ; qu'il résulte de ce qui précède que M. B est fondé à soutenir que c'est à tort que le tribunal administratif de Cergy-Pontoise s'est fondé sur ce grief pour annuler les opérations électorales qui se sont déroulées les 20 et 27 mars 2011 en vue de la désignation du conseiller général du canton de Saint-Cloud ;<br/>
<br/>
              Considérant qu'il appartient au Conseil d'Etat, saisi de l'ensemble du litige par l'effet dévolutif de l'appel, d'examiner l'autre grief présenté devant le tribunal administratif de Cergy-Pontoise par M. C ;<br/>
<br/>
              Considérant qu'aux termes de l'article R. 30 du code électoral : " Les bulletins doivent être imprimés en une seule couleur sur papier blanc, d'un grammage compris entre 60 et 80 grammes au mètre carré et avoir les formats suivants : / 105 mm x 148 mm pour les bulletins comportant un ou deux noms (...) " ; que la circonstance que M. B a imprimé ses bulletins de vote en format vertical, tandis que les autres candidats avaient imprimé leur bulletin en format horizontal de 105 x 148 mm, n'a pas constitué une irrégularité ou une manoeuvre de nature à altérer la sincérité du scrutin ;<br/>
<br/>
              Considérant qu'il résulte de ce qui précède que M. B est fondé à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Cergy-Pontoise a annulé les opérations électorales qui se sont déroulées les 20 et 27 mars 2011 en vue de la désignation du conseiller général du canton de Saint-Cloud ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : Le jugement du tribunal administratif de Cergy-Pontoise en date du 22 septembre 2011 est annulé.<br/>
<br/>
Article 2 : Les opérations électorales qui se sont déroulées les 20 et 27 mars 2011 dans le canton de Saint-Cloud en vue de la désignation du conseiller général de ce canton  sont validées.<br/>
<br/>
Article 3 : La protestation de M. C est rejetée.<br/>
<br/>
Article 4 : La présente décision sera notifiée à M. Eric B, à Mme Alexandra D, à M. Paul C et au ministre de l'intérieur, de l'outre-mer, des collectivités territoriales et de l'immigration.<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">28-005-04-01 ÉLECTIONS ET RÉFÉRENDUM. DISPOSITIONS GÉNÉRALES APPLICABLES AUX ÉLECTIONS. FINANCEMENT ET PLAFONNEMENT DES DÉPENSES ÉLECTORALES. DONS. - AVANTAGE EN NATURE ASSIMILABLE À UN DON D'UNE PERSONNE MORALE, PROHIBÉ PAR L'ARTICLE L. 52-8 DU CODE ÉLECTORAL - NOTION - TRIBUNE RÉSERVÉE À L'OPPOSITION DANS LE JOURNAL MUNICIPAL - EXCLUSION [RJ1].
</SCT>
<ANA ID="9A"> 28-005-04-01 Il résulte des dispositions de l'article L. 2121-27-1 du code général des collectivités territoriales que la commune est tenue de réserver dans son bulletin d'information municipale, lorsqu'elle diffuse un tel bulletin, un espace d'expression réservé à l'opposition municipale. La commune ne saurait contrôler le contenu des articles publiés dans ce cadre, qui n'engagent que la responsabilité de leurs auteurs. Dans ces conditions, si de tels articles sont susceptibles d'être regardés, en fonction de leur contenu et de leur date de parution, comme des éléments de propagande électorale de leurs auteurs, ils ne sauraient être assimilés à des dons émanant de la commune, personne morale, au sens des dispositions de l'article L. 52-8 du code électoral.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Ab. jur. CE, 3 mars 2009, Elections municipales de Montreuil-sous-Bois (Seine-Saint-Denis), Mme Konate, n° 322430, T. p. 758.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
