<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037600034</ID>
<ANCIEN_ID>JG_L_2018_11_000000421905</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/60/00/CETATEXT000037600034.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème chambre, 07/11/2018, 421905, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-11-07</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>421905</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème chambre</FORMATION>
<TYPE_REC>Rectif. d'erreur matérielle</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP THOUVENIN, COUDRAY, GREVY</AVOCATS>
<RAPPORTEUR>Mme Françoise Tomé</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Sophie-Justine Lieber</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:421905.20181107</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              Procédure contentieuse antérieure<br/>
<br/>
              Mme A...B...a demandé au tribunal administratif de Nancy d'annuler pour excès de pouvoir la décision du 17 juin 2015 du président du conseil départemental de Meurthe-et-Moselle refusant de lui délivrer un agrément aux fins d'adoption ainsi que la décision du 18 août 2015 rejetant son recours gracieux contre cette même décision. Par un jugement n°1502858 du 18 octobre 2016, le tribunal administratif a rejeté sa demande. <br/>
<br/>
              Par un arrêt n° 16NC02755 du 5 décembre 2017, la cour administrative d'appel de Nancy a rejeté l'appel formé par Mme B...contre ce jugement.<br/>
<br/>
              Par une ordonnance n°417093 du 2 mai 2018, le président de la 2ème chambre de la section du contentieux du Conseil d'Etat n'a pas admis le pourvoi formé par Mme B... contre cet arrêt. <br/>
<br/>
              Recours en rectification d'erreur matérielle<br/>
<br/>
              Par une requête, enregistrée le 2 juillet 2018 au secrétariat du contentieux du Conseil d'Etat, Mme A...B...demande au Conseil d'État : <br/>
<br/>
              1°) de rectifier pour erreur matérielle l'ordonnance n°417093 du 2 mai 2018 par laquelle  le président de la 2ème chambre de la section du contentieux du Conseil d'Etat a refusé d'admettre le pourvoi qu'elle avait formé contre l'arrêt n° 16NC02755 du 5 décembre 2017 de la cour administrative d'appel de Nancy ;<br/>
<br/>
              2°) statuant à nouveau sur le pourvoi n°417093, de faire droit à ses conclusions d'appel et d'annuler le jugement et les décisions attaqués ;<br/>
<br/>
              3°) de mettre à la charge du conseil départemental de Meurthe-et-Moselle la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.  <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la loi n°91-647 du 10 juillet 1991 ; <br/>
              - le décret n°91-1266 du 19 décembre 1991 ; <br/>
              - le code de justice administrative ; <br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Françoise Tomé, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Sophie-Justine Lieber, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Thouvenin, Coudray, Grevy, avocat de Mme B...;<br/>
<br/>
<br/>
<br/>
<br/>1.	Considérant qu'aux termes de l'article R. 833-1 du code de justice administrative : " Lorsqu'une décision d'une cour administrative d'appel ou du Conseil d'Etat est entachée d'une erreur matérielle susceptible d'avoir exercé une influence sur le jugement de l'affaire, la partie intéressée peut introduire devant la juridiction qui a rendu la décision un recours en rectification" ; qu'en se prévalant de ces dispositions, Mme B...présente un recours en rectification d'erreur matérielle contre l'ordonnance du 2 mai 2018 par laquelle le président de la 2ème chambre de la section du contentieux du Conseil d'Etat a refusé d'admettre le pourvoi qu'elle avait formé contre l'arrêt du 5 décembre 2017 de la cour administrative d'appel de Nancy rejetant son appel contre le jugement du tribunal administratif de Nancy du 18 octobre 2016 au motif qu'il n'était pas présenté par le ministère d'un avocat au Conseil d'Etat et à la Cour de cassation ; qu'à ce titre, elle soutient que l'ordonnance contestée ne pouvait être prise sans qu'une demande de régularisation de son pourvoi ne lui soit préalablement adressée ou, à tout le moins, sans qu'un nouveau délai de deux mois ne lui soit accordé pour régulariser son pourvoi après le rejet de sa demande d'aide juridictionnelle ; <br/>
<br/>
              2.	Considérant, d'une part, qu'aux termes de l'article R. 821-3 du code de justice administrative: " Le ministère d'un avocat au Conseil d'Etat et à la Cour de cassation est obligatoire pour l'introduction, devant le Conseil d'Etat, des recours en cassation, à l'exception de ceux dirigés contre les décisions de la commission centrale d'aide sociale et des juridictions de pension " ; qu'en vertu de l'article R. 612-1 du même code, le juge de cassation peut rejeter comme irrecevable, sans demande de régularisation préalable, le pourvoi présenté en méconnaissance de cette obligation, lorsqu'elle a été mentionnée dans la notification de l'arrêt attaqué ; qu'en outre, il n'est pas tenu, dans ce cas, d'attendre l'expiration du délai de recours contentieux ; <br/>
<br/>
              3.	Considérant, d'autre part, qu'aux termes de l'article L. 822-1 du même code : " Le pourvoi en cassation devant le Conseil d'Etat fait l'objet d'une procédure préalable d'admission. L'admission est refusée par décision juridictionnelle si le pourvoi est irrecevable ou n'est fondé sur aucun moyen sérieux " ; qu'aux termes de l'article R. 822-5 du même code : " Lorsque le pourvoi est irrecevable pour défaut de ministère d'avocat (...) le président de la chambre peut décider par ordonnance de ne pas l'admettre " ; <br/>
<br/>
              4.	Considérant que le pourvoi de MmeB..., enregistré le 5 janvier 2018 au secrétariat du contentieux du Conseil d'Etat, n'était pas présenté par le ministère d'un avocat au Conseil d'Etat et à la Cour de cassation, alors que l'obligation d'y recourir avait été mentionnée dans la notification de l'arrêt attaqué ; que si Mme B...a ultérieurement sollicité le bénéfice de l'aide juridictionnelle, sa demande a été rejetée par une décision du bureau d'aide juridictionnelle du 26 janvier 2018, notifiée le 1er février 2018 et son recours contre cette dernière décision a fait l'objet d'une ordonnance de rejet du président de la section du contentieux du Conseil d'Etat en date du 5 avril 2018, notifiée le 14 avril suivant ; que, dans ces conditions, il résulte de ce qui a été dit aux points précédents que c'est, en tout état de cause, sans erreur matérielle que son pourvoi a fait, le 2 mai 2018, l'objet d'un refus d'admission ; que, par suite, le recours de Mme B...n'est pas recevable et ne peut qu'être rejeté, de même que les conclusions qui y sont adjointes au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de Mme B...est rejetée.<br/>
Article 2 : La présente décision sera notifiée à Mme A...B....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
