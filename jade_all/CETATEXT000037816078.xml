<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037816078</ID>
<ANCIEN_ID>JG_L_2018_12_000000425874</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/81/60/CETATEXT000037816078.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 10/12/2018, 425874, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-12-10</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>425874</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2018:425874.20181210</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              M. ElzaloukAbdelrahmana demandé au juge des référés du tribunal administratif de Paris, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, d'enjoindre au préfet de police d'enregistrer sa demande d'asile, de lui délivrer un dossier de l'Office français de protection des réfugiés et apatrides ainsi qu'une attestation d'asile dans un délai de trois jours, sous astreinte de 100 euros par jour de retard. Par une ordonnance n° 1820737/9 du 16 novembre 2018, le juge des référés du tribunal administratif de Paris a enjoint au préfet de police d'enregistrer la demande d'asile de M. Abdelrahmandans un délai de sept jours à compter de la notification de cette ordonnance et de lui délivrer une attestation de demande d'asile puis un dossier de l'Office français de protection des réfugiés et apatrides.<br/>
<br/>
              Par une requête, enregistrée le 30 novembre 2018 au secrétariat du contentieux du Conseil d'Etat, le ministre de l'intérieur demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative : <br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) de rejeter la demande de première instance présentée par M.Abdelrahman.<br/>
<br/>
<br/>
<br/>
              Il soutient que le préfet de police n'a pas porté une atteinte grave et manifestement illégale au droit d'asile de M. Abdelrahmanen refusant d'enregistrer sa demande d'asile au-delà du délai de six mois suivant l'acceptation de son transfert par l'Italie, le délai de transfert ayant été porté à dix-huit mois dès lors que l'intéressé doit être regardé comme en fuite au sens de l'article 29 du règlement du Parlement européen et du Conseil du 26 juin 2013.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le règlement (CE) n° 1560/2003 de la Commission du 2 septembre 2003 portant modalités d'application du règlement (CE) n° 343/2003 du Conseil ;<br/>
              - le règlement (UE) n° 604/2013 du Parlement européen et du Conseil du 26 juin 2013 ;<br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
              1. Aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. Le juge des référés se prononce dans un délai de quarante-huit heures ". En vertu de l'article L. 522-3 du même code, le juge des référés peut rejeter une requête par une ordonnance motivée, sans instruction contradictoire ni audience publique, lorsque la condition d'urgence n'est pas remplie ou lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable ou qu'elle est mal fondée. A cet égard, il appartient au juge d'appel de prendre en considération les éléments recueillis par le juge du premier degré dans le cadre de la procédure écrite et orale qu'il a diligentée.<br/>
<br/>
              2. Le droit constitutionnel d'asile, qui a le caractère d'une liberté fondamentale, a pour corollaire le droit de solliciter le statut de réfugié. S'il implique que l'étranger qui sollicite la reconnaissance de la qualité de réfugié soit en principe autorisé à demeurer sur le territoire jusqu'à ce qu'il ait été statué sur sa demande, ce droit s'exerce dans les conditions définies par les articles L. 741-1 et suivants du code de l'entrée et du séjour des étrangers et du droit d'asile. L'article L. 742-3 de ce code prévoit que l'étranger dont l'examen de la demande d'asile relève de la responsabilité d'un autre Etat peut faire l'objet d'un transfert vers l'Etat qui est responsable de cet examen en application des dispositions du règlement du Parlement européen et du Conseil du 26 juin 2013. <br/>
<br/>
              3. Il résulte de l'article 29 du règlement du 26 juin 2013 que le transfert peut avoir lieu pendant une période de six mois à compter de l'acceptation de la demande de prise en charge, cette période étant susceptible d'être portée à dix-huit mois si l'intéressé " prend la fuite ". Aux termes de l'article 7 du règlement de la Commission du 2 septembre 2003 : " 1. Le transfert vers l'Etat responsable s'effectue de l'une des manières suivantes : a) à l'initiative du demandeur, une date limite étant fixée ; b) sous la forme d'un départ contrôlé, le demandeur étant accompagné jusqu'à l'embarquement par un agent de l'Etat requérant et le lieu, la date et l'heure de son arrivée étant notifiées à l'Etat responsable dans un délai préalable convenu : c) sous escorte, le demandeur étant accompagné par un agent de l'Etat requérant, ou par le représentant d'un organisme mandaté par l'Etat requérant à cette fin, et remis aux autorités de l'Etat responsable (...) ". Il résulte de ces dispositions que le transfert d'un demandeur d'asile vers un Etat membre qui a accepté sa prise ou sa reprise en charge, sur le fondement du règlement du 26 juin 2013, s'effectue selon l'une des trois modalités définies au 1. de l'article 7 cité ci-dessus : à l'initiative du demandeur, sous la forme d'un départ contrôlé ou sous escorte.<br/>
Le 3. du même article prévoit en outre que " L'État membre qui procède au transfert veille à ce que tous les documents du demandeur lui soient restitués avant son départ ou soient confiés aux membres de son escorte afin d'être remis aux autorités compétentes de l'État membre responsable ou soient transmis par d'autres voies appropriées ".<br/>
<br/>
              4. Il résulte clairement des dispositions mentionnées au point précédent que, d'une part, la notion de fuite doit s'entendre comme visant le cas où un ressortissant étranger se serait soustrait de façon intentionnelle et systématique au contrôle de l'autorité administrative en vue de faire obstacle à une mesure d'éloignement le concernant. D'autre part, dans l'hypothèse où le transfert du demandeur d'asile s'effectue sous la forme d'un départ contrôlé, il appartient, dans tous les cas, à l'Etat responsable de ce transfert d'en assurer effectivement l'organisation matérielle et d'accompagner le demandeur d'asile jusqu'à l'embarquement vers son lieu de destination. Une telle obligation recouvre la prise en charge du titre de transport permettant de rejoindre l'Etat responsable de l'examen de la demande d'asile depuis le territoire français ainsi que, le cas échéant et si nécessaire, celle du pré-acheminement du lieu de résidence du demandeur au lieu d'embarquement. Enfin, dans l'hypothèse où le demandeur d'asile se soustrait intentionnellement à l'exécution de son transfert ainsi organisé, il doit être regardé comme en fuite au sens des dispositions de l'article 29 du règlement du 26 juin 2013 rappelées au point 3.<br/>
<br/>
              5. Il résulte de l'instruction menée par le juge des référés du tribunal administratif de Paris et des précisions apportées en cause d'appel par le ministre de l'intérieur que M.Abdelrahman, ressortissant libyen, a déclaré être entré irrégulièrement en France le 16 décembre 2017. Il a sollicité l'asile le 26 décembre 2017 et le relevé de ses empreintes a alors fait apparaître qu'il avait été précédemment identifié en Italie et y avait déposé une demande d'asile le 16 novembre 2017. Le 15 janvier 2018, l'administration française a saisi les autorités italiennes d'une demande de reprise en charge, qui a été implicitement acceptée le 30 janvier 2018 en vertu de l'article 25 du règlement du 26 juin 2013. Par un arrêté du 20 février 2018, le préfet de police a pris un arrêté de transfert, notifié le même jour à M.Abdelrahman, qui ne l'a pas contesté et s'est présenté à l'ensemble des convocations qui lui ont été faites. Par un arrêté du 5 juin 2018, le préfet de police a placé M. Abdelrahmanen rétention administrative en vue de son éloignement. Le 6 juin 2018, M. Abdelrahmana refusé d'embarquer sur le vol à destination de Venise sur lequel une place lui avait été réservée au motif qu'il n'avait pu obtenir la restitution de son passeport libyen, qu'il avait remis à la préfecture de police sans que lui en soit donné récépissé ni qu'il ait été remis aux membres de son escorte en vue d'être transmis aux autorités italiennes. Le 7 juin 2018, les autorités italiennes ont été informées de la prolongation du délai de transfert de six à dix-huit mois en raison de la fuite de l'intéressé. Le 26 octobre 2018, M. Abdelrahmans'est à nouveau présenté à la convocation qui lui avait été faite à la préfecture de police et, par arrêté du même jour, a, de nouveau, fait l'objet d'une mesure de rétention administrative, prolongée le 28 octobre 2018 par le juge des libertés et de la détention du tribunal de grande instance de Paris jusqu'au 25 novembre 2018. Il a, alors, présenté une nouvelle demande d'enregistrement de sa demande d'asile le 7 novembre 2018, à laquelle un refus a été opposé. Le 14 novembre 2018, M. Abdelrahamna saisi le juge de référés du tribunal administratif de Paris d'un recours sollicitant, sur le fondement de l'article L. 521-2 du code de justice administrative, l'enregistrement de sa demande d'asile en raison de l'expiration du délai de six mois dans lequel la France pouvait procéder à son transfert. Par une ordonnance du 16 novembre 2018, dont le ministre de l'intérieur relève appel, le juge des référés du tribunal administratif de Paris a enjoint au préfet de police d'enregistrer la demande d'asile de M. Abdelrahman dans un délai de sept jours et de lui délivrer une attestation de demande d'asile ainsi que le dossier à présenter à l'Office français de protection des réfugiés et apatrides.<br/>
<br/>
              6. Il résulte de ce qui est indiqué au point 4 que, dans l'hypothèse où l'administration a respecté les obligations qui sont les siennes dans l'organisation d'un départ contrôlé et où l'intéressé s'est soustrait intentionnellement à l'exécution de ce départ, puis a demandé à nouveau l'enregistrement de sa demande après l'expiration du délai de transfert de six mois, le demandeur doit être regardé comme en fuite au sens des dispositions de l'article 29 du règlement du 26 juin 2013. En l'espèce, toutefois, ainsi qu'il a été dit au point 5, l'intéressé a déféré à l'ensemble des convocations qui lui avaient été faites par l'administration, y compris en vue de son placement en rétention et de l'exécution de son transfert, qu'il n'a d'ailleurs pas contesté, et n'a refusé d'embarquer qu'au motif qu'il souhaitait obtenir la restitution de son passeport libyen, qu'il avait remis à la préfecture de police sans que lui en soit donné récépissé ni qu'il ait été remis aux membres de son escorte en vue d'être transmis aux autorités italiennes. Dans ces circonstances, qui n'ont pas été contestées en première instance par le préfet de police, lequel n'a pas défendu et n'était pas présent à l'audience, et ne sont pas davantage contestées en appel par le ministre de l'intérieur, l'intéressé ne pouvait être regardé comme s'étant intentionnellement soustrait à l'exécution de la mesure de transfert dont il faisait l'objet ni, par suite, comme étant en fuite au sens des dispositions précitées.<br/>
<br/>
              7. Il résulte de tout ce qui précède que le ministre de l'intérieur, qui ne conteste pas que la condition d'urgence doit être regardée comme remplie, n'est pas fondé à soutenir que c'est à tort que le juge des référés du tribunal administratif de Paris a jugé que, le délai de six mois prévu par l'article 29 du règlement du 26 juin 2013 étant venu à expiration et la France étant, par suite, responsable du traitement de la demande d'asile de M.Abdelrahman, il y avait lieu d'enjoindre au préfet de police d'enregistrer la demande d'asile de l'intéressé, de lui délivrer une attestation de demande d'asile ainsi que le dossier à présenter à l'Office français de protection des réfugiés et apatrides. Par suite, il est manifeste que l'appel du ministre de l'intérieur ne peut être accueilli et il y a lieu de rejeter sa requête selon la procédure prévue par l'article L. 522-3 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête du ministre de l'intérieur est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée au ministre de l'intérieur et à M. ElazoukAbdelrahman.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
