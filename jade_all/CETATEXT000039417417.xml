<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039417417</ID>
<ANCIEN_ID>JG_L_2019_11_000000430764</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/41/74/CETATEXT000039417417.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème - 6ème chambres réunies, 22/11/2019, 430764</TITRE>
<DATE_DEC>2019-11-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>430764</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème - 6ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Alain Seban</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:XX:2019:430764.20191122</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
               Par un jugement n° 1705547 du 6 mai 2019, le tribunal administratif de Toulouse a transmis au Conseil d'Etat la requête, enregistrée le 1er décembre 2017 au greffe de ce tribunal, présentée par M. B... A.... Par cette requête, trois nouveaux mémoires enregistrés au greffe du tribunal administratif les 8 février, 10 août et 19 décembre 2018 et deux mémoires en réplique enregistrés au secrétariat du contentieux du Conseil d'Etat les 23 août et 4 octobre 2019, M. A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler la décision du 6 octobre 2017 par laquelle le Conseil national de l'ordre des masseurs-kinésithérapeutes a refusé de reconnaître le diplôme universitaire d'études complémentaires en kinésithérapie du sport délivré par l'université de Nice en 2016 ;<br/>
<br/>
              2°) d'enjoindre au Conseil national de l'ordre des masseurs-kinésithérapeutes de reconnaître ce diplôme dans un délai de quinze jours, ou, à titre subsidiaire, de lui enjoindre de réexaminer sa demande dans le même délai, sous astreinte de 150 euros par jour de retard ;<br/>
<br/>
              3°) de mettre à la charge du Conseil national de l'ordre des masseurs-kinésithérapeutes la somme de 2 000 euros au titre de l'article L. 761 1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de la santé publique ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Alain Seban, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public.<br/>
<br/>
              Vu la note en délibéré, enregistrée le 13 novembre 2019, présentée par le Conseil national de l'ordre des masseurs-kinésithérapeutes ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article R. 4321-122 du code de la santé publique : " Les indications qu'un masseur-kinésithérapeute est autorisé à mentionner sur ses documents professionnels sont : (...) 5° Ses diplômes, titres, grades et fonctions lorsqu'ils ont été reconnus par le conseil national de l'ordre ; (...) ". Aux termes de l'article R. 4321-123 du même code : " Les indications qu'un masseur-kinésithérapeute est autorisé à faire figurer dans les annuaires à usage du public, dans la rubrique : masseurs-kinésithérapeutes, quel qu'en soit le support, sont : (...) 3° La qualification, les titres reconnus conformément au règlement de qualification, les titres et les diplômes d'études complémentaires reconnus par le conseil national de l'ordre. (...) ". Enfin, aux termes de l'article R. 4321-125 : " Les indications qu'un masseur-kinésithérapeute est autorisé à faire figurer sur une plaque à son lieu d'exercice sont celles mentionnées à l'article R.4321-123 ". M. A..., masseur-kinésithérapeute inscrit au tableau de l'ordre de la Haute-Garonne, demande l'annulation de la décision du 6 octobre 2017 par laquelle le Conseil national de l'ordre des masseurs-kinésithérapeutes a, sur le fondement de ces dispositions, refusé de reconnaître le diplôme d'université d'études complémentaires de kinésithérapie du sport, délivré en 2016 par l'université de Nice.<br/>
<br/>
              2. Aux termes de l'article R. 4321-65 du code de la santé publique : " Le masseur-kinésithérapeute ne divulgue pas dans les milieux professionnels une nouvelle pratique insuffisamment éprouvée sans accompagner sa communication des réserves qui s'imposent. Il ne fait pas une telle divulgation auprès d'un public non professionnel ". Aux termes de l'article R. 4321-80 du même code : " Dès lors qu'il a accepté de répondre à une demande, le masseur-kinésithérapeute s'engage personnellement à assurer au patient des soins consciencieux, attentifs et fondés sur les données actuelles de la science. " Enfin, aux termes de l'article R. 4321-87 : " Le masseur-kinésithérapeute ne peut conseiller et proposer au patient ou à son entourage, comme étant salutaire ou sans danger, un produit ou un procédé, illusoire ou insuffisamment éprouvé. Toute pratique de charlatanisme est interdite. " <br/>
<br/>
              3. Il ressort de la motivation de la décision attaquée et des écritures en défense du Conseil national de l'ordre des masseurs kinésithérapeutes que la décision de refus litigieuse est motivée tant par la non-conformité aux données actuelles de la science, au sens des dispositions citées ci-dessus de l'article R. 4321-80 du code de la santé publique, des techniques dites " Tecar thérapie ", " ventouses " et " kinésio-taping " figurant dans le programme d'enseignement du diplôme universitaire en cause, que par la méconnaissance, par cet enseignement, de l'interdiction, posée à l'article R. 4321-65 du même code, de divulguer auprès d'un public non professionnel des pratiques insuffisamment éprouvées. <br/>
<br/>
              4. Si le caractère insuffisamment éprouvé ou non conforme aux données actuelles de la science de techniques enseignées dans une formation est susceptible de justifier légalement le refus de reconnaissance d'un diplôme sanctionnant une formation de masso-kinésithérapie, l'appréciation du Conseil national de l'ordre dans l'exercice de la compétence qui lui est conférée par les articles R. 4321-122 et R. 4321-123 du code de la santé publique doit cependant tenir compte de la place relative dévolue aux techniques en cause dans la formation et des modalités de la présentation qui est prévue, la présentation des caractéristiques et des risques de certaines techniques encore peu éprouvées n'étant pas nécessairement à exclure de la formation des praticiens.<br/>
<br/>
              5. En l'espèce, il ressort des pièces du dossier que le programme de la formation en litige ne réservait qu'une place très limitée à la présentation des techniques critiquées par le Conseil national de l'ordre, inférieure à cinq pour cent du volume d'heures total. Par ailleurs, il n'est ni établi ni même allégué que les modalités prévues pour cette présentation méconnaîtraient les principes déontologiques rappelés au point 2. Enfin, s'il est soutenu que ces techniques ne présentent pas une efficacité suffisamment établie, il n'est pas allégué qu'elles présentent un danger pour les patients.<br/>
<br/>
              6. Il suit de là qu'en refusant d'agréer le diplôme litigieux, le Conseil national de l'ordre des masseurs-kinésithérapeutes n'a pas fait une exacte application des articles R. 4321-122 et R. 4321-123 du code de la santé publique cités ci-dessus. M. A... est ainsi fondé, sans qu'il soit besoin d'examiner les autres moyens de sa requête, à demander l'annulation de la décision qu'il attaque.<br/>
<br/>
              7. L'exécution de la présente décision implique seulement que la demande de M. A... soit réexaminée par le Conseil national de l'ordre des masseurs-kinésithérapeutes. Il y a lieu, par suite, d'enjoindre à celui-ci de procéder à ce réexamen dans un délai de deux mois à compter de la notification de la présente décision. Dans les circonstances de l'espèce, il n'y a pas lieu d'assortir cette injonction d'une astreinte.<br/>
<br/>
              8. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise, à ce titre, à la charge de M. A... qui n'est pas, dans la présente instance, la partie perdante. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge du Conseil national de l'ordre des masseurs-kinésithérapeutes la somme de 2000 euros que demande, à ce titre, M. A....<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : La décision du 6 octobre 2017 du Conseil national de l'ordre des masseurs-kinésithérapeutes est annulée.<br/>
Article 2 : Il est enjoint au Conseil national de l'ordre des masseurs-kinésithérapeutes de réexaminer la demande de M. A... dans un délai de deux mois à compter de la notification de la présente décision.<br/>
Article 3 : Le Conseil national de l'ordre des masseurs-kinésithérapeutes versera à M. A... une somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à M. B... A... et au Conseil national de l'ordre des masseurs-kinésithérapeutes. <br/>
Copie en sera adressée à l'université de Nice et à la ministre des solidarités et de la santé.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">55-01-02-018 PROFESSIONS, CHARGES ET OFFICES. ORDRES PROFESSIONNELS - ORGANISATION ET ATTRIBUTIONS NON DISCIPLINAIRES. QUESTIONS PROPRES À CHAQUE ORDRE PROFESSIONNEL. - RECONNAISSANCE D'UN DIPLÔME PAR LE CONSEIL NATIONAL DE L'ORDRE - CARACTÈRE INSUFFISAMMENT ÉPROUVÉ OU NON CONFORME AUX DONNÉES ACTUELLES DE LA SCIENCE DE TECHNIQUES ENSEIGNÉES DANS UNE FORMATION - CIRCONSTANCE SUSCEPTIBLE DE JUSTIFIER LE REFUS DE RECONNAISSANCE - EXISTENCE, SOUS RÉSERVE DE TENIR COMPTE DE LA PLACE RELATIVE DÉVOLUE AUX TECHNIQUES EN CAUSE DANS LA FORMATION ET DES MODALITÉS DE LA PRÉSENTATION PRÉVUE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">55-02-035 PROFESSIONS, CHARGES ET OFFICES. ACCÈS AUX PROFESSIONS. - RECONNAISSANCE D'UN DIPLÔME PAR LE CONSEIL NATIONAL DE L'ORDRE - CARACTÈRE INSUFFISAMMENT ÉPROUVÉ OU NON CONFORME AUX DONNÉES ACTUELLES DE LA SCIENCE DE TECHNIQUES ENSEIGNÉES DANS UNE FORMATION - CIRCONSTANCE SUSCEPTIBLE DE JUSTIFIER LE REFUS DE RECONNAISSANCE - EXISTENCE, SOUS RÉSERVE DE TENIR COMPTE DE LA PLACE RELATIVE DÉVOLUE AUX TECHNIQUES EN CAUSE DANS LA FORMATION ET DES MODALITÉS DE LA PRÉSENTATION PRÉVUE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">61-035 SANTÉ PUBLIQUE. PROFESSIONS MÉDICALES ET AUXILIAIRES MÉDICAUX. - RECONNAISSANCE D'UN DIPLÔME PAR LE CONSEIL NATIONAL DE L'ORDRE - CARACTÈRE INSUFFISAMMENT ÉPROUVÉ OU NON CONFORME AUX DONNÉES ACTUELLES DE LA SCIENCE DE TECHNIQUES ENSEIGNÉES DANS UNE FORMATION - CIRCONSTANCE SUSCEPTIBLE DE JUSTIFIER LE REFUS DE RECONNAISSANCE - EXISTENCE, SOUS RÉSERVE DE TENIR COMPTE DE LA PLACE RELATIVE DÉVOLUE AUX TECHNIQUES EN CAUSE DANS LA FORMATION ET DES MODALITÉS DE LA PRÉSENTATION PRÉVUE.
</SCT>
<ANA ID="9A"> 55-01-02-018 Si le caractère insuffisamment éprouvé ou non conforme aux données actuelles de la science de techniques enseignées dans une formation est susceptible de justifier légalement le refus de reconnaissance d'un diplôme sanctionnant une formation de masso-kinésithérapie, l'appréciation du Conseil national de l'ordre dans l'exercice de la compétence qui lui est conférée par les articles R. 4321-122 et R. 4321-123 du code de la santé publique doit cependant tenir compte de la place relative dévolue aux techniques en cause dans la formation et des modalités de la présentation qui est prévue, la présentation des caractéristiques et des risques de certaines techniques encore peu éprouvées n'étant pas nécessairement à exclure de la formation des praticiens.</ANA>
<ANA ID="9B"> 55-02-035 Si le caractère insuffisamment éprouvé ou non conforme aux données actuelles de la science de techniques enseignées dans une formation est susceptible de justifier légalement le refus de reconnaissance d'un diplôme sanctionnant une formation de masso-kinésithérapie, l'appréciation du Conseil national de l'ordre dans l'exercice de la compétence qui lui est conférée par les articles R. 4321-122 et R. 4321-123 du code de la santé publique doit cependant tenir compte de la place relative dévolue aux techniques en cause dans la formation et des modalités de la présentation qui est prévue, la présentation des caractéristiques et des risques de certaines techniques encore peu éprouvées n'étant pas nécessairement à exclure de la formation des praticiens.</ANA>
<ANA ID="9C"> 61-035 Si le caractère insuffisamment éprouvé ou non conforme aux données actuelles de la science de techniques enseignées dans une formation est susceptible de justifier légalement le refus de reconnaissance d'un diplôme sanctionnant une formation de masso-kinésithérapie, l'appréciation du Conseil national de l'ordre dans l'exercice de la compétence qui lui est conférée par les articles R. 4321-122 et R. 4321-123 du code de la santé publique doit cependant tenir compte de la place relative dévolue aux techniques en cause dans la formation et des modalités de la présentation qui est prévue, la présentation des caractéristiques et des risques de certaines techniques encore peu éprouvées n'étant pas nécessairement à exclure de la formation des praticiens.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
