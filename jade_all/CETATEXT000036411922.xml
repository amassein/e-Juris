<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036411922</ID>
<ANCIEN_ID>JG_L_2017_12_000000410441</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/41/19/CETATEXT000036411922.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème - 7ème chambres réunies, 28/12/2017, 410441, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-12-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>410441</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème - 7ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP SPINOSI, SUREAU</AVOCATS>
<RAPPORTEUR>M. Clément Malverti</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Guillaume Odinet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:410441.20171228</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme G...T..., M. B...H..., Mme M...U..., Mme A...O..., M. C...E..., Mme D...I..., Mme N...S..., M. K...X..., Mme L...W..., M. P...Q..., M. V...F...et M. J... R...ont demandé au tribunal administratif de Montreuil d'annuler l'arrêté du 26 novembre 2015 par lequel le préfet de la Seine-Saint-Denis a ordonné la perquisition administrative des locaux situés au 35/37 rue Baudin au Pré-Saint-Gervais, de leurs dépendances privatives ainsi que de trois véhicules.<br/>
<br/>
              Par un jugement n° 1600529 du 4 octobre 2016, le tribunal administratif de Montreuil a rejeté cette demande.<br/>
<br/>
              Par un arrêt n° 16VE03486 du 14 mars 2017, la cour administrative d'appel de Versailles a, sur appel de Mme T...et autres, annulé ce jugement ainsi que l'arrêté du 26 novembre 2015.<br/>
<br/>
              Par un pourvoi, enregistré au secrétariat du contentieux du Conseil d'Etat le 11 mai 2017, le ministre de l'intérieur demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de Mme T...et autres.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la loi n° 55-385 du 3 avril 1955 ;<br/>
              - la loi n° 79-587 du 11 juillet 1979 ; <br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - la loi n° 2015-1501 du 20 novembre 2015 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Clément Malverti, auditeur, <br/>
<br/>
              - les conclusions de M. Guillaume Odinet, rapporteur public,<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Spinosi, Sureau, avocat de Mme T...et autres ; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1.	Considérant qu'il ressort des énonciations de l'arrêt attaqué que, par un arrêté du 26 novembre 2015, le préfet de la Seine-Saint-Denis a ordonné, sur le fondement de l'article 11 de la loi du 3 avril 1955 relative à l'état d'urgence, la perquisition de locaux situés au 35/37 rue Baudin au Pré-Saint-Gervais, de leurs dépendances privatives ainsi que de trois véhicules ; que Mme T...et autres ont demandé au tribunal administratif de Montreuil l'annulation de cet arrêté ; que, par un jugement du 4 octobre 2016, le tribunal a rejeté cette demande ; que, par un arrêt du 14 mars 2017, la cour administrative de Versailles a annulé ce jugement ainsi que l'arrêté du 26 novembre 2015 ; que le ministre de l'intérieur se pourvoit en cassation contre cet arrêt ;<br/>
              2.	Considérant, d'une part, qu'aux termes de l'article 11 de la loi du 3 avril 1955, dans sa rédaction applicable à la date de l'arrêté contesté : " I. - Le décret déclarant ou la loi prorogeant l'état d'urgence peut, par une disposition expresse, conférer aux autorités administratives mentionnées à l'article 8 le pouvoir d'ordonner des perquisitions en tout lieu, y compris un domicile, de jour et de nuit, sauf dans un lieu affecté à l'exercice d'un mandat parlementaire ou à l'activité professionnelle des avocats, des magistrats ou des journalistes, lorsqu'il existe des raisons sérieuses de penser que ce lieu est fréquenté par une personne dont le comportement constitue une menace pour la sécurité et l'ordre publics. / La décision ordonnant une perquisition précise le lieu et le moment de la perquisition (...) " ;<br/>
<br/>
              3.	Considérant, d'autre part, qu'aux termes de l'article 1er de la loi du 11 juillet 1979, applicable au litige et désormais codifié à l'article L. 211-2 du code des relations entre le public et l'administration : " Les personnes physiques ou morales ont le droit d'être informées sans délai des motifs des décisions administratives individuelles défavorables qui les concernent. / A cet effet, doivent être motivées les décisions qui : / - restreignent l'exercice des libertés publiques ou, de manière générale, constituent une mesure de police (...) " ; que l'article 4 de cette loi, désormais codifié à l'article L. 211-6 du code des relations entre le public et l'administration, prévoit que : " Lorsque l'urgence absolue a empêché qu'une décision soit motivée, le défaut de motivation n'entache pas d'illégalité cette décision (...) " ;<br/>
<br/>
              4.	Considérant que les décisions qui ordonnent des perquisitions sur le fondement de l'article 11 de la loi du 3 avril 1955 présentent le caractère de décisions administratives individuelles défavorables qui constituent des mesures de police ; qu'elles doivent, par suite, être motivées en application de l'article 1er de la loi du 11 juillet 1979 relative à la motivation des actes administratifs et à l'amélioration des relations entre l'administration et le public ; que si les dispositions de l'article 4 de la loi du 11 juillet 1979 prévoient qu'une absence complète de motivation n'entache pas d'illégalité une décision lorsqu'une urgence absolue a empêché qu'elle soit motivée, il appartient au juge administratif d'apprécier au cas par cas, en fonction des circonstances particulières de chaque espèce, si une urgence absolue a fait obstacle à ce que la décision comporte une motivation même succincte ;<br/>
<br/>
              5.	Considérant que, pour prononcer l'annulation pour excès de pouvoir de l'arrêté du 26 novembre 2015 qui avait ordonné la perquisition au motif qu'il n'était pas suffisamment motivé, la cour administrative d'appel a notamment relevé que le préfet de la Seine-Saint-Denis ne faisait état d'aucune circonstance particulière caractérisant une situation d'urgence absolue de nature à faire obstacle à ce que l'arrêté comportât une motivation même succincte ; que l'unique moyen du pourvoi du ministre vise à remettre en cause l'appréciation qui a été portée par la cour administrative d'appel quant à l'existence d'une telle situation d'urgence absolue ; que cette appréciation souveraine, exempte de dénaturation, ne peut toutefois être remise en cause par le juge cassation ; <br/>
<br/>
              6.	Considérant qu'il résulte de ce qui précède que le ministre de l'intérieur n'est pas fondé à demander l'annulation de l'arrêt qu'il attaque ; <br/>
<br/>
              7.	Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 3 000 euros à verser à la SCP Spinosi, Sureau, avocat de Mme T... et autres, au titre des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991, sous réserve que cette société renonce à percevoir la somme correspondant à la part contributive de l'Etat ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi du ministre de l'intérieur est rejeté.<br/>
<br/>
Article 2 : L'Etat versera à la SCP Spinosi, Sureau, avocat de Mme T...et autres, une somme de 3 000 euros au titre des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991, sous réserve que cette société renonce à percevoir la somme correspondant à la part contributive de l'Etat.<br/>
<br/>
Article 3 : La présente décision sera notifiée au ministre d'Etat, ministre de l'intérieur et à Mme G...T..., premier défendeur désigné.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
