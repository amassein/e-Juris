<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044611383</ID>
<ANCIEN_ID>JG_L_2021_12_000000447510</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/61/13/CETATEXT000044611383.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème chambre, 28/12/2021, 447510, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-12-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>447510</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GATINEAU, FATTACCINI, REBEYROL</AVOCATS>
<RAPPORTEUR>Mme Myriam Benlolo Carabot</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Laurent Domingo</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:447510.20211228</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. C... D... et M. E... A... ont demandé au tribunal administratif de Paris de réduire la cotisation primitive d'impôt sur le revenu à laquelle ils ont été assujettis au titre de l'année 2013. Par un jugement n° 1718817/2-3 du 14 février 2019, le tribunal administratif de Paris a rejeté leur demande.<br/>
<br/>
              Par un arrêt n° 19PA01289 du 13 octobre 2020, la cour administrative d'appel de Paris a rejeté l'appel formé par M. D... et M. A... contre ce jugement.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 11 décembre 2020, 15 février 2021 et 20 septembre 2021 au secrétariat du contentieux du Conseil d'Etat, MM. D... et A... demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à leurs conclusions d'appel ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 4 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - le code du travail ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Myriam Benlolo Carabot, maître des requêtes en service extraordinaire  <br/>
<br/>
              - les conclusions de M. Laurent Domingo, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Gatineau, Fattaccini, Rebeyrol, avocat de M. D... et M. A... ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que M. D..., qui exerçait les fonctions de directeur des ressources humaines Europe et France et de directeur juridique France de la société Dentsply France, a été licencié le 28 octobre 2013 pour inaptitude physique et impossibilité de reclassement professionnel. Après avoir contesté les motifs de son licenciement, il a conclu le 18 novembre 2013 un accord transactionnel avec son employeur prévoyant le versement d'une indemnité conventionnelle de licenciement. Après avoir déclaré une somme de 124 511 euros en revenus exceptionnels au titre de l'année 2013, il a sollicité une réduction de son imposition au motif que l'indemnité transactionnelle qui lui avait été versée entrait dans le champ de l'exonération d'impôt sur le revenu instituée par les dispositions de l'article 80 duodecies du code général des impôts. M. D... et M. A..., soumis à imposition commune par l'effet d'un pacte civil de solidarité, se pourvoient en cassation contre l'arrêt de la cour administrative d'appel de Paris du 13 octobre 2020 rejetant leur appel contre le jugement du 14 février 2019 par lequel le tribunal administratif de Paris a rejeté leur demande tendant à la réduction de leur cotisation d'impôt sur le revenu de l'année 2013.<br/>
<br/>
              2. En premier lieu, l'article 6 du code général des impôts dispose que " 1. Chaque contribuable est imposable à l'impôt sur le revenu, tant en raison de ses bénéfices et revenus personnels que de ceux de ses enfants et des personnes considérés comme étant à sa charge au sens des articles 196 et 196 A bis (...) / Les partenaires liés par un pacte civil de solidarité défini à l'article 515-1 du code civil font l'objet, pour les revenus visés au premier alinéa, d'une imposition commune. L'imposition est établie à leurs deux noms ". Aux termes de l'article R. 411-5 du code de justice administrative : " Sauf si elle est signée par un mandataire régulièrement constitué, la requête présentée par plusieurs personnes physiques ou morales doit comporter, parmi les signataires, la désignation d'un représentant unique. A défaut, le premier dénommé est avisé par le greffe qu'il est considéré comme le représentant mentionné à l'alinéa précédent, sauf à provoquer, de la part des autres signataires qui en informent la juridiction, la désignation d'un autre représentant unique choisi parmi eux ".<br/>
<br/>
              3. Il résulte des dispositions précitées du code général des impôts que les partenaires d'un pacte civil de solidarité, ayant la qualité de codébiteurs solidaires de l'impôt sur le revenu, sont réputés se représenter mutuellement dans les instances relatives à leur dette fiscale. Par suite, dès lors que M. D... et M. A... appartiennent au même foyer fiscal à raison du pacte civil de solidarité qui les unit, la cour n'a pas commis d'erreur de droit en jugeant que les dispositions de l'article R. 411-5 du code de justice administrative relatives aux requêtes collectives n'étaient pas applicables à leur requête devant le tribunal administratif qui avait pu, par conséquent, ne communiquer le mémoire en défense de l'administration fiscale et l'avis d'audience qu'à M. D... sans l'aviser préalablement qu'il était considéré comme représentant unique.<br/>
<br/>
              4. En deuxième lieu, aux termes de l'article 80 duodecies du code général des impôts dans sa rédaction applicable au litige : " 1. Toute indemnité versée à l'occasion de la rupture du contrat de travail constitue une rémunération imposable, sous réserve des dispositions suivantes. / Ne constituent pas une rémunération imposable : 1° Les indemnités mentionnées aux articles L. 1235-2 et L. 1235-3 et L. 1235-11 à L. 1235-13 du code du travail (...) ". Aux termes de l'article L. 1235-3 code du travail dans sa rédaction applicable au litige : " Si le licenciement d'un salarié survient pour une cause qui n'est pas réelle et sérieuse, le juge peut proposer la réintégration du salarié dans l'entreprise, avec maintien de ses avantages acquis. / Si l'une ou l'autre des parties refuse, le juge octroie une indemnité au salarié. Cette indemnité, à la charge de l'employeur, ne peut être inférieure aux salaires des six derniers mois. Elle est due sans préjudice, le cas échéant, de l'indemnité de licenciement prévue à l'article L. 1234 9 ". Aux termes de l'article R. 194-1 du livre des procédures fiscales : " Lorsque, ayant donné son accord à la rectification ou s'étant abstenu de répondre dans le délai légal à la proposition de rectification, le contribuable présente cependant une réclamation faisant suite à une procédure contradictoire de rectification, il peut obtenir la décharge ou la réduction de l'imposition, en démontrant son caractère exagéré. / Il en est de même lorsqu'une imposition a été établie d'après les bases indiquées dans la déclaration souscrite par un contribuable ou d'après le contenu d'un acte présenté par lui à la formalité de l'enregistrement ".<br/>
<br/>
              5. Pour déterminer si une indemnité versée en exécution d'une transaction conclue à l'occasion de la rupture d'un contrat de travail est imposable, il appartient à l'administration et, lorsqu'il est saisi, au juge de l'impôt de rechercher la qualification à donner aux sommes qui font l'objet de la transaction. Ces dernières ne sont susceptibles d'être regardées comme des indemnités pour licenciement sans cause réelle et sérieuse mentionnées à l'article L. 1235-3 du code du travail que s'il résulte de l'instruction que la rupture des relations de travail est assimilable à un tel licenciement. Dans ce cas, les indemnités, accordées au titre d'un licenciement sans cause réelle et sérieuse, sont exonérées d'imposition. La détermination par le juge de la nature des indemnités se fait au vu de l'instruction. Les dispositions de l'article R. 194-1 du livre des procédures fiscales, citées au point 4, ne sont alors pas applicables au contribuable.<br/>
<br/>
              6.  Il ressort des énonciations de l'arrêt attaqué que si la cour a mentionné qu'il incombait aux contribuables, en application de l'article R. 194-1 du livre des procédures fiscales, d'apporter la preuve du caractère exagéré des impositions en litige au motif que celles-ci avaient été établies conformément à leur déclaration initiale, elle a toutefois recherché, au vu de l'instruction, si les éléments produits laissaient présumer une absence de cause réelle et sérieuse du licenciement de M. D.... Par suite, le moyen tiré de l'erreur de droit commise par la cour au regard des règles d'administration de la preuve doit être écarté.<br/>
<br/>
              7. En dernier lieu, il ressort des pièces du dossier soumis aux juges du fond que M. D..., avant son licenciement pour inaptitude physique en octobre 2013, avait été en arrêt de maladie ou en mi-temps thérapeutique depuis le début de l'année 2010. Si M. D... a pu être sollicité par ses supérieurs pendant certains de ses arrêts de travail au cours de l'année 2010, ni le certificat médical du 17 juillet 2013, ni l'avis du médecin du travail du 3 octobre 2013 ne comporte d'élément justifiant de ce que l'inaptitude physique de M. D... aurait eu pour origine des faits de harcèlement moral commis par son employeur. Il s'ensuit qu'en jugeant, par une appréciation souveraine non entachée de dénaturation, qu'il ne résultait pas de l'instruction que la déclaration d'inaptitude physique dont M. D... a fait l'objet en octobre 2013 aurait résulté de faits constitutifs d'un harcèlement de telle sorte que son licenciement serait dépourvu de cause réelle et sérieuse, la cour n'a pas entaché son arrêt d'erreur de qualification juridique des faits.<br/>
<br/>
              8. Il résulte de tout ce qui précède que MM. D... et A... ne sont pas fondés à demander l'annulation de l'arrêt qu'ils attaquent. Dès lors, les conclusions qu'ils ont présentées au titre de l'article L. 761-1 du code de justice administrative ne peuvent qu'être rejetées.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de MM. D... et A... est rejeté. <br/>
Article 2 : La présente décision sera notifiée à M. C... D..., à M. E... A... et au ministre de l'économie, des finances et de la relance.<br/>
              Délibéré à l'issue de la séance du 15 décembre 2021 où siégeaient : M. Bertrand Dacosta, président de chambre, présidant ; Mme Nathalie Escaut, conseillère d'Etat et Mme Myriam Benlolo Carabot, maître des requêtes en service extraordinaire-rapporteure.<br/>
<br/>
              Rendu le 28 décembre 2021.<br/>
Le président : <br/>
Signé : M. Bertrand Dacosta<br/>
La rapporteure : <br/>
Signé : Mme Myriam Benlolo Carabot<br/>
La secrétaire :<br/>
Signé : Mme B... F...<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
