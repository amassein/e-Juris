<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032571715</ID>
<ANCIEN_ID>JG_L_2016_05_000000384816</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/57/17/CETATEXT000032571715.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère - 6ème chambres réunies, 20/05/2016, 384816, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-05-20</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>384816</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère - 6ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MONOD, COLIN, STOCLET</AVOCATS>
<RAPPORTEUR>M. Frédéric Puigserver</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Jean Lessi</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2016:384816.20160520</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              Par une requête et deux mémoires en réplique, enregistrés les 26 septembre 2014, 5 mars 2015 et 20 mai 2015 au secrétariat du contentieux du Conseil d'Etat, la Société nationale des chemins de fer français (SNCF), devenue SNCF Mobilités, demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir l'arrêté interministériel du 16 juillet 2014 fixant les composantes T1 définitive pour 2013 et provisionnelle pour 2014 du taux de la cotisation de la Société nationale des chemins de fer français au régime de retraite du personnel de la Société nationale des chemins de fer français ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de la sécurité sociale ;<br/>
              - le code du travail ;<br/>
              - le décret n° 2007-730 du 7 mai 2007 ;<br/>
              - le décret n° 2007-1056 du 28 juin 2007 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Frédéric Puigserver, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Jean Lessi, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Monod, Colin, Stoclet, avocat de la Société nationale des chemins de fer français ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant que les ressources du régime de retraites du personnel de la Société nationale des chemins de fer français (SNCF), devenue SNCF Mobilités, sont notamment constituées, en application de l'article 1er du décret du 28 juin 2007 relatif aux ressources de la caisse de prévoyance et de retraite du personnel de la Société nationale des chemins de fer français, du produit des cotisations dues par les agents de son cadre permanent et par cet établissement public ; qu'en vertu du I de l'article 2 du même décret, le taux de cotisation à la charge de la SNCF est la somme de deux composantes, désignées par les sigles T1 et T2 ;<br/>
<br/>
              2. Considérant qu'aux termes du premier alinéa du II de l'article 2 du décret 28 juin 2007 : " Le taux T1 est déterminé chaque année afin de couvrir, déduction faite du produit des cotisations salariales, les montants qui seraient dus si ses salariés relevaient du régime général et des régimes de retraite complémentaire mentionnés à l'article L. 921-4 du code de la sécurité sociale " ; qu'en application du 3° du même II de l'article 2, la caisse de prévoyance et de retraite du personnel de la Société nationale des chemins de fer français régie par le décret du 7 mai 2007 doit, afin de déterminer le taux T1 provisionnel, calculer chaque année, à partir des déclarations et des informations dont elle dispose au titre de l'exercice précédent, le rapport entre les montants des cotisations qui seraient dues au titre du régime général et des régimes de retraite complémentaire et l'assiette de cotisations correspondant à la rémunération des agents, et en soustraire le taux de la cotisation à la charge de ces derniers ; que la caisse soumet ensuite les " calculs conduisant à ce taux " aux ministres chargés du budget, de la sécurité sociale et des transports, lesquels, en application du 5° du II de l'article 2, fixent par arrêté le montant du taux provisionnel T1 ;<br/>
<br/>
              3. Considérant qu'en vertu du 6° du II de l'article 2 du même décret : " Le taux définitif de la cotisation à la charge de la Société nationale des chemins de fer français est calculé, une fois l'année écoulée, par la caisse, à partir des éléments d'assiette mentionnés au 1° dont elle dispose pour cette année. Il est transmis aux ministres chargés du budget, de la sécurité sociale et des transports, et est approuvé par l'arrêté mentionné au 5° " ;<br/>
<br/>
              4. Considérant qu'en application des dispositions précitées, il appartient aux ministres chargés du budget, de la sécurité sociale et des transports de déterminer, pour la fixation du taux T1, les catégories d'agents du cadre permanent de la SNCF qui, s'ils en relevaient, entreraient, de façon obligatoire, dans le champ d'application du régime de retraite complémentaire résultant de la convention collective nationale de retraite et de prévoyance des cadres du 14 mars 1947, géré par l'Association générale des institutions de retraite des cadres (AGIRC) et relevant de l'article L. 921-4 du code de la sécurité sociale ;<br/>
<br/>
              5. Considérant, d'une part, qu'aux termes de l'article 4 de la convention collective nationale de retraite et de prévoyance des cadres du 14 mars 1947 : " Le régime de prévoyance et de retraite institué par la présente Convention s'applique obligatoirement aux ingénieurs et cadres définis par les arrêtés de mise en ordre des salaires des diverses branches professionnelles ou par des conventions ou accords conclus sur le plan national ou régional en application des dispositions légales en vigueur en matière de convention collective et qui se sont substitués aux arrêtés de salaires (...) " ; qu'en vertu de l'article 4 bis de cette convention : " Pour l'application de la présente Convention, les employés, techniciens et agents de maîtrise sont assimilés aux ingénieurs et cadres visés à l'article précédent, dans les cas où ils occupent des fonctions : / a) classées par référence aux arrêtés de mise en ordre des salaires, à une cote hiérarchique brute égale ou supérieure à 300 ; / b) classées dans une position hiérarchique équivalente à celles qui sont visées au a) ci-dessus, dans des classifications d'emploi résultant de conventions ou d'accords conclus au plan national ou régional en application des dispositions légales en vigueur en matière de convention collective " ; qu'en vertu de l'article 4 ter de la convention, la prise en considération des classifications résultant de conventions ou d'accords est subordonnée à l'agrément de la commission paritaire composée des représentants des organisations signataires de la convention, qui détermine le niveau des emplois à partir duquel l'article 4 bis s'applique, de telle sorte que les catégories de bénéficiaires au titre de cet article ne soient pas modifiées par rapport à celles visées au a) ; qu'il résulte de ces stipulations que sont assimilés aux ingénieurs et cadres les employés, techniciens et agents de maîtrise qui occupent des fonctions correspondant à un certain positionnement hiérarchique, déterminé par référence aux arrêtés pris par le ministre chargé du travail, entre la Libération et la loi du 11 février 1950 relative aux conventions collectives et aux procédures de règlement des conflits collectifs de travail, en considération des fonctions exercées par les salariés, et désormais apprécié, sous le contrôle de la commission paritaire instituée par la convention du 14 mars 1947, à partir des classifications des conventions et accords collectifs de travail ; qu'ainsi, il y a lieu, pour l'application combinée des dispositions du II de l'article 2 du décret 28 juin 2007 et des stipulations de l'article 4 bis de la convention du 14 mars 1947, de se référer à la réalité des fonctions que les salariés classés dans les catégories d'emplois définies par l'entreprise ont vocation à exercer, compte tenu des responsabilités qu'elles comportent, sans nécessairement s'en tenir à la classification que l'entreprise a elle-même opérée ;<br/>
<br/>
              6. Considérant, d'autre part, que, si selon l'article 36 de l'annexe I de la convention, le régime de retraite qu'elle institue peut être étendu, à la demande de l'entreprise, à certains collaborateurs autres que ceux mentionnés aux articles 4 et 4 bis, il résulte de ce qui a été dit au point 4 qu'il y a lieu de ne tenir compte, en l'espèce, que des seuls agents du cadre permanent de la SNCF qui relèveraient, à titre obligatoire, de la convention, soit ceux qui sont mentionnés aux articles 4 et 4 bis ;<br/>
<br/>
              7. Considérant qu'il ressort des pièces du dossier, notamment de la directive de l'établissement public RH0001 relative au statut des relations collectives entre la SNCF et son personnel, que les agents relevant des catégories D et E de l'échelle des classifications en vigueur dans l'entreprise, qui s'échelonne de A à H, font partie du collège " maîtrise " et que les cadres supérieurs et les agents relevant des catégories F, G et H font partie du collège " cadres " ; <br/>
<br/>
              8. Considérant que, par une étude réalisée en 2009 à la demande de la SNCF, qui a été versée au dossier et dont les conclusions ont été confirmées par un courrier du 17 avril 2015, l'AGIRC a estimé, à partir de critères objectifs liés aux fonctions, compte tenu des responsabilités qu'elles comportent, et en procédant à des comparaisons avec des secteurs proches dans le domaine des transports, d'une part, que les agents relevant de la catégorie D ne pouvaient pas être assimilés à des cadres, au sens de l'article 4 bis de la convention collective du 14 mars 1947 et, d'autre part, que ne devraient y être assimilés, parmi les agents relevant de la catégorie E, que ceux qui ont été reconnus comme " dirigeants de proximité " et qui sont, de ce fait, éligibles à la gratification individuelle de résultat mise en place par l'entreprise ; que les fiches de poste d'agents des catégories D et E versées au dossier ne comportent pas de fonctions qui, eu égard à la nature et au niveau des responsabilités exercées, correspondraient à une position dans la hiérarchie des emplois de la SNCF permettant d'assimiler les intéressés à des ingénieurs et cadres en application de l'article 4 bis de la convention ; qu'au demeurant, le niveau des rémunérations globales des agents relevant de la catégorie D et de celles de la plupart des agents relevant de la catégorie E, telles qu'elles résultent du barème des rémunérations figurant à l'annexe 5 à la directive de l'établissement public RH0131, relative aux agents du cadre permanent de l'entreprise, confirmé par les fiches de paye versées au dossier, n'est pas davantage compatible avec l'assimilation prévue par l'article 4 bis de la convention collective du 14 mars 1947 ;<br/>
<br/>
              9. Considérant qu'il résulte de ce qui précède qu'eu égard à la nature des fonctions exercées, corroborée par le niveau des rémunérations perçues, les agents du cadre permanent relevant des catégories D ou E ne pouvaient pas légalement, pour le calcul du taux T1, être regardés comme relevant, dans leur ensemble, du régime de retraite complémentaire résultant de la convention collective nationale de retraite et de prévoyance des cadres du 14 mars 1947 ; que la circonstance que les agents contractuels de la SNCF de qualification égale ou supérieure à C cotisent au régime de retraite géré par l'AGIRC est sans influence sur le traitement des salariés du cadre permanent de l'entreprise au regard de la réglementation qui leur est applicable ; que, par suite, SNCF Mobilités est fondée à soutenir que l'arrêté attaqué est entaché d'une erreur de droit dans l'application de l'article 2 du décret du 28 juin 2007 ;<br/>
<br/>
              10. Considérant qu'il résulte de tout ce qui précède, sans qu'il soit besoin d'examiner l'autre moyen de la requête, que l'arrêté attaqué doit être annulé ;<br/>
<br/>
              11. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre la somme de 3 000 euros à la charge de l'Etat au profit de SNCF Mobilités au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêté interministériel du 16 juillet 2014 fixant les composantes T1 définitive pour 2013 et provisionnelle pour 2014 du taux de la cotisation de la Société nationale des chemins de fer français au régime de retraite du personnel de la Société nationale des chemins de fer français est annulé.<br/>
Article 2 : L'Etat versera à SNCF Mobilités la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée à SNCF Mobilités, à la ministre des affaires sociales et de la santé, à la ministre de l'environnement, de l'énergie et de la mer, chargée des relations internationales sur le climat et de la mer, et au ministre des finances et des comptes publics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
