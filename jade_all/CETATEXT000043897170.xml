<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043897170</ID>
<ANCIEN_ID>JG_L_2021_08_000000438199</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/89/71/CETATEXT000043897170.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème chambre, 03/08/2021, 438199, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-08-03</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>438199</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BUK LAMENT - ROBILLOT</AVOCATS>
<RAPPORTEUR>Mme Pauline Hot</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Stéphane Hoynck</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:438199.20210803</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
               M. D... B... a demandé au tribunal administratif de Besançon d'annuler pour excès de pouvoir la décision du 1er août 2017 par laquelle le préfet du Doubs a refusé le regroupement familial au bénéfice de son épouse, ainsi que la décision du 16 août 2017 rejetant son recours gracieux. Par un jugement n° 1702213 du 29 novembre 2018, le tribunal administratif a rejeté sa demande. <br/>
<br/>
               Par une ordonnance n° 19NC01016 du 31 octobre 2019, le premier vice-président de la cour administrative d'appel de Nancy a rejeté l'appel de M. B....<br/>
<br/>
               Par un pourvoi sommaire et un mémoire complémentaire enregistrés au secrétariat du contentieux du Conseil d'État les 3 février et 22 août 2020, M. B... demande au Conseil d'État :<br/>
               1°) d'annuler cette ordonnance ; <br/>
<br/>
               2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
               3°) de mettre à la charge de l'État la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
               Vu les autres pièces du dossier ;<br/>
<br/>
               Vu :<br/>
               - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
               - l'accord du 27 décembre 1968 relatif à la circulation, à l'emploi et au séjour en France des ressortissants algériens et de leurs familles ;<br/>
               - le code de justice administrative ;<br/>
<br/>
               Après avoir entendu en séance publique :<br/>
<br/>
               - le rapport de Mme F... A..., auditrice,  <br/>
<br/>
               - les conclusions de M. Stéphane Hoynck, rapporteur public ;<br/>
<br/>
               La parole ayant été donnée, après les conclusions, à la SCP Buk Lament - Robillot, avocat de M. B... ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
               1. Il ressort des pièces du dossier soumis aux juges du fond que M. B..., ressortissant algérien né en 1940, qui réside régulièrement en France depuis 1959 et bénéficie d'un certificat de résidence valable jusqu'au 22 avril 2023, a, le 4 octobre 2016, épousé Mme E... C..., née en Algérie en 1961 où elle réside. Par une décision du 1er août 2017, le préfet du Doubs a rejeté la demande par laquelle M. B... a sollicité le bénéfice du regroupement familial en faveur de son épouse et, par une décision du 16 août 2017, a rejeté le recours gracieux formé contre cette décision. Par une ordonnance du 31 octobre 2019, le premier vice-président de la cour administrative de Nancy a rejeté l'appel formé contre le jugement du tribunal administratif de Besançon du 29 novembre 2018 rejetant la demande de M. B... tendant à l'annulation de ces décisions du préfet. <br/>
<br/>
               2.  D'une part, aux termes de l'article 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales : " Toute personne a droit au respect de sa vie privée et familiale, de son domicile et de sa correspondance. / Il ne peut y avoir ingérence d'une autorité publique dans l'exercice de ce droit que pour autant que cette ingérence est prévue par la loi et qu'elle constitue une mesure qui, dans une société démocratique, est nécessaire à la sécurité nationale, à la sûreté publique, au bien-être économique du pays, à la défense de l'ordre et à la prévention des infractions pénales, à la protection de la santé ou de la morale, ou à la protection des droits et libertés d'autrui. ". D'autre part, aux termes de l'article 4 de l'accord franco-algérien du 27 décembre 1968 relatif à la circulation, à l'emploi et au séjour des ressortissants algériens et de leurs familles : " Les membres de la famille qui s'établissent en France sont mis en possession d'un certificat de résidence de même durée de validité que celui de la personne qu'ils rejoignent. / Sans préjudice des dispositions de l'article 9, l'admission sur le territoire français en vue de l'établissement des membres de famille d'un ressortissant algérien titulaire d'un certificat de résidence d'une durée de validité d'au moins un an, présent en France depuis au moins un an sauf cas de force majeure, et l'octroi du certificat de résidence sont subordonnés à la délivrance de l'autorisation de regroupement familial par l'autorité française compétente (...) ".<br/>
<br/>
               3. Il ressort des pièces du dossier soumis aux juges du fond que M. B..., qui vit régulièrement en France depuis plus de 60 ans et dont les enfants et petits-enfants vivent également en France, a demandé le bénéfice du regroupement familial au profit de son épouse quelques mois après leur mariage, puis, dans l'attente d'une réponse à cette demande, lui a rendu visite à plusieurs reprises en Algérie avant que son état de santé ne se dégrade fortement et a ensuite continué à communiquer avec elle par téléphone. Par suite, en estimant que les décisions par lesquelles le préfet du Doubs a rejeté la demande de M. B... tendant au bénéfice du regroupement familial en faveur de son épouse ne portaient pas au droit au respect de sa vie privée et familiale une atteinte disproportionnée aux buts en vue desquels elle avait été prise, le premier vice-président de la cour administrative d'appel de Nancy a entaché l'ordonnance attaquée d'une erreur de qualification juridique. Par suite, et sans qu'il soit besoin de se prononcer sur les autres moyens du pourvoi, M. B... est fondé à demander l'annulation de cette ordonnance.<br/>
<br/>
               4. Il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond, en application des dispositions de l'article L. 821-2 du code de justice administrative. <br/>
<br/>
               5. Il résulte de ce qui a été dit ci-dessus que les décisions du préfet du Doubs portent une atteinte disproportionnée au droit au respect de la vie familiale de M. B... garanti par l'article 8 de la Convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales. Par suite, M. B... est fondé à soutenir que c'est à tort que, par le jugement du 29 novembre 2018, le tribunal administratif de Besançon a rejeté sa demande tendant à l'annulation de ces décisions. <br/>
<br/>
               6. Cette annulation implique nécessairement, compte tenu de son motif et en l'absence au dossier de tout élément indiquant que la situation du requérant se serait modifiée, en droit ou en fait, depuis l'intervention de la décision attaquée, la délivrance à M. B... d'une autorisation de regroupement familial au profit de Mme C.... Il y a ainsi lieu, en application des dispositions de l'article L. 911-1 du code de justice administrative, d'enjoindre au ministre de l'intérieur de faire délivrer cette autorisation à l'intéressé dans le délai d'un mois à compter de la notification de la présente décision.<br/>
<br/>
               7. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat le versement à M. B... d'une somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.  <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'ordonnance du 31 octobre 2019 du premier président de la cour administrative d'appel de Nancy est annulée. <br/>
Article 2 : Le jugement du 29 novembre 2018 du tribunal administratif de Besançon et les décisions des 1er et 16 août 2017 du préfet du Doubs sont annulées. <br/>
Article 3 : Il est enjoint au ministre de l'intérieur de faire délivrer à M. B... une autorisation de regroupement familial au profit de Mme C..., son épouse, dans le délai d'un mois suivant la notification de la présente décision. <br/>
Article 4 : L'Etat versera à M. B... la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
Article 5 : La présente décision sera notifiée à M. D... B... et au ministre de l'intérieur. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
