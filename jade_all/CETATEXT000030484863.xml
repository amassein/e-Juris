<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030484863</ID>
<ANCIEN_ID>JG_L_2015_04_000000368135</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/48/48/CETATEXT000030484863.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème / 10ème SSR, 15/04/2015, 368135, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-04-15</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>368135</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème / 10ème SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DELAPORTE, BRIARD, TRICHET</AVOCATS>
<RAPPORTEUR>Mme Séverine Larere</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emilie Bokdam-Tognetti</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:368135.20150415</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
Procédure contentieuse antérieure<br/>
<br/>
              La société Agapes a demandé au tribunal administratif de Cergy-Pontoise de prononcer la réduction des cotisations d'impôt sur les sociétés, de contributions additionnelles à cet impôt et de contributions sociales auxquelles elle a été assujettie au titre des années 2005, 2006 et 2007, à hauteur, respectivement, des sommes de 365 512 euros, 105 158 euros et 464 688 euros. Par un jugement n° 0809608 et 0902754 du 14 octobre 2010, le tribunal administratif de Montreuil, auquel ces demandes ont été transférées, les a rejetées.<br/>
<br/>
              Par un arrêt n° 10VE04169 du 26 février 2013, la cour administrative d'appel de Versailles a rejeté l'appel formé par la société Agapes contre ce jugement. <br/>
<br/>
Procédure devant le Conseil d'Etat<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 29 avril et 29 juillet 2013 au secrétariat du contentieux du Conseil d'Etat, la société Agapes demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 10VE04169 du 26 février 2013 de la cour administrative d'appel de Versailles ;<br/>
<br/>
              2°) de surseoir à statuer sur le litige jusqu'à ce que la Cour de justice de l'Union européenne se soit prononcée sur la question qui lui sera transmise ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 15 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le traité instituant la Communauté européenne ;<br/>
              - le traité sur le fonctionnement de l'Union européenne ;<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Séverine Larere, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Emilie Bokdam-Tognetti, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Delaporte, Briard, Trichet, avocat de la société Agapes ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que la société Agapes, société française, mère du groupe fiscal intégré Agapes restauration, a demandé à l'administration fiscale, en 2007 et 2008, l'imputation sur le résultat d'ensemble du groupe, au titre des années 2005 à 2007, des pertes subies, au titre des exercices 2000 à 2002, par sa filiale polonaise Agapes Polska et par sa sous-filiale italienne Flunch Italie et, en conséquence, la restitution des cotisations d'impôt sur les sociétés correspondantes, en faisant valoir qu'en application des législations polonaise et italienne, ces sociétés ne pouvaient plus reporter leurs pertes respectives sur leurs propres résultats ; que l'administration a rejeté ces réclamations en se fondant sur les dispositions de l'article 223 A du code général des impôts qui réservent le régime d'intégration fiscale aux seuls sociétés et établissements soumis à l'impôt sur les sociétés en France ; que, par un jugement du 14 octobre 2010, le tribunal administratif de Montreuil a rejeté les demandes de décharge formées par la société Agapes ; que, par l'arrêt attaqué du 26 février 2013 la cour administrative d'appel de Versailles a confirmé ce jugement ;<br/>
<br/>
              Sur les motifs de l'arrêt relatifs à la régularité du jugement du tribunal administratif :<br/>
<br/>
              2. Considérant, en premier lieu, qu'aux termes de l'article R. 611-1 du code de justice administrative : " (...) La requête, le mémoire complémentaire annoncé dans la requête et le premier mémoire de chaque défendeur sont communiqués aux parties avec les pièces jointes dans les conditions prévues aux articles R. 611-3, R. 611-5 et R. 611-6. / Les répliques, autres mémoires et pièces sont communiqués s'ils contiennent des éléments nouveaux " ; que l'article R. 613-3 de ce code prévoit que : " Les mémoires produits après la clôture de l'instruction ne donnent pas lieu à communication et ne sont pas examinés par la juridiction (...) " ;<br/>
<br/>
              3. Considérant que, devant les juridictions administratives et dans l'intérêt d'une bonne justice, le juge a toujours la faculté de rouvrir l'instruction, qu'il dirige, lorsqu'il est saisi d'une production postérieure à la clôture de celle-ci ; qu'il lui appartient, dans tous les cas, de prendre connaissance de cette production avant de rendre sa décision et de la viser ; que, s'il décide d'en tenir compte, il rouvre l'instruction et soumet au débat contradictoire les éléments contenus dans cette production qu'il doit, en outre, analyser ; que, dans le cas particulier où cette production contient l'exposé d'une circonstance de fait ou d'un élément de droit dont la partie qui l'invoque n'était pas en mesure de faire état avant la clôture de l'instruction et qui est susceptible d'exercer une influence sur le jugement de l'affaire, le juge doit alors en tenir compte, à peine d'irrégularité de sa décision ;<br/>
<br/>
              4. Considérant qu'après avoir relevé, par des motifs non contestés de son arrêt, qu'un nouveau mémoire avait été produit par l'administration, le 27 septembre 2010, soit postérieurement à la clôture de l'instruction, la cour a estimé que l'administration fiscale se bornait à reprendre dans ce mémoire l'argumentation qu'elle avait précédemment développée sans introduire d'éléments nouveaux ; qu'elle a pu, dès lors, en déduire sans erreur de droit que le tribunal avait fait une exacte application des règles prévues à l'article R. 613-3 précité du code de justice administrative en se bornant à le viser sans l'analyser et n'avait pas méconnu le principe du contradictoire en statuant sans rouvrir l'instruction pour communiquer ce mémoire à la société Agapes ;<br/>
<br/>
              5. Considérant, en second lieu, qu'il ressort des écritures produites par la société Agapes devant le tribunal administratif que celle-ci a sollicité en première instance, outre la jonction des demandes dont elle avait saisi le tribunal, d'une part, l'annulation des décisions de rejet de ses réclamations préalables, d'autre part, la restitution des impositions acquittées au titre des exercices 2005, 2006 et 2007, à hauteur, respectivement, des sommes de 365 512 euros, 105 158 euros et 464 688 euros et, enfin, l'allocation de sommes au titre de l'article L. 761-1 du code de justice administrative ; qu'il ressort du jugement du 14 octobre 2010 que le tribunal administratif a statué sur l'ensemble de ces conclusions ; que la cour n'a, par suite, ni dénaturé les pièces du dossier et les termes de ce jugement, ni insuffisamment motivé son arrêt en jugeant que le tribunal n'avait omis de répondre à aucune des conclusions de la société ; <br/>
<br/>
              Sur les motifs de l'arrêt relatifs au bien-fondé des impositions contestées :<br/>
<br/>
              6. Considérant que l'article 43 du traité instituant la Communauté européenne, devenu l'article 49 du traité sur le fonctionnement de l'Union européenne stipule : " Dans le cadre des dispositions visées ci-après, les restrictions à la liberté d'établissement des ressortissants d'un État membre dans le territoire d'un autre État membre sont interdites. Cette interdiction s'étend également aux restrictions à la création d'agences, de succursales ou de filiales, par les ressortissants d'un État membre établis sur le territoire d'un État membre. La liberté d'établissement comporte l'accès aux activités non salariées et leur exercice, ainsi que la constitution et la gestion d'entreprises, et notamment de sociétés au sens de l'article 48, deuxième alinéa, dans les conditions définies par la législation du pays d'établissement pour ses propres ressortissants, sous réserve des dispositions du chapitre relatif aux capitaux " ; que, selon l'article 48 du même traité, devenu l'article 54 du traité sur le fonctionnement de l'Union européenne : "Les sociétés constituées en conformité de la législation d'un État membre et ayant leur siège statutaire, leur administration centrale ou leur principal établissement à l'intérieur de la Communauté sont assimilées, pour l'application des dispositions du présent chapitre, aux personnes physiques ressortissantes des États membres " ; que ces stipulations, telles qu'interprétées par la Cour de justice de l'Union européenne, notamment dans son arrêt du 25 février 2010 X Holding BV (affaire 337/08), ne s'opposent pas à la législation d'un Etat membre qui ouvre la possibilité, pour une société mère, de constituer une entité fiscale unique avec ses filiales résidentes, mais font obstacle à la constitution d'une entité fiscale unique avec une filiale non-résidente, dès lors que les bénéfices de cette dernière ne sont pas soumis à la loi fiscale de cet Etat membre ;<br/>
<br/>
              7. Considérant que pour l'application du dispositif d'intégration fiscale prévu à l'article 223 A du code général des impôts, seuls peuvent être membres du groupe les sociétés et établissements dont les résultats sont soumis à l'impôt sur les sociétés en France ; qu'une société mère ne peut imputer les pertes subies par une filiale sur le résultat d'ensemble du groupe fiscal intégré qu'en application de ces dispositions ; qu'il résulte de ce qui a été dit au point 6 que ces dispositions ne sont pas incompatibles avec les stipulations mentionnées au même point ; <br/>
<br/>
              8. Considérant que la société requérante soutient qu'en refusant l'imputation des pertes des sociétés Flunch Italie et Agapes Polska sur le résultat d'ensemble du groupe fiscalement intégré dont la société Agapes est la mère, la cour a méconnu les stipulations citées au point 6 et ainsi porté atteinte à la liberté d'établissement, sans que cette entrave soit justifiée par un motif impérieux d'intérêt général, dès lors que les pertes ne peuvent être déduites par les filiales dans leur pays de résidence, en vertu des législations applicables dans ces pays ; que, toutefois, il n'incombe pas à l'Etat de résidence de la société mère d'assurer la neutralisation de la charge fiscale que la société filiale supporte ou supportera du fait de la décision de l'Etat membre où elle réside elle-même, au titre de l'exercice de sa compétence fiscale, de limiter le droit d'imputer les pertes subies ; que, par suite, en écartant le moyen tiré de ce que le refus d'imputer les pertes des sociétés Flunch Italie et Agapes Polska sur le résultat d'ensemble du groupe fiscalement intégré dont la société Agapes est la mère méconnaîtrait les stipulations des articles 43 et 48 du traité instituant la Communauté européenne, devenus les articles 49 et 54 du traité sur le fonctionnement de l'Union européenne, la cour n'a pas commis d'erreur de droit ;<br/>
<br/>
              9. Considérant qu'il résulte de ce qui précède, et sans qu'il soit besoin de saisir la Cour de justice de l'Union européenne d'une question préjudicielle, que la société Agapes n'est pas fondée à demander l'annulation de l'arrêt qu'elle attaque ; que ses conclusions tendant à ce qu'une somme soit mise à la charge de l'Etat au titre de l'article L. 761-1 du code de justice administrative ne peuvent, par suite, qu'être rejetées ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : Le pourvoi de la société Agapes est rejeté.<br/>
Article 2  : La présente décision sera notifiée à la société Agapes et au ministre des finances et des comptes publics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
