<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038077347</ID>
<ANCIEN_ID>JG_L_2019_01_000000420426</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/07/73/CETATEXT000038077347.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème chambre, 30/01/2019, 420426, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-01-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>420426</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP CELICE, SOLTNER, TEXIDOR, PERIER</AVOCATS>
<RAPPORTEUR>M. Laurent-Xavier Simonel</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Laurent Cytermann</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2019:420426.20190130</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              La société civile immobilière (SCI) CG a demandé au tribunal administratif de Nancy d'annuler la décision par laquelle la communauté de communes de l'agglomération de Longwy a implicitement rejeté sa demande tendant à la suppression d'un remblai et d'une route implantés sur sa propriété ainsi qu'à la remise en état de sa clôture et de ses espaces verts et de condamner la communauté de communes à lui verser une somme de 24 325,06 euros en réparation de ses préjudices et une indemnité mensuelle d'occupation jusqu'à la libération des lieux.<br/>
<br/>
              Par un jugement n° 1501995 du 7 juin 2016, le tribunal administratif de Nancy a condamné la communauté de communes de l'agglomération de Longwy à verser à la SCI CG une somme de 3 803 euros en réparation de ses préjudices et a rejeté le surplus de ses conclusions.<br/>
<br/>
              Par un arrêt n° 16NC01629 du 8 mars 2018, la cour administrative d'appel de Nancy, sur appel de la SCI CG, a réformé ce jugement et a condamné la communauté de communes de l'agglomération de Longwy à verser à la SCI CG une somme de 8 469,50 euros sous déduction des sommes déjà versées et a rejeté le surplus de ses conclusions d'appel.<br/>
<br/>
              Par un pourvoi et un mémoire complémentaire, enregistrés les 7 mai et 7 août 2018 au secrétariat du contentieux du Conseil d'Etat, la SCI CG demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ; <br/>
<br/>
              3°) de mettre à la charge de la communauté de communes de l'agglomération de Longwy la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Laurent-Xavier Simonel, conseiller d'Etat en service extraordinaire, <br/>
<br/>
              - les conclusions de M. Laurent Cytermann, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Célice, Soltner, Texidor, Perier, avocat de la société civile immobilière CG ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 822-1 du code de justice administrative : " Le pourvoi en cassation devant le Conseil d'Etat fait l'objet d'une procédure préalable d'admission. L'admission est refusée par décision juridictionnelle si le pourvoi est irrecevable ou n'est fondé sur aucun moyen sérieux ". <br/>
<br/>
              2. Pour demander l'annulation de l'arrêt qu'elle attaque, la SCI CG soutient que la cour administrative d'appel de Nancy :<br/>
<br/>
              - s'agissant de la demande d'injonction, a commis une erreur de droit ou, à tout le moins, l'a insuffisamment motivé en jugeant que l'implantation irrégulière de l'ouvrage public était régularisable par une expropriation pour cause d'utilité publique, sans caractériser de façon effective et concrète qu'étaient réunies les conditions permettant de recourir à une déclaration d'utilité publique ;<br/>
              - a commis une erreur de qualification juridique des faits et une erreur de droit et a entaché son arrêt d'insuffisance de motivation en jugeant que le retrait du remblai porterait une atteinte excessive à l'intérêt général, au motif que ce remblai ne pouvait être supprimé sans compromettre la stabilité de la chaussée, sans rechercher si des solutions alternatives permettaient sa suppression ;<br/>
              - l'a entaché de contradiction de motifs en rejetant les conclusions tendant à ce qu'il soit enjoint à la communauté de communes de mettre en oeuvre une expropriation pour cause d'utilité publique, au motif qu'une solution amiable pouvait être trouvée, alors qu'elle a constaté qu'elle n'était disposée ni à céder la partie occupée de son terrain ni à accorder une servitude ;<br/>
              - a commis une erreur de droit en méconnaissant la valeur probante de la facture émise par son locataire pour les frais de déplacement de la clôture ;<br/>
              - a soulevé un moyen d'office en méconnaissance de l'article R. 611-7 du code de justice administrative et a dénaturé les pièces du dossier qui lui était soumis sur son préjudice lié à la perte de loyers pour la période du 1er juin 2008 au 1er juin 2011 ;<br/>
              - a méconnu son office et s'est méprise sur la portée de ses écritures en se refusant, après avoir rejeté ses conclusions à fins d'injonction de démolition, à les interpréter comme tendant au versement d'une indemnité réparant ses pertes mensuelles de loyer jusqu'à la régularisation de l'implantation de l'ouvrage public ;<br/>
              - a commis une erreur de droit et inexactement qualifié les faits en jugeant que la responsabilité devait être partagée entre elle et la communauté de communes.<br/>
<br/>
              3. Eu égard aux moyens soulevés, il y a lieu d'admettre les conclusions du pourvoi qui sont dirigées contre l'arrêt attaqué en tant qu'elles portent sur le préjudice résultant de la perte des loyers pour la période du 1er juin 2008 au 1er juin 2011. S'agissant du surplus des conclusions, aucun des moyens n'est de nature à justifier l'admission du pourvoi.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Les conclusions du pourvoi de la SCI CG qui sont dirigées contre l'arrêt attaqué en tant qu'elles portent sur le préjudice résultant de la perte des loyers pour la période du 1er juin 2008 au 1er juin 2011 sont admises. <br/>
Article 2 : Le surplus des conclusions du pourvoi n'est pas admis. <br/>
Article 3 : La présente décision sera notifiée à la société civile immobilière CG.<br/>
Copie en sera adressée à la communauté de communes de l'agglomération de Longwy.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
