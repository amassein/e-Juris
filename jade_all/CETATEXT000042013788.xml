<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042013788</ID>
<ANCIEN_ID>JG_L_2020_06_000000427025</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/01/37/CETATEXT000042013788.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème chambre, 17/06/2020, 427025, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-06-17</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>427025</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DIDIER, PINET ; SCP LE BRET-DESACHE</AVOCATS>
<RAPPORTEUR>M. Fabio Gennari</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Sophie Roussel</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2020:427025.20200617</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société à responsabilité limitée (SARL) Eden Caravaning, Mme A... B... et M. C... B... ont demandé au tribunal administratif de Nantes d'annuler pour excès de pouvoir l'arrêté du 16 décembre 2015 par lequel le maire de La Baule-Escoublac (Loire-Atlantique) a délivré à la SARL Jet La Baule un permis de construire pour l'édification d'un immeuble collectif de dix-huit logements, ensemble l'arrêté du 14 novembre 2017 par lequel le maire a délivré un permis modificatif à cette société. <br/>
<br/>
              Par un jugement n° 1601075 et 1710196 du 13 novembre 2018, le tribunal administratif de Nantes a rejeté cette demande. <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 14 janvier et 15 avril 2019 au secrétariat du contentieux du Conseil d'Etat, la société Eden Caravaning, Mme A... B... et M. C... B... demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ; <br/>
<br/>
              2°) de mettre à la charge de la commune de La Baule-Escoublac et de la société Jet La Baule une somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code l'urbanisme ; <br/>
              - le code de justice administrative et l'ordonnance n° 2020-305 du 25 mars 2020 ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Fabio Gennari, auditeur,<br/>
<br/>
              - les conclusions de Mme Sophie Roussel, rapporteur public,<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Didier, Pinet, avocat de la société Eden Caravaning et de M. et Mme B..., et à la SCP Le Bret-Desache, avocat de la société Jet La Baule ; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1.	Il ressort des pièces du dossier soumis aux juges du fond que la SARL Jet La Baule a bénéficié d'un permis de construire un immeuble collectif de dix-huit logements, sur un terrain situé 5 route de Ker Rivaud, délivré par le maire de La Baule-Escoublac, le 16 décembre 2015. Un permis de construire modificatif lui a été accordé par un arrêté du maire du 14 novembre 2017. La SARL Eden Caravaning et M. et Mme B..., ont demandé au tribunal administratif de Nantes l'annulation de ces arrêtés. Par un jugement du 13 novembre 2018, le tribunal administratif a rejeté leurs demandes. La SARL Eden Caravaning et M. et Mme B... se pourvoient en cassation contre ce jugement. <br/>
<br/>
              2.	En premier lieu, dans les communes littorales, aux termes du I de l'article L. 146-4 du code de l'urbanisme alors applicable, devenu l'article L. 121-8 du même code : " L'extension de l'urbanisation doit se réaliser soit en continuité avec les agglomérations et villages existants, soit en hameaux nouveaux intégrés à l'environnement. (...) ". Pour écarter le moyen tiré de la méconnaissance de ces dispositions, le tribunal administratif a relevé, au terme d'une appréciation souveraine, la proximité immédiate des constructions bordant le carrefour né du croisement de la rue où se trouve le projet avec une rue elle-même bordée de constructions le long du segment la reliant au village de Saint-Servais. En se fondant sur ces éléments pour juger que ce terrain d'assiette était situé en continuité d'un espace déjà urbanisé de l'agglomération, le tribunal administratif, qui n'avait pas à répondre à tous les arguments de la demande, en particulier celui tiré de la caractérisation de la zone d'implantation du projet, n'a pas dénaturé les pièces du dossier qui lui était soumis, ni par suite commis d'erreur de droit sur l'existence d'une continuité entre le terrain en litige et les villages existants.  <br/>
<br/>
              3.	En deuxième lieu, aux termes de l'article R. 431-9 du code de l'urbanisme : " Le projet architectural comprend également un plan de masse des constructions à édifier ou à modifier coté dans les trois dimensions. Ce plan de masse (...) indique également, le cas échéant, les modalités selon lesquelles les bâtiments ou ouvrages seront raccordés aux réseaux publics ou, à défaut d'équipements publics, les équipements privés prévus, notamment pour l'alimentation en eau et l'assainissement ". Selon l'article UB4.3 du règlement du plan local d'urbanisme de la commune de La Baule-Escoublac : " (...) Les locaux poubelles et points de collecte doivent présenter les caractéristiques suivantes : avoir une taille et une configuration conçues pour une manipulation aisée des conteneurs, y compris ceux dédiés au tri, être équipés d'un point d'eau et d'une évaluation raccordée au réseau d'assainissement s'il existe (...) ". <br/>
<br/>
              4.	Il ressort des pièces du dossier soumis aux juges du fond qu'une aire de présentation des ordures ménagères en bordure de voie publique, absente dans le permis initial, a été prévue dans la demande de permis modificatif, pour une superficie d'environ 4 m², à l'angle nord du terrain en litige. En jugeant que l'absence de description au dossier sur le rattachement de cette aire de collecte des déchets au réseau d'assainissement et sur l'existence d'un point d'eau était sans incidence sur la légalité du permis, alors le plan de masse faisait apparaître les réseaux existants et que rien ne permettait d'établir l'existence d'une quelconque difficulté technique d'équipement d'un point d'eau et de raccordement au réseau d'assainissement pour ce local, le tribunal administratif n'a pas commis d'erreurs de droit.  <br/>
<br/>
              5.	En troisième lieu, aux termes de l'article 6 du règlement de la zone UB du PLU, relatif à l'implantation des constructions par rapport aux voies et emprises publiques : " (...) 6.1. Dispositions générales : Les constructions doivent être implantées : Avec un retrait d'au moins 5 mètres par rapport à l'alignement, (...) ". En jugeant que l'angle sud du bâtiment dont l'emprise au sol a été modifiée n'était pas implanté à moins de 5 mètres de l'alignement de la voie publique, le tribunal administratif n'a, contrairement à ce qui est soutenu, pas dénaturé les pièces du dossier. <br/>
<br/>
              6.	En quatrième lieu, aux termes de l'article 12 du règlement de la zone UB du PLU, relatif au stationnement des véhicules : " 12.1. Nombre de places minimum selon les catégories de constructions : (...) Le calcul du nombre de places exigées se fait par tranche entamée. A titre d'exemple, lorsqu'il est exigé 1 place par tranche de 50 m² de surface de plancher, une construction de 80 m² de surface de plancher devra prévoir 2 places de stationnement. Lorsqu'il est exigé plus de 2 places de stationnement, celles-ci doivent être réalisées de sorte à ce qu'elles soient toutes accessibles de manière autonome. Lorsque deux places de stationnement seulement sont exigées, une des places peut être commandée. (...) Les places de stationnement doivent présenter les dimensions minimales suivantes : 5 mètres de profondeur ; et 2,50 mètres de largeur. Pour toute nouvelle construction, création d'un logement supplémentaire ou changement de destination, il est exigé au moins : pour les constructions destinées à l'habitation : 1 place par tranche de 50 m² de surface plancher, (...) ". Pour écarter le moyen tiré de la méconnaissance de ces dispositions, le tribunal a administratif a estimé, d'une part, qu'alors que ces prescriptions imposaient au pétitionnaire un minimum de 24 places, le projet en prévoyait 26 et, d'autre part, que les places prévues en sous-sol seraient d'une largeur de 2,50 mètres par stationnement, en moyenne. En jugeant ainsi alors même que toutes les places du sous-sol, au demeurant non séparées par un mur, ne mesuraient pas exactement 2,5 mètres, le tribunal administratif, qui n'a pas commis d'erreur de droit, n'a pas non plus dénaturé les pièces du dossier.<br/>
<br/>
              7.	En dernier lieu, un permis modificatif ne peut être délivré que si les modifications apportées au projet initial pour remédier au vice d'illégalité ne peuvent être regardées, par leur nature ou leur ampleur, comme remettant en cause sa conception générale. La seule circonstance que ces modifications portent sur des éléments tels que son implantation, ses dimensions ou son apparence ne fait pas, par elle-même, obstacle à ce qu'elles fassent l'objet d'un permis modificatif. En jugeant que le permis modificatif, qui modifie l'implantation du projet par la création d'un placette à cheval sur la parcelle voisine, et, principalement, élargit la rampe d'accès automobile et déplace le local vélo, crée deux places de stationnement supplémentaires, un point de collecte des déchets, pouvait faire l'objet d'un simple permis modificatif dès lors qu'aucune de ces modifications n'affectait la conception générale du projet, le tribunal administratif, qui n'a pas dénaturé les pièces du dossier, n'a pas commis d'erreur de droit. <br/>
<br/>
              8.	Il résulte de ce qui précède que la SARL Eden Caravaning et M. et Mme B... ne sont pas fondés à demander l'annulation du jugement qu'ils attaquent. <br/>
<br/>
              9.	Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de la société Jet La Baule. Il y a lieu de mettre à la charge de la SARL Eden Caravaning et de M. et Mme B... la somme de 1 500 euros chacun, à verser au titre de ces mêmes dispositions à la société Jet La Baule. <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : La requête de la SARL Eden Caravaning et de M. et Mme B... est rejetée.<br/>
<br/>
Article 2 : La SARL Eden Caravaning d'une part et M. et Mme B... d'autre part verseront chacun à la SARL Jet La Baule la somme de 1 500 euros au titre de l'article L. 761-1 du code de justice administrative.  <br/>
<br/>
Article 3 : La présente décision sera notifiée à la SARL Eden Caravaning, à Mme A... B..., à M. C... B..., à la SARL Jet La Baule et à la commune de La Baule-Escoublac.  <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
