<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038601869</ID>
<ANCIEN_ID>JG_L_2019_06_000000403413</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/60/18/CETATEXT000038601869.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème chambre, 12/06/2019, 403413, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-06-12</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>403413</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>CABINET BRIARD</AVOCATS>
<RAPPORTEUR>Mme Cécile Isidoro</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emmanuelle Cortot-Boucher</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2019:403413.20190612</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              La société à responsabilité limitée DLA a demandé au tribunal administratif de Montpellier de prononcer la décharge de la cotisation supplémentaire d'impôt sur les sociétés à laquelle elle a été assujettie au titre de l'exercice clos en 2008 ainsi que des pénalités correspondantes et le rétablissement d'un déficit reportable de 59 928 euros au titre de l'exercice clos en 2009. Par un jugement n° 1202878 du 4 juillet 2013, le tribunal administratif de Montpellier a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 13MA03618 du 7 juillet 2016, la cour administrative d'appel de Marseille a rejeté l'appel formé par la société DLA contre ce jugement.  <br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 9 septembre et 9 décembre 2016 et le 5 juin 2018, la société DLA demande au Conseil d'Etat :  <br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code général des impôts et le livre des procédures fiscales ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Cécile Isidoro, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Emmanuelle Cortot-Boucher, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, au Cabinet Briard, avocat de la société DLA ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 29 mai 2019, présentée par la société DLA. <br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que la société DLA, qui exerce une activité d'expertise comptable, a fait l'objet d'une vérification de comptabilité au titre des exercices clos en 2008 et 2009. A l'issue de ce contrôle, l'administration, après avoir, d'une part, rehaussé la valeur d'inscription à l'actif du bilan de l'exercice clos en 2008 de la clientèle acquise le 23 novembre 2007, et, d'autre part, réduit le déficit constaté au titre de l'exercice clos en 2009 du montant de loyers que la société n'avait pas facturés à sa société mère, l'a assujettie à une cotisation supplémentaire d'impôt sur les sociétés au titre de l'exercice clos en 2008 et a réduit son déficit au titre de l'exercice suivant. La société a saisi le tribunal administratif de Montpellier d'une demande tendant à la décharge de cette imposition et des pénalités correspondantes et au rétablissement du déficit. Par un jugement du 4 juillet 2013, le tribunal a rejeté la demande. La société a fait appel de ce jugement devant la cour administrative d'appel de Marseille qui a rejeté sa requête par un arrêt du 7 juillet 2016 dont elle demande l'annulation. <br/>
<br/>
              Sur la régularité de l'arrêt :<br/>
<br/>
              2. Il ressort des énonciations de l'arrêt attaqué, et notamment de ses points 4 à 6, que le moyen tiré de ce que la cour aurait omis de répondre au moyen tiré de ce que la société justifiait que la valeur d'acquisition de la clientèle du cabinet d'expertise-comptable n'était pas différente de la valeur vénale réelle de la clientèle manque en fait. <br/>
<br/>
              Sur le bien-fondé de l'arrêt :<br/>
<br/>
              En ce qui concerne le prix d'acquisition de la clientèle : <br/>
<br/>
              3. Aux termes de l'article 38 du code général des impôts, applicable en matière d'impôt sur les sociétés en vertu de l'article 209 du même code : " (...) 2. Le bénéfice net est constitué par la différence entre les valeurs de l'actif net à la clôture et à l'ouverture de la période dont les résultats doivent servir de base à l'impôt diminuée des suppléments d'apport et augmentée des prélèvements effectués au cours de cette période par l'exploitant ou par les associés. L'actif net s'entend de l'excédent des valeurs d'actif sur le total formé au passif par les créances des tiers, les amortissements et les provisions justifiés ". Aux termes du 1 de l'article 38 quinquies de l'annexe III au même code : " Les immobilisations sont inscrites au bilan pour leur valeur d'origine. / Cette valeur d'origine s'entend : / Pour les immobilisations acquises à titre onéreux, du coût d'acquisition, c'est-à-dire du prix d'achat minoré des remises, rabais commerciaux et escomptes de règlements obtenus et majoré des coûts directement engagés pour la mise en état d'utilisation du bien et des coûts d'emprunt dans les conditions prévues à l'article 38 undecies (...) ". <br/>
<br/>
              4. Il ressort des pièces du dossier soumis aux juges du fond que l'administration a constaté que la valeur d'inscription à l'actif du bilan de la société DLA de la clientèle d'un autre cabinet d'expertise comptable, acquise le 23 novembre 2007 au prix de 248 172 euros, dont 238 172 euros au titre des éléments incorporels, avait été déterminée en tenant compte d'une valeur du droit de présentation de la clientèle, fixée à 657 565 euros, dont avaient été déduites diverses sommes, et notamment celle de 260 033 euros au titre de produits constatés d'avance et celle de 29 000 euros au titre d'une provision pour congés payés. Elle a regardé ces deux sommes comme des éléments du prix de cession et estimé que ce prix avait fait l'objet d'une minoration justifiant la réintégration de ces sommes pour le calcul du résultat imposable de l'exercice clos en 2008. <br/>
<br/>
              5. En premier lieu, la cour, qui a suffisamment motivé son arrêt sur les données chiffrées du litige, n'a pas commis d'inexactitude matérielle, d'une part, en prenant en compte les deux sommes de 260 033 euros et de 29 000 euros, dès lors qu'il ressort de l'acte de cession qu'elles figuraient au paragraphe 4 comme venant en déduction de l'indemnité de présentation de clientèle et constituaient un des éléments de détermination du prix de cession, d'autre part, en retenant que la valeur de l'indemnité de présentation de clientèle avait été déterminée sur le fondement du chiffre d'affaires réalisé au cours de l'année 2007, dès lors qu'il ressortait de l'acte de cession que " ce prix a été déterminé en fonction des clients du cédant et dont la liste était annexée " et du chiffre d'affaires facturé et réalisé avec ces clients au cours de l'année 2007, soit, selon les déclarations du cédant, un montant de 939 378,42 euros, auquel était affecté un coefficient de 0,7. Elle n'a pas dénaturé les termes de l'acte de cession du 23 novembre 2007 en estimant qu'il en ressortait clairement que l'indemnité de présentation de la clientèle s'élevait à 657 565 euros, même si le prix de cession retenu par les parties était de 248 172 euros.<br/>
<br/>
              6. En deuxième lieu, la cour n'a pas commis d'erreur de droit en jugeant que la circonstance que les sommes de 260 033 euros et de 29 000 euros correspondant, d'une part, à des sommes facturées par le cédant et acquittés par ses clients à la date de la cession mais relatives à des prestations restant à exécuter par la société DLA et, d'autre part, à des charges liées aux congés payés acquis par les salariés au titre de la période au cours de laquelle ce personnel était employé par le cédant avaient été déduites dans l'acte de la valeur de présentation de la clientèle pour calculer le prix de cession de la clientèle était sans incidence sur la valeur à laquelle devait être inscrite la clientèle à l'actif de la société DLA.<br/>
<br/>
              7. En troisième lieu, la cour n'a ni commis d'erreur de droit ni méconnu l'article 38 du code général des impôts en ne tenant pas compte pour le calcul du prix de cession des chiffres d'affaires réalisés par le cédant les années antérieures à la cession ou les années postérieures à la cession par la société requérante dès lors qu'elle s'est fondée sur le seul acte de cession qui ne faisait pas état d'une autre méthode de calcul que celle qui a été décrite et qu'elle s'est bornée à juger que les sommes venant en déduction de l'indemnité de présentation de la clientèle correspondaient à des modalités de paiement du prix révélant une compensation entre les parties sans influence sur la valeur fixée pour la valeur de la clientèle. <br/>
<br/>
              En ce qui concerne les prestations de sous-traitance : <br/>
<br/>
              8. En vertu des dispositions combinées des articles 38 et 209 du code général des impôts, le bénéfice imposable à l'impôt sur les sociétés est celui qui provient des opérations de toute nature faites par l'entreprise, à l'exception de celles qui, en raison de leur objet ou de leurs modalités, sont étrangères à une gestion commerciale normale. La prise en charge par une entreprise de frais ne lui incombant pas directement ne relève d'une gestion commerciale normale que s'il apparaît qu'en consentant de tels avantages, l'entreprise a agi dans son intérêt propre. <br/>
<br/>
              9. Pour juger que la société DLA avait commis un acte anormal de gestion en 2009, la cour a relevé que l'administration faisait valoir que la société avait réalisé au profit de sa société mère des prestations de sous-traitance au titre desquelles seuls les coûts salariaux avaient été facturés, sans que soit prise en compte dans le prix la part des loyers correspondant à l'occupation par la société de locaux à Narbonne pour l'exécution de ces prestations. La cour a relevé également que la société s'était bornée à produire des factures d'honoraires émises à l'adresse de sa société mère ne mentionnant pas le détail des refacturations, ainsi que des tableaux récapitulatifs annuels retraçant le détail des sommes facturées, sans que soient connues les dates et modalités d'établissement de ces documents, et qu'elle ne justifiait pas par ces éléments que le taux horaire propre à chaque collaborateur serait identique à celui appliqué aux autres clients et inclurait une marge. La cour, qui a suffisamment motivé son arrêt sur ce point et n'a pas dénaturé les pièces qui lui étaient soumises, n'a pas inexactement qualifié les faits en jugeant que l'absence de facturation d'une somme de 3 166 euros, alors même qu'elle ne représentait qu'un faible pourcentage du montant facturé, était constitutive d'un abandon de recettes dépourvu de contrepartie.  <br/>
<br/>
              En ce qui concerne les pénalités : <br/>
<br/>
              10. En vertu de l'article 1729 du code général des impôts, les inexactitudes ou les omissions relevées dans une déclaration ou un acte comportant l'indication d'éléments à retenir pour l'assiette ou la liquidation de l'impôt entraînent l'application d'une majoration de 40 % lorsque le contribuable a délibérément entendu se soustraire à l'impôt. Aux termes de l'article L. 195 A du livre des procédures fiscales : " En cas de contestation des pénalités fiscales appliquées à un contribuable au titre des impôts directs (...), la preuve de la mauvaise foi (...) incombe à l'administration ". <br/>
<br/>
              11. En relevant, d'une part, que la valeur d'inscription du droit de présentation de la clientèle à l'actif du bilan de la société DLA avait été indûment minorée du montant des produits constatés d'avance et d'une provision pour congés payés et, d'autre part, que la société, qui exerçait une activité d'expertise-comptable, ne pouvait ignorer les règles comptables et fiscales auxquelles elle était soumise, la cour, dont l'arrêt est suffisamment motivé sur ce point, n'a pas inexactement qualifié les faits qui lui étaient soumis en jugeant que l'intention délibérée de la société d'éluder l'impôt en 2008 avait été caractérisée par l'administration.  <br/>
<br/>
              12. Il résulte de ce qui précède que la société DLA n'est pas fondée, par les moyens qu'elle invoque, à demander l'annulation de l'arrêt qu'elle attaque. Ses conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative ne peuvent, par suite, qu'être rejetées.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
<br/>
<br/>
Article 1er : Le pourvoi de la société DLA est rejeté. <br/>
Article 2 : La présente décision sera notifiée à la société à responsabilité limitée DLA et au ministre de l'action et des comptes publics. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
