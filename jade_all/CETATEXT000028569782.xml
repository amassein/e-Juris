<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028569782</ID>
<ANCIEN_ID>JG_L_2014_01_000000352808</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/56/97/CETATEXT000028569782.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème et 1ère sous-sections réunies, 29/01/2014, 352808, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-01-29</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>352808</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème et 1ère sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>FOUSSARD ; SCP NICOLAY, DE LANOUVELLE, HANNOTIN</AVOCATS>
<RAPPORTEUR>M. Samuel Gillis</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Xavier de Lesquen</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:352808.20140129</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi, enregistré le 20 septembre 2011 au secrétariat du contentieux du Conseil d'Etat, présenté pour la commune de Soignolles-en-Brie, représentée par son maire en exercice ; la commune demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler le jugement n° 1007429/4 du 30 juin 2011 du tribunal administratif de Melun en tant qu'il a annulé, à la demande de M. et MmeA..., l'arrêté du 20 août 2010 par lequel le maire de Soignolles-en-Brie a décidé de surseoir à statuer sur leur déclaration préalable relative à la division d'un terrain situé 108 rue de Cordon ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter la demande de M. et Mme A...; <br/>
<br/>
              3°) de mettre à la charge de M. et Mme A...une somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 8 janvier 2014, présentée pour M. et Mme A... ;<br/>
<br/>
              Vu le code des postes et communications électroniques ;<br/>
<br/>
              Vu le code de l'urbanisme ;<br/>
<br/>
              Vu l'arrêté du 7 février 2007 pris en application de l'article R. 2-1 du code des postes et des communications électroniques et fixant les modalités relatives au dépôt et à la distribution des envois postaux ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Samuel Gillis, Maître des Requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Xavier de Lesquen, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Foussard, avocat de la commune de Soignolles-en-Brie et à la SCP Nicolaÿ, de Lanouvelle, Hannotin, avocat de M. ou Mme A...;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant, d'une part, qu'aux termes de l'article R. 424-1 du code de l'urbanisme : " A défaut de notification d'une décision expresse dans le délai d'instruction (...) le silence gardé par l'autorité compétente vaut, selon les cas : a) Décision de non-opposition à la déclaration préalable (...) " ; qu'en vertu de l'article R. 423-23 du même code, le délai d'instruction d'une déclaration préalable est d'un mois, sauf majorations prévues par les articles R. 423-24 et suivants du même code ; <br/>
<br/>
              2. Considérant, d'autre part, qu'aux termes du premier alinéa de l'article         R. 423-10 du même code : " La décision accordant ou refusant le permis ou s'opposant au projet faisant l'objet d'une déclaration préalable est notifiée au demandeur par lettre recommandée avec demande d'avis de réception postal, ou, dans les cas prévus à l'article R. 423-48, par transmission électronique. " ; que ces dispositions s'appliquent également à la décision par laquelle l'autorité administrative sursoit à statuer sur la demande ou le projet qui lui est soumis ; que le demandeur est réputé avoir reçu notification de la décision à la date de la première présentation du courrier par lequel elle lui est adressée ;<br/>
<br/>
              3. Considérant, enfin, qu'en vertu de l'article 5 de l'arrêté du 7 février 2007, pris en application de l'article R. 2-1 du code des postes et des communications électroniques et fixant les modalités relatives au dépôt et à la distribution des envois postaux, en cas d'absence du destinataire à l'adresse indiquée par l'expéditeur lors du passage de l'employé chargé de la distribution, le prestataire de services postaux informe le destinataire que l'envoi postal est mis en instance pendant un délai de quinze jours à compter du lendemain de la présentation de l'envoi postal à son domicile ainsi que du lieu où cet envoi peut être retiré ; qu'il résulte de l'article 7 du même arrêté que le prestataire peut établir un avis de réception à la demande de l'expéditeur, retourné à ce dernier, attestant la distribution de l'envoi ; que cet avis comporte diverses informations circonstanciées, telles que la date de présentation, si l'envoi a fait l'objet d'une mise en instance conformément à l'article 5, la date de distribution et le numéro d'identification de l'envoi ; <br/>
<br/>
              4. Considérant qu'il résulte de ce qui a été dit aux points 1 et 2 que la notification d'une décision de sursis à statuer fait obstacle à la naissance d'une décision implicite de non-opposition à déclaration préalable au terme du délai d'instruction ; qu'il incombe à l'administration, lorsque sa décision est parvenue au pétitionnaire après l'expiration de ce délai et qu'elle entend contester devant le juge administratif l'existence d'une telle décision implicite, d'établir la date à laquelle le pli accompagnant sa décision a régulièrement fait l'objet d'une première présentation à l'adresse de l'intéressé ; que cette preuve peut résulter des mentions précises, claires et concordantes figurant sur les documents, le cas échéant électroniques, remis à l'expéditeur conformément à la réglementation postale, notamment de celles de l'avis de réception prévu à l'article 7 de l'arrêté du 7 février 2007 ; qu'à défaut, cette preuve peut résulter d'une attestation circonstanciée du prestataire ou d'autres éléments de preuve établissant que le courrier a bien été présenté au destinataire dans les conditions rappelées au point 3 ; <br/>
<br/>
              5. Considérant qu'il ressort des énonciations du jugement attaqué que, par un arrêté du 20 août 2010, le maire de Soignolles-en-Brie a décidé de surseoir à statuer sur la déclaration préalable présentée le 28 juillet 2010 par M. et Mme A...; que le 21 août, soit une semaine avant l'expiration du délai d'instruction prévu par la réglementation en vigueur, le maire a adressé aux intéressés, par lettre recommandée avec avis de réception, une décision de sursis à statuer fondée sur la contrariété du projet avec les orientations du plan local d'urbanisme en cours d'élaboration ; que, pour juger que la commune ne pouvait être regardée comme ayant notifié, dans le délai d'instruction, le pli contenant la décision litigieuse, le tribunal administratif s'est fondé sur ce que les intéressés, qui soutenaient ne pas avoir reçu l'avis de présentation prévu par l'article 5 de l'arrêté du 7 février 2007, avaient fourni une copie d'un volet du pli recommandé ne comportant aucune date de présentation ou de distribution ; qu'en se prononçant ainsi, sans rechercher si l'avis de réception retourné à l'expéditeur en application de l'article 7 du même arrêté, dont le jugement faisait d'ailleurs expressément état, ne comportait pas de mentions précises, claires et concordantes de nature à établir la date d'une première présentation du pli, le tribunal administratif a commis une erreur de droit ; que, par suite, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, la commune de Soignolles-en-Brie est fondée à demander l'annulation du jugement qu'elle attaque en tant qu'il statue sur la légalité de l'arrêté contesté ; <br/>
<br/>
              6. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de M. et Mme A...la somme de 1 500 euros, qui sera versée à la commune de Soignolles-en-Brie au titre des dispositions de l'article L. 761-1 du code de justice administrative ; que ces mêmes dispositions font obstacle à ce que soit mise à la charge de la commune de Soignolles-en-Brie, qui n'est pas la partie perdante dans la présente instance, la somme que demandent M. et Mme A...au même titre ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Les articles 1, 2 et 4 du jugement du 30 juin 2011 du tribunal administratif de Melun sont annulés.<br/>
<br/>
      Article 2 : L'affaire est renvoyée au tribunal administratif de Melun. <br/>
<br/>
Article 3 : M. et Mme A...verseront à la commune de Soignolles-en-Brie une somme globale de 1 500 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
Article 4 : Les conclusions présentées par M. et Mme A...au titre de l'article L.761-1 du code de justice administrative sont rejetées. <br/>
<br/>
Article 5 : La présente décision sera notifiée à la commune de Soignolles-en-Brie et à M. et Mme B...A.... <br/>
Copie en sera adressée pour information à la ministre de l'égalité des territoires et du logement.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
