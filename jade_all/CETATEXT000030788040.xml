<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030788040</ID>
<ANCIEN_ID>JG_L_2015_06_000000389682</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/78/80/CETATEXT000030788040.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème / 2ème SSR, 26/06/2015, 389682</TITRE>
<DATE_DEC>2015-06-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>389682</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème / 2ème SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FOUSSARD, FROGER ; SCP MASSE-DESSEN, THOUVENIN, COUDRAY</AVOCATS>
<RAPPORTEUR>M. Vincent Montrieux</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Bertrand Dacosta</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:389682.20150626</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Les sociétés Extérion Média France et Derichebourg SNG ont demandé au juge des référés du tribunal administratif de Paris, saisi sur le fondement de l'article L. 551-1 du code de justice administrative, d'annuler la procédure de passation du marché public destiné à la conception, la fourniture, l'entretien, la maintenance et l'exploitation publicitaire de kiosques de presse et quelques kiosques à autre usage ainsi qu'à la gestion de l'activité des kiosquiers, lancée par la ville de Paris. <br/>
<br/>
              Par une ordonnance n° 1504843/7-4 du 9 avril 2015, le juge des référés du tribunal administratif de Paris a annulé la procédure de dialogue compétitif engagée les 20 et 21 janvier 2015 par la ville de Paris.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 22 avril, 7 mai  et 5 juin 2015 au secrétariat du contentieux du Conseil d'Etat, la ville de Paris demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) statuant en référé, de rejeter les demandes des sociétés Extérion Média France et Derichebourg SNG ;<br/>
<br/>
              3°) de mettre à la charge solidaire des sociétés Extérion Média France et Derichebourg SNG le versement de la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code des marchés publics ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Vincent Montrieux, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Bertrand Dacosta, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Foussard, Froger, avocat de la ville de Paris, et à la SCP Masse-Dessen, Thouvenin, Coudray, avocat de la société Extérion Média France et de la société Derichebourg SNG ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article L. 551-1 du code de justice administrative : " Le président du tribunal administratif, ou le magistrat qu'il délègue, peut être saisi en cas de manquement aux obligations de publicité et de mise en concurrence auxquelles est soumise la passation par les pouvoirs adjudicateurs de contrats administratifs ayant pour objet l'exécution de travaux, la livraison de fournitures ou la prestation de services, avec une contrepartie économique constituée par un prix ou un droit d'exploitation, ou la délégation d'un service public. / Le juge est saisi avant la conclusion du contrat. " ; qu'aux termes de l'article L. 551-2 de ce code : " I. - Le juge peut ordonner à l'auteur du manquement de se conformer à ses obligations et suspendre l'exécution de toute décision qui se rapporte à la passation du contrat, sauf s'il estime, en considération de l'ensemble des intérêts susceptibles d'être lésés et notamment de l'intérêt public, que les conséquences négatives de ces mesures pourraient l'emporter sur leurs avantages. / Il peut, en outre, annuler les décisions qui se rapportent à la passation du contrat et supprimer les clauses ou prescriptions destinées à figurer dans le contrat et qui méconnaissent lesdites obligations. (...) " ; que, selon l'article L. 551-10 du même code : " Les personnes habilitées à engager les recours prévus aux articles L. 551-1 et L. 551-5 sont celles qui ont un intérêt à conclure le contrat et qui sont susceptibles d'être lésées par le manquement invoqué (...) " ;<br/>
<br/>
              2. Considérant que la ville de Paris se pourvoit en cassation contre l'ordonnance du 9 avril 2015 par laquelle le juge des référés du tribunal administratif de Paris a annulé, à la demande des sociétés Extérion Média France et Derichebourg SNG, la procédure qu'elle avait lancée en vue de la passation d'un marché public dont l'objet était la conception, la fourniture, l'entretien, la maintenance et l'exploitation publicitaire de kiosques de presse et de quelques kiosques à autre usage ainsi que la gestion de l'activité des kiosquiers ;<br/>
<br/>
              3. Considérant qu'il ressort des pièces du dossier soumis au juge des référés que, par un avis d'appel public à la concurrence publié le 23 octobre 2012, la ville de Paris a lancé une procédure de dialogue compétitif en vue de l'attribution d'un marché global qui devait se substituer à deux conventions existantes ayant respectivement pour objet, d'une part, la fourniture, l'entretien et l'exploitation publicitaire des kiosques, et, d'autre part la gestion de l'activité des kiosquiers ; que, selon les dispositions du " préprogramme fonctionnel " communiqué aux candidats, étaient principalement demandés au futur titulaire du marché, en premier lieu, le renouvellement d'au moins 200 des 404 kiosques actuels et l'adaptation des nouveaux mobiliers aux exigences de la ville en matière de fonctionnalités, de développement durable et d'innovation, en second lieu, la gestion de l'activité des kiosquiers, l'animation du réseau des kiosques et le soutien à l'activité de diffusion de la presse ; que le titulaire, également chargé de la rénovation, de la maintenance et de l'entretien de l'ensemble du parc de kiosques et redevable d'une redevance annuelle d'un montant à déterminer était autorisé, en contrepartie, à exploiter les espaces publicitaires sur les mobiliers ;<br/>
<br/>
              4. Considérant que, pour annuler l'ensemble de la procédure, le juge des référés a jugé que la ville de Paris avait méconnu, d'une part, l'article 10 du code des marchés publics en passant un marché global, d'autre part, l'article 36 du code en choisissant une procédure de dialogue compétitif ; <br/>
<br/>
              5. Considérant, en premier lieu, qu'aux termes de l'article 10 du code des marchés publics : " Afin de susciter la plus large concurrence, et sauf si l'objet du marché ne permet pas l'identification de prestations distinctes, le pouvoir adjudicateur passe le marché en lots séparés (...). Le pouvoir adjudicateur peut toutefois passer un marché global, avec ou sans identification de prestations distinctes, s'il estime que la dévolution en lots séparés est de nature, dans le cas particulier, à restreindre la concurrence, ou qu'elle risque de rendre techniquement difficile ou financièrement coûteuse l'exécution des prestations ou encore qu'il n'est pas en mesure d'assurer par lui-même les missions d'organisation, de pilotage ou de coordination " ; que, saisi d'un moyen tiré de l'irrégularité du recours à un marché global, il appartient au juge de déterminer si l'analyse à laquelle le pouvoir adjudicateur a procédé et les justifications qu'il fournit sont, eu égard à la marge d'appréciation qui lui est reconnue pour estimer que la dévolution en lots séparés présente l'un des inconvénients que les dispositions précitées mentionnent, entachée d'appréciations erronées ; <br/>
<br/>
              6. Considérant que le juge des référés a relevé que la ville de Paris faisait valoir que le fait de confier la gestion des kiosques et celle de l'activité des kiosquiers à un même opérateur la dispensait d'avoir à arbitrer elle-même les conflits récurrents opposant le gestionnaire des kiosques et les kiosquiers, dont les intérêts sont souvent divergents ; qu'il a cependant estimé que ces risques étaient inhérents à la nature de l'activité en cause et que, depuis plusieurs décennies, ils ne l'avaient pas mise en péril et en a déduit qu'ils n'étaient pas d'une gravité telle qu'ils rendent techniquement difficile l'exécution de deux contrats distincts ; qu'en jugeant ainsi, alors notamment que, dans le cadre du renouvellement que la ville attendait du titulaire du contrat dans la conception et la gestion des kiosques et des objectifs de valorisation de son domaine qu'elle s'assignait, la mise en oeuvre par deux opérateurs distincts des logiques propres à la gestion des ouvrages, à l'exploitation des espaces publicitaires et à la vente de journaux était de nature, ainsi qu'il ressortait de l'analyse qu'elle avait produite, à entraîner une multiplication des conflits et à rendre ainsi l'exécution de deux contrats techniquement difficile et coûteuse, le juge des référés a commis une erreur de qualification juridique ;<br/>
<br/>
              7. Considérant, en second lieu, qu'aux termes de l'article 36 du même code : " La procédure de dialogue compétitif est une procédure dans laquelle le pouvoir adjudicateur conduit un dialogue avec les candidats admis à y participer en vue de définir ou de développer une ou plusieurs solutions de nature à répondre à ses besoins et sur la base de laquelle ou desquelles les participants au dialogue seront invités à remettre une offre. / Le recours à la procédure de dialogue compétitif est possible lorsqu'un marché public est considéré comme complexe, c'est-à-dire lorsque l'une au moins des conditions suivantes est remplie : / 1° Le pouvoir adjudicateur n'est objectivement pas en mesure de définir seul et à l'avance les moyens techniques pouvant répondre à ses besoins ; / 2° Le pouvoir adjudicateur n'est objectivement pas en mesure d'établir le montage juridique ou financier d'un projet. " ;<br/>
<br/>
              8. Considérant que, pour juger que la ville de Paris ne pouvait recourir à la procédure de dialogue compétitif, le juge des référés a relevé que si la ville attendait des candidats des propositions innovantes pour la conception et le design des kiosques ainsi que pour l'amélioration des conditions de travail des kiosquiers, le titulaire du contrat aurait à fournir les autres prestations dans des conditions comparables à celles que prévoyaient les contrats en cours ; qu'il en a déduit que, bénéficiant d'une expérience séculaire en la matière, la ville ne se heurtait pas à des difficultés telles qu'elle ne serait pas en mesure de définir seule et à l'avance les moyens techniques pour répondre à ses besoins ou établir le montage financier ou juridique du projet ; qu'en jugeant ainsi, alors, d'une part, que la ville de Paris entendait qu'à l'occasion de la passation d'un nouveau marché, des solutions innovantes soient proposées, aussi bien pour la conception des ouvrages que pour la gestion de l'activité des kiosquiers, qui tiennent compte à la fois des évolutions technologiques et d'objectifs d'usage multiple des kiosques, de respect des contraintes architecturales et de l'environnement, de gestion optimale d'un réseau de kiosquiers et d'optimisation des recettes domaniales, et, d'autre part, qu'il ressortait des pièces du dossier qui lui était soumis et de l'argumentation des parties devant lui que la définition des moyens techniques pouvant répondre à de tels besoins était complexe, au sens des dispositions citées au point 7, le juge des référés a entaché son ordonnance d'une erreur de qualification juridique ;<br/>
<br/>
              9. Considérant, par suite, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, que la ville de Paris est fondée à demander l'annulation de l'ordonnance attaquée ;<br/>
<br/>
              10. Considérant qu'il y a lieu, dans les circonstances de l'espèce, en application de l'article L. 821-2 du code de justice administrative, de régler l'affaire au titre de la procédure de référé engagée par les sociétés Extérion Média France et Derichebourg SNG ;<br/>
<br/>
              11. Considérant, en premier lieu, ainsi qu'il a été dit ci-dessus au point 7, que la ville de Paris a pu, sans irrégularité, faire le choix d'un marché global, eu égard aux difficultés que soulèverait la réalisation, par deux opérateurs distincts, de prestations qui sont fortement imbriquées et obéissent cependant souvent à des logiques concurrentes ; que la circonstance que deux contrats distincts aient été auparavant mis en oeuvre est à cet égard, en tout état de cause, sans incidence;<br/>
<br/>
              12. Considérant, en deuxième lieu, qu'ainsi qu'il a été dit ci-dessus au point 8, les multiples objectifs assignés par la ville au futur titulaire et les contraintes qui lui sont imposées confèrent au marché une complexité justifiant, pour la définition des moyens pouvant répondre aux besoins de la ville, le recours au dialogue compétitif ; <br/>
<br/>
              13. Considérant, en troisième lieu, qu'aux termes du III de l'article 1er du code des marchés publics : " Les marchés publics de travaux sont les marchés conclus avec des entrepreneurs, qui ont pour objet soit l'exécution, soit conjointement la conception et l'exécution d'un ouvrage ou de travaux de bâtiment ou de génie civil répondant à des besoins précisés par le pouvoir adjudicateur qui en exerce la maîtrise d'ouvrage. / (...) Les marchés publics de services sont les marchés conclus avec des prestataires de services qui ont pour objet la réalisation de prestations de services. / (...) Lorsqu'un marché public porte à la fois sur des services et des travaux, il est un marché de travaux si son objet principal est de réaliser des travaux. Un marché public ayant pour objet l'acquisition de fournitures et, à titre accessoire, des travaux de pose et d'installation de celles-ci, est considéré comme un marché de fournitures " ; qu'aux termes de l'article 37 du même code : " Un marché de conception-réalisation est un marché de travaux qui permet au pouvoir adjudicateur de confier à un groupement d'opérateurs économiques ou, pour les seuls ouvrages d'infrastructure, à un seul opérateur économique, une mission portant à la fois sur l'établissement des études et l'exécution des travaux. (...) " ; que selon le I de l'article 69 du même code " I. - Les marchés de conception-réalisation définis à l'article 37 sont passés par les pouvoirs adjudicateurs soumis aux dispositions de la loi du 12 juillet 1985 susmentionnée selon la procédure d'appel d'offres restreint sous réserve des dispositions particulières qui suivent (...) " ;<br/>
<br/>
              14. Considérant que si le marché litigieux comporte à la fois des travaux et des prestations de services, il résulte de l'instruction que la réalisation des travaux en cause n'est pas son objet principal et que le marché ne peut donc être qualifié de marché de travaux ;  que, par suite, le moyen tiré de ce que la ville de Paris était tenue de suivre la procédure propre aux marchés de conception-réalisation, définie par le I de l'article 69 cité ci-dessus, ne peut qu'être écarté ;<br/>
<br/>
              15. Considérant, en dernier lieu, qu'il résulte de l'instruction que les candidatures retenues en vue de la phase de dialogue ont été évaluées, selon le règlement de la consultation, au regard de trois critères, pondérés à 50 %, 35 % et 15 %, relatifs, pour le premier, aux capacités techniques et aux moyens humains et matériels, pour le deuxième, aux références liées aux " compétences attendues dans le cadre de l'exécution du marché : exploitation publicitaire associée aux mobiliers urbains ; conception de mobiliers urbains ; fabrication et maintenance de mobiliers urbains ; entretien de mobiliers urbains ; animation d'un réseau de professionnels ", et, pour le troisième, aux capacités financières ; que la ville de Paris ayant décidé de n'admettre au dialogue que trois candidats, la candidature du groupement requérant, classée quatrième après évaluation de ses mérites, a été écartée ; qu'informant le groupement de ce rejet,  la ville lui a indiqué qu'il avait obtenu la note de 4/10 pour le deuxième critère en précisant que s'il avait " présenté trois références pertinentes dans l'exploitation publicitaire associée au mobilier urbain ", il n'en avait présenté " aucune en termes d'animation d'un réseau de professionnels " et n'avait fait état  " d'aucun titre d'études ou qualification professionnelles relatifs aux métiers de conception ou de design pour des mobiliers comparables à des kiosques " ni " d'aucune référence dans le domaine de la presse " ;<br/>
<br/>
              16. Considérant qu'en mentionnant l'absence de références dans le domaine de la presse, le courrier adressé par la ville de Paris aux requérantes n'a pas révélé l'existence d'un critère d'appréciation des candidatures différent du critère relatif aux compétences attendues qui avait été porté à la connaissance des candidats ; que si les requérantes font valoir les titres ou références qu'elles avaient présentés pour contester les termes de ce courrier, il n'appartient pas  au juge du référé précontractuel d'apprécier les mérites des candidatures ;  <br/>
<br/>
              17. Considérant qu'il résulte de tout ce qui précède que les demandes des sociétés Extérion Média France et Derichebourg SNG ne peuvent qu'être rejetées ;<br/>
<br/>
              18. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mise à la charge de la ville de Paris qui n'est pas, dans la présente instance, la partie perdante, la somme que demandent les sociétés Extérion Média France et Derichebourg SNG au titre des frais exposés par elles et non compris dans les dépens ; qu'en revanche, il y a lieu, dans les circonstances de l'espèce, au titre des mêmes dispositions, de mettre à la charge de ces dernières une somme globale de 4 500 euros à verser à la ville de Paris pour l'ensemble de la procédure ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'ordonnance du juge des référés du tribunal administratif de Paris du 9 avril 2015 est annulée.<br/>
Article 2 : Les demandes présentées par les sociétés Extérion Média France et Derichebourg SNG sont rejetées. <br/>
Article 3 : Les sociétés Extérion Média France et Derichebourg SNG verseront à la ville de Paris une somme globale de 4 500 euros au titre de l'article L. 761-1 du code de justice administrative.  <br/>
Article 4 : La présente décision sera notifiée à la ville de Paris, à la société Extérion Média France, à la société Derichebourg SNG, à la société Médiakiosk, à la société Clear Channel France, à la société Insert, à la société Séri et à la société Kawet.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">39-02-02 MARCHÉS ET CONTRATS ADMINISTRATIFS. FORMATION DES CONTRATS ET MARCHÉS. MODE DE PASSATION DES CONTRATS. - RECOURS À UN MARCHÉ GLOBAL (ART. 10 DU CMP) - DEGRÉ DE CONTRÔLE DU JUGE - CONTRÔLE NORMAL [RJ1] TENANT COMPTE DE LA MARGE D'APPRÉCIATION RECONNUE AU POUVOIR ADJUDICATEUR.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">39-08-015-01 MARCHÉS ET CONTRATS ADMINISTRATIFS. RÈGLES DE PROCÉDURE CONTENTIEUSE SPÉCIALES. PROCÉDURES D'URGENCE. - OFFICE - EXCLUSION - APPRÉCIATION DU MÉRITE DES CANDIDATURES.
</SCT>
<ANA ID="9A"> 39-02-02 Saisi d'un moyen tiré de l'irrégularité du recours à un marché global, il appartient au juge de déterminer si l'analyse à laquelle le pouvoir adjudicateur a procédé et les justifications qu'il fournit sont, eu égard à la marge d'appréciation qui lui est reconnue pour estimer que la dévolution en lots séparés présente l'un des inconvénients mentionnés à l'article 10 du code des marchés publics (CMP), entachées d'appréciations erronées.</ANA>
<ANA ID="9B"> 39-08-015-01 Il n'appartient pas au juge du référé précontractuel d'apprécier les mérites des candidatures.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 27 octobre 2011, Département des Bouches-du-Rhône, n° 350935, T. p. 1009.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
