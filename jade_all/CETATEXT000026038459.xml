<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026038459</ID>
<ANCIEN_ID>JG_L_2012_06_000000335920</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/03/84/CETATEXT000026038459.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  9ème sous-section jugeant seule, 18/06/2012, 335920, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2012-06-18</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>335920</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 9ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Jean-Pierre Jouguelet</PRESIDENT>
<AVOCATS>SCP DELAPORTE, BRIARD, TRICHET</AVOCATS>
<RAPPORTEUR>Mme Pauline Flauss</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Claire Legras</COMMISSAIRE_GVT>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 25 janvier et 26 avril 2010 au secrétariat du contentieux du Conseil d'Etat, présentés pour la SAS FROMAGERIE DES CHAUMES, dont le siège est au 155 avenue Rauski à Jurançon (64110) ; la SAS FROMAGERIE DES CHAUMES demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 08BX00089 du 19 novembre 2009 par lequel la cour administrative d'appel de Bordeaux, sur le recours du ministre du budget, des comptes publics et de la fonction publique, après avoir annulé les articles 1er et 2 du jugement n° 0400646 du 20 septembre 2007 par lequel le tribunal administratif de Pau lui a accordé la décharge des cotisations supplémentaires de taxe professionnelle qui lui ont été réclamées au titre des années 1999 et 2000 à concurrence de la diminution d'imposition résultant de la prise en compte dans l'application du plafonnement de la taxe professionnelle en fonction de la valeur ajoutée des redevances de propriété industrielle versées à la société Bongrain, a remis à sa charge les cotisations susmentionnées ;<br/>
<br/>
              2°) réglant l'affaire au fond, de prononcer la décharge des impositions en litige ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat le versement de la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales et l'article 1er du premier protocole additionnel à cette convention ;<br/>
<br/>
              Vu le code général des impôts et le livre des procédures fiscales ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Pauline Flauss, Maître des Requêtes en service extraordinaire,  <br/>
<br/>
              - les observations de la SCP Delaporte, Briard, Trichet, avocat de la SAS FROMAGERIE DES CHAUMES, <br/>
<br/>
              - les conclusions de Mme Claire Legras, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Delaporte, Briard, Trichet, avocat de la SAS FROMAGERIE DES CHAUMES ;<br/>
<br/>
<br/>
<br/>
              Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que la société par actions simplifiée " Fromagerie des Chaumes ", sous-filiale de la SA Bongrain, a fait l'objet d'une vérification de comptabilité portant notamment sur les années 1999 et 2000 ; qu'à l'issue de cette vérification, l'administration a remis en cause une partie des redevances de marques versées par cette société à la SA Bongrain, qu'elle a réintégrées dans les résultats de la société vérifiée ; que, corrélativement à cette réintégration, l'administration a, du fait d'une correction de la valeur ajoutée de l'exercice, partiellement remis en cause le bénéfice du plafonnement de sa cotisation de taxe professionnelle pour 1999 et 2000 en fonction de sa valeur ajoutée, qui lui avait initialement été accordé ; que la société a contesté les cotisations supplémentaires de taxe professionnelle mises à sa charge à la suite de cette rectification ; que la SAS FROMAGERIE DES CHAUMES se pourvoit en cassation contre l'arrêt du 19 novembre 2009 par lequel la cour administrative d'appel de Bordeaux, après avoir, sur le recours du ministre du budget, des comptes publics et de la fonction publique, annulé les articles 1er et 2 du jugement du 20 septembre 2007 par lequel le tribunal administratif de Pau lui a accordé la décharge des cotisations supplémentaires de taxe professionnelle qui lui ont été réclamées au titre des années 1999 et 2000 à concurrence de la diminution d'imposition résultant de la prise en compte dans l'application du plafonnement de la taxe professionnelle en fonction de la valeur ajoutée des redevances versées à la SA Bongrain, a remis à la charge de la société requérante les cotisations de taxe professionnelle en litige ;<br/>
<br/>
              Considérant, en premier lieu, qu'aux termes de l'article 1er du premier protocole à la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales : "Toute personne physique ou morale a droit au respect de ses biens. Nul ne peut être privé de sa propriété que pour cause d'utilité publique et dans les conditions prévues par la loi et les principes généraux du droit international. Les dispositions précédentes ne portent pas atteinte au droit que possèdent les Etats de mettre en vigueur les lois qu'ils jugent nécessaires pour réglementer l'usage des biens conformément à l'intérêt général ou pour assurer le paiement des impôts ou d'autres contributions ou des amendes. " ; que, selon l'article 14 de la même convention : " La jouissance des droits et libertés reconnus par la présente convention doit être assurée, sans distinction aucune, fondée notamment sur le sexe, la race, la couleur, la langue, la religion, les opinions publiques ou toutes autres opinions, l'origine nationale ou sociale, l'appartenance à une minorité nationale, la fortune, la naissance ou toute autre situation. " ; qu'aux termes de l'article R. 200-18 du livre des procédures fiscales, dans sa rédaction applicable à la présente procédure : " A compter de la notification du jugement du tribunal administratif qui a été faite au directeur du service de l'administration des impôts ou de l'administration des douanes et droits indirects qui a suivi l'affaire, celui-ci dispose d'un délai de deux mois pour transmettre, s'il y a lieu, le jugement et le dossier au ministre chargé du budget. / Le délai imparti pour saisir la cour administrative d'appel court, pour le ministre, de la date à laquelle expire le délai de transmission prévu à l'alinéa précédent ou de la date de la signification faite au ministre. " ;<br/>
<br/>
              Considérant que si les stipulations combinées des articles précités de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales et de son premier protocole additionnel peuvent être utilement invoquées pour soutenir que la loi fiscale serait à l'origine de discriminations injustifiées entre contribuables, elles sont en revanche sans portée dans les rapports institués entre la puissance publique et un contribuable ; que, par suite, le moyen invoqué par la société requérante, tiré de ce qu'en vertu des dispositions de l'article R. 200-18 du livre des procédures fiscales, le ministre peut disposer d'un délai d'appel qui peut excéder de deux mois celui dont dispose le contribuable sur le fondement de l'article R. 811-2 du code de justice administrative ne peut qu'être écarté ; que ce motif de pur droit, qui n'implique l'appréciation d'aucune circonstance de fait, doit être substitué à celui retenu par les juges du fond ;<br/>
<br/>
              Considérant, en deuxième lieu, qu'aux termes de l'article 1647 B sexies du code général des impôts, dans sa rédaction applicable aux années en litige : " I. Sur demande du redevable, la cotisation de taxe professionnelle de chaque entreprise est plafonnée en fonction de la valeur ajoutée produite au cours de l'année au titre de laquelle l'imposition est établie ou au cours du dernier exercice de douze mois clos au cours de cette même année lorsque cet exercice ne coïncide pas avec l'année civile. La valeur ajoutée est définie selon les modalités prévues au II (...) / II. 1. La valeur ajoutée mentionnée au I est égale à l'excédent hors taxe de la production sur les consommations de biens et services en provenance de tiers constaté pour la période définie au I. / 2. Pour la généralité des entreprises, la production de l'exercice est égale à la différence entre : d'une part, les ventes, les travaux, les prestations de services ou les recettes ; les produits accessoires ; les subventions d'exploitation ; les ristournes, rabais et remises obtenus ; les travaux faits par l'entreprise pour elle-même ; les stocks à la fin de l'exercice ; et d'autre part, les achats de matières et marchandises, droits de douane compris ; les réductions sur ventes ; les stocks en début de l'exercice. Les consommations de biens et services en provenance de tiers comprennent : les travaux, fournitures et services extérieurs, à l'exception des loyers afférents aux biens pris en crédit-bail, ou des loyers afférents à des biens, visés au a du 1° de l'article 1467, pris en location par un assujetti à la taxe professionnelle pour une durée de plus de six mois ou des redevances afférentes à ces biens résultant d'une convention de location-gérance, les frais de transports et déplacements, les frais divers de gestion " ;<br/>
<br/>
              Considérant que la circonstance que le calcul de la valeur ajoutée produite par l'entreprise au sens de l'article 1647 B sexies précité du code général des impôts s'effectue par référence aux normes comptables en vigueur lors de l'année d'imposition concernée et aux éléments de la comptabilité de l'entreprise ne fait pas obstacle à ce que l'administration puisse contrôler l'exactitude des montants déclarés en tant que charges d'exploitation, et ainsi remettre en cause, le cas échéant, le bien-fondé d'une écriture comptable et, par voie de conséquence, exclure du calcul de la valeur ajoutée de l'entreprise des sommes qui ne peuvent être regardées comme des consommations de biens et services ;<br/>
<br/>
              Considérant qu'après avoir relevé dans les motifs de son arrêt que l'administration s'est bornée, en l'espèce, à remettre en cause la qualification de charges d'exploitation d'une fraction des redevances versées par la SAS FROMAGERIE DES CHAUMES à la SA Bongrain, en sa qualité de société-mère, en se fondant sur l'absence de contrepartie aux sommes contestées, la cour a jugé que, dès lors que les sommes en litige constituaient des libéralités, elles ne pouvaient être regardées comme des consommations de biens et de services en provenance de tiers au sens des dispositions précitées de l'article 1647 B sexies du code général des impôts ; qu'en statuant ainsi, la cour n'a, contrairement à ce que soutient la société requérante, pas fait une application erronée des dispositions précitées de l'article 1647 B sexies du code général des impôts ;<br/>
<br/>
              Considérant, en troisième lieu, qu'aux termes du premier alinéa de l'article L. 80 A du livre des procédures fiscales dans sa rédaction applicable à la présente procédure : " Il ne sera procédé à aucun rehaussement d'impositions antérieures si la cause du rehaussement poursuivi par l'administration est un différend sur l'interprétation par le redevable de bonne foi du texte fiscal et s'il est démontré que l'interprétation sur laquelle est fondée la première décision a été, à l'époque, formellement admise par l'administration " ;<br/>
<br/>
              Considérant que la remise à la charge du contribuable de sommes précédemment dégrevées dans le cadre du plafonnement de la taxe professionnelle en fonction de la valeur ajoutée, ne constitue pas un rehaussement d'impositions antérieures au sens de l'article L. 80 A du livre des procédures fiscales ; qu'il suit de là qu'en jugeant que la société requérante ne pouvait se prévaloir, sur le fondement de cet article, de la documentation administrative de base 6 E- 4334 dans le cadre d'un litige portant sur le plafonnement de la taxe professionnelle en fonction de la valeur ajoutée, la cour n'a pas commis d'erreur de droit ;<br/>
<br/>
              Considérant qu'il résulte de tout ce qui précède que la SAS FROMAGERIE DES CHAUMES n'est pas fondée à demander l'annulation de l'arrêt qu'elle attaque ;<br/>
<br/>
              Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mise à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante, la somme demandée par la SAS FROMAGERIE DES CHAUMES au titre des frais exposés par elle et non compris dans les dépens ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : Le pourvoi de la SAS FROMAGERIE DES CHAUMES est rejeté. <br/>
Article 2 : La présente décision sera notifiée à la SAS FROMAGERIE DES CHAUMES et au ministre de l'économie, des finances et du commerce extérieur.<br/>
<br/>
</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
