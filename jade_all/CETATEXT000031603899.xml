<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031603899</ID>
<ANCIEN_ID>JG_L_2015_12_000000374924</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/60/38/CETATEXT000031603899.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème / 4ème SSR, 10/12/2015, 374924, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-12-10</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>374924</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème / 4ème SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP RICHARD ; SCP NICOLAY, DE LANOUVELLE, HANNOTIN</AVOCATS>
<RAPPORTEUR>M. Lionel Collet</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Laurence Marion</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:374924.20151210</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B...A...a demandé au tribunal administratif de Clermont-Ferrand de condamner l'hôpital local de Langeac à lui verser diverses sommes au titre de rappels de traitement, de gardes et d'astreintes, ainsi que des indemnités réparant des pertes de traitement et de pension de retraite résultant du fait qu'il n'avait pas bénéficié de l'avancement de carrière prévu par la convention conclue le 20 décembre 1990 lors de son recrutement. Par un jugement n° 1001140 du 21 juin 2012, le tribunal administratif a, d'une part, condamné l'établissement à verser à l'intéressé, pour la période comprise entre le 1er janvier 2003 et le 30 juin 2008, une somme égale à la différence entre le traitement mensuel correspondant à l'indice afférent au 12e échelon des praticiens des hôpitaux à temps partiel fixé par le décret du 29 mars 1985 et le traitement indiciaire mensuel qu'il avait effectivement perçu et, d'autre part, rejeté le surplus de ses conclusions.<br/>
<br/>
              Par un arrêt n° 12LY02182 du 26 novembre 2013, la cour administrative d'appel de Lyon, saisie d'un appel de M. A...et d'un appel incident de l'hôpital local de Langeac, a annulé ce jugement et rejeté la demande de l'intéressé.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique enregistrés les 27 janvier 2014, 28 avril 2014 et 9 février 2015 au secrétariat du contentieux du Conseil d'Etat, M. A...demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat le versement de la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
<br/>
              - le code de la santé publique ;<br/>
<br/>
              - le décret n° 85-384 du 29 mars 1985 ; <br/>
<br/>
              - le décret n° 93-701 du 27 mars 1993 ;<br/>
<br/>
              - le code de justice administrative ; <br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Lionel Collet, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Laurence Marion, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Richard, avocat de M. A...et à la SCP Nicolaÿ, de La Nouvelle, Hannotin, avocat de l'hôpital local de Langeac ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, par une convention conclue le 20 décembre 1990, l'hôpital local de Langeac a recruté M. A... en qualité de médecin généraliste dans les unités de soins de cure médicale et long séjour à compter du 1er janvier 1991 ; que cette convention a été résiliée et remplacée par une nouvelle convention conclue le 1er juillet 2008 sur le fondement de l'article R. 6152-401 du code de la santé publique ; que, par un jugement du 21 juin 2012, le tribunal administratif de Clermont-Ferrand a partiellement fait droit à une demande de M. A...tendant à ce que l'établissement soit condamné à lui verser diverses indemnités qu'il estimait dues au titre de la convention de 1990 ; que M. A...se pourvoit en cassation contre l'arrêt du 26 novembre 2013 par lequel la cour administrative d'appel de Lyon a annulé ce jugement du tribunal administratif et rejeté sa demande ;<br/>
<br/>
              Sur la régularité de l'arrêt attaqué :<br/>
<br/>
              2. Considérant qu'aux termes du premier alinéa de l'article R. 611-7 du code de justice administrative: " Lorsque la décision lui paraît susceptible d'être fondée sur un moyen relevé d'office, le président de la formation de jugement ou, au Conseil d'Etat, la sous-section chargée de l'instruction en informe les parties avant la séance de jugement et fixe le délai dans lequel elles peuvent, sans qu'y fasse obstacle la clôture éventuelle de l'instruction, présenter leurs observations sur le moyen communiqué " ; que la cour administrative d'appel de Lyon a informé M. A...qu'elle était susceptible de relever d'office le moyen d'ordre public " tiré de ce que la convention du 20 décembre 1990 ne pouvait légalement faire référence à une grille d'avancement statutaire " ; que cette information était suffisamment précise pour que M. A...pût connaitre le motif sur lequel la cour était susceptible de fonder sa décision et en discuter utilement ; qu'ainsi le moyen tiré de ce que la procédure suivie devant la cour administrative d'appel aurait été  irrégulière au regard des dispositions de l'article R. 611-7 du code de justice administrative doit être écarté ;<br/>
<br/>
              Sur le bien-fondé de l'arrêt :<br/>
<br/>
              En ce qui concerne les rappels de traitement :<br/>
<br/>
              3. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que la convention conclue en 1990 entre l'hôpital local de Langeac et M. A...prévoyait l'évolution de la rémunération de l'intéressé par référence aux durées moyennes d'ancienneté fixées dans le décret du 29 mars 1985 portant statut des praticiens exerçant leur activité à temps partiel dans les établissements d'hospitalisation publics ; que la cour administrative d'appel, dont l'arrêt n'est pas contesté sur ce point, a jugé qu'une telle clause, instaurant un déroulement de carrière pour un agent non titulaire, était irrégulière ; <br/>
<br/>
              4. Considérant que, sauf s'il présente un caractère fictif ou frauduleux, le contrat de recrutement d'un agent contractuel de droit public crée des droits au profit de celui-ci ; que lorsque le contrat est entaché d'une irrégularité, notamment parce qu'il méconnaît une disposition législative ou réglementaire applicable à la catégorie d'agents contractuels dont relève l'intéressé, l'administration est tenue de proposer à celui-ci une régularisation de son contrat afin que son exécution puisse se poursuivre régulièrement ; qu'en revanche, l'agent ne saurait prétendre à la mise en oeuvre des stipulations illégales du contrat ; qu'il suit de là que la cour administrative d'appel n'a pas commis d'erreur de droit en jugeant que, si la convention conclue en 1990 entre l'hôpital local de Langeac et M. A...avait créé au profit de ce dernier un droit à la poursuite de l'exécution de son contrat régularisé, M.A...  ne pouvait se prévaloir de celles des stipulations de ce contrat qui méconnaissaient les dispositions législatives et réglementaires applicables ; <br/>
<br/>
              5. Considérant que la cour a estimé que le préjudice dont se prévalait M. A... était qu'il se serait abstenu de conclure le contrat et aurait pu opter pour une autre activité plus rémunératrice s'il avait eu connaissance de l'irrégularité de la clause d'indexation figurant au contrat ; qu'en jugeant que le préjudice ainsi allégué ne présentait pas de lien de causalité directe avec la décision de l'hôpital local de Langeac de conclure un contrat comportant la clause irrégulière en cause, et en en déduisant que les conclusions indemnitaires de M. A... tendant aux rappels de traitement correspondants devaient être rejetées, la cour administrative d'appel a exactement qualifié les faits de l'espèce et  n'a pas commis d'erreur de droit ;<br/>
<br/>
              Sur la rémunération des gardes et astreintes :<br/>
<br/>
              6. Considérant qu'aux termes des dispositions, applicables au litige porté devant les juges du fond, du II de l'article 8 du décret du 27 mars 1993 relatif aux praticiens contractuels des établissements publics de santé : " La participation des praticiens contractuels au service de gardes et astreintes est indemnisée, selon les modalités définies par l'arrêté prévu au 20 de l'article 28 du décret n° 84-131 du 24 février 1984 susvisé " ; qu'il résulte de ces dispositions, ultérieurement codifiées et désormais reprises à l'article D. 6152-417 du code de la santé publique, que les périodes d'astreinte et de garde accomplies par les praticiens contractuels doivent donner lieu au versement d'indemnités spécifiques s'ajoutant à la rémunération prévue au contrat ; qu'en écartant l'indemnisation des périodes d'astreintes et de garde au motif que M. A...n'établissait pas que les tâches accomplies au titre de ces périodes excédaient les activités mentionnées par la convention de 1990, la cour a entaché son arrêt d'une erreur de droit ; que l'arrêt attaqué doit, par suite, être annulé en tant qu'il statue sur les conclusions tendant au versement d'indemnités pour les périodes de garde et d'astreintes ;<br/>
<br/>
              Sur les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              7. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'hôpital de Langeac, au titre des dispositions de l'article L. 761-1 du code de justice administrative, le versement d'une somme de 3 500 euros à M. A... ; que ces mêmes dispositions font obstacle à ce qu'une somme soit mise à ce titre à la charge de M. A...qui n'est pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>
<br/>			D E C I D E :<br/>
              			--------------<br/>
<br/>
Article 1er : L'arrêt du 26 novembre 2013 de la cour administrative d'appel de Lyon  est annulé en tant qu'il statue sur les conclusions tendant au versement d'indemnités pour les périodes de garde et d'astreintes.<br/>
Article 2 : L'affaire est renvoyée, dans cette mesure, à la cour administrative d'appel de Lyon.<br/>
Article 3 : L'hôpital de Langeac versera une somme de 3 500 euros à M. A... au titre de l'article L. 761-1 du code de justice administrative. Les conclusions présentées par l'hôpital de Langeac sur ce fondement sont rejetées.<br/>
Article 4 : Le surplus des conclusions présentées par M. A...est rejeté.<br/>
Article 5 : La présente décision sera notifiée à M. B...A...et à l'hôpital de Langeac.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
