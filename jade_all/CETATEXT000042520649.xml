<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042520649</ID>
<ANCIEN_ID>JG_L_2020_11_000000440183</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/52/06/CETATEXT000042520649.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème chambre, 13/11/2020, 440183, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-11-13</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>440183</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROUSSEAU, TAPIE</AVOCATS>
<RAPPORTEUR>M. Christian Fournier</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Marie-Gabrielle Merloz</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2020:440183.20201113</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Mme A... B... a demandé au tribunal administratif de Clermont-Ferrand de condamner l'État à lui verser les sommes de 500 000 euros au titre de ses préjudices professionnel et financier et de 40 000 euros au titre de son préjudice moral. Par un jugement n° 1402086 du 17 décembre 2015, le tribunal administratif de Clermont-Ferrand a rejeté sa demande. Par un arrêt n° 16LY00624 du 5 octobre 2017, la cour a annulé ce jugement et a condamné l'Etat à verser à Mme B... une somme de 15 000 euros en réparation des préjudices subis.<br/>
<br/>
              Par une décision n° 416268 du 1er juillet 2019, le Conseil d'Etat a annulé cet arrêt en tant qu'il écartait l'existence d'une faute de l'administration du fait de la communication d'informations erronées qu'elle avait faite à Mme B..., en tant qu'il omettait de statuer sur l'existence d'une faute à raison de la discrimination invoquée par Mme B... et en tant qu'il fixait à 15 000 euros le montant du préjudice subi par celle-ci, et a renvoyé à la cour, dans cette mesure, le jugement de l'affaire.<br/>
<br/>
              Par un arrêt n°19LY02569 du 27 février 2020, la cour administrative d'appel de Lyon a condamné l'Etat à verser à Mme B... une indemnité d'un montant total de 15 000 euros en réparation de son préjudice moral et rejeté le surplus des conclusions de la requête de Mme B....<br/>
<br/>
              Par un pourvoi et un mémoire complémentaire, enregistrés les 21 avril et 21 juillet 2020 au secrétariat du contentieux du Conseil d'Etat, Mme B... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à ses conclusions devant les juges du fond, les sommes allouées devant être majorées des intérêts au taux légal et les intérêts échus capitalisés en application de l'article 1154 du code civil ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'éducation ;<br/>
              - la loi n° 83-634 du 13 juillet 1983 ;<br/>
              - la loi n° 84-16 du 11 janvier 1984 ;<br/>
              - le décret n° 95-979 du 25 août 1995 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Christian Fournier, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Marie-Gabrielle Merloz, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Rousseau, Tapie, avocat de Mme B... ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 822-1 du code de justice administrative : " Le pourvoi en cassation devant le Conseil d'Etat fait l'objet d'une procédure préalable d'admission. L'admission est refusée par décision juridictionnelle si le pourvoi est irrecevable ou n'est fondé sur aucun moyen sérieux ".<br/>
<br/>
              2. Pour demander l'annulation de l'arrêt qu'elle attaque, Mme B... soutient que la cour administrative d'appel de Lyon :<br/>
<br/>
              - l'a insuffisamment motivé faute d'avoir répondu précisément aux griefs dont elle se prévalait au titre de la discrimination ;<br/>
              - a dénaturé les faits en jugeant que les éléments qui lui étaient soumis n'étaient pas susceptibles de faire présumer une atteinte au principe de l'égalité de traitement des personnes ;<br/>
              - l'a insuffisamment motivé en s'abstenant de rechercher si le comportement de l'administration avait pu lui faire perdre toute confiance dans l'institution et lui avait causé ainsi un préjudice moral ;<br/>
              - a dénaturé les faits en jugeant que la délivrance d'informations erronées n'avait pas présenté un caractère vexatoire de nature à lui causer un préjudice moral ;<br/>
              - a dénaturé les faits en jugeant qu'il était fait une juste appréciation de la réparation qui lui est due en condamnant l'État à lui verser une indemnité de 15 000 euros en réparation du préjudice moral qu'elle a subi du fait de la faute résultant du refus de l'administration de lui permettre, à l'issue de ses congés de longue maladie, d'accomplir le stage qui seul lui aurait permis, le cas échéant, d'obtenir un contrat définitif ;<br/>
              - a commis une erreur de droit en jugeant qu'elle ne pouvait demander réparation de ses préjudices professionnel et financier tenant à l'impossibilité d'effectuer le stage nécessaire à l'obtention d'un contrat définitif au motif qu'il résultait d'un certificat médical de son médecin traitant en date du 4 avril 2016 que son état de santé ne lui permettait pas de reprendre une activité professionnelle alors, d'une part, que ce certificat ne se prononce que sur son aptitude à la date du 4 avril 2016 et non pour la période comprise entre la fin de son contrat provisoire, le 31 août 2013, et le 4 avril 2016, période au cours de laquelle elle aurait pu effectuer son stage et, d'autre part, que ce certificat n'établit pas son inaptitude définitive à la reprise de son travail ;<br/>
              - a dénaturé les faits en jugeant qu'il ne résultait pas de l'instruction que son état de santé postérieur à la fin de son contrat provisoire lui aurait permis d'effectuer le stage nécessaire à l'obtention d'un contrat définitif ;<br/>
              - l'a insuffisamment motivé et commis une erreur de droit en ne recherchant pas si son inaptitude à reprendre ses fonctions résultait du comportement de l'administration, de nature à engager sa responsabilité.<br/>
<br/>
              3. Eu égard aux moyens soulevés, il y a lieu d'admettre les conclusions du pourvoi qui sont dirigées contre l'arrêt attaqué en tant qu'il s'est prononcé sur l'indemnisation du préjudice professionnel et financier subi par Mme B... à raison de la faute commise par l'administration en ne lui permettant pas d'accomplir effectivement le stage nécessaire à l'obtention d'un contrat définitif. En revanche, s'agissant du surplus des conclusions du pourvoi, aucun des moyens soulevés n'est de nature à permettre l'admission de ces conclusions.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : Les conclusions du pourvoi de Mme B... qui sont dirigées contre l'arrêt attaqué en tant qu'il s'est prononcé sur l'indemnisation du préjudice professionnel et financier qu'elle a subi à raison de la faute commise par l'administration en ne lui permettant pas d'accomplir effectivement le stage nécessaire à l'obtention d'un contrat définitifs sont admises.<br/>
Article 2 : Le surplus des conclusions du pourvoi de Mme B... n'est pas admis.<br/>
Article 3 : La présente décision sera notifiée à Mme A... B....<br/>
Copie en sera adressée au ministre de l'éducation nationale, de la jeunesse et des sports.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
