<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037022296</ID>
<ANCIEN_ID>JG_L_2018_06_000000410650</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/02/22/CETATEXT000037022296.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème - 3ème chambres réunies, 06/06/2018, 410650, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-06-06</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>410650</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème - 3ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GASCHIGNARD</AVOCATS>
<RAPPORTEUR>M. Laurent Domingo</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Romain Victor</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:410650.20180606</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A... B...a demandé au tribunal administratif de Toulon d'annuler la décision du 14 mars 2014 par laquelle le préfet du Var a rejeté sa demande tendant à la délivrance d'une autorisation d'occupation du domaine public maritime pour une passerelle, deux escaliers et un appontement, situés au droit de sa résidence sur le territoire de la commune de Saint-Raphaël et au retrait de la mise en demeure de démolir ces ouvrages que lui a adressée le préfet du Var le 10 décembre 2013. Par un jugement n° 1401895 du 6 novembre 2015, ce tribunal a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 16MA00077 du 16 mars 2017, la cour administrative d'appel de Marseille  a rejeté l'appel formé  par M. B...contre ce jugement.  <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 17 mai et 18 août 2017 au secrétariat du contentieux du Conseil d'Etat, M. B... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 2 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code général de la propriété des personnes publiques ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Laurent Domingo, maître des requêtes,<br/>
<br/>
              - les conclusions de M. Romain Victor, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Gaschignard, avocat de M.B....<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que, par une décision du 10 décembre 2013, le préfet du Var a mis en demeure M. B...de retirer du domaine public maritime une passerelle, deux escaliers, un appontement et un mât pour drapeaux lui appartenant, installés sans autorisation au droit de sa propriété située lieu-dit du Trayas à Saint Raphaël. M. B...a, le 10 février 2014, formé un recours gracieux contre cette décision et sollicité du préfet du Var des autorisations d'occupation du domaine public pour ces ouvrages. Par une lettre du 14 mars 2014, le préfet du Var a rejeté, d'une part, les demandes de délivrance d'autorisations d'occupation du domaine public, d'autre part, le recours gracieux de M. B...dirigé contre la demande de démolition des ouvrages.  Par un jugement du 6 novembre 2015, le tribunal administratif de Toulon a rejeté la demande de M. B...tendant à l'annulation de ces deux décisions. M. B...se pourvoit en cassation contre l'arrêt de la cour administrative d'appel de Marseille du 16 mars 2017 qui a rejeté son appel contre ce jugement.<br/>
<br/>
              Sur le bien-fondé de l'arrêt attaqué en tant qu'il concerne la passerelle : <br/>
<br/>
              2. Il ressort des pièces du dossier soumis aux juges du fond que M. B...a sollicité du préfet du Var, qui lui avait préalablement adressé une mise en demeure de démolir cet ouvrage, la délivrance d'une autorisation d'occupation du domaine public maritime pour une passerelle surplombant la mer à une hauteur d'environ 7 mètres, dont il soutenait cependant que les massifs d'ancrage étaient implantés sur sa propriété. Pour rejeter cette demande, le préfet du Var a fait valoir qu'afin de redonner au littoral son caractère naturel, il n'était plus possible de délivrer sur cette partie du littoral particulièrement protégée de nouveaux titres. Devant la cour, M. B...contestait l'affirmation du préfet du Var selon laquelle la passerelle occuperait le domaine public maritime, tel que défini par l'article L. 2111-4 du code général de la propriété des personnes publiques. En écartant ce moyen comme  inopérant, alors que l'appartenance au domaine public maritime des dépendances en cause conditionnait la faculté pour l'autorité préfectorale de délivrer des titres en vue de leur occupation, la cour a entaché son arrêt d'une erreur de droit. M. B...est, par suite, fondé à en demander l'annulation dans cette mesure. <br/>
<br/>
              Sur le bien-fondé de l'arrêt attaqué en ce qui concerne les autres ouvrages :<br/>
<br/>
              3. En jugeant que le refus du préfet du Var de délivrer à M. B...des autorisations d'occupation pour les autres ouvrages en litige, implantés irrégulièrement sur le domaine public maritime et plus précisément sur le rivage de la mer, était légal au motif que la présence de ces ouvrages était incompatible avec les objectifs d'intérêt général poursuivis par l'autorité préfectorale de redonner au littoral son caractère naturel et de limiter son occupation, la cour, qui n'a ni omis de prendre en considération, dans l'appréciation à laquelle elle s'est livrée, l'intérêt que présenterait, pour M. B..., le maintien de ces ouvrages, ni considéré qu'aucun ouvrage ne pourrait être autorisé sur le domaine public maritime, n'a pas commis d'erreur de droit.<br/>
<br/>
              4. Il résulte de tout ce qui précède que M. B...est seulement fondé à demander l'annulation de l'arrêt attaqué en tant qu'il concerne la passerelle.<br/>
<br/>
              5. Il y a lieu, dans les circonstances de l'espèce, de régler, dans cette mesure, l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative.<br/>
<br/>
              6. Aux termes du premier alinéa de l'article L. 2111-4 du code général de la propriété des personnes publiques : " Le domaine public maritime naturel de L'Etat comprend : 1° Le sol et le sous-sol de la mer entre la limite extérieure de la mer territoriale et, côté terre, le rivage de la mer. Le rivage de la mer est constitué par tout ce qu'elle couvre et découvre jusqu'où les plus hautes mers peuvent s'étendre en l'absence de perturbations météorologiques exceptionnelles (...) ; 3° Les lais et relais de la mer (...) ". Au sens de ces dispositions, le domaine public maritime ne comprend pas la masse des eaux.<br/>
<br/>
               7. Les parties hautes des falaises que la passerelle de M. B...permet de relier sont situées à environ 7 mètres au-dessus du niveau de la mer. Il ne ressort pas des pièces du dossier qu'elles seraient atteintes par les plus hautes mers en l'absence de perturbations météorologiques. Dès lors, les points de fixation de la passerelle ne peuvent être regardés comme installés sur le rivage de la mer et, par suite, sur le domaine public maritime. En outre, la seule présence de la passerelle de M. B...au surplomb de la mer  n'emporte pas occupation du domaine public maritime. Il en résulte que le préfet du Var ne pouvait que rejeter la demande de M. B...qui tendait à ce qu'il lui délivre une autorisation d'implanter la passerelle litigieuse sur le domaine public maritime. Il découle en revanche de ce que la passerelle n'occupait pas le domaine public maritime que le préfet ne pouvait pas légalement le mettre en demeure de la démolir en vue de mettre fin à une occupation irrégulière de ce domaine. M. B...est, par suite, seulement fondé à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Toulon a rejeté sa demande  relative à l'injonction de démolir la passerelle et à demander, dans cette mesure, l'annulation de la décision du préfet du Var du 14 mars 2014.<br/>
<br/>
              8. Il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions de M. B...présentées au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>                               D E C I D E :<br/>
                                              --------------<br/>
<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Marseille du 16 mars 2017 est annulé en tant qu'il se prononce sur les conclusions à fin d'annulation de la décision du préfet du Var du 14 mars 2014 en ce qui concerne la passerelle.<br/>
<br/>
Article 2 : Le jugement n° 1401895 du 6 novembre 2015 du tribunal administratif de Toulon et la décision du préfet du Var du 14 mars 2014 sont annulés en tant qu'ils sont relatifs à l'injonction de démolir la passerelle.<br/>
<br/>
Article  3 : Le surplus des conclusions du pourvoi de M. B...est rejeté.<br/>
<br/>
Article 4 : La présente décision sera notifiée à M.A... B... et au ministre d'Etat, ministre de la transition écologique et solidaire.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
