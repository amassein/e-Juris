<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034056068</ID>
<ANCIEN_ID>J5_L_2017_02_000001600519</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/05/60/CETATEXT000034056068.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>CAA de NANCY, 1ère chambre - formation à 3, 09/02/2017, 16NC00519, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-02-09</DATE_DEC>
<JURIDICTION>CAA de NANCY</JURIDICTION>
<NUMERO>16NC00519</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère chambre - formation à 3</FORMATION>
<TYPE_REC>plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. MESLAY</PRESIDENT>
<AVOCATS>SELARL BISMUTH</AVOCATS>
<RAPPORTEUR>M. Philippe  REES</RAPPORTEUR>
<COMMISSAIRE_GVT>M. FAVRET</COMMISSAIRE_GVT>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
       Procédure contentieuse antérieure :<br/>
<br/>
       Le centre hospitalier régional de Metz-Thionville a demandé au tribunal administratif de Nancy d'annuler pour excès de pouvoir, d'une part, la décision du 22 décembre 2010 par laquelle le directeur général de l'agence régionale de santé de Lorraine lui a infligé une sanction d'un montant de 1 230 114 euros et, d'autre part, la décision implicite de rejet résultant du silence gardé par ce dernier sur son recours gracieux dirigé contre cette décision. <br/>
<br/>
       Par un jugement n° 1101052 du 14 mai 2013, le tribunal administratif de Nancy a annulé ces décisions.<br/>
<br/>
       Par un arrêt n° 13NC01380 du 25 juillet 2014, la cour administrative d'appel de Nancy a, sur recours du ministre des affaires sociales et de la santé, fixé à 426 303 euros le montant de la sanction et réformé en ce sens le jugement du tribunal administratif de Nancy du 14 mai 2013.<br/>
<br/>
       Par une décision n° 384803 du 16 mars 2016, le Conseil d'Etat a annulé cet arrêt et renvoyé l'affaire à la cour administrative d'appel de Nancy. <br/>
<br/>
       Procédure devant la cour :<br/>
<br/>
       Par une requête enregistrée le 31 juillet 2013, le ministre des affaires sociales et de la santé demande à la cour :<br/>
<br/>
       1°) d'annuler le jugement n° 1101052 du 14 mai 2013 du tribunal administratif de Nancy ;<br/>
<br/>
       2°) de rejeter la demande présentée par le centre hospitalier régional de Metz-Thionville devant le tribunal administratif de Nancy.<br/>
<br/>
       Le ministre soutient que :<br/>
<br/>
       - le jugement est entaché d'irrégularité, les premiers juges ayant méconnu leur office et violé le principe du contradictoire en se déclarant dans l'impossibilité de fixer eux-mêmes la sanction, sans tenir compte des éléments qui leur avaient été fournis et leur permettaient de le faire ; <br/>
       - la sanction litigieuse a été infligée au terme d'une procédure régulière ;<br/>
       - les premiers juges étaient à même d'apprécier le caractère réitéré des manquements, dont il était justifié devant eux ;<br/>
       - la sanction a été calculée conformément au barème fixé par les nouvelles dispositions de l'article R. 162-42-12 du code de la sécurité sociale ;<br/>
       - aucun des autres moyens soulevés par le centre hospitalier en première instance n'est fondé.<br/>
<br/>
<br/>
       Par un mémoire en défense, enregistré le 10 janvier 2014, le centre hospitalier régional de Metz-Thionville, représenté par MeA..., conclut au rejet de la requête et à la condamnation de l'Etat au versement d'une somme de 3 000 euros en application de l'article L. 761-1 du code de justice administrative ainsi qu'à la prise en charge des entiers dépens. <br/>
<br/>
       Le centre hospitalier soutient que :<br/>
<br/>
       - la requête, enregistrée après expiration du délai d'appel, est irrecevable ;<br/>
       - le tribunal devait appliquer la loi pénale plus douce issue du décret n° 2011-1209 du 29 septembre 2011 ayant modifié notamment l'article R. 162-42-12 du code de la sécurité sociale ; il ne pouvait toutefois pas substituer sa propre sanction à celle décidée par l'administration ;<br/>
       - l'administration n'a pris en compte les critères, nouveaux, relatifs à la sous-facturation et au caractère réitéré des manquements ;<br/>
       - le caractère réitéré des manquements n'est pas établi ;<br/>
       - la décision de sanction n'est pas suffisamment motivée dès lors que sa motivation ne reflète pas la méthode de calcul mise en oeuvre et ne permet ni d'expliquer les réductions successives des propositions de sanction, ni de vérifier que ses observations ont été prises en compte ;<br/>
       - l'administration ne pouvait prendre la sanction litigieuse au terme d'une procédure engagée avant la modification de l'article L. 162-22-18 du code de la sécurité sociale par la loi du 20 décembre 2010 de financement de la sécurité sociale pour 2011, laquelle était entrée en vigueur à la date de la décision ;<br/>
       - les modalités de contrôle mises en oeuvre sont irrégulières au regard de l'article R. 162-42-10 du code de la sécurité sociale dès lors que les échantillons retenus, qui doivent tenir compte de la taille réelle de l'activité soumise à contrôle, ne sont pas représentatifs ; en outre, ils ne permettent pas de caractériser la réitération des manquements ; <br/>
       - les contrôles systématiques en fin d'exercice budgétaire dont il a fait l'objet en 2008, 2009 et 2010 ne lui permettent pas de corriger les erreurs relevées, de sorte que les mêmes manquements sont sanctionnés à plusieurs reprises ;<br/>
       - l'administration a commis une première erreur de droit dès lors que, pour fixer le montant de la sanction, elle a omis de prendre en compte le critère du caractère réitéré des manquements, prévu par l'article L. 162-22-18 du code de la sécurité sociale dans sa rédaction issue de la loi du 20 décembre 2010 ;<br/>
       - l'administration a commis une seconde erreur de droit dès lors qu'elle a appliqué de façon systématique le pourcentage de surfacturation au montant maximal des sanctions ;<br/>
       - la réalité des surfacturations n'est pas établie eu égard à la méthode d'échantillonnage mise en oeuvre, qui n'est pas représentative en ce qu'elle comporte une marge d'erreur ;<br/>
       - le montant de la sanction est manifestement disproportionné, alors qu'il est huit fois supérieur à celui des surfacturations et qu'aucune réitération, fraude ou mauvaise foi ne sont établies et que le bénéfice des actions correctrices n'a pu être observé du fait de la succession des contrôles externes.<br/>
<br/>
<br/>
       Par un mémoire, enregistré le 24 avril 2014, le ministre des affaires sociales conclut aux mêmes fins que dans son recours par les mêmes moyens.<br/>
<br/>
       Le ministre soutient, en outre, que son recours est recevable et qu'aucun des moyens soulevés par le centre hospitalier n'est fondé. <br/>
<br/>
<br/>
       Par des mémoires, enregistrés le 22 juillet et 19 septembre 2016, après renvoi, le centre hospitalier régional de Metz-Thionville conclut aux mêmes fins et par les mêmes moyens que précédemment. En outre, il demande à la cour d'annuler les décisions contestées du directeur général de l'Agence régionale de santé de Lorraine et il porte à la somme de 3 500 euros ses conclusions tendant à l'application de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
       Le centre hospitalier soutient, en outre, que :<br/>
<br/>
       - la décision est entachée d'un vice de procédure dès lors que la commission de contrôle mentionnée à l'article L. 162-22-18 du code de la sécurité sociale ne s'est prononcée que sur la base du pourcentage des sommes indûment perçues par rapport aux sommes dues, alors qu'elle aurait également dû prendre en compte le critère portant sur le caractère réitéré des manquements ;<br/>
       - la décision de sanction est notamment fondée sur l'existence d'une prestation non médicalement justifiée, alors qu'un tel motif ne figure pas au nombre de ceux pouvant être sanctionnés sur le fondement de l'article L. 162-22-18 du code de la sécurité sociale.<br/>
<br/>
<br/>
       Par un mémoire, enregistré le 1er septembre 2016, le ministre des affaires sociales conclut aux mêmes fins que dans son recours par les mêmes moyens.<br/>
<br/>
       Il soutient, en outre, que :<br/>
<br/>
       - le Conseil d'Etat a confirmé la régularité de la motivation de la décision ;<br/>
       - le critère du caractère réitéré des manquements n'était pas applicable à la date de la décision attaquée ;<br/>
       - c'est à tort que le tribunal administratif a fait application de la loi pénale plus douce alors qu'il devait statuer en excès de pouvoir. <br/>
<br/>
<br/>
       Vu les autres pièces du dossier.<br/>
<br/>
       Vu : <br/>
       - le code de la santé publique,<br/>
       - le code de la sécurité sociale,<br/>
       - la loi n° 79-587 du 11 juillet 1979 relative à la motivation des actes administratifs et à l'amélioration des relations entre l'administration et le public,<br/>
       - la loi n° 2010-1594 du 20 décembre 2010 de financement de la sécurité sociale pour 2011,<br/>
       - le décret n° 2011-1209 du 29 septembre 2011 modifiant les dispositions relatives au contrôle de la tarification à l'activité des établissements de santé,<br/>
       - le code de justice administrative. <br/>
<br/>
<br/>
       Les parties ont été régulièrement averties du jour de l'audience.<br/>
<br/>
       Ont été entendus au cours de l'audience publique :<br/>
       - le rapport de M. Rees, premier conseiller,<br/>
       - et les conclusions de M. Favret, rapporteur public.<br/>
<br/>
<br/>
       Considérant ce qui suit :<br/>
<br/>
       1. Le centre hospitalier régional de Metz-Thionville a fait l'objet, du 12 au 30 octobre 2009, dans le cadre du programme régional de contrôle mis en oeuvre par l'agence régionale de santé de Lorraine, d'un audit externe relatif à l'application, au cours de l'année 2008, de la tarification à l'activité. Le contrôle a porté, notamment, sur les activités codifiées par groupes homogènes de malades sous les intitulés " 24M ", " 24C15Z, 24C55Z, 24C48Z ou 24C02Z ", " 24K29Z ou 24K20Z ", " séjours avec comorbidité " et " séjours pour complications ".<br/>
<br/>
       2. Après avoir constaté, pour ces cinq catégories d'activités, des manquements aux règles de facturation prévues à l'article L. 162-22-6 du code de la sécurité sociale et aux règles de codage du guide méthodologique de production des résumés de sortie, le directeur général de l'agence régionale de santé a, le 22 décembre 2010, décidé d'infliger des sanctions financières au centre hospitalier régional de Metz-Thionville pour un montant total de 1 230 114 euros. Le 7 février 2011, le centre hospitalier a formé un recours gracieux auquel le directeur général de l'agence régionale de santé n'a pas répondu.<br/>
<br/>
       3. Par un jugement n° 1101052 du 14 mai 2013, le tribunal administratif de Nancy a prononcé l'annulation de la décision expresse du directeur général de l'agence régionale de santé du 22 décembre 2010, ainsi que de la décision implicite de rejet née du silence qu'il avait gardé sur le recours gracieux formé par le centre hospitalier. <br/>
<br/>
       4. Par un arrêt n° 13NC01380, la cour administrative d'appel de Nancy, sur l'appel du ministre des affaires sociales et de la santé, a, d'une part, fixé le montant de la sanction à 426 303 euros et, d'autre part, réformé le jugement du tribunal administratif de Nancy du 14 mai 2013 annulant cette sanction.<br/>
<br/>
       5. Par une décision n° 384803 du 16 mars 2016, le Conseil d'Etat a annulé cet arrêt et renvoyé l'affaire à la cour administrative d'appel de Nancy, laquelle se trouve ainsi à nouveau saisie de tout le litige.<br/>
<br/>
       Sans qu'il soit besoin de statuer sur la fin de non-recevoir soulevée par le centre hospitalier régional de Metz-Thionville et tirée de la tardiveté du recours en appel ;<br/>
<br/>
       Sur la régularité du jugement :<br/>
<br/>
       6. Le ministre des affaires sociales et de la santé soutient que les premiers juges ont méconnu leur office et violé le principe du contradictoire en se déclarant dans l'impossibilité de fixer eux-mêmes le montant de la sanction financière, sans tenir compte des éléments qui leur avaient pourtant été fournis et leur permettaient de le faire.<br/>
<br/>
       7. Toutefois, un recours devant le juge administratif tendant à l'annulation d'une sanction prononcée à l'encontre d'un établissement de santé sur le fondement de l'article L. 162-22-18 du code de la sécurité sociale, qui a le caractère d'une sanction visant à réprimer la méconnaissance des règles particulières auxquelles est soumis l'exercice d'une activité professionnelle déterminée, relève du contentieux de l'excès de pouvoir (Conseil d'Etat, 16 mars 2016, n° 384803).<br/>
<br/>
       8. Ainsi, il n'appartenait pas aux premiers juges de fixer eux-mêmes le montant de la sanction financière et ils n'ont ni méconnu leur office, ni violé le principe du contradictoire en s'abstenant de le faire. <br/>
<br/>
       9. Dès lors, le ministre n'est pas fondé à soutenir que le jugement attaqué se trouverait, de ce fait, entaché d'irrégularité.<br/>
<br/>
       Sur le motif d'annulation retenu par le tribunal :<br/>
<br/>
       10. Le tribunal administratif a prononcé l'annulation des décisions litigieuses au motif que les dispositions des articles L. 162-42-18 et R. 162-42-12 du code de la sécurité sociale, dans leurs rédactions issues, respectivement, de la loi du 20 décembre 2010 et du décret du 29 septembre 2011 susvisés, prévoient des peines moins sévères que dans leurs anciennes rédactions. <br/>
<br/>
       11. Il résulte de ce qui a été dit au point 7 qu'il appartenait aux premiers juges, statuant en excès de pouvoir, d'examiner la légalité des décisions attaquées au regard des règles de droit applicables à la date de leur édiction. <br/>
<br/>
       12. C'est donc à tort que le tribunal s'est fondé, notamment, sur les dispositions de l'article R. 162-42-12 du code de la sécurité sociale dans leur rédaction issue du décret du 29 septembre 2011, qui n'étaient pas applicables à la date des décisions attaquées.  <br/>
<br/>
       13. Toutefois, il appartient à la cour administrative d'appel, saisie de l'ensemble du litige par l'effet dévolutif de l'appel, d'examiner les autres moyens soulevés par le centre hospitalier régional de Metz-Thionville, tant devant le tribunal administratif de Nancy que devant la cour en appel.<br/>
<br/>
       Sur les autres moyens soulevés par le centre hospitalier régional de Metz-Thionville :<br/>
<br/>
       14. Le centre hospitalier soutient que le directeur général de l'Agence régionale de santé a commis une erreur de droit dès lors que, pour fixer le montant de la sanction, il a omis de prendre en compte le critère du caractère réitéré des manquements, prévu par l'article L. 162-22-18 du code de la sécurité sociale dans sa rédaction issue de la loi du 20 décembre 2010 susvisée.<br/>
<br/>
       15. Aux termes de l'article 1er du code civil : " Les lois et, lorsqu'ils sont publiés au Journal officiel de la République française, les actes administratifs entrent en vigueur à la date qu'ils fixent ou, à défaut, le lendemain de leur publication. Toutefois, l'entrée en vigueur de celles de leurs dispositions dont l'exécution nécessite des mesures d'application est reportée à la date d'entrée en vigueur de ces mesures ".<br/>
<br/>
       16. Tout d'abord, la loi du 20 décembre 2010 susvisée ne comporte aucune disposition fixant sa date d'entrée en vigueur. <br/>
<br/>
       17. Ensuite, son article 115 a complété la première phrase du troisième alinéa de l'article L. 162-22-18 du code de la sécurité sociale, aux termes de laquelle le montant de la sanction " est fonction du pourcentage des sommes indûment perçues par rapport aux sommes dues " par les mots " et du caractère réitéré des manquements ". <br/>
<br/>
       18. Contrairement à ce que soutient le ministre, l'article 115 ne comporte aucun renvoi à une mesure réglementaire pour la mise en oeuvre de cet ajout. La circonstance que l'article L. 162-22-18 prévoyait, déjà dans sa rédaction antérieure, à son dernier alinéa, que " les modalités d'application du présent article sont définies par décret en Conseil d'Etat ", ne suffit pas à rendre nécessaire l'intervention d'un tel décret en Conseil d'Etat à chacune des modifications apportées à cet article. <br/>
<br/>
       19. En outre, le critère précité est défini de manière suffisamment précise par l'article 115 de la loi et la prise en compte, pour la détermination du montant de la sanction, du caractère réitéré des manquements, n'est pas manifestement impossible en l'absence de mesure réglementaire d'application. <br/>
<br/>
       20. Enfin, la loi du 20 décembre 2010 a été publiée au Journal officiel de la République française le 21 décembre 2010. En application de l'article 1er du code civil précité, elle est entrée en vigueur le lendemain, soit le 22 décembre 2010. <br/>
<br/>
       21. Dès lors, il appartenait au directeur général de l'Agence régionale de santé, pour fixer le montant des sanctions financières infligées au centre hospitalier régional de Metz-Thionville en application de l'article L. 162-22-18 du code de la sécurité sociale dans sa rédaction en vigueur à la date de sa décision du 22 décembre 2010, de tenir compte non seulement du pourcentage des sommes indûment perçues par rapport aux sommes dues, mais également du caractère réitéré des manquements reprochés à l'établissement.<br/>
<br/>
       22. Il est constant que le directeur général de l'Agence régionale de santé a omis de prendre en compte ce second critère pour édicter sa décision du 22 décembre 2010 et pour rejeter de manière implicite le recours gracieux du centre hospitalier du 7 février 2011. Par suite, ses deux décisions sont entachées d'une erreur de droit et doivent être annulées. <br/>
<br/>
       23. En conclusion de tout ce qui précède, et sans qu'il soit nécessaire d'examiner les autres moyens présentés par le centre hospitalier régional de Metz-Thionville, le ministre des affaires sociales et de la santé n'est pas fondé à se plaindre de ce que le tribunal administratif de Nancy a annulé les deux décisions litigieuses. <br/>
<br/>
       Sur les conclusions tendant à l'application des dispositions de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
       24. Aux termes de l'article L. 761-1 du code de justice administrative : " Dans toutes les instances, le juge condamne la partie tenue aux dépens ou, à défaut, la partie perdante, à payer à l'autre partie la somme qu'il détermine, au titre des frais exposés et non compris dans les dépens. Le juge tient compte de l'équité ou de la situation économique de la partie condamnée. Il peut, même d'office, pour des raisons tirées des mêmes considérations, dire qu'il n'y pas lieu à cette condamnation ".<br/>
<br/>
       25. Dans les circonstances de l'espèce, il y a lieu de mettre à la charge de l'Etat une somme de 2 000 euros à verser au centre hospitalier régional de Metz-Thionville au titre de ces dispositions. <br/>
<br/>
<br/>
       Par ces motifs,<br/>
<br/>
<br/>
     DECIDE :<br/>
<br/>
Article 1er : La requête du ministre des affaires sociales et de la santé est rejetée.<br/>
Article 2 : L'Etat versera au centre hospitalier régional de Metz-Thionville une somme de 2 000 (deux mille) euros en application des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : Le présent arrêt sera notifié au ministre des affaires sociales et de la santé et au centre hospitalier régional de Metz-Thionville.<br/>
Copie en sera adressée au préfet de la Moselle.<br/>
2<br/>
16NC00519<br/>
<br/>
<br/>
<br/>
</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">61-09-01 Santé publique. Administration de la santé. Agences régionales d'hospitalisation.
</SCT>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
