<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043038335</ID>
<ANCIEN_ID>JG_L_2021_01_000000439248</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/03/83/CETATEXT000043038335.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème - 9ème chambres réunies, 21/01/2021, 439248</TITRE>
<DATE_DEC>2021-01-21</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>439248</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème - 9ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FOUSSARD, FROGER ; CARBONNIER</AVOCATS>
<RAPPORTEUR>M. Arno Klarsfeld</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Alexandre Lallet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:439248.20210121</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme B... C..., épouse A..., a demandé à la Cour nationale du droit d'asile (CNDA), en son nom et au nom de ses enfants mineurs, D... et F... A..., d'annuler la décision du 15 juillet 2019 par laquelle l'Office français de protection des réfugiés et apatrides (OFPRA) a rejeté leur demande d'asile. <br/>
<br/>
              Par une décision n° 19043332 du 31 décembre 2019, la CNDA a annulé la décision de l'OFPRA et a accordé le bénéfice de la protection subsidiaire à Mme C... et à ses deux enfants mineurs. <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 2 mars et 29 mai 2020 au secrétariat du contentieux du Conseil d'Etat, l'OFPRA demande au Conseil d'Etat d'annuler cette décision. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la convention de Genève du 28 juillet 1951 relative au statut des réfugiés et le protocole signé à New York le 31 janvier 1967 ;<br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Arno Klarsfeld, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Alexandre Lallet, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Foussard, Froger, avocat de l'OFPRA et à Me E..., avocat de Mme B... C..., épouse A..., de M. D... A... et de M. F... A... ;<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. L'article L. 712-1 du code de l'entrée et du séjour des étrangers et du droit d'asile dispose que : " Le bénéfice de la protection subsidiaire est accordé à toute personne qui ne remplit pas les conditions pour se voir reconnaître la qualité de réfugié et pour laquelle il existe des motifs sérieux et avérés de croire qu'elle courrait dans son pays un risque réel de subir l'une des atteintes graves suivantes : a) La peine de mort ou une exécution ; b) La torture ou des peines ou traitements inhumains ou dégradants ; c) S'agissant d'un civil, une menace grave et individuelle contre sa vie ou sa personne en raison d'une violence qui peut s'étendre à des personnes sans considération de leur situation personnelle et résultant d'une situation de conflit armé interne ou international ".<br/>
<br/>
              2. Aux termes du deuxième alinéa de l'article L. 741-1 du code de l'entrée et du séjour des étrangers et du droit d'asile : " Lorsque la demande d'asile est présentée par un étranger qui se trouve en France accompagné de ses enfants mineurs, la demande est regardée comme présentée en son nom et en celui de ses enfants. Lorsqu'il est statué sur la demande de chacun des parents, la décision accordant la protection la plus étendue est réputée prise également au bénéfice des enfants. Cette décision n'est pas opposable aux enfants qui établissent que la personne qui a présenté la demande n'était pas en droit de le faire. "<br/>
<br/>
              3. Mme B... C..., épouse A..., de nationalité albanaise, a été admise au bénéfice de la protection subsidiaire par une décision du 31 décembre 2019 de la Cour nationale du droit d'asile (CNDA). Par cette même décision, la CNDA a également accordé le bénéfice de la protection subsidiaire à ses enfants mineurs, D... et F... A..., en application des dispositions précitées de l'article L. 741-1 du code de l'entrée et du séjour des étrangers et du droit d'asile. A l'appui de son pourvoi en cassation contre cette décision, l'OFPRA soulève un unique moyen tiré de ce que la CNDA aurait commis une erreur de droit en faisant application de ces dispositions à la situation des enfants d'un parent admis au bénéfice de la protection subsidiaire.<br/>
<br/>
              4. Il résulte des dispositions du deuxième alinéa de l'article L. 741-1 citées au point 2, d'une part, que lorsqu'un étranger se trouvant en France accompagné de ses enfants mineurs se voit accorder l'asile, que ce soit en qualité de réfugié ou au titre de la protection subsidiaire, la protection qui lui est accordée l'est également à ses enfants mineurs et, d'autre part, que lorsqu'il est statué sur la demande de chacun des parents, la décision accordant la protection la plus étendue est réputée prise aussi au bénéfice des enfants. Ainsi, ces dispositions sont applicables aux enfants de réfugiés, qui pourraient par ailleurs invoquer le principe de l'unité de famille, mais également aux enfants des bénéficiaires de la protection subsidiaire, qui ne sauraient se prévaloir d'un tel principe général du droit des réfugiés. Dès lors, en interprétant ces dispositions comme s'appliquant aux enfants d'un parent bénéficiant de la protection subsidiaire, la CNDA n'a pas méconnu le champ d'application du principe de l'unité de famille résultant de la convention de Genève ni commis d'erreur de droit.<br/>
<br/>
              5. Il résulte de ce qui précède que le pourvoi de l'OFPRA doit être rejeté. Il y a lieu, dans les circonstances de l'espèce, de mettre à sa charge une somme de 3 000 euros à verser à Me E..., avocat de Mme C..., épouse A..., agissant en son nom et au nom de ses enfants mineurs, au titre des articles 37 de la loi du 10 juillet 1991 et L.761-1 du code de justice administrative, sous réserve que celui-ci renonce à percevoir la somme correspondant à la part contributive de l'Etat.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
                                   --------------<br/>
<br/>
Article 1er : Le pourvoi de l'Office français de protection des réfugiés et apatrides est rejeté.<br/>
Article 2 : L'Office français de protection des réfugiés et apatrides versera une somme de<br/>
3 000 euros à Me E... au titre des articles 37 de la loi du 10 juillet 1991 et L.761-1 du code de justice administrative, sous réserve que celui-ci renonce à percevoir la somme correspondant à la part contributive de l'Etat.<br/>
Article 3 : La présente décision sera notifiée à l'Office français de protection des réfugiés et apatrides et à Mme B... C..., épouse A.... <br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-04-03-04 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - VIOLATION DIRECTE DE LA RÈGLE DE DROIT. PRINCIPES GÉNÉRAUX DU DROIT. PRINCIPES GARANTISSANT L'EXERCICE DE LIBERTÉS INDIVIDUELLES OU COLLECTIVES. - PRINCIPES GÉNÉRAUX DU DROIT APPLICABLES AUX RÉFUGIÉS - PRINCIPE D'UNITÉ DE LA FAMILLE [RJ1] - APPLICABILITÉ AUX ENFANTS DES BÉNÉFICIAIRES DE LA PROTECTION SUBSIDIAIRE - ABSENCE [RJ2].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">095-03-03-01 - APPLICABILITÉ AUX ENFANTS DES BÉNÉFICIAIRES DE LA PROTECTION SUBSIDIAIRE - ABSENCE [RJ2].
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">095-06 - PROTECTION ÉTENDUE AUX ENFANTS MINEURS - AU TITRE DU PRINCIPE DE L'UNITÉ DE LA FAMILLE [RJ1] - ABSENCE [RJ2] - SUR LE FONDEMENT DE L'ARTICLE L. 741-1 DU CESEDA - EXISTENCE.
</SCT>
<ANA ID="9A"> 01-04-03-04 Il résulte du deuxième alinéa de l'article L. 741-1 du code de l'entrée et du séjour des étrangers et du droit d'asile (CESEDA), d'une part, que lorsqu'un étranger se trouvant en France accompagné de ses enfants mineurs se voit accorder l'asile, que ce soit en qualité de réfugié ou au titre de la protection subsidiaire, la protection qui lui est accordée l'est également à ses enfants mineurs et, d'autre part, que lorsqu'il est statué sur la demande de chacun des parents, la décision accordant la protection la plus étendue est réputée prise aussi au bénéfice des enfants. Ainsi, ces dispositions sont applicables aux enfants de réfugiés, qui pourraient par ailleurs invoquer le principe de l'unité de famille, mais également aux enfants des bénéficiaires de la protection subsidiaire, qui ne sauraient se prévaloir d'un tel principe général du droit des réfugiés.</ANA>
<ANA ID="9B"> 095-03-03-01 Il résulte du deuxième alinéa de l'article L. 741-1 du code de l'entrée et du séjour des étrangers et du droit d'asile (CESEDA), d'une part, que lorsqu'un étranger se trouvant en France accompagné de ses enfants mineurs se voit accorder l'asile, que ce soit en qualité de réfugié ou au titre de la protection subsidiaire, la protection qui lui est accordée l'est également à ses enfants mineurs et, d'autre part, que lorsqu'il est statué sur la demande de chacun des parents, la décision accordant la protection la plus étendue est réputée prise aussi au bénéfice des enfants. Ainsi, ces dispositions sont applicables aux enfants de réfugiés, qui pourraient par ailleurs invoquer le principe de l'unité de famille, mais également aux enfants des bénéficiaires de la protection subsidiaire, qui ne sauraient se prévaloir d'un tel principe général du droit des réfugiés.</ANA>
<ANA ID="9C"> 095-06 Il résulte du deuxième alinéa de l'article L. 741-1 du code de l'entrée et du séjour des étrangers et du droit d'asile (CESEDA), d'une part, que lorsqu'un étranger se trouvant en France accompagné de ses enfants mineurs se voit accorder l'asile, que ce soit en qualité de réfugié ou au titre de la protection subsidiaire, la protection qui lui est accordée l'est également à ses enfants mineurs et, d'autre part, que lorsqu'il est statué sur la demande de chacun des parents, la décision accordant la protection la plus étendue est réputée prise aussi au bénéfice des enfants. Ainsi, ces dispositions sont applicables aux enfants de réfugiés, qui pourraient par ailleurs invoquer le principe de l'unité de famille, mais également aux enfants des bénéficiaires de la protection subsidiaire, qui ne sauraient se prévaloir d'un tel principe général du droit des réfugiés.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf., sur le principe d'unité de la famille, CE, Assemblée, 2 décembre 1994,,, n° 112842, p. 523.,,[RJ2] Cf., sur l'inapplicabilité des principes généraux du droit des réfugiés aux bénéficiaires de la protection subsidiaire, CE, 18 décembre 2008, Office français de protection des réfugiés et apatrides c/ Mme,épouse,, n° 283245, T. p. 775.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
