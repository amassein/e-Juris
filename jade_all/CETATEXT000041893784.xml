<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000041893784</ID>
<ANCIEN_ID>JG_L_2020_05_000000440367</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/41/89/37/CETATEXT000041893784.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 15/05/2020, 440367, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-05-15</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>440367</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2020:440367.20200515</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              M. B... A... a demandé au juge des référés du tribunal administratif de Lille, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, d'ordonner toutes mesures qu'il estimera utiles afin de faire cesser les atteintes graves et manifestement illégales portées à ses libertés fondamentales durant l'épidémie de covid-19, et notamment de : <br/>
              - distribuer à l'ensemble des personnes détenues et en particulier aux auxiliaires qui assurent la distribution de ses repas des masques et gels hydro- alcooliques en quantité suffisante notamment durant l'exécution des tâches prévues ;<br/>
              - garantir qu'un nettoyage régulier et renforcé de l'ensemble des établissements soit réalisé, en particulier concernant les points de contact propices à la transmission du virus entre les détenus mais aussi avec l'ensemble du personnel pénitentiaire (portes, barreaux, linge, téléphones dans les coursives, etc.) ;<br/>
              - mettre en place des dépistages systématiques du covid-19 auprès des détenus, à tout le moins au sein du centre pénitentiaire de Vendin-le-Vieil et auprès de chaque nouveau détenu ou autres personnes entrant dans l'établissement pénitentiaire ;<br/>
              - communiquer le plan des mesures prévues en cas de diffusion rapide de l'épidémie au sein du centre pénitentiaire de Vendin-le-Vieil ou, en l'absence d'un tel plan à ce stade, de prévoir une série de plans au niveau de l'établissement pénitentiaire, en concertation avec les autorités et établissements sanitaires locaux ainsi qu'avec toute autre autorité publique compétente au niveau local ;<br/>
              - assurer le lavage des draps avec la régularité indispensable aux conditions minimales d'hygiène ;<br/>
              - assurer le nettoyage régulier du linge personnel des détenus ;<br/>
              - fournir du savon en quantité suffisante aux détenus afin de garantir leur hygiène personnelle en période de crise sanitaire ;<br/>
              - garantir la mise en place dans l'ensemble des établissements pénitentiaires de modalités de service des repas adaptées à la situation sanitaire ;<br/>
              - prévoir que le recours aux fouilles des détenus durant la période de crise sanitaire doit être particulièrement exceptionnel et accomplies en conformité avec les gestes barrières et la distanciation sociale adéquate.<br/>
<br/>
              Par une ordonnance n° 2003151 du 24 avril 2020, le juge des référés du tribunal administratif de Lille a rejeté sa demande. <br/>
<br/>
              Par une requête et un mémoire, enregistrés les 4 et 12 mai au secrétariat du contentieux du Conseil d'Etat, M. A... demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative :<br/>
<br/>
              1°) de l'admettre au bénéfice de l'aide juridictionnelle provisoire ;<br/>
<br/>
              2°) d'annuler cette ordonnance ;<br/>
<br/>
              3°) de prescrire toute mesure de constat, en application des dispositions de l'article R. 622-1 du code de justice administrative ;<br/>
<br/>
              4°) de faire droit à ses demandes de première instance ;<br/>
<br/>
              5°) de mettre à la charge de l'administration pénitentiaire le versement à son conseil de la somme de 2 000 euros au titre des dispositions combinées des articles L. 761-1 du code de justice administrative et 37 et 75-1 du décret du 11 juillet 1991.<br/>
<br/>
<br/>
<br/>
<br/>
              Il soutient que :<br/>
              - la condition d'urgence est remplie eu égard aux conditions de détention entrainant des risques particuliers d'exposition au covid-19, à la saturation actuelle des hôpitaux qui ne pourra que s'aggraver en cas de propagation massive du virus covid-19 et à son état de santé ;<br/>
              - l'absence d'édiction par l'administration, au sein du centre pénitentiaire de Vendin-le-Vieil, de mesures suffisantes pour le protéger de l'épidémie et réduire le risque de contamination révèle une carence qui porte une atteinte grave et manifestement illégale au droit respect de la vie, rappelé notamment à l'article 2 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, au droit de ne pas être soumis à des traitements inhumains ou dégradants, rappelé notamment à l'article 3 de la convention précitée, ainsi qu'au droit de recevoir les traitements et les soins appropriés à son état de santé. Une telle carence est caractérisée en particulier s'agissant de la distribution d'équipements de travail et de matériels de protection, tels que les masques et gels hydroalcooliques, et de produits d'hygiène et d'entretien en grande quantité, de la mise en place de dépistages systématiques du virus covid-19, de communication du plan des mesures prévues en cas de diffusion rapide de l'épidémie au sein de l'établissement et du respect des règles de distanciation sociale, en raison du maintien des mesures de palpations de sécurité.<br/>
              Par un mémoire en défense et un nouveau mémoire, enregistrés les 11 et 13 mai 2020, la garde des sceaux, ministre de la justice, conclut au rejet de la requête. Elle soutient que la requête est irrecevable dès lors qu'elle constitue la reproduction pure et simple de la demande de première instance et ne comporte aucun moyen d'appel et qu'il n'est porté aucune atteinte grave et manifestement illégale aux libertés fondamentales invoquées.<br/>
<br/>
              La requête a été communiquée au Premier ministre et au ministre des solidarités et de la santé qui n'ont pas produit d'observations.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la Constitution, et notamment son préambule ;<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - le code de la santé publique ;<br/>
              - le code de procédure pénale ;<br/>
              - la loi n° 2020-290 du 23 mars 2020 ;<br/>
              - la loi n° 91- du 10 juillet 1991 ;<br/>
              - l'ordonnance n° 2020-303 du 25 mars 2020 ;<br/>
              - le décret n° 2020-293 du 23 mars 2020 ;<br/>
              - le code de justice administrative et l'ordonnance n° 2020-305 du 25 mars 2020 ;<br/>
<br/>
              Les parties ont été informées, sur le fondement de l'article 9 de l'ordonnance du 25 mars 2020 portant adaptation des règles applicables devant les juridictions de l'ordre administratif, de ce qu'aucune audience ne se tiendrait et de ce que la clôture de l'instruction serait fixée le 13 mai 2020 à 17 heures. <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. L'article L. 511-1 du code de justice administrative dispose que : " Le juge des référés statue par des mesures qui présentent un caractère provisoire. Il n'est pas saisi du principal et se prononce dans les meilleurs délais. " Aux termes de l'article L. 521-2 du même code : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. Le juge des référés se prononce dans un délai de quarante-huit heures. ". <br/>
<br/>
              Sur les circonstances : <br/>
<br/>
              2. L'émergence d'un nouveau coronavirus (covid-19), de caractère pathogène et particulièrement contagieux et sa propagation sur le territoire français ont conduit le ministre des solidarités et de la santé à prendre, par plusieurs arrêtés à compter du 4 mars 2020, des mesures sur le fondement des dispositions de l'article L. 3131-1 du code de la santé publique. En particulier, par un arrêté du 14 mars 2020, un grand nombre d'établissements recevant du public ont été fermés au public, les rassemblements de plus de 100 personnes ont été interdits et l'accueil des enfants, élèves et étudiants dans les établissements les recevant et les établissements scolaires et universitaires a été suspendu. Puis, par un décret du 16 mars 2020 motivé par les circonstances exceptionnelles découlant de l'épidémie de covid-19, modifié par décret du 19 mars, le Premier ministre a interdit le déplacement de toute personne hors de son domicile, sous réserve d'exceptions limitativement énumérées et devant être dûment justifiées, à compter du 17 mars à 12h, sans préjudice de mesures plus strictes susceptibles d'être ordonnées par le représentant de l'Etat dans le département. Le ministre des solidarités et de la santé a pris des mesures complémentaires par des plusieurs arrêtés successifs. <br/>
<br/>
              3. Par l'article 4 de la loi du 23 mars 2020 d'urgence pour faire face à l'épidémie de covid-19, a été déclaré l'état d'urgence sanitaire pour une durée de deux mois sur l'ensemble du territoire national. Par un nouveau décret du 23 mars 2020 pris sur le fondement de l'article L. 3131-15 du code de la santé publique issu de la loi du 23 mars 2020, plusieurs fois modifié et complété depuis lors, le Premier ministre a réitéré les mesures précédemment ordonnées tout en leur apportant des précisions ou restrictions complémentaires. Leurs effets ont été prolongés en dernier lieu par décret du 14 avril 2020. Par un nouveau décret du 11 mai 2020, applicable les 11 et 12 mai 2020, le Premier ministre a modifié les mesures précédemment ordonnées par le décret du 23 mars 2020. Enfin, par un décret du 11 mai 2020, pris sur le fondement de la loi du 11 mai 2020 prorogeant l'état d'urgence sanitaire et complétant ses dispositions, le Premier ministre a prescrit les nouvelles mesures générales nécessaires pour faire face à l'épidémie de covid-19 dans le cadre de l'état d'urgence sanitaire.<br/>
<br/>
              Sur le cadre juridique du litige, l'office du juge des référés et les libertés fondamentales en jeu :<br/>
<br/>
              4. Dans l'actuelle période d'état d'urgence sanitaire, il appartient aux différentes autorités compétentes de prendre, en vue de sauvegarder la santé de la population, toutes dispositions de nature à prévenir ou à limiter les effets de l'épidémie. Ces mesures, qui peuvent limiter l'exercice des droits et libertés fondamentaux doivent, dans cette mesure, être nécessaires, adaptées et proportionnées à l'objectif de sauvegarde de la santé publique qu'elles poursuivent.<br/>
<br/>
              5. Il résulte de la combinaison des dispositions des articles L. 511-1 et L. 521-2 du code de justice administrative qu'il appartient au juge des référés, lorsqu'il est saisi sur le fondement de l'article L. 521-2 et qu'il constate une atteinte grave et manifestement illégale portée par une personne morale de droit public à une liberté fondamentale, résultant de l'action ou de la carence de cette personne publique, de prescrire les mesures qui sont de nature à faire disparaître les effets de cette atteinte, dès lors qu'existe une situation d'urgence caractérisée justifiant le prononcé de mesures de sauvegarde à très bref délai. Ces mesures doivent, en principe, présenter un caractère provisoire, sauf lorsque aucune mesure de cette nature n'est susceptible de sauvegarder l'exercice effectif de la liberté fondamentale à laquelle il est porté atteinte. Sur le fondement de l'article L. 521-2, le juge des référés peut ordonner à l'autorité compétente de prendre, à titre provisoire, des mesures d'organisation des services placés sous son autorité, dès lors qu'il s'agit de mesures d'urgence qui lui apparaissent nécessaires pour sauvegarder, à très bref délai, la liberté fondamentale à laquelle il est gravement, et de façon manifestement illégale, porté atteinte. Le caractère manifestement illégal de l'atteinte doit s'apprécier notamment en tenant compte des moyens dont dispose l'autorité administrative compétente et des mesures qu'elle a déjà prises. <br/>
<br/>
              6. Pour l'application de l'article L. 521-2 du code de justice administrative, le droit au respect de la vie, le droit de ne pas être soumis à des traitements inhumains ou dégradants ainsi que le droit de recevoir les traitements et les soins appropriés à son état de santé constituent des libertés fondamentales au sens des dispositions de cet article.<br/>
<br/>
              7. Eu égard à la vulnérabilité des détenus et à leur situation d'entière dépendance vis à vis de l'administration, il appartient à celle-ci, et notamment au garde des sceaux, ministre de la justice et aux directeurs des établissements pénitentiaires, en leur qualité de chefs de service, de prendre les mesures propres à protéger leur vie et à dispenser les traitements et les soins appropriés à leur état de santé ainsi qu'à leur éviter tout traitement inhumain ou dégradant afin de garantir le respect effectif des libertés fondamentales énoncées au point précédent. Lorsque la carence de l'autorité publique crée un danger caractérisé et imminent pour la vie des personnes, les expose à être soumises, de manière caractérisée, à un traitement inhumain ou dégradant ou conduit à ce qu'elles soient privées, de manière caractérisée, des traitements et des soins appropriés à leur état de santé portant ainsi une atteinte grave et manifestement illégale à ces libertés fondamentales, et que la situation permet de prendre utilement des mesures de sauvegarde dans un délai de quarante-huit heures, le juge des référés peut, au titre de la procédure particulière prévue par l'article L. 521-2, prescrire, dans les conditions et les limites définies au point 5, les mesures de nature à faire cesser la situation résultant de cette carence. <br/>
<br/>
              Sur la demande en référé :  <br/>
<br/>
              8. M. B... A..., détenu au centre pénitentiaire de Vendin-le-Vieil, a saisi, le 22 avril 2020, le juge des référés du tribunal administratif de Lille, sur le fondement de l'article L. 521-2 du code de justice administrative, d'une demande tendant à ce qu'il soit enjoint au chef de cet établissement pénitentiaire de prendre toutes les mesures nécessaires pour assurer sa protection, durant l'épidémie de covid-19, et réduire le risque de contamination. Il relève appel de l'ordonnance du 24 avril 2020 par laquelle le juge des référés a rejeté cette demande au motif qu'aucune atteinte grave et manifestement illégale n'était portée au droit au respect de la vie, au droit de ne pas être soumis à des traitements inhumains ou dégradants ainsi qu'au droit de recevoir les traitements et les soins appropriés à son état de santé. Au soutien de son appel, le requérant, qui fait valoir que son état de santé le place dans une situation de particulière vulnérabilité au virus du covid-19, invoque la carence de l'administration pénitentiaire en ce qui concerne la fourniture, en quantité suffisante, de produits d'hygiène et d'entretien, de savon et de gel hydro-alcoolique, de masques de protection, de gants ainsi que de tests de dépistage, le nettoyage des locaux et du linge, les modalités de distribution des repas et le recours aux fouilles. <br/>
<br/>
              9. Il résulte de l'instruction que, depuis que l'épidémie de covid-19 a atteint la France et au fur et à mesure de l'évolution des stades 1, 2 et 3 de l'épidémie, la ministre de la justice a édicté, au moyen de plusieurs instructions adressées aux services compétents, un certain nombre de mesures visant à prévenir le risque de propagation du virus au sein des établissements pénitentiaires. Ces instructions définissent des orientations générales et arrêtent des mesures d'organisation du service public pénitentiaire qu'il revient aux chefs des 187 établissements pénitentiaires de mettre en oeuvre et d'appliquer sous l'autorité des directions interrégionales des services pénitentiaires. Il appartient aux chefs d'établissements pénitentiaires responsables de l'ordre et de la sécurité au sein de ceux-ci, de s'assurer du respect des consignes données pour lutter contre la propagation du virus et de prendre, dans le champ de leurs compétences, toute mesure propre à garantir le respect effectif des libertés fondamentales des personnes détenues et des personnes y travaillant ou y intervenant.<br/>
<br/>
              10. Il résulte de l'instruction qu'à la date du 11 mai 2020, on ne recense, parmi les personnels pénitentiaires et les personnes détenues au centre pénitentiaire de Vendin-le-Vieil aucun cas confirmé de contamination au virus du covid-19. En l'état de l'instruction, ne se trouvent placés en confinement sanitaire que les nouveaux arrivants au titre de la quatorzaine dont ils font l'objet à titre préventif. Par ailleurs, cet établissement accueille 112 personnes placées dans 204 cellules, soit un taux d'occupation légèrement supérieur à 50 %.<br/>
              En ce qui concerne le nettoyage des locaux, l'accès aux produits d'hygiène et d'entretien et le lavage du linge :<br/>
<br/>
              11. Il résulte de l'instruction que le chef d'établissement du centre pénitentiaire de Vendin-le-Vieil, applique effectivement, au sein de son établissement, la consigne générale d'effectuer un nettoyage renforcé et une aération régulière des locaux. Il apparaît ainsi que le nombre des auxiliaires chargés de l'entretien des locaux a été augmenté afin de permettre un nettoyage plus fréquent des locaux, en particulier des points de contacts. Il résulte également de l'instruction qu'une quantité de produits d'hygiène et d'entretien est distribuée, sous forme de kit, gratuitement aux personnes détenues au centre pénitentiaire de Vendin-le-Vieil, afin de les mettre à même d'appliquer correctement les règles d'hygiène et d'entretenir leur cellule. Il résulte enfin de l'instruction qu'outre la machine à laver qui est disponible dans chaque coursive, l'ensemble des personnes détenues ont accès à la buanderie de l'établissement, deux fois par mois, afin d'y faire laver leurs draps et couettes. <br/>
<br/>
              12. Dans ces conditions, il n'apparaît pas, en l'état de l'instruction et à la date de la présente ordonnance, que les modalités retenues pour assurer, au sein du centre pénitentiaire de Vendin-le-Vieil, le nettoyage des locaux, le lavage du linge des personnes détenues et leur garantir un accès aux produits d'hygiène et d'entretien révèleraient une carence portant, de manière caractérisée, une atteinte grave et manifestement illégale aux libertés fondamentales invoquées.<br/>
<br/>
              En ce qui concerne la distribution de gants à usage unique et de masques de protection sanitaires :<br/>
<br/>
              13. En premier lieu, il est vrai que l'état de santé de M. A..., qui le rend particulièrement vulnérable au covid-19 appelle, à titre préventif, une vigilance renforcée. Il résulte toutefois de l'instruction que l'intéressé est placé en cellule individuelle. Il résulte également de l'instruction qu'il fait l'objet d'un suivi régulier par l'unité de consultations et de soins ambulatoires. A ces rendez-vous médicaux s'ajoutent les contacts quotidiens avec un membre de cette unité sanitaire qu'occasionne la distribution de médicaments. Ces différents rendez-vous permettent au personnel soignant d'assurer un suivi régulier de l'état de santé du requérant et, le cas échéant, de détecter, à bref délai, l'apparition de symptômes suspects.<br/>
<br/>
              14. En deuxième lieu, il a été décidé, dès le 27 février 2020, de limiter les mouvements à l'intérieur des établissements pénitentiaires et de réduire les flux de circulation entre l'intérieur et l'extérieur. A compter du 17 mars 2020, les activités socio-culturelles et d'enseignement, le sport en espace confiné, la pratique des cultes, la formation professionnelle, le travail ainsi que les visites aux parloirs, parloirs familiaux et unités de vie familiale et les entretiens avec les visiteurs de prison ont été suspendus. Il résulte de l'instruction que cette consigne générale, qui est appelée à connaître des évolutions à compter du 11 mai 2020, est effectivement appliquée au sein du centre pénitentiaire de Vendin-le-Vieil. En ce qui concerne l'activité de promenade et l'accès aux douches collectives qui ont été maintenus, les déplacements sont effectués par des groupes de personnes restant les mêmes d'un jour sur l'autre afin que les contacts ne se fassent qu'entre les membres d'un groupe préconstitué de personnes asymptomatiques. <br/>
              15. En troisième lieu, il résulte de l'instruction qu'à compter du 11 mai 2020, tous les personnels pénitentiaires intervenant au sein du centre pénitentiaire de Vendin-le-Vieil sont dotés d'un masque de protection sanitaire et non plus seulement ceux dont les fonctions impliquent un contact direct et prolongé avec les personnes détenues, ainsi que l'avait prévu la note du 31 mars 2020 qui a été effectivement appliquée dans cet établissement. En outre, à compter du 11 mai 2020, ainsi que cela ressort du mémoire en défense de la ministre de la justice, l'ensemble des auxiliaires du centre pénitentiaire de Vendin-le-Vieil amenés par leurs missions à se trouver en contact répété ou en contact direct et prolongé avec d'autres personnes détenues se verra remettre des masques de protection ainsi que des paires de gants à usage unique. Sont notamment concernés les auxiliaires d'étage chargés de la distribution des repas.<br/>
<br/>
              16. En quatrième lieu, il résulte de l'instruction, en particulier des éléments produits par la ministre de la justice, le 13 mai 2020, qu'afin de renforcer l' " anneau sanitaire " mis en place au sein de l'établissement, l'ensemble des personnes détenues au centre pénitentiaire de Vendin-le-Vieil est doté, à compter du 12 mai 2020, d'un masque de protection " grand public " à usage unique à l'occasion de chaque contact direct et prolongé avec un tiers extérieur qui devra, pour sa part, être muni d'un masque de protection personnel, notamment lors d'un " parloir avocat ", d'une commission de discipline, d'un entretien avec un intervenant extérieur, d'une rencontre avec un proche dans le cadre d'un parloir.<br/>
<br/>
              17. En cinquième lieu, un protocole relatif au signalement et à la détection des cas symptomatiques a été défini, par une note du 6 avril 2020 et par l'actualisation, à la même date, de la fiche intitulée " Etablissements pénitentiaires : organisation de la réponse sanitaire par les unités sanitaires en milieu pénitentiaire en collaboration avec les services pénitentiaires " afin que puissent être détectées, dans les meilleurs délais, les personnes détenues présentant les symptômes du covid-19. Il résulte de l'instruction que ce protocole, qui repose sur une responsabilité partagée entre les personnes détenues, le personnel pénitentiaire et les équipes des unités sanitaires en milieu pénitentiaire, est effectivement mis en oeuvre au sein du centre pénitentiaire de Vendin-le-Vieil.<br/>
<br/>
              18. Compte tenu des mesures effectivement prises par le chef d'établissement du centre pénitentiaire de Vendin-le-Vieil pour limiter, conformément aux instructions de la ministre de la justice, les contacts avec l'extérieur, organiser progressivement leur reprise et réduire les mouvements à l'intérieur de son établissement, des modalités d'entretien et de nettoyage des locaux de cet établissement, des mesures effectivement prises et de celles annoncées pour y assurer le respect des règles de sécurité sanitaire, en particulier celles décrites aux points 15 et 16 de la présente ordonnance, de l'application du protocole relatif au signalement et à la détection des cas symptomatiques ainsi que du suivi médical régulier dont fait l'objet M. A..., il n'apparaît pas, en l'état de l'instruction et à la date de la présente ordonnance, que l'absence de distribution au requérant de masques de protection, à titre permanent, et de gants à usage unique révèleraient une carence portant, de manière caractérisée, une atteinte grave et manifestement illégale aux libertés fondamentales qu'il invoque. <br/>
              En ce qui concerne le recours aux fouilles :<br/>
<br/>
              19. Il résulte de l'instruction que la consigne générale, qui a été formalisée par une note du 9 avril 2020, a été donnée à l'ensemble des établissements pénitentiaires, que soient privilégiées, pendant la durée de l'état d'urgence sanitaire, les mesures alternatives aux fouilles telle la détection par portique et que les fouilles de personnes détenues ne soient pratiquées que de manière exceptionnelle, par des personnels dotés de masque de protection et de gants à usage unique. Il résulte de l'instruction que le chef d'établissement du centre pénitentiaire de Vendin-le-Vieil applique effectivement ces mesures au sein de son établissement. Il a veillé, conformément aux consignes générales, à ce que les fouilles par palpation y soient effectuées de dos, par un agent de surveillance muni d'un masque de protection et d'une paire de gants à usage unique. Dans ces conditions, il n'apparaît pas, en l'état de l'instruction et à la date de la présente ordonnance, que devraient être définies, au motif d'une atteinte grave et manifestement illégale aux libertés fondamentales invoquées, des règles différentes que celles actuellement en vigueur au sein du centre pénitentiaire de Vendin-le-Vieil des modalités des fouilles dont le requérant est susceptible de faire l'objet.<br/>
<br/>
              En ce qui concerne la pratique de tests de dépistage :<br/>
<br/>
<br/>
              20. Il résulte de l'instruction que la doctrine d'usage des tests de dépistage a été définie par une note du 9 avril 2020 relative au déploiement des nouvelles capacités de tests de dépistage virologiques. Cette note identifie des populations prioritaires parmi lesquelles figurent les personnes détenues et les personnels pénitentiaires. Il y est préconisé que dans les établissements pénitentiaires, tout personnel symptomatique fasse l'objet d'un test de dépistage. S'il s'avère que ce cas est confirmé, l'ensemble des personnels doivent être dépistés. S'agissant des personnes détenues, il est préconisé, dans les établissements sans cas connu de contamination, ce qui correspond à la situation du centre pénitentiaire de Vendin-le-Vieil de dépister le premier cas symptomatique. Il résulte de l'instruction que l'administration pénitentiaire est en mesure, le cas échéant, de mettre effectivement en oeuvre, à l'échelle de cet établissement, les consignes arrêtées par la note du 9 avril 2020.  Dans ces conditions, il n'apparaît pas, en l'état de l'instruction et à la date de la présente ordonnance, eu égard aux critères, constamment ajustés, retenus en l'état des disponibilités des tests, pour effectuer les dépistages, et qui sont appelés à évoluer après le 11 mai 2020 avec la déclinaison en détention de la stratégie d'utilisation des tests virologiques arrêtée par l'instruction interministérielle du 6 mai 2020, que l'absence de dépistage systématique de l'ensemble des personnes détenues au centre pénitentiaire de Vendin-le-Vieil révèlerait une carence portant, de manière caractérisée, une atteinte grave et manifestement illégale aux libertés fondamentales invoquées.<br/>
              21. Il résulte de ce qui précède, sans qu'il soit besoin de se prononcer sur la condition d'urgence ni de prononcer les mesures d'expertise sollicitées, que M. A..., qu'il n'y a pas lieu, dans les circonstances de l'espèce, d'admettre au bénéfice de l'aide juridictionnelle à titre provisoire, n'est pas fondé à soutenir que c'est à tort que, par l'ordonnance du 29 avril 2020, le juge des référés du tribunal administratif de Lille a rejeté sa demande. Sa requête doit donc être rejetée, y compris les conclusions présentées au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991, sans qu'il soit besoin de se prononcer sur la fin de non recevoir opposée par la garde des sceaux, ministre de la justice.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : La demande de M. A... tendant au bénéfice de l'aide juridictionnelle à titre provisoire est rejetée.<br/>
Article 2 : La requête de M. A... est rejetée.<br/>
Article 3: La présente ordonnance sera notifiée à M. B... A... et à la garde des sceaux, ministre de la justice.<br/>
Copie en sera adressée au Premier ministre et au ministre des solidarités et de la santé.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
