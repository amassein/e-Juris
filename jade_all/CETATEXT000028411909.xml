<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028411909</ID>
<ANCIEN_ID>JG_L_2013_12_000000368208</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/41/19/CETATEXT000028411909.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème / 9ème SSR, 19/12/2013, 368208, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-12-19</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>368208</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème / 9ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Romain Godet</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Edouard Crépey</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:368208.20131219</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 2 mai 2013 au secrétariat du contentieux du Conseil d'Etat, présentée par M. G...AQ..., demeurant..., et MmeI...C..., demeurant... ; M. AQ...et MmeC...demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret n° 2013-182 du 27 février 2013 portant application des articles L. 134-1 à L. 134-9 du code de la propriété intellectuelle et relatif à l'exploitation numérique des livres indisponibles du XXe siècle ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu la Constitution, notamment son article 62 ;<br/>
<br/>
              Vu le traité de l'Organisation mondiale de la propriété intellectuelle sur le droit d'auteur ;<br/>
<br/>
              Vu l'accord de l'Organisation mondiale du commerce sur les aspects des droits de propriété intellectuelle qui touchent au commerce ;<br/>
<br/>
              Vu la convention de Berne pour la protection des oeuvres littéraires et artistiques ;<br/>
<br/>
              Vu la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales et son premier protocole additionnel ;<br/>
<br/>
              Vu le traité sur le fonctionnement de l'Union européenne ;<br/>
<br/>
              Vu la Charte des droits fondamentaux de l'Union européenne ;<br/>
<br/>
              Vu la directive 2001/29/CE du Parlement européen et du Conseil du 22 mai 2001 sur l'harmonisation de certains aspects du droit d'auteur et des droits voisins dans la société de l'information ;<br/>
<br/>
              Vu la directive 2004/48/CE du Parlement européen et du Conseil du 29 avril 2004 relative aux mesures et procédures visant à assurer le respect des droits de propriété intellectuelle ;<br/>
<br/>
              Vu la directive 2006/123/CE du Parlement européen et du Conseil du 12 décembre 2006 relative aux services dans le marché intérieur ;<br/>
<br/>
              Vu le code de la propriété intellectuelle, dans sa version résultant de la loi n° 2012-287 du 1er mars 2012 ;<br/>
<br/>
              Vu l'arrêt C-351/12 du 27 février 2014 de la Cour de justice de l'Union européenne ; <br/>
<br/>
              Vu la décision du 19 décembre 2013 par laquelle le Conseil d'Etat statuant au contentieux a renvoyé au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée par M. AQ...et MmeC... ;<br/>
<br/>
              Vu la décision du Conseil constitutionnel n° 2013-370 QPC du 28 février 2014 statuant sur la question prioritaire de constitutionnalité soulevée ;<br/>
	Vu les autres pièces du dossier ;<br/>
	Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Romain Godet, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Edouard Crépey, rapporteur public ;<br/>
<br/>
<br/>
              Vu la note en délibéré, enregistrée le 15 avril 2015, présentée par la SOFIA ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 20 avril 2015, présentée par la ministre de la culture et de la communication ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 23 avril 2015, présentée par M. G...AQ...et MmeI...C... ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant que, en créant les articles L. 134-1 à L. 134-9 du code de la propriété intellectuelle, la loi du 1er mars 2012 relative à l'exploitation numérique des livres indisponibles du XXème siècle a institué un dispositif destiné à favoriser, afin d'assurer la valorisation d'un patrimoine écrit devenu inaccessible faute de diffusion commerciale auprès du public, l'exploitation numérique d'oeuvres reproduites dans des livres publiés en France avant le 1er janvier 2001 ne faisant plus l'objet d'une diffusion commerciale par un éditeur et ne faisant pas l'objet d'une publication sous une forme imprimée ou numérique ; que le droit d'autoriser la reproduction ou la représentation de ces livres sous une forme numérique est exercé, à l'issue d'un délai de six mois à compter de leur inscription dans une base de données accessible au public placée sous la responsabilité de la Bibliothèque nationale de France, par des sociétés de perception et de répartition des droits agréées à cet effet par le ministre chargé de la culture ; que l'auteur d'un livre indisponible ou l'éditeur disposant sur celui-ci du droit de reproduction sous une forme imprimée peut s'opposer à l'exercice de ce droit au plus tard six mois après l'inscription du livre dans la base de données ; que, même après l'expiration de ce délai, l'auteur d'un livre indisponible peut s'opposer à tout moment à l'exercice du droit de reproduction ou de représentation s'il juge que la reproduction ou la représentation du livre est susceptible de nuire à son honneur ou à sa réputation ; que l'auteur d'un livre indisponible peut par ailleurs décider à tout moment de retirer à la société de perception et de répartition des droits agréée le droit d'autoriser la reproduction et la représentation du livre sous une forme numérique, dans les conditions prévues par l'article L. 134-6 du code de la propriété intellectuelle ; <br/>
<br/>
              2. Considérant que les requérants demandent l'annulation pour excès de pouvoir du décret du 27 février 2013 portant application des articles L. 134-1 à L. 134-9 du code de la propriété intellectuelle ; que, saisi d'une question prioritaire de constitutionnalité, le Conseil constitutionnel a jugé que les dispositions des articles L. 134-1 à L. 134-8 de ce code n'étaient pas contraires aux droits et libertés que la Constitution garantit ;<br/>
<br/>
              Sur les interventions présentées au soutien de la requête :<br/>
<br/>
              3. Considérant que MmeBF..., MM. AN...etJ..., MmeBN..., MM.AK..., Z..., K...etAP..., MmesAT...etW..., MM. Q...etAX..., MmesAR..., A...etAJ..., MM. BC...etAH..., MmeBL..., MM. S... etL..., MmeAF..., MM.BA..., N..., F...etP..., MmesAV..., Y...etAU..., MM.BB..., U..., BE...etBD..., M. et MmeT...etB... R... ont intérêt à l'annulation du décret attaqué ; qu'il en est de même du syndicat des écrivains de langue française et de l'association " Autour des auteurs " ; qu'ainsi leurs interventions sont recevables ;<br/>
<br/>
              Sur l'intervention présentée par la Société française des intérêts des auteurs de l'écrit (SOFIA) :<br/>
<br/>
              4. Considérant que la Société française des intérêts des auteurs de l'écrit (SOFIA) a intérêt au maintien du décret attaqué ; qu'ainsi son intervention est recevable ;<br/>
<br/>
              Sur la légalité du décret attaqué :<br/>
<br/>
              5. Considérant, en premier lieu, qu'aux termes de l'article 2 de la convention de Berne pour la protection des oeuvres littéraires et artistiques, qui définit les oeuvres protégées par le droit d'auteur : " 1) Les termes "oeuvres littéraires et artistiques" comprennent toutes les productions du domaine littéraire, scientifique et artistique, quel qu'en soit le mode ou la forme d'expression, telles que: les livres, brochures et autres écrits; (...) " ; qu'en vertu de l'article 5 de cette convention : " 1) Les auteurs jouissent, en ce qui concerne les oeuvres pour lesquelles ils sont protégés en vertu de la présente Convention, dans les pays de l'Union autres que le pays d'origine de l'oeuvre, des droits que les lois respectives accordent actuellement ou accorderont par la suite aux nationaux, ainsi que des droits spécialement accordés par la présente Convention. / 2) La jouissance et l'exercice de ces droits ne sont subordonnés à aucune formalité; cette jouissance et cet exercice sont indépendants de l'existence de la protection dans le pays d'origine de l'oeuvre. Par suite, en dehors des stipulations de la présente Convention, l'étendue de la protection ainsi que les moyens de recours garantis à l'auteur pour sauvegarder ses droits se règlent exclusivement d'après la législation du pays où la protection est réclamée. " ; que son article 6 bis stipule : " 1) Indépendamment des droits patrimoniaux d'auteur, et même après la cession desdits droits, l'auteur conserve le droit de revendiquer la paternité de l'oeuvre et de s'opposer à toute déformation, mutilation ou autre modification de cette oeuvre ou à toute autre atteinte à la même oeuvre, préjudiciables à son honneur ou à sa réputation. / 2) Les droits reconnus à l'auteur en vertu de alinéa 1) ci-dessus sont, après sa mort, maintenus au moins jusqu'à l'extinction des droits patrimoniaux et exercés par les personnes ou institutions auxquelles la législation nationale du pays où la protection est réclamée donne qualité. Toutefois, les pays dont la législation, en vigueur au moment de la ratification du présent Acte ou de l'adhésion à celui-ci, ne contient pas de dispositions assurant la protection après la mort de l'auteur de tous les droits reconnus en vertu de alinéa 1) ci-dessus ont la faculté de prévoir que certains de ces droits ne sont pas maintenus après la mort de l'auteur. " ; <br/>
<br/>
              6. Considérant que les requérants ne sauraient en tout état de cause soutenir que la loi du 1er mars 2012 et le décret attaqué ne concerneraient que le " support " des oeuvres qu'ils visent et méconnaîtraient, pour ce motif, le 1 de l'article 2 de la convention de Berne ;<br/>
<br/>
              7. Considérant que les formalités d'opposition ou de retrait auprès de la Bibliothèque nationale de France ou d'une société de perception et de répartition des droits agréée prévues par l'article L. 134-3 du code de la propriété intellectuelle n'ont ni pour objet ni pour effet de remettre en cause l'application des dispositions de l'article L. 111-1 du code de la propriété intellectuelle qui, conformément aux stipulations précitées, prévoient que " l'auteur d'une oeuvre de l'esprit jouit sur cette oeuvre, du seul fait de sa création, d'un droit de propriété incorporelle exclusif et opposable à tous " ; qu'ainsi, le moyen tiré de ce que le décret attaqué méconnaîtrait, en tant qu'il est pris pour l'application des dispositions de l'article L. 134-3 du code de la propriété intellectuelle, les stipulations du 2 de l'article 5 de la convention doit être, en tout état de cause, écarté ; <br/>
<br/>
              8. Considérant que les requérants soutiennent en outre que le droit d'opposition prévu au troisième alinéa du I de l'article L. 134-4 du code de la propriété intellectuelle ne peut être exercé par les ayants droit de l'auteur, en méconnaissance des stipulations de l'article 6 bis de la convention de Berne ; qu'à supposer que ce droit puisse se rattacher à l'exercice d'un droit moral, les dispositions combinées du troisième alinéa du I de l'article L. 134-4 et des articles R. 134-5 et R. 134-7 du code de la propriété intellectuelle ne font en tout état de cause pas obstacle à ce que les ayants droit de l'auteur exercent le droit d'opposition au décès de celui-ci ;<br/>
<br/>
              9. Considérant, en deuxième lieu, qu'aux termes du 1 de l'article 15 de la convention de Berne : " 1) Pour que les auteurs des oeuvres littéraires et artistiques protégés par la présente Convention soient, sauf preuve contraire, considérés comme tels et admis en conséquence devant les tribunaux des pays de l'Union à exercer des poursuites contre les contrefacteurs, il suffit que le nom soit indiqué sur l'oeuvre en la manière usitée. Le présent alinéa est applicable, même si ce nom est un pseudonyme, dès lors que le pseudonyme adopté par l'auteur ne laisse aucun doute sur son identité " ; que l'article 5 de la directive 2004/48/CE du Parlement européen et du Conseil du 29 avril 2004 relative aux mesures et procédures visant à assurer le respect des droits de propriété intellectuelle dispose : " Aux fins de l'application des mesures, procédures et réparations prévues dans la présente directive, / a) pour que l'auteur d'une oeuvre littéraire ou artistique soit, jusqu'à preuve du contraire, considéré comme tel et admis en conséquence à exercer des poursuites contre les contrefacteurs, il suffit que son nom soit indiqué sur l'oeuvre de la manière usuelle; / b) le point a) s'applique mutatis mutandis aux titulaires de droits voisins du droit d'auteur en ce qui concerne leur objet protégé. " ;<br/>
<br/>
              10. Considérant que, selon les requérants, ces stipulations et dispositions seraient méconnues par l'article L. 134-6 du code de la propriété intellectuelle, dans sa rédaction issue de la loi du 1er mars 2012, et par les articles R. 134-5 et R. 134-9 du même code, créés par le décret attaqué, qui en font application, dès lors qu'ils imposent à l'auteur qui entend s'opposer à l'exercice du droit de reproduction et de représentation sous forme numérique de son livre indisponible par une société de perception et de répartition des droits agréée d'apporter la preuve qu'il est " le seul titulaire " de ces droits en produisant soit " la copie d'une pièce d'identité et une déclaration sur l'honneur attestant sa qualité ", soit " tout élément probant " de nature à établir cette qualité ; que ces exigences, qui ont pour seul objet de s'assurer que le droit d'opposition ou de retrait prévu par la loi est exercé par les seules personnes ayant qualité pour ce faire, afin de protéger les droits reconnus aux auteurs eux-mêmes, à leurs ayants-droit ou aux cessionnaires de leurs droits de reproduction et de représentation sous forme numérique, n'ont pas pour effet de remettre en cause la " présomption de titularité " du droit d'auteur dont peuvent se prévaloir, en application de l'article L. 113-1 du même code, les personnes sous le nom de qui les oeuvres sont divulguées ; qu'en conséquence, le moyen doit, en tout état de cause, être écarté ;<br/>
<br/>
              11. Considérant, en troisième lieu, qu'aux termes de l'article 9 de la convention de Berne : " 1) Les auteurs d'oeuvres littéraires et artistiques protégés par la présente Convention jouissent du droit exclusif d'autoriser la reproduction de ces oeuvres, de quelque manière et sous quelque forme que ce soit. / 2) Est réservée aux législations des pays de l'Union la faculté de permettre la reproduction desdites oeuvres dans certains cas spéciaux, pourvu qu'une telle reproduction ne porte pas atteinte à l'exploitation normale de l'oeuvre ni ne cause un préjudice injustifié aux intérêts légitimes de l'auteur. (...) " ; que ces conditions, auxquelles la convention subordonne toute exception au droit de reproduction reconnu à l'auteur d'une oeuvre, sont reprises tant à l'article 13 de l'accord de l'Organisation mondiale du commerce sur les aspects des droits de propriété intellectuelle qui touchent au commerce et à l'article 10 du traité de l'Organisation mondiale de la propriété intellectuelle, qu'au 5 de l'article 5 de la directive 2001/29/CE du Parlement européen et du Conseil du 22 mai 2001 sur l'harmonisation de certains aspects du droit d'auteur et des droits voisins dans la société de l'information ;<br/>
<br/>
              12. Considérant que, ainsi qu'il a été dit, si la loi du 1er mars 2012 et le décret attaqué prévoient l'exercice du droit d'autoriser la reproduction et la représentation sous forme numérique de livres indisponibles  par une société de perception et de répartition des droits agréée, ils permettent à tout auteur de s'opposer ou de mettre fin à tout moment à cet exercice ; que, dès lors, les requérants ne sauraient en tout état de cause soutenir qu'ils méconnaîtraient les conditions, mentionnées ci-dessus, auxquelles la reproduction des oeuvres peut intervenir sans l'autorisation de leurs auteurs ;<br/>
<br/>
              13. Considérant, en quatrième lieu, que les requérants soutiennent qu'en introduisant au sein du code de la propriété intellectuelle un article L. 134-3 subordonnant à la détention d'un agrément délivré par le ministre chargé de la culture la faculté pour une société de perception et de répartition des droits d'exercer le droit de reproduction et de représentation sous forme numérique des livres indisponibles  et en précisant, aux articles R. 327-1 à R. 327-6 du même code, les conditions de délivrance de cet agrément, la loi du 1er mars 2012 et le décret attaqué institueraient une entrave injustifiée à la libre prestation de services, liberté fondamentale reconnue par les articles 26 et 56 du traité sur le fonctionnement de l'Union européenne, et méconnaitraient les dispositions de l'article 16 de la directive 2006/123/CE du 12 décembre 2006 relative aux services dans le marché intérieur ;<br/>
<br/>
              14. Considérant, toutefois, qu'il résulte de l'interprétation que donne de ces stipulations et dispositions la Cour de justice de l'Union européenne, notamment dans son arrêt du 27 février 2014, OSA - Ochranný svaz autorský pro práva k dílum hudebním o.s., C-351/12, que l'article 16 de la directive 2006/123 n'est pas applicable aux services rendus par des sociétés de gestion collective de droits d'auteur et que, par ailleurs, la protection des droits de propriété intellectuelle constitue une raison impérieuse d'intérêt général justifiant que soit accordé un monopole à une société de gestion pour la gestion des droits d'auteur relatifs à une catégorie d'oeuvres ; qu'en exigeant des sociétés de perception et de répartition des droits la détention d'un agrément aux fins d'exercer le droit d'autoriser la reproduction et la représentation sous forme numérique des livres indisponibles, la loi du 1er mars 2012 et le décret attaqué ont entendu, eu égard à la mission confiée à ces sociétés en application de l'article L. 134-3 du code de la propriété intellectuelle, garantir que seules les sociétés présentant certaines garanties puissent exercer ce droit ; que les critères de délivrance de l'agrément, prévus aux articles R. 327-1 à R. 327-6 du même code en application du III de l'article L. 134-3, qui se rapportent notamment à la représentativité des sociétés, à la représentation paritaire des auteurs et des éditeurs au sein de leurs organes, à la qualification professionnelle de leurs dirigeants et aux moyens qu'elles sont susceptibles de mettre en oeuvre, prévoient un encadrement suffisant de l'exercice du pouvoir d'appréciation du ministre chargé de la culture ; que, dans ces conditions, les dispositions litigieuses ne peuvent être regardées comme méconnaissant les stipulations précitées du traité sur le fonctionnement de l'Union européenne ;<br/>
<br/>
              15. Considérant, en cinquième lieu, qu'aux termes du 1 de l'article 6 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales : " Toute personne a droit à ce que sa cause soit entendue équitablement, publiquement et dans un délai raisonnable, par un tribunal indépendant et impartial, établi par la loi, qui décidera, soit des contestations sur ses droits et obligations de caractère civil, soit du bien-fondé de toute accusation en matière pénale dirigée contre elle. (...) " ; <br/>
<br/>
              16. Considérant qu'aux termes des articles R. 134-6 à R. 134-10 du code de la propriété intellectuelle, dans leur rédaction issue du décret attaqué, les sociétés de perception et de répartition des droits agréées sont chargées de recevoir les demandes d'opposition, le cas échéant transmises par la Bibliothèque nationale de France, ainsi que les demandes de retrait formulées par les auteurs ou les éditeurs de livres indisponibles et, au vu des éléments fournis par les demandeurs afin d'attester leur identité et leur qualité et des observations éventuellement formulées par l'éditeur, de donner suite à ces demandes si les conditions prévues sont remplies ; que, dans ces conditions, ces sociétés ne peuvent être regardées comme se prononçant sur des contestations sur des droits et obligations de caractère civil, au sens de convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ; que les requérants ne sauraient dès lors soutenir que le décret attaqué serait illégal, faute de prévoir que ces sociétés devraient respecter les règles d'indépendance et d'impartialité qui s'imposent à un tribunal ; <br/>
<br/>
              17. Considérant, en sixième lieu, qu'aux termes de l'article 1er du premier protocole additionnel à la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales : " Toute personne physique ou morale a droit au respect de ses biens. Nul ne peut être privé de sa propriété que pour cause d'utilité publique et dans les conditions prévues par la loi et les principes généraux du droit international. / Les dispositions précédentes ne portent pas atteinte au droit que possèdent les Etats de mettre en vigueur les lois qu'ils jugent nécessaires pour réglementer l'usage des biens conformément à l'intérêt général ou pour assurer le paiement des impôts ou d'autres contributions ou des amendes. " ; que les dispositions litigieuses, analysées au point 1 ci-dessus, n'emportent pas de privation de propriété au sens de ces stipulations ; que si elles réglementent l'usage du droit de reproduction et de représentation, elles répondent à des considérations d'intérêt général et n'apparaissent pas disproportionnées au regard de l'objectif poursuivi ; que l'atteinte portée aux conditions d'exercice du droit de l'auteur ou de ses ayants-droit d'autoriser la reproduction et la représentation sous forme numérique de livres indisponibles ne présente pas un caractère irrévocable ; qu'ainsi, elles ne peuvent être regardées comme soumettant les auteurs de livres indisponibles ou leurs ayants-droit à une charge excessive ou disproportionnée ; qu'il suit de là que le moyen tiré de la méconnaissance des stipulations précitées doit être écarté ;<br/>
<br/>
              18. Considérant, en septième lieu, que les requérants soutiennent qu'en prévoyant que la liste des livres indisponibles enregistrés dans la base de données tenue par la Bibliothèque nationale de France est arrêtée par un comité scientifique placé auprès du président de cet organisme, l'article R. 134-1 du code de la propriété intellectuelle, dans sa rédaction issue du décret attaqué, méconnaîtrait les dispositions du deuxième alinéa de l'article L. 134-2 du même code aux termes duquel " toute personne peut demander à la Bibliothèque nationale de France l'inscription d'un livre indisponible dans la base de données " ; que, toutefois, la circonstance que les dispositions litigieuses de l'article R. 134-1 du code de la propriété intellectuelle, prises pour l'application du premier alinéa de l'article L. 134-2 du même code, en vertu duquel la Bibliothèque nationale de France veille à la " mise en oeuvre " et à " l'actualisation " de cette base de données, confient le soin d'arrêter la liste des livres indisponibles enregistrés à un comité ad hoc est sans incidence sur la possibilité pour toute personne, quelle que soit sa qualité, de solliciter l'inscription d'un livre indisponible ; qu'il suit de là que le moyen ne peut qu'être écarté ;<br/>
<br/>
              19. Considérant, toutefois, en dernier lieu, qu'aux termes de l'article 2 de la directive 2001/29/CE du Parlement européen et du conseil du 22 mai 2001 sur l'harmonisation de certains aspects du droit d'auteur et des droits voisins dans la société de l'information : " Les États membres prévoient le droit exclusif d'autoriser ou d'interdire la reproduction directe ou indirecte, provisoire ou permanente, par quelque moyen et sous quelque forme que ce soit, en tout ou en partie : / a) pour les auteurs, de leurs oeuvres ; (...) " ; que son article 5 prévoit des " exceptions ou limitations " à ce droit que les Etats membres doivent ou, selon le cas, peuvent introduire dans leur législation ; que, selon les requérants, les articles L. 134-4 et L. 134-5 du code de la propriété intellectuelle, d'une part, et les articles R. 134-5 à R. 134-10 du même code, d'autre part, prévoient des exceptions ou limitations qui ne sont pas au nombre de celles qui sont limitativement énumérées par l'article 5 de la directive ;<br/>
<br/>
              20. Considérant que la réponse à ce moyen dépend de la question de savoir si les dispositions mentionnées ci-dessus de la directive 2001/29/CE du 22 mai 2001s'opposent à ce qu'une réglementation, telle que celle qui a été analysée au point 1 de la présente décision, confie à des sociétés de perception et de répartition des droits agréées l'exercice du droit d'autoriser la reproduction et la représentation sous une forme numérique de " livres indisponibles ", tout en permettant aux auteurs ou ayants-droit de ces livres de s'opposer ou de mettre fin à cet exercice, dans les conditions qu'elle définit ;<br/>
<br/>
              21. Considérant que cette question est déterminante pour la solution du litige que doit trancher le Conseil d'Etat ; qu'elle présente une difficulté sérieuse ; qu'il y a lieu, par suite, d'en saisir la Cour de justice de l'Union européenne en application de l'article 267 du traité sur le fonctionnement de l'Union européenne et, jusqu'à ce que celle-ci se soit prononcée, de surseoir à statuer sur la requête de M. AQ...et de MmeC... ;<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Les interventions de MmeBF..., MM.AN..., J..., MmeBN..., MM.AK..., Z..., K..., AP..., MmesAT..., W..., MM.Q..., AX...,Mmes AR..., A..., AJ..., MM. BC...-BS..., AH..., MmeBL..., MM. S..., L..., MmeAF..., MM.BA..., BH..., F..., P...,Mmes AG..., Y..., AU..., MM.BB..., U..., BE..., BD..., M. etB... T..., MmeR..., du Syndicat des écrivains de langue française, de l'association " Autour des auteurs " et de la Société française des intérêts des auteurs de l'écrit sont admises. <br/>
Article 2 : Il est sursis à statuer sur la requête présentée par M. AQ...et MmeC...jusqu'à ce que la Cour de justice de l'Union européenne se soit prononcée sur la question suivante : les dispositions mentionnées ci-dessus de la directive 2001/29/CE du 22 mai 2001s'opposent-elles à ce qu'une réglementation, telle que celle qui a été analysée au point 1 de la présente décision, confie à des sociétés de perception et de répartition des droits agréées l'exercice du droit d'autoriser la reproduction et la représentation sous une forme numérique de " livres indisponibles ", tout en permettant aux auteurs ou ayants-droit de ces livres de s'opposer ou de mettre fin à cet exercice, dans les conditions qu'elle définit ' <br/>
Article 3 : La présente décision sera notifiée à M. G... AQ...et MmeI...C..., au ministre de la culture et de la communication et au Premier ministre, ainsi qu'à MmeE...BF..., MM. O...AN..., AZ...J..., MmeBO..., MM. AD...AK..., AD...Z..., AI...K..., D...AP..., MmesBQ...AT..., AB...W..., MM. AE...Q..., AL...AX..., MmesBP...AR..., AW...A..., AO...AJ..., MM. AL...BC..., BI...-BT...AH..., MmeBM..., MM. BG...S..., M...L..., MmeBK...AF..., MM. X...BA..., V...N..., D...-BI...F..., AL...P..., MmesBR...AG..., AW...Y..., AY...AU..., MM. AA... BB..., AS...U..., AC...BE..., D...-BI...BD..., AM...T..., MmeBJ...T..., à la Société française des intérêts des auteurs de l'écrit, au Syndicat des écrivains de langue française, à l'association " Autour des auteurs " et au président de la Cour de justice de l'Union européenne.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
