<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039230818</ID>
<ANCIEN_ID>JG_L_2019_10_000000432147</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/23/08/CETATEXT000039230818.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème - 7ème chambres réunies, 16/10/2019, 432147</TITRE>
<DATE_DEC>2019-10-16</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>432147</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème - 7ème chambres réunies</FORMATION>
<TYPE_REC>Autres</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Sophie-Caroline de Margerie</RAPPORTEUR>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CECHR:2019:432147.20191016</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par un jugement n° 1902225 du 20 juin 2019, enregistré le 2 juillet 2019 au secrétariat du contentieux du Conseil d'Etat, le tribunal administratif de Toulouse, avant de statuer sur la demande de M. B... A... tendant à l'annulation de l'arrêté du 11 avril 2019 par lequel le préfet de la Haute-Garonne l'a obligé à quitter le territoire français dans le délai de trente jours et à la suspension de l'exécution de cette décision, a, en application des dispositions de l'article L. 113-1 du code de justice administrative, décidé de transmettre le dossier de cette requête au Conseil d'Etat, en soumettant à son examen les questions suivantes : <br/>
<br/>
              1°) les conclusions tendant à la suspension de l'exécution de la décision portant obligation de quitter le territoire français doivent-elles être regardées comme des conclusions de plein contentieux '<br/>
<br/>
              2°) Quel est le degré de contrôle que doit exercer le juge administratif sur le bien-fondé de la demande d'asile ' Le ressortissant étranger doit-il apporter des éléments suffisants permettant de créer un doute sérieux quant au caractère infondé de sa demande d'asile ou doit-il se borner à démontrer que cette demande n'est pas manifestement infondée '<br/>
<br/>
              3°) Sans préjudice de la question précédente, les éléments sérieux de nature à justifier, au titre de la demande d'asile, le maintien sur le territoire français du ressortissant étranger peuvent-ils être tirés des éventuels vices entachant la décision de l'Office français de protection des réfugiés et apatrides ' Dans l'hypothèse où ces moyens seraient opérants, convient-il de distinguer selon que le vice est susceptible d'avoir eu une influence sur le sens de la décision ou a privé l'intéressé d'une garantie essentielle au sens des décisions du Conseil d'Etat n°s 362798 et 362799 du 10 octobre 2013 '<br/>
<br/>
              ...........................................................................<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - la directive 2013/32/UE du 23 juin 2013 du Parlement européen et du Conseil du 26 juin 2013 ;<br/>
              - la loi n° 2018-778 du 10 septembre 2018 ;<br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              - le code de justice administrative.<br/>
<br/>
<br/>
<br/>
               Après avoir entendu en séance publique :<br/>
<br/>
- le rapport de Mme Sophie-Caroline de Margerie, conseiller d'Etat, <br/>
- les conclusions de M. Guillaume Odinet, rapporteur public ;<br/>
<br/>
<br/>
<br/>
              REND L'AVIS SUIVANT : <br/>
<br/>
              1.	L'article L. 743-1 du code de l'entrée et du séjour des étrangers et du droit d'asile, dans sa rédaction issue de la loi du 10 septembre 2018 pour une immigration maîtrisée, un droit d'asile effectif et une intégration réussie, pose le principe du droit au maintien sur le territoire du demandeur d'asile dans les termes suivants : " Le demandeur d'asile dont l'examen de la demande relève de la compétence de la France et qui a introduit sa demande auprès de l'Office français de protection des réfugiés et apatrides bénéficie du droit de se maintenir sur le territoire français jusqu'à la notification de la décision de l'office ou, si un recours a été formé, dans le délai prévu à l'article L. 731-2 contre une décision de rejet de l'office, soit jusqu'à la date de la lecture en audience publique de la décision de la Cour nationale du droit d'asile, soit, s'il est statué par ordonnance, jusqu'à la date de la notification de celle-ci. (...) ". Ce principe est assorti de dérogations énumérées à l'article L. 743-2 du même code, dans sa rédaction issue de la même loi. <br/>
<br/>
              2.	À ce titre, le droit au maintien sur le territoire prend fin notamment, selon le 4°bis de cet article, lorsque l'Office, saisi d'une demande de réexamen, a pris une décision d'irrecevabilité au motif qu'elle ne présente pas d'éléments nouveaux augmentant de manière significative la probabilité que le demandeur justifie des conditions requises pour prétendre à une protection. Le droit au maintien prend également fin, selon le 7° de l'article L. 743-2, " dans les cas prévus au I et au 5° du III de l'article L. 723-2 ", c'est-à-dire lorsque l'Office, statuant en procédure accélérée, a rejeté une demande présentée par un étranger ressortissant d'un " pays d'origine sûr " en application de l'article L. 722-1, une demande de réexamen infondée ou une demande émanant d'un demandeur dont la présence sur le territoire français a été regardée par l'autorité compétente de l'Etat comme constituant une menace grave pour l'ordre public, la sécurité publique ou la sûreté de l'Etat. <br/>
<br/>
              3.	Enfin, l'article L. 743-3 du même code, dans sa rédaction issue de la même loi, dispose que : " (...) Dans le cas où le droit de se maintenir sur le territoire a pris fin en application des 4° bis ou 7° de l'article L. 743-2, l'étranger peut demander au président du tribunal administratif ou au magistrat désigné statuant sur le recours formé en application de l'article L. 512-1 contre l'obligation de quitter le territoire français de suspendre l'exécution de la mesure d'éloignement jusqu'à l'expiration du délai de recours devant la Cour nationale du droit d'asile ou, si celle-ci est saisie, soit jusqu'à la date de la lecture en audience publique de la décision de la cour, soit, s'il est statué par ordonnance, jusqu'à la date de la notification de celle-ci. Le président du tribunal administratif ou le magistrat désigné à cette fin fait droit à la demande de l'étranger lorsque celui-ci présente des éléments sérieux de nature à justifier, au titre de sa demande d'asile, son maintien sur le territoire durant l'examen de son recours par la cour. ".<br/>
<br/>
              4.	Dans les cas mentionnés au point 3, l'étranger, faisant l'objet d'une obligation de quitter le territoire français qui forme, en application de l'article L. 512-1 du code de l'entrée et du séjour des étrangers et du droit d'asile, un recours contre celle-ci peut, en application de l'article L. 743-3 précité, saisir le tribunal administratif de conclusions à fins de suspension de cette mesure d'éloignement.  <br/>
<br/>
              5.	Il est fait droit à la demande de suspension de la mesure d'éloignement si le juge a un doute sérieux sur le bien-fondé de la décision de rejet ou d'irrecevabilité opposée par l'Office français de protection des réfugiés et apatrides à la demande de protection, au regard des risques de persécutions allégués ou des autres motifs retenus par l'Office. Les moyens tirés des vices propres entachant la décision de l'Office ne peuvent utilement être invoqués à l'appui des conclusions à fin de suspension de la mesure d'éloignement, à l'exception de ceux ayant trait à l'absence, par l'Office, d'examen individuel de la demande ou d'entretien personnel en dehors des cas prévus par la loi ou de défaut d'interprétariat imputable à l'Office.<br/>
<br/>
              6.	A l'appui de ses conclusions à fin de suspension, qui peuvent être présentées sans le ministère d'avocat, le requérant peut se prévaloir d'éléments apparus et de faits intervenus postérieurement à la décision de rejet ou d'irrecevabilité de sa demande de protection ou à l'obligation de quitter le territoire français, ou connus de lui postérieurement.<br/>
<br/>
<br/>
<br/>Le présent avis sera notifié au tribunal administratif de Toulouse, à M. B... A... et au ministre de l'intérieur. <br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">335-03-03 ÉTRANGERS. OBLIGATION DE QUITTER LE TERRITOIRE FRANÇAIS (OQTF) ET RECONDUITE À LA FRONTIÈRE. RÈGLES DE PROCÉDURE CONTENTIEUSE SPÉCIALES. - RECOURS CONTRE UNE OQTF (ART. L. 512-1 DU CESEDA) - CAS OÙ LE DROIT DE SE MAINTENIR SUR LE TERRITOIRE A PRIS FIN EN APPLICATION DES 4° BIS OU 7° DE L'ARTICLE L. 743-2 DU CESEDA - REQUÊTE AUX FINS DE SUSPENSION DE L'OQTF (ART. L. 743-3 DU CESEDA) - 1) SUSPENSION EN CAS DE DOUTE SÉRIEUX SUR LE BIEN-FONDÉ DE LA DÉCISION DE REJET OU D'IRRECEVABILITÉ OPPOSÉE PAR L'OFPRA, AU REGARD DES RISQUES DE PERSÉCUTIONS ALLÉGUÉS OU DES AUTRES MOTIFS RETENUS PAR L'OFFICE - 2) MOYENS TIRÉS DES VICES PROPRES DE LA DÉCISION DE L'OFPRA - PRINCIPE - INOPÉRANCE - EXCEPTION - MOYENS TIRÉS DE L'ABSENCE D'EXAMEN INDIVIDUEL DE LA DEMANDE OU D'ENTRETIEN PERSONNEL [RJ1] OU DE DÉFAUT D'INTERPRÉTARIAT IMPUTABLE À L'OFFICE [RJ2] - 3) POSSIBILITÉ DE SE PRÉVALOIR D'ÉLÉMENTS POSTÉRIEURS À LA DÉCISION DE REJET OU D'IRRECEVABILITÉ DE SA DEMANDE DE PROTECTION OU À L'OQTF.
</SCT>
<ANA ID="9A"> 335-03-03 Dans les cas où le droit de se maintenir sur le territoire a pris fin en application des 4° bis ou 7° de l'article L. 743-2 du code de l'entrée et du séjour des étrangers et du droit d'asile (CESEDA), l'étranger, faisant l'objet d'une obligation de quitter le territoire français qui forme un recours, en application de l'article L. 512-1 de ce code, contre celle-ci peut, en application de l'article L. 743-3, saisir le tribunal administratif de conclusions à fins de suspension de cette mesure d'éloignement....  ,,1) Il est fait droit à la demande de suspension de la mesure d'éloignement si le juge a un doute sérieux sur le bien-fondé de la décision de rejet ou d'irrecevabilité opposée par l'Office français de protection des réfugiés et apatrides à la demande de protection, au regard des risques de persécutions allégués ou des autres motifs retenus par l'Office.... ,,2) Les moyens tirés des vices propres entachant la décision de l'Office ne peuvent utilement être invoqués à l'appui des conclusions a fin de suspension de la mesure d'éloignement, à l'exception de ceux ayant trait à l'absence, par l'Office, d'examen individuel de la demande ou d'entretien personnel en dehors des cas prévus par la loi ou de défaut d'interprétariat imputable à l'Office.,,,3) A l'appui de ses conclusions à fin de suspension, qui peuvent être présentées sans le ministère d'avocat, le requérant peut se prévaloir d'éléments apparus et de faits intervenus postérieurement à la décision de rejet ou d'irrecevabilité de sa demande de protection ou à l'obligation de quitter le territoire français, ou connus de lui postérieurement.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr.  CE, 10 octobre 2013, OFPRA c/ M.,, n° 362798, 362799, p. 254 ; CE, 27 février 2015, OFPRA c/ M.,, n° 380489, T. pp. 561-835.,,[RJ2] Rappr. CE, 22 juin 2017, M.,, n° 400366, T. pp. 478-768.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
