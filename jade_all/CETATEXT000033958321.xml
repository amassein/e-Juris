<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033958321</ID>
<ANCIEN_ID>JG_L_2017_01_000000383972</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/95/83/CETATEXT000033958321.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème chambre, 30/01/2017, 383972, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-01-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>383972</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DIDIER, PINET</AVOCATS>
<RAPPORTEUR>Mme Déborah Coricon</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emmanuelle Cortot-Boucher</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2017:383972.20170130</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Par une requête, un mémoire en réplique et un nouveau mémoire, enregistrés au secrétariat du contentieux du Conseil d'Etat les 26 août 2014,  20 octobre 2015 et 16 janvier 2017, Mme C...A...B...demande au Conseil d'Etat d'annuler pour excès de pouvoir le décret n° 2014-708 du 25 juin 2014 relatif à l'appellation d'origine contrôlée (AOC) " Bergerac ". <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le traité sur le fonctionnement de l'Union européenne ;<br/>
              - le règlement (UE) n° 1308/2013 du 17 décembre 2013 ;<br/>
              - le code de la consommation ;<br/>
              - le code rural et de la pêche maritime ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Déborah Coricon, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Emmanuelle Cortot-Boucher, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Didier, Pinet, avocat de l'Institut national de l'origine et de la qualité ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 641-5 du code rural et de la pêche maritime : " Peuvent bénéficier d'une appellation d'origine contrôlée les produits agricoles, forestiers ou alimentaires et les produits de la mer, bruts ou transformés, qui remplissent les conditions fixées par les dispositions de l'article L. 115-1 du code de la consommation, possèdent une notoriété dûment établie et dont la production est soumise à des procédures comportant une habilitation des opérateurs, un contrôle des conditions de production et un contrôle des produits ". Aux termes du premier alinéa de l'article L. 641-7 du même code : " La reconnaissance d'une appellation d'origine contrôlée est prononcée par un décret qui homologue un cahier des charges où figurent notamment la délimitation de l'aire géographique de production de cette appellation ainsi que ses conditions de production ". Aux termes de l'article L. 115-1 du code de la consommation  : " Constitue une appellation d'origine la dénomination d'un pays, d'une région ou d'une localité servant à désigner un produit qui en est originaire et dont la qualité ou les caractères sont dus au milieu géographique, comprenant des facteurs naturels et des facteurs humains ". <br/>
<br/>
              2. En premier lieu, il ressort du cahier des charges de l'appellation d'origine contrôlée (AOC) " Bergerac " homologué par le décret attaqué que la description des facteurs humains propres à la viticulture relatifs à la zone parcellaire de l'AOC " Bergerac " y est suffisante. Par suite, la circonstance, invoquée par Mme A...B..., que ce cahier des charges ne précise pas que le vignoble originel de Bergerac était constitué d'un " vignoble seigneurial dépendant du castrum et d'un vignoble ecclésiastique dépendant du prieuré de Saint Martin " ne méconnait pas, contrairement à ce que soutient la requérante, les articles 93 et 94 du règlement (UE) n° 1308/2013 du 17 décembre 2013 et n'est en tout état de cause pas de nature à entacher d'illégalité le décret attaqué. Dès lors, le moyen tiré de ce que le lien à l'origine ne serait pas suffisamment précisé par le cahier des charges critiqué doit être écarté. <br/>
<br/>
              3. En deuxième lieu, si Mme A...B...soutient que le cahier des charges homologué par le décret attaqué n'apporte aucune garantie ni ne formule aucune proposition pour faire face à des épisodes de gel hivernal, le décret n'est pas entaché d'erreur manifeste d'appréciation sur ce point, dès lors que la prise en compte des aléas du climat ne figure pas au nombre des éléments, énumérés au 2 de l'article 94 du règlement (UE) n° 1308/2013 du 17 décembre 2013, qui doivent être mentionnés dans un tel cahier des charges.  Ce moyen doit dès lors être écarté. <br/>
<br/>
              4. En troisième lieu, il ressort des pièces du dossier que l'objectif des nouvelles règles de palissage prévues par le cahier des charges critiqué est d'optimiser la surface foliaire exposée au soleil, laquelle dépend directement de l'écartement entre les rangs, et non de l'écartement entre les pieds d'un même rang. Est sans incidence sur la légalité du cahier des charges le fait que l'axe de plantation des rangs de vigne pourrait constituer un autre élément permettant d'évaluer l'exposition au soleil de la surface foliaire. Dès lors, le moyen tiré de ce que les règles de palissage définies au c) du 1° du IV du cahier des charges homologué par le décret attaqué seraient entachées d'erreur manifeste d'appréciation, faute de prendre en compte l'écart entre les pieds au sein d'un même rang, doit être écarté. <br/>
<br/>
              5. En quatrième lieu, Mme A...B...soutient que la prescription, par le cahier des charges critiqué, de normes de conduite de la vigne telles que la densité de plantation des pieds de vigne et l'écartement entre les rangs de vigne est injustifiée, dès lors que le lien entre de telles normes et la qualité du vin n'est pas établi. Il ressort toutefois des pièces du dossier que l'augmentation de la densité des ceps, d'une part, induit un enracinement plus profond qui augmente la typicité des vins en obligeant chaque pied à optimiser l'espace qui lui est laissé et, d'autre part, améliore la qualité des tanins, ce qui tend à améliorer la qualité et la typicité des vins produits, compte tenu des caractéristiques des sols en cause. Les prescriptions introduites par le cahier des charges litigieux, en matière de normes de conduite de la vigne, ne sont dès lors pas entachées d'erreur manifeste d'appréciation.<br/>
<br/>
              6. En cinquième lieu, aux termes du f du 1° du IX du cahier des charges homologué par le décret attaqué : " Tout opérateur dispose d'une capacité de cuverie de vinification équivalente à 1 fois le produit de la surface en production par le rendement visé au 1° du point VIII pour les vins blancs et rosés et à 1,5 fois le produit de la surface en production par le rendement visé au 1° du point VIII pour les vins rouges ". Il ressort des pièces du dossier que la distinction ainsi opérée en matière de capacités de cuverie entre les règles applicables aux vins blancs et rosés d'une part, et aux vins rouges d'autre part, est justifiée par les conditions d'élaboration des vins rouges, avant embouteillage, qui nécessite une plus grande capacité de cuverie. Le moyen tiré de ce que le cahier des charges litigieux serait entaché à cet égard d'une discrimination illégale doit dès lors être écarté. <br/>
<br/>
              7. Enfin, si la requérante soutient que la densification des vignes imposée par le cahier des charges litigieux méconnaîtrait le principe de précaution et les engagements internationaux de la France pour la protection de l'environnement, ces moyens ne sont pas assortis des précisions permettant d'en apprécier le bien-fondé. <br/>
<br/>
              8. Il résulte de tout ce qui précède que Mme A...B...n'est pas fondée à demander l'annulation du décret attaqué. <br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de Mme A...B...est rejetée.  <br/>
Article 2 : La présente décision sera notifiée à Mme C...A...B..., au ministre de l'agriculture, de l'agroalimentaire et de la forêt et à l'Institut national de l'origine et de la qualité. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
