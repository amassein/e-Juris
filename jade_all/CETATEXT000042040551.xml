<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042040551</ID>
<ANCIEN_ID>JG_L_2020_06_000000431150</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/04/05/CETATEXT000042040551.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème chambre, 12/06/2020, 431150, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-06-12</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>431150</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FABIANI, LUC-THALER, PINATEL</AVOCATS>
<RAPPORTEUR>Mme Christelle Thomas</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Anne Iljic</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2020:431150.20200612</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B... A... a demandé à la Cour nationale du droit d'asile d'annuler la décision du 14 décembre 2018 par laquelle l'Office français de protection des réfugiés et apatrides (OFPRA) a rejeté sa demande d'asile. Par une ordonnance n° 19011947 du 29 mars 2019, le président désigné par la présidente de la Cour nationale du droit d'asile a rejeté sa demande comme irrecevable.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 28 mai et 12 août 2019 au secrétariat du contentieux du Conseil d'Etat, M. A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) réglant l'affaire au fond, de lui reconnaître la qualité de réfugié ou, à défaut, de lui accorder le bénéfice de la protection subsidiaire ;<br/>
<br/>
              3°) de mettre à la charge de l'OFPRA la somme de 3 000 euros à verser à la SCP Fabiani, Luc-Thaler, Pinatel, son avocat, en application des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - le code de justice administrative et l'ordonnance n° 2020-305 du 25 mars 2020 modifiée ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Christelle Thomas, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Anne Iljic, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Fabiani, Luc-Thaler, Pinatel, avocat de M. B... A... ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 731-2 du code de l'entrée et du séjour des étrangers et du droit d'asile : " La Cour nationale du droit d'asile statue sur les recours formés contre les décisions de l'Office français de protection des réfugiés et apatrides prises en application des articles L. 711-1 à L. 711-4, L. 712-1 à L. 712-3, L. 713-1 à L. 713-4, L. 723-1 à L. 723-8, L. 723-11, L. 723-15 et L. 723-16. A peine d'irrecevabilité, ces recours doivent être exercés dans le délai d'un mois à compter de la notification de la décision de l'office (...) ".<br/>
<br/>
              2. Il ressort des pièces du dossier soumis à la Cour nationale du droit d'asile que le pli recommandé contenant la décision du 14 décembre 2018 par laquelle l'Office français de protection des réfugiés et apatrides (OFPRA) a rejeté la demande d'asile de M. A... a été présenté à la dernière adresse indiquée par l'intéressé, domicilié dans un centre d'accueil pour demandeurs d'asile, puis a été retourné à l'OFPRA avec la mention " présenté / avisé " à la date du 1er février 2019 et la case " pli avisé et non réclamé " cochée. Toutefois, il ressort également des pièces du dossier soumis aux juges du fond qu'à l'appui des affirmations de M. A... selon lesquelles il n'a jamais reçu ce pli, la directrice du centre d'accueil pour demandeurs d'asile où il est domicilié a, d'une part, certifié par un courrier adressé à l'OFPRA que le centre d'accueil n'avait pas reçu l'avis de passage du 1er février 2019 et, d'autre part, adressé une réclamation au bureau de poste correspondant le 14 mars 2019, faisant état de trois recommandés prétendument " présentés " par les services postaux sans aucun avis de passage dans la boîte aux lettres du centre d'accueil. Il s'ensuit qu'en estimant que la décision de l'OFPRA avait été régulièrement notifiée à M. A..., la Cour a dénaturé les pièces du dossier qui lui était soumis. <br/>
<br/>
              3. Il résulte de ce qui précède, sans qu'il soit besoin de se prononcer sur l'autre moyen du pourvoi, que M. A... est fondé à demander l'annulation de l'ordonnance qu'il attaque.<br/>
<br/>
              4. M. A... a obtenu le bénéfice de l'aide juridictionnelle. Par suite, son avocat peut se prévaloir des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991. Il y a lieu, dans les circonstances de l'espèce, et sous réserve que la SCP Fabiani, Luc-Thaler, Pinatel, avocat de M. A..., renonce à percevoir la somme correspondant à la part contributive de l'Etat, de mettre la somme de 3 000 euros à la charge de l'Office français de protection des réfugiés et apatrides.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
                            --------------<br/>
<br/>
Article 1er : L'ordonnance du 29 mars 2019 de la Cour nationale du droit d'asile est annulée.<br/>
<br/>
Article 2 : L'affaire est renvoyée à la Cour nationale du droit d'asile.<br/>
<br/>
Article 3 : L'Office français de protection des réfugiés et apatrides versera la somme de 3 000 euros à la SCP Fabiani, Luc-Thaler, Pinatel, avocat de M. A..., au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991, sous réserve que cette société renonce à percevoir la somme correspondant à la part contributive de l'Etat. <br/>
<br/>
Article 4 : La présente décision sera notifiée à M. B... A... et à l'Office français de protection des réfugiés et apatrides. <br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
