<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036566164</ID>
<ANCIEN_ID>JG_L_2018_02_000000414138</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/56/61/CETATEXT000036566164.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème chambre, 01/02/2018, 414138, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-02-01</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>414138</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP THOUVENIN, COUDRAY, GREVY ; SCP LYON-CAEN, THIRIEZ</AVOCATS>
<RAPPORTEUR>M. Christian Fournier</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emmanuelle Cortot-Boucher</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:414138.20180201</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Mme A... a demandé au juge des référés du tribunal administratif de Marseille d'ordonner, sur le fondement des dispositions de l'article L. 521-1 du code de justice administrative, la suspension de la décision en date du 24 juillet 2017 par laquelle le président du conseil départemental des Bouches-du-Rhône a décidé de retirer son agrément d'assistante maternelle à compter du 25 juillet 2017.<br/>
<br/>
              Par une ordonnance n° 1705688 du 24 août 2017, le juge des référés du tribunal administratif de Marseille a rejeté sa demande. <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 8 et 20 septembre 2017 au secrétariat du contentieux du Conseil d'Etat, Mme A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance ; <br/>
<br/>
              2°) statuant en référé, de faire droit à sa demande de suspension ; <br/>
<br/>
              3°) de mettre à la charge du département des Bouches-du-Rhône la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code civil, notamment son article 371-2 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Christian Fournier, maître des requêtes,<br/>
<br/>
              - les conclusions de Mme Emmanuelle Cortot-Boucher, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Masse-Dessen, Thouvenin, Coudray, avocat de Mme A...et à la SCP Lyon-Caen, Thiriez, avocat du département des Bouches-du-Rhône ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Par ordonnance du 24 août 2017, le juge des référés du tribunal administratif de Marseille a rejeté la demande présentée par Mme A...tendant à la suspension de l'exécution de la décision du 24 juillet 2017 par laquelle le président du conseil départemental des Bouches-du-Rhône lui a retiré son agrément d'assistante maternelle.<br/>
<br/>
              2. Aux termes de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision (...) ".<br/>
<br/>
              3. L'urgence justifie que soit prononcée la suspension d'un acte administratif lorsque l'exécution de celui-ci porte atteinte, de manière suffisamment grave et immédiate, à un intérêt public, à la situation du requérant ou aux intérêts qu'il entend défendre. Il appartient au juge des référés d'apprécier concrètement, compte tenu des justifications fournies par le requérant, si les effets de l'acte litigieux sont de nature à caractériser une urgence justifiant que, sans attendre le jugement de la requête au fond, l'exécution de la décision soit suspendue. Il lui appartient également, l'urgence s'appréciant objectivement et compte tenu de l'ensemble des circonstances de chaque espèce, de faire apparaître dans sa décision tous les éléments qui, eu égard notamment à l'argumentation des parties, l'ont conduit à considérer que la suspension demandée revêtait un caractère d'urgence. <br/>
<br/>
              4. Pour estimer que la situation dans laquelle se trouvait la requérante du fait, d'une part, de la perte de revenus liés à son activité d'assistante maternelle et, d'autre part, du délai nécessaire pour percevoir des allocations chômage, le juge des référés du tribunal administratif de Marseille a comparé avec les charges fixes du ménage qu'elle formait avec son concubin, lesquelles s'élevaient à 1 461 euros hors frais de nourriture, d'assurance automobile et habitation et d'impôt sur le revenu, le salaire mensuel du concubin, qualifié de net et s'élevant, selon l'ordonnance attaquée, à 2 028 euros. Il ressort toutefois du bulletin de paie produit au titre du mois de juin 2017 que cette somme correspondait à une rémunération brute, avant déduction des cotisations patronales d'un montant de 497 euros et hors prise en compte d'une indemnité dite de grand déplacement, destinée à couvrir les frais d'hébergement et d'alimentation de l'intéressé lorsqu'il travaille loin de son domicile. Par suite, l'ordonnance attaquée est entachée d'inexactitude matérielle et, compte tenu de l'incidence de celle-ci sur l'appréciation portée sur l'urgence, la requérante est fondée à en demander l'annulation.<br/>
<br/>
              5. Il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au titre de la procédure de référé engagée par la requérante, en application des dispositions de l'article L. 821-2 du code de justice administrative.<br/>
<br/>
              6. Si la requérante fait valoir, en premier lieu, que le retrait de son agrément l'empêche d'assurer la garde de trois enfants à partir du 28 août 2017, il ne ressort pas des pièces du dossier que leurs familles n'aient pas trouvé un autre mode de garde alternatif depuis le mois de septembre 2017. Par ailleurs, la décision litigieuse est fondée notamment sur le motif, dont il n'apparaît pas, en l'état de l'instruction, qu'il serait entaché d'inexactitude matérielle, tenant au caractère inadéquat des conditions d'accueil qu'elle leur proposait. <br/>
<br/>
              7. S'agissant, en second lieu, de sa situation financière, dès lors que les charges que la requérante met en avant correspondent aussi à celles de son concubin, elle n'est pas fondée à soutenir que les revenus de celui-ci ne devrait pas être pris en compte, alors, au surplus, qu'il est le père de sa fille et doit à ce titre, en application de l'article 371-2 du code civil, contribuer à l'entretien de cette dernière à proportion de ses ressources. Par ailleurs, s'il résulte de ce qui a été dit au point 4, que ces revenus sont à peine supérieurs, hors indemnité de grand déplacement, aux charges fixes du ménage mises en avant par la requérante, cette dernière n'établit pas, plusieurs mois désormais après la décision litigieuse, être encore privée des allocations chômage et que ces dernières ne lui permettraient pas, dans l'attente du jugement au fond, de faire face à ses autres dépenses.<br/>
<br/>
              8. Dans ces conditions, il n'apparaît pas, en l'état de l'instruction, que l'urgence justifie la suspension de l'exécution de la décision attaquée. La demande de référé présentée par la requérante doit dès lors être rejetée. <br/>
<br/>
              9. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à la charge du département des Bouches-du-Rhône, qui n'est pas la partie perdante dans la présente instance. Il n'y a pas lieu, dans les circonstances de l'espèce, de mettre à la charge de Mme A...la somme que réclame le département des Bouches-du-Rhône au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : L'ordonnance du 24 août 2017 du juge des référés du tribunal administratif de Marseille est annulée.<br/>
Article 2 : La demande de Mme A...tendant à ce que soit ordonnée la suspension de l'exécution de la décision 24 juillet 2017 par laquelle le président du conseil départemental des Bouches-du-Rhône lui a retiré son agrément d'assistante maternelle et ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 3 : Les conclusions du département des Bouches-du-Rhône présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée à Madame A... et au département des Bouches-du-Rhône.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
