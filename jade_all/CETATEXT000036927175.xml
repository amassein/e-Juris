<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036927175</ID>
<ANCIEN_ID>JG_L_2018_05_000000415924</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/92/71/CETATEXT000036927175.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème chambre, 04/05/2018, 415924, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-05-04</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>415924</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DIDIER, PINET ; SCP GARREAU, BAUER-VIOLAS, FESCHOTTE-DESBOIS</AVOCATS>
<RAPPORTEUR>M. Cyrille Beaufils</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Louis Dutheillet de Lamothe</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:415924.20180504</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par deux arrêtés du 11 mai 2017, le maire de Plaudren a accordé, respectivement, un permis de construire à l'EARL du Mené pour la construction d'un poulailler et un permis de construire à la SCEA Guillevic pour la construction d'un hangar à compostage de fumier de volaille.  <br/>
<br/>
              M. B...A...et Mme C...A...ont demandé au juge des référés du tribunal administratif de Rennes, sur le fondement de l'article L.521-1 du code de justice administrative, d'une part, d'ordonner la suspension de l'exécution de l'arrêté du 11 mai 2017 relatif au permis de construire accordé à l'EARL du Mené et de la décision du 6 juillet 2017 rejetant leur recours gracieux, et d'enjoindre à la commune de Plaudren, dans les 24 heures suivant la notification de son ordonnance et sous astreinte de 500 euros par jour de retard, de s'assurer de l'interruption effective des travaux et, le cas échéant, de constater l'infraction et d'édicter un arrêté interruptif de travaux, d'autre part, d'ordonner la suspension de l'exécution de l'arrêté du 11 mai 2017 relatif au permis de construire accordé à la SCEA Guillevic et de la décision du 6 juillet 2017 rejetant leur recours gracieux, et d'enjoindre à la même commune, dans les 24 heures suivant la notification de son ordonnance et sous astreinte de 500 euros par jour de retard, de s'assurer de l'interruption effective des travaux, et le cas échéant, de constater l'infraction et d'édicter un arrêté interruptif de travaux.<br/>
<br/>
              Par une ordonnance nos 1704743, 1704744 du 8 novembre 2017, le juge des référés du tribunal administratif de Rennes, joignant les demandes, a suspendu l'exécution de ces deux arrêtés, et a enjoint au maire de Plaudren, dans l'hypothèse où les travaux de construction se poursuivraient malgré l'intervention de la présente ordonnance, de prescrire par arrêté leur interruption dans un délai de huit jours à compter de la notification de la décision.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 23 novembre et 8 décembre 2017 et le 26 février 2018 au secrétariat du contentieux du Conseil d'Etat, la commune de Plaudren, l'EARL du Mené et la SCEA Guillevic demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) de mettre à la charge de M. B...A...et Mme C...A...la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'environnement ;<br/>
              - le code de l'urbanisme ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Cyrille Beaufils, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Louis Dutheillet de Lamothe, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Didier, Pinet, avocat de la commune de Plaudren et autres et à la SCP Garreau, Bauer-Violas, Feschotte-Desbois, avocat de M. et MmeA....<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1.	Il ressort des pièces du dossier soumis au juge des référés du tribunal administratif de Rennes que, par deux arrêtés du 11 mai 2017, le maire de Plaudren a respectivement autorisé la construction d'un poulailler par l'EARL du Mené et la construction d'un hangar à compostage de fumier de volailles par la SCEA Guillevic dans le cadre d'un projet d'extension de l'activité avicole exercée sur le site dénommé " Le Mené ". Par une ordonnance du 8 novembre 2017, contre laquelle la commune de Plaudren, l'EARL du Mené et la SCEA Guillevic se pourvoient en cassation, le juge des référés, à la demande de M. et MmeA..., propriétaires d'une maison d'habitation voisine de l'exploitation, a suspendu l'exécution de ces arrêtés, au motif que le moyen tiré de la méconnaissance de l'article R. 431-16 du code de l'urbanisme était de nature à faire naître, en l'état de l'instruction, un doute sérieux quant à la légalité des permis de construire.<br/>
<br/>
              2.	D'une part, aux termes de l'article R. 431-16 du code de l'urbanisme, dans sa rédaction issue du décret du 11 août 2016 relatif à la modification des règles applicables à l'évaluation environnementale des projets, plans et programmes, applicable au présent litige : " Le dossier joint à la demande de permis de construire comprend en outre, selon les cas : / a) L'étude d'impact ou la décision de l'autorité environnementale dispensant le projet d'évaluation environnementale lorsque le projet relève du tableau annexé à l'article R. 122-2 du code de l'environnement. L'autorité compétente pour délivrer l'autorisation d'urbanisme vérifie que le projet qui lui est soumis est conforme aux mesures et caractéristiques qui ont justifié la décision de l'autorité environnementale de ne pas le soumettre à évaluation environnementale (...) ".<br/>
<br/>
              3.	D'autre part, aux termes de l'article L. 122-1 du code de l'environnement : " (...) II.-Les projets qui, par leur nature, leur dimension ou leur localisation, sont susceptibles d'avoir des incidences notables sur l'environnement ou la santé humaine font l'objet d'une évaluation environnementale en fonction de critères et de seuils définis par voie réglementaire et, pour certains d'entre eux, après un examen au cas par cas effectué par l'autorité environnementale. Pour la fixation de ces critères et seuils et pour la détermination des projets relevant d'un examen au cas par cas, il est tenu compte des données mentionnées à l'annexe III de la directive 2011/92/ UE modifiée du Parlement européen et du Conseil du 13 décembre 2011 concernant l'évaluation des incidences de certains projets publics et privés sur l'environnement. (...) ". Aux termes du II de l'article R. 122-2 du même code : " Les modifications ou extensions de projets déjà autorisés, qui font entrer ces derniers, dans leur totalité, dans les seuils éventuels fixés dans le tableau annexé ou qui atteignent en elles-mêmes ces seuils font l'objet d'une évaluation environnementale ou d'un examen au cas par cas. Les autres modifications ou extensions de projets soumis à évaluation environnementale systématique ou relevant d'un examen au cas par cas, qui peuvent avoir des incidences négatives notables sur l'environnement sont soumises à examen au cas par cas (...) ". L'annexe à cet article R. 122-2 prévoit, dans sa première rubrique, que les projets d'installations classées pour la protection de l'environnement sont soumis à évaluation environnementale systématique ou après un examen au cas par cas. Il résulte des dispositions combinées de cette annexe, de l'article L. 515-28 et de l'annexe 3 à l'article R. 511-9 du même code, dont l'annexe fixe en sa colonne A la nomenclature des installations classées, que les élevages intensifs de volailles comptant plus de 40 000 poulets mentionnés dans la rubrique 3660 de cette nomenclature sont soumis à évaluation environnementale systématique.<br/>
<br/>
              4.	 Il ressort des énonciations souveraines de l'ordonnance attaquée que les constructions projetées doivent permettre une extension de l'activité avicole en cause, le nouveau poulailler venant s'ajouter aux deux déjà existants afin d'augmenter la capacité d'élevage de 106 200 à 136 200 poulets et le futur hangar à compostage devant permettre le stockage de l'ensemble des fumiers produits, avec une capacité de 810 tonnes. Il en ressort également, d'une part, que le juge des référés a rappelé qu'en vertu des dispositions nouvelles de l'article R. 431-16 du code de l'urbanisme citées au point précédent, qu'il a jugé applicables au litige, l'obligation de joindre au dossier de demande de permis de construire l'étude d'impact ou la décision de l'autorité environnementale en dispensant le projet concernait désormais tous les projets relevant du tableau annexé à l'article R. 122-2 du code de l'environnement. Le juge des référés a, d'autre part, estimé qu'il ressortait des pièces du dossier qui lui était soumis que les projets de construction litigieux étaient au nombre de ceux-ci, dès lors qu'ils tendaient à l'extension d'activités d'élevage avicole soumises au régime des installations classées et relevant de ce tableau, que ce projet était susceptible d'avoir des incidences négatives notables sur l'environnement et qu'il n'était pas justifié de l'existence d'une étude d'impact ou d'une décision de l'autorité environnementale dispensant le projet d'une telle évaluation. Il en a enfin déduit que le moyen tiré de la méconnaissance de l'article R. 431-16 du code de l'urbanisme était de nature à faire naître, en l'état de l'instruction, un doute sérieux quant à la légalité des arrêtés attaqués. En statuant ainsi, le juge des référés n'a, eu égard à son office ainsi qu'à la teneur de l'argumentation présentée devant lui, pas entaché son ordonnance des erreurs de droit ni des insuffisances de motivation alléguées. <br/>
<br/>
              5.	Il résulte de tout ce qui précède que la commune de Plaudren et autres ne sont pas fondés à demander l'annulation de l'ordonnance qu'ils attaquent.<br/>
<br/>
              6.	Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise, à ce titre, à la charge de M. et MmeA..., qui ne sont pas, dans la présente instance, la partie perdante. En revanche, il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la commune de Plaudren de l'EARL du Mené et de la SCEA Guillevic la somme de 1 000 euros chacun à verser à M. et Mme A...au titre des mêmes dispositions.<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
      Article 1er : Le pourvoi de la commune de Plaudren et autres est rejeté.<br/>
Article 2 : La commune de Plaudren, l'EARL du Mené et la SCEA Guillevic verseront chacun à M. et Mme A...la somme de 1 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée à la commune de Plaudren, première dénommée, pour l'ensemble des requérants et à M. B...A..., premier dénommé, pour l'ensemble des défendeurs.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
