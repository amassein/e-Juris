<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000035774985</ID>
<ANCIEN_ID>JG_L_2017_10_000000401049</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/35/77/49/CETATEXT000035774985.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème chambre, 11/10/2017, 401049, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-10-11</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>401049</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FOUSSARD, FROGER ; SCP MARLANGE, DE LA BURGADE</AVOCATS>
<RAPPORTEUR>Mme Liza Bellulo</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Romain Victor</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2017:401049.20171011</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société France Télécom, devenue la société Orange, a demandé au tribunal administratif de Lyon de condamner la société des autoroutes Paris-Rhin-Rhône (APRR) à lui verser une indemnité en réparation des frais de déplacement de ses ouvrages. Par un jugement n° 1303403 du 9 juin 2015, ce tribunal a fait droit à cette demande.  <br/>
<br/>
              Par un arrêt n° 15LY02756 du 28 avril 2016, la cour administrative d'appel de Lyon a rejeté l'appel formé par la société APRR contre ce jugement.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en répliqué, enregistrés les 28 juin et 23 septembre 2016 et le 18 septembre 2017 au secrétariat du contentieux du Conseil d'Etat, la société APRR demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code général de la propriété des personnes publiques ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Liza Bellulo, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Romain Victor, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Foussard, Froger, avocat de la société des autoroutes Paris-Rhin-Rhône et à la SCP Marlange, de la Burgade, avocat de la société Orange.<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que la réalisation de travaux de création d'une nouvelle liaison entre les autoroutes A6 et A46 par la société des autoroutes Paris-Rhin-Rhône (ci-après APPR) a conduit la société France Télécom, devenue la société Orange, à déplacer ses ouvrages chemin de Batailly, sur le territoire de la commune des Chères, et route de la Thibaudière, sur le territoire de la commune de Quincieux. La société APRR se pourvoit en cassation contre l'arrêt de la cour administrative d'appel de Lyon du 28 avril 2016 confirmant le jugement par lequel le tribunal administratif de Clermont-Ferrand l'a condamnée à verser à la société Orange une somme de 19 833,31 euros, au titre de l'indemnisation du coût de ces déplacements.<br/>
<br/>
              2. En premier lieu, l'article R. 741-2 du code de justice administrative dispose que la décision rendue par une juridiction administrative " contient (...) les visas des dispositions législatives ou réglementaires dont elle fait application ". En énonçant que les voies où se situent les ouvrages de la société Orange relevaient de la voirie départementale ou communale et que les travaux ayant nécessité le déplacement de ces ouvrages prolongeaient un axe autoroutier par la création d'une voie nouvelle, dépendant du réseau national, la cour a relevé de simples éléments de fait sans faire application du code de la voirie routière. Par suite, l'arrêt attaqué n'avait pas à faire application des dispositions précitées.<br/>
<br/>
              3. En deuxième lieu, le bénéficiaire d'une autorisation d'occupation temporaire du domaine public doit, sauf convention contraire, quelle que soit sa qualité, supporter sans indemnité les frais de déplacement ou de modification des installations aménagées en vertu de cette autorisation lorsque ce déplacement est la conséquence de travaux entrepris dans l'intérêt du domaine public occupé et que ces travaux constituent une opération d'aménagement conforme à la destination de ce domaine.<br/>
<br/>
              4. Il ressort des pièces du dossier qui était soumis à la cour que les voies où étaient implantés les ouvrages de la société Orange, et pour lesquels elle bénéficiait d'une autorisation d'occupation temporaire du domaine public, relevaient de la voirie départementale ou communale et étaient situées en zone rurale et dédiées à la desserte locale de champs, de hameaux ou d'habitations. La cour a, par ailleurs, relevé que les travaux prolongeaient un axe autoroutier par la création d'une voie nouvelle, dépendant du réseau national, qui ne reprenait pas le tracé de routes préexistantes. Elle n'a commis ni erreur de droit ni erreur de qualification juridique des faits en déduisant de l'ensemble de ces éléments, de manière suffisamment motivée, que les travaux ne correspondaient pas à une évolution normale du domaine routier occupé par la société et qu'ils ne constituaient pas une opération d'aménagement conforme à la destination du domaine occupé par la société Orange, écartant ainsi implicitement le moyen tiré de ce que le domaine public routier dans son ensemble aurait une destination unique commune qui serait la circulation.<br/>
<br/>
              5. Si la société requérante soutient, en dernier lieu, que la cour a commis une erreur de droit en déduisant du caractère nouveau de la voie créée l'absence de conformité des travaux entrepris à la destination du domaine public occupé par la société Orange, il ressort des énonciations de l'arrêt attaqué que tel n'est pas le cas, la cour s'étant fondée sur un ensemble plus large d'éléments rappelés au point 4 pour déterminer si la condition de l'aménagement conforme était remplie. Par suite, ce moyen manque en fait. <br/>
<br/>
              6. Il résulte de ce qui précède que la société APRR n'est pas fondée à demander l'annulation de l'arrêt qu'elle attaque.<br/>
<br/>
              7. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la société APRR la somme de 3 000 euros à verser à la société Orange, au titre des dispositions de l'article L. 761-1 du code de justice administrative. Ces mêmes dispositions font obstacle à ce qu'une somme soit mise à la charge de la société Orange qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de la société APRR est rejeté.<br/>
Article 2 : La société APRR versera à la société Orange une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée à la société des autoroutes Paris-Rhin-Rhône et à la société Orange.  <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
