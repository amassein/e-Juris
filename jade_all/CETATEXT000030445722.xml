<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030445722</ID>
<ANCIEN_ID>JG_L_2015_03_000000384862</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/44/57/CETATEXT000030445722.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère sous-section jugeant seule, 20/03/2015, 384862, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-03-20</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>384862</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère sous-section jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Laurent Cytermann</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Alexandre Lallet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:384862.20150320</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              1° Par une requête, enregistrée sous le n° 384862 le 29 septembre 2014 au secrétariat du contentieux du Conseil d'Etat, M. B...A...demande au Conseil d'Etat d'annuler la décision du directeur général de Pôle emploi, par un accord salarial du 14 février 2014, d'augmenter de 1 % les salaires d'une partie seulement du personnel de Pôle emploi.<br/>
<br/>
<br/>
              2° Par une requête, enregistrée sous le n° 385760 le 17 novembre 2014 au secrétariat du contentieux du Conseil d'Etat, M. A...demande au Conseil d'Etat d'annuler la décision du directeur général de Pôle emploi, par un accord salarial du 15 septembre 2014, de majorer la partie fixe du salaire de 0,5 % et le point salaire de 0,5 %, rétroactivement au 1er janvier 2014, d'une partie seulement du personnel de Pôle emploi.<br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu les autres pièces des dossiers.<br/>
<br/>
              Vu :<br/>
              - le code du travail ; <br/>
              - la loi n° 2008-126 du 13 février 2008 ;<br/>
              - le décret n° 2003-1370 du 31 décembre 2003 ;<br/>
              - le code de justice administrative, notamment son article R. 611-8.<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Laurent Cytermann, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Alexandre Lallet, rapporteur public.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Les requêtes de M. A...présentent à juger les mêmes questions. Il y a lieu de les joindre pour statuer par une seule décision.<br/>
<br/>
              2. La loi du 13 février 2008 relative à la réforme de l'organisation du service public de l'emploi a substitué l'institution nationale Pôle emploi à l'Agence nationale pour l'emploi et aux associations pour l'emploi dans l'industrie et le commerce. <br/>
<br/>
              3. D'une part, l'article L. 5312-9 du code du travail, issu de cette loi, dispose que : " Les agents de l'institution nationale, qui sont chargés d'une mission de service public, sont régis par le présent code dans les conditions particulières prévues par une convention collective étendue agréée par les ministres chargés de l'emploi et du budget. Cette convention comporte des stipulations, notamment en matière de stabilité de l'emploi et de protection à l'égard des influences extérieures, nécessaires à l'accomplissement de cette mission. / Les règles relatives aux relations collectives de travail prévues par la deuxième partie du présent code s'appliquent à tous les agents de l'institution, sous réserve des garanties justifiées par la situation particulière de ceux qui restent contractuels de droit public. Ces garanties sont définies par décret en Conseil d'Etat ".<br/>
<br/>
              4. D'autre part, le I de l'article 7 de la loi du 13 février 2008 a prévu le transfert à Pôle emploi, à sa date de création, des agents de l'Agence nationale pour l'emploi, en disposant qu'" Ils restent régis par le décret n° 2003-1370 du 31 décembre 2003 fixant les dispositions applicables aux agents contractuels de droit public de l'Agence nationale pour l'emploi et par les dispositions générales applicables aux agents non titulaires de l'Etat prévues par le décret n° 86-83 du 17 janvier 1986 ", sauf s'ils choisissent d'opter pour la convention collective prévue à l'article L. 5312-9 du code du travail dans un délai d'un an suivant son agrément. Aux termes de l'article 19 du décret du 31 décembre 2003 : " Les agents de Pôle emploi ont droit, après service fait, à une rémunération mensuelle calculée en fonction de l'indice afférent à leur échelon de classement. La valeur du point d'indice est celle de la fonction publique et suit son évolution ".<br/>
<br/>
              5. M.A..., qui ne conteste pas que le directeur général de Pôle emploi n'a pas compétence pour augmenter la valeur du point d'indice applicable à la rémunération des agents contractuels de droit public de Pôle emploi, demande l'annulation des décisions de ce directeur général d'augmenter la rémunération des agents de droit privé de l'institution nationale, par deux accords salariaux conclus avec plusieurs organisations syndicales représentatives les 14 février et 15 septembre 2014.<br/>
<br/>
              6. Toutefois, alors même que Pôle emploi est un établissement public à caractère administratif, toute contestation portant sur la légalité ou l'application et la dénonciation d'une convention collective ou d'un avenant conclu en application de l'article L. 5312-9 du code du travail ou d'un accord conclu dans ce cadre relève de la compétence judiciaire, hormis le cas où la contestation concerne des clauses qui n'ont pas pour objet la détermination des conditions d'emploi, de formation professionnelle et de travail ainsi que des garanties sociales des personnels de Pôle emploi mais qui régissent l'organisation du service public.<br/>
<br/>
              7. Les accords des 14 février et 15 septembre 2014 ont pour seul objet de déterminer l'évolution des rémunérations des salariés de Pôle emploi régis par le code du travail et relevant du droit privé. Dès lors, la juridiction administrative n'est pas compétente pour connaître de recours contestant leur légalité. Les conclusions de M. A...dirigées contre les décisions du directeur général de Pôle emploi de signer ces accords doivent, par suite, être rejetées. <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : Les requêtes de M. A...dirigées contre les décisions du directeur général de Pôle emploi de conclure les accords des 14 février et 15 septembre 2014 sont rejetées comme portées devant une juridiction incompétente pour en connaître.<br/>
Article 2 : La présente décision sera notifiée à M. B...A....<br/>
Copie en sera adressée pour information à Pôle emploi.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
