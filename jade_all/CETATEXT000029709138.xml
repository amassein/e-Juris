<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029709138</ID>
<ANCIEN_ID>JG_L_2014_11_000000361016</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/70/91/CETATEXT000029709138.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème et 10ème sous-sections réunies, 05/11/2014, 361016, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-11-05</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>361016</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème et 10ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MONOD, COLIN, STOCLET ; SCP DE CHAISEMARTIN, COURJON</AVOCATS>
<RAPPORTEUR>Mme Séverine Larere</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Marie-Astrid Nicolazo de Barmon</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:361016.20141105</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
Procédure contentieuse antérieure<br/>
<br/>
              La société Lonfat et fils a demandé au tribunal administratif de la Polynésie française de prononcer la décharge de la cotisation supplémentaire d'impôt sur les sociétés et des pénalités correspondantes auxquelles elle a été assujettie, au titre de l'année 2003, à hauteur de la somme de 11 278 575 francs CFP. Par un jugement n°1000346 du 26 octobre 2010, le tribunal administratif de la Polynésie française a rejeté cette demande.<br/>
<br/>
              Par un arrêt n° 11PA00413 du 13 avril 2012, la cour administrative d'appel de Paris a rejeté l'appel formé par la société Lonfat et fils contre ce jugement.<br/>
<br/>
Procédure devant le Conseil d'Etat<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 13 juillet 2012, 15 octobre 2012 et 5 mars 2014 au secrétariat du contentieux du Conseil d'Etat, la société Lonfat et fils demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 11PA00413 du 13 avril 2012 de la cour administrative d'appel de Paris ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de la Polynésie française la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu :<br/>
              - les autres pièces du dossier ;<br/>
              - la loi organique n° 96-312 du 12 avril 1996 ; <br/>
              - la loi organique n° 2004-192 du 27 février 2004 ;<br/>
              - le code des impôts de la Polynésie française ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Séverine Larere, maître des requêtes,<br/>
<br/>
              - les conclusions de Mme Marie-Astrid Nicolazo de Barmon, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Monod, Colin, Stoclet, avocat de la société Lonfat et fils et à la SCP de Chaisemartin, Courjon, avocat du gouvernement de la Polynésie française ;<br/>
<br/>
<br/>
<br/>1. Considérant que l'article 115-1-1 du code des impôts de la Polynésie française, en vigueur à l'époque des impositions en litige, prévoyait l'octroi d'un crédit d'impôt, imputable sur l'impôt sur les sociétés, bénéficiant aux sociétés participant au financement de certains investissements immobiliers ; qu'en vertu du même article, en cas de non-présentation du certificat de conformité du bâtiment à l'issue du trentième mois suivant celui de la délivrance du permis de construire, l'impôt pour lequel un crédit avait préalablement été accordé devenait immédiatement exigible ;<br/>
<br/>
              2. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, faute de présentation du certificat de conformité dans le délai requis, le service des contributions de la Polynésie française a remis en cause, en 2007, le crédit d'impôt sur les sociétés dont la société Lonfat et fils avait bénéficié sur le fondement des dispositions de l'article 115-1-1 du code des impôts de la Polynésie française, à raison de sa participation au financement d'un projet de construction immobilière, réalisé par la SCI " Société financière d'investissements immobiliers ", qui avait fait l'objet d'un permis de construire délivré le 14 août 2003 ; que, par un jugement du 26 octobre 2010, le tribunal administratif de la Polynésie française a rejeté la demande de la société tendant à la décharge de la cotisation supplémentaire d'impôt sur les sociétés et des intérêts de retard correspondants, auxquels elle a, en conséquence, été assujettie ; qu'elle se pourvoit en cassation contre l'arrêt du 13 avril 2012 par lequel la cour administrative d'appel de Paris a rejeté son appel dirigé contre ce jugement ;<br/>
<br/>
              Sur la régularité de la procédure d'imposition :<br/>
<br/>
              3. Considérant que l'article 433-6 du code des impôts de la Polynésie française, relatif au fonctionnement de la commission des impôts, prévoit que, lorsqu'elle est saisie dans le cadre de la procédure de redressement prévue à l'article 421-1-2 du même code, la commission des impôts émet un avis qui ne lie pas l'administration ; que cet article précise, toutefois, que : " dans le cas où le service des contributions établit une imposition ou effectue un redressement sur la base d'une appréciation contraire à l'avis de la commission, il a l'obligation, avant la mise en recouvrement, sous peine de nullité de la procédure de vérification, de transmettre le dossier, pour décision, au Président de la Polynésie française ou à son délégataire. (...) " ; <br/>
<br/>
              4. Considérant qu'il résulte de ces dispositions, éclairées par les travaux préparatoires de la délibération n° 2002-159 APF du 5 décembre 2002 dont elles sont issues, que la procédure de vérification qu'elles visent s'entend de la seule procédure de vérification de comptabilité prévue à l'article 412-1 du code des impôts et que, dès lors, l'obligation de transmission du dossier au président de la Polynésie française ou à son délégataire qu'elles prévoient ne s'applique pas lorsque le redressement fait suite, comme en l'espèce, à un contrôle sur pièces ; que, par suite, c'est sans erreur de droit que la cour a écarté comme inopérant le moyen invoqué par la société Lonfat et fils tiré de ce que la procédure d'imposition aurait été entachée d'irrégularité, faute pour le service des contributions d'avoir transmis le dossier au président de la Polynésie française alors que le redressement avait été effectué sur la base d'une appréciation contraire à l'avis de la commission des impôts ; <br/>
<br/>
              Sur le bien-fondé des impositions :<br/>
<br/>
              En ce qui concerne la prescription :<br/>
<br/>
              5. Considérant qu'aux termes de l'article 115-1-1 du code des impôts de la Polynésie française dans sa rédaction applicable à l'imposition en litige : "Les personnes morales passibles de l'impôt sur les sociétés bénéficient d'un crédit d'impôt pour tout financement égal ou supérieur à 10 millions de francs dans un projet de construction immobilière (...) d'un coût total égal ou supérieur à 100 millions de francs dont la demande de permis de construire aura été déposée avant le 31 décembre 2001. (...) Ce crédit d'impôt est imputable sur la moitié de l'impôt sur les sociétés dû, établi au titre de l'exercice de la réalisation du financement, sur présentation d'une attestation précisant les modalités du financement délivrée par le constructeur. / Le solde éventuel du crédit d'impôt est imputable dans la même limite sur les cinq exercices suivants. (...) / Ces avantages sont remis en cause, et l'impôt dont le crédit a été préalablement accordé devient immédiatement exigible, nonobstant le cas échéant l'expiration des délais de prescription, dans les circonstances suivantes : (...) - non-présentation du certificat de conformité à l'issue du trentième mois suivant celui de la délivrance du permis de construire (...) " ;<br/>
<br/>
              6. Considérant qu'après avoir jugé que les dispositions précitées de l'article 115-1-1 du code des impôts méconnaissaient le principe de sécurité juridique, en tant qu'elles excluaient, de manière générale, l'application des délais de prescription et avoir écarté, dans cette mesure, leur application, la cour a jugé qu'il y avait lieu de faire application des dispositions de droit commun prévues par l'article 451-1 du code des impôts de la Polynésie française qui, dans sa rédaction alors applicable, prévoyait que : "Les omissions totales ou partielles constatées dans l'assiette ou la liquidation des impôts et taxes visés au présent code ainsi que les erreurs commises dans l'établissement des impositions, dans l'application des tarifs ou dans le calcul des cotisations peuvent être réparées jusqu'à l'expiration de la troisième année suivant celle au titre de laquelle l'imposition est due " ; que la cour a ensuite relevé qu'il résultait des dispositions précitées de l'article 115-1-1 du code des impôts de la Polynésie française que le certificat de conformité relatif aux constructions autorisées par le permis de construire du 14 août 2003 aurait dû être présenté à l'issue du trentième mois suivant celui de la délivrance de ce permis de construire, soit avant le 1er mars 2006 ; qu'elle a jugé que ce n'était qu'à compter de cette date que le service des contributions de la Polynésie française avait été en mesure de constater le manquement à l'obligation de présenter le certificat de conformité et, par conséquent, de remettre en cause pour ce motif le crédit d'impôt dont la contribuable avait bénéficié, devenu immédiatement exigible à cette même date ; que la cour en a déduit qu'en application des dispositions précitées de l'article 451-1 du code des impôts de la Polynésie française, l'administration disposait en l'espèce, pour exercer son droit de reprise, d'un délai expirant au plus tard le 31 décembre 2009 ;<br/>
<br/>
              7. Considérant qu'il résulte des dispositions précitées de l'article 115-1-1 du code des impôts de la Polynésie française que la remise en cause, pour non-présentation du certificat de conformité, des avantages fiscaux résultant de l'imputation du crédit d'impôt qu'elles prévoient ne peut intervenir qu'à l'issue du délai de trente mois imparti au contribuable pour présenter ce certificat ; que le fait générateur de l'imposition résultant de cette remise en cause se rattache, ainsi, à l'année au cours de laquelle ce délai a expiré ; que c'est, par suite, sans erreur de droit qu'après avoir constaté, par des motifs non contestés de son arrêt, que le délai de trente mois imparti à la société Lonfat et fils pour produire le certificat de conformité expirait le 1er mars 2006, la cour en a déduit que le droit de reprise de l'administration pouvait s'exercer jusqu'au 31 décembre 2009 ;<br/>
<br/>
              En ce qui concerne le bien-fondé de la remise en cause du crédit d'impôt :<br/>
<br/>
              8. Considérant qu'ainsi qu'il a été dit, il résulte des dispositions précitées de l'article 115-1-1 du code des impôts de Polynésie française que les avantages résultant de l'imputation du crédit d'impôt qu'elles prévoient peuvent être remis en cause lorsque le certificat de conformité n'a pas été présenté à l'issue du trentième mois suivant celui de la délivrance du permis de construire ; qu'il suit de là que la cour n'a pas commis d'erreur de droit en jugeant, d'une part, que ces dispositions ne mentionnaient aucune circonstance permettant de suspendre le délai de présentation du certificat de conformité et, d'autre part, qu'elles n'autorisaient ni le président du gouvernement de la Polynésie française ni aucune autre autorité à proroger ce délai et en en déduisant que la société Lonfat et fils ne pouvait utilement se prévaloir, à l'appui de ses conclusions tendant à la décharge de l'imposition en litige, de la circonstance que, par une lettre du 15 février 2007, dans laquelle il était fait référence aux dispositions de l'article 914-4 du code des impôts de la Polynésie française, le ministre des finances et de la fonction publique du gouvernement de la Polynésie française avait demandé au chef du service des contributions de " suspendre la procédure engagée ", après avoir notamment fait valoir que " le dépassement du délai pour la production du certificat de conformité n'était pas imputable aux investisseurs " ;<br/>
<br/>
              9. Considérant qu'il résulte de tout ce qui précède que la société Lonfat et fils n'est pas fondée à demander l'annulation de l'arrêt qu'elle attaque ; que ses conclusions tendant à ce qu'une somme soit mise à la charge de la Polynésie française, en application de l'article L. 761-1 du code de justice administrative doivent, par suite, être rejetées ; qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions de la Polynésie française tendant à ce qu'une somme soit mise à ce titre à la charge de la société Lonfat et fils ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              D E C I D E :<br/>
              --------------<br/>
Article 1er : Le pourvoi de la société Lonfat et fils est rejeté.<br/>
Article 2 : Les conclusions présentées par la Polynésie française au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La présente décision sera notifiée à la société Lonfat et fils et au gouvernement de la Polynésie française.<br/>
Copie en sera adressée au ministre des finances et des comptes publics.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
