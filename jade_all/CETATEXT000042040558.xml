<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042040558</ID>
<ANCIEN_ID>JG_L_2020_06_000000434775</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/04/05/CETATEXT000042040558.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème chambre, 12/06/2020, 434775, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-06-12</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>434775</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GATINEAU, FATTACCINI, REBEYROL</AVOCATS>
<RAPPORTEUR>Mme Myriam Benlolo Carabot</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Anne Iljic</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2020:434775.20200612</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B... A... a demandé au tribunal administratif de Dijon d'annuler pour excès de pouvoir la décision de déclassement prise à son encontre par le directeur du centre pénitentiaire de Varennes-le-Grand le 3 novembre 2017. Par un jugement n° 1702863 du 21 décembre 2018, le tribunal administratif de Dijon a rejeté sa demande. Par une ordonnance n° 19LY00784 du 20 mai 2019, la cour administrative d'appel de Lyon, saisie de l'appel formé par M. A..., a donné acte de son désistement. <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 23 septembre et 4 décembre 2019 au secrétariat du contentieux du Conseil d'Etat, M. A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 000 euros à verser à son avocat, la SCP Gatineau, Fattaccini, au titre des articles L.761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991. <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - le code de justice administrative et l'ordonnance n° 2020-305 du 25 mars 2020 modifiée ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Myriam Benlolo Carabot, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Anne Iljic, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Gatineau, Fattaccini, Rebeyrol, avocat de M. B... A... ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. L'article R. 222-1 du code de justice administrative dispose que : " (...) les présidents de formation de jugement des tribunaux et des cours (...) peuvent, par ordonnance : 1° Donner acte des désistements ; (...) ". Aux termes de l'article R. 612-5 du même code : " Devant les tribunaux administratifs et les cours administratives d'appel, si le demandeur, malgré la mise en demeure qui lui a été adressée, n'a pas produit le mémoire complémentaire dont il avait expressément annoncé l'envoi (...), il est réputé s'être désisté ". <br/>
<br/>
              2. Il ressort des pièces de la procédure devant la cour administrative d'appel de Lyon que, dans sa requête sommaire enregistrée le 26 février 2019 au greffe de la cour, M. A... avait annoncé son intention de produire un mémoire complémentaire. A la suite de la mise en demeure qui lui a été notifiée le 15 mars 2019 sur le fondement de l'article R. 612-5 du code de justice administrative, M. A... a indiqué, par un courrier enregistré au greffe de la cour le 15 mars 2019, avoir produit un mémoire complémentaire le 8 mars 2019. S'il ressort des pièces du dossier que le contenu du mémoire enregistré le 8 mars 2019 au greffe de la cour était identique à celui de la requête sommaire, l'auteur de l'ordonnance attaquée a commis une erreur de droit en se fondant sur l'identité du contenu des mémoires produits pour constater le désistement d'office de M. A... en application de l'article R. 612-5 précité. Il résulte de ce qui précède, sans qu'il soit besoin de se prononcer sur les autres moyens du pourvoi, que M. A... est fondé à demander l'annulation de l'ordonnance qu'il attaque.<br/>
<br/>
              3. M. A... a obtenu le bénéfice de l'aide juridictionnelle. Par suite, son avocat peut se prévaloir des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991. Il y a lieu, dans les circonstances de l'espèce, et sous réserve que la SCP Gatineau, Fattaccini, Rebeyrol renonce à percevoir la somme correspondant à la part contributive de l'Etat, de mettre la somme de 3 000 euros à la charge de l'Etat.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
                          --------------<br/>
<br/>
Article 1er : L'ordonnance du 20 mai 2019 est annulée.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Lyon.<br/>
Article 3 : L'Etat versera la somme de 3 000 euros à la SCP Gatineau, Fattaccini, Rebeyrol avocat de M. A..., au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991, sous réserve que cette société renonce à percevoir la somme correspondant à la part contributive de l'Etat.<br/>
Article 4 : La présente décision sera notifiée à M. B... A... et à la garde des sceaux, ministre de la justice.<br/>
<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
