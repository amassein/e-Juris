<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033684999</ID>
<ANCIEN_ID>JG_L_2016_12_000000378879</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/68/49/CETATEXT000033684999.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème - 3ème chambres réunies, 23/12/2016, 378879, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-12-23</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>378879</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème - 3ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP THOUIN-PALAT, BOUCARD ; HAAS</AVOCATS>
<RAPPORTEUR>Mme Karin Ciavaldini</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Romain Victor</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2016:378879.20161223</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Procédure contentieuse antérieure<br/>
<br/>
              La société Photo J.L. Josse a demandé au tribunal administratif d'Orléans l'annulation pour excès de pouvoir de la décision implicite par laquelle le maire de la commune de Tours (Indre-et-Loire) a rejeté sa demande d'autorisation de prendre des clichés de certaines oeuvres appartenant aux collections du musée des Beaux-Arts de la commune. Par un jugement n° 0603317 du 20 janvier 2009, le tribunal administratif d'Orléans a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 09NT00705 du 4 mai 2010, la cour administrative d'appel de Nantes a, à la demande de la société Photo J. L. Josse, annulé ce jugement et la décision implicite de rejet attaquée.<br/>
<br/>
              Par la décision n° 341173 du 29 octobre 2012, le Conseil d'Etat, statuant au contentieux, a annulé cet arrêt et renvoyé l'affaire devant la cour administrative d'appel de Nantes.<br/>
<br/>
              Par un arrêt n° 12NT02907 du 28 février 2014, la cour administrative d'appel de Nantes, statuant sur renvoi du Conseil d'Etat, a rejeté l'appel formé contre le jugement du tribunal administratif d'Orléans par la société Photo J. L. Josse.<br/>
<br/>
              Procédure devant le Conseil d'Etat<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et deux nouveaux mémoires, enregistrés les 28 avril, 26 juillet et 22 décembre 2014 et le 16 novembre 2016 au secrétariat du contentieux du Conseil d'Etat, la société Photo J. L. Josse demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de la commune de Tours la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code général de la propriété des personnes publiques ;<br/>
              - le code de la propriété intellectuelle ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Karin Ciavaldini, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Romain Victor, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Thouin-Palat, Boucard, avocat de la société Photo J. L. Josse et à Me Haas, avocat de la commune de Tours.<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que l'entreprise unipersonnelle à responsabilité limitée (EURL) Photo Josse, aux droits de laquelle est venue la société Photo J. L. Josse, a, le 11 mai 2006, demandé au maire de la commune de Tours (Indre-et-Loire) l'autorisation de prendre des clichés de certaines des oeuvres appartenant aux collections du musée des Beaux-Arts de la commune. Le maire a implicitement rejeté cette demande. Par un jugement du 20 janvier 2009, le tribunal administratif d'Orléans a rejeté la demande de l'EURL Photo Josse tendant à l'annulation pour excès de pouvoir du refus du maire et, d'autre part, cette décision implicite. Par un arrêt du 4 mai 2010, faisant droit à l'appel de la société, la cour administrative d'appel de Nantes a annulé, d'une part, ce jugement et, d'autre part, cette décision implicite. Par une décision n° 341173 du 29 octobre 2012, le Conseil d'Etat, statuant au contentieux, a annulé cet arrêt et renvoyé l'affaire à la cour administrative d'appel de Nantes. La cour administrative d'appel de Nantes, statuant sur renvoi du Conseil d'Etat, a rejeté l'appel de la société contre le jugement du 20 janvier 2009. La société Photo J. L. Josse se pourvoit en cassation contre cet arrêt. <br/>
<br/>
              2. En premier lieu, aux termes de l'article L. 2112-1 du code général de la propriété des personnes publiques, en vigueur à la date de la décision implicite du maire : " Sans préjudice des dispositions applicables en matière de protection des biens culturels, font partie du domaine public mobilier de la personne publique propriétaire les biens présentant un intérêt public du point de vue de l'histoire, de l'art, de l'archéologie, de la science ou de la technique, notamment : /(...) 8° Les collections des musées (...) ". <br/>
<br/>
              3. Ainsi qu'il a été jugé par la décision du Conseil d'Etat statuant au contentieux n° 341173 du 29 octobre 2012, la prise de vues d'oeuvres appartenant aux collections d'un musée public, à des fins de commercialisation des reproductions photographiques ainsi obtenues, doit être regardée comme une utilisation privative du domaine public mobilier impliquant la nécessité, pour celui qui entend y procéder, d'obtenir une autorisation ainsi que le prévoit l'article L. 2122-1 du code général de la propriété des personnes publiques. Une telle autorisation peut être délivrée dès lors qu'en vertu de l'article L. 2121-1 de ce code, cette activité demeure compatible avec l'affectation des oeuvres au service public culturel et avec leur conservation. Il est toutefois loisible à la collectivité publique affectataire d'oeuvres relevant de la catégorie des biens mentionnés au 8° de l'article L. 2112-1 du même code, dans le respect du principe d'égalité et sous le contrôle du juge de l'excès de pouvoir, de ne pas autoriser un usage privatif de ce domaine public mobilier. <br/>
<br/>
              4. Il ressort des pièces du dossier soumis aux juges du fond que les motifs avancés par la commune pour justifier la décision implicite de refus opposée, en l'espèce, à la société requérante étaient tirés de ce qu'elle entendait conserver un contrôle sur les conditions dans lesquelles sont établies et diffusées des reproductions photographiques des oeuvres exposées dans le musée et de ce qu'une diffusion excessive de telles reproductions pourrait préjudicier à l'attractivité de ce musée et nuire à sa fréquentation par le public. En jugeant que de tels motifs, qui se rapportent à l'intérêt du domaine public et de son affectation, étaient de nature à fonder légalement cette décision, la cour n'a pas commis d'erreur de droit et n'a pas insuffisamment motivé son arrêt.<br/>
<br/>
              5. Le principe d'égalité ne s'oppose pas à ce que l'autorité administrative règle de façon différente des situations différentes, pourvu que la différence de traitement qui en résulte soit en rapport avec l'objet de la décision qui l'établit et ne soit pas manifestement disproportionnée au regard des motifs susceptibles de la justifier. <br/>
<br/>
              6. D'une part, au regard de l'objet de la mesure en cause, qui concerne une autorisation d'utilisation privative de tout ou partie du domaine public mobilier constitué par les collections d'un musée, les photographes professionnels sollicitant une telle autorisation pour réaliser des clichés des oeuvres à des fins de commercialisation pour leur propre compte des reproductions obtenues ne sont pas placés dans la même situation que les photographes auxquels l'autorité domaniale délivre une telle autorisation afin qu'ils réalisent, à sa demande et pour ses propres besoins, des photographies des oeuvres du musée, sans qu'ait à cet égard d'incidence la circonstance que l'autorisation délivrée à cette seconde catégorie de photographes pourrait avoir, le cas échéant, indirectement pour effet de leur permettre de commercialiser pour leur propre compte les reproductions réalisées. La cour n'a, par suite, ni commis d'erreur de droit, ni insuffisamment motivé son arrêt, en jugeant que les deux catégories de photographes mentionnées ci-dessus pouvaient, sans que soit méconnu le principe d'égalité, faire l'objet d'un traitement différent. <br/>
<br/>
              7. D'autre part, en estimant qu'il n'était pas établi que la commune de Tours aurait accordé récemment à d'autres photographes, se trouvant dans une situation analogue à celle de la société requérante, l'autorisation qu'elle lui avait refusée, la cour n'a pas dénaturé les pièces du dossier qui lui était soumis.<br/>
<br/>
              8. En deuxième lieu, l'illégalité d'un acte administratif, qu'il soit ou non réglementaire, ne peut être utilement invoquée par voie d'exception à l'appui de conclusions dirigées contre une décision administrative ultérieure que si cette dernière décision a été prise pour l'application du premier acte ou s'il en constitue la base légale. La cour a jugé, ce qui n'est pas sérieusement contesté par la société requérante, que la décision implicite dont elle demandait l'annulation trouvait son fondement, non dans les dispositions du règlement intérieur du musée, mais dans les règles découlant des dispositions du code général de la propriété des personnes publiques. Dès lors, la cour n'a pas commis d'erreur de droit en jugeant que la société ne pouvait utilement invoquer le moyen tiré de l'illégalité par voie d'exception du règlement intérieur.<br/>
<br/>
              9. En troisième lieu, comme il a été dit au point 3, la prise de vues d'oeuvres relevant des collections d'un musée, à des fins de commercialisation des reproductions photographiques ainsi obtenues, doit être regardée comme une utilisation privative du domaine public mobilier impliquant la nécessité, pour celui qui entend y procéder, d'obtenir une autorisation. Si la société avait rappelé, dans ses écritures devant la cour postérieures à la décision du Conseil d'Etat n° 341173, que la prise des clichés ne nécessitait pas de déplacer les oeuvres, la cour n'était nullement saisie d'un moyen tiré de ce que, de ce fait, aucune autorisation d'occupation du domaine public n'aurait été nécessaire. Le moyen tiré de ce que la cour aurait dû rechercher si les prises de vues auxquelles souhaitait procéder la société Photo J.L. Josse impliquaient que les oeuvres soient soustraites au public ne peut donc qu'être écarté.<br/>
<br/>
              10. En quatrième lieu, aux termes de l'article L. 123-1 du code de la propriété intellectuelle : " L'auteur jouit, sa vie durant, du droit exclusif d'exploiter son oeuvre sous quelque forme que ce soit et d'en tirer un profit pécuniaire. / Au décès de l'auteur, ce droit persiste au bénéfice de ses ayants droit pendant l'année civile en cours et les soixante-dix années qui suivent ". <br/>
              11. Il était constant devant la cour que les oeuvres que la société requérante souhaitait photographier étaient tombées dans le domaine public, les soixante-dix années mentionnées à l'article L. 123-1 du code de la propriété intellectuelle étant depuis longtemps écoulées. Par suite, le moyen tiré de ce que la cour aurait dû rechercher si la commune était ou non cessionnaire des droits d'auteurs attachés à ces oeuvres ne peut qu'être écarté.<br/>
<br/>
              12. Les dispositions de l'article L. 123-1 du code de la propriété intellectuelle, qui prévoient qu'à l'expiration des soixante-dix années suivant l'année civile du décès de l'auteur d'une oeuvre, il n'existe plus, au profit de ses ayants droit, de droit exclusif d'exploiter cette oeuvre, n'ont ni pour objet, ni pour effet de faire obstacle à l'application à des oeuvres relevant du 8° de l'article L. 2112-1 du code général de la propriété des personnes publiques des règles découlant de ce code, et notamment de celles relatives aux conditions de délivrance d'une autorisation devant être regardée comme tendant à l'utilisation privative de ce domaine public mobilier. Il suit de là qu'en écartant le moyen tiré de ce que, s'agissant des oeuvres énumérées dans la demande de la société Photo J. L. Josse, la période de soixante-dix ans mentionnée à l'article L. 123-1 du code de la propriété intellectuelle étant échue, l'exploitation de ces oeuvres serait libre et gratuite, de sorte que la commune de Tours ne saurait y faire obstacle, sauf à justifier du trouble anormal que cette exploitation pourrait lui causer, la cour n'a pas commis d'erreur de droit.<br/>
<br/>
              13. Il résulte de tout ce qui précède que la société Photo J.L. Josse n'est pas fondée à demander l'annulation de l'arrêt qu'elle attaque.<br/>
<br/>
              14. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de la commune de Tours qui n'est pas, dans la présente instance, la partie perdante. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la société Photo J. L. Josse le versement au même titre à la commune de Tours de la somme de 3 000 euros.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de la société Photo J. L. Josse est rejeté.<br/>
Article 2 : La société Photo J. L. Josse versera à la commune de Tours la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée à la société Photo J. L. Josse et à la commune de Tours.<br/>
Copie en sera adressée au ministre de l'économie et des finances et à la ministre de la culture et de la communication.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
