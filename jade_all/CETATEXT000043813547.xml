<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043813547</ID>
<ANCIEN_ID>JG_L_2021_07_000000450792</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/81/35/CETATEXT000043813547.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème chambre, 19/07/2021, 450792, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-07-19</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>450792</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Olivier Guiard</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emilie Bokdam-Tognetti</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:450792.20210719</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La commission nationale des comptes de campagne et des financements politiques (CNCCFP) a, le 18 décembre 2020, saisi le tribunal administratif de Lille, en application de l'article L. 52-15 du code électoral, sur le fondement de sa décision du 5 novembre 2020 rejetant le compte de campagne de M. A... B..., candidat tête de liste aux élections municipales et communautaires qui se sont déroulées le 15 mars 2020 dans la commune de Bondues (Nord).<br/>
<br/>
              Par un jugement n° 2009148 du 18 février 2021, le tribunal administratif de Lille a, notamment, déclaré M. B... inéligible pour une durée d'un an à compter de la date à laquelle son jugement deviendrait définitif.<br/>
<br/>
              Par une requête enregistrée le 18 mars 2021 au secrétariat du contentieux du Conseil d'Etat, le préfet du Nord demande au Conseil d'Etat d'annuler ce jugement en tant qu'il ne déclare pas M. B... démissionnaire d'office de son mandat de conseiller municipal et qu'il ne prononce pas élu M. D... C....<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code électoral ;<br/>
              - le code de justice administrative.<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Olivier Guiard, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Emilie Bokdam-Tognetti, rapporteure publique ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. À l'issue des opérations électorales qui se sont déroulées le 15 mars 2020 dans la commune de Bondues (Nord), la liste " Bien vivre à Bondues " conduite par M. A... B... a recueilli 413 voix et obtenu deux sièges au conseil municipal. Par une décision du 5 novembre 2020, la Commission nationale des comptes de campagne et des financements politiques (CNCCFP) a rejeté le compte de campagne déposé par M. B..., candidat tête de liste, puis a saisi le tribunal administratif de Lille sur le fondement de l'article L. 52-15 du code électoral. Par un jugement du 18 février 2021, le tribunal administratif de Lille a, d'une part, jugé que le compte de campagne déposé par M. B... avait été rejeté à bon droit par la CNCCFP, d'autre part, confirmé que M. B... n'avait pas droit au remboursement forfaitaire de ses dépenses électorales, enfin déclaré M. B... inéligible pour une durée d'un an à compter de la date à laquelle sa décision deviendrait définitive. Le préfet du Nord relève appel de ce jugement en tant qu'il ne déclare pas M. B..., en outre, démissionnaire d'office de son mandat de conseiller municipal, ni ne proclame M. D... C... élu.  <br/>
<br/>
              2. D'une part, aux termes du dernier alinéa de l'article L. 118-3 du code électoral : " Si le juge de l'élection a prononcé l'inéligibilité d'un candidat ou des membres d'un binôme proclamé élu, il annule son élection ou, si l'élection n'a pas été contestée, déclare le candidat ou les membres du binôme démissionnaires d'office. " D'autre part, selon le premier alinéa de l'article L. 270 du même code : " (...) La constatation, par la juridiction administrative, de l'inéligibilité d'un ou plusieurs candidats n'entraîne l'annulation de l'élection que du ou des élus inéligibles. La juridiction saisie proclame en conséquence l'élection du ou des suivants de liste. " Il résulte de ces dispositions que lorsqu'un candidat proclamé élu est déclaré inéligible en application de l'article L. 118-3 du code électoral, il appartient au juge, d'une part, d'annuler son élection ou, si cette élection n'a pas été contestée, de le déclarer démissionnaire d'office, et, d'autre part, de proclamer élu le candidat suivant, non encore élu, de la liste dont il était membre. <br/>
<br/>
              3. Par le jugement attaqué du 18 février 2021, après avoir relevé que le compte de campagne déposé par M. B... n'avait pas été présenté par un membre de l'ordre des experts-comptables et qu'il présentait de graves irrégularités, le tribunal administratif de Lille a prononcé l'inéligibilité de l'intéressé pour une durée d'un an mais a omis de le déclarer démissionnaire d'office de son mandat de conseiller municipal de la commune de Bondues et, par ailleurs, de proclamer élu M. D... C..., premier candidat non élu de la liste " Bien vivre à Bondues ". Ce faisant le tribunal a méconnu la règle applicable même sans texte à toute juridiction administrative, qui lui impartit, sauf dans le cas où un incident de procédure y ferait obstacle, d'épuiser son pouvoir juridictionnel. Par suite, il y a lieu d'annuler le jugement attaqué dans cette mesure et, dès lors que le délai imparti par l'article R. 120 du code électoral au tribunal administratif pour statuer est expiré, de statuer immédiatement sur les conséquences de la déclaration d'inéligibilité de M. B.... <br/>
<br/>
              4. Dès lors que le jugement du tribunal administratif de Lille du 18 février 2021 déclare M. B... inéligible, celui-ci doit également être déclaré démissionnaire d'office de son mandat de conseiller municipal. Il y a lieu, par suite, de proclamer M. D... C... élu en qualité de conseiller municipal de la commune de Bondues. <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : Le jugement du 18 février 2021 du tribunal administratif de Lille est annulé en tant qu'il ne statue pas sur l'appartenance de M. B... au conseil municipal de la commune de Bondues.<br/>
Article 2 : M. B... est déclaré démissionnaire d'office de ses fonctions de conseiller municipal de la commune de Bondues. <br/>
Article 3 : M. D... C... est proclamé élu en qualité de conseiller municipal de la commune de Bondues.<br/>
Article 4 : La présente décision sera notifiée au préfet du Nord, à M. A... B... et à M. D... C.... <br/>
Copie en sera adressée à la Commission nationale des comptes de campagne et des financements politiques, à la commune de Bondues et au ministre de l'intérieur.  <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
