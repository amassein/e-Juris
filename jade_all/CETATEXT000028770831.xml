<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028770831</ID>
<ANCIEN_ID>JG_L_2014_03_000000363902</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/77/08/CETATEXT000028770831.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème et 1ère sous-sections réunies, 24/03/2014, 363902, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-03-24</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>363902</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème et 1ère sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Clémence Olsina</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Suzanne von Coester</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:363902.20140324</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 14 novembre 2012 au secrétariat du contentieux du Conseil d'Etat, présentée par M. A...B..., demeurant ... ; M. B...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision de la commission d'avancement prévue à l'article 34 de l'ordonnance du 22 décembre 1958 portant loi organique relative au statut de la magistrature, en ce qu'elle émet un avis défavorable à sa candidature à l'intégration directe dans le corps judiciaire ; <br/>
<br/>
              2°) d'enjoindre à la commission d'avancement de procéder à un nouvel examen de sa candidature dans un délai de trois mois à compter de la notification de la décision à intervenir ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative ainsi que la contribution pour l'aide juridique prévue à l'article R. 761-1 du même code ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 4 mars 2014, présentée par M. B...; <br/>
<br/>
              Vu l'ordonnance n° 58-1270 du 22 décembre 1958 ;<br/>
<br/>
              Vu la loi n° 2000-321 du 12 avril 2000 ;<br/>
<br/>
              Vu le décret n° 93-21 du 7 janvier 1993 ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Clémence Olsina, auditeur,  <br/>
<br/>
              - les conclusions de Mme Suzanne von Coester, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article 22 de l'ordonnance du 22 décembre 1958 portant loi organique relative au statut de la magistrature : " Peuvent être nommées directement aux fonctions du second grade de la hiérarchie judiciaire, à condition d'être âgés de trente-cinq ans au moins : / 1° les personnes remplissant les conditions prévues à l'article 16 et justifiant de sept années au moins d'exercice professionnel les qualifiant particulièrement pour exercer des fonctions judiciaires (...) " ; qu'en vertu de l'article 25-2 de la même ordonnance, ces nominations interviennent après avis conforme de la commission d'avancement prévue à l'article 34 ;<br/>
<br/>
              2. Considérant que, sur le fondement de ces dispositions, M. B...a déposé une demande d'intégration dans le corps des magistrats judiciaires ; que, par une décision notifiée le 14 septembre 2012, la commission d'avancement, réunie les 8, 11, 12, 13 et 14 juin 2012, a émis un avis défavorable à sa candidature ; que M. B...demande l'annulation pour excès de pouvoir de cette décision ; <br/>
<br/>
              3. Considérant, en premier lieu, que, s'agissant d'un organisme collégial tel que la commission d'avancement, il est satisfait aux exigences découlant des prescriptions de l'article 4 de la loi du 12 avril 2000 dès lors que la décision prise comporte la signature de son président, accompagnée des mentions prévues par cet article ; qu'en vertu de l'article 35 de l'ordonnance du 22 décembre 1958, la commission d'avancement est présidée par le doyen des présidents de chambre de la Cour de cassation ; qu'il ressort des pièces du dossier que le procès-verbal de la réunion de la commission d'avancement des 8, 11, 12, 13 et 14 juin 2012 comporte les prénom, nom et signature de ce magistrat, qui présidait cette réunion ; que, par suite, le moyen tiré de la méconnaissance de l'article 4 de la loi du 12 avril 2000 doit être écarté ;<br/>
<br/>
              4. Considérant, en deuxième lieu, qu'il ressort de l'avis du premier président et du procureur général près la cour d'appel de Caen versé au dossier de candidature de M. B..., exprimé dans une lettre du 22 mars 2012 adressée au garde des sceaux, que l'intéressé a eu un entretien préalable avec leurs représentants ; qu'aucun texte ne prévoit que le compte rendu détaillé de cet entretien devrait être versé au dossier de candidature transmis à la commission d'avancement ; que, dès lors, le requérant n'est pas fondé à soutenir que la décision attaquée aurait été prise au terme d'une procédure irrégulière, faute de l'avoir été sur la base d'un dossier contenant le compte rendu de cet entretien ; <br/>
<br/>
              5. Considérant, en troisième lieu, qu'il ne ressort pas des pièces du dossier que la commission d'avancement se serait crue liée par le contenu de l'avis rendu par le premier président et par le procureur général près la cour d'appel de Caen dans leur lettre du 22 mars 2012, ni qu'elle aurait décidé de se l'approprier ; que, dès lors, le moyen tiré de ce que la décision attaquée serait, compte tenu de cet avis,entachée d'une erreur de droit et d'une erreur manifeste d'appréciation ne peut, en tout état de cause, qu'être écarté ; <br/>
<br/>
              6. Considérant, en quatrième lieu, que les dispositions de l'ordonnance du 22 décembre 1958 citées au point 1 ne créent aucun droit à l'intégration directe dans le corps des magistrats judiciaires au profit des personnes remplissant les conditions qu'elles énumèrent ; que si l'intéressé fait valoir qu'il est titulaire d'un doctorat en droit ainsi que d'un certificat d'aptitude à la profession d'avocat, qu'il bénéficie depuis 2001 d'une large expérience professionnelle dans le domaine juridique et que plusieurs documents attestent qu'il possède les qualités lui permettant de postuler aux fonctions de magistrat, les circonstances ainsi alléguées ne sont pas de nature à établir que la commission d'avancement, que le législateur organique a entendu investir d'un large pouvoir d'appréciation de l'aptitude des candidats à exercer les fonctions de magistrat, aurait elle-même commis une erreur manifeste d'appréciation en émettant un avis défavorable à sa candidature ;<br/>
<br/>
              7. Considérant, en dernier lieu, que si, lorsque qu'il est soutenu qu'une mesure a pu être empreinte de discrimination, c'est au défendeur qu'il incombe de produire tous les éléments permettant d'établir que la décision attaquée repose sur des éléments objectifs étrangers à toute discrimination, il appartient au requérant qui s'estime lésé par une telle mesure de soumettre au juge des éléments de fait susceptibles de faire présumer une atteinte au principe de non discrimination ; qu'il ressort des pièces du dossier qu'en l'espèce, le requérant n'apporte pas d'élément permettant de faire présumer que la mesure qu'il attaque procéderait, comme il l'allègue, d'une pratique discriminatoire en raison de son appartenance syndicale ; que le moyen tiré de ce que la décision de la commission d'avancement procéderait d'une telle discrimination doit dès lors être écarté ;<br/>
<br/>
              8. Considérant qu'il résulte de tout ce qui précède que M. B...n'est pas fondé à demander l'annulation de la décision qu'il attaque ; que, par suite, ses conclusions présentées au titre des dispositions des articles L. 761-1 et L. 911-1 du code de justice administrative ne peuvent qu'être rejetées ; qu'enfin, il y a lieu de laisser à la charge de M. B... la contribution pour l'aide juridique prévue à l'article R. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : La requête de M. B...est rejetée. <br/>
<br/>
Article 2 : La présente décision sera notifiée à M. A...B...et à la garde des sceaux, ministre de la justice.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
