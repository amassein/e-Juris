<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027689953</ID>
<ANCIEN_ID>JG_L_2013_07_000000362304</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/68/99/CETATEXT000027689953.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème / 2ème SSR, 10/07/2013, 362304</TITRE>
<DATE_DEC>2013-07-10</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>362304</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème / 2ème SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BARTHELEMY, MATUCHANSKY, VEXLIARD ; SCP WAQUET, FARGE, HAZAN</AVOCATS>
<RAPPORTEUR>Mme Laurence Marion</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Gilles Pellissier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:XX:2013:362304.20130710</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu 1°, sous le n° 362304, le pourvoi sommaire et le mémoire complémentaire, enregistrés les 29 août et 29 novembre 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour la commune de Vias, représentée par son maire ; la commune de Vias demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler l'arrêt n° 09MA03176 du 28 juin 2012 de la cour administrative d'appel de Marseille en tant que, à la demande de la société d'économie mixte de la ville de Béziers et du Littoral (SEBLI), après annulation de l'article 1er du jugement n° 0702459 du 12 juin 2009 du tribunal administratif de Montpellier, la cour n'a annulé l'avenant n° 5 à la concession d'aménagement du 24 juin 1988 conclu le 21 octobre 2001 entre la commune de Vias et la SEBLI, ainsi que l'avenant n° 1 à ce dernier, conclu le 16 janvier 2004, qu'à compter du 26 mai 2005 ; <br/>
<br/>
              2°) de mettre à la charge de la SEBLI une somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu 2°, sous le n° 362318, le pourvoi sommaire et le mémoire complémentaire, enregistrés les 29 août et 29 novembre 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour la société d'économie mixte de la ville de Béziers et du Littoral (SEBLI), dont le siège est 15, place Jean Jaurès à Béziers (34500) ; la SEBLI demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 09MA03176 du 28 juin 2012 par lequel la cour administrative d'appel de Marseille, après annulation de l'article 1er du jugement n° 0702459 du 12 juin 2009 du tribunal administratif de Montpellier, a annulé l'avenant n° 5 à la concession d'aménagement du 24 juin 1988 et l'avenant n° 1 à celui-ci, du 16 janvier 2004, à compter du 26 mai 2005 ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ; <br/>
<br/>
              3°) de mettre à la charge de la commune de Vias la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu le code de l'urbanisme ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Laurence Marion, Maître des Requêtes,  <br/>
<br/>
              - les conclusions de M. Gilles Pellissier, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Barthélemy, Matuchansky, Vexliard, avocat de la commune de Vias, à la SCP Waquet, Farge, Hazan, avocat de la société d'économie mixte de la ville de Béziers et du Littoral (SEBLI) ;<br/>
<br/>
<br/>
<br/>1. Considérant que les pourvois présentés par la commune de Vias et la société d'économie mixte de la Ville de Béziers et du littoral sont dirigés contre le même arrêt de la cour administrative d'appel de Marseille ; qu'il y a lieu de les joindre pour statuer par une même décision ;<br/>
<br/>
              2. Considérant que les parties à un contrat administratif peuvent saisir le juge d'un recours de plein contentieux contestant la validité du contrat qui les lie ; qu'il appartient alors au juge, lorsqu'il constate l'existence d'irrégularités, d'en apprécier l'importance et les conséquences, après avoir vérifié que les irrégularités dont se prévalent les parties sont de celles qu'elles peuvent, eu égard à l'exigence de loyauté des relations contractuelles, invoquer devant lui ; qu'il lui revient, après avoir pris en considération la nature de l'illégalité commise et en tenant compte de l'objectif de stabilité des relations contractuelles, soit de décider que la poursuite de l'exécution du contrat est possible, éventuellement sous réserve de mesures de régularisation prises par la personne publique ou convenues entre les parties, soit de prononcer, le cas échéant avec un effet différé, après avoir vérifié que sa décision ne portera pas une atteinte excessive à l'intérêt général, la résiliation du contrat ou, en raison seulement d'une irrégularité invoquée par une partie ou relevée d'office par lui, tenant au caractère illicite du contenu du contrat ou à un vice d'une particulière gravité relatif notamment aux conditions dans lesquelles les parties ont donné leur consentement, son annulation ;<br/>
<br/>
              Sur le pourvoi n° 362318 présenté par la SEBLI : <br/>
<br/>
              3. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, par un jugement du 26 mai 2005, devenu définitif, le tribunal administratif de Montpellier a jugé que l'opération d'aménagement de la zone d'aménagement concerté (ZAC) de " Vias plage ", située sur le territoire de la commune de Vias dans une zone comprise entre 100 mètres et 600 mètres du rivage, qui prévoyait la réalisation de plus de 2 300 logements ainsi que des commerces et autres activés de service, ne constituait pas une extension limitée de l'urbanisation au sens de l'article L. 146-4 II du code de l'urbanisme, seule autorisée dans une telle zone littorale, et a notamment annulé la délibération du 30 août 2001 autorisant le maire à signer une convention publique d'aménagement avec la société d'économie mixte de la ville de Béziers et du littoral, par avenant à la concession d'aménagement conclue le 24 juin 1988, pour la réalisation de cette opération ; que, par l'article 1er de son jugement du 12 juin 2009, le même tribunal a annulé la convention publique d'aménagement, conclue le 21 octobre 2001, ainsi que son premier avenant, conclu le 16 janvier 2004 ; que, par un arrêt du 28 juin 2012, la cour administrative d'appel de Marseille, après avoir annulé l'article 1er du jugement du tribunal administratif, a annulé la convention et son avenant et décidé que cette annulation prendrait effet au 26 mai 2005 ;<br/>
<br/>
              4. Considérant, en premier lieu, que la cour administrative d'appel de Marseille n'a pas commis d'erreur de droit en déduisant de l'illégalité de l'opération d'aménagement de la ZAC de " Vias plage " que l'objet de la convention confiant à la SEBLI l'aménagement de cette zone d'activité était illicite et qu'une telle irrégularité du contrat était susceptible de conduire le juge à en prononcer l'annulation ;<br/>
<br/>
              5. Considérant, en deuxième lieu, que, contrairement à ce que soutient la SEBLI, la cour a pu, sans dénaturer les faits, estimer que l'exigence de loyauté des relations contractuelles ne faisait pas obstacle, dans les circonstances de l'espèce qui lui était soumise, à ce que l'illicéité du contrat litigieux soit invoquée par la commune de Vias ; <br/>
<br/>
              6. Considérant, en troisième lieu, qu'en jugeant que l'objectif de stabilité des relations contractuelles ne faisait pas obstacle, dans les circonstances de l'espèce qui lui était soumise, à l'annulation de la convention litigieuse et en décidant l'annulation de la convention, et non sa résiliation, la cour n'a entaché son arrêt d'aucune erreur de qualification juridique des faits ; <br/>
<br/>
              7. Considérant qu'il résulte de ce qui précède que la SEBLI n'est pas fondée à demander l'annulation de l'arrêt attaqué, qui a répondu à l'ensemble des moyens soulevés par une motivation suffisante, en tant qu'il a annulé la convention conclue le 21 octobre 2001 ainsi que son avenant du 16 janvier 2004 ;<br/>
<br/>
<br/>
<br/>
              Sur le pourvoi n° 362304 de la commune de Vias : <br/>
<br/>
              8. Considérant qu'en décidant de ne faire prendre effet à sa décision d'annulation qu'à compter du 26 mai 2005, date du jugement du tribunal administratif de Montpellier mentionné au point 3, la cour a méconnu son office et commis une erreur de droit ; que, par suite, la commune de Vias est fondée, sans qu'il soit besoin d'examiner l'autre moyen de son pourvoi, à demander l'annulation de l'arrêt attaqué en tant qu'il n'a annulé la convention litigieuse et son avenant qu'à compter du 26 mai 2005 ;<br/>
<br/>
              9. Considérant qu'aucune question ne reste à juger ; qu'il n'y a lieu dès lors, ni de statuer au fond en application de l'article L. 821-2 du code de justice administrative, ni de renvoyer l'affaire ; <br/>
<br/>
              10. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mise à la charge de commune de Vias, qui n'est pas la partie perdante dans la présente instance, la somme que réclame la SEBLI au titre des frais exposés par elle et non compris dans les dépens ; qu'il y a lieu, en revanche, dans les circonstances de l'espèce, de mettre à la charge de la SEBLI une somme de 3 000 euros à verser à la commune de Vias au titre des mêmes dispositions ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Marseille du 28 juin 2012 est annulé en tant qu'il n'a annulé la convention du 21 octobre 2001 et son avenant n° 1 conclu le 16 janvier 2004, qu'à compter du 26 mai 2005.<br/>
<br/>
Article 2 : Le pourvoi de la société d'économie mixte de la ville de Béziers et du littoral est rejeté.<br/>
<br/>
Article 3 : La société d'économie mixte de la ville de Béziers et du littoral versera une somme de 3 000 euros à la commune de Vias au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : Les conclusions présentées au titre de l'article L. 761-1 par la société d'économie mixte de la ville de Béziers et du littoral sont rejetées.<br/>
<br/>
Article 5 : La présente décision sera notifiée à la société d'économie mixte de la ville de Béziers et du Littoral et à la commune de Vias.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">39-02-04 MARCHÉS ET CONTRATS ADMINISTRATIFS. FORMATION DES CONTRATS ET MARCHÉS. CONTENU. - OBJET DU CONTRAT - RÉALISATION D'UNE OPÉRATION D'AMÉNAGEMENT ILLÉGALE - CARACTÈRE ILLICITE - EXISTENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">39-08-01 MARCHÉS ET CONTRATS ADMINISTRATIFS. RÈGLES DE PROCÉDURE CONTENTIEUSE SPÉCIALES. RECEVABILITÉ. - IRRÉGULARITÉS DONT UNE PARTIE PEUT SE PRÉVALOIR DANS LE CADRE D'UN RECOURS EN VALIDITÉ DU CONTRAT - APPRÉCIATION DU JUGE REQUISE AU REGARD DE L'EXIGENCE DE LOYAUTÉ DES RELATIONS CONTRACTUELLES [RJ1] - CONTRÔLE DU JUGE DE CASSATION - DÉNATURATION.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">39-08-03-02 MARCHÉS ET CONTRATS ADMINISTRATIFS. RÈGLES DE PROCÉDURE CONTENTIEUSE SPÉCIALES. POUVOIRS ET OBLIGATIONS DU JUGE. POUVOIRS DU JUGE DU CONTRAT. - JUGE DE PLEIN CONTENTIEUX SAISI PAR UNE PARTIE À UN CONTRAT D'UN RECOURS EN VALIDITÉ DE CE CONTRAT [RJ1] - 1) IRRÉGULARITÉS DONT UNE PARTIE PEUT SE PRÉVALOIR DANS LE CADRE D'UN RECOURS EN VALIDITÉ DU CONTRAT - APPRÉCIATION DU JUGE REQUISE AU REGARD DE L'EXIGENCE DE LOYAUTÉ DES RELATIONS CONTRACTUELLES - CONTRÔLE DU JUGE DE CASSATION - DÉNATURATION - 2) POUVOIRS ET DEVOIRS DU JUGE FACE À UNE IRRÉGULARITÉ - APPRÉCIATION DU JUGE SUR LES CONSÉQUENCES À TIRER SUR LE CONTRAT DES IRRÉGULARITÉS CONSTATÉES - CONTRÔLE DU JUGE DE CASSATION - CONTRÔLE DE LA QUALIFICATION JURIDIQUE.
</SCT>
<SCT ID="8D" TYPE="PRINCIPAL">54-08-02-02-01-02 PROCÉDURE. VOIES DE RECOURS. CASSATION. CONTRÔLE DU JUGE DE CASSATION. BIEN-FONDÉ. QUALIFICATION JURIDIQUE DES FAITS. - JUGE DE PLEIN CONTENTIEUX SAISI PAR UNE PARTIE À UN CONTRAT D'UN RECOURS EN VALIDITÉ DE CE CONTRAT - POUVOIRS ET DEVOIRS DU JUGE FACE À UNE IRRÉGULARITÉ - APPRÉCIATION DU JUGE REQUISE AU REGARD DE L'OBJECTIF DE STABILITÉ DES RELATIONS CONTRACTUELLES [RJ1].
</SCT>
<SCT ID="8E" TYPE="PRINCIPAL">54-08-02-02-01-03 PROCÉDURE. VOIES DE RECOURS. CASSATION. CONTRÔLE DU JUGE DE CASSATION. BIEN-FONDÉ. APPRÉCIATION SOUVERAINE DES JUGES DU FOND. - IRRÉGULARITÉS DONT UNE PARTIE PEUT SE PRÉVALOIR DANS LE CADRE D'UN RECOURS EN VALIDITÉ DU CONTRAT - APPRÉCIATION DU JUGE REQUISE AU REGARD DE L'EXIGENCE DE LOYAUTÉ DES RELATIONS CONTRACTUELLES [RJ1].
</SCT>
<SCT ID="8F" TYPE="PRINCIPAL">54-08-02-02-01-04 PROCÉDURE. VOIES DE RECOURS. CASSATION. CONTRÔLE DU JUGE DE CASSATION. BIEN-FONDÉ. DÉNATURATION. - IRRÉGULARITÉS DONT UNE PARTIE PEUT SE PRÉVALOIR DANS LE CADRE D'UN RECOURS EN VALIDITÉ DU CONTRAT - APPRÉCIATION DU JUGE REQUISE AU REGARD DE L'EXIGENCE DE LOYAUTÉ DES RELATIONS CONTRACTUELLES [RJ1].
</SCT>
<ANA ID="9A"> 39-02-04 L'illégalité d'une opération d'aménagement (en l'espèce, au regard du II de l'article L. 146-4 II du code de l'urbanisme) confère un caractère illicite à l'objet de la convention confiant à l'aménageur la réalisation de cette opération.</ANA>
<ANA ID="9B"> 39-08-01 Lorsqu'une partie à un contrat administratif saisit le juge du contrat d'un recours de plein contentieux pour en contester la validité, il revient à ce juge de vérifier que les irrégularités dont se prévaut cette partie sont de celles qu'elle peut, eu égard à l'exigence de loyauté des relations contractuelles, invoquer devant lui. Cette vérification relève de son appréciation souveraine, susceptible seulement d'un contrôle de dénaturation par le juge de cassation.</ANA>
<ANA ID="9C"> 39-08-03-02 1) Lorsqu'une partie à un contrat administratif saisit le juge du contrat d'un recours de plein contentieux pour en contester la validité, il revient à ce juge de vérifier que les irrégularités dont se prévaut cette partie sont de celles qu'elle peut, eu égard à l'exigence de loyauté des relations contractuelles, invoquer devant lui. Cette vérification relève de son appréciation souveraine, susceptible seulement d'un contrôle de dénaturation par le juge de cassation.,,,2) Il appartient alors au juge, lorsqu'il constate l'existence d'irrégularités, d'en apprécier l'importance et les conséquences et il lui revient, après avoir pris en considération la nature de l'illégalité commise et en tenant compte de l'objectif de stabilité des relations contractuelles, soit de décider que la poursuite de l'exécution du contrat est possible, éventuellement sous réserve de mesures de régularisation prises par la personne publique ou convenues entre les parties, soit de prononcer, le cas échéant avec un effet différé, après avoir vérifié que sa décision ne portera pas une atteinte excessive à l'intérêt général, la résiliation du contrat ou, en raison seulement d'une irrégularité invoquée par une partie ou relevée d'office par lui, tenant au caractère illicite du contenu du contrat ou à un vice d'une particulière gravité relatif notamment aux conditions dans lesquelles les parties ont donné leur consentement, son annulation. L'appréciation des conséquences qu'il convient de tirer des irrégularités sur le contrat compte tenu de l'objectif de stabilité des relations contractuelles est soumise à un contrôle de la qualification juridique par le juge de cassation.</ANA>
<ANA ID="9D"> 54-08-02-02-01-02 Lorsqu'une partie à un contrat administratif saisit le juge du contrat d'un recours de plein contentieux pour en contester la validité, il appartient au juge, lorsqu'il constate l'existence d'irrégularités, d'en apprécier l'importance et les conséquences, après avoir vérifié que les irrégularités dont se prévalent les parties sont de celles qu'elles peuvent, eu égard à l'exigence de loyauté des relations contractuelles, invoquer devant lui. Il lui revient alors, après avoir pris en considération la nature de l'illégalité commise et en tenant compte de l'objectif de stabilité des relations contractuelles, soit de décider que la poursuite de l'exécution du contrat est possible, éventuellement sous réserve de mesures de régularisation prises par la personne publique ou convenues entre les parties, soit de prononcer, le cas échéant avec un effet différé, après avoir vérifié que sa décision ne portera pas une atteinte excessive à l'intérêt général, la résiliation du contrat ou, en raison seulement d'une irrégularité invoquée par une partie ou relevée d'office par lui, tenant au caractère illicite du contenu du contrat ou à un vice d'une particulière gravité relatif notamment aux conditions dans lesquelles les parties ont donné leur consentement, son annulation. L'appréciation des conséquences qu'il convient de tirer des irrégularités sur le contrat compte tenu de l'objectif de stabilité des relations contractuelles est soumise à un contrôle de la qualification juridique par le juge de cassation.</ANA>
<ANA ID="9E"> 54-08-02-02-01-03 Lorsqu'une partie à un contrat administratif saisit le juge du contrat d'un recours de plein contentieux pour en contester la validité, il revient à ce juge de vérifier que les irrégularités dont se prévaut cette partie sont de celles qu'elle peut, eu égard à l'exigence de loyauté des relations contractuelles, invoquer devant lui. Cette vérification relève de son appréciation souveraine, susceptible seulement d'un contrôle de dénaturation par le juge de cassation.</ANA>
<ANA ID="9F"> 54-08-02-02-01-04 Lorsqu'une partie à un contrat administratif saisit le juge du contrat d'un recours de plein contentieux pour en contester la validité, il revient à ce juge de vérifier que les irrégularités dont se prévaut cette partie sont de celles qu'elle peut, eu égard à l'exigence de loyauté des relations contractuelles, invoquer devant lui. Cette vérification relève de son appréciation souveraine, susceptible seulement d'un contrôle de dénaturation par le juge de cassation.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, Assemblée, 28 décembre 2009, Commune de Béziers, n° 304802, p. 509.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
