<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038466947</ID>
<ANCIEN_ID>JG_L_2019_05_000000417190</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/46/69/CETATEXT000038466947.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème et 6ème chambres réunies, 13/05/2019, 417190</TITRE>
<DATE_DEC>2019-05-13</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>417190</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème et 6ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LEDUC, VIGAND</AVOCATS>
<RAPPORTEUR>M. Alain Seban</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Cécile Barrois de Sarigny</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:417190.20190513</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A...B...a demandé au tribunal administratif de Melun d'annuler pour excès de pouvoir la décision du 24 mars 2016 par laquelle la commission de médiation du Val-de-Marne a rejeté sa requête tendant à ce que sa demande de logement soit reconnue prioritaire et urgente, ainsi que la décision du 4 août 2016 par laquelle la même commission a rejeté son recours gracieux, et d'enjoindre à cette commission de médiation de reconnaître le caractère prioritaire et urgent de sa demande de logement. Par un jugement n° 1607567 du 5 octobre 2017, le tribunal administratif a rejeté cette demande.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés au secrétariat du contentieux du Conseil d'Etat les 10 janvier et 10 avril 2018, M. B...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande de première instance ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat une somme de 3 500 euros à verser à la SCP Leduc, Vigand, son avocat, au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
<br/>
              - le code de la construction et de l'habitation ;<br/>
<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Alain Seban, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Cécile Barrois de Sarigny, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Leduc, Vigand, avocat de M.B... ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis au tribunal administratif que M. B..., arrivé en France en 2000 et admis au statut de réfugié, a déposé une demande de logement social le 5 février 2003, qui est restée infructueuse. Le 10 février 2016, M. B...a présenté à la commission départementale de médiation du Val-de-Marne une demande au titre du droit au logement opposable. Celle-ci a été rejetée par une décision du 24 mars 2016, confirmée sur recours gracieux par une décision du 4 août 2016 suivant. L'intéressé a demandé l'annulation pour excès de pouvoir de ces deux décisions au tribunal administratif de Melun qui, par un jugement du 5 octobre 2017, a rejeté sa demande. Il se pourvoit en cassation contre ce jugement.<br/>
<br/>
              2. Aux termes du II de l'article L. 441-2-3 du code de la construction et de l'habitation : " La commission de médiation peut être saisie par toute personne qui, satisfaisant aux conditions réglementaires d'accès à un logement locatif social, n'a reçu aucune proposition adaptée en réponse à sa demande de logement dans le délai fixé en application de l'article L. 441-1-4. / Elle peut être saisie sans condition de délai lorsque le demandeur, de bonne foi, est dépourvu de logement, menacé d'expulsion sans relogement, hébergé ou logé temporairement dans un établissement ou un logement de transition, un logement-foyer ou une résidence hôtelière à vocation sociale, logé dans des locaux impropres à l'habitation ou présentant un caractère insalubre ou dangereux (...) ". L'article R. 441-14-1 du même code dispose que : " Peuvent être désignées par la commission comme prioritaires et devant être logées d'urgence en application du II de l'article L. 441-2-3 les personnes de bonne foi qui satisfont aux conditions réglementaires d'accès au logement social qui se trouvent dans l'une des situations prévues au même article et qui répondent aux caractéristiques suivantes : (...) avoir fait l'objet d'une décision de justice prononçant l'expulsion du logement (...) ". Il résulte de ces dispositions que pour être désigné comme prioritaire et devant se voir attribuer d'urgence un logement social, le demandeur doit être de bonne foi, satisfaire aux conditions réglementaires d'accès au logement social et justifier qu'il se trouve dans une des situations prévues au II de l'article L. 441-2-3 du code de la construction et de l'habitation et qu'il satisfait à un des critères définis à l'article R. 441-14-1 de ce code. Il appartient au juge de l'excès de pouvoir d'exercer un entier contrôle sur l'appréciation portée la commission de médiation quant à la bonne foi du demandeur. L'appréciation ainsi portée par le juge de l'excès de pouvoir relève du pouvoir souverain des juges du fond et ne peut, dès lors qu'elle est exempte de dénaturation, être discutée devant le juge de cassation. <br/>
<br/>
              3. En l'espèce, le tribunal administratif s'est borné à rechercher si la commission de médiation n'avait pas commis d'erreur manifeste d'appréciation en estimant que M. B...n'était pas de bonne foi. En statuant ainsi, alors qu'il lui appartenait, ainsi qu'il vient d'être dit, d'exercer un entier contrôle sur ce point, le tribunal administratif a commis une erreur de droit. Il en résulte que son jugement doit être annulé pour ce motif, sans qu'il soit besoin d'examiner les autres moyens du pourvoi.<br/>
<br/>
              4. Il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application de l'article L. 821-2 du code de justice administrative. <br/>
<br/>
              5. Ne peut être regardé comme de bonne foi, au sens de l'article L. 441-2-3 du code de la construction et de l'habitation, le demandeur qui a délibérément créé par son comportement la situation rendant son relogement nécessaire. <br/>
<br/>
              6. Il ne ressort pas des pièces du dossier que M.B..., locataire dans le parc privé, qui a certes laissé s'accumuler d'importants retards de loyers à partir de son licenciement, alors qu'il avait pour seule ressource le revenu de solidarité active pour un montant inférieur à celui du loyer, et qui n'a pas été en mesure d'honorer le plan d'apurement de cette dette conclu avec son propriétaire, ait cherché délibérément à échapper à ses obligations de locataire et créé ainsi la situation qui a conduit à une mesure judiciaire d'expulsion rendant son relogement nécessaire. Par suite, en estimant qu'il ne pouvait être regardé comme un demandeur de bonne foi au sens du deuxième alinéa du II de l'article L. 441-2-3 du code de la construction et de l'habitation, la commission de médiation a entaché sa décision d'une erreur d'appréciation. M. B...est fondé à demander, pour ce motif, l'annulation de la décision qu'il attaque. <br/>
<br/>
              7. La présente décision impliquant seulement que la commission départementale de médiation réexamine la situation de l'intéressé, il y a lieu d'émettre une injonction en ce sens.<br/>
<br/>
              8. Il y a lieu, dans les circonstances de l'espèce, de faire application des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 et de mettre à la charge de l'Etat une somme de 2 000 euros à verser à la SCP Leduc, Vigan, avocat de M.B..., sous réserve que cette société renonce à percevoir la somme correspondant à la part contributive de l'Etat.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du tribunal administratif de Melun est annulé.<br/>
Article 2 : La décision de la commission départementale de médiation du Val-de-Marne du 24 mars 2016 et la décision du 4 août 2016 rejetant le recours gracieux formé par M. B... sont annulées.<br/>
Article 3 : Il est enjoint à la commission départementale de médiation du Val-de-Marne de réexaminer la situation de M. B...dans un délai d'un mois à compter de la notification de la présente décision.<br/>
Article 4 : L'Etat versera à la SCP Leduc, Vigan, avocat au Conseil d'Etat et à la Cour de cassation, une somme de 2 000 euros au titre des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 sous réserve que cette société renonce à percevoir la somme correspondant à la part contributive de l'Etat.<br/>
Article 5 : La présente décision sera notifiée à M. A...B..., à la commission départementale de médiation du Val-de-Marne et à la ministre de la cohésion des territoires et des relations avec les collectivités territoriales.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">38-07-01 LOGEMENT. - CONDITION DE BONNE FOI DU DEMANDEUR (II DE L'ART. L. 441-2-3 ET ART. R. 441-14-1 DU CCH) - 1) CONTRÔLE DU JUGE DE L'EXCÈS DE POUVOIR SUR L'APPRÉCIATION DE CETTE CONDITION PAR LA COMMISSION DE MÉDIATION - CONTRÔLE NORMAL - 2) CONTRÔLE DU JUGE DE CASSATION - APPRÉCIATION SOUVERAINE DES JUGES DU FOND - 3) NOTION DE BONNE FOI - A) DÉFINITION [RJ1] - B) ILLUSTRATION EN L'ESPÈCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-07-02-03 PROCÉDURE. POUVOIRS ET DEVOIRS DU JUGE. CONTRÔLE DU JUGE DE L'EXCÈS DE POUVOIR. APPRÉCIATIONS SOUMISES À UN CONTRÔLE NORMAL. - BONNE FOI DU DEMANDEUR DE LOGEMENT SOUHAITANT SE VOIR RECONNAÎTRE COMME PRIORITAIRE ET URGENT (II DE L'ART. L. 441-2-3 ET ART. R. 441-14-1 DU CCH).
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">54-08-02-02-01-03 PROCÉDURE. VOIES DE RECOURS. CASSATION. CONTRÔLE DU JUGE DE CASSATION. BIEN-FONDÉ. APPRÉCIATION SOUVERAINE DES JUGES DU FOND. - BONNE FOI DU DEMANDEUR DE LOGEMENT SOUHAITANT SE VOIR RECONNAÎTRE COMME PRIORITAIRE ET URGENT (II DE L'ART. L. 441-2-3 ET ART. R. 441-14-1 DU CCH).
</SCT>
<ANA ID="9A"> 38-07-01 Il résulte du II de l'article L. 441-2-3 et de l'article R. 441-14-1 du code de la construction et de l'habitation (CCH) que pour être désigné comme prioritaire et devant se voir attribuer d'urgence un logement social, le demandeur doit être de bonne foi, satisfaire aux conditions réglementaires d'accès au logement social et justifier qu'il se trouve dans une des situations prévues au II de l'article L. 441-2-3 du CCH et qu'il satisfait à un des critères définis à l'article R. 441-14-1 de ce code.... ,,1) Il appartient au juge de l'excès de pouvoir d'exercer un entier contrôle sur l'appréciation portée par la commission de médiation quant à la bonne foi du demandeur.... ,,2) L'appréciation ainsi portée par le juge de l'excès de pouvoir relève du pouvoir souverain des juges du fond et ne peut, dès lors qu'elle est exempte de dénaturation, être discutée devant le juge de cassation.... ,,3) a) Ne peut être regardé comme de bonne foi, au sens de l'article L. 441-2-3 du CCH, le demandeur qui a délibérément créé par son comportement la situation rendant son relogement nécessaire.... ,,b) Il ne ressort pas des pièces du dossier que l'intéressé, locataire dans le parc privé, qui a certes laissé s'accumuler d'importants retards de loyers à partir de son licenciement, alors qu'il avait pour seule ressource le revenu de solidarité active pour un montant inférieur à celui du loyer, et qui n'a pas été en mesure d'honorer le plan d'apurement de cette dette conclu avec son propriétaire, ait cherché délibérément à échapper à ses obligations de locataire et créé ainsi la situation qui a conduit à une mesure judiciaire d'expulsion rendant son relogement nécessaire. Par suite, en estimant qu'il ne pouvait être regardé comme un demandeur de bonne foi au sens du deuxième alinéa du II de l'article L. 441-2-3 du CCH, la commission de médiation a entaché sa décision d'une erreur d'appréciation.</ANA>
<ANA ID="9B"> 54-07-02-03 Il résulte du II de l'article L. 441-2-3 et de l'article R. 441-14-1 du code de la construction et de l'habitation (CCH) que pour être désigné comme prioritaire et devant se voir attribuer d'urgence un logement social, le demandeur doit être de bonne foi, satisfaire aux conditions réglementaires d'accès au logement social  et justifier qu'il se trouve dans une des situations prévues au II de l'article L. 441-2-3 du CCH et qu'il satisfait à un des critères définis à l'article R. 441-14-1 de ce code.... ...Il appartient au juge de l'excès de pouvoir d'exercer un entier contrôle sur l'appréciation portée par la commission de médiation quant à la bonne foi du demandeur. L'appréciation ainsi portée par le juge de l'excès de pouvoir relève du pouvoir souverain des juges du fond et ne peut, dès lors qu'elle est exempte de dénaturation, être discutée devant le juge de cassation.</ANA>
<ANA ID="9C"> 54-08-02-02-01-03 Il résulte du II de l'article L. 441-2-3 et de l'article R. 441-14-1 du code de la construction et de l'habitation (CCH) que pour être désigné comme prioritaire et devant se voir attribuer d'urgence un logement social, le demandeur doit être de bonne foi, satisfaire aux conditions réglementaires d'accès au logement social  et justifier qu'il se trouve dans une des situations prévues au II de l'article L. 441-2-3 du CCH et qu'il satisfait à un des critères définis à l'article R. 441-14-1 de ce code.... ...Il appartient au juge de l'excès de pouvoir d'exercer un entier contrôle sur l'appréciation portée par la commission de médiation quant à la bonne foi du demandeur. L'appréciation ainsi portée par le juge de l'excès de pouvoir relève du pouvoir souverain des juges du fond et ne peut, dès lors qu'elle est exempte de dénaturation, être discutée devant le juge de cassation.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. sur la possibilité pour la commission de médiation, pour apprécier la bonne foi du demandeur, de tenir compte du comportement de ce dernier, CE, 17 juillet 2013, Ministre de l'écologie, du développement durable, des transports et du logement c/ M. et Mme,, n° 349315, T. p. 686.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
