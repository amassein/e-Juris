<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034600546</ID>
<ANCIEN_ID>JG_L_2017_05_000000407309</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/60/05/CETATEXT000034600546.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème chambre, 05/05/2017, 407309, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-05-05</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>407309</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BOUTET-HOURDEAUX</AVOCATS>
<RAPPORTEUR>Mme Cécile Isidoro</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emmanuelle Cortot-Boucher</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2017:407309.20170505</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La communauté de communes du Mont d'Or et des Deux lacs et les communes des Fourgs, de Labergement-Sainte-Marie et de Métabief ont demandé au juge des référés du tribunal administratif de Besançon d'ordonner, sur le fondement de l'article L. 521-1 du code de justice administrative, la suspension de l'exécution de l'arrêté du 28 octobre 2016 par lequel le préfet du Doubs a décidé de créer, à compter du 1er janvier 2017, la communauté de communes des lacs et montagnes du Haut-Doubs, par fusion des communautés de communes du Mont d'Or et des Deux lacs, d'une part, et des Hauts du Doubs, d'autre part.<br/>
<br/>
              Par une ordonnance n° 1602025 du 16 janvier 2017, le juge des référés du tribunal administratif a suspendu l'exécution de l'arrêté litigieux jusqu'à ce que le tribunal administratif statue sur les conclusions aux fins d'annulation de celui-ci.<br/>
<br/>
              Par un pourvoi, enregistré le 31 janvier 2017 au secrétariat du contentieux du Conseil d'Etat, le ministre de l'intérieur demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) statuant en référé, de rejeter la demande de suspension présentée par la communauté de communes du Mont d'Or et des Deux lacs et autres.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la convention européenne de sauvegarde et des droits de l'homme  et des libertés fondamentales ;<br/>
              - le code général des collectivités territoriales ;<br/>
              - la loi n° 2015-991 du 7 août 2015 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Cécile Isidoro, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Emmanuelle Cortot-Boucher, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Boutet-Hourdeaux, avocat de la commune des Fourgs, de la commune de Labergement-Sainte-Marie, de la commune de Métabief et de la communauté de communes du Mont d'Or et des deux Lacs.<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis au juge des référés que, par un arrêté du 29 mars 2016, le préfet du Doubs a arrêté le schéma départemental de coopération intercommunale du département du Doubs. Aux fins de mise en oeuvre de ce schéma et en application du III de l'article 35 de la loi du 7 août 2015 portant nouvelle organisation territoriale de la République, le préfet a défini, par arrêté du 14 avril 2016, un projet de périmètre d'un nouvel établissement public de coopération intercommunale fusionnant les communautés de communes du Mont d'Or et des Deux lacs, d'une part, et des Hauts du Doubs, d'autre part. Après avoir consulté les communautés de communes et les communes concernées par ce projet et obtenu l'avis de la commission départementale de coopération intercommunale réunie le 12 septembre 2016, le préfet du Doubs a décidé, par un arrêté du 28 octobre 2016, de créer à compter du 1er janvier 2017 la " communauté de communes des lacs et montagnes du Haut-Doubs " par fusion des deux communautés de communes mentionnées ci-dessus. Par une ordonnance du 16 janvier 2017, faisant droit à la demande présentée par la communauté de communes du Mont d'Or et des Deux lacs et les communes des Fourgs, de Labergement-Sainte-Marie et de Métabief, le juge des référés du tribunal administratif de Besançon a suspendu l'exécution de l'arrêté du 28 octobre 2016, en application de l'article L. 521-1 du code de justice administrative. Le ministre de l'intérieur se pourvoit en cassation contre cette ordonnance.<br/>
<br/>
              2. Aux termes de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision (...) ".<br/>
<br/>
              3. Aux termes du III l'article L. 5210-1-1 du code général des collectivités territoriales, le schéma départemental de coopération intercommunale  " prend en compte les orientations suivantes : / 1° La constitution d'établissements publics de coopération intercommunale à fiscalité propre regroupant au moins 15 000 habitants ; toutefois, ce seuil est adapté, sans pouvoir être inférieur à 5 000 habitants pour les établissements publics de coopération intercommunale à fiscalité propre ainsi que pour les projets d'établissement public de coopération intercommunale à fiscalité propre : (...) c) Comprenant une moitié au moins de communes situées dans une zone de montagne délimitée en application de l'article 3 de la loi n° 85-30 du 9 janvier 1985 relative au développement et à la protection de la montagne ou regroupant toutes les communes composant un territoire insulaire ; (...) Pour l'application du présent 1°, la population à prendre en compte est la population municipale authentifiée par le plus récent décret publié en application de l'article 156 de la loi n° 2002-276 du 27 février 2002 relative à la démocratie de proximité (...) ". <br/>
<br/>
              4. Pour ordonner la suspension de l'exécution de l'arrêté litigieux, le juge des référés a estimé qu'était de nature à créer un doute sérieux quant à la légalité de la décision attaquée le moyen tiré de ce que à tort que le préfet du Doubs s'était estimé tenu de modifier le périmètre de la communauté de communes des Hauts du Doubs, afin d'assurer le respect du seuil minimal de 5 000 habitants prévu à l'article L. 5210-1-1 du code général des collectivités territoriales. Il résulte toutefois des dispositions citées au point 3 que le législateur a entendu imposer un seuil minimal de population pour tout établissement public de coopération intercommunale à fiscalité propre, ce seuil devant être interprété strictement. Dès lors, le juge des référés a commis une erreur de droit en retenant le moyen qu'il a accueilli. Par suite et sans qu'il soit besoin d'examiner les autres moyens de son pourvoi, le ministre de l'intérieur est fondé à demander l'annulation de l'ordonnance attaquée.<br/>
<br/>
              5. Il y a lieu, dans les circonstances de l'espèce, de faire application des dispositions de l'article L. 821-2 du code de justice administrative et de régler l'affaire au titre de la procédure de référé engagée.<br/>
<br/>
              6. A l'appui de leur demande tendant à la suspension de l'exécution de l'arrêté litigieux, la communauté de communes du Mont d'Or et des Deux lacs et autres soutiennent qu'il est entaché d'un vice de procédure tiré d'un défaut d'information des élus lors de la consultation des communes sur le projet de périmètre, en méconnaissance de l'article L. 5211-5-1 du code général des collectivités territoriales et d'un principe général du droit à l'information des élus, qu'il est insuffisamment motivé, qu'il est entaché d'erreur de droit en ce que le préfet s'est estimé lié par les seuils de population définies au III de l'article L. 5210-1-1 du code général des collectivités territoriales, qu'il est entaché d'erreur manifeste d'appréciation du fait de l'absence de cohérence spatiale de la nouvelle communauté de communes qu'il a instituée, qu'il porte une atteinte disproportionnée à l'intérêt des communes concernées en imposant une réorganisation des services et un accroissement des charges, qu'il méconnaît les objectifs du 4° du III de l'article L. 5210-1-1 du code général des collectivités territoriales en ce qu'il entraînerait la création d'un nouveau syndicat à vocation multiple pour exercer certaines compétences que la communauté de communes des Hauts du Doubs ne souhaite pas transférer à la nouvelle communauté de communes, qu'il porte atteinte au principe de libre-administration des collectivités territoriales et à l'autonomie locale en ce que la fusion litigieuse est dépourvue de rationalité, qu'il méconnaît le principe de sécurité juridique et le principe d'égalité protégé par l'article 14 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales au motif que les mandats des conseillers communautaires se trouveraient être remis en cause en application des articles L. 5211-6-2 et L. 5211-41-3 du code général des collectivités territoriales.<br/>
<br/>
              7. Aucun des moyens mentionnés ci-dessus ne paraît, en l'état de l'instruction, propre à créer un doute sérieux quant à la légalité de l'arrêté litigieux. Dès lors, cette demande de suspension ne peut qu'être rejetée.<br/>
<br/>
              8. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que la somme qui est demandée à ce titre par la communauté de communes du Mont d'Or et des Deux lacs et autres soit mise à la charge de l'Etat, qui n'est pas la partie perdante dans la présente instance.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'ordonnance du juge des référés du tribunal administratif de Besançon du 16 janvier 2017 est annulée.<br/>
Article 2 : La demande présentée par la communauté de communes du Mont d'Or et des Deux lacs et autres devant le juge des référés du tribunal administratif de Besançon est rejetée.<br/>
Article 3 : Les conclusions présentées par la communauté de communes du Mont d'Or et des Deux lacs et autres au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée au ministre de l'intérieur, à la communauté de communes du Mont d'Or et des Deux lacs et aux communes des Fourgs, de Labergement-Sainte-Marie et de Métabief.<br/>
Copie en sera adressée pour information au préfet du Doubs et à la communauté de communes des Hauts du Doubs.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
