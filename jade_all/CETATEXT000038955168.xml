<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038955168</ID>
<ANCIEN_ID>JG_L_2019_08_000000418498</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/95/51/CETATEXT000038955168.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  6ème chambre jugeant seule, 21/08/2019, 418498, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-08-21</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>418498</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 6ème chambre jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP NICOLAY, DE LANOUVELLE, HANNOTIN</AVOCATS>
<RAPPORTEUR>Mme Coralie Albumazard</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Louis Dutheillet de Lamothe</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2019:418498.20190821</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés les 22 février 2018 et 3 juin 2019 au secrétariat du contentieux du Conseil d'Etat, l'Union nationale des industries de carrières et matériaux de construction Provence-Alpes-Côte-d'Azur et Corse (UNICEM PACAC) demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler le décret n° 2017-1716 du 20 décembre 2017 portant classement du parc naturel régional de la Sainte-Baume ;<br/>
<br/>
              2°) d'enjoindre au Premier ministre d'assurer la rectification du plan du parc de façon à rendre ce dernier conforme à la mesure 7 du rapport de la charte ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'environnement ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Coralie Albumazard, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Louis Dutheillet de Lamothe, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Nicolaÿ, de Lanouvelle, Hannotin, avocat de la l'Union nationale des industries de carrières et matériaux de construction Provence-Alpes-Côte-d'Azur et Corse (UNICEM PACAC) ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. L'Union nationale des industries de carrières et matériaux de construction Provence-Alpes-Côte-d'Azur et Corse (UNICEM PACAC) demande au Conseil d'Etat d'annuler pour excès de pouvoir le décret du 20 décembre 2017 portant classement du parc naturel régional de la Sainte-Baume.<br/>
<br/>
              2. Aux termes de l'article L. 333-1 du code de l'environnement : " (...) / II.- La charte constitue le projet du parc naturel régional. Elle comprend : 1° Un rapport déterminant les orientations de protection, de mise en valeur et de développement, notamment les objectifs de qualité paysagère définis à l'article L. 350-1 C, ainsi que les mesures permettant de les mettre en oeuvre et les engagements correspondants ; / 2° Un plan, élaboré à partir d'un inventaire du patrimoine, indiquant les différentes zones du parc et leur vocation ; (...) IV.- (...) / La charte est adoptée par décret portant classement ou renouvellement du classement en parc naturel régional, pour une durée de quinze ans, du territoire des communes comprises dans le périmètre de classement ou de renouvellement de classement approuvé par la région. (...) / V. - L'Etat et les collectivités territoriales ainsi que les établissements publics de coopération intercommunale à fiscalité propre ayant approuvé la charte appliquent les orientations et les mesures de la charte dans l'exercice de leurs compétences sur le territoire du parc. Ils assurent, en conséquence, la cohérence de leurs actions et des moyens qu'ils y consacrent, ainsi que, de manière périodique, l'évaluation de la mise en oeuvre de la charte et le suivi de l'évolution du territoire. (...) ".  Le II de l'article R. 333-3 dispose que " La charte comprend : 1° Un rapport déterminant : / a) Les orientations de protection, de mise en valeur et de développement envisagées pour la durée du classement. (...) / b) Les mesures qui seront mises en oeuvre sur le territoire classé, applicables à l'ensemble du parc ou dans des zones déterminées à partir des spécificités du territoire et, parmi ces mesures, celles qui sont prioritaires, avec l'indication de leur échéance prévisionnelle de mise en oeuvre ; (...) / 2° Un plan du parc représentant le périmètre de classement potentiel et le périmètre classé, sur lequel sont délimitées, en fonction du patrimoine et des paysages, les différentes zones où s'appliquent les orientations et les mesures définies dans le rapport ; le plan caractérise toutes les zones du territoire selon leur nature et leur vocation dominante ; / (...) ".<br/>
<br/>
              3. Il résulte de ces dispositions que la charte d'un parc naturel régional est un acte destiné à orienter l'action des pouvoirs publics dans un souci de protection de l'environnement, d'aménagement du territoire, de développement économique et social et d'éducation et de formation du public sur le territoire du parc. Il appartient, dès lors, aux auteurs de la charte de s'assurer de la cohérence des orientations et mesures mentionnées dans le rapport de la charte avec le zonage figurant dans le plan du parc et les indications qu'il comporte. <br/>
<br/>
              4. Il ressort des pièces du dossier que, d'une part, la mesure n° 7 du rapport de la charte du parc naturel régional de la Sainte-Baume, qui se rattache à l'orientation consistant à " affirmer l'excellence environnementale du territoire pour la gestion de ses ressources naturelles ", prévoit d'" assurer l'intégration environnementale des infrastructures d'exploitation des ressources naturelles. " S'agissant de l'exploitation des carrières, elle comporte des " dispositions " visant à la fois à " protéger de toute création de carrières les espaces paysagers, agricoles et naturels à enjeux identifiés au plan du parc (paysages remarquables et paysages agricoles sensibles, sites soumis à un arrêté de biotope, sites Natura 2000, réservoirs de biodiversité, zones de vulnérabilité des masses d'eau souterraine) " et à " valoriser les gisements potentiels à travers la possibilité de renouvellement ou d'extension des carrières existantes, sous réserve de tenir compte des enjeux paysagers, agricoles et naturels des espaces identifiés au plan du parc (paysages remarquables et paysages agricoles sensibles, sites soumis à un arrêté de biotope, site Natura 2000, réservoirs de biodiversité, zone de vulnérabilité souterraine). " D'autre part, le plan du parc délimite les zones à enjeux, notamment les espaces de paysages remarquables, de paysages agricoles sensibles, de réservoirs de biodiversité et des sites Natura 2000. <br/>
<br/>
              5. En premier lieu, il ne ressort pas des pièces du dossier que le plan du parc serait entaché d'erreur manifeste d'appréciation dans le repérage des zones à enjeux. <br/>
<br/>
              6. En second lieu, si les éléments du rapport cités au point 4 rendent impossible la création de nouvelles carrières dans les espaces paysagers, agricoles et naturels à enjeux identifiés dans le plan du parc, ils permettent cependant le renouvellement ou l'extension des carrières existantes, y compris dans ces zones, sous réserve de tenir compte des enjeux identifiés par le plan. La circonstance que les zones identifiées sur le plan pour le renouvellement ou l'extension des carrières correspondent aux périmètres d'autorisation des carrières existantes au titre de la législation des installations classées pour la protection de l'environnement et soient contigües de zones à enjeux, voire les chevauchent, n'a pas pour effet d'empêcher tout renouvellement ou extension des exploitations en cours mais impose uniquement que ceux-ci se fassent dans le respect des contraintes spécifiques aux zones à enjeux. Dès lors, le moyen tiré d'une incohérence sur ce point entre le rapport et le plan de la charte du parc ne peut qu'être écarté. <br/>
<br/>
              7. Il résulte de ce qui précède que l'UNICEM PACAC n'est pas fondée à demander l'annulation du décret du 20 décembre 2017 portant classement du parc naturel régional de la Sainte-Baume ni à ce qu'il soit enjoint au Premier ministre d'assurer la rectification du plan du parc de façon à le rendre conforme à la mesure n° 7 du rapport de la charte.<br/>
<br/>
              8. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat, qui n'est pas, dans la présente instance, la partie perdante..<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de l'UNICEM PACAC est rejetée.<br/>
Article 2 : La présente décision sera notifiée à l'UNICEM  PACAC, au Premier ministre et à la ministre de la transition écologique et solidaire.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
