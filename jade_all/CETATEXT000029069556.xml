<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029069556</ID>
<ANCIEN_ID>JG_L_2014_06_000000353159</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/06/95/CETATEXT000029069556.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  4ème sous-section jugeant seule, 11/06/2014, 353159, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-06-11</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>353159</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 4ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP NICOLAY, DE LANOUVELLE, HANNOTIN</AVOCATS>
<RAPPORTEUR>M. Bruno Bachini</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Gaëlle Dumortier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2014:353159.20140611</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête sommaire et le mémoire complémentaire, enregistrés les 5 octobre 2011 et 4 janvier 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour la chambre de commerce et d'industrie de Montpellier, dont le siège est situé 32 Grand rue Jean Moulin à Montpellier (34944), la chambre de commerce et d'industrie de la Lozère, dont le siège est situé 16 Boulevard de Soubeyran à Mende (48002), la fédération des associations de commerçants et d'artisans de l'Aveyron, dont le siège est situé 17 rue Aristide Briand à Rodez (12033), l'association " Clermont Plein Coeur ", dont le siège est situé 7 rue Doyen Gosse à Clermont l'Herault (34800), l'association " Lodèvois commerçants artisans ", dont le siège est situé square Georges Auric à Lodève (34072), l'association U.C.A.C.H, dont le siège est situé square Georges Auric à Lodève (34072), l'association " Vivre Millau ", dont le siège est situé à Millau BP 433 (12104), la communauté de communes du Lodèvois et Larzac, dont le siège est situé 9 place Alsace-Lorraine à Lodève (34700), la communauté de communes de la vallée de l'Hérault, dont le siège est situé 2 parc d'activités de Camalcé à Gignac (34150) et la communauté de communes du Clermontais, dont le siège est situé 20 avenue Raymond Lacombe à Clermont l'Herault (34800) ; la chambre de commerce et d'industrie de Montpellier et autres demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision implicite de rejet résultant du silence gardé par la Commission nationale d'aménagement commercial sur les recours dirigés contre la décision de la commission départementale d'aménagement commercial de l'Aveyron du 23 février 2011 autorisant la SCI Tommy à créer un ensemble commercial d'une surface de vente de 5 988 m ² sur le territoire de la commune de la Cavalerie (Aveyron) ; <br/>
<br/>
              2°) d'annuler pour excès de pouvoir la décision expresse du 14 septembre 2011 par laquelle la Commission nationale d'aménagement commercial a rejeté les recours dirigés contre cette décision de la commission départementale d'aménagement commercial de l'Aveyron et autorisé la création de l'ensemble commercial en question ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat et de la SCI Tommy la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 7 mai 2014, présentée pour la chambre de commerce et d'industrie de Montpellier et autres ;<br/>
<br/>
              Vu le code de commerce ;<br/>
<br/>
              Vu la loi n° 73-1193 du 27 décembre 1973 ;<br/>
<br/>
              Vu la loi n° 2008-776 du 4 août 2008 ;<br/>
<br/>
              Vu le décret n° 2005-850 du 27 juillet 2005 ;<br/>
<br/>
              Vu le décret n° 2008-1212 du 24 novembre 2008 ;<br/>
<br/>
              Vu le décret n° 2011-921 du 1er août 2011 ; <br/>
<br/>
              Vu l'arrêté du 5 janvier 2010 portant organisation de l'administration centrale du ministère de l'écologie, de l'énergie, du développement durable et de l'aménagement du territoire ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Bruno Bachini, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Gaëlle Dumortier, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Nicolaÿ, de Lanouvelle, Hannotin, avocat de la chambre de commerce et d'industrie de Montpellier et autres ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant que, par deux recours enregistrés le 4 avril 2011, la chambre de commerce et d'industrie de Montpellier et autres ont saisi la Commission nationale d'aménagement commercial aux fins d'obtenir l'annulation de la décision par laquelle la commission départementale d'aménagement commercial de l'Aveyron a autorisé la SCI Tommy à créer un ensemble commercial d'une surface de vente de 5 988 m ² à La Cavalerie ; que la commission nationale ne s'étant pas prononcée sur ce recours avant l'expiration du délai de quatre mois prévu par les dispositions de l'article L. 752-17 du code de commerce, une décision implicite de rejet des recours de la chambre de commerce et d'industrie de Montpellier et autres, valant autorisation implicite au profit de la SCI Tommy, est intervenue le 4 août 2011 ; que, par une décision expresse du 14 septembre 2011, la commission nationale a retiré cette décision implicite, rejeté le recours de la chambre de commerce et d'industrie de Montpellier et autres et délivré, à la demande du pétitionnaire, une nouvelle autorisation ; <br/>
<br/>
              2. Considérant que, le délai de quatre mois dans lequel la Commission nationale d'aménagement commercial doit statuer en application des dispositions de l'article L. 752-17 du code du commerce n'étant pas imparti à peine de dessaisissement, la commission nationale a pu légalement retirer la décision implicite née de son silence dans le délai de deux mois à compter de son adoption, dès lors que celle-ci était illégale ; que, par suite, les conclusions de la requête dirigées contre cette décision implicite ne peuvent qu'être rejetées comme irrecevables ; <br/>
<br/>
              Sur la légalité de la décision de la commission nationale du 14 septembre 2011 : <br/>
<br/>
              En ce qui concerne la régularité de la procédure devant la commission nationale :<br/>
<br/>
              3. Considérant qu'aux termes du quatrième alinéa de l'article R. 752-51 du code de commerce : " Le commissaire du gouvernement recueille les avis des ministres intéressés, qu'il présente à la commission nationale " ; qu'aux termes du deuxième alinéa de l'article R. 752-16 du même code : " Pour les projets d'aménagement commercial, l'instruction des demandes est effectuée conjointement par les services territorialement compétents chargés du commerce ainsi que ceux chargés de l'urbanisme et de l'environnement. " ;<br/>
<br/>
              4. Considérant, d'une part, qu'il résulte de la combinaison de ces dispositions que les ministres intéressés, au sens de l'article R. 752-51 du code de commerce, sont ceux qui ont autorité sur les services chargés d'instruire les demandes, soit les ministres en charge du commerce, de l'urbanisme et de l'environnement ; qu'en l'espèce, il ressort des pièces du dossier que les avis de ces ministres ont été présentés à la commission ; que, dès lors, le moyen tiré de l'absence des avis des ministres intéressés manque en fait ; que, d'autre part, le moyen tiré de ce que le nom et la qualité de l'auteur de l'avis rendu par le ministre de l'écologie, de l'énergie, du développement durable et de l'aménagement du territoire n'auraient pas été mentionnés manque en fait ; qu'il résulte des dispositions combinées du décret du 27 juillet 2005 relatif aux délégations de signature des membres du gouvernement, de l'arrêté du 5 janvier 2010 portant organisation de l'administration centrale du ministère de l'écologie, de l'énergie, du développement durable et de l'aménagement du territoire, et de la décision du 10 mai 2011 portant délégation de signature (direction de l'habitat, de l'urbanisme et des paysages) que M. A..., adjoint à la sous-directrice de la qualité du cadre de vie, avait qualité pour signer au nom du ministre l'avis du 5 septembre 2011 recueilli par le commissaire du gouvernement au titre de l'article R. 752-51 ; que, par ailleurs, le moyen tiré de ce que les avis d'autres ministres n'ont pas été recueillis est inopérant ;<br/>
<br/>
              En ce qui concerne la composition du dossier de demande : <br/>
<br/>
              5. Considérant qu'aux termes de l'article R. 752-6 du code de commerce : " La demande d'autorisation prévue à l'article L. 752-1 est présentée soit par le propriétaire de l'immeuble, soit par une personne justifiant d'un titre l'habilitant à construire sur le terrain ou à exploiter commercialement l'immeuble (...) " ; qu'il ressort des pièces du dossier de demande d'autorisation qu'une attestation notariée en date du 17 mai 2011 établissait l'existence du titre de propriété de la SARL N.C.G.M sur une parcelle formant une partie de l'assiette du projet ; que la société pétitionnaire justifiait d'une autorisation de la SARL N.C.G.M à l'effet de finaliser l'acquisition de cette parcelle ; que, par suite, le moyen tiré de ce que la société pétitionnaire n'aurait pas justifié de sa maîtrise foncière manque en fait ;<br/>
<br/>
              6. Considérant que, contrairement à ce que soutient la société requérante, il ressort des pièces du dossier que le dossier de demande d'autorisation soumis à la commission nationale comportait des indications détaillées concernant la desserte en transports collectifs et les accès pédestres et cyclistes du projet litigieux, les flux de transports et les accès au site, et qu'il permettait à la commission nationale d'apprécier la conformité du projet aux objectifs fixés par le législateur ; qu'ainsi, le moyen tiré de ce que la commission nationale se serait prononcée au vu d'un dossier incomplet ne peut qu'être écarté ;<br/>
<br/>
              En ce qui concerne l'appréciation de la commission nationale :<br/>
<br/>
              7. Considérant qu'il appartient aux commissions d'aménagement commercial, lorsqu'elles se prononcent sur un projet d'exploitation commerciale soumis à autorisation en application de l'article L. 752-1 du code de commerce, d'apprécier la conformité de ce projet aux objectifs prévus à l'article 1er de la loi du 27 décembre 1973 et à l'article L. 750-1 du code de commerce, au vu des critères d'évaluation mentionnés à l'article L. 752-6 du même code ; que l'autorisation ne peut être refusée que si, eu égard à ses effets, le projet compromet la réalisation de ces objectifs ;<br/>
<br/>
              8. Considérant que si la requérante soutient que la décision attaquée a méconnu l'objectif fixé par le législateur en matière d'aménagement du territoire pour ce qui concerne l'animation de la vie urbaine et rurale, il ressort des pièces du dossier que le village de marques projeté permettra de développer une offre inexistante sur la zone de chalandise et est susceptible d'attirer une clientèle nouvelle bénéficiant aux commerces déjà présents dans la commune d'implantation du projet ; qu'en matière de flux de transport, la commission nationale a, d'une part, correctement pris en compte les flux de transport additionnels liés au projet et, d'autre part, estimé, sans commettre d'erreur d'appréciation, que les infrastructures existantes permettraient d'y faire face ; qu'ainsi, le moyen tiré de ce que la commission nationale aurait méconnu l'objectif posé par le législateur en matière d'aménagement du territoire doit être écarté ; <br/>
<br/>
              9. Considérant que si les requérantes font valoir que le projet, eu égard aux enjeux définis par la charte du parc naturel régional des Grands Causses, portera atteinte à un espace naturel sensible sans mesures compensatoires en matière de qualité environnementale, en violation de l'objectif de développement durable posé par le législateur, il ressort des pièces du dossier, d'une part, que le pétitionnaire a tenu compte de la sensibilité de l'environnement en prévoyant une insertion paysagère suffisante pour le village de marques projeté, d'autre part, que le pétitionnaire a prévu l'installation de dispositifs d'économie d'énergie et des mesures concernant le traitement des déchets ; que si le projet se situe dans le périmètre rapproché de la source de l'Espérelle, l'étude hydrologique réalisée par la société pétitionnaire a montré que le projet n'était pas en zone humide ou marécageuse ; que si le site d'implantation du projet se situe au sein d'une zone naturelle d'intérêt écologique, faunistique et floristique (ZNIEFF), il ressort des pièces du dossier que l'emprise examinée par la commission nationale ne concerne qu'une petite partie de la superficie totale de cette ZNIEFF ; que la desserte du site par les transports en commun est suffisante ; qu'ainsi, la Commission nationale d'aménagement commercial n'a pas commis, s'agissant des effets du projet sur le développement durable, d'erreur d'appréciation ;<br/>
<br/>
              10. Considérant qu'il résulte de tout ce qui précède, et sans qu'il soit besoin de statuer sur les fins de non-recevoir tirées du défaut d'intérêt à agir opposées par la SCI Tommy, que la chambre de commerce et d'industrie de Montpellier et autres ne sont pas fondées à demander l'annulation de la décision attaquée ; <br/>
<br/>
              Sur les conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              11. Considérant que ces dispositions font obstacle à ce qu'une somme soit mise à ce titre à la charge de la SCI Tommy qui n'est pas, dans la présente instance, la partie perdante ; qu'en revanche, il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la chambre de commerce et d'industrie de Montpellier et des autres requérants la somme de 500 euros chacun à verser à la SCI Tommy au titre de ces mêmes dispositions ;<br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er : La requête de la chambre de commerce et d'industrie de Montpellier et autres est rejetée. <br/>
Article 2 : La chambre de commerce et d'industrie de Montpellier et chacun des autres requérants verseront à la SCI Tommy la somme de 500 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
Article 3 : La présente décision sera notifiée à la chambre de commerce et d'industrie de Montpellier, à la SCI Tommy et à la Commission nationale d'aménagement commercial. Les autres parties requérantes seront informées de la présente décision par la SCP Nicolaÿ, de Lanouvelle, Hannotin, avocat au Conseil d'Etat et à la Cour de cassation, qui les représente devant le Conseil d'Etat. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
