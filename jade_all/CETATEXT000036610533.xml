<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036610533</ID>
<ANCIEN_ID>JG_L_2018_02_000000411138</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/61/05/CETATEXT000036610533.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème - 7ème chambres réunies, 16/02/2018, 411138, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-02-16</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>411138</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème - 7ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LYON-CAEN, THIRIEZ</AVOCATS>
<RAPPORTEUR>M. Clément Malverti</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Guillaume Odinet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:411138.20180216</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés les 1er juin et 4 décembre 2017 au secrétariat du contentieux du Conseil d'Etat, le Syndicat mixte de l'aéroport de Beauvais-Tillé et la Société aéroportuaire de gestion et d'exploitation de Beauvais demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir l'avis n° 2017-041 de l'Autorité de régulation des activités ferroviaires et routières du 29 mars 2017 relatif au projet de décision du Syndicat mixte de l'aéroport de Beauvais-Tillé d'interdiction du service déclaré par la société FlixBus France sur la liaison entre cet aéroport et Paris (cours des Maréchaux) ;<br/>
<br/>
              2°) de mettre à la charge de l'Autorité la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code des transports ;<br/>
              - le décret n° 85-891 du 16 août 1985 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Clément Malverti, auditeur, <br/>
<br/>
              - les conclusions de M. Guillaume Odinet, rapporteur public,<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Lyon-Caen, Thiriez, avocat du Syndicat mixte de l'aéroport de Beauvais-Tillé et de la Société aéroportuaire de gestion et d'exploitation de Beauvais ; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1.	Considérant qu'aux termes de l'article L. 3111-17 du code des transports, issu de la loi du 6 août 2015 pour la croissance, l'activité et l'égalité des chances économiques : " Les entreprises de transport public routier de personnes établies sur le territoire national peuvent assurer des services réguliers interurbains " ; qu'aux termes de l'article L. 3111-18 de ce code : " Tout service assurant une liaison dont deux arrêts sont distants de 100 kilomètres ou moins fait l'objet d'une déclaration auprès de l'Autorité de régulation des activités ferroviaires et routières, préalablement à son ouverture. L'autorité publie sans délai cette déclaration. / Une autorité organisatrice de transport peut, après avis conforme de l'Autorité de régulation des activités ferroviaires et routières, dans les conditions définies à l'article L. 3111-19, interdire ou limiter les services mentionnés au premier alinéa du présent article lorsqu'ils sont exécutés entre des arrêts dont la liaison est assurée sans correspondance par un service régulier de transport qu'elle organise et qu'ils portent, seuls ou dans leur ensemble, une atteinte substantielle à l'équilibre économique de la ligne ou des lignes de service public de transport susceptibles d'être concurrencées ou à l'équilibre économique du contrat de service public de transport concerné " ;<br/>
<br/>
              2.	Considérant que la société FlixBus France a déposé auprès de l'Autorité de régulation des activités ferroviaires et routières (ARAFER), le 21 décembre 2016, une déclaration portant sur un service régulier interurbain de transport par autocar entre Paris et l'aéroport de Beauvais-Tillé consistant en quinze départs par jour depuis chacun de ces deux lieux ; que le Syndicat mixte de l'aéroport de Beauvais-Tillé a saisi l'Autorité de régulation des activités ferroviaires et routières d'un projet d'interdiction de ces services ; que, par un avis n° 2017-041 du 29 mars 2017, l'Autorité a estimé que la liaison déclarée par la société FlixBus France ne constituait pas une liaison similaire à celle du service organisé par le Syndicat mixte de l'aéroport de Beauvais-Tillé et a, pour ce motif, émis un avis défavorable sur le projet d'interdiction du syndicat mixte ; que le Syndicat mixte de l'aéroport de Beauvais-Tillé et la Société aéroportuaire de gestion et d'exploitation de Beauvais, concessionnaire de la desserte de l'aéroport, demandent l'annulation pour excès de pouvoir de cet avis ;<br/>
<br/>
              3.	Considérant, en premier lieu, que les circonstances que les emplacements des arrêts mentionnés dans la déclaration ne respectent pas la réglementation relative à la circulation et au stationnement des autocars et que la société de transport n'est pas établie sur le territoire national ne sont pas au nombre des motifs de nature à permettre de prendre légalement une décision d'interdiction ou de limitation en application de l'article L. 3111-18 du code des transports ; qu'il n'appartient donc pas à l'Autorité de régulation des activités ferroviaires et routières, saisie d'un projet de décision dans les conditions définies à l'article L. 3111-19 de ce code, de vérifier que le service déclaré respecte la réglementation relative à la circulation et au stationnement des autocars ; qu'il ne lui appartient pas davantage de vérifier que l'entreprise qui l'a déclaré est établie sur le territoire national ; que l'Autorité de régulation des activités ferroviaires et routières n'a donc pas commis d'erreur de droit en s'abstenant de procéder à de telles vérifications ;<br/>
<br/>
              4.	Considérant, en deuxième lieu, que, contrairement à ce que soutiennent les requérants, il ne ressort d'aucune des pièces du dossier que la société FlixBus France aurait obtenu l'avis litigieux par fraude ;<br/>
<br/>
              5.	Considérant, en dernier lieu, qu'il résulte de l'article L. 3111-18 du code des transports qu'un service déclaré ne peut être interdit ou limité par l'autorité publique, sur avis conforme de l'ARAFER, qu'à condition qu'il relie des arrêts dont la liaison est également assurée par un service régulier de transport que la personne publique qui a saisi l'ARAFER organise ; que l'article R. 3111-51 du même code précise qu'une décision d'interdiction ou de limitation ne peut porter que sur un service déclaré assurant une liaison déjà assurée par un service régulier de la personne publique ou une liaison similaire, définie par le 14° de l'art. R. 3111-37 du code comme toute " liaison soumise à régulation dont l'origine et la destination se situent à une distance respective de l'origine et de la destination de celle de l'autorité, mesurée en ligne droite, d'au plus 5 km, cette valeur étant portée à 10 km entre les origines ou entre les destinations des deux liaisons si elles sont situées en région d'Ile-de-France " ; que le pouvoir réglementaire ne saurait être regardé comme ayant ainsi restreint illégalement la possibilité d'interdire ou de limiter des services déclarés ; qu'il n'est pas contesté que la distance séparant l'arrêt du service organisé par le Syndicat mixte de l'aéroport de Beauvais-Tillé, situé porte Maillot à Paris 16ème, de l'arrêt du service déclaré par la société FlixBus France, situé cours des Maréchaux à Paris 12ème, est supérieure au seuil de 10 kilomètres fixé par les dispositions de l'article R. 3111-51 ; que, par suite, les requérants ne sont pas fondés à soutenir que l'Autorité aurait entaché son avis d'erreur d'appréciation en estimant que le service déclaré par la société FlixBus France ne constituait pas une liaison similaire à celle du service organisé par le Syndicat mixte de l'aéroport de Beauvais-Tillé ;<br/>
<br/>
              6.	Considérant qu'il résulte de tout ce qui précède que le Syndicat mixte de l'aéroport de Beauvais-Tillé et la Société aéroportuaire de gestion et d'exploitation de Beauvais ne sont pas fondés à demander l'annulation pour excès de pouvoir de l'avis qu'ils attaquent ;<br/>
<br/>
              7.	Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise, à ce titre, à la charge de l'Autorité de régulation des activités ferroviaires et routières, qui n'est pas, dans la présente instance, la partie perdante ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : La requête du Syndicat mixte de l'aéroport de Beauvais-Tillé et de la Société aéroportuaire de gestion et d'exploitation de Beauvais est rejetée.<br/>
<br/>
Article 2 : La présente décision sera notifiée au Syndicat mixte de l'aéroport de Beauvais-Tillé, à la Société aéroportuaire de gestion et d'exploitation de Beauvais, à la société FlixBus France et à l'Autorité de régulation des activités ferroviaires et routières. Copie en sera adressée au ministre d'Etat, ministre de la transition écologique et solidaire.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
