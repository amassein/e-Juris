<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044588624</ID>
<ANCIEN_ID>JG_L_2021_12_000000458964</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/58/86/CETATEXT000044588624.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 15/12/2021, 458964, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-12-15</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>458964</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>CABINET ROUSSEAU ET TAPIE</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2021:458964.20211215</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, enregistrée le 29 novembre 2021 au secrétariat du contentieux du Conseil d'Etat, M. C... B... demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative :<br/>
<br/>
              1°) de suspendre l'exécution du décret du 19 novembre 2021 du président de la République portant nomination du directeur de l'Institut d'études politiques de Paris ainsi que l'arrêté du même jour de la ministre de l'enseignement supérieur, de la recherche et de l'innovation portant désignation de l'administrateur de la Fondation nationale des sciences politiques ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Il soutient que :<br/>
              - la condition d'urgence est satisfaite dès lors que, d'une part, les décisions contestées affectent de manière grave le déroulement de sa carrière en portant notamment atteinte à sa réputation, d'autre part, leur annulation dans plusieurs mois nuira au bon fonctionnement de l'Institut d'études politiques de Paris et, enfin, leur suspension aura pour seul effet la mise en place d'une gouvernance provisoire ; <br/>
              - il existe un doute sérieux quant à la légalité des décisions contestées ; <br/>
              - la délibération de la commission de proposition du 23 septembre 2021 est entachée d'irrégularités tenant, en premier lieu, à sa composition, en deuxième lieu, au recours à un cabinet de recrutement, en troisième lieu, à la méconnaissance du principe de confidentialité, en quatrième lieu, à la mise en concurrence de son dossier de candidature avec d'autres dossiers irrecevables et, en dernier lieu, à la méconnaissance des principes d'impartialité et d'égalité.  <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le décret n° 2015-1829 du 29 décembre 2015 ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision ". En vertu de l'article L. 522-3 du même code, le juge des référés peut, par une ordonnance motivée, rejeter une requête sans instruction ni audience lorsque la condition d'urgence n'est pas remplie ou lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable ou qu'elle est mal fondée.<br/>
<br/>
              2. M. B... demande la suspension de l'exécution, d'une part, du décret du 19 novembre 2021 par lequel le président de la République a nommé M. A... D... directeur de l'Institut d'études politiques de Paris et, d'autre part, de l'arrêté du même jour par lequel la ministre de l'enseignement supérieur, de la recherche et de l'innovation l'a nommé aux fonctions d'administrateur de la Fondation nationale des sciences politiques. <br/>
<br/>
              3. La condition d'urgence à laquelle est subordonné le prononcé d'une mesure de suspension doit être regardée comme remplie lorsque la décision administrative contestée préjudicie de manière suffisamment grave et immédiate à un intérêt public, à la situation du requérant ou aux intérêts qu'il entend défendre. Il appartient au juge des référés d'apprécier concrètement, compte tenu des justifications fournies par le requérant, si les effets de l'acte contesté sont de nature à caractériser une urgence justifiant que, sans attendre le jugement de la requête au fond, l'exécution de la décision soit suspendue. L'urgence doit être appréciée objectivement et compte tenu de l'ensemble des circonstances de l'affaire.<br/>
<br/>
              4. Pour justifier l'urgence qui s'attacherait à la suspension du décret et de l'arrêté contestés, M. B... fait valoir que leur exécution porte une atteinte grave, d'une part, à sa réputation et à sa situation professionnelle dès lors qu'il est définitivement privé d'une chance d'accéder au poste de directeur de l'Institut d'études politiques de Paris et qu'il n'a recueilli qu'une seule voix lors du premier tour de sélection et, d'autre part, à l'intérêt public, compte tenu des effets que leur annulation produira dans quelques mois sur le bon fonctionnement de l'institution. Il soutient, en outre, que la suspension des décisions contestées se traduira par la mise en place d'une gouvernance provisoire à même de garantir la continuité de l'activité au sein de l'Institut d'études politiques de Paris. Toutefois, il ne résulte pas de ces seuls éléments que l'exécution des décisions contestées, dont il n'est au demeurant pas établi qu'elles conduiraient, comme le soutient le requérant, au renouvellement du mandat de M. D... dans cinq ans, porterait une atteinte suffisamment grave et immédiate à sa situation ou à un intérêt public pour caractériser une situation d'urgence justifiant sa suspension. <br/>
<br/>
              5. Il résulte de ce qui précède que la condition d'urgence n'est pas remplie, sans qu'il soit besoin d'examiner si l'un des moyens soulevés est de nature à créer, en l'état de l'instruction, un doute sérieux quant à la légalité du décret et de l'arrêté contestés. Par suite, sa requête doit être rejetée en toutes ses conclusions, selon la procédure prévue à l'article L. 522-3 du code de justice administrative. <br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
              ------------------<br/>
<br/>
Article 1er : La requête de M. B... est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à M. C... B....<br/>
<br/>
Fait à Paris, le 15 décembre 2021<br/>
Signé : Christophe Chantepy<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
