<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042687555</ID>
<ANCIEN_ID>JG_L_2020_12_000000442351</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/68/75/CETATEXT000042687555.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème chambre, 16/12/2020, 442351, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-12-16</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>442351</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Géraud Sajust de Bergues</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Marie-Gabrielle Merloz</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2020:442351.20201216</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              M. D... B... a demandé au tribunal administratif de Toulon d'annuler les opérations électorales qui se sont déroulées le 15 mars 2020 dans la commune de Sanary-sur-Mer (Var) et d'ordonner la tenue d'un nouveau scrutin. Par une ordonnance n° 2000954 du 1er juillet 2020, la présidente de la 4ème chambre du tribunal administratif de Toulon a rejeté sa protestation.<br/>
<br/>
              Par une requête, un mémoire récapitulatif et deux autres mémoires, enregistrés au secrétariat de la section du contentieux les 31 juillet, 22 octobre, 12 et 16 novembre 2020, M. B... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) d'annuler les opérations électorales contestées, ainsi que l'ensemble des opérations électorales qui se sont déroulées le 15 mars 2020 pour le premier tour des élections municipales en France ;<br/>
<br/>
              3°) d'ordonner le remboursement par l'Etat de ses dépenses de campagne.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la loi n° 2020-290 du 23 mars 2020 ;<br/>
              - la décision n° 2020-849 QPC du 17 juin 2020 ;<br/>
              - le code électoral ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Géraud Sajust de Bergues, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Marie-Gabrielle Merloz, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. A l'issue des opérations électorales qui se sont déroulées le 15 mars 2020 dans la commune de Sanary-sur-Mer (Var), les 33 sièges de conseillers municipaux ont été pourvus, 29 des sièges étant attribués à des candidats de la liste " Toujours mieux vivre à Sanary " conduite par M. E... C..., qui a obtenu 68,71 % des voix, 3 sièges étant attribués à des candidats de la liste " Emmanuel B..., avec Sanary au coeur " conduite par M. D... B..., qui a obtenu 22,72 % des voix, et 1 siège étant attribué à la liste " Renouveau pour Sanary, engagement citoyen pour le progrès social, la démocratie et l'environnement " conduite par M. F... A..., qui a obtenu 8,56 % des voix. M. B... relève appel de l'ordonnance du 1er juillet 2020 par laquelle la présidente de la 4ème chambre du tribunal administratif de Toulon a rejeté la protestation qu'il a formée contre ces opérations électorales.<br/>
<br/>
              Sur les conclusions de M. B... tendant à l'annulation de l'ensemble des opérations électorales qui se sont déroulées le 15 mars 2020 pour le premier tour des élections municipales en France :<br/>
<br/>
              2. Aux termes du premier alinéa de l'article L. 248 du code électoral : " Tout électeur et tout éligible a le droit d'arguer de nullité les opérations électorales de la commune devant le tribunal administratif ". Le ministre de l'intérieur est fondé à soutenir que la protestation de M. B..., électeur de la commune de Sanary-sur-Mer et qui y est éligible, n'est pas recevable en tant qu'elle conteste, par des conclusions au demeurant nouvelles en appel, les opérations électorales qui se sont déroulées dans d'autres communes que cette dernière.<br/>
<br/>
              Sur les conclusions de M. B... tendant à l'annulation des opérations électorales qui se sont déroulées le 15 mars 2020 dans la commune de Sanary-sur-Mer :<br/>
<br/>
              3. L'émergence d'un nouveau coronavirus, responsable de la maladie à coronavirus 2019 ou covid-2019 et particulièrement contagieux, a été qualifiée d'urgence de santé publique de portée internationale par l'Organisation mondiale de la santé du 30 janvier 2020, puis de pandémie le 11 mars 2020. La propagation du virus sur le territoire français a conduit le ministre des solidarités et de la santé à prendre, à compter du 4 mars 2020, des mesures de plus en plus strictes destinées à réduire les risques de contagion. Dans ce contexte, le Premier ministre a adressé à l'ensemble des maires, le 7 mars 2020, une lettre présentant les mesures destinées à assurer le bon déroulement des élections municipales et communautaires prévues les 15 et 22 mars 2020. Ces mesures ont été précisées par une circulaire du ministre de l'intérieur du 9 mars 2020 relative à l'organisation des élections municipales des 15 et 22 mars 2020 en situation d'épidémie de coronavirus covid-19, formulant des recommandations relatives à l'aménagement des bureaux de vote et au respect des consignes sanitaires, et par une instruction de ce ministre, du même jour, destinée à faciliter l'exercice du droit de vote par procuration. Après consultation par le Gouvernement du conseil scientifique mis en place pour lui donner des informations scientifiques utiles à l'adoption des mesures nécessaires pour faire face à l'épidémie de covid-19, les 12 et 14 mars 2020, le premier tour des élections municipales a eu lieu comme prévu le 15 mars 2020. A l'issue du scrutin, les conseils municipaux ont été intégralement renouvelés dans 30 143 communes ou secteurs. Le taux d'abstention a atteint 55,34 % des inscrits, contre 36,45 % au premier tour des élections municipales de 2014.<br/>
<br/>
              4. Au vu de la situation sanitaire, l'article 19 de la loi du 23 mars 2020 d'urgence pour faire face à l'épidémie de covid 19 a reporté le second tour des élections, initialement fixé au 22 mars 2020, au plus tard en juin 2020 et prévu que : " Dans tous les cas, l'élection régulière des conseillers municipaux et communautaires, des conseillers d'arrondissement, des conseillers de Paris et des conseillers métropolitains de Lyon élus dès le premier tour organisé le 15 mars 2020 reste acquise, conformément à l'article 3 de la Constitution ". Ainsi que le Conseil constitutionnel l'a jugé dans sa décision n° 2020-849 du 17 juin 2020, ces dispositions n'ont ni pour objet ni pour effet de valider rétroactivement les opérations électorales du premier tour ayant donné lieu à l'attribution de sièges et ne font ainsi pas obstacle à ce que ces opérations soient contestées devant le juge de l'élection. <br/>
<br/>
              5. Aux termes de l'article L. 262 du code électoral, applicable aux communes de 1 000 habitants et plus : " Au premier tour de scrutin, il est attribué à la liste qui a recueilli la majorité absolue des suffrages exprimés un nombre de sièges égal à la moitié du nombre des sièges à pourvoir, arrondi, le cas échéant, à l'entier supérieur lorsqu'il y a plus de quatre sièges à pourvoir et à l'entier inférieur lorsqu'il y a moins de quatre sièges à pourvoir. Cette attribution opérée, les autres sièges sont répartis entre toutes les listes à la représentation proportionnelle suivant la règle de la plus forte moyenne, sous réserve de l'application des dispositions du troisième alinéa ci-après. / Si aucune liste n'a recueilli la majorité absolue des suffrages exprimés au premier tour, il est procédé à un deuxième tour. (...) ". <br/>
<br/>
              6. Ni par ces dispositions, ni par celles de la loi du 23 mars 2020, le législateur n'a subordonné à un taux de participation minimal la répartition des sièges au conseil municipal à l'issue du premier tour de scrutin dans les communes de mille habitants et plus, lorsqu'une liste a recueilli la majorité absolue des suffrages exprimés. Le niveau de l'abstention n'est ainsi, par lui-même, pas de nature à remettre en cause les résultats du scrutin, s'il n'a pas altéré, dans les circonstances de l'espèce, sa sincérité.<br/>
<br/>
              7. En l'espèce, M. B... fait valoir que le taux d'abstention s'est élevé à 69,7 % dans la commune de Sanary-sur-Mer, que cette abstention, qui est à la fois supérieure à la moyenne nationale et à celle observée lors des élections municipales de 2014, résulte des craintes suscitées par le coronavirus, a affecté davantage les personnes âgées, et que le vote par procuration a pu être compliqué par la fermeture du commissariat de police de la commune lors des jours précédant le scrutin. Il ne soutient cependant pas que le vote par procuration aurait été rendu impossible dans la commune, et les autres circonstances invoquées ne font pas davantage apparaître qu'il aurait été porté atteinte au libre exercice du droit de vote ou à l'égalité entre les électeurs ou entre les candidats.<br/>
<br/>
              8. Dans ces conditions, le niveau de l'abstention constatée ne peut être regardé comme ayant altéré la sincérité du scrutin.<br/>
<br/>
              9. Par suite, M. B... n'est pas fondé à soutenir que c'est à tort que la présidente de la 4ème chambre du tribunal administratif de Toulon, qui n'a ni méconnu les termes de sa protestation, ni entaché son ordonnance de défaut de réponse à moyen, a rejeté sa demande tendant à l'annulation des opérations électorales qui se sont déroulées le 15 mars 2020 dans la commune de Sanary-sur-Mer. <br/>
<br/>
              Sur les conclusions de M. B... tendant au remboursement intégral de ses dépenses de campagne liées aux opérations électorales :<br/>
<br/>
              10. Il résulte de ce qui précède que les conclusions de M. B... tendant, par voie de conséquence, au remboursement intégral de ses dépenses de campagne liées aux opérations électorales doivent, en tout état de cause, être rejetées.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : La requête de M. B... est rejetée. <br/>
<br/>
Article 2 : La présente décision sera notifiée à M. D... B..., à M. E... C..., à M. F... A... et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
