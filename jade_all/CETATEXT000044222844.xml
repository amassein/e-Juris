<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044222844</ID>
<ANCIEN_ID>JG_L_2021_10_000000447202</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/22/28/CETATEXT000044222844.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère chambre, 18/10/2021, 447202, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-10-18</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>447202</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Manon Chonavel</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Vincent Villette</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:447202.20211018</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              Par une requête, enregistrée le 3 décembre 2020 au secrétariat du contentieux du Conseil d'Etat, le syndicat des médecins d'Aix et Région (SMAER) demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret n° 2020-1215 du 2 octobre 2020 relatif à la procédure applicable aux refus de soins discriminatoires et aux dépassements d'honoraires abusifs ou illégaux ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la Constitution ;<br/>
              - le code de la santé publique ;<br/>
              - le code de la sécurité sociale ; <br/>
              - le code de la justice administrative ; <br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Manon Chonavel, auditrice,  <br/>
<br/>
              - les conclusions de M. Vincent Villette, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Le syndicat des médecins d'Aix et Région demande l'annulation pour excès de pouvoir du décret du 2 octobre 2020 relatif à la procédure applicable aux refus de soins discriminatoires et aux dépassements d'honoraires abusifs ou illégaux, pris en application de l'article L. 1110-3 du code de la santé publique aux termes duquel : " Aucune personne ne peut faire l'objet de discriminations dans l'accès à la prévention ou aux soins./  Un professionnel de santé ne peut refuser de soigner une personne pour l'un des motifs visés au premier alinéa de l'article 225-1 ou à l'article 225-1-1 du code pénal [définissant les discriminations] ou au motif qu'elle est bénéficiaire de la protection complémentaire en matière de santé prévue à l'article L. 861-1 du code de la sécurité sociale [c'est-à-dire la complémentaire santé solidaire], ou du droit à l'aide prévue à l'article L. 251-1 du code de l'action sociale et des familles [c'est-à-dire de l'aide médicale de l'Etat]./Toute personne qui s'estime victime d'un refus de soins illégitime peut saisir le directeur de l'organisme local d'assurance maladie ou le président du conseil territorialement compétent de l'ordre professionnel concerné des faits qui permettent d'en présumer l'existence. Cette saisine vaut dépôt de plainte (...) /Hors cas de récidive, une conciliation est menée (...). / En cas d'échec de la conciliation, ou en cas de récidive, le président du conseil territorialement compétent transmet la plainte à la juridiction ordinale compétente avec son avis motivé et en s'y associant le cas échéant. / En cas de carence du conseil territorialement compétent, dans un délai de trois mois, le directeur de l'organisme local d'assurance maladie peut prononcer à l'encontre du professionnel de santé une sanction dans les conditions prévues à l'article L. 162-1-14-1 du code de la sécurité sociale. (..) Les modalités d'application du présent article sont fixées par voie réglementaire. "<br/>
<br/>
              2. Le syndicat requérant soutient qu'il incombait au pouvoir réglementaire, en prenant, par le décret attaqué, les mesures qu'impliquent nécessairement l'article L. 1110-3 du code de la santé publique, de mettre en œuvre, pour éviter toute rupture d'égalité entre les médecins selon la convention qu'ils ont conclue avec les organismes d'assurance maladie, la compétence qu'il tient de l'article L. 162-5 du code de la sécurité sociale, afin de modifier dans le même temps l'article 69 de la convention nationale organisant les rapports entre les médecins libéraux et l'assurance maladie signée le 25 août 2016, afin que tous les médecins concernés puissent bénéficier, pour la prise en charge des patients bénéficiaires de la complémentaire santé solidaire, à l'égard desquels ils sont tenus en vertu de l'article L. 162-5-13 du code de la sécurité sociale de ne pas dépasser les tarifs conventionnels, de l'avantage accordé aux médecins pratiquant des honoraires opposables par cet article de la convention, lequel prévoit que : " les caisses d'assurance maladie participent au financement des cotisations dues par les médecins conventionnés en secteur à honoraires opposables pour les risques maladie, maternité, décès, allocations familiales et allocation supplémentaire vieillesse. (...) La participation de l'assurance maladie pour les risques maladie, maternité, décès, allocations familiales et allocation supplémentaire vieillesse est assise sur les revenus acquis au titre de l'activité libérale effectuée dans le cadre de la présente convention à l'exclusion des dépassements d'honoraires ".<br/>
<br/>
              3. Toutefois, contrairement à ce que soutient le syndicat requérant, le pouvoir réglementaire ne tenait, ni des dispositions de l'article L. 1110-3 du code de la santé publique pour l'application desquelles le décret attaqué a été pris, dispositions qui se bornent à interdire les discriminations dans l'accès à la prévention ou aux soins et à prévoir la procédure applicable en cas de plainte pour refus illégitime de soins auprès de l'organisme local d'assurance maladie ou de l'ordre professionnel compétent, ni des dispositions de l'article L. 162-5 du code de la sécurité sociale, prévoyant que " les rapports entre les organismes d'assurance maladie et les médecins sont définis par des conventions nationales (...) ", conventions qu'il revient seulement en vertu de l'article L. 162-15 de ce code aux ministres chargés de la santé et de la sécurité sociale d'approuver dans les vingt et un jours de leur réception, le cas échéant en en disjoignant alors les dispositions divisibles non conformes aux lois et règlements en vigueur, compétence pour modifier la convention nationale organisant les rapports entre les médecins libéraux et l'assurance maladie signée le 25 août 2016, en particulier son article 69. Au demeurant, le syndicat requérant ne saurait utilement soutenir que le décret attaqué, qui a pour objet de prendre les mesures réglementaires qu'implique nécessairement l'application de l'article L. 1110-3 du code de la santé publique, serait illégal faute pour le pouvoir réglementaire d'avoir exercé de façon plus étendue une compétence qu'il aurait détenue.<br/>
<br/>
              4. Il résulte de tout ce qui précède que le syndicat requérant n'est pas fondé à demander l'annulation du décret qu'il attaque. Par suite, sa requête, y compris les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative, doit être rejetée.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête du syndicat des médecins d'Aix et Région est rejetée.<br/>
<br/>
Article 2 : La présente décision sera notifiée au syndicat des médecins d'Aix et Région et au ministre des solidarités et de la santé.<br/>
Copie en sera adressée au Premier ministre.<br/>
              Délibéré à l'issue de la séance du 23 septembre 2021 où siégeaient : Mme Gaëlle Dumortier, présidente de chambre, présidant ; M. Damien Botteghi, conseiller d'Etat et Mme Manon Chonavel, auditrice-rapporteure. <br/>
<br/>
              Rendu le 18 octobre 2021.<br/>
                 La présidente : <br/>
                 Signé : Mme Gaëlle Dumortier<br/>
 		La rapporteure : <br/>
      Signé : Mme Manon Chonavel<br/>
                 La secrétaire :<br/>
                 Signé : Mme A... B...<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
