<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000025469044</ID>
<ANCIEN_ID>JG_L_2012_03_000000332805</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/25/46/90/CETATEXT000025469044.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème et 8ème sous-sections réunies, 07/03/2012, 332805, Publié au recueil Lebon</TITRE>
<DATE_DEC>2012-03-07</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>332805</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème et 8ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Christian Vigouroux</PRESIDENT>
<AVOCATS/>
<RAPPORTEUR>Mme Emilie Bokdam-Tognetti</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Vincent Daumas</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2012:332805.20120307</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 16 octobre 2009 au secrétariat du contentieux du Conseil d'Etat, présentée par le MOUVEMENT POUR LES DROITS ET LE RESPECT DES GENERATIONS FUTURES, dont le siège est 935 rue de la Montagne à Ons-en-Braye (60650), représenté par son président, et M. A, demeurant ... ; le MOUVEMENT POUR LES DROITS ET LE RESPECT DES GENERATIONS FUTURES et autre demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision implicite par laquelle le ministre chargé de l'agriculture a rejeté leur demande d'abrogation de la décision n° 2010321 autorisant la mise sur le marché du produit phytopharmaceutique Roundup Express ; <br/>
<br/>
              2°) d'enjoindre au ministre chargé de l'agriculture, à titre principal, d'abroger l'autorisation de mise sur le marché du produit phytopharmaceutique Roundup Express dans un délai d'un mois à compter de la décision à intervenir, sous astreinte de 15 000 euros par jour de retard, ou à titre subsidiaire, de réexaminer leur demande d'abrogation ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 8 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 16 février 2012, présentée par LE MOUVEMENT POUR LES DROITS ET LE RESPECT DES GENERATIONS FUTURES et autres ;<br/>
<br/>
              Vu la directive 91/414/CEE du Conseil du 15 juillet 1991 modifiée ;<br/>
<br/>
              Vu la directive  20085/127/CE de la Commission du 18 décembre 2008 ;<br/>
<br/>
              Vu le code rural ;<br/>
<br/>
              Vu l'arrêté interministériel du 6 septembre 1994 portant application du décret n° 94-359 du 5 mai 1994 relatif au contrôle des produits phytopharmaceutiques ;<br/>
<br/>
              Vu l'arrêté du 6 octobre 2004 relatif aux conditions d'autorisation et d'utilisation de la mention " emploi autorisé dans les jardins " pour les produits phytopharmaceutiques ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Emilie Bokdam-Tognetti, Auditeur,  <br/>
<br/>
              - les conclusions de M. Vincent Daumas, rapporteur public ;<br/>
<br/>
<br/>
<br/>Considérant qu'aux termes de l'article L. 253-4 du code rural dans sa rédaction à la date de la décision attaquée : " A l'issue d'une évaluation des risques et des bénéfices que présente le produit [phytopharmaceutique], l'autorisation de mise sur le marché est délivrée par l'autorité administrative après avis de l'Agence française de sécurité sanitaire des aliments, si les substances actives contenues dans ce produit sont inscrites sur la liste communautaire des substances actives, à l'exception de celles bénéficiant d'une dérogation prévue par la réglementation communautaire, et si l'instruction de la demande d'autorisation révèle l'innocuité du produit à l'égard de la santé publique et de l'environnement, son efficacité et sa sélectivité à l'égard des végétaux et produits végétaux dans les conditions d'emploi prescrites. / L'autorisation peut être retirée s'il apparaît, après nouvel examen, que le produit ne satisfait pas aux conditions définies au premier alinéa (...) " ; que l'article R. 253-38 du code rural prévoit que l'autorisation de mise sur le marché d'un produit phytopharmaceutique est délivrée pour dix ans par le ministre chargé de l'agriculture, après avis de l'Agence française de sécurité sanitaire des aliments (AFSSA) ; que l'article R. 253-46 du même code dispose que : " L'autorisation de mise sur le marché peut être retirée ou modifiée par le ministre chargé de l'agriculture, le cas échéant après avis de l'Agence française de sécurité sanitaire des aliments. / L'autorisation de mise sur le marché d'un produit phytopharmaceutique est retirée : / 1° Si les conditions requises pour son obtention ne sont plus remplies ; / 2° Ou si des indications fausses ou fallacieuses ont été fournies dans la demande d'autorisation (...) " ; <br/>
<br/>
              Considérant qu'il résulte de ces dispositions que le ministre chargé de l'agriculture ne peut autoriser la mise sur le marché d'un produit phytopharmaceutique que si les substances actives contenues dans ce produit sont inscrites sur la liste communautaire des substances actives autorisées ou  bénéficient d'une dérogation prévue par la réglementation communautaire, et après que l'instruction de la demande d'autorisation a établi l'innocuité, l'efficacité et la sélectivité du produit ; que l'autorisation de mise sur le marché délivrée à un produit peut être renouvelée à l'expiration d'un délai de dix ans, si les conditions requises pour son obtention sont toujours remplies, mais doit être abrogée dans le cas contraire ; <br/>
<br/>
              Considérant qu'il ressort des pièces du dossier que les risques liés à l'utilisation de la préparation phytopharmaceutique Roundup Express de la société Monsanto Agriculture France SAS, qui disposait d'une autorisation de mise sur le marché n° 2010321, ont fait l'objet d'une réévaluation à la suite de l'inscription du glyphosate, substance qui entre dans la composition de cet herbicide en tant que substance active, sur la liste communautaire des substances actives autorisées à l'annexe I de la directive 91/414/CEE du Conseil du 15 juillet 1991 concernant la mise sur le marché des produits phytopharmaceutiques ; qu'après que l'AFSSA a, le 16 avril 2007, émis un avis favorable, le ministre chargé de l'agriculture a renouvelé l'autorisation de la préparation Roundup Express ; qu'à la suite de la publication de travaux scientifiques sur les effets du glyphosate et des préparations à base de cette substance dans la revue " Chemical Research in Toxicology ", le ministre chargé de l'agriculture a, le 28 janvier 2009, saisi l'AFSSA d'une nouvelle demande d'avis ; que, par un avis du 26 mars 2009, l'AFSSA a estimé que les résultats avancés dans ces publications n'apportaient pas de nouveaux éléments pertinents de nature à remettre en cause les conclusions de l'évaluation européenne du glyphosate ni celles de l'évaluation nationale des préparations contenant cette substance ; que, par un courrier du 16 juin 2009, reçu le 18 juin suivant, le MOUVEMENT POUR LES DROITS ET LE RESPECT DES GENERATIONS FUTURES (MDRGF) et M. A ont demandé au ministre de l'agriculture et de la pêche d'abroger la décision n° 2010321 autorisant la mise sur le marché du produit Roundup Express ;  que le silence gardé pendant plus de deux mois par le ministre sur cette demande a fait naître une décision implicite de rejet ; que le MDRGF et M. A demandent l'annulation pour excès de pouvoir de cette décision ;<br/>
<br/>
              Sans qu'il soit besoin d'examiner les autres moyens de la requête ;<br/>
<br/>
              Considérant que l'article L. 253-1 du code rural, pris pour la transposition de l'article 2 de la directive 91/414/CEE du Conseil du 15 juillet 1991, définit les produits phytopharmaceutiques comme " les préparations contenant une ou plusieurs substances actives et les produits composés en tout ou partie d'organismes génétiquement modifiés présentés sous la forme dans laquelle ils sont livrés à l'utilisateur final, destinés à : / a) Protéger les végétaux ou produits végétaux contre tous les organismes nuisibles ou à prévenir leur action ; / b) Exercer une action sur les processus vitaux des végétaux, dans la mesure où il ne s'agit pas de substances nutritives ; / c) Assurer la conservation des produits végétaux, à l'exception des substances et produits faisant l'objet d'une réglementation communautaire particulière relative aux agents conservateurs ; / d) Détruire les végétaux indésirables ; / e) Détruire des parties de végétaux, freiner ou prévenir une croissance indésirable des végétaux " ; que l'article 2 de la même directive définit par ailleurs les " substances " comme " les éléments chimiques et leurs composés tels qu'ils se présentent à l'état naturel ou tels que produits par l'industrie, incluant toute impureté résultant inévitablement du procédé de fabrication ", et les " substances actives " comme " les substances ou micro-organismes, y compris les virus exerçant une action générale ou spécifique : / 4.1. sur les organismes nuisibles ou / 4.2. sur les végétaux, parties de végétaux ou produits végétaux. " ; que le point 3.1 de l'annexe II à cette directive, repris au point 3.1 de l'annexe I à l'arrêté interministériel du 6 septembre 1994 portant application du décret n° 94-359 du 5 mai 1994 relatif au contrôle des produits phytopharmaceutiques, impose de préciser, dans le dossier de demande d'inscription d'une substance active sur la liste des substances actives autorisées, la " fonction " de cette substance active parmi les fonctions " acaricide, bactéricide, fongicide, herbicide, insecticide, molluscicide, nématicide, régulateur de croissance végétale, répulsif, rodenticide, médiateur chimique, taupicide, virucide, autres (à préciser) " ; que les points 1.4.2 à 1.4.4 de l'annexe III à la directive 91/414/CEE, transposés aux points 1.4.2 à 1.4.4 de l'annexe II à l'arrêté du 6 septembre 1994, prévoient par ailleurs que doivent être indiquées, dans le dossier de demande d'autorisation d'un produit phytopharmaceutique, la composition de la préparation, la concentration de la substance active et celle des " autres produits ", et qu'il y a lieu d'identifier ces autres produits de la formule en précisant leur " fonction ..:/ - adhésif, / - agent antimoussant, / - antigel, / - liant, / - tampon, / - agent porteur, / - déodorant, / - agent dispersant, / - teinture, / - émétique, / - émulsifiant, / - fertilisant, / - conservateur, /(...)/ - agent mouillant/ , - divers (à spécifier). " ; <br/>
<br/>
              Considérant qu'il résulte de ces dispositions qu'une substance qui n'exerce dans une préparation phytopharmaceutique donnée, eu égard notamment à ses caractéristiques propres et à son degré de concentration dans ce produit, aucune des fonctions qui caractérisent une " action générale ou spécifique " sur des végétaux ou organismes cibles, mais permet seulement d'obtenir, en remplissant l'une des fonctions énumérées au point 1.4.4 de l'annexe III à la directive 91/414/CEE transposé au point 1.4.4 de l'annexe II à l'arrêté du 6 septembre 1994, une certaine forme de cette préparation, par exemple un simple agent " mouillant ", ne constitue pas une " substance active " de cette préparation au sens de l'article L. 253-1 du code rural ; que le ministre soutient que l'acide pélargonique, alors même qu'il est inscrit sur la liste des substances actives autorisées figurant à l'annexe I de la directive 91/414/CEE du Conseil du 15 juillet 1991 et qu'il doit donc en principe être regardé comme une substance active, ne devrait pas, compte tenu de son degré de concentration dans la préparation Roundup Express, être considéré comme une substance active de cette préparation au sens des dispositions précitées ; qu'il incombe en tout état de cause au ministre, dans l'hypothèse où il estime qu'une substance inscrite sur la liste des substances actives autorisées qui constitue l'un des composants d'une préparation ne remplit pas, dans cette préparation, l'une des fonctions qui caractérisent une des " actions générales ou spécifiques " mentionnées ci-dessus et qu'elle n'y est donc pas " active ", de l'établir ; qu'à défaut, sa décision est entachée d'erreur de droit ;  <br/>
<br/>
              Considérant qu'il est constant que figure dans la composition déclarée de la préparation Roundup Express, à hauteur d'environ 1% de sa masse pondérale, de l'acide pélargonique ; que cette substance a été inscrite sur la liste des substances actives autorisées à compter du 1er septembre 2009 par la directive 2008/127/CE de la Commission du 18 décembre 2008 modifiant la directive 91/414/CEE du Conseil en vue d'y inscrire plusieurs substances actives ; que cette substance était utilisée comme substance active dans des produits phytopharmaceutiques déjà sur le marché deux ans après la date de notification de la directive 91/414/CEE et figurait, à la date à laquelle le ministre a renouvelé l'autorisation de mise sur le marché de la préparation Roundup Express, sur la liste des substances actives faisant l'objet d'un réexamen graduel par la Commission européenne et pouvant être utilisées dans des produits phytopharmaceutiques à titre dérogatoire dans l'attente de l'achèvement de ce réexamen ; qu'ainsi, il était notoire que cette substance pouvait présenter les caractéristiques d'une substance active d'un produit phytopharmaceutique ; <br/>
<br/>
              Considérant que l'avis du 16 avril 2007 de l'AFSSA ne fait aucune mention de la présence d'acide pélargonique dans la préparation Roundup Express, ni à titre de substance active, ni à titre de coformulant, et se borne à affirmer que la seule substance active de ce produit est le glyphosate ; que l'avis du 26 mars 2009 de l'Agence ne mentionne pas davantage l'acide pélargonique ni n'analyse les effets de cet acide dans la préparation Roundup Express ; que, en l'absence de tout autre élément de nature à établir que l'acide pélargonique ne remplirait pas les fonctions d'une substance active dans cette préparation, la décision du ministre ne saurait être regardée comme ayant mis en oeuvre une méthode d'évaluation appropriée, prenant en compte l'ensemble des éléments nécessaires à la délivrance ou au maintien de l'autorisation de mise sur le marché de la préparation Roundup Express ; que la décision attaquée, par laquelle le ministre a rejeté la demande d'abrogation de cette autorisation, est, dès lors, entachée d'erreur de droit et doit, par suite, être annulée ;<br/>
<br/>
              Sur les conclusions aux fins d'injonction :<br/>
<br/>
              Considérant que l'annulation de la décision par laquelle le ministre chargé de l'agriculture a rejeté la demande d'abrogation de l'autorisation de mise sur le marché de la préparation Roundup Express présentée par le MDRGF et M. A n'implique pas nécessairement, eu égard aux motifs retenus ci-dessus, l'abrogation de cette autorisation, mais implique seulement que cette demande d'abrogation soit réexaminée ; qu'il y a lieu, par suite, d'enjoindre au ministre chargé de l'agriculture de procéder, après avis de l'AFSSA, à ce réexamen dans un délai de six  mois à compter de la notification de la présente décision ; que dans les circonstances de l'espèce, il n'y a pas lieu d'assortir cette injonction de l'astreinte demandée par les requérants ;<br/>
<br/>
              Sur les conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat le versement au MOUVEMENT POUR LES DROITS ET LE RESPECT DES GENERATIONS FUTURES, désormais dénommé ASSOCIATION GENERATIONS FUTURES, et à M. A, d'une somme globale de 3000 euros  au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La décision implicite par laquelle le ministre chargé de l'agriculture a rejeté la demande présentée par le MOUVEMENT POUR LES DROITS ET LE RESPECT DES GENERATIONS FUTURES et M. A tendant à l'abrogation de la décision n° 2010321 autorisant la mise sur le marché du produit phytopharmaceutique Roundup Express est annulée.<br/>
Article 2 : Il est enjoint au ministre de l'agriculture, de l'alimentation, de la pêche, de la ruralité et de l'aménagement du territoire de procéder au réexamen de la demande présentée par le MOUVEMENT POUR LES DROITS ET LE RESPECT DES GENERATIONS FUTURES et M. A dans un délai de six mois à compter de la notification de la présente décision.<br/>
Article 3 : L'Etat versera à l'ASSOCIATION GENERATIONS FUTURES et M. A une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Le surplus des conclusions de la requête est rejeté.<br/>
Article 5 : La présente décision sera notifiée à l'ASSOCIATION GENERATIONS FUTURES, à M. A, au ministre de l'agriculture, de l'alimentation, de la pêche, de la ruralité et de l'aménagement du territoire et à la société Monsanto SAS.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">03-11 AGRICULTURE, CHASSE ET PÊCHE. - AUTORISATION DE MISE SUR LE MARCHÉ - PORTÉE DE L'INSCRIPTION SUR LA LISTE DES SUBSTANCES ACTIVES (ART. L. 253-1 DU CODE RURAL).
</SCT>
<ANA ID="9A"> 03-11 Il résulte des dispositions de l'article L. 253-1 du code rural et de la directive 91/414/CEE du 15 juillet 1991 qu'une substance qui n'exerce dans une préparation phytopharmaceutique donnée, eu égard notamment à ses caractéristiques propres et à son degré de concentration dans ce produit, aucune des fonctions qui caractérisent une « action générale ou spécifique » sur des végétaux ou organismes cibles, mais permet seulement d'obtenir, en remplissant l'une des fonctions énumérées au point 1.4.4 de l'annexe III à la directive précitée et transposé au point 1.4.4 de l'annexe II à l'arrêté du 6 septembre 1994, une certaine forme de cette préparation, par exemple un simple agent « mouillant », ne constitue pas une « substance active » de cette préparation au sens de l'article L. 253-1 du code rural. Mais il incombe en tout état de cause au ministre, dans l'hypothèse où il estime qu'une substance inscrite sur la liste des substances actives autorisées qui constitue l'un des composants d'une préparation ne remplit pas, dans cette préparation, l'une des fonctions qui caractérisent une des « actions générales ou spécifiques » mentionnées ci-dessus et qu'elle n'y est donc pas « active », de l'établir.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
