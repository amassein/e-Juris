<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036606821</ID>
<ANCIEN_ID>JG_L_2018_02_000000403650</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/60/68/CETATEXT000036606821.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère et 4ème chambres réunies, 05/02/2018, 403650</TITRE>
<DATE_DEC>2018-02-05</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>403650</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère et 4ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DELAMARRE, JEHANNIN ; SCP GARREAU, BAUER-VIOLAS, FESCHOTTE-DESBOIS</AVOCATS>
<RAPPORTEUR>Mme Florence Marguerite</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Rémi Decout-Paolini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:403650.20180205</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme C...B...a demandé au tribunal administratif de Lille d'annuler :<br/>
              - la décision en date du 5 février 2015 par laquelle le président du conseil général du Pas-de-Calais a rejeté son recours administratif préalable dirigé contre la décision de la caisse d'allocations familiales de ce département en date du 1er octobre 2014 mettant fin à ses droits à l'allocation de revenu de solidarité active ;<br/>
              - la décision implicite par laquelle le président du conseil général du Pas-de-Calais a rejeté son recours administratif préalable dirigé contre la décision de la caisse d'allocations familiales de ce département en date du 21 octobre 2014 lui réclamant un trop-perçu d'allocation de revenu de solidarité active d'un montant 14 460, 86 euros pour la période du 1er octobre 2011 au 31 juillet 2014, ainsi que sa demande de remise gracieuse ; <br/>
              - les titres exécutoires émis à son encontre le 20 février 2015 par le département du Pas-de-Calais pour des montants de 4 994,31 et 9 466,55 euros ;<br/>
              - l'opposition à tiers détenteur émise le 21 avril 2015 par la paierie départementale du Pas-de-Calais pour un montant de 14 460, 86 euros.<br/>
<br/>
              Par un jugement n°s 1503642, 1504284 et 1504700 du 13 juillet 2016, le tribunal administratif de Lille a rejeté les demandes de Mme B... .  <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 20 septembre et 16 décembre 2016 au secrétariat du contentieux du Conseil d'Etat, Mme B...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement du tribunal administratif de Lille du 13 juillet 2016 ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à ses demandes ;<br/>
<br/>
              3°) de mettre à la charge du département du Pas-de-Calais la somme de 3 000 euros à verser à son avocat, MeD..., au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'action sociale et des familles ;<br/>
              - le code général des collectivités territoriales ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Florence Marguerite, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Rémi Decout-Paolini, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCPD..., Jéhannin, avocat de Mme C...B...et à la SCP Garreau, Bauer-Violas, Feschotte-Desbois, avocat du département du Pas-de-Calais.<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis au juge du fond que, par deux décisions des 1er et 21 octobre 2014, la caisse d'allocations familiales du Pas-de-Calais a mis fin aux droits de Mme B...au revenu de solidarité active et lui a notifié un indu d'un montant de 14 460,86 euros correspondant à l'allocation qu'elle avait perçue à ce titre du 1er octobre 2011 au 31 juillet 2014. Le 20 février 2015, deux titres exécutoires ont été émis par le département du Pas-de-Calais en recouvrement de cet indu puis, le 21 avril 2015, la paierie départementale du Pas-de-Calais a délivré au Crédit mutuel une opposition à tiers détenteur. Par trois requêtes distinctes, Mme B...a saisi le tribunal administratif de Lille de conclusions tendant à l'annulation, d'une part, des décisions expresse et implicite par lesquelles le président du conseil général du Pas-de-Calais a rejeté ses recours administratifs préalables contre des décisions de la caisse d'allocations familiales et, d'autre part, des titres exécutoires et de l'opposition à tiers détenteur. Par un jugement du 13 juillet 2016, le tribunal administratif de Lille a joint les requêtes de Mme B...et les a rejetées. Mme B...se pourvoit en cassation contre ce jugement.<br/>
<br/>
              Sur le jugement attaqué, en tant qu'il rejette les conclusions dirigées contre les décisions du président du conseil général du Pas-de-Calais et les titres exécutoires : <br/>
<br/>
              2. Aux termes de l'article L. 262-2 du code de l'action sociale et des familles, dans sa rédaction applicable au litige : " Toute personne résidant en France de manière stable et effective, dont le foyer dispose de ressources inférieures à un niveau garanti, a droit au revenu de solidarité active dans les conditions définies au présent chapitre. / Le revenu garanti est calculé, pour chaque foyer, en faisant la somme : / 1° D'une fraction des revenus professionnels des membres du foyer ; / 2° D'un montant forfaitaire, dont le niveau varie en fonction de la composition du foyer et du nombre d'enfants à charge. / Le revenu de solidarité active est une allocation qui porte les ressources du foyer au niveau du revenu garanti (...) ". L'article L. 262-9 du même code, dans sa rédaction issue de la loi du 21 décembre 2011 de financement de la sécurité sociale pour 2012, précise, à propos de la majoration au profit de la personne isolée assumant la charge d'un ou de plusieurs enfants  : " Est considérée comme isolée une personne veuve, divorcée, séparée ou célibataire, qui ne vit pas en couple de manière notoire et permanente et qui notamment ne met pas en commun avec un conjoint, concubin ou partenaire de pacte civil de solidarité ses ressources et ses charges (...) ". Aux termes de l'article 515-8 du code civil : " Le concubinage est une union de fait, caractérisée par une vie commune présentant un caractère de stabilité et de continuité, entre deux personnes, de sexe différent ou de même sexe, qui vivent en couple ". Le premier alinéa de l'article R. 262-1 du code de l'action sociale et des familles, dans sa rédaction applicable au litige, précise le niveau du montant forfaitaire mentionné au 2° de l'article L. 262-2 selon la composition du foyer, lequel peut comprendre, outre le bénéficiaire de l'allocation, son conjoint, son partenaire lié par un pacte civil de solidarité ou son concubin et les personnes présentes au foyer et à la charge de l'intéressé. L'article R. 262-3 du même code précise enfin que : " Pour le bénéfice du revenu de solidarité active, sont considérés comme à charge : / 1° Les enfants ouvrant droit aux prestations familiales ; / 2° Les autres enfants et personnes de moins de vingt-cinq ans qui sont à la charge effective et permanente du bénéficiaire à condition, lorsqu'ils sont arrivés au foyer après leur dix-septième anniversaire, d'avoir avec le bénéficiaire ou son conjoint, son concubin ou le partenaire lié par un pacte civil de solidarité un lien de parenté jusqu'au quatrième degré inclus (...) ".  <br/>
<br/>
              3. Il résulte de ces dispositions que, pour le bénéfice du revenu de solidarité active, le foyer s'entend du demandeur, ainsi que, le cas échéant, de son conjoint, partenaire lié par un pacte civil de solidarité ou concubin et des enfants ou personnes de moins de vingt-cinq ans à charge qui remplissent les conditions précisées par l'article R. 262-3 du code de l'action sociale et des familles. Pour l'application de ces dispositions, le concubin est la personne qui mène avec le demandeur une vie de couple stable et continue. Une telle vie de couple peut être établie par un faisceau d'indices concordants, au nombre desquels la circonstance que les intéressés mettent en commun leurs ressources et leurs charges.<br/>
<br/>
              4. Pour juger que Mme B... et M. A... formaient un foyer au sens des dispositions de l'article L. 262-2 du code de l'action sociale et des familles, le tribunal administratif de Lille a notamment relevé, en premier lieu, qu'aux termes du rapport établi le 5 août 2014 par un agent assermenté de la caisse d'allocations familiales du Pas-de-Calais, il avait été constaté que M. A... était domicilié chez B... depuis 2008 et qu'ils étaient regardés comme un couple par les services fiscaux et sociaux et, en second lieu, que la requérante n'apportait aucun élément de nature à remettre sérieusement en cause ces constatations à l'appui de ses allégations selon lesquelles cette cohabitation n'était pas de nature maritale Ce faisant, le tribunal administratif a pu, sans inverser la charge de la preuve ni méconnaître son office en matière d'instruction, estimer, au terme d'une appréciation souveraine des pièces du dossier exempte de dénaturation, que la réalité d'une vie de couple stable et continue entre Mme B...et M. A... devait être regardée comme établie. <br/>
<br/>
              Sur le jugement attaqué, en tant qu'il rejette les conclusions de Mme B...dirigées contre l'opposition à tiers détenteur : <br/>
<br/>
              5. Aux termes de l'article L. 262-46 du code de l'action sociale et des familles, dans sa rédaction applicable au litige : " Tout paiement indu de revenu de solidarité active est récupéré par l'organisme chargé du service de celui-ci ainsi que, dans les conditions définies au présent article, par les collectivités débitrices du revenu de solidarité active. / Toute réclamation dirigée contre une décision de récupération de l'indu, le dépôt d'une demande de remise ou de réduction de créance ainsi que les recours administratifs et contentieux, y compris en appel, contre les décisions prises sur ces réclamations et demandes ont un caractère suspensif. / Sauf si le bénéficiaire opte pour le remboursement de l'indu en une seule fois, l'organisme mentionné au premier alinéa procède au recouvrement de tout paiement indu de revenu de solidarité active par retenues sur les montants à échoir. / A défaut, l'organisme mentionné au premier alinéa peut également, dans des conditions fixées par décret, procéder à la récupération de l'indu par retenues sur les échéances à venir dues au titre [d'autres] prestations (...). / (...) / Après la mise en oeuvre de la procédure de recouvrement sur prestations à échoir, l'organisme chargé du service du revenu de solidarité active transmet, dans des conditions définies par la convention mentionnée au I de l'article L. 262-25 du présent code, les créances du département au président du conseil général. (...) Le président du conseil général constate la créance du département et transmet au payeur départemental le titre de recettes correspondant pour le recouvrement (...) ". Aux termes de l'article L. 262-47 du même code, dans sa rédaction alors applicable : " Toute réclamation dirigée contre une décision relative au revenu de solidarité active fait l'objet, préalablement à l'exercice d'un recours contentieux, d'un recours administratif auprès du président du conseil général (...) ".  <br/>
<br/>
              6. Par ailleurs, aux termes de l'article L. 1617-5 du code général des collectivités territoriales : "(...) 1° En l'absence de contestation, le titre de recettes individuel ou collectif émis par la collectivité territoriale ou l'établissement public local permet l'exécution forcée d'office contre le débiteur. / Toutefois, l'introduction devant une juridiction de l'instance ayant pour objet de contester le bien-fondé d'une créance assise et liquidée par une collectivité territoriale ou un établissement public local suspend la force exécutoire du titre. (...) /  2° L'action dont dispose le débiteur d'une créance assise et liquidée par une collectivité territoriale ou un établissement public local pour contester directement devant la juridiction compétente le bien-fondé de ladite créance se prescrit dans le délai de deux mois suivant la réception du titre exécutoire ou, à défaut, du premier acte procédant de ce titre ou de la notification d'un acte de poursuite. / L'action dont dispose le débiteur de la créance visée à l'alinéa précédent pour contester directement devant le juge de l'exécution mentionné aux articles L. 213-5 et L. 213-6 du code de l'organisation judiciaire la régularité formelle de l'acte de poursuite diligenté à son encontre se prescrit dans le délai de deux mois suivant la notification de l'acte contesté (...) ". <br/>
<br/>
              7. Il résulte des dispositions citées au point 5 qu'une décision de récupération d'un indu de revenu de solidarité active prise par le président du conseil général, devenu départemental, ou par délégation de celui-ci ne peut, à peine d'irrecevabilité, faire l'objet d'un recours contentieux sans qu'ait été préalablement exercé un recours administratif auprès de cette autorité. Si la recevabilité d'un recours contentieux dirigé contre le titre exécutoire émis pour recouvrer un indu de revenu de solidarité active n'est pas, en vertu des dispositions citées au point 6, subordonnée à l'exercice d'un recours administratif préalable, le débiteur ne peut toutefois, à l'occasion d'un tel recours, contester devant le juge administratif le bien-fondé de cet indu en l'absence de tout recours préalable saisissant de cette contestation le président du conseil général. En revanche, une telle contestation reste possible à l'occasion d'un recours contre les actes de poursuite qui procèdent du titre exécutoire exercé conformément aux dispositions précitées de l'article L. 1617-5 du code général des collectivités territoriales, même en l'absence de recours administratif préalable.<br/>
<br/>
              8. Il résulte de ce qui précède que Mme B...est fondée à soutenir que le tribunal administratif de Lille a commis une erreur de droit en jugeant que ses conclusions dirigées contre l'opposition à tiers détenteur émise le 21 avril 2015 par le comptable public de la paierie départementale du Pas-de-Calais pour assurer le recouvrement de l'indu de revenu de solidarité active mis à sa charge étaient irrecevables au motif qu'elles n'avaient pas été elles-mêmes précédées du recours préalable prévu à l'article L. 262-47 du code de l'action sociale et des familles. <br/>
<br/>
              9. Toutefois, il résulte des dispositions précitées tant du deuxième alinéa de l'article L. 262-46 du code de l'action sociale et des familles que du deuxième alinéa du 1° de l'article L. 1617-5 du code général des collectivités territoriales que les recours administratifs et contentieux formés soit contre une décision de récupération d'un indu d'allocation de revenu de solidarité active, soit contre le titre exécutoire émis pour en assurer le recouvrement forcé suspendent l'exigibilité de la créance. Il s'ensuit que l'intervention d'un tel recours frappe de caducité les actes de poursuite qui ont pu être, le cas échéant, préalablement délivrés.<br/>
<br/>
              10. Il ressort des pièces du dossier soumis au juge du fond que, le 21 avril 2015, date à laquelle la paierie départementale du Pas-de-Calais a délivré au Crédit mutuel une opposition à tiers détenteur pour assurer le recouvrement de l'indu de revenu de solidarité active mis à la charge de MmeB..., son recours administratif en date du 18 février 2015, reçu par le président du conseil général le lendemain, devait être regardé comme ayant été rejeté par une décision implicite acquise le 19 avril 2015 et, par suite, que la créance était redevenue exigible. Toutefois, le recours contentieux que l'intéressée a formé, dès le 24 avril 2015, contre les titres exécutoires émis le 20 février 2015 a, de nouveau, suspendu l'exigibilité de la créance qui lui était réclamée. Dès lors, ses conclusions dirigées contre l'opposition à tiers détenteur étaient sans objet à la date d'introduction du recours contentieux qu'elle a formé contre cet acte, le 15 juin 2015, et, par suite, étaient irrecevables. Ce motif, exclusif de toute appréciation de fait de la part du juge de cassation, doit être substitué au motif, erroné en droit, du jugement attaqué, dont il justifie légalement le dispositif en tant qu'il rejette les conclusions de Mme B...dirigées contre l'opposition à tiers détenteur. <br/>
<br/>
              11. Il résulte de tout ce qui précède que le pourvoi de Mme B...doit être rejeté.<br/>
<br/>
              Sur les frais liés au litige : <br/>
<br/>
              12. Les dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 font obstacle à ce qu'une somme soit mise à ce titre à la charge du département du Pas-de-Calais, qui n'est pas, dans la présente instance, la partie perdante. Il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées au titre de l'article L. 761-1 du code de justice administrative par ce département.<br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				--------------<br/>
Article 1er : Le pourvoi de Mme B... est rejeté.<br/>
Article 2 : Les conclusions présentées par le département du Pas-de-Calais au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article  3: La présente décision sera notifiée à MmeC... B... et au département du Pas-de-Calais.<br/>
Copie en sera adressée à la ministre des solidarités et de la santé.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">04-02-06 AIDE SOCIALE. DIFFÉRENTES FORMES D'AIDE SOCIALE. REVENU MINIMUM D'INSERTION (RMI). - RSA - 1) RECOURS CONTRE LA DÉCISION DE RÉCUPÉRATION D'UN INDU DE RSA - RECEVABILITÉ SUBORDONNÉE À L'EXERCICE D'UN RAPO - EXISTENCE - 2) RECOURS CONTRE LE TITRE EXÉCUTOIRE ÉMIS POUR RECOUVRER UN INDU - RECEVABILITÉ SUBORDONNÉE À L'EXERCICE D'UN RAPO - ABSENCE - FACULTÉ DE CONTESTER, DANS LE CADRE DE CE RECOURS, LE BIEN-FONDÉ DE L'INDU - ABSENCE, SAUF SI UN RAPO A ÉTÉ EXERCÉ - 2) RECOURS CONTRE LES ACTES DE POURSUITES QUI PROCÈDENT DU TITRE EXÉCUTOIRE - RECEVABILITÉ SUBORDONNÉE À L'EXERCICE D'UN RAPO - ABSENCE - FACULTÉ DE CONTESTER, DANS LE CADRE DE CE RECOURS, LE BIEN-FONDÉ DE L'INDU - EXISTENCE, Y COMPRIS SI UN RAPO N'A PAS ÉTÉ EXERCÉ.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-01-02-01 PROCÉDURE. INTRODUCTION DE L'INSTANCE. LIAISON DE L'INSTANCE. RECOURS ADMINISTRATIF PRÉALABLE. - RSA - 1) RECOURS CONTRE LA DÉCISION DE RÉCUPÉRATION D'UN INDU DE RSA - RECEVABILITÉ SUBORDONNÉE À L'EXERCICE D'UN RAPO - EXISTENCE - 2) RECOURS CONTRE LE TITRE EXÉCUTOIRE ÉMIS POUR RECOUVRER UN INDU - RECEVABILITÉ SUBORDONNÉE À L'EXERCICE D'UN RAPO - ABSENCE - FACULTÉ DE CONTESTER, DANS LE CADRE DE CE RECOURS, LE BIEN-FONDÉ DE L'INDU - ABSENCE, SAUF SI UN RAPO A ÉTÉ EXERCÉ - 2) RECOURS CONTRE LES ACTES DE POURSUITES QUI PROCÈDENT DU TITRE EXÉCUTOIRE - RECEVABILITÉ SUBORDONNÉE À L'EXERCICE D'UN RAPO - ABSENCE - FACULTÉ DE CONTESTER, DANS LE CADRE DE CE RECOURS, LE BIEN-FONDÉ DE L'INDU - EXISTENCE, Y COMPRIS SI UN RAPO N'A PAS ÉTÉ EXERCÉ.
</SCT>
<ANA ID="9A"> 04-02-06 1) Il résulte des articles L. 262-46 et L. 262-47 du code de l'action sociale et des familles (CASF) qu'une décision de récupération d'un indu de revenu de solidarité active (RSA) prise par le président du conseil général, devenu départemental, ou par délégation de celui-ci ne peut, à peine d'irrecevabilité, faire l'objet d'un recours contentieux sans qu'ait été préalablement exercé un recours administratif auprès de cette autorité.... ,,2) Si la recevabilité d'un recours contentieux dirigé contre le titre exécutoire émis pour recouvrer un indu de RSA n'est pas, en vertu de l'article L. 1617-5 du code général des collectivités territoriales (CGCT), subordonnée à l'exercice d'un recours administratif préalable, le débiteur ne peut toutefois, à l'occasion d'un tel recours, contester devant le juge administratif le bien-fondé de cet indu en l'absence de tout recours préalable saisissant de cette contestation le président du conseil général.... ,,3) En revanche, une telle contestation reste possible à l'occasion d'un recours contre les actes de poursuite qui procèdent du titre exécutoire exercé conformément aux dispositions de l'article L. 1617-5 du CGCT, même en l'absence de recours administratif préalable.</ANA>
<ANA ID="9B"> 54-01-02-01 Il résulte des articles L. 262-46 et L. 262-47 du code de l'action sociale et des familles (CASF) qu'une décision de récupération d'un indu de revenu de solidarité active (RSA) prise par le président du conseil général, devenu départemental, ou par délégation de celui-ci ne peut, à peine d'irrecevabilité, faire l'objet d'un recours contentieux sans qu'ait été préalablement exercé un recours administratif auprès de cette autorité.... ,,1) Si la recevabilité d'un recours contentieux dirigé contre le titre exécutoire émis pour recouvrer un indu de RSA n'est pas, en vertu de l'article L. 1617-5 du code général des collectivités territoriales (CGCT), subordonnée à l'exercice d'un recours administratif préalable, le débiteur ne peut toutefois, à l'occasion d'un tel recours, contester devant le juge administratif le bien-fondé de cet indu en l'absence de tout recours préalable saisissant de cette contestation le président du conseil général.... ,,2) En revanche, une telle contestation reste possible à l'occasion d'un recours contre les actes de poursuite qui procèdent du titre exécutoire exercé conformément aux dispositions de l'article L. 1617-5 du CGCT, même en l'absence de recours administratif préalable.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
