<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038618852</ID>
<ANCIEN_ID>JG_L_2019_06_000000412796</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/61/88/CETATEXT000038618852.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème chambre, 07/06/2019, 412796, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-06-07</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>412796</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP WAQUET, FARGE, HAZAN ; CABINET BRIARD ; SCP FOUSSARD, FROGER</AVOCATS>
<RAPPORTEUR>Mme Airelle Niepce</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Louis Dutheillet de Lamothe</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2019:412796.20190607</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. D...A..., Mme I...C..., Mme J...C..., M. E...C..., M. G...C...et M. B...F...ont demandé au tribunal administratif de Marseille d'annuler pour excès de pouvoir l'arrêté du 28 septembre 2015 par lequel le maire de La Ciotat (Bouches-du-Rhône) a délivré à la société civile immobilière (SCI) Mellimmo un permis de construire pour la construction de deux immeubles collectifs de 12 logements sur un terrain situé 58 impasse Saint-Mar à La Ciotat, ainsi que la décision implicite de rejet de leur recours gracieux. Par un jugement n° 1602390 du 16 mars 2017, le tribunal administratif de Marseille a, en application de l'article L. 600-5 du code de l'urbanisme, annulé le permis de construire attaqué en tant qu'il méconnaît les dispositions de l'article UD 6 du règlement du plan local d'urbanisme de La Ciotat et rejeté le surplus des conclusions des requérants. <br/>
<br/>
              Par une ordonnance n° 17MA01959 du 17 juillet 2017, enregistrée le 26 juillet 2017 au secrétariat du contentieux du Conseil d'Etat, le président de la cour administrative d'appel de Marseille a transmis au Conseil d'Etat, en application de l'article R. 351-2 du code de justice administrative, le pourvoi, enregistré le 12 mai 2017 au greffe de cette cour, présenté par M. A...et autres. Par ce pourvoi, par un nouveau mémoire et trois mémoires en réplique, enregistrés les 26 octobre 2017, 29 mars, 6 mai et 10 mai 2019 au secrétariat du contentieux du Conseil d'Etat, M. A...et autres demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à leur demande ;<br/>
<br/>
              3°) de mettre à la charge de la commune de La Ciotat et de la SCI Mellimmo, chacun, la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'urbanisme ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Airelle Niepce, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Louis Dutheillet de Lamothe, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Waquet, Farge Hazan, avocat de M.A..., de M. H...C..., de Mme I...C..., de Mme J...C..., de M. G...C...et de M. F...et au Cabinet Briard, avocat de la société Melimmo et à la SCP Foussard, Froger, avocat de la commune de la Ciotat ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1.	Il ressort des pièces du dossier soumis aux juges du fond que, par un arrêté du 28 septembre 2015, le maire de La Ciotat a délivré à la société civile immobilière (SCI) Mellimmo un permis de construire un ensemble de deux immeubles collectifs de douze logements, d'une surface de plancher de 958 m2, sur un terrain situé 58 impasse Saint-Mar à La Ciotat, en zone UD du plan local d'urbanisme (PLU). M. A...et cinq autres personnes ont saisi le tribunal administratif de Marseille d'un recours pour excès de pouvoir contre cet arrêté, modifié par un arrêté du 25 avril 2016 portant permis de construire modificatif. Par un jugement du 16 mars 2017, le tribunal administratif de Marseille a, en application de l'article L. 600-5 du code de l'urbanisme, annulé l'arrêté attaqué en tant seulement que le projet comporte un local à poubelles au nord-est du terrain d'assiette, en méconnaissance des dispositions de l'article UD 6 du règlement du PLU. M. A...et autres demandent au Conseil d'Etat l'annulation de ce jugement en tant qu'il n'a pas fait entièrement droit à leur demande.<br/>
<br/>
              2.	En premier lieu, l'article R. 741-2 du code de justice administrative dispose que la décision " contient le nom des parties, l'analyse des conclusions et mémoires ainsi que les visas des dispositions législatives ou réglementaires dont elle fait application ". Toutefois, l'omission dans les visas de la mention ou de l'analyse d'un mémoire produit avant la clôture de l'instruction n'est, par elle-même, de nature à vicier la régularité du jugement ou de l'arrêt attaqué que s'il ressort des pièces du dossier que ces écritures apportaient des éléments nouveaux auxquels il n'aurait pas été répondu dans les motifs.<br/>
<br/>
              3.	Il ressort des pièces du dossier soumis aux juges du fond que M. A...et autres ont produit un nouveau mémoire le 21 février 2017, lequel est visé par le jugement attaqué mais non analysé, qui comportait des photographies et des précisions se rapportant aux moyens déjà soulevés par les requérants dans leurs précédentes écritures. En outre, s'il faisait état d'un jugement du tribunal de grande instance de Marseille en date du 7 novembre 2016 dont l'arrêté attaqué n'aurait pas tiré les conséquences, d'une part, cet élément, même s'il était présenté comme un moyen nouveau, n'avait pour but que d'étayer le moyen tiré de l'atteinte aux articles UD 3 et R. 111-2 du code de l'urbanisme, d'autre part, n'était assorti d'aucune précision de nature à en apprécier la portée, faute de produire le jugement et d'indiquer les conséquences qu'aurait dû en tirer l'autorité administrative. Par suite, ces écritures n'apportant pas d'éléments nouveaux, le tribunal administratif a pu, sans commettre d'irrégularité, ne pas les analyser ni y répondre dans ses motifs.<br/>
<br/>
              4.	En deuxième lieu, l'article R. 431-10 du code de l'urbanisme précise la nature des pièces que doit comporter le projet architectural et prévoit notamment que celui-ci comprend " c) Un document graphique permettant d'apprécier l'insertion du projet de construction par rapport aux constructions avoisinantes et aux paysages, son impact visuel ainsi que le traitement des accès et du terrain ; / d) Deux documents photographiques permettant de situer le terrain respectivement dans l'environnement proche et, sauf si le demandeur justifie qu'aucune photographie de loin n'est possible, dans le paysage lointain (...) ". La circonstance que le dossier de demande de permis de construire ne comporterait pas l'ensemble des documents exigés par les dispositions du code de l'urbanisme, ou que les documents produits seraient insuffisants, imprécis ou comporteraient des inexactitudes, n'est susceptible d'entacher d'illégalité le permis de construire qui a été accordé que dans le cas où les omissions, inexactitudes ou insuffisances entachant le dossier ont été de nature à fausser l'appréciation portée par l'autorité administrative sur la conformité du projet à la réglementation applicable.<br/>
<br/>
              5.	Il ressort des pièces du dossier soumis aux juges du fond que le dossier de demande de permis de construire comporte plusieurs photographies et photomontages permettant d'apprécier l'insertion du projet dans son environnement. Il suit de là que le tribunal administratif a pu, sans dénaturer les faits et les pièces du dossier, juger que ces pièces avaient permis à l'autorité administrative d'apprécier la conformité du projet à la réglementation. Par suite, le moyen doit être écarté.<br/>
<br/>
              6.	En troisième lieu, le préambule du chapitre du règlement du plan local d'urbanisme (PLU) relatif à la zone UD précise que " la zone UD est une zone d'extension périurbaine peu dense, à vocation essentiellement d'habitat résidentiel sous forme dominante de type pavillonnaire. Dans cette zone peuvent également être présents des services et des activités non ou peu nuisants, mais qui sont néanmoins nécessaires au fonctionnement urbain. Cette zone assure par sa morphologie une transition entre les zones urbaines plus denses et les zones naturelles (...) ". <br/>
<br/>
              7.	Si le tribunal administratif a jugé à tort que les dispositions citées au point précédent se bornaient à une description des secteurs en cause et ne comportaient aucune disposition de portée réglementaire, il ressort des pièces du dossier soumis aux juges du fond que les dispositions du préambule  invoquées par les requérants ont été précisées par les articles du règlement de la zone, qui autorisent les projets de logement collectifs tout en encadrant les constructions, en particulier par les règles limitant leur emprise au sol, fixant leur hauteur maximale ainsi que leur implantation par rapport aux voies, emprises publiques et limites séparatives, et définissant leur aspect extérieur. Dès lors, ce motif, qui n'exige l'appréciation d'aucune circonstance de fait, peut être substitué à celui qui a été retenu dans le jugement attaqué pour écarter le moyen tiré de la méconnaissance du préambule du chapitre du règlement du PLU relatif à la zone UD, dont il justifie légalement, sur le point en débat, le dispositif.<br/>
<br/>
              8.	En quatrième lieu, d'une part, l'article R. 111-2 du code de l'urbanisme prévoit que " le projet peut être refusé ou n'être accepté que sous réserve de l'observation de prescriptions spéciales s'il est de nature à porter atteinte à la salubrité ou à la sécurité publique du fait de sa situation, de ses caractéristiques, de son importance ou de son implantation à proximité d'autres installations ". D'autre part, l'article UD 3 du règlement du PLU, qui est relatif à la desserte des terrains par les voies et accès, prévoit : " 1 - Voirie / Les terrains doivent être desservis par des voies publiques ou privées, dont les caractéristiques permettent de répondre de manière suffisante à l'importance et à la destination de la construction ou de l'ensemble des constructions qui y sont, ou seront, édifiées. Notamment, les voies doivent permettre de satisfaire aux règles de sécurité et de fonctionnement des services publics (défense contre l'incendie, protection civile, brancardage, ramassage des ordures ménagères, ...) / 2 - Accès / Pour recevoir une occupation ou une utilisation du sol, un terrain doit avoir accès à une voie publique ou privée, soit directement, soit par l'intermédiaire d'un passage aménagé sur fonds voisins. Les accès sur les voies publiques ou privées doivent être aménagés de manière à ne pas créer de difficultés ou de dangers pour la circulation générale en particulier en raison de leurs positions (ex : accès dans un virage, retrait du portail insuffisant ...) et de leurs nombres. Ils doivent satisfaire aux besoins des constructions projetées, notamment en ce qui concerne les possibilités d'intervention des services publics ".<br/>
<br/>
              9.	D'une part, le tribunal n'a pas commis d'erreur de droit en jugeant que les conditions générales de la circulation dans le secteur ne pouvaient être utilement invoquées pour établir l'insuffisance de la desserte du terrain d'assiette de la construction par l'impasse Saint-Mar, tout en précisant qu'il n'était pas établi que le projet, qui prévoit la réalisation de seulement douze logements, serait de nature à augmenter le trafic de manière significative et que, en tout état de cause, le débouché de cette impasse sur l'avenue de la Gare ferait l'objet d'une signalisation, conformément à l'avis du gestionnaire de la voirie. <br/>
<br/>
              10.	D'autre part, contrairement à ce qui est soutenu, il ne ressort pas des pièces du dossier soumis aux juges du fond, au vu notamment de la configuration des lieux et de l'ampleur relativement modeste du projet litigieux, que les conditions d'aménagement des accès à ce dernier depuis l'impasse Saint-Mar seraient de nature à faire peser un risque grave sur la sécurité des automobilistes et des piétons usagers de cette voie. En particulier, contrairement à ce que soutiennent les requérants, les accès créés à l'est du projet pour atteindre l'impasse Saint-Mar, reliés par la voie interne du projet et respectivement dédiés à l'entrée et à la sortie des véhicules, permettent un accès sécurisé au terrain d'assiette et à la voie privée dans des conditions de visibilité satisfaisantes. Il résulte de ce qui précède que le tribunal administratif n'a pas entaché son jugement d'erreur de droit, ni dénaturé les pièces du dossier qui lui était soumis en écartant le moyen tiré de la méconnaissance des articles R. 111-2 du code de l'urbanisme et UD 3 du règlement du PLU.<br/>
<br/>
              11.	En cinquième lieu, l'article UD 9 du règlement du PLU dispose que l'emprise au sol des constructions ne doit pas excéder 40 % dans le secteur UD1, dans lequel se trouve le terrain d'assiette du projet litigieux. Aux termes du lexique du règlement du PLU, l'emprise au sol se définit comme " la projection verticale de toutes les parties d'un bâtiment qui s'élèvent à plus de 0,60 mètre au-dessus du sol naturel situé à l'aplomb de ces points et qui ne sont pas construites en porte à faux par rapport au volume de la construction qui prend fondation au sol. / Sont donc pris en compte dans l'emprise au sol : les vérandas, les terrasses (ou parties de celles-ci) dès lors qu'elles s'élèvent à plus de 0,60 mètre au-dessus du sol naturel. / Ne sont pas pris en compte dans l'emprise au sol : / - les terrasses (ou parties de celles-ci) qui ne dépassent pas de plus de 0,60 mètre le sol naturel (même si elles comportent un garde-corps) ainsi que les sous-sols et garages situés sous la construction ou sous le sol naturel ; / - les balcons et auvents en surplomb, ainsi que les débords de toiture ". <br/>
<br/>
              12.	D'une part, il résulte de ces dispositions que doivent notamment être pris en compte, dans le calcul de l'emprise au sol, les terrasses et les garages qui s'élèvent à plus de 0,60 mètre au-dessus du sol naturel. Cependant, le tribunal administratif n'a pas commis d'erreur de droit en estimant que, dès lors que la projection verticale des terrasses situées à plus de 0,60 mètre du sol naturel se confond avec le toit des garages, l'emprise au sol de ces toits de garages n'a pas à être comptabilisée une seconde fois dans le calcul.<br/>
<br/>
              13.	D'autre part, il ressort des pièces du dossier soumis aux juges du fond que seul le bâtiment A comporte des garages. Si le tribunal, induit en erreur par les écritures des requérants, s'est prononcé sur la prise en compte des garages en sous-sol débordant au sud du bâtiment B, sa motivation doit être regardée comme visant les garages prévus par le projet au niveau du bâtiment A. Par suite, le jugement n'est pas entaché d'insuffisance de motivation sur ce point, ni de dénaturation dans la prise en compte des garages pour la détermination de l'emprise au sol du projet.<br/>
<br/>
              14.	En sixième lieu, aux termes de l'article UD 11 du règlement du PLU, qui règlemente l'aspect extérieur des constructions et l'aménagement de leurs abords : " 1 - Dispositions générales / Compte tenu de leur situation, les constructions ne doivent pas porter atteinte par leur aspect, leur architecture et leurs dimensions, au caractère des paysages naturels et urbains avoisinants. / En outre : / - dans un site classé ou inscrit, les constructions doivent respecter le caractère du site. / - à proximité d'un bâtiment inscrit ou classé, les constructions ne doivent pas nuire à la conservation du caractère de l'édifice. / 2- Façades / Les façades doivent faire l'objet d'un traitement en harmonie avec les constructions avoisinantes (matériaux, couleurs...). / Toutefois, les constructions d'aspect architectural contemporain sont autorisées, dans la mesure où elles participent à la mise en valeur d'un lieu. / Aucun élément technique (climatisateur, antenne, cheminée...) n'est autorisé en saillie des façades. / Les antennes doivent être de préférence intégrées dans la construction, ou lorsque leur fonctionnement les impose à l'extérieur, apposées en toiture. / Les climatisateurs intégrés dans les façades devront être masqués par un dispositif esthétique (...) ".<br/>
<br/>
              15.	Il ne résulte pas des pièces du dossier soumis aux juges du fond que le projet litigieux, qui est d'ampleur et de hauteur limitées et dont le terrain d'assiette, contrairement à ce qui est soutenu, n'est ni inscrit dans un site naturel qui bénéficierait d'une protection particulière, ni dans un environnement présentant un intérêt architectural particulier, créerait une rupture dans cet environnement par son aspect architectural contemporain et méconnaîtrait les dispositions précitées, ainsi que l'a jugé le tribunal administratif dans son jugement qui n'est entaché ni d'insuffisance de motivation, ni d'erreur de qualification juridique, ni de dénaturation sur ce point. <br/>
<br/>
              16.	En septième lieu, aux termes de l'article UD 13 du PLU, relatif aux espaces libres, aires de jeux et de loisirs et plantations : " il devra être apporté un soin particulier au traitement des espaces libres situés dans la bande de retrait des constructions par rapport aux voies. Celle-ci sera traitée en espace paysager et planté. / Toutefois, dans cette bande sont autorisées les emprises nécessaires au dégagement des accès, et à l'accueil de places de stationnement à l'air libre, à condition que celles-ci soient masquées par des haies ou des clôtures. / En outre : / - pour toute opération d'au moins trois logements, et pour laquelle il est prévu que des surfaces libres du terrain (hors voies de desserte, parkings ou autres usages spécifiques d'intérêt collectif) restent à usage commun, 15% au moins de la surface totale du terrain doit être aménagé en espaces verts communs plantés. Ces espaces devront comporter au moins un arbre de haute tige pour 150 m2 et intégreront une aire de jeux pour enfants (...) ".<br/>
<br/>
              17.	D'une part, contrairement à ce qui est soutenu, c'est sans contradiction de motifs que le tribunal a pu juger à la fois que les espaces libres situés dans la bande de retrait par rapport aux voies seraient occupés par trois espaces verts qui comprendront des arbres ou des arbustes et que les espaces libres du terrain d'assiette n'avaient pas été laissés à l'usage commun, dès lors que les dispositions citées au point précédent distinguent les espaces libres situés dans la bande de retrait des constructions par rapport aux voies des autres espaces verts communs plantés.<br/>
<br/>
              18.	D'autre part, les dispositions précitées s'appliquent aux seules surfaces libres du terrain restant à usage commun, à l'exclusion des jardins privatifs. Dès lors, c'est sans erreur de droit ni dénaturation que le tribunal administratif a pu estimer que le projet litigieux ne méconnaissait pas les prescriptions de l'article UD 13 du règlement du PLU, sans incidence étant la circonstance que les jardins privatifs prévus par le projet litigieux résultent du permis modificatif délivré au pétitionnaire. Il suit de là que le moyen doit être écarté.<br/>
<br/>
              19.	Il résulte de tout ce qui précède que M. A...et autres ne sont pas fondés à demander l'annulation du jugement qu'ils attaquent, en tant que celui-ci n'a pas entièrement fait droit à leur demande. Par suite, leur pourvoi doit être rejeté.<br/>
<br/>
              20.	Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'il soit fait droit aux conclusions présentées à ce titre par M. A...et autres. <br/>
<br/>
              21.	Il y a lieu, en revanche, dans les circonstances de l'espèce, de mettre à la charge de M. A...et autres la somme de 3 000 euros à verser, d'une part, à la commune de La Ciotat, et, d'autre part, à la SCI Mellimmo.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de M. A...et autres est rejeté.<br/>
Article 2 : M. A...et autres verseront à la commune de La Ciotat et à la SCI Mellimmo, chacun, une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée à M. D...A..., premier dénommé pour l'ensemble des requérants, à la SCI Mellimmo et à la commune de La Ciotat.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
