<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026531781</ID>
<ANCIEN_ID>JG_L_2012_10_000000356709</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/53/17/CETATEXT000026531781.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  7ème sous-section jugeant seule, 24/10/2012, 356709, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2012-10-24</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>356709</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 7ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Rémy Schwartz</PRESIDENT>
<AVOCATS>SCP DELAPORTE, BRIARD, TRICHET ; SCP LYON-CAEN, THIRIEZ</AVOCATS>
<RAPPORTEUR>M. Stéphane Bouchard</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Bertrand Dacosta</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2012:356709.20121024</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi, enregistré le 13 février 2012 au secrétariat du contentieux du Conseil d'Etat, présenté pour la société de participations industrielles, dont le siège est 16 place de l'Iris à Paris La Défense (92740) ; la société de participations industrielles demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 11LY00194 du 13 décembre 2011, rectifié par l'ordonnance du 6 janvier 2012, par lequel la cour administrative d'appel de Lyon, statuant sur renvoi du Conseil d'Etat après cassation de son arrêt n° 06LY02138 du 16 juillet 2009 en tant qu'il rejetait les conclusions dirigées contre les constructeurs et le maître d'ouvrage délégué et qu'il ne portait qu'à 642 031,56 euros la condamnation de la société Auxiwaste Services prononcée au bénéfice du syndicat mixte pour le traitement des résidus urbains (SYMTRU) par le jugement n° 0300789 du 27 juillet 2006 du tribunal administratif de Clermont-Ferrand en indemnisation des désordres affectant l'usine de tri-valorisation des déchets ménagers de Châteldon, en premier lieu, l'a condamnée à payer, solidairement avec la société Eiffage Construction et la société Auxiwaste Services, la somme de 11 685 541,45 euros au syndicat mixte, majorée d'intérêts au taux légal à compter du 19 mai 2003, capitalisés le 17 février 2006 puis à chaque date anniversaire, en deuxième lieu, a mis à leur charge les dépens, solidairement, pour un montant de 39 187,61 euros, en troisième lieu, a rejeté le surplus des conclusions des parties, et, en quatrième lieu, a annulé le jugement en ce qu'il avait de contraire ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter la requête du syndicat mixte ;<br/>
<br/>
              3°) de mettre à la charge du syndicat mixte la somme de 6 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Stéphane Bouchard, Maître des Requêtes en service extraordinaire,  <br/>
<br/>
              - les observations de la SCP Delaporte, Briard, Trichet, avocat de la société de participations industrielles,<br/>
<br/>
              - les conclusions de M. Bertrand Dacosta, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Delaporte, Briard, Trichet, avocat de la société de participations industrielles ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article L. 822-1 du code de justice administrative : " Le pourvoi en cassation devant le Conseil d'Etat fait l'objet d'une procédure préalable d'admission. L'admission est refusée par décision juridictionnelle si le pourvoi est irrecevable ou n'est fondé sur aucun moyen sérieux " ;<br/>
<br/>
              2. Considérant que pour demander l'annulation de l'arrêt et de l'ordonnance attaqués, la société de participations industrielles soutient que cet arrêt a été rendu en méconnaissance du principe du caractère contradictoire de la procédure, dans la mesure où il retient la responsabilité des constructeurs en raison du pouvoir calorifique prétendument insuffisant des briquettes combustibles, qui n'a été invoqué par le SYMTRU que dans un mémoire produit tardivement, sans que la cour administrative d'appel le communique et rouvre l'instruction, comme elle aurait dû le faire ; qu'en jugeant recevable, en l'absence de décision expresse du SYMTRU prolongeant la période de la garantie de parfait achèvement, l'action engagée par le SYMTRU à son encontre, près de six ans après la réception de l'ouvrage, la cour administrative d'appel, qui devait relever d'office le moyen tiré de l'expiration des relations contractuelles, a commis une erreur de droit ; qu'en retenant la responsabilité des constructeurs à raison de l'insuffisance de pouvoir calorifique des briquettes, tout en relevant qu'il n'était pas établi que l'usine qu'ils avaient construite était impropre à produire des briquettes ayant un pouvoir calorifique conforme aux stipulations contractuelles, la cour administrative d'appel a entaché son arrêt d'une contradiction de motifs ; que c'est au prix d'une contradiction de motifs et d'une dénaturation des pièces du dossier que la cour administrative d'appel a retenu que les briquettes n'atteignaient pas, sauf exception, le pouvoir calorifique prescrit ; qu'en estimant que le groupement de constructeurs s'était engagé sur la qualité du compost, la cour administrative d'appel a dénaturé les pièces du dossier et commis une erreur de droit ; que la cour administrative d'appel a omis de répondre au moyen tiré de ce qu'aucune réserve ne pouvait utilement être émise sur la qualité du compost ; qu'en se bornant à retenir que la mauvaise qualité des entrants n'était pas établie, la cour administrative d'appel a dénaturé les pièces du dossier et insuffisamment motivé son arrêt ; qu'ayant retenu que la responsabilité des constructeurs ne pouvait pas être engagée du fait des difficultés de commercialisation des briquettes, la cour administrative d'appel ne pouvait, sans contradiction de motifs, les condamner à réparer une perte de recettes sur la vente de celles-ci ; qu'en retenant que le contrat souscrit par le groupement de conception-réalisation engageait sa responsabilité au titre de l'exploitation de l'usine, la cour administrative d'appel en a dénaturé les stipulations ; qu'ayant retenu que l'usine n'était pas impropre à produire des briquettes ayant un pouvoir calorifique conforme aux prescriptions contractuelles, la cour administrative d'appel ne pouvait, sans contradiction de motifs, retenir l'existence d'un lien de causalité entre l'insuffisance de pouvoir calorifique des briquettes et la fermeture définitive de l'usine ; que l'ordonnance du 6 janvier 2012, qui ne s'explique pas sur le relèvement du montant de l'indemnité mis à la charge des constructeurs, n'est pas suffisamment motivée   ;<br/>
<br/>
              3. Considérant qu'eu égard aux moyens soulevés, il y a lieu d'admettre les conclusions du pourvoi dirigé contre l'arrêt en tant qu'il statue sur la responsabilité contractuelle de la société de participations industrielles ; qu'en revanche, s'agissant des autres conclusions du pourvoi, aucun des moyens n'est de nature à permettre son admission ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Les conclusions du pourvoi de la société de participations industrielles contre l'arrêt attaqué en tant qu'il statue sur sa responsabilité contractuelle sont admises.<br/>
Article 2 : Le surplus des conclusions du pourvoi de la société de participations industrielles n'est pas admis.<br/>
Article 3 : La présente décision sera notifiée à la société de participations industrielles. <br/>
Copie en sera adressée pour information au syndicat mixte pour le traitement des résidus urbains, à la société d'équipement de l'Auvergne, à la société Eiffage Construction et à Maître Patrick Ouizille, en sa qualité de mandataire liquidateur de la société Auxiwaste Services.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
