<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033685013</ID>
<ANCIEN_ID>JG_L_2016_12_000000384450</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/68/50/CETATEXT000033685013.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème chambre, 23/12/2016, 384450, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-12-23</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>384450</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DELAPORTE, BRIARD</AVOCATS>
<RAPPORTEUR>Mme Séverine Larere</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emilie Bokdam-Tognetti</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2016:384450.20161223</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une décision du 16 décembre 2015, le Conseil d'Etat, statuant au contentieux, a prononcé l'admission des conclusions du pourvoi de la SARL Villennes-sur-Seine Invest Hôtel dirigées contre l'arrêt n° 13VE00318 du 24 juin 2014 de la cour administrative d'appel de Versailles en tant seulement que cet arrêt se prononce sur le litige en matière de taxe foncière sur les propriétés bâties. <br/>
<br/>
              Par un mémoire en défense, enregistré le 5 avril 2016, le ministre des finances et des comptes publics conclut au rejet du pourvoi. Il soutient que les moyens invoqués par la SARL Villennes-sur-Seine Invest Hôtel ne sont pas fondés.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code général des impôts ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Séverine Larere, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Emilie Bokdam-Tognetti, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Delaporte, Briard, avocat de la societe Villennes-sur-seine Invest Hotel ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              Sur les conclusions de la SARL Villennes-sur-Seine Invest Hôtel tendant à l'annulation de l'arrêt du 24 juin 2014 de la cour administrative d'appel de Versailles :<br/>
<br/>
              1. Il résulte des dispositions du 5° de l'article R. 222-13 du code de justice administrative et de l'article R. 811-1 du même code, dans leur rédaction applicable au litige, que si le tribunal administratif statue en premier et dernier ressort sur les litiges concernant la taxe foncière, les jugements relatifs à cette taxe peuvent toutefois faire l'objet d'un appel devant la cour administrative d'appel lorsque le premier juge a statué par un seul jugement, d'une part, sur des conclusions relatives à la taxe foncière, d'autre part, sur des conclusions relatives soit à la taxe professionnelle, soit à la cotisation foncière des entreprises à la demande du même contribuable, et que ces impositions reposent, en tout ou partie, sur la valeur des mêmes biens appréciée la même année. <br/>
<br/>
              2. La cour administrative d'appel de Versailles a, par son arrêt du 24 juin 2014, statué en appel sur les conclusions du ministre de l'économie et des finances relatives, d'une part, à la taxe foncière sur les propriétés bâties et, d'autre part, à la taxe professionnelle, pour la même année 2006 et pour la même commune de Villennes-sur-Seine, alors même que la valeur locative des biens sur lesquels reposent ces impositions n'est pas appréciée la même année. Il résulte de ce qui précède que la cour administrative d'appel de Versailles n'était pas compétente pour statuer sur les conclusions relatives à la taxe foncière sur les propriétés bâties. Par suite, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, son arrêt doit être annulé dans cette mesure.<br/>
<br/>
              Sur les conclusions du ministre dirigées contre le jugement du 20 novembre 2012 du tribunal administratif de Versailles :<br/>
<br/>
              3. Il y a lieu de regarder les conclusions relatives aux cotisations de taxe foncière présentées devant la cour comme des conclusions de cassation dirigées contre le jugement du 20 novembre 2012 par lequel le tribunal administratif a, d'une part, ramené à 10,05 euros le mètre carré le tarif unitaire applicable pour la détermination de la valeur locative de l'hôtel exploité sous l'enseigne " Campanile " à Villennes-sur-Seine par la SARL Villennes-sur-Seine Invest Hôtel et, d'autre part, déchargé cette société des cotisations de taxe foncière sur les propriétés bâties auxquelles elle a été assujettie au titre de l'année 2006 à concurrence de cette réduction de la base d'imposition. <br/>
<br/>
              4. Aux termes de l'article 1498 du code général des impôts : " La valeur locative de tous les biens autres que les locaux visés au I de l'article 1496 et que les établissements industriels visés à l'article 1499 est déterminée au moyen de l'une des méthodes indiquées ci-après : 1° Pour les biens donnés en location à des conditions de prix normales, la valeur locative est celle qui ressort de cette location ; 2° a. Pour les biens loués à des conditions de prix anormales ou occupés par leur propriétaire, occupés par un tiers à un autre titre que la location, vacants ou concédés à titre gratuit, la valeur locative est déterminée par comparaison. / Les termes de comparaison sont choisis dans la commune. Ils peuvent être choisis hors de la commune pour procéder à l'évaluation des immeubles d'un caractère particulier ou exceptionnel ; / b. La valeur locative des termes de comparaison est arrêtée : / Soit en partant du bail en cours à la date de référence de la révision lorsque l'immeuble type était loué normalement à cette date, / Soit, dans le cas contraire, par comparaison avec des immeubles similaires situés dans la commune ou dans une localité présentant, du point de vue économique, une situation analogue à celle de la commune en cause et qui faisaient l'objet à cette date de locations consenties à des conditions de prix normales ; 3° A défaut de ces bases, la valeur locative est déterminée par voie d'appréciation directe ".<br/>
<br/>
              5. Le tribunal administratif de Versailles, après avoir relevé que le local-type n° 33 du procès-verbal des opérations de révision des évaluations foncières de la commune de Saint-Mandé, construit en 1895, correspondait à un hôtel de type traditionnel et que l'immeuble en litige était un hôtel de chaîne de conception moderne, a dénaturé les pièces du dossier en jugeant que ce local-type pouvait être retenu comme terme pertinent de comparaison pour la détermination de la valeur locative selon la méthode prévue au 2° de l'article 1498 du code général des impôts, alors qu'il ressort des pièces du dossier soumis aux juges du fond que ses caractéristiques au regard de sa construction, de sa structure et de son aménagement n'étaient pas similaires à celles de l'immeuble à évaluer. Par suite, sans qu'il soit besoin d'examiner les autres moyens du pourvoi du ministre, le jugement du tribunal administratif de Versailles doit être annulé. <br/>
<br/>
              6. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat qui n'est pas la partie perdante dans la présente instance.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 24 juin 2014 de la cour administrative d'appel de Versailles est annulé en tant qu'il statue sur les conclusions relatives aux cotisations de taxe foncière sur les propriétés bâties. <br/>
Article 2 : Le jugement du tribunal administratif de Versailles du 20 novembre 2012 est annulé en tant qu'il statue sur les conclusions relatives aux cotisations de taxe foncière sur les propriétés bâties. <br/>
Article 3 : L'affaire est renvoyée au tribunal administratif de Versailles. <br/>
Article 4 : Le surplus des conclusions du pourvoi de la SARL Villennes-sur-Seine Invest Hôtel est rejeté.<br/>
Article 5 : La présente décision sera notifiée à la SARL Villennes-sur-Seine Invest Hôtel et au ministre de l'économie et des finances.  <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
