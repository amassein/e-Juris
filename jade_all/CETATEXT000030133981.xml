<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030133981</ID>
<ANCIEN_ID>JG_L_2015_01_000000377497</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/13/39/CETATEXT000030133981.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème et 2ème sous-sections réunies, 19/01/2015, 377497</TITRE>
<DATE_DEC>2015-01-19</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>377497</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème et 2ème sous-sections réunies</FORMATION>
<TYPE_REC>Contentieux des pensions</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DELAPORTE, BRIARD, TRICHET</AVOCATS>
<RAPPORTEUR>M. Jean-Dominique Nuttens</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Gilles Pellissier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:377497.20150119</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 14 avril et 15 juillet 2014 au secrétariat du contentieux du Conseil d'Etat, présentés pour Mme B...A..., demeurant ... ; Mme A...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler le jugement n° 1103406 du 21 novembre 2013 du tribunal administratif de Nîmes en tant qu'il a rejeté ses conclusions tendant à l'annulation de son brevet de pension en ce qu'il refusait de lui reconnaître un droit à pension pour invalidité imputable au service ; <br/>
<br/>
              2°) de mettre à la charge de la Caisse nationale de retraite des agents des collectivités locales (CNRACL) le versement à la SCP Delaporte-Briard-Trichet, son avocat, de la somme de 3 000 euros au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 ; <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la loi n° 91-647 du 10 juillet 1991 ;<br/>
<br/>
              Vu le décret n° 2003-1306 du 26 décembre 2003 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Dominique Nuttens, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Gilles Pellissier, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Delaporte, Briard, Trichet, avocat de Mme A...;<br/>
<br/>
<br/>
<br/>1. Considérant que Mme A...se pourvoit en cassation contre le jugement par lequel le tribunal administratif de Nîmes a rejeté sa demande d'annulation du brevet de pension qui lui a été délivré le 27 juin 2011 par la Caisse nationale de retraite des agents des collectivités locales en tant qu'il ne prévoit pas, en sus de sa pension de retraite, le versement d'une rente viagère d'invalidité ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article 36 du décret du 26 décembre 2003 relatif au régime de retraite des fonctionnaires affiliés à la Caisse nationale de retraite des agents des collectivités locales : " Le fonctionnaire qui a été mis dans l'impossibilité permanente de continuer ses fonctions en raison d'infirmités résultant de blessures ou de maladies contractées ou aggravées, soit en service, soit en accomplissant un acte de dévouement dans un intérêt public (...) peut être mis à la retraite par anticipation soit sur sa demande, soit d'office (...) " ; qu'aux termes de l'article 37 du même décret, les fonctionnaires qui ont été mis à la retraite dans les conditions prévues à l'article 36 " bénéficient d'une rente viagère d'invalidité cumulable avec la pension rémunérant les services prévus à l'article précédent. Le bénéfice de cette rente viagère d'invalidité est attribuable si la radiation des cadres ou le décès en activité (...) sont imputables à des blessures ou des maladies survenues dans l'exercice des fonctions ou à l'occasion de l'exercice des fonctions, ou résultant de l'une des autres circonstances énumérées à l'article 36 ci-dessus "  ;<br/>
<br/>
              3. Considérant qu'il résulte de ces dispositions que le droit pour un fonctionnaire territorial de bénéficier de la rente viagère d'invalidité prévue par l'article 37 du décret du 26 décembre 2003 est subordonné à la condition que les blessures ou maladies contractées ou aggravées en service aient été de nature à entraîner, à elles seules ou non, la mise à la retraite de l'intéressé ; <br/>
<br/>
              4. Considérant que pour rejeter la demande de MmeA..., le tribunal administratif de Nîmes s'est fondé sur la circonstance que le lien entre les faits survenus en service et l'impossibilité pour Mme A...de continuer ses fonctions n'était pas à la fois direct et exclusif ; qu'il résulte de ce qui a été dit ci-dessus qu'en posant ainsi une condition d'exclusivité du lien de causalité entre la maladie contractée ou aggravée en service et la mise à la retraite de l'intéressée, le tribunal a commis une erreur de droit ; que, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, son jugement doit donc être annulé en tant qu'il a rejeté les conclusions tendant à l'annulation du brevet de pension délivré à Mme A...en tant que celui-ci ne prévoit le versement d'aucune rente viagère d'invalidité ;<br/>
<br/>
              5. Considérant qu'il y a lieu, dans les circonstances de l'espèce, en application des dispositions de l'article L. 821-2 du code de justice administrative, de régler l'affaire au fond dans la mesure de la cassation prononcée ;<br/>
<br/>
              6. Considérant qu'il résulte de l'instruction, et n'est d'ailleurs pas contesté, que Mme A...a fait l'objet, alors qu'elle était employée par la commune de Cheval-Blanc et affectée au sein de l'école primaire, de brimades répétées dont le ou les auteurs n'ont jamais pu être identifiés, caractérisés en particulier par des dégradations systématiquement commises dans les salles de classe après son passage pour les nettoyer ; qu'à la suite de ces agissements, et alors qu'elle n'avait manifesté jusque là aucun trouble d'ordre psychique ou comportemental, elle a été placée en congé de maladie en raison d'un état anxio-dépressif important et n'a, jusqu'à la délivrance de son brevet de pension, plus jamais réoccupé son emploi ; que si un rapport d'expertise psychiatrique du 31 août 2012 énonce que le décalage entre la gravité de l'état dépressif présenté par Mme A...et les difficultés qu'elle avait rencontrées dans son milieu professionnel témoignait de l'existence d'une " faille psychique " qui, jusqu'alors, ne s'était pas manifestée, il relève également que cet état constitue une conséquence des agissements dont elle a été victime ; qu'ainsi, eu égard à la gravité et au caractère exceptionnel des faits survenus dans l'exécution de son service et de l'absence de toute manifestation antérieure de la maladie dont elle souffre, l'impossibilité permanente d'exercer ses fonctions dans laquelle s'est trouvée Mme A... doit être regardée comme ayant pour cause directe des faits précis survenus dans le cadre du service ; que, par suite, Mme A...est fondée à demander l'annulation du brevet de pension qui lui a été délivré en tant qu'il ne prévoit le versement d'aucune rente viagère d'invalidité ;<br/>
<br/>
              7. Considérant que Mme A...a obtenu le bénéfice de l'aide juridictionnelle ; que, par suite, son avocat peut se prévaloir des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 ; qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la Caisse nationale de retraite des agents des collectivités locales la somme de 3 000 euros à verser à la SCP Delaporte-Briard-Trichet, sous réserve que cette dernière renonce à percevoir la somme correspondant à la part contributive de l'Etat ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'article 4 du jugement du 21 novembre 2013 du tribunal administratif de Nîmes est annulé.<br/>
Article 2 : Le brevet de pension délivré le 27 juin 2011 à Mme A...est annulé en tant qu'il ne prévoit le versement d'aucune rente viagère d'invalidité. <br/>
Article 3 : La Caisse nationale de retraite des agents des collectivités locales versera à la SCP Delaporte-Briard-Trichet, avocat de MmeA..., une somme de 3 000 euros en application des dispositions du deuxième alinéa de l'article 37 de la loi du 10 juillet 1991, sous réserve qu'elle renonce à la somme correspondant à la part contributive de l'Etat.<br/>
Article 4 : La présente décision sera notifiée à Mme B...A...et à la Caisse nationale de retraite des agents des collectivités locales.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">48-02-02-04-02 PENSIONS. PENSIONS CIVILES ET MILITAIRES DE RETRAITE. PENSIONS CIVILES. PENSIONS OU ALLOCATIONS POUR INVALIDITÉ. RENTE VIAGÈRE D'INVALIDITÉ (ARTICLES L. 27 ET L. 28 DU NOUVEAU CODE). - FONCTIONNAIRES TERRITORIAUX (DÉCRET N° 2003-1306  DU 26 DÉCEMBRE 2003) - CONDITIONS - LIEN DE CAUSALITÉ DIRECT ENTRE LES BLESSURES ET LA MISE À LA RETRAITE - EXISTENCE - BLESSURES CONSTITUANT LA CAUSE EXCLUSIVE DE LA MISE À LA RETRAITE - ABSENCE [RJ1].
</SCT>
<ANA ID="9A"> 48-02-02-04-02 Le droit pour un fonctionnaire territorial de bénéficier de la rente viagère d'invalidité prévue par l'article 37 du décret n° 2003-1306  du 26 décembre 2003 est subordonné à la condition que les blessures ou maladies contractées ou aggravées en service aient été de nature à entraîner, à elles seules ou non, la mise à la retraite de l'intéressé.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1]Cf., sous l'empire du décret du décret n° 65-773 du 9 septembre 1965, CE, 3 novembre 2006, M. Doumerc, n° 233178, T. p. 976. Rappr., s'agissant du congé de maladie, CE, 23 septembre 2013, Mme Fonvieille, n° 353093, T. p. 650.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
