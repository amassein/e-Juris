<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028782064</ID>
<ANCIEN_ID>JG_L_2014_03_000000363116</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/78/20/CETATEXT000028782064.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  5ème sous-section jugeant seule, 26/03/2014, 363116, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-03-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>363116</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 5ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Dominique Chelle </RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Fabienne Lambolez</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2014:363116.20140326</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 28 septembre 2012 au secrétariat du contentieux du Conseil d'Etat, présentée par la société Vortex, dont le siège est 37 bis, rue Greneta à Paris (75002) ; la société Vortex demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler, d'une part, la décision du 13 mars 2012 par laquelle le Conseil supérieur de l'audiovisuel (CSA) l'a mise en demeure de respecter les conditions techniques de diffusion de son service de radio fixées par l'autorisation n° 2007-695 du 24 juillet 2007 et, d'autre part, la décision du 27 juillet 2012 par laquelle ce conseil a rejeté son recours gracieux à l'encontre de cette mise en demeure ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat le versement d'une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la loi n° 86-1067 du 30 septembre 1986 ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Dominique Chelle, Conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Fabienne Lambolez, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'aux termes de l'article 25 de la loi du 30 septembre 1986 : " L'usage de la ressource radioélectrique pour la diffusion de services de communication audiovisuelle par voie hertzienne terrestre est subordonné au respect des conditions techniques définies par le Conseil supérieur de l'audiovisuel et concernant notamment : / 1° Les caractéristiques des signaux émis et des équipements de transmission et de diffusion utilisés ; / 1° bis Les conditions techniques du multiplexage et les caractéristiques des équipements utilisés ;  / 2° Le lieu d'émission ; / 3° La limite supérieure et, le cas échéant, inférieure de puissance apparente rayonnée ; / 4° La protection contre les interférences possibles avec l'usage des autres techniques de télécommunications (...) " ; qu'aux termes du premier alinéa de l'article 42 de la même loi : " Les éditeurs et distributeurs de services de communication audiovisuelle et les opérateurs de réseaux satellitaires peuvent être mis en demeure de respecter les obligations qui leur sont imposées par les textes législatifs et réglementaires et par les principes définis aux articles 1er et 3-1 " ; <br/>
<br/>
              2. Considérant qu'il ressort des pièces du dossier que le Conseil supérieur de l'audiovisuel (CSA) a autorisé par une décision du 24 juillet 2007 la société Vortex à exploiter, pour la zone de Paris, un service de radio dénommé " Skyrock " sur la fréquence 96 MHz ; que cette décision prévoit, à son article 3, que la valeur maximale d'" excursion de fréquence " autorisée sur la fréquence 96 MHz à Paris est de 75 kHz ; qu'un agent assermenté du CSA a constaté, le 6 janvier 2012, que la société Vortex émettait le programme " Skyrock " avec une excursion de fréquence de 83,2 kHz ; que, sur le fondement de ce constat, le CSA a, par une décision du 13 mars 2012, mis en demeure la société requérante de se conformer à ses obligations ; qu'il a rejeté, par une décision du 27 juillet 2012, le recours gracieux formé par la société Vortex contre cette mise en demeure ; que la société requérante demande l'annulation de ces deux décisions ; <br/>
<br/>
              Sur la légalité externe des décisions attaquées : <br/>
<br/>
              3. Considérant, en premier lieu, que le 4° de l'article 3 de la décision du 24 juillet 2007 autorisant la société Vortex a émettre sur la fréquence 96 MHz à Paris prévoit que, lorsque le CSA constate le non-respect des conditions techniques dont est assortie l'autorisation délivrée, son titulaire est tenu de faire procéder, par un organisme agréé, à une vérification de la conformité de son installation aux prescriptions figurant dans l'annexe technique de l'autorisation et transmet au Conseil les résultats de cette vérification ; que ces dispositions n'impliquent pas, contrairement à ce que soutient la société requérante, que le CSA serait tenu, préalablement à toute décision de mise en demeure fondée sur la méconnaissance des conditions techniques d'une autorisation de fréquence, d'informer le titulaire de l'autorisation des manquements qu'il a constatés ; que les mises en demeure du CSA, qui ne constituent pas des décisions qui infligent une sanction, n'ont à être précédées d'aucune procédure contradictoire préalable ; que, par suite, le moyen tiré de ce que la décision de mise en demeure du 13 mars 2012 aurait été prise à l'issue d'une procédure irrégulière, faute pour le CSA d'avoir communiqué au préalable à la société les griefs formés à son encontre, doit être écarté ; <br/>
<br/>
              4. Considérant, en second lieu, que les décisions attaquées mentionnent les éléments de droit et de fait sur lesquels le CSA s'est fondé pour prononcer la mise en demeure ; qu'ainsi le moyen tiré de ce que la motivation des décisions des 13 mars et 27 juillet 2012 serait insuffisante doit être écarté ; <br/>
<br/>
              Sur la légalité interne des décisions attaquées : <br/>
<br/>
              5. Considérant, en premier lieu, que, pour contester l'exactitude matérielle des mesures " d'excursion de fréquence " effectuées par le CSA, la société Vortex se borne à contester la fiabilité de la méthodologie et du matériel utilisés, sans fournir aucun élément de preuve à l'appui de ses allégations ; qu'il ressort au demeurant des pièces du dossier que la méthodologie utilisée par le CSA pour mesurer " l'excursion maximale de fréquence " des émissions de radiodiffusion est conforme aux recommandations de l'Union internationale des télécommunications (UIT) ; que, par suite, le moyen tiré de ce que le CSA aurait fondé sa décision de mettre en demeure la société Vortex de respecter ses obligations quant à " l'excursion maximale " autorisée pour la fréquence 96 MHz à Paris sur des éléments de faits erronés ne peut qu'être écarté ; <br/>
<br/>
              6. Considérant, en second lieu, que le CSA peut adresser une mise en demeure au titulaire d'une autorisation de fréquence en cas de méconnaissance des obligations qui lui sont imposées par les textes législatifs et réglementaires ; que la circonstance qu'il n'est pas établi que la méconnaissance par la société Vortex de " l'excursion maximale " autorisée pour la fréquence 96 MHz à Paris aurait causé une gêne technique pour les autres opérateurs autorisés à émettre sur la zone, à la supposer établie, est sans incidence sur la possibilité pour le CSA de mettre en demeure cette société de respecter une " valeur maximale d'excursion " de 75 kHz, qui constitue l'une de ses obligations réglementaires ; <br/>
<br/>
              7. Considérant qu'il résulte de tout ce qui précède que la société Vortex n'est pas fondée à demander l'annulation des décisions qu'elle attaque ; <br/>
<br/>
              Sur les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative : <br/>
<br/>
              8. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
		Article 1er : La requête de la société Vortex est rejetée. <br/>
<br/>
Article 2 : La présente décision sera notifiée à la société Vortex et au Conseil supérieur de l'audiovisuel. <br/>
Copie en sera adressée pour information à la ministre de la culture et de la communication.  <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
