<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033657433</ID>
<ANCIEN_ID>JG_L_2016_12_000000396626</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/65/74/CETATEXT000033657433.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème chambre, 16/12/2016, 396626, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-12-16</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>396626</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Florian Roussel</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Laurence Marion</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2016:396626.20161216</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Mme A...B...a demandé au tribunal administratif de Bordeaux d'annuler la décision du 9 janvier 2015 par laquelle le ministre de l'intérieur a constaté la perte de validité de son permis de conduire pour solde de points nul. Par un jugement n° 1501019 du 1er décembre 2015, le tribunal administratif a annulé cette décision.<br/>
<br/>
              Par un pourvoi enregistré le 1er février 2016 au secrétariat du contentieux du Conseil d'Etat, le ministre de l'intérieur demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter la demande de MmeB....<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
<br/>
              - le code de la route ;<br/>
<br/>
              - la loi n° 2011-267 du 14 mars 2011 ;<br/>
<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Florian Roussel, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Laurence Marion, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, par une décision du 9 janvier 2015, le ministre de l'intérieur a constaté la perte de validité du permis de conduire de Mme B...pour solde de points nul, à la suite d'infractions au code de la route commises les 18 juillet 2009, 10 novembre et 3 décembre 2010, 17 juillet 2013 et 1er octobre 2014 ; que le ministre de l'intérieur se pourvoit en cassation contre le jugement du 1er décembre 2015 par lequel le tribunal administratif de Bordeaux a annulé cette décision à la demande de Mme B...;<br/>
<br/>
              2. Considérant que, d'une part, aux termes du premier alinéa de l'article L. 223-6 du code de la route, dans sa rédaction antérieure à la loi du 14 mars 2011 d'orientation et de programmation pour la performance de la sécurité intérieure : " Si le titulaire du permis de conduire n'a pas commis, dans le délai de trois ans à compter de la date du paiement de la dernière amende forfaitaire, de l'émission du titre exécutoire de la dernière amende forfaitaire majorée, de l'exécution de la dernière composition pénale ou de la dernière condamnation définitive, une nouvelle infraction ayant donné lieu au retrait de points, son permis est affecté du nombre maximal de points " ; que l'article 76 de la loi du 14 mars 2011 a modifié ces dispositions en ramenant à deux ans à compter de l'un des quatre événements qu'elles mentionnent le délai de reconstitution du nombre maximal de points et a ajouté au même article L. 223-6 un deuxième alinéa disposant que : " Le délai de deux ans mentionné au premier alinéa est porté à trois ans si l'une des infractions ayant entraîné un retrait de points est un délit ou une contravention de la quatrième ou de la cinquième classe " ; que, d'autre part, l'article 138 de la loi du 14 mars 2011 prévoit que la modification apportée par cette loi à l'article L. 223-6 du code de la route " s'applique aux infractions commises à compter du 1er janvier 2011 et aux infractions antérieures pour lesquelles le paiement de l'amende forfaitaire, l'émission du titre exécutoire de l'amende forfaitaire majorée, l'exécution de la composition pénale ou la condamnation définitive ne sont pas intervenus " ;<br/>
<br/>
              3. Considérant qu'il résulte de ces dispositions que la date à laquelle la réalité d'une infraction entraînant retrait de points du permis de conduire est établie par le paiement de l'amende forfaitaire, l'émission d'un titre exécutoire d'amende forfaitaire majorée, l'exécution d'une composition pénale ou le prononcé d'une condamnation pénale définitive, fait courir un délai à l'expiration duquel, en l'absence de nouvelle infraction ayant entraîné un retrait de points, le titulaire du permis bénéficie d'une reconstitution intégrale de son capital de points ; que, lorsque la réalité de l'infraction a été établie à une date antérieure au 1er janvier 2011, la reconstitution de points n'a pu intervenir, en l'absence de nouvelle infraction, qu'à l'expiration du délai de trois ans prévu dans tous les cas par les dispositions de l'article L. 223-6 dans sa rédaction antérieure à la loi du 14 mars 2011 ; que, lorsque la réalité de l'infraction a été établie postérieurement au 31 décembre 2010, la durée du délai de reconstitution intégrale est déterminée par les dispositions du même article tel que modifié par cette loi ; qu'elle est normalement de deux ans mais est portée à trois ans si une des infractions commises par l'intéressé depuis la délivrance de son permis de conduire ou, le cas échéant, depuis la date de la dernière reconstitution intégrale opérée en application des deux premiers alinéas de l'article L. 223-6 a présenté le caractère d'un délit ou d'une contravention de la quatrième ou de la cinquième classe ;<br/>
<br/>
              4. Considérant qu'il résulte de ce qui précède que, pour déterminer si Mme B... devait bénéficier du délai de reconstitution du nombre maximal de points de deux ans à la suite de l'émission, le 25 février 2011, du titre exécutoire de l'amende forfaitaire majorée relative à l'infraction qu'elle avait commise le 10 novembre 2010, il appartenait au tribunal administratif de s'assurer que l'intéressée n'avait pas commis d'infraction revêtant le caractère d'un délit ou d'une contravention de la quatrième ou de la cinquième classe depuis la délivrance de son permis de conduire ou, le cas échéant, depuis la date de la dernière reconstitution intégrale de son capital de points ; qu'il ressortait des pièces du dossier qui lui était soumis que Mme B...avait, le 18 juillet 2009, dépassé la vitesse maximale autorisée ; qu'alors même que le dépassement n'avait pas excédé 20 km/h, cette infraction présentait, en vertu du I de l'article R. 413-14 du code de la route, le caractère d'une contravention de la quatrième classe dès lors que la vitesse maximale autorisée était égale ou inférieure à 50 km/h ; que, par suite, le délai de reconstitution intégrale du capital de points du permis était de trois ans par application des dispositions citées ci-dessus du deuxième alinéa de l'article L. 223-6 du code de la route, modifié par la loi du 14 mars 2011 ; qu'en jugeant que ce délai était de deux ans, le tribunal administratif de Bordeaux a commis une erreur de droit ; que son jugement doit, dès lors, être annulé ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du 1er décembre 2015 du tribunal administratif de Bordeaux est annulé. <br/>
<br/>
		Article 2 : L'affaire est renvoyée au tribunal administratif de Bordeaux.  <br/>
<br/>
Article 3 : La présente décision sera notifiée au ministre de l'intérieur et à Mme A...B....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
