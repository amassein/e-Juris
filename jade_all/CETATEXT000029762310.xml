<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029762310</ID>
<ANCIEN_ID>JG_L_2014_11_000000359457</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/76/23/CETATEXT000029762310.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  6ème sous-section jugeant seule, 14/11/2014, 359457, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-11-14</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>359457</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 6ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP WAQUET, FARGE, HAZAN ; SCP GARREAU, BAUER-VIOLAS, FESCHOTTE-DESBOIS ; SCP FABIANI, LUC-THALER</AVOCATS>
<RAPPORTEUR>Mme Mireille Le Corre</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Xavier de Lesquen</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2014:359457.20141114</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et les mémoires complémentaires, enregistrés les 16 mai, 23 août et 19 novembre 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour l'association interdépartementale et intercommunale pour la protection du lac de Sainte-Croix, de son environnement et des lacs et sites du Verdon, dont l'adresse postale est Hameau du Pont d'Aiguines, Les Salles du Verdon BP n° 1 à Aups (83630), représentée par son président ; l'association interdépartementale et intercommunale pour la protection du lac de Sainte-Croix, de son environnement et des lacs et sites du Verdon demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 10MA01957 du 15 mars 2012 par lequel la cour administrative d'appel de Marseille, après avoir annulé l'article 2 du jugement n° 0705679-0803847-0902196 du 2 avril 2010 du tribunal administratif de Toulon décidant qu'il n'y avait pas lieu de statuer sur les demandes n° 0705679 et 0803847 de l'association requérante tendant à l'annulation, respectivement, de l'arrêté du 26 juin 2006 par lequel le maire de Bauduen a délivré le permis de construire n° PC 83 0150 05AC021 à M. A...B...en vue de l'édification d'une bergerie d'une surface brute de 625 m² sur un terrain cadastré section C n°191 au lieu-dit Majastre, et de l'arrêté du 19 juin 2008 par lequel le maire a délivré le permis de construire         n° PC 083 015 08 A002 à l'intéressé pour le même projet, a jugé qu'il n'y avait pas lieu de statuer sur ces demandes, a rejeté le surplus des conclusions de sa requête tendant à l'annulation du jugement précité en tant qu'il a rejeté sa demande tendant à l'annulation de l'arrêté du 26 août 2009 par lequel le maire a retiré le permis de construire du 19 juin 2008 et a délivré un nouveau permis de construire PC 083 015 08 A0002 à M. B...pour la même construction ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à ses conclusions d'appel ;<br/>
<br/>
              3°) de mettre à la charge solidairement de la commune de Bauduen et de M. B...la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 16 octobre 2014, présentée pour l'association interdépartementale et intercommunale pour la protection du lac de Sainte-Croix, de son environnement et des lacs et sites du Verdon ;<br/>
<br/>
              Vu le code de l'urbanisme ;<br/>
<br/>
              Vu le code de l'environnement ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Mireille Le Corre, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Xavier de Lesquen, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Waquet, Farge, Hazan, avocat de l'association interdépartementale et intercommunale pour la protection du lac de Sainte-Croix, à la SCP Fabiani, Luc-Thaler, avocat de la commune de Bauduen et à la SCP Garreau, Bauer-Violas, Feschotte-Desbois, avocat de M. A...B...;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que le maire de Bauduen a successivement délivré à M. A...B..., à la suite de diverses procédures administratives et contentieuses, trois permis de construire similaires, par arrêtés des 26 juin 2006, 19 juin 2008 et 26 août 2009, en vue de l'édification d'une bergerie d'une surface brute de 625 m² sur un terrain, soumis aux dispositions d'urbanisme relatives au littoral et à la montagne, situé dans le parc naturel régional du Verdon, près du lac de Saint-Croix ; que l'arrêté du 19 juin 2008 procède au retrait d'un précédent arrêté du 22 avril 2008 portant refus de permis de construire et celui du 26 août 2009 au retrait de l'arrêté du 19 juin 2008 précité, auquel il s'est substitué ; que, par un jugement du 2 avril 2010, le tribunal administratif de Toulon a rejeté les conclusions de l'association interdépartementale et intercommunale pour la protection du lac de Sainte-Croix, de son environnement et des lacs et sites du Verdon dirigées contre l'arrêté du 26 août 2009 et décidé qu'il n'y avait pas lieu de statuer sur celles dirigées contre les deux précédents arrêtés des 26 juin 2006 et 19 juin 2008 ; que, par un arrêt du 15 mars 2012, contre lequel  cette association se pourvoit en cassation, la cour administrative d'appel de Marseille, après avoir partiellement annulé le jugement, a confirmé qu'il n'y avait pas lieu de statuer sur les conclusions dirigées contre les arrêtés des 26 juin 2006 et 19 juin 2008 et rejeté le surplus des conclusions de la requête ; <br/>
<br/>
<br/>
<br/>
              Sur le pourvoi :<br/>
<br/>
              2. Considérant qu'aux termes de l'article R. 741-2 du code de justice administrative : " La décision mentionne que l'audience a été publique (...). / Elle contient le nom des parties, l'analyse des conclusions et mémoires ainsi que les visas des dispositions législatives ou réglementaires dont il est fait application / (...) / Mention est également faite de la production d'une note en délibéré. / (...) " ; qu'il ressort des pièces de la procédure qu'après l'audience publique du 23 février 2012, l'association requérante a adressé à la cour administrative d'appel de Nantes une note en délibéré, datée du 24 février 2012, qui a été enregistrée au greffe de cette cour le 28 février 2012 ; que l'arrêt attaqué, dont les visas ne font pas mention de ce mémoire, est ainsi entaché d'une irrégularité ; qu'en conséquence, et sans qu'il soit besoin d'examiner les autres moyens de son pourvoi, l'association requérante est fondée à demander l'annulation de l'arrêt qu'elle attaque ;<br/>
<br/>
              3. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond, en application des dispositions de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              Sur la requête d'appel :<br/>
<br/>
              En ce qui concerne la régularité du jugement attaqué :<br/>
<br/>
              4. Considérant, en premier lieu, que le moyen tiré de ce que le jugement attaqué ne serait pas signé par le rapporteur, le président de la formation de jugement et le greffier d'audience, en méconnaissance des dispositions de l'article R. 741-7 du code de justice administrative, manque en fait ;<br/>
<br/>
              5. Considérant, en deuxième lieu, que la requérante soutient que les premiers juges ont irrégulièrement soulevé d'office, sans avoir préalablement informé les parties ainsi que l'exigent les dispositions de l'article R. 611-7 du code de justice administrative, le moyen tiré de ce que les demandes n° 0705679 et n° 0803847 étaient devenues sans objet ; qu'il ressort toutefois des pièces de la procédure que l'association requérante et la commune de Bauduen avaient présenté des conclusions à fin de non-lieu sur les arrêtés concernés par ces demandes ; que, par suite, le moyen ci-dessus mentionné doit être écarté ;<br/>
<br/>
              6. Considérant, enfin, qu'il ne résulte ni des dispositions des articles R. 612-3 et R. 612-6 du code de justice administrative ni d'aucune autre disposition ou principe que le juge serait tenu, à peine d'irrégularité de sa décision, de mettre en demeure les parties qui ne l'ont pas fait de produire un mémoire ; qu'il ne ressort pas des pièces de la procédure que le tribunal administratif de Toulon, qui a tenu audience le 2 avril 2010 et rendu sa décision le 2 avril suivant, pour des demandes d'ailleurs similaires enregistrées respectivement les 25 octobre 2007, 2 juillet 2008 et 3 septembre 2009, et dont il n'est pas soutenu qu'il aurait méconnu les obligations relatives à la communication des écritures des parties, aurait instruit et jugé les demandes dont il était saisi de façon excessivement hâtive ; que le moyen tiré de ce que le tribunal administratif aurait cherché à anticiper sur une décision du Conseil d'Etat statuant sur un pourvoi dirigé contre une ordonnance du juge des référés du tribunal administratif est, en tout état de cause, inopérant ; que, par suite, le moyen tiré de la méconnaissance du principe du caractère contradictoire de la procédure juridictionnelle doit être écarté ;<br/>
<br/>
<br/>
<br/>
              En ce qui concerne les conclusions dirigées contre l'arrêté du 26 août 2009 :<br/>
<br/>
              7. Considérant, en premier lieu, que le permis de construire du 26 août 2009 est identique à celui qui avait été délivré le 19 juin 2008 et dont le juge des référés du tribunal administratif de Toulon avait ordonné la suspension de l'exécution ; que, saisi de la demande du 30 juillet 2009, qui avait pour seul objet de le ressaisir du même projet afin de répondre à l'irrégularité de procédure retenue par le juge des référés comme étant de nature à créer un doute sérieux sur la légalité de la décision attaquée, le maire n'était pas tenu, en l'absence de changement de circonstances de fait ou de droit, de reprendre dans sa totalité l'instruction du permis ; qu'est sans incidence la circonstance qu'un précédent arrêté du 22 avril 2008 avait opposé un refus de permis de construire à M. B...; que les moyens tirés de la méconnaissance des dispositions du code de l'urbanisme relatives à l'instruction des demandes de permis de construire et de ce que le maire ne pouvait légalement statuer sur une demande de permis qui était caduque doivent être écartés ; <br/>
<br/>
              8. Considérant, en deuxième lieu, qu'aux termes de l'article R. 431-8 du code de l'urbanisme : " Le projet architectural comprend une notice précisant : / 1° L'état initial du terrain et de ses abords indiquant, s'il y a lieu, les constructions, la végétation et les éléments paysagers existants ; / 2° Les partis retenus pour assurer l'insertion du projet dans son environnement et la prise en compte des paysages, faisant apparaître, en fonction des caractéristiques du projet : / a) L'aménagement du terrain, en indiquant ce qui est modifié ou supprimé ; b) L'implantation, l'organisation, la composition et le volume des constructions nouvelles, notamment par rapport aux constructions ou paysages avoisinants ; c) Le traitement des constructions, clôtures, végétations ou aménagements situés en limite de terrain ; d) Les matériaux et les couleurs des constructions ; e) Le traitement des espaces libres, notamment les plantations à conserver ou à créer ; f) L'organisation et l'aménagement des accès au terrain, aux constructions et aux aires de stationnement. " ; qu'aux termes de l'article R. 431-9 du même code : " Le projet architectural comprend également un plan de masse des constructions à édifier ou à modifier coté dans les trois dimensions. Ce plan de masse fait apparaître les travaux extérieurs aux constructions, les plantations maintenues, supprimées ou créées et, le cas échéant, les constructions existantes dont le maintien est prévu (...) " ; qu'aux termes de l'article R. 431-10 du même code : " Le projet architectural comprend également : / (...) c) Un document graphique permettant d'apprécier l'insertion du projet de construction par rapport aux constructions avoisinantes et aux paysages, son impact visuel ainsi que le traitement des accès et du terrain (...) " ; que le projet architectural comporte une notice mentionnant les caractéristiques du sol, l'absence de construction sur la parcelle, ainsi que le type de végétation ; que cette notice décrit le projet de bergerie, en précisant les matériaux utilisés et en en justifiant les dimensions par les contraintes de l'élevage ; que le projet architectural comporte également un plan de masse, coté en deux dimensions, auquel sont joints notamment une coupe altimétrique, qui fournit des indications sur le profil du terrain naturel, ainsi que des plans de façade indiquant les hauteurs de la construction ; que le projet architectural comporte enfin une photographie qui permet de situer la construction dans son environnement ; que le caractère succinct de certains des éléments fournis, que peuvent, au demeurant, suppléer les autres pièces du dossier de demande de permis de construire, se justifie, en l'espèce, par la simplicité des volumes de la construction projetée ; qu'il résulte des pièces du dossier qu'un paysagiste conseil des services du ministère chargé de l'équipement, dont l'intervention a été requise au cours de l'instruction, s'est rendu sur place et a précisé les prescriptions du permis au vu des réserves assortissant l'accord du préfet ; qu'il résulte de ce qui précède que le maire de Bauduen a pu légalement délivré le permis de construire au vu de l'ensemble des éléments ainsi recueillis ; <br/>
<br/>
              9. Considérant, en troisième lieu, qu'aux termes du I de l'article L. 146-4 du code de l'urbanisme : " L'extension de l'urbanisation doit se réaliser soit en continuité avec les agglomérations et villages existants, soit en hameaux nouveaux intégrés à l'environnement. / Par dérogation aux dispositions de l'alinéa précédent, les constructions ou installations liées aux activités agricoles ou forestières qui sont incompatibles avec le voisinage des zones habitées peuvent être autorisées, en dehors des espaces proches du rivage, avec l'accord du préfet après avis de la commission départementale compétente en matière de nature, de paysages et de sites. Cet accord est refusé si les constructions ou installations sont de nature à porter atteinte à l'environnement ou aux paysages (...) " ; <br/>
<br/>
              10. Considérant qu'à l'issue de sa séance du 30 mai 2008, la commission départementale compétente en matière de nature, de paysages et de sites a émis un avis favorable au projet litigieux, sous réserve que le pétitionnaire prenne l'attache du paysagiste conseil des services du ministère chargé de l'équipement, afin d'assurer la meilleure intégration paysagère du bâtiment, notamment par la plantation d'arbres autour de la construction ; que cet avis n'est pas soumis, en application des dispositions citées au point 9, à une obligation de motivation ; qu'il ne résulte pas des pièces du dossier, eu égard notamment au compte-rendu de la séance du 30 mai 2008, que la commission n'aurait pas procédé à un examen effectif du projet litigieux ; que, par suite et en tout état de cause, le moyen tiré de ce que cet avis ne serait pas motivé ne peut être accueilli ;<br/>
<br/>
              11. Considérant, en quatrième lieu, que, contrairement à ce que soutient la requérante, le préfet qui, avant de se prononcer conformément aux dispositions de l'article L. 146-4 ci-dessus rappelées, a dû porter sa propre appréciation sur le projet, n'était pas en situation de compétence liée pour refuser son accord en raison de la réserve émise par cette commission ;<br/>
<br/>
              12. Considérant, en cinquième lieu, d'une part, que le préfet a assorti cet accord d'une prescription, demandant au pétitionnaire, comme l'avait demandé la commission, de prendre l'attache du paysagiste conseil des services du ministère chargé de l'équipement, afin d'assurer la meilleure intégration paysagère possible du projet de bâtiment, notamment par la plantation d'arbres autour de la construction ; que, contrairement à ce que soutient la requérante, le préfet peut assortir son accord d'une réserve à la condition qu'elle n'entraîne que des modifications sur des points précis et limités et ne nécessitant pas la présentation d'un nouveau projet ; que, d'autre part, le terrain d'assiette du projet litigieux, qui est situé à plus de cinq kilomètres du lac de Sainte-Croix et sans covisibilité avec celui-ci, ne se trouve pas dans un espace proche du rivage au sens de la loi littoral ; que l'activité d'élevage qui doit s'y dérouler n'est pas compatible avec le voisinage de zones habitées en raison des nuisances sonores et sanitaires qu'elle est susceptible de générer ; que par suite, et compte tenu de ce qui est dit aux points 17 et 18, en donnant le 10 juin 2008 un accord à la construction d'une bergerie dans un espace où l'agropastoralisme s'exerce traditionnellement, le préfet n'a pas commis d'erreur manifeste dans l'appréciation qu'il lui appartenait de porter sur les atteintes que cette construction était susceptible de porter à l'environnement ou aux paysages ; que, dès lors, les moyens dirigés contre cet accord doivent être écartés ;<br/>
<br/>
              13. Considérant, en sixième lieu, qu'il résulte du permis litigieux qu'il a repris les prescriptions assortissant l'accord préfectoral du 10 juin 2008 ; que, dès lors, l'association requérante n'est pas fondée à soutenir que l'accord du préfet n'aurait pas été respecté ;<br/>
<br/>
              14. Considérant, en septième lieu, que le maire pouvait légalement assortir le permis de construire du 26 août 2009 de prescriptions particulières sans qu'il soit nécessaire de présenter un nouveau projet dès lors que ces conditions n'entraînent que des modifications sur des points précis et limités ; <br/>
<br/>
              15. Considérant, en huitième lieu, que la demande de permis de construire porte sur une construction à usage exclusivement agricole et liée à l'activité d'élevage ; qu'au vu du dossier dont il était saisi, le maire de Bauduen ne disposait d'aucun élément susceptible de jeter un doute sur la destination de la construction projetée ; que l'association requérante ne peut utilement exciper devant le juge de l'excès de pouvoir de ce que la destination finalement donnée à cet ouvrage ne serait pas conforme à celle pour laquelle il a été autorisé ; <br/>
<br/>
              16. Considérant, en neuvième lieu, qu'aux termes de l'article R. 111-15 du code de l'urbanisme : " Le permis ou la décision prise sur la déclaration préalable doit respecter les préoccupations d'environnement définies aux articles L. 110-1 et L. 110-2 du code de l'environnement. Le projet peut n'être accepté que sous réserve de l'observation de prescriptions spéciales si, par son importance, sa situation ou sa destination, il est de nature à avoir des conséquences dommageables pour l'environnement. " ; qu'aux termes de l'article 3 de l'arrêté du 23 avril 2007 fixant les listes des insectes protégés sur l'ensemble du territoire et les modalités de leur protection : " Pour les espèces d'insectes dont la liste est fixée ci-après : /  I. - Sont interdits, sur tout le territoire métropolitain et en tout temps, la destruction ou l'enlèvement des oeufs, des larves et des nymphes, la destruction, la mutilation, la capture ou l'enlèvement des animaux. / II. - Sont interdits, sur tout le territoire national et en tout temps, la détention, le transport, la naturalisation, le colportage, la mise en vente, la vente ou l'achat, l'utilisation, commerciale ou non, des spécimens prélevés: / - dans le milieu naturel du territoire métropolitain de la France, après le 24 septembre 1993 ; / - dans le milieu naturel du territoire européen des autres Etats membres de l'Union européenne, après la date d'entrée en vigueur de la directive du 21 mai 1992 susvisée. / (...) /  Le criquet hérisson (Prionotropis hystrix spp. azami) (Uvarov, 1923) (...) " ; qu'il ressort des pièces du dossier que si la présence de ces insectes protégés a été antérieurement décelée sur le site, le pastoralisme ovin contribue à réduire la lignification excessive des milieux et fournit ainsi à cette espèce un environnement favorable à son développement ; qu'en l'absence d'autres éléments de nature à accréditer l'existence des risques allégués, le moyen tiré de ce que le maire de Bauduen aurait entaché sa décision d'une erreur manifeste d'appréciation et méconnu les dispositions de l'article R. 111-15 du code de l'urbanisme, dès lors que la construction de la bergerie aurait pour effet de détruire les criquets hérissons présents sur le site compte tenu de la faible mobilité de ces derniers, doit être écarté ; <br/>
<br/>
              17. Considérant, en dixième lieu, qu'aux termes de l'article R. 111-21 du code de l'urbanisme : " Le projet peut être refusé ou n'être accepté que sous réserve de l'observation de prescriptions spéciales si les constructions, par leur situation, leur architecture, leurs dimensions ou l'aspect extérieur des bâtiments ou ouvrages à édifier ou à modifier, sont de nature à porter atteinte au caractère ou à l'intérêt des lieux avoisinants, aux sites, aux paysages naturels ou urbains ainsi qu'à la conservation des perspectives monumentales. " ; qu'aux termes de l'article NC11 du règlement du plan d'occupation des sols : " En aucun cas, les constructions, installations et dépôts à l'air libre ne doivent par leur situation, leurs dimensions ou leur aspect extérieur, porter atteinte au site (...) " ; que la construction dont s'agit est un bâtiment agricole de couleur ocre présentant une façade de 50 m de longueur, pour une surface hors oeuvre brute de 625 m² ; qu'un soin particulier a été, à la demande du préfet, apporté à l'intégration paysagère de cette construction, avec la plantation parallèlement à la route et à la bergerie, d'essences locales, telles que des chênes truffiers, chênes verts et chênes blancs mélangés à des cèdres de l'Atlas, ainsi que des genévriers, cade, lavande et thym ; qu'en raison de l'environnement naturel assez arboré du site, le bâtiment devrait à terme se fondre dans le paysage ; que la qualité des matériaux utilisés, charpente en bois, tuiles, parpaings recouverts d'un enduit ocre, favorise cette intégration ; que, dès lors, et alors même que le terrain d'assiette est situé dans les espaces remarquables du parc naturel régional du Verdon, en délivrant le permis de construire dans un lieu où s'exerce traditionnellement l'agropastoralisme, classé en zone NC du plan d'occupation des sols, le maire de Bauduen n'a pas commis d'erreur manifeste d'appréciation dans son application de l'article R. 111-21 du code de l'urbanisme ni méconnu l'article NC11 du règlement du plan d'occupation des sols ;<br/>
<br/>
              18. Considérant, en dernier lieu, qu'aux termes du II de l'article L. 145-3 du code de l'urbanisme : " Les documents et décisions relatifs à l'occupation des sols comportent les dispositions propres à préserver les espaces, paysages et milieux caractéristiques du patrimoine naturel et culturel montagnard. " ; qu'il résulte de ces dispositions que, dans les espaces, milieux et paysages caractéristiques du patrimoine naturel et culturel montagnard, les documents et décisions relatifs à l'occupation des sols doivent être compatibles avec les exigences de préservation de ces espaces ; que, pour satisfaire à cette exigence de compatibilité, les documents et décisions cités ci-dessus doivent comporter des dispositions de nature à concilier l'occupation du sol projetée et les aménagements s'y rapportant avec l'exigence de préservation de l'environnement montagnard prévue par la loi ; que le projet de construction, qui porte sur l'édification d'une bergerie, est situé dans un espace où l'agropastoralisme s'exerce traditionnellement ; que, compte tenu de ce qui a été dit au point précédent, il est de nature à préserver les espaces, paysages et milieux caractéristiques du patrimoine naturel et culturel montagnard ; que le moyen tiré de la méconnaissance du II de l'article L. 145-3 doit être écarté ;<br/>
<br/>
              19. Considérant qu'il résulte de tout ce qui précède que l'association requérante n'est pas fondée à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Toulon a rejeté sa demande tendant à l'annulation de l'arrêté du 26 août 2009 par lequel le maire de la commune de Bauduen a retiré le permis de construire du 19 juin 2008 et a délivré un nouveau permis de construire à M. B...en vue de l'édification d'une bergerie ;<br/>
<br/>
              Sur les conclusions dirigées contre les arrêtés des 26 juin 2006 et 19 juin 2008 :<br/>
<br/>
              20. Considérant qu'à la date à laquelle le tribunal administratif s'est prononcé, les permis de construire des 26 juin 2006 et 19 juin 2008 n'avaient pas été définitivement retirés de l'ordonnancement juridique ; que, par suite, c'est à tort que, par l'article 2 du jugement attaqué, le tribunal administratif a décidé qu'il n'y avait pas lieu de statuer sur les demandes n° 0705679 et 0803847 ; que, dès lors, le jugement attaqué doit être annulé dans cette mesure ; qu'il y a lieu d'évoquer et de statuer sur les demandes présentées par la requérante devant le tribunal administratif de Toulon ;<br/>
<br/>
              21. Considérant qu'il résulte de la présente décision que le retrait du permis de construire du 19 juin 2008 est devenu définitif ; que, par suite, la demande d'annulation dirigée contre ce permis de construire est devenue sans objet ; que le permis de construire du 26 août 2009 s'est implicitement mais nécessairement substitué au permis de construire du 26 juin 2006 ; que la demande d'annulation dirigée contre ce permis de construire est, par suite, également devenue sans objet ;<br/>
              Sur les conclusions tendant à la suppression d'un passage injurieux :<br/>
<br/>
              22. Considérant qu'aux termes de l'article L. 741-2 du code de justice administrative : " Sont également applicables les dispositions des alinéas 3 à 5 de l'article 41 de la loi du 29 juillet 1881 ci-après reproduites : " Art. 41, alinéas 3 à 5. - Ne donneront lieu à aucune action en diffamation, injure ou outrage, ni le compte rendu fidèle fait de bonne foi des débats judiciaires, ni les discours prononcés ou les écrits produits devant les tribunaux. Pourront néanmoins les juges, saisis de la cause et statuant sur le fond, prononcer la suppression des discours injurieux, outrageants ou diffamatoires, et condamner qui il appartiendra à des dommages-intérêts. (...) " ;<br/>
<br/>
              23. Considérant que, contrairement à ce que soutient l'association requérante, la phrase selon laquelle : " Contrairement aux représentants de l'association (pièce adverse n°6), M. A...B...n'a pas pour habitude de pénétrer sur la propriété d'autrui pour prendre des photographies. ", ne présente pas un caractère injurieux ou diffamatoire qui justifierait qu'elle soit supprimée en application des dispositions précitées ; que, dans ces conditions, les conclusions tendant à cette suppression doivent être rejetées ; <br/>
<br/>
              Sur les conclusions tendant à l'application de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              24. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mise à la charge de M. B...et de la commune de Bauduen, qui ne sont pas la partie perdante, la somme que demande l'association interdépartementale et intercommunale pour la protection du lac de Sainte-Croix, de son environnement et des lacs et sites du Verdon au titre des frais exposés par elle et non compris dans les dépens ; qu'il y a lieu, en revanche, dans les circonstances de l'espèce, de mettre à la charge de l'association requérante une somme de 2 000 euros à payer à la commune de Bauduen et une somme de 2 000 euros à payer à M. A...B...au même titre ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
  Article 1er : L'arrêt de la cour administrative d'appel de Marseille du 15 mars 2012 est annulé.<br/>
<br/>
  Article 2 : L'article 2 du jugement n° 0705679-0803847-0902196 du 2 avril 2010 du tribunal administratif de Toulon est annulé.<br/>
<br/>
  Article 3 : Il n'y a pas lieu de statuer sur les demandes n° 0705679 et n° 0803847.<br/>
<br/>
  Article 4 : Le surplus des conclusions de la requête de l'association interdépartementale et intercommunale pour la protection du lac de Sainte-Croix, de son environnement et des lacs et sites du Verdon est rejeté.<br/>
<br/>
  Article 5 : L'association interdépartementale et intercommunale pour la protection du lac de Sainte-Croix, de son environnement et des lacs et sites du Verdon versera à la commune de Bauduen et à M. A...B...une somme de 2 000 euros chacun au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
  Article 6 : La présente décision sera notifiée à l'association interdépartementale et intercommunale pour la protection du lac de Sainte-Croix, de son environnement et des lacs et sites du Verdon, à la commune de Bauduen et à M. A...B....<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
