<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000041560780</ID>
<ANCIEN_ID>JG_L_2020_02_000000437651</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/41/56/07/CETATEXT000041560780.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 06/02/2020, 437651, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-02-06</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>437651</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LYON-CAEN, THIRIEZ</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2020:437651.20200206</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés les 15 et 30 janvier 2020 au secrétariat du contentieux du Conseil d'Etat, l'association One Voice demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative :<br/>
<br/>
              1°) de suspendre l'exécution de l'arrêté interministériel du 30 décembre 2019 portant expérimentation de diverses dispositions en matière de dérogations aux interdictions de destruction pouvant être accordées par les préfets concernant le loup (Canis lupus) ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Elle soutient que :<br/>
              - la condition d'urgence est remplie, dès lors que l'augmentation du nombre maximum de loups dont la destruction est autorisée porte une atteinte grave et immédiate aux intérêts qu'elle entend défendre, fait courir un risque aux éleveurs en favorisant les attaques et n'est pas de nature par elle-même à diminuer les atteintes portées aux élevages ;<br/>
              - il existe des doutes sérieux quant à la légalité de l'arrêté contesté ;<br/>
              - ses articles 2 et 5 sont entachés d'incompétence et méconnaissent le décret du 12 septembre 2018 relatif à certaines attributions du préfet coordonnateur du plan national d'actions sur le loup, en tant qu'ils attribuent à ce dernier des compétences non prévues par le décret ; <br/>
              - les dérogations qu'il prévoit ne sont pas justifiées au regard de la directive 92/43/CEE du Conseil du 21 mai 1992 transposée dans le code de l'environnement, qui les subordonne à la triple condition de dommages importants dus aux loups, à l'absence d'autre solution satisfaisante et au maintien dans un état de conservation favorable de l'espèce dans son aire de répartition naturelle ;<br/>
              - il porte atteinte aux principes de précaution et de non régression prévus à l'article 191 du traité sur le fonctionnement de l'Union européenne (TFUE) et à l'article L. 110-1 du code de l'environnement, dès lors que les données scientifiques concernant la population de loups en France sont incertaines.<br/>
<br/>
              Par un mémoire en défense, enregistré le 28 janvier 2020, la ministre de la transition écologique et solidaire conclut au rejet de la requête. Elle soutient que la condition d'urgence n'est pas remplie et qu'aucun des moyens soulevés n'est propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de l'arrêté contesté.<br/>
<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, l'association One Voice et, d'autre part, la ministre de la transition écologique et solidaire ;<br/>
<br/>
              Ont été entendus au cours de l'audience publique du vendredi 31 janvier 2020, à 11 heures :<br/>
<br/>
              - Me Lyon-Caen, avocat au Conseil d'Etat et à la Cour de cassation, avocat de l'association One Voice ;<br/>
<br/>
              - la représentante de l'association One Voice ;<br/>
<br/>
              - les représentants de la ministre de la transition écologique et solidaire ;<br/>
              à l'issue de laquelle le juge des référés a reporté la clôture de l'instruction au 31 janvier 2020 à 19 heures ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le traité sur le fonctionnement de l'Union européenne ;<br/>
              - la directive 92/43/CEE du Conseil du 21 mai 1992 concernant la conservation des habitats naturels ainsi que de la faune et de la flore sauvages ;<br/>
              - le code de l'environnement ;<br/>
              - le décret n° 2018-786 du 12 septembre 2018 ;<br/>
              - l'arrêté du 19 février 2018 fixant les conditions et limites dans lesquelles des dérogations aux interdictions de destruction peuvent être accordées par les préfets concernant le loup (Canis lupus) ;<br/>
              - l'arrêté du 19 février 2018 fixant le nombre maximum de spécimens de loups (Canis lupus) dont la destruction pourra être autorisée chaque année ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
              1. Aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision ".<br/>
<br/>
              2. Aux termes de l'article 12 de la directive 92/43/CEE du 21 mai 1992 concernant la conservation des habitats naturels ainsi que de la faune et de la flore sauvage, dite directive " Habitats " : " 1. Les Etats membres prennent les mesures nécessaires pour instaurer un système de protection stricte des espèces animales figurant à l'annexe IV point a), dans leur aire de répartition naturelle, interdisant : a) toute forme de capture ou de mort intentionnelle de spécimens de ces espèces dans la nature ; b) la perturbation intentionnelle de ces espèces, notamment durant la période de reproduction et de dépendance (...) ". Le loup est au nombre des espèces figurant au point a) de l'annexe IV de la directive. L'article 16 de la même directive énonce toutefois que : " 1. A condition qu'il n'existe pas une autre solution satisfaisante et que la dérogation ne nuise pas au maintien, dans un état de conservation favorable, des populations des espèces concernées dans leur aire de répartition naturelle, les Etats membres peuvent déroger aux dispositions des articles 12, 13, 14 et de l'article 15 points a) et b) : (...) b) pour prévenir des dommages importants notamment aux cultures, à l'élevage, aux forêts, aux pêcheries, aux eaux et à d'autres formes de propriété ".<br/>
<br/>
              3. Aux termes du I de l'article L. 411-1 du code de l'environnement, pris pour la transposition de la directive " Habitats " : " Lorsqu'un intérêt scientifique particulier, le rôle essentiel dans l'écosystème ou les nécessités de la préservation du patrimoine naturel justifient la conservation (...) d'espèces animales non domestiques (...) et de leurs habitats, sont interdits : 1° (...) la mutilation, la destruction, la capture ou l'enlèvement, la perturbation intentionnelle, la naturalisation d'animaux de ces espèces (...) ". Aux termes de l'article L. 411-2 du même code, pris pour la transposition de l'article 16 de la même directive : " Un décret en Conseil d'Etat détermine les conditions dans lesquelles sont fixées : 1° La liste limitative des habitats naturels, des espèces animales non domestiques (...) ainsi protégés ; 2° La durée et les modalités de mise en oeuvre des interdictions prises en application du I de l'article L. 411-1 ; 3° La partie du territoire sur laquelle elles s'appliquent (...) ; 4° La délivrance de dérogations aux interdictions mentionnées aux 1°, 2° et 3° de l'article L. 411-1, à condition qu'il n'existe pas d'autre solution satisfaisante, pouvant être évaluée par une tierce expertise menée, à la demande de l'autorité compétente, par un organisme extérieur choisi en accord avec elle, aux frais du pétitionnaire, et que la dérogation ne nuise pas au maintien, dans un état de conservation favorable, des populations des espèces concernées dans leur aire de répartition naturelle : a) Dans l'intérêt de la protection de la faune et de la flore sauvages et de la conservation des habitats naturels ; / b) Pour prévenir des dommages importants notamment aux cultures, à l'élevage (...) et à d'autres formes de propriété ".<br/>
<br/>
              4. Pour l'application de ces dernières dispositions, l'article R. 411-1 du code de l'environnement prévoit que la liste des espèces animales non domestiques faisant l'objet des interdictions définies à l'article L. 411-1 est établie par arrêté conjoint du ministre chargé de la protection de la nature et du ministre chargé de l'agriculture. L'article R. 411-6 du même code précise que : " Les dérogations définies au 4° de l'article L. 411-2 sont accordées par le préfet, sauf dans les cas prévus aux articles R. 411-7 et R. 411-8. / (...) ". Son article R. 411-13 prévoit que les ministres chargés de la protection de la nature et de l'agriculture fixent par arrêté conjoint pris après avis du Conseil national de la protection de la nature " (...) / 2° Si nécessaire, pour certaines espèces dont l'aire de répartition excède le territoire d'un département, les conditions et limites dans lesquelles les dérogations sont accordées afin de garantir le respect des dispositions du 4° de l'article L. 411-2 du code de l'environnement ". <br/>
<br/>
              5. Dans ce cadre, a été pris, le 19 février 2018, un arrêté interministériel fixant les conditions et limites dans lesquelles des dérogations aux interdictions de destruction peuvent être accordées par les préfets concernant le loup. Son article 2 prévoit que le nombre maximum de loups dont la destruction est autorisée, en application de l'ensemble des dérogations qui pourront être accordées par les préfets, est fixé chaque année selon des modalités prévues par arrêté ministériel. Ses articles 3 à 5 prescrivent diverses mesures pour assurer le respect de ce seuil, en particulier l'obligation pour les bénéficiaires de dérogations d'informer les préfets en cas de destruction ou de blessure d'un loup lors des opérations qu'ils mettent en oeuvre et, pour les préfets, d'informer les administrations et établissements publics concernés ainsi que les bénéficiaires de dérogations. L'arrêté encadre les conditions dans lesquelles il peut être recouru à des mesures, d'effet gradué, et pouvant être combinées, destinées à mettre les troupeaux à l'abri de la prédation du loup. Ainsi, peuvent être opérés des opérations d'effarouchement aux fins d'éviter les tentatives de prédation du loup, des tirs de défense, éventuellement renforcés, destinés directement à défendre les troupeaux et des tirs de prélèvement, éventuellement renforcés, qui permettent la destruction en dehors d'une opération de protection immédiate d'un troupeau. Par ailleurs, dans certains " fronts de colonisation du loup ", définis par voie réglementaire, où il a été établi que les modes de conduite des troupeaux les rendent particulièrement vulnérables aux attaques de loup en l'absence de mesure de protection à la fois efficaces et compatibles avec ces modes de conduite, l'article 37 de l'arrêté permet, sous certaines conditions, de recourir aux tirs de défense et de prélèvement sans que les troupeaux bénéficient de mesure de protection.<br/>
<br/>
              6.  Un second arrêté interministériel en date du 19 février 2018 fixe le nombre maximum de spécimens de loups dont la destruction pourra être autorisée chaque année. Son article 1er fixe à quarante le nombre maximum de loups pouvant être détruits au cours de l'année civile 2018 tout en prévoyant une actualisation de ce nombre en cours d'année pour qu'il corresponde à 10 % de l'effectif moyen de l'espèce tel que calculé au printemps 2018. Son article 2 fixe, pour les années civiles suivantes, ce nombre à 10 % de l'effectif moyen de l'espèce tout en prévoyant un dépassement possible de ce plafond correspondant à 2 % de cet effectif moyen pour les tirs de défense simple comme renforcée lorsque le plafond de 10 % est atteint avant la fin de l'année civile.<br/>
<br/>
              7. La ministre de la transition écologique et solidaire et le ministre de l'agriculture et de la pêche ont pris, le 26 juillet 2019, un arrêté portant expérimentation de diverses dispositions en matière de dérogations aux interdictions de destruction pouvant être accordées par les préfets concernant le loup. Ces dispositions se sont appliquées jusqu'au 31 décembre 2019. <br/>
<br/>
              8. Les mêmes ministres ont pris, le 30 décembre 2019, un nouvel arrêté ayant le même objet, dont les dispositions s'appliquent jusqu'au 31 décembre 2020. L'association One Voice demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative, d'en suspendre l'exécution. <br/>
<br/>
              9. L'arrêté du 30 décembre 2019 déroge à la fois aux dispositions de l'arrêté du 19 février 2018 fixant les conditions et limites dans lesquelles des dérogations aux interdictions de destruction peuvent être accordées par les préfets concernant le loup et à celles de l'arrêté du même jour fixant le nombre maximum de loups dont la destruction peut être autorisée. <br/>
<br/>
              10. S'agissant des conditions et limites dans lesquelles des dérogations aux interdictions de destruction peuvent être accordées par les préfets, l'arrêté du 30 décembre 2019 procède aux aménagements suivants :<br/>
              - lorsque le nombre de destructions de loups atteint un seuil correspondant au plafond minoré de quatre unités, la suspension automatique de tous les arrêtés préfectoraux autorisant des destructions de loups pendant vingt-quatre heures après chaque destruction est limitée aux seuls arrêtés ordonnant des tirs de prélèvement, alors que l'arrêté du 19 février 2018 étend cette suspension automatique aux tirs de défense ;<br/>
              - une nouvelle zone, dite " cercle zéro ", est créée, concernant les communes où la récurrence interannuelle de dommages importants aux troupeaux a été constatée ; dans cette zone, ainsi que dans les zones mentionnées au I de l'article 37 de l'arrêté du 19 février 2018, c'est-à-dire les zones dans lesquelles la mise en oeuvre des mesures de protection des troupeaux contre la prédation du loup présente des difficultés importantes, peuvent être mise en oeuvre des tirs de défense " mixtes ", réalisés simultanément par trois tireurs au maximum ;<br/>
              - des tirs de prélèvement simples peuvent être mis en oeuvre à partir du 1er juillet en " cercle zéro ", dans les zones mentionnées au I de l'article 37 de l'arrêté du 19 février 2018, ainsi, sous certaines conditions qu'en " cercle 1 ", c'est-à-dire dans les zones où la prédation sur le cheptel domestique a été constatée au cours des deux dernières années, alors que de tels tirs, dans le cadre du régime fixé par l'arrêté du 19 février 2018, ne sont possibles qu'à compter du 1er septembre ;<br/>
              - des tirs de prélèvement simples peuvent être autorisés en " cercle 1 ", lorsque des dommages exceptionnels sont survenus au cours des douze derniers mois et que les dommages se poursuivent malgré des tirs de défense simples ou renforcés, alors que l'arrêté du 19 février 2018 subordonne cette possibilité à la mise en oeuvre préalable de tirs de défense renforcés.<br/>
<br/>
              11. En ce qui concerne le nombre maximum de loups dont la destruction peut être autorisée, l'arrêté contesté prévoit que le plafond est porté, pour l'année 2020, à 17 % de l'effectif moyen avec un dépassement possible, sous certaines conditions, de ce plafond dans la limite de 2 % de cet effectif moyen lorsque le plafond de 17 % est atteint avant la fin de l'année civile.<br/>
<br/>
              12. Il ressort des pièces du dossier, et notamment d'une note technique établie par le Muséum national d'histoire naturelle et l'Office national de la chasse et de la faune sauvage, que la population de loups sur le territoire français a connu une croissance de 13 % en 2018 malgré un plafond de prélèvements fixé à 10 %, ce constat autorisant une marge de manoeuvre supplémentaire sur les tirs tout en maintenant une croissance du nombre de loups. La population de loups était estimée à plus de 500 spécimens au printemps 2019, contre 430 au printemps 2018, soit un chiffre au moins égal au seuil de viabilité démographique ; cinq nouvelles meutes ont en outre été détectées lors du suivi de l'automne 2019. L'arrêté du 19 février 2018 fixant le nombre maximum de spécimens de loups dont la destruction pourra être autorisée chaque année, qui a fait l'objet d'un recours pour excès de pouvoir rejeté par une décision du Conseil d'Etat, statuant au contentieux, du 18 décembre 2019, permettait de détruire en 2020 une soixantaine de loups. La mise en oeuvre de l'arrêté du 30 décembre 2019 conduirait à une augmentation significative de ce plafond, en le portant à une centaine de loups, mais, compte tenu de la saisonnalité des attaques de troupeaux, ce n'est pas avant le mois de juillet 2020 qu'elle risquerait de se traduire par un nombre de destructions supérieur à celui autorisé par l'arrêté du 19 février 2018. Par ailleurs, en ce qui concerne les conditions et limites dans lesquelles des dérogations aux interdictions de destruction peuvent être accordées par les préfets, si l'arrêté contesté apporte des assouplissements par rapport au régime préexistant, une partie d'entre eux ne produira pas ses effets avant le 1er juillet 2020 et les autres ont une portée limitée. En outre, de nouvelles données sur la population de loups, dont il appartiendra aux ministres compétents de tirer les conséquences, seront disponibles avant l'été.<br/>
<br/>
              13. Il résulte de ce qui précède que la condition d'urgence prévue par l'article L. 521-1 du code de justice administrative ne peut être regardée comme remplie. Par suite, et sans qu'il soit besoin d'examiner la condition tenant à l'existence d'un doute sérieux sur la légalité de l'arrêté attaqué, la requête de l'association One Voice doit être rejetée, y compris, par voie de conséquence, ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de l'association One Voice est rejetée.<br/>
<br/>
Article 2 : La présente ordonnance sera notifiée à l'association One Voice, à la ministre de la transition écologique et solidaire et au ministre de l'agriculture et de la pêche.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
