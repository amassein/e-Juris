<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038135392</ID>
<ANCIEN_ID>JG_L_2019_02_000000409399</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/13/53/CETATEXT000038135392.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème chambre, 15/02/2019, 409399, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-02-15</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>409399</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème chambre</FORMATION>
<TYPE_REC>Contentieux des pensions</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BARADUC, DUHAMEL, RAMEIX</AVOCATS>
<RAPPORTEUR>Mme Céline  Guibé</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Marie-Gabrielle Merloz</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2019:409399.20190215</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme B...A...a demandé au tribunal administratif de Paris d'annuler la décision du 3 février 2015 par laquelle le ministre de l'éducation nationale, de l'enseignement supérieur et de la recherche a fixé la période des services auxiliaires à prendre en compte pour la révision de la pension de retraite qui lui a été concédée ainsi que les retenues rétroactives correspondantes et de condamner l'Etat à lui verser diverses sommes. Par un jugement nos 1504791 et 1510724 du 31 janvier 2017, le tribunal administratif de Paris a condamné l'Etat à verser à Mme A... la somme de 1 500 euros, assortie des intérêts au taux légal à compter du 26 mars 2015, et rejeté le surplus des conclusions de la demande de MmeA....<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 30 mars et 15 juin 2017 et 19 février 2018 au secrétariat du contentieux du Conseil d'Etat, Mme A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande ;<br/>
<br/>
              3°) à titre subsidiaire, d'enjoindre, sur le fondement des articles L. 911-1 et L. 911-3 du code de justice administrative, au ministre de l'éducation nationale, de l'enseignement supérieur et de la recherche d'exécuter la décision du 3 février 2015, sous astreinte de 500 euros par jour de retard, et de lui verser la différence entre la pension qu'elle a perçue et celle qu'elle aurait dû percevoir à compter du 24 avril 2009 en application de la décision du 3 février 2015 ;<br/>
<br/>
              4°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le décret n° 50-581 du 25 mai 1950 ;<br/>
              - le code des pensions civiles et militaires de retraite ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Céline Guibé, maître des requêtes,<br/>
<br/>
              - les conclusions de Mme Marie-Gabrielle Merloz, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Baraduc, Duhamel, Rameix, avocat de MmeA....<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que MmeA..., qui a été titularisée dans le corps des inspecteurs des affaires sociales et sanitaires le 1er janvier 1976, a demandé la validation, pour la détermination de ses droits à pension, de divers services accomplis tant antérieurement que postérieurement à sa titularisation. Statuant sur renvoi effectué par la décision n° 354729 du 17 juillet 2013 du Conseil d'Etat, le tribunal administratif de Paris a, par jugement n° 0916796 du 6 février 2014, d'une part, annulé la décision du 30 septembre 2009 et le titre de pension du 20 avril 2009 en tant qu'ils refusaient la validation des services accomplis par Mme A...entre 1970 et 1973 en qualité de maître contractuel et d'autre part, enjoint au ministre de l'éducation nationale et au ministre de l'économie et des finances de procéder à la validation de ces services, de réviser en ce sens le titre de pension de Mme A...et de lui reverser les sommes dues par voie de conséquence, dans un délai de trois mois à compter de la notification du jugement. Le 3 février 2015, le ministre de l'éducation nationale, de l'enseignement supérieur et de la recherche a pris, après contestation par Mme A...de la première décision du 21 octobre 2014, une seconde décision de validation de ses services auxiliaires, fixant à 5 trimestres la durée de ces services pris en compte pour la liquidation de sa pension et procédant au décompte des retenues rétroactives correspondantes sur la base de l'indice correspondant au 9ème échelon de son grade. Mme A... se pourvoit en cassation contre le jugement du 31 janvier 2017 par lequel le tribunal administratif de Paris a condamné l'Etat à lui verser la somme de 1 500 euros avec intérêts au taux légal et capitalisation des intérêts et a rejeté le surplus de ses demandes.<br/>
<br/>
              2. Il découle des moyens présentés à l'appui du pourvoi que Mme A... doit être regardée comme demandant l'annulation du jugement du 31 janvier 2017 en tant seulement qu'il rejette ses conclusions tendant à l'annulation de la décision du 3 février 2015 du ministre de l'éducation nationale, de l'enseignement supérieur et de la recherche.<br/>
<br/>
              3. Aux termes de l'article L. 5 du code des pensions civiles et militaires de retraite, dans sa rédaction alors applicable : " (...) Peuvent également être pris en compte pour la constitution du droit à pension les services d'auxiliaire, de temporaire, d'aide ou de contractuel, (...) accomplis dans les administrations centrales de l'Etat, les services extérieurs en dépendant et les établissements publics de l'Etat ne présentant pas un caractère industriel et commercial (...) ". Aux termes de l'article R. 7 du même code : " (...) La validation est subordonnée au versement rétroactif de la retenue légale calculée sur le traitement ou la solde afférent à l'indice détenu par le fonctionnaire titulaire ou le militaire à la date de la demande. (...) / Est admise à validation toute période de services effectués - de façon continue ou discontinue, sur un emploi à temps complet ou incomplet, occupé à temps plein ou à temps partiel - quelle qu'en soit la durée, en qualité d'agent non titulaire de l'un des employeurs mentionnés aux 1°, 2° et 3° de l'article L. 86-1. La durée des périodes de services validés s'exprime en trimestres. Le nombre de trimestres validés est égal à la durée totale des services effectivement accomplis divisée par le quart de la durée légale annuelle de travail prévue à l'article 1er du décret n° 2000-815 du 25 avril 2000 relatif à l'aménagement et à la réduction du temps de travail dans la fonction publique de l'Etat. / Toutefois, lorsque les services admis à validation relèvent d'un régime d'obligations de service défini par un texte législatif ou réglementaire, la durée légale annuelle du travail mentionnée à l'alinéa précédent prise en compte est la durée annuelle, exprimée en heures, requises pour ces services à temps complet. / Dans le décompte final des trimestres admis à validation, la fraction de trimestre égale ou supérieure à quarante-cinq jours est comptée pour un trimestre, la fraction du trimestre inférieure à quarante-cinq jours est négligée ". Il résulte de ces dispositions que la durée des périodes de services validés, qui s'exprime en trimestres, doit être calculée année après année, en divisant, pour chaque année civile, la durée totale des services effectivement accomplis, qui s'apprécie en jours effectivement travaillés, par le quart de la durée légale annuelle de travail.<br/>
<br/>
              4. Pour retenir un nombre de trimestres validés égal à 5,44 et écarter, par voie de conséquence, la demande d'annulation de la décision du ministre de l'éducation nationale, de l'enseignement supérieur et de la recherche ayant estimé que les services auxiliaires accomplis par la requérante sur les périodes du 14 septembre 1970 au 8 janvier 1971, du 14 septembre 1971 au 12 septembre 1972 et du 13 septembre 1972 au 5 février 1973 en qualité de maître contractuel dans un établissement d'enseignement privé sous contrat ouvraient droit à validation de 5 trimestres, le tribunal administratif de Paris a calculé, pour chacune des périodes susmentionnées, la durée totale des services effectivement accomplis. Il résulte de ce qui a été dit au point 2 qu'en calculant les services accomplis par période d'activité et non par année civile, le tribunal a commis une erreur de droit. Par suite, et sans qu'il soit besoin d'examiner les autres moyens de son pourvoi, Mme A...est fondée à demander l'annulation de ce jugement en tant qu'il rejette ses conclusions tendant à l'annulation de la décision du 3 février 2015 du ministre de l'éducation nationale, de l'enseignement supérieur et de la recherche.<br/>
<br/>
              5. Il y a lieu, dans les circonstances de l'espèce, de régler, dans cette mesure, l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative.<br/>
<br/>
              6. Aux termes de l'article 1er du décret du 25 mai 1950 portant règlement d'administration publique pour la fixation des maximums de service hebdomadaire du personnel enseignant des établissements d'enseignement du second degré : " Les membres du personnel enseignant dans les établissements du second degré sont tenus de fournir, sans rémunération supplémentaire, dans l'ensemble de l'année scolaire, les maxima de service hebdomadaire suivants : / A) Enseignements littéraires, scientifiques, technologiques et artistiques : (...) / Non agrégés : 18 heures (...) ".<br/>
<br/>
              7. D'une part, il résulte de l'instruction que compte tenu d'une durée légale annuelle de travail pour les professeurs non-agrégés de 648 heures pour un service à temps complet, Mme A...a travaillé, premièrement, à mi-temps du 14 septembre au 31 décembre 1970, ce qui représente une durée totale de services effectivement accomplis de (54,5 jours / 365 jours) x 648 heures, soit 96,76 heures, ce qui correspond à 0,60 trimestre de 162 heures, deuxièmement, à mi-temps du 1er au 08 janvier 1971 et à temps complet du 14 septembre au 31 décembre 1971, ce qui représente une durée totale de services effectivement accomplis de (113 jours / 365 jours) x 648 heures, soit 200,61 heures, ce qui correspond à 1,24 trimestre de 162 heures, troisièmement, à temps complet du 1er  janvier au 12 septembre 1972, ce qui représente une durée totale de services effectivement accomplis de (311 jours / 365 jours) x 648 heures, soit 552,13 heures, ce qui correspond à 3,41 trimestres de 162 heures, quatrièmement à mi-temps du 1er janvier au 5 février 1973, ce qui représente une durée totale de services effectivement accomplis de (18 jours / 365 jours) x 648 heures, soit 31,96 heures, ce qui correspond à 0,20 trimestre de 162 heures.<br/>
<br/>
              8. Le décompte final des trimestres admis à validation au titre des services mentionnés au point précédent s'établissant à 5,45 trimestres, la fraction de trimestre au-delà de 5 trimestres ne peut être comptée pour un trimestre en application du dernier alinéa de l'article R. 7 précité du code des pensions civiles et militaires. Dès lors, la durée des périodes de services validés s'établit à 5 trimestres. <br/>
<br/>
              9. D'autre part, il résulte de l'instruction qu'à la date de sa demande de validation des services accomplis entre 1970 et 1973 en qualité de maître contractuel, laquelle a été fixée à la date du 8 septembre 2006 par le jugement n° 0916796 du tribunal administratif de Paris du 6 février 2014, qui est devenu définitif, Mme A...détenait l'indice 733 correspondant au 9ème échelon du grade de professeur agrégé de classe normale en vertu d'un arrêté du ministre de l'éducation nationale, de la recherche et de la technologie du 31 août 1998 qui prononce sa promotion au 9ème échelon à compter du 6 juin 1998. La circonstance qu'elle ait été placée en disponibilité du 1er septembre 1998 au 23 avril 2009 et que sa pension ait été liquidée le 24 avril 2009, en application de l'article L. 15 du code des pensions civiles et militaires de retraite, sur la base de l'indice majoré 684 correspondant au 8ème échelon de son grade sont sans incidence sur l'indice qui devait être retenu pour le calcul du versement rétroactif de la retenue légale en application des dispositions précitées du quatrième alinéa de l'article R. 7 du même code. Par suite, les retenues rétroactives mises à la charge de Mme A...devaient être calculées sur la base de l'indice majoré 733 du 9ème échelon du grade de professeur agrégé de classe normale.<br/>
<br/>
              10. Il résulte de ce qui précède que Mme A...n'est pas fondée à demander l'annulation de la décision du 3 février 2015 par laquelle le ministre de l'éducation nationale, de l'enseignement supérieur et de la recherche a fixé la période des services auxiliaires à prendre en compte pour la révision de la pension de retraite qui lui a été concédée ainsi que les retenues rétroactives correspondantes. Ses conclusions aux fins d'injonction et d'indemnisation doivent, par voie de conséquence, être également rejetées.<br/>
<br/>
              11. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise, à ce titre, à la charge de l'Etat, qui n'est pas la partie perdante.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du tribunal administratif de Paris du 31 janvier 2017 est annulé en tant qu'il rejette les conclusions de la demande de Mme A... tendant à l'annulation de la décision du 3 février 2015 du ministre de l'éducation nationale, de l'enseignement supérieur et de la recherche.<br/>
<br/>
Article 2 : Les conclusions de la demande de Mme A...tendant à l'annulation de la décision du 3 février 2015 du ministre de l'éducation nationale, de l'enseignement supérieur et de la recherche, ainsi que ses conclusions aux fins d'injonction et d'indemnisation sont rejetées. <br/>
<br/>
Article 4 : Les conclusions présentées par Mme A...au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 5 : La présente décision sera notifiée à Mme B... A...et au ministre de l'éducation nationale et de la jeunesse.<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
