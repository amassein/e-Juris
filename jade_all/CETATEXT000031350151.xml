<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031350151</ID>
<ANCIEN_ID>JG_L_2015_10_000000385779</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/35/01/CETATEXT000031350151.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème / 2ème SSR, 21/10/2015, 385779</TITRE>
<DATE_DEC>2015-10-21</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>385779</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème / 2ème SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LYON-CAEN, THIRIEZ ; SCP CAPRON ; SCP ODENT, POULET</AVOCATS>
<RAPPORTEUR>M. François Lelièvre</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Gilles Pellissier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:385779.20151021</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La commune de Tracy-sur-Loire a demandé au tribunal administratif de Dijon de condamner l'Etat, la société Merlot TP et la société Esportec à l'indemniser du préjudice subi du fait de désordres affectant la chaussée d'une place de la commune ayant fait l'objet d'une opération de réaménagement.<br/>
<br/>
              Par un jugement n° 1101537 du 10 janvier 2013, le tribunal administratif de Dijon a, d'une part, condamné l'Etat à indemniser la commune de la moitié du préjudice tenant à l'absence de pénalités de retard infligés à la société Merlot TP, d'autre part, mis les frais d'expertise à la charge de l'Etat et, enfin, rejeté le surplus de sa demande.<br/>
<br/>
              La commune de Tracy-sur-Loire a interjeté appel de ce jugement en tant qu'il n'avait pas fait droit à l'ensemble de ses demandes. Par arrêt n° 13LY00728 du 18 septembre 2014, la cour administrative d'appel de Lyon a, d'une part, annulé le jugement du tribunal administratif en tant qu'il avait statué sur les conclusions de la commune dirigées contre la société Esportec, d'autre part, après avoir évoqué, rejeté ces mêmes conclusions comme portées devant une juridiction incompétente pour en connaître et, enfin, rejeté le surplus de la requête d'appel de la commune.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 18 novembre 2014 et 18 février 2015 au secrétariat du contentieux du Conseil d'Etat, la commune de Tracy-sur-Loire demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt en tant qu'il a rejeté ses conclusions tendant à la condamnation de la société Esportec comme portées devant une juridiction incompétente pour en connaître et en tant qu'il a rejeté le surplus de ses conclusions ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge solidaire de l'Etat, de la société Merlot TP et de la société Esportec la somme de 9 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code civil ;<br/>
              - le code des marchés publics ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. François Lelièvre, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Gilles Pellissier, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Lyon-Caen, Thiriez, avocat de la commune de Tracy-sur-Loire, à la SCP Odent, Poulet, avocat de la société Merlot TP, et à la SCP Capron, avocat de la société Esportec ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que la commune de Tracy-sur-Loire a confié à la société Merlot TP l'aménagement d'une place et choisi l'Etat comme maître d'oeuvre ; que des désordres étant apparus, la commune a recherché devant le tribunal administratif de Dijon l'engagement de la responsabilité de cette entreprise et du maître d'oeuvre ainsi que de la société Esportec, qui avait fourni à la société Merlot TP un stabilisant de sols ; que par jugement du 10 janvier 2013, le tribunal a condamné l'Etat à verser à la commune la somme de 17 250 euros et rejeté le surplus de ses demandes ; par l'arrêt attaqué du 18 septembre 2014, la cour administrative d'appel de Lyon, après avoir partiellement annulé sur appel de la commune le jugement du tribunal administratif de Dijon, a rejeté le surplus de ses conclusions ; <br/>
<br/>
              Sur l'arrêt en tant qu'il se prononce sur les conclusions dirigées contre la société Esportec :<br/>
<br/>
              2. Considérant qu'aux termes du premier alinéa de l'article 1792-4 du code civil : " Le fabricant d'un ouvrage, d'une partie d'ouvrage ou d'un élément d'équipement conçu et produit pour satisfaire, en état de service, à des exigences précises et déterminées à l'avance, est solidairement responsable des obligations mises par les articles 1792, 1792-2 et 1792-3 à la charge du locateur d'ouvrage qui a mis en oeuvre, sans modification et conformément aux règles édictées par le fabricant, l'ouvrage, la partie d'ouvrage ou élément d'équipement considéré (...) " ; <br/>
<br/>
              3. Considérant que, conformément aux principes régissant la responsabilité décennale des constructeurs, la personne publique maître de l'ouvrage peut rechercher devant le juge administratif la responsabilité des constructeurs pendant le délai d'épreuve de dix ans, ainsi que, sur le fondement de l'article 1792-4 du code civil précité, la responsabilité solidaire du fabricant d'un ouvrage, d'une partie d'ouvrage ou d'un élément d'équipement conçu et produit pour satisfaire, en état de service, à des exigences précises et déterminées à l'avance ;<br/>
<br/>
              4. Considérant qu'après avoir relevé, par une motivation suffisante, que la société Esportec était un fournisseur et non un sous-traitant, et estimé que cette société avait livré un stabilisant de sols, simple matériau qui ne pouvait être qualifié d'ouvrage, de partie d'ouvrage, ou d'élément d'équipement au sens des dispositions de l'article 1792-4 du code civil, la cour n'a pas commis d'erreur de droit en en déduisant que la responsabilité de la société Esportec ne pouvait être solidairement recherchée devant le juge administratif sur le fondement de l'article 1792-4 du code civil ; <br/>
<br/>
              5. Considérant qu'il résulte de ce qui précède que les conclusions de la commune dirigées contre l'arrêt en tant qu'il statue sur la responsabilité de la société Esportec doivent être rejetées ; <br/>
<br/>
              Sur l'arrêt en tant qu'il statue sur la responsabilité contractuelle de l'Etat agissant en qualité de maître d'oeuvre :<br/>
<br/>
              6. Considérant, d'une part, qu'il ressort des pièces du dossier soumis aux juges du fond que la commune de Tracy-sur-Loire, qui compte moins de 1 000 habitants, avait confié à l'Etat la maîtrise d'oeuvre des travaux d'aménagement, faute de disposer des compétences pour assurer la mise en oeuvre et le suivi de l'opération, tant sur le plan juridique que technique ; qu'en jugeant, au vu de ces circonstances, que la commune avait commis une imprudence fautive en ne relevant pas que le décompte général du marché, présenté par le maître d'oeuvre, ne comportait pas de pénalités de retard et en estimant nécessairement que la commune ne pouvait ignorer qu'elle ne pourrait plus ensuite réclamer de telles pénalités en raison de leur absence du décompte, la cour a inexactement qualifié les faits de l'espèce ;<br/>
<br/>
              7. Considérant, d'autre part, que la responsabilité des maîtres d'oeuvre pour manquement à leur devoir de conseil peut être engagée, dès lors qu'ils se sont abstenus d'appeler l'attention du maître d'ouvrage sur des désordres affectant l'ouvrage et dont ils pouvaient avoir connaissance, en sorte que la personne publique soit mise à même de ne pas réceptionner l'ouvrage ou d'assortir la réception de réserves ; qu'en se fondant sur la circonstance que les vices en cause n'avaient pas présenté un caractère apparent lors de la réception des travaux pour écarter la responsabilité de l'Etat en sa qualité de maître d'oeuvre, alors qu'elle aurait dû vérifier si le maître d'oeuvre aurait pu avoir connaissance de ces vices s'il avait accompli sa mission selon les règles de l'art, la cour administrative d'appel de Lyon a également entaché son arrêt d'erreur de droit ; <br/>
<br/>
              8. Considérant qu'il résulte de ce qui précède que l'arrêt de la cour doit être annulé en tant qu'il statue sur la responsabilité contractuelle de l'Etat ;<br/>
<br/>
              Sur l'arrêt en tant qu'il statue sur la responsabilité de la société Merlot TP au titre de la garantie décennale :<br/>
<br/>
              9. Considérant que pour rejeter les conclusions de la commune tendant à la mise en cause de la responsabilité décennale de la société Merlot TP, la cour s'est fondée sur la circonstance qu'il ne résultait pas de l'instruction que les vices constatés compromettaient la solidité de l'ouvrage " dans sa généralité " ; qu'en subordonnant ainsi l'engagement de la responsabilité décennale des constructeurs au caractère général des désordres constatés, la cour a commis une erreur de droit ; que, par suite, l'arrêt doit être annulé en tant qu'il statue sur la responsabilité de cette société ;<br/>
<br/>
              10. Considérant qu'il résulte de tout de ce qui précède que la commune de Tracy-sur-Loire est fondée, sans qu'il soit besoin d'examiner les autres moyens de son pourvoi, à demander l'annulation de l'arrêt attaqué en tant qu'il statue sur les responsabilités de la société Merlot TP et de l'Etat ;<br/>
<br/>
              11. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle, d'une part, à ce que soit mis à la charge de la société Esportec, qui n'est pas une partie perdante, le versement des sommes que demande la commune de Tracy-sur-Loire, d'autre part, à ce que soit mis à la charge de cette commune le versement des sommes demandées par la société Merlot TP, la commune n'étant pas perdante dans le litige qui l'oppose à cette société ; qu'en revanche, il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la commune le versement d'une somme de 3 000 euros à la société Esportec et de mettre à la charge de la société Merlot TP et de l'Etat le versement à la commune d'une somme de 3 000 euros chacun, au titre des mêmes dispositions ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Lyon est annulé en tant qu'il a rejeté les conclusions de la commune de Tracy-sur-Loire tendant à la condamnation de la société Merlot TP et de l'Etat.<br/>
Article 2 : L'affaire est renvoyée, dans la mesure de la cassation prononcée, à la cour administrative d'appel de Lyon.<br/>
Article 3 : La commune de Tracy-sur-Loire versera à la société Esportec la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. L'Etat et la société Merlot TP verseront, chacun, la somme de 3 000 euros à la commune de Tracy-sur-Loire au même titre.<br/>
Article 4 : Le surplus des conclusions de la commune de Tracy-sur-Loire et les conclusions présentées par la société Merlot TP au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée à la commune de Tracy-sur-Loire, à la société Merlot TP, à la société Esportec et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">39-06-01-04-005 MARCHÉS ET CONTRATS ADMINISTRATIFS. RAPPORTS ENTRE L'ARCHITECTE, L'ENTREPRENEUR ET LE MAÎTRE DE L'OUVRAGE. RESPONSABILITÉ DES CONSTRUCTEURS À L'ÉGARD DU MAÎTRE DE L'OUVRAGE. RESPONSABILITÉ DÉCENNALE. CHAMP D'APPLICATION. - 1) APPLICATION DE LA RESPONSABILITÉ SOLIDAIRE DU FABRICANT (ART. 1792-4 C. CIV.) - 2) CAS D'ESPÈCE.
</SCT>
<ANA ID="9A"> 39-06-01-04-005 1) Conformément aux principes régissant la responsabilité décennale des constructeurs, la personne publique maître de l'ouvrage peut rechercher devant le juge administratif la responsabilité des constructeurs pendant le délai d'épreuve de dix ans, ainsi que, sur le fondement de l'article 1792-4 du code civil, la responsabilité solidaire du fabricant d'un ouvrage, d'une partie d'ouvrage ou d'un élément d'équipement conçu et produit pour satisfaire, en état de service, à des exigences précises et déterminées à l'avance.,,,2) Une société qui était un fournisseur et non un sous-traitant et qui avait livré un stabilisant de sols, simple matériau qui ne pouvait être qualifié d'ouvrage, de partie d'ouvrage ou d'élément d'équipement au sens de l'article 1792-4 du code civil, ne peut voir sa responsabilité solidairement recherchée devant le juge administratif sur le fondement de cet article.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
