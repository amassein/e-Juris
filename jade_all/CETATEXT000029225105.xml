<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029225105</ID>
<ANCIEN_ID>JG_L_2014_07_000000361714</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/22/51/CETATEXT000029225105.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère et 6ème sous-sections réunies, 11/07/2014, 361714, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-07-11</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>361714</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère et 6ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BARTHELEMY, MATUCHANSKY, VEXLIARD, POUPOT</AVOCATS>
<RAPPORTEUR>M. Didier-Roland Tabuteau</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Maud Vialettes</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:361714.20140711</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi, enregistré le 7 août 2012 au secrétariat du contentieux du Conseil d'Etat, présenté par le ministre des affaires sociales et de la santé ; le ministre demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 11VE03754 du 31 mai 2012 par lequel la cour administrative d'appel de Versailles a rejeté son recours tendant à l'annulation du jugement n° 0912750 du 20 septembre 2011 par lequel le tribunal administratif de Montreuil a annulé la décision de la commission exécutive de l'agence régionale de l'hospitalisation d'Ile-de-France du 17 juillet 2009 en tant qu'elle rejette la demande d'autorisation de la société Clinique du Vert Galant en vue de poursuivre l'activité de traitement des cancers par chirurgie des pathologies gynécologiques ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de la santé publique ;<br/>
<br/>
              Vu le décret n° 2004-1289 du 26 novembre 2004 ;<br/>
<br/>
              Vu le décret n° 2007-388 du 21 mars 2007 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Didier-Roland Tabuteau, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Maud Vialettes, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Barthélemy, Matuchansky, Vexliard, Poupot, avocat de la société Clinique du Vert Galant ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que la société Clinique du Vert Galant a présenté une demande d'autorisation d'exercer certaines activités de traitement du cancer ; que, par une décision du 17 juillet 2009, la commission exécutive de l'agence régionale de l'hospitalisation d'Ile-de-France a rejeté cette demande en tant qu'elle concernait la chirurgie des cancers gynécologiques ; que, par un jugement du 20 septembre 2011, le tribunal administratif de Montreuil a annulé cette décision de rejet ; que le ministre des affaires sociales et de la santé se pourvoit en cassation contre l'arrêt du 31 mai 2012 par lequel la cour administrative d'appel de Versailles a rejeté son recours contre ce jugement ;<br/>
<br/>
              2. Considérant que l'article L. 6122-1 du code de la santé publique, dans sa rédaction applicable à la décision litigieuse, soumet à l'autorisation de l'agence régionale de l'hospitalisation, à laquelle a succédé l'agence régionale de santé, " la création, la conversion et le regroupement des activités de soins " dont la liste est fixée par décret en Conseil d'État ; que l'article R. 6122-25 du même code, qui reprend des dispositions issues du décret du 26 novembre 2004 relatif à la liste des activités de soins et des équipements matériels lourds soumis à autorisation en application de l'article L. 6122-1 du code de la santé publique et modifiant ce code, fait figurer le traitement du cancer au nombre des activités de soins soumises à autorisation ;<br/>
<br/>
              3. Considérant, d'une part, que l'article R. 6123-89 du même code, issu du décret du 21 mars 2007 relatif aux conditions d'implantation applicables à l'activité de soins de traitement du cancer et modifiant le code de la santé publique, subordonne la délivrance de l'autorisation mentionnée précédemment au respect de seuils d'activité minimale annuelle arrêtés par le ministre chargé de la santé en tenant compte des connaissances disponibles en matière de sécurité et de qualité des pratiques médicales ; qu'il prévoit toutefois, à son deuxième alinéa, qu'" à titre dérogatoire, la première autorisation peut être accordée à un demandeur dont l'activité prévisionnelle annuelle est, au commencement de la mise en oeuvre de cette autorisation, au moins égale à 80 % du seuil d'activité minimale prévu à l'alinéa précédent sous la condition que l'activité réalisée atteigne le niveau de ce seuil au plus tard 18 mois après la visite de conformité (...) " ; que ces dispositions définissent le régime permanent régissant l'octroi des autorisations ;<br/>
<br/>
              4. Considérant, d'autre part, qu'en application de l'article 2 du décret du 21 mars 2007 mentionné ci-dessus, les schémas régionaux d'organisation sanitaire en vigueur à la date de publication de ce texte doivent être révisés dans un délai de dix-huit mois à compter de cette date ; que le même décret comporte à son article 3 des dispositions transitoires, en vertu desquelles les établissements de santé qui, à la date de sa publication, exercent l'activité de traitement du cancer doivent demander l'autorisation correspondante dans les deux mois suivant la publication des dispositions du schéma d'organisation sanitaire ; que cette autorisation leur est accordée s'ils attestent notamment, au moment de la décision de l'agence régionale de l'hospitalisation, " d'une activité minimale annuelle réalisée au moins égale à 80 % de l'activité minimale annuelle qui leur est applicable, établie conformément aux dispositions de l'article R. 6123-89 du même code " ; que cet article précise, en outre, que les demandeurs peuvent " poursuivre leurs activités jusqu'à ce qu'il soit statué sur leur demande " ;<br/>
<br/>
              5. Considérant qu'il résulte des dispositions qui précèdent que la délivrance des autorisations relatives à l'activité de traitement du cancer aux établissements qui exerçaient une telle activité préalablement à l'intervention du décret du 21 mars 2007 est régie par les seules dispositions de l'article 3 de ce texte ; que ces dispositions s'appliquent, que les établissements concernés aient ou non été détenteurs d'une autorisation pour les activités d'" utilisation thérapeutique de radioéléments en sources non scellées " et de " traitement des affections cancéreuses par rayonnements ionisants de haute énergie ", précédemment soumises à autorisation en vertu de l'article R. 712-2 du code de la santé publique, applicable jusqu'à l'entrée en vigueur du décret du 26 novembre 2004 mentionné ci-dessus ; que, par suite, la cour administrative d'appel de Versailles a commis une erreur de droit en déduisant de la circonstance que la société Clinique du Vert Galant ne disposait pas jusque-là d'une autorisation d'activité de traitement du cancer que sa demande devait s'analyser comme une première demande et être  examinée dans le cadre du régime dérogatoire prévu au deuxième alinéa de l'article R. 6123-89 du code de la santé publique ; <br/>
<br/>
              6. Considérant qu'il suit de là que l'arrêt de la cour administrative d'appel de Versailles du 31 mai 2012 doit être annulé ; que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat, qui n'est pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
                              ---------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Versailles du 31 mai 2012 est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Versailles.<br/>
Article 3 : Les conclusions de la société Clinique du Vert Galant présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
Article 4 : La présente décision sera notifiée à la ministre des affaires sociales et de la santé et à la société Clinique du Vert Galant<br/>
<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
