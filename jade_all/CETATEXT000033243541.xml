<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033243541</ID>
<ANCIEN_ID>JG_L_2016_10_000000401696</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/24/35/CETATEXT000033243541.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème - 3ème chambres réunies, 13/10/2016, 401696, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-10-13</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>401696</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème - 3ème chambres réunies</FORMATION>
<TYPE_REC>Autres</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Vincent Uher</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Romain Victor</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2016:401696.20161013</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société par actions simplifiée Alinéa a produit, à l'appui de sa demande tendant à fixer le montant de sa créance sur le Trésor résultant de son option pour le report en arrière sur le bénéfice de l'année 2007 de son déficit constaté en matière d'impôt sur les sociétés au titre de l'exercice clos en 2010 à la somme de 1 850 524 euros, trois mémoires, enregistrés les 9 juin, 6 juillet et 13 juillet 2016 au greffe de la cour administrative d'appel de Versailles, en application de l'article 23-1 de l'ordonnance n° 58-1067 du 7 novembre 1958, par lesquels elle soulève une question prioritaire de constitutionnalité.<br/>
<br/>
              Par une ordonnance n° 14VE00855 du 19 juillet 2016, enregistrée le 21 juillet 2016 au secrétariat du contentieux du Conseil d'Etat, le président de la 6ème chambre de cette cour, avant qu'il soit statué sur la demande de la société Alinéa, a décidé, par application des dispositions de l'article 23-2 de l'ordonnance du 7 novembre 1958, de transmettre au Conseil d'Etat la question de la conformité aux droits et libertés garantis par la Constitution des dispositions du II de l'article 31 de la loi de finances rectificative pour 2011 du 28 décembre 2011.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la Constitution, notamment son Préambule et son article 61-1 ;<br/>
              - l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - la loi n° 2011-1117 du 19 septembre 2011 de finances rectificative pour 2011, notamment son article 2 ;<br/>
              - la loi n° 2011-1978 du 28 décembre 2011 de finances rectificative pour 2011, notamment son article 31 ;<br/>
              - le code de justice administrative ; 	<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Vincent Uher, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Romain Victor, rapporteur public.<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il résulte des dispositions de l'article 23-4 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel que, lorsqu'une juridiction relevant du Conseil d'Etat a transmis à ce dernier, en application de l'article 23-2 de cette même ordonnance, la question de la conformité à la Constitution d'une disposition législative, le Conseil constitutionnel est saisi de cette question de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement de circonstances, et que la question soit nouvelle ou présente un caractère sérieux.<br/>
<br/>
              2. En vertu de l'article 220 quinquies du code général des impôts dans sa rédaction résultant de l'article 2 de la loi du 19 septembre 2011 de finances rectificative pour 2011, l'option pour le report en arrière d'un déficit sur le bénéfice de l'exercice précédent n'est admise qu'à la condition qu'elle porte sur le déficit constaté au titre de l'exercice, dans la limite du montant le plus faible entre le bénéfice déclaré au titre de l'exercice précédent et un montant de 1 000 000 euros ; cette option est exercée au titre de l'exercice au cours duquel le déficit est constaté et dans les mêmes délais que ceux qui sont prévus pour le dépôt de la déclaration de résultats de cet exercice. <br/>
<br/>
              3. Les dispositions du II de l'article 31 de la loi du 28 décembre 2011 de finances rectificative pour 2011, qui ont un caractère interprétatif en vertu de son III, prévoient que les dispositions de l'article 220 quinquies issues de l'article 2 de la loi du 19 septembre 2011 s'appliquent aux déficits constatés au titre des exercices clos à compter du 21 septembre 2011, ainsi qu'aux déficits restant à reporter à la clôture de l'exercice précédant le premier exercice clos à compter de cette même date. Elles sont applicables au litige dont est saisie la cour administrative d'appel de Versailles et n'ont pas déjà été déclarées conformes à la Constitution par le Conseil constitutionnel. Le moyen tiré de ce qu'elles portent atteinte au principe de garantie des droits énoncé à l'article 16 de la Déclaration des droits de l'homme et du citoyen soulève une question présentant un caractère sérieux. Ainsi, il y a lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité invoquée.<br/>
<br/>
<br/>
<br/>                  D E C I D E :<br/>
                                   --------------<br/>
<br/>
Article 1er : La question de la conformité à la Constitution des dispositions du II de l'article 31 de la loi du 28 décembre 2011 de finances rectificative pour 2011 est renvoyée au Conseil constitutionnel.<br/>
<br/>
Article 2 : La présente décision sera notifiée à la société par actions simplifiée Alinéa et au ministre de l'économie et des finances.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
