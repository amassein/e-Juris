<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042375655</ID>
<ANCIEN_ID>JG_L_2020_09_000000432434</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/37/56/CETATEXT000042375655.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème chambre, 28/09/2020, 432434, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-09-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>432434</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GHESTIN ; SCP BAUER-VIOLAS, FESCHOTTE-DESBOIS, SEBAGH</AVOCATS>
<RAPPORTEUR>M. François Charmont</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Cécile Barrois de Sarigny</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2020:432434.20200928</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu les procédures suivantes :<br/>
<br/>
              M. B... A... a demandé, à la commission du contentieux du stationnement payant d'annuler les titres exécutoires n° 07506287818020380 et n° 075062878180651565 émis par l'Agence nationale de traitement automatisé des infractions (ANTAI) en vue du recouvrement des forfaits de post-stationnement mis à sa charge les 16 janvier 2018 et 22 février 2018 par la ville de Paris et de la majoration dont ils sont assortis. Par deux ordonnances n° 19000706 et n° 19000688 du 19 avril 2019, la présidente de la 2ème chambre de la commission du contentieux du stationnement payant a rejeté ses requêtes.<br/>
<br/>
              1° Sous le n° 432434, par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 9 juillet et 9 octobre 2019 au secrétariat du contentieux du Conseil d'Etat, M. A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'ordonnance n° 19000688 du 19 avril 2019 ;<br/>
<br/>
              2°) de mettre à la charge de la ville de Paris la somme de 1 500 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
              2° Sous le n° 432435, par un pourvoi sommaire et un mémoire complémentaire enregistrés les 9 juillet et 9 octobre 2019 au secrétariat du contentieux du Conseil d'Etat, M. A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'ordonnance n° 19000706 du 19 avril 2019 ;<br/>
<br/>
              2°) de mettre à la charge de la ville de Paris la somme de 1 500 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
<br/>
	Vu les autres pièces des dossiers ;<br/>
              Vu : <br/>
              - le code général des collectivités territoriales ;<br/>
              - le code de la route ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. François Charmont, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Cécile Barrois de Sarigny, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Ghestin, avocat de M. A....<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces des dossiers soumis aux juges du fond que deux titres exécutoires émis par l'Agence nationale de traitement automatisé des infractions (ANTAI) en vue du recouvrement de deux forfaits de post-stationnement d'un montant de 35 euros chacun, assortis chacun d'une majoration de 50 euros, ont été adressés à M. A... à raison de deux stationnements constatés les 16 janvier et 22 février 2018 à Paris. M. A... se pourvoit en cassation contre les ordonnances du 19 avril 2019 par lesquelles la présidente de la 2ème chambre de la commission du contentieux du stationnement payant a rejeté ses requêtes contre ces titres exécutoires. Les pourvois de M. A... présentant à juger les mêmes questions, il y a lieu de les joindre pour statuer par une seule décision.<br/>
<br/>
              2. Il résulte des dispositions de l'article L. 2333-87 du code général des collectivités territoriales qu'il appartient en principe au redevable d'un forfait de post-stationnement qui entend contester le bien-fondé de la somme mise à sa charge de saisir l'autorité administrative d'un recours administratif préalable dirigé contre l'avis de paiement et, en cas de rejet de ce recours, d'introduire une requête contre cette décision de rejet devant la commission du contentieux du stationnement payant. En cas d'absence de paiement de sa part dans les trois mois et d'émission, en conséquence, d'un titre exécutoire portant sur le montant du forfait de post-stationnement augmenté de la majoration due à l'Etat, il est loisible au même redevable de contester ce titre exécutoire devant la commission du contentieux du stationnement payant, qu'il ait ou non engagé un recours administratif contre l'avis de paiement. Dans le cadre du litige ainsi introduit, aucune disposition ne fait, par principe, obstacle à ce qu'il conteste, s'il s'y croit fondé, l'obligation de payer la somme réclamée par l'administration.<br/>
<br/>
              3. Il ressort des termes mêmes des ordonnances attaquées que, pour rejeter les requêtes de M. A..., elles jugent que ce dernier ne pouvait utilement contester l'obligation de payer la somme réclamée par l'administration, au motif qu'une telle contestation met forcément en cause la légalité de l'avis de paiement auquel le titre exécutoire s'est substitué. Il résulte de ce qui a été dit ci-dessus que les ordonnances attaquées sont entachées, sur ce point, d'erreur de droit. M. A... est, par suite, sans qu'il soit besoin de se prononcer sur les autres moyens de ses pourvois, fondé à en demander l'annulation.<br/>
<br/>
              4. M. A... a obtenu le bénéfice de l'aide juridictionnelle. Par suite, son avocat peut se prévaloir des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la ville de Paris une somme de 2 000 euros à verser à la SCP Ghestin, avocat de M. A..., sous réserve que cette société renonce à percevoir la somme correspondant à la part contributive de l'Etat. En revanche, les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit versée à ce titre par l'ANTAI qui, ayant été appelée en cause pour produire des observations, n'est pas partie à la présente instance.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Les ordonnances de la présidente de la 2ème chambre de la commission du contentieux du stationnement payant du 19 avril 2019 sont annulées.<br/>
Article 2 : Les affaires sont renvoyées à la commission du contentieux du stationnement payant.<br/>
Article 3 : La ville de Paris versera à la SCP Ghestin la somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative et du deuxième alinéa de l'article 37 de la loi du 10 juillet 1991, sous réserve que cette société renonce à percevoir la somme correspondant à la part contributive de l'Etat.<br/>
Article 4 : La présente décision sera notifiée à M. B... A..., à la ville de Paris et au ministre de l'économie, des finances et de la relance.<br/>
Copie en sera adressée à l'Agence nationale de traitement automatisé des infractions. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
