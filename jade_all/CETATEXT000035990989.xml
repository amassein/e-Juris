<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000035990989</ID>
<ANCIEN_ID>JG_L_2017_11_000000400294</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/35/99/09/CETATEXT000035990989.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème - 8ème chambres réunies, 08/11/2017, 400294, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-11-08</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>400294</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème - 8ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GARREAU, BAUER-VIOLAS, FESCHOTTE-DESBOIS ; SCP MEIER-BOURDEAU, LECUYER</AVOCATS>
<RAPPORTEUR>M. Géraud Sajust de Bergues</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Vincent Daumas</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:400294.20171108</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société Alsace Lait a demandé au tribunal administratif de Strasbourg, d'une part, d'annuler l'état exécutoire du 8 mars 2012 par lequel l'Etablissement national des produits de l'agriculture et de la mer (FranceAgriMer) a mis à sa charge la somme de 120 951,15 euros et, d'autre part, de la décharger de cette somme dont elle a été déclarée redevable. Par un jugement n° 1202024 du 21 mai 2015, le tribunal administratif de Strasbourg a annulé l'état exécutoire du 8 mars 2012 et déchargé la société Alsace Lait de la somme de 120 951,15 euros.<br/>
<br/>
              Par un arrêt n° 15NC01623 du 31 mars 2016, la cour administrative d'appel de Nancy a, sur l'appel formé par FranceAgriMer, annulé ce jugement et rejeté la demande de la société Alsace Lait.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 31 mai et 11 août 2016 au secrétariat du contentieux du Conseil d'Etat, la société Alsace Lait demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt de la cour administrative d'appel de Nancy du 31 mars 2016 ; <br/>
<br/>
              2°) de rejeter la requête de FranceAgriMer ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le règlement (CEE) n° 3950/92 du Conseil, du 28 décembre 1992, établissant un prélèvement supplémentaire dans le secteur du lait et des produits laitiers ;<br/>
              - le code rural et de la pêche maritime ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Géraud Sajust de Bergues, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Vincent Daumas, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Garreau, Bauer-Violas, Feschotte-Desbois, avocat de la société Alsace Lait et à la SCP Meier-Bourdeau, Lecuyer, avocat de l'Etablissement national des produits de l'agriculture et de la mer (FranceAgriMer).<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis au juge du fond que, par un titre exécutoire en date du 8 mars 2012, FranceAgriMer, venant aux droits de l'Office national interprofessionnel de l'élevage et de ses productions et de l'Onilait, a mis à la charge de la société Alsace Lait, agréée comme acheteur de lait, la somme de 120 951,15 euros au titre du prélèvement supplémentaire dont l'établissement estimait qu'il lui était dû pour dépassement des quantités de référence en ce qui concerne la campagne de lait 2003-2004. Par un jugement du 21 mai 2015, le tribunal administratif de Strasbourg a annulé ce titre exécutoire et déchargé la société Alsace Lait de la somme de 120 951,15 euros. La société Alsace Lait se pourvoit en cassation contre l'arrêt du 31 mars 2016 par lequel la cour administrative d'appel de Nancy a annulé ce jugement et rejeté la requête de cette société.<br/>
<br/>
              2. Aux termes du sixième considérant du règlement (CEE) n° 3950/92 du Conseil, du 28 décembre 1992, établissant un prélèvement supplémentaire dans le secteur du lait et des produits laitiers, qui était applicable à la campagne 2003-2004 : " (...) le dépassement de l'une ou l'autre des quantités globales garanties pour l'Etat membre entraîne le paiement du prélèvement par les producteurs qui ont contribué au dépassement ; (...) ". Aux termes de l'article 2, paragraphe 1, du même règlement : " 1. Le prélèvement est dû sur toutes les quantités de lait ou d'équivalent-lait commercialisées pendant la période de douze mois en question et qui dépassent l'une ou l'autre des quantités visées à l'article 3. Il est réparti entre les producteurs qui ont contribué au dépassement. / (...) ". Aux termes de l'article 3 du même règlement : " 1. La somme des quantités de référence individuelles de même nature ne peut dépasser les quantités globales correspondantes [' Livraisons' et ' Ventes directes'] pour chaque Etat membre. / 2. Les quantités globales suivantes sont fixées sans préjudice d'une révision éventuelle à la lumière de la situation générale du marché et des conditions particulières existant dans certains Etats membres. / (...) ". <br/>
<br/>
              3. Il résulte des dispositions précitées que le prélèvement supplémentaire n'est dû qu'en cas de dépassement, par un Etat membre, des quantités globales " Livraisons " ou " Ventes directes " qui lui ont été attribuées, sans qu'importe, contrairement à ce que soutient FranceAgriMer, la faculté offerte par l'article 2, paragraphe 1, second alinéa, et paragraphe 4 du règlement 3950/92 à l'Etat membre se trouvant dans cette situation de réallouer les quantités de référence inutilisées en fin de période, au niveau national ou entre acheteurs, et d'affecter le montant perçu qui dépasse le prélèvement dû au financement de programmes nationaux de restructuration ou de la restituer aux producteurs de certaines catégories ou qui se trouvent dans une situation exceptionnelle. Par suite, en jugeant que ce prélèvement était dû indépendamment du dépassement des quantités de référence globales attribuées à un Etat membre, la cour a entaché son arrêt d'une erreur de droit. La société Alsace Lait est donc fondée à demander, pour ce motif, l'annulation de l'arrêt attaqué, sans qu'il soit besoin d'examiner les autres moyens du pourvoi.<br/>
<br/>
              4. Dans les circonstances de l'espèce, il y a lieu de régler l'affaire au fond en application de l'article L. 821-2 du code de justice administrative.<br/>
<br/>
              5. Si FranceAgriMer soutient que le jugement du tribunal administratif de Strasbourg du 21 mai 2015 est irrégulier dès lors qu'il ne vise qu'imparfaitement les moyens des parties, il ressort de ce jugement que ce tribunal a régulièrement visé et analysé les mémoires et moyens des parties. Par suite, le moyen tiré de la méconnaissance des dispositions de l'article R. 741-2 du code de justice administrative doit être écarté.<br/>
<br/>
              6. Si FranceAgriMer soutient également que la société Alsace Lait n'a pas formulé de moyen de légalité interne dans le délai de recours contentieux, il ressort des pièces du dossier que cette société, dans sa requête introductive d'instance devant le tribunal administratif de Strasbourg, a bien soulevé un moyen contestant le bien-fondé de l'état exécutoire. Par suite, c'est à bon droit que le tribunal administratif de Strasbourg a jugé que la fin de non recevoir opposée par FranceAgrimer devait être écartée.<br/>
<br/>
              7. Il résulte enfin de ce qui a été dit au point 3 ci-dessus que c'est à bon droit que le tribunal administratif de Strasbourg a jugé que le prélèvement supplémentaire n'était exigible qu'en cas de dépassement par la France des quantités globales qui lui avaient été allouées au titre de la campagne 2003-2004 et que, dès lors que la France n'avait pas dépassé ces quantités globales au cours de cette campagne, le titre de perception émis à l'encontre de la société Alsace Lait était dépourvu de base légale. <br/>
<br/>
              8. Il résulte de tout ce qui précède que la requête de FranceAgriMer tendant à l'annulation du jugement du tribunal administratif de Strasbourg du 21 mai 2015 et au rejet de la demande de la société Alsace Lait, ainsi que ses conclusions tendant à l'application des dispositions de l'article L. 761-1 du code de justice administrative doivent être rejetées. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de FranceAgriMer la somme de 4 500 euros à verser à la société Alsace Lait en application des mêmes dispositions au titre de la procédure suivie devant le Conseil d'Etat et la cour administrative d'appel de Nancy.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Nancy du 31 mars 2016 est annulé.<br/>
Article 2 : La requête présentée par l'Etablissement national des produits de l'agriculture et de la mer (FranceAgriMer) devant la cour administrative d'appel de Nancy et ses conclusions tendant à l'application des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : L'Etablissement national des produits de l'agriculture et de la mer (FranceAgriMer) versera la somme de 4 500 euros à la société Alsace Lait au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à la société Alsace Lait et à l'Etablissement national des produits de l'agriculture et de la mer (FranceAgriMer).<br/>
Copie en sera adressée au ministre de l'agriculture et de l'alimentation. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
