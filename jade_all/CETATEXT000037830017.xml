<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037830017</ID>
<ANCIEN_ID>JG_L_2018_12_000000406086</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/83/00/CETATEXT000037830017.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème chambre, 14/12/2018, 406086, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-12-14</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>406086</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>CABINET BRIARD</AVOCATS>
<RAPPORTEUR>Mme Cécile Viton</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emilie Bokdam-Tognetti</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:406086.20181214</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société Commerzbank AG a demandé au tribunal administratif de Montreuil de prononcer la restitution de la contribution exceptionnelle sur l'impôt sur les sociétés mise à sa charge au titre de ses exercices clos en 2011, 2012 et 2013. Par un jugement nos 1311730, 1408662 du 1er juin 2015, le tribunal a fait droit à sa demande.<br/>
<br/>
              Par un arrêt n° 15VE02400 du 3 novembre 2016, la cour administrative d'appel de Versailles a rejeté l'appel formé par le ministre des finances et des comptes publics contre ce jugement.<br/>
<br/>
              Par un pourvoi enregistré le 19 décembre 2016 au secrétariat du contentieux du Conseil d'Etat, le ministre de l'économie et des finances demande au Conseil d'Etat d'annuler cet arrêt. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la Déclaration des droits de l'homme et du citoyen du 26 août 1789, notamment ses articles 6 et 13 ;<br/>
              - la Constitution, notamment son Préambule et son article 61-1 ;<br/>
              - l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
              - la convention conclue le 21 juillet 1959 entre la République française et la République fédérale d'Allemagne en vue d'éviter les doubles impositions et d'établir des règles d'assistance administrative et juridique réciproque en matière d'impôts sur le revenu et sur la fortune ;<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - les décisions n° 395015 et n° 396160 du 9 décembre 2016 du Conseil d'Etat statuant au contentieux ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Cécile Viton, maître des requêtes en service extraordinaire,<br/>
<br/>
              - les conclusions de Mme Emilie Bokdam-Tognetti, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, au Cabinet Briard, avocat de la société Commerzbank AG.<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que la société de droit allemand Commerzbank AG a été assujettie, au titre des exercices clos en 2011, 2012 et 2013, à la contribution exceptionnelle sur l'impôt sur les sociétés en application de l'article 235 ter ZAA du code général des impôts. Le ministre de l'économie et des finances se pourvoit en cassation contre l'arrêt du 3 novembre 2016 par lequel la cour administrative d'appel de Versailles a rejeté l'appel qu'il avait formé contre le jugement du 1er juin 2015 du tribunal administratif de Montreuil faisant droit aux demandes de décharge présentées par la société.<br/>
<br/>
              Sur la question prioritaire de constitutionnalité invoquée par la société Commerzbank AG :<br/>
<br/>
              2. Aux termes du premier alinéa de l'article 23-5 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel : " Le moyen tiré de ce qu'une disposition législative porte atteinte aux droits et libertés garantis par la Constitution peut être soulevé, y compris pour la première fois en cassation, à l'occasion d'une instance devant le Conseil d'Etat (...) ". Il résulte des dispositions de ce même article que le Conseil constitutionnel est saisi de la question prioritaire de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et qu'elle soit nouvelle ou présente un caractère sérieux. <br/>
<br/>
              3. Aux termes du I de l'article 235 ter ZAA du code général des impôts dans sa rédaction applicable aux exercices en litige : "Les redevables de l'impôt sur les sociétés réalisant un chiffre d'affaires supérieur à 250 millions d'euros sont assujettis à une contribution exceptionnelle égale à une fraction de cet impôt calculé sur leurs résultats imposables, aux taux mentionnés à l'article 219, des exercices clos à compter du 31 décembre 2011 et jusqu'au 30 décembre 2013.  / (...) / Le chiffre d'affaires mentionné au premier alinéa du présent I s'entend du chiffre d'affaires réalisé par le redevable au cours de l'exercice ou de la période d'imposition, ramené à douze mois le cas échéant, et pour la société mère d'un groupe mentionné à l'article 223 A, de la somme des chiffres d'affaires de chacune des sociétés membres de ce groupe." .<br/>
<br/>
              4. Il ressort des dispositions contestées, éclairées par les travaux préparatoires de la loi du 28 décembre 2011 de finances rectificative pour 2011 de laquelle  elles sont issues, que le législateur a entendu soumettre les grandes entreprises à une contribution supplémentaire, compte tenu de leurs capacités contributives plus fortes. A cette fin, il a prévu un seuil de chiffre d'affaires de 250 millions d'euros, au-delà duquel cette contribution est due. Ce seuil, ainsi que l'ont jugé les décisions du 9 décembre 2016 du Conseil d'Etat statuant au contentieux, visées ci-dessus, s'apprécie par référence aux recettes tirées de l'ensemble des opérations réalisées par le redevable dans le cadre de son activité professionnelle, exercée en France et hors de France, quel que soit le régime fiscal du résultat des opérations correspondant à ce chiffre d'affaires. La société Commerzbank AG soutient qu'en déterminant le champ d'application de la contribution supplémentaire à partir du chiffre d'affaires réalisé par les redevables en France et hors de France, ces dispositions, telles qu'interprétées par le Conseil d'État, méconnaissent les principes d'égalité devant la loi et devant les charges publiques garantis par les articles 6 et 13 de la Déclaration des droits de l'homme et du citoyen.<br/>
<br/>
              5. Le principe d'égalité ne s'oppose ni à ce que le législateur règle de façon différente des situations différentes, ni à ce qu'il déroge à l'égalité pour des raisons d'intérêt général, pourvu que, dans l'un et l'autre cas, la différence de traitement qui en résulte soit en rapport direct avec l'objet de la loi qui l'établit. <br/>
<br/>
              6. En premier lieu, à supposer que la détermination du montant du chiffre d'affaires réalisé en France et hors de France par la société redevable de la contribution exceptionnelle, ainsi que le contrôle, par l'administration fiscale, de ce même montant puissent se heurter à des difficultés d'ordre pratique, ces difficultés ne caractérisent pas une méconnaissance du principe d'égalité par les dispositions de la loi telles qu'interprétées par le Conseil d'État.<br/>
<br/>
              7. En deuxième lieu, en ne prévoyant pas les modalités de traitement comptable des transactions entre une succursale et le siège d'une société de droit étranger, les dispositions attaquées, dont l'objet consiste seulement, comme il a été dit au point  4 ci-dessus, à définir le champ d'application de la contribution supplémentaire à l'impôt sur les sociétés à partir d'un seuil de chiffre d'affaires, ne méconnaissent pas par elles-mêmes le principe d'égalité devant la loi.<br/>
<br/>
              8. En troisième et dernier lieu, s'il existe, pour l'application des dispositions de l'article 235 ter ZAA du code général des impôts, une différence entre les chiffres d'affaires à prendre en compte, selon qu'une société étrangère exerce des activités en France dans le cadre d'une succursale ou dans le cadre d'une filiale, cette différence est la conséquence nécessaire de l'absence de personnalité propre de la succursale et ne peut être regardée comme portant elle-même atteinte à l'égalité devant les charges publiques.  <br/>
<br/>
              9. Il résulte de tout ce qui précède que la question de constitutionnalité invoquée, qui n'est pas nouvelle, ne présente pas un caractère sérieux. Par suite, sans qu'il soit besoin de saisir le Conseil constitutionnel, le moyen tiré de ce que les dispositions du I de l'article 235 ter ZAA du code général des impôts, telles qu'interprétées par le Conseil d'Etat, portent atteinte aux droits et libertés garantis par la Constitution, doit être écarté.<br/>
<br/>
              Sur les moyens du pourvoi :<br/>
<br/>
              10. Aux termes du premier alinéa du I de l'article 209 du code général des impôts : "(...) les bénéfices passibles de l'impôt sur les sociétés sont déterminés (...) en tenant compte uniquement des bénéfices réalisés dans les entreprises exploitées en France (...) ainsi que de ceux dont l'imposition est attribuée à la France par une convention internationale relative aux doubles impositions". L'article 4 de la convention fiscale conclue le 21 juillet 1959 entre la République française et la République fédérale d'Allemagne stipule que : " 1. Les bénéfices d'une entreprise de l'un des Etats contractants ne sont imposables que dans cet Etat, à moins que l'entreprise n'effectue des opérations commerciales dans l'autre Etat par l'intermédiaire d'un établissement stable qui y est situé. Si l'entreprise effectue de telles opérations commerciales, l'impôt peut être perçu sur les bénéfices de l'entreprise dans l'autre Etat, mais uniquement dans la mesure où ces bénéfices peuvent être attribués audit établissement stable. Cette fraction des bénéfices n'est pas imposable dans le premier mentionné des Etats contractants (...) ".<br/>
<br/>
              11. En application des dispositions de l'article 235 ter ZAA citées au point 3 ci-dessus, l'assujettissement à la contribution exceptionnelle sur l'impôt sur les sociétés repose sur la qualité de redevable de l'impôt sur les sociétés et sur la réalisation d'un chiffre d'affaires annuel d'au moins 250 millions d'euros. Si la territorialité de l'impôt sur les sociétés résultant de l'article 209 du code général des impôts limite les bénéfices passibles de l'impôt sur les sociétés à ceux qui sont réalisés en France, sous réserve des stipulations des conventions internationales relatives aux doubles impositions, elle n'a ni pour objet ni pour effet de limiter le chiffre d'affaires pris en compte pour apprécier le seuil d'assujettissement à la contribution exceptionnelle à celui qui est réalisé en France. Par suite, en jugeant qu'il y a lieu, pour l'application de l'article 235 ter ZAA du code général des impôts, de retenir le seul chiffre d'affaires qui se rattache aux bénéfices soumis en France à l'impôt sur les sociétés conformément à l'article 209 de ce code et aux stipulations de la convention fiscale franco-allemande, la cour a fait une inexacte interprétation de l'article 235 ter ZAA.<br/>
<br/>
              12. Il résulte de tout ce qui précède, sans qu'il soit besoin de renvoyer à la Cour de justice de l'Union européenne la question préjudicielle soulevée par la société Commerzbank AG, que le ministre est fondé à demander l'annulation de l'arrêt qu'il attaque.<br/>
<br/>
              13. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat, qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Il n'y a pas lieu de transmettre au Conseil constitutionnel  la question prioritaire de constitutionnalité invoquée par la société Commerzbank AG.<br/>
Article 2 : L'arrêt du 3 novembre 2016 de la cour administrative d'appel de Versailles est annulé.<br/>
Article 3 : L'affaire est renvoyée, dans cette mesure, à la cour administrative d'appel de Versailles.<br/>
Article 4 : Les conclusions présentées par la société Commerzbank AG au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée au ministre de l'action et des comptes publics et à la société Commerzbank AG. <br/>
Copie en sera au Premier ministre.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
