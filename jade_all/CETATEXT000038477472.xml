<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038477472</ID>
<ANCIEN_ID>JG_L_2019_05_000000409630</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/47/74/CETATEXT000038477472.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème chambre, 15/05/2019, 409630, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-05-15</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>409630</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP SPINOSI, SUREAU</AVOCATS>
<RAPPORTEUR>Mme Airelle Niepce</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Stéphane Hoynck</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2019:409630.20190515</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B...A...a demandé au tribunal administratif de Rouen d'annuler pour excès de pouvoir l'arrêté du préfet de la Seine-Maritime du 11 octobre 2016 ordonnant sa reconduite à la frontière et fixant le pays de destination. <br/>
<br/>
              Par une ordonnance n° 1603382 du 25 octobre 2016, le président de la 3ème chambre du tribunal administratif de Rouen a rejeté sa demande.<br/>
<br/>
              Par une ordonnance n° 17DA00069 du 7 février 2017, le président de la cour administrative d'appel de Douai a rejeté son appel formé contre cette ordonnance.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 7 avril et 7 juillet 2017 et le 24 avril 2018, au secrétariat du contentieux du Conseil d'Etat, M. A...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à ses conclusions d'appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat le versement d'une somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - la directive 2008/115/CE du Parlement européen et du Conseil du 16 décembre 2008 relative aux normes et procédures communes applicables dans les États membres au retour des ressortissants de pays tiers en séjour irrégulier ;<br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              - la décision n° 2018-741 QPC du 19 octobre 2018 du Conseil constitutionnel statuant sur la question prioritaire de constitutionnalité soulevée par M. A...;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Airelle Niepce, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Stéphane Hoynck, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Spinosi, Sureau, avocat de M. A...;<br/>
<br/>
              Vu la note en délibéré, enregistré le 11 avril 2019, présentée par M.A....<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que, par un arrêté du 11 octobre 2016, le préfet de la Seine-Maritime a décidé la reconduite à la frontière de M. A..., ressortissant algérien, sur le fondement de l'article L. 533-1 du code de l'entrée et du séjour des étrangers et du droit d'asile. Par une ordonnance du 25 octobre 2016, le président de la 3ème chambre du tribunal administratif de Rouen a rejeté la demande de l'intéressé tendant à l'annulation pour excès de pouvoir de cet arrêté au motif qu'elle était manifestement tardive. Par une ordonnance du 7 février 2017, contre laquelle M. A...se pourvoit en cassation, le président de la cour administrative d'appel de Douai a rejeté son appel. <br/>
<br/>
              2. La Section française de l'Observatoire international des prisons, la CIMADE et le Groupe d'information et de soutien des immigrés justifient, eu égard à la nature et à l'objet du présent litige, d'un intérêt suffisant pour intervenir au soutien du pourvoi de M.A.... Leur intervention est donc recevable. <br/>
<br/>
              3. L'article L. 533-1 code de l'entrée et du séjour des étrangers et du droit d'asile, dans sa rédaction issue de la loi du 16 juin 2011 relative à l'immigration, l'intégration et à la nationalité, prévoit que : " L'autorité administrative compétente peut, par arrêté motivé, décider qu'un étranger (...) doit être reconduit à la frontière : / 1° Si son comportement constitue une menace pour l'ordre public. / (...) / Les articles L. 511-4, L. 512-1 à L. 512-3, le premier alinéa de l'article L. 512-4, le premier alinéa du I de l'article L. 513-1 et les articles L. 513-2, L. 513-3, L. 514-1, L. 514-2 et L. 561-1 du présent code sont applicables aux mesures prises en application du présent article ". Dans sa rédaction issue de la même loi, le II de l'article L. 512-1 du même code dispose que : " L'étranger qui fait l'objet d'une obligation de quitter le territoire sans délai peut, dans les quarante-huit heures suivant sa notification par voie administrative, demander au président du tribunal administratif l'annulation de cette décision (...) ". L'article L. 776-1 du code de justice administrative, dans sa rédaction issue de la même loi, dispose que : " Les modalités selon lesquelles le tribunal administratif examine les recours en annulation formés contre les obligations de quitter le territoire français (...) et les arrêtés de reconduite à la frontière pris en application de l'article L. 533-1 du code de l'entrée et du séjour des étrangers et du droit d'asile obéissent, sous réserve des articles L. 514-1, L. 514-2 et L. 532-1 du même code, aux règles définies par les articles L. 512-1, L. 512-3 et L. 512-4 dudit code ". Enfin, l'article R. 776-4 du même code précise qu'un délai de quarante-huit heures s'applique pour la contestation des arrêtés de reconduite à la frontière.<br/>
<br/>
              4. En premier lieu, le Conseil constitutionnel a, par sa décision n° 2018-741 QPC du 19 octobre 2018, déclaré conformes à la Constitution la référence " L. 512-1 " figurant au dernier alinéa de l'article L. 533-1 du code de l'entrée et du séjour des étrangers et du droit d'asile et les mots " et les arrêtés de reconduite à la frontière pris en application de l'article L. 533-1 du code de l'entrée et du séjour des étrangers et du droit d'asile " figurant à l'article L. 776-1 du code de justice administrative, dans leur rédaction résultant de la loi n° 2011-672 du 16 juin 2011 relative à l'immigration, à l'intégration et à la nationalité. Par suite, le moyen tiré de ce que ces dispositions méconnaîtraient les droits et libertés garantis par la Constitution doit être écarté.<br/>
<br/>
              5. En deuxième lieu, il est soutenu que les dispositions précitées de l'article R. 776-4 du code de justice administrative, en ce qu'elles prévoient un délai bref de 48 heures pour saisir le juge administratif, sans tenir compte de la spécificité de la situation dans laquelle se trouvent les étrangers incarcérés, méconnaîtraient les stipulations des articles 6 et 13 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ainsi que l'article 13 de la directive 2008/115/CE du Parlement européen et du Conseil du 16 décembre 2008 relative aux normes et procédures communes applicables dans les États membres au retour des ressortissants de pays tiers en séjour irrégulier. Il résulte toutefois des dispositions citées au point 3 que le délai de saisine du juge administratif est prévu par la loi, sans que le législateur ait entendu prévoir un régime particulier pour les étrangers détenus. Au surplus devant le juge d'appel le requérant s'est borné à soutenir que la décision attaquée, et non l'article R. 776-4 du code de justice, méconnaissait les textes précités. Le moyen ne peut, par suite, et en tout état de cause, qu'être écarté comme inopérant. <br/>
<br/>
              6. En troisième lieu, il ressort des énonciations souveraines de l'ordonnance attaquée, non arguées de dénaturation, que si M. A...a soutenu que, étant détenu, il n'avait pas été mis en mesure de déposer sa requête dans les délais prescrits, il n'a produit pas davantage en appel qu'en première instance d'éléments de nature à corroborer cette allégation et notamment à établir qu'il aurait été privé de la faculté de transmettre son recours par l'intermédiaire du greffe de la maison d'arrêt dans laquelle il était incarcéré. En en déduisant que l'intéressé, qui avait la possibilité de demander l'annulation de la mesure d'éloignement prise à son encontre dans le respect des délais impartis par le code de l'entrée et du séjour des étrangers et du droit d'asile en s'adressant au greffe de la maison d'arrêt, ne saurait soutenir qu'il n'a pu bénéficier d'un droit à un recours effectif dès lors qu'il lui appartenait d'introduire ce recours dans ces délais et que la circonstance que le recours avait été adressé par voie postale au tribunal administratif dans le délai de 48 heures était sans incidence sur l'irrecevabilité de sa requête, reçue après l'expiration de ce délai, le président de la cour administrative d'appel de Douai, qui devait se prononcer au regard de l'argumentation dont il était saisi, n'a entaché son ordonnance d'aucune erreur de droit. <br/>
<br/>
              7. Il résulte de ce qui précède que M. A...n'est pas fondé à demander l'annulation de l'ordonnance qu'il attaque. Par suite, ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative ne peuvent qu'être rejetées. <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Les interventions de la Section française de l'Observatoire international des prisons (SF-OIP), de la CIMADE (Comité inter-mouvements auprès des évacués) et du Groupe d'information et de soutien des immigrés (GISTI) sont admises.<br/>
<br/>
		Article 2 : Le pourvoi de M. A...est rejeté.<br/>
Article 3 : La présente décision sera notifiée à M. B...A..., à la Section française de l'Observatoire international des prisons (SF-OIP), première intervenante dénommée, et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
