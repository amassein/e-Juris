<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044841278</ID>
<ANCIEN_ID>JG_L_2021_12_000000446763</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/84/12/CETATEXT000044841278.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère - 4ème chambres réunies, 30/12/2021, 446763</TITRE>
<DATE_DEC>2021-12-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>446763</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère - 4ème chambres réunies</FORMATION>
<TYPE_REC/>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>CABINET COLIN - STOCLET ; SARL DIDIER-PINET</AVOCATS>
<RAPPORTEUR>Mme Manon Chonavel</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Vincent Villette</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:446763.20211230</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu les procédures suivantes :<br/>
<br/>
              1° M. G... B..., M. C... AA..., M. E... M..., Mme Q... A... et M. H... O... ont demandé au tribunal administratif de Montpellier d'annuler pour excès de pouvoir l'arrêté du 14 juin 2019 du maire de Lavérune délivrant à la société à responsabilité limitée Kalithys un permis de construire une résidence " seniors et jeunes " de 99 logements en R+2 et attique avec un parking souterrain, ainsi que la décision du 5 août 2019 rejetant leur recours gracieux. Par un jugement n° 1905307 du 23 septembre 2020, le tribunal, après avoir donné acte du désistement de M. O... et Mme A..., a fait droit à cette demande.  <br/>
<br/>
              Sous le n° 446763, par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 23 novembre 2020, 12 février et 28 septembre 2021 au secrétariat du contentieux du Conseil d'Etat, la commune de Lavérune et la société Kalithys demandent au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler ce jugement ; <br/>
<br/>
              2°) de mettre à la charge de M. B... et autres la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Sous le n° 446766, par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 23 novembre 2020, 12 février et 28 septembre 2021 au secrétariat du contentieux du Conseil d'Etat, la commune de Lavérune et la société Kalithys demandent au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler ce jugement ; <br/>
<br/>
              2°) de mettre à la charge AC... V... et autres la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu les autres pièces des dossiers ; <br/>
<br/>
              Vu : <br/>
              - le code de l'urbanisme ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport AC... Manon Chonavel, auditrice,  <br/>
<br/>
              - les conclusions de M. Vincent Villette, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, au Cabinet Colin - Stoclet, avocat de la commune de Lavérune et de la société Kalithys, à la SARL Didier-Pinet, avocat de M. B..., de M. AA... et de M. M..., et à la SCP Bauer-Violas, Feschotte-Desbois, Sebagh, avocat AC... V..., de M. I..., de M. et Mme P..., AC... K..., de M. N... et de M. et Mme S... ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces des dossiers soumis aux juges du fond que, par un arrêté du 14 juin 2019, le maire de Lavérune a accordé à la société Kalithys un permis de construire une résidence intergénérationnelle pour jeunes adultes et personnes âgées. Par deux requêtes distinctes, des voisins du projet ont demandé l'annulation pour excès de pouvoir de ce permis de construire ainsi que de la décision rejetant leur recours gracieux. Par deux jugements du 23 septembre 2020, le tribunal administratif de Montpellier a annulé l'arrêté du 14 juin 2019 du maire de Lavérune et les décisions rejetant les recours gracieux. Par deux pourvois qu'il y a lieu de joindre, la commune de Lavérune et la société Kalithys demandent l'annulation de chacun de ces jugements. <br/>
<br/>
              2. En vertu de l'article L. 123-1 du code de l'urbanisme, désormais repris à l'article L. 151-2 de ce code, le plan local d'urbanisme comprend un rapport de présentation, un projet d'aménagement et de développement durables, des orientations d'aménagement et de programmation, un règlement et des annexes, chacun de ces éléments pouvant comprendre des documents graphiques. Aux termes de l'article L. 123-1-4 du code de l'urbanisme, dans sa rédaction applicable au litige : " Dans le respect des orientations définies par le projet d'aménagement et de développement durables, les orientations d'aménagement et de programmation comprennent des dispositions portant sur l'aménagement, l'habitat, les transports et les déplacements. / 1. En ce qui concerne l'aménagement, les orientations peuvent définir les actions et opérations nécessaires pour mettre en valeur l'environnement, les paysages, les entrées de villes et le patrimoine, lutter contre l'insalubrité, permettre le renouvellement urbain et assurer le développement de la commune. (...) / 2. En ce qui concerne l'habitat, elles définissent les objectifs et les principes d'une politique visant à répondre aux besoins en logements et en hébergements, à favoriser le renouvellement urbain et la mixité sociale et à améliorer l'accessibilité du cadre bâti aux personnes handicapées en assurant entre les communes et entre les quartiers d'une même commune une répartition équilibrée et diversifiée de l'offre de logements (...) ". L'article L. 123-5 du code de l'urbanisme prévoit, dans sa rédaction applicable, que : " Le règlement et ses documents graphiques sont opposables à toute personne publique ou privée pour l'exécution de tous travaux, constructions, plantations, affouillements ou exhaussements des sols, pour la création de lotissements et l'ouverture des installations classées appartenant aux catégories déterminées dans le plan. / Ces travaux ou opérations doivent en outre être compatibles, lorsqu'elles existent, avec les orientations d'aménagement mentionnées au troisième alinéa de l'article L. 123-1 et avec leurs documents graphiques (...) ". En vertu de l'article L. 123-3 du code de l'urbanisme, dans sa rédaction applicable au litige : " Dans les zones d'aménagement concerté, le plan local d'urbanisme peut en outre préciser : / a) La localisation et les caractéristiques des espaces publics à conserver, à modifier ou à créer ; / b) La localisation prévue pour les principaux ouvrages publics, les installations d'intérêt général et les espaces verts ", l'article R*. 123-3-2 du même code précisant que les dispositions prévues aux a) et b) figurent dans le règlement du plan local d'urbanisme ou dans les orientations d'aménagement et de programmation ou leurs documents graphiques. <br/>
<br/>
              3. Il résulte de ces dispositions qu'une autorisation d'urbanisme ne peut être légalement délivrée si les travaux qu'elle prévoit sont incompatibles avec les orientations d'aménagement et de programmation d'un plan local d'urbanisme et, en particulier, en contrarient les objectifs. Il y a lieu de tenir compte, lorsque l'orientation d'aménagement et de programmation porte sur une zone d'aménagement concerté, de la localisation, prévue dans les documents graphiques, des principaux ouvrages publics, des installations d'intérêt général et des espaces verts. Dans l'hypothèse où l'orientation d'aménagement et de programmation prévoit, comme élément de programmation d'une zone d'aménagement concerté, la localisation d'un équipement public précis, la compatibilité de l'autorisation d'urbanisme portant sur cet équipement doit s'apprécier au regard des caractéristiques concrètes du projet et du degré de précision de l'orientation d'aménagement et de programmation, sans que les dispositions du code de l'urbanisme relatives aux destinations des constructions, qui sont sans objet dans l'appréciation à porter sur ce point, aient à être prises en compte. <br/>
<br/>
              4. Il ressort des pièces des dossiers soumis aux juges du fond que, par une délibération du 26 octobre 2011, le conseil municipal de Lavérune a approuvé le dossier de réalisation de la zone d'aménagement concerté du Pouget, dans laquelle s'insère le projet de construction en litige, et que le plan local d'urbanisme a défini, dans le périmètre de cette zone d'aménagement concerté, une orientation d'aménagement et de programmation. Parmi les " grands principes de composition " de la zone d'aménagement concerté figure la réalisation d'" équipements publics (notamment EHPAD) ", tandis que le plan de composition de l'orientation d'aménagement et de programmation identifie les environs du terrain d'assiette du projet en litige comme devant accueillir un " équipement public ". <br/>
<br/>
              5. Il résulte de ce qui a été dit au point 3 qu'en se fondant, pour apprécier la compatibilité du projet autorisé par le permis de construire litigieux avec l'orientation d'aménagement et de programmation ayant prévu un établissement d'hébergement pour personnes âgées dépendantes (EHPAD) au sein de la zone d'aménagement concerté du Pouget, sur la circonstance qu'en vertu de l'article R. 151-28 du code de l'urbanisme, il ne relevait pas de la même sous-destination de construction " équipements d'intérêts collectifs et services publics " qu'un EHPAD, alors qu'il lui incombait de rechercher si, au regard des caractéristiques concrètes du projet et des termes de l'orientation d'aménagement et de programmation, ce dernier contrariait la réalisation des objectifs poursuivis par cette orientation, le tribunal administratif a commis une erreur de droit. <br/>
<br/>
              6. En outre, il ressort des pièces des dossiers soumis aux juges du fond que le projet autorisé est une résidence intergénérationnelle de quatre-vingt-dix-neuf logements, dont soixante-deux ont vocation à accueillir des personnes âgées, qui inclut des espaces collectifs et dont la gestion sera confiée à une association spécialisée dans la gestion de résidences pour personnes âgés et qui est autorisée à fournir des services d'aide à domicile, notamment aux personnes âgées. Par suite, en jugeant qu'il n'était pas compatible avec l'orientation d'aménagement et de programmation de la zone d'aménagement concerté du Pouget, qui poursuivait notamment un objectif de développement d'une offre de logements adaptée aux personnes âgées en situation de dépendance, le tribunal administratif a inexactement qualifié les faits qui lui étaient soumis.<br/>
<br/>
              7. Il résulte de tout ce qui précède que, sans qu'il soit besoin de se prononcer sur les autres moyens des pourvois, la commune de Lavérune et la société Kalithys sont fondées à demander l'annulation des jugements du tribunal administratif de Montpellier qu'elles attaquent.<br/>
<br/>
              8. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise, à ce titre, à la charge de la commune de Lavérune et de la société Kalithys, qui ne sont pas les parties perdantes dans les présentes instances. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de M. B... et autres, d'une part, et AC... V... et autres, d'autre part, le versement à la commune de Lavérune et à la société Kalithys d'une somme de 1 500 euros chacune au titre de ces dispositions.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : Les jugements du 23 septembre 2020 du tribunal administratif de Montpellier sont annulés. <br/>
Article 2 : Les affaires sont renvoyées au tribunal administratif de Montpellier.<br/>
Article 3 : M. B... et autres, d'une part, et Mme V... et autres, d'autre part, verseront une somme de 1 500 euros à la commune de Lavérune et une somme de 1 500 euros à la société Kalithys au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 :  Les conclusions présentées par M. B... et autres et par Mme V... et autres au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 5 : La présente décision sera notifiée à la commune de Lavérune, première dénommée, pour les deux requérantes, à M. G... B..., premier dénommé, pour l'ensemble des défendeurs sous le n° 446763, à Mme X... V..., première dénommée, pour l'ensemble des défendeurs ayant produit sous le n° 446766 et à Mme U... K....<br/>
              Délibéré à l'issue de la séance du 29 novembre 2021 où siégeaient : Mme Maud Vialettes, présidente de chambre, présidant ; Mme Gaëlle Dumortier, présidente de chambre ; Mme D... L..., Mme W... Z..., M. Y... R..., M. F... T..., Mme Carine Chevrier, conseillers d'Etat ; Mme Cécile Chaduteau-Monplaisir, maître des requêtes et Mme Manon Chonavel, auditrice-rapporteure. <br/>
<br/>
Rendu le 30 décembre 2021.<br/>
<br/>
<br/>
                 La présidente : <br/>
                 Signé : Mme Maud Vialettes<br/>
 		La rapporteure : <br/>
      Signé : Mme Manon Chonavel<br/>
                 La secrétaire :<br/>
                 Signé : Mme AB... J...<br/>
<br/>
<br/>
<br/>
La République mande et ordonne à la ministre de la cohésion des territoires et des relations avec les collectivités territoriales en ce qui la concerne ou à tous huissiers de justice à ce requis en ce qui concerne les voies de droit commun contre les parties privées, de pourvoir à l'exécution de la présente décision.<br/>
			Pour expédition conforme,<br/>
			Pour la secrétaire du contentieux, par délégation :<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-08-02-02-01-02 PROCÉDURE. - VOIES DE RECOURS. - CASSATION. - CONTRÔLE DU JUGE DE CASSATION. - BIEN-FONDÉ. - QUALIFICATION JURIDIQUE DES FAITS. - COMPATIBILITÉ DES AUTORISATIONS D'URBANISME AVEC LES OAP D'UN PLU [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">68-01-01-02-019 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. - PLANS D'AMÉNAGEMENT ET D'URBANISME. - PLANS D`OCCUPATION DES SOLS (POS) ET PLANS LOCAUX D’URBANISME (PLU). - APPLICATION DES RÈGLES FIXÉES PAR LES POS OU LES PLU. - PORTÉE DES DIFFÉRENTS ÉLÉMENTS DU PLAN. - OAP - COMPATIBILITÉ DES AUTORISATIONS D'URBANISME - 1) MODALITÉS D'APPRÉCIATION - A) PRINCIPES - B) CONTRÔLE DU JUGE DE CASSATION - QUALIFICATION JURIDIQUE [RJ1] - 2) CAS D'UNE ZAC - OAP PRÉVOYANT LA LOCALISATION D'UN ÉQUIPEMENT PUBLIC PRÉCIS - APPRÉCIATION AU REGARD DES CARACTÉRISTIQUES CONCRÈTES DU PROJET ET DU DEGRÉ DE PRÉCISION DE L'OAP ET NON DES DISPOSITIONS DU CODE DE L'URBANISME RELATIVE À LA DESTINATION DES CONSTRUCTIONS - 2) ESPÈCE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">68-03-03-02-02 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. - PERMIS DE CONSTRUIRE. - LÉGALITÉ INTERNE DU PERMIS DE CONSTRUIRE. - LÉGALITÉ AU REGARD DE LA RÉGLEMENTATION LOCALE. - POS OU PLU (VOIR SUPRA : PLANS D`AMÉNAGEMENT ET D`URBANISME). - COMPATIBILITÉ AVEC UNE OAP - 1) MODALITÉS D'APPRÉCIATION - A) PRINCIPES - B) CONTRÔLE DU JUGE DE CASSATION - QUALIFICATION JURIDIQUE [RJ1] - 2) CAS D'UNE ZAC - OAP PRÉVOYANT LA LOCALISATION D'UN ÉQUIPEMENT PUBLIC PRÉCIS - APPRÉCIATION AU REGARD DES CARACTÉRISTIQUES CONCRÈTES DU PROJET ET DU DEGRÉ DE PRÉCISION DE L'OAP ET NON DES DISPOSITIONS DU CODE DE L'URBANISME RELATIVE À LA DESTINATION DES CONSTRUCTIONS - 2) ESPÈCE.
</SCT>
<ANA ID="9A"> 54-08-02-02-01-02 Le juge de cassation exerce un contrôle de qualification juridique sur la compatibilité de travaux exigeant une autorisation d'urbanisme avec les orientations d'aménagement et de programmation (OAP) d'un plan local d'urbanisme (PLU).</ANA>
<ANA ID="9B"> 68-01-01-02-019 1) a) Il résulte de l'article L. 123-1 du code de l'urbanisme, désormais repris à l'article L. 151-2 de ce code, et de ces articles L. 123-1-4, L. 123-3 et L. 123-5 dans leur rédaction applicable au litige qu'une autorisation d'urbanisme ne peut être légalement délivrée si les travaux qu'elle prévoit sont incompatibles avec les orientations d'aménagement et de programmation (OAP) d'un plan local d'urbanisme (PLU) et, en particulier, en contrarient les objectifs. ......b) Le juge de cassation exerce un contrôle de qualification juridique sur ce point.......2) Il y a lieu de tenir compte, lorsque l'OAP porte sur une zone d'aménagement concerté (ZAC), de la localisation, prévue dans les documents graphiques, des principaux ouvrages publics, des installations d'intérêt général et des espaces verts. ......Dans l'hypothèse où l'OAP prévoit, comme élément de programmation d'une ZAC, la localisation d'un équipement public précis, la compatibilité de l'autorisation d'urbanisme portant sur cet équipement doit s'apprécier au regard des caractéristiques concrètes du projet et du degré de précision de l'OAP, sans que les dispositions du code de l'urbanisme relatives aux destinations des constructions, qui sont sans objet dans l'appréciation à porter sur ce point, aient à être prises en compte. ......3) Conseil municipal ayant approuvé le dossier de réalisation d'une ZAC, dans laquelle s'insère le projet de construction en litige. PLU de la commune ayant défini, dans le périmètre de cette ZAC, une OAP. Parmi les grands principes de composition de la ZAC figure la réalisation d' équipements publics (notamment EHPAD), tandis que le plan de composition de OAP identifie les environs du terrain d'assiette du projet en litige comme devant accueillir un équipement public.......Pour apprécier la compatibilité du projet autorisé par le permis de construire litigieux avec l'OAP ayant prévu un établissement d'hébergement pour personnes âgées dépendantes (EHPAD) au sein de la ZAC, il convient de rechercher si, au regard des caractéristiques concrètes du projet et des termes de l'OAP, ce dernier contrariait la réalisation des objectifs poursuivis par cette orientation, sans qu'ait d'incidence la circonstance qu'en vertu de l'article R. 151-28 du code de l'urbanisme, le projet ne relevait pas de la même sous-destination de construction équipements d'intérêts collectifs et services publics qu'un EHPAD.</ANA>
<ANA ID="9C"> 68-03-03-02-02 1) a) Il résulte de l'article L. 123-1 du code de l'urbanisme, désormais repris à l'article L. 151-2 de ce code, et de ces articles L. 123-1-4, L. 123-3 et L. 123-5 dans leur rédaction applicable au litige qu'une autorisation d'urbanisme ne peut être légalement délivrée si les travaux qu'elle prévoit sont incompatibles avec les orientations d'aménagement et de programmation (OAP) d'un plan local d'urbanisme (PLU) et, en particulier, en contrarient les objectifs. ......b) Le juge de cassation exerce un contrôle de qualification juridique sur ce point.......2) Il y a lieu de tenir compte, lorsque l'OAP porte sur une zone d'aménagement concerté (ZAC), de la localisation, prévue dans les documents graphiques, des principaux ouvrages publics, des installations d'intérêt général et des espaces verts. ......Dans l'hypothèse où l'OAP prévoit, comme élément de programmation d'une ZAC, la localisation d'un équipement public précis, la compatibilité de l'autorisation d'urbanisme portant sur cet équipement doit s'apprécier au regard des caractéristiques concrètes du projet et du degré de précision de l'OAP, sans que les dispositions du code de l'urbanisme relatives aux destinations des constructions, qui sont sans objet dans l'appréciation à porter sur ce point, aient à être prises en compte. ......3) Conseil municipal ayant approuvé le dossier de réalisation d'une ZAC, dans laquelle s'insère le projet de construction en litige. PLU de la commune ayant défini, dans le périmètre de cette ZAC, une OAP. Parmi les grands principes de composition de la ZAC figure la réalisation d' équipements publics (notamment EHPAD), tandis que le plan de composition de OAP identifie les environs du terrain d'assiette du projet en litige comme devant accueillir un équipement public.......Pour apprécier la compatibilité du projet autorisé par le permis de construire litigieux avec l'OAP ayant prévu un établissement d'hébergement pour personnes âgées dépendantes (EHPAD) au sein de la ZAC, il convient de rechercher si, au regard des caractéristiques concrètes du projet et des termes de l'OAP, ce dernier contrariait la réalisation des objectifs poursuivis par cette orientation, sans qu'ait d'incidence la circonstance qu'en vertu de l'article R. 151-28 du code de l'urbanisme, le projet ne relevait pas de la même sous-destination de construction équipements d'intérêts collectifs et services publics qu'un EHPAD.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Comp., en matière d'aménagement commercial, CE, 6 juin 2018, Société Hurtevent LC, n° 405608, T. pp. 588-589-869. Rappr., s'agissant de la compatibilité d'un document d'urbanisme avec les orientations d'un schéma directeur d'aménagement et d'urbanisme, CE, 29 décembre 1999, Commune de Moze-sur-Louet, n° 197206, aux Tables.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
