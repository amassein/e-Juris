<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032825419</ID>
<ANCIEN_ID>JG_L_2016_07_000000396051</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/82/54/CETATEXT000032825419.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème chambre, 01/07/2016, 396051, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-07-01</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>396051</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP COUTARD, MUNIER-APAIRE</AVOCATS>
<RAPPORTEUR>Mme Marie-Anne Lévêque</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Olivier Henrard</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2016:396051.20160701</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés au secrétariat du contentieux du Conseil d'Etat les 12 janvier et 6 juin 2016, M. B...A...demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler la décision implicite par laquelle le Premier ministre a rejeté sa demande tendant à la modification de l'article R. 6 du code des pensions de retraite des marins français du commerce, de pêche ou de plaisance et de l'article 2 du décret n° 2013-992 du 6 novembre 2013 ; <br/>
<br/>
              2°) d'enjoindre au Premier ministre de modifier les dispositions attaquées dans un délai de quatre mois, sous une astreinte de 500 euros par jour de retard ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 000 euros en application des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code des transports ;<br/>
              - le code des pensions de retraite des marins français du commerce, de pêche ou de plaisance ; <br/>
              - la loi n° 99-882 du 18 octobre 1999 ; <br/>
              - l'ordonnance n° 2010-1307 du 28 octobre 2010 ;<br/>
              - le décret n° 2013-992 du 6 novembre 2013 ; <br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Marie-Anne Lévêque, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Olivier Henrard, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Coutard, Munier-Apaire, avocat de M. A...; <br/>
<br/>
              Vu la note en délibéré, enregistrée le 14 juin 2016, présentée par M. A...;<br/>
<br/>
<br/>
<br/>1. Considérant que M.A..., marin retraité, demande au Conseil d'Etat l'annulation du refus opposé par le Premier ministre à sa demande tendant à la modification de l'article R. 6 du code des pensions de retraite des marins français du commerce, de pêche ou de plaisance et de l'article 2 du décret du 6 novembre 2013 afin de permettre l'attribution de la bonification pour services militaires accomplis en temps de guerre à l'ensemble des marins ayant accompli leurs obligations militaires en Afrique du Nord pendant la période de la guerre d'Algérie et des combats en Tunisie et au Maroc, en autorisant la révision des pensions liquidées avant le 19 octobre 1999 ;<br/>
<br/>
              Sur les conclusions dirigées contre le refus de modifier l'article R. 6 du code des pensions de retraite des marins français du commerce, de pêche ou de plaisance :<br/>
<br/>
              2. Considérant qu'aux termes de l'article L. 5552-17 du code des transports, issu de l'ordonnance du 28 octobre 2010 qui a abrogé l'article L. 11 du code des pensions de retraite des marins français du commerce, de pêche ou de plaisance : " Par dérogation à l'article L. 5552-14, entrent en compte pour le double de leur durée : / 1° Les services militaires et les temps de navigation active et professionnelle accomplis en période de guerre ; / (...) / Un décret en Conseil d'Etat fixe les conditions d'application du présent article " ; qu'aux termes de l'article R. 6 du code des pensions de retraite des marins français du commerce, de pêche ou de plaisance, dans sa rédaction issue du 6 novembre 2013 : " En application du 1° de l'article L. 5552-17 du code des transports, comptent pour le double de leur durée : / (...) / D. - Pendant la guerre d'Algérie et les combats en Tunisie et au Maroc, entre le 1er janvier 1952 et le 2 juillet 1962, les services militaires embarqués au large des côtes algériennes, tunisiennes et marocaines et les services militaires à terre en Algérie, en Tunisie et au Maroc durant lesquels le marin a pris part à une action de feu ou de combat ou a subi le feu. / L'exposition invoquée en faveur de ce bénéfice sera établie par les archives collectives de l'unité à laquelle les marins étaient rattachés ou l'unité concernant le secteur dans lequel se sont produites ces actions " ;<br/>
<br/>
              3. Considérant qu'il résulte des dispositions citées ci-dessus de l'article L. 5552-17 du code des transports qu'il appartient au pouvoir réglementaire de fixer les conditions d'application de cet article et, en particulier, de déterminer la nature et la durée des services permettant l'octroi de la bonification dont ces dispositions ont institué le principe ; qu'ainsi que le Conseil d'Etat statuant au contentieux l'a jugé par sa décision n° 376500 du 11 février 2015, en prévoyant que comptaient pour le double de leur durée, entre le 1er janvier 1952 et le 2 juillet 1962, les services militaires embarqués au large des côtes algériennes, tunisiennes et marocaines et les services militaires à terre en Algérie, en Tunisie et au Maroc durant lesquels le marin a pris part à une action de feu ou de combat ou a subi le feu, l'auteur du décret attaqué n'a pas méconnu ces dispositions législatives et n'est donc pas tenu de modifier les dispositions réglementaires contestées par le requérant ;<br/>
<br/>
              Sur les conclusions dirigées contre le refus de modifier l'article 2 du décret du 6 novembre 2013 : <br/>
<br/>
              4. Considérant que la loi du 18 octobre 1999 a substitué aux mots : " aux opérations effectuées en Afrique du Nord " les mots : " à la guerre d'Algérie ou aux combats en Tunisie et au Maroc " aux articles L. 1er bis, L. 243, L. 253 bis et L. 401 bis du code des pensions militaires d'invalidité et des victimes de guerre, ainsi qu'à l'article L. 321-9 du code de la mutualité ; qu'ainsi que le Conseil d'Etat statuant au contentieux l'a jugé par sa décision du 11 février 2015, il ne résulte ni des termes de la loi, ni de ses travaux préparatoires que le législateur ait entendu donner une portée rétroactive aux dispositions qu'il a édictées, seule à même de permettre la révision des pensions liquidées avant leur entrée en vigueur ; que le Premier ministre n'a donc pas commis d'erreur de droit en refusant de faire droit à la demande du requérant tendant à ce que l'article 2 du décret du 6 novembre 2013 soit modifié afin de permettre la révision des pensions liquidées avant le 19 octobre 1999 ;<br/>
<br/>
              5. Considérant qu'il résulte de tout ce qui précède, et sans qu'il soit besoin de statuer sur la fin de non recevoir opposée par la ministre de l'environnement, de l'énergie et de la mer, chargée des relations internationales sur le climat que les conclusions de M. A...dirigées contre la décision attaquée doivent être rejetées ainsi que, par suite, ses conclusions aux fins d'injonction et celles présentées au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête M. A...est rejetée.<br/>
Article 2 : La présente décision sera notifiée à M. B...A..., au Premier ministre, à la ministre de l'environnement, de l'énergie et de la mer, chargée des relations internationales sur le climat, à la ministre des affaires sociales et de la santé et au ministre de l'économie, de l'industrie et du numérique.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
