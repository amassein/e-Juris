<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044153770</ID>
<ANCIEN_ID>JG_L_2021_09_000000449925</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/15/37/CETATEXT000044153770.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème chambre, 30/09/2021, 449925, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-09-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>449925</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Arno Klarsfeld</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Laurent Domingo</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:449925.20210930</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La Commission nationale des comptes de campagne et des financements politiques a saisi le tribunal administratif d'Amiens, en application de l'article L. 52-15 du code électoral,  de sa décision du 26 novembre 2020 par laquelle elle a constaté le dépôt hors délai du compte de campagne de M. A... H..., candidat tête de liste aux élections municipales et communautaires qui se sont tenues les 15 mars et 28 juin 2020 dans la commune de Crépy-en-Valois (Oise) et a décidé que celui-ci n'avait pas droit au remboursement forfaitaire de l'Etat. Par un jugement n° 2003947 du 28 janvier 2021, le tribunal administratif a jugé que le dépôt tardif du compte de campagne et l'absence de droit au remboursement ont été respectivement constatés et décidés à bon droit, a prononcé l'inéligibilité de M. H... pour une durée de six mois à compter de la date à laquelle son jugement deviendrait définitif et, par voie de conséquence, a annulé son élection à compter de la même date et déclaré élus à sa place M. F... J... en qualité de conseiller municipal et M. D... B... en qualité de conseiller communautaire. <br/>
<br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés les 19 février et 12 mai 2021 au secrétariat du contentieux du Conseil d'Etat, M. H... demande au Conseil d'Etat :   <br/>
<br/>
              1°) d'annuler ce jugement en tant qu'il l'a déclaré inéligible ;  <br/>
<br/>
              2°) de décider qu'il n'y a pas lieu de le déclarer inéligible ; <br/>
<br/>
              3°) à titre infiniment subsidiaire dans le cas où son inéligibilité serait confirmée, de proclamer élus d'autres candidats que ceux désignés par le tribunal administratif, ceux-ci étant déjà membres, respectivement, du conseil municipal et du conseil communautaire. <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code électoral ;<br/>
              - la loi n° 2020-290 du 23 mars 2020 ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Arno Klarsfeld, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Laurent Domingo, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. En vertu de l'article L. 52-12 du code électoral, chaque candidat ou candidat tête de liste soumis au plafonnement des dépenses électorales prévu à l'article L. 52-11 est tenu d'établir un compte de campagne lorsqu'il a obtenu au moins 1 % des suffrages exprimés ou s'il a bénéficié de dons de personnes physiques et de le déposer à la Commission nationale des comptes de campagne et des financements politiques au plus tard avant 18 heures le dixième vendredi suivant le premier tour de scrutin. Le 4° du XII de l'article 19 de la loi du 23 mars 2020 d'urgence pour faire face à l'épidémie de covid-18 a reporté la date limite mentionnée par ces dispositions, pour les listes de candidats présentes au second tour, au 11 septembre 2020 à 18 heures. Aux termes du troisième alinéa de l'article L. 52-15 du code électoral, " Lorsque la commission a constaté que le compte de campagne n'a pas été déposé dans le délai prescrit (...), la commission saisit le juge de l'élection ". Aux termes de l'article L. 118-3: " Lorsqu'il relève une volonté de fraude ou un manquement d'une particulière gravité aux règles de financement des campagnes électorales, le juge de l'élection, saisi par la Commission nationale des comptes de campagne et des financements politiques, peut déclarer inéligible : / 1° Le candidat qui n'a pas déposé son compte de campagne dans les conditions et le délai prescrits à l'article L. 52-12 ; / (...) / L'inéligibilité mentionnée au présent article est prononcée pour une durée maximale de trois ans et s'applique à toutes les élections. Toutefois, elle n'a pas d'effet sur les mandats acquis antérieurement à la date de la décision. / (...) / Si le juge de l'élection a prononcé l'inéligibilité d'un candidat ou des membres d'un binôme proclamé élu, il annule son élection ou, si l'élection n'a pas été contestée, déclare le candidat ou les membres du binôme démissionnaires d'office ". <br/>
<br/>
              2. En dehors des cas de fraude, le juge de l'élection ne peut prononcer l'inéligibilité d'un candidat sur le fondement de ces dispositions que s'il constate un manquement d'une particulière gravité aux règles de financement des campagnes électorales. Il lui incombe à cet effet de prendre en compte l'ensemble des circonstances de l'espèce et d'apprécier s'il s'agit d'un manquement caractérisé à une règle substantielle relative au financement des campagnes électorales et s'il présente un caractère délibéré. <br/>
<br/>
              3. Il est constant que M. H..., candidat tête de liste aux élections qui se sont déroulées les 15 mars et 28 juin 2020 en vue de la désignation des conseillers municipaux et communautaires de la commune de Crépy-en-Valois, a déposé son compte de campagne le 28 octobre 2020, postérieurement au délai prescrit. Il soutient que son mandataire financer a cru, par erreur, que les frais de la campagne officielle, bien que donnant droit par ailleurs à un remboursement par l'Etat en vertu de l'article R. 39 du code électoral, devaient être portées en dépense au compte de campagne, que lui-même a donc voulu attendre de connaître le montant de leur remboursement, dans l'intention d'inscrire celui-ci en recettes, et que la préfecture ne lui a communiqué cette information que trop tardivement. Toutefois, le deuxième alinéa de l'article L. 52-12 du code électoral dispose en termes explicites que les dépenses de la campagne officielle ne doivent pas figurer au compte de campagne. La conduite à tenir pour ce type de dépenses était expressément indiquée par les documents que la Commission avait mis à la disposition des candidats. M. H... a ainsi commis un manquement caractérisé et délibéré à une règle substantielle relative au financement des campagnes électorales.<br/>
<br/>
              4. Le requérant fait également valoir que la commune a été, dès février 2020, parmi les toutes premières et les plus sévèrement touchées par la covid 19, qu'il a été atteint de la maladie fin février, que son mandataire a subi une épreuve d'ordre personnel, et que lui-même, en tant que maire, a donné la priorité à la lutte contre l'épidémie, à l'aide aux habitants et à la préparation de son audition par la commission d'enquête du Sénat en juillet 2020. Si ces circonstances ne sont pas contestées, il ne résulte pas de l'instruction qu'elles auraient mis M. H... dans l'impossibilité de déposer le compte dans le délai qui, ainsi qu'il a été dit au point 1, avait été reporté par la loi pour tenir compte de la crise et expirait le 11 septembre. <br/>
<br/>
              5. Il résulte de ce qui précède que M. H... doit être regardé comme ayant méconnu de manière délibérée une règle substantielle relative au financement des campagnes électorales. Par suite, il n'est pas fondé à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif d'Amiens l'a déclaré inéligible pour une durée de 6 mois. <br/>
<br/>
              6. Aux termes de l'article L. 270 du code électoral : " Le candidat venant sur une liste immédiatement après le dernier élu est appelé à remplacer le conseiller municipal élu sur cette liste dont le siège devient vacant pour quelque cause que ce soit (...) ". Aux termes de de l'article L. 273-10 : " Lorsque le siège d'un conseiller communautaire devient vacant, pour quelque cause que ce soit, il est pourvu par le candidat de même sexe élu conseiller municipal ou conseiller d'arrondissement suivant sur la liste des candidats aux sièges de conseiller communautaire sur laquelle le conseiller à remplacer a été élu. (...) ". En vertu de ces mêmes articles et dans l'un et l'autre cas, la constatation, par la juridiction administrative, de l'inéligibilité d'un ou plusieurs candidats n'entraîne l'annulation de l'élection que du ou des élus inéligibles. La juridiction saisie proclame en conséquence l'élection du ou des candidats désignés en application de ces dispositions. Il résulte de l'instruction que M. J... et M. B..., proclamés élus par le tribunal administratif respectivement au conseil municipal et au conseil communautaire, en sont déjà devenus membres à la suite d'autres vacances. En conséquence, il y a lieu de proclamer élus M. I... C... au conseil municipal et M. E... G... au conseil communautaire. <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Les conclusions de la requête de M. H... dirigées contre le jugement attaqué du tribunal administratif d'Amiens en tant qu'il le déclare inéligible sont rejetées.<br/>
Article 2 : M. C... est proclamé élu conseiller municipal de Crépy-en-Valois. M. G... est proclamé élu conseiller communautaire. <br/>
<br/>
Article 3 : Le jugement du 28 janvier 2021 du tribunal administratif d'Amiens est réformé en ce qu'il a de contraire à la présente décision.<br/>
Article 4 : La présente décision sera notifiée à M. A... H..., à M. F... J..., à M. D... B..., à M. I... C..., à Daniel G... et à la commune de Crépy-en-Valois. <br/>
Copie en sera adressée au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
