<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044806233</ID>
<ANCIEN_ID>JG_L_2021_12_000000450415</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/80/62/CETATEXT000044806233.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème chambre, 30/12/2021, 450415, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-12-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>450415</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Bruno Bachini</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Stéphane Hoynck</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:450415.20211230</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu les procédures suivantes :<br/>
<br/>
              La Commission nationale des comptes de campagne et des financements politiques a saisi le tribunal administratif de Montpellier, en application de l'article L. 52-15 du code électoral, sur le fondement de sa décision du 12 novembre 2020 rejetant le compte de campagne de M. D... A..., candidat tête de liste aux élections municipales du 15 mars 2020 à Perpignan (Pyrénées-Orientales). Par un jugement n° 2005489 du 23 février 2021, le tribunal administratif a déclaré M. A... inéligible à tout mandat pour une durée de six mois. <br/>
<br/>
              1° Sous le numéro 450415, par une requête enregistrée le 5 mars 2021 au secrétariat du contentieux du Conseil d'Etat, M. A... demande au Conseil d'Etat d'annuler ce jugement. <br/>
<br/>
<br/>
<br/>
              2° Sous le n° 450821, par une requête enregistrée le 18 mars 2021 au secrétariat du contentieux du Conseil d'Etat, M. A... demande au Conseil d'Etat d'ordonner qu'il soit sursis à l'exécution de ce jugement. <br/>
<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu : <br/>
              - le code électoral ;<br/>
              - la loi n° 2019-1269 du 2 décembre 2019 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Bruno Bachini, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Stéphane Hoynck, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Le requête en appel de M. A... et sa requête aux fins de sursis à exécution sont dirigées contre le même jugement du tribunal administratif de Montpellier du 23 février 2021. Il y a lieu de les joindre pour statuer par une seule décision.<br/>
<br/>
              2. Il résulte de l'instruction que, par une décision du 12 novembre 2020, la Commission nationale des comptes de campagne et des financements politiques a rejeté le compte de campagne de M. A..., candidat non élu aux élections municipales du 15 mars 2020 dans la commune de Perpignan (Pyrénées-Orientales). En application des dispositions de l'article L. 52-15 du code électoral, la Commission a saisi le tribunal administratif de Montpellier qui, par un jugement du 23 février 2021, a déclaré M. A... inéligible pour une durée de six mois. M. A... relève appel de ce jugement en contestant seulement l'inéligibilité prononcée.<br/>
<br/>
              3. D'une part, aux termes de l'article L 52-7-1 du code électoral : " Les personnes physiques peuvent consentir des prêts à un candidat dès lors que ces prêts ne sont pas effectués à titre habituel. / La durée de ces prêts ne peut excéder cinq ans. Un décret en Conseil d'Etat fixe le plafond et les conditions d'encadrement du prêt, de la durée ainsi que de ses modalités et de ses conditions de remboursement. (...) ". Aux termes de l'article R. 39-2-1 du même code : " Les candidats auxquels sont applicables les dispositions de l'article L. 52-7-1 du code électoral peuvent emprunter auprès de personnes physiques à un taux d'intérêt compris entre zéro et le taux d'intérêt légal en vigueur au moment du consentement des prêts. Le taux légal d'intérêt est celui applicable aux créances des personnes physiques n'agissant pas pour des besoins professionnels. Ces prêts sont consentis aux conditions suivantes : /1° La durée de chaque prêt est inférieure ou égale à 18 mois ; /2° Le montant total dû par le candidat à des personnes physiques est inférieur ou égal à 47,5% du plafond de remboursement forfaitaire des dépenses de campagne mentionné à l'article L. 52-11-1 du code électoral ; (...) ". <br/>
<br/>
              4. D'autre part, aux termes de l'article L. 118-3 du code électoral, dans sa rédaction issue de la loi du 2 décembre 2019 visant à clarifier diverses dispositions du droit électoral : " Lorsqu'il relève une volonté de fraude ou un manquement d'une particulière gravité aux règles de financement des campagnes électorales, le juge de l'élection, saisi par la Commission nationale des comptes de campagne et des financements politiques, peut déclarer inéligible : (...) 3° Le candidat dont le compte de campagne a été rejeté à bon droit. (...) ". Il résulte de ces dispositions qu'en dehors des cas de fraude, le juge de l'élection ne peut prononcer l'inéligibilité d'un candidat que s'il constate un manquement d'une particulière gravité aux règles de financement des campagnes électorales. Il lui incombe, à cet effet, de prendre en compte l'ensemble des circonstances de l'espèce et d'apprécier, d'une part, si elles révèlent un manquement caractérisé à une règle substantielle relative au financement des campagnes électorales et, d'autre part, si ce manquement présente un caractère délibéré. <br/>
<br/>
              5. S'il n'est pas contesté qu'en bénéficiant d'un prêt à taux zéro pour un montant de 35 000 euros pour une durée de cinq ans, M. A... a méconnu les dispositions combinées des articles L. 52-7-1 et R. 39-2-1 du code électoral citées au point 3, ce qui justifiait le rejet de son compte de campagne, il résulte de l'instruction, d'une part, que ce manquement ne présente pas un caractère délibéré, d'autre part, que le prêt a été remboursé dans un délai d'un an, inférieur au délai de dix-huit mois prévu par l'article R. 39-2-1 et, enfin, que le compte de campagne de M. A... ne fait pas apparaître d'autres irrégularités. Dans ces conditions, il n'y a pas lieu de prononcer l'inéligibilité de M. A.... Dès lors, celui-ci est fondé à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Montpellier l'a déclaré inéligible pour une durée de six mois.<br/>
<br/>
              6. Il résulte de ce qui précède que les conclusions tendant à ce qu'il soit sursis à l'exécution du jugement du 23 février 2021 du tribunal administratif de Montpellier deviennent sans objet. <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : Le jugement du 23 février 2021 du tribunal administratif de Montpellier est annulé. <br/>
Article 2 : Il n'y a pas lieu de déclarer M. A... inéligible.<br/>
Article 3 : Il n'y a pas lieu de statuer sur les conclusions de la requête n° 450821 tendant à ce qu'il soit sursis à l'exécution du jugement du 23 février 2021 du tribunal administratif de Montpellier.<br/>
Article 4 : La présente décision sera notifiée à M. D... A..., à la Commission nationale des comptes de campagne et des financements politiques et au ministre de l'intérieur.   <br/>
<br/>
<br/>
<br/>
              Délibéré à l'issue de la séance du 20 décembre 2021 où siégeaient : M. Fabien Raynaud, président de chambre, présidant ; Mme Suzanne von Coester, conseillère d'Etat et M. Bruno Bachini, maître des requêtes-rapporteur. <br/>
<br/>
              Rendu le 30 décembre 2021.<br/>
<br/>
                 Le président : <br/>
                 Signé : M. Fabien Raynaud<br/>
<br/>
 		Le rapporteur : <br/>
      Signé : M. Bruno Bachini<br/>
<br/>
                 La secrétaire :<br/>
                 Signé : Mme C... B...<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
