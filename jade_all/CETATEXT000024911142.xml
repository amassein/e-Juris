<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000024911142</ID>
<ANCIEN_ID>JG_L_2011_12_000000347497</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/24/91/11/CETATEXT000024911142.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère et 6ème sous-sections réunies, 02/12/2011, 347497</TITRE>
<DATE_DEC>2011-12-02</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>347497</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère et 6ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Jacques Arrighi de Casanova</PRESIDENT>
<AVOCATS/>
<RAPPORTEUR>M. Jean Lessi</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Claire Landais</COMMISSAIRE_GVT>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 14 mars 2011 au secrétariat du contentieux du Conseil d'Etat, présentée par la FNATH, ASSOCIATION DES ACCIDENTES DE LA VIE, dont le siège est 42, rue des Alliés à Saint-Etienne (42000), représentée par son président, par le COLLECTIF INTER ASSOCIATIF SUR LA SANTE (CISS), dont le siège est 10, villa Bosquet à Paris (75007), représentée par son président et par l'UNION NATIONALE DES ASSOCIATIONS FAMILIALES (UNAF), dont le siège est 28, place Saint-Georges à Paris (75009), représentée par son président ; la FNATH et autres demandent au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret n° 2011-56 du 14 janvier 2011 relatif à la participation de l'assuré prévue à l'article L. 322-2 du code de la sécurité sociale ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat le versement de la somme de 2 500 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de la sécurité sociale ; <br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean Lessi, Auditeur,  <br/>
<br/>
              - les conclusions de Mme Claire Landais, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>Considérant qu'il résulte du I de l'article L. 322-2 du code de la sécurité sociale que " La participation de l'assuré aux tarifs servant de base au calcul des prestations prévues aux 1°, 2° et 3° de l'article L. 321-1 (...) est fixée dans des limites et des conditions fixées par décret en Conseil d'Etat, par décision de l'Union nationale des caisses d'assurance maladie, après avis de l'Union nationale des organismes d'assurance maladie complémentaire. Le ministre chargé de la santé peut s'opposer à cette décision pour des motifs de santé publique. La décision du ministre est motivée (...) " ; <br/>
<br/>
              Considérant qu'eu égard aux moyens qu'elle invoque, la requête de la FNATH, ASSOCIATION DES ACCIDENTES DE LA VIE, du COLLECTIF INTER ASSOCIATIF SUR LA SANTE (CISS), et de l'UNION NATIONALE DES ASSOCIATIONS FAMILIALES (UNAF) doit être regardée comme étant seulement dirigée contre l'article 3 du décret du 14 janvier 2011 relatif à la participation de l'assuré prévue à l'article L. 322-2 du code de la sécurité sociale, qui complète l'article R. 322-9-4 de ce code par un alinéa aux termes duquel : " Si, dans un délai de deux mois à compter de l'entrée en vigueur d'une modification des limites de taux de participation de l'assuré mentionnées à l'article R. 322-1, le conseil de l'UNCAM n'a pas fixé le taux de la participation de l'assuré dans les limites ainsi modifiées, le taux applicable est égal soit à la limite minimale si le taux fixé antérieurement lui est inférieur, soit à la limite maximale si le taux fixé antérieurement lui est supérieur. Un arrêté du ministre chargé de la santé publié au Journal officiel de la République française constate le nouveau taux applicable " ; <br/>
<br/>
              Sur la recevabilité de la requête :<br/>
<br/>
              Considérant qu'eu égard à leur objet statutaire, la FNATH, le CISS et l'UNAF justifient d'un intérêt leur donnant qualité pour demander l'annulation pour excès de pouvoir de l'article 3 du décret attaqué, qui affecte la réglementation des conditions de prise en charge des prestations délivrées aux assurés sociaux et peut avoir pour effet d'augmenter le taux de participation de ces derniers aux tarifs de ces prestations ;<br/>
<br/>
              Sur la légalité de l'article 3 du décret attaqué :<br/>
<br/>
              Considérant que les dispositions contestées ont pour effet, en cas de refus ou de carence prolongée pendant deux mois de l'UNCAM à fixer un taux de participation de l'assuré relatif à une catégorie de frais compatible avec les nouvelles limites de la fourchette de taux afférente à cette catégorie, arrêtées par décret en Conseil d'Etat, de provoquer la fixation automatique du taux à une valeur égale à la limite de la nouvelle fourchette la plus proche de l'ancien taux, en dehors de toute décision de l'UNCAM ; qu'il résulte toutefois des dispositions précitées de l'article L. 322-2 que la fixation du taux de participation de l'assuré ressortit exclusivement à la compétence de cet établissement public ; que, par suite, les requérants sont fondés à soutenir que l'auteur du décret attaqué a méconnu ces dispositions législatives et, sans qu'il soit besoin d'examiner les autres moyens de la requête, à demander l'annulation des dispositions de l'article 3 du décret attaqué, qui sont divisibles des autres dispositions de ce décret ; <br/>
<br/>
              Sur les conséquences de l'illégalité des dispositions contestées :<br/>
<br/>
              Considérant que, compte tenu des effets excessifs qu'emporterait une annulation rétroactive de ces dispositions, notamment pour la gestion de plusieurs millions de remboursements effectués depuis que ces dispositions ont été mises en oeuvre par les caisses d'assurance maladie, il y a lieu de prévoir que l'annulation prononcée par la présente décision ne prendra effet qu'au 30 avril 2012 et que, sous réserve des actions contentieuses engagées à la date de la présente décision contre des actes pris sur leur fondement, les effets produits par les dispositions attaquées antérieurement à leur annulation seront réputés définitifs ;<br/>
<br/>
              Sur l'application des dispositions de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme globale de 2 500 euros à verser à la FNATH, au CISS et à l'UNAF au titre de ces dispositions ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'article 3 du décret n° 2011-56 du 14 janvier 2011 est annulé. Cette annulation prendra effet le 30 avril 2012.<br/>
Article 2 : Sous réserve des actions contentieuses engagées à la date de la présente décision contre des actes pris sur le fondement des dispositions annulées, les effets produits par ces dispositions antérieurement à leur annulation sont réputés définitifs.<br/>
Article 3 : L'Etat versera à la FNATH, au CISS et à l'UNAF une somme globale de 2 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à la FNATH, ASSOCIATION DES ACCIDENTES DE LA VIE, à l'ASSOCIATION LE COLLECTIF INTER ASSOCIATIF SUR LA SANTE, à l'UNION NATIONALE DES ASSOCIATIONS FAMILIALES, au Premier ministre et au ministre du travail, de l'emploi et de la santé.<br/>
<br/>
</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-07-023 PROCÉDURE. POUVOIRS ET DEVOIRS DU JUGE. - ANNULATION DES DISPOSITIONS RÉGLEMENTAIRES FIXANT LE TAUX DE PARTICIPATION DES ASSURÉS AUX TARIFS SERVANT DE BASE AU CALCUL DE CERTAINES PRESTATIONS D'ASSURANCE MALADIE - EFFETS EXCESSIFS D'UNE ANNULATION RÉTROACTIVE POUR LA GESTION DES REMBOURSEMENTS - CONSÉQUENCE - ANNULATION AYANT L'EFFET D'UNE ABROGATION POUR L'AVENIR [RJ2].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">62-03-02-01 SÉCURITÉ SOCIALE. COTISATIONS. ASSIETTE, TAUX ET CALCUL DES COTISATIONS. ASSURANCE MALADIE, MATERNITÉ, INVALIDITÉ ET DÉCÈS. - PARTICIPATION DE L'ASSURÉ AUX TARIFS SERVANT DE BASE AU CALCUL DE CERTAINES PRESTATIONS - FIXATION DU TAUX DE PARTICIPATION - COMPÉTENCE EXCLUSIVE DE L'UNCAM (ART. L. 322-2 DU CODE DE LA SÉCURITÉ SOCIALE) - CONSÉQUENCE - ILLÉGALITÉ DE DISPOSITIONS RÉGLEMENTAIRES PRÉVOYANT LA FIXATION AUTOMATIQUE D'UN TAUX EN CAS DE CARENCE DE L'UNCAM.
</SCT>
<ANA ID="9A"> 54-07-023 Annulation des dispositions réglementaires, introduites par l'article 3 du décret n° 2011-56 du 14 janvier 2011 à l'article R. 322-9-4 du code de la sécurité sociale, prévoyant certaines modalités de fixation du taux de participation des assurés aux tarifs servant de base au calcul de certaines prestations d'assurance maladie. Compte tenu des effets excessifs qu'emporterait une annulation rétroactive de ces dispositions, notamment pour la gestion de plusieurs millions de remboursements effectués depuis qu'elles ont été mises en oeuvre par les caisses d'assurance maladie, l'annulation prononcée ne prendra effet que postérieurement à la décision et, sous réserve des actions contentieuses engagées à la date de la présente décision contre les actes pris sur leur fondement, les effets produits par les dispositions attaquées antérieurement à leur annulation seront réputés définitifs.</ANA>
<ANA ID="9B"> 62-03-02-01 Il résulte des dispositions du I de l'article L. 322-2 du code de la sécurité sociale que la fixation du taux de participation de l'assuré aux tarifs servant de base au calcul des prestations prévues au 1°, 2° et 3° de l'article L. 321-1 du même code ressortit exclusivement à la compétence de l'Union nationale des caisses d'assurance maladie (UNCAM). Par suite, les dispositions introduites par l'article 3 du décret n° 2011-56 du 14 janvier 2011 à l'article R. 322-9-4 du code, prévoyant, en cas de carence de l'UNCAM, une fixation automatique de ce taux, constaté par arrêté du ministre chargé de la santé, en dehors de toute décision de l'UNCAM, sont illégales.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE Section, 18 décembre 2002, Mme Duvignères, n° 233618, p. 463.,,[RJ2] Cf., pour cet effet abrogatif de l'annulation différée, CE, Assemblée, 11 mai 2004, Association AC! et autres, n°s 255886 255887 255888 255889 255890 255891 255892, p. 197, en tant qu'elle statue sur les arrêtés d'agrément relatifs à la convention d'assurance-chômage du 1er janvier 2001 (article 5 du dispositif).</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
