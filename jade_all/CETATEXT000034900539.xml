<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034900539</ID>
<ANCIEN_ID>JG_L_2017_06_000000400530</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/90/05/CETATEXT000034900539.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème - 7ème chambres réunies, 09/06/2017, 400530, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-06-09</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>400530</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème - 7ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Louise Bréhier</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Xavier Domino</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:400530.20170609</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, un mémoire en réplique et un nouveau mémoire, enregistrés le 9 juin 2016 et les 6 février et 6 mars 2017 au secrétariat du contentieux du Conseil d'Etat, l'association professionnelle Transport i Logistyka Polska demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret n° 2016-418 du 7 avril 2016 adaptant le titre VI du livre II de la première partie du code du travail aux entreprises de transport détachant des salariés roulants ou navigants sur le territoire national et modifiant le code des transports en tant qu'il concerne les entreprises de transport routier ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le traité sur le fonctionnement de l'Union européenne ; <br/>
              - la directive n° 96/71/CE du Parlement européen et du Conseil du 16 décembre 1996 ; <br/>
              - le règlement n° 1071/2009 du Parlement européen et du Conseil du 21 octobre 2009 ; <br/>
              - le règlement n° 1072/2009 du Parlement européen et du Conseil du 21 octobre 2009 ; <br/>
              - la directive n° 2014/67/UE du Parlement européen et du Conseil du 15 mai 2014 ;<br/>
              - le code des transports ;<br/>
              - le code du travail ;<br/>
              - la loi n° 2014-790 du 10 juillet 2014 ; <br/>
              - la loi n° 2015-990 du 6 août 2015 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Louise Bréhier, auditrice, <br/>
<br/>
              - les conclusions de M. Xavier Domino, rapporteur public.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1.	Considérant que le titre VI du livre II de la première partie du  code du travail, relatif  aux salariés détachés temporairement par une entreprise non établie en France, fixe les conditions de détachement ainsi que la réglementation et les modalités de contrôle et de répression applicables ; que ces dispositions ont été adaptées au secteur des transports par les articles L. 1331-1 et suivants du code des transports issus de l'article 281 de la loi du 6 août 2015 pour la croissance, l'activité et l'égalité des chances économiques ; qu'en application de ces articles, le décret du 7 avril 2016 adaptant le titre VI du livre II de la première partie du code du travail aux entreprises de transport détachant des salariés roulants ou navigants sur le territoire national et modifiant le code des transports, dont le syndicat Transport i logistyka Polska demande l'annulation pour excès de pouvoir, a notamment instauré une attestation de détachement, prévu l'obligation pour les entreprises du secteur des transports non établies en France de tenir les documents relatifs à la prestation de transport en cause à la disposition des agents de contrôle français et fixé à la durée du détachement suivie d'une période de dix-huit mois la durée pendant laquelle le représentant de l'entreprise sur le territoire national devra assurer la liaison avec les agents chargés de l'inspection du travail ;<br/>
<br/>
              Sur la légalité externe : <br/>
<br/>
              2.	Considérant, en premier lieu, que le décret attaqué a été signé par le Premier ministre et contresigné par les ministres chargés de son exécution ; que, par suite, le moyen tiré de ce  qu'il ne serait pas revêtu de ces signatures doit être écarté ;<br/>
<br/>
              3.	Considérant, en second lieu, que la circonstance que le décret attaqué ne vise  pas le règlement CE n° 1072/2009 du Parlement européen et du Conseil du 21 octobre 2009 est sans influence sur sa légalité. <br/>
<br/>
              Sur la légalité interne : <br/>
<br/>
              4.	Considérant qu'aux termes de l'article 56 du traité sur le fonctionnement de l'Union européenne : " (...) les restrictions à la libre prestation des services à l'intérieur de l'Union sont interdites à l'égard des ressortissants des États membres établis dans un État membre autre que celui destinataire de la prestation (...) " ; que la directive 96/71/CE concernant le détachement des travailleurs effectué dans le cadre d'une prestation de service précise les conditions de travail et d'emploi pour lesquelles  les États membres doivent  veiller à ce que les entreprises qui détachent des travailleurs sur leur territoire garantissent à ces derniers le respect des conditions prévues par l'État membre sur le territoire duquel le travail est exécuté ; que le 1 de l'article 9 " exigences administratives et mesures de contrôle " de la directive du Parlement européen et du Conseil du 15 mai 2014 relative à l'exécution de la directive 96/71/CE prévoit que " Les États membres ne peuvent imposer que les exigences administratives et les mesures de contrôle nécessaires aux fins du contrôle effectif du respect des obligations énoncées par la présente directive et la directive 96/71/CE, pour autant que celles-ci soient justifiées et proportionnées, conformément au droit de l'Union. (...) " ;<br/>
<br/>
              5.	Considérant que le syndicat requérant critique au regard du droit de l'Union les dispositions de l'article L. 1331-1 du code des transports et celles des articles R. 1331-1 à R. 1331-11 du même code issues du décret attaqué en ce qu'elles imposent aux entreprises de transport qui détachent des salariés roulants des formalités et des contraintes qui constitueraient des restrictions prohibées à la libre prestation des services ; <br/>
<br/>
              En ce qui concerne l'attestation de détachement :<br/>
<br/>
              6.	Considérant, en premier lieu, que l'article R. 1331-2 du code des transports issu de l'article 1er du décret attaqué prévoit que les entreprises qui détachent des salariés roulants doivent établir une attestation de détachement qui se substitue à la déclaration de détachement prévue à l'article L. 1262-12 du code du travail ; que l'article R. 1331-7 du même code précise que l'attestation est établie en deux exemplaires, dont l'un est remis au salarié et conservé à bord du moyen de transport et l'autre est détenu, selon le cas, soit par le représentant de l'entreprise sur le territoire national soit par l'entreprise utilisatrice du salarié détaché ; que les informations qui doivent figurer sur cette attestation correspondent à celles qui sont énumérées par le a) du 1 de l'article 9 de la directive 2014/67/UE qui impose de procéder à une déclaration de détachement auprès de l'autorité nationale compétente dans la langue officielle de l'État d'accueil ainsi qu'aux références d'immatriculation au registre électronique national des entreprises de transport par route prévu à l'article 16 du règlement (CE) n° 1071/2009 ; que l'attestation de détachement n'est soumise à aucune formalité d'accusé de réception, d'acceptation ou de vérification par les autorités françaises ; qu'ainsi elle ne constitue pas une autorisation préalable au détachement de travailleurs sur le sol français prohibée par le droit de l'Union ; que, par suite, le moyen tiré de ce que, en instaurant un régime d'autorisation préalable de détachement, l'article L. 1331-1 du code des transports et le décret attaqué porteraient atteinte au principe de libre prestation des services ne peut qu'être écarté ; <br/>
<br/>
              7.	Considérant, en deuxième lieu, que si, aux termes du 2 de l'article 9 de la directive 2014/67/UE : " d'autres exigences administratives et mesures de contrôle [peuvent être introduites] au cas où surviendraient des circonstances ou des éléments nouveaux dont il ressortirait que les exigences administratives et mesures de contrôle qui existent ne sont pas suffisantes ou efficaces pour permettre le contrôle effectif du respect des obligations énoncées dans la directive 96/71/CE et la présente directive, pour autant qu'elles soient justifiées et proportionnées ", il ressort des termes même du décret attaqué que, contrairement à ce que soutient l'association requérante, le dispositif d'attestation qu'il instaure ne comporte aucune exigence administrative ou mesure de contrôle autre que celles que prévoit l'article 1 de l'article 9 de la directive ; que, par suite, le moyen tiré de ce que le décret attaqué instaurerait un régime qui n'est pas justifié par des circonstances nouvelles au sens du 2 de l'article 9 de la directive 2014/67/UE ne peut, en tout état de cause, qu'être écarté ; <br/>
<br/>
              En ce qui concerne l'obligation de désigner un représentant de l'entreprise sur le sol national : <br/>
<br/>
              8.	Considérant, en troisième lieu, que le 1 de l'article 9 de la directive 2014/67/UE dispose que les États membres peuvent notamment imposer aux entreprises qui détachent des salariés sur leur sol " (...) b) l'obligation de conserver ou de fournir, sur support papier ou en format électronique, le contrat de travail, ou tout document équivalent (...), les fiches de paie, les relevés d'heures indiquant le début, la fin et la durée du travail journalier et les preuves de paiement des salaires  (...) / c) l'obligation de fournir les documents visés au point b) après la période de détachement, à la demande des autorités de l'État d'accueil, pendant une période raisonnable ; / d) l'obligation de fournir une traduction des documents visés au point b) (...) : e) l'obligation de désigner une personne chargée d'assurer la liaison avec les autorités compétentes dans l'État membre d'accueil dans lequel les services sont fournis et, si nécessaire, de transmettre et de recevoir des documents et/ou des avis " ; que le II de l'article R. 1331-1 du code des transports, qui dispose que : " L'entreprise désigne en ce cas son représentant sur le territoire national en application du II de l'article 1262-2-1 du [code du travail] " et l'article R. 1331-5 du code des transports, qui dispose que : " Pour l'application de l'article R. 1263-2-1 du code du travail, la période pendant laquelle est assurée la liaison entre les agents mentionnés à l'article L. 8271-1-2 du code du travail et le représentant sur le territoire national désigné, en application du II de l'article L. 1262-1 du même code par les entreprises, ne peut être inférieure à la durée du détachement du salarié suivie d'une période de dix-huit mois qui suit l'expiration de celle-ci. " procèdent à la transposition dans l'ordre juridique interne des objectifs de la directive précitée ; qu'au demeurant, le II de l'article R. 1331-1 du code des transports laisse l'entreprise libre de désigner le représentant de son choix sur le territoire national, sans condition de titre ni de qualification ; que cette désignation  n'est pas limitée dans le temps ; qu'il n'est pas exclu que plusieurs entreprises désignent un même représentant ; qu'en outre, cette désignation n'est soumise à aucun formalisme ni à aucune procédure d'acceptation par les autorités françaises ; que, par suite, le moyen tiré de ce qu'en imposant la désignation d'un représentant pendant une durée égale à la durée du détachement augmentée de dix-huit mois, le décret attaqué ferait peser une charge excessive sur les petites et moyennes entreprises de transport qui serait de nature à porter atteinte au principe de libre prestation des services doit être écarté ; <br/>
<br/>
              En ce qui concerne l'obligation de salaire minimal :<br/>
<br/>
              9.	Considérant, en dernier lieu, que le salaire minimal de référence applicable aux travailleurs détachés est déterminé par l'article L. 1264-4 du code du travail, pris pour la transposition de la directive 96/71/CE du 16 décembre 1996, laquelle prévoit à son article 3 que les taux de salaire minimal, y compris les taux majorés pour heures supplémentaires fixés par les dispositions applicables dans l'État sur le territoire duquel le travail est exécuté doivent être garantis aux salariés détachés et que ces taux sont définis par la législation ou la pratique de l'État sur le territoire duquel le travailleur est détaché ; que le décret attaqué se borne à prévoir des formalités déclaratives adaptées au secteur des transports et n'apporte aucune modification aux règles issues du code du travail relatives au salaire minimal ; que, par suite, l'association requérante ne saurait utilement soutenir que le décret attaqué imposerait un mode de calcul du salaire minimal ayant pour effet de restreindre l'accès au marché français des entreprises de transport étrangères ; <br/>
<br/>
              10.	Considérant qu'il résulte de tout ce qui précède, sans qu'il soit besoin de statuer sur la fin de non-recevoir soulevée par la ministre, que l'association professionnelle Transport i Logistyka Polska n'est pas fondée à demander l'annulation pour excès de pouvoir du décret du 7 avril 2016 ; que ses conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative ne peuvent, par suite, qu'être rejetées ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : La requête de l'association professionnelle Transport i Logistyka Polska est rejetée.<br/>
<br/>
Article 2 : La présente décision sera notifiée à l'association Transport i Logistyka Polska, au Premier ministre et au ministre d'Etat, ministre de la transition écologique et solidaire.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
