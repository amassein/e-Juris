<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033551437</ID>
<ANCIEN_ID>JG_L_2016_12_000000387328</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/55/14/CETATEXT000033551437.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème chambre, 02/12/2016, 387328, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-12-02</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>387328</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ODENT, POULET</AVOCATS>
<RAPPORTEUR>M. Vincent Uher</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Romain Victor</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2016:387328.20161202</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société civile immobilière Paricap a demandé au tribunal administratif de Paris la décharge des rappels de taxe sur la valeur ajoutée qui lui ont été réclamés au titre de la période du 1er janvier 2006 au 31 décembre 2008. Par un jugement n° 1309191 du 25 avril 2014, ce tribunal a rejeté cette demande.<br/>
<br/>
              Par un arrêt n° 14PA02716 du 26 novembre 2014, la cour administrative d'appel de Paris a rejeté l'appel formé contre ce jugement par la société Paricap.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un nouveau mémoire, enregistrés les 22 janvier et 14 avril et 15 septembre 2015 au secrétariat du contentieux du Conseil d'Etat, la société Paricap demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat le versement de la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code général des impôts et le livre des procédures fiscales ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Vincent Uher, maître des requêtes en service extraordinaire,<br/>
<br/>
              - les conclusions de M. Romain Victor, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Odent, Poulet, avocat de la SCI Paricap ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond qu'à la suite d'un contrôle sur place, la société Paricap a été assujettie à des rappels de taxe sur la valeur ajoutée au titre de la période du 1er janvier 2006 au 31 décembre 2008. <br/>
<br/>
              2. Après avoir estimé que la procédure d'imposition avait été conduite dans le respect des règles posées par les dispositions du livre des procédures fiscale, la cour en a déduit que le moyen tiré de ce que la charte des droits fondamentaux de l'Union européenne aurait, au cours de cette procédure, été méconnue devait être écarté. En statuant ainsi, la cour n'a pas entaché son arrêt d'insuffisance de motivation sur ce point.<br/>
<br/>
              3. Aux termes de l'article L. 76 B du livre des procédures fiscales : " L'administration est tenue d'informer le contribuable de la teneur et de l'origine des renseignements et documents obtenus de tiers sur lesquels elle s'est fondée pour établir l'imposition faisant l'objet de la proposition prévue au premier alinéa de l'article L. 57 (...). Elle communique, avant la mise en recouvrement, une copie des documents susmentionnés au contribuable qui en fait la demande ".<br/>
<br/>
              4. En premier lieu, pour écarter un moyen tiré de ce que, faute d'avoir fait droit à une demande de communication à la société requérante des relevés bancaires dont l'administration fiscale avait demandé et obtenu communication auprès de l'organisme bancaire teneur de ses comptes, l'administration avait entaché d'irrégularité la procédure d'imposition, la cour administrative d'appel de Paris s'est fondée sur ce que les redressements en litige ne procédaient pas des éléments contenus dans les relevés bancaires en cause mais des documents bancaires consultés durant le contrôle, qui s'est déroulé dans les locaux de l'expert comptable de la société. En jugeant ainsi, la cour, qui a porté sur les faits de l'espèce une appréciation souveraine non arguée de dénaturation, n'a pas méconnu les dispositions précitées de l'article L. 76 B du livre des procédures fiscales, ni entaché son arrêt d'insuffisance de motivation. Si la cour a ajouté, par un motif surabondant, qu'il n'était au demeurant ni soutenu, ni même d'ailleurs allégué que la société contrôlée n'aurait pas été en possession de ses relevés bancaires et ne les aurait pas présentés elle-même au vérificateur, la société requérante ne peut utilement soutenir qu'elle aurait, en statuant ainsi, commis une erreur de droit.<br/>
<br/>
              5. En second lieu, l'administration n'est tenue de communiquer au contribuable, en application de l'article L. 76 B précité du livre des procédures fiscales, une copie des documents qu'elle a obtenus de tiers que dans la mesure où celui-ci, après avoir été informé de leur origine et de leur teneur, lui en a fait la demande. Par suite, le contribuable qui, ayant présenté une première demande de communication, souhaite ensuite obtenir copie de documents obtenus auprès de tiers par l'administration fiscale postérieurement à cette première demande, doit en former une nouvelle. Il en résulte que la cour n'a pas commis d'erreur de droit en jugeant, après avoir relevé par une appréciation souveraine non entachée de dénaturation que la demande de communication de la société, formulée le 27 novembre 2009, ne pouvait concerner des documents demandés par l'administration le 25 janvier 2010, obtenus par elle le 5 février suivant et mentionnés dans la réponse au observations du contribuable du 9 février, que, faute d'une nouvelle demande de la contribuable, l'administration fiscale n'était pas tenue de procéder d'elle-même à la transmission de ces derniers documents.<br/>
<br/>
              6. Il résulte de ce qui précède que le pourvoi de la société Paricap doit être rejeté, y compris ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de la société Paricap est rejeté.<br/>
Article 2 : La présente décision sera notifiée à la société civile immobilière Paricap et au ministre de l'économie et des finances.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
