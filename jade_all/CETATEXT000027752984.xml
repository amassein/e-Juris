<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027752984</ID>
<ANCIEN_ID>JG_L_2013_07_000000361196</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/75/29/CETATEXT000027752984.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème sous-section jugeant seule, 25/07/2013, 361196, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-07-25</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>361196</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BARADUC, DUHAMEL</AVOCATS>
<RAPPORTEUR>Mme Isabelle Lemesle</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Delphine Hedary</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2013:361196.20130725</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>		VU LA PROCEDURE SUIVANTE :<br/>
<br/>
<br/>
 Procédure contentieuse antérieure<br/>
<br/>
              M. A...B...a demandé au tribunal administratif de Marseille d'annuler la décision du 24 novembre 2011 par laquelle le directeur interrégional des services pénitentiaires de Provence-Alpes-Côte d'Azur-Corse lui a retiré le poste d'auxiliaire bibliothécaire qu'il occupait au centre de détention de Salon-de-Provence. Il s'est ensuite désisté de sa requête, ce dont le tribunal administratif lui a donné acte par ordonnance du 7 février 2012.<br/>
<br/>
              Le président de la cinquième chambre de la cour administrative d'appel de Marseille a rejeté, par ordonnance du 23 mai 2012, la requête par laquelle M. B...lui demandait d'une part, de réformer, pour vice de consentement, l'ordonnance du 7 février 2012, d'autre part, d'annuler la décision de l'administration pénitentiaire du 24 novembre 2011 et d'enjoindre à cette dernière, sous astreinte, de le réintégrer dans ses fonctions d'auxiliaire bibliothécaire.<br/>
<br/>
<br/>
 Procédure devant le Conseil d'Etat<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 19 juillet et 2 novembre 2012 au secrétariat du contentieux du Conseil d'Etat, M.B..., représenté par la SCP Baraduc et Duhamel, demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'ordonnance n° 12MA01323 du 23 mai 2012 du président de la cinquième chambre de la cour administrative d'appel de Marseille ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 000 euros à verser à son avocat au titre des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991.<br/>
<br/>
              Par un mémoire enregistré le 24 mai 2013 au secrétariat du contentieux du Conseil d'Etat, la garde des sceaux, ministre de la justice déclare s'en remettre à la sagesse du Conseil d'Etat.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Isabelle Lemesle, Maître des Requêtes,  <br/>
<br/>
              - les conclusions de Mme Delphine Hedary, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Baraduc, Duhamel, avocat de M. B...;<br/>
<br/>
<br/>
<br/>
<br/>			CONSIDERANT CE QUI SUIT :<br/>
<br/>
<br/>
              1. Aux termes de l'article R. 636-1 du code de justice administrative : " Le désistement peut être fait et accepté par des actes signés des parties ou de leurs mandataires et adressés au greffe. / Il est instruit dans les formes prévues pour la requête ".<br/>
<br/>
              2. Un désistement a, en principe, le caractère d'un désistement d'instance. Il n'en va autrement que si le caractère de désistement d'action résulte sans aucune ambiguïté des écritures du requérant. Lorsque le dispositif de la décision de justice qui donne acte d'un désistement ne comporte aucune précision sur la nature du désistement dont il est donné acte, ce désistement doit être regardé comme un désistement d'instance.<br/>
<br/>
              3. Il résulte de ce qui précède que l'ordonnance attaquée, qui a regardé le désistement dont l'ordonnance du président de la cinquième chambre de la cour administrative d'appel de Marseille donnait acte comme un désistement d'action, alors que son dispositif ne comportait aucune précision en ce sens, est entachée d'erreur de droit.<br/>
<br/>
              4. Sans qu'il soit besoin d'examiner les autres moyens du pourvoi tirés de l'insuffisance de motivation de l'ordonnance attaquée et de la dénaturation des pièces du dossier dont elle serait entachée, M. B...est dès lors fondé à demander l'annulation de l'ordonnance du 23 mai 2012 du président de la cinquième chambre de  la cour administrative d'appel de Marseille ;<br/>
<br/>
              5. M. B...a obtenu le bénéfice de l'aide juridictionnelle. Par suite, son avocat peut se prévaloir des dispositions des articles L. 761-1 du cde de justice administrative et 37 de la loi du 10 juillet 1991. Il y a donc lieu, dans les circonstances de l'espèce, et sous réserve que la SCP Baraduc et Duhamel, avocat de M.B..., renonce à percevoir la somme correspondant à la part contributive de l'Etat, de mettre à la charge de l'Etat la somme de 3 000 euros à verser cet avocat ;<br/>
<br/>
<br/>
<br/>				D E C I D E  :<br/>
              				--------------<br/>
<br/>
Article 1er : L'ordonnance du 23 mai 2012 du président de la cinquième chambre de la cour administrative d'appel de Marseille est annulée.<br/>
<br/>
 Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Marseille.<br/>
<br/>
Article 3 : L'Etat versera à la SCP Baraduc et Duhamel, avocat de M.B..., une somme de 3 000 euros en application des dispositions du deuxième alinéa de l'article 37 de la loi du 10 juillet 1991, sous réserve que cette société renonce à percevoir la somme correspondant à la part contributive de l'Etat.<br/>
<br/>
Article 4 : La présente décision sera notifiée à M. A...B...et à la garde des sceaux, ministre de la justice.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
