<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027150944</ID>
<ANCIEN_ID>JG_L_2013_03_000000364462</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/15/09/CETATEXT000027150944.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème et 1ère sous-sections réunies, 08/03/2013, 364462, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-03-08</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>364462</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème et 1ère sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Jean-Baptiste de Froment</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Suzanne von Coester</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:364462.20130308</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu l'ordonnance n°12PA02598 du 10 décembre 2012, enregistrée le 12 décembre 2012 au secrétariat du contentieux du Conseil d'Etat, par laquelle le président de la première chambre de la cour administrative d'appel de Paris, avant qu'il soit statué sur l'appel de l'association Réseau " Sortir du Nucléaire " et autres tendant à l'annulation de la décision du 22 octobre 2010 par laquelle le ministre de l'écologie, de l'énergie, du développement durable et de la mer a autorisé la société TN International à exécuter un transport de matières nucléaires de catégorie III de Valognes à Gorleben (Allemagne), a décidé, par application des dispositions de l'article 23-2 de l'ordonnance n°58-1067 du 7 novembre 1958, de transmettre au Conseil d'Etat la question de la conformité aux droits et libertés garantis par la Constitution du I de l'article L.542-2-1 du code de l'environnement ;<br/>
<br/>
              Vu le mémoire, enregistré le 16 juillet 2012 au greffe de la cour administrative d'appel de Paris, présenté par l'association Réseau " Sortir du Nucléaire ", représentée par son coordinateur général, dont le siège est 9, rue Dumenge à Lyon Cedex 04 (69317), en application de l'article 23-1 de l'ordonnance n°58-1067 du 7 novembre 1958 ; <br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 26 février 2013, présentée par l'association Réseau " Sortir du Nucléaire " ;<br/>
<br/>
              Vu la Constitution, notamment son article 61-1 ;<br/>
<br/>
              Vu l'ordonnance n°58-1067 du 7 novembre 1958 ;<br/>
<br/>
              Vu le code de l'environnement, notamment son article L. 542-2-1 ;<br/>
<br/>
              Vu le décret n°2008-1369 du 19 décembre 2008 ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Baptiste de Froment, Maître des Requêtes, <br/>
<br/>
              - les conclusions de Mme Suzanne von Coester, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il résulte des dispositions de l'article 23-4 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel que, lorsqu'une juridiction relevant du Conseil d'Etat a transmis à ce dernier, en application de l'article 23-2 de cette même ordonnance, la question de la conformité à la Constitution d'une disposition législative, le Conseil constitutionnel est saisi de cette question de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux ;<br/>
<br/>
              2. Considérant que la question prioritaire de constitutionnalité transmise par la cour administrative d'appel de Paris a été soulevée par l'association Réseau " Sortir du Nucléaire " à l'occasion d'un litige né de la contestation de la décision du 22 octobre 2010 par laquelle le ministre chargé de l'énergie a autorisé la société TN International à exécuter un transport de matières nucléaires de catégorie III entre Valognes (France) et Gorleben (Allemagne) ;<br/>
<br/>
              3. Considérant qu'un accord sous forme d'échange de lettres entre le gouvernement de la République française et le gouvernement de la République fédérale d'Allemagne relatif au transport de colis de déchets radioactifs provenant du retraitement de combustibles irradiés a été signé à Paris les 20 et 28 octobre 2008 et publié par le décret du 19 décembre 2008 ; qu'aux termes de cet accord, des combustibles irradiés issus de centrales nucléaires allemandes devaient être importés en France en vue d'être retraités dans l'usine française de retraitement de La Hague et les déchets radioactifs issus du retraitement en France de ces combustibles nucléaires irradiés être ensuite retournés en Allemagne ; que l'ensemble des modalités pratiques relatives à la réalisation de ces transports devaient être décidées par un groupe de travail franco-allemand, qui arrêterait notamment la date des transports et leur réalisation ; que, dans ces conditions, la décision du ministre chargé de l'énergie du 22 octobre 2010 qui met en oeuvre les décisions de ce groupe de travail bilatéral relatives au transport en Allemagne des déchets nucléaires traités en France a pour fondement l'accord franco-allemand et non les dispositions du I de l'article L. 542-2-1 du code de l'environnement, qui précisent les conditions dans lesquelles des combustibles usés ou des déchets radioactifs peuvent être introduits sur le territoire national ; que, par suite, ces dispositions législatives ne sauraient être regardées comme applicables au litige au sens et pour l'application des dispositions de l'article 23-4 de l'ordonnance du 7 novembre 1958 ;<br/>
<br/>
              4. Considérant qu'il résulte de ce qui précède qu'il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité invoquée ; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              			D E C I D E :<br/>
              			--------------<br/>
Article 1er : Il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité transmise par la cour administrative d'appel de Paris.<br/>
Article 2 : La présente décision sera notifiée à l'association Réseau " Sortir du Nucléaire " et à la ministre de l'écologie, du développement durable et de l'énergie. <br/>
Copie en sera adressée au Conseil constitutionnel et à la cour administrative d'appel de Paris. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
