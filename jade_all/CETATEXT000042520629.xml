<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042520629</ID>
<ANCIEN_ID>JG_L_2020_11_000000430378</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/52/06/CETATEXT000042520629.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème - 8ème chambres réunies, 13/11/2020, 430378</TITRE>
<DATE_DEC>2020-11-13</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>430378</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème - 8ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP THOUVENIN, COUDRAY, GREVY ; SCP CELICE, TEXIDOR, PERIER</AVOCATS>
<RAPPORTEUR>M. Thomas Janicot</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Laurent Cytermann</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:430378.20201113</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B... A... a demandé au tribunal administratif de Lyon de condamner le service départemental et métropolitain d'incendie et de secours (SDMIS) du Rhône à lui verser, à titre principal, les indemnités horaires pour travaux supplémentaires correspondant aux heures supplémentaires qu'il a effectuées au-delà du seuil annuel de 1 607 heures en 2010 et 2011 ou, à titre subsidiaire, aux heures supplémentaires effectuées en 2010 et 2011 au-delà du seuil de 44 heures hebdomadaires en moyenne par période de quatre mois ou, à titre encore subsidiaire, une indemnité représentative des indemnités horaires pour travaux supplémentaires correspondant à ces heures supplémentaires. Il a également demandé la condamnation du SDMIS à lui verser une somme de 3 000 euros au titre de ses préjudices personnels et de ses troubles dans ses conditions d'existence.<br/>
<br/>
              Par un jugement n° 1303687 du 6 décembre 2016, le magistrat désigné par le président du tribunal administratif de Lyon a condamné le service départemental et métropolitain d'incendie et de secours du Rhône à verser à M. A... une somme de 9 000 euros et a rejeté le surplus des conclusions des parties. <br/>
<br/>
              Par une requête, enregistrée au secrétariat du contentieux du Conseil d'Etat le 3 février 2017, M. A... s'est pourvu contre ce jugement. Par une ordonnance du 21 mars 2017, le président de la section du contentieux du Conseil d'Etat a attribué le jugement de la requête de M. A... à la cour administrative d'appel de Lyon.<br/>
<br/>
              Par une requête enregistrée au secrétariat du contentieux du Conseil d'Etat le 6 février 2017, le SDMIS du Rhône s'est également pourvu contre ce jugement. Par une ordonnance du 21 mars 2017, le président de la section du contentieux du Conseil d'Etat a attribué le jugement de la requête à la cour administrative d'appel de Lyon.<br/>
<br/>
              Par un arrêt n° 17LY01230, 17LY01433 du 5 mars 2019, la cour administrative d'appel de Lyon, après avoir joints les requêtes de M. A... et du SDMIS du Rhône, a annulé le jugement du magistrat désigné par le président du tribunal administratif de Lyon, condamné le SDMIS du Rhône à verser à M. A... une somme correspondant à la rémunération de 240 heures supplémentaires effectuées en 2010 et de 270 heures supplémentaires effectuées en 2011, renvoyé ce dernier devant le SDMIS du Rhône pour la liquidation de cette somme et rejeté le surplus des conclusions des parties.<br/>
<br/>
              Par un pourvoi, un mémoire complémentaire et un mémoire en réplique, enregistrés les 6 mai et 15 juillet 2019 et 14 janvier 2020 au secrétariat du contentieux du Conseil d'Etat, M. A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt en tant qu'il n'a pas fait droit à ses conclusions ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire entièrement droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge du SDMIS du Rhône la somme de 1 000 euros au titre des dispositions des articles L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - la directive 2003/88/CE du Parlement européen et du Conseil du 4 novembre 2003 ;<br/>
              - la loi n° 84-53 du 26 janvier 1984 ;<br/>
              - le décret n° 2000-815 du 25 août 2000 ;<br/>
              - le décret n° 2001-623 du 12 juillet 2001 ;<br/>
              - le décret n° 2001-1382 du 31 décembre 2001 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Thomas Janicot, auditeur,  <br/>
<br/>
              - les conclusions de M. Laurent Cytermann, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Thouvenin, Coudray, Grevy, avocat de M. B... A... et à la SCP Célice, Texidor, Perier, avocat du service départemental et métropolitain d'incendie et de secours du Rhône ;<br/>
<br/>
<br/>
              Vu la note en délibéré, enregistrée le 22 octobre 2020, présentée par M. A... ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que M. A..., sapeur-pompier professionnel, a demandé au tribunal administratif de Lyon de condamner le service départemental et métropolitain d'incendie et de secours (SDMIS) du Rhône à lui verser, à titre principal, les indemnités horaires pour travaux supplémentaires correspondant aux heures supplémentaires qu'il soutient avoir accomplies en 2010 et en 2011 au-delà du seuil annuel de 1 607 heures, à titre subsidiaire, les heures supplémentaires effectuées en 2010 et 2011 au-delà du seuil de 44 heures hebdomadaires en moyenne par période de quatre mois, à titre encore subsidiaire, une indemnité représentative des indemnités horaires pour travaux supplémentaires correspondant à ces heures supplémentaires et, en tout état de cause, une indemnité réparant ses préjudices personnels et ses troubles dans ses conditions d'existence en raison du régime illégal de la durée du travail à laquelle il a été assujetti. Par un jugement du 6 décembre 2016, le magistrat désigné par le président du tribunal administratif de Lyon a condamné le SDMIS du Rhône à verser à M. A... une indemnité de 9 000 euros en réparation du préjudice qui a résulté pour lui de l'accomplissement d'heures supplémentaires non rémunérées en 2010 et 2011 et a rejeté le surplus des conclusions de sa demande. M. A... se pourvoit en cassation, en tant qu'il n'a pas fait complètement droit à ses conclusions, contre l'arrêt du 5 mars 2019 par lequel, après avoir annulé ce jugement, la cour administrative d'appel de Lyon a condamné le SDMIS du Rhône à verser à M. A... une somme correspondant à la rémunération de 240 heures supplémentaires effectuées en 2010 et de 270 heures supplémentaires effectuées en 2011, renvoyé ce dernier devant le SDMIS du Rhône pour la liquidation de cette somme et rejeté le surplus des conclusions des parties. Par la voie du pourvoi incident, le SDMIS du Rhône demande l'annulation du même arrêt en tant qu'il l'a condamné à verser à M. A... une somme correspondant à la rémunération de 240 heures supplémentaires effectuées en 2010 et de 270 heures supplémentaires effectuées en 2011 et renvoyé ce dernier devant lui pour la liquidation de cette somme.<br/>
<br/>
              Sur le pourvoi de M. A... :<br/>
<br/>
              En ce qui concerne l'arrêt en tant qu'il se prononce sur le paiement des heures supplémentaires :<br/>
<br/>
              2. En premier lieu, aux termes de l'article 1er du décret du 25 août 2000 relatif à l'aménagement de la réduction de temps de travail dans la fonction publique de l'Etat, rendu applicable aux agents des collectivités territoriales par l'article 1er du décret du 12 juillet 2001 pris pour l'application de l'article 7-1 de la loi du 26 janvier 1984 et relatif à l'aménagement et à la réduction du temps de travail dans la fonction publique territoriale : " (...) Le décompte du temps de travail est réalisé sur la base d'une durée annuelle de travail effectif de 1 607 heures maximum, sans préjudice des heures supplémentaires susceptibles d'être effectuées ". Aux termes de l'article 2 du même décret : " La durée du travail effectif s'entend comme le temps pendant lequel les agents sont à la disposition de leur employeur et doivent se conformer à ses directives sans pouvoir vaquer librement à des occupations personnelles ". Aux termes de l'article 1er du décret du 31 décembre 2001 relatif au temps de travail des sapeurs-pompiers professionnels, dans sa rédaction applicable au présent litige : " La durée de travail effectif des sapeurs-pompiers professionnels est définie conformément à l'article 1er du décret du 25 août 2000 susvisé auquel renvoie le décret du 12 juillet 2001 susvisé (...) ". Aux termes de l'article 2 du même décret : " La durée de travail effectif journalier définie à l'article 1er ne peut pas dépasser 12 heures consécutives (...) ". Aux termes de l'article 3 du même décret : " Compte tenu des missions des services d'incendie et de secours et des nécessités de service, un temps de présence supérieur à l'amplitude journalière prévue à l'article 2 peut être fixé à 24 heures consécutives par le conseil d'administration du service départemental d'incendie et de secours après avis du comité technique paritaire (...) ". Enfin, aux termes de l'article 4 du même décret : " Lorsqu'il est fait application de l'article 3 ci-dessus, une délibération du conseil d'administration après avis du comité technique paritaire fixe un temps d'équivalence au décompte annuel du temps de travail. / La durée équivalente ne peut être inférieure à 2 280 heures ni excéder 2 520 heures. / A compter du 1er janvier 2005, elle ne peut être inférieure à 2 160 heures ni excéder 2 400 heures ".<br/>
<br/>
              3. Le régime d'horaire d'équivalence constituant un mode particulier de comptabilisation du travail effectif qui consiste à prendre en compte la totalité des heures de présence, tout en leur appliquant un mécanisme de pondération tenant à la moindre intensité du travail fourni pendant les périodes d'inaction, seules peuvent ouvrir droit à un complément de rémunération les heures de travail effectif réalisées par les sapeurs-pompiers au-delà du temps d'équivalence au décompte annuel du temps de travail fixé, dans les limites prévues par l'article 4 du décret du 31 décembre 2001, par le conseil d'administration du service départemental d'incendie et de secours. Le dépassement des durées maximales de travail prévues tant par le droit de l'Union européenne que par le droit national ne peut ouvrir droit par lui-même qu'à l'indemnisation des préjudices résultant de l'atteinte à la santé et à la sécurité ainsi que des troubles subis dans les conditions d'existence. Par suite, en jugeant que la totalité du temps de présence des sapeurs-pompiers, si elle ne doit pas dépasser les limites fixées par la directive 2003/88/CE du Parlement européen et du Conseil du 4 novembre 2003 concernant certains aspects de l'aménagement du temps de travail, ne peut pas être assimilée à du temps de travail effectif pour l'appréciation des heures supplémentaires devant être rémunérées lorsque, comme en l'espèce, le conseil d'administration du service a institué un régime dérogatoire sur le fondement des dispositions des articles 3 et 4 du décret du 31 décembre 2001 cités au point 2, la cour administrative d'appel n'a pas commis d'erreur de droit.<br/>
<br/>
              4. En deuxième lieu, après avoir relevé, par une appréciation souveraine exempte de dénaturation, que l'activité professionnelle de M. A... avait, au cours des années 2010 et 2011, été organisée selon des cycles comportant des gardes de 24 heures, la cour a pu en déduire, sans commettre d'erreur de droit, que l'intéressé n'était pas soumis au régime de droit commun de gardes de 12 heures avec une durée annuelle de 1 607 heures prévu par la délibération du conseil d'administration du SDIS du Rhône du 11 janvier 2002 mais au régime dérogatoire, prévu par cette même délibération et en faveur duquel les agents étaient libre d'opter, de gardes de 24 heures avec une durée annuelle de 2 240 heures. Par ailleurs, le moyen tiré de ce qu'en l'absence de mesures adéquates de compensation ou de protection, ce régime dérogatoire serait contraire au droit communautaire n'a pas été soulevé devant la cour administrative d'appel et ne présente pas le caractère d'un moyen d'ordre public. Ce moyen, nouveau en cassation, est inopérant et ne peut qu'être écarté.<br/>
<br/>
              5. En troisième lieu, aux termes de l'article 3 du décret du 25 août 2000 relatif à l'aménagement et à la réduction du temps de travail dans la fonction publique de l'Etat et dans la magistrature : " I.- L'organisation du travail doit respecter les garanties minimales ci-après définies./ La durée hebdomadaire du travail effectif, heures supplémentaires comprises, ne peut excéder ni quarante-huit heures au cours d'une même semaine, ni quarante-quatre heures en moyenne sur une période quelconque de douze semaines consécutives et le repos hebdomadaire, comprenant en principe le dimanche, ne peut être inférieur à trente-cinq heures. / La durée quotidienne du travail ne peut excéder dix heures. (...) / II.- Il ne peut être dérogé aux règles énoncées au I que dans les cas et conditions ci-après : / a) Lorsque l'objet même du service public en cause l'exige en permanence, notamment pour la protection des personnes et des biens, par décret en Conseil d'Etat, pris après avis du comité d'hygiène et de sécurité le cas échéant, du comité technique ministériel et du Conseil supérieur de la fonction publique, qui détermine les contreparties accordées aux catégories d'agents concernés ; / (...) ". Aux termes de l'article 3 du décret du 12 juillet 2001 pris pour l'application de l'article 7-1 de la loi n° 84-53 du 26 janvier 1984 et relatif à l'aménagement et à la réduction du temps de travail dans la fonction publique territoriale : " Le décret en Conseil d'Etat prévu pour l'application du a du II de l'article 3 du décret du 25 août 2000 susvisé est pris après avis du Conseil supérieur de la fonction publique territoriale. " Si les dispositions du décret du 31 décembre 2001 relatif au temps de travail des sapeurs-pompiers professionnels citées au point 2, ont dérogé, selon la procédure susmentionnée, à la durée quotidienne maximale de travail, il n'en va pas de même vis-à-vis de la règle limitant la durée hebdomadaire du travail effectif des fonctionnaires territoriaux, heures supplémentaires comprises, à quarante-quatre heures en moyenne sur une période quelconque de douze semaines consécutives. Ce plafond doit, toutefois, être apprécié, après application du coefficient d'équivalence retenu pour les gardes de 24 heures. Dès lors qu'il est constant que le coefficient d'équivalence retenu par la délibération du conseil d'administration du SDIS du Rhône du 11 janvier 2002 pour les gardes de 24 heures était de 1,5, la cour administrative d'appel n'a pas commis d'erreur de droit en jugeant que le calcul proposé par M. A... ne permettait pas d'établir que le " régime de travail dérogatoire en 24 heures " prévu par cette délibération, qui comportait 90 gardes de 24 heures, plus 2 semaines de 5 jours de 8 heures méconnaissait le plafond de 44 heures en moyenne sur une période quelconque de 12 semaines consécutives.<br/>
<br/>
              6. En quatrième lieu, il résulte de l'arrêt attaqué que la cour administrative d'appel a jugé que M. A... ne pouvait utilement soutenir que le régime de la durée du travail des sapeurs-pompiers bénéficiaires d'un logement fixé par les dispositions du II de la délibération du conseil d'administration du SDIS du 26 juin 2009, qui avait été déclaré illégal par un arrêt devenu définitif de la même cour du 8 janvier 2013, serait contraire au principe de non-discrimination prévu par les stipulations de l'article 14 de la convention européenne de sauvegarde des droits de l'homme. Dès lors, le moyen du pourvoi tiré de ce que la cour administrative d'appel aurait commis une erreur de droit en jugeant que M. A... ne pouvait utilement se prévaloir d'une méconnaissance de ce principe de non-discrimination par le " régime de travail dérogatoire en 24 heures " fixé par le II de la délibération du conseil d'administration du SDIS du 11 janvier 2002 est inopérant et ne peut qu'être écarté.<br/>
<br/>
              En ce qui concerne l'arrêt en tant qu'il se prononce sur l'indemnisation des préjudices personnels et des troubles dans les conditions d'existence :<br/>
<br/>
              7. Le dépassement de la durée maximale de travail prévue tant par le droit de l'Union européenne que par le droit national est susceptible de porter atteinte à la sécurité et à la santé des intéressés en ce qu'il les prive du repos auquel ils ont droit et peut leur causer, de ce seul fait, un préjudice indépendamment de leurs conditions de rémunération ou d'hébergement. Par suite, en rejetant la demande d'indemnisation de M. A... présentée au titre des préjudices personnels et des troubles dans les conditions d'existence subis en raison de ce qu'il estimait être de tels dépassements au motif que la rémunération des heures supplémentaires qui lui était accordée réparait l'intégralité du préjudice subi, la cour administrative d'appel a commis une erreur de droit.<br/>
<br/>
              En ce qui concerne l'arrêt en tant qu'il se prononce sur la demande de versement des intérêts :<br/>
<br/>
              8. Il ressort des pièces du dossier soumis aux juges du fond que M. A... a conclu, dans son pourvoi en cassation dirigé contre le jugement du tribunal administratif de Lyon du 6 décembre 2016, transmis à la cour administrative d'appel de Lyon par une ordonnance du 21 mars 2017 du président de la section du contentieux du Conseil d'Etat, prise en application de l'article R. 351-1 du code de justice administrative, à ce que le service départemental métropolitain d'incendie et de secours du Rhône soit condamné à lui verser les intérêts dus sur l'indemnité qu'il demandait, ainsi que les intérêts capitalisés. La cour administrative d'appel, en omettant de statuer sur ce chef de conclusions, auquel M. A... n'avait pas explicitement renoncé, a entaché son arrêt d'irrégularité.<br/>
<br/>
              9. Il résulte de tout ce qui précède que M. A... n'est fondé à demander l'annulation de l'arrêt qu'il attaque qu'en tant qu'il se prononce sur ses conclusions relatives à l'indemnisation de ses préjudices personnels et des troubles dans les conditions de l'existence et en tant qu'il a omis de se prononcer sur la demande de versement des intérêts et des intérêts capitalisés.<br/>
<br/>
              Sur le pourvoi incident du service départemental métropolitain d'incendie et de secours du Rhône<br/>
<br/>
              10. Le service départemental métropolitain d'incendie et de secours du Rhône s'est désisté de son pourvoi incident. Ce désistement étant pur et simple, rien ne s'oppose à ce qu'il en soit donné acte.<br/>
<br/>
              Sur les conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              11. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge du service départemental métropolitain d'incendie et de secours du Rhône le versement à M. A... d'une somme de 1 000 euros au titre de l'article L. 761-1 du code de justice administrative. Les dispositions de cet article font en revanche obstacle à ce qu'il soit fait droit aux conclusions présentées à ce titre par le service départemental métropolitain d'incendie et de secours du Rhône.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : Il est donné acte du désistement du pourvoi incident du service départemental métropolitain d'incendie et de secours du Rhône.<br/>
<br/>
Article 2 : L'arrêt du 5 mars 2019 de la cour administrative d'appel de Lyon est annulé en tant qu'il se prononce sur les conclusions de M. A... relatives à l'indemnisation de ses préjudices personnels et des troubles dans les conditions d'existence et en tant qu'il a omis de se prononcer sur la demande de versement des intérêts et des intérêts capitalisés.<br/>
<br/>
Article 3 : L'affaire est renvoyée, dans cette mesure, à la cour administrative d'appel de Lyon.<br/>
<br/>
Article 4 : Le service départemental métropolitain d'incendie et de secours du Rhône versera à M. A... une somme de 1 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 5 : Les conclusions du service départemental métropolitain d'incendie et de secours du Rhône présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
<br/>
Article 6 : Le surplus des conclusions du pourvoi de M. A... est rejeté.<br/>
<br/>
Article 7 : La présente décision sera notifiée à M. A... et au service départemental et métropolitain d'incendie et de secours du Rhône.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">135-01-04-02-03 COLLECTIVITÉS TERRITORIALES. DISPOSITIONS GÉNÉRALES. SERVICES PUBLICS LOCAUX. DISPOSITIONS PARTICULIÈRES. SERVICES D'INCENDIE ET SECOURS. - HEURES SUPPLÉMENTAIRES - CAS DU RÉGIME D'HORAIRE D'ÉQUIVALENCE (DÉCRET N° 2001-1382 DU 31 DÉCEMBRE 2001) - 1) DÉPASSEMENT OUVRANT DROIT À UN COMPLÉMENT DE RÉMUNÉRATION [RJ1] - 2) DÉPASSEMENT OUVRANT DROIT À UNE INDEMNITÉ [RJ2].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">36-08-02 FONCTIONNAIRES ET AGENTS PUBLICS. RÉMUNÉRATION. TRAITEMENT. - HEURES SUPPLÉMENTAIRES - CAS DU RÉGIME D'HORAIRE D'ÉQUIVALENCE DES SAPEURS-POMPIERS PROFESSIONNELS (DÉCRET N° 2001-1382 DU 31 DÉCEMBRE 2001) - 1) DÉPASSEMENT OUVRANT DROIT À UN COMPLÉMENT DE RÉMUNÉRATION [RJ1] - 2) DÉPASSEMENT OUVRANT DROIT À UNE INDEMNITÉ [RJ2].
</SCT>
<ANA ID="9A"> 135-01-04-02-03 1) Le régime d'horaire d'équivalence, prévu à l'article 4 du décret n° 2001-1382 du 31 décembre 2001, constituant un mode particulier de comptabilisation du travail effectif qui consiste à prendre en compte la totalité des heures de présence, tout en leur appliquant un mécanisme de pondération tenant à la moindre intensité du travail fourni pendant les périodes d'inaction, seules peuvent ouvrir droit à un complément de rémunération les heures de travail effectif réalisées par les sapeurs-pompiers au-delà du temps d'équivalence au décompte annuel du temps de travail fixé, dans les limites prévues par les textes.,,,2) Le dépassement des durées maximales de travail prévues tant par le droit de l'Union européenne que par le droit national ne peut ouvrir droit par lui-même qu'à l'indemnisation des préjudices résultant de l'atteinte à la santé et à la sécurité ainsi que des troubles subis dans les conditions d'existence.</ANA>
<ANA ID="9B"> 36-08-02 1) Le régime d'horaire d'équivalence, prévu à l'article 4 du décret n° 2001-1382 du 31 décembre 2001, constituant un mode particulier de comptabilisation du travail effectif qui consiste à prendre en compte la totalité des heures de présence, tout en leur appliquant un mécanisme de pondération tenant à la moindre intensité du travail fourni pendant les périodes d'inaction, seules peuvent ouvrir droit à un complément de rémunération les heures de travail effectif réalisées par les sapeurs-pompiers au-delà du temps d'équivalence au décompte annuel du temps de travail fixé, dans les limites prévues par les textes.,,,2) Le dépassement des durées maximales de travail prévues tant par le droit de l'Union européenne que par le droit national ne peut ouvrir droit par lui-même qu'à l'indemnisation des préjudices résultant de l'atteinte à la santé et à la sécurité ainsi que des troubles subis dans les conditions d'existence.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., pour la méthode de calcul, CE, 19 octobre 2011, Service départemental d'incendie et de secours du Finistère, n° 333746, T. p. 982.,,[RJ2] Rappr., sur le droit à indemnité, CE, 19 décembre 2019, Service départemental d'incendie et de secours du Loiret, n°s 426031 428635, (pt. 11), aux Tables sur un autre point.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
