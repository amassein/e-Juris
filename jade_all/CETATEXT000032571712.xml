<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032571712</ID>
<ANCIEN_ID>JG_L_2016_05_000000384395</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/57/17/CETATEXT000032571712.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème - 8ème chambres réunies, 20/05/2016, 384395, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-05-20</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>384395</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème - 8ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FABIANI, LUC-THALER, PINATEL</AVOCATS>
<RAPPORTEUR>M. Christophe Pourreau</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emmanuelle Cortot-Boucher</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2016:384395.20160520</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Le 21 septembre 2010, la société Solar Electric Martinique a demandé au tribunal administratif de Fort-de-France de la décharger des rappels de taxe sur la valeur ajoutée auxquels elle a été assujettie au titre de la période du 1er janvier 2005 au 31 décembre 2007, ainsi que des pénalités correspondantes. Par un jugement n° 1000625 du 26 juin 2012, le tribunal administratif a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 12BX02010 du 10 juillet 2014, la cour administrative d'appel de Bordeaux a rejeté l'appel formé par la société contre ce jugement.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 10 septembre et 10 décembre 2014 au secrétariat du contentieux du Conseil d'Etat, la société Solar Electric Martinique demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la directive 77/388/CEE du 17 mai 1977 ;<br/>
              - la directive 2006/112/CE du 28 novembre 2006 ;<br/>
              - le code général des impôts ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Christophe Pourreau, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Emmanuelle Cortot-Boucher, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Fabiani, Luc-Thaler, Pinatel, avocat de la société Solar Electric Martinique ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'en vertu du 1° du IV de l'article 256 du code général des impôts, dans sa rédaction applicable aux impositions litigieuses, " les travaux immobiliers sont considérés comme des prestations de services " soumises à la taxe sur la valeur ajoutée ; qu'aux termes de l'article 266 du même code : " 1. La base d'imposition est constituée : / (...) f. Pour les travaux immobiliers, par le montant des marchés, mémoires ou factures ; (...) " ; qu'aux termes de l'article 268 bis de ce code : " Lorsqu'une personne effectue concurremment des opérations se rapportant à plusieurs des catégories prévues aux articles du présent chapitre, son chiffre d'affaires est déterminé en appliquant à chacun des groupes d'opérations les règles fixées par ces articles " ; qu'enfin, aux termes de l'article 295 de ce code : " 1. Sont exonérés de la taxe sur la valeur ajoutée : / (...) 5° Dans les départements de la Guadeloupe, de la Martinique et de la Réunion : / a) Les importations de (...) produits dont la liste est fixée par arrêtés conjoints du ministre de l'économie et des finances et du ministre (...) chargé des départements d'outre-mer ; b) Les ventes... des produits de fabrication locale analogues à ceux dont l'importation dans les départements susvisés est exemptée en vertu des dispositions qui précèdent ; (...) " ; que le I de l'article 50 duodecies de l'annexe IV à ce code mentionne, parmi les produits dont l'importation dans les départements de la Guadeloupe, de la Martinique et de la Réunion peut avoir lieu en franchise de taxe sur la valeur ajoutée, les " dispositifs photosensibles à semi-conducteur, y compris les cellules photovoltaïques même assemblées en modules ou constituées en panneaux " ; <br/>
<br/>
              2. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, sur le fondement des dispositions citées au point 1, la société Solar Electric Martinique, qui a notamment pour activité la vente et l'installation d'équipements liés à l'énergie solaire en Martinique, n'a soumis à la taxe sur la valeur ajoutée les opérations d'installation sur le toit d'immeubles d'habitation de panneaux photovoltaïques et de chauffe-eau solaires qu'à concurrence du coût des prestations d'installation ; qu'à la suite d'une vérification portant sur la période du 1er janvier 2005 au 31 décembre 2007, l'administration a considéré que ces opérations avaient le caractère de travaux immobiliers et devaient, par suite, inclure le coût des équipements fournis ; qu'elle a rectifié, en conséquence, l'assiette imposable de ces opérations ; que la société Solar Electric Martinique se pourvoit en cassation contre l'arrêt du 10 juillet 2014 par lequel la cour administrative d'appel de Bordeaux a rejeté son appel contre le jugement du 26 juin 2012 par lequel le tribunal administratif de Fort-de-France a rejeté sa demande de décharge des rappels de taxe sur la valeur ajoutée auxquels elle a été assujettie à la suite de cette rectification ;<br/>
<br/>
              3. Considérant, en premier lieu, que le moyen tiré de ce que la société Solar Electric Martinique n'aurait pas été régulièrement avertie du jour de l'audience publique manque en fait ;<br/>
<br/>
              4. Considérant, en deuxième lieu, que le moyen tiré de ce que l'installation de panneaux photovoltaïques et de chauffe-eau solaires ne pouvait pas avoir le caractère de travaux immobiliers, dès lors que l'acquisition de ces équipements avait reçu l'agrément du ministre du budget en vue de l'application de la réduction d'impôt sur le revenu prévue à l'article 199 undecies B du code général des impôts, était inopérant ; que, par suite, le moyen tiré de ce que la cour administrative d'appel y aurait répondu de façon insuffisamment motivée doit être écarté ;<br/>
<br/>
              5. Considérant, en troisième lieu, que la doctrine administrative 3 B 271 est relative à l'interprétation du c du 2 de l'article 269 du code général des impôts, dont l'administration n'a pas fait application dans le présent litige ; que, par suite, la cour administrative d'appel n'a pas commis d'erreur de droit en jugeant que la société Solar Electric Martinique ne pouvait pas se prévaloir de l'interprétation de la notion de travaux immobiliers figurant dans cette doctrine ;<br/>
<br/>
              6. Considérant, en quatrième lieu, qu'il résulte de la combinaison des dispositions citées au point 1 que les opérations de vente et d'installation d'équipements mentionnés à l'article 295 du code général des impôts et au I de l'article 50 duodecies de l'annexe IV à ce code ne sont imposées à la taxe sur la valeur ajoutée qu'à concurrence du coût des opérations d'installation, à l'exclusion du coût d'acquisition des équipements, sauf à ce que ces opérations d'installation aient le caractère de prestations de travaux immobiliers et soient, dès lors, imposées à la taxe sur la valeur ajoutée à concurrence de l'ensemble du prix facturé au preneur ; que, si l'article 295 du code général des impôts ne s'applique que dans des départements d'outre-mer, soit en dehors du champ d'application territorial de la sixième directive du 17 mai 1977 en matière d'harmonisation des législations des Etats membres relatives aux taxes sur le chiffre d'affaires alors en vigueur, les dispositions précitées de l'article 256 relatives aux travaux immobiliers s'appliquent aussi en France métropolitaine ; qu'elles assurent la transposition en droit national de l'article 5, paragraphe 5, et de l'article 6, paragraphe 1, de la sixième directive du 17 mai 1977, devenus l'article 14, paragraphe 3, et l'article 24, paragraphe 1, de la directive du 28 novembre 2006 relative au système commun de la taxe sur la valeur ajoutée ; qu'il y a lieu de rechercher une application uniforme des dispositions de ces directives au sein de l'Union européenne ; qu'ainsi, le litige que doit trancher le Conseil d'Etat, bien qu'il concerne des opérations réalisées en dehors du champ d'application territorial de ces directives, pose la question de savoir si la vente et l'installation de panneaux photovoltaïques et de chauffe-eau solaires sur des immeubles ou en vue d'alimenter des immeubles en électricité ou en eau chaude constituent une opération unique ayant le caractère de travaux immobiliers au sens de ces directives ;<br/>
<br/>
              7. Considérant que cette question, qui est déterminante pour la solution du litige, présente une difficulté sérieuse ; que, par suite, il y a lieu, en application de l'article 267 du traité sur le fonctionnement de l'Union européenne, d'en saisir la Cour de justice de l'Union européenne et, jusqu'à ce que celle-ci se soit prononcée, de surseoir à statuer sur le pourvoi présenté par la société Solar Electric Martinique ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : Il est sursis à statuer sur le pourvoi présenté par la société Solar Electric Martinique jusqu'à ce que la Cour de justice de l'Union européenne se soit prononcée sur la question de savoir si la vente et l'installation de panneaux photovoltaïques et de chauffe-eau solaires sur des immeubles ou en vue d'alimenter des immeubles en électricité ou en eau chaude constituent une opération unique ayant le caractère de travaux immobiliers au sens de l'article 5, paragraphe 5, et de l'article 6, paragraphe 1, de la sixième directive du 17 mai 1977 en matière d'harmonisation des législations des Etats membres relatives aux taxes sur le chiffre d'affaires, devenus l'article 14, paragraphe 3, et l'article 24, paragraphe 1, de la directive du 28 novembre 2006 relative au système commun de la taxe sur la valeur ajoutée.<br/>
Article 2 : La présente décision sera notifiée à la société Solar Electric Martinique, au ministre des finances et des comptes publics et au greffe de la Cour de justice de l'Union européenne.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
