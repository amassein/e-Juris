<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042164481</ID>
<ANCIEN_ID>JG_L_2020_07_000000441611</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/16/44/CETATEXT000042164481.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 13/07/2020, 441611, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-07-13</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>441611</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2020:441611.20200713</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Par une requête, enregistrée le 5 juillet 2020 au secrétariat du contentieux du Conseil d'Etat, Mme B... A... demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative :<br/>
              1°) d'ordonner la suspension de l'exécution de la décision du 3 juillet 2020 par laquelle l'Office français de l'immigration et de l'intégration (OFII) a réduit le montant de l'allocation pour demandeur d'asile ; <br/>
<br/>
              2°) d'enjoindre à l'OFII de produire le détail du calcul de l'allocation pour demandeur d'asile et de justifier les sommes prélevées ;<br/>
<br/>
              3°) d'enquêter sur la légitimité de la décision et de l'action de l'OFII à s'introduire dans son compte relatif à l'allocation pour demandeur d'asile et à réduire son montant sans notification, et en cas d'absence de légitimité pour ce faire, de condamner l'OFII ;<br/>
<br/>
              4°) d'enjoindre à l'OFII de communiquer à son détenteur automatiquement la situation mensuelle de la tenue de son compte relatif à l'allocation pour demandeur d'asile, comprenant les interventions, le nom des agents, le motif et le détail de l'intervention ;<br/>
<br/>
              5°) d'enjoindre à l'OFII de lui assurer, par anticipation de la fin de la prise en charge par le SAMU social, des conditions d'accueil en adéquation avec sa situation de demanderesse de protection internationale.<br/>
<br/>
<br/>
<br/>
              Elle soutient que : <br/>
              - la condition d'urgence est remplie dès lors que, d'une part, par suite de l'absence de proposition de logement par l'OFII et d'une réduction substantielle au mois de juin 2020 du montant de l'allocation pour demandeur d'asile, elle est exposée, ainsi que son fils, à une dégradation flagrante de sa situation sociale, de son état de santé physique et psychologique et, d'autre part, la prise en charge par les services du SAMU social dont elle bénéficie dans le cadre de la protection des femmes battues prend fin au 1er août 2020 ;<br/>
              - la réduction du montant de l'allocation pour demandeur d'asile dont elle bénéficie est injustifiée dès lors notamment que l'OFII ne fournit pas le logement prévu par la procédure d'accueil du demandeur d'asile ;<br/>
              - la prise en charge d'hébergement par l'OFII ne saurait être subordonnée à une participation financière ;<br/>
              - l'OFII ne pouvait utiliser ses données bancaires pour prélever la contribution financière exigée.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. Le juge des référés se prononce dans un délai de quarante-huit heures ". En vertu de l'article L. 522-3 du même code, le juge des référés peut, par une ordonnance motivée, rejeter une requête sans instruction ni audience lorsque la condition d'urgence n'est pas remplie ou lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable ou qu'elle est mal fondée.<br/>
<br/>
              2. Le juge des référés du Conseil d'Etat ne peut être régulièrement saisi, en premier et dernier ressort, d'une requête tendant à la mise en oeuvre de l'une des procédures régies par le livre V du code de justice administrative que pour autant que le litige principal auquel se rattache ou est susceptible de se rattacher la mesure d'urgence qu'il lui est demandé de prendre ressortit lui-même de la compétence directe du Conseil d'Etat. L'article R. 522-8-1 du même code prévoit que, par dérogation aux dispositions du titre V du livre III relatif au règlement des questions de compétence au sein de la juridiction administrative, le juge des référés qui entend décliner la compétence de la juridiction rejette les conclusions dont il est saisi par voie d'ordonnance.<br/>
<br/>
              3. Mme A... sollicite le prononcé de plusieurs mesures dont la suspension de l'exécution de la décision du 3 juillet 2020 par laquelle l'Office français de l'immigration et de l'intégration (OFII) a réduit le montant de l'allocation pour demandeur d'asile dont elle bénéficie. Cette décision n'est manifestement pas au nombre de celles dont il appartient au Conseil d'Etat de connaître en premier ressort et dernier ressort en vertu des dispositions de l'article R. 311-1 du code de justice administrative.<br/>
<br/>
              4. Il résulte de ce qui précède que la requête de Mme A... ne peut être accueillie. Par suite, sa requête doit être rejetée selon la procédure prévue par l'article L. 522-3 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de Mme A... est rejetée. <br/>
Article 2 : La présente ordonnance sera notifiée à Mme B... A....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
