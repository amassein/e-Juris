<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043751451</ID>
<ANCIEN_ID>JG_L_2021_07_000000451362</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/75/14/CETATEXT000043751451.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème - 8ème chambres réunies, 01/07/2021, 451362, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-07-01</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>451362</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème - 8ème chambres réunies</FORMATION>
<TYPE_REC>Autres</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FOUSSARD, FROGER</AVOCATS>
<RAPPORTEUR>M. Martin Guesdon</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Marie-Gabrielle Merloz</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:451362.20210701</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              La commune d'Epinay-sur-Seine, en défense au déféré du préfet de la Seine-Saint-Denis tendant, sur le fondement de l'article L. 2131-6 du code général des collectivités territoriales, à l'annulation de l'arrêté du 16 septembre 2019 par lequel le maire de la commune a interdit l'utilisation du glyphosate et d'autres substances chimiques visant à lutter contre des organismes considérés comme nuisibles pour l'entretien de certains espaces, sur le territoire de la commune, a produit un mémoire, enregistré le 25 janvier 2021 au greffe du tribunal administratif de Montreuil, en application de l'article 23-1 de l'ordonnance n° 58-1067 du 7 novembre 1958, par lequel il soulève une question prioritaire de constitutionnalité. <br/>
<br/>
              Par une ordonnance n° 2002482 du 11 mars 2021, enregistrée le 2 avril 2021 au secrétariat du contentieux du Conseil d'Etat, la présidente de la 2ème chambre du tribunal administratif de Montreuil, avant qu'il soit statué sur le déféré du préfet de la Seine-Saint-Denis, a décidé, par application de l'article 23-2 de l'ordonnance n° 58-1067 du 7 novembre 1958, de transmettre au Conseil d'Etat la question de la conformité aux droits et libertés garantis par la Constitution des dispositions de l'article L. 253-7 du code rural et de la pêche maritime. <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la Constitution, notamment son Préambule et son article 61-1 ; <br/>
              - l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
              - le code général des collectivités territoriales ; <br/>
              - le code rural et de la pêche maritime ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Martin Guesdon, auditeur,  <br/>
<br/>
              - les conclusions de Mme Marie-Gabrielle Merloz, rapporteure publique ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Foussard, Froger, avocat de la commune d'Epinay-sur-Seine ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Par un arrêté du 16 septembre 2019, le maire d'Epinay-sur-Seine a interdit l'utilisation du glyphosate et des produits phytopharmaceutiques mentionnés au premier alinéa de l'article L. 253-1 du code rural et de la pêche maritime pour l'entretien de certains espaces. Le préfet de la Seine-Saint-Denis a déféré cet arrêté au tribunal administratif de Montreuil sur le fondement de l'article L. 2131-6 du code général des collectivités territoriales. Par mémoire distinct, la commune d'Epinay-sur-Seine, en défense, a demandé au tribunal de transmettre au Conseil d'Etat la question de la conformité aux droits et libertés garantis par la Constitution des dispositions de l'article L. 253-7 du code rural et de la pêche maritime. Par une ordonnance du 11 mars 2021, la présidente de la 2ème chambre du tribunal administratif de Montreuil a transmis au Conseil d'Etat la question prioritaire de constitutionnalité ainsi soulevée et a sursis à statuer jusqu'à ce qu'il ait été statué sur cette dernière.<br/>
<br/>
              2. Il résulte des dispositions de l'article 23-4 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel que, lorsqu'une juridiction relevant du Conseil d'Etat a transmis à ce dernier, en application de l'article 23-2 de cette même ordonnance, la question de la conformité à la Constitution d'une disposition législative, le Conseil constitutionnel est saisi de cette question de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux.<br/>
<br/>
              3. En posant une question prioritaire de constitutionnalité sur une disposition législative, tout justiciable a le droit de contester la constitutionnalité de la portée effective qu'une interprétation jurisprudentielle constante confère à cette disposition.<br/>
<br/>
              4. Aux termes du I de l'article L. 253-7 du code rural et de la pêche maritime : " I. - Sans préjudice des missions confiées à l'Agence nationale de sécurité sanitaire de l'alimentation, de l'environnement et du travail et des dispositions de l'article L. 211-1 du code de l'environnement, l'autorité administrative peut, dans l'intérêt de la santé publique ou de l'environnement, prendre toute mesure d'interdiction, de restriction ou de prescription particulière concernant la mise sur le marché, la délivrance, l'utilisation et la détention des produits mentionnés à l'article L. 253-1 du présent code et des semences traitées par ces produits (...). L'autorité administrative peut interdire ou encadrer l'utilisation des produits phytopharmaceutiques dans des zones particulières (...) ". <br/>
<br/>
              5. Il résulte de ces dispositions, et de celles des articles L. 253-1, L. 253-7-1, L. 253-8, R. 253-1, R. 253-45, D. 253-45-1 du code rural et de la pêche maritime ainsi que de l'article 5 de l'arrêté du 4 mai 2017, que le législateur a organisé une police spéciale de la mise sur le marché, de la détention et de l'utilisation des produits phytopharmaceutiques, confiée à l'Etat et dont l'objet est, conformément au droit de l'Union européenne, d'assurer un niveau élevé de protection de la santé humaine et animale et de l'environnement tout en améliorant la production agricole et de créer un cadre juridique commun pour parvenir à une utilisation des pesticides compatible avec le développement durable, alors que les effets de long terme de ces produits sur la santé restent, en l'état des connaissances scientifiques, incertains. Si les articles L. 2212-1 et L. 2212-2 du code général des collectivités territoriales habilitent le maire à prendre, pour la commune, les mesures de police générale nécessaires au bon ordre, à la sûreté, à la sécurité et à la salubrité publiques, celui-ci ne peut légalement user de cette compétence pour édicter une réglementation portant sur les conditions générales d'utilisation des produits phytopharmaceutiques qu'il appartient aux seules autorités de l'Etat de prendre. <br/>
<br/>
              6. La commune soutient que les dispositions de l'article L. 253-7 du code rural et de la pêche maritime, telles qu'interprétées au point 5 ci-dessus selon une jurisprudence constante du Conseil d'Etat, méconnaîtraient les articles 1er, 2, 3 et 5 de la Charte de l'environnement, le droit à la protection de la santé découlant du onzième alinéa du Préambule de la Constitution de 1946 ainsi que les deuxième et troisième alinéas de l'article 72 de la Constitution, en ce qu'elles excluent toute possibilité d'intervention du maire pour réglementer les conditions générales d'utilisation des produits phytopharmaceutiques, y compris lorsque serait constatée la carence de l'Etat à prendre les mesures nécessaires à la protection de la santé publique et de l'environnement. <br/>
<br/>
              7. En premier lieu, il résulte des dispositions des articles 1er, 2, 3 et 5 de la Charte de l'environnement que les principes, droits et devoirs qu'ils énoncent s'imposent aux autorités publiques dans leur domaine de compétence respectif. La commune ne saurait, dès lors, utilement soutenir que ces dispositions seraient méconnues au seul motif que le maire ne peut légalement user de la compétence qu'il tient des articles L. 2212-1 et L. 2212-2 du code général des collectivités territoriales pour édicter une réglementation portant sur les conditions générales d'utilisation des produits phytopharmaceutiques. <br/>
<br/>
              8. En deuxième lieu, ainsi qu'il a été dit au point 5, le législateur a organisé une police spéciale de la mise sur le marché, de la détention et de l'utilisation des produits phytopharmaceutiques, confiée à l'Etat et dont l'objet est d'assurer un niveau élevé de protection de la santé humaine et animale et de l'environnement tout en améliorant la production agricole et de créer un cadre juridique commun pour parvenir à une utilisation des pesticides compatible avec le développement durable. Il appartient ainsi à l'autorité administrative de l'Etat de prendre toute mesure d'interdiction, de restriction ou de prescription particulière, s'agissant de l'utilisation de produits phytopharmaceutiques, qui s'avère nécessaire à la protection de la santé publique. En cas de carence de l'autorité administrative dans l'exercice de ces pouvoirs de police spéciale constatée par une décision juridictionnelle, il incombe à cette autorité de prendre les mesures qu'implique l'exécution de cette décision, dans le délai qu'elle impartit. Dès lors, la seule circonstance que le maire ne puisse édicter une réglementation portant sur les conditions générales d'utilisation des produits phytopharmaceutiques en cas de carence des autorités de l'Etat ne peut être regardée comme portant atteinte au droit à la protection de la santé.<br/>
<br/>
              9. En troisième lieu, d'une part, les dispositions du deuxième alinéa de l'article 72 de la Constitution ne peuvent être utilement invoquées à l'appui d'une question prioritaire de constitutionnalité. D'autre part, contrairement à ce que soutient la commune, les dispositions de l'article L. 253-7 du code rural et de la pêche maritime n'ont ni pour objet ni pour effet de restreindre la libre administration des communes, la compétence qu'elles tiennent des articles L. 2212-1 et L. 2212-2 du code général des collectivités territoriales et la compétence dévolue par la loi aux autorités de l'Etat pour réglementer les conditions générales d'utilisation des produits phytopharmaceutiques ayant un objet distinct. Dès lors, le grief tiré de ce que ces dispositions porteraient atteinte au principe de libre administration des collectivités territoriales ne peut qu'être écarté.<br/>
<br/>
              10. Il résulte de ce qui précède que la question prioritaire de constitutionnalité soulevée, qui n'est pas nouvelle, ne présente pas un caractère sérieux. Il n'y a, dès lors, pas lieu de la renvoyer au Conseil constitutionnel.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée par la commune d'Epinay-sur-Seine. <br/>
Article 2 : La présente décision sera notifiée à la commune d'Epinay-sur-Seine et au ministre de l'agriculture et de l'alimentation. <br/>
Copie en sera adressée au Conseil constitutionnel, au Premier ministre et au tribunal administratif de Montreuil. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
