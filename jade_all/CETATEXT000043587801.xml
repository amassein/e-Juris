<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043587801</ID>
<ANCIEN_ID>JG_L_2021_05_000000438847</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/58/78/CETATEXT000043587801.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème - 7ème chambres réunies, 28/05/2021, 438847</TITRE>
<DATE_DEC>2021-05-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>438847</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème - 7ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MARLANGE, DE LA BURGADE ; SCP FOUSSARD, FROGER</AVOCATS>
<RAPPORTEUR>Mme Sophie-Caroline de Margerie</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Guillaume Odinet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:438847.20210528</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. E... A... et Mme C... épouse A... ont demandé à la Cour nationale du droit d'asile d'annuler les décisions du directeur général de l'Office français de protection des réfugiés et apatrides du 12 décembre 2017 refusant de leur reconnaître la qualité de réfugié et, à défaut, de leur accorder le bénéfice de la protection subsidiaire. <br/>
<br/>
              Par une décision n° 18005109, 18005030 du 22 octobre 2019, la Cour nationale du droit d'asile a rejeté leurs demandes.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 19 février et 15 mai 2020 au secrétariat du contentieux du Conseil d'Etat, M. et Mme A... demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette décision ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à leurs demandes ;<br/>
<br/>
              3°) de mettre à la charge de l'Office français de protection des réfugiés et apatrides la somme de 3 500 euros à verser à la SCP Marlange, de La Burgade, leur avocat, au titre des dispositions de l'article 37 de la loi du 10 juillet 1991 et de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la convention de Genève du 28 juillet 1951 relative au statut des réfugiés et le protocole signé à New-York le 31 janvier 1967 ;<br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020.<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme D... B..., conseillère d'Etat, <br/>
<br/>
              - les conclusions de M. Guillaume Odinet, rapporteur public,<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Marlange, de la Burgade, avocat de M. et Mme A... et à la SCP Foussard, Froger, avocat de l'office français de protection des réfugiés et apatrides ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que par deux décisions des 12 décembre 2017, le directeur général de l'Office français de protection des réfugiés et apatrides a refusé de faire droit aux demandes de M. et Mme A..., de nationalité irakienne, tendant à ce que leur soit reconnu le statut de réfugié ou, à défaut, accordé le bénéfice de la protection subsidiaire. Par une décision du 22 octobre 2019, contre laquelle M. et Mme A... se pourvoient en cassation, la Cour nationale du droit d'asile a rejeté leur demande d'annulation de ces décisions. <br/>
<br/>
              2. En premier lieu, le premier alinéa de l'article L. 733-1 du code de l'entrée et du séjour des étrangers et du droit d'asile, devenu l'article L. 532-12, dispose : " Les intéressés peuvent présenter leurs explications à la Cour nationale du droit d'asile et s'y faire assister d'un conseil et d'un interprète ". Aux termes du premier alinéa de l'article R. 733-19 du même code, devenu l'article R. 532-32 : " L'avis d'audience est adressé aux parties trente jours au moins avant le jour où l'affaire sera appelée à l'audience. (...) ". Ces dispositions ont pour objet, non seulement d'informer l'intéressé de la date de l'audience afin de lui permettre d'y être présent ou représenté, mais aussi de lui laisser un délai suffisant pour préparer utilement ses observations. Il s'ensuit que leur méconnaissance est de nature à entacher d'irrégularité la procédure suivie. <br/>
<br/>
              3. Il ressort des pièces de la procédure devant la Cour nationale du droit d'asile qu'un avis a été adressé à M. et Mme A... le 30 août 2019 pour les informer que leur affaire serait appelée à l'audience du 1er octobre 2019, soit dans le respect du délai franc de trente jours prévu par les dispositions précitées du premier alinéa de l'article R. 733-19 du code de l'entrée et du séjour des étrangers et du droit d'asile, sans qu'il y ait lieu de distinguer selon que ce délai commence ou s'achève par un samedi, un dimanche ou un jour férié. Par suite, le moyen tiré de ce que la procédure devant la Cour nationale du droit d'asile aurait été entachée d'irrégularité en raison de la méconnaissance de ces dispositions doit être écarté.  <br/>
<br/>
              4. En deuxième lieu, aux termes de l'article 1er A 2° de la convention de Genève du 28 juillet 1951 sur le statut des réfugiés, modifiée par l'article 1er 2 du protocole signé le 31 janvier 1967, la qualité de réfugié est notamment reconnue à : " Toute personne (. . .) qui, craignant avec raison d'être persécutée du fait de sa race, de sa religion, de sa nationalité, de son appartenance à un certain groupe social ou de ses opinions politiques, se trouve hors du pays dont elle a la nationalité et qui ne peut, ou, du fait de cette crainte, ne veut se réclamer de la protection de ce pays ". Selon l'article L. 712-1 du code de l'entrée et du séjour des étrangers et du droit d'asile, devenu l'article L. 512-1, dans sa rédaction applicable au litige : " Le bénéfice de la protection subsidiaire est accordé à toute personne qui ne remplit pas les conditions pour se voir reconnaître la qualité de réfugié et pour laquelle il existe des motifs sérieux et avérés de croire qu'elle courrait dans son pays un risque réel de subir l'une des atteintes graves suivantes : / a) La peine de mort ou une exécution ; / b) La torture ou des peines ou traitements inhumains ou dégradants ; / c) S'agissant d'un civil, une menace grave et individuelle contre sa vie ou sa personne en raison d'une violence qui peut s'étendre à des personnes sans considération de leur situation personnelle et résultant d'une situation de conflit armé interne ou international. ". Il résulte de ces dernières dispositions du c) de l'article L. 712-1 que l'existence d'une menace grave, directe et individuelle contre la vie ou la personne d'un demandeur à la protection subsidiaire n'est pas subordonnée à la condition qu'il rapporte la preuve qu'il est visé spécifiquement en raison d'éléments propres à sa situation personnelle dès lors que le degré de violence généralisée caractérisant le conflit armé atteint un niveau si élevé qu'il existe des motifs sérieux et avérés de croire qu'un civil renvoyé dans le pays ou la région concernés courrait, du seul fait de sa présence sur le territoire, un risque réel de subir ces menaces. <br/>
<br/>
              5. D'une part, pour rejeter la demande de M. et Mme A..., la Cour nationale du droit d'asile, s'est fondée sur le caractère faiblement circonstancié de leur récit relatif aux persécutions et menaces dont ils disent avoir été victimes dans leur pays d'origine et qu'ils disent redouter en cas de retour. Ce faisant, elle a porté sur les faits qui lui étaient soumis une appréciation souveraine exempte de dénaturation et n'a ni méconnu les règles de dévolution de la charge de la preuve ni commis d'erreur de droit. En ne mentionnant pas le certificat médical relatif à une mutilation subie par Mme A..., produit à l'appui d'une note en délibéré enregistrée postérieurement à la tenue de l'audience, elle n'a pas davantage commis d'erreur de droit. <br/>
<br/>
              6. D'autre part, il ressort des énonciations de la décision attaquée et des pièces de la procédure que pour analyser la situation sécuritaire dans le Kurdistan irakien, plus particulièrement dans le gouvernorat de Souleymaniye dont M. et Mme A... sont originaires, et estimer que cette situation n'était pas caractérisée par un niveau de violence susceptible de s'étendre à des personnes sans considération de leur situation personnelle, la cour s'est fondée sur des rapports et documents librement accessibles au public, dont elle a indiqué l'origine. En fondant sa décision sur ces éléments d'information générale sans les avoir versés au dossier, la cour n'a pas méconnu le caractère contradictoire de la procédure ou les droits de la défense. Par ailleurs, la cour a porté sur les faits qui lui étaient soumis une appréciation souveraine exempte de dénaturation et a pu en déduire, sans inexacte appréciation des pièces du dossier, que les intéressés ne pouvaient bénéficier de l'application des dispositions du c) de l'article L. 712-1 du code de l'entrée et du séjour des étrangers et du droit d'asile. <br/>
<br/>
              7. Il résulte de tout ce qui précède que M. et Mme A... ne sont pas fondés à demander l'annulation de la décision qu'ils attaquent. <br/>
<br/>
              8. Les dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 font obstacle ce que soit mis à la charge de l'OFPRA qui n'est pas, dans la présente instance, la partie perdante, une somme à ce titre. <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
        --------------<br/>
<br/>
Article 1er : Le pourvoi de M. et Mme A... est rejeté. <br/>
<br/>
Article 2 : La présente décision sera notifiée à M. et Mme A... et à l'Office français de protection des réfugiés et apatrides.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">095-08-04-03-01 - CONVOCATION À L'AUDIENCE (ART. R. 733-19 DU CESEDA) - DÉLAI DE 30 JOURS ENTRE L'ENVOI DE L'AVIS DE L'AUDIENCE ET LA TENUE DE CELLE-CI [RJ1] - CARACTÈRE FRANC - EXISTENCE [RJ2].
</SCT>
<ANA ID="9A"> 095-08-04-03-01 Le délai de trente jours prévu par l'article R. 733-19, devenu R. 532-32, du code de l'entrée et du séjour des étrangers et du droit d'asile (CESEDA) entre l'envoi de l'avis d'audience et la tenue de celle-ci a le caractère d'un délai franc.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., sur l'objet de ce délai, CE, 27 mars 2020, Mme,, n° 431290, T. p. 615.,,[RJ2] Cf., sur le caractère franc d'un délai de procédure devant la juridiction administrative, sauf dispositions contraires, CE, 11 mai 2001,,, n° 211912, p. 231.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
