<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033442760</ID>
<ANCIEN_ID>JG_L_2016_11_000000386637</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/44/27/CETATEXT000033442760.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème chambre, 18/11/2016, 386637, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-11-18</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>386637</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP CELICE, SOLTNER, TEXIDOR, PERIER</AVOCATS>
<RAPPORTEUR>M. Géraud Sajust de Bergues</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Vincent Daumas</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2016:386637.20161118</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Le 13 novembre 2009, M. A... B...a demandé au tribunal administratif de Paris de le décharger des suppléments d'impôt sur le revenu auxquels il a été assujetti au titre des années 2002 à 2005, ainsi que des pénalités correspondantes. Par un jugement n° 0914269 du 18 novembre 2011, le tribunal administratif de Montreuil, à qui la demande a été transmise, a rejeté la demande de M. B.ou établie hors de France<br/>
<br/>
              Par un arrêt n° 12VE00605 du 18 juillet 2014, la cour administrative d'appel de Versailles a rejeté l'appel formé par M. B... contre ce jugement.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 22 décembre 2014 et 23 mars 2015 au secrétariat du contentieux du Conseil d'Etat, M. B... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le traité sur le fonctionnement de l'Union européenne ;<br/>
              - la convention entre la France et le Grand-Duché de Luxembourg tendant à éviter les doubles impositions en matière d'impôt sur le revenu et sur la fortune du 1er avril 1958 modifiée ; <br/>
              - le code général des impôts ;<br/>
              - la décision n° 2010-70 QPC du Conseil constitutionnel du 26 novembre 2010 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Géraud Sajust de Bergues, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Vincent Daumas, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Célice, Soltner, Texidor, Perier, avocat de M. B...;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que M. A... B..., qui exerce l'activité d'agent commercial, et son épouse sont les uniques salariés et détiennent chacun 25 % des parts de la société Recherche Médicale et Distribution (RMD), établie au Luxembourg. Les 15 novembre 1998 et 1er janvier 2000, la société RMD et la société Guidant, établie en France, ont conclu des contrats par lesquels la seconde accordait à la première un mandat exclusif de commercialisation de matériels médicaux, essentiellement sur le territoire français. A la suite de l'exercice de son droit de communication et d'un contrôle sur pièces, l'administration a estimé que les sommes facturées par la société RMD à la société Guidant en rémunération des prestations d'agent commercial rendues en France par M. B... avaient le caractère de traitements et salaires, imposables en France sur le fondement de l'article 155 A du code général des impôts, et rectifié en conséquence les bases d'imposition à l'impôt sur le revenu de M. B... au titre des années 2002 à 2005. Par un arrêt du 18 juillet 2014, contre lequel M. B... se pourvoit en cassation, la cour administrative d'appel de Versailles a rejeté l'appel qu'il a formé contre le jugement du 18 novembre 2011 par lequel le tribunal administratif de Montreuil a rejeté sa demande de décharge des impositions supplémentaires auxquelles il a été assujetti ainsi que des pénalités correspondantes.<br/>
<br/>
              2. Aux termes de l'article 155 A du code général des impôts : " I. Les sommes perçues par une personne domiciliée ou établie hors de Franceen rémunération de services rendus par une ou plusieurs personnes domiciliées ou établies en France sont imposables au nom de ces dernières : / - soit, lorsque celles-ci contrôlent directement ou indirectement la personne qui perçoit la rémunération des services ; / - soit, lorsqu'elles n'établissent pas que cette personne exerce, de manière prépondérante, une activité industrielle ou commerciale, autre que la prestation de services ; / - soit, en tout état de cause, lorsque la personne qui perçoit la rémunération des services est domiciliée ou établie hors de Franceoù elle est soumise à un régime fiscal privilégié au sens mentionné à l'article 238 A. / II. Les règles prévues au I ci-dessus sont également applicables aux personnes domiciliées hors de France pour les services rendus en France (...) ".<br/>
<br/>
              3. En premier lieu, les moyens tirés de ce que les sommes versées à la société RMD ne pouvaient être imposées sur le fondement de l'article 155 A du code général des impôts dès lors qu'elles étaient imposables sur le fondement soit de l'article 209 du même code, la société disposant en France d'un établissement stable, soit de l'article 10 du même code, ces sommes ayant ensuite été reversées à M. B..., n'ont pas été invoqués devant la cour administrative d'appel. Dès lors que leur bien-fondé ne ressortait pas des pièces du dossier qui lui était soumis, ils n'avaient pas à être soulevés d'office. Par suite, M. B... ne peut invoquer ces moyens pour contester le bien-fondé de l'arrêt attaqué.<br/>
<br/>
              4. En deuxième lieu, il ressort des pièces du dossier soumis aux juges du fond que les contrats signés entre les sociétés RMD et Guidant ainsi que les avenants à ces contrats fixaient le périmètre d'intervention de la société RMD, les hôpitaux et cliniques à démarcher, la liste des matériels médicaux à proposer et les modalités de calcul et de paiement des commissions et comportaient des clauses de non concurrence. Par suite, en énonçant que M. B... ne disposait d'aucune autonomie réelle pour réaliser les opérations de démarchages au profit de la société Guidant et se trouvait ainsi dans un lien de subordination vis-à-vis de cette société, pour en déduire que les sommes en litige étaient imposables dans la catégorie des traitements et salaires, la cour administrative d'appel s'est livrée à une appréciation souveraine des faits de l'espèce, qui n'est pas entachée de  dénaturation.<br/>
<br/>
              5. En troisième lieu, les moyens tirés de ce que la cour administrative d'appel aurait omis, pour déterminer si les conditions d'application de l'article 155 A du code général des impôts étaient remplies, de rechercher si la société RMD exerçait une activité industrielle et commerciale propre, distincte des prestations rendues par M. B... à la société Guidant et aurait ainsi commis une erreur de droit, et de ce qu'elle aurait omis de répondre au moyen tiré de l'incompatibilité de l'article 155 A du code général des impôts avec la liberté d'établissement et la libre prestation de services manquent en fait.<br/>
<br/>
              6. En quatrième lieu, la cour administrative d'appel a suffisamment répondu à l'argumentation tirée de ce que M. B...subissait une double imposition incompatible avec la liberté d'établissement et la libre prestation de services, en renvoyant au 1 de l'article 14 de la convention fiscale franco-luxembourgeoise dans l'hypothèse où M. B...subirait effectivement une telle double imposition.<br/>
<br/>
              7. Il résulte de tout de ce qui précède que M. B... n'est pas fondé à demander l'annulation de l'arrêt attaqué. Par suite, ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative doivent être rejetées.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : Le pourvoi de M. B... est rejeté.<br/>
Article 2 : La présente décision sera notifiée à M. A... B...et au ministre de l'économie et des finances.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
