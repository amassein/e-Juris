<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000041675188</ID>
<ANCIEN_ID>JG_L_2020_03_000000416833</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/41/67/51/CETATEXT000041675188.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème - 1ère chambres réunies, 02/03/2020, 416833, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-03-02</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>416833</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème - 1ère chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MATUCHANSKY, POUPOT, VALDELIEVRE ; SCP DELAMARRE, JEHANNIN</AVOCATS>
<RAPPORTEUR>Mme Yaël Treille</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Dieu</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:416833.20200302</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A... B... a demandé au tribunal administratif de Nouvelle-Calédonie d'annuler pour excès de pouvoir, d'une part, l'article 17 de la délibération n°13-2015/APS du 30 avril 2015 de l'assemblée de la province Sud de la Nouvelle-Calédonie relative à la bourse d'accès aux grandes écoles et, d'autre part, la décision du président de l'assemblée de la province du 12 août 2015 lui refusant le renouvellement de sa bourse d'excellence pour l'année universitaire 2015-2016. Par un jugement n°s 1500300, 1500415 du 26 mai 2016, le tribunal administratif a rejeté ses demandes.<br/>
<br/>
              Par un arrêt n° 16PA02511 du 20 septembre 2017, la cour administrative d'appel de Paris a, sur appel de M. B..., annulé ce jugement ainsi que, d'une part, l'article 17 de la délibération n°13-2015/APS du 30 avril 2015 en tant qu'il prévoit l'application de la condition de ressources prévue à l'article 5 de la délibération aux étudiants qui ont été déclarés bénéficiaires d'une bourse d'excellence sur le fondement de la délibération n° 48-2012/APS du 18 décembre 2012 et, d'autre part, la décision du président de l'assemblée de la province du 12 août 2015 refusant le renouvellement de la bourse. <br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 26 décembre 2017, 26 mars et 17 juillet 2018 au secrétariat du contentieux du Conseil d'Etat, la province Sud de la Nouvelle-Calédonie demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de M. B... ;<br/>
<br/>
              3°) de mettre à la charge de M. B... la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales et son protocole additionnel ;<br/>
              - la loi organique n° 99-209 du 19 mars 1999 ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Yaël Treille, auditeur,  <br/>
<br/>
              - les conclusions de M. Frédéric Dieu, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Matuchansky, Poupot, Valdelièvre, avocat de la province Sud de la Nouvelle-Calédonie et à la SCP Delamarre, Jéhannin, avocat de M. B... ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. L'assemblée de la province Sud de la Nouvelle-Calédonie a institué, par une délibération du 18 décembre 2012, une bourse d'excellence destinée, chaque année, à dix étudiants originaires de cette province et " inscrits dans un établissement d'enseignement supérieur en France ou à l'étranger ", dont les diplômes ou formations permettent d'exercer une activité professionnelle en Nouvelle-Calédonie et qui figurent sur une liste fixée par un arrêté du 27 février 2013 du président de l'assemblée de la province. Sur le fondement de cette délibération, le président de l'assemblée de la province, par arrêté du 16 septembre 2014, a attribué à M. A... B..., admis à l'école nationale des mines de Saint-Etienne pour y accomplir des études d'ingénieur à compter de l'année universitaire 2014-2015, une bourse d'excellence au titre de cette année universitaire 2014-2015. Par une décision du 12 août 2015, le directeur de l'éducation de la province Sud a toutefois refusé de renouveler cette bourse d'excellence sollicitée pour l'année universitaire 2015-2016 par M. B..., admis à poursuivre ses études dans le même établissement, au motif que la délibération de l'assemblée de la province Sud du 30 avril 2015 relative à la bourse d'accès aux grandes écoles, ayant abrogé la délibération du 18 décembre 2012, avait introduit une condition de ressources parmi les conditions d'attribution de cette bourse et que les revenus des parents de M. B... étaient trop élevés pour que fût remplie cette condition. Par un jugement du 26 mai 2016, le tribunal administratif de Nouvelle-Calédonie a rejeté la demande de M. B... tendant à l'annulation, d'une part, de la décision de refus de renouvellement de sa bourse du 12 août 2015 et, d'autre part, de l'article 17 de la délibération du 30 avril 2015, qui abroge la délibération du 18 décembre 2012 et soumet le renouvellement des bourses d'excellence accordées en application de celle-ci " aux conditions, procédures et autres modalités d'octroi définies par la présente délibération ", au nombre desquelles figure la condition de ressources. Par un arrêt du 20 septembre 2017, contre lequel la province Sud se pourvoit en cassation, la cour administrative d'appel de Paris, sur appel de M. B..., a annulé, d'une part, le jugement du tribunal administratif de Nouvelle-Calédonie du 26 mai 2016 et, d'autre part, la décision du président de l'assemblée de la province Sud du 12 août 2015 ainsi que l'article 17 de la délibération du 30 avril 2015 en tant que celui-ci prévoit l'application d'une condition de ressources aux étudiants qui ont été déclarés bénéficiaires d'une bourse d'excellence sur le fondement de la délibération du 18 décembre 2012. <br/>
<br/>
              2. En premier lieu, il ressort des pièces du dossier soumis aux juges du fond qu'en vertu des articles 1er et 3 de la délibération du 18 décembre 2012, dix bourses d'excellence pouvaient être attribuées chaque année à des étudiants originaires de la province Sud, inscrits dans un établissement d'enseignement supérieur figurant sur une liste fixée par arrêté du président de l'assemblée de cette province, et s'engageant par écrit à revenir travailler en Nouvelle-Calédonie pendant une durée minimale de cinq ans à la fin de leurs études. En vertu de l'article 4 de cette délibération, les demandes tendant au renouvellement d'une bourse d'excellence n'étaient pas soumises pour avis à la commission des bourses d'excellence, alors que les demandes tendant à l'attribution initiale d'une telle bourse y étaient soumises. Aux termes de l'article 6 de cette délibération, " la bourse d'excellence est renouvelée chaque année sur production d'un certificat de scolarité ou de tout autre justificatif attestant de l'inscription dans un établissement d'enseignement supérieur, sous réserve de l'article 10 ". L'article 10 de cette même délibération prévoyait que : " la bourse est supprimée : / - en cas de redoublement pour résultats insuffisants ; / - en cas d'exclusion de l'établissement pour raison disciplinaire ; / - en cas de renonciation aux études pour lesquelles la bourse avait été initialement attribuée (...)", et que dans ces deux derniers cas, la province Sud pouvait demander le remboursement total ou partiel des sommes versées. Il résulte de l'ensemble de ces éléments qu'en jugeant, au regard de ces dispositions et de l'économie générale de la délibération du 18 décembre 2012, que les étudiants bénéficiant d'une bourse d'excellence au titre de l'année précédente étaient assurés de voir celle-ci renouvelée, dès lors que ceux-ci justifiaient du bon déroulement de leurs études dans l'établissement dans lequel ils étaient inscrits, la cour n'a pas commis d'erreur de droit.<br/>
<br/>
              3. En second lieu, aux termes des stipulations de l'article 1er du protocole additionnel à la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales : " Toute personne physique ou morale a droit au respect de ses biens. Nul ne peut être privé de sa propriété que pour cause d'utilité publique et dans les conditions prévues par la loi et les principes généraux du droit international. / Les dispositions précédentes ne portent pas atteinte au droit que possèdent les Etats de mettre en vigueur les lois qu'ils jugent nécessaires pour réglementer l'usage des biens conformément à l'intérêt général (...) ". Une personne ne peut prétendre au bénéfice de ces stipulations que si elle peut faire état de la propriété d'un bien qu'elles ont pour objet de protéger et à laquelle il aurait été porté atteinte. A défaut de créance certaine, l'espérance légitime d'obtenir une somme d'argent doit être regardée comme un bien au sens de ces stipulations qui ne font en principe pas obstacle à ce que le législateur adopte de nouvelles dispositions remettant en cause, fût-ce de manière rétroactive, des droits patrimoniaux découlant de lois en vigueur ayant le caractère d'un bien au sens de ces stipulations, à la condition cependant de ménager un juste équilibre entre l'atteinte portée à ces droits et les motifs d'intérêt général susceptibles de la justifier. <br/>
<br/>
              4. Il ressort des pièces du dossier soumis aux juges du fond que M. B... avait été admis à passer en deuxième année d'études au sein de l'école nationale des mines de Saint-Etienne et qu'il avait justifié auprès de la province Sud de la poursuite de son cursus universitaire. Il suit de là, compte tenu de ce qui a été dit au point 2, que la cour n'a entaché son arrêt d'aucune erreur de droit, ni inexactement qualifié les faits en jugeant que M. B... était fondé à se prévaloir d'une espérance légitime, au sens des stipulations de l'article 1er du premier protocole additionnel à la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, à voir sa bourse renouvelée. <br/>
<br/>
              5. Pour juger, en l'espèce, que l'article 17 de la délibération du 30 avril 2015, en ce qu'il prévoit l'application de la nouvelle condition de ressources aux étudiants qui avaient été déclarés bénéficiaires d'une bourse d'excellence sur le fondement de la délibération du 18 décembre 2012, portait une atteinte excessive à l'espérance légitime de M. B... de continuer de percevoir cette bourse, la cour administrative d'appel a retenu que si la province Sud avait pu, dans un but d'intérêt général, eu égard au coût budgétaire de ces aides et au nombre limité de nouvelles bourses attribuées en conséquence chaque année à des primo-entrants dans les établissements concernés, décider de réduire le montant annuel de ces aides et d'en réserver le bénéfice à des étudiants dont les familles ne disposaient pas de revenus annuels supérieurs à 12 millions de Francs CFP, ce motif ne suffisait pas à justifier l'application de cette nouvelle condition de ressources à des étudiants qui s'étaient engagés dans un cursus scolaire sous l'empire du dispositif institué en 2012, lequel conduisait à prendre en charge une grande partie du coût de leurs études et de leurs frais, et qui se retrouvaient, du fait d'une disposition intervenue quatre mois seulement avant le début de l'année scolaire 2015-2016, privés de cette aide financière. En statuant ainsi, en l'état de ses constatations souveraines, la cour administrative d'appel n'a pas commis d'erreur de droit et n'a pas inexactement qualifié les faits de l'espèce.<br/>
<br/>
              6. Il résulte de tout ce qui précède que la province Sud n'est pas fondée à demander l'annulation de l'arrêt qu'elle attaque. Ses conclusions présentées au titre des dispositions de l'article L. 761-1 du code justice administrative ne peuvent, en conséquence, qu'être rejetées.<br/>
<br/>
              7. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la province Sud une somme de 1 500 euros à verser à M. B..., au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de la province Sud de la Nouvelle-Calédonie est rejeté.<br/>
Article 2 : La province Sud de la Nouvelle-Calédonie versera à M. B... une somme de 1 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée à la province Sud de la Nouvelle-Calédonie et à M. A... B....<br/>
Copie en sera adressée à la ministre des outre-mer.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
