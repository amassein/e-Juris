<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037317283</ID>
<ANCIEN_ID>JG_L_2018_08_000000414012</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/31/72/CETATEXT000037317283.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème chambre, 16/08/2018, 414012, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-08-16</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>414012</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Mireille Le Corre</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Louis Dutheillet de Lamothe</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:414012.20180816</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, enregistrée le 4 septembre 2015 au greffe du tribunal administratif de Bordeaux, et transmise au Conseil d'Etat par l'ordonnance de ce tribunal n° 1503990 du 25 juillet 2016, Mme B...A...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision du garde des sceaux, ministre de la justice, du 30 juin 2015 par laquelle il ne l'a pas autorisée à participer aux épreuves du concours de recrutement de magistrats du second grade de la hiérarchie judiciaire ouvert au titre de la session 2015, ainsi que la décision rejetant son recours gracieux ;<br/>
<br/>
              2°) d'enjoindre à l'administration de réexaminer son dossier de candidature afin de lui permettre de concourir au concours complémentaire de recrutement des magistrats du second degré de la hiérarchie judiciaire ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 1 500 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
	Vu : <br/>
- l'ordonnance n° 58-1270 du 22 décembre 1958 ;<br/>
               - l'arrêté du 22 novembre 2001 relatif aux concours de recrutement de magistrats prévus par l'article 21-1 de l'ordonnance n° 58-1270 du 22 décembre 1958 portant loi organique relative au statut de la magistrature ; <br/>
- le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Mireille Le Corre, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Louis Dutheillet de Lamothe, rapporteur public.<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article 21-1 de l'ordonnance du 22 décembre 1958 portant loi organique relative au statut de la magistrature dans sa rédaction applicable au litige : " Deux concours sont ouverts pour le recrutement de magistrats du second et du premier grade de la hiérarchie judiciaire. / Les candidats doivent remplir les conditions prévues à l'article 16. : Ils doivent en outre : / 1° Pour les candidats aux fonctions du second grade de la hiérarchie judiciaire, être âgés de trente-cinq ans au moins au 1er janvier de l'année d'ouverture du concours et justifier d'au moins dix ans d'activité professionnelle dans le domaine juridique, administratif, économique ou social, les qualifiant particulièrement pour exercer des fonctions judiciaires ; (...) ".<br/>
<br/>
              2.	 Par une décision du 30 juin 2015, le garde des sceaux, ministre de la justice, a refusé d'autoriser Mme A...à participer aux épreuves du concours de recrutement de magistrats du second grade de la hiérarchie judiciaire ouvert au titre de la session 2015, au motif que l'intéressée ne justifiait pas d'au moins dix années d'activité professionnelle dans le domaine juridique, administratif, économique ou social, la qualifiant particulièrement pour exercer des fonctions judiciaires. Mme A...demande l'annulation pour excès de pouvoir de cette décision ainsi que de la décision rejetant son recours gracieux.<br/>
<br/>
              3.	En premier lieu, aux termes de l'article 6 de l'arrêté du 22 novembre 2001 relatif aux concours de recrutement de magistrats prévus par l'article 21-1 de l'ordonnance du 22 décembre 1958 portant loi organique relative au statut de la magistrature : " Le directeur de l'Ecole nationale de la magistrature s'assure que les dossiers de candidature sont régulièrement constitués. / Il transmet alors les dossiers en état au garde des sceaux, ministre de la justice, qui fixe par arrêté, après avis du directeur de l'Ecole nationale de la magistrature, la liste des candidats admis à prendre part respectivement à chacun des concours. La vérification des conditions exigées par l'article 16 (3°) de l'ordonnance du 22 décembre 1958 susvisée est toutefois effectuée dans les conditions prévues à l'article 7. (...) ". Il ressort des pièces du dossier que l'avis du directeur de l'école nationale de la magistrature, dont le visa dans les décisions attaquées n'est pas exigé par ces dispositions, a été pris préalablement à la décision attaquée du garde des sceaux, ministre de la justice, en date du 30 juin 2015. Par suite, le moyen tiré de ce que l'avis du directeur de l'école nationale de la magistrature n'est pas mentionné dans les décisions attaquées doit être écarté.<br/>
<br/>
              4. En deuxième lieu, l'exigence de justifier d'au moins dix ans d'activité professionnelle dans le domaine juridique, administratif, économique ou social, les qualifiant particulièrement pour exercer des fonctions judiciaires pour être admis au concours pour le recrutement de magistrats du second grade de la hiérarchie judiciaire résulte de l'article 21-1 de l'ordonnance du 22 décembre 1958, le Conseil constitutionnel ayant précisé, dans sa décision n° 2001-445 DC du 19 juin 2001, que les épreuves du concours, définies par la voie réglementaire, devaient en outre permettre de vérifier les connaissances juridiques du candidat. Par suite, en se fondant sur l'insuffisance de l'expérience professionnelle de l'intéressée, le garde des sceaux, ministre de la justice n'a pas, contrairement à ce que soutient MmeA..., méconnu les dispositions de l'article 21-1 de l'ordonnance du 22 décembre 1958.<br/>
<br/>
              5. En troisième lieu, il ressort des pièces du dossier que c'est sans erreur manifeste d'appréciation que le garde des sceaux, ministre de la justice, a estimé que les périodes durant lesquelles la requérante a exercé les activités d'aide à domicile, d'agent vacataire à la cour d'appel de Pau, de secrétaire dactylographe, de secrétaire stagiaire, d'employée de bureau, d'agent des services hospitaliers, de manutentionnaire saisonnier, d'agent de nettoyage et d'entretien, d'agent participant à des fouilles archéologiques, de femme de chambre, d'assistante maternelle agréée et de caissière ne pouvaient être prises en compte au titre des activités professionnelles dans le domaine juridique, administratif, économique ou social, la qualifiant particulièrement pour exercer des fonctions judiciaires, pour en déduire que sa candidature ne satisfaisait à la condition fixée par le 1° de l'article 21-1 de l'ordonnance du 22 décembre 1958. Si Mme A...fait par ailleurs valoir que d'autres candidatures ont été retenues, il ne ressort pas des pièces du dossier que les décisions attaquées méconnaitraient le principe d'égalité eu égard à la différence des parcours des candidats.<br/>
<br/>
              6. Il résulte de ce qui précède que Mme A...n'est pas fondée à demander l'annulation des décisions qu'elle attaque. Sa requête ne peut, par suite, qu'être rejetée, y compris ses conclusions tendant à ce qu'elle soit autorisée à se présenter au concours complémentaire, ainsi que celles présentées au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de Mme A...est rejetée<br/>
Article 2 : La présente décision sera notifiée à Mme B...A...et à la garde des sceaux, ministre de la justice.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
