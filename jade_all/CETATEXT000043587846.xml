<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043587846</ID>
<ANCIEN_ID>JG_L_2021_06_000000443238</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/58/78/CETATEXT000043587846.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème chambre, 01/06/2021, 443238, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-06-01</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>443238</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Edouard Solier</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Raphaël Chambon</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:443238.20210601</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme G... D... a saisi le tribunal administratif de Caen d'une protestation relative aux opérations électorales qui se sont déroulées le 28 juin 2020 dans la commune de Longueville (Calvados) en vue de l'élection de deux conseillers municipaux. Par une ordonnance n° 2001200 du 17 juillet 2020, le président de la 3ème chambre du tribunal administratif a rejeté sa protestation.<br/>
<br/>
              Par une requête et deux mémoires, enregistrés les 24 août, 8 décembre et 17 décembre 2020 au secrétariat du contentieux du Conseil d'Etat, Mme D... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) d'annuler ces opérations électorales.<br/>
<br/>
<br/>
<br/>
              Vu : <br/>
              - le code électoral ;<br/>
              - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Edouard Solier, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Raphaël Chambon, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. À l'issue des opérations électorales qui se sont déroulées le 15 mars 2020 pour l'élection des conseillers municipaux de la commune de Longueville, qui compte moins de 1 000 habitants, 9 des 11 sièges à pourvoir ont été pourvus. A l'issue du second tour du 28 juin 2020, M. H... et M. F... ont été proclamés élus. Mme D... relève appel de l'ordonnance du 17 septembre 2020 par laquelle le président de la 3ème chambre du tribunal administratif de Caen a rejeté sa protestation comme irrecevable.<br/>
<br/>
              2. D'une part, aux termes de l'article R. 222-1 du code de justice administrative : " (...) les présidents de formation de jugement des tribunaux (...) peuvent, par ordonnance : (...) / 4° Rejeter les requêtes manifestement irrecevables, lorsque la juridiction n'est pas tenue d'inviter leur auteur à les régulariser (...) ".<br/>
<br/>
              3. D'autre part, aux termes de l'article R. 119 du code électoral : " Les réclamations contre les opérations électorales doivent être consignées au procès-verbal, sinon être déposées, à peine d'irrecevabilité, au plus tard à dix-huit heures le cinquième jour qui suit l'élection, à la sous-préfecture ou à la préfecture. Elles sont immédiatement adressées au préfet qui les fait enregistrer au greffe du tribunal administratif (...) ".<br/>
<br/>
              4. Il résulte de l'instruction que la réclamation adressée au préfet du Calvados par Mme D... contenait un grief précis mettant en cause la régularité du vote par procuration de deux électeurs lors des opérations électorales du 28 juin 2020. Par suite, et alors même qu'elle ne comportait pas de conclusions expresses tendant à l'annulation des opérations électorales ou à la réformation des résultats et qu'elle ne mentionnait pas le nom de ces deux électeurs, cette réclamation devait être regardée comme une protestation tendant à l'annulation de ces opérations au sens de l'article R. 119 du code électoral. <br/>
<br/>
              5. Il en résulte que Mme D... est fondée à soutenir que c'est à tort que par l'ordonnance attaquée, le président de la 3ème chambre du tribunal administratif de Caen a rejeté sa protestation comme manifestement irrecevable. <br/>
<br/>
              6. Il y a lieu, par suite, d'annuler cette ordonnance et de statuer immédiatement sur la protestation de Mme D..., le délai imparti au tribunal administratif par l'article R. 120 du code électoral pour statuer étant expiré.<br/>
<br/>
              7. Aux termes de l'article R. 76-1 du code électoral : " Au fur et à mesure de la réception des procurations, le maire inscrit sur un registre ouvert à cet effet les noms et prénoms du mandant et du mandataire, le nom et la qualité de l'autorité qui a dressé l'acte de procuration et la date de son établissement ainsi que la durée de validité de la procuration. Le registre est tenu à la disposition de tout électeur, y compris le jour du scrutin. Dans chaque bureau de vote, un extrait du registre comportant les mentions relatives aux électeurs du bureau est tenu à la disposition des électeurs le jour du scrutin. Le défaut de réception par le maire d'une procuration fait obstacle à ce que le mandataire participe au scrutin ". <br/>
<br/>
              8. Il résulte de l'instruction, et en particulier des observations consignées au procès-verbal, que lors du second tour des élections municipales qui se sont déroulées le 28 juin 2020 à Longueville, deux mandataires ont été autorisés à voter pour le compte de deux mandants sur la seule foi de la présentation des récépissés. Il est constant que les procurations correspondantes n'avaient pas été reçues en mairie le jour du scrutin. Dès lors, et alors même que la circonstance que les deux électeurs ayant donné procuration aient été admis à voter ne résulte pas d'une manoeuvre, ces deux votes doivent être regardés comme irréguliers. <br/>
<br/>
              9. Il appartient au juge de l'élection de tirer les conséquences des irrégularités commises au cours du scrutin, en rectifiant, le cas échéant, les résultats de l'élection. Lorsqu'il est impossible de déterminer sur quelle liste ou en faveur de quel candidat s'est portée la voix à retrancher ou à ajouter aux suffrages exprimés, le juge de l'élection procède au calcul des résultats qui seraient constatés dans chacune des hypothèses, en vérifiant si la liste ou le candidat arrivé en tête conserve la majorité des suffrages. <br/>
<br/>
              10. Il résulte de ce qui est dit au point 9 qu'il y a lieu par suite de retrancher deux voix du nombre total de suffrages obtenus par chacun des candidats proclamés élus. M. H... obtient, après ce retranchement, 63 voix, soit un nombre de suffrages égal aux suffrages obtenus par les deux premiers candidats non élus, M. C... et Mme B.... M. F..., n'obtient, après ce retranchement, que 62 voix et ne peut plus, par suite, être regardé comme élu. <br/>
<br/>
              11. Aux termes de l'article L. 253 du code électoral : " (...) / Au deuxième tour de scrutin, l'élection a lieu à la majorité relative, quel que soit le nombre des votants. Si plusieurs candidats obtiennent le même nombre de suffrages, l'élection est acquise au plus âgé ". Il résulte de la mesure d'instruction supplémentaire ordonnée par la 4ème chambre de la section du contentieux que, parmi les trois candidats obtenant chacun 63 suffrages en application du point 10, M. H... est le plus jeune et ne peut, dès lors, être proclamé élu.<br/>
<br/>
              12. Il résulte donc de l'instruction qu'il n'est pas possible, après déduction des deux suffrages irrégulièrement émis, d'attribuer de façon certaine les deux sièges à pourvoir en application de ce qui a été dit aux points 9 et 10, à l'un quelconque des candidats.<br/>
<br/>
              13. Il résulte de ce qui précède que Mme D... est fondée à demander l'annulation du second tour de l'élection municipale de Longueville.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'ordonnance du président de la 3ème chambre du tribunal administratif de Caen du 17 septembre 2020 est annulée.<br/>
Article 2 : L'élection de M. H... et de M. F... est annulée.<br/>
Article 3 : La présente décision sera notifiée à Mme G... D..., à M. A... H..., à M. E... F... et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
