<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043890820</ID>
<ANCIEN_ID>JG_L_2021_07_000000444787</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/89/08/CETATEXT000043890820.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème chambre, 30/07/2021, 444787, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-07-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>444787</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BAUER-VIOLAS, FESCHOTTE-DESBOIS, SEBAGH</AVOCATS>
<RAPPORTEUR>Mme Flavie Le Tallec</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:444787.20210730</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme A... B... a demandé au tribunal administratif de Lyon d'annuler pour excès de pouvoir la décision du 13 décembre 2017 par laquelle le préfet du Rhône a refusé d'échanger son permis de conduire algérien contre un titre de conduite français. Par une ordonnance n° 1903168 du 21 avril 2020, la présidente de la première chambre du tribunal administratif de Lyon a rejeté sa demande. <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 22 septembre et 21 décembre 2020 au secrétariat du contentieux du Conseil d'Etat, Mme B... demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande.<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 000 euros à verser à la SCP Bauer-Violas, Feschotte-Desbois, Sebagh, son avocat, au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de la route ; <br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - le décret n° 91-1266 du 19 décembre 1991 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Flavie Le Tallec, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Bauer-Violas, Feschotte-Desbois, Sebagh, avocat de Mme B....<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis au juge du fond que, par une décision du 13 décembre 2017, le préfet du Rhône a refusé de procéder à l'échange du permis de conduire algérien de Mme B... contre un titre de conduite français. Mme B... se pourvoit en cassation contre l'ordonnance du 21 avril 2020 par laquelle la présidente de la première chambre du tribunal administratif de Lyon a rejeté comme tardive sa demande d'annulation de cette décision.<br/>
<br/>
              2. L'article 38 du décret du 19 décembre 1991 portant application de la loi n° 91-647 du 10 juillet 1991 relative à l'aide juridique prévoit que : " Lorsqu'une action en justice ou un recours doit être intenté avant l'expiration d'un délai devant les juridictions de première instance (...), l'action ou le recours est réputé avoir été intenté dans le délai si la demande d'aide juridictionnelle s'y rapportant est adressée au bureau d'aide juridictionnelle avant l'expiration dudit délai et si la demande en justice ou le recours est introduit dans un nouveau délai de même durée à compter : / a) De la notification de la décision d'admission provisoire ; / b) De la notification de la décision constatant la caducité de la demande ; / c) De la date à laquelle le demandeur à l'aide juridictionnelle ne peut plus contester la décision d'admission ou de rejet de sa demande en application du premier alinéa de l'article 56 et de l'article 160 ou, en cas de recours de ce demandeur, de la date à laquelle la décision relative à ce recours lui a été notifiée ; / d) Ou, en cas d'admission, de la date, si elle est plus tardive, à laquelle un auxiliaire de justice a été désigné (...) ". Aux termes du deuxième alinéa de l'article 23 de la même loi du 10 juillet 1991 : " Les recours contre les décisions du bureau d'aide juridictionnelle peuvent être exercés par l'intéressé lui-même lorsque le bénéfice de l'aide juridictionnelle lui a été refusé, ne lui a été accordé que partiellement ou lorsque ce bénéfice lui a été retiré " et, en vertu du premier alinéa de l'article 56 du décret du 19 décembre 1991, le délai de ce recours " est de quinze jours à compter du jour de la notification de la décision à l'intéressé ".<br/>
<br/>
              3. Il résulte de la combinaison de ces dispositions qu'une demande d'aide juridictionnelle interrompt le délai de recours contentieux et qu'un nouveau délai de même durée recommence à courir à compter de l'expiration d'un délai de quinze jours après la notification à l'intéressé de la décision se prononçant sur sa demande d'aide juridictionnelle ou, si elle est plus tardive, à compter de la date de désignation de l'auxiliaire de justice au titre de l'aide juridictionnelle. Il en va ainsi quel que soit le sens de la décision se prononçant sur la demande d'aide juridictionnelle, qu'elle en ait refusé le bénéfice, qu'elle ait prononcé une admission partielle ou qu'elle ait admis le demandeur au bénéfice de l'aide juridictionnelle totale, quand bien même dans ce dernier cas le ministère public ou le bâtonnier ont, en vertu de l'article 23 de la loi du 10 juillet 1991, seuls vocation à contester une telle décision.<br/>
<br/>
              4. Il ressort des termes mêmes de l'ordonnance attaquée que, pour juger que la demande de Mme B... était tardive, elle se fonde sur la circonstance que le tribunal administratif a été saisi plus de deux mois après la notification de la décision du bureau d'aide juridictionnelle ayant admis l'intéressée au bénéfice de l'aide juridictionnelle totale. Il résulte de ce qui a été dit au point précédent qu'en statuant ainsi, alors qu'il ressort des pièces du dossier qui lui était soumis que la demande de Mme B..., introduite le 23 avril 2019, avait, en tout état de cause, été enregistrée au tribunal administratif moins de deux mois après l'expiration d'un délai de quinze jours suivant la notification à l'intéressée, le 7 février 2019, de la décision se prononçant sur sa demande d'aide juridictionnelle, la présidente de la première chambre du tribunal administratif de Lyon a entaché sa décision d'une erreur de droit.<br/>
<br/>
              5. Mme B... a obtenu le bénéfice de l'aide juridictionnelle. Par suite, son avocat peut se prévaloir des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991. Il y a lieu, dans les circonstances de l'espèce, de mettre à ce titre à la charge de l'Etat la somme de 1 500 euros à verser à la SCP Bauer-Violas, Feschotte-Desbois, Sebagh, avocat de Mme B..., sous réserve que cette société renonce à percevoir la somme correspondant à la part contributive de l'Etat.  <br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
		Article 1er : L'ordonnance du 21 avril 2020 est annulée. <br/>
<br/>
		Article 2 : L'affaire est renvoyée au tribunal administratif de Lyon. <br/>
<br/>
Article 3 : L'Etat versera à la SCP Bauer-Violas, Feschotte-Desbois, Sebagh une somme de 1 500 euros, au titre de l'article L. 761-1 du code de justice administrative et de l'article 37 de la loi du 10 juillet 1991, sous réserve que cette société renonce à percevoir la somme correspondant à la part contributive de l'Etat.<br/>
<br/>
Article 4 : La présente décision sera notifiée à Mme A... B... et au ministre de l'intérieur. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
