<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031537071</ID>
<ANCIEN_ID>JG_L_2015_11_000000374373</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/53/70/CETATEXT000031537071.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème / 4ème SSR, 27/11/2015, 374373, Publié au recueil Lebon</TITRE>
<DATE_DEC>2015-11-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>374373</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème / 4ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BOULLOCHE</AVOCATS>
<RAPPORTEUR>Mme Manon Perrière</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:374373.20151127</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 2 janvier 2014 au secrétariat du contentieux du Conseil d'Etat, présentée pour l'association Comité de défense des auditeurs de Radio solidarité (CDARS), dont le siège est 61, boulevard Murat à Paris (75016) ; l'association CDARS demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision n° 2013-518 du 24 juillet 2013 par laquelle le Conseil supérieur de l'audiovisuel l'a mise en demeure de respecter les obligations résultant de la convention relative au service radiophonique "Radio Courtoisie", ainsi que la décision du 23 octobre 2013 rejetant son recours gracieux ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat le versement d'une somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la loi n° 86-1067 du 30 septembre 1986 ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Manon Perrière, auditeur,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Boulloche, avocat de l'association CDARS ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 5 novembre 2015, présentée par le Conseil supérieur de l'audiovisuel ; <br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article 1er de la loi du 30 septembre 1986 relative à la liberté de communication : " La communication au public par voie électronique est libre. / L'exercice de cette liberté ne peut être limité que dans la mesure requise, d'une part, par le respect de la dignité de la personne humaine, de la liberté et de la propriété d'autrui, du caractère pluraliste de l'expression des courants de pensée et d'opinion et, d'autre part, par la protection de l'enfance et de l'adolescence, par la sauvegarde de l'ordre public, par les besoins de la défense nationale, par les exigences de service public, par les contraintes techniques inhérentes aux moyens de communication, ainsi que par la nécessité, pour les services audiovisuels, de développer la production audiovisuelle " ; qu'aux termes de l'article 28 de la même loi : " La délivrance des autorisations d'usage de la ressource radioélectrique pour chaque nouveau service diffusé par voie hertzienne terrestre autre que ceux exploités par les sociétés nationales de programme, est subordonnée à la conclusion d'une convention passée entre le Conseil supérieur de l'audiovisuel au nom de l'Etat et la personne qui demande l'autorisation. / Dans le respect de l'honnêteté et du pluralisme de l'information et des programmes et des règles générales fixées en application de la présente loi et notamment de son article 27, cette convention fixe les règles particulières applicables au service, compte tenu de l'étendue de la zone desservie, de la part du service dans le marché publicitaire, du respect de l'égalité de traitement entre les différents services et des conditions de concurrence propres à chacun d'eux, ainsi que du développement de la radio et de la télévision numériques de terre (...) " ; <br/>
<br/>
              2. Considérant qu'il ressort des pièces du dossier que l'association Comité de défense des auditeurs de Radio solidarité (CDARS) dispose d'une autorisation d'exploiter dans la zone de Paris un service de catégorie A par voie hertzienne en modulation de fréquence dénommé " Radio courtoisie ", qui lui a été attribuée par des décisions du Conseil supérieur de l'audiovisuel (CSA) des 24 juillet 2007 et 25 novembre 2008 et renouvelée pour cinq ans par des décisions du 8 février 2012 et du 5 avril 2013 ; que, conformément aux dispositions de l'article 28 de la loi du 30 septembre 1986, la délivrance de cette autorisation a été subordonnée à la signature d'une convention avec le CSA ; qu'aux termes de cette convention, le CSA peut adresser au bénéficiaire de l'autorisation une mise en demeure de respecter les obligations imposées tant par l'autorisation d'émettre que par la convention elle-même ; qu'à la suite de propos tenus à l'antenne le 27 mai 2013 au cours de l'émission " Le libre journal d'Henry de Lesquen ", le CSA a estimé que l'association CDARS avait méconnu les prescriptions des articles 2-3, 2-4 et 2-10 de la convention ; que, par la décision du 24 juillet 2013 dont l'association CDARS demande l'annulation, le CSA a mis cette association en demeure de respecter à l'avenir les obligations prévues par ces articles ; <br/>
<br/>
              3. Considérant que la décision attaquée, qui vise la loi du 30 septembre 1986, énonce avec une précision suffisante les considérations de droit et de fait qui fondent la mise en demeure qu'elle adresse à l'association requérante ; que, dès lors, elle n'est pas entachée d'insuffisance de motivation ; <br/>
<br/>
              4. Considérant que, si la décision attaquée vise " la convention conclue le 24 juillet 2007 entre le CSA et  l'association CDARS ", alors que celle-ci a été remplacée par une nouvelle convention conclue le 8 février 2012, cette seconde convention reprend intégralement le contenu de la précédente ;  que, dans ces conditions, l'erreur dont la décision est entachée sur ce point n'a pas d'incidence sur sa légalité ; <br/>
<br/>
              5. Considérant qu'il ressort de la retranscription versée au dossier de l'émission à la suite de laquelle a été adressée la mise en demeure litigieuse que son animateur a notamment tenu à l'antenne les propos suivants : " le mariage homosexuel devrait susciter l'horreur tellement il est contre-nature, tellement il est scandaleux [...] le mariage homosexuel est tellement abject [...] il faut être inconscient pour ne pas comprendre à quel point l'islam est dangereux et radicalement incompatible avec notre identité [...] A première vue, il n'y a aucun rapport entre le mariage des homosexuels et le déferlement des immigrés et pourtant la loi scélérate qui autorise le mariage contre nature entre deux personnes de même sexe découle de la même source idéologique que la politique d'ouverture de la France à l'immigration, elles ont l'une comme l'autre pour but de porter atteinte aux fondements de notre identité nationale " ;<br/>
<br/>
              6. Considérant que l'article 2-3 de la convention conclue entre le CSA et l'association CDARS le 8 février 2012 prévoit que le titulaire de l'autorisation veille au respect du pluralisme des courants de pensée et d'opinion, en particulier dans les émissions d'information politique générale ; que de telles prescriptions, qui imposent au titulaire de l'autorisation de réserver un accès à l'antenne à différents courants de pensée et d'opinion, ne peuvent être légalement imposées à l'exploitant d'un service radiophonique qui se donne pour vocation d'assurer l'expression d'un courant particulier d'opinion ; qu'ainsi, la décision du Conseil supérieur de l'audiovisuel du 24 juillet 2013, en tant qu'elle met l'association Comité de défense des auditeurs de Radio solidarité en demeure de respecter les obligations prévues par l'article 2-3 de la convention, et la décision du 23 octobre 2013 du CSA rejetant le recours gracieux de l'association sur ce point doivent être annulées ;  <br/>
<br/>
              7. Considérant que l'article 2-4 de la même convention prévoit que le titulaire de l'autorisation veille à ne pas encourager des comportements discriminatoires à l'égard des personnes en raison de leur appartenance ou de leur non-appartenance, vraie ou supposée, à une ethnie, une nation, une race ou une religion déterminée et s'engage à promouvoir les valeurs d'intégration et de solidarité qui sont celles de la République ; que cette prescription a pour objet de faire obstacle à ce que le titulaire de l'autorisation d'émettre en fasse usage pour encourager les comportements discriminatoires, contraires aux valeurs d'intégration et de solidarité de la République ; que le moyen tiré de ce qu'elle méconnaîtrait la liberté d'expression doit être écarté ; qu'en estimant que les propos de l'animateur, partiellement reproduits au point 5 ci-dessus, étaient de nature à encourager les comportements discriminatoires et avaient donc été tenus en méconnaissance de cette prescription et en mettant en demeure l'association requérante de se conformer à celle-ci à  l'avenir, le CSA a  fait une exacte application de l'article 2-4 de la convention et n'a commis aucune erreur d'appréciation ;  <br/>
<br/>
              8 Considérant qu'en estimant que les prescriptions de l'article 2-10 de la convention relatives à la maîtrise de l'antenne avaient été méconnues et en mettant en demeure l'association de s'y conformer à l'avenir, le CSA n'a pas davantage commis d'erreur de droit ou d'erreur d'appréciation ;   <br/>
<br/>
              9. Considérant que le détournement de pouvoir allégué n'est pas établi ;<br/>
<br/>
              Sur les conclusions présentées par l'association CDARS au titre des dispositions de l'article L. 761-1 du code de justice administrative : <br/>
<br/>
              10.  Considérant qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées à ce titre par l'association requérante ; <br/>
<br/>
<br/>
<br/>
<br/>			D E C I D E :<br/>
              			--------------<br/>
<br/>
Article 1er : La décision n° 2013-518 du 24 juillet 2013 du Conseil supérieur de l'audiovisuel en tant qu'elle met l'association Comité de défense des auditeurs de Radio solidarité en demeure de respecter les obligations prévues par l'article 2-3 de la convention relative au service radiophonique "Radio Courtoisie" et la décision du 23 octobre 2013 du CSA rejetant le recours gracieux de l'association sur ce point sont annulées. <br/>
Article 2 : Le surplus des conclusions de la requête est rejeté.<br/>
Article 3 : La présente décision sera notifiée à l'association Comité de défense des auditeurs de Radio solidarité et au Conseil supérieur de l'audiovisuel. <br/>
Copie en sera adressée à la ministre de la culture et de la communication. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">56-04 RADIO ET TÉLÉVISION. SERVICES PRIVÉS DE RADIO ET DE TÉLÉVISION. - SERVICES AUTORISÉS - ARTICLE DE LA CONVENTION CONCLUE EN APPLICATION DE L'ART. 28 DE LA LOI DU 30 SEPTEMBRE 1986 PRÉVOYANT QUE LE TITULAIRE DE L'AUTORISATION VEILLE AU RESPECT DU PLURALISME DES COURANTS DE PENSÉE ET D'OPINION - PORTÉE - ILLÉGALITÉ DANS LE CAS D'UN SERVICE RADIOPHONIQUE QUI SE DONNE POUR VOCATION D'ASSURER L'EXPRESSION D'UN COURANT PARTICULIER D'OPINION.
</SCT>
<ANA ID="9A"> 56-04 Article de la convention conclue, en application de l'article 28 de la loi n° 86-1067 du 30 septembre 1986, entre le CSA et le titulaire de l'autorisation d'exploiter un service radio prévoyant que celui-ci veille au respect du pluralisme des courants de pensée et d'opinion, en particulier dans les émissions d'information politique générale.,,,De telles prescriptions, qui imposent au titulaire de l'autorisation de réserver un accès à l'antenne à différents courants de pensée et d'opinion, ne peuvent être légalement imposées à l'exploitant d'un service radiophonique qui se donne pour vocation d'assurer l'expression d'un courant particulier d'opinion. Illégalité de la mise en demeure de respecter ces prescriptions.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
