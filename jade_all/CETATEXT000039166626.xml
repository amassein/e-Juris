<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039166626</ID>
<ANCIEN_ID>JG_L_2019_09_000000428510</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/16/66/CETATEXT000039166626.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère - 4ème chambres réunies, 25/09/2019, 428510</TITRE>
<DATE_DEC>2019-09-25</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>428510</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère - 4ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP SEVAUX, MATHONNET ; SCP GATINEAU, FATTACCINI</AVOCATS>
<RAPPORTEUR>M. Damien Pons</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Rémi Decout-Paolini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:428510.20190925</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Le cabinet d'expertise comptable APEX a demandé au juge des référés du tribunal administratif de Montreuil, sur le fondement de l'article L. 521-3 du code de justice administrative, d'ordonner au conseil d'administration de l'Agence nationale pour la formation professionnelle des adultes (AFPA) de lui communiquer " le contrat d'objectifs et de performance déterminant avec précision les objectifs de politique publique assignés à l'AFPA, le pilotage stratégique de cette dernière, ses objectifs et priorités ainsi que les modalités par lesquelles ceux-ci sont suivis et évalués par l'Etat, ainsi que le chiffre d'affaires par financeurs et par région, le chiffre d'affaires, l'excédent brut d'exploitation et le résultat opérationnel par centre, le chiffre d'affaires, l'heure travaillée stagiaire, le MCD et le MOP par groupes de référence nationaux, les présentations des actions commerciales/développement visant à l'accroissement d'activité par type d'offre et un point sur les attentes relatives au PIC, le cahier des charges et les rapports des cabinets de conseil ayant contribué à l'élaboration du projet, les documents relatifs à la conduite du changement et à l'accompagnement des transformations : budgets, objectifs, plans de formation et de communication, calendrier de déploiement ". Par une ordonnance n° 1900945 du 12 février 2019, le juge des référés du tribunal administratif de Montreuil a rejeté cette demande.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 28 février et 15 mars 2019 au secrétariat du contentieux du Conseil d'Etat, le cabinet d'expertise comptable APEX demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) statuant en référé, de faire droit à sa demande ;<br/>
<br/>
              3°) de mettre à la charge de l'AFPA la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code du travail ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Damien Pons, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Rémi Decout-Paolini, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Sevaux, Mathonnet, avocat du cabinet d'expertise comptable APEX et à la SCP Gatineau, Fattaccini, avocat de l'Agence nationale pour la formation professionnelle des adultes ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis au juge des référés qu'un projet de réorganisation et un projet de plan de sauvegarde de l'emploi ont été présentés au comité central d'entreprise de l'Agence nationale pour la formation professionnelle des adultes (AFPA) le 7 novembre 2018, le projet de réorganisation conduisant à envisager la création de 603 emplois, la modification de 454 emplois et la suppression de 1 541 emplois avant 2020. Par une délibération du même jour, le comité central d'entreprise de l'AFPA a décidé de recourir à l'assistance d'un expert-comptable, ainsi que le lui permettait l'article L. 1233-34 du code du travail dans sa rédaction alors applicable, et a mandaté à cette fin le cabinet d'expertise comptable APEX. Par lettres des 8 novembre et 31 décembre 2018, celui-ci a demandé à l'AFPA diverses informations qu'il jugeait nécessaires à l'exercice de sa mission. Saisi le 29 janvier 2019 par ce cabinet d'expertise comptable, sur le fondement de l'article L. 521-3 du code de justice administrative, d'une demande tendant à ce qu'il soit enjoint au conseil d'administration de l'AFPA de lui communiquer le contrat d'objectifs et de performance déterminant les objectifs assignés à cet établissement public par l'Etat, ainsi que d'autres documents afférents, le juge des référés du tribunal administratif de Montreuil l'a, par une ordonnance rendue en application de l'article L. 522-3 du même code, rejetée comme portée devant un ordre de juridiction incompétent pour en connaître. Le cabinet d'expertise comptable APEX se pourvoit en cassation contre cette ordonnance.<br/>
<br/>
              Sur les conclusions à fin de non-lieu et sur la fin de non-recevoir opposée par l'Agence nationale pour la formation professionnelle des adultes :<br/>
<br/>
              2. Sous réserve, pour les entreprises qui sont en redressement ou en liquidation judiciaire, des dispositions de l'article L. 1233-58 du code du travail, l'information et la consultation du comité d'entreprise ou, désormais, du comité social et économique, sur un licenciement collectif donnant lieu à l'établissement d'un plan de sauvegarde de l'emploi sont régies par l'article L. 1233-30 du même code, duquel il résulte que l'administration ne peut être régulièrement saisie d'une demande d'homologation  ou de validation d'un plan de sauvegarde de l'emploi que si cette demande est accompagnée des avis rendus par le comité d'entreprise ou, en l'absence de ces avis, si, du fait de l'expiration du délai que cet article prévoit pour qu'il émette les deux avis requis, le comité d'entreprise est réputé avoir été consulté. Toutefois, la circonstance que le comité d'entreprise ou, désormais, le comité social et économique ait rendu ses avis au-delà des délais qu'elles prévoient est par elle-même, lorsque la demande d'homologation ou de validation adressée à l'administration est accompagnée des avis rendus par le comité d'entreprise, sans incidence sur la régularité de la procédure d'information et de consultation du comité. Par suite, dès lors qu'il  ne résulte pas de l'instruction, en dépit de la mesure diligentée par la 1ère chambre de la section du contentieux, qu'en l'espèce l'administration aurait été saisie d'une demande d'homologation ou de validation, l'Agence nationale pour la formation professionnelle des adultes n'est pas fondée à soutenir que le litige aurait perdu son objet avant l'introduction du pourvoi ou au cours de l'instance devant le Conseil d'Etat, du seul fait que le cabinet d'expertise comptable requérant aurait remis son rapport au comité central d'entreprise et que celui-ci aurait rendu ses avis.<br/>
<br/>
              Sur l'ordonnance attaquée :<br/>
<br/>
              3. Aux termes de l'article L. 1233-57-5 du code du travail : " Toute demande tendant, avant transmission de la demande de validation ou d'homologation, à ce qu'il soit enjoint à l'employeur de fournir les éléments d'information relatifs à la procédure en cours ou de se conformer à une règle de procédure prévue par les textes législatifs, les conventions collectives ou un accord collectif est adressée à l'autorité administrative. Celle-ci se prononce dans un délai de cinq jours ". Aux termes de l'article L. 1235-7-1 du même code : " L'accord collectif mentionné à l'article L. 1233-24-1, le document élaboré par l'employeur mentionné à l'article L. 1233-24-4, le contenu du plan de sauvegarde de l'emploi, les décisions prises par l'administration au titre de l'article L. 1233-57-5 et la régularité de la procédure de licenciement collectif ne peuvent faire l'objet d'un litige distinct de celui relatif à la décision de validation ou d'homologation mentionnée à l'article L. 1233-57-4. / Ces litiges relèvent de la compétence, en premier ressort, du tribunal administratif, à l'exclusion de tout autre recours administratif ou contentieux. / Le recours est présenté dans un délai de deux mois par l'employeur à compter de la notification de la décision de validation ou d'homologation, et par les organisations syndicales et les salariés à compter de la date à laquelle cette décision a été portée à leur connaissance conformément à l'article L. 1233-57-4 (...) / Le livre V du code de justice administrative est applicable ". Il résulte de ces dispositions que la juridiction administrative est seule compétente pour statuer sur un litige relatif à la communication par l'employeur de pièces demandées par l'expert-comptable désigné dans le cadre de la procédure de consultation du comité d'entreprise en cas de licenciements collectifs pour motif économique prévue à l'article L. 1233-30 du code du travail. <br/>
<br/>
              4. Par suite, en jugeant que la demande du cabinet d'expertise comptable mandaté par le comité d'entreprise de l'AFPA était manifestement insusceptible de se rattacher à un litige relevant de la juridiction administrative, le juge des référés a commis une erreur de droit. Le requérant est, dès lors, fondé, sans qu'il soit besoin d'examiner l'autre moyen de son pourvoi, à demander l'annulation de l'ordonnance qu'il attaque.<br/>
<br/>
              5. Il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au titre de la procédure de référé engagée, en application des dispositions de l'article L. 821-2 du code de justice administrative.<br/>
<br/>
              6. Il résulte des dispositions combinées des articles L. 1233-57-5 et  L. 1235-7-1 du code du travail, citées au point 3, qu'une demande tendant à ce qu'il soit enjoint à l'employeur de communiquer des pièces à l'expert-comptable désigné dans le cadre de la procédure de consultation du comité d'entreprise en cas de licenciements collectifs pour motif économique prévue à l'article L. 1233-30 du code du travail ne peut être adressée qu'à l'autorité administrative et ne peut faire l'objet d'un litige distinct du litige relatif à la décision de validation ou d'homologation mentionnée à l'article L. 1233-57-4 de ce code. Par suite, en l'absence de tout litige relatif à une décision de validation ou d'homologation non encore intervenue à la date de la présente décision, la demande présentée par le cabinet d'expertise comptable APEX devant le tribunal administratif de Montreuil ne peut qu'être rejetée comme irrecevable.<br/>
<br/>
              7. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à la charge de l'Agence nationale pour la formation professionnelle des adultes, qui n'est pas, dans la présente instance, la partie perdante. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge du cabinet d'expertise comptable APEX une somme de 3 000 euros au titre des frais exposés par cette agence et non compris dans les dépens.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er :  L'ordonnance du juge des référés du tribunal administratif de Montreuil du 12 février 2019 est annulée.<br/>
Article 2 : La demande présentée par le cabinet d'expertise comptable APEX devant le juge des référés du tribunal administratif de Montreuil est rejetée.<br/>
Article 3 : Le cabinet d'expertise comptable APEX versera à l'Agence nationale pour la formation professionnelle des adultes une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Les conclusions du cabinet d'expertise comptable APEX présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée au cabinet d'expertise comptable APEX et à l'Agence nationale pour la formation professionnelle des adultes.<br/>
Copie en sera adressée à la ministre du travail.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">17-03-01-01 COMPÉTENCE. RÉPARTITION DES COMPÉTENCES ENTRE LES DEUX ORDRES DE JURIDICTION. COMPÉTENCE DÉTERMINÉE PAR DES TEXTES SPÉCIAUX. ATTRIBUTIONS LÉGALES DE COMPÉTENCE AU PROFIT DES JURIDICTIONS ADMINISTRATIVES. - LITIGE RELATIF À LA COMMUNICATION PAR L'EMPLOYEUR DE PIÈCES DEMANDÉES PAR L'EXPERT-COMPTABLE DÉSIGNÉ DANS LE CADRE DE LA PROCÉDURE DE CONSULTATION DU COMITÉ D'ENTREPRISE EN CAS DE LICENCIEMENTS COLLECTIFS POUR MOTIF ÉCONOMIQUE - COMPÉTENCE DE LA JURIDICTION ADMINISTRATIVE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">66-07 TRAVAIL ET EMPLOI. LICENCIEMENTS. - DEMANDE TENDANT À CE QU'IL SOIT ENJOINT À L'EMPLOYEUR DE COMMUNIQUER LES PIÈCES DEMANDÉES PAR L'EXPERT-COMPTABLE DÉSIGNÉ DANS LE CADRE DE LA PROCÉDURE DE CONSULTATION DU COMITÉ D'ENTREPRISE EN CAS DE LICENCIEMENTS COLLECTIFS POUR MOTIF ÉCONOMIQUE (ART. L. 1233-30 DU CODE DU TRAVAIL) - 1) LITIGE RELEVANT DE LA COMPÉTENCE DE LA JURIDICTION ADMINISTRATIVE - 2) DEMANDE NE POUVANT ÊTRE ADRESSÉE QU'À L'AUTORITÉ ADMINISTRATIVE ET NE POUVANT FAIRE L'OBJET D'UN LITIGE DISTINCT DU LITIGE RELATIF À LA DÉCISION DE VALIDATION OU D'HOMOLOGATION MENTIONNÉE À L'ARTICLE L. 1233-57-4 DU CODE DU TRAVAIL.
</SCT>
<ANA ID="9A"> 17-03-01-01 Il résulte des articles L. 1233-57-5 et L. 1235-7-1 du code du travail que la juridiction administrative est seule compétente pour statuer sur un litige relatif à la communication par l'employeur de pièces demandées par l'expert-comptable désigné dans le cadre de la procédure de consultation du comité d'entreprise en cas de licenciements collectifs pour motif économique prévue à l'article L. 1233-30 du code du travail.</ANA>
<ANA ID="9B"> 66-07 1) Il résulte des articles L. 1233-57-5 et L. 1235-7-1 du code du travail que la juridiction administrative est seule compétente pour statuer sur un litige relatif à la communication par l'employeur de pièces demandées par l'expert-comptable désigné dans le cadre de la procédure de consultation du comité d'entreprise en cas de licenciements collectifs pour motif économique prévue à l'article L. 1233-30 du code du travail.... ,,2) Il résulte des dispositions combinées des articles L. 1233-57-5 et  L. 1235-7-1 du code du travail qu'une demande tendant à ce qu'il soit enjoint à l'employeur de communiquer des pièces à l'expert-comptable désigné dans le cadre de la procédure de consultation du comité d'entreprise en cas de licenciements collectifs pour motif économique prévue à l'article L. 1233-30 du code du travail ne peut être adressée qu'à l'autorité administrative et ne peut faire l'objet d'un litige distinct du litige relatif à la décision de validation ou d'homologation mentionnée à l'article L. 1233-57-4 de ce code. Par suite, en l'absence de tout litige relatif à une décision de validation ou d'homologation non encore intervenue à la date de la présente décision, la demande présentée par le cabinet d'expertise comptable requérant devant le tribunal administratif ne peut qu'être rejetée comme irrecevable.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
