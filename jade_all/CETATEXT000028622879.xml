<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028622879</ID>
<ANCIEN_ID>JG_L_2014_02_000000359776</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/62/28/CETATEXT000028622879.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème et 5ème sous-sections réunies, 05/02/2014, 359776, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-02-05</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>359776</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème et 5ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. David Moreau</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Rémi Keller</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:359776.20140205</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 29 mai 2012 au secrétariat du contentieux du Conseil d'Etat, présentée par Mme G...E..., demeurant..., Mme A...B..., demeurant..., M. F...H..., demeurant ... et M. C...D..., demeurant ... ; Mme E...et autres demandent au Conseil d'Etat d'annuler pour excès de pouvoir le décret n° 2012-435 du 29 mars 2012 modifiant le décret n° 2004-186 du 26 février 2004 portant création de l'université de technologie en sciences des organisations et de la décision de Paris-Dauphine ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de l'éducation ;<br/>
<br/>
              Vu le décret n° 2004-186 du 26 février 2004 ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. David Moreau, Maître des Requêtes, <br/>
<br/>
              - les conclusions de M. Rémi Keller, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant que l'université de technologie en sciences des organisations et de la décision de Paris-Dauphine a été créée par le décret n° 2004-186 du 26 février 2004 sous la forme statutaire de " grand établissement " prévue par l'article L. 717-1 du code de l'éducation ; que par un décret du 29 mars 2012, le Premier ministre a modifié le premier alinéa de l'article 5 du décret du 26 février 2004, relatif aux conditions de désignation du président de l'université Paris-Dauphine et à la durée de son mandat ; que, compte tenu de leur argumentation, Mme E... et autres doivent être regardés comme demandant l'annulation de ce décret uniquement en tant qu'il a modifié la durée du mandat du président ;<br/>
<br/>
              2. Considérant que le titre Ier du livre VII du code de l'éducation, relatif aux établissements publics à caractère scientifique, culturel et professionnel, comporte neuf chapitres, trois d'entre eux, les chapitres I, IV et IX, ayant une portée générale et comportant des dispositions applicables, en principe, à tous ces établissements publics, les autres chapitres étant relatifs à des catégories particulières d'établissements ; que l'article L. 717-1, article unique du chapitre VII spécifique aux " grands établissements ", disposait, dans sa rédaction en vigueur à la date du décret attaqué : " Des décrets en Conseil d'Etat fixent les règles particulières d'organisation et de fonctionnement des grands établissements dans le respect des principes d'autonomie et de démocratie définis par le présent titre. / Ils peuvent déroger aux dispositions des articles L. 711-1, L. 711-4, L. 711-5, L. 711-7, L. 711-8, L. 714-2, L. 719-1, L. 719-2 à L. 719-5, L. 719-7 à L. 719-11 en fonction des caractéristiques propres de chacun de ces établissements. / Les dispositions des articles L. 712-4, L. 811-5, L. 811-6, L. 952-7 à L. 952-9 sont applicables aux établissements mentionnés au présent article, sous réserve des dérogations fixées par décret en Conseil d'Etat, compte tenu de leurs caractéristiques propres " ; qu'il résulte de ces dispositions que les règles communes à l'ensemble des établissements publics à caractère scientifique, culturel et professionnel qui figurent aux chapitres I, IV et IX du titre Ier du livre VII, sont applicables aux grands établissements, le pouvoir réglementaire pouvant seulement déroger aux articles de ces chapitres énoncés au deuxième alinéa de l'article L. 717-1 lorsque les caractéristiques propres de l'établissement le justifient ; que les règles propres aux autres catégories d'établissements, qui figurent aux chapitres II, III, V, VI et VIII, ne sont applicables aux grands établissements que dans la mesure où elles sont mentionnées par le troisième alinéa de l'article L. 717-1, le pouvoir réglementaire pouvant toujours y déroger si les caractéristiques propres de l'établissement le justifient ;<br/>
<br/>
              3. Considérant, en premier lieu, que le deuxième alinéa de l'article L. 711-1 du code de l'éducation, qui est applicable aux grands établissements, se borne à prévoir que les établissements publics à caractère scientifique, culturel et professionnel " sont gérés de façon démocratique avec le concours de l'ensemble des personnels, des étudiants et de personnalités extérieures " ; qu'il n'implique pas que le conseil d'administration d'un tel établissement soit informé des projets de décrets relatifs aux statuts de l'établissement élaborés par l'autorité de tutelle ; que, par suite, le moyen tiré de ce que le conseil d'administration de l'université Paris-Dauphine aurait dû être informé du contenu du décret litigieux préalablement à son adoption doit être écarté ;<br/>
<br/>
              4. Considérant, en deuxième lieu, que si les requérants soutiennent que le décret attaqué ne pouvait légalement déroger à l'article L. 712-2 du code de l'éducation, qui prévoyait, dans sa rédaction en vigueur à la date du décret attaqué, que le mandat des présidents d'université, d'une durée de quatre ans, " expire à l'échéance du mandat des représentants élus des personnels du conseil d'administration ", il résulte de ce qui a été dit au point 2 que cet article, qui figure au chapitre II du titre VII, lequel est relatif aux universités, n'est pas applicable aux grands établissements ; que, par suite, ce moyen est inopérant ;<br/>
<br/>
              5. Considérant, en troisième lieu, que le décret contesté n'est pas fondé sur le II de l'article L. 711-4 du code de l'éducation, permettant l'expérimentation de nouveaux modes d'organisation et d'administration dans les établissements publics à caractère scientifique, culturel et professionnel ; que les requérants ne peuvent donc utilement soutenir que les conditions prévues par ces dispositions ne sont pas satisfaites ;<br/>
<br/>
              6. Considérant, en dernier lieu, que si les requérants excipent de l'illégalité du décret n° 2010-1035 du 1er septembre 2010 ayant modifié l'article 27 du décret du 26 février 2004, le décret contesté ne constitue pas une mesure d'application de ce texte ni n'a été pris sur son fondement ; que, par suite, l'exception d'illégalité soulevée ne peut qu'être écartée ; <br/>
<br/>
              7. Considérant qu'il résulte de ce qui précède, et sans qu'il soit besoin de statuer sur la fin de non-recevoir opposée par l'université, que les requérants ne sont pas fondés à demander l'annulation du décret attaqué ; <br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er : La requête de Mme E...et autres est rejetée.<br/>
<br/>
Article 2 : La présente décision sera notifiée à Mme G...E..., à Mme A...B..., à M. F...H..., à M. C...D..., à l'université Paris-Dauphine, au Premier ministre et à la ministre de l'enseignement supérieur et de la recherche.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
