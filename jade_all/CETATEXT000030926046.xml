<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030926046</ID>
<ANCIEN_ID>JG_L_2015_07_000000367567</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/92/60/CETATEXT000030926046.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème / 8ème SSR, 22/07/2015, 367567</TITRE>
<DATE_DEC>2015-07-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>367567</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème / 8ème SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP THOUIN-PALAT, BOUCARD</AVOCATS>
<RAPPORTEUR>M. Christophe Pourreau</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emmanuelle Cortot-Boucher</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:367567.20150722</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Le 14 mai 2010, la société Halliburton Manufactoring and Services France a demandé au tribunal administratif de Pau de la décharger de l'obligation de payer la somme de 185 372 euros résultant du titre de perception n° 2009/2572 émis par le trésorier-payeur général des Pyrénées-Atlantiques le 27 novembre 2009. Par un jugement n° 1000934 du 29 novembre 2011, le tribunal administratif de Pau a fait droit à la demande de la société.<br/>
<br/>
              Par un arrêt n° 12BX00231,12BX00592 du 5 février 2013, la cour administrative d'appel de Bordeaux, statuant sur le recours présenté par le ministre du budget, des comptes publics et de la réforme de l'Etat a, premièrement, annulé le jugement du tribunal administratif, deuxièmement, dit qu'il n'y avait pas lieu de statuer sur le recours du ministre en tant qu'il tendait au sursis à l'exécution de ce jugement, troisièmement, rejeté la demande de décharge de la société et, quatrièmement, rejeté les conclusions de la société, présentées par la voie de l'appel incident, tendant à ce qu'il soit enjoint au ministre du budget de restituer les sommes versées, assorties des intérêts moratoires.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 10 avril et 10 juillet 2013 et le 27 janvier 2015 au secrétariat du contentieux du Conseil d'Etat, la société Halliburton Manufactoring and Services France (HMS France) demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler les articles 1er et 2 de l'arrêt de la cour administrative d'appel de Bordeaux du 5 février 2013 ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 5 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le traité sur le fonctionnement de l'Union européenne ;<br/>
              - le règlement (CE) n° 1999/659 du Conseil du 22 mars 1999 ;<br/>
              - le décret n° 62-1587 du 29 décembre 1962 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Christophe Pourreau, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Emmanuelle Cortot-Boucher, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Thouin-Palat, Boucard, avocat de la société Halliburton Manufactoring and Services France ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond qu'au titre des exercices clos les 31 mars 1997 et 31 mars 1998, la société PES France, qui a été absorbée par la société Halliburton Manufactoring and Services France (HMS France) avec effet au 1er janvier 2002, a bénéficié de l'exonération d'impôt sur les sociétés en faveur de la reprise d'entreprises en difficulté prévue à l'article 44 septies du code général des impôts ; que, par une décision 2004/343/CE du 16 décembre 2003, la Commission européenne a déclaré que les exonérations octroyées en application de cet article, autres que celles qui remplissent les conditions d'octroi des aides de minimis et des aides à finalité régionale, constituaient des aides d'Etat illégales et ordonné la récupération sans délai des aides versées ; que, par l'arrêt rendu le 13 novembre 2008 dans l'affaire C-214/07, Commission c/ France, la Cour de justice des Communautés européennes a jugé que la République française avait manqué à ses obligations de recouvrement de ces aides ; que, le 27 novembre 2009, le trésorier-payeur général des Pyrénées-Atlantiques a émis à l'encontre de la société Halliburton Manufactoring and Services France un titre de perception d'un montant de 185 372 euros, correspondant aux cotisations d'impôt sur les sociétés et de contribution additionnelle à l'impôt sur les sociétés dont la société PES France avait été exonérée au titre des exercices clos les 31 mars 1997 et 31 mars 1998 en application de l'article 44 septies du code général des impôts, diminuées des aides de minimis et des aides à finalité régionale dont elle pouvait bénéficier, et majorées des intérêts dus ; que, le 29 novembre 2011, le tribunal administratif de Pau, faisant droit à la demande de la société, l'a déchargée de l'obligation de payer la somme de 185 372 euros ; que, par un arrêt du 5 février 2013, contre lequel la société se pourvoit en cassation, la cour administrative d'appel de Bordeaux, faisant droit à l'appel du ministre du budget, des comptes publics et de la réforme de l'Etat, a annulé le jugement du tribunal administratif, rejeté la demande de la société et rejeté ses conclusions, présentées par la voie de l'appel incident, tendant à la restitution des sommes versées, assorties des intérêts moratoires ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article 81 du décret du 29 décembre 1962 portant règlement général sur la comptabilité publique, dans sa rédaction applicable au litige : " Tout ordre de recette doit indiquer les bases de la liquidation " ; qu'ainsi, l'Etat ne peut mettre en recouvrement une créance sans indiquer, soit dans le titre de perception lui-même, soit par une référence précise à un document joint à ce titre ou précédemment adressé au débiteur, les bases et les éléments de calcul sur lesquels il s'est fondé pour déterminer le montant de la créance ;<br/>
<br/>
              3. Considérant qu'il ressort des énonciations de l'arrêt attaqué que le titre de perception litigieux indique que le montant de 185 372 euros réclamé à la société correspond à l'aide que la société PES France a perçue en 1997 et 1998 au titre de l'article 44 septies du code général des impôts et que la Commission européenne a déclarée incompatible avec le marché commun par sa décision 2004/343/CE du 16 décembre 2003, ainsi qu'à des intérêts calculés conformément au règlement n° 794/2004/CE de la Commission du 21 avril 2004 ; qu'en déduisant de ces mentions que le titre de perception litigieux satisfaisait aux exigences de l'article 81 du décret du 29 décembre 1962, alors qu'elles ne permettaient pas à la société de connaître les modalités de calcul de la créance de l'Etat, la cour administrative d'appel a commis une erreur de droit ; que la société Halliburton Manufactoring and Services France est fondée, pour ce motif, et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, à demander l'annulation des articles 1er et 2 de l'arrêt attaqué ;<br/>
<br/>
              4. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              Sur le recours du ministre du budget, des comptes publics et de la réforme de l'Etat :<br/>
<br/>
              5. Considérant qu'aux termes de l'article 14 du règlement n° 659/1999/CE du 22 mars 1999 portant modalités d'application de l'article 93 du traité CE, devenu l'article 108 du traité sur le fonctionnement de l'Union européenne : " 1. En cas de décision négative concernant une aide illégale, la Commission décide que l'Etat membre concerné prend toutes les mesures nécessaires pour récupérer l'aide auprès de son bénéficiaire (ci-après dénommée " décision de récupération "). La Commission n'exige pas la récupération de l'aide si, ce faisant, elle allait à l'encontre d'un principe général du droit communautaire./ (...)/3. (...) la récupération s'effectue sans délai et conformément aux procédures prévues par le droit national de l'Etat membre concerné, pour autant que ces dernières permettent l'exécution immédiate et effective de la décision de la Commission. A cette fin et en cas de procédure devant les tribunaux nationaux, les Etats membres concernés prennent toutes les mesures prévues par leurs systèmes juridiques respectifs, y compris les mesures provisoires, sans préjudice du droit communautaire " :<br/>
<br/>
              6. Considérant qu'il résulte de ce qui a été dit au point 3 que le titre de perception litigieux ne satisfaisait pas aux exigences de motivation de l'article 81 du décret du 29 décembre 1962 ; que, toutefois, ce vice peut être régularisé par l'émission d'un nouveau titre de perception ; qu'ainsi, les dispositions de l'article 14, paragraphe 3, du règlement (CE) du 22 mars 1999, qui impliquent de concilier le respect des procédures prévues par le droit national avec l'exigence de permettre l'exécution effective et immédiate de la décision de la Commission, ne font pas obstacle, contrairement à ce que soutient le ministre, à l'annulation du titre de perception litigieux ; que, dès lors, le ministre du budget, des comptes publics et de la réforme de l'Etat n'est pas fondé à se plaindre de ce que, par le jugement attaqué, le tribunal administratif de Pau a annulé ce titre de perception ;<br/>
<br/>
              Sur les conclusions d'appel incident de la société Halliburton Manufactoring and Services France tendant à ce qu'il soit ordonné à l'administration de lui reverser les sommes litigieuses avec intérêts :<br/>
<br/>
              7. Considérant, en premier lieu, qu'aux termes de l'article 15 du règlement du 22 mars 1999 : " 1. Les pouvoirs de la Commission en matière de récupération de l'aide sont soumis à un délai de prescription de dix ans. / 2. Le délai de prescription commence le jour où l'aide illégale est accordée au bénéficiaire, à titre d'aide individuelle ou dans le cadre d'un régime d'aide. Toute mesure prise par la Commission ou un Etat membre, agissant à la demande de la Commission, à l'égard de l'aide illégale interrompt le délai de prescription. Chaque interruption fait courir de nouveau le délai. Le délai de prescription est suspendu aussi longtemps que la décision de la Commission fait l'objet d'une procédure devant la Cour de justice des Communautés européennes. (...) " ; qu'après avoir notamment été interrompue par la décision de la Commission européenne du 16 décembre 2003, la prescription a de nouveau été interrompue par le recours en manquement introduit par la Commission européenne le 23 avril 2007 ; qu'un nouveau délai de dix ans a commencé à courir à compter de l'intervention de l'arrêt de la Cour de justice des Communautés européennes du 13 novembre 2008 ; que le moyen tiré de ce que la créance de l'Etat à l'égard de la société Halliburton Manufactoring and Services France serait prescrite doit, par suite, être écarté ;<br/>
<br/>
              8. Considérant, en deuxième lieu, qu'aux termes de l'article 14, paragraphe 2, du même règlement : " L'aide à récupérer en vertu d'une décision de récupération comprend des intérêts qui sont calculés sur la base d'un taux approprié fixé par la Commission. Ces intérêts courent à compter de la date à laquelle l'aide illégale a été mise à la disposition du bénéficiaire jusqu'à celle de sa récupération. (...) " ; qu'il résulte de ces dispositions que le paiement d'intérêts n'est subordonné à aucune condition relative aux modalités de l'action en restitution de l'aide d'Etat illégale ; que le moyen tiré de ce que les intérêts ne seraient pas dus au motif que le retard pris dans la récupération de l'aide serait imputable à l'Etat doit, par suite, être écarté ;<br/>
<br/>
              9. Considérant, en troisième lieu, qu'il résulte de l'instruction que la société Halliburton Manufactoring and Services France a repris les actifs et poursuivi l'activité économique de la société PES France ; que, par suite, le moyen tiré de ce que la société Halliburton Manufactoring and Services France ne peut être réputée avoir bénéficié de l'aide d'Etat illégale doit être écarté ;<br/>
<br/>
              10. Considérant qu'il résulte de ce qui précède que l'appel incident de la société Halliburton Manufactoring and Services France ne peut, en tout état de cause, qu'être rejeté ; <br/>
<br/>
              Sur les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              11. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 3 000 euros, à verser à la société Halliburton Manufactoring and Services France, au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Les articles 1er et 2 de l'arrêt de la cour administrative d'appel de Bordeaux du 5 février 2013 sont annulés.<br/>
Article 2 : Le recours du ministre du budget, des comptes publics et de la réforme de l'Etat est rejeté.<br/>
Article 3 : Les conclusions d'appel incident présentées par la société Halliburton Manufactoring and Services France devant la cour administrative d'appel de Bordeaux sont rejetées.<br/>
Article 4 : L'Etat versera à la société Halliburton Manufactoring and Services France la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 5 : La présente décision sera notifiée à la société Halliburton Manufactoring and Services France et au ministre des finances et des comptes publics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-03-01-02 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - FORME ET PROCÉDURE. QUESTIONS GÉNÉRALES. MOTIVATION. - RÉCUPÉRATION D'UNE AIDE D'ETAT - EMISSION PAR LES AUTORITÉS FRANÇAISES D'UN TITRE DE PERCEPTION - OPÉRANCE D'UN MOYEN DE DÉFAUT DE MOTIVATION - EXISTENCE [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">14-05-04 COMMERCE, INDUSTRIE, INTERVENTION ÉCONOMIQUE DE LA PUISSANCE PUBLIQUE. DÉFENSE DE LA CONCURRENCE. AIDES D'ETAT. - RÉCUPÉRATION - EMISSION PAR LES AUTORITÉS FRANÇAISES D'UN TITRE DE PERCEPTION - OPÉRANCE D'UN MOYEN DE DÉFAUT DE MOTIVATION - EXISTENCE [RJ1].
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">15-05-06-02 COMMUNAUTÉS EUROPÉENNES ET UNION EUROPÉENNE. RÈGLES APPLICABLES. DROIT DE LA CONCURRENCE. RÈGLES APPLICABLES AUX ÉTATS (AIDES). - RÉCUPÉRATION D'UNE AIDE D'ETAT - EMISSION PAR LES AUTORITÉS FRANÇAISES D'UN TITRE DE PERCEPTION - OPÉRANCE D'UN MOYEN DE DÉFAUT DE MOTIVATION - EXISTENCE [RJ1].
</SCT>
<SCT ID="8D" TYPE="PRINCIPAL">18-03-02-01-01 COMPTABILITÉ PUBLIQUE ET BUDGET. CRÉANCES DES COLLECTIVITÉS PUBLIQUES. RECOUVREMENT. PROCÉDURE. ÉTAT EXÉCUTOIRE. - RÉCUPÉRATION D'UNE AIDE D'ETAT - EMISSION PAR LES AUTORITÉS FRANÇAISES D'UN TITRE DE PERCEPTION - OPÉRANCE D'UN MOYEN DE DÉFAUT DE MOTIVATION - EXISTENCE [RJ1].
</SCT>
<ANA ID="9A"> 01-03-01-02 Le vice tiré du défaut de motivation d'un titre de perception émis pour la récupération d'une aide d'Etat peut être régularisé par l'émission d'un nouveau titre de perception. Par suite, les dispositions de l'article 14, paragraphe 3, du règlement n° 659/1999/CE du 22 mars 1999, qui impliquent de concilier le respect des procédures prévues par le droit national avec l'exigence d'exécution effective et immédiate de la décision de récupération prise par la Commission, ne font pas obstacle à l'annulation du titre de perception litigieux. Le moyen tiré d'un tel vice est dès lors opérant à l'appui du recours dirigé contre ce titre.</ANA>
<ANA ID="9B"> 14-05-04 Le vice tiré du défaut de motivation d'un titre de perception émis pour la récupération d'une aide d'Etat peut être régularisé par l'émission d'un nouveau titre de perception. Par suite, les dispositions de l'article 14, paragraphe 3, du règlement n° 659/1999/CE du 22 mars 1999, qui impliquent de concilier le respect des procédures prévues par le droit national avec l'exigence d'exécution effective et immédiate de la décision de récupération prise par la Commission, ne font pas obstacle à l'annulation du titre de perception litigieux. Le moyen tiré d'un tel vice est dès lors opérant à l'appui du recours dirigé contre ce titre.</ANA>
<ANA ID="9C"> 15-05-06-02 Le vice tiré du défaut de motivation d'un titre de perception émis pour la récupération d'une aide d'Etat peut être régularisé par l'émission d'un nouveau titre de perception. Par suite, les dispositions de l'article 14, paragraphe 3, du règlement n° 659/1999/CE du 22 mars 1999, qui impliquent de concilier le respect des procédures prévues par le droit national avec l'exigence d'exécution effective et immédiate de la décision de récupération prise par la Commission, ne font pas obstacle à l'annulation du titre de perception litigieux. Le moyen tiré d'un tel vice est dès lors opérant à l'appui du recours dirigé contre ce titre.</ANA>
<ANA ID="9D"> 18-03-02-01-01 Le vice tiré du défaut de motivation d'un titre de perception émis pour la récupération d'une aide d'Etat peut être régularisé par l'émission d'un nouveau titre de perception. Par suite, les dispositions de l'article 14, paragraphe 3, du règlement n° 659/1999/CE du 22 mars 1999, qui impliquent de concilier le respect des procédures prévues par le droit national avec l'exigence d'exécution effective et immédiate de la décision de récupération prise par la Commission, ne font pas obstacle à l'annulation du titre de perception litigieux. Le moyen tiré d'un tel vice est dès lors opérant à l'appui du recours dirigé contre ce titre.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CJUE, 20 mai 2010, Scott SA et Kimberly Clark SAS, anciennement Kimberly Clark SNC c/ Ville d'Orléans, aff. C-210/09. Rappr., pour les aides agricoles, CE, Section, 13 mars 2015, Office de développement de l'économie agricole d'outre-mer, n° 364612, à publier au Recueil.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
