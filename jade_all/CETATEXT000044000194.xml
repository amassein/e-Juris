<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044000194</ID>
<ANCIEN_ID>JG_L_2021_08_000000455744</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/00/01/CETATEXT000044000194.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, formation collégiale, 25/08/2021, 455744, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-08-25</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>455744</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés, formation collégiale</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROCHETEAU, UZAN-SARANO</AVOCATS>
<RAPPORTEUR>M. B. Dacosta</RAPPORTEUR>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2021:455744.20210825</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
              I. Sous le n° 455744, par une requête enregistrée le 20 août 2021 au secrétariat du contentieux du Conseil d'Etat, M. N... G... demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative :<br/>
<br/>
              1°) de l'admettre au bénéfice de l'aide juridictionnelle provisoire ; <br/>
<br/>
              2°) d'enjoindre au Premier ministre, au ministre de l'Europe et des affaires étrangères et au ministre de l'intérieur de compléter le dispositif de rapatriement annoncé par le Président de la République pour permettre aux personnes pouvant bénéficier du droit à la réunification familiale d'être rapatriées en France grâce au pont aérien et aux rotations en cours depuis l'aéroport de Kaboul et de prendre toute mesure de nature à assurer le respect de leur droit à la réunification familiale ; <br/>
<br/>
              3°) d'enjoindre au Premier ministre, au ministre de l'Europe et des affaires étrangères et au ministre de l'intérieur de prévoir, à titre principal, que la vérification des documents d'identité, des documents de voyage et des actes d'état civil démontrant la réalité des liens familiaux s'effectue sur le territoire afghan et de façon accélérée par le personnel diplomatique français, à titre subsidiaire, dans le cas où la situation ne le permettrait pas, que cette vérification soit réalisée en France par les services diplomatiques et de l'Office français de protection des réfugiés et des apatrides ou, à titre encore subsidiaire, que la vérification soit confiée à d'autres représentations diplomatiques accessibles aux familles ; <br/>
<br/>
              4°) d'enjoindre au Premier ministre, au ministre de l'Europe et des affaires étrangères et au ministre de l'intérieur qu'à l'issue de cette instruction accélérée des demandes de visa au titre de la réunification familiale, les titulaires d'un tel visa soient rapatriés vers la France grâce au dispositif de rotation des deux avions militaires mis en place et si nécessaire par la mise à disposition de moyens aériens alliés ou français supplémentaires ;<br/>
<br/>
              5°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative et de l'article 37 de la loi du 10 juillet 1991. <br/>
<br/>
<br/>
<br/>
              Il soutient que :<br/>
              - le Conseil d'Etat est compétent en premier et dernier ressort pour connaître de sa requête en ce qu'elle tend à l'édiction de mesures de portée générale ; <br/>
              - la condition d'urgence est satisfaite dès lors qu'il est séparé depuis plus de deux ans de sa famille, en méconnaissance des impératifs de la directive du 22 septembre 2003 relative au regroupement familial, que la situation se dégrade de façon alarmante et très rapide en Afghanistan, que son fils risque d'être privé de scolarisation et est, surtout, menacé dans son intégrité physique et mentale, se trouvant avec sa mère dans une situation de grande vulnérabilité, qui laisse craindre pour leur vie et appelle leur évacuation immédiate ; <br/>
              - il est porté une atteinte grave et manifestement illégale à leur droit à mener une vie familiale normale, compte tenu du retard déraisonnable pris dans l'instruction des demandes de visas et de l'impossibilité d'en enregistrer de nouvelles depuis un an et demi, en méconnaissance du principe de continuité du service public.<br/>
<br/>
              La requête a été communiquée au Premier ministre et au ministre de l'Europe et des affaires étrangères, qui n'ont pas produit de mémoire.<br/>
<br/>
<br/>
<br/>
              II. Sous le n° 455745, par une requête enregistrée le 20 août 2021 au secrétariat du contentieux du Conseil d'Etat, M. K... B..., son épouse et ses enfants mineurs et M. D... C..., son épouse et ses enfants mineurs demandent au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative :<br/>
<br/>
              1°) de les admettre au bénéfice de l'aide juridictionnelle provisoire ; <br/>
<br/>
              2°) d'enjoindre au Premier ministre, au ministre de l'Europe et des affaires étrangères et au ministre de l'intérieur de compléter le dispositif de rapatriement annoncé par le Président de la République pour permettre aux personnes pouvant bénéficier du droit à la réunification familiale d'être rapatriées en France grâce au pont aérien et aux rotations en cours depuis l'aéroport de Kaboul et de prendre toute mesure de nature à assurer le respect de leur droit à la réunification familiale ; <br/>
<br/>
              3°) d'enjoindre au Premier ministre, au ministre de l'Europe et des affaires étrangères et au ministre de l'intérieur de prévoir, à titre principal, que la vérification des documents d'identité, des documents de voyage et des actes d'état civil démontrant la réalité des liens familiaux s'effectue sur le territoire afghan et de façon accélérée par le personnel diplomatique français, à titre subsidiaire, dans le cas où la situation ne le permettrait pas, que cette vérification soit réalisée en France par les services diplomatiques et de l'Office français de protection des réfugiés et des apatrides ou, à titre encore subsidiaire, que la vérification soit confiée à d'autres représentations diplomatiques accessibles aux familles ;  <br/>
<br/>
              4°) d'enjoindre au Premier ministre, au ministre de l'Europe et des affaires étrangères et au ministre de l'intérieur qu'à l'issue de cette instruction accélérée des demandes de visa au titre de la réunification familiale, les titulaires d'un tel visa soient rapatriés vers la France grâce au dispositif de rotation des deux avions militaires mis en place et si nécessaire par la mise à disposition de moyens aériens alliés ou français supplémentaires ;<br/>
<br/>
              5°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative et de l'article 37 de la loi du 10 juillet 1991. <br/>
<br/>
<br/>
<br/>
              Ils présentent les mêmes moyens que ceux invoqués à l'appui de la requête enregistrée sous le n° 455744. <br/>
<br/>
              La requête a été communiquée au Premier ministre et au ministre de l'Europe et des affaires étrangères, qui n'ont pas produit de mémoire.<br/>
<br/>
<br/>
<br/>
              III. Sous le n° 455746, par une requête, enregistrée au secrétariat du contentieux du Conseil d'Etat, M. O... J... demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative :<br/>
<br/>
              1°) de l'admettre au bénéfice de l'aide juridictionnelle provisoire ; <br/>
<br/>
              2°) d'enjoindre au Premier ministre, au ministre de l'Europe et des affaires étrangères et au ministre de l'intérieur de compléter le dispositif de rapatriement annoncé par le Président de la République pour permettre aux personnes pouvant bénéficier du droit à la réunification familiale d'être rapatriées en France grâce au pont aérien et aux rotations en cours depuis l'aéroport de Kaboul et de prendre toute mesure de nature à assurer le respect de leur droit à la réunification familiale ; <br/>
<br/>
              3°) d'enjoindre au Premier ministre, au ministre de l'Europe et des affaires étrangères et au ministre de l'intérieur de prévoir, à titre principal, que la vérification des documents d'identité, des documents de voyage et des actes d'état civil démontrant la réalité des liens familiaux s'effectue sur le territoire afghan et de façon accélérée par le personnel diplomatique français, à titre subsidiaire, dans le cas où la situation ne le permettrait pas, que cette vérification soit réalisée en France par les services diplomatiques et de l'Office français de protection des réfugiés et des apatrides ou, à titre encore subsidiaire, que la vérification soit confiée à d'autres représentations diplomatiques accessibles aux familles ;  <br/>
<br/>
              4°) d'enjoindre au Premier ministre, au ministre de l'Europe et des affaires étrangères et au ministre de l'intérieur qu'à l'issue de cette instruction accélérée des demandes de visa au titre de la réunification familiale, les titulaires d'un tel visa soient rapatriés vers la France grâce au dispositif de rotation des deux avions militaires mis en place et si nécessaire par la mise à disposition de moyens aériens alliés ou français supplémentaires ;<br/>
<br/>
              5°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative et de l'article 37 de la loi du 10 juillet 1991. <br/>
<br/>
<br/>
<br/>
              Il présente les mêmes moyens que ceux invoqués à l'appui de la requête enregistrée sous le n° 455744.<br/>
<br/>
              Par un mémoire en défense, enregistré le 24 août 2021, le ministre de l'intérieur conclut au rejet des requêtes nos 455744, 455745 et 455746. Il soutient que le Conseil d'Etat n'est pas compétent pour connaître de ces requêtes, que la condition d'urgence n'est pas satisfaite et que les moyens soulevés ne sont pas fondés. <br/>
<br/>
              La requête a été communiquée au Premier ministre et au ministre de l'Europe et des affaires étrangères, qui n'ont pas produit de mémoire.<br/>
              Par quatre mémoires en intervention, enregistrés les 21, 23 et 24 août 2021, l'association Avocats pour la défense des droits des étrangers, le Groupe d'information et de soutien des immigrés et le Syndicat des avocats de France, l'association La Cimade, la Ligue française pour la défense des droits de l'Homme et le Conseil national des barreaux demandent au juge des référés du Conseil d'Etat de faire droit aux conclusions des requêtes nos 455744, 455745 et 455746. Ils s'associent aux moyens des requêtes.<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, M. G..., M. B... et autres et M. J... et, d'autre part, le Premier ministre, le ministre de l'Europe et des affaires étrangères et le ministre de l'intérieur ;<br/>
<br/>
              Ont été entendus lors de l'audience publique du 24 août 2021, à 14 heures : <br/>
<br/>
              - Me Uzan-Sarano, avocat au Conseil d'Etat et à la Cour de cassation, avocat des requérants ;<br/>
              - la représentante des requérants ;<br/>
              - les représentantes du ministre de l'intérieur ; <br/>
<br/>
              à l'issue de laquelle le juge des référés a reporté la clôture de l'instruction au 25 août à 16 heures.<br/>
              Vu le nouveau mémoire, enregistré le 25 août 2021, présenté par le ministre de l'intérieur, qui maintient ses conclusions ; il soutient que les mesures demandées sont inutiles compte tenu de la situation prévalant à Kaboul et que l'Etat accomplit toutes les diligences utiles pour procéder au rapatriement des personnes concernées ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu : <br/>
              - la Constitution, notamment son Préambule ; <br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - la directive (CE) 2003/86 du 22 septembre 2003 ; <br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              - la loi n° 71-1130 du 31 décembre 1971 ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. (...) ". <br/>
<br/>
              2. Les requêtes enregistrées sous les numéros 455744, 455745 et 455746 tendent à ce que le juge des référés du Conseil d'Etat, statuant sur le fondement des dispositions précitées de l'article L. 521-2 du code de justice administrative, ordonne les mesures nécessaires à la sauvegarde du droit des ressortissants afghans s'étant vus reconnaître la qualité de réfugié ou ayant obtenu le bénéfice de la protection subsidiaire à être rejoints par leur conjoint et leurs enfants mineurs, découlant de leur droit à mener une vie familiale normale, en vue de permettre l'acheminement de ces derniers en France par le pont aérien organisé à partir de l'aéroport de Kaboul depuis le 15 août 2021. Il y a lieu de les joindre pour statuer par une seule ordonnance. <br/>
<br/>
              Sur les interventions :<br/>
<br/>
              3. L'association Avocats pour la défense des droits des étrangers, le Groupe d'information et de soutien des immigrés, l'association La Cimade et la Ligue française pour la défense des droits de l'Homme justifient d'un intérêt suffisant pour intervenir au soutien des requêtes. Leurs interventions sont, par suite, recevables. <br/>
<br/>
              4. En revanche, si le Conseil national des barreaux fait valoir qu'il aurait notamment pour mission de défendre les intérêts des justiciables et le droit au recours effectif des usagers du service public, son objet statutaire, défini par l'article 21-1 de la loi du 31 décembre 1971 portant réforme de certaines professions judiciaires et juridiques, ne lui confère pas d'intérêt de nature à lui donner qualité pour intervenir à l'appui des requêtes. Par suite, son intervention n'est pas recevable. <br/>
<br/>
              Sur les demandes en référé : <br/>
<br/>
              5. Aux termes de l'article L. 561-2 du code de l'entrée et du séjour des étrangers et du droit d'aile : " Sauf si sa présence constitue une menace pour l'ordre public, le ressortissant étranger qui s'est vu reconnaître la qualité de réfugié ou qui a obtenu le bénéfice de la protection subsidiaire peut demander à bénéficier de son droit à être rejoint, au titre de la réunification familiale : / 1° Par son conjoint ou le partenaire avec lequel il est lié par une union civile, âgé d'au moins dix-huit ans, si le mariage ou l'union civile est antérieur à la date d'introduction de sa demande d'asile ; / 2° Par son concubin, âgé d'au moins dix-huit ans, avec lequel il avait, avant la date d'introduction de sa demande d'asile, une vie commune suffisamment stable et continue ; / 3° Par les enfants non mariés du couple, n'ayant pas dépassé leur dix-neuvième anniversaire. / (...) L'âge des enfants est apprécié à la date à laquelle la demande de réunification familiale a été introduite ". Aux termes de l'article L. 561-5 du même code : " Les membres de la famille d'un réfugié ou d'un bénéficiaire de la protection subsidiaire sollicitent, pour entrer en France, un visa d'entrée pour un séjour d'une durée supérieure à trois mois auprès des autorités diplomatiques et consulaires, qui statuent sur cette demande dans les meilleurs délais. Ils produisent pour cela les actes de l'état civil justifiant de leur identité et des liens familiaux avec le réfugié ou le bénéficiaire de la protection subsidiaire. / En l'absence d'acte de l'état civil ou en cas de doute sur leur authenticité, les éléments de possession d'état définis à l'article 311-1 du code civil et les documents établis ou authentifiés par l'Office français de protection des réfugiés et apatrides, sur le fondement de l'article L. 121-9 du présent code, peuvent permettre de justifier de la situation de famille et de l'identité des demandeurs. Les éléments de possession d'état font foi jusqu'à preuve du contraire. Les documents établis par l'office font foi jusqu'à inscription de faux ".<br/>
<br/>
              6. A la suite de la prise de contrôle de la ville de Kaboul par les Talibans, des évacuations en urgence aux fins d'acheminement vers la France ont été organisées à partir de l'aéroport international Hamid Karzaï. Cet aéroport est sécurisé par l'armée américaine, dont le retrait du pays est prévu le 31 août prochain dans le cadre des accords de Doha signés le 29 février 2020. Il résulte de l'instruction que, dans ce cadre, les autorités françaises sont en mesure d'assurer deux vols par jour, chacun pouvant transporter 250 personnes. L'organisation de telles opérations d'évacuation à partir d'un territoire étranger et de rapatriement vers la France n'est pas détachable de la conduite des relations internationales de la France. Par suite, la juridiction administrative n'est pas compétente pour connaître des demandes tendant à ce que le dispositif de rapatriement soit complété et à ce que des rapatriements soient ordonnés, alors même qu'est en cause l'évacuation de ressortissants afghans ayant vocation à bénéficier des dispositions citées au point 5. Les conclusions des requérants présentées à ce titre ne peuvent donc qu'être rejetées.<br/>
<br/>
              7. En revanche, leurs conclusions tendant à ce qu'il soit ordonné au ministre des affaires étrangères et au ministère de l'intérieur de prendre, en urgence, les mesures permettant aux ressortissants afghans pouvant bénéficier d'une réunification familiale de faire valoir leur droit par la délivrance d'un visa ou de toute autre mesure équivalente, ne peuvent être regardées comme échappant à la compétence que le juge des référés du Conseil d'Etat tient des dispositions de l'article L. 521-2 du code de justice administrative. <br/>
              8. L'activité du poste consulaire de Kaboul ayant cessé, et les demandes de visa ne pouvant être présentées par des ressortissants afghans, en l'état de la réglementation, que devant les postes consulaires de Téhéran et New Dehli, les requérants demandent, à cet effet, une adaptation de la procédure d'instruction et de délivrance des visas sollicités au titre de la réunification familiale pour permettre aux membres de leurs familles de bénéficier du pont aérien et des rotations organisées pour l'évacuation de ressortissants afghans vers la France. Il résulte toutefois de l'instruction et des échanges à l'audience que, dans le contexte prévalant depuis le 15 août 2021 à Kaboul, la détention d'un visa d'entrée en France n'est pas requise pour prétendre au bénéficie de ces opérations d'évacuation. Les personnes présentes à l'intérieur de la zone dédiée à la France dans l'enceinte de l'aéroport de Kaboul et éligibles à la réunification familiale, qu'elles soient ou non munies d'un visa, ont ainsi vocation à être prises en charge par les moyens militaires français, dans la mesure de leur disponibilité, en vue d'un transfert vers le territoire national, tant que la situation locale permet la poursuite de ces opérations. Dans ces conditions, en l'état de l'instruction, le défaut de délivrance de visas apparaît sans incidence, dans l'immédiat, sur l'exercice du droit des requérants à bénéficier de la réunification de leur famille. Par suite, les conclusions tendant à l'adaptation de la procédure de vérification des documents démontrant la réalité des liens familiaux en vue de la délivrance de visas ne peuvent qu'être rejetées.<br/>
<br/>
              9. Si les requérants demandent également qu'il soit enjoint aux autorités françaises à Kaboul de faire en sorte que les conjoints et enfants des ressortissants afghans titulaires du droit à réunification familiale qui parviennent aux abords de l'aéroport de Kaboul puissent y accéder, ces conclusions ont trait à l'organisation de l'évacuation de ressortissants afghans depuis l'aéroport de Kaboul qui, ainsi qu'il a été dit au point 6, relève de la conduite des relations internationales de la France, et ne ressortissent donc pas de la compétence du juge administratif. En tout état de cause, il résulte de l'instruction que les personnes qui se sont manifestées au préalable auprès de l'ambassade ou du centre de crise et de soutien du ministère de l'Europe et des affaires étrangères reçoivent, dans la mesure du possible, un SMS et, si elles se présentent à l'un des points d'accès à l'aéroport, doivent tenter de se signaler aux autorités françaises selon les indications qui leur ont été données. Dans ce contexte, qui a déjà donné lieu à 160 000 saisines par courriel ou par téléphone du centre de crise et de soutien, il ne résulte pas de l'instruction que les autorités françaises, en métropole ou sur place, n'auraient pas accompli, à la date de la présence ordonnance, les diligences qui leur incombent.<br/>
<br/>
              10. Il résulte de tout ce qui précède que les requêtes de M. G..., M. et Mme C..., M. et Mme B... et M. J... doivent être rejetées, y compris leurs conclusions présentées au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991, sans qu'il y ait lieu, dans les circonstances de l'espèce, de prononcer l'admission provisoire des requérants au bénéfice de l'aide juridictionnelle.<br/>
<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : L'intervention de l'association Avocats pour la défense des droits des étrangers, du Groupe d'information et de soutien des immigrés et du Syndicat des avocats de France, celle de l'association La Cimade et celle de la Ligue française pour la défense des droits de l'Homme sont admises.<br/>
Article 2 : L'intervention du Conseil national des barreaux n'est pas admise.<br/>
Article 3 : Les requêtes de M. G..., M. et Mme C..., M. B... et Mme L... et M. J... sont rejetées.<br/>
Article 4 : La présente ordonnance sera notifiée à M. N... G..., à M. D... C... , à M. K... B... , à M. O... J..., au ministre de l'Europe et des affaires étrangères et au ministre de l'intérieur.<br/>
Copie en sera adressée au Premier ministre. <br/>
              Délibéré à l'issue de la séance du 24 août 2021 où siégeaient : M. H... E..., présidant ; Mme A... I... et Mme F... M..., conseillères d'Etat, juges des référés. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
