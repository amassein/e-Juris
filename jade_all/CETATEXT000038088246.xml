<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038088246</ID>
<ANCIEN_ID>JG_L_2019_02_000000421694</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/08/82/CETATEXT000038088246.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème - 2ème chambres réunies, 01/02/2019, 421694, Publié au recueil Lebon</TITRE>
<DATE_DEC>2019-02-01</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>421694</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème - 2ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP SPINOSI, SUREAU</AVOCATS>
<RAPPORTEUR>M. Jean-Yves Ollier</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Olivier Henrard</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:421694.20190201</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B...C...A...a demandé au juge des référés du tribunal administratif de Paris, sur le fondement de l'article L. 521-1 du code de justice administrative, de l'admettre provisoirement à l'aide juridictionnelle, d'ordonner la suspension de l'exécution de la décision implicite du ministre des armées refusant de lui accorder la protection fonctionnelle jusqu'à ce qu'il soit statué au fond sur la légalité de cette décision, et d'enjoindre au ministre des armées de réexaminer sa demande dans un délai de 8 jours suivant la notification de l'ordonnance du tribunal administratif, sous astreinte de 500 euros par jour de retard. <br/>
<br/>
              Par une ordonnance n° 1807369 du 7 juin 2018, le juge des référés du tribunal administratif de Paris a admis M. A...à l'aide juridictionnelle à titre provisoire et a rejeté sa demande.<br/>
<br/>
              Par un pourvoi et trois nouveaux mémoires, enregistrés les 22 juin, 24 juillet, 18 octobre et 20 novembre 2018 au secrétariat du contentieux du Conseil d'Etat, M. A...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit aux demandes qu'il a présentées devant le juge des référés du tribunal administratif de Paris ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le règlement UE n° 604/2013 du Parlement européen et du Conseil du 26 juin 2013 ;<br/>
              - la loi n° 83-634 du 13 juillet 1983 ;<br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Yves Ollier, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Olivier Henrard, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Spinosi, Sureau, avocat de M.A....<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision ".<br/>
<br/>
              2. Il ressort des pièces du dossier soumis au juge des référés que M.A..., ressortissant afghan, a exercé entre les mois de septembre 2011 et septembre 2012 les fonctions d'interprète auprès des forces armées françaises alors déployées en Afghanistan. Les autorités françaises ont annoncé au mois de mai 2012 le retrait des forces françaises dans ce pays à partir du mois de juillet. M. A...a sollicité auprès des autorités consulaires françaises le 15 juillet 2015 la délivrance d'un visa de long séjour dans le cadre du dispositif de réinstallation des personnels civils de recrutement local (PCRL) employés par l'armée française en Afghanistan. Sa demande a été rejetée par une décision notifiée le 8 octobre 2015. La commission de recours contre les décisions de refus de visa en France a implicitement rejeté le recours qu'il a formé contre cette décision. Par un jugement n° 1602689 du 27 juin 2018, le tribunal administratif de Nantes a annulé cette décision et a enjoint au ministre de l'intérieur de réexaminer la situation administrative de M. A...dans un délai d'un mois à compter de la notification de ce jugement. Il ne ressort pas des pièces du dossier que l'administration s'est conformée à cette injonction.<br/>
<br/>
              3. Par une lettre du 22 septembre 2017, M.A..., qui séjourne en France depuis mai 2017 sans titre de séjour, a demandé à la ministre des armées de lui accorder la protection fonctionnelle, sous la forme notamment de la délivrance d'un titre de séjour. Cette demande étant restée sans réponse, il a demandé, par des lettres du 1er décembre 2017 et du 21 février 2018, la communication des motifs de la décision implicite de rejet qui lui a été opposée. Il a ensuite formé un recours contre ce refus de protection fonctionnelle devant le tribunal administratif de Paris. Il a parallèlement demandé au juge des référés de ce tribunal, sur le fondement de l'article L. 521-1 du code de justice administrative, d'en suspendre l'exécution. Par une ordonnance du 7 juin 2018, le juge des référés a rejeté cette demande. M. A...se pourvoit en cassation contre cette ordonnance. <br/>
<br/>
              Sur le pourvoi :<br/>
<br/>
              4. Il résulte d'un principe général du droit que, lorsqu'un agent public est mis en cause par un tiers à raison de ses fonctions, il incombe à la collectivité dont il dépend de le couvrir des condamnations civiles prononcées contre lui, dans la mesure où une faute personnelle détachable du service ne lui est pas imputable, de lui accorder sa protection dans le cas où il fait l'objet de poursuites pénales, sauf s'il a commis une faute personnelle, et, à moins qu'un motif d'intérêt général ne s'y oppose, de le protéger contre les menaces, violences, voies de fait, injures, diffamations ou outrages dont il est l'objet. Ce principe général du droit s'étend aux agents non-titulaires de l'Etat recrutés à l'étranger, alors même que leur contrat est soumis au droit local. La juridiction administrative est compétente pour connaître des recours contre les décisions des autorités de l'Etat refusant aux intéressés le bénéfice de cette protection. <br/>
<br/>
              5. Il ressort des pièces du dossier soumis au juge des référés que le contrat conclu pour une durée d'un an à compter du 1er septembre 2011 entre le ministre de la défense et M. A...ne précise pas le droit qui lui est applicable et se borne à renvoyer à " l'arrangement technique militaire entre la Force internationale d'assistance à la sécurité (FIAS) et l'administration intérimaire d'Afghanistan, ratifié le 2 janvier 2002 et amendé par les lettres du 22 novembre 2004 ". Le point 14 de l'annexe A de cet arrangement prévoit que la FIAS, dont faisait partie l'armée française, peut recruter du personnel local qui demeure soumis aux lois et règlements locaux. Par suite, le contrat de M. A...est soumis au droit afghan. Cependant, il résulte de ce qui a été dit au point 4 que la juridiction administrative française est compétente pour connaître du recours de l'intéressé contre la décision de la ministre des armées refusant de lui octroyer la protection fonctionnelle. <br/>
<br/>
              6. Lorsqu'il s'agit, compte tenu de circonstances très particulières, du moyen le plus approprié pour assurer la sécurité d'un agent étranger employé par l'Etat, la protection fonctionnelle peut exceptionnellement conduire à la délivrance d'un visa ou d'un titre de séjour à l'intéressé et à sa famille. Par suite, en jugeant qu'eu égard à l'indépendance des législations, la décision de la ministre des armées refusant d'octroyer la protection fonctionnelle à M. A...était sans lien avec l'examen de la possibilité d'octroyer un titre de séjour en France au requérant, le juge des référés du tribunal administratif de Paris a commis une erreur de droit. M. A... est dès lors fondé à demander l'annulation de l'ordonnance qu'il attaque.<br/>
<br/>
              7. Il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au titre de la procédure de référé engagée, en application de l'article L. 821-2 du code de justice administrative.<br/>
<br/>
              Sur la demande de suspension :<br/>
<br/>
              8. L'urgence justifie que soit prononcée la suspension d'un acte administratif lorsque l'exécution de celui-ci porte atteinte, de manière suffisamment grave et immédiate, à un intérêt public, à la situation du requérant ou aux intérêts qu'il entend défendre. <br/>
<br/>
              9. Il résulte de l'instruction que M. A...a servi en qualité d'interprète auprès des forces françaises au sein d'un groupement tactique inter-armes dans la province de Kapisa, de septembre 2011 à septembre 2012. Il ressort de ses fiches de paie qu'il a perçu notamment en janvier, en mars et en août 2012, des indemnités d'activité opérationnelle. Il fait valoir qu'il a fait l'objet de menaces en raison de sa qualité d'ancien auxiliaire de l'armée française, qui l'ont obligé à quitter l'Afghanistan, où l'armée et la police afghanes, mobilisées par la lutte contre différents groupes insurgés, ne sont pas en capacité de lui apporter une protection, et que faisant l'objet d'un arrêté de transfert, il risque d'être renvoyé vers son pays, dans la mesure où sa demande d'asile a été rejetée par les Pays-Bas.<br/>
<br/>
              10. Les risques dont fait état M. A...paraissent de nature à porter une atteinte grave et immédiate à sa situation. Il résulte toutefois de l'instruction, en particulier des éléments produits par la ministre des armées, que la préfecture de la Marne a délivré le 9 janvier 2019 une attestation de demande d'asile à M.A.... La délivrance de cette attestation, qui traduit l'exercice par la France de la faculté prévue par les dispositions de l'article 17, paragraphe 1 du règlement (UE) du 26 juin 2013, d'examiner sa demande de protection internationale, même si cet examen ne lui incombe pas en vertu des critères fixés dans ce règlement, fait obstacle à ce que l'arrêté du 6 septembre 2017 transférant l'intéressé aux autorités néerlandaises soit exécuté, la France étant désormais l'Etat membre responsable au sens de ce règlement. En application des dispositions de l'article L. 743-1 du code de l'entrée et du séjour des étrangers et du droit d'asile, le dépôt d'une demande auprès de l'Office français de protection des réfugiés et apatrides donne à l'intéressé le droit de se maintenir sur le territoire français jusqu'à la notification de la décision de l'office et s'il forme un recours contre cette décision jusqu'à la date de la lecture en audience publique de la décision de la Cour nationale du droit d'asile. Dans ces conditions, à la date à laquelle le Conseil d'Etat se prononce, et en l'état de l'instruction, la condition d'urgence doit être regardée comme n'étant pas remplie.<br/>
<br/>
              11. Il résulte de ce qui précède, sans qu'il soit besoin de se prononcer sur l'existence d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision contestée, que M. A...n'est pas fondé à demander la suspension de l'exécution de la décision implicite de la ministre des armées refusant de lui accorder la protection fonctionnelle. Par suite, ses conclusions tendant au prononcé d'une injonction et à l'application de l'article L. 761-1 du code de justice administrative ne peuvent qu'être rejetées.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'ordonnance du 7 juin 2018 du juge des référés du tribunal administratif de Paris est annulée. <br/>
Article 2 : La demande présentée par M. A...devant le juge des référés du tribunal administratif de Paris ainsi que ses conclusions présentées devant le Conseil d'Etat au titre de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
Article 3 : La présente décision sera notifiée à M. B...C...A...et à la ministre des armées.<br/>
Copie en sera adressée au ministre de l'Europe et des affaires étrangères, au ministre de l'action et des comptes publics et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-04-03-07-04 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - VIOLATION DIRECTE DE LA RÈGLE DE DROIT. PRINCIPES GÉNÉRAUX DU DROIT. PRINCIPES INTÉRESSANT L'ACTION ADMINISTRATIVE. GARANTIES DIVERSES ACCORDÉES AUX AGENTS PUBLICS. - PROTECTION FONCTIONNELLE (PGD) - 1) CHAMP D'APPLICATION - AGENTS NON-TITULAIRES DE L'ETAT RECRUTÉS À L'ÉTRANGER, ALORS MÊME QUE LEUR CONTRAT EST SOUMIS AU DROIT LOCAL - INCLUSION [RJ1] - 2) MISE EN &#140;UVRE - DÉLIVRANCE D'UNE AUTORISATION DE SÉJOUR LORSQU'IL S'AGIT DU MOYEN LE PLUS APPROPRIÉ POUR ASSURER LA SÉCURITÉ D'UN AGENT ÉTRANGER EMPLOYÉ PAR L'ETAT [RJ2].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">17-01-01 COMPÉTENCE. COMPÉTENCE DE LA JURIDICTION FRANÇAISE. EXISTENCE. - RECOURS CONTRE LES DÉCISIONS REFUSANT LE BÉNÉFICE DE LA PROTECTION FONCTIONNELLE AUX AGENTS NON TITULAIRES DE L'ETAT RECRUTÉS À L'ÉTRANGER, ALORS MÊME QUE LEUR CONTRAT EST SOUMIS AU DROIT LOCAL.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">335-01-02-02 ÉTRANGERS. SÉJOUR DES ÉTRANGERS. AUTORISATION DE SÉJOUR. OCTROI DU TITRE DE SÉJOUR. - MISE EN OEUVRE DE LA PROTECTION FONCTIONNELLE - DÉLIVRANCE D'UNE AUTORISATION DE SÉJOUR LORSQU'IL S'AGIT DU MOYEN LE PLUS APPROPRIÉ POUR ASSURER LA SÉCURITÉ D'UN AGENT ÉTRANGER EMPLOYÉ PAR L'ETAT [RJ2].
</SCT>
<SCT ID="8D" TYPE="PRINCIPAL">36-07-10-005 FONCTIONNAIRES ET AGENTS PUBLICS. STATUTS, DROITS, OBLIGATIONS ET GARANTIES. GARANTIES ET AVANTAGES DIVERS. PROTECTION CONTRE LES ATTAQUES. - PROTECTION FONCTIONNELLE (PGD) - 1) CHAMP D'APPLICATION - AGENTS NON-TITULAIRES DE L'ETAT RECRUTÉS À L'ÉTRANGER, ALORS MÊME QUE LEUR CONTRAT EST SOUMIS AU DROIT LOCAL - INCLUSION [RJ1] - 2) MISE EN &#140;UVRE - DÉLIVRANCE D'UNE AUTORISATION DE SÉJOUR LORSQU'IL S'AGIT DU MOYEN LE PLUS APPROPRIÉ POUR ASSURER LA SÉCURITÉ D'UN AGENT ÉTRANGER EMPLOYÉ PAR L'ETAT [RJ2].
</SCT>
<ANA ID="9A"> 01-04-03-07-04 1) Il résulte d'un principe général du droit que, lorsqu'un agent public est mis en cause par un tiers à raison de ses fonctions, il incombe à la collectivité dont il dépend de le couvrir des condamnations civiles prononcées contre lui, dans la mesure où une faute personnelle détachable du service ne lui est pas imputable, de lui accorder sa protection dans le cas où il fait l'objet de poursuites pénales, sauf s'il a commis une faute personnelle, et, à moins qu'un motif d'intérêt général ne s'y oppose, de le protéger contre les menaces, violences, voies de fait, injures, diffamations ou outrages dont il est l'objet. Ce principe général du droit s'étend aux agents non-titulaires de l'Etat recrutés à l'étranger, alors même que leur contrat est soumis au droit local.,,2) Lorsqu'il s'agit, compte tenu de circonstances très particulières, du moyen le plus approprié pour assurer la sécurité d'un agent étranger employé par l'Etat, la protection fonctionnelle peut exceptionnellement conduire à la délivrance d'un visa ou d'un titre de séjour à l'intéressé et à sa famille.</ANA>
<ANA ID="9B"> 17-01-01 Il résulte d'un principe général du droit que, lorsqu'un agent public est mis en cause par un tiers à raison de ses fonctions, il incombe à la collectivité dont il dépend de le couvrir des condamnations civiles prononcées contre lui, dans la mesure où une faute personnelle détachable du service ne lui est pas imputable, de lui accorder sa protection dans le cas où il fait l'objet de poursuites pénales, sauf s'il a commis une faute personnelle, et, à moins qu'un motif d'intérêt général ne s'y oppose, de le protéger contre les menaces, violences, voies de fait, injures, diffamations ou outrages dont il est l'objet. Ce principe général du droit s'étend aux agents non-titulaires de l'Etat recrutés à l'étranger, alors même que leur contrat est soumis au droit local. La juridiction administrative est compétente pour connaître des recours contre les décisions des autorités de l'Etat refusant aux intéressés le bénéfice de cette protection.</ANA>
<ANA ID="9C"> 335-01-02-02 Lorsqu'il s'agit, compte tenu de circonstances très particulières, du moyen le plus approprié pour assurer la sécurité d'un agent étranger employé par l'Etat, la protection fonctionnelle peut exceptionnellement conduire à la délivrance d'un visa ou d'un titre de séjour à l'intéressé et à sa famille.</ANA>
<ANA ID="9D"> 36-07-10-005 1) Il résulte d'un principe général du droit que, lorsqu'un agent public est mis en cause par un tiers à raison de ses fonctions, il incombe à la collectivité dont il dépend de le couvrir des condamnations civiles prononcées contre lui, dans la mesure où une faute personnelle détachable du service ne lui est pas imputable, de lui accorder sa protection dans le cas où il fait l'objet de poursuites pénales, sauf s'il a commis une faute personnelle, et, à moins qu'un motif d'intérêt général ne s'y oppose, de le protéger contre les menaces, violences, voies de fait, injures, diffamations ou outrages dont il est l'objet. Ce principe général du droit s'étend aux agents non-titulaires de l'Etat recrutés à l'étranger, alors même que leur contrat est soumis au droit local.... ...2) Lorsqu'il s'agit, compte tenu de circonstances très particulières, du moyen le plus approprié pour assurer la sécurité d'un agent étranger employé par l'Etat, la protection fonctionnelle peut exceptionnellement conduire à la délivrance d'un visa ou d'un titre de séjour à l'intéressé et à sa famille.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., s'agissant des collaborateurs occasionnels du service public, CE, 13 janvier 2017,,, n° 386799, p. 1,,[RJ2] Rappr., s'agissant de la protection fonctionnelle des agents publics contre les menaces, violences et voies de fait dont ils sont l'objet, CE, Sect., 8 juin 2011,,, n° 312700, p. 270.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
