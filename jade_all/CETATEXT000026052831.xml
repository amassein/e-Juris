<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026052831</ID>
<ANCIEN_ID>JG_L_2012_06_000000350152</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/05/28/CETATEXT000026052831.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème et 7ème sous-sections réunies, 22/06/2012, 350152, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2012-06-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>350152</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème et 7ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Edmond Honorat</PRESIDENT>
<AVOCATS>SCP BARADUC, DUHAMEL</AVOCATS>
<RAPPORTEUR>Mme Sophie-Caroline de Margerie</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Béatrice Bourgeois-Machureau</COMMISSAIRE_GVT>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi, enregistré le 15 juin 2011 au secrétariat du contentieux du Conseil d'Etat, présenté pour M. Roman A et Mlle Nina A, demeurant ... ; M. et Mlle A demandent au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler l'arrêt n° 09PA05735 du 31 décembre 2010 par lequel la cour administrative d'appel de Paris a rejeté la requête formée par leur mère, Mme Hélène C, agissant en sa qualité de représentante de ses enfants alors mineurs, tendant à l'annulation du jugement n° 0619682 et 0904408 du 24 juillet 2009 par lequel le tribunal administratif de Paris a rejeté sa demande tendant à l'annulation pour excès de pouvoir de la décision en date du 15 mai 2006 par laquelle le garde des sceaux, ministre de la justice, a rejeté sa demande tendant à ce que ses enfants Roman et Nina soient autorisés à prendre le nom de C, et à ce qu'il soit enjoint au garde des sceaux, ministre de la justice de faire droit à cette demande ;<br/>
<br/>
              2°) réglant l'affaire au fond, d'annuler le jugement du 24 juillet 2009 du tribunal administratif de Paris et d'enjoindre au garde des sceaux, ministre de la justice et des libertés, de réexaminer leur demande de changement de nom dans un délai d'un mois à compter de la décision à intervenir ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 000 euros au profit de la SCP Baraduc et Duhamel, leur avocat, qui renonce à percevoir la part contributive de l'Etat au titre de l'aide juridictionnelle, sur le fondement des dispositions combinées des articles 37 de la loi du 10 juillet 1991 et L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code civil ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Sophie-Caroline de Margerie, Conseiller d'Etat,<br/>
<br/>
              - les observations de la SCP Baraduc, Duhamel, avocat de M. A,<br/>
<br/>
              - les conclusions de Mme Béatrice Bourgeois-Machureau, Rapporteur public,<br/>
<br/>
              La parole ayant à nouveau été donnée à la SCP Baraduc, Duhamel, avocat de M. A ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant qu'aux termes de l'article 61 du code civil : " Toute personne qui justifie d'un intérêt légitime peut demander à changer de nom (...) " ;<br/>
<br/>
              Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que M. Roman Yvan A, né en 1990, et Mlle Nina A, née en 1992, ont porté entre leur naissance et 2005 le nom de leur mère, Mme C, en vertu de l'article 334-1 du code civil, alors applicable, selon lequel l'enfant naturel acquérait le nom de celui de ses deux parents à l'égard de qui sa filiation était établie en premier lieu ; qu'à la suite du mariage de leurs parents, célébré en juin 2005, ils ont été légitimés par l'effet de l'article 331 du code civil qui, dans sa rédaction alors applicable, disposait que " tous les enfants nés hors mariage sont légitimés de plein droit par le mariage subséquent de leurs père et mère " ; qu'ils ont reçu, en conséquence, par application des règles gouvernant alors la dévolution du nom patronymique, le nom de A, qui est le nom de leur père ; que Mme C a, le 9 novembre 2005, saisi le garde des sceaux, ministre de la justice, sur le fondement de l'article 61 du code civil, d'une demande de changer le nom de ses enfants de A en C ; que cette demande a été rejetée par décision du 15 mai 2006 ; que le recours qu'elle a formé devant le tribunal administratif de Paris a été rejeté par un jugement du 24 juillet 2009 ; que M. Roman Yvan A et Mlle Nina A, devenus majeurs, se pourvoient en cassation contre l'arrêt du 31 décembre 2010 par lequel la cour administrative d'appel de Paris a confirmé ce jugement ; <br/>
<br/>
              Considérant, en premier lieu, que, contrairement à ce que soutiennent les requérants, ils n'ont invoqué ni à l'appui de leur demande de changement de nom ni devant les juges du fond la circonstance que le nom maternel de C serait menacé d'extinction ; qu'ainsi, le moyen tiré de ce que l'arrêt qu'ils attaquent est entaché d'insuffisance de motivation sur ce point doit être écarté ;<br/>
<br/>
              Considérant, en deuxième lieu, que la cour administrative d'appel de Paris n'a pas commis d'erreur de droit en estimant que le souhait de reprendre le nom maternel, sur lequel se fondait la demande, ne suffisait pas à caractériser l'intérêt légitime requis pour déroger aux principes de dévolution et de fixité du nom établis par la loi ; qu'elle n'a pas davantage commis d'erreur de droit en estimant que la possession d'état dont se prévalaient les requérants, scolarisés et connus par leur entourage sous le nom de leur mère, n'était pas suffisamment prolongée pour constituer pareil intérêt ; <br/>
<br/>
              Considérant qu'il résulte de ce qui précède que le pourvoi ne peut être accueilli ; que, par voie de conséquence, les conclusions des requérants tendant à l'application de l'article L. 761-1 du code de justice administrative et de l'article 37 de la loi du 10 juillet 1991 ne peuvent pas davantage être accueillies ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : Le pourvoi de M. Roman A et de Mlle Nina A est rejeté.<br/>
<br/>
Article 2 : La présente décision sera notifiée à M. Roman A, à Mlle Nina A et au garde des sceaux, ministre de la justice. <br/>
<br/>
<br/>
<br/>
</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
