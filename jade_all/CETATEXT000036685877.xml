<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036685877</ID>
<ANCIEN_ID>JG_L_2018_02_000000406742</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/68/58/CETATEXT000036685877.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème - 2ème chambres réunies, 09/02/2018, 406742, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-02-09</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>406742</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème - 2ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MATUCHANSKY, POUPOT, VALDELIEVRE</AVOCATS>
<RAPPORTEUR>M. Marc Pichon de Vendeuil</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Gilles Pellissier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:406742.20180209</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              Par une requête, un mémoire complémentaire et un mémoire en réplique, enregistrés les 10 janvier, 20 février et 22 août 2017 au secrétariat du contentieux du Conseil d'Etat, l'association professionnelle nationale des militaires de la marine nationale (APNM-Marine) demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir l'arrêté du 21 octobre 2016 du ministre de la défense pris en application des articles R. 4126-1 à R. 4126-7 du code de la défense relatifs aux associations professionnelles nationales de militaires ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 2 000 euros au titre de l'article L. 761 1 du code de justice administrative.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la Constitution ;<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ; <br/>
              - le code de la défense ; <br/>
              - la loi du 1er juillet 1901 ; <br/>
              - la loi n° 78-17 du 6 janvier 1978 ;<br/>
              - la loi n° 2015-917 du 28 juillet 2015 ;<br/>
              - le décret n° 2016-1043 du 29 juillet 2016 ; <br/>
              - l'arrêté du 21 juillet 2016 du ministre de la défense portant création, par la direction des ressources humaines du ministère de la défense, d'un traitement automatisé de données à caractère personnel relatif au suivi et au contrôle des listes d'adhérents des associations professionnelles nationales de militaires ou fédérations ou unions ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Marc Pichon de Vendeuil, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Gilles Pellissier, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Matuchansky, Poupot, Valdelièvre, avocat de l'association APNM-Marine.<br/>
<br/>
              Vu la note en délibéré, enregistrée le 12 janvier 2018, présentée par l'APNM-Marine.<br/>
<br/>
<br/>
<br/>Sur le cadre juridique :<br/>
<br/>
              1. Considérant que les dispositions du chapitre VI du titre II du livre premier de la 4ème partie du code de la défense, issues de la loi du 28 juillet 2015 actualisant la programmation militaire pour les années 2015 à 2019 et portant diverses dispositions concernant la défense, ont ouvert aux militaires en activité la possibilité d'adhérer à des groupements professionnels à la condition que ces derniers soient constitués sous la forme d'associations professionnelles nationales de militaires ; que ces dispositions réservent aux associations professionnelles nationales de militaires reconnues représentatives la possibilité de participer au dialogue organisé, au niveau national, par les ministres de la défense et de l'intérieur ainsi que par les autorités militaires sur les questions générales intéressant la condition militaire ; <br/>
<br/>
              2. Considérant qu'aux termes des deuxième et troisième alinéas de l'article L. 4121-4 du code de la défense : " L'existence de groupements professionnels militaires à caractère syndical ainsi que, sauf dans les conditions prévues au troisième alinéa, l'adhésion des militaires en activité à des groupements professionnels sont incompatibles avec les règles de la discipline militaire. / Les militaires peuvent librement créer une association professionnelle nationale de militaires régie par le chapitre VI du présent titre, y adhérer et y exercer des responsabilités " ; que selon l'article L. 4126-1 du même code : " Les associations professionnelles nationales de militaires sont régies par le présent chapitre et, en tant qu'elles n'y sont pas contraires, par les dispositions du titre Ier de la loi du 1er juillet 1901 relative au contrat d'association et, pour les associations qui ont leur siège dans les départements du Bas-Rhin, du Haut-Rhin ou de la Moselle, par les dispositions du code civil local " ; qu'aux termes de l'article L. 4126-8 du même code : " I. - Peuvent être reconnues représentatives les associations professionnelles nationales de militaires satisfaisant aux conditions suivantes: / (...) 4° Une influence significative, mesurée en fonction de l'effectif des adhérents, des cotisations perçues et de la diversité des groupes de grades mentionnés aux 1° à 3° du I de l'article L. 4131-1 représentés. / II. Peuvent siéger au Conseil supérieur de la fonction militaire les associations professionnelles nationales de militaires ou leurs unions et fédérations reconnues, en outre, représentatives d'au moins trois forces armées autres que les services de soutien mentionnés au dernier alinéa de l'article L. 3211-1 et de deux formations rattachées ou services de soutien mentionnés au dernier alinéa de l'article L. 3211-1, dans des conditions fixées par le décret mentionné à l'article L. 4126-10. / III. - La liste des associations professionnelles nationales de militaires représentatives est fixée par l'autorité administrative compétente (...) " ; qu'aux termes de l'article L. 4126-10 du même code, les conditions d'application de ces dispositions sont fixées par un décret en Conseil d'Etat ; <br/>
<br/>
              3. Considérant que pour l'application de ces dispositions, le décret du 29 juillet 2016 relatif aux associations professionnelles nationales de militaires a inséré les articles R. 4126-1 à R. 4126-17 dans le code de la défense ; que l'article R. 4126-6 de ce code dispose que : " Une association professionnelle nationale de militaire doit, pour être regardée comme bénéficiant d'une influence significative au sens du 4° du I de l'article L. 4126-8, satisfaire à l'ensemble des conditions suivantes: / 1° L'effectif des adhérents doit être égal à un pourcentage minimal de l'effectif total de la force armée ou de la formation rattachée représentée ; / 2° L'association doit compter parmi ses adhérents des militaires relevant de chacun des groupes de grade mentionnés à l'article R. 4131-14. L'effectif des adhérents relevant de chaque groupe de grade doit être égal à un pourcentage minimal de l'effectif total des militaires relevant de ce groupe de grade  au sein de la force armée ou de la formation rattachée représentée. Ce pourcentage minimal peut être différent selon le groupe de grade. / (...) Lorsque les adhérents sont issus de plusieurs forces armées ou formations rattachées, ces pourcentages doivent être respectés pour l'une d'entre elles au moins. (...) / Jusqu'au 1er janvier 2021, les pourcentages prévus aux alinéas précédents sont compris entre 1 % et 5 % et sont fixés par le ministre de la défense de manière à assurer le caractère effectif du dialogue social prévu à l'article L. 4126-9. Ils peuvent être différents selon la force armée ou la formation rattachée concernée. / (...) Lorsque le militaire adhère à plusieurs associations professionnelles nationales de militaires, une seule adhésion, de son choix, est comptabilisée, dans les conditions fixées par l'arrêté prévu à l'article R. 4126-17 (...) " ; que l'article R. 4126-7 du même code détermine les conditions d'appartenance à diverses forces armées ou services de soutien et d'effectifs d'adhérents que doivent remplir les associations professionnelles nationales de militaires représentatives pour pouvoir siéger au Conseil supérieur de la fonction militaire et prévoit que jusqu'au 1er janvier 2021, les pourcentages minimaux d'effectifs " sont compris entre 1 % et 5% et sont fixés par le ministre de la défense " ; qu'aux termes de l'article R. 4126-8 du même code : " A chaque renouvellement du Conseil supérieur de la fonction militaire, le ministre de la défense fixe la liste des associations professionnelles nationales de militaires représentatives. Il détermine également parmi ces associations, celles pouvant siéger au Conseil supérieur de la fonction militaire. Le nombre d'adhérents déclarés par les associations est préalablement vérifié par la commission prévue à l'article R. 4124-22. / Le traitement des informations contenues dans les listes d'adhérents ainsi que la conservation de ces informations sont assurés dans le respect des obligations de sécurité et de confidentialité prévues par la loi n° 78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés " ; que l'article R. 4126-17 du même code énonce que : " Un arrêté du ministre de la défense précise les modalités d'application du présent chapitre " ;<br/>
<br/>
              4. Considérant, enfin, qu'aux termes de l'article R. 4124-22 du code de la défense : " L'élection ou le tirage au sort des membres du Conseil supérieur de la fonction militaire et des membres des conseils de la fonction militaire sont effectués sous le contrôle d'une commission présidée par un conseiller d'Etat et comprenant le secrétaire général du Conseil supérieur de la fonction militaire, un membre du corps militaire du contrôle général des armées, un officier, un sous-officier ou officier marinier et un militaire du rang désignés par le ministre de la défense " ; <br/>
<br/>
              5. Considérant que l'association professionnelle nationale des militaires de la marine nationale (APNM-Marine) demande l'annulation de l'arrêté du 21 octobre 2016 du ministre de la défense pris en application des articles R. 4126-1 à R. 4126-7 du code de la défense ; <br/>
<br/>
              Sur la légalité de l'arrêté attaqué pris dans son ensemble : <br/>
<br/>
              6. Considérant qu'aux termes de l'article L. 4124-1 du code de la défense, le Conseil supérieur de la fonction militaire " est obligatoirement saisi des projets de loi modifiant le livre Ier et des textes d'application de ce livre ayant une portée statutaire, indiciaire ou indemnitaire " ; qu'aux termes de l'article R. 4124-1 du même code, le conseil supérieur exprime son avis " (...) 3° Sur les projets de décret portant statut particulier des militaires mentionnés à l'article L. 4111-2 ainsi que les projets de décret comportant des dispositions statutaires communes à plusieurs corps ou catégories de militaires ; / 4° Sur les projets de texte réglementaire portant sur les dispositions indiciaires ou indemnitaires relatives aux militaires " ; qu'il résulte de ces dispositions que seuls les arrêtés comportant des dispositions indiciaires ou indemnitaires doivent être obligatoirement soumis à l'avis du Conseil supérieur de la fonction militaire ; que l'arrêté attaqué ne comportant pas de telles dispositions, l'association APNM-Marine n'est pas fondée à soutenir qu'il est intervenu au terme d'une procédure irrégulière faute d'avoir été précédé de la consultation de ce conseil ; <br/>
<br/>
              Sur la légalité de l'article 3 de l'arrêté attaqué :<br/>
<br/>
              7. Considérant qu'en vertu des dispositions précitées le ministre de la défense fixe la liste des associations représentatives par groupes de grade dans chaque force armée ou formation rattachée ; qu'à cette fin, les dispositions du décret du 29 juillet 2016 citées au point 3 prévoient que la commission prévue à l'article R. 4124-22 du code de la défense, présidée par un conseiller d'Etat, vérifie le nombre d'adhérents déclarés par les associations avant que le ministre ne fixe la liste des associations professionnelles nationales de militaires représentatives et celle des associations pouvant siéger au Conseil supérieur de la fonction militaire et que les modalités d'application de cette procédure de vérification sont précisées par un arrêté ministériel ; que toutefois ni ces dispositions ni aucune autre n'habilitent le ministre de la défense à prévoir par arrêté que les listes d'adhérents comportant ces données seront transmises au secrétariat général du Conseil supérieur de la fonction militaire, auquel il revient d'assurer le secrétariat de la commission ; que, par suite, l'article 3 de l'arrêté attaqué, sans qu'il soit besoin d'examiner les autres moyens dirigés contre cet article, doit être annulé ; <br/>
<br/>
<br/>
<br/>
<br/>
              Sur la légalité de l'article 2 de l'arrêté attaqué : <br/>
<br/>
              8. Considérant que l'article L. 4126-10 du code de la défense a renvoyé à un décret en Conseil d'Etat le soin de déterminer " (...) 2° Les seuils à partir desquels les associations satisfont à la condition de représentativité (...) prévue au 4° du I de l'article L. 4126-8 du même code " ; qu'aux termes de l'article R. 4126-6 du code de la défense, ces seuils sont jusqu'au 1er janvier 2021 " fixés par le ministre de la défense de manière à assurer le caractère effectif du dialogue social prévu à l'article L. 4126-9 " ; que l'article R. 4126-7 du code de la défense prévoit de la même façon que jusqu'au 1er janvier 2021, les pourcentages des effectifs permettant à une association professionnelle nationale de militaires représentative de siéger au Conseil supérieur de la fonction militaire " sont fixés par le ministre de la défense " ; qu'après cette date, la fixation de ces différents pourcentages relève d'un décret en Conseil d'Etat ; que dès lors, les dispositions du I et du II de l'article 2 de l'arrêté attaqué doivent, en ce qu'elles fixent les pourcentages prévus aux articles R. 4126-6 et R. 4126-7 du code de la défense au-delà du 1er janvier 2021, être annulées ;<br/>
<br/>
              9. Considérant qu'il résulte de tout ce qui précède qu'en l'absence d'autres moyens dirigés contre les autres dispositions qui en sont divisibles, il y a lieu, d'une part, d'annuler les dispositions de l'article 3 de l'arrêté du 21 octobre 2016 ainsi que celles du I et du II de son article 2 en ce qu'elles fixent les pourcentages prévus aux articles R. 4126-6 et R. 4126-7 du code de la défense au-delà du 1er janvier 2021 et, d'autre part, de rejeter le surplus des conclusions de l'association professionnelle nationale des militaires de la marine nationale tendant à l'annulation des autres dispositions de l'arrêté ;<br/>
<br/>
              10. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 2 000 euros à verser à l'APNM-Marine au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'article 3 de l'arrêté du 21 octobre 2016 du ministre de la défense et les dispositions du I et du II de son article 2 en ce qu'elles fixent les pourcentages prévus aux articles R. 4126-6 et R. 4126-7 du code de la défense au-delà du 1er janvier 2021 sont annulés. <br/>
Article 2 : L'Etat versera à l'APNM-Marine une somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : Le surplus des conclusions de la requête de l'association professionnelle nationale des militaires de la marine nationale est rejeté. <br/>
Article 4 : La présente décision sera notifiée à l'association professionnelle nationale des militaires de la marine nationale et à la ministre des armées.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
