<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038151201</ID>
<ANCIEN_ID>JG_L_2019_02_000000417477</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/15/12/CETATEXT000038151201.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème chambre, 20/02/2019, 417477, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-02-20</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>417477</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP THOUVENIN, COUDRAY, GREVY</AVOCATS>
<RAPPORTEUR>M. Géraud Sajust de Bergues</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Laurent Cytermann</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2019:417477.20190220</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés les 19 janvier et 18 juillet 2018 au secrétariat du contentieux du Conseil d'Etat, le syndicat CFDT de Voies navigables de France demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir les décisions implicites par lesquelles le ministre d'Etat, ministre de la transition écologique et solidaire et le ministre de la cohésion des territoires ont rejeté sa demande tendant à l'abrogation de la note de service du 13 décembre 2016 relative aux parcours professionnels, carrières et rémunérations des personnels d'exploitation des travaux publics de l'Etat, en tant que cette note a prévu que, pendant une période transitoire de deux ans, les personnels qui relevaient du grade d'agent d'exploitation spécialisé des travaux publics de l'Etat ou du grade de chef d'équipe d'exploitation des travaux publics de l'Etat conserveront les montants de la prime technique de l'entretien, des travaux et de l'exploitation ainsi que de la prime pour services rendus qui étaient respectivement les leurs dans l'ancienne structure des carrières ; <br/>
<br/>
              2°) d'enjoindre à ces ministres d'abroger cette note dans la mesure de l'annulation prononcée ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le décret n° 55-1002 du 26 juillet 1955 ; <br/>
              - le décret n° 91-393 du 25 avril 1991 ;<br/>
              - le décret n° 2002-534 du 16 avril 2002 ; <br/>
              - le décret n° 2016-580 du 11 mai 2016 ; <br/>
              - le décret n° 2016-1084 du 3 août 2016 ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Géraud Sajust de Bergues, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Laurent Cytermann, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Thouvenin, Coudray, Grevy, avocat du syndicat CFDT de Voies navigables de France ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article 1er du décret du 11 mai 2016 relatif à l'organisation des carrières des fonctionnaires de catégorie C de la fonction publique de l'Etat : " Les corps des fonctionnaires des administrations de l'Etat classés dans la catégorie C au sens de la loi du 13 juillet 1983 (...) comportent deux ou trois grades. / Ces grades sont classés dans les échelles de rémunération C1, C2, C3 (...) ". Le même article prévoit que, pour les corps qui comportent trois grades, ceux-ci " sont classés, en allant vers le grade le plus élevé : / 1° Pour le premier grade, dans l'échelle de rémunération C1 ; / 2° Pour le deuxième grade, dans l'échelle de rémunération C2 ; / 3° Pour le troisième grade, dans l'échelle de rémunération C3 ". Pour l'application de ces dispositions au corps des personnels d'exploitation des travaux publics de l'Etat, le décret du 3 août 2016 a modifié le décret du 25 avril 1991 portant dispositions statutaires applicables à ce corps et prévu, notamment, que ce corps comportera trois grades, dont le deuxième, de " chef d'équipe d'exploitation des travaux publics de l'Etat ", classé dans l'échelle de rémunération C2. Les grades préexistants d'agent d'exploitation spécialisé des travaux publics de l'Etat et de chef d'équipe d'exploitation des travaux publics de l'Etat ont été fusionnés dans ce deuxième grade et les agents appartenant aux deux anciens grades fusionnés ont été reclassés dans l'échelle de rémunération C2. <br/>
<br/>
              2. La note de service du 13 décembre 2016 de la ministre de l'environnement, de l'énergie et de la mer et de la ministre du logement et de l'habitat durable a, notamment, prescrit de conserver pendant une période de deux ans le régime indemnitaire antérieur dont bénéficiaient de manière distincte les fonctionnaires du corps des personnels d'exploitation des travaux publics de l'Etat relevant du grade d'agent d'exploitation spécialisé ou du grade de chef d'équipe d'exploitation avant la fusion de ces deux grades, au 1er janvier 2017, au sein d'un grade unique de chef d'équipe d'exploitation des travaux publics de l'Etat. Le syndicat CFDT de Voies navigables de France demande l'annulation pour excès de pouvoir du refus implicite opposé par le ministre d'Etat, ministre de la transition écologique et solidaire, et par le ministre de la cohésion des territoires à sa demande d'abrogation de cette note en tant qu'elle porte sur le régime indemnitaire.  <br/>
<br/>
              3. En premier lieu, dans le cadre d'un recours pour excès de pouvoir dirigé contre la décision refusant d'abroger un acte réglementaire, les vices de forme et de procédure dont un acte serait entaché ne peuvent être utilement invoqués que dans le cadre du recours pour excès de pouvoir dirigé contre cet acte réglementaire lui-même et introduit avant l'expiration du délai de recours contentieux. Par suite, le syndicat requérant ne peut utilement invoquer à l'appui de son recours pour excès de pouvoir le moyen tiré de ce que les commissions administratives paritaires locales compétentes n'auraient pas été consultées avant l'édiction de la note de service. <br/>
<br/>
              4. En second lieu, l'égalité de traitement à laquelle ont droit les agents du même grade d'un même corps fait obstacle à ce que puissent être institués entre eux des régimes indemnitaires différents, à moins que des circonstances exceptionnelles ne puissent justifier une pareille mesure dans l'intérêt du service.  Ces circonstances doivent être appréciées en fonction, notamment, des exigences spécifiques de la transition vers un nouveau mode de structuration des grades d'un même corps. <br/>
<br/>
              5. Le décret du 16 avril 2002 relatif à l'attribution d'une prime technique de l'entretien, des travaux et de l'exploitation à certains personnels du ministère de l'équipement, des transports et du logement prévoit, à son article 2, que cette prime " est fixée au sein de chaque service par type de poste de travail homogène ". L'article 1er de l'arrêté du 14 mai 2009 fixant les montants de la prime pour services rendus allouée à certains fonctionnaires relevant du ministère de l'écologie, de l'énergie, du développement durable et de l'aménagement du territoire, pris en application du décret du 25 avril 1991 relatif aux indemnités pour travaux dangereux, insalubres ou particulièrement pénibles et aux primes pour services rendus allouées aux conducteurs de chantiers et agents de travaux des ponts et chaussées, prévoit que " le montant individuel de la prime pour services rendus est fixé en tenant compte des responsabilités, du niveau d'expertise et des sujétions spéciales liées aux fonctions et également en tenant compte de la qualité des services rendus ". Il résulte de ces dispositions que l'attribution de ces primes est modulée selon la nature des fonctions exercées ou des conditions de leur exercice. <br/>
<br/>
              6. La note contestée se borne à prévoir que, pendant une période transitoire limitée de deux ans après la fusion des deux anciens grades, laquelle n'a pas pour effet d'entraîner un changement des fonctions exercées, les agents concernés continueront de bénéficier du régime indemnitaire qui leur était appliqué pour les fonctions qu'ils exerçaient avant la fusion. <br/>
<br/>
              7. Cette note ne porte pas atteinte au principe d'égalité, eu égard, d'une part, à l'objectif d'intérêt général de rénovation de la structure de la carrière des agents de catégorie C du corps des personnels d'exploitation des travaux publics de l'Etat poursuivi, en particulier, par la création du grade de chef d'équipe d'exploitation des travaux publics de l'Etat et, d'autre part, aux dispositions des articles cités au point 5, alors même qu'elle maintient temporairement les modulations attachées aux anciens grades. Par suite, les moyens tirés de la méconnaissance par la note de service du principe d'égalité et des dispositions du décret du 25 avril 1991 doivent être écartés.  <br/>
<br/>
              8. Il résulte de ce qui précède que le syndicat requérant n'est pas fondé à demander l'annulation du refus qui lui a été opposé. En conséquence, ses conclusions à fin d'injonction et celles présentées au titre de l'article L. 761-1 du code de justice administrative doivent être rejetées.  <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête du syndicat CFDT de Voies navigables de France est rejetée.<br/>
Article 2 : La présente décision sera notifiée au syndicat CFDT de Voies navigables de France, au ministre de la transition écologique et solidaire et au ministre de la cohésion des territoires. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
