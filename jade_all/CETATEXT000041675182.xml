<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000041675182</ID>
<ANCIEN_ID>JG_L_2020_02_000000438663</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/41/67/51/CETATEXT000041675182.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 18/02/2020, 438663, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-02-18</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>438663</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2020:438663.20200218</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
              Mme C... A... a demandé au juge des référés du tribunal administratif de Nantes, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, à titre principal, d'enjoindre à l'Office français de l'immigration et de l'intégration (OFII) de mettre à sa disposition un lieu d'hébergement susceptible de l'accueillir avec son enfant, sans délai et sous astreinte de 200 euros par jour de retard, à titre subsidiaire, d'enjoindre au préfet de la Loire-Atlantique de mettre à sa disposition un lieu d'hébergement susceptible de l'accueillir avec son enfant, adapté à son statut de demandeuse d'asile et à sa vulnérabilité, sans délai et sous astreinte de 200 euros par jour de retard et, à titre infiniment subsidiaire, d'enjoindre au département de la Loire-Atlantique de mettre à sa disposition un lieu d'hébergement susceptible de l'accueillir avec son enfant sans délai et sous astreinte de 200 euros par jour de retard. Par une ordonnance n° 2001543 du 12 février 2020, le juge des référés du tribunal administratif de Nantes a rejeté sa demande.<br/>
<br/>
              Par une requête, enregistrée le 13 février 2019 au secrétariat du contentieux du Conseil d'Etat, Mme A... demande au juge des référés du Conseil d'Etat de l'admettre à titre provisoire au bénéfice de l'aide juridictionnelle et, statuant sur le fondement de l'article L. 521-2 du code de justice administrative : <br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
              2°) d'enjoindre à l'OFII de prendre les dispositions nécessaires à sa mise à l'abri immédiate et à celle de son fils dans le cadre du dispositif d'hébergement des demandeurs d'asile, dans un délai de 24 heures suivant la notification de l'ordonnance à intervenir sous astreinte de 100 euros par jour de retard ou, à défaut, d'enjoindre au préfet de la Loire-Atlantique de les orienter vers une structure d'hébergement d'urgence, sous les mêmes conditions de délai et d'astreinte, à défaut, d'enjoindre au président du conseil départemental de leur proposer un hébergement d'urgence et de leur apporter une aide éducative matérielle et psychologique ;<br/>
<br/>
              3°) d'enjoindre, en tout état de cause, au préfet de la Loire-Atlantique d'enregistrer sa demande d'asile en procédure normale ; <br/>
              4°) de mettre à la charge de l'OFII, de l'Etat ou du département de la Loire-Atlantique, la somme de 4 000 euros en application des dispositions des articles 37 de la loi n° 91-647 du 10 juillet 1991 et L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Elle soutient que : <br/>
              - la condition d'urgence est remplie dès lors que l'absence de proposition d'hébergement porte une atteinte grave et immédiate à sa situation et à celle de son nourrisson ;<br/>
              - l'ordonnance attaquée est entachée d'irrégularité dès lors que le juge des référés a, d'une part, omis de statuer sur les conclusions relatives à son fils et, d'autre part, omis d'examiner les moyens relatifs à celui-ci ;<br/>
              - l'ordonnance attaquée est entachée d'une erreur de droit et d'une erreur manifeste d'appréciation tirées du refus de reconnaitre la carence caractérisée de l'OFII, ou, à titre subsidiaire, celle de la préfecture ou du département, comme étant constitutive d'une atteinte grave et manifestement illégale au droit d'asile.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 relative à l'aide juridique ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. Le juge des référés se prononce dans un délai de quarante-huit heures ". En vertu de l'article L. 522-3 du même code, le juge des référés peut, par une ordonnance motivée, rejeter une requête sans instruction ni audience lorsque la condition d'urgence n'est pas remplie ou lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable ou qu'elle est mal fondée. <br/>
<br/>
              2. Au sens de l'article L. 521-2 précité du code de justice administrative, la notion de liberté fondamentale englobe, s'agissant des ressortissants étrangers qui sont soumis à des mesures spécifiques réglementant leur entrée et leur séjour en France, et qui ne bénéficient donc pas, à la différence des nationaux, de la liberté d'entrée sur le territoire, le droit constitutionnel d'asile qui a pour corollaire le droit de solliciter le statut de réfugié, dont l'obtention est déterminante pour l'exercice par les personnes concernées des libertés reconnues de façon générale aux ressortissants étrangers. Si la privation du bénéfice des mesures prévues par la loi afin de garantir aux demandeurs d'asile des conditions matérielles d'accueil décentes, jusqu'à ce qu'il ait été statué sur leur demande, est susceptible de constituer une atteinte grave et manifestement illégale à la liberté fondamentale que constitue le droit d'asile, le caractère grave et manifestement illégal d'une telle atteinte s'apprécie en tenant compte des moyens dont dispose l'autorité administrative compétente et de la situation du demandeur. Ainsi, le juge des référés ne peut faire usage des pouvoirs qu'il tient de l'article L. 521-2 du code de justice administrative en adressant une injonction à l'administration que dans le cas où, d'une part, le comportement de celle-ci fait apparaître une méconnaissance manifeste des exigences qui découlent du droit d'asile et où, d'autre part, il résulte de ce comportement des conséquences graves pour le demandeur d'asile, compte tenu notamment de son âge, de son état de santé ou de sa situation de famille.<br/>
<br/>
              3. Mme A..., ressortissante guinéenne née le 2 mars 1996, a déposé une demande d'asile le 4 janvier 2019 qui a été examinée selon la procédure prévue par le règlement (UE) n° 604/2013 du Parlement européen et du Conseil du 26 juin 2013 dit " Dublin III ". A ce titre, une attestation de demandeur d'asile valable jusqu'au 12 juillet 2019 lui a été délivrée. Par une décision en date du 26 avril 2019, l'OFII a suspendu le bénéfice des conditions matérielles d'accueil au motif de la non présentation de l'intéressée aux autorités chargées de l'asile. Elle n'a pas contesté cette décision ni sollicité postérieurement le rétablissement de ses conditions matérielles d'accueil ou fait valoir des éléments de vulnérabilité. Enfin, Mme A..., qui a indiqué au juge des référés de première instance avoir été hébergée par des tiers, a donné naissance le 21 septembre 2019 à un enfant, reconnu par son père, M. B..., un compatriote dont la demande d'asile aurait été rejetée. Par une ordonnance du 12 février 2020, dont il est relevé appel, le juge des référés du tribunal administratif de Nantes a rejeté sa demande tendant, à titre principal, à ce qu'il soit fait injonction à l'OFII de mettre à sa disposition un lieu d'hébergement susceptible de l'accueillir avec son enfant, sans délai et sous astreinte de<br/>
200 euros par jour de retard, à titre subsidiaire, à ce qu'il soit fait injonction au préfet de la Loire-Atlantique de mettre à sa disposition un lieu d'hébergement susceptible de l'accueillir avec son enfant, sous les mêmes conditions de délai et d'astreinte et, à titre infiniment subsidiaire, à ce qu'il soit fait injonction au département de la Loire-Atlantique de mettre à sa disposition un lieu d'hébergement susceptible de l'accueillir avec son enfant sous les mêmes conditions de délai et d'astreinte.<br/>
<br/>
              4. Pour critiquer l'ordonnance attaquée, Madame A... soutient d'abord qu'elle aurait omis de répondre à ses conclusions relatives à son fils et ses moyens tirés de l'atteinte aux droits fondamentaux de celui-ci. Il résulte toutefois des motifs mêmes de l'ordonnance qu'elle a statué sur l'ensemble des moyens articulés, y compris en tant qu'ils l'étaient pour le compte du fils de la requérante, et que les conclusions sur lesquelles elle a statué concernaient tant la requérante que son fils. <br/>
<br/>
              5. Madame A... soutient ensuite que l'ordonnance n'a pu écarter sans erreur de droit ni erreur manifeste d'appréciation ses moyens et conclusions dirigés contre l'OFII, la préfecture et le département de Loire-Atlantique pour écarter l'existence d'une atteinte grave et immédiate à une liberté fondamentale. Toutefois, la requête se borne à réitérer les circonstances de fait déjà alléguées en première instance, sans fournir d'éléments ou d'arguments qui permettraient de remettre en cause le raisonnement suivi par l'ordonnance, qui ne peut qu'être confirmée, pour écarter l'existence d'une atteinte grave et immédiate à un droit ou une liberté fondamentale de la requérante et de son fils.<br/>
<br/>
              6. La requête de Mme A... ne peut donc, sans qu'il soit besoin d'examiner l'urgence qui s'y attache, et sans qu'il y ait lieu de l'admettre au bénéfice de l'aide juridictionnelle, qu'être rejetée, sur le fondement de l'article L. 522-3 du code de justice administrative, y compris en tant qu'elle tend au prononcé d'une injonction sous astreinte et à ce que l'Etat verse à Mme A... une somme d'argent dès lors que, l'Etat n'étant pas la partie perdante, les dispositions de l'article L 761-1 du même code y font obstacle.<br/>
<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de Mme A... est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à Mme C... A.... <br/>
Copie en sera adressée à l'Office français de l'immigration et de l'intégration, au ministre de l'intérieur, à la ministre des solidarités et de la santé et au département de la Loire-Atlantique.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
