<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037847537</ID>
<ANCIEN_ID>JG_L_2018_12_000000421326</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/84/75/CETATEXT000037847537.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère et 4ème chambres réunies, 21/12/2018, 421326, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-12-21</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>421326</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère et 4ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP THOUVENIN, COUDRAY, GREVY ; SCP NICOLAY, DE LANOUVELLE, HANNOTIN</AVOCATS>
<RAPPORTEUR>M. Frédéric Pacoud</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Rémi Decout-Paolini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:421326.20181221</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B...A...a demandé au juge des référés du tribunal administratif de Grenoble, sur le fondement des dispositions de l'article L. 521-1 du code de justice administrative :<br/>
              - de suspendre la décision du président du conseil départemental de l'Isère du 12 février 2018 refusant sa prise en charge en qualité de jeune majeur ;<br/>
              - d'enjoindre au département de l'Isère, sous astreinte de 100 euros par jour de retard, de le prendre en charge en qualité de jeune majeur, dans un délai de 48 heures à compter de la notification de l'ordonnance à intervenir, ou, à défaut, de réexaminer sa demande dans le délai de deux mois et, dans l'intervalle, de poursuivre sa prise en charge par les services de l'aide sociale à l'enfance, dans un délai de 24 heures.<br/>
<br/>
              Par une ordonnance n° 1801606 du 25 avril 2018, le juge des référés du tribunal administratif de Grenoble a rejeté sa demande.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un nouveau mémoire, enregistrés les 8 et 25 juin et le 15 octobre 2018 au secrétariat du contentieux du Conseil d'Etat, M. A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance ; <br/>
<br/>
              2°) statuant en référé, de faire droit à ses conclusions de première instance ;<br/>
<br/>
              3°) de mettre à la charge du département de l'Isère la somme de 3 500 euros, à verser à la SCP Thouvenin, Coudray, Grevy, son avocat, au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'action sociale et des familles ;<br/>
              - le code civil ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - la loi n° 2016-297 du 14 mars 2016 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Frédéric Pacoud, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Rémi Decout-Paolini, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Thouvenin, Coudray, Grevy, avocat de M. A...et à la SCP Nicolaÿ, de Lanouvelle, Hannotin, avocat du département de l'Isère.<br/>
<br/>
<br/>
              Vu la note en délibéré, enregistrée le 22 octobre 2018, présentée pour le département de l'Isère ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision ". <br/>
<br/>
              2. Il ressort des pièces du dossier soumis au juge des référés du tribunal administratif de Grenoble que M. B...A..., né le 10 janvier 2000 au Mali, a été confié au service de l'aide sociale à l'enfance du département de l'Isère, jusqu'à sa majorité, en vertu d'un jugement en assistance éducative du 6 janvier 2017. Par un courrier du 1er février 2018, alors qu'il était scolarisé au lycée Emmanuel Mounier de Grenoble, M. A...a demandé à bénéficier d'un accompagnement en tant que jeune majeur, afin de terminer sa scolarité et de trouver un apprentissage. Par une décision du 12 février 2018, le président du conseil départemental de l'Isère a rejeté sa demande et lui a indiqué qu'il devait quitter le dispositif d'hébergement et de mise à l'abri dès la réception de cette réponse. M. A...se pourvoit en cassation contre l'ordonnance du 25 avril 2018 par laquelle le juge des référés du tribunal administratif de Grenoble a rejeté sa demande de suspension de l'exécution de cette décision.<br/>
<br/>
              3. Aux termes de l'article L. 221-1 du code de l'action sociale et des familles : " Le service de l'aide sociale à l'enfance est un service non personnalisé du département chargé des missions suivantes : / 1° Apporter un soutien matériel, éducatif et psychologique tant aux mineurs et à leur famille ou à tout détenteur de l'autorité parentale, confrontés à des difficultés risquant de mettre en danger la santé, la sécurité, la moralité de ces mineurs ou de compromettre gravement leur éducation ou leur développement physique, affectif, intellectuel et social, qu'aux mineurs émancipés et majeurs de moins de vingt-et-un ans confrontés à des difficultés familiales, sociales et éducatives susceptibles de compromettre gravement leur équilibre (...) ". L'article L. 222-5 du même code détermine les personnes susceptibles, sur décision du président du conseil départemental, d'être prises en charge par le service de l'aide sociale à l'enfance, parmi lesquelles, au titre du 1° de cet article, les mineurs qui ne peuvent demeurer provisoirement dans leur milieu de vie habituel et dont la situation requiert un accueil à temps complet ou partiel et, au titre de son 3°, les mineurs confiés au service par le juge des enfants parce que leur protection l'exige.. Aux termes du sixième alinéa de cet article : " Peuvent être également pris en charge à titre temporaire par le service chargé de l'aide sociale à l'enfance les mineurs émancipés et les majeurs âgés de moins de vingt-et-un ans qui éprouvent des difficultés d'insertion sociale faute de ressources ou d'un soutien familial suffisants ". La loi du 14 mars 2016 relative à la protection de l'enfant a complété cet article par un septième alinéa prévoyant qu' " Un accompagnement est proposé aux jeunes mentionnés au 1° du présent article devenus majeurs et aux majeurs mentionnés à l'avant-dernier alinéa, au-delà du terme de la mesure, pour leur permettre de terminer l'année scolaire ou universitaire engagée ". L'article L. 222-5-1 inséré dans ce code par la même loi prévoit qu'" un entretien est organisé par le président du conseil départemental avec tout mineur accueilli au titre des 1°, 2° ou 3° de l'article L. 222-5, un an avant sa majorité, pour faire un bilan de son parcours et envisager les conditions de son accompagnement vers l'autonomie. Dans le cadre du projet pour l'enfant, un projet d'accès à l'autonomie est élaboré par le président du conseil départemental avec le mineur. Il y associe les institutions et organismes concourant à construire une réponse globale adaptée à ses besoins en matière éducative, sociale, de santé, de logement, de formation, d'emploi et de ressources (...) ". Enfin, aux termes du dernier alinéa de l'article R. 221-2 du même code : " S'agissant de mineurs émancipés ou de majeurs âgés de moins de vingt et un ans, le président du conseil départemental ne peut agir que sur demande des intéressés et lorsque ces derniers éprouvent des difficultés d'insertion sociale faute de ressources ou d'un soutien familial suffisants ".<br/>
<br/>
              4. Il résulte de ces dispositions que s'il incombe au président du conseil départemental de préparer l'accompagnement vers l'autonomie de tout mineur pris en charge par le service de l'aide sociale à l'enfance dans l'année précédant sa majorité, il dispose, sous le contrôle du juge, d'un large pouvoir d'appréciation pour accorder ou maintenir la prise en charge par ce service d'un jeune majeur de moins de vingt-et-un ans éprouvant des difficultés d'insertion sociale faute de ressources ou d'un soutien familial suffisants. Toutefois, lorsqu'une mesure de prise en charge d'un mineur parvenant à sa majorité, quel qu'en soit le fondement, arrive à son terme en cours d'année scolaire ou universitaire, il doit proposer à ce jeune un accompagnement, qui peut prendre la forme de toute mesure adaptée à ses besoins et à son âge, pour lui permettre de ne pas interrompre l'année scolaire ou universitaire engagée. <br/>
<br/>
              5. Pour rejeter la demande de suspension de l'exécution de la décision du 12 février 2018 présentée par M.A..., le juge des référés du tribunal administratif de Grenoble, après avoir relevé qu'il était scolarisé en classe " UPE2A " ou " unité pédagogique pour élèves allophones arrivants ", ce dont il résulte qu'il était en cours de scolarité, a jugé que l'intéressé ne se trouvait pas dans le cas où un accompagnement devrait lui être proposé pour lui permettre de terminer l'année scolaire ou universitaire engagée. En statuant ainsi, le juge des référés a commis une erreur de droit.<br/>
<br/>
              6. Par suite, M. A...est fondé à demander l'annulation de l'ordonnance qu'il attaque. Le moyen retenu suffisant à entraîner cette annulation, il n'est pas nécessaire d'examiner les autres moyens de son pourvoi.<br/>
<br/>
              7. Il y a lieu, en application du premier alinéa de l'article L. 821-2 du code de justice administrative, de régler l'affaire au titre de la procédure de référé engagée par M.A....<br/>
<br/>
              Sur l'intervention de la Cimade :<br/>
<br/>
              8. La Cimade justifie, eu égard à son objet statutaire et à la nature du litige, d'un intérêt suffisant pour intervenir au soutien de la demande de M.A.... Ainsi, son intervention est recevable.<br/>
<br/>
              Sur l'urgence :<br/>
<br/>
              9. L'urgence justifie que soit prononcée la suspension d'un acte administratif lorsque l'exécution de celui-ci porte atteinte, de manière suffisamment grave et immédiate, à un intérêt public, à la situation du requérant ou aux intérêts qu'il entend défendre. Eu égard aux effets particuliers d'une décision refusant de poursuivre la prise en charge au titre des deux derniers alinéas de l'article L. 222-5 du code de l'action sociale et des familles d'un jeune jusque là confié à l'aide sociale à l'enfance, la condition d'urgence doit en principe être constatée lorsqu'il demande la suspension d'une telle décision de refus. Il peut toutefois en aller autrement dans les cas où l'administration justifie de circonstances particulières, qu'il appartient au juge des référés de prendre en considération en procédant à une appréciation globale des circonstances de l'espèce qui lui est soumise.  <br/>
<br/>
              10. Il résulte de l'instruction que M.A..., confié au service de l'aide sociale à l'enfance du département de l'Isère jusqu'à sa majorité le 10 janvier 2018, s'est vu refuser le bénéfice d'un accompagnement en tant que jeune majeur par une décision du 12 février 2018. Pour soutenir que cette décision ne porte pas une atteinte grave et immédiate à sa situation, le département de l'Isère fait seulement valoir qu'il n'établit pas ne pas pouvoir être admis dans l'un des centres provisoires d'hébergement gérés par les services de l'Etat. Dans ces conditions, la condition d'urgence prévue à l'article L. 521-1 du code de justice administrative doit être regardée comme remplie.<br/>
<br/>
              Sur l'existence d'un moyen propre à créer un doute sérieux quant à la légalité de la décision litigieuse : <br/>
<br/>
              11. Aux termes de l'article R. 223-2 du code de l'action sociale et des familles, applicable à l'aide sociale à l'enfance : " Les décisions d'attribution, de refus d'attribution, de modification de la nature ou des modalités d'attribution d'une prestation doivent être motivées (...) ". Il résulte de ces dispositions qu'une décision refusant à un jeune majeur la mesure de prise en charge temporaire qu'il sollicite doit être motivée et, à ce titre, mentionner les considérations de droit et de fait sur lesquelles elle se fonde. <br/>
<br/>
              12. Le moyen tiré du défaut de motivation de la décision du 12 février 2018 par laquelle le président du conseil départemental de l'Isère a rejeté la demande de prise en charge de M. A...est de nature, en l'état de l'instruction, à faire naître un doute sérieux sur la légalité de cette décision.<br/>
<br/>
              13. Il résulte de tout ce qui précède que M. A...est fondé, sans qu'il soit besoin d'examiner les autres moyens qu'il soulève, à demander la suspension de l'exécution de cette décision du président du conseil départemental de l'Isère. <br/>
<br/>
              Sur les conclusions à fin d'injonction et d'astreinte :<br/>
<br/>
              14. La présente décision implique nécessairement que le département de l'Isère procède au réexamen de la situation du requérant. Il y a lieu, dès lors, de lui enjoindre de prendre une nouvelle décision dans un délai de quinze jours à compter de sa notification, sans qu'il soit besoin, dans les circonstances de l'espèce, d'assortir cette injonction d'une astreinte.<br/>
<br/>
              Sur les frais liés au litige :<br/>
<br/>
              15. M. A...a obtenu le bénéfice de l'aide juridictionnelle. Par suite, son avocat peut se prévaloir des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991. Il y a lieu, dans les circonstances de l'espèce, et sous réserve que la SCP Thouvenin, Coudray, Grévy, avocat de M.A..., renonce à percevoir la somme correspondant à la part contributive de l'État, de mettre à la charge du département de l'Isère une somme de 1 000 euros à verser à cette SCP.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : L'ordonnance du juge des référés du tribunal administratif de Grenoble du 25 avril 2018 est annulée. <br/>
Article 2 : L'affaire est renvoyée au juge des référés du tribunal administratif de Grenoble.<br/>
Article 3 : Le département de l'Isère versera une somme de 1 000 euros à la SCP Thouvenin, Coudray et Grévy, avocat de M.A..., en application des dispositions de l'article L. 761-1 du code de justice administrative et du deuxième alinéa de l'article 37 de la loi du 10 juillet 1991, sous réserve que cette société renonce à percevoir la somme correspondant à la part contributive de l'Etat.<br/>
Article 4 : La présente décision sera notifiée à M. B...A...et au département de l'Isère.<br/>
Copie en sera adressée à la Cimade et à la ministre des solidarités et de la santé.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
