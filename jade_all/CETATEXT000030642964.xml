<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030642964</ID>
<ANCIEN_ID>JG_L_2015_05_000000385430</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/64/29/CETATEXT000030642964.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème / 2ème SSR, 13/05/2015, 385430, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-05-13</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>385430</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème / 2ème SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MONOD, COLIN, STOCLET ; FOUSSARD</AVOCATS>
<RAPPORTEUR>M. François Lelièvre</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Bertrand Dacosta</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:385430.20150513</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              M. J...U...a demandé au tribunal administratif de Melun d'annuler les résultats du premier tour des élections municipales de Pontault-Combault qui s'est déroulé le 23 mars 2014, et, par voie de conséquence, ceux du second tour du 30 mars 2014.<br/>
<br/>
              Par un jugement n° 1402924-4 du 1er octobre 2014, le tribunal administratif de Melun a annulé ces opérations électorales.<br/>
<br/>
              Par une requête sommaire, un mémoire complémentaire, un mémoire en réplique et un nouveau mémoire, enregistrés les 31 octobre, 1er décembre 2014, 2 mars et 17 avril 2015 au secrétariat du contentieux du Conseil d'Etat, Mme AE...AS..., M. M... A..., Mme AJ...AQ..., M. AF...AA..., Mme R...H..., M. BA... AV..., Mme E...AB..., M. N...S..., Mme AK...AM..., M. AH... F...'homme, Mme AL...BD..., M. T...AP..., Mme K...Q..., M. L...I..., Mme C...P..., M. AT...AI..., Mme W...D..., M. AG... AU..., Mme AY...O..., M. X...Z..., Mme AT...G..., M. AX... AW..., Mme BE...-W...AN..., M. Y...AC..., Mme AZ...B..., Mme AR...V..., M. BB...AO..., MmeBC..., M. BB...AD...demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ; <br/>
<br/>
              2°) de rejeter la protestation de M. U...; <br/>
<br/>
              3°) subsidiairement, d'ordonner, sur le fondement de l'article R. 623-1 du code justice administrative, une enquête afin de recueillir un témoignage ;<br/>
<br/>
              4°) de mettre à la charge de M. U...le versement de la somme de 4 000 euros au titre de l'article L. 761-1 du code justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              -  le code électoral ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. François Lelièvre, Maître des Requêtes,  <br/>
<br/>
              - les conclusions de M. Bertrand Dacosta, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Monod, Colin, Stoclet, avocat de Mme AS...et autres, et à Me Foussard, avocat de M. U... ;<br/>
<br/>
<br/>
<br/>1. Considérant que lors du premier tour des élections municipales qui se sont déroulées le 23 mars 2014 dans la commune de Pontault-Combault, six listes étaient en présence, dont la " Liste démocratique et solidaire d'union de la gauche ", conduite par Mme AE...AS..., et la liste " Osons l'avenir ", conduite par M. J...U...; que la totalité des bulletins de la liste " Osons l'avenir " ont été déclarés nuls ; qu'à l'issue du second tour ayant eu lieu le 30 mars 2014, la liste conduite par Mme AS...a obtenu le plus grand nombre de voix et la majorité des sièges au conseil municipal ; que, par le jugement attaqué du 1er octobre 2014, le tribunal administratif de Melun a, sur la protestation de M.U..., annulé ces opérations électorales ;<br/>
<br/>
              2. Considérant, en premier lieu, qu'en sa qualité de candidat tête de la liste " Osons l'avenir " dont la totalité des bulletins du premier tour ont été déclarés nuls et qui n'a pu, pour ce motif, participer au second tour, M. U...dispose d'un intérêt à agir contre les résultats de ces élections ; que ne le prive pas de cet intérêt la circonstance qu'il serait à l'origine des irrégularités ayant conduit à ce que les bulletins de sa liste ont été déclarés nuls ;  <br/>
<br/>
              3. Considérant, en deuxième lieu, d'une part, qu'aux termes du premier alinéa de l'article L.O. 247-1 du code électoral, dans sa version issue de la loi organique n° 2013-402 du 17 mai 2013 : " Dans les communes soumises au mode de scrutin prévu au chapitre III du présent titre, les bulletins de vote imprimés distribués aux électeurs comportent, à peine de nullité, en regard du nom des candidats ressortissants d'un Etat membre de l'Union européenne autre que la France, l'indication de leur nationalité " ; qu'il résulte des termes mêmes de cet article que l'omission de l'indication de la nationalité sur les bulletins de vote des candidats ressortissants d'un Etat membre de l'Union européenne autre que la France entache, à elle seule, ces bulletins de nullité ; que, d'autre part, aux termes de l'article L. 241 de ce code : " Des commissions, dont la composition et le fonctionnement sont fixés par décret, sont chargées, pour les communes de 2 500 habitants et plus, d'assurer l'envoi et la distribution des documents de propagande électorale " ; qu'aux termes du 3ème alinéa de l'article R. 38 de ce code : " La commission n'assure pas l'envoi des circulaires qui ne sont pas conformes aux articles R. 27 et R. 29 et des bulletins de vote qui ne sont pas conformes à l'article R. 30 et aux prescriptions édictées pour chaque catégorie d'élection. " ;<br/>
<br/>
              4. Considérant qu'en application des dispositions de l'article L.O. 247-1 du code électoral, les bulletins de la liste " Osons l'avenir ", conduite par M.U..., qui ne mentionnaient pas les nationalités respectivement espagnole et portugaise de deux de ses candidates, ont été déclarés nuls lors des opérations de dépouillement à l'issue du premier tour de scrutin ; que la seule circonstance que, quelques jours avant le premier tour, la candidate d'une autre liste ait indiqué à M. U...que les bulletins en cause ne comportaient pas la mention requise, sans que ce dernier décide de procéder à leur rectification, n'est pas de nature à établir que l'intéressé aurait sciemment commis cette irrégularité dans le but d'obtenir l'annulation des opérations électorales ; <br/>
<br/>
              5. Considérant, en troisième lieu, qu'en raison de cette irrégularité, la liste " Osons l'avenir ", qui a recueilli 1 876 voix sur un total de 11 711 votants, soit 16,02 % des suffrages au premier tour, n'a pu être candidate au second tour et n'a obtenu aucun représentant au conseil municipal ; qu'ainsi, l'expression du suffrage des électeurs de Pontault-Combault qui ont voté pour cette liste s'est trouvée privée de portée utile ; qu'il suit de là, sans que les requérants puissent utilement faire valoir qu'au premier tour, l'écart de voix entre leur liste et les deux listes arrivées en deuxième et troisième positions était très significatif, que la sincérité du scrutin en a été altérée ;<br/>
<br/>
              6. Considérant qu'il résulte de ce qui précède, et sans qu'il soit besoin de prescrire la mesure d'instruction qu'ils demandent, que Mme AS...et autres ne sont pas fondés à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Melun a annulé les opérations électorales ;<br/>
<br/>
              7. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mis à la charge de M.U..., qui n'est pas la partie perdante dans la présente instance, le versement des sommes que demandent Mme AS...et autres ; qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions de M. U... au titre des mêmes dispositions ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de Mme AS...et autres est rejetée.<br/>
Article 2 : Les conclusions de M. U...au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La présente décision sera notifiée à Mme AE...AS..., première requérante dénommée, à M. J...U...et au ministre de l'intérieur. <br/>
Copie en sera adressée pour information à la Commission nationale des comptes de campagne et des financements politiques.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
