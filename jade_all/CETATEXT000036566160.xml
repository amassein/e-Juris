<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036566160</ID>
<ANCIEN_ID>JG_L_2018_02_000000411472</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/56/61/CETATEXT000036566160.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème - 8ème chambres réunies, 01/02/2018, 411472</TITRE>
<DATE_DEC>2018-02-01</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>411472</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème - 8ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Cécile Isidoro</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Vincent Daumas</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:411472.20180201</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, enregistrée le 12 juin 2017 au secrétariat du contentieux du Conseil d'Etat, M. A... doit être regardé comme demandant au Conseil d'Etat d'annuler l'arrêté du 23 mai 2017 par lequel le ministre des affaires étrangères et du développement international a prononcé sa démission d'office de son mandat de conseiller consulaire à Pékin (Chine-2ème circonscription).<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la loi organique n° 76-97 du 31 janvier 1976 ;<br/>
              - la loi n° 2013-659 du 22 juillet 2013 ; <br/>
              - le décret n° 2005-1613 du 22 décembre 2005 ;<br/>
              - le code électoral ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Cécile Isidoro, maître des requêtes, <br/>
<br/>
              - les conclusions de M. Vincent Daumas, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article 16 de la loi du 22 juillet 2013 relative à la représentation des Français établis hors de France : " Sont éligibles au conseil consulaire les électeurs inscrits sur l'une des listes électorales consulaires de la circonscription électorale dans laquelle ils se présentent (...) ". Aux termes du troisième alinéa de l'article 17 de la même loi : " Tout conseiller consulaire (...) qui, pour une cause survenue postérieurement à son élection, se trouve dans un des cas d'inéligibilité prévus par la présente loi est dans les trois mois déclaré démissionnaire d'office par arrêté du ministre des affaires étrangères, sauf recours devant le Conseil d'Etat formé dans le délai d'un mois à compter de la notification ". L'article 15 de cette même loi rend applicables à l'élection des conseillers consulaires les dispositions de l'article L. 330-2 du code électoral qui prévoit que : " Sont électrices les personnes inscrites sur les listes électorales consulaires dressées en application de la loi organique du 31 janvier 1976 ". Indépendamment de la voie administrative de retranchement des listes électorales consulaires qui s'y trouve prévue, et dont les conditions sont précisées par le décret du 22 décembre 2005 pris pour son application, l'article 9 de la loi du 31 janvier 1976 relative aux listes électorales consulaires et au vote des Français établis hors de France pour l'élection du Président de la République ouvre une voie juridictionnelle de radiation des listes électorales consulaires, qui dispose que tout citoyen peut réclamer devant le tribunal d'instance du 1er arrondissement de Paris la radiation d'électeurs indûment inscrits. <br/>
<br/>
              2. Eu égard au lien créé par ces dispositions entre la qualité d'électeur, tirée de l'inscription sur les listes électorales consulaires, et la condition d'éligibilité à la fonction de conseiller consulaire, il appartient au ministre des affaires étrangères de prononcer la démission d'office d'un conseiller consulaire dont la perte de la qualité d'électeur résulterait d'un retranchement administratif opéré sur les listes électorales ou d'un retranchement juridictionnel prononcé par le tribunal d'instance du 1er arrondissement de Paris.<br/>
<br/>
              3. Il résulte de l'instruction que, par une requête en date du 17 mars 2017, M. C... et M. B...ont saisi le tribunal d'instance du 1er arrondissement de Paris afin d'obtenir la radiation de M. A...de la liste électorale consulaire de Pékin (Chine). Par un jugement du 21 avril 2017, ce tribunal a fait droit à leur demande. Par un arrêté du 23 mai 2017, le ministre des affaires étrangères a déclaré démissionnaire d'office M. A...de sa qualité de conseiller consulaire. Par un nouveau jugement du 31 août 2017, après l'annulation pour vice de procédure de son précédent jugement par un arrêt rendu par la Cour de Cassation le 2 juin 2017, le tribunal a autorisé la radiation immédiate de M. A...de la liste électorale par un jugement du 31 août 2017. <br/>
<br/>
              4. Considérant qu'à la date de la présente décision, la base légale de l'arrêté du 23 mai 2017 est constituée par le jugement du 31 août 2017 du tribunal d'instance du 1er arrondissement de Paris constatant la perte de la qualité d'électeur de M. A...en raison de son déménagement hors de la circonscription électorale consulaire de Pékin et autorisant en conséquence sa radiation de la liste électorale consulaire. Il n'est pas contesté que ce jugement ne serait pas devenu définitif. En conséquence, M. A...n'est pas fondé à demander l'annulation de cet arrêté au motif qu'il aurait conservé sa qualité d'électeur, sans que revête d'incidence sur l'issue du litige la circonstance que le jugement sur lequel était initialement fondé l'arrêté litigieux a été annulé par la Cour de cassation.<br/>
<br/>
              5. Compte tenu de l'intervention du jugement du 31 août 2017, les conclusions à fin de sursis à statuer présentées par le ministre de l'Europe et des affaires étrangères doivent être rejetées.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de M. A... est rejetée.<br/>
Article 2 : La présente décision sera notifiée à M. A...et au ministre de l'Europe et des affaires étrangères.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">28-07 ÉLECTIONS ET RÉFÉRENDUM. ÉLECTIONS DIVERSES. - CONSEILLERS CONSULAIRES - DÉMISSION D'OFFICE (ART. 17 DE LA LOI DU 22 JUILLET 2013) - 1) CONSEILLER CONSULAIRE AYANT PERDU SA QUALITÉ D'ÉLECTEUR EN RAISON D'UN RETRANCHEMENT ADMINISTRATIF OU JURIDICTIONNEL DES LISTES ÉLECTORALES - OBLIGATION POUR LE MINISTRE DE PRONONCER LA DÉMISSION D'OFFICE - EXISTENCE - 2) RECOURS TENDANT À L'ANNULATION D'UN ARRÊTÉ PORTANT DÉMISSION D'OFFICE - DATE À LAQUELLE LE JUGE SE PLACE POUR STATUER SUR LA LÉGALITÉ DE CET ARRÊTÉ - DATE DE SA DÉCISION.
</SCT>
<ANA ID="9A"> 28-07 1) Eu égard au lien créé par les  articles 15, 16 et 17 de la loi n° 2013-659 du 22 juillet 2013 relative à la représentation des français établis hors de France entre la qualité d'électeur, tirée de l'inscription sur les listes électorales consulaires, et la condition d'éligibilité à la fonction de conseiller consulaire, il appartient au ministre des affaires étrangères de prononcer la démission d'office d'un conseiller consulaire dont la perte de la qualité d'électeur résulterait d'un retranchement administratif opéré sur les listes électorales ou d'un retranchement juridictionnel prononcé par le tribunal d'instance du 1er arrondissement de Paris, en application de l'article 9 de la loi n° 76-97 du 31 janvier 1976 relative aux listes électorales consulaires et au vote des Français établis hors de France pour l'élection du Président de la République.... ,,2) Requête tendant à l'annulation d'un arrêté du ministère des affaires étrangères déclarant un conseiller consulaire démissionnaire d'office à la suite de sa radiation de la liste électorale par un jugement du tribunal d'instance du premier arrondissement de Paris. Postérieurement à cet arrêté de radiation, la Cour de cassation a annulé ce jugement pour vice de forme et le tribunal a, par nouveau jugement, autorisé la radiation immédiate du conseiller consulaire de la liste électorale.... ,,Eu égard à l'office du juge électoral, juge de plein contentieux, la légalité de l'arrêté devait être appréciée à la date à laquelle il statuait. A cette date, la base légale de l'arrêté était constituée par le second jugement du tribunal d'instance du 1er arrondissement de Paris. Le requérant n'était donc pas fondé à demander son annulation, sans qu'ait d'incidence sur ce point la circonstance que le jugement sur lequel était initialement fondé l'arrêté avait été annulé par la Cour de cassation.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
