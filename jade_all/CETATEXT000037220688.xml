<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037220688</ID>
<ANCIEN_ID>JG_L_2018_07_000000408805</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/22/06/CETATEXT000037220688.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère et 4ème chambres réunies, 18/07/2018, 408805, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-07-18</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>408805</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère et 4ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GARREAU, BAUER-VIOLAS, FESCHOTTE-DESBOIS</AVOCATS>
<RAPPORTEUR>M. Jean-Luc Nevache</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Charles Touboul</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:408805.20180718</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 10 mars 2017, 12 juin 2017 et 4 juillet 2018 au secrétariat du contentieux du Conseil d'Etat, l'association des pharmaciens dispensateurs et distributeurs de gaz médicinal et la société SOS Oxygène distribution demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret n° 2017-20 du 9 janvier 2017 relatif aux établissements pharmaceutiques et à l'inspection par l'Agence nationale de sécurité du médicament et des produits de santé et portant simplification de procédures mises en oeuvre par cette agence ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 7 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la Constitution, notamment son Préambule et ses articles 34 et 37 ;<br/>
              - la directive 2001/83/CE du parlement européen et du conseil du 6 novembre 2001 instituant un code communautaire relatif aux médicaments à usage humain ;<br/>
              - le code de la santé publique ;<br/>
              - l'ordonnance n° 45-1014 du 23 mai 1945 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Luc Nevache, conseiller d'Etat, <br/>
<br/>
              - les conclusions de M. Charles Touboul, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Garreau, Bauer-Violas, Feschotte-Desbois, avocat de l'association des pharmaciens dispensateurs et distributeurs de gaz médicinal et de la société SOS Oxygène distribution.<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Par le décret du 9 janvier 2017 relatif aux établissements pharmaceutiques et à l'inspection par l'Agence nationale de sécurité du médicament et des produits de santé et portant simplification de procédures mises en oeuvre par cette agence, le Premier ministre a notamment modifié l'article R. 5124-2 du code de la santé publique, qui définissait le " distributeur en gros de gaz à usage médical " comme " l'entreprise se livrant à l'achat et au stockage de gaz à usage médical, en vue de leur distribution en gros et en l'état ", pour préciser qu'il s'agit des seuls gaz à usage médical " conditionnés ". Il a, également, modifié l'article R. 5124-7 du même code pour supprimer la possibilité, pour les établissements pharmaceutiques se livrant à la fabrication ou à l'importation de gaz à usage médical, de comprendre des réservoirs de stockage situés dans des lieux dépendant des distributeurs en gros de gaz à usage médical et pour supprimer la possibilité, pour les établissements se livrant à la distribution en gros de gaz à usage médical, de comprendre des réservoirs situés dans des lieux dépendant des établissements ou organismes disposant d'une pharmacie à usage intérieur ou des personnes morales autorisées à dispenser à domicile des gaz à usage médical. L'association des pharmaciens dispensateurs et distributeurs de gaz médicinal et la société SOS Oxygène distribution doivent être regardées comme demandant l'annulation pour excès de pouvoir du décret du 9 janvier 2017 en tant qu'il modifie ainsi les dispositions applicables au gaz à usage médical.<br/>
<br/>
              Sur la légalité externe des dispositions critiquées : <br/>
<br/>
              2. En premier lieu, aux termes de l'article 34 de la Constitution du 4 octobre 1958 : " La loi fixe les règles concernant (...)  les garanties fondamentales accordées aux citoyens pour l'exercice des libertés publiques ". Selon le premier alinéa de son article 37 : " Les matières autres que celles qui sont du domaine de la loi ont un caractère réglementaire ". Au nombre des libertés publiques, dont les garanties fondamentales doivent, en vertu de la Constitution, être déterminées par le législateur, figure le libre exercice par les citoyens d'une activité professionnelle n'ayant fait l'objet d'aucune limitation légale. Toutefois, l'acte dit loi du 11 septembre 1941 relative à l'exercice de la pharmacie, validé et modifié par l'ordonnance du 23 mai 1945, a réservé l'exercice de l'activité de préparation et de vente en gros des produits pharmaceutiques aux pharmaciens et l'a subordonnée à l'obtention d'une autorisation. Compte tenu des limitations qui ont été ainsi apportées par le législateur, antérieurement à la Constitution du 4 octobre 1958, à l'exercice de cette activité, le Premier ministre pouvait,  en vertu des pouvoirs qu'il tient de l'article 37 de la Constitution, préciser, par des dispositions qui s'appliquent aux gaz à usage médical ayant le statut de médicament, les activités relevant de la fabrication et celles relevant de la distribution en gros, en réservant les opérations de conditionnement aux fabricants. Par suite, le moyen tiré de ce que les dispositions attaquées auraient été édictées par une autorité incompétente doit être écarté.  <br/>
<br/>
              3. En deuxième lieu, lorsqu'un décret doit être pris en Conseil d'Etat, le texte retenu par le Gouvernement ne peut être différent à la fois du projet qu'il avait soumis au Conseil d'Etat et du texte adopté par ce dernier. En l'espèce, il ressort de la copie de la minute de la section sociale du Conseil d'Etat, versée au dossier par le ministre des solidarités et de la santé, que le projet a été examiné par cette section le 12 décembre 2016, avant la signature du décret, et que le texte de ce dernier ne contient pas de disposition qui diffèrerait à la fois du projet initial du Gouvernement et du texte adopté par la section sociale. Par suite, les règles qui gouvernent l'examen par le Conseil d'Etat des projets de décret n'ont pas été méconnues.<br/>
<br/>
              4. En troisième lieu, aux termes de l'article L. 5311-2 du code de la santé publique, l'Agence nationale de sécurité du médicament et des produits de santé " participe à la préparation des textes législatifs et réglementaires ". Toutefois, ces dispositions ne font pas, par elles-mêmes, obligation à l'autorité compétente de consulter l'Agence nationale du médicament et des produits de santé préalablement à l'édiction d'actes réglementaires. Au demeurant, il ressort des pièces du dossier que cette agence a été associée à l'élaboration du décret contesté. Par suite, le moyen tiré du défaut de sa consultation ne peut qu'être écarté.<br/>
<br/>
              Sur la légalité interne des dispositions critiquées :<br/>
<br/>
              5. En premier lieu, l'association et la société requérantes soutiennent que les dispositions réglementaires qu'elles critiquent méconnaissent la liberté d'entreprendre, la liberté du commerce et de l'industrie ou le principe de libre concurrence, porteraient atteinte au principe d'égalité par la différence de traitement opérée entre fabricants et distributeurs en gros de gaz à usage médical et sont entachées d'erreur manifeste d'appréciation.<br/>
<br/>
              6. Eu égard aux dispositions de l'article 88-1 de la Constitution, selon lesquelles " la République participe à l'Union européenne constituée d'Etats qui ont choisi librement d'exercer en commun certaines de leurs compétences en vertu du traité sur l'Union européenne et du traité sur le fonctionnement de l'Union européenne, tels qu'ils résultent du traité signé à Lisbonne le 13 décembre 2007 ", d'où découle une obligation constitutionnelle de transposition des directives, le contrôle de légalité et de constitutionnalité des actes réglementaires assurant directement cette transposition est appelé à s'exercer selon des modalités particulières dans le cas où le contenu de ces actes découle nécessairement des obligations prévues par les directives, sans que le pouvoir réglementaire ne dispose de pouvoir d'appréciation. Si le contrôle des règles de compétence et de procédure ne se trouve pas affecté, il appartient au juge administratif, saisi d'un moyen tiré de la méconnaissance d'une disposition ou d'un principe de valeur constitutionnelle, de rechercher s'il existe une règle ou un principe général du droit  de l'Union européenne qui, eu égard à sa nature et à sa portée, tel qu'il est interprété en l'état actuel de la jurisprudence du juge de l'Union, garantit par son application l'effectivité du respect de la disposition ou du principe constitutionnel invoqué. Dans l'affirmative, il y a lieu pour le juge administratif, afin de s'assurer de la constitutionnalité du décret, de rechercher si la directive que ce décret transpose est conforme à cette règle ou à ce principe général du droit de l'Union. Il lui revient, en l'absence de difficulté sérieuse, d'écarter le moyen invoqué, ou, dans le cas contraire, de saisir la Cour de justice  de l'Union européenne d'une question préjudicielle, dans les conditions prévues par l'article 167 du traité sur le fonctionnement de l'Union européenne. En revanche, s'il n'existe pas de règle ou de principe général du droit  de l'Union garantissant l'effectivité du respect de la disposition ou du principe constitutionnel invoqué, il revient au juge administratif d'examiner directement la constitutionnalité des dispositions réglementaires contestées.<br/>
<br/>
              7. La directive 2001/83/CE du 6 novembre 2001 instituant un code communautaire relatif aux médicaments à usage humain définit, par le point 23 de son article 1er, le " conditionnement primaire " d'un médicament comme le " récipient ou toute autre forme de conditionnement qui se trouve en contact direct avec le médicament " et exige, par son article 40, une autorisation de fabrication tant pour la fabrication totale ou partielle des médicaments que pour les opérations de division, de conditionnement ou de présentation, tout en précisant que " cette autorisation n'est pas exigée pour les préparations, divisions, changements de conditionnement ou présentation, dans la mesure où ces opérations sont exécutées, uniquement en vue de la délivrance au détail, par des pharmaciens dans une officine ou par d'autres personnes légalement autorisées dans les États membres à effectuer lesdites opérations ". L'article 41 de la directive exige, pour l'obtention de l'autorisation de fabrication, que le demandeur satisfasse à des conditions, tenant aux  locaux, à l'équipement technique et aux possibilités de contrôle ainsi qu'à la qualification de la personne responsable, plus rigoureuses que celles exigées par l'article 79 pour l'obtention de l'autorisation de distribution, les premières devant garantir la sécurité de la fabrication, du contrôle et de la conservation des médicaments, tandis que les secondes doivent seulement permettre d'assurer une bonne conservation et une bonne distribution des médicaments. Enfin, le paragraphe 3 de l'article 77 de la directive prévoit que : " La possession d'une autorisation de fabrication emporte celle de distribuer en gros les médicaments concernés par cette autorisation. La possession d'une autorisation d'exercer l'activité de grossiste en médicaments ne dispense pas de l'obligation de posséder l'autorisation de fabrication et de respecter les conditions fixées à cet égard, même lorsque l'activité de fabrication ou d'importation est exercée accessoirement ". <br/>
<br/>
              8. Il résulte clairement de ces dispositions qu'une personne autorisée à exercer l'activité de grossiste en médicaments ne peut, sauf à posséder également une autorisation de fabrication, procéder à des opérations de conditionnement de médicaments, lesquelles incluent tout changement du récipient se trouvant en contact direct avec le médicament.<br/>
<br/>
              9. Dès lors, en réservant aux fabricants les activités de conditionnement de gaz à usage médical ayant le statut de médicament en vue de la vente en gros, y compris lorsque ces activités prennent la forme du remplissage, par du gaz liquéfié, de réservoirs de stockage, les dispositions attaquées du décret du 9 janvier 2017 assurent une exacte transposition des dispositions de la directive du 6 novembre 2001 citées au point 7, par des dispositions dont le contenu découle nécessairement des obligations prévues par cette directive. <br/>
<br/>
              10. Aux termes de l'article 16 de la charte des droits fondamentaux de l'Union européenne : " La liberté d'entreprise est reconnue conformément au droit de l'Union et aux législations et pratiques nationales ". En vertu de la jurisprudence de la Cour de justice, la protection conférée par l'article 16 de la charte comporte la liberté d'exercer une activité économique ou commerciale, la liberté contractuelle et la concurrence libre. La liberté d'entreprise ne constituant pas une prérogative absolue, mais devant être examinée au regard de sa fonction dans la société, elle peut être soumise à un large éventail d'interventions de la puissance publique susceptibles d'établir, dans l'intérêt général, des limitations à l'exercice de l'activité économique. Toute limitation de l'exercice de cette liberté doit être prévue par la loi, respecter son contenu essentiel et, dans le respect du principe de proportionnalité, être nécessaire et répondre effectivement à des objectifs d'intérêt général reconnus par l'Union ou au besoin de protection des droits et libertés d'autrui. En outre, le principe d'égalité de traitement, qui fait partie des principes généraux du droit de l'Union, exige notamment que des situations comparables ne soient pas traitées de manière différente, à moins qu'un tel traitement ne soit objectivement justifié. Ces principes ont une portée garantissant l'effectivité du respect des principes constitutionnels de liberté d'entreprendre et d'égalité, lequel implique la libre concurrence, dont la méconnaissance est alléguée par les requérants.<br/>
<br/>
              11. En exigeant que tout acteur de la chaîne d'approvisionnement qui conditionne des médicaments détienne une autorisation de fabrication, soumise à niveau d'exigence plus élevé que l'autorisation de distribution, le législateur européen a posé, eu égard au caractère très particulier des médicaments, dont les effets thérapeutiques les distinguent substantiellement de toute autre marchandise, et des risques que présente toute opération de conditionnement, pouvant s'entendre de toute modification du récipient ou autre conditionnement qui se trouve en contact direct avec le médicament, une condition qui est justifiée par l'objectif d'intérêt général de protection de la santé humaine, reconnu par l'article 35 de la charte, auquel elle est proportionnée. Il n'a pas davantage enfreint le principe d'égalité de traitement, eu égard à la différence de situation existant entre opérateurs économiques selon qu'ils satisfont aux conditions leur permettant d'obtenir une autorisation de fabrication ou seulement à une autorisation de distribution.   <br/>
<br/>
              12. Par suite, les moyens tirés de la méconnaissance de la liberté d'entreprendre, du principe d'égalité et, en tout état de cause, du principe de libre concurrence, doivent être écartés. Enfin, eu égard à ce qui a été dit au point 9, les requérants ne peuvent utilement soutenir que les dispositions réglementaires qu'ils critiquent méconnaîtraient la liberté du commerce et de l'industrie ou seraient entachées d'erreur manifeste d'appréciation.<br/>
<br/>
              13. En second lieu, l'article L. 4211-5 du code de la santé publique, qui prévoit que " des personnes morales respectant les bonnes pratiques de distribution définies par arrêté du ministre chargé de la santé peuvent être autorisées à dispenser à domicile, sous la responsabilité d'un pharmacien inscrit à l'ordre des pharmaciens en section A, D et E, des gaz à usage médical ", fait usage de la faculté ouverte par l'article 40 de la directive du 6 novembre 2001, cité au point 7, qui permet à de telles personnes de procéder à des changements de conditionnement en vue de la délivrance au détail sans exiger d'autorisation de fabrication. Par suite, les requérants ne peuvent utilement soutenir que les dispositions qu'ils critiquent, en ce qu'elles traitent les distributeurs en gros de gaz à usage médical différemment des personnes autorisées à dispenser à domicile des gaz à usage médical, méconnaîtraient le principe d'égalité. Enfin, s'ils soutiennent que les dispositions de l'article R. 5124-7 du code de la santé publique relatives à ces personnes seraient illégales, ces dispositions ne sont pas issues du décret attaqué et leur illégalité supposée ne peut être utilement invoquée par voie d'exception à l'appui de leurs conclusions dirigées contre ce décret, qui n'a pas été pris pour leur application et n'y trouve pas sa base légale. <br/>
<br/>
              14. Il résulte de tout ce qui précède que l'association des pharmaciens dispensateurs et distributeurs de gaz médicinal et la société SOS Oxygène distribution ne sont pas fondées à demander l'annulation des dispositions qu'elles attaquent du décret du 9 janvier 2017.<br/>
<br/>
              15. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise, à ce titre, à la charge de l'Etat, qui n'est pas la partie perdante dans la présente instance. <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
                            --------------<br/>
<br/>
Article 1er : La requête de l'association des pharmaciens dispensateurs et distributeurs de gaz médicinal et de la société SOS Oxygène distribution est rejetée.<br/>
Article 2 : La présente décision sera notifiée à l'association des pharmaciens dispensateurs et distributeurs de gaz médicinal, à la société SOS Oxygène distribution et à la ministre des solidarités et de la santé. <br/>
Copie en sera adressée au Premier ministre.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
