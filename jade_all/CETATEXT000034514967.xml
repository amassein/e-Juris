<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034514967</ID>
<ANCIEN_ID>JG_L_2017_04_000000400832</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/51/49/CETATEXT000034514967.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème - 1ère chambres réunies, 28/04/2017, 400832</TITRE>
<DATE_DEC>2017-04-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>400832</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème - 1ère chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>LE PRADO</AVOCATS>
<RAPPORTEUR>M. Jean-Baptiste de Froment</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Suzanne von Coester</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:400832.20170428</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              Par une requête, enregistrée le 21 juin 2016 au secrétariat du contentieux du Conseil d'Etat, la SELARL " Avocats consultants associés pour le conseil et le contentieux immobilier et administratif " (A.C.A.C.C.I.A.) demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision du 14 janvier 2016 du Conseil national des barreaux portant réforme du règlement intérieur national de la profession d'avocat ;<br/>
<br/>
              2°) d'annuler pour excès de pouvoir la décision du 4 mai 2016 par laquelle le Conseil national des barreaux a rejeté sa demande tendant à la modification de sa décision du 14 janvier, afin qu'elle ne s'applique pas aux sociétés d'avocats enregistrées au registre du commerce et des sociétés avant le 16 avril 2016 ; <br/>
<br/>
              3°) à titre subsidiaire, d'annuler cette décision en tant qu'elle s'applique aux situations en cours et aux contrats de sociétés existants et à titre très subsidiaire, d'annuler cette décision en tant qu'elle ne comporte pas de période transitoire.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              -	la directive 2006/123/CE du Parlement européen et du Conseil du 12 décembre 2006 ;<br/>
              -	la loi n° 71-1130 du 31 décembre 1971 ;<br/>
              - 	le décret n° 91-1197 du 27 novembre 1991 ;<br/>
              -	le décret n° 2005-790 du 12 juillet 2005 ;<br/>
              -	le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Baptiste de Froment, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Suzanne von Coester, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Le Prado, avocat du Conseil national des barreaux ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant que l'article 53 de la loi du 31 décembre 1971 portant réforme de certaines professions judiciaires et juridiques dispose que : " Dans le respect de l'indépendance de l'avocat, de l'autonomie des conseils de l'ordre et du caractère libéral de la profession, des décrets en Conseil d'Etat fixent les conditions d'application du présent titre. / Ils précisent notamment : (...) / 2° Les règles de déontologie (...) " ; que l'article 21-1 de la même loi, dans sa rédaction applicable au litige, dispose que : " Dans le respect des dispositions législatives et réglementaires en vigueur, le Conseil national des barreaux unifie par voie de dispositions générales les règles et usages de la profession d'avocat " ; qu'aux termes de l'article 17 de la même loi, le conseil de l'ordre de chaque barreau " a pour attribution de traiter toutes questions intéressant l'exercice de la profession et de veiller à l'observation des devoirs des avocats ainsi qu'à la protection de leurs droits (...) " et a pour tâches, notamment : " 1° D'arrêter et, s'il y a lieu, de modifier les dispositions du règlement intérieur (...) / ; 10° D'assurer dans son ressort l'exécution des décisions prises par le Conseil national des barreaux " ;<br/>
<br/>
              2. Considérant qu'il résulte de ces dispositions que le Conseil national des barreaux est investi par la loi d'un pouvoir réglementaire, qui s'exerce en vue d'unifier les règles et usages des barreaux et dans le cadre des lois et règlements qui régissent la profession ; que ce pouvoir trouve cependant sa limite dans les droits et libertés qui appartiennent aux avocats et dans les règles essentielles de l'exercice de la profession ; que le Conseil national des barreaux ne peut légalement fixer des prescriptions nouvelles qui mettraient en cause la liberté d'exercice de la profession d'avocat ou les règles essentielles qui la régissent et qui n'auraient aucun fondement dans les règles législatives ou dans celles fixées par les décrets en Conseil d'Etat prévus par l'article 53 de la loi du 31 décembre 1971, ou ne seraient pas une conséquence nécessaire d'une règle figurant au nombre des traditions de la profession ; <br/>
<br/>
              3. Considérant que l'article 3 du décret du 12 juillet 2005 relatif aux règles de déontologie de la profession d'avocat dispose que : " L'avocat exerce ses fonctions avec dignité, conscience, indépendance, probité et humanité, dans le respect des termes de son serment. / Il respecte en outre, dans cet exercice, les principes d'honneur, de loyauté, de désintéressement, de confraternité, de délicatesse, de modération et de courtoisie. / Il fait preuve, à l'égard de ses clients, de compétence, de dévouement, de diligence et de prudence. " ; <br/>
<br/>
              4. Considérant que selon les dispositions de l'article 10.6.3 du règlement intérieur national de la profession d'avocat, issues de la décision attaquée du Conseil national des barreaux du 14 janvier 2016 : " Les dénominations s'entendent du nom commercial, de l'enseigne, de la marque, de la dénomination ou raison sociale ou de tout autre terme par lequel un avocat ou une structure d'exercice sont identifiés ou reconnus. / La dénomination, quelle qu'en soit la forme, est un mode de communication. / L'utilisation de dénominations évoquant de façon générique le titre d'avocat ou un titre pouvant prêter à confusion, un domaine du droit, une spécialisation ou une activité relevant de celle de l'avocat, est interdite. " ; <br/>
<br/>
              5. Considérant, en premier lieu, que l'article 24 de la directive 2006/123/CE du 12 décembre 2006 du Parlement européen et du Conseil relative aux services dans le marché intérieur prévoit que : " 1. Les Etats membres suppriment toutes les interdictions totales visant les communications commerciales des professions réglementées. / 2. Les Etats membres veillent à ce que les communications commerciales faites par les professions réglementées respectent les règles professionnelles conformes au droit communautaire, qui visent notamment l'indépendance, la dignité et l'intégrité de la profession ainsi que le secret professionnel, en fonction de la spécificité de chaque profession. Les règles professionnelles en matière de communications commerciales doivent être non discriminatoires, justifiées par une raison impérieuse d'intérêt général et proportionnées. " ; que le paragraphe 12 de l'article 4 de la même directive définit la communication commerciale comme " toute forme de communication destinée à promouvoir directement ou indirectement, les biens, les services ou l'image d'une (...) personne exerçant une profession réglementée " et précise que " Ne constituent pas en tant que telles des communications commerciales : (...) b) les communications relatives aux biens, aux services ou à l'image de l'entreprise, de l'organisation ou de la personne élaborées d'une manière indépendante, en particulier lorsqu'elles sont fournies sans contrepartie financière " ; qu'il résulte clairement de ces dispositions que la dénomination par laquelle un avocat ou une structure d'exercice est identifié ou reconnu ne constitue pas une " communication commerciale " au sens de cette directive ; que, dès lors, le moyen tiré de la méconnaissance de la directive 2006/123/CE ne peut qu'être écarté ; <br/>
<br/>
              6. Considérant, en deuxième lieu, que les dispositions de l'article 10.6.3 du règlement intérieur national de la profession d'avocat visent, en précisant les conditions et limites dans lesquelles peuvent être choisies les dénominations des cabinets d'avocats et des sociétés d'exercice, à assurer, dans l'intérêt général de la profession, le respect des principes essentiels qui régissent la profession d'avocat et des exigences déontologiques rappelées au point 3 ci-dessus ; qu'en particulier elles tendent à empêcher que, par sa dénomination, un cabinet ou une société d'exercice cherche à s'approprier, directement ou indirectement, un domaine du droit ou un domaine d'activité que se partage la profession dans des conditions qui seraient de nature à créer la confusion dans l'esprit du public, au détriment des autres avocats ; qu'en revanche, les dispositions critiquées n'interdisent pas aux professionnels concernés de mentionner dans leurs dénominations leurs domaines de spécialité ou d'activité, dès lors qu'elles ne sont pas de nature à prêter à confusion dans l'esprit du public et satisfont ainsi à l'objectif rappelé ci dessus ; qu'il résulte de ce qui précède que, contrairement à ce que soutient la société requérante, le Conseil national des barreaux était compétent, sur le fondement des dispositions citées au point 1 ci-dessus, pour édicter, au titre de sa mission d'harmonisation des usages et règles de la profession avec les lois et décrets en vigueur, les règles contestées, qui ne subordonnent pas à des conditions nouvelles l'exercice de la profession d'avocat et ne méconnaissent ni la liberté d'exercice de la profession d'avocat ni les règles essentielles qui la régissent ; que les dispositions critiquées sont par ailleurs exemptes d'erreur manifeste d'appréciation ; <br/>
<br/>
              7. Considérant, en troisième et dernier lieu, que les dispositions de la loi du 31 décembre 1971 portant réforme de certaines professions judiciaires et juridiques relatives à la déontologie des avocats citées au point 1, dont la mise en oeuvre est assurée par le décret du 12 juillet 2005 relatif aux règles de déontologie de la profession d'avocat mais aussi par le règlement intérieur national des avocats ont, en raison des impératifs d'ordre public sur lesquelles elles reposent, vocation à s'appliquer à l'ensemble des membres de la profession ; que les dispositions critiquées du règlement intérieur national se bornent, ainsi qu'il a été dit au point 6 ci-dessus, à préciser les conditions et limites dans lesquelles peuvent être choisies les dénominations par lesquelles les avocats ou les structures d'exercice sont identifiés ou reconnus, afin d'assurer le respect des exigences déontologiques qui s'imposent aux avocats ; qu'il en résulte, eu égard à l'objet et à la portée des dispositions critiquées et aux impératifs d'ordre public sur lesquelles elles reposent, qu'en n'exceptant pas de leur application les structures existantes, le Conseil national des barreaux n'a pas illégalement porté atteinte, contrairement à ce que soutient la société requérante, à des situations contractuelles existantes ; qu'en ne prévoyant pas une période transitoire, le Conseil national des barreaux n'a pas davantage méconnu le principe de sécurité juridique, dès lors que, en tout état de cause, aucune sanction ne pourrait, le cas échéant, être prise contre une société sans qu'il soit tenu compte du temps nécessaire pour que celle-ci puisse procéder au changement de sa dénomination ;  <br/>
<br/>
              8. Considérant qu'il résulte de tout ce qui précède que la requête de la SELARL A.C.A.C.C.I.A. doit être rejetée ; <br/>
<br/>
              9. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mise à la charge du Conseil national des barreaux, qui n'est pas la partie perdante dans la présente instance, la somme que demande la SELARL A.C.A.C.C.I.A. au titre des frais exposés par elle et non compris dans les dépens ; qu'il n'y a pas lieu, dans les circonstances de l'espèce, de mettre à la charge de la SELARL A.C.A.C.C.I.A. la somme que demande le Conseil national des barreaux, au titre de ces mêmes dispositions ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de la SELARL A.C.A.C.C.I.A. est rejetée. <br/>
Article 2 : Les conclusions du Conseil national des barreaux au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La présente décision sera notifiée à la SELARL A.C.A.C.C.I.A. et au Conseil national des barreaux. <br/>
Copie en sera adressée au garde des sceaux, ministre de la justice. <br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-04-03-07 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - VIOLATION DIRECTE DE LA RÈGLE DE DROIT. PRINCIPES GÉNÉRAUX DU DROIT. PRINCIPES INTÉRESSANT L'ACTION ADMINISTRATIVE. - PRINCIPE DE SÉCURITÉ JURIDIQUE - OBLIGATION POUR L'AUTORITÉ INVESTIE DU POUVOIR RÉGLEMENTAIRE D'ÉDICTER LES MESURES TRANSITOIRES QU'IMPLIQUE, S'IL Y A LIEU, UNE RÉGLEMENTATION NOUVELLE [RJ1] - ABSENCE EN L'ESPÈCE, DÈS LORS QU'AUCUNE SANCTION NE POURRAIT ÊTRE PRONONCÉE SANS QU'IL SOIT TENU COMPTE DU TEMPS NÉCESSAIRE POUR SE METTRE EN CONFORMITÉ AVEC LA RÉGLEMENTATION NOUVELLE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">01-08-01 ACTES LÉGISLATIFS ET ADMINISTRATIFS. APPLICATION DANS LE TEMPS. ENTRÉE EN VIGUEUR. - PRINCIPE DE SÉCURITÉ JURIDIQUE - OBLIGATION POUR L'AUTORITÉ INVESTIE DU POUVOIR RÉGLEMENTAIRE D'ÉDICTER LES MESURES TRANSITOIRES QU'IMPLIQUE, S'IL Y A LIEU, UNE RÉGLEMENTATION NOUVELLE [RJ1] - ABSENCE EN L'ESPÈCE, DÈS LORS QU'AUCUNE SANCTION NE POURRAIT ÊTRE PRONONCÉE SANS QU'IL SOIT TENU COMPTE DU TEMPS NÉCESSAIRE POUR SE METTRE EN CONFORMITÉ AVEC LA RÉGLEMENTATION NOUVELLE.
</SCT>
<ANA ID="9A"> 01-04-03-07 Modification par le Conseil national des barreaux du règlement intérieur national de la profession d'avocat pour préciser les conditions et limites dans lesquelles peuvent être choisies les dénominations par lesquelles les avocats ou les structures d'exercice sont identifiés ou reconnus, afin d'assurer le respect des exigences déontologiques qui s'imposent aux avocats.... ...En ne prévoyant pas une période transitoire, le Conseil national des barreaux n'a pas méconnu le principe de sécurité juridique dès lors que, en tout état de cause, aucune sanction ne pourrait, le cas échéant, être prise contre une société sans qu'il soit tenu compte du temps nécessaire pour que celle-ci puisse procéder au changement de sa dénomination.</ANA>
<ANA ID="9B"> 01-08-01 Modification par le Conseil national des barreaux du règlement intérieur national de la profession d'avocat pour préciser les conditions et limites dans lesquelles peuvent être choisies les dénominations par lesquelles les avocats ou les structures d'exercice sont identifiés ou reconnus, afin d'assurer le respect des exigences déontologiques qui s'imposent aux avocats.... ...En ne prévoyant pas une période transitoire, le Conseil national des barreaux n'a pas méconnu le principe de sécurité juridique dès lors que, en tout état de cause, aucune sanction ne pourrait, le cas échéant, être prise contre une société sans qu'il soit tenu compte du temps nécessaire pour que celle-ci puisse procéder au changement de sa dénomination.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, Assemblée, 24 mars 2006, Société KPMG et autres, n° 288460, p. 154.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
