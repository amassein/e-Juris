<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042409941</ID>
<ANCIEN_ID>JG_L_2020_10_000000423114</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/40/99/CETATEXT000042409941.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème chambre, 07/10/2020, 423114, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-10-07</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>423114</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FABIANI, LUC-THALER, PINATEL</AVOCATS>
<RAPPORTEUR>Mme Catherine Brouard-Gallet</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Raphaël Chambon</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2020:423114.20201007</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Par une requête sommaire et un mémoire complémentaire, enregistrés les 10 août et 12 novembre 2018 au secrétariat du contentieux du Conseil d'Etat, M. A... D... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir, en premier lieu, la décision du 9 février 2018 par laquelle la 5ème section du Conseil national des universités n'a pas retenu sa candidature en vue d'une inscription sur la liste de qualification aux fonctions de professeur des universités, en deuxième lieu, la lettre du 11 juin 2018 de la ministre de l'enseignement supérieur, de la recherche et de l'innovation l'informant de ce que, à la suite de son recours hiérarchique, la 5ème section du Conseil national des universités a de nouveau délibéré sur sa candidature, et en troisième lieu, la décision du 4 mai 2018 par laquelle la 5ème section du Conseil national des universités a refusé de nouveau de l'inscrire sur la liste de qualification aux fonctions de professeur des universités ;<br/>
<br/>
              2°) d'enjoindre au Conseil national des universités de réexaminer sa demande d'inscription sur la liste de qualification aux fonctions de professeur des universités ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 000 euros en application de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'éducation ;<br/>
              - la loi n° 2008-496 du 27 mai 2008 ; <br/>
              - le décret n° 84-431 du 6 juin 1984 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme B... C..., conseillère d'Etat en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Raphaël Chambon, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Fabiani, Luc-Thaler, Pinatel, avocat de M. D... ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier que, par une délibération du 9 février 2018, la 5ème section du Conseil national des universités (CNU) n'a pas retenu la candidature de M. D... en vue d'une inscription sur la liste de qualification aux fonctions de professeur des universités. A la suite du recours de M. D... auprès de la ministre de l'enseignement supérieur, de la recherche et de l'innovation, la 5ème section du CNU a de nouveau délibéré sur sa candidature lors de sa séance du 4 mai 2018 et refusé de l'inscrire sur la liste de qualification aux fonctions de professeur des universités. Par une lettre du 11 juin 2018, la ministre de l'enseignement supérieur, de la recherche et de l'innovation a informé M. D... de ce que, à la suite de son recours, la même section avait de nouveau délibéré sur sa candidature et lui a indiqué que les résultats de cette nouvelle délibération lui seraient communiqués prochainement. M. D... demande l'annulation pour excès de pouvoir des décisions du 9 février 2018 et du 4 mai 2018 de la 5ème section du CNU refusant de l'inscrire sur la liste de qualification aux fonctions de professeur des universités et de la lettre de la ministre de l'enseignement supérieur, de la recherche et de l'innovation du 11 juin 2018.<br/>
<br/>
              Sur les conclusions dirigées contre la décision du Conseil national des universités du 9 février 2018 : <br/>
<br/>
              2. Il ressort des pièces du dossier que la 5ème section du CNU, à la demande de la ministre de l'enseignement supérieur, de la recherche et de l'innovation qui avait été saisie du recours de M. D... contre la délibération de cette section en date du 9 février 2018 refusant son inscription sur la liste de qualification aux fonctions de professeur des universités, a pris une nouvelle décision en date du 4 mai 2018, qui refuse son inscription sur la liste de qualification aux fonctions de professeur des universités et qui s'est substituée à sa première décision du 9 février 2018. Par suite, les conclusions de M. D... tendant à l'annulation de la décision du 9 février 2018, qui ont été présentées au Conseil d'Etat par une requête enregistrée le 10 août 2018, sont irrecevables et ne peuvent qu'être rejetées. <br/>
<br/>
              Sur les conclusions dirigées contre la lettre de la ministre de l'enseignement supérieur, de la recherche et de l'innovation du 11 juin 2018 : <br/>
<br/>
              3. Il ressort des pièces du dossier que, par la lettre du 11 juin 2018, le chef de service, adjoint au directeur général des ressources humaines du ministère de l'éducation nationale et de la jeunesse et du ministère de l'enseignement supérieur, de la recherche et de l'innovation, s'est borné à informer M. D... que, à la suite de son recours, la 5ème section du CNU avait de nouveau délibéré sur sa candidature lors de sa séance du 4 mai 2018 et de ce que les résultats de cette nouvelle délibération lui seraient communiqués prochainement. Cette lettre ne présente pas le caractère d'une décision susceptible de faire l'objet d'un recours pour excès de pouvoir. Il s'ensuit que les conclusions tendant à son annulation ne peuvent qu'être rejetées comme irrecevables. <br/>
<br/>
              Sur les conclusions dirigées contre la décision du Conseil national des universités du 4 mai 2018 : <br/>
<br/>
              4. Aux termes de l'article 45 du décret du 6 juin 1984 fixant les dispositions statutaires communes applicables aux enseignants-chercheurs et portant statut particulier du corps des professeurs des universités et du corps des maîtres de conférences : " I.- Les demandes d'inscription sur la liste de qualification aux fonctions de professeur des universités, assorties d'un dossier individuel de qualification, sont examinées par la section compétente du Conseil national des universités (...). La qualification est appréciée par rapport aux différentes fonctions des enseignants-chercheurs mentionnées à l'article L. 952-3 du code de l'éducation et compte tenu des diverses activités des candidats. / Après avoir entendu deux rapporteurs désignés par son bureau pour chaque candidat, la section compétente du Conseil national des universités arrête, par ordre alphabétique, la liste de qualification aux fonctions de professeur des universités. (...) / III.- Le bureau communique par écrit à chaque candidat non inscrit sur la liste les motifs pour lesquels sa candidature a été écartée. (...) ".<br/>
<br/>
              5. En premier lieu, il résulte des dispositions de l'article 45 du décret du 6 juin 1984 citées au point 4 que la notification de la décision par laquelle la section compétente du CNU refuse d'inscrire un candidat sur la liste de qualification aux fonctions de professeur d'université doit, à peine d'illégalité de la décision, être assortie de la communication de l'énoncé des motifs qui la fondent. En l'espèce, il ressort des pièces du dossier que la décision attaquée mentionne que la candidature de M. D... est rejetée en raison de l'absence de publication dont il soit le seul auteur dans une revue scientifique de qualité dans sa nouvelle spécialité, l'économie de l'éducation, d'un portefeuille de cours dispensés étroit et recouvrant peu de champs en économie. Par suite, le moyen tiré du défaut de communication des motifs de la décision attaquée doit être écarté.<br/>
<br/>
              6. En deuxième lieu, il ressort des pièces du dossier, en particulier du procès-verbal de la séance de la 5ème section du CNU en date du 4 mai 2018, que celle-ci a examiné de nouveau la candidature de M. D... à l'inscription sur la liste de qualification aux fonctions de professeur d'université lors de cette séance et qu'elle a refusé cette inscription. Par suite, M. D... n'est pas fondé à soutenir que la décision du 4 mai 2018 serait irrégulière en ce que la 5ème section du CNU n'aurait pas délibéré sur sa candidature lors de sa séance du même jour. <br/>
<br/>
              7. En troisième lieu, M. D... soutient que la décision attaquée est entachée de discrimination, directe et indirecte, en ce que l'appréciation de la 5ème section du CNU serait fondée sur des critères ayant pour objet et pour effet de désavantager les candidats appartenant au courant académique dit " hétérodoxe ".<br/>
<br/>
              8. Aux termes de l'article 1er de la loi du 27 mai 2008 portant diverses dispositions d'adaptation au droit communautaire dans le domaine de la lutte contre les discriminations : " Constitue une discrimination directe la situation dans laquelle, sur le fondement de son origine, de son sexe, de sa situation de famille, de sa grossesse, de son apparence physique, de la particulière vulnérabilité résultant de sa situation économique, apparente ou connue de son auteur, de son patronyme, de son lieu de résidence ou de sa domiciliation bancaire, de son état de santé, de sa perte d'autonomie, de son handicap, de ses caractéristiques génétiques, de ses moeurs, de son orientation sexuelle, de son identité de genre, de son âge, de ses opinions politiques, de ses activités syndicales, de sa capacité à s'exprimer dans une langue autre que le français, de son appartenance ou de sa non-appartenance, vraie ou supposée, à une ethnie, une nation, une prétendue race ou une religion déterminée, une personne est traitée de manière moins favorable qu'une autre ne l'est, ne l'a été ou ne l'aura été dans une situation comparable. / Constitue une discrimination indirecte une disposition, un critère ou une pratique neutre en apparence, mais susceptible d'entraîner, pour l'un des motifs mentionnés au premier alinéa, un désavantage particulier pour des personnes par rapport à d'autres personnes, à moins que cette disposition, ce critère ou cette pratique ne soit objectivement justifié par un but légitime et que les moyens pour réaliser ce but ne soient nécessaires et appropriés ".<br/>
<br/>
              9. D'une part, il n'appartient pas au juge de l'excès de pouvoir de contrôler l'appréciation générale de la qualité des travaux des candidats à laquelle doit procéder une section du CNU dans le cadre de la procédure d'inscription sur la liste de qualification aux fonctions de professeur des universités. D'autre part, la circonstance, à la supposer établie, que la 5ème section du CNU apprécierait la production scientifique des candidats au regard d'un outil de classification des revues élaboré par le Comité national de la recherche scientifique qui défavoriserait le courant académique dit " hétérodoxe ", ne relève pas d'un des motifs de discrimination énumérés par les dispositions invoquées par le requérant et citées au point 8. M. D... n'est donc pas fondé, par les moyens qu'il invoque, à soutenir que la décision attaquée résulterait d'une discrimination directe ou indirecte prohibée par l'article 1er de la loi du 27 mai 2008.<br/>
<br/>
              10. Il résulte de tout ce qui précède que la requête de M.  D... doit être rejetée, y compris ses conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de M. D... est rejetée.<br/>
Article 2 : La présente décision sera notifiée à M. A... D... et à la ministre de l'enseignement supérieur, de la recherche et de l'innovation.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
