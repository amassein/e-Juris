<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030956616</ID>
<ANCIEN_ID>JG_L_2015_07_000000374207</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/95/66/CETATEXT000030956616.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème SSJS, 27/07/2015, 374207, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-07-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>374207</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème SSJS</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>LE PRADO</AVOCATS>
<RAPPORTEUR>Mme Marie Gautier-Melleray</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Fabienne Lambolez</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2015:374207.20150727</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Procédure contentieuse antérieure :<br/>
<br/>
              M. A...B...a demandé au tribunal administratif de Marseille d'annuler la décision du 31 août 2005 par laquelle le ministre de l'intérieur, de la sécurité intérieure et des libertés locales l'a mis à la retraite d'office par mesure disciplinaire. Par un jugement n° 0507891 du 22 mai 2008, le tribunal administratif a rejeté la demande.<br/>
<br/>
              Par un arrêt n° 08MA3284 du 8 juillet 2010, la cour administrative d'appel de Marseille a rejeté l'appel formé contre ce jugement par M. B....<br/>
<br/>
              Par une décision n° 346666  du 10 octobre 2012, le Conseil d'Etat, faisant droit au pourvoi de M.B..., a annulé cet arrêt. <br/>
<br/>
              Par un arrêt n° 12MA04194 du 19 juillet 2013, la cour administrative d'appel de Marseille a rejeté l'appel formé par M.B....<br/>
<br/>
<br/>
              Procédure devant le Conseil d'Etat :<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 23 décembre 2013 et 17 mars 2014 au secrétariat du contentieux du Conseil d'Etat, M. B...demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat  la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu :<br/>
<br/>
              - la loi n° 83-634 du 13 juillet 1983 ;<br/>
<br/>
              - le décret n° 84-961 du 25 octobre 1984 ;<br/>
<br/>
              - le code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Marie Gautier-Melleray, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Fabienne Lambolez, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Le Prado, avocat de M. B...;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, par un arrêté du 31 août 2005, le ministre de l'intérieur et de l'aménagement du territoire a prononcé à l'encontre de M.B..., gardien de la paix, la sanction de mise à la retraite d'office ; que, par un jugement du 22 mai 2008, le tribunal administratif de Marseille a rejeté la demande de l'intéressé tendant à l'annulation de cette décision ; que, par une décision du 10 octobre 2012, le Conseil d'Etat, statuant au contentieux, a annulé l'arrêt du 8 juillet 2010 par lequel la cour administrative d'appel de Marseille avait rejeté l'appel formé par M.B... ; que ce dernier se pourvoit en cassation contre l'arrêt du 19 juillet 2013 par lequel la même cour, statuant sur renvoi après cassation, a rejeté à nouveau son appel ;<br/>
<br/>
              2. Considérant qu'il appartient au juge de l'excès de pouvoir, saisi de moyens en ce sens, de rechercher si les faits reprochés à un agent public ayant fait l'objet d'une sanction disciplinaire constituent des fautes de nature à justifier une sanction et si la sanction retenue est proportionnée à la gravité de ces fautes ; qu'en se bornant à juger que la sanction infligée à M. B... n'était pas, dans les circonstances de l'espèce, manifestement disproportionnée par rapport aux fautes constatées, la cour a commis une erreur de droit ; que, par suite et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, son arrêt doit être annulé ;<br/>
<br/>
              3. Considérant qu'aux termes du second alinéa de l'article L. 821-2 du code de justice administrative : " Lorsque l'affaire fait l'objet d'un second pourvoi en cassation, le Conseil d'Etat statue définitivement sur cette affaire " ; qu'il y a lieu, par suite, de régler l'affaire au fond ; <br/>
<br/>
              Sur la régularité du jugement attaqué :<br/>
<br/>
              4. Considérant que le jugement du 22 mai 2008 du tribunal administratif de Marseille dont M. B...relève appel expose de manière circonstanciée les faits retenus à l'encontre de celui-ci et énonce les raisons pour lesquelles le tribunal les regarde comme constitutifs de fautes disciplinaires ; que, par suite, contrairement à ce que soutient le requérant, ce jugement n'est pas entaché d'insuffisance de motivation ; <br/>
<br/>
              Sur la légalité de l'arrêté du 31 août 2005 :<br/>
<br/>
              5. Considérant, en premier lieu, que la décision attaquée, qui prononce au terme d'une procédure disciplinaire une des sanctions prévues par les dispositions applicables, ne saurait être regardée comme une " sanction déguisée " ; que le moyen tiré de ce que les garanties inhérentes à la procédure disciplinaire n'auraient pas été respectées n'est pas assorti des précisions permettant d'en apprécier le bien-fondé ; <br/>
<br/>
              6. Considérant en deuxième lieu que, pour prononcer la sanction litigieuse, le ministre de l'intérieur et de l'aménagement du territoire a retenu que, le 11 mai 2004, M. B...avait quitté précipitamment le service, sans prévenir ses supérieurs ni donner la moindre explication, que, le 21 mai 2004, alors qu'il était en congé maladie, il avait quitté son domicile en dehors des heures de sorties autorisées et effectuait des travaux sur le chantier de son pavillon en construction, qu'après sa reprise de travail le 7 septembre 2004 à l'issue d'un nouvel arrêt de maladie, il avait refusé de porter l'uniforme réglementaire et que le comportement général de cet agent, qui s'était déjà signalé défavorablement à plusieurs reprises, à l'égard de ses supérieurs et de ses collègues, portait atteinte à la bonne marche du service ; <br/>
<br/>
              7. Considérant qu'aux termes de l'article 113-34 du règlement général d'emploi de la police nationale, résultant de l'arrêté du 22 juillet 1996 en vigueur à la date de l'arrêté litigieux : " Les fonctionnaires actifs de la police nationale qui feront l'objet d'un contrôle administratif à domicile ou d'un contrôle médical et qui auront refusé de s'y soumettre ou qui seront absents en dehors des heures de sortie autorisée s'exposent à des sanctions disciplinaires " ; qu'il ressort des pièces du dossier que, lors d'un contrôle administratif effectué le 11 mai 2004 par ses supérieurs, M.B..., alors en congé de maladie, était absent de son domicile ; que, toutefois, l'intéressé soutient qu'aucun horaire de présence à domicile n'avait été fixé lors de l'octroi du congé maladie ; que cette affirmation, que le ministre de l'intérieur n'a pas contestée, n'est contredite par aucune pièce du dossier ; <br/>
<br/>
              8. Considérant, cependant, que les autres faits retenus par le ministre de l'intérieur pour prononcer la sanction litigieuse ne sont pas sérieusement contestés par le requérant et présentent le caractère de fautes disciplinaires ; qu'il résulte de l'instruction que le ministre aurait pris la même décision s'il n'avait retenu que ces seuls faits ; qu'en raison de la répétition de comportements incompatibles avec les obligations professionnelles des gardiens de la paix, et compte tenu de l'âge de l'intéressé à la date de la décision attaquée, la sanction de la mise à la retraite d'office ne peut être regardée comme disproportionnée ; <br/>
<br/>
              9. Considérant, enfin, que le détournement de pouvoir allégué n'est pas établi ;<br/>
<br/>
              10. Considérant qu'il résulte de ce qui précède que M. B...n'est pas fondé à se plaindre de ce que, par son jugement du 22 mai 2008, le tribunal administratif de Marseille a rejeté son recours pour excès de pouvoir contre l'arrêté du 31 août 2005 du ministre de l'intérieur et de l'aménagement du territoire ; <br/>
<br/>
              Sur les conclusions indemnitaires :<br/>
<br/>
              11. Considérant que M.B..., qui s'est borné, devant le tribunal administratif de Marseille, à demander l'annulation de l'arrêté du 31 août 2005, n'est pas recevable à présenter pour la première fois devant le juge d'appel des conclusions tendant à la condamnation de l'Etat à lui verser une indemnité ; <br/>
<br/>
              Sur les conclusions à fin d'injonction :<br/>
<br/>
              12. Considérant que la présente décision n'appelle aucune mesure d'exécution ; que les conclusions de M. B...tendant au prononcé d'une injonction ne sauraient, par suite, être accueillies ;<br/>
<br/>
              Sur les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              13. Considérant que les dispositions de cet article font obstacle à ce que les sommes demandées à ce titre par M. B...soient mises à la charge de l'Etat ; <br/>
<br/>
<br/>
<br/>
<br/>			D E C I D E :<br/>
              			--------------<br/>
<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Marseille du 19 juillet 2013 est annulé.<br/>
Article 2 : La requête présentée par M. B... devant la cour administrative d'appel de Marseille et le surplus des conclusions de son pourvoi sont rejetés.<br/>
Article 3 : La présente décision sera notifiée à M. A... B...et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
