<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029626702</ID>
<ANCIEN_ID>JG_L_2014_10_000000361231</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/62/67/CETATEXT000029626702.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème / 1ère SSR, 24/10/2014, 361231</TITRE>
<DATE_DEC>2014-10-24</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>361231</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème / 1ère SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP NICOLAY, DE LANOUVELLE, HANNOTIN</AVOCATS>
<RAPPORTEUR>Mme Sophie Roussel</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Xavier de Lesquen</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:361231.20141024</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 20 juillet et 22 octobre 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour la société Unibail-Rodamco, dont le siège est 7, place du Chancelier Adenauer à Paris (75116) ; la société demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler l'arrêt n° 11PA01103 du 11 mai 2012 par lequel la cour administrative d'appel de Paris a rejeté sa requête tendant à la réformation du jugement n° 0708802/4 du 16 décembre 2010 par lequel le tribunal administratif de Melun n'a que partiellement accueilli sa demande tendant à la condamnation de l'Etat à l'indemniser des préjudices qu'elle aurait subis du fait des fautes commises par le préfet de Seine-et-Marne dans l'exercice de ses pouvoirs de police en matière d'installations classées pour la protection de l'environnement ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 7 octobre 2014, présentée pour la société Unibail-Rodamco ; <br/>
<br/>
              Vu le code de l'environnement ;<br/>
<br/>
              Vu la loi n° 75-633 du 15 juillet 1975 ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Sophie Roussel, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Xavier de Lesquen, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Nicolaÿ, de Lanouvelle, Hannotin, avocat de la société Unibail Rodamco ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que la société anonyme Imprimerie François, autorisée à exploiter une installation classée pour la protection de l'environnement à Ozoir-la-Ferrière (Seine-et-Marne), a été placée en liquidation judiciaire le 1er juillet 1991 par un jugement du tribunal de commerce de Melun ; qu'une partie des salariés de cette entreprise a alors occupé le site pour tenter de continuer l'exploitation, sans autorisation, jusqu'en 1994 ; que, dès le 16 octobre 1991, le préfet de Seine-et-Marne a rappelé à l'exploitant autorisé, représenté par le liquidateur, son obligation de remettre le site en état et lui a prescrit diverses mesures à cette fin, sur le fondement de la législation relative aux installations classées pour la protection de l'environnement, puis l'a mis en demeure d'y procéder, par arrêté du 23 janvier 1992 ; que, par un arrêté du 19 octobre 1994, le préfet a mis en demeure la société Unibail-Rodamco, entreprise propriétaire d'une partie du site depuis 1994 et liée à l'exploitant par un contrat de crédit-bail depuis le 1er janvier 1990, de réaliser les opérations de remise en état dans un délai de deux mois et de déterminer l'éventuelle nécessité de travaux de dépollution des lieux puis, par des arrêtés en date des 17 janvier 1996 et 5 octobre 1998, lui a imposé des prescriptions complémentaires et l'a, par un arrêté du 26 juillet 2001, mise en demeure de respecter ces prescriptions ; que la société Unibail-Rodamco a saisi le tribunal administratif de Melun d'une demande tendant notamment à la condamnation de l'Etat en réparation des préjudices nés, selon elle, des fautes que le préfet de Seine-et-Marne aurait commises, d'une part, en n'obtenant pas de la part du mandataire liquidateur, en sa qualité de dernier exploitant, qu'il s'acquitte de ses obligations de remise en état du site et, d'autre part, en lui prescrivant d'effectuer une remise en état qui ne peut légalement incomber, selon elle, qu'à l'ancien exploitant ; que, par un jugement du 16 décembre 2010, le tribunal administratif de Melun a rejeté cette demande ; que, par un arrêt du 11 mai 2012, contre lequel la société Unibail-Rodamco se pourvoit en cassation, la cour administrative d'appel de Paris a rejeté la requête de cette société ; <br/>
<br/>
              Sur la responsabilité de l'Etat au titre des obligations de l'ancien exploitant :<br/>
<br/>
              2. Considérant, en premier lieu, que contrairement à ce qui est soutenu, la cour administrative d'appel de Paris n'a pas exigé que les faits reprochés à l'Etat vis-à-vis du mandataire liquidateur de la société Imprimerie François entre 1991 et 1994 soient la cause exclusive et intégrale du dommage mais s'est bornée, par une appréciation souveraine suffisamment motivée et sans entacher son arrêt de contradiction de motifs, à constater l'absence de lien de causalité entre le préjudice invoqué et le comportement, à le supposer fautif, du préfet ; que si la cour a par ailleurs relevé, d'une part, que les travaux de dépollution du site étaient en tout état de cause nécessaires du fait, pour partie, d'une pollution antérieure à la période litigieuse et, d'autre part, que le préjudice invoqué par la société, constitué par un manque à gagner du fait d'un retard prétendument apporté dans les possibilités de réutilisation de la partie du site qu'elle a conservée, n'était pas établi, il résulte des termes mêmes de l'arrêt attaqué que ces motifs présentent un caractère surabondant ; <br/>
<br/>
              3. Considérant, en second lieu, que le moyen tiré de ce que l'immobilisation du site résultant de la pollution, qui le rendait impropre à toute utilisation, serait, par elle-même, à l'origine d'un préjudice indemnisable, n'avait pas été soulevé devant la cour administrative d'appel de Paris ; qu'il n'est pas d'ordre public ; qu'il ne peut par suite être utilement invoqué en cassation pour contester l'arrêt attaqué ; <br/>
<br/>
              Sur la responsabilité de l'Etat au titre des mesures prise à l'égard de la société Unibail-Rodamco :<br/>
<br/>
              4. Considérant que, pour écarter l'engagement de la responsabilité de l'Etat au titre de l'illégalité fautive des trois arrêtés préfectoraux, respectivement en date des 19 octobre 1994, 17 janvier 1996 et 5 octobre 1998, pris sur le fondement de la législation relative aux installations classées pour la protection de l'environnement et prescrivant à la société Unibail-Rodamco la remise en état du site, la cour administrative d'appel de Paris a jugé que la circonstance selon laquelle la société pouvait être regardée, en sa qualité de propriétaire, comme détenteur des déchets polluant le sous-sol et les eaux souterraines du site, au sens des dispositions de la loi du 15 juillet 1975 relative à l'élimination des déchets et à la récupération des matériaux, aujourd'hui codifiée aux articles L. 541-1 et suivants du code de l'environnement, faisait obstacle à ce que l'illégalité fautive entachant ces arrêtés puisse être regardée comme étant à l'origine des préjudices allégués par la société à l'appui de sa demande indemnitaire ; <br/>
<br/>
              5. Considérant, toutefois, que sont responsables des déchets, au sens des dispositions de la loi du 15 juillet 1975, les producteurs ou autres détenteurs connus des déchets ; qu'en leur absence, le propriétaire du terrain sur lequel ils ont été déposés peut être regardé comme leur détenteur, au sens de l'article L. 541-2 du code de l'environnement, et être de ce fait assujetti à l'obligation de les éliminer, notamment s'il a fait preuve de négligence à l'égard d'abandons sur son terrain ou s'il ne pouvait ignorer, à la date à laquelle il est devenu propriétaire de ce terrain, d'une part, l'existence de ces déchets, d'autre part, que la personne y ayant exercé une activité productrice de déchets ne serait pas en mesure de satisfaire à ses obligations ;  <br/>
<br/>
              6. Considérant qu'il résulte de ce qui précède qu'en se fondant, pour juger que la société Unibail-Rodamco était responsable de l'élimination de ces déchets, sur la seule circonstance qu'elle était propriétaire des terrains pollués par des solvants chlorés provenant de l'exploitation de l'Imprimerie François, alors qu'il lui appartenait de se prononcer au regard des principes rappelés au point 5, la cour administrative d'appel de Paris a commis une erreur de droit ; que, par suite, son arrêt doit être annulé en tant qu'il statue sur l'indemnisation des préjudices résultant des illégalités fautives qui entacheraient les arrêtés préfectoraux des 19 octobre 1994, 17 janvier 1996 et 5 octobre 1998 ; <br/>
<br/>
              7. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat une somme de 3 000 euros à verser à la société Unibail-Rodamco au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Paris du 11 mai 2012 est annulé en tant qu'il statue sur l'indemnisation du préjudice résultant de l'illégalité fautive des arrêtés préfectoraux des 19 octobre 1994, 17 janvier 1996 et 5 octobre 1998.  <br/>
<br/>
Article 2 : L'affaire est renvoyée, dans cette mesure, à la cour administrative d'appel de Paris. <br/>
<br/>
Article 3 : L'Etat versera à la société Unibail-Rodamco une somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : La présente décision sera notifiée à la société Unibail-Rodamco et à la ministre de l'écologie, du développement durable et de l'énergie.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">44-035-05 NATURE ET ENVIRONNEMENT. - NOTION DE RESPONSABLE DES DÉCHETS - PRODUCTEURS OU DÉTENTEURS DES DÉCHETS - POSSIBILITÉ, À DÉFAUT, DE REGARDER LE PROPRIÉTAIRE DU TERRAIN COMME RESPONSABLE - EXISTENCE, DANS CERTAINS CAS [RJ1].
</SCT>
<ANA ID="9A"> 44-035-05 Sont responsables des déchets, au sens des dispositions de la loi du 15 juillet 1975, les producteurs ou autres détenteurs connus des déchets. En leur absence, le propriétaire du terrain sur lequel ils ont été déposés peut être regardé comme leur détenteur, au sens de l'article L. 541-2 du code de l'environnement, et être de ce fait assujetti à l'obligation de les éliminer, notamment s'il a fait preuve de négligence à l'égard d'abandons sur son terrain ou s'il ne pouvait ignorer, à la date à laquelle il est devenu propriétaire de ce terrain, d'une part, l'existence de ces déchets, d'autre part, que la personne y ayant exercé une activité productrice de déchets ne serait pas en mesure de satisfaire à ses obligations.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1]Cf. CE, 26 juillet 2011, Commune de Palais-sur-Vienne, n° 328651, T. p. 1035.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
