<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000041775174</ID>
<ANCIEN_ID>JG_L_2020_03_000000432076</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/41/77/51/CETATEXT000041775174.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème - 3ème chambres réunies, 27/03/2020, 432076</TITRE>
<DATE_DEC>2020-03-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>432076</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème - 3ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BAUER-VIOLAS, FESCHOTTE-DESBOIS, SEBAGH</AVOCATS>
<RAPPORTEUR>M. Laurent Domingo</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Karin Ciavaldini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:432076.20200327</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société Blue Boats a demandé au tribunal administratif de Montpellier d'annuler les décisions du maire de la commune de Palavas-les-Flots du 24 mai 2017 et du 1er août 2017 prononçant la résiliation de la convention d'occupation du domaine public qu'ils avaient conclue le 8 juillet 2014, de prononcer la reprise des relations contractuelles et de l'indemniser des préjudices qu'elle estimait avoir subis. Par un jugement nos 1703389, 1704695 du 12 avril 2018, le tribunal a prononcé un non-lieu à statuer sur les conclusions dirigées contre la décision du 24 mai 2017 et a rejeté le surplus des conclusions de la société.<br/>
<br/>
              Par un arrêt n° 18MA02718 du 29 avril 2019, la cour administrative d'appel de Marseille a, sur appel de la société Blue Boats, annulé ce jugement et prononcé la reprise immédiate des relations contractuelles entre la commune de Palavas-les-Flots et la société Blue Boats dans le cadre de la convention conclue le 8 juillet 2014.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 1er juillet et 1er octobre 2019 au secrétariat du contentieux du Conseil d'Etat, la commune de Palavas-les-Flots demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler les articles 1er à 3 de cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de la société Blue Boats ;<br/>
<br/>
              3°) de mettre à la charge de la société Blue Boats la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code général de la propriété des personnes publiques ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Laurent Domingo, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Karin Ciavaldini, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Bauer-Violas, Feschotte-Desbois, Sebagh, avocat de la commune de Palavas-les-flots ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que la commune de Palavas-les-Flots a conclu, le 8 juillet 2014, avec la société Blue Boats une convention l'autorisant à occuper, pour une durée de quinze ans, une dépendance du domaine public communal constituée par un terre-plein situé sur le quai de l'île Cazot, en vue d'y exploiter une activité de location de bateaux sans permis et une activité de restauration. Par une décision du 24 mai 2017, le maire de la commune de Palavas-les-Flots a informé la société Blue Boats de sa décision de résilier cette convention. Après avoir retiré cette décision, le maire de la commune a de nouveau informé la société, par un courrier du 1er août 2017, de sa décision de résilier la convention à compter du 10 août 2017. Par un jugement du 12 avril 2018, le tribunal administratif de Montpellier a, d'une part, constaté qu'il n'y avait pas lieu de statuer sur les conclusions relatives à la première résiliation et a, d'autre part, rejeté les conclusions de la société Blue Boats contestant la validité de la seconde résiliation et tendant à la reprise des relations contractuelles ainsi qu'à l'indemnisation du préjudice qu'elle estimait avoir subi. Par un arrêt du 29 avril 2019, la cour administrative d'appel de Marseille, après avoir annulé le jugement du tribunal administratif, a prononcé la reprise immédiate des relations contractuelles dans le cadre de la convention conclue le 8 juillet 2014, mis à la charge de la commune une somme de 2 000 euros à verser à la société Blue Boats au titre de l'article L. 761-1 du code de justice administrative et rejeté le surplus des conclusions de la requête de cette société. La commune de Palavas-les-Flots se pourvoit en cassation contre cet arrêt en tant qu'il statue sur les conclusions de la société relatives à la résiliation décidée le 24 mai 2017 et à la reprise des relations contractuelles.<br/>
<br/>
              2. En premier lieu, après avoir relevé que le tribunal administratif de Montpellier avait à bon droit constaté qu'il n'y avait pas lieu de statuer sur les conclusions relatives à la résiliation du 24 mai 2017, retirée par une décision du 26 juillet 2017 devenue définitive, la cour administrative d'appel de Marseille a annulé le jugement du tribunal, y compris son article 1er, qui constatait un non-lieu à statuer sur les conclusions dirigées contre la résiliation du 24 mai 2017. La commune de Palavas-les-Flots est fondée à soutenir que la cour a entaché sur ce point son arrêt d'une contradiction entre ses motifs et son dispositif. <br/>
<br/>
              3. En second lieu, pour prononcer la reprise des relations contractuelles entre la commune de Palavas-les-Flots et la société Blue Boats, la cour administrative d'appel de Marseille s'est fondée sur ce que le motif invoqué par le maire, tiré de la volonté de la commune d'utiliser la dépendance du domaine public litigieuse pour le stationnement des véhicules du personnel d'une maison de retraite relevant du centre communal d'action sociale  implantée à proximité, ne pouvait justifier la résiliation de la convention du 8 juillet 2014, dès lors que la commune disposait déjà d'un parc de stationnement municipal à proximité, au sein duquel dix-sept places de stationnement avaient été prévues à l'usage exclusif de la maison de retraite, que les difficultés de stationnement rencontrées par le personnel de la maison de retraite n'étaient pas établies par les pièces du dossier et qu'en outre, aucun élément ne permettait d'établir une modification significative de la fréquentation touristique du quartier depuis 2014. En procédant ainsi à une appréciation des besoins de stationnement dans la commune et de la pertinence des choix des autorités municipales, alors que la volonté de la commune d'utiliser la dépendance litigieuse en vue de créer un espace de stationnement en centre-ville pour les besoins d'une maison de retraite caractérisait un motif d'intérêt général de nature à justifier la résiliation d'une convention par laquelle elle avait accordé une autorisation, précaire et révocable en vertu de l'article L. 2122-3 du code général de la propriété des personnes publiques, en vue de l'occupation privative de la dépendance à des fins d'activité commerciale, la cour administrative d'appel a entaché son arrêt d'une erreur de droit.<br/>
<br/>
              4. Il résulte de ce qui précède que la commune de Palavas-les-Flots est fondée à demander l'annulation des articles 1er à 3 de l'arrêt qu'elle attaque. <br/>
<br/>
              5. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la société Blue Boats une somme de 3 000 euros à verser à la commune de Palavas-les-Flots au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Les articles 1er à 3 de l'arrêt du 29 avril 2019 de la cour administrative d'appel de Marseille sont annulés. <br/>
Article 2 : L'affaire est renvoyée, dans cette mesure, à la cour administrative d'appel de Marseille.<br/>
Article 3 : La société Blue Boats versera à la commune de Palavas-les-Flots la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à la commune de Palavas-les-Flots et à la société Blue Boats.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">24-01-02-01 DOMAINE. DOMAINE PUBLIC. RÉGIME. OCCUPATION. - CONVENTION D'OCCUPATION DU DOMAINE PUBLIC - RÉSILIATION POUR MOTIF D'INTÉRÊT GÉNÉRAL [RJ1] - CONTRÔLE DU JUGE - 1) EXISTENCE D'UN MOTIF D'INTÉRÊT GÉNÉRAL DE NATURE À JUSTIFIER LA RÉSILIATION DE LA CONVENTION - EXISTENCE - 2) APPRÉCIATION DE LA PERTINENCE DU CHOIX DE L'AUTORITÉ DOMANIALE - ABSENCE [RJ2].
</SCT>
<ANA ID="9A"> 24-01-02-01 1) La volonté d'une commune d'utiliser une dépendance du domaine public communal en vue de créer un espace de stationnement en centre-ville pour les besoins d'une maison de retraite caractérise un motif d'intérêt général de nature à justifier la résiliation de la convention par laquelle elle avait accordé une autorisation, précaire et révocable en vertu de L. 2122-3 du code général de la propriété des personnes publiques (CG3P), en vue de l'occupation privative de cette dépendance à des fins d'activité commerciale.... ,,2) Commet une erreur de droit la cour qui procède à une appréciation des besoins de stationnement dans la commune et de la pertinence des choix des autorités municipales pour apprécier si ce motif justifiait la résiliation de la convention.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf., sur la faculté de résilier une convention d'occupation domaniale pour un tel motif, CE, 2 mai 1958, Distillerie de Magnac-Laval, n° 32401, p. 246 ; CE, Assemblée, 2 février 1987, Société T.V.6., n° 81131, p. 29.,,[RJ2] Rappr., s'agissant d'une résiliation justifiée par la volonté d'adopter un nouveau mode de gestion d'une activité jusqu'alors exercée dans le cadre d'une convention d'occupation domaniale, CE, 19 janvier 2011, n° 323924, Commune de Limoges, T. pp. 923-1012-1065.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
