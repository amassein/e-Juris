<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026910030</ID>
<ANCIEN_ID>JG_L_2012_12_000000350559</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/91/00/CETATEXT000026910030.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème et 1ère sous-sections réunies, 28/12/2012, 350559, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2012-12-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>350559</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème et 1ère sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BORE ET SALVE DE BRUNETON ; SCP TIFFREAU, CORLAY, MARLANGE</AVOCATS>
<RAPPORTEUR>M. Marc Pichon de Vendeuil</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Suzanne von Coester</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2012:350559.20121228</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu le pourvoi sommaire, le pourvoi rectificatif et le mémoire complémentaire, enregistrés les 4 juillet, 5 juillet et 30 septembre 2011 au secrétariat du contentieux du Conseil d'Etat, présentés pour Maître Arthur B, demeurant ..., en sa qualité de liquidateur judiciaire du cabinet de M. C, et pour M. Patrick C, demeurant ... ; MM. B et C demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 09VE01594 du 26 avril 2011 par lequel la cour administrative d'appel de Versailles a rejeté leur requête tendant, d'une part, à l'annulation du jugement n° 0606780 du 11 mars 2009 par lequel le tribunal administratif de Cergy-Pontoise a rejeté la demande de M. C tendant à la réparation du préjudice causé du fait des consultations juridiques gratuites organisées à la maison de justice et du droit de Cergy installée depuis 1997 à proximité de son cabinet d'avocat, d'autre part, à ce que lui soit versée la somme de 2 410 684 euros en réparation de son préjudice ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à leur appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat, du conseil départemental de l'accès au droit du Val-d'Oise et de l'ordre des avocats au barreau du Val-d'Oise une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales et le premier protocole additionnel à cette convention ;<br/>
<br/>
              Vu le traité instituant la Communauté européenne ;<br/>
<br/>
              Vu le code de commerce ;<br/>
<br/>
              Vu le code de l'organisation judiciaire ;<br/>
<br/>
              Vu la loi n° 71-1130 du 31 décembre 1971 ; <br/>
<br/>
              Vu la loi n° 91-647 du 10 juillet 1991 ;<br/>
<br/>
              Vu la loi n° 98-1163 du 18 décembre 1998 ;<br/>
<br/>
              Vu le décret n° 91-1197 du 27 novembre 1991 ;<br/>
<br/>
              Vu le décret n° 2001-1009 du 29 octobre 2001 ; <br/>
<br/>
              Vu le décret n° 2005-790 du 12 juillet 2005 ;<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Marc Pichon de Vendeuil, Maître des Requêtes en service extraordinaire,  <br/>
<br/>
              - les observations de la SCP Boré et Salve de Bruneton, avocat de MM. Arthur B et Patrick C, et de la SCP Tiffreau, Corlay, Marlange, avocat de l'ordre des avocats au barreau du Val-d'Oise,<br/>
<br/>
              - les conclusions de Mme Suzanne von Coester, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Boré et Salve de Bruneton, avocat de MM. Arthur B et Patrick C, et à la SCP Tiffreau, Corlay, Marlange, avocat de l'ordre des avocats au barreau du Val-d'Oise ;<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des énonciations de l'arrêt attaqué que M. C a exercé son activité d'avocat à Cergy-Pontoise de 1987 jusqu'à la mise en liquidation judiciaire de son cabinet intervenue en 2006 ; que l'intéressé et le liquidateur judiciaire de son cabinet, Maître B, ont notamment recherché devant le juge administratif la responsabilité de l'Etat sur le terrain de la faute et de la rupture d'égalité devant les charges publiques, en se prévalant de ce que les consultations juridiques dispensées au sein de la maison de justice et du droit qui a été transférée en 1997 à proximité du cabinet de l'intéressé, avaient entraîné à compter de cette date un détournement de clientèle et étaient par suite à l'origine de ses difficultés financières ; que, par un jugement du 11 mai 2009, le tribunal administratif de Cergy-Pontoise a rejeté les conclusions indemnitaires présentées sur ces fondements ; que, par l'arrêt attaqué du 26 avril 2011, la cour administrative d'appel de Versailles a confirmé ce jugement ;<br/>
<br/>
              2. Considérant qu'en vue d'assurer l'égalité d'accès à la justice et faciliter l'accès au droit, des maisons de justice et du droit ont été mises en place dans certaines communes au début des années 1990 ; que leur existence a été consacrée par la loi du 18 décembre 1998 relative à l'accès au droit et à la résolution amiable des conflits, dont l'article 21 a introduit dans le code de l'organisation judiciaire des dispositions, aujourd'hui reprises aux articles R. 131-1 et suivants de ce code, prévoyant que les maisons de justice et du droit, placées sous l'autorité des chefs des tribunaux de grande instance dans le ressort desquels elles sont situées, assurent une présence judiciaire de proximité et concourent à la prévention de la délinquance, à l'aide aux victimes et à l'accès au droit ; qu'aux termes de l'article 3 du décret du 29 octobre 2001 modifiant le code de l'organisation judiciaire et relatif aux maisons de justice et du droit, qui est applicable à la maison de justice et du droit de Cergy-Pontoise en cause dans le présent litige : " Les maisons de justice et du droit créées avant la date d'entrée en vigueur du présent décret peuvent, dans la limite de trois ans à compter de cette date, poursuivre les activités prévues dans leur convention constitutive jusqu'à l'expiration de la durée fixée par celle-ci (...) " ; que les articles 53 et 57 de la loi du 10 juillet 1991 relative à l'aide juridique, issus de la loi du 18 décembre 1998, prévoient que l'aide à l'accès au droit, qui comporte notamment la consultation en matière juridique, s'exerce dans les conditions déterminées par les conseils départementaux de l'accès au droit qui peuvent conclure à cet effet des conventions avec des membres des professions juridiques ou judiciaires réglementées ou leurs organismes professionnels, en conformité avec leurs règles de déontologie ; qu'au rang de ces règles déontologiques figure notamment, conformément à l'article 3 de la loi du 31 décembre 1971 portant réforme de certaines professions judiciaires et juridiques, l'obligation pour les avocats d'exercer leurs " fonctions avec dignité, conscience, indépendance, probité et humanité " ;<br/>
<br/>
              3. Considérant, en premier lieu, qu'en relevant que les consultations gratuites délivrées au sein de la maison de justice et du droit de Cergy-Pontoise dans les conditions rappelées ci-dessus ne pouvaient, eu égard à leur nombre restreint, à leur durée limitée et à la nature générale des informations qui y étaient délivrées, être assimilées aux prestations juridiques fournies par un avocat dans le cadre de son cabinet, la cour administrative d'appel de Versailles, qui n'a en tout état de cause pas entendu juger que ces consultations échappaient aux règles déontologiques qui s'imposent à l'ensemble de la profession, n'a pas entaché son arrêt d'erreur de droit ;<br/>
<br/>
              4. Considérant, en deuxième lieu, qu'il résulte de ce qui a été dit précédemment que l'organisation de consultations juridiques, le cas échéant gratuites, dans les maisons de justice et du droit, qui trouve depuis 1998 un fondement explicite dans la loi, est justifiée par les motifs d'intérêt général de mieux garantir l'égalité devant la justice et de faciliter l'accès au droit ; qu'en jugeant que l'organisation de ces consultations n'était, compte tenu de leurs caractéristiques, pas de nature à porter atteinte à l'activité professionnelle des avocats exerçant sur le territoire de la même commune et ne pouvait dès lors constituer une pratique anticoncurrentielle prohibée, la cour administrative d'appel de Versailles, qui a suffisamment motivé son arrêt sur ce point, n'a pas commis d'erreur de droit ; <br/>
<br/>
              5. Considérant, en troisième lieu, qu'en relevant que  M. C ne contestait pas les difficultés financières de son cabinet survenues avant même l'installation en 1997 de la maison de justice et du droit de Cergy-Pontoise à proximité de celui-ci et qu'il n'apportait la preuve ni de l'existence d'un détournement de sa clientèle au profit de ses confrères assurant les consultations juridiques litigieuses ni de ce que ses clients auraient eux-mêmes bénéficié à son insu de telles consultations, la cour a porté sur les pièces du dossier une appréciation souveraine qui, dès lors qu'elle est exempte de dénaturation, ne peut être discutée devant le juge de cassation ;<br/>
<br/>
              6. Considérant qu'il résulte de ce qui précède que c'est sans commettre d'erreur de droit ni dénaturer les pièces du dossier que la cour a, pour rejeter les conclusions indemnitaires présentées par M. C sur le fondement de la rupture d'égalité devant les charges publiques, jugé qu'aucun lien de causalité n'était établi entre la baisse d'activité de son cabinet et l'organisation de consultations juridiques gratuites ; que, dès lors, le moyen tiré de ce qu'en statuant ainsi la cour administrative d'appel de Versailles aurait méconnu les stipulations de l'article 1er du premier protocole additionnel à la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ne peut, en tout état de cause, qu'être écarté ;<br/>
<br/>
              7. Considérant qu'il résulte de tout ce qui précède que le pourvoi de                      M.  C et de M. B doit être rejeté ;<br/>
<br/>
              8. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'il soit fait droit aux conclusions présentées par M. C au titre des frais exposés par lui et non compris dans les dépens ; qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par l'Etat et par l'ordre des avocats au barreau du Val-d'Oise au titre des mêmes dispositions ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
 --------------<br/>
<br/>
Article 1er : Le pourvoi de MM. C et B est rejeté.<br/>
Article 2 : Les conclusions de l'ordre des avocats au barreau du Val-d'Oise et du garde des sceaux, ministre de la justice, présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La présente décision sera notifiée à M. Patrick C, à M. Arthur B, ès-qualité de liquidateur judiciaire de la société personnelle de M. C, à la garde des sceaux, ministre de la justice et à l'ordre des avocats au barreau du Val-d'Oise. <br/>
Copie en sera adressée pour information au conseil départemental de l'accès au droit du Val-d'Oise<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
