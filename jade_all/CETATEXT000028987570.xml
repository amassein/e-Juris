<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028987570</ID>
<ANCIEN_ID>JG_L_2014_05_000000367832</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/98/75/CETATEXT000028987570.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère sous-section jugeant seule, 26/05/2014, 367832, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-05-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>367832</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère sous-section jugeant seule</FORMATION>
<TYPE_REC/>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DE NERVO, POUPET ; SCP FABIANI, LUC-THALER ; SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR>M. Pascal Trouilly</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Maud Vialettes</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:367832.20140526</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>VU LA PROCEDURE SUIVANTE :<br/>
<br/>
 Procédure contentieuse antérieure<br/>
<br/>
              M. D...B...a demandé au tribunal administratif de Melun d'annuler pour excès de pouvoir la décision du 10 juillet 2008 et les arrêtés des 23 mars et 28 avril 2009 par lesquels le maire de Charenton-le-Pont (Val-de-Marne) n'a pas fait opposition à la déclaration préalable déposée par la SARL Provini et fils pour aménager un lotissement au 22, rue Thiébault, a accordé à M. C... A...un permis de démolir un entrepôt et des box à la même adresse et a délivré à ce dernier un permis de construire pour l'édification d'un bâtiment de 15 logements et de 31 places de stationnement au 20/22 rue Thiébault. <br/>
<br/>
              Par une ordonnance n° 1103778 du 3 octobre 2011, le président de la 4ème chambre du tribunal administratif de Melun a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 11PA04408 du 22 novembre 2012, la cour administrative d'appel de Paris a rejeté l'appel formé par M. B...contre cette ordonnance.<br/>
<br/>
 Procédure devant le Conseil d'Etat<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 18 avril 2013, 17 juillet 2013 et 4 avril 2014 au secrétariat du contentieux du Conseil d'Etat, M. B...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt n° 11PA04408 de la cour administrative d'appel de Paris du 22 novembre 2012 ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel.<br/>
<br/>
              Il soutient que :<br/>
              - la cour a commis une erreur de droit en jugeant qu'il n'établissait pas avoir satisfait à l'obligation de notification prévue à l'article R. 600-1 du code de l'urbanisme ;<br/>
              - elle a dénaturé les pièces du dossier en relevant qu'il avait seulement produit devant le tribunal les originaux des preuves de dépôt de deux lettres recommandées ;<br/>
              - elle a mentionné des lettres du 21 septembre 2007 à la suite d'une erreur matérielle ou, à défaut, en conséquence d'une dénaturation des pièces du dossier.<br/>
<br/>
              Par un mémoire en défense, enregistré le 5 décembre 2013, la SARL Provini et fils et M. A...concluent au rejet du pourvoi.<br/>
<br/>
              Ils soutiennent que les moyens du pourvoi ne sont pas fondés et que le moyen d'erreur de droit critique un motif surabondant de l'arrêt.<br/>
<br/>
              Par un mémoire en défense, enregistré le 16 janvier 2014, la commune de Charenton-le-Pont conclut au rejet du pourvoi et à ce que la somme de 4 500 euros soit mise à la charge de M. B...au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Elle soutient que les moyens du pourvoi ne sont pas fondés.<br/>
<br/>
<br/>
              Vu :<br/>
              - les autres pièces du dossier ;<br/>
              - le code de l'urbanisme ;<br/>
              - le code de justice administrative.<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Pascal Trouilly, Maître des Requêtes,  <br/>
<br/>
              - les conclusions de Mme Maud Vialettes, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP de Nervo, Poupet, avocat de M.B..., à la SCP Fabiani, Luc-Thaler, avocat de la commune de Charenton-le-Pont, et à la SCP Piwnica, Molinié, avocat de M. A...et de la SARL Provini et fils.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              CONSIDERANT CE QUI SUIT :<br/>
<br/>
              1. Aux termes de l'article R. 600-1 du code de l'urbanisme : " En cas (...) de recours contentieux à l'encontre (...) d'une décision de non-opposition à une déclaration préalable ou d'un permis de construire, d'aménager ou de démolir, (...) l'auteur du recours est tenu, à peine d'irrecevabilité, de notifier son recours à l'auteur de la décision et au titulaire de l'autorisation (...) / La notification prévue au précédent alinéa doit intervenir par lettre recommandée avec accusé de réception, dans un délai de quinze jours francs à compter du dépôt du (...) recours. / La notification du recours à l'auteur de la décision et, s'il y a lieu, au titulaire de l'autorisation est réputée accomplie à la date d'envoi de la lettre recommandée avec accusé de réception. Cette date est établie par le certificat de dépôt de la lettre recommandée auprès des services postaux ". <br/>
<br/>
              2. Pour rejeter l'appel de M.B..., la cour administrative d'appel de Paris a jugé qu'il n'avait pas établi en première instance avoir satisfait à l'obligation de notification instituée par ces dispositions, en relevant qu'il s'était borné à adresser au tribunal les originaux des preuves de dépôt de deux lettres recommandées avec accusé de réception, sans transmettre la copie du recours contentieux qu'il aurait adressé à la commune et aux bénéficiaires des décisions d'urbanisme contestées.<br/>
<br/>
              3. Il résulte des dispositions de l'article R. 600-1 du code de l'urbanisme que l'auteur d'un recours contentieux est tenu de notifier une copie du recours tant à l'auteur de l'acte ou de la décision qu'il attaque qu'à son bénéficiaire. Il appartient au juge, au besoin d'office, de rejeter le recours comme irrecevable lorsque son auteur, après y avoir été invité par lui, n'a pas justifié de l'accomplissement des formalités requises par les dispositions précitées. A cet égard, la production du certificat de dépôt de la lettre recommandée suffit à justifier de l'accomplissement de ces formalités lorsqu'il n'est pas soutenu devant le juge qu'elle aurait eu un contenu insuffisant au regard de l'obligation d'information qui pèse sur l'auteur du recours. Il suit de là qu'en jugeant, par un motif qui ne présente pas un caractère surabondant, que les dispositions précitées de l'article R. 600-1 " imposent au juge non seulement de vérifier qu'un document a bien été transmis à l'auteur et au bénéficiaire de la décision, mais d'analyser en outre concrètement ce que ces derniers ont reçu " et en considérant, par suite, que M. B...n'établissait pas avoir satisfait à l'obligation de notification au motif qu'il n'avait pas transmis au tribunal administratif la copie de son recours, alors que le contenu des courriers expédiés n'était pas contesté, la cour administrative d'appel a entaché son arrêt d'erreur de droit. <br/>
<br/>
              4. Il résulte de ce qui précède que M. B...est fondé à demander l'annulation de l'arrêt qu'il attaque, sans qu'il soit besoin d'examiner les autres moyens de son pourvoi.<br/>
<br/>
              5. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que la somme demandée à ce titre par la commune de Charenton-le-Pont soit mise à la charge de M. B...qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Paris du 22 novembre 2012 est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Paris.<br/>
Article 3 : Les conclusions de la commune de Charenton-le-Pont présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée à M. D...B..., à la commune de Charenton-le-Pont, à M. C...A...et à la SARL Provini et fils.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
