<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044487215</ID>
<ANCIEN_ID>JG_L_2021_11_000000451030</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/48/72/CETATEXT000044487215.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème chambre, 22/11/2021, 451030, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-11-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>451030</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Géraud Sajust de Bergues</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Marie-Gabrielle Merloz</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:451030.20211122</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              M. R... T... a demandé au tribunal administratif de Marseille, à titre principal, d'annuler les opérations électorales qui se sont déroulées le 28 juin 2020 en vue de la désignation des conseillers municipaux et communautaires de la commune de Saint-Rémy-de-Provence (Bouches-du-Rhône), après avoir déclaré nuls les bulletins de vote émis en faveur de la liste conduite par M. F... AN... à l'occasion du premier tour du scrutin et, à titre subsidiaire, de rectifier les résultats du second tour du scrutin en ajoutant 8 voix à la liste " Le renouveau Saint-Rémois avec Romain T... " et de proclamer élues les 22 personnes figurant sur cette liste. Par un jugement nos 2004934, 2004979 du 22 février 2021, le tribunal administratif de Marseille a annulé les opérations électorales qui se sont déroulées les 15 mars et 28 juin 2020 dans cette commune.<br/>
<br/>
              Par une requête, enregistrée le 24 mars 2021 au secrétariat du contentieux du Conseil d'Etat, M. F... AN..., Mme W... AR..., M. D... AF..., Mme P... Z..., M. X... AE..., Mme AG... L..., M. AC... K..., Mme AO... G..., M. E... I..., Mme A... B..., M. U... A..., Mme O... Y..., M. D... J..., Mme S... AS..., M. AA... A..., Mme AM... AI..., M. AD... Q..., Mme AH... N..., M. AP... V..., Mme AJ... AQ..., M. AL... M... et Mme AK... AB... demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ; <br/>
<br/>
              2°) de rejeter la protestation de M. T....<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code électoral ;<br/>
              - la loi n° 2020-290 du 23 mars 2020 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Géraud Sajust de Bergues, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Marie-Gabrielle Merloz, rapporteure publique ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. A l'issue des opérations électorales qui se sont déroulées les 15 mars et 28 juin 2020 à Saint-Rémy-de-Provence, commune de plus de 1 000 habitants, en vue de l'élection des conseillers municipaux et des conseillers communautaires, la liste " Défendons notre histoire construisons notre avenir avec Hervé AN... " conduite par M. AN... a obtenu 2 332 voix, soit 50,05 % des suffrages exprimés, tandis que la liste concurrente " Le renouveau Saint-Rémois avec Romain T... " menée par M. T... a obtenu 2 327 voix, soit 49,94 % des suffrages exprimés. M. AN... relève appel du jugement du 22 février 2021 par lequel le tribunal administratif de Marseille a annulé les opérations électorales qui se sont déroulées les 15 mars et 28 juin 2020 dans cette commune. <br/>
<br/>
              2. D'une part, des irrégularités ayant affecté les opérations électorales d'un premier tour de scrutin, à l'issue duquel aucun candidat n'a été proclamé élu, peuvent être invoquées à l'appui de conclusions dirigées contre les résultats du second tour, dès lors que ceux-ci ont pu être affectés par ces irrégularités. <br/>
<br/>
              3. D'autre part, aux termes de l'article LO 247-1 du code électoral applicable aux communes de 1 000 habitants et plus : " (...) les bulletins de vote imprimés distribués aux électeurs comportent, à peine de nullité, en regard du nom des candidats ressortissants d'un Etat membre de l'Union européenne autre que la France, l'indication de leur nationalité. / (...) ". L'article R. 66-2 du même code dispose que : " Sont nuls et n'entrent pas en compte dans le résultat du dépouillement : / 1° Les bulletins ne répondant pas aux prescriptions légales ou réglementaires édictées pour chaque catégorie d'élections (...) ". Il résulte des termes mêmes de cet article que l'omission sur les bulletins de vote de l'indication de la nationalité des candidats ressortissants d'un Etat membre de l'Union européenne autre que la France entache, à elle seule, ces bulletins de nullité.<br/>
<br/>
              4. Il résulte de l'instruction que les bulletins de vote de la liste " " Défendons notre histoire construisons notre avenir avec Hervé AN... " conduite par M. AN... ne mentionnaient pas, lors du premier tour des élections, la nationalité belge de la candidate inscrite en 28ème position sur cette liste. En dépit de la nullité dont ces bulletins étaient entachés, les 1 979 voix qui se sont portées sur la liste menée par M. AN... ont été prises en compte lors du dépouillement et ont conduit à ce que cette liste participe au second tour et obtienne 22 sièges au conseil municipal. L'irrégularité résultant de la prise en compte de ces bulletins, qui auraient dû être tenus pour nuls, a été ainsi de nature à altérer la sincérité de l'ensemble du scrutin.<br/>
<br/>
              5. Il résulte de tout ce qui précède, sans qu'il soit besoin de se prononcer sur la fin de non-recevoir opposée par M. T..., que M. AN... et autres ne sont pas fondés à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Marseille a annulé les opérations électorales qui se sont déroulées dans la commune de Saint-Rémy-de-Provence les 15 mars et 28 juin 2020. <br/>
<br/>
              6. Il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par M. T... au titre des dispositions de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de M. AN... et autres est rejetée. <br/>
Article 2 : Les conclusions présentées par M. T... au titre de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
Article 3 : La présente décision sera notifiée à M. F... AN..., premier dénommé, pour l'ensemble des requérants, et à M. R... T.... <br/>
Copie en sera adressée au ministre de l'intérieur. <br/>
              Délibéré à l'issue de la séance du 21 octobre 2021 où siégeaient : M. Stéphane Verclytte, conseiller d'Etat, présidant ; M. Christian Fournier, conseiller d'Etat et M. Géraud Sajust de Bergues, conseiller d'Etat-rapporteur. <br/>
<br/>
              Rendu le 22 novembre 2021.<br/>
                 Le Président : <br/>
                 Signé : M. Stéphane Verclytte<br/>
 		Le rapporteur : <br/>
      Signé : M. Géraud Sajust de Bergues<br/>
                 La secrétaire :<br/>
                 Signé : Mme AK... AT...<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
