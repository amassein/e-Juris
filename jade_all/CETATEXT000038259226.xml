<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038259226</ID>
<ANCIEN_ID>JG_L_2019_03_000000426439</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/25/92/CETATEXT000038259226.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème - 9ème chambres réunies, 13/03/2019, 426439, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-03-13</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>426439</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème - 9ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP POTIER DE LA VARDE, BUK LAMENT, ROBILLOT</AVOCATS>
<RAPPORTEUR>M. Richard Senghor</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Aurélie Bretonneau</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:426439.20190313</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu les procédures suivantes :<br/>
<br/>
              1° Par une requête, enregistrée sous le n° 426439 le 19 décembre 2018 au secrétariat du contentieux du Conseil d'Etat, M. B...demande au Conseil d'Etat :<br/>
<br/>
              1°) de déclarer illégale la " loi du pays " n° 2018-34 LP/APF du 15 novembre 2018 modifiant les conditions de création des officines de pharmacie et certaines dispositions relatives à l'exercice de la pharmacie ;<br/>
<br/>
              2°) de mettre à la charge de la Polynésie française la somme de 200 000 francs CFP au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              2° Par une requête, enregistrée sous le n° 426562 le 24 décembre 2018 et un mémoire en réplique enregistré le 12 février 2019 au secrétariat du contentieux du Conseil d'Etat, le conseil de l'ordre des pharmaciens de la Polynésie française, le syndicat des pharmaciens de la Polynésie française et le syndicat des pharmaciens des îles et de Tahiti demandent au Conseil d'Etat :<br/>
<br/>
              1°) de déclarer illégale la " loi du pays " n° 2018-34 LP/APF du 15 novembre 2018 modifiant les conditions de création des officines de pharmacie et certaines dispositions relatives à l'exercice de la pharmacie ;<br/>
<br/>
              2°) de mettre à la charge de la Polynésie française la somme de 1 500 euros  à verser à chacun d'eux au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu : <br/>
              - la Constitution, notamment son Préambule et son article 74 ;<br/>
              - la loi organique n° 2004-192 du 27 février 2004 ;<br/>
              - la délibération n° 88-153 AT du 20 octobre 1988 modifiée relative à certaines dispositions concernant l'exercice de la pharmacie ;<br/>
              - la délibération n° 2005-59 APF du 13 mai 2005 modifiée portant règlement intérieur de l'assemblée de la Polynésie française ;<br/>
              - la délibération n° 2005-64 APF du 13 juin 2005 modifiée portant composition, organisation et fonctionnement du conseil économique, social et culturel de la Polynésie française ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Richard Senghor, Conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Aurélie Bretonneau, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Potier de la Varde, Buk Lament, Robillot, avocat du conseil de l'ordre des pharmaciens de la Polynésie Française, du syndicat des pharmaciens de la Polynésie française et du syndicat des pharmaciens des îles et de Tahiti ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Les requêtes n°s 426439 et 426562 présentent à juger des questions semblables. Il y a lieu de les joindre pour statuer par une seule décision.<br/>
<br/>
              2. Aux termes du II de l'article 176 de la loi organique du 27 février 2004 portant statut d'autonomie de la Polynésie française : " A l'expiration de la période de huit jours suivant l'adoption d'un acte prévu à l'article 140 dénommé "loi du pays" ou au lendemain du vote intervenu à l'issue de la nouvelle lecture prévue à l'article 143, l'acte prévu à l'article 140 dénommé "loi du pays" est publié au Journal officiel de la Polynésie française à titre d'information pour permettre aux personnes physiques ou morales, dans le délai d'un mois à compter de cette publication, de déférer cet acte au Conseil d'Etat. / Le recours des personnes physiques ou morales est recevable si elles justifient d'un intérêt à agir ".<br/>
<br/>
              3. L'assemblée de la Polynésie française a adopté le 15 novembre 2018, sur le fondement de l'article 140 de la loi organique du 27 février 2004 portant statut d'autonomie de la Polynésie française, la " loi du pays " n° 2018-34 LP/APF modifiant les conditions de création des officines de pharmacie et certaines dispositions relatives à l'exercice de la pharmacie, qui a été publiée pour information au Journal officiel de la Polynésie française le 23 novembre 2018. Sous le n° 426439, M.B..., et, sous le n° 426562, le conseil de l'ordre des pharmaciens de la Polynésie française, le syndicat des pharmaciens de la Polynésie française et le syndicat des pharmaciens des îles et de Tahiti ont saisi le Conseil d'Etat d'une requête tendant à ce que cette " loi du pays " soit déclarée non conforme au bloc de légalité défini au III de l'article 176 de la loi organique du 27 février 2004.<br/>
<br/>
              Sur les fins de non-recevoir opposées par le président de la Polynésie française et l'assemblée de la Polynésie française :<br/>
<br/>
              4. Aux termes du II de l'article 151 de la loi organique du 27 février 2004 : " le conseil économique, social et culturel est consulté sur les projets et propositions d'actes prévus à l'article 140 dénommés " lois du pays " à caractère économique ou social. A cet effet, il est saisi, pour les projets, par le président de la Polynésie française (...). Il dispose dans ces cas pour donner son avis d'un délai d'un mois, ramené à quinze jours en cas d'urgence déclarée selon le cas par le gouvernement ou par l'assemblée. A l'expiration de ce délai, l'avis est réputé rendu (...) ".<br/>
<br/>
              5. La " loi du pays " n° 2018-34 LP/APF modifiant les conditions de création des officines de pharmacie et certaines dispositions relatives à l'exercice de la pharmacie, qui tend à accroître la concurrence en permettant  l'ouverture de nouvelles pharmacies sur le territoire de la Polynésie française, revêt un caractère économique au sens des dispositions du II de l'article 151 de la loi organique du 27 février 2004. Ce projet était, dès lors, soumis à la consultation obligatoire du conseil économique, social et culturel. Il en résulte que M. B..., en sa qualité de membre de ce conseil, justifie d'un intérêt pour agir contre la " loi du pays " qu'il attaque. Les fins de non-recevoir opposées à ce titre par le président de la Polynésie française et l'assemblée de la Polynésie française doivent dès lors être écartées. <br/>
<br/>
              Sur la " loi du pays " du 15 novembre 2018 modifiant les conditions de création des officines de pharmacie et certaines dispositions relatives à l'exercice de la pharmacie : <br/>
<br/>
              6. Aux termes de l'article 27 de la délibération du 13 mai 2005 portant règlement intérieur de l'assemblée de la Polynésie française : " Les projets de loi du pays présentés par le gouvernement ainsi que les propositions de loi du pays déposées par les représentants, accompagnés de leur exposé des motifs, sont enregistrés au secrétariat général de l'assemblée puis transmis par le président de l'assemblée à la commission compétente. / (...) Le rapporteur de la loi du pays dépose, pour enregistrement au secrétariat général de l'assemblée, son rapport qui tient compte des observations de la commission compétente ayant examiné le projet ou la proposition de loi du pays. Ce rapport est imprimé puis transmis à la conférence des présidents pour inscription à l'ordre du jour d'une séance. Il est diffusé aux représentants douze jours au moins avant la séance (...) ". Il résulte de ces dispositions ainsi que des dispositions du II de l'article 151 de la loi organique du 27 février 2004 que si les projets d'actes prévus à l'article 140 de la loi organique du 27 février 2004 à caractère économique ou social doivent, en principe, être déposés au secrétariat général de l'Assemblée accompagnés de l'avis du conseil économique, social et culturel, cet avis peut encore intervenir jusqu'à leur adoption par l'Assemblée, sans que cette circonstance soit, à elle seule, de nature à entacher d'irrégularité la procédure, dès lors que cet avis a pour objet d'éclairer les représentants à l'assemblée de la Polynésie française. <br/>
<br/>
              7. Il ressort des pièces du dossier que la " loi du pays " du 15 novembre 2018 attaquée n'a pas été soumise à la consultation du conseil économique, social et culturel alors que cette consultation, qui constitue une garantie, était obligatoire en vertu du II de l'article 151 de la loi organique du 27 février 2004. Comme le conseil économique, social et culturel dispose, en vertu des dispositions du II de l'article 151 de la loi organique du 27 février 2004, citées au point 4, d'un délai d'un mois pour rendre son avis, la circonstance que la nouvelle mandature de ce conseil ait débuté le 3 septembre 2018 n'était pas de nature à rendre la consultation impossible, dès lors que la " loi du pays " attaquée a été adoptée par l'assemblée de la Polynésie française le 15 novembre 2018. Dans ces conditions, M. B...et autres sont fondés à soutenir que la " loi du pays " attaquée a été adoptée selon une procédure irrégulière.<br/>
<br/>
              8. Il résulte de ce qui précède que M.B..., le conseil de l'ordre des pharmaciens de la Polynésie française, le syndicat des pharmaciens de la Polynésie française et le syndicat des pharmaciens des îles et de Tahiti sont fondés à demander que soient déclarées illégales les dispositions de cette " loi du pays ", sans qu'il soit besoin d'examiner les autres moyens de leur requête. <br/>
<br/>
              9. Dans les circonstances de l'espèce, il y a lieu de mettre à la charge de la Polynésie française la somme de 1 500 euros à verser respectivement, d'une part, au conseil de l'ordre des pharmaciens de Polynésie française, au syndicat des pharmaciens de la Polynésie française et au syndicat des pharmaciens des îles et de Tahiti et, d'autre part, à M. B...au titre de l'article L. 761-1 du code de justice administrative. Ces dispositions font, en revanche, obstacle à ce qu'une somme soit mise à la charge de M.B..., qui n'est pas la partie perdante dans la présente instance.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La " loi du pays " n° 2018-34 LP/APF du 15 novembre 2018 modifiant les conditions de création des officines de pharmacie et certaines dispositions relatives à l'exercice de la pharmacie est déclarée illégale et ne peut être promulguée.<br/>
Article 2 : La Polynésie française versera, d'une part, à M. B...et, d'autre part, au conseil de l'ordre des pharmaciens de la Polynésie française, au syndicat des pharmaciens de la Polynésie française et au syndicat des pharmaciens des îles et de Tahiti, une somme de 1 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : Les conclusions présentées par la Polynésie française au titre de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
Article 4 : La présente décision sera notifiée à M. A...B..., au conseil de l'ordre des pharmaciens de la Polynésie française, au syndicat des pharmaciens de la Polynésie française, au syndicat des pharmaciens des îles et de Tahiti, au président de la Polynésie française, au président de l'assemblée de la Polynésie française, au haut-commissaire de la République en Polynésie française et à la ministre des outre-mer.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
