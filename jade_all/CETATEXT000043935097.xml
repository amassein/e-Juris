<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043935097</ID>
<ANCIEN_ID>JG_L_2021_07_000000454793</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/93/50/CETATEXT000043935097.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 27/07/2021, 454793, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-07-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>454793</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2021:454793.20210727</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, enregistrée le 20 juillet 2020 au secrétariat du contentieux du Conseil d'Etat, Mme B... A... demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative :<br/>
<br/>
              1°) de suspendre l'exécution du décret n° 2021-724 du 7 juin 2021 modifiant le décret n° 2021-699 du 1er juin 2021 prescrivant les mesures générales nécessaires à la gestion de la sortie de crise sanitaire ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 1 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Elle soutient que :<br/>
              - il existe un doute sérieux quant à la légalité de la décision contestée ;<br/>
              - le décret n° 2021-724 du 7 juin 2021 porte atteinte à la liberté de circulation dès lors qu'il exclut du " passe sanitaire " les personnes justifiant de la présence d'anticorps, notamment par la réalisation d'un test sérologique, contrairement à ce que prévoit le règlement (UE) 2021/953 du 14 juin 2021 ;<br/>
              - ce décret porte atteinte de manière grave, disproportionnée et injustifiée aux libertés individuelles dès lors que l'accès aux lieux de culture et de loisirs et la réalisation d'activités quotidiennes est conditionné à la présentation d'un " passe sanitaire " à partir de la fin du mois de juillet 2021 et du début du mois d'août 2021, alors qu'il ressort des avis du conseil scientifique que l'immunité résiduelle acquise lors d'une infection par le virus reste protectrice. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le règlement (UE) 2021/953 du 14 juin 2021 ;<br/>
              - le décret n°2021-699 du 1er juin 2021 ; <br/>
              - le décret n°2021-724 du 7 juin 2021 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision ". En vertu de l'article L. 522-3 du même code, le juge des référés peut, par une ordonnance motivée, rejeter une requête sans instruction ni audience lorsque la condition d'urgence n'est pas remplie ou lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable ou qu'elle est mal fondée.<br/>
<br/>
              2. L'urgence justifie que soit prononcée la suspension d'un acte administratif lorsque l'exécution de celui-ci porte atteinte, de manière suffisamment grave et immédiate, à un intérêt public, à la situation du requérant ou aux intérêts qu'il entend défendre Il appartient au juge des référés d'apprécier concrètement, compte tenu des justifications apportées par le requérant, si les effets de l'acte litigieux sont de nature à caractériser une urgence justifiant que, sans attendre le jugement au fond, l'exécution de la décision soit suspendue. <br/>
<br/>
              3. La requérante fait valoir que le décret, en réservant l'obtention d'un certificat de rétablissement aux seules personnes contaminées par le virus de la Covid 19 depuis moins de 6 mois, met les personnes qui ont été contaminées depuis plus de 6 mois et qui, ayant encore des anticorps, ne peuvent justifier d'un test RT-PCR ou antigénique négatif, dans l'impossibilité d'obtenir un passe sanitaire. Toutefois, l'obtention d'un passe sanitaire est également possible pour les personnes justifiant d'une attestation de vaccination complète. La requérante, qui n'établit pas avoir été dans l'impossibilité de se faire vacciner et qui se borne à soutenir, par des allégations très générales, que le décret la met dans l'impossibilité d'obtenir le certificat Covid numérique de l'Union européenne, ce qui entrave sa liberté de circulation au sein de l'Union européenne et porte une atteinte grave disproportionnée et non justifiée à ses libertés individuelles, ne justifie pas, ce faisant, que le décret contesté préjudicierait de manière suffisamment grave et immédiate à sa situation. <br/>
<br/>
              4. Il résulte de ce qui précède qu'il est manifeste que la requête ne peut être accueillie. Elle doit, par suite, être rejetée, selon la procédure prévue par l'article L. 522-3 du code de justice administrative.<br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
              ------------------<br/>
<br/>
Article 1er : La requête de Mme A... est rejetée. <br/>
Article 2 : La présente ordonnance sera notifiée à Mme B... A.... <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
