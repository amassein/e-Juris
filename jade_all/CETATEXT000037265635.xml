<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037265635</ID>
<ANCIEN_ID>JG_L_2018_07_000000421816</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/26/56/CETATEXT000037265635.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 25/07/2018, 421816, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-07-25</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>421816</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>OCCHIPINTI</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2018:421816.20180725</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, enregistrée le 27 juin 2018 au secrétariat du contentieux du Conseil d'Etat, Messieurs et Mesdames Pierre A...A L'Huissier, Jean-Christophe Lagarde, Philippe Vigier, Thierry Benoit, Yannick Favennec Becot, Guy Bricout, Sophie Auconie, Isabelle Valentin, Eric Straumann, Olivier Becht, Sébastien Leclerc, Jean-Yves Bony, Bertrand Pancher, Emmanuelle Anthoine, Antoine Herth, Nicolas Forissier, Philippe Gosselin, Jean-Marie Sermier, Didier Quentin, Franck Marlin, Jean-Charles Taugourdeau, Vincent Descoeur, Francis Vercamer, Emmanuel Maquet, Stéphane Viry, Olivier Dassault, Fabrice Brun, Patrice Verchere, Raphaël Schellenberger, Marianne Dubois, Geneviève Levy, Jean-Luc Reitzer, Jean-Jacques Gaultier, Jean-François Parigi, Arnaud Viala, Frédérique Meunier, Fabien Di Filippo, Josiane Corneloup, Gérard Menuel, Ian Boucard, Philippe Gomes, Philippe Dunoyer, Maina Sage, Jacques Cattin, Christophe Naegelen, Jean-Pierre Vigier, Jean-Pierre Door, Meyer Habib, André Villiers, Jean Lassalle, Guillaume Peltier, Jean-Paul Dufrègne, Loïc Prud'homme, Valérie Lacroute, Sébastien Jumel, Valérie Bazin-Malgras demandent au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative, la suspension de l'exécution du décret n° 2018-487 du 15 juin 2018 relatif aux vitesses maximales autorisées des véhicules.<br/>
<br/>
<br/>
<br/>
              Ils soutiennent que :<br/>
              - leur requête est recevable ;<br/>
              - la condition d'urgence est remplie, dès lors que l'entrée en vigueur de cette nouvelle réglementation est imminente ;<br/>
              - il existe un doute sérieux quant à la légalité du décret contesté ;<br/>
              - le décret contesté est entaché d'incompétence, les routes concernées relevant de la compétence des départements ; <br/>
              - il est entaché d'un vice de forme, faute d'être revêtu des contreseings du ministre de la cohésion des territoires et du ministre d'Etat, ministre de la transition écologique et solidaire ;<br/>
              - il est entaché de vices de procédure, en ce que, d'une part, les ministres chargés de l'aménagement du territoire, des transports et du budget ainsi que les autorités administratives détentrices du pouvoir de police de circulation sur les routes départementales n'ont pas été consultés préalablement à son édiction, d'autre part, d'autres sections que la section des travaux publics du Conseil d'Etat aurait dû être consultées pour avis ;  <br/>
              - qu'il a été irrégulièrement édicté, la consultation publique préalable organisée en application de l'article L. 123-19-1 du code de l'environnement ayant été centrée sur des questions de sécurité routière et non sur des questions environnementales ;<br/>
              - il ne contient aucune motivation de nature à justifier des circonstances de droit et de fait ayant présidé à son édiction ; <br/>
              - le Premier ministre, en édictant le décret attaqué, s'est cru, à tort, lié par la position arrêtée le 9 janvier 2018 par le Comité interministériel de la sécurité routière ; <br/>
              - le décret est entaché d'erreur de fait, d'erreur de droit, d'erreur d'appréciation et porte une atteinte disproportionnée à la liberté de circulation, dès lors qu'il ne repose, ni sur une expérimentation conclusive, ni sur des études incontestables concluant à l'abaissement à 80 km/h de la vitesse maximale autorisée sur les routes bidirectionnelles à chaussée non séparée et qu'il ne distingue pas selon les voies et les départements en cause ; <br/>
              - il a été pris en violation du principe de sécurité juridique, faute de comporter des dispositions transitoires ; <br/>
              - il est entaché de détournement de procédure, faute de mentionner, comme cela avait été annoncé, qu'il fera l'objet, au bout de deux ans, d'une évaluation ;<br/>
              -il est entaché d'un détournement de pouvoir, le décret attaqué procédant de considérations purement arbitraires ; <br/>
              - il a été pris en méconnaissance des principes généraux d'égalité, de non-discrimination et de proportionnalité.<br/>
<br/>
<br/>
              Par un mémoire en défense, enregistré le 17 juillet 2018, le ministre d'Etat, ministre de l'intérieur, conclut au rejet de la requête. Il soutient, à titre principal, que la requête au fond présentée par M. A...B...et autres est irrecevable, les requérants ne justifiant pas d'un intérêt leur donnant qualité pour agir et, à titre subsidiaire, que la condition d'urgence n'est pas remplie et que les moyens soulevés par M. A...B...et autres ne sont pas propres à créer un doute sérieux quant à la légalité du décret contesté.<br/>
<br/>
              La requête a été communiquée au Premier ministre qui n'a pas produit de mémoire en défense.<br/>
<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, M. A... B... et autres, d'autre part, le Premier ministre et le ministre d'Etat, ministre de l'intérieur.<br/>
<br/>
              Vu le procès-verbal de l'audience publique du jeudi 19 juillet 2018 à 11 heures au cours de laquelle ont été entendus :<br/>
              - Me Occhipinti, avocat au Conseil d'Etat et à la Cour de cassation, avocat des requérants ;<br/>
<br/>
              - M. A...B... ;<br/>
<br/>
              - le représentant des requérants ;<br/>
<br/>
- les représentants du ministre d'Etat, ministre de l'intérieur ;<br/>
<br/>
              et à l'issue de laquelle le juge des référés a différé la clôture de l'instruction au vendredi 20 juillet 2018 à 18 heures, puis, après en avoir avisé les parties, au lundi 23 juillet à 15 heures ;<br/>
              Vu le mémoire, enregistré le 20 juillet 2018, par lequel le ministre d'Etat, ministre de l'intérieur persiste dans ses précédentes écritures ; <br/>
<br/>
              Vu le mémoire, enregistré le 20 juillet 2018, par lequel M. A...B...et autres persistent dans leurs précédentes écritures ;<br/>
<br/>
              Vu le nouveau mémoire, enregistré le 23 juillet 2018, présenté par M. A...B...et autres, par lequel ils persistent dans leurs précédentes écritures ; ils soutiennent, en outre, que la procédure suivie leur paraît contraire au droit à un procès équitable que garantit les stipulations de l'article 6 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, dès lors que le décret du 15 juin 2018 a été pris au vu d'un avis de la section des travaux publics du Conseil d'Etat, dont seul l'Etat a été destinataire et qui ne leur a pas été communiqué, de sorte qu'il en résulte une violation de l'égalité des armes entre les parties à la présente procédure ;<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la Constitution ;<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ; <br/>
              - le code de l'environnement ;<br/>
              - le code général des collectivités territoriales ; <br/>
              - le code de la route ;<br/>
      - le code de justice administrative ;		<br/>
<br/>
<br/>
<br/>1. Considérant que M. A...B...et cinquante-cinq autres requérants, députés, ont formé un recours pour excès de pouvoir contre le décret du 15 juin 2018 relatif aux vitesses maximales autorisées des véhicules lequel, à compter du 1er juillet 2018, réduit la vitesse maximale autorisée de 90 à 80 km/h sur les routes bidirectionnelles à chaussée unique sans séparateur central, à l'exception des sections de ces routes comportant au moins deux voies affectées à un même sens de circulation pour lesquelles la vitesse maximale autorisée reste fixée à 90 km/h ; que les mêmes requérants demandent, par la présente requête, la suspension de l'exécution de ce décret sur le fondement de l'article L. 521-1 du code de justice administrative ; <br/>
<br/>
              2. Considérant qu'aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision " ; <br/>
<br/>
              3. Considérant que l'urgence justifie que soit prononcée la suspension d'un acte administratif lorsque l'exécution de celui-ci porte atteinte, de manière suffisamment grave et immédiate, à un intérêt public, à la situation du requérant ou aux intérêts qu'il entend défendre ; qu'à ce titre, il appartient au juge des référés d'apprécier concrètement, compte tenu des justifications fournies par le requérant, si les effets de l'acte litigieux sont de nature à caractériser une urgence justifiant que, sans attendre le jugement de la requête au fond, l'exécution de la décision soit suspendue ; que l'urgence doit être appréciée objectivement et compte tenu de l'ensemble des circonstances de l'affaire, à la date à laquelle le juge des référés se prononce ;<br/>
<br/>
              4. Considérant que, dans leur requête, M. A...B...et autres se bornent à faire valoir, pour établir que la condition d'urgence est, en l'espèce, remplie, que l'entrée en vigueur du décret, à compter du 1er juillet 2018, crée, en elle-même, une situation d'urgence ; qu'en outre, ils ont indiqué, lors de l'audience publique, que le décret s'appliquant à l'ensemble des usagers de la route ainsi que dans certains départementaux ruraux, à l'ensemble des routes et pouvant, par suite, être à l'origine de nombreuses pertes de points pour les automobilistes, il y a urgence à en suspendre les effets ; que, dans ces conditions, ni les écritures des requérants, ni les pièces versées au dossier, ni, au demeurant, les échanges au cours de l'audience publique, ne permettent de faire apparaître d'éléments concrets et circonstanciés propres à établir que l'exécution du décret du 15 juin 2018 porterait atteinte, de manière suffisamment grave et immédiate, à un intérêt public ou à la situation des requérants ; que, par suite, la condition d'urgence prévue par l'article L. 521-1 du code de justice administrative n'est, en l'état de l'instruction, pas satisfaite ; qu'il n'y a pas lieu, dès lors, d'examiner si les moyens soulevés par les requérants sont de nature à créer un doute sérieux sur la légalité du décret du 15 juin 2018 ;<br/>
<br/>
              5. Considérant qu'il résulte de ce qui précède, et alors qu'il ne saurait utilement être allégué que l'égalité des armes entre les parties aurait été rompue devant le juge des référés du seul fait que le décret du 15 juin 2018 a été pris au vu d'un avis de la section des travaux publics du Conseil d'Etat, dont seul l'Etat a été le destinataire et qui n'a pas été communiqué aux requérants, dès lors, en tout état de cause, que les requérants n'ont formulé aucun moyen, assorti des précisions permettant d'en examiner le bien-fondé, relatif à la consultation préalable du Conseil d'Etat, la demande de M. A...B...et autres tendant à la suspension de l'exécution de ce décret doit être rejetée ;<br/>
<br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de M. A...B...et autres est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à M. C...A...B..., premier dénommé, pour l'ensemble des requérants, au Premier ministre et au ministre d'Etat, ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
