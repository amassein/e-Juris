<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044367646</ID>
<ANCIEN_ID>JG_L_2021_11_000000435178</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/36/76/CETATEXT000044367646.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème - 5ème chambres réunies, 24/11/2021, 435178, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-11-24</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>435178</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème - 5ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>HAAS ; SCP GASCHIGNARD</AVOCATS>
<RAPPORTEUR>Mme Cécile Vaullerin</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Stéphane Hoynck</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:435178.20211124</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. E... I... et Mme D... I... ont demandé au tribunal administratif de Grenoble d'annuler pour excès de pouvoir la délibération du 15 novembre 2016 par laquelle le conseil municipal de Lapeyrouse-Mornay a approuvé le plan local d'urbanisme de la commune. Par un jugement n° 1700252 du 25 octobre 2018, le tribunal administratif a annulé la délibération du 15 novembre 2016 en tant qu'elle approuve le classement en zone agricole des parcelles bâties du hameau de Bois-Vieux et de la parcelle cadastrée Section ZC n° 40 et a rejeté le surplus des conclusions de la demande des consorts I....<br/>
<br/>
              Par un arrêt n° 18LY04640 18LY04663 du 6 août 2019, la cour administrative de Lyon a, sur appel de la commune de Lapeyrouse-Mornay, annulé l'article 1er de ce jugement et rejeté, d'une part, les conclusions de la demande des consorts I... devant le tribunal administratif de Grenoble et, d'autre part, leur appel formé contre ce jugement.  <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés le 7 octobre 2019 et le 8 janvier 2020 au secrétariat du contentieux du Conseil d'Etat, M. E... I... et Mme D... I... demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) de mettre à la charge de la commune de Lapeyrouse-Mornay la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'urbanisme ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Cécile Vaullerin, auditrice,  <br/>
<br/>
              - les conclusions de M. Stéphane Hoynck, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Gaschignard, avocat de M. I... et autre et à Me Haas, avocat de la commune de Lapeyrouse-Mornay ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que les consorts I... ont demandé au tribunal administratif de Grenoble d'annuler la délibération du 15 novembre 2016 par laquelle le conseil municipal de Lapeyrouse-Mornay a approuvé le plan local d'urbanisme (PLU) de la commune. Par un jugement du 25 octobre 2018, le tribunal administratif de Grenoble a partiellement fait droit à cette demande et annulé la délibération litigieuse en tant qu'elle approuve le classement en zone agricole des parcelles bâties du hameau du Bois-Vieux et de la parcelle cadastrée Section ZC n° 40. Sur appel de la commune de Lapeyrouse-Mornay, la cour administrative d'appel de Nantes a, par un arrêt du 6 août 2019, annulé ce jugement et rejeté la demande des consorts I... devant le tribunal administratif. M. et Mme I... se pourvoient en cassation contre cet arrêt. <br/>
<br/>
              2. En premier lieu, en vertu de l'article L. 151-5 du code de l'urbanisme, le projet d'aménagement et de développement durables du plan local d'urbanisme définit notamment " Les orientations générales des politiques d'aménagement, d'équipement, d'urbanisme, de paysage, de protection des espaces naturels, agricoles et forestiers, et de préservation ou de remise en bon état des continuités écologiques ". En vertu de l'article L. 151-9 du même code : " Le règlement délimite les zones urbaines ou à urbaniser et les zones naturelles ou agricoles et forestières à protéger. / Il peut préciser l'affectation des sols selon les usages principaux qui peuvent en être faits ou la nature des activités qui peuvent y être exercées et également prévoir l'interdiction de construire. / Il peut définir, en fonction des situations locales, les règles concernant la destination et la nature des constructions autorisées ". Aux termes de l'article R. 151-22 du code de l'urbanisme : " Les zones agricoles sont dites " zones A ". Peuvent être classés en zone agricole les secteurs de la commune, équipés ou non, à protéger en raison du potentiel agronomique, biologique ou économique des terres agricoles ". L'article R. 151-23 du même code précise que " Peuvent être autorisées, en zone A : / 1° Les constructions et installations nécessaires à l'exploitation agricole ou au stockage et à l'entretien de matériel agricole par les coopératives d'utilisation de matériel agricole agréées au titre de l'article L. 525-1 du code rural et de la pêche maritime ; / 2° Les constructions, installations, extensions ou annexes aux bâtiments d'habitation, changements de destination et aménagements prévus par les articles L. 151-11, L. 151-12 et L. 151-13, dans les conditions fixées par ceux-ci. ".<br/>
<br/>
              3. Il résulte de ces dispositions qu'une zone agricole, dite " zone A ", du plan local d'urbanisme a vocation à couvrir, en cohérence avec les orientations générales et les objectifs du projet d'aménagement et de développement durables, un secteur, équipé ou non, à protéger en raison du potentiel agronomique, biologique ou économique des terres agricoles. <br/>
<br/>
              4. Si, pour apprécier la légalité du classement d'une parcelle en zone A, le juge n'a pas à vérifier que la parcelle en cause présente, par elle-même, le caractère d'une terre agricole et peut se fonder sur la vocation du secteur auquel cette parcelle peut être rattachée, en tenant compte du parti urbanistique retenu ainsi que, le cas échéant, de la nature et de l'ampleur des aménagements ou constructions qu'elle supporte, ce classement doit cependant être justifié par la préservation du potentiel agronomique, biologique ou économique des terres agricoles de la collectivité concernée, à plus forte raison lorsque les parcelles en cause comportent des habitations voire présentent un caractère urbanisé. <br/>
<br/>
              5. Pour juger que le classement en zone A de l'ensemble du secteur du hameau du Bois-Vieux, situé à environ un kilomètre du centre-bourg, dont il ressort de l'arrêt attaqué qu'il comporte notamment une trentaine d'habitations et présente un caractère urbanisé, n'était pas entaché d'une erreur manifeste d'appréciation, la cour, en relevant que les auteurs du PLU avaient entendu préserver les ressources agricoles de la commune et rechercher un équilibre entre le développement résidentiel et le maintien du " caractère rural " du hameau, situé au cœur d'une vaste plaine agricole de bonne valeur agronomique et facilement exploitable, alors qu'il ne ressort pas des pièces du dossier que ce classement permet d'assurer la préservation du potentiel agronomique, biologique ou économique des terres agricoles de cette commune, a dénaturé les pièces du dossier et les faits de l'espèce. <br/>
<br/>
              6. Il résulte de ce qui précède, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, que les consorts I... sont fondés à demander l'annulation de l'arrêt de la cour administrative d'appel de Lyon qu'ils attaquent.<br/>
<br/>
              7. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la commune de Lapeyrouse-Mornay la somme de 4 000 euros à verser aux consorts I... au titre des dispositions de l'article L. 761-1 du code de justice administrative. Ces mêmes dispositions font obstacle à ce qu'une somme soit mise à la charge des consorts I... qui ne sont pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 6 août 2019 de la cour administrative d'appel de Lyon est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Lyon.<br/>
Article 3 : La commune de Lapeyrouse-Mornay versera la somme de 4 000 euros à M. et Mme I... au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à M. E... I..., à Mme D... I... et à la commune de Lapeyrouse-Mornay.<br/>
<br/>
              Délibéré à l'issue de la séance du 22 octobre 2021 où siégeaient : M. Rémy Schwartz, président adjoint de la Section du contentieux, présidant ; M. B... J..., M. Fabien Raynaud, présidents de chambre ; M. N... F..., Mme H... M..., M. G... K..., M. A... L..., Mme Bénédicte Fauvarque-Cosson, conseillers d'Etat et Mme Cécile Vaullerin, auditrice-rapporteure. <br/>
<br/>
              Rendu le 24 novembre 2021.<br/>
                 Le président : <br/>
                 Signé : M. Rémy Schwartz<br/>
 		La rapporteure : <br/>
      Signé : Mme Cécile Vaullerin<br/>
                 La secrétaire :<br/>
                 Signé : Mme O... C...<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
