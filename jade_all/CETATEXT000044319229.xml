<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044319229</ID>
<ANCIEN_ID>JG_L_2021_11_000000449574</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/31/92/CETATEXT000044319229.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème chambre, 10/11/2021, 449574, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-11-10</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>449574</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème chambre</FORMATION>
<TYPE_REC/>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Audrey Prince</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Marc Pichon de Vendeuil</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:449574.20211110</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme D... B... a demandé au tribunal administratif de Limoges d'annuler l'arrêté du 3 juillet 2018 lui concédant une pension de retraite en tant qu'il lui applique le coefficient de minoration prévu au II de l'article L. 14 du code des pensions civiles et militaires de retraite.<br/>
<br/>
              Par un jugement n° 1900114 du 16 décembre 2020, le tribunal administratif de Limoges a fait droit à cette demande.<br/>
<br/>
              Par un pourvoi, enregistré le 10 février 2021 au secrétariat du contentieux du Conseil d'Etat, le ministre de l'économie, des finances et de la relance demande au Conseil d'Etat d'annuler ce jugement. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ; <br/>
<br/>
              Vu :<br/>
              -le code de la défense ;<br/>
              -le code des pensions civiles et militaires de retraite ;<br/>
              -le code de justice administrative ; <br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Audrey Prince, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Marc Pichon de Vendeuil, rapporteur public ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des énonciations du jugement attaqué que Mme B... est titulaire d'une pension militaire de retraite qui lui a été concédée par arrêté du 3 juillet 2018. Elle a demandé au tribunal administratif de Limoges d'annuler cet arrêté en tant qu'il faisait application d'un coefficient de 1,25% de minoration de sa pension, faute de tenir compte d'une période de six mois de congé parental. Le ministre de l'économie, des finances et de la relance se pourvoit en cassation contre le jugement du 16 décembre 2020 par lequel le tribunal administratif de Limoges a fait droit à cette demande.<br/>
<br/>
              2. Aux termes du deuxième alinéa du II de l'article L. 14 du code des pensions civiles et militaires de retraite : " Lorsque la durée de services militaires effectifs est inférieure à la durée nécessaire pour pouvoir bénéficier d'une liquidation de la pension, définie au II de l'article L. 24, augmentée d'une durée de services effectifs de dix trimestres, un coefficient de minoration de 1,25 % s'applique au montant de la pension militaire liquidée en application des articles L. 13 et L. 15 dans la limite de dix trimestres. Le temps passé en congé de longue durée pour maladie et en congé de longue maladie est assimilé à des services militaires effectifs. " <br/>
<br/>
              3. Il résulte de ces dispositions que seule la durée des services militaires effectifs, lesquels excluent les services accomplis à titre civil, est prise en compte pour la détermination de l'éventuel coefficient de minoration de la pension militaire de retraite et que cette durée n'inclut par assimilation que les congés limitativement énumérés par ces dispositions.<br/>
<br/>
              4. Pour annuler l'arrêté de concession de pension en tant qu'il applique un coefficient de minoration de 1,25% à la pension de Mme B... faute de tenir compte de la période de congé parental, le tribunal administratif de Limoges s'est fondé sur la circonstance que cette période pouvait être considérée comme du " service effectif " au sens de l'article L. 4138-14 du code de la défense. En en déduisant qu'elle devait également être assimilée du temps de " services militaires effectifs " pour l'application des dispositions du deuxième alinéa du II de l'article L. 14 du code des pensions civiles et militaires de retraite, alors que le congé parental n'est pas au nombre des congés assimilés à des " services militaires effectifs " mentionnés par ces dispositions, le tribunal administratif de Limoges a commis une erreur de droit.<br/>
<br/>
              5. Il résulte de ce qui précède que le ministre de l'économie, des finances et de la relance est fondé à demander l'annulation du jugement qu'il attaque.<br/>
<br/>
              6. Il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative.<br/>
<br/>
              7. Il résulte de ce qui a été dit au point 4 qu'en tant qu'il refuse d'assimiler la période de congé parental à des services militaires effectifs, l'arrêté du 3 juillet 2018 concédant une pension de retraite à Mme B... n'est pas entaché d'erreur de droit. Par suite, Mme B... n'est pas fondée à en demander l'annulation. <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du 16 décembre 2020 du tribunal administratif de Limoges est annulé.<br/>
Article 2 : La demande de Mme B... devant le tribunal administratif de Limoges est rejetée.<br/>
Article 3 : La présente décision sera notifiée au ministre de l'économie, des finances et de la relance et à Mme D... B.... <br/>
Copie en sera adressée à la ministre des armées.<br/>
              Délibéré à l'issue de la séance du 21 octobre 2021 où siégeaient : M. Olivier Japiot, Président de chambre, Présidant ; M. Gilles Pellissier, Conseiller d'Etat et Mme Audrey Prince, maître des requêtes en service extraordinaire-rapporteure. <br/>
<br/>
              Rendu le 10 novembre 2021.<br/>
<br/>
<br/>
                 Le président : <br/>
                 Signé : M. Olivier Japiot<br/>
 		La rapporteure : <br/>
      Signé : Mme Audrey Prince<br/>
                 La secrétaire :<br/>
                 Signé : Mme C... A...<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
