<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031861346</ID>
<ANCIEN_ID>JG_L_2015_12_000000388264</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/86/13/CETATEXT000031861346.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème SSJS, 30/12/2015, 388264, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-12-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>388264</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème SSJS</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Bastien Lignereux</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emilie Bokdam-Tognetti</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2015:388264.20151230</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, un nouveau mémoire et un mémoire en réplique, enregistrés les 24 février, 10 mars et 6 novembre 2015 au secrétariat du contentieux du Conseil d'Etat, le syndicat Artema, le syndicat Symacap, la société DK Technologies et la société Bosch Rexroth demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir l'arrêté du 22 décembre 2014 définissant les opérations standardisées d'économie d'énergie, et plus particulièrement la fiche opération n° IND-UT-129 intitulée " Presse à injecter toute électrique ou hybride " figurant à l'annexe 4 ce même arrêté ;<br/>
<br/>
              2°) de mettre à la charge de l'État la somme de 3 000 euros en application des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - l'arrêté attaqué ; <br/>
              - la directive 2012/27/UE du Parlement européen et du Conseil du 25 octobre 2012, notamment son article 7 ;<br/>
              - le code de l'énergie ;<br/>
              - le décret n° 2005-850 du 27 juillet 2005 ;<br/>
              - le décret n° 2010-1664 du 29 décembre 2010 ;<br/>
              - la décision du 24 décembre 2012 du directeur général de l'énergie et du climat portant délégation de signature ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Bastien Lignereux, auditeur,<br/>
<br/>
              - les conclusions de Mme Emilie Bokdam-Tognetti, rapporteur public ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il résulte des dispositions de l'article L. 221-1 du code de l'énergie que les personnes morales qui fournissent de l'énergie aux consommateurs finals et dont les ventes excèdent un seuil sont soumises à des obligations d'économies d'énergie, dont elles peuvent se libérer soit en réalisant elles-mêmes des économies d'énergie, soit en acquérant des certificats d'économies d'énergie et des articles L. 221-7 et L. 221-8 du même code que les certificats d'économies d'énergie sont délivrés par l'État ou, en son nom, par un organisme habilité à cet effet, aux personnes dont l'action, additionnelle par rapport à leur activité habituelle, permet la réalisation d'économies d'énergie d'un volume supérieur à un seuil fixé par arrêté. Selon les dispositions de l'article 2 du décret du 29 décembre 2010 relatif aux certificats d'économies d'énergie : " Les actions menées par les personnes mentionnées à l'article 1er qui peuvent donner lieu à la délivrance de certificats d'économies d'énergie sont : / - la réalisation d'opérations standardisées définies par arrêté du ministre chargé de l'énergie et assorties d'une valeur forfaitaire d'économies d'énergie déterminée par rapport à la situation de référence de performance énergétique définie au deuxième alinéa de l'article 3 (...) ". Les requérants demandent l'annulation pour excès de pouvoir de l'arrêté du 22 décembre 2014 définissant les opérations standardisées d'économies d'énergie.<br/>
<br/>
              Sur l'incompétence alléguée de l'auteur de l'arrêté :<br/>
<br/>
              2. Aux termes de l'article 1er de la décision du 24 décembre 2012, publiée au Journal officiel de la République française le 5 janvier 2013, du directeur général de l'énergie et du climat portant délégation de signature, M. B...A..., chef du service climat et efficacité énergétique disposait d'une délégation " (...) donnée à l'effet de signer, au nom du ministre chargé de l'énergie et des affaires climatiques, du ministre chargé des matières premières et des mines et du ministre chargé des transports, tous actes, arrêtés, décisions, à l'exclusion des décrets, dans la limite des attributions de la direction générale de l'énergie et du climat, aux fins d'exercice des permanences ". Il résulte de ces dispositions que M. B...A...avait qualité pour signer l'arrêté attaqué au nom du ministre de l'écologie, du développement durable et de l'énergie. Dès lors, le moyen d'incompétence invoqué ne peut qu'être écarté.<br/>
<br/>
              Sur le moyen tiré de la contrariété de la procédure d'élaboration des fiches d'opérations standardisées d'économie d'énergie avec la directive du Parlement européen et du Conseil du 25 octobre 2012 relative à l'efficacité énergétique :<br/>
<br/>
              3. Selon les dispositions précitées de l'article 2 du décret du 29 décembre 2010 relatif aux certificats d'économie d'énergie, les opérations standardisées d'économie d'énergie sont définies par arrêtés du ministre chargé de l'énergie. Ce texte ne soumet à aucune obligation procédurale particulière l'élaboration de ces fiches. Il ressort, toutefois, des écritures du ministre en défense que les projets de fiches d'opération sont préparés par des groupes de travail associant les professionnels du secteur, puis sont soumis à l'Agence de l'environnement et de la maîtrise de l'énergie et au conseil supérieur de l'énergie, avant que les fiches ne soient arrêtées par le ministre. Par ailleurs, aux termes du point 6 de l'article 7 de la directive du 25 octobre 2012 relative à l'efficacité énergétique : " Les États membres (...) mettent en place des systèmes de mesure, de contrôle et de vérification assurant la vérification d'au moins une proportion statistiquement significative et représentative des mesures visant à améliorer l'efficacité énergétique instaurées par les parties obligées. Cette mesure, ce contrôle et cette vérification sont effectués indépendamment des parties obligées. ". Enfin, l'annexe V de cette même directive fixe diverses règles méthodologiques relatives au chiffrage des mesures visant à améliorer l'efficacité énergétique définies par ce même article 7.<br/>
<br/>
              4. Pour demander l'annulation de l'arrêté attaqué, les requérants font valoir que celui-ci a été pris au terme d'une procédure méconnaissant les dispositions précitées et le " principe d'indépendance et de transparence " qu'elles instaureraient. Toutefois, il se borne à alléguer en des termes très généraux que les opérations standardisées d'économies d'énergie seraient en réalité définies par un groupe qui rassemblerait les seules personnes obligées à la réalisation d'économies d'énergie et n'assortit pas son moyen des précisions qui permettraient d'en apprécier le bien-fondé. Dès lors, à supposer même que l'article 7 de la directive du 25 octobre 2012 soit applicable à la définition des opérations standardisées d'économie d'énergie, le moyen ne peut, en tout état de cause, qu'être écarté.<br/>
<br/>
              Sur le moyen tiré de la méconnaissance du principe d'égalité par la fiche n°IND-UT-129 intitulée " Presse à injecter toute électrique ou hybride " :<br/>
<br/>
              5. A l'annexe 4 de l'arrêté attaqué, la fiche n° IND-UT-129 intitulée " Presse à injecter toute électrique ou hybride " prévoit que la mise en place d'une presse à injecter toute électrique ou d'une presse à injecter hybride (électrique et hydraulique) ou la transformation d'une presse à injecter hydraulique en presse à injecter hybride ouvre droit à la délivrance de certificats. Les requérants demandent l'annulation de cette fiche en tant qu'elle conduit, selon eux, à une rupture d'égalité entre les concurrents du secteur.<br/>
<br/>
              6. Le principe d'égalité ne s'oppose ni à ce que le pouvoir réglementaire règle de façon différente des situations différentes ni à ce qu'il déroge à l'égalité pour des raisons d'intérêt général, pourvu que, dans l'un et l'autre cas, la différence de traitement qui en résulte soit en rapport direct avec l'objet de la loi qui l'établit. Selon les requérants, la différence de traitement qui résulte de la fiche attaquée, en défaveur des presses à injecter hydrauliques, n'est pas en rapport avec l'objectif d'efficacité énergétique qui est celui du dispositif des certificats d'économie d'énergie. Toutefois, ils ne contredisent pas utilement l'argument avancé par le ministre en défense selon lequel les presses hydrauliques, du fait des pertes engendrées par le mécanisme de conversion de l'énergie électrique utilisé pour faire fonctionner leur système hydraulique, auraient une moindre efficacité énergétique que les presses intégralement électriques ou hybrides.<br/>
<br/>
              Sur le moyen tiré des inexactitudes matérielles dont serait entachée la fiche n°IND-UT-129 :<br/>
<br/>
              7. Les requérants font valoir que la durée de vie conventionnelle de 15 ans retenue par la fiche pour les presses à injecter électriques ne serait pas établie, que la référence sémantique au " tout électrique " serait un abus de langage dès lors qu'un minimum d'énergie hydraulique serait requis et qu'enfin les pertes de puissances électriques ne sont pas prises en compte dans la note de calcul associée à la fiche attaquée. A supposer que les documents versés aux débats au soutien de cette argumentation soient regardés comme probants, ils ne sont pas de nature à démontrer que la technologie hydraulique serait aussi performante en termes d'économies d'énergie que la technologie " toute électrique ou hybride ". Le moyen ne peut donc qu'être écarté.<br/>
<br/>
              Sur le moyen tiré de l'erreur manifeste d'appréciation dont serait entachée la fiche attaquée :<br/>
<br/>
              8. Si les requérants soutiennent que la fiche opération attaquée est entachée d'erreur manifeste d'appréciation au motif " qu'elle ne refléterait pas la réalité industrielle ", ils se prévalent à l'appui de ce moyen, d'une part, de leurs propres études, notamment de celle produite par Bosch Rexroth, et, d'autre part, de documents issus de l'association professionnelle EUROMAP, qui regroupe certains industriels européens du secteur des machines-outils destinées à l'industrie du plastique et du caoutchouc, faisant seulement état de perspectives à l'horizon temporel de 2020. Le moyen tiré de ce que la fiche serait entachée d'erreur manifeste d'appréciation ne peut dès lors qu'être écarté.<br/>
<br/>
              9. Il résulte de tout ce qui précède que la requête des syndicats Artema et Symacap et des sociétés DK Technologies et Bosch Rexroth doit être rejetée.<br/>
<br/>
              10. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'État qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
Article 1er : La requête des syndicats Artema et Symacap et des sociétés DK Technologies et Bosch Rexroth est rejetée.<br/>
Article 2 : La présente décision sera notifiée au syndicat Artema, au syndicat Symacap, à la société DK Technologies, à la société Bosch Rexroth et à la ministre de l'écologie, du développement durable et de l'énergie.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
