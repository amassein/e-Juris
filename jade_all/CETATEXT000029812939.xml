<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029812939</ID>
<ANCIEN_ID>JG_L_2014_11_000000361105</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/81/29/CETATEXT000029812939.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème / 1ère SSR, 28/11/2014, 361105</TITRE>
<DATE_DEC>2014-11-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>361105</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème / 1ère SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP NICOLAY, DE LANOUVELLE, HANNOTIN ; SCP SPINOSI, SUREAU</AVOCATS>
<RAPPORTEUR>Mme Sophie Roussel</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Suzanne von Coester</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:361105.20141128</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 16 juillet et 16 octobre 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour la société GIAT Industries, dont le siège est 13, route de la Minière à Versailles (78000) ; la société GIAT Industries demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler l'arrêt n° 10NT01920 du 14 mai 2012 par lequel la cour administrative d'appel de Nantes, d'une part, a annulé, sur la requête de la société Territoires et développement, le jugement nos 06-1943 et 06-3385 du 10 juin 2010 par lequel le tribunal administratif de Rennes a annulé, à la demande de la société GIAT Industries, les arrêtés des 2 février et 6 juin 2006 du préfet d'Ille-et-Vilaine déclarant cessibles à son profit les terrains nécessaires à la réalisation de la zone d'aménagement concerté (ZAC) de la Courrouze sur le territoire des communes de Rennes et Saint-Jacques-de-la-Lande et, d'autre part, rejeté la demande de la société devant le tribunal administratif de Rennes contre ces arrêtés ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à ses conclusions d'appel ; <br/>
<br/>
              3°) de mettre à la charge de la société Territoires et développement une somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              elle soutient que : <br/>
              -	la cour administrative d'appel de Nantes a omis de répondre au moyen tiré des inexactitudes matérielles entachant l'arrêté du 2 février 2006 ; <br/>
              -	elle a insuffisamment motivé son arrêt s'agissant de l'absence d'utilité publique de l'opération envisagée, invoquée au soutien de l'exception d'illégalité dirigée contre l'arrêté du 4 janvier 2006 déclarant d'utilité publique le projet ;<br/>
              -	en jugeant que le commissaire enquêteur avait effectivement rendu un avis sur l'emprise des ouvrages projetés, la cour a entaché son arrêt d'insuffisance de motivation et d'erreur de droit et a dénaturé les pièces du dossier ; <br/>
              -	pour écarter le moyen tiré d'un vice propre entachant les arrêtés de cessibilité, la cour administrative d'appel de Nantes a insuffisamment motivé son arrêt et commis une erreur de droit ;<br/>
<br/>
              Vu l'arrêt attaqué ;<br/>
<br/>
              Vu le mémoire en défense, enregistré le 24 mai 2013, présenté pour la société Territoires et développement, qui conclut, à titre principal, au rejet du pourvoi et, à titre subsidiaire, à ce qu'il soit fait droit à ses conclusions d'appel ; elle soutient que les moyens soulevés par la requérante ne sont pas fondés ; <br/>
<br/>
              Vu le mémoire en réplique, enregistré le 6 janvier 2014, présenté pour la société GIAT Industries, qui reprend les conclusions de son pourvoi et les mêmes moyens ; <br/>
<br/>
              Vu le nouveau mémoire, enregistré le 9 avril 2014, présenté pour la société GIAT Industries, qui reprend les conclusions de son pourvoi et les mêmes moyens ; elle soutient en outre, dans l'hypothèse d'un règlement de l'affaire au fond, que l'annulation par un jugement définitif du tribunal administratif de Rennes de la décision du président de la communauté d'agglomération Rennes Métropole du 26 décembre 2003 de signer la convention publique d'aménagement conclue entre la communauté d'agglomération et la société Territoires et développement concernant la ZAC La Courrouze emporte, par voie de conséquence, l'annulation des arrêtés de cessibilité des 2 février et 6 juin 2006 ; <br/>
<br/>
              Vu les pièces desquelles il ressort que le pourvoi a été communiqué au ministre de l'intérieur et à la ministre du logement, de l'égalité des territoires et de la ruralité, qui n'ont pas produit de mémoire ; <br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Sophie Roussel, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Suzanne von Coester, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Nicolaÿ, de Lanouvelle, Hannotin, avocat de la société GIAT Industries et à la SCP Spinosi, Sureau, avocat de la société Territoires et développement ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des énonciations de l'arrêt attaqué que, par un arrêté du 2 février 2006, modifié le 6 juin suivant, le préfet d'Ille-et-Vilaine a déclaré cessibles, au profit de la société Territoires et développement, les terrains nécessaires à la réalisation de la zone d'aménagement concerté (ZAC) de la Courrouze, sur le territoire des communes de Rennes et Saint-Jacques-de-la-Lande ; qu'à la demande de la société GIAT Industries, le tribunal administratif de Rennes a, par un jugement du 10 juin 2010, annulé ces arrêtés ; que, par un arrêt du 14 mai 2012, la cour administrative d'appel de Nantes a, sur la requête de la société Territoires et développement, annulé le jugement du tribunal administratif de Rennes et rejeté la demande présentée par la société GIAT Industries devant ce tribunal ; que cette dernière se pourvoit en cassation contre cet arrêt ; <br/>
<br/>
              2. Considérant qu'il ressort des pièces de la procédure que la société GIAT Industrie avait soulevé le moyen, dirigé par la voie de l'exception contre l'arrêté du 4 janvier 2006 déclarant d'utilité publique le projet d'acquisition de terrains en vue de l'aménagement de la ZAC de la Courrouze, tiré de l'absence d'utilité publique de l'opération ; que la cour administrative d'appel de Nantes, statuant dans le cadre de l'effet dévolutif de l'appel, n'a pas répondu à ce moyen, qui n'était pas inopérant et que la société GIAT Industries n'avait pas abandonné ; que, par suite et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, la société est fondée, pour ce motif, à demander l'annulation de l'arrêt qu'elle attaque ; <br/>
<br/>
              3. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              4. Considérant que, pour annuler les arrêtés attaqués, le tribunal administratif de Rennes s'est fondé sur le moyen, dont il était saisi par la voie de l'exception, tiré de ce que la convention confiant à la société Territoires et développement l'aménagement de la ZAC de la Courrouze, signée le 26 décembre 2003, avait été irrégulièrement conclue sans publicité préalable, en méconnaissance des objectifs fixés par la directive 93/37/CEE du 14 juin 1993 ; qu'il a relevé que la délibération du conseil communautaire l'approuvant était par suite illégale et que cette illégalité viciait par voie de conséquence l'arrêté du 4 janvier 2006 déclarant d'utilité publique le projet d'aménagement de la ZAC, ainsi que les arrêtés de cessibilité des 2 février et 6 juin 2006 attaqués ;<br/>
<br/>
              5. Considérant que l'illégalité d'un acte administratif, qu'il soit ou non réglementaire, ne peut être utilement invoquée par voie d'exception à l'appui de conclusions dirigées contre une décision administrative ultérieure que si cette dernière décision a été prise pour l'application du premier acte ou s'il en constitue la base légale ; que les actes, déclaration d'utilité publique et arrêtés de cessibilité, tendant à l'acquisition par voie d'expropriation des terrains nécessaires à la réalisation d'une ZAC ne sont pas des actes pris pour l'application de la délibération approuvant la convention par laquelle la commune a confié à une société l'aménagement de cette zone, laquelle ne constitue pas davantage leur base légale, alors même que la déclaration d'utilité publique a été prise pour permettre la réalisation de cette opération d'aménagement et qu'elle précisait que l'expropriation était réalisée au profit de la société chargée de l'aménagement de la zone ; que, par suite, la société Territoires et développement est fondée à soutenir que c'est à tort que le tribunal administratif de Rennes s'est fondé, pour annuler les arrêtés litigieux, sur le moyen tiré, par la voie de l'exception, de l'illégalité de la délibération approuvant la convention d'aménagement ; <br/>
<br/>
              6. Considérant toutefois qu'il appartient au Conseil d'Etat, saisi de l'ensemble du litige par l'effet dévolutif de l'appel, d'examiner les autres moyens soulevés par la société GIAT Industries devant le tribunal administratif de Rennes ; <br/>
<br/>
              7. Considérant, d'une part, qu'aux termes de l'article L. 311-1 du code de l'urbanisme : " Les zones d'aménagement concerté sont les zones à l'intérieur desquelles une collectivité publique ou un établissement public y ayant vocation décide d'intervenir pour réaliser ou faire réaliser l'aménagement et l'équipement des terrains, notamment de ceux que cette collectivité ou cet établissement a acquis ou acquerra en vue de les céder ou de les concéder ultérieurement à des utilisateurs publics ou privés. / (...) " ; <br/>
<br/>
              8. Considérant, d'autre part, qu'aux termes du I de l'article L. 11-1 du code de l'expropriation pour cause d'utilité publique : " L'expropriation d'immeubles, en tout ou partie, ou de droits réels immobiliers ne peut être prononcée qu'autant qu'elle aura été précédée d'une déclaration d'utilité publique intervenue à la suite d'une enquête publique et qu'il aura été procédé contradictoirement à la détermination des parcelles à exproprier ainsi qu'à la recherche des propriétaires, des titulaires de droits réels et autres intéressés. " ; que, s'agissant du dossier soumis à enquête préalable à la déclaration d'utilité publique, l'article R. 11-3 de ce code distingue les documents devant y figurer selon que la déclaration d'utilité publique est demandée " en vue de la réalisation de travaux ou d'ouvrages " ou qu'elle est demandée " en vue de l'acquisition d'immeubles ou en vue de la réalisation d'une opération d'aménagement ou d'urbanisme importante et qu'il est nécessaire de procéder à l'acquisition des immeubles avant que le projet n'ait pu être établi " ; que, s'agissant du dossier soumis à enquête parcellaire, l'article R. 11-19 du code dispose :  " L'expropriant adresse au préfet, pour être soumis à enquête dans chacune des communes où sont situés les immeubles à exproprier : / 1° Un plan parcellaire régulier des terrains et bâtiments ; / 2° La liste des propriétaires établie à l'aide d'extraits des documents cadastraux délivrés par le service du cadastre ou à l'aide des renseignements délivrés par le conservateur des hypothèques au vu du fichier immobilier ou par tous autres moyens. " ; qu'aux termes de l'article R. 11-21 du code : " Lorsque l'expropriant est en mesure, avant la déclaration d'utilité publique, de déterminer les parcelles à exproprier et de dresser le plan parcellaire et la liste des propriétaires, l'enquête parcellaire peut être faite soit en même temps que l'enquête préalable à la déclaration d'utilité publique, soit postérieurement. " ; qu'enfin, aux termes de l'article R. 11-25 du code, relatif à la clôture de l'enquête parcellaire : " (...). Le commissaire enquêteur ou la commission d'enquête donne son avis sur l'emprise des ouvrages projetés (...) " ; <br/>
<br/>
              9. Considérant qu'il résulte de ces dispositions qu'il peut être recouru à la procédure d'expropriation pour cause d'utilité publique, non seulement en vue de la réalisation d'ouvrages ou de travaux préalablement identifiés, mais également  lorsque, pour la réalisation d'une opération d'aménagement ou d'urbanisme, il est nécessaire, notamment dans un but de maîtrise foncière, de procéder à l'acquisition d'immeubles avant que les caractéristiques principales des travaux ou des ouvrages et leur localisation aient pu être établies ; que, dans ce dernier cas, l'avis du commissaire enquêteur ou de la commission d'enquête désigné dans le cadre de l'enquête parcellaire, exigé par les dispositions de l'article R. 11-25 du code de l'expropriation pour cause d'utilité publique citées ci-dessus, doit porter non pas sur l'emprise des ouvrages projetés mais sur le périmètre des acquisitions d'immeubles nécessaires à la réalisation du projet d'aménagement ou d'urbanisme en vue duquel l'expropriation a été demandée ; <br/>
<br/>
              10. Considérant qu'il ressort des pièces du dossier que, dans son avis rendu à la suite de l'enquête parcellaire délimitant les immeubles à acquérir pour l'aménagement de la ZAC de la Courrouze, le commissaire enquêteur a d'abord indiqué que " le dossier parcellaire déposé (...) contenait les documents fixés par l'article R. 11-19 du code de l'expropriation. " ; qu'il a ensuite relevé que " Dans la mesure où les plans contenus dans le dossier de DUP ne définissent pas clairement l'implantation des bâtiments et que l'aménagement de la ZAC de la Courrouze n'a pas encore été déclaré d'utilité publique, il m'est difficile de dire si les expropriations demandées sont actuellement nécessaires. " ; qu'en se prononçant ainsi, le commissaire enquêteur ne peut être regardé comme ayant donné son avis sur le périmètre des parcelles à exproprier ; que, par suite, la société GIAT Industries est fondée à soutenir que les arrêtés de cessibilité attaqués ont été pris au terme d'une procédure irrégulière, le préfet ne pouvant légalement prendre ces arrêtés sans avoir sollicité du commissaire enquêteur un nouvel avis ; que cette irrégularité a privé d'une garantie les propriétaires susceptibles de faire l'objet de la procédure d'expropriation et a été de nature d'exercer, en l'espèce, une influence sur le sens de la décision prise par le préfet d'Ille-et-Vilaine ; qu'il résulte de ce qui précède, sans qu'il soit besoin d'examiner les autres moyens présentés, que la société Territoires et développement n'est pas fondée à se plaindre de ce que, par le jugement attaqué, le tribunal administratif de Rennes a annulé les arrêtés contestés des 2 février et 6 juin 2006 du préfet d'Ille-et-Vilaine ;<br/>
<br/>
              11. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la société Territoires et développement la somme de 3 000 euros à verser à la société GIAT Industries, au titre des dispositions de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Nantes du 14 mai 2012 est annulé.<br/>
<br/>
Article 2 : La requête de la société Territoires et développement devant la cour administrative d'appel de Nantes est rejetée. <br/>
<br/>
Article 3 : La société Territoires et développement versera à la société GIAT Industries une somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
Article 4 : La présente décision sera notifiée à la société GIAT Industries et à la société Territoires et développement. Copie en sera adressée au ministre de l'intérieur et à la ministre du logement, de l'égalité des territoires et de la ruralité.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">34-02-01-02 EXPROPRIATION POUR CAUSE D'UTILITÉ PUBLIQUE. RÈGLES GÉNÉRALES DE LA PROCÉDURE NORMALE. ENQUÊTES. ENQUÊTE PARCELLAIRE. - EXPROPRIATION EN VUE D'UNE OPÉRATION D'AMÉNAGEMENT ET D'URBANISME - CONTENU DE L'AVIS DU COMMISSAIRE ENQUÊTEUR - PÉRIMÈTRE DES ACQUISITIONS D'IMMEUBLES NÉCESSAIRES À LA RÉALISATION DU PROJET.
</SCT>
<ANA ID="9A"> 34-02-01-02 Il peut être recouru à la procédure d'expropriation pour cause d'utilité publique, non seulement en vue de la réalisation d'ouvrages ou de travaux préalablement identifiés, mais également  lorsque, pour la réalisation d'une opération d'aménagement ou d'urbanisme, il est nécessaire, notamment dans un but de maîtrise foncière, de procéder à l'acquisition d'immeubles avant que les caractéristiques principales des travaux ou des ouvrages et leur localisation aient pu être établies. Dans ce dernier cas, l'avis du commissaire enquêteur ou de la commission d'enquête désigné dans le cadre de l'enquête parcellaire, exigé par les dispositions de l'article R. 11-25 du code de l'expropriation pour cause d'utilité publique, doit porter non pas sur l'emprise des ouvrages projetés mais sur le périmètre des acquisitions d'immeubles nécessaires à la réalisation du projet d'aménagement ou d'urbanisme en vue duquel l'expropriation a été demandée.... ,,En l'espèce, le commissaire enquêteur ne pouvant être regardé comme ayant donné son avis sur le périmètre des parcelles à exproprier, les arrêtés de cessibilité attaqués ont été pris au terme d'une procédure irrégulière, le préfet ne pouvant légalement prendre ces arrêtés sans avoir sollicité du commissaire enquêteur un nouvel avis.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
