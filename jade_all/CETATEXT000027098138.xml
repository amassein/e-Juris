<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027098138</ID>
<ANCIEN_ID>JG_L_2013_02_000000360926</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/09/81/CETATEXT000027098138.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème et 7ème sous-sections réunies, 22/02/2013, 360926</TITRE>
<DATE_DEC>2013-02-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>360926</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème et 7ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LYON-CAEN, THIRIEZ</AVOCATS>
<RAPPORTEUR>Mme Sophie-Caroline de Margerie</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Damien Botteghi</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:360926.20130222</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la protestation, enregistrée le 10 juillet 2012 au secrétariat du contentieux du Conseil d'Etat, présentée par Mlle F...H..., demeurant...; Mlle H...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler la désignation de Mme B...C...comme représentant au Parlement européen en remplacement de M. G...E...;<br/>
<br/>
              2°) d'enjoindre avant dire droit au ministre chargé des affaires européennes de communiquer la lettre de désignation de Mme B...C...et d'obtenir du président du Parlement européen la lettre de démission du Parlement européen de Mme B...C...;<br/>
<br/>
              3°) d'enjoindre au ministre chargé des affaires européennes de communiquer au Parlement européen le nom de Mlle F...H...en tant que remplaçante de M. D...A...;<br/>
<br/>
              4°) de mettre à la charge de l'Etat le versement d'une somme de 35 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales et le premier protocole additionnel à cette convention ;<br/>
<br/>
              Vu l'acte portant élection des représentants au Parlement européen au suffrage universel direct, annexé à la décision 76/87/CECA, CEE du Conseil du 20 septembre 1976 ;<br/>
<br/>
              Vu le règlement intérieur du Parlement européen ;<br/>
<br/>
              Vu le code électoral ;<br/>
<br/>
              Vu la loi n° 77-729 du 7 juillet 1977 telle que modifiée notamment par la loi n° 2009-39 du 13 janvier 2009 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Sophie-Caroline de Margerie, Conseiller d'Etat,<br/>
<br/>
              - les observations de la SCP Lyon-Caen, Thiriez, avocat de Mme B...C...,<br/>
<br/>
              - les conclusions de M. Damien Botteghi, rapporteur public,<br/>
<br/>
              La parole ayant à nouveau été donnée à SCP Lyon-Caen, Thiriez, avocat de Mme B...C... ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant que l'article R. 733-3 du code de justice administrative prévoit que la demande tendant à ce que le rapporteur public n'assiste pas au délibéré peut être présentée " à tout moment de la procédure avant le délibéré " ; qu'il en résulte qu'une telle demande n'est pas recevable et ne peut être accueillie lorsqu'elle est présentée après le délibéré ; qu'en l'espèce il a été délibéré de la requête de Mlle H... le 15 février 2013 ; que la demande de l'intéressée, enregistrée le 18 février 2013, tendant à ce que le rapporteur public n'assiste pas au délibéré n'est donc pas recevable ; <br/>
<br/>
              2. Considérant qu'aux termes de l'article 7, paragraphe 1, de l'acte portant élection des représentants au Parlement européen au  suffrage universel direct, annexé à la décision du Conseil du 20 septembre  1976 : " La qualité de membre du Parlement européen est incompatible avec celle de : / - membre du gouvernement d'un Etat membre ; (...) " ; qu'aux termes de l'article 8, paragraphe 1, du même acte : " Sous réserve des dispositions du présent acte, la  procédure électorale est régie, dans chaque Etat membre, par les dispositions nationales " ; qu'aux termes du premier alinéa de l'article 24 de la loi du 7 juillet 1977 relative à l'élection des représentants au Parlement européen, prise sur le fondement de ces dispositions, dans sa rédaction issue de la loi du 13 janvier 2009 : " Le représentant dont le siège devient vacant pour quelque cause que ce soit est remplacé par le candidat figurant sur la même liste immédiatement après le dernier candidat devenu représentant conformément à l'ordre de cette liste " et qu'aux termes du sixième alinéa du même article : " En cas d'acceptation par un représentant de fonctions gouvernementales, son remplacement est effectué, conformément au premier alinéa, jusqu'à l'expiration d'un délai d'un mois suivant la cessation de ces fonctions. A l'expiration du délai d'un mois, le représentant reprend l'exercice de son mandat. Le caractère temporaire du remplacement pour cause d'acceptation de fonctions gouvernementales s'applique au dernier candidat devenu représentant conformément à l'ordre de la liste. Celui-ci est replacé en tête des candidats non élus de cette liste " ; qu'enfin, aux termes de l'article 25 de cette même loi : " L'élection des représentants au Parlement européen peut, durant les dix jours qui suivent la proclamation des résultats du scrutin  et pour tout ce qui concerne l'application de la présente loi, être contestée par tout électeur devant le Conseil d'Etat statuant au  contentieux (...) " ;<br/>
<br/>
              3. Considérant qu'il résulte de l'instruction que Mme B...C...a été élue représentante au Parlement européen lors des élections qui se sont déroulées le 7 juin 2009 et a été proclamée élue le 11 juin 2009 par la commission nationale de recensement général des votes pour l'élection des représentants au Parlement européen ; que, nommée secrétaire d'Etat aux Aînés par décret du 23 juin 2009, elle a été remplacée au Parlement européen, en application du sixième alinéa de l'article 24 de la loi du 7 juillet 1977, par M. G...E..., candidat qui figurait immédiatement après elle sur la liste ; qu'à la suite de la cessation des fonctions ministérielles de MmeC..., le ministre chargé des affaires européennes a informé le président du Parlement européen par une lettre du 9 mai 2012 que Mme C...reprendrait l'exercice de son mandat en remplacement de M. E...; que Mme C...a assisté à la séance du Parlement européen le 4 juillet 2012 ; que Mlle H...conteste la désignation de Mme C...comme membre du Parlement européen ; qu'ainsi sa requête a le caractère d'une protestation en matière électorale ; <br/>
<br/>
              4. Considérant qu'il ressort des lettres des 24 juin et 9 juillet 2009 adressés par Mme C... au président du Parlement européen qu'elle n'a pas entendu démissionner du Parlement européen mais a informé cette autorité de ce que, ayant accepté des fonctions ministérielles au sein du Gouvernement français, elle serait remplacée à titre temporaire par M. G... E..., candidat figurant immédiatement après elle sur la liste ; qu'ainsi, contrairement à ce que soutient la requérante, elle se trouvait dans une situation non de démission mais d'incompatibilité, laquelle a pris fin le 15 mai 2012 avec la cessation de ses fonctions gouvernementales ; <br/>
<br/>
              5. Considérant que la circonstance que Mme C...a exercé simultanément, du 21 mars 2010 au 21 avril 2011, un mandat de conseiller régional de la région Rhône-Alpes et un mandat de conseiller municipal de Lyon ne peut la faire regarder comme démissionnaire d'office de son mandat européen en raison de l'incompatibilité d'un mandat européen avec l'exercice de plus d'un mandat local en vertu de l'article 6-3 de la loi du 7 juillet 1977, dès lors qu'elle ne siégeait pas au Parlement européen pendant la période concernée par cette incompatibilité ; <br/>
<br/>
              6. Considérant que le sixième alinéa de l'article 24 de la loi du 7 juillet 1977 organisant le remplacement temporaire des représentants au Parlement européen ayant accepté des fonctions gouvernementales s'applique aux membres du Parlement européen se trouvant, en cours de mandat, dans une situation d'incompatibilité du fait de l'acceptation de telles fonctions, et également à ceux qui, proclamés élus au Parlement européen, ont été remplacés à leur siège avant même la première session du Parlement ; que si l'article 5 de l'acte du 20 septembre 1976 dispose que : " 1. La période quinquennale pour laquelle sont élus les membres du Parlement européen commence à l'ouverture de la première session tenue après chaque élection. (...) / 2. Le mandat de chaque membre du Parlement européen commence et expire en même temps que la période visée au paragraphe 1 ", ces dispositions n'ont ni pour objet ni pour effet de régir la procédure de remplacement des sièges devenus vacants, ladite procédure relevant en application de l'article 13-2 du même acte, de la compétence des Etats membres ; qu'il résulte de ce qui précède qu'alors même que Mme C...n'avait pas siégé au Parlement européen lors de la première session  suivant l'élection, cette dernière, proclamée élue au Parlement européen le 11 juin 2009 et ayant exercé des fonctions gouvernementales du 23 juin 2009 au 15 mai 2012, entrait dans le champ des dispositions du sixième alinéa de l'article 24 de la loi du 7 juillet 1977 ;  <br/>
<br/>
              7. Considérant que les moyens tirés de la violation du premier article de la Constitution relatif au principe d'égal accès des citoyens aux mandats électoraux et aux fonctions électives et d'un détournement de pouvoir sont, en tout état de cause, inopérants ;<br/>
<br/>
              8. Considérant que la désignation attaquée de Mme C...ne porte pas atteinte à la liberté d'expression de la requérante ; qu'ainsi, le moyen selon lequel cette désignation méconnaîtrait les stipulations de l'article 10 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ne peut qu'être écarté ;<br/>
<br/>
              9. Considérant qu'il résulte de tout ce qui précède que Mlle H...n'est pas fondée à demander l'annulation de la désignation de Mme C...comme représentante au Parlement européen en remplacement de M. E...;<br/>
<br/>
              Sur les conclusions tendant à l'application des dispositions de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              10. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mis à la charge de l'Etat, qui n'est pas partie à la présente instance, la somme que demande Mlle H...au titre des frais qu'elle a exposés et qui ne sont pas compris dans les dépens ; qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire application de ces dispositions et de mettre à la charge de Mlle H...la somme que demande Mme C... au titre des frais exposés par elle et non compris dans les dépens ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : La protestation de Mlle H...est rejetée.<br/>
<br/>
Article 2 : Les conclusions de Mme C...tendant à l'application des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 3 : La présente décision sera notifiée à Mlle F...H..., à Mme B...C..., au Premier ministre, au ministre des affaires étrangères et au ministre de l'intérieur.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">28-023 ÉLECTIONS ET RÉFÉRENDUM. ÉLECTIONS AU PARLEMENT EUROPÉEN. - DÉPUTÉ ÉLU AU PARLEMENT EUROPÉEN ET REMPLACÉ POUR INCOMPATIBILITÉ - REPRISE DE L'EXERCICE DU MANDAT LORSQUE L'INCOMPATIBILITÉ PREND FIN - EXISTENCE, ALORS MÊME QU'IL N'A PAS SIÉGÉ AU PARLEMENT EUROPÉEN LORS DE LA PREMIÈRE SESSION SUIVANT L'ÉLECTION.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-05-02 PROCÉDURE. INCIDENTS. RÉCUSATION. - DEMANDE TENDANT À CE QUE LE RAPPORTEUR PUBLIC N'ASSISTE PAS AU DÉLIBÉRÉ (2È AL. DE L'ART. R. 733-3 DU CJA) - DEMANDE PRÉSENTÉE APRÈS LE DÉLIBÉRÉ - CONSÉQUENCE - IRRECEVABILITÉ (SOL. IMPL).
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">54-06-01 PROCÉDURE. JUGEMENTS. RÈGLES GÉNÉRALES DE PROCÉDURE. - DEMANDE TENDANT À CE QUE LE RAPPORTEUR PUBLIC N'ASSISTE PAS AU DÉLIBÉRÉ (2È AL. DE L'ART. R. 733-3 DU CJA) - DEMANDE PRÉSENTÉE APRÈS LE DÉLIBÉRÉ - CONSÉQUENCE - IRRECEVABILITÉ (SOL. IMPL).
</SCT>
<ANA ID="9A"> 28-023 Un député proclamé élu au Parlement européen et remplacé pour incompatibilité retrouve son siège lorsque cette incompatibilité prend fin, alors même qu'il n'a pas siégé au Parlement européen lors de la première session suivant l'élection.</ANA>
<ANA ID="9B"> 54-05-02 L'article R. 733-3 du code de justice administrative (CJA) prévoit que :  Sauf demande contraire d'une partie, le rapporteur public assiste au délibéré. Il n'y prend pas part. / La demande prévue à l'alinéa précédent est présentée par écrit. Elle peut l'être à tout moment de la procédure avant le délibéré.,,Une demande tendant à ce que le rapporteur public n'assiste pas au délibéré n'est pas recevable lorsqu'elle est présentée après le délibéré.</ANA>
<ANA ID="9C"> 54-06-01 L'article R. 733-3 du code de justice administrative (CJA) prévoit que :  Sauf demande contraire d'une partie, le rapporteur public assiste au délibéré. Il n'y prend pas part. / La demande prévue à l'alinéa précédent est présentée par écrit. Elle peut l'être à tout moment de la procédure avant le délibéré.,,Une demande tendant à ce que le rapporteur public n'assiste pas au délibéré n'est pas recevable lorsqu'elle est présentée après le délibéré.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
