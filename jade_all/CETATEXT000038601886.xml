<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038601886</ID>
<ANCIEN_ID>JG_L_2019_06_000000418475</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/60/18/CETATEXT000038601886.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème chambre, 12/06/2019, 418475, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-06-12</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>418475</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>CABINET BRIARD</AVOCATS>
<RAPPORTEUR>M. Laurent Domingo</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Karin Ciavaldini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2019:418475.20190612</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une décision du 12 novembre 2018 le Conseil d'Etat statuant au contentieux a prononcé l'admission des conclusions du pourvoi de la société civile immobilière (SCI) les Sailliers de Bourgogne dirigées contre le jugement du 21 décembre 2017 du tribunal administratif de Dijon en tant qu'il s'est prononcé sur la taxe d'enlèvement des ordures ménagères. <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code général des impôts ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Laurent Domingo, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Karin Ciavaldini, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, au Cabinet Briard, avocat de la société les Sailliers de Bourgogne ;<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. La société les Sailliers de Bourgogne, propriétaire de locaux à usage d'habitation situés 80 rue Maréchal Foch au Creusot (Saône-et-Loire), a saisi le tribunal administratif de Dijon d'une demande tendant à la réduction des cotisations de taxe foncière sur les propriétés bâties et de taxe d'enlèvement des ordures ménagères auxquelles elle a été assujettie au titre des années 2014 et 2015 dans les rôles de cette commune. Par une décision du 12 novembre 2018 le Conseil d'Etat, statuant au contentieux, a prononcé l'admission des conclusions du pourvoi qu'elle a formé contre le jugement du 21 décembre 2017 par lequel ce tribunal a rejeté ses demandes, en tant seulement qu'il s'est prononcé sur la taxe d'enlèvement des ordures ménagères.<br/>
<br/>
              2. Il résulte des dispositions de l'article R. 732-1-1 du code de justice administrative, relatives à la dispense du rapporteur public de prononcer des conclusions à l'audience, que les litiges relatifs à la taxe d'enlèvement des ordures ménagères ne figurent pas parmi ceux pour lesquels le président de la formation de jugement ou le magistrat statuant seul peut accorder une telle dispense. <br/>
<br/>
              3. Compte tenu des dispositions rappelées au point 2, le rapporteur public ne pouvait être dispensé de prononcer des conclusions dans le litige introduit par la société requérante relatif à la taxe d'enlèvement des ordures ménagères. Par suite, le jugement du 21 décembre 2017, intervenu à la suite d'une audience qui n'a pas donné lieu au prononcé de conclusions par le rapporteur public, a été rendu à l'issue d'une procédure irrégulière. Il suit de là, sans qu'il soit besoin d'examiner l'autre moyen du pourvoi, que la société les Sailliers de Bourgogne est fondée à demander l'annulation de ce jugement, en tant qu'il s'est prononcé sur la taxe d'enlèvement des ordures ménagères.<br/>
<br/>
              4. Il y a lieu, dans les circonstances, de l'espèce de régler l'affaire au fond par l'application des dispositions de l'article R. 821-2 du code de justice administrative. <br/>
<br/>
              5. Aux termes de l'article 1415 du code général des impôts : " La taxe foncière sur les propriétés bâties, la taxe foncière sur les propriétés non bâties et la taxe d'habitation sont établies pour l'année entière d'après les faits existants au 1er janvier de l'année de l'imposition ". Aux termes de l'article 1389 du même code : " I. - Les contribuables peuvent obtenir le dégrèvement de la taxe foncière en cas de vacance d'une maison normalement destinée à la location ou d'inexploitation d'un immeuble utilisé par le contribuable lui-même à usage commercial ou industriel, à partir du premier jour du mois suivant celui du début de la vacance ou de l'inexploitation jusqu'au dernier jour du mois au cours duquel la vacance ou l'inexploitation a pris fin. / Le dégrèvement est subordonné à la triple condition que la vacance ou l'inexploitation soit indépendante de la volonté du contribuable, qu'elle ait une durée de trois mois au moins et qu'elle affecte soit la totalité de l'immeuble, soit une partie susceptible de location ou d'exploitation séparée. (...) ". L'article 1524 de ce code dispose, s'agissant de la taxe d'enlèvement des ordures ménagères, que : " En cas de vacance d'une durée supérieure à trois mois, il peut être accordé décharge ou réduction de la taxe sur réclamation présentée dans les conditions prévues en pareil cas, en matière de taxe foncière ". Il résulte de ces dispositions que le contribuable qui prétend obtenir le bénéfice de la décharge des cotisations de taxe d'enlèvement des ordures ménagères doit apporter la preuve qu'il a accompli toutes diligences pour mettre l'immeuble en location et démontrer ainsi que la vacance est indépendante de sa volonté. <br/>
<br/>
              6. En premier lieu, au soutien de son argumentation, la société les Sailliers de Bourgogne  produit des annonces publicitaires, une attestation du maire honoraire de la commune de Saint-Symphorien de Marmagne en date du 23 décembre 2013 indiquant que ces annonces décrivent de façon détaillée les biens immobiliers mis en location, une attestation du 21 janvier 2014 du gérant de la SARL Agence Dailcroix indiquant que l'immeuble situé 80 rue Maréchal Foch et 2 rue des Puddleurs répond aux normes de qualité mais que l'état du marché locatif ne permet pas depuis 2007, face aux offres des bailleurs sociaux, de louer les appartements à un prix compétitif, ainsi qu'une attestation en date du 6 février 2014 du gérant de la SARL Sani-Beaune indiquant que les logements de ce même immeuble ne nécessitent pas de travaux d'amélioration. Ces éléments ne suffisent toutefois pas à établir que la société requérante aurait accompli les diligences nécessaires pour mettre ses locaux en locations, notamment en adaptant, pour les années en litige, le niveau des loyers auxquels elle les mettait sur le marché et que, par suite, la situation de vacance serait indépendante de sa volonté, au sens des dispositions précitées du I de l'article 1389 du code général des impôts. <br/>
<br/>
              7. En deuxième lieu, la circonstance que la société aurait obtenu des dégrèvements partiels de taxe foncière au titre des années 2008 et 2009 en application des dispositions du I de l'article 1389 du code général des impôts est, par elle-même, sans incidence sur l'appréciation du caractère involontaire de la vacance des mêmes locaux au cours des années 2014 et 2015. <br/>
<br/>
              8. En troisième lieu, la société requérante ne peut davantage se prévaloir sur le fondement de l'article L. 80B du livre des procédures fiscales, au soutien de sa demande relative aux années 2014 et 2015, de ce que le tribunal administratif de Dijon lui a accordé, par un jugement du 5 avril 2001, la décharge des cotisations de taxe foncière sur les propriétés et de taxe d'enlèvement des ordures ménagères au titre des années 1996, 1997 et 1998 à raison d'une partie des locaux en litige dans la présente instance.<br/>
<br/>
              9. Il résulte de tout ce qui précède que la demande de décharge de cotisations de taxe d'enlèvement des ordures ménagères présentée par la société requérante ne peut qu'être rejetée.<br/>
<br/>
              10. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du 21 décembre 2017 du tribunal administratif de Dijon est annulé en tant qu'il s'est prononcé sur la taxe d'enlèvement des ordures ménagères.<br/>
Article 2 : La demande de la société les Sailliers de Bourgogne relative à la taxe d'enlèvement des ordures ménagères et le surplus de ses conclusions de cassation sont rejetés.<br/>
Article 3 : La présente décision sera notifiée à la société civile immobilière <br/>
les Sailliers de Bourgogne, ainsi qu'au ministre de l'action et des comptes publics.  <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
