<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031861305</ID>
<ANCIEN_ID>JG_L_2015_12_000000386723</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/86/13/CETATEXT000031861305.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère - 6ème SSR, 30/12/2015, 386723, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-12-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>386723</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère - 6ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROUSSEAU, TAPIE ; SCP GASCHIGNARD</AVOCATS>
<RAPPORTEUR>Mme Marie Sirinelli</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Jean Lessi</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:386723.20151230</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              La SARL Ambulances Ateli a demandé au juge des référés du tribunal administratif de Rouen, sur le fondement des dispositions de l'article L. 521-1 du code de justice administrative, d'ordonner la suspension de l'exécution de la décision du 18 novembre 2014 par laquelle la caisse primaire d'assurance maladie (CPAM) de l'Eure a prononcé, pour une durée de six mois à compter du 1er décembre 2014, la résiliation de la convention conclue avec elle. Par une ordonnance n° 1404239 du 16 décembre 2014, le juge des référés du tribunal administratif de Rouen a suspendu l'exécution de cette décision.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 26 décembre 2014 et 9 janvier 2015 au secrétariat du contentieux du Conseil d'Etat, la CPAM de l'Eure demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance du juge des référés du tribunal administratif de Rouen du 16 décembre 2014 ;<br/>
<br/>
              2°) de mettre à la charge de la SARL Ambulances Ateli la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de la sécurité sociale ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Marie Sirinelli, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Jean Lessi, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Rousseau, Tapie, avocat de la caisse primaire d'assurance maladie de l'Eure, et à la SCP Gaschignard, avocat de la SARL Ambulances Ateli ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision " ; que la demande présentée sur ce fondement ne doit pas être manifestement insusceptible de se rattacher à un litige relevant de la compétence de la juridiction administrative ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article L. 322-5 du code de la sécurité sociale, dans sa rédaction applicable à la décision en litige : " (...) Les frais d'un transport effectué par une entreprise de taxi ne peuvent donner lieu à remboursement que si cette entreprise a préalablement conclu une convention avec un organisme local d'assurance maladie. Cette convention, conclue pour une durée au plus égale à cinq ans, conforme à une convention type établie par décision du directeur général de l'Union nationale des caisses d'assurance maladie après avis des organisations professionnelles nationales les plus représentatives du secteur, détermine, pour les prestations de transport par taxi, les tarifs de responsabilité qui ne peuvent excéder les tarifs des courses de taxis résultant de la réglementation des prix applicable à ce secteur et fixe les conditions dans lesquelles l'assuré peut être dispensé de l'avance des frais. Elle peut également prévoir la possibilité de subordonner le conventionnement à une durée d'existence préalable de l'autorisation de stationnement " ; que la convention-type mentionnée par ces dispositions a été arrêtée par décision du 8 septembre 2008 du directeur général de l'Union nationale des caisses d'assurance maladie ; <br/>
<br/>
              3. Considérant qu'il ressort des pièces du dossier soumis au juge des référés du tribunal administratif de Rouen que, le 18 novembre 2014, la caisse primaire d'assurance maladie de l'Eure a décidé de résilier, pour une durée de six mois à compter du 1er décembre 2014, la convention locale conclue avec la SARL Ambulances Ateli pour son activité de transport sanitaire par taxi, sur le fondement de l'article L. 322-5 précité du code de la sécurité sociale, au motif que cette société avait méconnu les obligations prévues à l'article 2 de cette convention, relatif aux prestations susceptibles d'être prises en charge, à son article 4, relatif à la déclaration des conducteurs, au 1 de l'article 6 de cette convention, relatif à la prescription médicale du transport, et, enfin, à ses annexes n° 4 et 5, relatives à la dispense d'avance des frais et à la facturation ; que, saisi par la société sur le fondement de l'article L. 521-1 précité du code de justice administrative, le juge des référés du tribunal administratif de Rouen a, par une ordonnance du 10 décembre 2014, suspendu l'exécution de la décision du 18 novembre 2014 ; que la caisse primaire d'assurance maladie de l'Eure se pourvoit en cassation contre cette ordonnance ;<br/>
<br/>
              4. Considérant qu'il ressort des pièces soumises au juge des référés que, par sa décision du 18 novembre 2014, la caisse primaire d'assurance maladie de l'Eure a fait application de l'article 9 de la convention locale passée avec la SARL Ambulances Ateli, qui prévoit que la résiliation de cette convention peut être prononcée " dans le cas où l'entreprise de taxi ne respecte pas les engagements, déterminés par la présente convention, notamment ceux figurant aux articles 2, 3, 4, 6 et 8 " ; qu'ainsi, cette décision, qui ne se rattache pas à l'exercice de prérogatives de puissance publique, est intervenue en application des stipulations de cette convention de droit privé déterminant les conditions de sa mise en oeuvre ; que sa contestation ne saurait, dès lors, être regardée comme relevant, par nature, d'un autre contentieux que celui des " différends auxquels donne lieu l'application des législations et réglementations de sécurité sociale ", pour lesquels l'article L. 142-1 du code de la sécurité sociale donne compétence aux juridictions du contentieux général de la sécurité sociale ; que la demande tendant à la suspension de son exécution était, en conséquence, manifestement insusceptible de se rattacher à un litige relevant de la compétence de la juridiction administrative ; que, par suite, sans qu'il soit besoin d'examiner les moyens soulevés par la caisse primaire d'assurance maladie de l'Eure, l'ordonnance du juge des référés du tribunal administratif de Rouen doit être annulée ;<br/>
<br/>
              5. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de statuer sur la demande de suspension présentée par la SARL Ambulances Ateli en application des dispositions de l'article L. 821-2 du code de justice administrative ; <br/>
<br/>
              6. Considérant qu'ainsi qu'il a été dit ci-dessus, la demande de suspension présentée par la société Ambulances Ateli devant le juge des référés du tribunal administratif de Rouen est manifestement insusceptible de se rattacher à un litige relevant de la compétence de la juridiction administrative ; qu'elle ne peut, par suite, qu'être rejetée ;<br/>
<br/>
              7. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de la caisse primaire d'assurance maladie de l'Eure, qui n'est pas, dans la présente instance, la partie perdante ; qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par cette dernière au titre des mêmes dispositions ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'ordonnance du juge des référés du tribunal administratif de Rouen du 16 décembre 2014 est annulée.<br/>
Article 2 : La demande présentée par la SARL Ambulances Ateli devant le juge des référés du tribunal administratif de Rouen est rejetée.<br/>
Article 3 : Les conclusions de la SARL Ambulances Ateli et de la caisse primaire d'assurance maladie de l'Eure présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée à la caisse primaire d'assurance maladie de l'Eure et à la SARL Ambulances Ateli. <br/>
Copie en sera adressée à la ministre des affaires sociales, de la santé et des droits des femmes.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
