<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044293058</ID>
<ANCIEN_ID>JG_L_2021_11_000000451597</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/29/30/CETATEXT000044293058.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème chambre, 04/11/2021, 451597, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-11-04</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>451597</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Clément Tonon</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Sophie Roussel</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:451597.20211104</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, enregistrée le 12 avril 2021 au secrétariat du contentieux du Conseil d'Etat, M. E... C... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret du 25 janvier 2021 par lequel le Premier ministre a rapporté le décret du 12 avril 2018 en ce qu'il lui avait accordé la nationalité française ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code civil ;<br/>
              - le décret n° 93-1362 du 30 décembre 1993 ;<br/>
              - le code justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Clément Tonon, auditeur,<br/>
<br/>
              - les conclusions de Mme Sophie Roussel, rapporteure publique,<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1.	Aux termes de l'article 27-2 du code civil, dans sa rédaction applicable au présent litige : " Les décrets portant acquisition, naturalisation ou réintégration peuvent être rapportés sur avis conforme du Conseil d'Etat dans le délai d'un an à compter de leur publication au Journal officiel si le requérant ne satisfait pas aux conditions légales ; si la décision a été obtenue par mensonge ou fraude, ces décrets peuvent être rapportés dans le délai de deux ans à partir de la découverte de la fraude ".<br/>
<br/>
              2.	Il ressort des pièces du dossier que M. C..., ressortissant camerounais, a déposé une demande de naturalisation, le 5 septembre 2016, par laquelle il a indiqué être célibataire et sans enfant et s'est engagé sur l'honneur à signaler tout changement qui viendrait à survenir dans sa situation personnelle et familiale. Au vu de ses déclarations, il a été naturalisé par décret du 12 avril 2018, publié au Journal Officiel de la République Française du 14 avril 2018. Toutefois, par courriel reçu le 1er février 2019, le préfet du Val d'Oise a informé le ministre chargé des naturalisations de ce que M. C... était père d'un enfant, né au Cameroun en 2011. Par décret du 25 janvier 2021, le Premier ministre a rapporté le décret du 5 septembre 2016 de naturalisation de M. C... au motif qu'il avait été pris au vu d'informations mensongères délivrées par l'intéressé quant à sa situation familiale. M. C... demande l'annulation pour excès de pouvoir de ce décret.<br/>
<br/>
              3.	En premier lieu, le décret attaqué énonce les considérations de droit et de fait sur lesquelles il se fonde et est, dès lors, suffisamment motivé. <br/>
<br/>
              4.	En second lieu, l'article 21-16 du code civil dispose que " Nul ne peut être naturalisé s'il n'a en France sa résidence au moment de la signature du décret de naturalisation ". Il résulte de ces dispositions que la demande de naturalisation n'est pas recevable lorsque l'intéressé n'a pas fixé en France de manière durable le centre de ses intérêts. Pour apprécier si cette condition est remplie, l'autorité administrative peut notamment prendre en compte, sous le contrôle du juge de l'excès de pouvoir, la situation personnelle et familiale en France de l'intéressé à la date du décret lui accordant la nationalité française. Par suite, alors même qu'il remplirait les autres conditions requises pour l'obtention de la nationalité française, la circonstance que l'intéressé soit le père d'un enfant, né à Yaoundé (Cameroun) et résidant habituellement à l'étranger, était de nature à modifier l'appréciation qui a été portée par l'autorité administrative sur la fixation du centre de ses intérêts.<br/>
<br/>
              5.	Il ressort des pièces du dossier que, si M. C... soutient qu'il n'a découvert l'existence de son fils que postérieurement au décret du 12 avril 2018 lui accordant la nationalité française, il ressort de l'acte dressé par l'officier du centre d'état civil de Ndamvout Yaoundé IV que l'intéressé a déclaré la naissance de son fils H... B... C... le 10 janvier 2011, soit antérieurement à sa demande de naturalisation. Si M. C... soutient qu'à cette date, il n'était pas présent au Cameroun et que l'officier d'état civil aurait avancé de lui-même les effets de cette reconnaissance au 10 janvier 2011, la simple copie de son passeport ne saurait établir qu'il n'y était effectivement pas. M. C... n'a pas davantage informé les services chargés de sa demande lors de son entretien d'assimilation de la réalité de sa situation familiale lorsque lui ont été demandés les liens qui le rattachent encore à son pays d'origine et ne fait par ailleurs état d'aucune circonstance qui l'aurait mis dans l'impossibilité d'exposer sa situation familiale au service chargé de l'instruction de son dossier avant l'intervention du décret lui accordant la nationalité française. En outre, la circonstance que M. C... ait sollicité la régularisation de la situation de son enfant auprès des services du ministère de l'intérieur, postérieurement à l'intervention du décret lui accordant la nationalité française, n'est pas de nature à remettre en cause l'appréciation du caractère frauduleux des déclarations faites en vue d'obtenir la nationalité française. Enfin, l'intéressé, qui maîtrise la langue française, ainsi qu'il ressort du compte-rendu d'assimilation du 21 juin 2017, ne pouvait se méprendre ni sur la teneur des indications devant être portées à la connaissance de l'administration chargée d'instruire sa demande, ni sur la portée de la déclaration sur l'honneur qu'il a signée. Dans ces conditions, M. C... doit être regardé comme ayant volontairement dissimulé sa situation familiale. Par suite, en rapportant sa naturalisation, dans le délai de deux ans à compter de la découverte de la fraude, le Premier ministre n'a pas fait une inexacte application des dispositions de l'article 27-2 du code civil. <br/>
<br/>
              6.	Il résulte de ce qui précède que M. C... n'est pas fondé à demander l'annulation pour excès de pouvoir du décret du 25 janvier 2021 par lequel le Premier ministre a rapporté le décret du 12 avril 2018. Ses conclusions présentées au titre de l'article L. 761-1, ne peuvent, en conséquence, qu'être rejetées.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : La requête de M. C... est rejetée.<br/>
Article 2 : La présente décision sera notifiée à M. E... C... et au ministre de l'intérieur.<br/>
<br/>
              Délibéré à l'issue de la séance du 14 octobre 2021 où siégeaient : Mme A... G..., assesseure, présidant ; M. Jean-Yves Ollier, conseiller d'Etat et M. Clément Tonon, auditeur-rapporteur. <br/>
<br/>
              Rendu le 4 novembre 2021.<br/>
<br/>
<br/>
                 La Présidente : <br/>
                 Signé : Mme A... G...<br/>
 		Le rapporteur : <br/>
      Signé : M. Clément Tonon<br/>
                 La secrétaire :<br/>
                 Signé : Mme F... D...<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
