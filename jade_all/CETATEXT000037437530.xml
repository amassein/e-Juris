<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037437530</ID>
<ANCIEN_ID>JG_L_2018_09_000000409063</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/43/75/CETATEXT000037437530.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème chambre, 26/09/2018, 409063, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-09-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>409063</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>LE PRADO</AVOCATS>
<RAPPORTEUR>M. Cyrille Beaufils</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Julie Burguburu</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:409063.20180926</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme B...A...a demandé au tribunal administratif de Paris d'annuler pour excès de pouvoir l'arrêté du 18 novembre 2015 par laquelle le  préfet de police a refusé de lui délivrer un titre de séjour, lui a fait obligation de quitter le territoire français et a fixé le pays de destination. Par un jugement n° 1605370/6-3 du 1er septembre 2016, le tribunal administratif a rejeté sa  demande.<br/>
<br/>
              Par un arrêt n° 16PA03015 du 7 mars 2017, la cour administrative d'appel de Paris  a rejeté l'appel formé par Mme A...contre ce jugement.  <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 20 mars et 20 juin 2017 au secrétariat du contentieux du Conseil d'Etat, Mme A...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu : <br/>
              - la convention internationale relative aux droits de l'enfant signée à New-York le 26 janvier 1990 ;<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Cyrille Beaufils, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Julie Burguburu, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Le Prado, avocat de Mme A...;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que MmeA..., de nationalité camerounaise, est entrée sur le territoire français en décembre 2011 pour y accompagner deux de ses enfants, alors mineurs, atteints d'une pathologie neurologique chronique grave. Elle a bénéficié au cours de l'année 2012 d'une autorisation provisoire de séjour en qualité d'accompagnant d'enfants mineurs malades, en application de l'article L. 311-12 du code de l'entrée et du séjour des étrangers et du droit d'asile, dont elle a sollicité le renouvellement. Par un arrêté du 18 novembre 2015, le préfet de police a refusé de lui délivrer un titre de séjour, lui a fait obligation de quitter le territoire français et a fixé le pays de destination. Par un jugement du 1er septembre 2016, le tribunal administratif de Paris a rejeté la demande de Mme A...tendant à l'annulation de cet arrêté. Par un arrêt du 7 mars 2017, contre lequel Mme A...se pourvoit en cassation, la cour administrative d'appel de Paris a rejeté sa requête tendant à l'annulation de ce jugement. <br/>
<br/>
              2. Pour rejeter l'appel de MmeA..., la cour s'est fondée sur la circonstance que les fils de la requérante étaient devenus majeurs à la date de l'arrêté attaqué et qu'ainsi l'intéressée ne pouvait utilement se prévaloir de la méconnaissance des dispositions de l'article L. 311-12 du code de l'entrée et du séjour des étrangers et du droit d'asile, qui ne concernent que les parents accompagnants d'enfants mineurs. Il ressort toutefois des pièces de la procédure qu'en excipant dans sa requête de la jurisprudence relative à l'atteinte à la vie privée et familiale l'intéressée devait être regardée comme soulevant un moyen tiré de ce que la décision attaquée portait une telle atteinte à sa situation personnelle. En s'abstenant de répondre à ce moyen, qui n'était pas inopérant, la cour a insuffisamment motivé son arrêt. Mme A...est, par suite, fondée, pour ce motif, à demander l'annulation de l'arrêt qu'elle attaque. <br/>
<br/>
              3. Il y a lieu, en application de l'article L. 821-2 du code de justice administrative, de régler l'affaire au fond. <br/>
<br/>
              4. Il ressort des pièces du dossier que si Mme A...est arrivée en France en 2011 et si son conjoint réside au Cameroun, où elle a conservé des attaches, ses deux fils souffrent d'une déficience intellectuelle pour laquelle ils sont suivis en milieu hospitalier spécialisé, cette affection étant à l'origine d'un taux d'incapacité de 80 %. A la date de la décision attaquée, l'un des deux enfants était déjà placé, par jugement du 26 juin 2013, sous la tutelle de MmeA.... Postérieurement à la décision attaquée, son deuxième enfant a d'ailleurs été également placé sous sa tutelle, tandis que, par deux jugements du 8 mars 2017 devenus définitifs, le tribunal administratif de Paris a, compte tenu de leur état de santé, annulé les refus de délivrance de titres de séjour qui avaient été opposés aux deux fils de Mme A...et enjoint au préfet de police de leur délivrer ces titres. Il résulte de ces éléments que, dans les circonstances de l'espèce, la décision attaquée a porté au droit de l'intéressée au respect de sa vie privée et familiale garanti par l'article 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales une atteinte disproportionnée. Par suite, et sans qu'il soit besoin d'examiner les autres moyens de la requête, Mme A...est fondée à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Paris a rejeté sa demande tendant à l'annulation de l'arrêté du 18 novembre 2015. <br/>
<br/>
              5. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 3 000 euros qui sera versée à Mme A...au titre des dispositions de l'article L. 761-1 du code de justice administrative et d'enjoindre au préfet de police de réexaminer la demande de l'intéressée tendant à la délivrance d'un titre de séjour dans un délai d'un mois à compter de la présente décision. <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 7 mars 2016 de la cour administrative d'appel de Paris, le jugement du 1er septembre 2016 du tribunal administratif de Paris et l'arrêté du 18 novembre 2015 du préfet de police sont annulés. <br/>
Article 2 : Il est enjoint au préfet de police de procéder au réexamen de la demande de Mme A... tendant à la délivrance d'un titre de séjour dans un délai d'un mois à compter de la présente décision.<br/>
 Article 3 : L'Etat versera à Mme A...une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée à Mme B...A...et au ministre d'Etat, ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
