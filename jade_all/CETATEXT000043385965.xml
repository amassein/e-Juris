<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043385965</ID>
<ANCIEN_ID>JG_L_2021_04_000000430500</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/38/59/CETATEXT000043385965.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème - 5ème chambres réunies, 15/04/2021, 430500</TITRE>
<DATE_DEC>2021-04-15</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>430500</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème - 5ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MARLANGE, DE LA BURGADE ; SCP ROCHETEAU, UZAN-SARANO</AVOCATS>
<RAPPORTEUR>Mme Carine Chevrier</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Stéphane Hoynck</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:430500.20210415</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              L'association Société pour la protection des paysages et de l'esthétique de la France (SPPEF), M. D... N..., M. L... J..., M. et Mme G... et Gwénola H..., M. P... M..., Mme B... A..., M. O... H..., Mme E... F... et M. et Mme I... et Yolande Métairie, ainsi que l'association Bretagne vivante - SEPNB ont demandé au tribunal administratif de Rennes d'annuler l'arrêté du 4 février 2015 par lequel le préfet du Morbihan a accordé à la société Les Moulins du Lohan, en application de l'article L. 411-2 du code de l'environnement, l'autorisation de déroger aux interdictions mentionnées à l'article L. 411-1 du même code relatives à la capture, l'enlèvement, le transport, la perturbation intentionnelle, la destruction de spécimens d'espèces protégées et la destruction d'habitats d'espèces protégées, pendant la durée des travaux et de l'exploitation d'un parc éolien sur le territoire de la commune des Forges. Par un jugement nos 1500727, 1501595 du 7 juillet 2017, le tribunal administratif a annulé cet arrêté. <br/>
<br/>
              Par un arrêt nos 17NT02791, 17NT02794 du 5 mars 2019, la cour administrative d'appel de Nantes a, sur les appels formés par la société Les Moulins du Lohan et par le ministre de la transition écologique et solidaire, annulé ce jugement et rejeté les demandes tendant à l'annulation de l'arrêté du 4 février 2015.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 6 mai et 31 juillet 2019 et le 23 septembre 2020 au secrétariat du contentieux du Conseil d'Etat, l'association Société pour la protection des paysages et de l'esthétique de la France, M. L... J..., M. et Mme G... et Gwénola H..., M. P... M..., Mme B... A..., M. O... H..., Mme E... F..., et M. et Mme I... et Yolande Métairie demandent au Conseil d'Etat :<br/>
<br/>
              1°) de donner acte à Mme E... F... de son désistement ;<br/>
<br/>
              2°) d'annuler cet arrêt ;<br/>
<br/>
              3°) réglant l'affaire au fond, de rejeter les appels de la société Les Moulins du Lohan et du ministre de la transition écologique et solidaire ;<br/>
<br/>
              4°) de mettre à la charge solidaire de l'Etat et de la société Les Moulins du Lohan la somme de 3 500 euros en application de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la directive 92/43/CEE du Conseil du 21 mai 1992 ;<br/>
              - la directive 97/62/CEE du Conseil du 27 octobre 1997 ;<br/>
              - la directive 2009/28/CE du Parlement européen et du Conseil du 23 avril 2009 ;<br/>
              - le code de l'énergie ;<br/>
              - le code de l'environnement ;<br/>
              - la loi n°2009-967 du 3 août 2009 ;<br/>
              - l'arrêté du 19 février 2007 fixant les conditions de demande et d'instruction des dérogations définies au 4° de l'article L. 411-2 du code de l'environnement portant sur des espèces de faune et de flore sauvages protégées ;<br/>
              - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme C... K..., conseillère d'Etat,  <br/>
<br/>
              - les conclusions de M. Stéphane Hoynck, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Marlange, de la Burgade, avocat de l'association Société pour la protection des paysages et de l'esthétique de la France et autres et à la SCP Rocheteau, Uzan-Sarano, avocat de la société Les Moulins du Lohan ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 24 mars 2021, présentée par la société Les Moulins du Lohan ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que, par un arrêté du 4 février 2015, le préfet du Morbihan a autorisé la société Les Moulins du Lohan à déroger aux interdictions mentionnées à l'article L. 411-1 du code de l'environnement pendant la durée des travaux et de l'exploitation d'un parc éolien sur le territoire de la commune des Forges. Par un jugement du 7 juillet 2017, le tribunal administratif de Rennes a, sur la demande de l'association Société pour la protection des paysages et de l'esthétique de la France et autres, annulé cet arrêté. Par un arrêt du 5 mars 2019, contre lequel l'association Société pour la protection des paysages et de l'esthétique de la France et autres se pourvoient en cassation, la cour administrative d'appel de Nantes a, sur les appels formés par la société pétitionnaire et le ministre de la transition écologique et solidaire, annulé ce jugement et rejeté les conclusions tendant à l'annulation de l'arrêté du 4 février 2015. <br/>
<br/>
              Sur le désistement : <br/>
<br/>
              2. Le désistement de Mme F... est pur et simple. Rien ne s'oppose à ce qu'il en soit donné acte. <br/>
<br/>
              Sur l'arrêt attaqué : <br/>
<br/>
              3. En premier lieu, l'article L. 411-1 du code de l'environnement prévoit, lorsque les nécessités de la préservation du patrimoine naturel justifient la conservation d'espèces animales non domestiques, l'interdiction de " 1° La destruction ou l'enlèvement des oeufs ou des nids, la mutilation, la destruction, la capture ou l'enlèvement, la perturbation intentionnelle, la naturalisation d'animaux de ces espèces ou, qu'ils soient vivants ou morts, leur transport, leur colportage, leur utilisation, leur détention, leur mise en vente, leur vente ou leur achat / 2° La destruction, la coupe, la mutilation, l'arrachage, la cueillette ou l'enlèvement de végétaux de ces espèces, de leurs fructifications ou de toute autre forme prise par ces espèces au cours de leur cycle biologique, leur transport, leur colportage, leur utilisation, leur mise en vente, leur vente ou leur achat, la détention de spécimens prélevés dans le milieu naturel ; / 3° La destruction, l'altération ou la dégradation de ces habitats naturels ou de ces habitats d'espèces (...) ". D'autre part, le I de l'article L. 411-2 du même code renvoie à un décret en Conseil d'Etat la détermination des conditions dans lesquelles sont fixées, notamment : " 4° La délivrance de dérogations aux interdictions mentionnées aux 1°, 2° et 3° de l'article L. 411-1, à condition qu'il n'existe pas d'autre solution satisfaisante, pouvant être évaluée par une tierce expertise menée, à la demande de l'autorité compétente, par un organisme extérieur choisi en accord avec elle, aux frais du pétitionnaire, et que la dérogation ne nuise pas au maintien, dans un état de conservation favorable, des populations des espèces concernées dans leur aire de répartition naturelle : (...) / c) Dans l'intérêt de la santé et de la sécurité publiques ou pour d'autres raisons impératives d'intérêt public majeur, y compris de nature sociale ou économique, et pour des motifs qui comporteraient des conséquences bénéfiques primordiales pour l'environnement ; (...) ".<br/>
<br/>
              4. Il résulte de ces dispositions qu'un projet de travaux, d'aménagement ou de construction d'une personne publique ou privée susceptible d'affecter la conservation d'espèces animales ou végétales protégées et de leur habitat ne peut être autorisé, à titre dérogatoire, que s'il répond, par sa nature et compte tenu des intérêts économiques et sociaux en jeu, à une raison impérative d'intérêt public majeur. En présence d'un tel intérêt, le projet ne peut cependant être autorisé, eu égard aux atteintes portées aux espèces protégées appréciées en tenant compte des mesures de réduction et de compensation prévues, que si, d'une part, il n'existe pas d'autre solution satisfaisante et, d'autre part, cette dérogation ne nuit pas au maintien, dans un état de conservation favorable, des populations des espèces concernées dans leur aire de répartition naturelle.<br/>
<br/>
              5. Pour apprécier si le projet litigieux répond à une raison impérative d'intérêt public majeur au sens des dispositions précédemment citées du code de l'environnement, la cour administrative d'appel, après avoir souverainement constaté que le projet consiste en la réalisation d'un parc éolien composé de seize ou dix-sept éoliennes d'une puissance totale de plus de 51 mégawatts permettant l'approvisionnement en électricité de plus de 50 000 personnes, a retenu que ce projet s'inscrit dans l'objectif, fixé par la loi du 3 août 2009 puis par l'article L. 100-4 du code de l'énergie, visant à porter la part des énergies renouvelables à 23 % de la consommation finale brute d'énergie en 2020 et à 32 % de cette consommation en 2030, conformément à l'objectif de la directive 2009/28/CE du Parlement européen et du Conseil du 23 avril 2009 relative à la promotion de l'utilisation de l'énergie produite à partir de sources renouvelables qui a imposé à la France un relèvement de la part d'énergie produite à partir de sources renouvelables de 10,3 % en 2005 à 23 % en 2020. La cour administrative d'appel a, en outre, relevé le caractère fragile de l'approvisionnement électrique de la Bretagne, résultant d'une faible production locale ne couvrant que 8 % des besoins de la région, et retenu que le projet s'inscrit dans l'objectif du " pacte électrique ", signé le 14 décembre 2010 entre l'Etat, la région Bretagne, l'Agence de l'environnement et de la maîtrise de l'énergie (ADEME), le réseau de transport de l'électricité (RTE) et l'agence nationale de l'habitat (ANAH), prévoyant d'accroître la production d'électricité renouvelable dans cette région. En jugeant que ce projet de parc éolien répond, en dépit de son caractère privé, à une raison impérative d'intérêt public majeur, la cour administrative d'appel a exactement qualifié les faits de l'espèce.<br/>
<br/>
              6. Pour juger, ensuite, qu'il n'existait pas d'autre solution satisfaisante, la cour a relevé que la société porteuse du projet litigieux, après avoir envisagé plusieurs types d'énergies renouvelables, a retenu la forêt de Lanouée parce qu'elle permet l'implantation d'un parc éolien à plus d'un kilomètre des habitations, situation particulièrement rare en Bretagne où l'on observe un étalement de l'urbanisation et un habitat dispersé, qu'elle ne comporte ni zone Natura 2000, ni espace boisé classé, ni zones humides et qu'elle dispose d'un réseau important de voies forestières et de capacités de raccordement. La cour a également relevé qu'il n'était pas sérieusement contesté que la société avait étudié plusieurs implantations possibles pour le parc éolien avant de retenir comme emplacement du projet la zone sud-est de la forêt de Lanouée, qui présente une moindre sensibilité sur le plan paysager et fait partie de la zone de développement éolien de la communauté de communes de Josselin Communauté approuvée par un arrêté préfectoral du 12 mars 2012, et estimé qu'il n'était pas possible d'implanter le parc éolien en lisière de la forêt. La cour a, enfin, relevé qu'aucune pièce du dossier ne mettait en évidence une solution alternative qui aurait été ignorée. En jugeant, au vu de ces éléments, que le préfet n'avait pas commis d'erreur d'appréciation en estimant qu'il n'existait pas d'autre solution satisfaisante, la cour administrative d'appel s'est livrée à une appréciation souveraine des faits de l'espèce, exempte de dénaturation, et n'a pas commis d'erreur de droit.<br/>
<br/>
              7. En second lieu, aux termes du premier alinéa de l'article 1er de l'arrêté du ministre de l'agriculture et de la pêche et de la ministre de l'écologie et du développement durable du 19 février 2007 fixant les conditions de demande et d'instruction des dérogations définies au 4° de l'article L. 411-2 du code de l'environnement portant sur des espèces de faune et de flore sauvages protégées : " Les dérogations définies au 4° de l'article L. 411-2 du code de l'environnement portant sur des espèces de faune et de flore sauvages protégées sont, sauf exceptions mentionnées aux articles 5 et 6, délivrées par le préfet du département du lieu de l'opération pour laquelle la dérogation est demandée (...) ". Aux termes de l'article 2 du même arrêté : " La demande de dérogation (...) comprend : / (...) La description, en fonction de la nature de l'opération projetée : / - du programme d'activité dans lequel s'inscrit la demande, de sa finalité et de son objectif ; / - des espèces (nom scientifique et nom commun) concernées ; /  du nombre et du sexe des spécimens de chacune des espèces faisant l'objet de la demande /   (...) s'il y a lieu, des mesures d'atténuation ou de compensation mises en oeuvre, ayant des conséquences bénéfiques pour les espèces concernées ;(...) ". Aux termes de son article 4 : " La décision précise : / (...) / En cas d'octroi d'une dérogation et, en tant que de besoin, en fonction de la nature de l'opération projetée, les conditions de celle-ci, notamment :/ (...) / -nombre et sexe des spécimens sur lesquels porte la dérogation / (...) ". <br/>
<br/>
              8. Après avoir relevé, par une appréciation souveraine exempte de dénaturation, que le dossier de la demande de dérogation comportait la liste exhaustive et l'indication précise des espèces concernées par l'opération, en distinguant les amphibiens et reptiles, les oiseaux, les mammifères terrestres et les chauves-souris et en indiquant pour chaque catégorie le nombre d'espèces observées, et que le dossier exposait en détail les mesures de réduction, de compensation et d'accompagnement envisagées, la cour administrative d'appel a jugé que le contenu du dossier de demande de dérogation satisfaisait, compte tenu de l'objet de la demande, aux dispositions des articles 1er et 4 de l'arrêté du 19 février 2007, alors même qu'il ne précisait pas le nombre et le sexe des spécimens concernés. En statuant ainsi, la cour administrative d'appel, qui a suffisamment motivé sa décision sur ce point, n'a pas, eu égard à la nature de l'opération pour laquelle la dérogation était sollicitée, commis d'erreur de droit.<br/>
<br/>
              9. Il résulte de tout ce qui précède que l'association Société pour la protection des paysages et de l'esthétique de la France et autres ne sont pas fondés à demander l'annulation de l'arrêt qu'ils attaquent. <br/>
<br/>
              10. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise, à ce titre, à la charge de la société Les Moulins du Lohan qui n'est pas, dans la présente instance, la partie perdante. Il y a lieu, en revanche, dans les circonstances de l'espèce, de mettre à la charge de l'association Société pour la protection des paysages et de l'esthétique de la France et autres la somme de 1 000 euros à verser à la société Les Moulins du Lohan au titre de ces mêmes dispositions.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Il est donné acte du désistement du pourvoi de Mme E... F.... <br/>
Article 2 : Le pourvoi de l'association Société pour la protection des paysages et de la l'esthétique de la France et autres est rejeté.<br/>
Article 3 : L'association Société pour la protection des paysages et de l'esthétique de la France et autres verseront à la société Les Moulins du Lohan la somme de 1 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à l'association Société pour la protection des paysages et de l'esthétique de la France, représentante unique désignée pour l'ensemble des requérants, à la société Les Moulins du Lohan et à la ministre de la transition écologique. <br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">44-045-01 NATURE ET ENVIRONNEMENT. - PROTECTION DES ESPÈCES ANIMALES ET VÉGÉTALES - OCTROI D'UNE DÉROGATION POUR UN PROJET D'AMÉNAGEMENT OU DE CONSTRUCTION (ART. L. 411-2 DU CODE DE L'ENVIRONNEMENT) - CONTRÔLE DU JUGE DE CASSATION SUR L'ABSENCE D'AUTRE SOLUTION SATISFAISANTE - DÉNATURATION [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-08-02-02-01-04 PROCÉDURE. VOIES DE RECOURS. CASSATION. CONTRÔLE DU JUGE DE CASSATION. BIEN-FONDÉ. DÉNATURATION. - ABSENCE D'AUTRE SOLUTION SATISFAISANTE QUE LE PROJET ENVISAGÉ POUR JUSTIFIER UNE DÉROGATION AUX INTERDICTIONS VISANT À ASSURER LA CONSERVATION D'ESPÈCES ANIMALES OU VÉGÉTALES PROTÉGÉES ET DE LEURS HABITATS (ART. L. 411-2 DU CODE DE L'ENVIRONNEMENT) [RJ1].
</SCT>
<ANA ID="9A"> 44-045-01 Le juge de cassation laisse à l'appréciation souveraine des juges du fond, sous réserve de dénaturation, le point de savoir s'il n'existe pas de solution satisfaisante autre que le projet envisagé pour répondre à une raison impérative d'intérêt public majeur de nature à justifier, en application de l'article L. 411-2 du code de l'environnement, une dérogation aux d'interdictions visant à assurer la conservation d'espèces animales ou végétales protégées et de leurs habitats.</ANA>
<ANA ID="9B"> 54-08-02-02-01-04 Le juge de cassation laisse à l'appréciation souveraine des juges du fond, sous réserve de dénaturation, le point de savoir s'il n'existe pas de solution satisfaisante autre que le projet envisagé pour répondre à une raison impérative d'intérêt public majeur de nature à justifier, en application de l'article L. 411-2 du code de l'environnement, une dérogation aux d'interdictions visant à assurer la conservation d'espèces animales ou végétales protégées et de leurs habitats.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Comp., s'agissant du contrôle du juge de cassation sur l'existence d'une raison impérative d'intérêt public majeur, CE, 24 juillet 2019, Société PCE et autres, n° 414353, T. pp. 854-958-961.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
