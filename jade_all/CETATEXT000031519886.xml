<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031519886</ID>
<ANCIEN_ID>JG_L_2015_11_000000388184</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/51/98/CETATEXT000031519886.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère SSJS, 23/11/2015, 388184, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-11-23</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>388184</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère SSJS</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROUSSEAU, TAPIE ; SCP BOULLEZ</AVOCATS>
<RAPPORTEUR>M. Marc Thoumelou</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Jean Lessi</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2015:388184.20151123</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
 Procédure contentieuse antérieure <br/>
              M. A...B...a demandé au tribunal administratif de Dijon :<br/>
              - d'annuler la décision du préfet de l'Yonne du 5 juillet 2012 prononçant la suppression de son revenu de remplacement à compter du 13 septembre 2009 ainsi que la décision de rejet de son recours gracieux ;<br/>
              - d'enjoindre au préfet de l'Yonne de faire procéder au versement de ses allocations chômage du 1er juin 2011 inclus au 14 juin 2012 ;<br/>
              - subsidiairement, de condamner l'Etat à lui verser la somme de 30 513,42 euros à titre d'indemnité au titre du préjudice qu'il estime avoir subi.<br/>
<br/>
              Par un jugement n° 1300179 du 4 décembre 2014, le tribunal administratif de Dijon a rejeté sa demande.<br/>
 Procédure devant le Conseil d'Etat<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 23 février et 26 mai 2015 au secrétariat du contentieux du Conseil d'Etat, M. B...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler le jugement du tribunal administratif de Dijon du 4 décembre 2014 ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code du travail ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Marc Thoumelou, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Jean Lessi, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Rousseau, Tapie, avocat de M.B..., et à la SCP Boullez, avocat du Pôle emploi Bourgogne ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article R. 611-1 du code de justice administrative : " La requête et les mémoires, ainsi que les pièces produites par les parties, sont déposés ou adressés au greffe. / La requête, le mémoire complémentaire annoncé dans la requête et le premier mémoire de chaque défendeur sont communiqués aux parties avec les pièces jointes dans les conditions prévues aux articles R. 611-3, R. 611-5 et R. 611-6. / Les répliques, autres mémoires et pièces sont communiqués s'ils contiennent des éléments nouveaux ". L'article R. 613-2 de ce code dispose : " Si le président de la formation de jugement n'a pas pris une ordonnance de clôture, l'instruction est close trois jours francs avant la date de l'audience indiquée dans l'avis d'audience prévu à l'article R. 711-2. Cet avis le mentionne (...) ". Selon l'article R. 613-3 du même code : " Les mémoires produits après la clôture de l'instruction ne donnent pas lieu à communication et ne sont pas examinés par la juridiction. / Si les parties présentent avant la clôture de l'instruction des conclusions nouvelles ou des moyens nouveaux, la juridiction ne peut les adopter sans ordonner un supplément d'instruction ". Enfin, l'article R. 613-4 du même code dispose : " Le président de la formation de jugement peut rouvrir l'instruction par une décision qui n'est pas motivée et ne peut faire l'objet d'aucun recours. Cette décision est notifiée dans les mêmes formes que l'ordonnance de clôture (...) ".<br/>
<br/>
              2. Il résulte de ces dispositions que, lorsqu'il décide de rouvrir l'instruction après sa clôture pour verser au contradictoire un mémoire qui a été produit par les parties et que le délai qui reste à courir jusqu'à la date de l'audience ne permet plus l'intervention de la clôture automatique trois jours francs avant l'audience prévue par l'article R. 613-2 du code de justice administrative mentionné ci-dessus, il appartient au président de la formation de jugement du tribunal administratif ou de la cour administrative d'appel, qui, par ailleurs, peut toujours, s'il l'estime nécessaire, fixer une nouvelle date d'audience, de clore l'instruction ainsi rouverte. Si l'article R. 772-9 du code de justice administrative prévoit désormais, pour les requêtes relatives aux droits des travailleurs privés d'emploi, que l'instruction est close soit après que les parties ou leurs mandataires ont formulé leurs observations orales, soit, si ces parties sont absentes ou ne sont pas représentées, après appel de leur affaire à l'audience, ces dispositions s'appliquent, en vertu du III de l'article 16 du décret du 13 août 2013, aux requêtes enregistrées à compter du 1er janvier 2014. Elles n'étaient donc pas applicables au jugement de la requête de M.B..., enregistrée le 23 janvier 2013.<br/>
<br/>
              3. Il ressort des mentions du jugement attaqué ainsi que des pièces du dossier de la procédure devant le tribunal administratif de Dijon que, le 18 novembre 2014, soit moins de trois jours francs avant l'audience du 20 novembre 2014 après laquelle a été rendu le jugement attaqué, le président de la troisième chambre du tribunal administratif de Dijon a pris une ordonnance de réouverture de l'instruction et le greffe de la chambre a communiqué aux défendeurs un nouveau mémoire du requérant, suivi le lendemain de la communication au requérant et à l'Etat d'un mémoire de Pôle emploi Bourgogne. Par suite, en s'abstenant de clore à nouveau l'instruction alors que le délai qui restait à courir avant l'audience ne permettait plus que cette clôture intervienne, ainsi que le prévoit l'article R. 613-2, sans qu'une ordonnance ne soit prise par le président de la formation de jugement, le tribunal administratif a rendu son jugement au terme d'une procédure irrégulière.<br/>
<br/>
              4. Il résulte de ce qui précède que M. B...est fondé à demander l'annulation du jugement qu'il attaque. L'irrégularité de la procédure ainsi relevée suffisant à entraîner cette annulation, il n'est pas nécessaire d'examiner les autres moyens du pourvoi.<br/>
<br/>
              5. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat le versement d'une somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du tribunal administratif de Dijon du 4 décembre 2014 est annulé.<br/>
Article 2 : L'affaire est renvoyée au tribunal administratif de Dijon.<br/>
Article 3 : L'Etat versera une somme de 2 000 euros à M. B...au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à M. A...B..., à Pôle emploi et à la ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
