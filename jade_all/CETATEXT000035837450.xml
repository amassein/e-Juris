<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000035837450</ID>
<ANCIEN_ID>JG_L_2017_10_000000410560</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/35/83/74/CETATEXT000035837450.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème chambre, 19/10/2017, 410560, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-10-19</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>410560</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LE BRET-DESACHE ; SCP BOUZIDI, BOUHANNA</AVOCATS>
<RAPPORTEUR>Mme Laure DURAND-VIEL</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Louis Dutheillet de Lamothe</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2017:410560.20171019</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme B...A...a demandé au juge des référés du tribunal administratif de Bordeaux d'ordonner, sur le fondement des dispositions de l'article L. 521-1 du code de justice administrative, la suspension de l'exécution de l'arrêté du maire de la Tour Blanche-Cercles du 7 mars 2017 prononçant son licenciement sans préavis ni indemnité à compter du 4 mars 2017, et d'enjoindre sous astreinte à la commune de la réintégrer dans ses fonctions. Par une ordonnance n° 1701508 du 28 avril 2017, le juge des référés a, d'une part, suspendu l'exécution de l'arrêté du 7 mars 2017, et, d'autre part, enjoint au maire de La Tour Blanche- Cercles de réintégrer Mme A...dans ses fonctions dans un délai de quinze jours à compter de la notification de l'ordonnance jusqu'à ce qu'il soit statué sur la requête au fond.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et deux mémoires en réplique, enregistrés les 15 et 29 mai, le 29 août et le 4 octobre 2017 au secrétariat du contentieux du Conseil d'Etat, la commune de La Tour Blanche-Cercles demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) de mettre à la charge de Mme A...la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la loi n° 83-634 du 13 juillet 1983 ;  <br/>
              - la loi n° 84-53 du 26 janvier 1984 ;<br/>
              - le décret n° 88-145 du 15 février 1988 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Laure Durand-Viel, auditeur,  <br/>
<br/>
              - les conclusions de M. Louis Dutheillet de Lamothe, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Le Bret-Desaché, avocat de la commune de La Tour Blanche-Cercles et à la SCP Bouzidi, Bouhanna, avocat de MmeA....<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant que la commune de Cercles, aujourd'hui fusionnée au sein de la commune de La Tour Blanche-Cercles, a recruté Mme A...par contrats à durée déterminée d'un an régulièrement renouvelés à compter du 25 août 2008 puis, le 25 août 2014, par contrat à durée indéterminée, comme agent contractuel pour assurer notamment les fonctions d'adjoint technique de restauration scolaire à l'école de Cercles ; que, par une décision du 7 mars 2017, le maire de La Tour Blanche-Cercles a prononcé son licenciement disciplinaire sans préavis ni indemnité avec effet au 4 mars 2017 aux motifs d'attitudes discriminantes à l'égard de certains enfants, d'incitations à la discrimination entre les enfants, d'atteintes à la dignité, d'utilisation de sa position pour intervenir sur des situations qui concernaient sa fille, de violences verbales et de qualificatifs inappropriés face à des enfants ; <br/>
<br/>
              2. Considérant que, pour prononcer la suspension de la décision de licenciement de MmeA..., le juge des référés du tribunal administratif de Bordeaux a estimé que la condition d'urgence était remplie du seul fait que cette décision plaçait l'intéressée, au regard des charges et des ressources de son foyer, dans une situation financière très difficile ; qu'en ne se prononçant pas, pour apprécier l'urgence au sens des dispositions de l'article L. 521-1 du code de justice administrative, sur les motifs d'intérêt public invoqués en défense par la commune pour justifier l'urgence à maintenir l'exécution de cette décision, tirés de l'intérêt des enfants fréquentant la cantine de cette école, il a entaché sa décision d'une insuffisance de motivation ; que par suite, et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, l'ordonnance attaquée doit être annulée ;<br/>
<br/>
              3. Considérant qu'il y a lieu, en application des dispositions de l'article L. 821 2 du code de justice administrative, de régler l'affaire au titre de la procédure de référé engagée ;<br/>
<br/>
              4. Considérant qu'aux termes de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision (...) " ; qu'aux termes du premier alinéa de l'article R. 522-1 du même code : " La requête visant au prononcé de mesures d'urgence doit (...) justifier de l'urgence de l'affaire " ;<br/>
<br/>
              5. Considérant qu'il résulte de l'instruction que, pour prononcer l'arrêté de licenciement de MmeA..., le maire de La Tour Blanche-Cercles s'est fondé sur cinq courriers de parents d'élèves décrivant de façon détaillée des faits de maltraitance relatés par leurs enfants, qui seraient survenus lorsque l'intéressée assurait ses fonctions de cantinière en l'absence d'autres adultes ; qu'il ressort des pièces du dossier que le maire, accompagné de membres du conseil municipal, a rencontré ces parents, le 17 décembre 2016, pour obtenir confirmation de leurs déclarations avant de procéder à la suspension puis au licenciement de l'intéressée ; que, à la suite de la réintégration de Mme A...dans ses fonctions conformément à l'injonction prononcée par le juge des référés, cinq nouveaux courriers de plaintes ont été adressés au maire, dans lesquels certains parents évoquent un changement de cantine ou d'école et le recours à un suivi psychologique pour leur enfant, et sept parents d'élèves ont adressé au procureur de la République un courrier demandant l'ouverture d'une enquête ; que, si l'intéressée se prévaut de témoignages en sa faveur de parents d'enfants dont elle avait la charge dans le cadre de ses fonctions, ces témoignages ne sont pas de nature à contredire les témoignages à son encontre, selon lesquels certains enfants étaient victimes des agissements allégués ; qu'il résulte de ce qui précède que le moyen tiré de ce que la matérialité des faits retenus pour motiver la décision de licenciement n'est pas établie n'est pas de nature à créer, en l'état de l'instruction, un doute sérieux sur sa légalité ; que, compte tenu de l'ensemble des circonstances de l'espèce et notamment de la nature et de la gravité des faits de maltraitance reprochés à la requérante à l'égard d'enfants scolarisés à l'école primaire dont elle assurait seule l'encadrement, le moyen tiré de ce que le maire de La Tour Blanche-Cercles aurait commis une erreur manifeste d'appréciation en prononçant à son encontre la sanction de licenciement n'est pas non plus de nature à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision attaquée ;<br/>
<br/>
              6. Considérant qu'il résulte de ce qui précède, sans qu'il soit besoin d'examiner la condition d'urgence ni de se prononcer sur la fin de non recevoir opposée par le maire de La Tour Blanche-Cercles, que la demande de Mme A...doit être rejetée, y compris les conclusions aux fins d'injonction et celles présentées au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
              7. Considérant qu'il n'y a pas lieu, dans les circonstances de l'espèce, de mettre à la charge de Mme A...le versement de la somme que demande la commune de La Tour Blanche-Cercles au titre de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'ordonnance du juge des référés du tribunal administratif de Bordeaux du 28 avril 2017 est annulée.<br/>
Article 2 : La demande présentée par Mme A...devant le juge des référés du tribunal administratif de Bordeaux et ses conclusions présentées devant le Conseil d'Etat au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 3 : Les conclusions de la commune de La Tour Blanche - Cercles présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 5 : La présente décision sera notifiée à la commune de La Tour Blanche-Cercles et à Mme B...A....<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
