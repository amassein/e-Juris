<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029926610</ID>
<ANCIEN_ID>JG_L_2014_12_000000368651</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/92/66/CETATEXT000029926610.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème SSJS, 19/12/2014, 368651</TITRE>
<DATE_DEC>2014-12-19</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>368651</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème SSJS</FORMATION>
<TYPE_REC>Contentieux des pensions</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ODENT, POULET</AVOCATS>
<RAPPORTEUR>Mme Clémence Olsina</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Xavier De Lesquen</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2014:368651.20141219</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 21 mai et 26 septembre 2013 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. A... B..., demeurant ...; M. B...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 12/00391 du 18 mars 2013 par lequel la cour régionale des pensions de Bastia, sur appel du ministre de la défense, a annulé le jugement du 15 octobre 2012 du tribunal départemental des pensions de Haute-Corse en tant qu'il lui a accordé un droit à pension au taux de 10 % pour l'infirmité d'acouphènes invalidants ; <br/>
<br/>
              2°) réglant l'affaire au fond, de confirmer le jugement du tribunal départemental des pensions de Haute-Corse ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 1 500 euros à verser à la SCP Odent - Poulet, avocat de M.B..., au titre des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 ; <br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code des pensions militaires d'invalidité et des victimes de la guerre ;<br/>
<br/>
              Vu la loi n° 91-647 du 10 juillet 1991 ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Clémence Olsina, auditeur,  <br/>
<br/>
              - les conclusions de M. Xavier de Lesquen, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Odent, Poulet, avocat de M. B...;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que M.B..., affecté depuis juillet 1999 au groupement de gendarmerie départementale de Haute-Corse au grade d'adjudant et exerçant les fonctions de chef d'atelier automobile, a demandé, le 2 août 2007, le bénéfice d'une pension militaire d'invalidité pour les infirmités " hypoacousie bilatérale " et " acouphènes " ; que, par une décision du 14 avril 2009, le ministre de la défense a rejeté sa demande ; que par un jugement avant dire droit du 4 avril 2011, le tribunal départemental des pensions de Haute-Corse, saisi par M.B..., a désigné un expert afin d'évaluer le pourcentage d'invalidité résultant des infirmités invoquées ; que l'expert a conclu à un taux d'invalidité de 0 % au titre de " l'hypoacousie bilatérale " et de 10 % au titre des acouphènes ; que, par un jugement du 15 octobre 2012, le tribunal départemental des pensions de Haute-Corse a accordé à M. B...un droit à pension au taux de 10 % pour l'infirmité " acouphènes permanents invalidants " à compter du 2 août 2007 et a rejeté le surplus de ses conclusions ; que M. B...se pourvoit en cassation contre l'arrêt du 18 mars 2013 par lequel la cour régionale des pensions de Bastia, sur l'appel formé par le ministre de la défense, a annulé ce jugement en tant qu'il lui accorde ce droit à pension ; <br/>
<br/>
              2. Considérant qu'aux termes de l'article L. 4 du code des pensions militaires et d'invalidité et des victimes de la guerre : " Les pensions sont établies d'après le degré d'invalidité. / Sont prises en considération les infirmités entraînant une invalidité égale ou supérieure à 10 % " ; qu'il résulte de ces dispositions qu'une infirmité, même lorsqu'elle ne se manifeste que de façon intermittente, ouvre droit au versement d'une pension, sous réserve que les conditions d'imputabilité au service prévues par le code des pensions militaires d'invalidité et des victimes de la guerre soient par ailleurs remplies, dès lors qu'elle entraîne une invalidité égale ou supérieure à 10 % ; qu'il résulte de ce qui précède qu'en jugeant que l'infirmité " acouphènes " ne pouvait être indemnisée que lorsqu'elle est qualifiée de continue ou de permanente, la cour régionale des pensions de Bastia a commis une erreur de droit ; que, par suite, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, M. B...est fondé à en demander, pour ce motif, l'annulation ; <br/>
<br/>
              3. Considérant que M. B...a obtenu le bénéfice de l'aide juridictionnelle ; que, par suite, son avocat peut se prévaloir des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 ; qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 1 500 euros à verser à la SCP Odent - Poulet, avocat de M.B..., sous réserve qu'elle renonce à percevoir la somme correspondant à la part contributive de l'Etat ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 18 mars 2013 de la cour régionale des pensions de Bastia est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour régionale des pensions de Bastia. <br/>
Article 3 : L'Etat versera à la SCP Odent - Poulet, avocat de M.B..., la somme de 1 500 euros au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991.<br/>
Article 4 : La présente décision sera notifiée à M. A...B...et au ministre de la défense.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">48-01-02 PENSIONS. PENSIONS MILITAIRES D'INVALIDITÉ ET DES VICTIMES DE GUERRE. CONDITIONS D'OCTROI D'UNE PENSION. - INFIRMITÉ NE SE MANIFESTANT QUE DE FAÇON INTERMITTENTE - DROIT AU VERSEMENT D'UNE PENSION, SOUS RÉSERVE QUE LES CONDITIONS D'IMPUTABILITÉ AU SERVICE SOIENT PAR AILLEURS REMPLIES, DÈS LORS QU'ELLE ENTRAÎNE UNE INVALIDITÉ ÉGALE OU SUPÉRIEURE À 10 %.
</SCT>
<ANA ID="9A"> 48-01-02 Il résulte des dispositions de l'article L. 4 du code des pensions militaires et d'invalidité et des victimes de la guerre qu'une infirmité, même lorsqu'elle ne se manifeste que de façon intermittente, ouvre droit au versement d'une pension, sous réserve que les conditions d'imputabilité au service prévues par le code soient par ailleurs remplies, dès lors qu'elle entraîne une invalidité égale ou supérieure à 10 %.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
