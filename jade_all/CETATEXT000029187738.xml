<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029187738</ID>
<ANCIEN_ID>JG_L_2014_06_000000380645</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/18/77/CETATEXT000029187738.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème SSJS, 27/06/2014, 380645, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-06-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>380645</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème SSJS</FORMATION>
<TYPE_REC>Autres</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP NICOLAY, DE LANOUVELLE, HANNOTIN</AVOCATS>
<RAPPORTEUR>Mme Clémence Olsina</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Xavier de Lesquen</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2014:380645.20140627</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu l'ordonnance n° 1401166/7-1 du 23 mai 2014, enregistrée le 26 mai 2014 au secrétariat du contentieux du Conseil d'Etat, par laquelle la présidente de la 7ème section du tribunal administratif de Paris, avant qu'il soit statué sur la demande de la Society of architects and developers tendant à l'annulation pour excès de pouvoir de l'arrêté du 24 décembre 2013 par lequel le préfet de la région Ile-de-France, préfet de Paris, a délivré un permis de construire à la Fédération de Russie pour la construction d'un centre culturel et spirituel 2, avenue Rapp, 1 à 5, quai Branly et 192, rue de l'Université à Paris, a décidé, par application des dispositions de l'article 23-2 de l'ordonnance n° 58-1067 du 7 novembre 1958, de transmettre au Conseil d'Etat la question de la conformité aux droits et libertés garantis par la Constitution des dispositions de l'article L. 600-1-2 du code de l'urbanisme, issues de l'ordonnance n° 2013-638 du 18 juillet 2013 relative au contentieux de l'urbanisme ;<br/>
<br/>
              Vu le mémoire, enregistré le 28 mars 2014 au greffe du tribunal administratif de Paris, présenté, en application de l'article L. 23-1 de l'ordonnance n° 58-1067 du 7 novembre 1958, par la Society of architects and developers, dont le siège est 10, rue Coëtlogon à Paris (75006), représentée par son gérant en exercice ; <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
              Vu la Constitution, notamment son Préambule et son article 61-1 ;<br/>
<br/>
              Vu l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
<br/>
              Vu le code de la construction et de l'habitation ;<br/>
<br/>
              Vu le code de l'urbanisme ;<br/>
<br/>
              Vu la loi n° 2014-366 du 24 mars 2014 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Clémence Olsina, auditeur,  <br/>
<br/>
              - les conclusions de M. Xavier de Lesquen, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Nicolaÿ, de Lanouvelle, Hannotin, avocat de la Fédération de Russie ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il résulte des dispositions de l'article 23-4 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel que, lorsqu'une juridiction relevant du Conseil d'Etat a transmis à ce dernier, en application de l'article 23-2 de cette même ordonnance, la question de la conformité à la Constitution d'une disposition législative, le Conseil constitutionnel est saisi de cette question de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article L. 600-1-2 du code de l'urbanisme, créé par l'ordonnance du 18 juillet 2013 relative au contentieux de l'urbanisme, laquelle a été ratifiée par le 4° du IV de l'article 172 de la loi du 24 mars 2014 pour l'accès au logement et un urbanisme rénové : " Une personne autre que l'Etat, les collectivités territoriales ou leurs groupements ou une association n'est recevable à former un recours pour excès de pouvoir contre un permis de construire, de démolir ou d'aménager que si la construction, l'aménagement ou les travaux sont de nature à affecter directement les conditions d'occupation, d'utilisation ou de jouissance du bien qu'elle détient ou occupe régulièrement ou pour lequel elle bénéficie d'une promesse de vente, de bail, ou d'un contrat préliminaire mentionné à l'article L. 261-15 du code de la construction et de l'habitation " ; <br/>
<br/>
              3. Considérant que la Society of architects and developers soutient que les dispositions citées ci-dessus portent une atteinte substantielle au droit des personnes intéressées d'exercer un recours effectif devant une juridiction, tel qu'il est garanti par l'article 16 de la Déclaration des droits de l'homme et du citoyen ; que, toutefois, les décisions statuant sur les permis de construire, de démolir ou d'aménager, prises dans le cadre de la police spéciale de l'urbanisme, ont pour objet de contrôler que les projets en cause sont conformes aux règles d'urbanisme relatives à l'utilisation des sols, à l'implantation, la destination, la nature, l'architecture, les dimensions, l'assainissement des constructions et à l'aménagement de leurs abords ; que les dispositions contestées de l'article L. 600-1-2, définissant les conditions de recevabilité auxquelles sont soumis les recours dirigés contre les permis de construire, de démolir ou d'aménager, poursuivent un objectif d'intérêt général, consistant à prévenir le risque d'insécurité juridique auquel ces actes sont exposés ainsi que les contestations abusives ; qu'eu égard au champ d'application de ces dispositions, à la portée des décisions en cause et aux critères de recevabilité retenus, le moyen tiré de ce que ces dispositions porteraient atteinte aux garanties de l'article 16 de la Déclaration des droits de l'homme et du citoyen en restreignant excessivement le droit au recours ne peut être regardé comme ayant un caractère sérieux ; <br/>
<br/>
              4. Considérant qu'il résulte de ce qui précède que la question de la conformité des dispositions contestées aux droits et libertés garantis par la Constitution, qui n'est pas nouvelle, ne présente pas un caractère sérieux ; qu'il n'y a pas lieu, par suite, de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité invoquée ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
  Article 1er : Il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée par la Society of architects and developers.<br/>
<br/>
  Article 2 : La présente décision sera notifiée à la Society of architects and developers, à la Fédération de Russie et à la ministre du logement et de l'égalité des territoires. <br/>
  Copie en sera adressée au Conseil constitutionnel, au Premier ministre, à la garde des sceaux, ministre de la justice et au tribunal administratif de Paris.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
