<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043867914</ID>
<ANCIEN_ID>JG_L_2021_07_000000447512</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/86/79/CETATEXT000043867914.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème chambre, 13/07/2021, 447512, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-07-13</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>447512</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FOUSSARD, FROGER</AVOCATS>
<RAPPORTEUR>Mme Cécile Isidoro</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Marie-Gabrielle Merloz</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:447512.20210713</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              M. A... B... a demandé au tribunal administratif d'Orléans d'annuler la décision du 16 juin 2017 par laquelle la ministre du travail a refusé de lui octroyer la protection fonctionnelle et de condamner l'Etat à lui verser les sommes de 18 338,03 euros en réparation de son préjudice matériel et financier, 14 780,18 euros en réparation de son préjudice de carrière et 314 400 euros en réparation de son préjudice moral résultant du harcèlement moral dont il déclare avoir été victime. Par un jugement n° 1702682 du 10 janvier 2019, le tribunal administratif d'Orléans a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 19NT00984 du 13 octobre 2020, la cour administrative d'appel de Nantes a, sur appel de M. B..., annulé le jugement du tribunal administratif d'Orléans puis a rejeté la demande de M. B... et le surplus de ses conclusions d'appel.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 14 décembre 2020 et 15 mars 2021 au secrétariat du contentieux du Conseil d'Etat, M. B... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code du travail ;<br/>
              - la loi n° 83-634 du 13 juillet 1983 ;<br/>
              - la loi n° 84-16 du 11 janvier 1984 ;<br/>
              - le code de justice administrative ;  <br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Cécile Isidoro, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Marie-Gabrielle Merloz, rapporteure publique ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Foussard, Froger, avocat de M. B... ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 29 juin 2021, présentée par M. B... ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. Aux termes de l'article L. 822-1 du code de justice administrative : " Le pourvoi en cassation devant le Conseil d'Etat fait l'objet d'une procédure préalable d'admission. L'admission est refusée par décision juridictionnelle si le pourvoi est irrecevable ou n'est fondé sur aucun moyen sérieux ".<br/>
<br/>
              2. Pour demander l'annulation de l'arrêt qu'il attaque, M. B... soutient que la cour administrative d'appel de Nantes : <br/>
              - a entaché son arrêt d'irrégularité en omettant de statuer, d'une part, sur ses conclusions tendant à la réparation intégrale du préjudice matériel et financier résultant de sa maladie reconnue imputable au service ainsi que des préjudices personnels ou extra-patrimoniaux, au titre des souffrances morales et des troubles dans les conditions d'existence, engendrés par celle-ci, et d'autre part sur celles tendant à la réparation du préjudice résultant du caractère tardif du rétablissement de son activité à temps complet ; <br/>
              - a commis une erreur de qualification juridique des faits, dénaturé les pièces du dossier et commis une erreur de droit en jugeant qu'il n'était pas victime de harcèlement moral ;<br/>
              - a inexactement qualifié les faits et les a dénaturés en jugeant qu'aucune discrimination syndicale n'avait été pratiquée à son égard ;<br/>
              - a commis une erreur de qualification juridique en refusant de reconnaître que son employeur avait manqué à l'obligation de protection de l'article L. 4121-1 du code du travail.<br/>
<br/>
              3. Eu égard aux moyens soulevés, il y a lieu d'admettre les conclusions du pourvoi qui sont dirigées contre l'arrêt en tant qu'il a omis de se prononcer, d'une part, sur la réparation intégrale du préjudice matériel et financier de M. B... résultant de sa maladie reconnue imputable au service et d'autre part sur la réparation du préjudice résultant du caractère tardif du rétablissement de son activité à temps complet.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Les conclusions de M. B... qui sont dirigées contre l'arrêt en tant qu'il a omis de se prononcer, d'une part, sur ses conclusions tendant à la réparation intégrale du préjudice matériel et financier résultant de sa maladie reconnue imputable au service ainsi que des préjudices personnels ou extra-patrimoniaux, au titre des souffrances morales et des troubles dans les conditions d'existence, engendrés par celle-ci, et d'autre part sur celles tendant à la réparation du préjudice résultant du caractère tardif du rétablissement de son activité à temps complet sont admises.<br/>
Article 2 : Le surplus des conclusions du pourvoi de M. B... n'est pas admis.<br/>
Article 3 : La présente décision sera notifiée à M. A... B....<br/>
Copie en sera adressée à la ministre du travail, de l'emploi et de l'insertion.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
