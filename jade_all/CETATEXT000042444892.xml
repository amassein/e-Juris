<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042444892</ID>
<ANCIEN_ID>JG_L_2020_10_000000437711</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/44/48/CETATEXT000042444892.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème - 9ème chambres réunies, 19/10/2020, 437711</TITRE>
<DATE_DEC>2020-10-19</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>437711</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème - 9ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP THOUIN-PALAT, BOUCARD</AVOCATS>
<RAPPORTEUR>Mme Isabelle Lemesle</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Alexandre Lallet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:437711.20201019</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
     La Commission nationale des comptes de campagne et des financements politiques (CNCCFP) a saisi le Conseil d'État en application de l'article L. 52-15 du code électoral, par un mémoire du 17 janvier 2020 et deux mémoires en réplique des 2 et 11 mars 2020, sur le fondement de sa décision du 13 janvier 2020 rejetant le compte de campagne de M. A... B..., candidat tête de liste à l'élection qui s'est déroulée le 12 mai 2019 en vue de la désignation des membres du congrès et des assemblées provinciales de Nouvelle-Calédonie.<br/>
<br/>
     Par deux mémoires en défense, enregistrés les 12 février et 9 mars 2020, M. B... conclut au rejet de la saisine de la CNCCFP, à la réformation de sa décision du 13 janvier 2020, à ce que soit ordonné le remboursement forfaitaire des dépenses de campagne qu'il a exposées et, à titre subsidiaire, à ce que soit constaté l'absence de fraude et de manquement d'une particulière gravité aux règles relatives au financement des campagnes électorales et, dans tous les cas,  à ce que la somme de 5 500 euros soit mise à la charge de l'Etat au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
     La saisine a été communiquée à la province Nord de Nouvelle-Calédonie et au ministre des outre-mer, qui n'ont pas produit d'observations.<br/>
<br/>
<br/>
<br/>
<br/>
     Vu les autres pièces du dossier ;<br/>
<br/>
     Vu :<br/>
     - la loi organique n°99-210 du 19 mars 1999 ;<br/>
     - le code électoral ;<br/>
     - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
     Après avoir entendu en séance publique :<br/>
<br/>
     - le rapport de Mme Isabelle Lemesle, conseiller d'Etat,  <br/>
<br/>
     - les conclusions de M. Alexandre Lallet, rapporteur public ;<br/>
<br/>
     La parole ayant été donnée, avant et après les conclusions, à la SCP Thouin-Palat, Boucard, avocat de M. A... B... ;<br/>
<br/>
Vu la note en délibéré, enregistrée le 15 octobre 2020, présentée par M. B....<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Par une décision du 13 janvier 2020, la Commission nationale des comptes de campagne et des financements politiques (CNCCFP) a rejeté le compte de campagne de M. B..., candidat tête de la liste " Calédonie ensemble " lors de l'élection qui s'est déroulée le 12 mai 2019, dans la province Nord de la Nouvelle-Calédonie, en vue de la désignation des membres du congrès et de l'assemblée provinciale, et a estimé qu'en raison du dépassement du plafond des dépenses électorales résultant de la réintégration de montants indûment déduits le candidat n'avait pas droit à leur remboursement forfaitaire par l'Etat. Elle saisit le Conseil d'Etat en application de l'article L 52-15 du code électoral.<br/>
<br/>
              Sur le rejet du compte de campagne :<br/>
<br/>
              2. L'article L. 52-4 du code électoral dispose que : " (...) Le mandataire recueille, pendant les six mois précédant le premier jour du mois de l'élection et jusqu'à la date du dépôt du compte de campagne du candidat, les fonds destinés au financement de la campagne. / Il règle les dépenses engagées en vue de l'élection et antérieures à la date du tour de scrutin où elle a été acquise, à l'exception des dépenses prises en charge par un parti ou groupement politique (...) ". Aux termes de l'article L. 52-12 du même code : " Chaque candidat ou candidat tête de liste soumis au plafonnement prévu à l'article L. 52-11 et qui a obtenu au moins 1 % des suffrages exprimés est tenu d'établir un compte de campagne retraçant, selon leur origine, l'ensemble des recettes perçues et, selon leur nature, l'ensemble des dépenses engagées ou effectuées en vue de l'élection, hors celles de la campagne officielle, par lui-même ou pour son compte, au cours de la période mentionnée à l'article L. 52-4 (...) Au plus tard avant 18 heures le dixième vendredi suivant le premier tour de scrutin, chaque candidat ou candidat tête de liste présent au premier tour dépose à la Commission nationale des comptes de campagne et des financements politiques son compte de campagne et ses annexes accompagné (...) des factures, devis et autres documents de nature à établir le montant des dépenses payées ou engagées par le candidat ou pour son compte (...) ". Aux termes de l'article L. 52-11 du même code : " Pour les élections auxquelles l'article L. 52-4 est applicable, il est institué un plafond des dépenses électorales, autres que les dépenses de propagande directement prises en charge par l'Etat, exposées par chaque candidat ou chaque liste de candidats, ou pour leur compte, au cours de la période mentionnée au même article (...) ". L'article L. 52-11-1 précise que : " Les dépenses électorales des candidats aux élections auxquelles l'article L. 52-4 est applicable font l'objet d'un remboursement forfaitaire de la part de l'Etat égal à 47,5 % de leur plafond de dépenses. Ce remboursement ne peut excéder le montant des dépenses réglées sur l'apport personnel des candidats et retracées dans leur compte de campagne. / Le remboursement forfaitaire n'est pas versé aux candidats qui (...) ne se sont pas conformés aux prescriptions de l'article L. 52-11 (...) ". Enfin, aux termes de l'article L. 52-15 : " La Commission nationale des comptes de campagne et des financements politiques approuve et, après procédure contradictoire, rejette ou réforme les comptes de campagne. Elle arrête le montant du remboursement forfaitaire prévu à l'article L. 52-11-1 (...) / Lorsque la commission a constaté que le compte de campagne n'a pas été déposé dans le délai prescrit, si le compte a été rejeté ou si, le cas échéant après réformation, il fait apparaître un dépassement du plafond des dépenses électorales, la commission saisit le juge de l'élection (...) ". Il résulte de ces dispositions, en premier lieu, que le compte de campagne du candidat doit retracer l'ensemble des recettes perçues et des dépenses engagées ou effectuées par son mandataire en vue de l'élection, hors celles de la campagne officielle, pendant la période mentionnée à l'article L. 52-4, et, en second lieu, que le remboursement forfaitaire par l'Etat d'une partie des dépenses électorales exposées par le candidat ou pour son compte n'est pas dû lorsque le compte de campagne, le cas échéant après réformation, fait apparaitre un dépassement du plafond des dépenses électorales.<br/>
<br/>
              3. Il résulte de l'instruction que le parti " Calédonie ensemble " a conclu une convention avec les mandataires financiers de ses candidats têtes de liste dans chacune des trois provinces, afin de répartir les dépenses électorales communes selon trois taux correspondant à la proportion des électeurs inscrits dans chaque circonscription. Ce taux est de 23,53% pour la province Nord. En outre, le compte de campagne de M. B... a été établi en déduisant 11,57% des dépenses d'impression et d'affranchissement de documents électoraux hors programme et 50% des frais de réception, pour les mettre à la charge du parti, au motif que 11,57% des foyers calédoniens sont des militants ou des sympathisants du parti et que ces derniers représentent la moitié des personnes qui participent aux réceptions données lors de la campagne, à l'occasion de réunions publiques. Toutefois, en premier lieu, les frais d'impression et d'affranchissement engagés pour informer les électeurs, notamment sur le calendrier des événements de la campagne du candidat, le sont en vue de l'élection, sans qu'il y ait lieu de distinguer si les électeurs sont des militants ou des sympathisants du parti qui soutient le candidat. En second lieu, les réunions publiques ayant occasionné des frais de réception se sont tenues dans le ressort de la circonscription électorale du candidat, en prévision du scrutin et dans le but de soutenir la liste qu'il conduit. Les dépenses engagées à ce titre doivent dès lors être regardées comme procédant de circonstances particulières résultant de la campagne et par suite engagées en vue de l'élection. Il s'ensuit que c'est à bon droit que la Commission nationale des comptes de campagne et des financements politiques a réintégré les sommes correspondant à ces dépenses prises en charge par le parti " Calédonie ensemble ", pour un montant total de 918 854 F CFP, dans le compte de campagne de M. B....<br/>
<br/>
              4. Par ailleurs, si à l'appui de ses mémoires en défense M. B... reproche à la Commission nationale des comptes de campagne et des financements politiques d'avoir omis de relever que son compte de campagne comportait quatre postes de dépenses comptabilisées à tort, il résulte de l'instruction que ces dépenses, relatives à la réalisation d'un " mur d'expression " sur la notion de " peuple calédonien " dans le cadre de la rencontre citoyenne du 9 mars 2019, à des arrhes versés pour la location d'une salle en vue d'un dîner républicain organisé avec des chefs d'entreprise le 29 avril 2019, à la facturation d'une prestation de " danse guerrière " offerte le 12 avril 2019 en prélude à une réunion publique dédiée à la communauté Kanak et, enfin, aux frais d'impression et d'affranchissement de deux lettres d'information, munies du sigle du parti et portant la mention " Provinciales 2019 ", destinées à l'ensemble des agriculteurs et pêcheurs de la province Nord, ont été engagées en vue de l'élection et ont donc été à bon droit intégrées au compte de campagne de M. B....<br/>
<br/>
              5. Il résulte de ce qui précède que M. B... n'est pas fondé à demander la réformation de la décision du 13 janvier 2020 par laquelle la Commission nationale des comptes de campagne et des financements politiques a rejeté son compte de campagne et constaté qu'en application de l'article L. 52-11-1 du code électoral cité au point 2 il n'avait pas droit au remboursement forfaitaire des dépenses électorales par l'Etat.<br/>
<br/>
              Sur l'inéligibilité :<br/>
<br/>
              6. L'article L. 118-3 du code électoral dispose que : " Saisi par la commission instituée par l'article L. 52-14, le juge de l'élection peut prononcer l'inéligibilité du candidat dont le compte de campagne, le cas échéant après réformation, fait apparaître un dépassement du plafond des dépenses électorales (...) Il prononce également l'inéligibilité du candidat ou des membres du binôme de candidats dont le compte de campagne a été rejeté à bon droit en cas de volonté de fraude ou de manquement d'une particulière gravité aux règles relatives au financement des campagnes électorales (...)". En dehors des cas de fraude, le juge de l'élection ne prononce l'inéligibilité d'un candidat dont le compte de campagne a été rejeté à bon droit que s'il constate un manquement d'une particulière gravité aux règles de financement des campagnes électorales. Il lui incombe à cet effet de prendre en compte l'ensemble des circonstances de l'espèce et d'apprécier s'il s'agit d'un manquement caractérisé à une règle substantielle relative au financement des campagnes électorales et s'il présente un caractère délibéré.<br/>
<br/>
              7. La somme de 918 854 F CFP initialement soustraite des dépenses du compte de campagne de M. B... et réintégrée à bon droit par la Commission nationale des comptes de campagne et des financements politiques correspond à 13% des dépenses réellement engagées et entraîne un dépassement de 2,7 % du plafond des dépenses autorisées dans la circonscription. En déduisant indûment des dépenses engagées en vue de l'élection et en évitant ainsi de faire apparaître un dépassement du plafond des dépenses autorisées, M. B..., sénateur et élu expérimenté de la province Nord, doit être regardé comme ayant méconnu de manière délibérée une règle substantielle du financement des campagnes électorales qu'il ne pouvait ignorer. Il a commis, dans ces conditions, un manquement d'une particulière gravité aux règles de financement des campagnes électorales.<br/>
<br/>
              8. Il résulte de ce qui précède qu'il y a lieu, en application de l'article L. 118-3 du code électoral, de déclarer M. B... inéligible pendant douze mois à compter de la présente décision.<br/>
              9. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : M. B... est déclaré inéligible à toutes les élections pour une durée de douze mois à compter de la présente décision.<br/>
Article 2 : Les conclusions de M. B... tendant à la réformation de la décision de la Commission nationale des comptes de campagne et des financements politiques et à l'application de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 3 : La présente décision sera notifiée à la Commission nationale des comptes de campagne et des financements politiques et à M. A... B....<br/>
Copie en sera adressée au ministre des outre-mer.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">28-005-04-02-04 ÉLECTIONS ET RÉFÉRENDUM. DISPOSITIONS GÉNÉRALES APPLICABLES AUX ÉLECTIONS. FINANCEMENT ET PLAFONNEMENT DES DÉPENSES ÉLECTORALES. COMPTE DE CAMPAGNE. DÉPENSES. - DÉPENSES ENGAGÉES EN VUE DE L'ÉLECTION (ART. L. 52-12 DU CODE ÉLECTORAL) - 1) FRAIS D'IMPRESSION ET D'AFFRANCHISSEMENT ENGAGÉS POUR INFORMER LES ÉLECTEURS - INCLUSION, MÊME SI LES DESTINATAIRES SONT MILITANTS OU SYMPATHISANTS - 2) FRAIS DE RÉCEPTION - CIRCONSTANCES PARTICULIÈRES DE TEMPS, DE LIEU ET D'OBJET ÉTABLISSANT LE BUT ÉLECTORAL - INCLUSION [RJ1] - ESPÈCE - 3) MANIFESTATIONS DIVERSES ORGANISÉES EN VUE DE L'ÉLECTION - ESPÈCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">28-005-04-04 ÉLECTIONS ET RÉFÉRENDUM. DISPOSITIONS GÉNÉRALES APPLICABLES AUX ÉLECTIONS. FINANCEMENT ET PLAFONNEMENT DES DÉPENSES ÉLECTORALES. PORTÉE DE L'INÉLIGIBILITÉ. - INÉLIGIBILITÉ PRÉVUE PAR L'ARTICLE L. 118-3 DU CODE ÉLECTORAL - 3E ALINÉA (MANQUEMENT D'UNE PARTICULIÈRE GRAVITÉ) [RJ2] - ESPÈCE.
</SCT>
<ANA ID="9A"> 28-005-04-02-04 1) Les frais d'impression et d'affranchissement engagés pour informer les électeurs, notamment sur le calendrier des événements de la campagne du candidat, le sont en vue de l'élection, sans qu'il y ait lieu de distinguer si les électeurs sont des militants ou des sympathisants du parti qui soutient le candidat.... ,,2) Les réunions publiques ayant occasionné des frais de réception en cause en l'espèce se sont tenues dans le ressort de la circonscription électorale du candidat, en prévision du scrutin et dans le but de soutenir la liste qu'il conduit. Les dépenses engagées à ce titre doivent dès lors être regardées comme procédant de circonstances particulières résultant de la campagne et par suite engagées en vue de l'élection, quand bien même la moitié des participants à ces réunions seraient des militants et sympathisants du parti qui soutient le candidat.... ,,3) Des dépenses, relatives à la réalisation d'un mur d'expression sur la notion de peuple calédonien dans le cadre de la rencontre citoyenne, à des arrhes versés pour la location d'une salle en vue d'un dîner républicain organisé avec des chefs d'entreprise, à la facturation d'une prestation de danse guerrière offerte en prélude à une réunion publique dédiée à la communauté Kanak et, enfin, aux frais d'impression et d'affranchissement de deux lettres d'information, munies du sigle du parti et portant la mention Provinciales 2019, destinées à l'ensemble des agriculteurs et pêcheurs de la province Nord de la Nouvelle-Calédonie, ont été engagées en vue de l'élection.</ANA>
<ANA ID="9B"> 28-005-04-04 La somme initialement soustraite des dépenses du compte de campagne et réintégrée à bon droit par la Commission nationale des comptes de campagne et des financements politiques (CNCCFP) correspond à 13 % des dépenses réellement engagées et entraîne un dépassement de 2,7 % du plafond des dépenses autorisées dans la circonscription. En déduisant indûment des dépenses engagées en vue de l'élection et en évitant ainsi de faire apparaître un dépassement du plafond des dépenses autorisées, l'intéressé, sénateur et élu expérimenté, doit être regardé comme ayant méconnu de manière délibérée une règle substantielle du financement des campagnes électorales qu'il ne pouvait ignorer. Il a commis, dans ces conditions, un manquement d'une particulière gravité aux règles de financement des campagnes électorales.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 10 avril 2009,,, n° 315011, T. pp. 662-760-761.,,[RJ2] Cf., sur les critères appliqués, CE, Assemblée, 4 juillet 2011, Elections régionales d'Ile-de-France, n° 338033, 338199, p. 317 ; CE, 27 mars 2012, M.,, n° 357453, T. pp. 773-774.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
