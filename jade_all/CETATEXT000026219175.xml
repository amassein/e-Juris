<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026219175</ID>
<ANCIEN_ID>JG_L_2012_07_000000341932</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/21/91/CETATEXT000026219175.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème et 8ème sous-sections réunies, 23/07/2012, 341932</TITRE>
<DATE_DEC>2012-07-23</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>341932</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème et 8ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Jacques Arrighi de Casanova</PRESIDENT>
<AVOCATS>SCP ANCEL, COUTURIER-HELLER, MEIER-BOURDEAU ; SCP DE CHAISEMARTIN, COURJON</AVOCATS>
<RAPPORTEUR>M. Romain Victor</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Vincent Daumas</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2012:341932.20120723</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 27 juillet et 25 octobre 2010 au secrétariat du contentieux du Conseil d'Etat, présentés pour le département de la Marne, représenté par le président du conseil général ; le département demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 09NC011155 du 17 juin 2010 par lequel la cour administrative d'appel de Nancy a rejeté sa requête tendant, d'une part, à l'annulation du jugement du 11 juin 2009 par lequel le tribunal administratif de Châlons-en-Champagne a rejeté sa demande tendant à la condamnation de Réseau Ferré de France à réparer le préjudice, correspondant au coût de destruction et de reconstruction d'un pont, qu'il affirme avoir subi du fait de l'insuffisant entretien de cet ouvrage par la Société nationale des chemins de fer français puis par l'établissement Réseau Ferré de France, en méconnaissance de conventions antérieurement conclues, majorée de l'indemnité réparant l'évolution de ce préjudice au jour du jugement, et, d'autre part, à la condamnation de Réseau Ferré de France à lui verser la somme de 7 297 000 euros en réparation de ce préjudice ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code général des collectivités territoriales ;<br/>
<br/>
              Vu le code de la voirie routière ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Romain Victor, Maître des Requêtes en service extraordinaire,<br/>
<br/>
              - les observations de la SCP Ancel, Couturier-Heller, Meier-Bourdeau, avocat de réseau ferré de France et de la SCP de Chaisemartin, Courjon, avocat du département de la Marne, <br/>
<br/>
              - les conclusions de M. Vincent Daumas, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Ancel, Couturier-Heller, Meier-Bourdeau, avocat de réseau ferré de France et à la SCP de Chaisemartin, Courjon, avocat du département de la Marne ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, par une décision du 10 novembre 1932, le ministre des travaux publics a autorisé la réalisation de travaux de construction d'un pont situé sur la route départementale n° 74 à Bétheny (Marne) et traversant la ligne de chemin de fer de Châlons à Reims, afin de remplacer un passage à niveau ; qu'à l'achèvement des travaux, deux procès-verbaux de récolement, réception et remise de l'ouvrage ont été établis, puis homologués par le préfet de la Marne les 5 décembre 1936 et 27 avril 1937 ; qu'il a été fait mention, dans ces procès-verbaux, d'une répartition de la charge des dépenses d'entretien du pont entre la Compagnie des Chemins de fer de l'Est, concessionnaire de la ligne de chemin de fer de Châlons à Reims, et le service vicinal ; qu'à la suite de travaux de rehaussement du pont destinés à permettre l'électrification de la ligne de chemin de fer, un nouveau procès-verbal de récolement des parties de l'ouvrage modifiées et de remise a été établi et homologué par le préfet de la Marne le 26 mai 1964 ; qu'il a été fait mention, dans ce nouveau procès-verbal, de la répartition de la charge des dépenses d'entretien des parties de l'ouvrage ayant été modifiées entre la commune de Bétheny, la Société nationale des chemins de fer français (SNCF), venue aux droits de la Compagnie des Chemins de fer de l'Est, et le service départemental des ponts et chaussées ; qu'un expert ayant constaté que l'état de vétusté du pont nécessitait sa démolition et sa reconstruction, le département de la Marne, propriétaire de la route départementale n° 74, a demandé le 10 juillet 2006 à Réseau Ferré de France (RFF), propriétaire des voies ferrées, venu aux droits de la SNCF, de se conformer aux obligations d'entretien du pont contractées lors de l'établissement des différents procès-verbaux de récolement ; qu'après avoir rejeté l'offre de RFF de lui verser une indemnité forfaitaire de 1 ,6 million d'euros sous réserve que le département assure la maîtrise d'ouvrage de l'opération de démolition-reconstruction et l'entretien ultérieur du pont, le département de la Marne a demandé au tribunal administratif de Châlons-en-Champagne la condamnation de cet établissement à lui verser la somme de 5 millions d'euros représentant le coût de la démolition et de la reconstruction du pont ; qu'il se pourvoit en cassation contre l'arrêt du 17 juin 2010 par lequel la cour administrative d'appel de Nancy a confirmé le jugement du tribunal administratif de Châlons-en-Champagne du 11 juin 2009 rejetant sa demande indemnitaire ;<br/>
<br/>
              2. Considérant, d'une part, que les ponts ne constituent pas des éléments accessoires des cours d'eau ou des voies ferrées qu'ils traversent mais sont au nombre des éléments constitutifs des voies dont ils relient les parties séparées de façon à assurer la continuité du passage ; que, par suite, un pont supportant une route départementale appartient à la voirie départementale ;<br/>
<br/>
              3. Considérant, d'autre part, qu'aux termes de l'article L. 131-2 du code de la voirie routière : " Les dépenses relatives à la construction, à l'aménagement et à l'entretien des routes départementales sont à la charge du département " ; qu'aux termes de l'article L. 3321-1 du code général des collectivités territoriales : " Sont obligatoires pour le département : / (...) 16° Les dépenses d'entretien et construction de la voirie départementale (...) " ; que ces dispositions ne font pas obstacle à ce que le département conclue avec le propriétaire ou l'exploitant de la voie franchie par un pont appartenant à la voirie départementale une convention mettant à la charge de celui-ci tout ou partie des frais d'entretien de cet ouvrage ; que si le département peut, lorsqu'une telle convention a été conclue, réclamer sur ce fondement le versement d'une indemnité réparant le préjudice que lui a causé l'inexécution fautive du contrat par l'autre partie, il reste toutefois tenu, dans tous les cas, d'assurer l'entretien normal du pont en faisant procéder aux réparations nécessaires et en inscrivant les dépenses correspondantes à son budget ;<br/>
<br/>
              4. Considérant qu'il résulte de ce qui précède qu'en jugeant, après avoir relevé que, par les différents procès-verbaux de récolement établis à la suite de la construction et du rehaussement du pont traversant, à Bétheny, la ligne de chemin de fer de Châlons à Reims et supportant la route départementale n° 74, la charge de l'entretien de cet ouvrage avait été conventionnellement répartie entre l'exploitant ferroviaire et le département, que les stipulations de ces conventions n'avaient pu, en tout état de cause, faire naître d'obligations de l'exploitant ferroviaire à l'égard du département de la Marne, la cour a commis une erreur de droit ; que, dès lors, le département est fondé, pour ce motif, et sans qu'il soit besoin d'examiner les autres moyens de son pourvoi, à en demander l'annulation ;<br/>
<br/>
              5. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge du département de la Marne qui n'est pas, dans la présente instance, la partie perdante ; qu'en revanche, il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de RFF le versement au département de la Marne de la somme de 3 000 euros au titre de ces dispositions ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 17 juin 2010 de la cour administrative d'appel de Nancy est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Nancy.<br/>
Article 3 : Réseau Ferré de France versera au département de la Marne une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Les conclusions de Réseau Ferré de France présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée au département de la Marne et à Réseau Ferré de France.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">135-03-04-02-01 COLLECTIVITÉS TERRITORIALES. DÉPARTEMENT. FINANCES DÉPARTEMENTALES. DÉPENSES. DÉPENSES OBLIGATOIRES. - VOIRIE DÉPARTEMENTALE - PONT FRANCHISSANT UNE VOIE FERRÉE ET SUPPORTANT UNE ROUTE DÉPARTEMENTALE - INCLUSION [RJ1] - CONSÉQUENCE - ENTRETIEN ET AMÉNAGEMENT DE L'OUVRAGE INCOMBANT AU DÉPARTEMENT - EXISTENCE - POSSIBILITÉ DE CONCLURE UNE CONVENTION AVEC L'EXPLOITANT FERROVIAIRE POUR RÉPARTIR LA CHARGE DE L'ENTRETIEN DE L'OUVRAGE - EXISTENCE - CONSÉQUENCES SUR LES DROITS ET OBLIGATIONS DU DÉPARTEMENT.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">24-01-01-01-01-02 DOMAINE. DOMAINE PUBLIC. CONSISTANCE ET DÉLIMITATION. DOMAINE PUBLIC ARTIFICIEL. BIENS FAISANT PARTIE DU DOMAINE PUBLIC ARTIFICIEL. VOIES PUBLIQUES ET LEURS DÉPENDANCES. - PONT ENJAMBANT UNE VOIE FERRÉE ET SUPPORTANT UNE ROUTE DÉPARTEMENTALE - OUVRAGE APPARTENANT À LA VOIRIE DÉPARTEMENTALE [RJ1] - CONSÉQUENCE - ENTRETIEN ET AMÉNAGEMENT INCOMBANT AU DÉPARTEMENT - EXISTENCE - POSSIBILITÉ DE CONCLURE UNE CONVENTION AVEC L'EXPLOITANT FERROVIAIRE POUR RÉPARTIR LA CHARGE DE L'ENTRETIEN DE L'OUVRAGE - EXISTENCE - CONSÉQUENCES SUR LES DROITS ET OBLIGATIONS DU DÉPARTEMENT.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">39-02-04 MARCHÉS ET CONTRATS ADMINISTRATIFS. FORMATION DES CONTRATS ET MARCHÉS. CONTENU. - POSSIBILITÉ POUR LE DÉPARTEMENT DE CONCLURE UNE CONVENTION AVEC L'EXPLOITANT FERROVIAIRE POUR RÉPARTIR LA CHARGE DE L'ENTRETIEN D'UN PONT ENJAMBANT UNE VOIE FERRÉE ET SUPPORTANT UNE ROUTE DÉPARTEMENTALE - EXISTENCE - CONSÉQUENCES SUR LES DROITS ET OBLIGATIONS DU DÉPARTEMENT.
</SCT>
<SCT ID="8D" TYPE="PRINCIPAL">65-01-005-05-01 TRANSPORTS. TRANSPORTS FERROVIAIRES. LIGNES DE CHEMIN DE FER. - PONT ENJAMBANT LES VOIES ET SUPPORTANT UNE ROUTE DÉPARTEMENTALE - OUVRAGE APPARTENANT À LA VOIRIE DÉPARTEMENTALE [RJ1] - CONSÉQUENCE - ENTRETIEN ET AMÉNAGEMENT INCOMBANT AU DÉPARTEMENT - EXISTENCE - POSSIBILITÉ DE CONCLURE UNE CONVENTION AVEC L'EXPLOITANT FERROVIAIRE POUR RÉPARTIR LA CHARGE DE L'ENTRETIEN DE L'OUVRAGE - EXISTENCE - CONSÉQUENCES SUR LES DROITS ET OBLIGATIONS DU DÉPARTEMENT.
</SCT>
<ANA ID="9A"> 135-03-04-02-01 Les dispositions de l'article L. 131-2 du code de la voirie routière, qui prévoient que les dépenses relatives à la construction, à l'aménagement et à l'entretien des routes départementales sont à la charge du département, et celles de l'article L. 3321-1 du code général des collectivités territoriales, en vertu desquelles les dépenses d'entretien et construction de la voirie départementale « sont obligatoires pour le département », ne font pas obstacle à ce que le département conclue avec le propriétaire ou l'exploitant de la voie franchie par un pont appartenant à la voirie départementale une convention mettant à la charge de celui-ci tout ou partie des frais d'entretien de cet ouvrage. Si le département peut, lorsqu'une telle convention a été conclue, réclamer sur ce fondement le versement d'une indemnité réparant le préjudice que lui a causé l'inexécution fautive du contrat par l'autre partie, il reste toutefois tenu, dans tous les cas, d'assurer l'entretien normal du pont en faisant procéder aux réparations nécessaires et en inscrivant les dépenses correspondantes à son budget.</ANA>
<ANA ID="9B"> 24-01-01-01-01-02 Les dispositions de l'article L. 131-2 du code de la voirie routière, qui prévoient que les dépenses relatives à la construction, à l'aménagement et à l'entretien des routes départementales sont à la charge du département, et celles de l'article L. 3321-1 du code général des collectivités territoriales, en vertu desquelles les dépenses d'entretien et construction de la voirie départementale « sont obligatoires pour le département », ne font pas obstacle à ce que le département conclue avec le propriétaire ou l'exploitant de la voie franchie par un pont appartenant à la voirie départementale une convention mettant à la charge de celui-ci tout ou partie des frais d'entretien de cet ouvrage. Si le département peut, lorsqu'une telle convention a été conclue, réclamer sur ce fondement le versement d'une indemnité réparant le préjudice que lui a causé l'inexécution fautive du contrat par l'autre partie, il reste toutefois tenu, dans tous les cas, d'assurer l'entretien normal du pont en faisant procéder aux réparations nécessaires et en inscrivant les dépenses correspondantes à son budget. Par suite, possibilité pour un département ayant conclu avec l'exploitant ferroviaire une convention répartissant la charge de l'entretien d'un pont traversant une voie ferrée et supportant une route départementale de réclamer une indemnité réparant le préjudice subi du fait de l'inexécution fautive de cette convention.</ANA>
<ANA ID="9C"> 39-02-04 Les dispositions de l'article L. 131-2 du code de la voirie routière, qui prévoient que les dépenses relatives à la construction, à l'aménagement et à l'entretien des routes départementales sont à la charge du département, et celles de l'article L. 3321-1 du code général des collectivités territoriales, en vertu desquelles les dépenses d'entretien et construction de la voirie départementale « sont obligatoires pour le département », ne font pas obstacle à ce que le département conclue avec le propriétaire ou l'exploitant de la voie franchie par un pont appartenant à la voirie départementale une convention mettant à la charge de celui-ci tout ou partie des frais d'entretien de cet ouvrage. Si le département peut, lorsqu'une telle convention a été conclue, réclamer sur ce fondement le versement d'une indemnité réparant le préjudice que lui a causé l'inexécution fautive du contrat par l'autre partie, il reste toutefois tenu, dans tous les cas, d'assurer l'entretien normal du pont en faisant procéder aux réparations nécessaires et en inscrivant les dépenses correspondantes à son budget. Par suite, possibilité pour un département ayant conclu avec l'exploitant ferroviaire une convention répartissant la charge de l'entretien d'un pont traversant une voie ferrée et supportant une route départementale de réclamer une indemnité réparant le préjudice subi du fait de l'inexécution fautive de cette convention.</ANA>
<ANA ID="9D"> 65-01-005-05-01 Les dispositions de l'article L. 131-2 du code de la voirie routière, qui prévoient que les dépenses relatives à la construction, à l'aménagement et à l'entretien des routes départementales sont à la charge du département, et celles de l'article L. 3321-1 du code général des collectivités territoriales, en vertu desquelles les dépenses d'entretien et construction de la voirie départementale « sont obligatoires pour le département », ne font pas obstacle à ce que le département conclue avec le propriétaire ou l'exploitant de la voie franchie par un pont appartenant à la voirie départementale une convention mettant à la charge de celui-ci tout ou partie des frais d'entretien de cet ouvrage. Si le département peut, lorsqu'une telle convention a été conclue, réclamer sur ce fondement le versement d'une indemnité réparant le préjudice que lui a causé l'inexécution fautive du contrat par l'autre partie, il reste toutefois tenu, dans tous les cas, d'assurer l'entretien normal du pont en faisant procéder aux réparations nécessaires et en inscrivant les dépenses correspondantes à son budget. Par suite, possibilité pour un département ayant conclu avec l'exploitant ferroviaire une convention répartissant la charge de l'entretien d'un pont traversant une voie ferrée et supportant une route départementale de réclamer une indemnité réparant le préjudice subi du fait de l'inexécution fautive de cette convention.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf., sur le fait que les ponts sont au nombre des éléments constitutifs des voies dont ils relient les parties séparées de façon à assurer la continuité du passage, CE, 26 septembre 2001, Département de la Somme, n° 219338, p. 426.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
