<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031603900</ID>
<ANCIEN_ID>JG_L_2015_12_000000375581</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/60/39/CETATEXT000031603900.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème / 4ème SSR, 10/12/2015, 375581</TITRE>
<DATE_DEC>2015-12-10</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>375581</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème / 4ème SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GARREAU, BAUER-VIOLAS, FESCHOTTE-DESBOIS</AVOCATS>
<RAPPORTEUR>M. Jean-Dominique Langlais</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Laurence Marion</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:375581.20151210</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La commune de Villeurbanne a demandé au tribunal administratif de Lyon de condamner l'Etat à lui verser, à titre principal, la somme de 543 933 euros en réparation de l'illégalité fautive entachant le décret n° 99-973 du 25 novembre 1999 modifiant le décret n° 55-1397 du 22 octobre 1955 instituant la carte nationale d'identité et le décret n° 2001-185 du 26 février 2001 relatif aux conditions de délivrance et de renouvellement des passeports, ou, à titre subsidiaire, de lui verser la somme de 186 456 euros au titre de la dotation exceptionnelle instituée par l'article 103 de la loi du 30 décembre 2008 de finances rectificative pour 2008. Par un jugement n° 1005023-1101590 du 20 décembre 2012, le tribunal administratif a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 13LY00655 du 19 décembre 2013, la cour administrative d'appel de Lyon a rejeté l'appel formé contre ce jugement par la commune de Villeurbanne.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 19 février et 19 mai 2014 au secrétariat du contentieux du Conseil d'Etat, la commune de Villeurbanne demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
<br/>
              - la charte européenne de l'autonomie locale du 15 octobre 1985 ; <br/>
<br/>
              - la loi n° 2008-1443 du 30 décembre 2008, notamment son article 103, et la décision n° 2010-29/37 QPC du Conseil constitutionnel du 22 septembre 2010 ;<br/>
<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Dominique Langlais, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Laurence Marion, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Garreau, Bauer-Violas, Feschotte-Desbois, avocat de la commune de Villeurbanne ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, par un arrêt du 28 novembre 2006, la cour administrative d'appel de Lyon a condamné l'Etat à verser à la commune de Villeurbanne une provision de 908 036,50 euros à valoir sur l'indemnisation des préjudices ayant résulté pour elle, pendant une période allant jusqu'au 31 octobre 2005, de l'illégalité, d'une part, du décret du 25 novembre 1999 confiant aux communes la réception des demandes de cartes nationales d'identité, la transmission des dossiers aux services de l'Etat et la remise des titres aux demandeurs, d'autre part, du décret du 26 février 2001 leur confiant les mêmes opérations pour les passeports ; que le pourvoi en cassation formé contre cet arrêt par le ministre de l'intérieur a été rejeté par une décision du 14 septembre 2007 du Conseil d'Etat, statuant au contentieux ; que, par un courrier du 11 décembre 2008, la commune de Villeurbanne a demandé au ministre de l'intérieur de l'indemniser des préjudices de même nature qu'elle estimait avoir subis entre le 1er novembre 2005 et le 31 janvier 2008 ; que, par un courrier du 18 février 2009, elle a demandé au ministre, à titre subsidiaire, de lui verser pour cette même période la dotation exceptionnelle prévue par l'article 103 de la loi du 30 décembre 2008 de finances rectificative pour 2008 ; qu'à la suite du refus du ministre, la commune a demandé au tribunal administratif de Lyon de mettre à la charge de l'Etat, à titre principal, le versement d'une indemnité de 543 933 euros et, à titre subsidiaire, le versement d'une somme de 186 456 euros au titre de la dotation exceptionnelle ; que le tribunal administratif a rejeté l'ensemble de ces conclusions par un jugement du 20 décembre 2012, confirmé par un arrêt du 19 décembre 2013 de la cour administrative d'appel de Lyon contre lequel la commune se pourvoit en cassation ; <br/>
<br/>
              2. Considérant que le II de l'article 103 de la loi du 30 décembre 2008 interdit aux communes de se prévaloir de l'illégalité des décrets des 25 novembre 1999 et 26 février 2001 mentionnés ci-dessus pour demander réparation des préjudices correspondant aux dépenses ayant résulté de l'exercice par les maires des missions que leur confient ces décrets postérieurement au 26 février 2001 " sous réserve des décisions passées en force de chose jugée " ; que le III de cet article prévoit, en contrepartie de cette interdiction, l'attribution aux communes d'une dotation exceptionnelle destinée à les indemniser des charges ayant résulté pour elles de l'application des décrets jusqu'au 31 décembre 2008, le montant dû à chaque commune étant calculé au prorata des titres émis de 2005 à 2008 ; qu'aux termes du dernier alinéa de ce III : " Les communes qui ont engagé un contentieux indemnitaire fondé sur l'illégalité du décret n° 99-973 du 25 novembre 1999 ou du décret n° 2001-185 du 26 février 2001 précités ne sont éligibles à cette dotation exceptionnelle qu'à la condition que cette instance soit close par une décision passée en force de chose jugée et excluant toute condamnation de l'Etat " ; qu'il résulte de ces dispositions, éclairées par les travaux parlementaires qui ont précédé leur adoption, que sont exclues du bénéfice de la dotation exceptionnelle aussi bien les communes ayant engagé un contentieux en cours à la date de publication de la loi que les communes ayant engagé un contentieux déjà clos à cette date lorsque ces contentieux ont abouti à une condamnation de l'Etat en raison de l'illégalité des décrets, au titre de quelque période que ce soit ; <br/>
<br/>
              3. Considérant, en premier lieu, que les stipulations de l'article 9 de la charte européenne de l'autonomie locale du 15 octobre 1985, qui prévoient que les collectivités locales ont droit, dans le cadre de la politique économique nationale, à des ressources propres suffisantes dont elles peuvent disposer librement dans l'exercice de leurs compétences et que leurs ressources financières doivent être proportionnées aux compétences prévues par la Constitution ou la loi, ne garantissent pas aux collectivités locales un droit à une compensation spécifique des charges liées à l'exercice de chacune de leurs compétences ; qu'ainsi, en jugeant que l'article 103 de la loi du 30 décembre 2008, qui répare forfaitairement et de manière égalitaire entre les communes les transferts de charges illégalement provoqués par les décrets des 25 novembre 1999 et 26 février 2001, ne portait pas, par lui-même, atteinte aux droits garantis par ces stipulations, la cour administrative d'appel de Lyon n'a, en tout état de cause, pas commis d'erreur de droit ; <br/>
<br/>
              4. Considérant, en second lieu, qu'il résulte de ce qui a été dit au point 2 que la cour administrative d'appel n'a pas commis d'erreur de droit en jugeant que les dispositions de l'article 103 de la loi du 30 décembre 2008 interdisaient à la commune de Villeurbanne, qui avait obtenu du juge administratif la condamnation de l'Etat, au titre de l'illégalité des décrets des 25 novembre 1999 et 26 février 2001, à lui verser une provision à valoir sur l'indemnisation des préjudices ayant résulté pour elle de la mise en oeuvre des dispositions de ces textes, de prétendre au bénéfice de la dotation exceptionnelle, alors même que les indemnités obtenues n'avaient réparé que le préjudice subi entre le 1er janvier 2000 et le 31 octobre 2005 pour les cartes nationales d'identité et entre le 1er mars 2001 et le 31 octobre 2005 pour les passeports ; <br/>
<br/>
              5. Considérant qu'il résulte de ce qui précède que le pourvoi de la commune de Villeurbanne doit être rejeté, y compris ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
<br/>
<br/>			D E C I D E :<br/>
              			--------------<br/>
<br/>
Article 1er : Le pourvoi de la commune de Villeurbanne est rejeté.<br/>
Article 2 : La présente décision sera notifiée à la commune de Villeurbanne et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">60-01-02 RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. FAITS SUSCEPTIBLES OU NON D'OUVRIR UNE ACTION EN RESPONSABILITÉ. FONDEMENT DE LA RESPONSABILITÉ. - LOIS DE VALIDATION - LOI DE VALIDATION RELATIVE À L'ILLÉGALITÉ DES DÉCRETS DES 25 NOVEMBRE 1999 ET 26 FÉVRIER 2001 CONFIANT AUX COMMUNES L'INSTRUCTION DES DEMANDES DES CARTES D'IDENTITÉ ET DES PASSEPORTS (LOI N° 2008-1443 DU 30 DÉCEMBRE 2008) - DOTATION EXCEPTIONNELLE DESTINÉE À COMPENSER LE PRÉJUDICE - MODALITÉS D'APPLICATION.
</SCT>
<ANA ID="9A"> 60-01-02 Le II de l'article 103 de la loi n° 2008-1443 du 30 décembre 2008 a interdit aux communes de se prévaloir de l'illégalité des décrets des 25 novembre 1999 et 26 février 2001 ayant confié aux communes la gestion de certaines opérations de traitement des demandes de cartes nationales d'identité et de passeports et accordé, à certaines conditions, une dotation exceptionnelle destinées à les indemniser des charges ayant résulté pour elles de l'application de ces décrets jusqu'au 31 décembre 2008. Il résulte de ces dispositions, éclairées par les travaux parlementaires qui ont précédé leur adoption, que sont exclues du bénéfice de la dotation exceptionnelle aussi bien les communes ayant engagé un contentieux en cours à la date de publication de la loi que les communes ayant engagé un contentieux déjà clos à cette date et qui a abouti à une condamnation de l'Etat en raison de l'illégalité des décrets, au titre de quelque période que ce soit. Dès lors, une commune ayant obtenu du juge administratif la condamnation de l'Etat, pour cette faute, à lui verser une provision à valoir sur l'indemnisation des préjudices ayant résulté pour elle de la mise en oeuvre des ces deux décrets ne peut prétendre au bénéfice de la dotation exceptionnelle, alors même que les indemnités obtenues n'avaient réparé que le préjudice subi jusqu'à octobre 2005.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
