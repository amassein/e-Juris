<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000021497527</ID>
<ANCIEN_ID>JG_L_2009_12_000000300257</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/21/49/75/CETATEXT000021497527.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème et 2ème sous-sections réunies, 16/12/2009, 300257</TITRE>
<DATE_DEC>2009-12-16</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>300257</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème et 2ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Stirn</PRESIDENT>
<AVOCATS>SCP BORE ET SALVE DE BRUNETON ; ODENT</AVOCATS>
<RAPPORTEUR>M. Denis  Prieur</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Boulouis Nicolas</COMMISSAIRE_GVT>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 2 janvier et 28 mars 2007 au secrétariat du contentieux du Conseil d'Etat, présentés pour la CAISSE DES DEPOTS ET CONSIGNATIONS, domiciliée en son établissement sis rue du Vergne à BORDEAUX (33059) ; la CAISSE DES DEPOTS ET CONSIGNATIONS demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt du 24 octobre 2006 par lequel la cour administrative d'appel de Marseille a rejeté sa requête tendant à l'annulation du jugement du 16 juin 2003 par lequel le tribunal administratif de Nice a rejeté sa requête en tierce opposition dirigée contre le jugement du 19 mars 2001 par lequel le tribunal a, à la demande de M. A, annulé la décision implicite du ministre de la défense refusant de lui accorder la révision de sa pension de retraite ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande en déclarant non avenu le jugement du 19 mars 2001 du tribunal administratif de Nice ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le décret du 15 décembre 1928 ; <br/>
<br/>
              Vu le décret n° 65-836 du 24 septembre 1965 ; <br/>
<br/>
              Vu le décret n° 2004-1056 du 5 octobre 2004 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Denis Prieur, Conseiller d'Etat,  <br/>
<br/>
              - les observations de Me Odent, avocat de la CAISSE DES DEPOTS ET CONSIGNATIONS et de la SCP Boré et Salve de Bruneton, avocat de M. A, <br/>
<br/>
              - les conclusions de M. Nicolas Boulouis, rapporteur public ;<br/>
<br/>
<br/>
              La parole ayant été à nouveau donnée à Me Odent, avocat de la CAISSE DES DEPOTS ET CONSIGNATIONS et à la SCP Boré et Salve de Bruneton, avocat de M. A ; <br/>
<br/>
<br/>
<br/>
<br/>Considérant qu'il résulte des pièces du dossier soumis aux juges du fond que M. A, ancien ouvrier de l'Etat, a demandé au tribunal administratif de Nice d'annuler la décision du 5 avril 1993, ainsi que le rejet implicite de son recours administratif contre cette décision, par laquelle le ministre de la défense a rejeté sa demande tendant à ce que l'indemnité de congés payés dont il a bénéficié entre le 4 juillet 1972 et le 1er février 1993, date à laquelle il a été admis à faire valoir ses droits à la retraite, soit recalculée afin d'y inclure l'indemnité pour travaux insalubres attachée aux fonctions de travaux sous-marins à l'aide d'un scaphandre, dite prime de plongée, et à ce que sa pension de retraite soit révisée afin de prendre en compte cette indemnité ; que, par un jugement du 19 mars 2001, rendu sans que la CAISSE DES DEPOTS ET CONSIGNATIONS ait été appelée à l'instance, le tribunal administratif de Nice a annulé les décisions contestées en tant qu'elles refusent la révision de la pension de retraite de l'intéressé et le versement des rappels d'indemnités de congés payés pour la période allant du 1er janvier 1989 au 1er février 1993 ; que la CAISSE DES DEPOTS ET CONSIGNATIONS a formé devant le même tribunal une requête en tierce opposition tendant à ce que le jugement du 19 mars 2001 soit déclaré non avenu en tant qu'il statue sur la révision de la pension de retraite de M. A ; que, par jugement du 16 juin 2003, le tribunal administratif de Nice a rejeté cette demande ; que, par l'arrêt attaqué en date du 24 octobre 2006, la cour administrative d'appel de Marseille a rejeté l'appel formé par la CAISSE DES DEPOTS ET CONSIGNATIONS contre ce dernier jugement au motif que les décisions relatives à la carrière et à la rémunération des agents publics ne peuvent être regardées comme préjudiciant aux droits des organismes gestionnaires des régimes de pension en cause, alors même que ces décisions ont une incidence sur le montant de pensions que les organismes concernés seront amenés à verser audit agent ; que la CAISSE DES DEPOTS ET CONSIGNATIONS se pourvoit en cassation contre cet arrêt ;<br/>
<br/>
              Considérant qu'en vertu de l'article R. 832-1 du code de justice administrative : Toute personne peut former tierce opposition à une décision juridictionnelle qui préjudicie à ses droits, dès lors que ni elle, ni ceux qu'elle représente n'ont été présents ou régulièrement appelés à l'instance ayant abouti à cette décision ;<br/>
<br/>
              Considérant qu'aux termes de l'article 2 du décret du 24 septembre 1965 : Le fonds spécial des pensions des ouvriers de l'Etat est chargé d'assurer le service des pensions concédées ou révisées au profit des bénéficiaires du présent décret. Il est géré par la Caisse des dépôts et consignations et fonctionne sous le régime de la répartition (...) ; que selon l'article 41 du décret du 15 décembre 1928 alors applicable et repris en des termes identiques par l'article 35 du décret du 5 octobre 2004 relatif au régime des pensions des ouvriers des établissements industriels de l'Etat : La liquidation de la pension est faite par décision de l'employeur dont l'ouvrier relève, après accord de la Caisse des dépôts et consignations ; qu'il résulte de ces dispositions que la liquidation est faite par décision conjointe de la Caisse des dépôts et consignations et de l'employeur dont relève l'ouvrier et que, par suite, toute décision relative à la liquidation des retraites des ouvriers de l'Etat préjudicie aux droits de la CAISSE DES DEPOTS ET CONSIGNATIONS ; <br/>
<br/>
              Considérant qu'il est constant que le jugement du 19 mars 2001 a été rendu par le tribunal administratif de Nice sans que la CAISSE DES DEPOTS ET CONSIGNATIONS ait été présente ou régulièrement appelée à l'instance ; qu'ainsi qu'il a été dit plus haut, le tribunal ne pouvait adopter ce jugement, qui préjudiciait aux droits de la CAISSE DES DEPOTS ET CONSIGNATIONS, sans l'avoir préalablement appelée à l'instance ; que la cour administrative d'appel de Marseille a par suite entaché d'erreur de droit son arrêt du 24 octobre 2006 en rejetant l'appel de la CAISSE DES DEPOTS ET CONSIGNATIONS contre le jugement du 16 juin 2003 du tribunal administratif de Nice rejetant sa requête en tierce opposition, au motif que le jugement du 19 mars 2001, en tant qu'il statue sur la révision de la pension, avait pu être régulièrement rendu sans qu'elle fût appelée dans l'instance ; que la CAISSE DES DEPOTS ET CONSIGNATIONS  est dès lors fondée à en demander l'annulation de l'arrêt attaqué ; <br/>
<br/>
              Considérant qu'il y a lieu dans les circonstances de l'espèce, de faire application des dispositions de l'article L. 821-2 du code de justice administrative et de statuer sur l'appel présenté par la CAISSE DES DEPOTS ET CONSIGNATIONS contre le jugement du 16 juin 2003 par lequel le tribunal administratif de Nice après avoir admis la recevabilité de la tierce opposition formée par la  CAISSE DES DEPOTS ET CONSIGNATIONS contre le jugement du 19 mars 2001 en tant qu'il statue sur la révision de la pension de M. A, l'a rejetée comme non fondée ; <br/>
<br/>
              Considérant qu'aux termes de l'article R. 342-1 du code de justice administrative : Le tribunal administratif saisi d'une demande relevant de sa compétence territoriale est également compétent pour connaître d'une demande connexe à la précédente et relevant normalement de la compétence territoriale d'un autre tribunal administratif ; qu'il résulte de ces dispositions que la CAISSE DES DEPOTS ET CONSIGNATIONS n'est pas fondée à soutenir que le tribunal administratif de Nice n'était pas compétent pour statuer sur la requête de M. A tendant à l'annulation de la décision implicite du ministre de la défense refusant la révision de sa pension au motif que le tribunal compétent est celui dans le ressort duquel se trouve le lieu d'assignation du paiement de la pension, dès lors que l'intéressé, par la même requête, demandait au tribunal administratif de Nice l'annulation de la décision ministérielle tant en ce qu'elle lui refusait le versement des rappels d'indemnités de congés payés pour la période du 1er janvier 1989 au 1er février 1993 qu'en ce qu'elle rejetait sa demande de révision de sa pension ; <br/>
<br/>
              Considérant qu'il résulte de ce qui a été dit ci-dessus que le jugement du 19 mars 2001 par lequel le tribunal administratif de Nice a annulé la décision implicite du ministre de la défense refusant le versement des rappels d'indemnités de congés payés pour la période du 1er janvier 1989 au 1er février 1993 ainsi que la révision de la pension de retraite de M. A préjudicie aux droits de la CAISSE DES DEPOTS ET CONSIGNATIONS ; que compte tenu des dispositions précitées de l'article 41 du décret du 15 décembre 1928, il  ne saurait utilement être soutenu que la CAISSE DES DEPOTS ET CONSIGNATIONS aurait été représentée à cette instance par le ministre de la défense ; qu'est en outre inopérante à l'appui de la contestation de la recevabilité de la tierce opposition de la CAISSE DES DEPOTS ET CONSIGNATIONS la circonstance que celle-ci, comme elle en avait d'ailleurs l'obligation, ait tiré les conséquences nécessaires du jugement du 19 mars 2001 en recalculant la base de liquidation de la pension de l'intéressé eu égard aux motifs de cette décision ; qu'il suit de là que l'exception d'irrecevabilité opposée à  l'appel formé devant la cour administrative d'appel de Marseille par la CAISSE DES DEPOTS ET CONSIGNATIONS ne peut qu'être écartée ;<br/>
<br/>
              Considérant qu'aux termes de l'article 9 du décret du 24 septembre 1965 relatif au régime des pensions des ouvriers des établissements industriels de l'Etat, alors en vigueur : I. La pension est basée sur les émoluments annuels soumis à retenue afférents à l'emploi occupé effectivement depuis six mois au moins par l'intéressé au moment de sa radiation des contrôles ou, dans le cas contraire, sauf s'il y a eu rétrogradation par mesure disciplinaire, sur les émoluments annuels soumis à retenue afférents à l'emploi antérieurement occupé (...) / En ce qui concerne les intéressés rémunérés en fonction des salaires pratiqués dans l'industrie, les émoluments susvisés sont déterminés par la somme brute obtenue en multipliant par 1.960 le salaire horaire de référence correspondant à leur catégorie professionnelle au moment de la radiation des contrôles ou, dans le cas visé à l'alinéa précédent, à la catégorie professionnelle correspondant à l'emploi occupé. Ce produit est affecté d'un coefficient égal au rapport existant entre le salaire horaire résultant des gains et de la durée effective du travail pendant l'année expirant à la fin de la période dont il doit éventuellement être fait état et le salaire horaire de référence durant la même année ; qu'aux termes de l'article 28 du même décret : I. Les personnels visés à l'article 1er supportent une retenue de 8,9 %, calculée sur les émoluments représentés : (...) / b) Pour les intéressés rémunérés en fonction des salaires pratiqués dans l'industrie, par la somme brute obtenue en multipliant par 1.960 le salaire horaire moyen déterminé d'après le nombre d'heures de travail effectif dans l'année et les gains y afférents constitués par le salaire proprement dit et, éventuellement, la prime d'ancienneté, la prime de fonction, la prime de rendement ainsi que les heures supplémentaires, à l'exclusion de tout autre avantage, quelle qu'en soit la nature (...) ; que la prime de plongée, qui est liée à la nature de l'activité professionnelle exercée par l'intéressé et non pas à ses modalités d'exercice, remplit les conditions pour être qualifiée de prime de fonction au sens des dispositions précitées de l'article 28 du décret du 24 septembre 1965 et supporte la retenue pour pension ; qu'il en résulte que le ministre ne pouvait sans erreur de droit refuser à M. A de prendre en compte l'indemnité en cause pour le calcul de la pension de l'intéressé ; que dès lors, la CAISSE DES DEPOTS ET CONSIGNATIONS n'est pas fondée à soutenir que c'est à tort que par son jugement du 16 juin 2003, le tribunal administratif de Nice a rejeté sa requête tendant à ce que le jugement du 19 mars 2001 de la même juridiction soit déclaré non avenu en tant qu'il statue sur la révision de la pension de M. A ;<br/>
<br/>
              Sur les conclusions de M. A tendant à l'application des dispositions de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              Considérant qu'il y a lieu, dans les circonstances de l'espèce, de faire application de ces dispositions et de mettre à la charge de la CAISSE DES DEPOTS ET CONSIGNATIONS le versement à M. A de la somme de 3 000 euros au titre des frais exposés par lui et non compris dans les dépens ;  <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Marseille en date du 24 octobre 2006 est annulé.<br/>
<br/>
Article 2 : Les conclusions d'appel présentées par la CAISSE DES DEPOTS ET CONSIGNATIONS devant la cour administrative d'appel de Marseille sont rejetées.<br/>
Article 3 : La CAISSE DES DEPOTS ET CONSIGNATIONS versera à M. A une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
Article 4 : La présente décision sera notifiée à la CAISSE DES DEPOTS ET CONSIGNATIONS, à M. Emmanuel A et au ministre de la défense.<br/>
<br/>
</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">48-03-01 PENSIONS. RÉGIMES PARTICULIERS DE RETRAITE. OUVRIERS DES ÉTABLISSEMENTS INDUSTRIELS DE L'ETAT. - LIQUIDATION - DÉCISION CONJOINTE DE LA CAISSE DES DÉPÔTS ET CONSIGNATIONS ET DE L'EMPLOYEUR - CONSÉQUENCE - DÉCISION RELATIVE À LA LIQUIDATION D'UNE PENSION D'UN OUVRIER DE L'ETAT - DÉCISION PRÉJUDICIANT AUX DROITS DE LA CDC - EXISTENCE - CDC DEVANT ÊTRE APPELÉE EN LA CAUSE - QUALITÉ DU MINISTRE POUR LA REPRÉSENTER DANS L'INSTANCE - ABSENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-01-05-005 PROCÉDURE. INTRODUCTION DE L'INSTANCE. QUALITÉ POUR AGIR. REPRÉSENTATION DES PERSONNES MORALES. - CAISSE DES DÉPÔTS ET CONSIGNATIONS - DÉCISION RELATIVE À LA LIQUIDATION D'UNE PENSION D'UN OUVRIER DE L'ETAT - DÉCISION PRÉJUDICIANT AUX DROITS DE LA CDC - EXISTENCE - CDC DEVANT ÊTRE APPELÉE EN LA CAUSE - QUALITÉ DU MINISTRE POUR LA REPRÉSENTER DANS L'INSTANCE - ABSENCE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">54-08-04-01-01 PROCÉDURE. VOIES DE RECOURS. TIERCE-OPPOSITION. RECEVABILITÉ. NOTION DE DROIT LÉSÉ. - CAISSE DES DÉPÔTS ET CONSIGNATIONS - DÉCISION RELATIVE À LA LIQUIDATION D'UNE PENSION D'UN OUVRIER DE L'ETAT - DÉCISION PRÉJUDICIANT AUX DROITS DE LA CDC - EXISTENCE - CDC DEVANT ÊTRE APPELÉE EN LA CAUSE - QUALITÉ DU MINISTRE POUR LA REPRÉSENTER DANS L'INSTANCE - ABSENCE.
</SCT>
<ANA ID="9A"> 48-03-01 Il résulte des dispositions de l'article 2 du décret n° 65-836 du 24 septembre 1965 et de l'article 41 du décret du 15 décembre 1928 alors applicable et repris en termes identiques par l'article 35 du décret n° 2004-1056 du 5 octobre 2004 que la liquidation de la pension des ouvriers des établissements industriels de l'Etat est faite par décision conjointe de la Caisse des dépôts et consignations et de l'employeur dont relève l'ouvrier et que, par suite, toute décision relative à la liquidation des retraites des ouvriers de l'Etat préjudicie aux droits de la Caisse des dépôts et consignations. Il en résulte que le ministre de la défense ne peut être regardé comme ayant représenté la Caisse des dépôts et consignations. Cette dernière n'ayant pas été appelée dans l'instance par le tribunal administratif, sa tierce opposition était recevable.</ANA>
<ANA ID="9B"> 54-01-05-005 Il résulte des dispositions de l'article 2 du décret n° 65-836 du 24 septembre 1965 et de l'article 41 du décret du 15 décembre 1928 alors applicable et repris en termes identiques par l'article 35 du décret n° 2004-1056 du 5 octobre 2004 que la liquidation de la pension des ouvriers des établissements industriels de l'Etat est faite par décision conjointe de la Caisse des dépôts et consignations et de l'employeur dont relève l'ouvrier et que, par suite, toute décision relative à la liquidation des retraites des ouvriers de l'Etat préjudicie aux droits de la Caisse des dépôts et consignations. Il en résulte que le ministre de la défense ne peut être regardé comme ayant représenté la Caisse des dépôts et consignations. Cette dernière n'ayant pas été appelée dans l'instance par le tribunal administratif, sa tierce opposition était recevable.</ANA>
<ANA ID="9C"> 54-08-04-01-01 Il résulte des dispositions de l'article 2 du décret n° 65-836 du 24 septembre 1965 et de l'article 41 du décret du 15 décembre 1928 alors applicable et repris en termes identiques par l'article 35 du décret n° 2004-1056 du 5 octobre 2004 que la liquidation de la pension des ouvriers des établissements industriels de l'Etat est faite par décision conjointe de la Caisse des dépôts et consignations et de l'employeur dont relève l'ouvrier et que, par suite, toute décision relative à la liquidation des retraites des ouvriers de l'Etat préjudicie aux droits de la Caisse des dépôts et consignations. Il en résulte que le ministre de la défense ne peut être regardé comme ayant représenté la Caisse des dépôts et consignations. Cette dernière n'ayant pas été appelée dans l'instance par le tribunal administratif, sa tierce opposition était recevable.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
