<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031861465</ID>
<ANCIEN_ID>JG_L_2016_01_000000390441</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/86/14/CETATEXT000031861465.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème SSJS, 07/01/2016, 390441, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-01-07</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>390441</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème SSJS</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MASSE-DESSEN, THOUVENIN, COUDRAY ; SCP MONOD, COLIN, STOCLET</AVOCATS>
<RAPPORTEUR>Mme Marie Gautier-Melleray</RAPPORTEUR>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CESJS:2016:390441.20160107</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme B...A...a demandé au juge des référés du tribunal administratif de Melun, sur le fondement de l'article L. 521-2 du code de justice administrative, de suspendre l'exécution de l'arrêté 10 avril 2015 du maire de Champs-sur-Marne la mettant en demeure, ainsi que tous les occupants installés sur la parcelle cadastrée AM0332 située route de Malnoue à Champs-sur-Marne, de quitter les lieux dans un délai de 48 heures à compter de la notification de cet arrêté. Par une ordonnance n° 1502773 du 16 avril 2015, prise en application de l'article L. 522-3 du même code, le juge des référés a rejeté sa demande. <br/>
<br/>
              Par un pourvoi, un mémoire complémentaire et des observations complémentaires enregistrés les 27 mai, 11 juin et 20 juillet 2015, Mme A...demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cette ordonnance ; <br/>
<br/>
              2°) statuant en référé, de faire droit à sa demande ; <br/>
<br/>
              3°) de mettre à la charge de la commune de Champs-sur-Marne la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
<br/>
              - la convention de New York relative aux droits de l'enfant du 26 janvier 1990 ;<br/>
<br/>
              - le code général des collectivités territoriales ;<br/>
<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
<br/>
              - le décret n° 91-1266 du 19 décembre 1991 ;<br/>
<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Marie Gautier-Melleray, maître des requêtes, <br/>
<br/>
              - les conclusions de Mme Laurence Marion, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Masse-Dessen, Thouvenin, Coudray, avocat de Mme A...et à la SCP Monod, Colin, Stoclet, avocat de la commune de Champs-sur-Marne ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un servie public aurait porté, dans l'exercice de ses pouvoirs, une atteinte grave et manifestement illégale. Le juge des référés statue dans un délai de quarante-huit heures " ; qu'en vertu de l'article L. 522-3 du même code, le juge des référés peut, par une ordonnance motivée, rejeter une requête sans instruction ni audience, lorsqu'il est manifeste, au vu de la demande, qu'elle est mal fondée ; <br/>
<br/>
              2. Considérant qu'il ressort des pièces du dossier soumis au juge des référés que, par un arrêté du 10 avril 2015, pris en application de l'article L. 2212-2 du code général des collectivités territoriales, le maire de Champs-sur-Marne a mis en demeure les occupants de deux campements installés sur la parcelle AM0332, située route de Malnoue à Champs-sur-Marne, de quitter les lieux dans un délai de quarante-huit heures à compter de la notification de cet arrêté ; que MmeA..., qui était au nombre de ces occupants, a saisi le juge des référés du tribunal administratif de Melun, sur le fondement de l'article L. 521-2 du code de justice administrative, d'une demande tendant à la suspension de l'exécution de cet arrêté ; qu'elle se pourvoit en cassation contre l'ordonnance du 16 avril 2015 par laquelle le juge des référés a rejeté sa demande comme manifestement non fondée selon la procédure prévue à l'article L. 522-3 du même code ;<br/>
<br/>
              Sur les conclusions à fin de non-lieu et la recevabilité du pourvoi :<br/>
<br/>
              3. Considérant qu'il ressort des pièces du dossier que, postérieurement à l'ordonnance attaquée, mais antérieurement à l'introduction du pourvoi, les campements concernés par l'arrêté du 10 avril 2015 ont été évacués avec le concours de la force publique ; que, toutefois, eu égard aux pouvoirs qui sont ceux du juge des référés dans le cadre de la procédure de l'article L. 521-2 du code de justice administrative, cette circonstance n'est, contrairement à ce que le maire de Champs-sur-Marne soutient, pas de nature à priver d'objet le présent pourvoi ;<br/>
<br/>
              Sur la régularité de l'ordonnance attaquée :<br/>
<br/>
              4. Considérant, d'une part, qu'aux termes du premier alinéa de l'article 7 de la loi du 10 juillet 1991 relative à l'aide juridique : " L'aide juridique est accordée à la personne dont l'action n'apparaît pas manifestement irrecevable ou dénuée de fondement " ; que ces dispositions ont pour objet d'éviter que soient mises à la charge de l'Etat les dépenses afférentes aux actions qui, de manière manifeste, apparaissent dépourvues de toute chance de succès ;<br/>
<br/>
              5. Considérant, d'autre part, qu'en vertu des articles 12 et 20 de la même loi, l'admission à l'aide juridictionnelle est prononcée par un bureau d'aide juridictionnelle ou, en cas d'urgence et à titre provisoire par le président de ce bureau, par la juridiction compétente ou par son président ; qu'aux termes du second alinéa l'article 62 du décret du 19 décembre 1991 : " (...) L'admission provisoire peut être prononcée d'office si l'intéressé a formé une demande d'aide juridictionnelle sur laquelle il n'a pas encore été définitivement statué " ; <br/>
<br/>
              6. Considérant qu'il ressort tant des pièces du dossier soumis au juge des référés que des mentions de l'ordonnance attaquée que Mme A...a sollicité, dans sa demande en référé, l'assistance d'un avocat puis transmis le justificatif d'une demande adressée au bureau d'aide juridictionnelle ; que, dans ces circonstances, en énonçant, après avoir indiqué les raisons pour lesquelles il estimait que la demande en référé était manifestement mal fondée, qu'il ne lui appartenait pas de " désigner un avocat dans la cadre de la présente procédure ", le juge des référés a méconnu sa compétence ; que par suite, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, son ordonnance doit être annulée ;<br/>
<br/>
              7. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au titre de la procédure de référé liberté, en application des dispositions de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              8. Considérant qu'aux termes de l'article L. 2212-2 du code général des collectivités territoriales : " La police municipale a pour objet d'assurer le bon ordre, la sûreté, la sécurité et la salubrité publiques. Elle comprend notamment : (...) 5° Le soin de prévenir, par des précautions convenables, et de faire cesser, par la distribution des secours nécessaires, les accidents et les fléaux calamiteux ainsi que les pollutions de toute nature, tels que les incendies, les inondations, les ruptures de digues, les éboulements de terre ou de rochers, les avalanches ou autres accidents naturels, les maladies épidémiques ou contagieuses, les épizooties, de pourvoir d'urgence à toutes les mesures d'assistance et de secours et, s'il y a lieu, de provoquer l'intervention de l'administration supérieure " ; <br/>
<br/>
              9. Considérant qu'il ressort des pièces du dossier que l'arrêté litigieux a été signé par le maire de la commune et entre dans le champ des pouvoirs dont il dispose en vertu des dispositions du code général des collectivités territoriales citées au point précédent ; que le moyen tiré de l'incompétence de l'auteur de l'acte doit être écarté ;<br/>
<br/>
              10. Considérant qu'il résulte de l'instruction et notamment des constats dressés par deux procès-verbaux de police, que les deux campements dont l'évacuation était ordonnée, installés respectivement en décembre 2014 et février 2015 mais qui avaient connu une croissance importante avec l'arrivée de nouveaux occupants les 28 et 29 mars 2015, comportaient des branchements et des fils électriques défectueux, des feux de camp allumés ainsi que des braseros destinés au chauffage des baraques, lesquelles étaient réalisées en matériaux précaires et inflammables alors que l'accès du terrain, qui est boisé, était difficile pour les services d'incendie et de secours, et que la parcelle occupée était envahie de détritus et déjections divers ; que la requérante, qui ne peut utilement invoquer la circonstance que la commune est chargée de l'élimination des déchets ménagers, n'apporte à l'appui de sa demande en référé aucun élément de nature à établir que ces constatations seraient entachées d'inexactitude ; que, dans l'attente que les occupants du campement puissent être accueillis dans des centres d'hébergement d'urgence ouverts par l'Etat, ils ont été temporairement hébergés dans les vestiaires du club de rugby de la commune ; que, dans les circonstances de l'espèce, eu égard à la gravité des risques encourus par les occupants des campements et nonobstant l'absence de mesure d'accompagnement social autre que la mise à disposition d'hébergements temporaires, l'arrêté litigieux n'est pas entaché d'une méconnaissance manifeste des conditions de nécessité et de proportionnalité au regard des exigences de la sécurité et de la salubrité publiques ; que, compte tenu de la nécessité de sécurité publique justifiant le départ des occupants du campement, il ne porte pas une atteinte manifestement illégale à la dignité, à la liberté d'aller et venir, à l'inviolabilité du domicile, à la vie privée et familiale ni à l'intérêt supérieur des enfants ; <br/>
<br/>
              11. Considérant qu'il résulte de ce qui précède, que Mme A...n'est pas fondée à demander la suspension de l'arrêté attaqué ; que ses conclusions au titre de l'article L. 761-1 du code de justice administrative ne peuvent dès lors qu'être rejetées ; <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'ordonnance du juge des référés du tribunal administratif de Melun du 16 avril 2015 est annulée.<br/>
Article 2 : La demande présentée par Mme A...devant le juge des référés du tribunal administratif de Melun et le surplus des conclusions de son pourvoi sont rejetés.<br/>
Article 3 : La présente décision sera notifiée à Mme B...A...et à la commune de Champs-sur-Marne.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
