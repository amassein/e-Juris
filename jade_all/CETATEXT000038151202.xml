<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038151202</ID>
<ANCIEN_ID>JG_L_2019_02_000000417609</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/15/12/CETATEXT000038151202.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème chambre, 20/02/2019, 417609, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-02-20</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>417609</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>CABINET BRIARD ; HAAS</AVOCATS>
<RAPPORTEUR>M. Pierre Ramain</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Aurélie Bretonneau</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2019:417609.20190220</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La SARL ER Travaux a demandé au tribunal administratif de Nouvelle-Calédonie de prononcer la décharge du rappel de taxe de solidarité sur les services, ainsi que des pénalités y afférentes, auxquels elle a été assujettie au titre de la période du 1er août 2008 au 31 décembre 2010. Par un jugement n° 1400133 du 4 juin 2015, le tribunal administratif de Nouvelle-Calédonie a substitué la pénalité de 40 % pour mauvaise foi à celle pour manoeuvres frauduleuses appliquée au taux de 50 %, a déchargé la société ER Travaux de la différence résultant de cette substitution et a rejeté le surplus de sa demande. <br/>
<br/>
              Par un arrêt n° 15PA03496 du 23 octobre 2017, la cour administrative d'appel de Paris a rejeté l'appel formé par la société ER Travaux contre ce jugement et remis à sa charge, sur appel incident du gouvernement de la Nouvelle-Calédonie, la majoration pour manoeuvres frauduleuses au taux de 50 %.  <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 24 janvier 2018 et 24 avril 2018 au secrétariat du contentieux du Conseil d'Etat, le gouvernement de la Nouvelle-Calédonie demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt en tant qu'il a limité à 50 % le taux de la pénalité pour manoeuvres frauduleuses ; <br/>
<br/>
              2°) réglant l'affaire au fond, de remettre à la charge de la SARL ER Travaux la majoration pour manoeuvres frauduleuses au taux de 80 % prévu par l'article 1054 du code des impôts de la Nouvelle-Calédonie ; <br/>
<br/>
              3°) de mettre à la charge de la SARL ER Travaux la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code des impôts de la Nouvelle-Calédonie ; <br/>
              - le code de justice administrative ;  <br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Pierre Ramain, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Aurélie Bretonneau, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, au Cabinet Briard, avocat du gouvernement de la Nouvelle-Calédonie et à Me Haas, avocat de la SARL ER Travaux ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que la société ER Travaux a fait l'objet d'une vérification de comptabilité à l'issue de laquelle l'administration fiscale de la Nouvelle-Calédonie l'a assujettie à des rappels de taxe de solidarité sur les services assortis d'une majoration pour manoeuvres frauduleuses. Le gouvernement de la Nouvelle-Calédonie se pourvoit en cassation contre l'arrêt de la cour administrative d'appel de Paris du 23 octobre 2017 en tant que, réformant le jugement du tribunal administratif de Nouvelle-Calédonie du 4 juin 2015 qui avait substitué la pénalité pour mauvaise foi à la pénalité pour manoeuvres frauduleuses prévue à l'article 1054 du code des impôts de Nouvelle-Calédonie, il a remis à la charge de la société ER Travaux la majoration pour manoeuvres frauduleuses au taux de 50 % et non au taux de 80 %.<br/>
<br/>
              2. Si le gouvernement de la Nouvelle-Calédonie se prévaut de ce que le taux de la pénalité pour manoeuvres frauduleuses prévue par l'article 1054 du code des impôts de Nouvelle-Calédonie et infligée à la société ER Travaux est de 80 %, il ressort des écritures d'appel que le gouvernement de la Nouvelle-Calédonie avait demandé à la cour administrative d'appel de Paris, au titre de son appel incident, le rétablissement de la pénalité pour manoeuvres frauduleuses au taux de 50%. En faisant droit aux conclusions présentées par le gouvernement de la Nouvelle-Calédonie dont elle n'a pas dénaturé la portée, la cour administrative d'appel de Paris lui a donné entièrement satisfaction. Il résulte de ce qui précède que le gouvernement de la Nouvelle-Calédonie n'a pas intérêt à contester l'arrêt qu'il attaque. Son pourvoi doit donc être rejeté comme irrecevable. <br/>
<br/>
              3. Il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par la SARL ER Travaux au titre de l'article L. 761-1 du code de justice administrative. Ces dispositions font obstacle à ce qu'une somme soit mise à ce titre à la charge de la SARL ER Travaux qui n'est pas, dans la présente instance, la partie perdante. <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : Le pourvoi du gouvernement de la Nouvelle-Calédonie est rejeté. <br/>
Article 2 : Les conclusions présentées par la SARL ER Travaux au titre de l'article L. 761-1 du code de justice administrative sont rejetées.  <br/>
Article 3 : La présente décision sera notifiée au gouvernement de la Nouvelle-Calédonie et à la SARL ER Travaux. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
