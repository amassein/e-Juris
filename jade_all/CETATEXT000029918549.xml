<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029918549</ID>
<ANCIEN_ID>JG_L_2014_12_000000369037</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/91/85/CETATEXT000029918549.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème et 1ère sous-sections réunies, 17/12/2014, 369037, Publié au recueil Lebon</TITRE>
<DATE_DEC>2014-12-17</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>369037</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème et 1ère sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BARTHELEMY, MATUCHANSKY, VEXLIARD, POUPOT</AVOCATS>
<RAPPORTEUR>Mme Sophie Roussel</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Xavier De Lesquen</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:369037.20141217</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 4 juin et 4 septembre 2013 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. B...A..., demeurant ... ; M. A...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler la décision n° 2008-05 D du 4 avril 2013 par laquelle le Haut conseil du commissariat aux comptes a confirmé l'irrecevabilité son recours en révision contre la décision du 6 février 2008 de la chambre régionale de discipline des commissaires aux comptes près la cour d'appel de Paris ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à ses conclusions d'appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 6 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
<br/>
              Vu le code de commerce ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Sophie Roussel, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Xavier de Lesquen, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Barthélemy, Matuchansky, Vexliard, Poupot, avocat de M. A...;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'en vertu d'une règle générale de procédure découlant des exigences de la bonne administration de la justice, la voie du recours en révision est ouverte devant les juridictions administratives qui ne relèvent pas du code de justice administrative et pour lesquelles aucun texte n'a prévu un tel recours ; que le recours en révision peut alors être formé à l'égard d'une décision passée en force de chose jugée, dans l'hypothèse où cette décision l'a été sur pièces fausses ou si elle l'a été faute pour la partie perdante d'avoir produit une pièce décisive qui était retenue par son adversaire ; que cette possibilité est ouverte à toute partie à l'instance, dans un délai de deux mois courant, en principe, à compter du jour où la partie a eu connaissance de la cause de révision qu'elle invoque ;<br/>
<br/>
              2. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que M. A...a introduit le 13 juin 2007, devant la chambre régionale de discipline de Paris, un recours en révision contre la décision du 1er décembre 2004 lui infligeant une sanction disciplinaire d'avertissement, assortie d'une inéligibilité de trois ans dans les organes de représentation de la profession ; que par une décision du 6 février 2008, confirmée par une décision du 25 juin 2009 du Haut conseil du commissariat aux comptes, la chambre régionale de discipline a rejeté son recours comme irrecevable, faute de texte le prévoyant ; que, par une décision du 16 mai 2012, le Conseil d'Etat, statuant au contentieux, a annulé cette décision et renvoyé l'affaire au Haut conseil du commissariat aux comptes ; que M. A...se pourvoit en cassation contre la décision du 4 avril 2013 par laquelle cette juridiction a de nouveau rejeté son recours ;<br/>
<br/>
              Sur la régularité de la décision attaquée :<br/>
<br/>
              3. Considérant que si l'un des membres de la formation disciplinaire du Haut conseil du commissariat aux comptes ayant délibéré sur la décision du 4 avril 2013 attaquée était, à cette même date, directeur du contrôle de gestion de la société Cap Gemini, et si le cabinet dont M. A...était l'un des associés était, jusqu'en 2001, l'un des commissaires aux comptes de cette société, ce seul fait ne permet pas, par lui-même, de caractériser une violation du principe d'impartialité, eu égard au délai écoulé entre 2001 et 2013 et à l'absence de circonstance particulière alléguée ; <br/>
<br/>
              Sur le bien fondé de la décision attaquée :<br/>
<br/>
              4. Considérant que s'il appartenait au Haut conseil du commissariat aux comptes, statuant à nouveau sur l'appel formé par M. A...contre la décision du 6 février 2008 de la chambre régionale de discipline ayant rejeté son recours en révision, d'appliquer les règles rappelées ci-dessus, il ne pouvait, sans porter atteinte au droit au recours de M.A..., opposer à ce dernier la circonstance que le délai de deux mois aurait couru à compter du 17 mars 2007, jour où l'intéressé aurait eu connaissance de la cause de révision qu'il invoquait, dès lors que cette règle de forclusion a été énoncée pour la première fois par la décision du Conseil d'Etat du 16 mai 2012 ; que M. A...est par suite fondé à soutenir que la décision attaquée est, sur ce point, entachée d'erreur de droit ; <br/>
<br/>
              5. Considérant toutefois que, pour rejeter l'appel de M.A..., le Haut conseil du commissariat aux comptes s'est également fondé sur un autre motif, qui n'était pas surabondant, tiré de ce que le recours en révision introduit par le requérant ne répondait pas aux autres conditions rappelées au point 1, dès lors qu'il n'était pas établi que le courrier du 23 février 1995, sur lequel il avait fondé son recours en révision, eût été décisif pour rapporter la preuve que la situation d'incompatibilité reprochée à M. A...avait pris fin ; que, dès lors qu'il ne ressort pas des pièces du dossier qui lui était soumis que le projet de transaction évoqué dans ce courrier, mettant fin à la situation d'incompatibilité à l'origine des poursuites disciplinaires, se fût réalisé, le Haut conseil du commissariat aux comptes n'a pas, en statuant ainsi, donné aux faits qu'il a souverainement appréciés, sans les dénaturer, une inexacte qualification juridique ; que ce motif justifie à lui seul le rejet, par la décision attaquée, du recours en révision introduit par M. A...;<br/>
<br/>
              6. Considérant qu'il résulte de ce qui précède que M. A...n'est pas fondé à demander l'annulation de cette décision ; que ses conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative ne peuvent, par suite, qu'être rejetées ; <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de M. A...est rejeté. <br/>
<br/>
      Article 2 : La présente décision sera notifiée à M. B...A....<br/>
Copie en sera adressée pour information au Haut conseil du commissariat aux comptes.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-07 PROCÉDURE. POUVOIRS ET DEVOIRS DU JUGE. - APPLICATION D'UNE NOUVELLE RÈGLE JURISPRUDENTIELLE AUX INSTANCES EN COURS - EXISTENCE EN PRINCIPE - LIMITE - ATTEINTE, EN L'ESPÈCE, AU DROIT AU RECOURS DE L'INTÉRESSÉ [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-08-02-02-01-02 PROCÉDURE. VOIES DE RECOURS. CASSATION. CONTRÔLE DU JUGE DE CASSATION. BIEN-FONDÉ. QUALIFICATION JURIDIQUE DES FAITS. - CONDITIONS AUQUEL EST SOUMIS LE RECOURS EN RÉVISION OUVERT SANS TEXTE [RJ2].
</SCT>
<ANA ID="9A"> 54-07 Par une décision n° 331346 du 16 mai 2012, le Conseil d'Etat, statuant au contentieux, avait, sur recours du requérant, jugé qu'en vertu d'une règle générale de procédure la voie du recours en révision est ouverte devant les juridictions administratives ne relevant pas du code de justice administrative et doit être exercée dans un délai de deux mois à compter du jour où la partie a eu connaissance de la cause de révision qu'elle invoque. Par suite, le Conseil d'Etat avait cassé la décision du Haut conseil du commissariat aux comptes (HCCC) qui avait, au motif qu'aucun texte ne prévoyait un tel recours, rejeté le recours en révision présenté par le requérant.... ,,Sur renvoi du Conseil d'Etat, le HCCC avait opposé ce délai de recours au requérant. S'il appartenait au HCCC, statuant à nouveau sur l'appel formé par le requérant, d'appliquer les règles dégagées par la décision du 16 mai 2012, il ne pouvait, sans porter atteinte au droit au recours de l'intéressé, opposer à ce dernier la circonstance que le délai de deux mois aurait couru à compter du 17 mars 2007, jour où l'intéressé aurait eu connaissance de la cause de révision qu'il invoquait, dès lors que cette règle de forclusion a été énoncée pour la première fois par la décision du Conseil d'Etat du 16 mai 2012.</ANA>
<ANA ID="9B"> 54-08-02-02-01-02 Le recours en révision ouvert sans texte, en vertu d'une règle générale de procédure, devant les juridictions administratives qui ne relèvent pas du code de justice administrative, peut être formé à l'égard d'une décision passée en force de chose jugée, dans l'hypothèse où cette décision l'a été sur pièces fausses ou si elle l'a été faute pour la partie perdante d'avoir produit une pièce décisive qui était retenue par son adversaire....  ,,Le juge de cassation exerce un contrôle de qualification juridique sur l'appréciation que porte le juge du fond sur les conditions auxquelles est subordonné le recours en révision.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr. CE, 22 octobre 2014, Centre hospitalier de Dinan c/ Consorts Etienne, n° 368904, à publier au recueil Lebon. Comp., pour un cas où la décision module elle-même les effets dans le temps de la règle jurisprudentielle, CE, Assemblée, 16 juillet 2007, Société Tropic Travaux Signalisation, n° 291545, p. 360.,,[RJ2]Cf., sur l'existence du recours, CE, Section, 16 mai 2012, M. Serval, p. 225.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
