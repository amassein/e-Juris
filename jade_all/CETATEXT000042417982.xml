<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042417982</ID>
<ANCIEN_ID>JG_L_2020_10_000000432061</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/41/79/CETATEXT000042417982.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème - 6ème chambres réunies, 08/10/2020, 432061, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-10-08</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>432061</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème - 6ème chambres réunies</FORMATION>
<TYPE_REC/>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DELVOLVE ET TRICHET</AVOCATS>
<RAPPORTEUR>M. Olivier Rousselle</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Cécile Barrois de Sarigny</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:432061.20201008</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B... A... a demandé au tribunal administratif de Versailles de condamner l'Etat à lui verser une somme de 46 000 euros, majorée des intérêts légaux à compter du 22 février 2016, en réparation des préjudices qu'il estime avoir subis en raison de l'absence d'offre d'hébergement de la part du préfet des Yvelines. Par un jugement n° 1607881 du 14 mars  2019, le tribunal administratif a rejeté sa demande.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 28 juin, 27 septembre 2019 et 11 septembre 2020, au secrétariat du contentieux du Conseil d'Etat, M. A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 500 euros à verser à la SCP Delvolvé Trichet, son avocat, au titre de l'article L. 761-1 du code de justice administrative et de l'article 37 de la loi du 10 juillet 1991.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de la construction et de l'habitation ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Olivier Rousselle, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Cécile Barrois de Sarigny, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Delvolvé et Trichet, avocat de M. A....<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. Le II de l'article L. 441-2-3 du code de la construction et de l'habitation prévoit les conditions dans lesquelles une commission de médiation peut être saisie d'une demande de logement locatif social. Aux termes du III du même article : " La commission de médiation peut également être saisie, sans condition de délai, par toute personne qui, sollicitant l'accueil dans une structure d'hébergement, un logement de transition, un logement-foyer ou une résidence hôtelière à vocation sociale, n'a reçu aucune proposition adaptée en réponse à sa demande (...) ". Aux termes du IV du même article : " Lorsque la commission de médiation est saisie d'une demande de logement dans les conditions prévues au II et qu'elle estime, au vu d'une évaluation sociale, que le demandeur est prioritaire mais qu'une offre de logement n'est pas adaptée, elle transmet au représentant de l'Etat dans le département (...) cette demande pour laquelle il doit être proposé un accueil dans une structure d'hébergement, un établissement ou un logement de transition, un logement-foyer ou une résidence hôtelière à vocation sociale ".<br/>
<br/>
              2. Il ressort des pièces du dossier soumis aux juges du fond que, après avoir formé une demande de logement sur le fondement du II de l'article L. 441-2-3 du code de la construction et de l'habitation, M. A... a été, par application des dispositions, citées ci-dessus, du IV de cet article, reconnu par la commission de médiation des Yvelines comme prioritaire et  devant être accueilli dans une structure d'hébergement, un logement de transition, un logement-foyer ou une résidence hôtelière à vocation sociale, par une décision du 4 avril 2014. Par un jugement du 23 avril 2015, le tribunal administratif de Versailles, saisi par M. A... sur le fondement du I de l'article L. 441-2-3-1 du code de la construction et de l'habitation, a enjoint au préfet des Yvelines d'assurer son accueil dans une de ces structures. Faute d'en avoir bénéficié, M. A... a alors demandé au même tribunal de condamner l'Etat à lui verser une somme de 46 000 euros en réparation des préjudices qu'il estimait avoir subis. Il se pourvoit en cassation contre le jugement du 14 mars 2019 par lequel le tribunal administratif a rejeté sa demande.<br/>
<br/>
              3. Lorsqu'une personne a été reconnue comme prioritaire et devant être accueillie dans une structure d'hébergement, un logement de transition, un logement-foyer ou une résidence hôtelière à vocation sociale par une commission de médiation, en application des dispositions du III ou du IV de l'article L. 441-2-3 du code de la construction et de l'habitation, la carence fautive de l'Etat à exécuter cette décision dans le délai imparti engage sa responsabilité à l'égard du demandeur au titre des troubles dans les conditions d'existence résultant du maintien de la situation qui a motivé la décision de la commission. La période de responsabilité de l'Etat court à compter de l'expiration du délai de six semaines que l'article R. 441-18 du même code impartit au préfet, à compter de la décision de la commission de médiation, pour proposer un accueil dans une structure d'hébergement, un logement de transition, un logement-foyer ou une résidence hôtelière à vocation sociale, ce délai étant porté à trois mois si la décision de la commission spécifie que l'accueil ne peut être proposé que dans un logement de transition ou dans un logement-foyer. Les troubles dans les conditions d'existence doivent être appréciés en tenant notamment compte des conditions d'hébergement ou de logement qui ont perduré du fait de la carence de l'Etat, de la durée de cette carence et du nombre de personnes composant le foyer du demandeur pendant la période de responsabilité de l'Etat.<br/>
<br/>
              4. En premier lieu, il ressort des termes mêmes du jugement attaqué que, pour écarter l'existence d'un préjudice indemnisable et rejeter la demande de M. A..., le tribunal s'est fondé sur ce que, compte tenu des conditions dans lesquelles il avait pu être hébergé chez des proches, la carence de l'Etat à mettre en oeuvre la décision de la commission de médiation ne lui avait pas causé un préjudice significatif dans ses conditions d'existence. Il résulte de ce qui a été dit ci-dessus qu'en écartant ainsi l'existence de tout préjudice résultant de la carence fautive de l'Etat, alors que, l'intéressé avait fait l'objet d'une décision prise sur le fondement du IV de l'article L. 441-2-3 du code de la construction et de l'habitation, le tribunal a entaché son jugement d'une erreur de droit.<br/>
<br/>
              5. En second lieu, il ressort également des termes du jugement attaqué que le tribunal administratif s'est, au surplus, fondé sur la circonstance que M. A... avait, postérieurement à la décision de la commission de médiation, demandé, lors d'un entretien avec le service intégré d'accueil et d'orientation (SIAO), que lui soit attribué un logement et qu'il avait signé en ce sens une " attestation de refus de réorientation vers le volet hébergement ". En se bornant à relever  cette déclaration de M. A..., sans se fonder soit sur un refus d'offre d'hébergement de sa part qui, faute d'obéir à un motif impérieux, était de nature à lui faire perdre le bénéfice de la décision de la commission de médiation, soit sur un comportement de l'intéressé de nature à faire obstacle à l'exécution de cette décision et alors que de surcroît, l'Etat n'aurait pu en tout état de cause être  exonéré de sa responsabilité que pour la période postérieure à cet entretien avec le SIAO, le tribunal administratif a commis une autre erreur de droit.<br/>
<br/>
              6. Il résulte de tout ce qui précède que M. A... est, sans qu'il soit besoin de se prononcer sur les autres moyens de son pourvoi, fondé à demander l'annulation du jugement qu'il attaque.<br/>
<br/>
              7. M. A... a obtenu le bénéfice de l'aide juridictionnelle. Par suite, son avocat peut se prévaloir des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 relative à l'aide juridique. Il y a lieu, dans les circonstances de l'espèce, et sous réserve que cette société renonce à percevoir la somme correspondant à la part contributive de l'Etat, de mettre à la charge de l'Etat la somme de 2 000 euros à verser à la SCP Delvolvé-Trichet, avocat de M. A....<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du 14 mars 2019 du tribunal administratif de Versailles est annulé.<br/>
		Article 2 : L'affaire est renvoyée au tribunal administratif de Versailles.<br/>
Article 3 : L'Etat versera à la SCP Delvolvé-Trichet une somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative et de l'article 37 de la loi du 10 juillet 1991, sous réserve que cette société renonce à percevoir la somme correspondant à la part contributive de l'Etat.<br/>
Article 4 : La présente décision sera notifiée à M. B... A... et à la ministre de la transition écologique.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
