<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027377278</ID>
<ANCIEN_ID>JG_L_2013_04_000000357574</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/37/72/CETATEXT000027377278.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème / 3ème SSR, 29/04/2013, 357574, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-04-29</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>357574</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème / 3ème SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DEFRENOIS, LEVIS</AVOCATS>
<RAPPORTEUR>M. Maxime Boutron</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Benoît Bohnert</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:357574.20130429</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 13 mars et 11 juin 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. et Mme A... B..., demeurant... ; ils demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'article 3 de l'arrêt n° 09PA07116 du 12 janvier 2012 par lequel la cour administrative d'appel de Paris, après avoir annulé l'article 2 du jugement n° 0504059, 0608875, 0703066, 0716242 du tribunal administratif de Paris du 12 novembre 2009 en tant qu'il rejetait leurs demandes sans constater que leurs conclusions étaient devenues sans objet à concurrence de la somme de 3 195 108 euros, a rejeté le surplus de leurs conclusions tendant à la décharge des cotisations d'impôt sur le revenu et de contributions sociales auxquelles ils ont été assujettis au titre des années 2001 à 2006 ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à leur appel ;  <br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 27 mars 2013, présentée pour M. et Mme B... ; <br/>
<br/>
              Vu la Constitution, notamment son préambule  et son article 61-1 ;<br/>
<br/>
              Vu le traité instituant la Communauté européenne ;<br/>
<br/>
              Vu le traité sur le fonctionnement de l'Union européenne ;<br/>
<br/>
              Vu la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales et son protocole additionnel n° 4 ;<br/>
<br/>
              Vu la convention entre la République française et la Confédération suisse en vue d'éviter les doubles impositions en matière d'impôts sur le revenu et sur la fortune du 9 septembre 1966 ;<br/>
<br/>
              Vu l'accord entre la Communauté européenne et ses Etats membres, d'une part, et la Confédération suisse, d'autre part, sur la libre circulation des personnes, fait à Luxembourg le 21 juin 1999 ;<br/>
<br/>
              Vu l'ordonnance n° 58-1067 du 7 novembre 1958 ; <br/>
<br/>
              Vu le code de la sécurité sociale ;<br/>
<br/>
              Vu le code général des impôts et le livre des procédures fiscales ;<br/>
<br/>
              Vu la loi n° 2004-1484 du 30 décembre 2004 ;<br/>
<br/>
              Vu la décision du 17 juillet 2012 par laquelle le Conseil d'Etat statuant au contentieux n'a pas renvoyé au Conseil constitutionnel les questions prioritaires de constitutionnalité soulevées par M. et MmeB... ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Maxime Boutron, Auditeur,  <br/>
<br/>
              - les conclusions de M. Benoît Bohnert, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Defrenois, Levis, avocat de M. et Mme B...;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que M. et Mme B...ont transféré leur domicile de France vers la Suisse le 23 avril 2001 et sont devenus résidents de cet Etat ; qu'au moment de leur départ, ils ont déposé une déclaration de plus-values latentes sur les titres de participation qu'ils détenaient et ont, en application du II de l'article 167 bis du code général des impôts, obtenu un sursis de paiement des impositions auxquelles ils ont été assujettis ; qu'au cours des années 2001 à 2006, ils ont cédé des titres, ce qui a rendu immédiatement exigibles les impositions correspondantes ; qu'ils ont adressé plusieurs réclamations à l'administration fiscale afin d'obtenir le dégrèvement de la totalité des impositions résultant de ces plus-values ; que l'administration n'ayant pas fait entièrement droit à  ces réclamations, les requérants ont saisi le tribunal administratif de Paris, qui,  par un jugement du 12 novembre 2009, n'a que partiellement fait droit à leurs demandes tendant à la décharge des impositions demeurant... ; que, sous le même numéro, ils se pourvoient en cassation contre l'article 3 de l'arrêt du 12 janvier 2012 de la cour administrative d'appel de Paris qui a rejeté le surplus des conclusions de leur requête et ils contestent l'ordonnance du 25 mars 2010 par laquelle le président de la septième chambre de la cour a rejeté leur demande tendant à ce que soit transmise au Conseil d'Etat la question de la conformité aux droits et libertés garantis par la Constitution de l'article 167 bis du code général des impôts, tel qu'il était issu de l'article 24 de la loi du 30 décembre 1998 de finances pour 1999, et du II de l'article 19 de la loi du 30 décembre 2004 de finances pour 2005  ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article 167 bis du code général des impôts, dans sa rédaction en vigueur à la date des impositions en litige : " I. 1. Les contribuables fiscalement domiciliés en France pendant au moins six années au cours des dix dernières années sont imposables, à la date du transfert de leur domicile hors de France, au titre des plus-values constatées sur les droits sociaux mentionnés à l'article  150-0 A et détenus dans les conditions du f de l'article 164 B (...)/ II. 1. Le paiement de l'impôt afférent à la plus-value constatée peut être différé jusqu'au moment où s'opérera la transmission, le rachat, le remboursement ou l'annulation des droits sociaux concernés. (...)/ 3 (...) L'impôt acquitté localement par le contribuable et afférent à la plus-value effectivement réalisée hors de France est imputable sur l'impôt sur le revenu établi en France à condition d'être comparable à cet impôt (...)/ III. A l'expiration d'un délai de cinq ans suivant la date du départ ou à la date à laquelle le contribuable transfère de nouveau son domicile en France si cet événement est antérieur, l'impôt établi en application du I est dégrevé d'office en tant qu'il se rapporte à des plus-values afférentes aux droits sociaux qui, à cette date, demeurent en litige(... " ;  <br/>
<br/>
              Sur l'ordonnance du 25 mars 2010 :<br/>
<br/>
              3. Considérant que les dispositions de l'article 23-2 de l'ordonnance portant loi organique du 7 novembre 1958 prévoient que, lorsqu'une juridiction relevant du Conseil d'Etat est saisie de moyens contestant la conformité d'une disposition législative aux droits et libertés garantis par la Constitution, elle transmet au Conseil d'Etat la question de constitutionnalité ainsi posée à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et qu'elle ne soit pas dépourvue de caractère sérieux ;<br/>
<br/>
              4. Considérant qu'aux termes de l'article 19 de la loi du 30 décembre 2004 de finances pour 2005 : " I. - Le 1 bis de l'article 167 et l'article 167 bis du code général des impôts sont abrogés. / II. - Les dispositions du I sont applicables aux contribuables qui transfèrent leur domicile hors de France à compter du 1er janvier 2005 " ; <br/>
<br/>
              5. Considérant que M. et  Mme B...ont notamment demandé à la cour que soit transmise au Conseil d'Etat la question de la conformité du II de l'article 19 de la loi du 30 décembre 2004 en soutenant qu'en limitant les effets de l'abrogation des dispositions de l'article 167 bis du code général des impôts aux contribuables qui transfèrent leur domicile hors de France à compter du 1er janvier 2005, cette disposition a institué une différence de traitement injustifiée entre les contribuables selon la date du transfert de leur domicile à l'étranger et ainsi méconnu le principe d'égalité devant les charges publiques garanti par l'article 13 de la Déclaration des droits de l'homme et du citoyen ; <br/>
<br/>
              6. Considérant que la cour a commis une erreur de droit en jugeant que ces dispositions n'étaient pas applicables au litige au sens et pour l'application de l'article 23-4 de l'ordonnance du 7 novembre 1958 au motif que les requérants avaient transféré leur domicile en Suisse en 2001, alors qu'ils avaient fait l'objet d'une imposition sur le fondement de l'article 167 bis dans sa rédaction en vigueur jusqu'à son abrogation par l'article 19 de la loi de finances pour 2005 au titre des plus-values constatées à l'occasion de ce transfert de domicile fiscal ; qu'en conséquence, M. et Mme B...sont fondés à demander l'annulation de l'ordonnance attaquée en tant qu'elle porte sur le refus de transmettre la question de la conformité aux droits et libertés garantis par la Constitution du II de l'article 19 de la loi du 30 décembre 2004 ; <br/>
<br/>
              7. Considérant qu'il y a lieu, en application des dispositions de l'article L. 821-2 du code de justice administrative, d'examiner immédiatement la question prioritaire de constitutionnalité soulevée devant la cour ;<br/>
<br/>
              8. Considérant qu'il résulte des dispositions de l'article 23-5 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel  que le Conseil constitutionnel est saisi de la question prioritaire de constitutionnalité par le Conseil d'Etat à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux ;<br/>
<br/>
              9. Considérant, en premier lieu, que si les requérants soutiennent que le II de l'article 19 de la loi du 30 décembre 2004 méconnaît le principe d'égalité devant les charges publiques garanti par l'article 13 de la Déclaration des droits de l'homme et du citoyen, il est à tout moment loisible au législateur, statuant dans le domaine de sa compétence, de modifier des textes antérieurs ou d'abroger ceux-ci en leur substituant, le cas échéant, d'autres dispositions ; que, ce faisant, il ne saurait toutefois priver de garanties légales des exigences constitutionnelles ; que la différence de traitement qui résulte de la succession de deux régimes juridiques dans le temps n'est pas, en elle-même, contraire au principe d'égalité devant les charges publiques ; <br/>
<br/>
              10. Considérant, en second lieu, que les requérants soutiennent, d'une part, qu'en limitant les effets de l'abrogation des dispositions de l'article 167 bis du code général des impôts aux contribuables qui transfèrent leur domicile hors de France à compter du 1er janvier 2005, l'article 19 de la loi du 30 décembre 2004 a méconnu le principe, garanti par l'article 16 de la Déclaration des droits de l'homme et du citoyen, selon lequel le législateur doit assurer le respect de la Constitution et, d'autre part, que le législateur a méconnu le principe de la présomption d'innocence garanti par l'article 9 de la Déclaration des droits de l'homme et du citoyen en s'abstenant de supprimer l'article 167 bis du code général des impôts pour les contribuables ayant transféré leur domicile fiscal hors de France avant le 1er janvier 2005 ; que, par sa décision n° 357574 du 17 juillet 2012, le Conseil d'Etat a décidé qu'il n'y avait pas lieu de renvoyer la question prioritaire de constitutionnalité soulevée par M. et Mme B...et portant sur les mêmes moyens ; qu'il n'y a pas lieu, en conséquence, de renvoyer au Conseil constitutionnel la question  prioritaire de constitutionnalité  soulevée devant la cour ; <br/>
<br/>
              11. Considérant qu'il résulte de ce qui précède que la question de la conformité aux droits et libertés garantis par la Constitution du II de l'article 19 de la loi du 30 décembre 2004 de finances pour 2005, qui n'est pas nouvelle, ne présente pas un caractère sérieux ; <br/>
<br/>
              Sur l'arrêt du 12 janvier 2012 :<br/>
<br/>
              En ce qui concerne l'impôt sur le revenu : <br/>
<br/>
              12. Considérant, en premier lieu, qu'en vertu de l'article 55 de la Constitution, les traités ou accords régulièrement ratifiés ou approuvés ont, dès leur publication, une autorité supérieure à celle des lois, sous réserve, pour chaque accord ou traité, de son application par l'autre partie ; qu'aux termes de l'article 2-2 du protocole n° 4 à la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales : " Toute personne est libre de quitter n'importe quel pays, y compris le sien " ;  <br/>
<br/>
              13. Considérant que l'article 167 bis du code général des impôts prévoyait l'imposition immédiate, en cas de transfert du domicile fiscal du contribuable hors de France, des plus-values constatées sur les valeurs mobilières qu'il détenait lorsque l'ensemble des droits détenus par sa famille et lui-même dans les bénéfices d'une société avait dépassé 25 % de ces bénéfices au cours des cinq années précédentes ; que cet article n'avait ni pour objet, ni pour effet de soumettre à de quelconques restrictions ou conditions l'exercice effectif, par les personnes qu'elles visent, de la  liberté d'aller et venir, et notamment de quitter le territoire pour s'installer dans un autre Etat ; que, par suite, en jugeant que l'article 167 bis ne méconnaissait pas les stipulations de l'article 2-2 du protocole additionnel n° 4 à la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, la cour n'a pas commis d'erreur de droit ;<br/>
<br/>
              14. Considérant, en second lieu, qu'aux termes du 4 de l'article 4 de la convention fiscale conclue entre la France et la Suisse en date du 9 septembre 1966 : " Lorsqu'une personne physique a transféré définitivement son domicile d'un Etat contractant dans l'autre, elle cesse d'être assujettie dans le premier Etat aux impôts pour lesquels le domicile fait règle dès l'expiration du jour où s'est accompli le transfert du domicile. L'assujettissement aux impôts pour lesquels le domicile fait règle commence dans l'autre Etat à compter de la même date " ; qu'aux termes du 5 de l'article 15 de cette même convention : " Les gains provenant de l'aliénation de tous biens autres que ceux qui sont mentionnés aux paragraphes 1, 2 et 3 ne sont imposables que dans l'Etat dont le cédant est un résident " ;<br/>
<br/>
              15. Considérant que les stipulations du 5 de l'article 15 de cette convention, qui réservent à l'Etat de résidence l'imposition des plus-values réalisées lors de la cession de valeurs mobilières qui ne relèvent pas du 2 ou du 3 de cet article, ne font pas obstacle à ce qu'une personne ayant son domicile fiscal en France soit imposée sur les plus-values constatées lorsqu'elle transfère son domicile fiscal hors de France ; que, par suite, M. et Mme B...qui, en application du 4 de l'article 4 de cette convention, conservaient la qualité de résidents en France jusqu'à l'expiration du jour où s'est accompli le transfert de leur domicile en Suisse, ne sont pas fondés à soutenir que la cour a commis une erreur de droit en jugeant que l'article 15 de la convention ne faisait pas obstacle à l'imposition en France de leurs plus-values latentes ; qu'en statuant ainsi, et alors qu'elle avait relevé qu'en application du  III de l'article 167 bis du code général des impôts, l'impôt établi sur le fondement du I du même article est dégrevé d'office dans la mesure où les titres n'étaient pas cédés dans les cinq années consécutives au départ, la cour n'a pas entaché son arrêt de contradiction de motifs ;<br/>
<br/>
              En ce qui concerne les contributions sociales :<br/>
<br/>
              16. Considérant, en premier lieu,  qu'en vertu du e) des articles L. 136-6 du code de la sécurité sociale et 1600-0 C du code général des impôts, les personnes physiques fiscalement domiciliées en France au sens de l'article 4 B du code général des impôts sont assujetties à une contribution sur les revenus du patrimoine assise sur le montant net retenu pour l'établissement de l'impôt sur le revenu des plus-values soumis à l'impôt sur le revenu à un taux proportionnel ; qu'en vertu de l'article 1600-0 F bis du code général des impôts, les personnes physiques fiscalement domiciliées en France au sens de l'article 4 B du même code sont assujetties à un prélèvement sur les revenus et les sommes visés à l'article 1600-0 C ; qu'aux termes de l'article 1600-0 G du même code, les personnes physiques désignées à l'article L. 136-1 du code de la sécurité sociale sont assujetties à une contribution perçue à compter de 1996 et assise sur les revenus du patrimoine définis au I de l'article L. 136-6 du même code ; que l'ensemble de ces dispositions, qui n'établissent aucune distinction entre les plus-values constatées et les plus-values effectivement réalisées, s'appliquent en conséquence aux plus-values mentionnées à l'article 167 bis du code général des impôts ;  <br/>
<br/>
              17. Considérant, en deuxième lieu, qu'ainsi qu'il a été dit au point 15, M. et Mme B... conservaient la qualité de résidents en France jusqu'à l'expiration du jour où s'est accompli le transfert de leur domicile en Suisse ; qu'au moment de l'imposition, lors de ce transfert, de la plus-value latente constatée sur le territoire français en application des dispositions de l'article 167 bis du code général des impôts, ils étaient fiscalement domiciliés en France ; que, les dispositions relatives aux contributions sociales étant applicables aux plus-values latentes,  la cour n'a pas commis d'erreur de droit en ne déchargeant pas les requérants de ces impositions ;<br/>
<br/>
              18. Considérant, enfin,  que les moyens tirés de ce que les dispositions relatives aux contributions sociales méconnaitraient l'article 2-2 du protocole additionnel n° 4 à la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, l'accord du 21 juin 1999 entre la Communauté européenne et ses Etats membres, d'une part, et la Confédération suisse, d'autre part, sur la libre circulation des personnes et le  principe de liberté d'établissement garanti alors par l'article  43 du traité instituant la Communauté européenne et repris à l'article 49 du traité sur le fonctionnement de l'Union européenne, qui n'ont pas été soulevés devant la cour et ne sont pas d'ordre public, sont nouveaux en cassation et par suite sans influence sur le bien-fondé de l'arrêt ; <br/>
<br/>
              19. Considérant qu'il résulte de ce qui précède que  M. et Mme B...ne sont pas fondés à demander l'annulation de l'arrêt attaqué ; <br/>
<br/>
              20. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante ;  <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'ordonnance du 25 mars 2010 du président de la septième chambre de la cour administrative d'appel de Paris est annulée en tant qu'elle porte sur le refus de transmettre au Conseil d'Etat la question de la conformité aux droits et libertés garantis par la Constitution du II de l'article 19 de la loi du 30 décembre 2004 de finances pour 2005 .<br/>
Article 2 : Il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée par M. et MmeB.en litige<br/>
Article 3 : Le surplus des conclusions du pourvoi est rejeté.<br/>
 Article 4 : La présente décision sera notifiée à M. et Mme A...B...et au ministre de l'économie et des finances.<br/>
Copie en sera adressée au Premier ministre et au Conseil constitutionnel.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
