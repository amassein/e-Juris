<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032892407</ID>
<ANCIEN_ID>JG_L_2016_07_000000382872</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/89/24/CETATEXT000032892407.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème - 4ème chambres réunies, 13/07/2016, 382872</TITRE>
<DATE_DEC>2016-07-13</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>382872</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème - 4ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BARADUC, DUHAMEL, RAMEIX</AVOCATS>
<RAPPORTEUR>M. Jean-Dominique Langlais</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Laurence Marion</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2016:382872.20160713</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Mme A...B..., agissant en son nom propre et en qualité de représentant légal de ses deux enfants mineurs, a demandé au tribunal administratif de Nice de condamner l'Etat à les indemniser des préjudices ayant résulté de l'absence de proposition d'un logement adapté à ses besoins et capacités. Par un jugement n° 1302228 du 31 mars 2014, le tribunal administratif n'a que partiellement fait droit à sa demande en condamnant l'Etat à lui verser la somme de 1 500 euros et en rejetant les conclusions présentées au nom de ses enfants.<br/>
<br/>
              Par une ordonnance n° 14MA02752 du 30 juin 2014, enregistrée au secrétariat du contentieux du Conseil d'Etat le 21 juillet 2014, le président de la cour administrative d'appel de Marseille a transmis au Conseil d'Etat, en application de l'article R. 351-2 du code de justice administrative, le pourvoi, enregistré le 2 juin 2014 au greffe de la cour administrative d'appel de Marseille, présenté devant cette cour par MmeB....<br/>
<br/>
              Par ce pourvoi et par un nouveau mémoire, enregistré le 18 décembre 2014 au secrétariat du contentieux du Conseil d'Etat, Mme B...demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler le jugement du tribunal administratif de Nice en tant qu'il rejette les conclusions présentées au nom de ses enfants ;<br/>
<br/>
              2°) réglant l'affaire au fond, de condamner l'Etat à lui verser la somme de 8 500 euros en qualité de représentante de ses enfants mineurs ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 000 euros à verser à son avocat, la SCP Baraduc, Duhamel, Rameix au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
<br/>
              - le code de la construction et de l'habitation ; <br/>
<br/>
              - la loi n° 91-647 du 10 juillet 1991 ; <br/>
<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Dominique Langlais, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Laurence Marion, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Baraduc, Duhamel, Rameix, avocat de Mme A...B...;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que Mme B... a été reconnue comme prioritaire et devant être relogée en urgence, sur le fondement de l'article L. 441-2-3 du code de la construction et de l'habitation, par une décision du 20 décembre 2011 de la commission de médiation des Alpes-Maritimes ; que, par un jugement du 20 décembre 2012, le tribunal administratif de Nice a enjoint au préfet des Alpes-Maritimes, sur le fondement de l'article L. 441-2-3-1 du même code, de lui fournir un logement de type T3 dans un délai de dix jours ; que Mme B...a ensuite demandé au tribunal administratif de Nice, en son nom propre et au nom de ses deux enfants mineurs, de condamner l'Etat à les indemniser des préjudices moraux et matériels ayant résulté de la carence de l'Etat à assurer leur relogement ; que, par un jugement du 31 mars 2014, le tribunal administratif a partiellement fait droit à sa demande en condamnant l'Etat à lui verser une indemnité de 1 500 euros au titre des troubles de toute nature subis dans ses conditions d'existence ; qu'il a, en revanche, rejeté les conclusions indemnitaires présentées par Mme B... au nom de ses enfants ; que celle-ci demande, dans cette mesure, l'annulation de ce jugement ;  <br/>
<br/>
              2. Considérant que, lorsqu'un demandeur a été reconnu prioritaire et devant être relogé en urgence par une commission de médiation, en application des dispositions de l'article L. 441-2-3 du code de la construction et de l'habitation, et que le juge administratif a ordonné son logement ou son relogement par l'Etat, en application de l'article L. 441-2-3-1 de ce code, la carence fautive de l'Etat à assurer son logement dans le délai imparti engage sa responsabilité à l'égard du seul demandeur, au titre des troubles dans les conditions d'existence qu'elle a entraînés pour ce dernier ; que ce préjudice doit toutefois s'apprécier en tenant compte, notamment, du nombre de personnes composant le foyer du demandeur pendant la période de responsabilité de l'Etat ; <br/>
<br/>
              3. Considérant qu'il résulte des principes énoncés au point 2 qu'en écartant les conclusions indemnitaires présentées au nom des enfants de MmeB..., le tribunal administratif, qui n'a pas omis de prendre en compte la présence de ces enfants au foyer de Mme B... pour évaluer le montant de son préjudice, n'a pas commis d'erreur de droit ; qu'en refusant de procéder à l'indemnisation distincte des enfants de MmeB..., il n'a pas soulevé d'office un moyen en défense ni, notamment, opposé une fin de non-recevoir mais s'est borné, dans le cadre de son office de juge du plein contentieux, à constater que les conditions d'engagement de la responsabilité publique à leur égard n'étaient pas réunies ; qu'il n'a, dès lors, commis aucune irrégularité en n'informant pas les parties de ce qu'il comptait fonder son rejet sur l'absence de préjudice indemnisable subi par eux ; <br/>
<br/>
              4. Considérant qu'il résulte de ce qui précède que le pourvoi de Mme B...doit être rejeté ; que doivent être rejetées, par voie de conséquence, ses conclusions présentées au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
		Article 1er : Le pourvoi de Mme B...est rejeté.<br/>
<br/>
Article 2 : La présente décision sera notifiée à Mme A...B...et à la ministre du logement, de l'égalité des territoires et de la ruralité. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">38-07-01 LOGEMENT. - RESPONSABILITÉ DE L'ETAT À RAISON DE LA CARENCE FAUTIVE À ASSURER LE LOGEMENT D'UN DEMANDEUR RECONNU PRIORITAIRE ET URGENT DANS LE DÉLAI FIXÉ PAR LE JUGE DE L'INJONCTION - RESPONSABILITÉ À L'ÉGARD DU SEUL DEMANDEUR - APPRÉCIATION DU PRÉJUDICE EN TENANT COMPTE DU NOMBRE DE PERSONNES COMPOSANT LE FOYER DU DEMANDEUR.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">60-02-012 RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. RESPONSABILITÉ EN RAISON DES DIFFÉRENTES ACTIVITÉS DES SERVICES PUBLICS. SERVICES SOCIAUX. - DALO - CARENCE FAUTIVE DE L'ETAT À ASSURER LE LOGEMENT D'UN DEMANDEUR RECONNU PRIORITAIRE ET URGENT DANS LE DÉLAI FIXÉ PAR LE JUGE DE L'INJONCTION - RESPONSABILITÉ À L'ÉGARD DU SEUL DEMANDEUR - APPRÉCIATION DU PRÉJUDICE EN TENANT COMPTE DU NOMBRE DE PERSONNES COMPOSANT LE FOYER DU DEMANDEUR.
</SCT>
<ANA ID="9A"> 38-07-01 Lorsqu'un demandeur a été reconnu prioritaire et devant être relogé en urgence par une commission de médiation, en application des dispositions de l'article L. 441-2-3 du code de la construction et de l'habitation, et que le juge administratif a ordonné son logement ou son relogement par l'Etat, en application de l'article L. 441-2-3-1 de ce code, la carence fautive de l'Etat à assurer son logement dans le délai imparti engage sa responsabilité à l'égard du seul demandeur, au titre des troubles dans les conditions d'existence qu'elle a entraînés pour ce dernier. Ce préjudice doit toutefois s'apprécier en tenant compte, notamment, du nombre de personnes composant le foyer du demandeur pendant la période de responsabilité de l'Etat.</ANA>
<ANA ID="9B"> 60-02-012 Lorsqu'un demandeur a été reconnu prioritaire et devant être relogé en urgence par une commission de médiation, en application des dispositions de l'article L. 441-2-3 du code de la construction et de l'habitation, et que le juge administratif a ordonné son logement ou son relogement par l'Etat, en application de l'article L. 441-2-3-1 de ce code, la carence fautive de l'Etat à assurer son logement dans le délai imparti engage sa responsabilité à l'égard du seul demandeur, au titre des troubles dans les conditions d'existence qu'elle a entraînés pour ce dernier. Ce préjudice doit toutefois s'apprécier en tenant compte, notamment, du nombre de personnes composant le foyer du demandeur pendant la période de responsabilité de l'Etat.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
