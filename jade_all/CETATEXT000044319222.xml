<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044319222</ID>
<ANCIEN_ID>JG_L_2021_11_000000447693</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/31/92/CETATEXT000044319222.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème chambre, 10/11/2021, 447693, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-11-10</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>447693</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>CABINET MUNIER-APAIRE ; SCP WAQUET, FARGE, HAZAN</AVOCATS>
<RAPPORTEUR>M. Frédéric Gueudar Delahaye</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Marc Pichon de Vendeuil</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:447693.20211110</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu les procédures suivantes :<br/>
<br/>
              Procédure contentieuse antérieure <br/>
<br/>
              Mme F... a demandé au tribunal administratif de Grenoble, d'une part, d'annuler l'arrêté du 20 juillet 2016 par lequel le recteur de l'académie de Grenoble a affecté Mme D... E... aux fonctions de gestionnaire et agent comptable du lycée du Grévisaudan et la décision du 21 décembre 2016 de rejet de son recours gracieux. Par un jugement n° 1700937 du 4 décembre 2018, le tribunal administratif de Grenoble a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 19LY00500 du 15 octobre 2020, la cour administrative d'appel de Lyon a, sur appel de Mme C..., annulé l'arrêté du 20 juillet 2016 du recteur de l'académie de Grenoble et la décision de rejet du recours gracieux et rejeté le surplus des conclusions des parties.<br/>
<br/>
              Procédures devant le Conseil d'Etat <br/>
<br/>
              1° Sous le n° 447693, par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 15 décembre 2020 et 15 mars 2021 au secrétariat du contentieux du Conseil d'État, Mme E... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à ses conclusions d'appel ;<br/>
<br/>
              3°) de mettre à la charge de Mme C... la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              2° Sous le n° 447897, par un pourvoi, enregistré le 16 décembre 2020 au secrétariat du contentieux du Conseil d'État, le ministre de l'éducation nationale, de la jeunesse et des sports demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler les articles 1er et 2 du même arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de Mme C....<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu :<br/>
              - la loi n° 83-634 du 13 juillet 1983 ;<br/>
              - la loi n° 84-16 du 11 janvier 1984 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Frédéric Gueudar Delahaye, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Marc Pichon de Vendeuil, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, au cabinet Munier-Apaire, avocat de Mme E... et à la SCP Waquet, Farge, Hazan, avocat de Mme C... ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que par un arrêté du 20 juillet 2016, le recteur de l'académie de Grenoble a affecté Mme E... aux fonctions de gestionnaire et agent comptable du lycée du Grévisaudan. Par une décision du 21 décembre 2016, il a rejeté le recours gracieux formé par Mme C... contre cet arrêté. Par un jugement du 4 décembre 2018, le tribunal administratif de Grenoble a rejeté la demande de Mme C... tendant à l'annulation pour excès de pouvoir de cet arrêté et de cette décision. Par un arrêt du 15 octobre 2020, contre lequel se pourvoient en cassation, d'une part, Mme E... et, d'autre part, le ministre de l'éducation nationale, de la jeunesse et des sports, la cour administrative d'appel de Lyon a, sur appel de Mme C..., annulé les décisions litigieuses du recteur de l'académie de Grenoble. Les pourvois de Mme E... et du ministre de l'éducation nationale, de la jeunesse et des sports étant dirigés contre la même décision, il y a lieu de les joindre pour statuer par une seule décision.<br/>
<br/>
              2. Aux termes de l'article 60 de la loi du 11 janvier 1984 portant dispositions statutaires relatives à la fonction publique de l'Etat, dans sa rédaction applicable au litige : " L'autorité compétente procède aux mouvements des fonctionnaires après avis des commissions administratives paritaires. / Dans les administrations ou services où sont dressés des tableaux périodiques de mutations, l'avis des commissions est donné au moment de l'établissement de ces tableaux. / Toutefois, lorsqu'il n'existe pas de tableaux de mutation, seules les mutations comportant changement de résidence ou modification de la situation de l'intéressé sont soumises à l'avis des commissions. / Dans toute la mesure compatible avec le bon fonctionnement du service, les affectations prononcées doivent tenir compte des demandes formulées par les intéressés et de leur situation de famille. Priorité est donnée aux fonctionnaires séparés de leur conjoint pour des raisons professionnelles, aux fonctionnaires séparés pour des raisons professionnelles du partenaire avec lequel ils sont liés par un pacte civil de solidarité lorsqu'ils produisent la preuve qu'ils se soumettent à l'obligation d'imposition commune prévue par le code général des impôts, aux fonctionnaires handicapés relevant de l'une des catégories mentionnées aux 1°, 2°, 3°, 4°, 9°, 10° et 11° de l'article L. 5212-13 du code du travail et aux fonctionnaires qui exercent leurs fonctions, pendant une durée et selon des modalités fixées par décret en Conseil d'Etat, dans un quartier urbain où se posent des problèmes sociaux et de sécurité particulièrement difficiles. Lorsqu'un service ou une administration ne peut offrir au fonctionnaire affecté sur un emploi supprimé un autre emploi correspondant à son grade, le fonctionnaire bénéficie, sur sa demande, dans des conditions fixées par décret en Conseil d'Etat, d'une priorité d'affectation sur tout emploi correspondant à son grade et vacant dans un service ou une administration situé dans la même zone géographique, après avis de la commission administrative paritaire compétente. / (...) ".<br/>
<br/>
              3. Sauf texte contraire en disposant autrement pour certains corps, l'administration peut pourvoir un poste vacant selon la voie -concours, mutation, détachement, affectation après réintégration- qu'elle détermine. Lorsque, dans le cadre d'un mouvement de mutation, un poste a été déclaré vacant, et que, comme elle le peut, l'administration envisage de le pourvoir par une affectation après réintégration, alors que des agents se sont portés candidats dans le cadre du mouvement, elle doit toutefois comparer l'ensemble des candidatures dont elle est saisie, au titre des mutations comme des affectations après réintégration, en fonction, d'une part, de l'intérêt du service, d'autre part, si celle-ci est invoquée, de la situation de famille des intéressés appréciée, pour ce qui concerne les agents qui demandent leur mutation, compte tenu des priorités fixées par les dispositions citées ci-dessus de l'article 60 de la loi du 11 janvier 1984. Dès lors, en jugeant que les prérequis définis dans l'avis de vacance conditionnent l'admissibilité des candidatures individuelles en vue de leur classement selon les seuls critères prévus par le quatrième alinéa de l'article 60 sans pouvoir, eux-mêmes, donner lieu à aucun classement des candidatures par l'administration, la cour a entaché son arrêt d'une erreur de droit.<br/>
<br/>
              4. Il résulte de ce qui précède que, sans qu'il soit besoin de se prononcer sur les autres moyens des pourvois, Mme E... et le ministre de l'éducation nationale, de la jeunesse et des sports sont fondés à demander l'annulation de l'arrêt qu'ils attaquent.<br/>
<br/>
              5. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge, d'une part, de Mme E... et, d'autre part, de l'État, qui ne sont pas, dans la présente instance, la partie perdante. En revanche, il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de Mme C... la somme de 3 000 euros à verser à Mme E... au titre des mêmes dispositions.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 15 octobre 2020 de la cour administrative d'appel de Lyon est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Lyon.<br/>
Article 3 : Mme C... versera à Mme E... une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Les conclusions présentées par Mme C... au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée à Mme D... E..., au ministre de l'éducation nationale, de la jeunesse et des sports et à Mme F....<br/>
              Délibéré à l'issue de la séance du 21 octobre 2021 où siégeaient : M. Olivier Japiot, Président de chambre, Présidant ; M. Gilles Pellissier, Conseiller d'Etat et M. Frédéric Gueudar Delahaye, Conseiller d'Etat-rapporteur. <br/>
<br/>
              Rendu le 10 novembre 2021.<br/>
<br/>
<br/>
                 Le président : <br/>
                 Signé : M. Olivier Japiot<br/>
 		Le rapporteur : <br/>
      Signé : M. Frédéric Gueudar Delahaye<br/>
                 Le secrétaire :<br/>
                 Signé : Mme B... A...<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
