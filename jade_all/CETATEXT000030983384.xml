<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030983384</ID>
<ANCIEN_ID>JG_L_2015_07_000000380557</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/98/33/CETATEXT000030983384.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère SSJS, 31/07/2015, 380557, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-07-31</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>380557</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère SSJS</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DE CHAISEMARTIN, COURJON ; SCP FOUSSARD, FROGER</AVOCATS>
<RAPPORTEUR>M. Denis Rapone</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Alexandre Lallet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2015:380557.20150731</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société Crozondis a demandé au tribunal administratif de Rennes d'annuler l'arrêté du 11 mars 2011 par lequel le maire de la commune de Telgruc-sur-Mer (Finistère) a délivré à la société Soditelmer un permis de construire un bâtiment à usage de commerce de détail. Par un jugement n° 1101832 du 1er juin 2012, le tribunal administratif de Rennes a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 12NT02231 du 21 mars 2014, la cour administrative d'appel de Nantes a, à la demande de la société Crozondis, annulé le jugement du tribunal administratif de Rennes du 1er juin 2012 et l'arrêté du 11 mars 2011.<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 22 mai et 18 août 2014 au secrétariat du contentieux du Conseil d'Etat, la commune de Telgruc-sur-Mer demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt de la cour administrative d'appel de Nantes du 21 mars 2014 ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de la société Crozondis ; <br/>
<br/>
              3°) de mettre à la charge de la société Crozondis la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Vu les autres pièces du dossier ; <br/>
<br/>
                          Vu :<br/>
              - le code de commerce ; <br/>
              - le code de l'urbanisme ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Denis Rapone, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Alexandre Lallet, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Foussard, Froger, avocat de la commune de Telgruc-sur-Mer et à la SCP de Chaisemartin, Courjon, avocat de la société Crozondis ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Aux termes de l'article L. 425-7 du code de l'urbanisme dans sa version applicable à la date de l'arrêté litigieux : " Conformément à l'article L. 752-1 du code de commerce, lorsque le permis de construire porte sur un projet soumis à une autorisation d'exploitation commerciale, le permis ne peut être accordé avant la délivrance de cette autorisation. Sa mise en oeuvre ne peut être entreprise avant l'expiration des recours entrepris contre elle ". Il résulte de ces dispositions, éclairées par les travaux préparatoires de l'article 105 de la loi du 4 août 2008 de modernisation de l'économie qui en a amendé la rédaction en vue de réduire les délais d'obtention d'un permis de construire portant sur un projet soumis à une autorisation d'exploitation commerciale, qu'à compter de l'entrée en vigueur de cette modification, l'octroi du permis est devenu possible dès la délivrance de cette autorisation par la commission départementale d'aménagement commercial, seule la mise en oeuvre de ce permis étant désormais subordonnée à l'expiration des délais de recours contre l'autorisation ou, en cas de recours, à l'intervention de la décision de la commission nationale. Par suite, alors même que le législateur n'avait pas, à la date de l'arrêté attaqué du maire de Telgruc-sur-Mer, tiré les conséquences de cette modification, par coordination, sur l'article L. 752-18 du code de commerce qui, à l'instar de l'article L. 425-7 du code de l'urbanisme dans sa version antérieure à la loi du 4 août 2008 de modernisation de l'économie, prévoyait que le permis de construire ne pouvait être accordé avant l'expiration du délai de recours contre la décision de la commission départementale d'aménagement commercial ou, en cas de recours, avant la décision de la commission nationale, la cour administrative d'appel a entaché son arrêt d'une erreur de droit en jugeant que le maire n'avait légalement pu délivrer un permis de construire à la société Soditelmer sans attendre que la commission nationale d'aménagement commercial se prononce sur le recours dont elle était saisie contre l'autorisation d'exploitation commerciale qui avait été délivrée à cette société.<br/>
<br/>
              2. Il résulte de ce qui précède, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, que la commune de Telgruc-sur-Mer est fondée à demander l'annulation de l'arrêt de la cour administrative d'appel de Nantes qu'elle attaque.<br/>
<br/>
              3. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge la société Crozondis une somme de 3 000 euros à verser à la commune de Telgruc-sur-Mer, au titre des dispositions de l'article L. 761-1 du code de justice administrative. Ces dispositions font obstacle à ce qu'une somme soit mise à ce titre à la charge de la commune de Telgruc-sur-Mer qui n'est pas, dans la présente instance, la partie perdante. <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Nantes du 21 mars 2014 est annulé. <br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Nantes.<br/>
Article 3 : La société Crozondis versera à la commune de Telgruc-sur-Mer une somme de 3 000 euros au titre de l'article L.761-1 du code de justice administrative.<br/>
Article 4 : Les conclusions de la société Crozondis présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée à la commune de Telgruc-sur-Mer, à la société Crozondis, à la société Soditelmer et à la ministre du logement, de l'égalité des territoires et de la ruralité.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
