<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032928811</ID>
<ANCIEN_ID>JG_L_2016_07_000000385609</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/92/88/CETATEXT000032928811.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème chambre, 22/07/2016, 385609, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-07-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>385609</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GATINEAU, FATTACCINI</AVOCATS>
<RAPPORTEUR>M. Julien Anfruns</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emilie Bokdam-Tognetti</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2016:385609.20160722</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, un mémoire en réplique et un nouveau mémoire, enregistrés les 7 novembre 2014, 13 mars 2015 et 24 juin 2016 au secrétariat du contentieux du Conseil d'Etat, la caisse régionale de crédit agricole mutuel de Pyrénées-Gascogne demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler la lettre du 8 septembre 2014 du secrétaire général de l'Autorité de contrôle prudentiel et de résolution (ACPR) relative à la désignation de M. Marc Didier, président de son conseil d'administration, en qualité de " dirigeant effectif " ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code monétaire et financier ;<br/>
              - l'ordonnance n° 2014-158 du 20 février 2014 ;<br/>
              - le règlement n° 96-16 du 20 décembre 1996 du comité de la réglementation bancaire et financière ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Julien Anfruns, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Emilie Bokdam-Tognetti, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Gatineau, Fattaccini, avocat de l'Autorité de contrôle prudentiel et de résolution ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Aux termes du deuxième alinéa de l'article L. 511-13 du code monétaire et financier, dans sa rédaction applicable au litige : " La direction effective de l'activité des établissements de crédit ou des sociétés de financement doit être assurée par deux personnes au moins ". Aux termes de l'article 9 du règlement du 20 décembre 1996 du comité de la réglementation bancaire et financière (CRBF) relatif aux modifications de situation des établissements de crédit, des sociétés de financement et des entreprises d'investissement autres que les sociétés de gestion des portefeuilles modifié : " La désignation de toute nouvelle personne appelée, en application de l'article L. 511-13 ou L. 532-2 du code monétaire et financier, à assurer la direction effective de l'activité d'une entreprise assujettie doit être immédiatement déclarée à l'Autorité de contrôle prudentiel et de résolution. Cette déclaration est accompagnée de tous éléments permettant d'apprécier l'honorabilité et l'expérience de la personne concernée./(...) lorsque l'entreprise assujettie est un établissement de crédit, l'Autorité de contrôle prudentiel et de résolution dispose également d'un délai d'un mois, à compter soit de la déclaration qui lui est faite en application de l'alinéa premier du présent article (...) pour faire savoir au déclarant que la désignation n'est pas compatible avec l'agrément précédemment délivré ". En vertu de l'article L. 511-15 du code monétaire et financier, l'ACPR peut prononcer d'office le retrait d'agrément notamment si l'entreprise ne remplit plus les conditions auxquelles était subordonné son agrément.<br/>
<br/>
              2. Il ressort des pièces du dossier qu'en application des dispositions précitées de l'article 9 du règlement du 20 décembre 1996 du CRBF, la caisse régionale de crédit agricole mutuel de Pyrénées-Gascogne a déclaré à l'ACPR avoir désigné M. Marc Didier, président de son conseil d'administration, en tant que " dirigeant effectif ". La caisse régionale de crédit agricole mutuel de Pyrénées-Gascogne demande l'annulation de la lettre du 8 septembre 2014 par lequel le secrétaire général de l'ACPR a répondu à son courrier.<br/>
<br/>
              3. Il résulte des termes mêmes de la lettre du 8 septembre 2014 que le secrétaire général de l'ACPR a répondu à la caisse régionale que, dans sa " position " 2014-P-07 du 20 juin 2014, l'ACPR avait indiqué que, compte tenu du nouveau cadre juridique issu de la transposition de la directive 2013/36/UE du 26 juin 2013 par l'ordonnance du 20 février 2014, la fonction de " dirigeant effectif " est exercée, au sein des établissements de crédit constitués sous la forme d'une société anonyme à conseil d'administration, d'une part, par le directeur général et, d'autre part, par le directeur général délégué ou, si la situation particulière de l'établissement faisait obstacle à la désignation d'un directeur général délégué pour des raisons qui seraient exposées à l'ACPR, par un cadre dirigeant disposant des pouvoirs nécessaires, attribués par le conseil d'administration, à l'exercice d'une direction effective de l'activité de l'établissement, et que le président du conseil d'administration ne peut être désigné comme " dirigeant effectif " de l'établissement, sauf dans les cas où il est expressément autorisé par l'Autorité à cumuler ses fonctions avec celles de directeur général. Le secrétaire général de l'ACPR en a déduit que la direction effective de l'établissement n'était actuellement assurée que par une seule personne, son directeur général, et a invité la caisse régionale à communiquer à l'Autorité le dossier de la personne qu'elle entendait désigner comme deuxième " dirigeant effectif ", soit, en principe, le directeur général délégué ou, en cas de situation particulière le justifiant, un cadre dirigeant disposant des pouvoirs nécessaires, attribués par le conseil d'administration, à l'exercice d'une direction effective de l'établissement. Cette lettre ne présente pas le caractère d'une décision prise sur le fondement de l'article 9 précité du règlement du 20 décembre 1996 du CRBF, en vertu duquel l'ACPR dispose d'un délai d'un mois pour faire savoir au déclarant que la désignation n'est pas compatible avec l'agrément précédemment délivré, ni sur celui des pouvoirs de police administrative que détient l'ACPR, tel celui d'adresser la mise en demeure prévue à l'article L. 612-31 du code monétaire et financier. Dès lors, l'invitation adressée à la caisse régionale par le secrétaire général de l'ACPR est, par elle-même, dépourvue de caractère contraignant. Il suit de là que la lettre du 8 septembre 2014 ne peut être regardée comme une décision faisant grief susceptible de faire l'objet d'un recours pour excès de pouvoir. La requête de la caisse régionale de crédit agricole de Pyrénées-Gascogne est, par suite, irrecevable.<br/>
<br/>
              4. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat (ACPR), qui n'est pas, dans la présente instance, la partie perdante. Dans les circonstances de l'espèce, il n'y a pas lieu de faire droit aux conclusions présentées par l'Etat (ACPR) à ce même titre.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de la caisse régionale de crédit agricole mutuel de Pyrénées-Gascogne est rejetée.<br/>
Article 2 : Les conclusions de l'Etat (ACPR) présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
Article 3 : La présente décision sera notifiée à la caisse régionale de crédit agricole mutuel de Pyrénées-Gascogne et à l'Autorité de contrôle prudentiel et de résolution.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
