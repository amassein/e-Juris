<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043930667</ID>
<ANCIEN_ID>JG_L_2021_08_000000437545</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/93/06/CETATEXT000043930667.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème chambre, 06/08/2021, 437545, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-08-06</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>437545</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Joachim Bendavid</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Cécile Barrois de Sarigny</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:437545.20210806</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une ordonnance n° 1924035 du 7 janvier 2020, enregistrée le 10 janvier 2020 au secrétariat du contentieux du Conseil d'Etat, le président du tribunal administratif de Paris a transmis au Conseil d'Etat, en application de l'article R. 351-2 du code de justice administrative, la requête, enregistrée le 12 novembre 2019 au greffe de ce tribunal, présentée par M. C... V..., Mme S... B..., M. M... I..., M. K... D..., M. L... J..., M. U... Q..., M. N... E..., M. R... F..., M. P... T..., Mme G... A... et M. O... H.... Par cette requête, M. V... et autres demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir l'arrêté du ministre de l'intérieur du 5 septembre 2019 portant sur l'organisation relative au temps de travail dans les services de la police nationale ;<br/>
<br/>
              2°) d'annuler pour excès de pouvoir la note du directeur général de la police nationale du 15 octobre 2019 relative à la campagne d'indemnisation exceptionnelle 2019 des heures supplémentaires pour les personnels de la police nationale ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 1 000 euros pour chacun des requérants, au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la loi n° 83634 du 13 juillet 1983 ;<br/>
              - la loi n° 84-16 du 11 janvier 1984 ;<br/>
              - le décret n° 2000-194 du 3 mars 2000 ;<br/>
              - le décret n° 2000-815 du 25 août 2000 ;<br/>
              - le décret n° 2002-819 du 3 mai 2002 ;<br/>
              - le décret n° 2002-1279 du 23 octobre 2002 ;<br/>
              - l'arrêté du 3 mai 2002 pris pour l'application dans la police nationale des articles 1er, 4, 5 et 10 du décret n° 2000-815 du 25 août 2000 relatif à l'aménagement et à la réduction du temps de travail dans la fonction publique de l'Etat ;<br/>
              - l'arrêté du 3 mai 2002 pris pour l'application de l'article 5 du décret n° 2002-819 du 3 mai 2002 relatif aux modalités de rémunération ou de compensation des astreintes des personnels de la police nationale ;<br/>
              - l'arrêté du 6 juin 2006 portant règlement général d'emploi de la police nationale ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Joachim Bendavid, auditeur,  <br/>
<br/>
              - les conclusions de Mme Cécile Barrois de Sarigny, rapporteure publique.<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Par les moyens qu'ils invoquent, les requérants doivent être regardés comme ne demandant l'annulation que de certains articles de l'arrêté du ministre de l'intérieur du 5 septembre 2019 portant sur l'organisation relative au temps de travail dans les services de la police nationale, relatifs notamment au régime des astreinte. Ils demandent également l'annulation d'une note de service du 15 octobre 2019 par laquelle le directeur général de la police nationale a prévu l'indemnisation exceptionnelle, en 2019, d'une partie des heures supplémentaires effectués par les agents placés sous son autorité.<br/>
<br/>
              Sur les conclusions dirigées contre certains articles de l'arrêté du 5 septembre 2019 :<br/>
<br/>
              En ce qui concerne les articles 52 et 78 :<br/>
<br/>
              2. Il ressort des pièces du dossier que M. V... et les autres requérants font valoir leurs qualités de brigadier, brigadier-chef ou major de la police nationale. Ils appartiennent ainsi au corps d'encadrement et d'application des membres actifs de la police nationale et sont, dès lors, sans intérêt et, par suite, sans qualité pour demander l'annulation des dispositions des articles 52 et 78 de l'arrêté attaqué, lesquelles ne sont contestés par les requérants qu'en tant qu'ils concernent les membres du corps de conception et de direction et ceux du corps de commandement de la police nationale. Les conclusions qu'ils présentent contre ces dispositions sont, ainsi, irrecevables et doivent être rejetées.<br/>
<br/>
              En ce qui concerne les articles 27, 47 et 51 :<br/>
<br/>
              3. L'article 5 du décret du 25 août 2000 relatif à l'aménagement et à la réduction du temps de travail dans la fonction publique de l'Etat et dans la magistrature prévoit, après avoir défini la période d'astreinte, que : " Des arrêtés du ministre intéressé, du ministre chargé de la fonction publique et du ministre chargé du budget, pris après consultation des comités techniques ministériels, déterminent les cas dans lesquels il est possible de recourir à des astreintes.(...) ". Pour l'application de ces dispositions au personnel de la police nationale, un arrêté conjoint du ministre de l'économie, des finances et de l'industrie, du ministre de l'intérieur, du ministre de la fonction publique et de la réforme de l'Etat et du secrétaire d'Etat au budget est intervenu le 3 mai 2002.<br/>
<br/>
              4. Contrairement à ce que soutiennent les requérants, les dispositions des articles 27, 47 et 51 de l'arrêté attaqué, qui se bornent à rappeler des dispositions figurant dans plusieurs autres textes et, notamment, dans l'arrêté du 3 mai 2002 mentionné ci-dessus, ne sont pas prises sur le fondement de l'article 5 du décret du 25 août 2000. L'article 27 rappelle ainsi que les astreintes sont compatibles avec un régime hebdomadaire avec horaires variables et peuvent être compensées, l'article 47 rappelle que certains personnels de police peuvent être amenés, pour assurer la continuité du service public et la protection des personnes et des biens, à effectuer des services supplémentaires dont l'astreinte et l'article 51 précise les catégories d'agents pour lesquels il peut être recouru au régime de l'astreinte, dont la définition est reprise de l'article 5 du décret du 25 août 2000 cité ci-dessus, ainsi que ses modalités pratiques de mise en œuvre. Le ministre de l'intérieur était ainsi compétent pour prendre, en sa qualité de chef de service, les dispositions attaquées qui ne sont par suite, contrairement à ce que soutiennent les requérants, pas entachées d'incompétence.<br/>
<br/>
              Sur les conclusions dirigées contre la note du 15 octobre 2019 :<br/>
<br/>
              5. En premier lieu, si la note attaquée prévoit l'indemnisation exceptionnelle, au titre de l'année 2019, de tout ou partie des heures supplémentaires excédant le seuil de 160 heures pour les seuls agents ayant accumulé, au 30 septembre 2019, un nombre d'heures supplémentaires, non indemnisées et non récupérées, excédant ce même seuil, la différence de traitement qui en résulte nécessairement entre les agents dont le nombre d'heures supplémentaires susceptibles d'être indemnisées dépasse ce seuil et ceux dont le nombre d'heure supplémentaire est inférieur à ce seuil est, contrairement à ce qui est soutenu, justifiée par la différence de situation entre ces agents au regard du nombre d'heures supplémentaires qu'ils ont effectuées sans qu'elles soient indemnisées ou récupérées. Par suite, le moyen tiré de ce qu'elle méconnaît le principe d'égalité entre agents d'un même corps doit être écarté. <br/>
<br/>
              6. En deuxième lieu, la circonstance, à la supposer établie, que certains chefs d'unité n'auraient pas comptabilisé certaines heures supplémentaires de leurs agents est, par elle-même, sans incidence sur la légalité de la note attaquée, dont l'objet est de prévoir les modalités d'indemnisation des heures supplémentaires effectivement enregistrées. Par ailleurs, il ne ressort pas des pièces du dossier que la note attaquée serait intervenue dans un délai ne permettant pas l'actualisation des " compteurs individuels " d'heures supplémentaires des agents, dits GEOPOL, avant la mise en œuvre des opérations de mise en paiement.<br/>
<br/>
              7. En troisième lieu, le moyen tiré de ce qu'en prévoyant que le montant de l'indemnisation exceptionnelle serait déterminé en fonction des crédits disponibles au niveau interministériel en fin de gestion, la note attaquée aurait méconnu les " règles budgétaires de l'Etat " n'est pas assorti des précisions permettant d'en apprécier le bien-fondé.<br/>
<br/>
              8. Enfin, il ressort des pièces du dossier et n'est d'ailleurs pas contesté que la note attaquée retient, pour l'indemnisation des heures supplémentaires, un taux horaire conforme au montant qui résulte de l'application des dispositions de l'article 3 du décret du 3 mars 2000 fixant les conditions d'attribution d'une indemnité pour services supplémentaires aux fonctionnaires actifs de la police nationale. Les requérants ne sont, par suite, pas fondés à soutenir que ce taux horaire fixerait le montant d'indemnisation à un " niveau insuffisant ".<br/>
<br/>
              9. Il résulte de tout ce qui précède que M. V... et autres ne sont pas fondés à demander l'annulation des articles litigieux de l'arrêté du 5 septembre 2019 et de la note du 15 octobre 2019 du directeur général de la police nationale. Leur requête doit, par suite, être rejetée, y compris, par voie de conséquence, ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
		Article 1er : La requête de M. V... et autres est rejetée. <br/>
<br/>
Article 2 : La présente décision sera notifiée à M. C... V..., représentant désigné pour l'ensemble des requérants et au ministre de l'intérieur. <br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
