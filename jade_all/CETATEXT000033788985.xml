<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033788985</ID>
<ANCIEN_ID>JG_L_2016_12_000000392841</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/78/89/CETATEXT000033788985.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème chambre, 30/12/2016, 392841, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-12-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>392841</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MATUCHANSKY, POUPOT, VALDELIEVRE</AVOCATS>
<RAPPORTEUR>M. Vincent Villette</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Aurélie Bretonneau</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2016:392841.20161230</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A...B...a demandé au tribunal administratif de Grenoble la décharge des cotisations complémentaires d'impôt sur le revenu et de contributions sociales auxquelles il a été assujetti au titre des années 2002 à 2004 ainsi que des pénalités y afférentes. Par un jugement n° 1004386 du 7 novembre 2013, le tribunal administratif de Grenoble a partiellement fait droit à sa demande. <br/>
<br/>
              Par un arrêt n° 14LY00068 du 21 avril 2015, la cour administrative d'appel de Lyon a rejeté l'appel formé contre ce jugement par M.B....<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 21 août et 23 novembre 2015 au secrétariat du contentieux du Conseil d'Etat, M. B...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la convention fiscale du 9 septembre 1966 modifiée, signée entre la France et la Suisse ;<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Vincent Villette, auditeur,  <br/>
<br/>
              - les conclusions de Mme Aurélie Bretonneau, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Matuchansky, Poupot, Valdelièvre, avocat de M. A...B...;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond qu'à la suite d'un examen de sa situation fiscale personnelle portant sur les années 2002 à 2004, M. B...a été assujetti à des cotisations supplémentaires d'impôt sur le revenu et de contributions sociales, assorties de pénalités. Le jugement du 7 novembre 2013 du tribunal administratif de Grenoble n'ayant fait que partiellement droit à ses conclusions tendant à la décharge de ces impositions, M. B... se pourvoit contre l'arrêt du 21 avril 2015 par lequel la cour administrative d'appel de Lyon a rejeté son appel contre ce jugement.<br/>
<br/>
Sur l'arrêt attaqué en tant qu'il statue sur l'imposition des revenus de l'année 2002 : <br/>
<br/>
              2. Aux termes de l'article R. 196-1 du livre des procédures fiscales : " Pour être recevables, les réclamations relatives aux impôts autres que les impôts directs locaux et les taxes annexes à ces impôts, doivent être présentées à l'administration au plus tard le 31 décembre de la deuxième année suivant celle, selon le cas : a) De la mise en recouvrement du rôle ou de la notification d'un avis de mise en recouvrement ; (...) ". Aux termes de l'article R. 196-3 du même livre : " Dans le cas où un contribuable fait l'objet d'une procédure de reprise ou de redressement de la part de l'administration des impôts, il dispose d'un délai égal à celui de l'administration pour présenter ses propres réclamations ". <br/>
<br/>
              3. S'il appartient en principe à l'administration de procéder à la notification d'une proposition de rectification à l'adresse indiquée par le contribuable aux services fiscaux, elle peut toutefois, lorsqu'elle apporte la preuve de ce que le domicile dont l'adresse lui a été indiquée présente un caractère fictif, retenir une autre adresse, si elle a établi qu'elle est celle où il réside effectivement.<br/>
<br/>
              4. Il ressort des pièces du dossier soumis aux juges du fond que le domicile fiscal déclaré par M. B...à l'administration fiscale se situait, au cours de l'année 2005, en Suisse, à Etoy, adresse à laquelle la proposition de rectification du 21 décembre 2005 relative aux rehaussements opérés au titre de l'année 2002 lui a été envoyée, et que le pli a été retiré au bureau de poste par le contribuable le 3 janvier 2006. Dès lors qu'il n'était pas établi ou allégué devant la cour administrative d'appel que cette adresse présentait un caractère fictif ni que M. B... avait une autre résidence effective, celle-ci a commis une erreur de droit en jugeant que la notification de la proposition de redressement faite par l'administration fiscale au siège de la société Arte, à Gières, dont le requérant est le dirigeant et le principal associé, avait régulièrement été effectuée à la date de la présentation du pli soit avant le 31 décembre 2005 au motif que ce pli était revenu à l'administration fiscale avec les mentions " non réclamé ", " avisé " et " retour à l'envoyeur ". <br/>
<br/>
Sur l'arrêt attaqué en tant qu'il statue sur l'imposition des revenus  des années 2003 et 2004 :<br/>
<br/>
En ce qui concerne la motivation des propositions de rectification des 6 et 7 juin 2006 :<br/>
<br/>
              5. Aux termes de l'article L. 57 du livre des procédures fiscales : " L'administration adresse au contribuable une proposition de rectification qui doit être motivée de manière à lui permettre de formuler ses observations ou de faire connaître son acceptation ; (...) ". Il résulte de ces dispositions que l'administration doit indiquer au contribuable, dans la proposition de rectification, les motifs et le montant des rehaussements envisagés, leur fondement légal et la catégorie de revenus dans laquelle ils sont opérés, ainsi que les années d'imposition concernées. <br/>
<br/>
              6. En relevant que les propositions de rectification notifiées les 6 et 7 juin 2006 à M. B...indiquaient les raisons pour lesquelles, en sa qualité d'associé de la société Paper Industry Consulting, il devait être regardé comme ayant appréhendé les bénéfices regardés distribués par cette dernière du fait de la taxation d'office de ses résultats, ainsi que les bases imposables à l'impôt sur le revenu dans la catégorie des capitaux mobiliers, la cour, qui n'a pas jugé que ces propositions de rectification étaient motivées par référence à la proposition de rectification adressée le 19 décembre 2005 à la société Paper Industry Consulting, a souverainement apprécié le caractère suffisant de leur motivation sans dénaturer les pièces du dossier. <br/>
<br/>
En ce qui concerne la détermination du foyer fiscal du requérant :<br/>
<br/>
              7. Aux termes de l'article 4 A du code général des impôts : " Les personnes qui ont en France leur domicile fiscal sont passibles de l'impôt sur le revenu en raison de l'ensemble de leurs revenus ". Et aux termes de l'article 4 B : " 1. Sont considérées comme ayant leur domicile fiscal en France au sens de l'article 4 A : a. Les personnes qui ont en France leur foyer ou le lieu de leur séjour principal ".<br/>
<br/>
              8. La cour administrative d'appel s'est fondée sur la circonstance que M. B... disposait, au cours des années d'imposition, d'une résidence en Isère où il habitait avec la mère de ses deux enfants qui étaient scolarisés dans un établissement de la commune, que ses courriers étaient envoyés à cette adresse et qu'il effectuait régulièrement des dépenses courantes dans les commerces voisins. En retenant ces éléments, la cour s'est livrée à une appréciation souveraine des faits exempte de dénaturation ; elle n'a pas commis d'erreur de qualification juridique en déduisant que M. B...devait être regardé comme ayant son foyer et donc son domicile fiscal en France au sens des dispositions citées au point 2. <br/>
<br/>
En ce qui concerne l'application de la convention fiscale franco-suisse :<br/>
<br/>
              9.  Aux termes de l'article 4 de la convention conclue le 9 septembre 1966 entre la France et la Suisse en vue d'éviter les doubles impositions en matière d'impôt sur le revenu et sur la fortune : " 1. Au sens de la présente convention  l'expression "résident d'un Etat contractant" désigne toute personne qui, en vertu de la législation dudit Etat, est assujettie à l'impôt dans cet Etat en raison de son domicile, de sa résidence, de son siège de direction ou de tout autre critère de nature analogue. 2. Lorsque, selon la disposition du paragraphe 1, une personne physique est considérée comme résident de chacun des Etats contractants, le cas est résolu d'après les règles suivantes : a) Cette personne est considérée comme résident de l'Etat contractant où elle dispose d'un foyer d'habitation permanent, cette expression désignant le centre des intérêts vitaux, c'est-à-dire le lieu avec lequel les relations personnelles sont les plus étroites ; (...) ".<br/>
<br/>
              10. Eu égard aux différents éléments mentionnés au point 8, la cour n'a pas inexactement qualifié les faits qui lui étaient soumis en jugeant que même si M. B...pouvait être regardé comme ayant eu, au titre des années d'imposition en litige, la qualité de résident suisse au sens du 1 de l'article 4 de la convention fiscale franco-suisse, il devait être regardé comme ayant eu en France son foyer d'habitation permanent au sens des stipulations du a) du 2 de l'article 4 de cette même convention et comme y ayant été domicilié. <br/>
<br/>
              11. Il résulte de tout ce qui précède que le requérant n'est fondé à demander l'annulation de l'arrêt attaqué qu'en tant qu'il statue sur les redressements en litige notifiés au titre de l'année 2002.<br/>
<br/>
              12. Dans les circonstances de l'espèce, il y a lieu, en application des dispositions de l'article L. 821-2 du code de justice administrative, de régler l'affaire au fond dans la mesure de la cassation prononcée.<br/>
<br/>
              13. Ainsi qu'il a été dit au point 4, la proposition de rectification du 21 décembre 2005, relative aux redressements opérés au titre de l'année 2002, n'a été régulièrement notifiée au requérant que le 3 janvier 2006. En application du délai spécial de réclamation de l'article R. 196-3 du livre des procédures fiscales, cité au point 2, la réclamation présentée par M. B...le 22 décembre 2009 n'était dès lors pas tardive. La fin de non-recevoir opposée par l'administration fiscale doit par suite être écartée. <br/>
<br/>
              14. Aux termes de l'article L. 169 du livre des procédures fiscales : " Pour l'impôt sur le revenu et l'impôt sur les sociétés, le droit de reprise de l'administration des impôts s'exerce jusqu'à la fin de la troisième année qui suit celle au titre de laquelle l'imposition est due ". Et aux termes de l'article L. 189 du même livre : " La prescription est interrompue par la notification d'une proposition de rectification, par la déclaration ou la notification d'un procès-verbal, de même que par tout acte comportant reconnaissance de la part des contribuables et par tous les autres actes interruptifs de droit commun (...) ".<br/>
<br/>
              15. Il résulte de ce qui a été jugé au point 4 que la notification de la proposition de rectification présentée le 23 décembre 2005 au siège de la société Arte à Gières n'a pas eu pour effet d'interrompre la prescription du droit de reprise de l'administration pour l'année 2002, lequel est venu à expiration le 31 décembre 2005. La prescription était, en conséquence, acquise lorsque le contribuable a reçu notification à son domicile, le 3 janvier 2006, de cette proposition de rectification. Dès lors, M. B...est fondé à soutenir que c'est à tort que, par le jugement du 7 novembre 2013, le tribunal administratif de Grenoble a rejeté ses conclusions tendant à la décharge des cotisations supplémentaires d'impôt sur le revenu et de contributions sociales auxquels il a été assujetti au titre de l'année 2002 ainsi que des pénalités correspondantes. <br/>
<br/>
Sur les conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative : <br/>
<br/>
              16. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 2 000 euros à verser à M. B...au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er  : L'arrêt du 21 avril 2015 de la cour administrative d'appel de Lyon et l'article 2 du jugement du 7 novembre 2013 du tribunal administratif de Grenoble sont annulés en tant qu'ils portent sur les cotisations supplémentaires d'impôt sur le revenu et de contributions sociales auxquelles M. B...a été assujetti au titre de l'année 2002 ainsi que des pénalités correspondantes.<br/>
Article 2 : M. B...est déchargé des cotisations supplémentaires d'impôt sur le revenu et de contributions sociales auxquelles il a été assujetti au titre de l'année 2002 ainsi que des pénalités correspondantes.<br/>
Article 3 : L'Etat versera à M. B...la somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Le surplus des conclusions du pourvoi présenté par M. B...est rejeté.<br/>
Article 5 : La présente décision sera notifiée à M. A...B...et au ministre de l'économie et des finances. <br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
