<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036807210</ID>
<ANCIEN_ID>JG_L_2018_04_000000417235</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/80/72/CETATEXT000036807210.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème chambre, 16/04/2018, 417235, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-04-16</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>417235</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP SPINOSI, SUREAU ; SCP BOULLOCHE ; BALAT</AVOCATS>
<RAPPORTEUR>M. Marc Pichon de Vendeuil</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Gilles Pellissier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:417235.20180416</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société SNT Petroni a demandé au juge des référés du tribunal administratif de Bastia, sur le fondement de l'article L. 551-1 du code de justice administrative, d'annuler la décision du 28 novembre 2017 du département de Corse du Sud d'attribuer le lot n° 1 VRD du marché pour l'aménagement d'une section de la route départementale 72 au groupement composé de la société Sotrarout, de la société TBP Debene et de la société Natali et d'ordonner au département de reprendre la procédure de marché au stade de l'examen des offres. Par une ordonnance n° 1701357 du 27 décembre 2017, le juge des référés du tribunal administratif de Bastia a fait droit à cette demande.<br/>
<br/>
              Par un pourvoi, enregistré le 11 janvier 2018 au secrétariat du contentieux du Conseil d'Etat, rectifié le 22 mars 2018, la collectivité de Corse, venant aux droits du département de Corse du Sud, demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) statuant en référé, de rejeter la demande présentée par la société SNT Petroni ;<br/>
<br/>
              3°) de mettre à la charge de la société SNT Petroni la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - l'ordonnance n° 2015-899 du 23 juillet 2015 ;<br/>
              - le décret n° 2016-360 du 25 mars 2016 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Marc Pichon de Vendeuil, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Gilles Pellissier, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Spinosi, Sureau, avocat de la collectivité de Corse, à Me Balat, avocat de la société Petroni et à la SCP Boulloche, avocat du groupement des sociétés Sotrarout / TPB Debene / Natali.<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des énonciations de l'ordonnance attaquée que le département de Corse du Sud a lancé le 29 septembre 2017 une consultation en vue de la passation, selon une procédure d'appel d'offres formalisée, d'un marché public de travaux ayant pour objet l'aménagement de la traversée de Caldaniccia - section 1 sur le territoire de la commune de Sarrola-Carcopino, divisé en cinq lots ; que la société SNT Petroni s'est portée candidate pour le lot n° 1 VRD ; que, par courrier du 28 novembre 2017, elle a été informée que son offre avait été rejetée comme irrégulière et que le lot n° 1 VRD avait été attribué au groupement constitué de la société Sotrarout, de la société TBP Debene et de la société Natali ; que par une ordonnance du 27 décembre 2017 contre laquelle la collectivité de Corse, venue aux droits du département de la Corse du Sud, se pourvoit en cassation, le juge des référés précontractuels du tribunal administratif de Bastia a, à la demande de la société SNT Petroni, sur le fondement des dispositions de l'article L. 551-1 du code de justice administrative, annulé la décision du 28 novembre 2017 du département d'attribuer le lot n° 1 au groupement précité et enjoint au département, s'il entend poursuivre la passation du contrat, de reprendre la procédure au stade de l'analyse des offres, en y intégrant l'offre de la société SNT Petroni ; <br/>
<br/>
              2. Considérant que les sociétés Sotrarout, TPB Debene et Natali avaient qualité pour se pourvoir en cassation contre l'ordonnance attaquée ; que, dès lors, leurs interventions doivent être regardées comme des pourvois en cassation contre cette ordonnance ; <br/>
<br/>
              3. Considérant qu'aux termes de l'article L. 551-1 du code de justice administrative : " Le président du tribunal administratif, ou le magistrat qu'il délègue, peut être saisi en cas de manquement aux obligations de publicité et de mise en concurrence auxquelles est soumise la passation par les pouvoirs adjudicateurs de contrats administratifs ayant pour objet l'exécution de travaux, la livraison de fournitures ou la prestation de services, avec une contrepartie économique constituée par un prix (...) " ; qu'aux termes de l'article 59 du décret du 25 mars 2016 relatif aux marchés publics : " Une offre irrégulière est une offre qui ne respecte pas les exigences formulées dans les documents de la consultation notamment parce qu'elle est incomplète, ou qui méconnaît la législation applicable notamment en matière sociale et environnementale / (...) / II. - Dans les procédures d'appel d'offres et les procédures adaptées sans négociation, les offres irrégulières, inappropriées ou inacceptables sont éliminées. Toutefois, l'acheteur peut autoriser tous les soumissionnaires concernés à régulariser les offres irrégulières dans un délai approprié, à condition qu'elles ne soient pas anormalement basses. / (...) / IV. - La régularisation des offres irrégulières ne peut avoir pour effet de modifier des caractéristiques substantielles des offres " ; <br/>
<br/>
              4. Considérant, en premier lieu, qu'il ressort des énonciations de l'ordonnance attaquée que le juge des référés a relevé que la société SNT Petroni avait transmis au soutien de son offre, le 31 octobre 2017, le bordereau initial des prix, sur lequel n'apparaissaient pas les prescriptions attendues concernant la rubrique 7.11 du règlement de consultation relative à la zone de sécurité, et que cette société avait pris connaissance de la modification du bordereau effectuée par le pouvoir adjudicateur le 12 octobre 2017 dont elle a nécessairement tenu compte pour rédiger son offre, ainsi que le détail estimatif des prix le confirme ; qu'en en déduisant que la circonstance, pour regrettable qu'elle soit, que la SNT Petroni n'ait pas utilisé le bordereau des prix tel qu'il avait été modifié par le pouvoir adjudicateur n'était pas de nature, à elle seule, à pouvoir faire regarder son offre comme irrégulière et en relevant, au surplus, que le département aurait pu lever toute éventuelle ambiguïté en demandant une régularisation à cette candidate, le juge des référés n'a pas commis d'erreur de droit ;<br/>
<br/>
              5. Considérant, en second lieu, que, contrairement à ce qui est soutenu par les requérantes, le juge des référés n'a pas dénaturé les pièces du dossier en estimant que le document dénommé " plans de signalisation " joint à l'offre de la société SNT Petroni, qui fait apparaître la nature des travaux routiers, la gestion des flux piétons et la gestion des véhicules lors des trois phases des travaux, permettait au département, conformément aux exigences du règlement de la consultation, " d'évaluer la pertinence du phasage en adéquation avec les contraintes du chantier et du planning fourni " ;<br/>
<br/>
              6. Considérant qu'il résulte de ce qui précède que les requérantes ne sont pas fondées à demander l'annulation de l'ordonnance qu'elles attaquent ; <br/>
<br/>
              7. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de la société SNT Petroni qui n'est pas, dans la présente instance, la partie perdante ; qu'il y a lieu, en revanche, dans les circonstances de l'espèce, de mettre à la charge de la collectivité de Corse, sur le fondement des mêmes dispositions, la somme de 3 500 euros à verser à la société SNT Petroni ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Les pourvois de la collectivité de Corse et des sociétés Sotrarout, TPB Debene et Natali sont rejetés.<br/>
Article 2 : La collectivité de Corse versera à la société SNT Petroni la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée à la collectivité de Corse, à la société SNT Petroni, à la société Sotrarout, à la société TPB Debene et à la société Natali.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
