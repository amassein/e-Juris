<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042065798</ID>
<ANCIEN_ID>JG_L_2020_06_000000433465</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/06/57/CETATEXT000042065798.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème chambre, 29/06/2020, 433465, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-06-29</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>433465</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP THOUVENIN, COUDRAY, GREVY ; SCP FOUSSARD, FROGER</AVOCATS>
<RAPPORTEUR>M. Bertrand Mathieu</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Sophie Roussel</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2020:433465.20200629</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
               Mme C... B... et M. D... A... B... ont demandé à la Cour nationale du droit d'asile d'annuler les décisions du 21 septembre 2017 du directeur général de l'Office français de protection des réfugiés et apatrides (OFPRA) en tant que celles-ci leur ont seulement accordé le bénéfice de la protection subsidiaire, ainsi que de leur reconnaitre la qualité de réfugié. <br/>
<br/>
              Par une décision n°s 17048833-17048834 du 2 mai 2019, la Cour nationale du droit d'asile a rejeté leur demande. <br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 9 août et 5 novembre 2019 et 3 juin 2020 au secrétariat du contentieux du Conseil d'Etat, Mme B... et M. A... B... demandent au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cette décision ; <br/>
<br/>
              2°) de mettre à la charge de l'OFPRA la somme de 3 500 euros à verser à la SCP Thouvenin, Coudray, Grevy, leur avocat, au titre des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la convention de Genève du 28 juillet 1951 et le protocole signé à New York le 31 janvier 1967 relatifs au statut des réfugiés ; <br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ; <br/>
              - la loi n° 91-647 du 10 juillet 1991 ; <br/>
              - le code de justice administrative et l'ordonnance n° 2020-305 du 25 mars 2020 ; <br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Bertrand Mathieu, conseiller d'Etat en service extraordinaire,<br/>
<br/>
              - les conclusions de Mme Sophie Roussel, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Thouvenin, Coudray, Grévy, avocat de Mme B... et M. A... B... ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1.	La Cour nationale du droit d'asile est tenue, comme toute juridiction administrative, de faire application des règles générales relatives à toutes les productions postérieures à la clôture de l'instruction. Il lui appartient ainsi, dans tous les cas, de prendre connaissance des notes en délibéré et de les viser. Il ressort des pièces de la procédure que Mme B... a produit une note en délibéré qui a été enregistrée le 26 avril 2019 au greffe de la Cour nationale du droit d'asile, après la date de l'audience publique tenue le 10 avril 2019 et avant la lecture de la décision. Or, les visas de la décision du 2 mai 2019 ne font pas mention de cette note en délibéré. Dès lors, la cour a entaché sa décision d'irrégularité.<br/>
<br/>
              2.	Il résulte de ce qui précède que Mme B... et M. A... B... sont fondés, sans qu'il soit besoin de se prononcer sur les autres moyens du pourvoi, à demander l'annulation de la décision de la Cour nationale du droit d'asile qu'ils attaquent.<br/>
<br/>
              3.	Mme B... et M. A... B... ont obtenu le bénéfice de l'aide juridictionnelle. Dès lors, leur avocat peut se prévaloir des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 relative à l'aide juridique. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Office français de protection des réfugiés et apatrides le versement à la SCP Thouvenin, Coudray, Grevy, avocat de Mme B... et M. A... B..., d'une somme de 1 000 euros, sous réserve que cette société renonce à percevoir la somme correspondant à la part contributive de l'Etat.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : La décision du 2 mai 2019 de la Cour nationale du droit d'asile est annulée.<br/>
Article 2 : L'affaire est renvoyée à la Cour nationale du droit d'asile.<br/>
<br/>
Article 3 : L'Office français de protection des réfugiés et apatrides versera, au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991, à la SCP Thouvenin, Coudray, Grevy, une somme de 1 000 euros sous réserve que cette société renonce à percevoir la somme correspondant à la part contributive de l'Etat.<br/>
<br/>
Article 4 : La présente décision sera notifiée à Mme C... B... et M. D... A... B... et à l'Office français de protection des réfugiés et apatrides.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
