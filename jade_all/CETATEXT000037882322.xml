<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037882322</ID>
<ANCIEN_ID>JG_L_2018_12_000000422014</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/88/23/CETATEXT000037882322.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème chambre, 26/12/2018, 422014, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-12-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>422014</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GHESTIN</AVOCATS>
<RAPPORTEUR>Mme Déborah Coricon</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emmanuelle Cortot-Boucher</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:422014.20181226</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A...B...a demandé au juge des référés du tribunal administratif de Versailles, sur le fondement de l'article L. 521-1 du code de justice administrative, de suspendre l'exécution de la délibération n° 01/522 du 8 mars 2018 du conseil municipal de Savigny-sur-Orge.<br/>
<br/>
              Par une ordonnance n° 1803248 du 23 mai 2018, le juge des référés du tribunal administratif de Versailles a rejeté cette demande.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et des observations complémentaires, enregistrés les 5 et 12 juillet et 17 décembre 2018 au secrétariat du contentieux du Conseil d'Etat, M. B...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) statuant en référé, de faire droit à sa demande de suspension de l'exécution de la délibération litigieuse ;<br/>
<br/>
              3°) de mettre à la charge de la commune de Savigny-sur-Orge la somme de 3 500 euros à verser à la SCP Ghestin, avocat de M.B..., au titre des dispositions combinées des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code général des collectivités territoriales ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - la loi n° 2014-58 du 27 janvier 2014 ;<br/>
              - la décision n° 2017-640 QPC du 23 juin 2017 du Conseil constitutionnel ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Déborah Coricon, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Emmanuelle Cortot-Boucher, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Ghestin, avocat de M. B...;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision ". Aux termes du second alinéa de l'article R. 522-1 de ce code : " A peine d'irrecevabilité, les conclusions tendant à la suspension d'une décision administrative ou de certains de ses effets doivent être présentées par requête distincte de la requête à fin d'annulation ou de réformation et accompagnées d'une copie de cette dernière ". En vertu de l'article L. 522-3 du même code, le juge des référés peut, lorsqu'il apparaît manifeste qu'une requête est irrecevable, la rejeter par une ordonnance motivée sans instruction ni audience. Enfin, selon l'article R. 522-2 du même code, les dispositions de l'article R. 612-1 de ce code qui imposent au juge d'inviter l'auteur de conclusions entachées d'une irrecevabilité susceptible d'être couverte après l'expiration du délai de recours à les régulariser, ne sont pas applicables au juge des référés statuant en urgence.<br/>
<br/>
              2. Il résulte des pièces du dossier soumis au juge des référés que M.B..., inscrit sur les listes électorales de la commune de Savigny-sur-Orge où il est domicilié,.... Parmi les pièces produites au soutien de cette requête en référé et reprises dans un inventaire détaillé les présentant par des libellés explicites, M. B...a produit sa requête à fin d'annulation de la délibération litigieuse, sur laquelle apparaissent ses nom et prénom, sa signature et la date du 5 mai 2018, et dont le timbre sec de la juridiction établit qu'elle a été enregistrée le 7 mai 2018. De plus, la lettre de transmission de cette requête en référé précise, à la fois dans l'énoncé de son objet et dans son corps, que la requête en référé est accompagnée de la requête en annulation. Dès lors, en relevant que M. B...ne justifiait pas avoir introduit une requête distincte à fin d'annulation contre la décision dont il demandait la suspension, le juge des référés a entaché son ordonnance de dénaturation. Par suite et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, son ordonnance doit être annulée.<br/>
<br/>
              3. Il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au titre de la procédure de référé engagée par M.B..., en application des dispositions de l'article L. 821-2 du code de justice administrative.<br/>
<br/>
              4. Aux termes du premier alinéa de l'article R. 119 du code électoral : " Les réclamations contre les opérations électorales doivent être consignées au procès-verbal, sinon être déposées, à peine d'irrecevabilité, au plus tard à dix-huit heures le cinquième jour qui suit l'élection, à la sous-préfecture ou à la préfecture ". Ces dispositions relatives aux élections des conseillers municipaux sont applicables, sauf texte contraire, aux élections destinées à constituer les organes délibérants des établissements publics de coopération intercommunale.<br/>
<br/>
              5. Il ressort des pièces du dossier que la réclamation de M.B..., dont l'unique objet était de remettre en cause l'élection d'un conseiller de territoire intervenue le 8 mars 2018, a été enregistrée le 7 mai 2018 au greffe du tribunal administratif de Versailles, soit postérieurement au délai de 5 jours fixé par les dispositions de l'article R. 119 précité. Sa requête est dès lors tardive, et partant, irrecevable.<br/>
<br/>
              6. Il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par M. B...au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'ordonnance du juge des référés du tribunal administratif de Versailles du 23 mai 2018 est annulée.<br/>
Article 2 : La demande présentée par M. B...devant le juge des référés du tribunal administratif de Versailles est rejetée.<br/>
Article 3 : Les conclusions de M. B...présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée à M. A...B...et à la commune de Savigny-sur-Orge.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
