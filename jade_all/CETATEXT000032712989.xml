<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032712989</ID>
<ANCIEN_ID>JG_L_2016_06_000000379852</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/71/29/CETATEXT000032712989.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème - 10ème chambres réunies, 15/06/2016, 379852, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-06-15</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>379852</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème - 10ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP WAQUET, FARGE, HAZAN</AVOCATS>
<RAPPORTEUR>M. Jean-Luc Matt</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emilie Bokdam-Tognetti</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2016:379852.20160615</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La SAS Festina France a demandé au tribunal administratif de Besançon la décharge de la somme de 183 339 euros qu'elle a acquittée au titre de la retenue à la source sur des prestations publicitaires et sportives payées en décembre 1998 à la société de droit andorran Prosport. Par un jugement n° 1101078 du 20 décembre 2012, le tribunal administratif de Besançon a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 13NC00276 du 27 mars 2014, la cour administrative d'appel de Nancy a rejeté l'appel formé par la SAS Festina France contre ce jugement.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 9 mai 2014, 5 août 2014 et 2 juin 2015 au secrétariat du contentieux du Conseil d'Etat, la SAS Festina France demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Luc Matt, maître des requêtes,<br/>
<br/>
              - les conclusions de Mme Emilie Bokdam-Tognetti, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Waquet, Farge, Hazan, avocat de la SAS Festina France ;<br/>
<br/>
<br/>
<br/>1. Considérant, d'une part, que l'article 182 B du code général des impôts institue une retenue à la source sur les sommes payées " par un débiteur qui exerce une activité en France à des personnes ou des sociétés, relevant de l'impôt sur le revenu ou de l'impôt sur les sociétés, qui n'ont pas dans ce pays d'installation professionnelle permanente " ; qu'en vertu de l'article 219 quinquies du même code, cette retenue à la source est " imputable sur le montant de l'impôt sur les sociétés éventuellement exigible à raison des revenus qui l'ont supportée " ; <br/>
<br/>
              2. Considérant, d'autre part, qu'en vertu du c de l'article R. 196-1 du livre des procédures fiscales dans sa rédaction alors applicable, une réclamation doit être présentée au plus tard le 31 décembre de la deuxième année suivant celle " de la réalisation de l'événement qui motive la réclamation " ; que seuls doivent être regardés comme constituant le point de départ du délai ainsi prévu les événements qui sont de nature à exercer une influence sur le principe même de l'imposition, son régime ou son mode de calcul ;<br/>
<br/>
              3. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que la SAS Festina France a acquitté spontanément, le 12 janvier 1999, un montant de retenue à la source de 183 339 euros, en application de l'article 182 B du code général des impôts, sur des sommes qu'elle avait versées en décembre 1998 en rémunération de prestations publicitaires et sportives à la SA Prosport Team Festina-Lotus, dont le siège social était situé en Andorre ; que, par un arrêt du 17 décembre 2009, la cour administrative d'appel de Lyon a jugé que la SA Prosport Team Festina-Lotus devait être regardée comme une entreprise exploitée en France et qu'elle était donc passible de l'impôt sur les sociétés au titre de l'exercice clos le 31 décembre 1998 ; que la SAS Festina France a présenté une réclamation le 4 janvier 2011, en vue d'obtenir le remboursement du montant de retenue à la source acquitté en 1999, en soutenant que cet arrêt constituait, au sens de l'article R. 196-2 du livre des procédures fiscales, un événement de nature à rouvrir le délai de réclamation ; qu'elle se pourvoit en cassation contre l'arrêt du 27 mars 2014 par lequel la cour administrative d'appel de Nancy, confirmant le jugement du tribunal administratif de Besançon, a rejeté son appel ;<br/>
<br/>
              4. Considérant que si les dispositions de l'article 182 B du code général des impôts doivent être regardées comme instituant des modalités particulières de liquidation et de règlement de l'impôt sur les sociétés lorsque le bénéficiaire des revenus ayant supporté la retenue à la source qu'elles instituent est imposable en France à cet impôt, seule la société bénéficiaire des revenus peut demander, en vertu de l'article 219 quinquies du code général des impôts, que la retenue à la source soit imputée sur l'impôt sur les sociétés auquel elle a été assujettie ; que, dès lors, ainsi que l'a jugé la cour sans erreur de droit, la décision par laquelle le bénéficiaire des revenus a été reconnu redevable de l'impôt sur les sociétés ne constitue pas, pour la personne lui ayant versé ces revenus, un événement de nature à rouvrir le délai dans lequel, en application de l'article R. 196-2 du livre des procédures fiscales, elle est recevable à présenter une réclamation tendant à la restitution de la retenue à la source qu'ils ont supportée ;<br/>
<br/>
              5. Considérant qu'il résulte de ce qui précède que le pourvoi de la SAS Festina France doit être rejeté, y compris les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
Article 1er : Le pourvoi de la SAS Festina France est rejeté.<br/>
Article 2 : La présente décision sera notifiée à la SAS Festina France et au ministre des finances et des comptes publics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
