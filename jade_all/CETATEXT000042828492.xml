<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042828492</ID>
<ANCIEN_ID>JG_L_2020_12_000000436980</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/82/84/CETATEXT000042828492.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème - 1ère chambres réunies, 29/12/2020, 436980, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-12-29</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>436980</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème - 1ère chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Catherine Brouard-Gallet</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Raphaël Chambon</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:436980.20201229</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu les procédures suivantes :<br/>
<br/>
              1° Sous le n° 436980, par une requête et un mémoire en réplique, enregistrés le 20 décembre 2019 et le 14 août 2020 au secrétariat du contentieux du Conseil d'Etat, la Fédération nationale de l'enseignement privé demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir l'arrêté du ministre de l'éducation nationale et de la jeunesse du 11 octobre 2019 modifiant l'arrêté du 16 juillet 2018 relatif aux modalités d'organisation du contrôle continu pour l'évaluation des enseignements dispensés dans les classes conduisant aux baccalauréats général et technologique, ainsi que l'article 9 de l'arrêté du 16 juillet 2018 modifié ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              2 ° Sous le n° 440927, par une requête, un mémoire en réplique et un nouveau mémoire, enregistrés les 29 mai, 21 septembre et 3 décembre 2020 au secrétariat du contentieux du Conseil d'Etat, la Fédération nationale de l'enseignement privé demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision implicite de rejet née du silence gardé par le ministre de l'éducation nationale et de la jeunesse sur sa demande d'abrogation, à défaut de modification, de l'arrêté du 26 mars 2019 modifiant l'arrêté du 16 juillet 2018 relatif aux modalités d'organisation du contrôle continu pour l'évaluation des enseignements dispensés dans les classes conduisant au baccalauréat général et au baccalauréat technologique ;<br/>
<br/>
              2°) d'annuler pour excès de pouvoir l'arrêté du 26 mars 2019 ;  <br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 10 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
			....................................................................................<br/>
<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu : <br/>
              - le code de l'éducation ;<br/>
              - le décret n° 2018-614 du 16 juillet 2018 ;<br/>
              - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme A... B..., conseillère d'Etat en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Raphaël Chambon, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. Les requêtes enregistrées sous les n°s 440927 et 436980, présentées par la Fédération nationale de l'enseignement privé (FNEP), tendent à l'annulation pour excès de pouvoir, respectivement, de l'arrêté du 26 mars 2019 modifiant l'arrêté du 16 juillet 2018 relatif aux modalités d'organisation du contrôle continu pour l'évaluation des enseignements dispensés dans les classes conduisant au baccalauréat général et au baccalauréat technologique et de la décision implicite par laquelle le ministre chargé de l'éducation nationale a rejeté sa demande d'abrogation de cet arrêté, et de l'arrêté du 11 octobre 2019 modifiant ce même arrêté du 16 juillet 2018. Il y a lieu de joindre ces requêtes pour statuer par une seule décision.<br/>
<br/>
              Sur l'intervention de l'association " Créer son école " : <br/>
<br/>
              2. Les conclusions de la requête n° 440927 de la Fédération nationale de l'enseignement privé ne tendent pas à l'annulation de l'article 9 de l'arrêté du 16 juillet 2018 dans sa rédaction issue de l'arrêté du 11 octobre 2019. Par suite, l'intervention présentée par l'association " Créer son école " au soutien de cette requête n° 440927, dirigée contre ces dispositions, n'est pas recevable.<br/>
<br/>
              Sur le cadre juridique :<br/>
<br/>
              3. Le décret du 16 juillet 2018, relatif aux enseignements conduisant au baccalauréat général et aux formations technologiques conduisant au baccalauréat technologique, a modifié l'article D. 334-4 du code de l'éducation en disposant, pour ce qui est du baccalauréat général, que : " l'évaluation des enseignements obligatoires repose sur des épreuves terminales et sur des évaluations de contrôle continu tout au long du cycle terminal " et qu'" un arrêté du ministre chargé de l'éducation nationale définit les modalités d'organisation du contrôle continu pour le baccalauréat général et les conditions dans lesquelles est attribuée une note de contrôle continu aux candidats qui ne suivent les cours d'aucun établissement, aux candidats inscrits dans un établissement d'enseignement privé hors contrat, aux candidats scolarisés au Centre national d'enseignement à distance et aux sportifs de haut niveau (...) ". L'article D. 336-4 du même code comporte des dispositions identiques applicables au baccalauréat technologique.<br/>
<br/>
              4. Pour l'application de ces dispositions, l'arrêté du 16 juillet 2018 du ministre chargé de l'éducation nationale a défini les modalités d'organisation du contrôle continu. Par ses articles 1er à 8, il a fixé les modalités d'organisation de ce contrôle pour les candidats scolarisés dans les établissements publics d'enseignement et dans les établissements d'enseignement privés sous contrat. Par son article 9, il a fixé des modalités particulières pour les candidats scolarisés dans les établissements d'enseignement privés hors contrat, les candidats inscrits au Centre national d'enseignement à distance et les candidats qui ne sont inscrits dans aucun établissement. <br/>
<br/>
              5. En vertu des articles 1er et 2 de l'arrêté du 16 juillet 2018, les candidats scolarisés dans les établissements d'enseignement publics ou privés sous contrat subissent trois sessions d'épreuves de contrôle continu, deux en classe de première et une en classe de terminale. <br/>
<br/>
              6. Dans sa rédaction d'origine, l'article 9 du même arrêté prévoyait que les candidats scolarisés dans les établissements d'enseignement privés hors contrat, les candidats scolarisés au Centre national d'enseignement à distance et les candidats qui ne sont scolarisés dans aucun établissement subissent à la fin de l'année de terminale une épreuve ponctuelle pour chaque enseignement faisant l'objet du contrôle continu. <br/>
<br/>
              7. Dans sa rédaction issue de l'arrêté du 26 mars 2019, le I de cet article 9 prévoyait que ces candidats subissent deux sessions d'épreuves, consistant en une épreuve ponctuelle à la fin de l'année de première pour l'enseignement de spécialité abandonné en terminale et une session d'épreuves communes de contrôle continu dans les autres enseignements, qui a lieu au deuxième trimestre de la classe de terminale. Par ailleurs, le II de l'article 9 dans sa rédaction issue de l'arrêté du 26 mars 2019 a modifié les modalités de détermination de la note de contrôle continu attribuée aux candidats inscrits au Centre national d'enseignement à distance sur le fondement des dispositions du dernier alinéa de l'article R. 426-2 du code de l'éducation. <br/>
<br/>
              8. Enfin, l'arrêté du 11 octobre 2019 a modifié l'article 2 et le I de l'article 9 de l'arrêté du 16 juillet 2018 en décalant du deuxième au troisième trimestre de l'année de terminale la série d'épreuves communes de contrôle continu passées en classe de terminale, tant par les candidats scolarisés dans l'enseignement public et dans l'enseignement privé sous contrat que pour les autres candidats.<br/>
<br/>
              9. Eu égard aux moyens soulevés, les requêtes doivent être regardées comme visant les dispositions de l'article 9 de l'arrêté du 16 juillet 2018 dans sa rédaction issue des arrêtés des 26 mars et 11 octobre 2019, en tant qu'elles concernent les candidats scolarisés dans des établissements d'enseignement privés hors contrat. <br/>
<br/>
              Sur la requête n° 440927 :<br/>
<br/>
              Sur les conclusions à fin de non-lieu :<br/>
<br/>
              10. La fédération requérante ne conteste pas les dispositions de l'article 9 de l'arrêté du 16 juillet 2018 dans sa rédaction issue de l'arrêté du 26 mars 2019 en tant qu'elles avancent du troisième au deuxième trimestre les épreuves ponctuelles de spécialité faisant l'objet d'une épreuve de contrôle continu au cours de l'année de terminale. Par suite, le ministre chargé de l'éducation nationale n'est pas fondé à soutenir que la requête aurait perdu son objet au motif que l'arrêté du 11 octobre 2019 a modifié ces dispositions en reportant ces épreuves du deuxième au troisième trimestre de l'année de terminale. <br/>
<br/>
              Sur les conclusions tendant à l'annulation de l'arrêté du 26 mars 2019 :<br/>
<br/>
              11. L'article R. 421-1 du code de justice administrative dispose que : " Sauf en matière de travaux publics, la juridiction ne peut être saisie que par voie de recours formé contre une décision, et ce, dans les deux mois à partir de la notification ou de la publication de la décision attaquée (...) ". L'arrêté du 26 mars 2019 a été publié au Journal officiel de la République française le 30 mars 2019. La requête n° 440927, enregistrée au secrétariat du contentieux du Conseil d'Etat le 29 mai 2020, est tardive, et par suite irrecevable, en tant qu'elle tend à l'annulation pour excès de pouvoir de cet arrêté. <br/>
<br/>
              Sur les conclusions tendant à l'annulation du refus d'abroger les dispositions de l'article 9 de l'arrêté du 16 juillet 2018 issues de l'arrêté du 26 mars 2019 :<br/>
<br/>
              12. Les articles 1er et 2 de l'arrêté du 16 juillet 2018 prévoient que la note de contrôle continu attribuée aux candidats au baccalauréat qui sont scolarisés dans les établissements publics d'enseignement et dans les établissements d'enseignement privés sous contrat compte pour quarante pour cent des coefficients attribués pour l'examen et est fixée, pour une part de trente pour cent, sur la base de trois sessions d'épreuves dites " épreuves communes de contrôle continu " et pour une part de dix pour cent, sur la base de l'évaluation des résultats de l'élève au cours du cycle terminal, telle qu'elle résulte des notes attribuées par ses professeurs. Le I de l'article 9 de cet arrêté dans sa rédaction issue de l'arrêté du 26 mars 2019 attaqué prévoit en revanche que, pour les candidats scolarisés dans les établissements privés hors contrat, ceux-ci sont convoqués à une épreuve ponctuelle pour l'enseignement de spécialité ne donnant pas lieu à une épreuve terminale et à une épreuve ponctuelle pour chacun des autres enseignements faisant l'objet d'épreuves communes de contrôle continu, la note de contrôle continu mentionnée à l'article 1er étant fixée, conformément au II de l'article 9, en tenant compte des notes obtenues aux épreuves ponctuelles prévues au I. <br/>
<br/>
              13. En premier lieu, conformément aux dispositions de l'article D. 334-4 du code de l'éducation rappelées au point 3, le II de l'article 9 de l'arrêté du 16 juillet 2018 dans sa rédaction issue de l'arrêté du 26 mars 2019 précise qu'une note de contrôle continu, déterminée en fonction des notes obtenues aux épreuves ponctuelles de contrôle continu, est attribuée aux candidats qui ne suivent les cours d'aucun établissement et à ceux qui sont scolarisés dans les établissements privés hors contrat. Par suite, le moyen tiré de ce que l'arrêté attaqué méconnaîtrait les dispositions précitées en ce qu'il aurait supprimé toute prise en compte du contrôle continu ne peut qu'être écarté.<br/>
<br/>
              14. En deuxième lieu, la fédération requérante soutient que les dispositions citées ci-dessus méconnaissent l'égalité de traitement entre candidats à un même diplôme en ce qu'elles soumettent les candidats scolarisés dans les établissements privés hors contrat non plus à une seule mais à deux sessions d'épreuves ponctuelles de contrôle continu, et en ce qu'elles ne font pas bénéficier ces candidats de l'évaluation chiffrée annuelle des résultats du cycle terminal alors que les autres candidats, d'une part, sont soumis à trois sessions d'épreuve de contrôle continu, d'autre part bénéficient à hauteur de dix pour cent de la prise en compte de l'évaluation chiffrée annuelle des résultats au cours du cycle terminal dans la note de contrôle continu. Elle fait valoir en outre que les candidats inscrits au Centre national d'enseignement à distance sur le fondement du dernier alinéa de l'article R. 426-2 du code de l'éducation bénéficient également de la prise en compte de l'évaluation chiffrée annuelle de leurs résultats dans la note de contrôle continu. <br/>
<br/>
              15. Toutefois, l'article L. 442-2 du code de l'éducation dispose que : " (...) le contrôle de l'Etat sur les établissements d'enseignement privés qui ne sont pas liés à l'Etat par contrat se limite aux titres exigés des directeurs et des enseignants, à l'obligation scolaire, à l'instruction obligatoire, au respect de l'ordre public, à la prévention sanitaire et sociale et à la protection de l'enfance et de la jeunesse ". Compte tenu de la liberté ainsi reconnue à ces établissements en matière de programmes d'enseignement et de déroulement de la scolarité pour l'enseignement du second degré, la différence de traitement consistant à n'organiser, pour les élèves qui y sont scolarisés, qu'une seule session d'examen dans chaque matière est justifiée par une différence de situation qui est en rapport direct avec l'objet de ces dispositions et qui n'est pas manifestement disproportionnée. Par ailleurs, la faculté de prévoir des modalités distinctes de fixation de la note de contrôle continu du baccalauréat d'une part pour les candidats inscrits dans des établissements publics et des établissements privés sous contrat, d'autre part pour ceux qui ne suivent les cours d'aucun établissement ou sont inscrits dans des établissements privés hors contrat, ainsi que pour ceux qui sont scolarisés au Centre national d'enseignement à distance, résulte des termes mêmes des dispositions de l'article D. 334-4 du code de l'éducation. La différence de traitement réservée par l'arrêté attaqué à ces candidats par rapport à ceux des établissements publics ou privés sous contrat et aux candidats inscrits au Centre national d'enseignement à distance trouve ainsi son origine dans le décret du 16 juillet 2018. La fédération requérante n'est, par suite, pas fondée à soutenir que l'article 9 de l'arrêté du 16 juillet 2018 dans sa rédaction issue de l'arrêté du 26 mars 2019 serait entaché d'illégalité. <br/>
<br/>
              16. En dernier lieu, la fédération requérante n'est pas fondée à soutenir que les dispositions de l'arrêté du 26 mars 2019, en ce qu'elles prévoient des modalités d'organisation du contrôle continu différentes pour les candidats scolarisés dans les établissements d'établissement privé hors contrat et pour ceux qui sont scolarisés dans les établissements d'enseignement publics ou privés sous contrat, méconnaîtraient le principe de la liberté de l'enseignement.<br/>
<br/>
              Sur la requête n° 436980 :<br/>
<br/>
              17. L'arrêté du 16 juillet 2018 a été publié au Journal officiel de la République française le 17 juillet 2018. Les conclusions de la requête n° 436980, enregistrée le 20 décembre 2019 au secrétariat du contentieux du Conseil d'Etat, sont tardives et par suite irrecevables en tant qu'elles sont dirigées contre les dispositions de l'article 9 de l'arrêté du 16 juillet 2018 modifié qui n'ont pas été modifiées par l'arrêté du 11 octobre 2019.<br/>
<br/>
              18. En premier lieu, aux termes de l'article L. 221-5 du code des relations entre le public et l'administration : " L'autorité administrative investie du pouvoir réglementaire est tenue, dans la limite de ses compétences, d'édicter des mesures transitoires dans les conditions prévues à l'article L. 221-6 lorsque l'application immédiate d'une nouvelle réglementation est impossible ou qu'elle entraîne, au regard de l'objet et des effets de ses dispositions, une atteinte excessive aux intérêts publics ou privés en cause (...) ". En application de l'article 34 du décret du 16 juillet 2018, les dispositions relatives aux nouvelles épreuves du baccalauréat organisées en classe de terminale sont applicables au cours ou à la fin de l'année scolaire 2020-2021. L'arrêté attaqué du 11 octobre 2019 s'étant borné à décaler du deuxième au troisième trimestre de la classe de terminale la tenue des épreuves de contrôle continu pour toutes les disciplines évaluées au titre de ce contrôle en classe de terminale, la Fédération nationale de l'enseignement privé n'est pas fondée à soutenir que, faute de mesures transitoires différant son entrée en vigueur à l'année scolaire 2021-2022, il méconnaîtrait le principe de sécurité juridique, mis en oeuvre par l'article L. 221-5 du code des relations entre le public et l'administration. <br/>
<br/>
              19. En deuxième lieu, l'article L. 612-3 du code de l'éducation dispose que : " (...) L'inscription dans une formation du premier cycle dispensée par un établissement public est précédée d'une procédure nationale de préinscription " - dite " Parcoursup " - qui permet aux candidats de bénéficier d'un dispositif d'information et d'orientation (...) ". Aux termes de l'article D. 612-1-5 du même code : " Les caractéristiques des formations proposées sur la plateforme Parcoursup sont portées à la connaissance des candidats. Elles comprennent notamment : (...) - les éléments, pièces et documents qui sont demandés pour l'analyse des candidatures ". L'arrêté attaqué, qui fixe le calendrier des épreuves de contrôle continu organisées en classe de terminale pour l'ensemble des candidats au baccalauréat, n'a ni pour objet, ni pour effet de déterminer en application de l'article D. 612-1-5 du code de l'éducation les éléments, pièces et documents demandés pour l'analyse des candidatures aux formations initiales du premier cycle de l'enseignement supérieur dans le cadre de la procédure nationale de préinscription. <br/>
<br/>
              20. Dès lors, s'il appartient au ministre chargé de l'enseignement supérieur de veiller au respect de l'égalité entre les candidats à une formation initiale du premier cycle de l'enseignement supérieur, la Fédération nationale de l'enseignement privé ne peut utilement soutenir, dans le présent litige, qu'en décalant du deuxième au troisième trimestre de la classe de terminale les épreuves communes de contrôle continu subies en terminale, l'arrêté du 11 octobre 2019 méconnaîtrait le principe d'égalité en ce que les candidats scolarisés dans les établissements d'enseignement privés hors contrat seront dans l'impossibilité de se prévaloir des résultats des épreuves de contrôle continu de la classe de terminale dans le cadre de la procédure nationale de préinscription dans les établissements d'enseignement supérieur, dite " Parcoursup ", prévue à l'article L. 612-3 du code de l'éducation, alors que les candidats scolarisés dans les établissements d'enseignement publics ou privés sous contrat pourront, pour leur part, se prévaloir des deux séries d'épreuves de contrôle continu passées en classe de première. <br/>
<br/>
              21. En troisième lieu, aux termes de l'article L. 442-2 du code de l'éducation : " (...) le contrôle de l'Etat sur les établissements d'enseignement privés qui ne sont pas liés à l'Etat par contrat se limite aux titres exigés des directeurs et des enseignants, à l'obligation scolaire, à l'instruction obligatoire, au respect de l'ordre public, à la prévention sanitaire et sociale et à la protection de l'enfance et de la jeunesse ". Les dispositions de l'arrêté du 11 octobre 2019, qui se bornent à décaler l'organisation de l'épreuve de contrôle continu du baccalauréat organisée en classe de terminale du deuxième au troisième trimestre, n'ont pas pour effet de porter atteinte à la liberté reconnue à ces établissements en matière de programmes d'enseignement et de déroulement de la scolarité pour l'enseignement du second degré. <br/>
<br/>
              22. Il résulte de tout ce qui précède que les requêtes n°s 436980 et 440927 de la Fédération nationale de l'enseignement privé doivent être rejetées, y compris les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative, sans qu'il soit besoin de se prononcer sur les autres fins de non-recevoir opposées par le ministre de l'éducation nationale, de la jeunesse et des sports.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : L'intervention de l'association " Créer son école " n'est pas admise.<br/>
<br/>
Article 2 : Les requêtes de la Fédération nationale de l'enseignement privé sont rejetées.<br/>
Article 3 : La présente décision sera notifiée à la Fédération nationale de l'enseignement privé, à l'association " Créer son école " et au ministre de l'éducation nationale, de la jeunesse et des sports.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
