<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026856788</ID>
<ANCIEN_ID>JG_L_2012_12_000000339118</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/85/67/CETATEXT000026856788.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème sous-section jugeant seule, 28/12/2012, 339118, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2012-12-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>339118</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR>M. Jean-Claude Hassan</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Edouard Crépey</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2012:339118.20121228</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi, enregistré le 30 avril 2010 au secrétariat du contentieux du Conseil d'Etat, présenté par le ministre du budget, des comptes publics et de la reforme de l'Etat ; le ministre demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler les articles 1er et 2 de l'arrêt n° 07PA02535 du 4 mars 2010 par lequel la cour administrative d'appel de Paris, faisant partiellement droit à l'appel de M. Alain A et réformant le jugement n° 0113568/1 du 22 mai 2007 du tribunal administratif de Paris, a déchargé l'intéressé des cotisations supplémentaires d'impôt sur le revenu auxquelles il a été assujetti au titre des années 1994 et 1995 ainsi que des pénalités dont elles ont été assorties ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rétablir M. A aux rôles de l'impôt sur le revenu au titre des années 1994 et 1995, à hauteur des droits et pénalités dont la décharge a été prononcée à tort par le juge d'appel ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code général des impôts et le livre des procédures fiscales ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Claude Hassan, Conseiller d'Etat,  <br/>
<br/>
              - les observations de la SCP Piwnica, Molinié, avocat de M. A,<br/>
<br/>
              - les conclusions de M. Edouard Crépey, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Piwnica, Molinié, avocat de M. A ;<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que M. A a fait l'objet d'un examen de sa situation fiscale personnelle au titre des années 1994 et 1995 et d'un contrôle sur pièces au titre de l'année 1996 ; que la société civile professionnelle (SCP) d'avocats A-Belot et l'entreprise unipersonnelle à responsabilité limitée (EURL) Joal Investissement, dont il est l'unique associé, ont fait l'objet de vérifications de comptabilité au titre des mêmes années ; qu'à l'issue de ces contrôles, l'administration fiscale, après avoir rectifié les bénéfices non commerciaux provenant de l'activité de la SCP pour les années 1994 et 1995 et les bénéfices industriels et commerciaux procédant de l'activité de l'EURL pour les années 1993 et 1994, a notifié à M. A des suppléments d'imposition sur le revenu et de contributions sociales au titre des années 1994, 1995 et 1996, résultant notamment de ces vérifications ; que M. A a contesté ces impositions supplémentaires devant le tribunal administratif de Paris qui, par un jugement du 22 mai 2007, a, d'une part, constaté qu'il n'y avait plus lieu de statuer à hauteur du dégrèvement consenti par l'administration sur les impositions mises à la charge du requérant au titre de l'année 1995, d'autre part fait partiellement droit à sa demande de décharge en rétablissant l'abattement pour adhésion à une association de gestion agréée ; que, saisie par M. A, la cour administrative d'appel de Paris a, par un arrêt du 4 mars 2010, déchargé le requérant des cotisations supplémentaires d'impôt sur le revenu auxquelles il avait été assujetti au titre des années 1994 et 1995 et des pénalités dont elles ont été assorties, au motif que la procédure de vérification sur place des documents comptables de l'EURL Joal Investissement était irrégulière pour avoir duré plus de trois mois et rejeté le surplus des conclusions de son appel relatif à l'année 1996 ; que le ministre du budget, des comptes publics et de la reforme de l'Etat  se pourvoit en cassation contre cet arrêt en tant qu'il a fait droit aux conclusions de M. A ;<br/>
<br/>
              2. Considérant qu'en jugeant, pour prononcer la décharge des impositions litigieuses, qu'elles procédaient d'une vérification de comptabilité irrégulière, la cour administrative d'appel a porté sur les faits du litige une appréciation souveraine insusceptible d'être discutée devant le juge de cassation ; que la seule circonstance qu'aucune rectification n'avait été apportée au résultat déclaré à la clôture de l'exercice 1995 de l'EURL  ne faisait pas, par elle-même, obstacle à ce que la cour déduisît de l'irrégularité de la vérification de comptabilité de cette société la décharge des suppléments à l'imposition sur le revenu de son unique associé au titre de l'année 1995 ;<br/>
<br/>
              3. Considérant que dès lors qu'elle prononçait la décharge totale des impositions supplémentaires auxquelles M. A avait été assujetti au titre des années 1994 et 1995, la cour administrative d'appel n'était pas tenue, contrairement à ce que soutient le ministre, de préciser le montant des droits à restituer ;<br/>
<br/>
              4. Considérant qu'aux termes de l'article L. 52 du livre des procédures fiscales : " Sous peine de nullité de l'imposition, la vérification sur place des livres ou documents comptables ne peut s'étendre sur une durée supérieure à trois mois en ce qui concerne : / 1° Les entreprises industrielles et commerciales ou les contribuables se livrant à une activité non commerciale dont le chiffre d'affaires ou le montant annuel des recettes brutes n'excède pas les limites prévues au I de l'article 302 septies A du code général des impôts (...) " ;<br/>
<br/>
              5. Considérant que, pour juger que la vérification de l'EURL Joal Investissement avait excédé la durée maximale de trois mois prévue par l'article L. 52 précité, la cour administrative d'appel a relevé, d'une part, que la notification de redressement en date du 17 décembre 1997 indiquait que : " pour l'année 1995, la vérification est en cours ", d'autre part que l'administration n'apportait aucun élément de nature à établir que, contrairement à ce que pouvait laisser entendre cette mention, le dernier contrôle sur place avait effectivement eu lieu avant l'expiration, le 21 juillet 1997, du délai de trois mois ; qu'en statuant ainsi la cour, qui n'était pas tenue, pour juger dépassé le délai de trois mois, de déterminer la date de la dernière intervention sur place du vérificateur, mais pouvait, comme elle l'a fait, estimer qu'il résultait de l'instruction que ce délai avait été méconnu, a porté sur la régularité de la procédure une appréciation souveraine exempte de dénaturation et n'a pas fait peser sur la seule administration fiscale la charge de la preuve du respect de ce délai ; <br/>
<br/>
              6. Considérant qu'il résulte de ce qui précède que le ministre du budget, des comptes publics et de la reforme de l'Etat  n'est pas fondé à demander l'annulation de l'arrêt qu'il attaque ; qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 3 000 euros au titre des frais exposés par M. A et non compris dans les dépens ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : Le pourvoi du ministre du budget, des comptes publics et de la réforme de l'Etat, est rejeté.<br/>
<br/>
Article 2 : L'Etat versera la somme de 3 000 euros à M. A au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article  3 : La présente décision sera notifiée au ministre de l'économie et des finances et à M. Alain A.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
