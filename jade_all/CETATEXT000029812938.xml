<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029812938</ID>
<ANCIEN_ID>JG_L_2014_11_000000361063</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/81/29/CETATEXT000029812938.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème / 1ère SSR, 28/11/2014, 361063</TITRE>
<DATE_DEC>2014-11-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>361063</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème / 1ère SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GATINEAU, FATTACCINI</AVOCATS>
<RAPPORTEUR>Mme Mireille Le Corre</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Suzanne von Coester</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:361063.20141128</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 16 juillet et 16 octobre 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour Mme A...B..., demeurant..., Mme C...B..., demeurant..., M. D...B..., demeurant ... et M. E...B..., demeurant ... ; Mme B...et autres demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 11NC0414 du 14 mai 2012 par lequel la cour administrative d'appel de Nancy a rejeté leur requête tendant, d'une part, à l'annulation du jugement n° 0900950 du 13 janvier 2011 par lequel le tribunal administratif de Besançon a rejeté leur demande tendant à l'annulation de l'arrêté du 27 février 2009 par lequel le préfet de la région Franche-Comté a inscrit au titre des monuments historiques, un pavillon de jardin dit "loge", avec ses décors, situé sur la parcelle cadastrée n° 85 à Montbéliard, d'autre part, à l'annulation de cet arrêté ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à leur appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code civil ;<br/>
<br/>
              Vu le code du patrimoine ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Mireille Le Corre, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Suzanne von Coester, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Gatineau, Fattaccini, avocat de Mme A...B..., de Mme C...B..., de M. D...B...et de M. E...B...;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond qu'après avis de la commission régionale du patrimoine et des sites en date du 4 décembre 2008, le préfet de la région Franche-Comté a, par arrêté du 27 février 2009, inscrit au titre des monuments historiques la totalité d'un pavillon de jardin dit " loge ", y compris ses décors, situé sur la parcelle cadastrée n° 85 à Montbéliard (Doubs) ; que par un jugement du 13 janvier 2011, le tribunal administratif de Besançon a rejeté la demande présentée par Mme B...et autres, propriétaires indivis du pavillon, tendant à l'annulation de cet arrêté ; que ces derniers se pourvoient en cassation contre l'arrêt du 14 mai 2012 par lequel la cour administrative d'appel de Nancy a confirmé ce jugement ;<br/>
<br/>
              Sur la régularité de l'arrêt attaqué :<br/>
<br/>
              2. Considérant, en premier lieu, qu'aux termes de l'article R. 741-7 du code de justice administrative : " Dans les tribunaux administratifs et les cours administratives d'appel, la minute de la décision est signée par le président de la formation de jugement, le rapporteur et le greffier d'audience " ; qu'il ressort des pièces du dossier soumis aux juges du fond que sont apposées, sur la minute du jugement attaqué, les signatures manuscrites du président de la formation de jugement, du magistrat rapporteur ainsi que de la greffière ; que, par suite, le moyen tiré du défaut de signature de la minute manque en fait ;<br/>
<br/>
              3. Considérant, en deuxième lieu, qu'aux termes de l'article R. 613-3 du code de justice administrative : " Les mémoires produits après la clôture de l'instruction ne donnent pas lieu à communication et ne sont pas examinés par la juridiction (...) " ; qu'il résulte de ces dispositions que lorsque, postérieurement à la clôture de l'instruction, le juge est saisi d'une production, mémoire ou pièce, émanant de l'une des parties à l'instance, il lui appartient de prendre connaissance de cette production avant de rendre sa décision, ainsi que de la viser sans l'analyser, mais qu'il ne peut la prendre en compte sans avoir préalablement rouvert l'instruction afin de la soumettre au débat contradictoire ; <br/>
<br/>
              4. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond  que, postérieurement à la clôture de l'instruction devant la cour administrative d'appel de Nancy le 13 février 2012, le ministre de la culture et de la communication a produit un nouveau mémoire en réponse enregistré le 20 avril 2012 ; que l'arrêt attaqué mentionne ce mémoire dans ses visas ; que, contrairement à ce que soutiennent les requérants, ce mémoire n'apporte pas d'éléments qui n'auraient pas été développés dans les précédentes écritures du ministre ; que la cour n'était donc pas tenue de rouvrir l'instruction ; que, par suite, le moyen tiré du caractère irrégulier de la procédure n'est pas fondé ; <br/>
<br/>
              5. Considérant, en troisième lieu, qu'aux termes de l'article 516 du code civil : " Tous les biens sont meubles ou immeubles " ; qu'aux termes de l'article 517 du même code : " Les  biens sont immeubles, ou par leur nature, ou par leur destination, ou par l'objet auquel ils s'appliquent " ; qu'aux termes de l'article 518 du même code : " Les fonds de terre et les bâtiments sont immeubles par leur nature " ; qu'aux termes du dernier alinéa de l'article 524 du même code : " Sont aussi immeubles par destination, tous effets mobiliers que le propriétaire a attachés au fonds à perpétuelle demeure " ; qu'aux termes du premier alinéa de l'article            L. 621-25 du code du patrimoine : " Les immeubles ou parties d'immeubles publics ou privés qui, sans justifier une demande de classement immédiat au titre des monuments historiques, présentent un intérêt d'histoire ou d'art suffisant pour en rendre désirable la préservation peuvent, à toute époque, être inscrits, par décision de l'autorité administrative, au titre des monuments historiques " ; qu'aux termes de l'article L. 622-20 du même code : " Les objets mobiliers, soit meubles proprement dits, soit immeubles par destination qui, sans justifier une demande de classement immédiat, présentent, au point de vue de l'histoire, de l'art, de la science ou de la technique, un intérêt suffisant pour en rendre désirable la préservation, peuvent, à toute époque, être inscrits au titre des monuments historiques. Les objets mobiliers appartenant à une personne privée ne peuvent être inscrits qu'avec son consentement " ; <br/>
<br/>
              6. Considérant que, pour écarter le moyen tiré de ce que l'inscription des décors du pavillon au titre des monuments historiques, en l'absence du consentement des propriétaires, aurait méconnu les dispositions précitées de l'article L. 622-20 du code du patrimoine, la cour s'est fondée sur ce que les panneaux de bois peints avaient le caractère d'immeuble par nature et non par destination ; qu'en affirmant au soutien de ce moyen que les dispositions de l'article 532 du code civil en vertu desquelles les matériaux provenant de la démolition d'un édifice sont meubles jusqu'à leur utilisation dans une construction imposeraient que les propriétaires fussent saisis pour consentir à leur classement, les requérants ont soumis à la cour une simple argumentation à laquelle la cour n'était pas tenue de répondre ; que, par suite, la cour n'a pas entaché son arrêt d'une insuffisance de motivation ;<br/>
<br/>
              Sur le bien fondé de l'arrêt attaqué :<br/>
<br/>
              7. Considérant, en premier lieu, que la cour a estimé que le bâtiment ayant fait l'objet de l'inscription au titre des monuments historiques, érigé au XVIIème siècle, qui avait la destination d'un pavillon de plaisance, était en bon état de conservation, avait conservé sa forme et ses caractéristiques architecturales originales, propres à la ville de Montbéliard, et demeurait représentatif d'une histoire locale particulière, en dépit des modifications intervenues à plusieurs reprises et de la dépose des panneaux de bois peints à laquelle les propriétaires avaient procédé ; que la cour a également relevé que les panneaux de bois peints étaient incorporés à la couverture du pavillon et avaient été déposés afin de préserver leur état ; que, la cour, qui a procédé à une appréciation de l'intérêt d'histoire ou d'art du bâtiment au sens des dispositions de l'article L. 621-25 du code du patrimoine et en a déduit que cet intérêt avait un caractère suffisant, n'a pas commis d'erreur de droit et n'a pas donné aux faits ainsi énoncés une qualification juridique erronée ;<br/>
<br/>
              8. Considérant, en deuxième lieu, qu'il ressort des énonciations de l'arrêt attaqué que les panneaux de bois peint comportant des décors épousaient parfaitement les formes spécifiques données à la couverture du pavillon constituée d'une voûte en arc de cloître, et avaient été spécialement conçus pour être incorporés au plafond ; qu'en en déduisant que ces panneaux avaient le caractère d'immeuble par nature, dès lors qu'ils ne pouvaient être séparés du plafond du pavillon sans porter atteinte à son intégrité, la cour a donné aux faits, qu'elle a souverainement appréciés, sans les dénaturer, une exacte qualification et n'a pas commis d'erreur de droit ; <br/>
<br/>
              9. Considérant, en troisième lieu, qu'en estimant que, contrairement à ce qui était soutenu, l'ensemble du terrain appartenant aux requérants se situe depuis 1989 à l'intérieur du périmètre d'une zone rouge du plan de prévention des risques inondation du Doubs et de l'Allan engendrant des limitations du droit à construire et qu'en tout état de cause, la restriction à la constructibilité de la parcelle appartenant aux requérants qui résulte de la décision litigieuse et est justifiée par la protection des monuments historiques, ne porte pas une atteinte illégale au droit de construire des propriétaires de cette parcelle, la cour n'a pas commis d'erreur de droit ; <br/>
<br/>
              10. Considérant qu'il résulte de tout ce qui précède que Mme B...et autres ne sont pas fondés à demander l'annulation de l'arrêt qu'ils attaquent ; <br/>
              Sur les conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              11. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : Le pourvoi de Mme B...et autres est rejeté.<br/>
<br/>
Article 2 : La présente décision sera notifiée à MmeB..., première requérante dénommée et à la ministre de la culture et de la communication. Les autres requérants seront informés de la présente décision par la SCP Gatineau-Fattaccini, avocat au Conseil d'Etat et à la Cour de cassation, qui les représente devant le Conseil d'Etat.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">41-01-01-01 MONUMENTS ET SITES. MONUMENTS HISTORIQUES. CLASSEMENT. CLASSEMENT DES IMMEUBLES. - CLASSEMENT DE PANNEAUX DE BOIS PEINTS INCORPORÉS À UN PLAFOND - CARACTÈRE D'IMMEUBLE PAR NATURE - EXISTENCE EN L'ESPÈCE [RJ1].
</SCT>
<ANA ID="9A"> 41-01-01-01 Des panneaux de bois peint, décorés, épousant parfaitement les formes spécifiques données à la couverture d'un pavillon et spécialement conçus pour être incorporés au plafond, ont le caractère d'immeuble par nature dès lors qu'ils ne peuvent être séparés du plafond du pavillon sans porter atteinte à son intégrité.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1]Rappr., s'agissant des bas-relief d'un salon, CE, 24 février 1999, Société Transurba, n° 191317, p. 33.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
