<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032928806</ID>
<ANCIEN_ID>JG_L_2016_07_000000384410</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/92/88/CETATEXT000032928806.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème chambre, 22/07/2016, 384410, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-07-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>384410</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Jean-Dominique Langlais</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2016:384410.20160722</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              M. A...B...a demandé au tribunal administratif de Cergy-Pontoise, d'une part, d'annuler la décision du 5 juillet 2013 par laquelle le ministre de l'intérieur a constaté la perte de validité de son permis de conduire pour solde de points nul, la décision implicite rejetant son recours gracieux ainsi que les décisions de retrait de points de son permis de conduire prises à la suite de quatre infractions commises les 22 janvier 2009, 21 avril 2010, 14 février 2012 et 18 novembre 2012, d'autre part, d'enjoindre au ministre de lui restituer son permis de conduire affecté d'un capital de douze points. Par un jugement n° 1307430 du 10 juillet 2014, le tribunal administratif a partiellement fait droit à sa demande en annulant la décision de retrait de points consécutive à l'infraction du 18 novembre 2012, en annulant par voie de conséquence la décision du 5 juillet 2013 ainsi que la décision rejetant le recours gracieux de M. B...et en enjoignant au ministre de réexaminer la situation de l'intéressé après lui avoir reconnu le bénéfice des quatre points illégalement retirés.<br/>
<br/>
              Par un pourvoi, enregistré le 11 septembre 2014 au secrétariat du contentieux du Conseil d'Etat, le ministre de l'intérieur demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler ce jugement ; <br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter la demande de M.B.... <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
<br/>
              - le code de procédure pénale ; <br/>
<br/>
              - le code de la route ; <br/>
<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Dominique Langlais, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que le ministre de l'intérieur a procédé au retrait de treize points du permis de conduire de M. B... à la suite de quatre infractions commises les 22 janvier 2009, 21 avril 2010, 14 février 2012 et 18 novembre 2012 ; qu'il a pris le 5 juillet 2013 une décision par laquelle il a constaté la perte de validité de ce titre pour solde de points nul et enjoint à M. B...de le restituer ; que, par un jugement du 10 juillet 2014, le tribunal administratif de Cergy-Pontoise, statuant sur la demande de M.B..., a annulé la décision de retrait de quatre points consécutive à l'infraction du 18 novembre 2012, annulé par voie de conséquence la décision du 5 juillet 2013 ainsi que la décision par laquelle le ministre de l'intérieur a rejeté le recours gracieux de M. B...et enjoint au ministre de l'intérieur de réexaminer la situation de ce dernier après lui avoir reconnu le bénéfice des quatre points illégalement retirés ; que le ministre de l'intérieur demande l'annulation de son jugement en tant qu'il fait partiellement droit aux conclusions de l'intéressé ; <br/>
<br/>
              2. Considérant qu'aux termes du quatrième alinéa de l'article L. 223-1 du code de la route : " La réalité d'une infraction entraînant retrait de points est établie par (...) l'émission du titre exécutoire de l'amende forfaitaire majorée (...) " ; qu'il résulte des dispositions de l'article 530 du code de procédure pénale qu'une réclamation contre le titre exécutoire d'une amende forfaitaire majorée, lorsqu'elle est formée dans les délais et dans les formes prévus par cet article et par l'article 529-10 du même code, entraîne l'annulation du titre exécutoire ; qu'il appartient à l'officier du ministère public d'apprécier la recevabilité de la réclamation, sous le contrôle de la juridiction pénale devant laquelle l'auteur de la réclamation dispose d'un recours ; que si le titulaire du permis de conduire peut utilement faire valoir devant le tribunal administratif, à l'appui d'une contestation relative au retrait de points, que la réalité de l'infraction n'est pas établie compte tenu de l'annulation du titre exécutoire du fait d'une réclamation, il ne saurait se borner à justifier de la présentation de cette réclamation mais doit établir qu'elle a été regardée comme recevable et a, par suite, entraîné l'annulation du titre ; que cette preuve peut être apportée soit par un document émanant de l'autorité judiciaire, soit, au besoin, par le document couramment intitulé " bordereau de situation des amendes et des condamnations pécuniaires ", tenu par le comptable public pour chaque contrevenant et dont la personne concernée peut obtenir communication en application de l'article L. 311-1 du code des relations entre le public et l'administration ; <br/>
<br/>
              3. Considérant qu'il résulte de ce qui précède qu'en jugeant qu'il appartenait au ministre de l'intérieur d'établir que la réclamation formée par M. B...sur le fondement de l'article 530 du code de procédure pénale contre le titre exécutoire d'amende forfaitaire majorée relatif à l'infraction du 18 novembre 2012 avait été rejetée par l'officier du ministère public, le tribunal administratif a commis une erreur de droit ; que cette erreur justifie, sans qu'il soit besoin d'examiner l'autre moyen du pourvoi, l'annulation des articles 1er, 2 et 4 de son jugement ;<br/>
<br/>
<br/>
<br/>
<br/>			D E C I D E :<br/>
              			--------------<br/>
<br/>
Article 1er : Les articles 1er, 2 et 4 du jugement du tribunal administratif de Cergy-Pontoise du 10 juillet 2014 sont annulés.<br/>
Article 2 : L'affaire est renvoyée au tribunal administratif de Cergy-Pontoise dans la limite de la cassation ainsi prononcée.<br/>
Article 3 : La présente décision sera notifiée au ministre de l'intérieur et à M. A...B....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
