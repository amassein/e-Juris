<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039456979</ID>
<ANCIEN_ID>JG_L_2019_12_000000415731</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/45/69/CETATEXT000039456979.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Section, 06/12/2019, 415731, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-12-06</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>415731</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Section</FORMATION>
<TYPE_REC>Recours en interprétation</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GARREAU, BAUER-VIOLAS, FESCHOTTE-DESBOIS ; SCP FABIANI, LUC-THALER, PINATEL</AVOCATS>
<RAPPORTEUR>M. Damien Pons</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Rémi Decout-Paolini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESEC:2019:415731.20191206</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              Par une ordonnance n° 1707394 du 15 novembre 2017, enregistrée le 16 novembre suivant au secrétariat du contentieux du Conseil d'Etat, le président du tribunal administratif de Versailles a transmis au Conseil d'Etat, en application de l'article R. 351-2 du code de justice administrative, le recours en interprétation, enregistré le 17 octobre 2017 au greffe de ce tribunal, présenté par la société cabinet dentaire C..., M. D... C..., Mme A... F... et M. E... B....<br/>
<br/>
              Par ce recours, la société cabinet dentaire C..., M. C..., Mme F... et M. B... demandent au Conseil d'Etat d'interpréter l'arrêté du 14 juin 2006 portant approbation de la convention nationale des chirurgiens-dentistes destinée à régir les rapports entre les chirurgiens-dentistes et les caisses d'assurance maladie et de se prononcer sur l'opposabilité du rapport d'évaluation technologique de la Haute Autorité de santé de septembre 2008 intitulé " Traitement endodontique " ainsi que sur la valeur probatoire des déclarations des praticiens conseils des services médicaux des organismes d'assurance maladie.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de la sécurité sociale ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Damien Pons, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Rémi Decout-Paolini, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Fabiani, Luc-Thaler, Pinatel, avocat de la société Cabinet Dentaire C..., de M. C..., de Mme F... et de M. B... et à la SCP Garreau, Bauer-Violas, Feschotte-Desbois, avocat de la Caisse nationale de l'assurance maladie ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il résulte de l'instruction que, par trois décisions rendues le 1er juin 2017, la section des assurances sociales de la chambre disciplinaire de première instance d'Ile-de-France de l'ordre des chirurgiens-dentistes a, sur le fondement de l'article L. 145-1 du code de la sécurité sociale, prononcé des sanctions d'interdiction de donner des soins aux assurés sociaux à l'encontre de M. C..., de Mme F... et de M. B.... Parallèlement aux appels qu'ils ont formés contre ces décisions, sur lesquels la section des assurances sociales du Conseil national de l'ordre des chirurgiens-dentistes a depuis lors statué par des décisions du 26 juin 2019 faisant l'objet, pour deux d'entre elles, d'un pourvoi en cassation, ces praticiens, ainsi que la société cabinet dentaire C... au sein de laquelle ils exercent, ont formé un recours en interprétation devant le tribunal administratif de Versailles, qui l'a transmis au Conseil d'Etat en application de l'article R. 351-2 du code de justice administrative. Par ce recours, ils demandent au Conseil d'Etat " de clarifier les limites de l'opposabilité et de la valeur probatoire des déclarations des praticiens conseils des services médicaux des organismes d'assurance maladie " devant les juridictions du contrôle technique et d'interpréter l'arrêté du 14 juin 2006 portant approbation de la convention nationale des chirurgiens-dentistes destinée à régir les rapports entre les chirurgiens-dentistes et les caisses d'assurance maladie, ainsi que les recommandations du rapport d'évaluation technologique de septembre 2008 de la Haute Autorité de santé intitulé " Traitement endodontique ", en ce sens qu'aucune obligation n'existerait pour les chirurgiens-dentistes, à laquelle un manquement de leur part pourrait être sanctionné, de produire des radiographies pré-, per- et post-interventionnelles pour la prise en charge de traitements endodontiques et prothétiques. <br/>
<br/>
              2. La recevabilité d'un recours direct en interprétation d'un acte administratif est subordonnée à l'existence d'un différend né et actuel susceptible de relever de la compétence du juge administratif, dont la résolution est subordonnée à l'interprétation demandée. Toutefois, l'auteur d'un tel recours ne peut invoquer à cette fin un différend porté devant une juridiction administrative, à laquelle il revient de procéder elle-même à l'interprétation des actes administratifs dont dépend la solution du litige qui lui est soumis. En outre, si le différend est porté devant une juridiction administrative après l'introduction du recours en interprétation, celui-ci perd son objet, de sorte qu'il n'y a plus lieu d'y statuer. <br/>
<br/>
              3. Il résulte de ce qui précède que le recours en interprétation de l'arrêté du 14 juin 2006 et du rapport de la Haute Autorité de santé de septembre 2008 formé par M. C..., Mme F... et M. B..., parallèlement aux appels qu'ils ont relevés contre les décisions de la section des assurances sociales de la chambre disciplinaire de première instance d'Ile-de-France de l'ordre des chirurgiens-dentistes, ainsi que par la société cabinet dentaire C..., qui ne se prévaut d'aucun autre différend que celui ayant donné lieu à l'action engagée devant la juridiction du contentieux du contrôle technique des chirurgiens-dentistes, n'est pas recevable.<br/>
<br/>
              4. Si les requérants demandent en outre que soit " clarifiée la valeur probatoire des déclarations des praticiens conseils des services médicaux des organismes d'assurance maladie " devant les juridictions du contrôle technique, dont ils estiment qu'elle devrait pouvoir être combattue par des expertises judiciaires auxquelles les professionnels mis en cause auraient recours, ces conclusions, qui ne tendent pas à l'interprétation d'un acte administratif, ne sont pas recevables.<br/>
<br/>
              5. Dans les circonstances de l'espèce, il n'y a pas lieu de faire droit aux conclusions présentées par la Caisse nationale de l'assurance maladie au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : La requête de la société cabinet dentaire C..., de M. C..., de Mme F... et de M. B... est rejetée.<br/>
Article 2 : Les conclusions de la Caisse nationale de l'assurance maladie présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La présente décision sera notifiée, pour les requérants, à la société cabinet dentaire C..., première dénommée, ainsi qu'à la ministre des solidarités et de la santé et à la Caisse nationale de l'assurance maladie.<br/>
Copie en sera adressée à l'Union nationale des caisses d'assurance maladie, à la Confédération nationale des syndicats dentaires, à l'Union des jeunes chirurgiens-dentistes - Union dentaire et à la Haute Autorité de santé. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
