<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039394291</ID>
<ANCIEN_ID>JG_L_2019_11_000000422350</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/39/42/CETATEXT000039394291.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème chambre, 18/11/2019, 422350, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-11-18</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>422350</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROUSSEAU, TAPIE</AVOCATS>
<RAPPORTEUR>M. Marc Lambron</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Cécile Barrois de Sarigny</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2019:422350.20191118</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A... C... et Mme B... C... ont demandé au tribunal administratif de Paris d'annuler la décision implicite par laquelle le ministre de l'intérieur a rejeté leurs demandes de mutation formées le 9 avril 2015, ainsi que l'annulation de vingt-neuf décisions des 9 avril 2015 et 4 juin 2015 par lesquelles ce même ministre a prononcé les mutations de vingt-neuf gardiens de la paix. Par deux jugements n° 1513553/5-1 et 1513554/5-1 du 16 mars 2017, le tribunal administratif a annulé dix-sept des mutations contestées et rejeté le surplus de leurs conclusions.<br/>
<br/>
              Par un arrêt n° 17 PA01564-17PA011585 du 15 mai 2018, la cour administrative d'appel de Paris a, sur appel de M. et Mme C..., annulé six autres mutations et rejeté le surplus de leurs conclusions.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés au secrétariat du contentieux du Conseil d'Etat les 18 juillet 2018, 18 octobre 2018 et 18 juin 2019, M. et Mme C... demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt en tant qu'il rejette le surplus de leurs conclusions ;<br/>
<br/>
              2°) réglant l'affaire au fond, d'enjoindre au ministre de l'intérieur de prononcer leur mutation à la circonscription de sécurité publique de Mont-de-Marsan, ou à la direction départementale de la police de l'air et des frontières d'Hendaye ou à la direction zonale de la police de l'air et des frontières de Bordeaux à compter de la décision à intervenir, sous astreinte de 100 euros par jour de retard ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
	Vu :<br/>
	- la loi n° 83-634 du 13 juillet 1983 ;<br/>
	- la loi n° 84-16 du 11 janvier 1984 ;<br/>
	- le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Marc Lambron, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Cécile Barrois de Sarigny, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Rousseau, Tapie, avocat de M. et Mme C....<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que M. et Mme C..., gardiens de la paix affectés à la Préfecture de police, ont déposé le 9 avril 2015 leur candidature à la mutation, par ordre de préférence, à la circonscription de sécurité publique de Mont-de-Marsan, à la direction départementale de la police de l'air et des frontières d'Hendaye et à la direction zonale de la police de l'air et des frontières de Bordeaux. Ils ont saisi le tribunal administratif de Paris de deux demandes tendant, d'une part à l'annulation des décisions implicites de rejet opposées par le ministre de l'intérieur à leurs candidatures et à leurs recours gracieux et, d'autre part, à l'annulation des décisions par lesquelles le ministre avait prononcé les mutations de vingt-neuf gardiens de la paix, lors de deux mouvements successifs soumis pour avis à la commission administrative paritaire compétente dans ses séances des 9 avril et 4 juin 2015. Par deux jugements du 16 mars 2017, le tribunal administratif de Paris a annulé les décisions de mutation de dix-sept gardiens de la paix et rejeté le surplus des demandes. Par un arrêt du 15 mai 2018, la cour administrative d'appel de Paris a, sur appel de M. et Mme C..., annulé les décisions de mutation de six autres gardiens de la paix, mais rejeté les conclusions par lesquelles ils demandaient l'annulation des refus opposés à leurs propres demandes de mutation. M. et Mme C... se pourvoient contre cet arrêt en tant qu'il ne fait pas intégralement droit à leur appel.<br/>
<br/>
              2. En premier lieu, il ressort des termes de l'arrêt attaqué que, pour rejeter les conclusions de M. et Mme C... dirigées contre les décisions refusant leurs mutations à l'occasion du mouvement soumis le 9 avril 2015 à la commission administrative paritaire, la cour a estimé que ces décisions n'étaient pas entachées d'erreur manifeste d'appréciation compte tenu de l'ancienneté respective de M. et Mme C... et des six agents, mutés lors de ce même mouvement, dont elle annulait la mutation pour défaut de publication préalable des postes. Toutefois, en l'absence, dans le dossier soumis aux juges du fond, de toute pièce susceptible d'établir l'ancienneté de ces six agents, les requérants sont fondés à soutenir que la cour a, sur ce point, entaché son arrêt de dénaturation.<br/>
<br/>
              3. En second lieu, il ressort également des termes de l'arrêt attaqué que, pour rejeter les conclusions des requérants dirigées contre les décisions refusant leurs mutations à l'occasion du mouvement soumis le 4 juin 2015 à la commission administrative paritaire, la cour s'est fondée sur ce que, les six agents mutés lors de ce mouvement étant mariés avec enfants, leur situation familiale était similaire à celle de M. et Mme C.... Or, il ressort des pièces du dossier soumis à la cour administrative d'appel que l'un au moins de ces six agents n'avait pas d'enfant. Par suite, M. et Mme C... sont fondés à soutenir que l'arrêt est également entaché de dénaturation sur ce point.<br/>
<br/>
              4. Il résulte de tout ce qu'il précède que M. et Mme C... sont fondés à demander l'annulation de l'arrêt qu'ils attaquent en tant qu'il rejette le surplus de leurs conclusions d'appel. Il y a lieu, par ailleurs, dans les circonstances de l'espèce, de mettre à la charge de l'Etat le versement à M. et Mme C... d'une somme de 3000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 15 mai 2018 de la cour administrative d'appel de Paris est annulé en tant qu'il rejette le surplus des conclusions d'appel de M. et Mme C....<br/>
Article 2 : L'affaire est renvoyée dans cette mesure à la cour administrative d'appel de Paris.<br/>
Article 3 : L'Etat versera à M. et Mme C... une somme de 3 000 euros au titre de l'article L. 761- du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à M. A... C..., à Mme B... C... et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
