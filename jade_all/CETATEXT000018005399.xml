<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000018005399</ID>
<ANCIEN_ID/>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/18/00/53/CETATEXT000018005399.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Assemblée, 08/02/2007, 279522, Publié au recueil Lebon</TITRE>
<DATE_DEC>2007-02-08</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>279522</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Assemblée</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Sauvé</PRESIDENT>
<AVOCATS>SCP NICOLAY, DE LANOUVELLE</AVOCATS>
<RAPPORTEUR>M. Michel  Delpech</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Derepas</COMMISSAIRE_GVT>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête sommaire et le mémoire complémentaire, enregistrés les 11 avril et 5 août 2005 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. Alain A, demeurant Résidence Les Fontaines, 18, rue de Gouvieux à Chantilly (60500) ; M. A demande au Conseil d'Etat :
              
              1°) d'annuler les articles 2 et 3 de l'arrêt du 19 janvier 2005 par lequel la cour administrative d'appel de Paris a rejeté sa requête tendant, d'une part, à  l'annulation du jugement du 9 avril 2002 du tribunal administratif de Paris rejetant sa demande tendant à ce que l'Etat soit condamné à l'indemniser du préjudice subi du fait de l'intervention de la loi du 25 juillet 1994 relative à la sécurité sociale, d'autre part, à la condamnation de l'Etat à lui payer la somme de 400 000 euros à titre de dommages et intérêts, majorée des intérêts de droit à compter du jugement à intervenir ;
              
              2°) statuant au fond, de faire droit à ses conclusions de première instance et d'appel et d'assortir en outre la condamnation de l'Etat du versement des intérêts légaux à compter de la demande préalable d'indemnité, ainsi que de la capitalisation des intérêts ;
              
              3°) de mettre à la charge de l'Etat le versement de la somme de 5 000 euros au titre de l'article L. 761&#143;1 du code de justice administrative ;
              
     
              Vu les autres pièces du dossier ;
              
              Vu la Constitution, notamment son préambule et son article 55 ;
              
              Vu la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;
              
              Vu le code de la sécurité sociale ;
              
              Vu la loi n° 94-637 du 25 juillet 1994 relative à la sécurité sociale, notamment son article 41 ;
              
              Vu le décret n° 85-283 du 27 février 1985 ;
              
              Vu le code de justice administrative ;
              
              
     
              Après avoir entendu en séance publique :
              
              - le rapport de M. Michel Delpech, Rapporteur,  
              
              - les observations de la SCP Nicolaÿ, de Lanouvelle, avocat de M. A,
              - les conclusions de M. Luc Derepas, Commissaire du gouvernement ;
              
              
     
     <br/>Considérant que la responsabilité de l'Etat du fait des lois est susceptible d'être engagée, d'une part, sur le fondement de l'égalité des citoyens devant les charges publiques, pour assurer la réparation de préjudices nés de l'adoption d'une loi à la condition que cette loi n'ait pas entendu exclure toute indemnisation et que le préjudice dont il est demandé réparation, revêtant un caractère grave et spécial, ne puisse, dès lors, être regardé comme une charge incombant normalement aux intéressés, d'autre part, en raison des obligations qui sont les siennes pour assurer le respect des conventions internationales par les autorités publiques, pour réparer l'ensemble des préjudices qui résultent de l'intervention d'une loi adoptée en méconnaissance des engagements internationaux de la France ;
              
              Considérant que, saisi d'un litige opposant M. A à la caisse de retraite des chirurgiens-dentistes et portant sur le paiement des cotisations prévues par le décret du 27 février 1985 relatif au régime d'assurance vieillesse complémentaire géré par cette caisse, dont l'intéressé contestait la légalité, le tribunal des affaires de sécurité sociale de Beauvais a sursis à statuer sur la question préjudicielle dont dépendait l'instance portée devant lui ; que, par décision du 18 février 1994, le Conseil d'Etat statuant au contentieux a jugé que ce décret était entaché d'illégalité ; que, toutefois, à la suite de l'intervention de la loi du 25 juillet 1994 relative à la sécurité sociale dont le IV de l'article 41 dispose que : « sont validés, sous réserve des décisions de justice devenues définitives, les appels de cotisations du régime d'assurance vieillesse complémentaire des chirurgiens&#143;dentistes effectués en application du décret n° 85-283 du 27 février 1985 (&#133;) », le tribunal des affaires de sécurité sociale a en définitive écarté les prétentions de M. A ; que, celui&#143;ci ayant recherché la responsabilité de l'Etat, la cour administrative d'appel de Paris a, par l'arrêt attaqué, confirmé le jugement du tribunal administratif de Paris refusant de condamner l'Etat à l'indemniser du préjudice qu'il imputait à l'intervention de cette loi ; que M. A demande au Conseil d'Etat, à titre principal, d'annuler cet arrêt en tant qu'il a jugé que la responsabilité de l'Etat n'était pas engagée à son égard en raison de la contrariété de la loi aux engagements internationaux de la France et, à titre subsidiaire, en tant que la cour a également rejeté ses conclusions fondées sur la rupture de l'égalité devant les charges publiques ;
              
              Considérant qu'aux termes du § 1 de l'article 6 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales : « Toute personne a droit à ce que sa cause soit entendue équitablement, publiquement et dans un délai raisonnable par un tribunal indépendant et impartial qui décidera soit des contestations sur ses droits et obligations de caractère civil, soit du bien&#143;fondé de toute accusation en matière pénale dirigée contre elle » ; 
              
              Considérant que, pour écarter le moyen tiré de ce que le IV de l'article 41 de la loi du 25 juillet 1994 était incompatible avec ces stipulations, la cour a jugé que la validation litigieuse, qui avait eu pour objet de préserver l'équilibre financier de la caisse autonome de retraite des chirurgiens&#143;dentistes, était intervenue dans un but d'intérêt général suffisant ; qu'en statuant ainsi, alors que l'Etat ne peut, sans méconnaître ces stipulations, porter atteinte au droit de toute personne à un procès équitable en prenant, au cours d'un procès, des mesures législatives à portée rétroactive dont la conséquence est la validation des décisions objet du procès, sauf lorsque l'intervention de ces mesures est justifiée par d'impérieux motifs d'intérêt général, la cour administrative d'appel a commis une erreur de droit ; que, dès lors, et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, M. A est fondé à demander l'annulation des articles 2 et 3 de l'arrêt attaqué ;
              
              Considérant qu'il y a lieu, en application de l'article L. 821&#143;2 du code de justice administrative, de régler l'affaire au fond ;
              
              Considérant, d'une part, que l'intérêt financier auquel ont entendu répondre les dispositions de l'article 41 de la loi du 25 juillet 1994 ne peut suffire à caractériser un motif impérieux d'intérêt général permettant de justifier la validation législative des appels de cotisations intervenus sur la base du décret du 27 février 1985 ; que ces dispositions sont, dès lors, incompatibles avec les stipulations citées plus haut du §1 de l'article 6 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales et que, par suite, leur intervention est susceptible d'engager la responsabilité de l'Etat ; que, d'autre part, la validation litigieuse est directement à l'origine du rejet, par le tribunal des affaires de sécurité sociale de Beauvais, des conclusions de M. A tendant à être déchargé des cotisations qui lui étaient réclamées sur le fondement d'un décret jugé illégal par le Conseil d'Etat ; qu'il suit de là que le requérant est fondé à demander la condamnation de l'Etat à en réparer les conséquences dommageables ; que M. A est, par suite, fondé à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Paris a rejeté sa demande ;
              
              Considérant qu'il résulte de ce qui a été dit ci-dessus que la réparation à laquelle M. A peut prétendre doit être déterminée en tenant compte du montant des cotisations dont le bien&#143;fondé était en cause dans l'instance l'opposant à sa caisse de retraite ; qu'en l'absence de tout autre élément utile produit par l'intéressé, il y a lieu de retenir les indications figurant dans le jugement avant dire droit du tribunal des affaires de sécurité sociale de Beauvais et d'évaluer le préjudice indemnisable à la somme de 2 800 euros ; que M. A a droit aux intérêts au taux légal de cette somme à compter du 24 décembre 1996, date de réception de sa demande préalable d'indemnité par le Premier ministre ;
              
              Considérant, enfin, que M. A a demandé la capitalisation des intérêts le 5 août 2005 ; qu'à cette date, il était dû au moins une année d'intérêts ; que, dès lors, il y a lieu de faire droit à cette demande, tant à cette date qu'à chaque échéance annuelle à compter de cette date ;
              
              Considérant qu'il y a lieu, dans les circonstances de l'espèce, de faire application des dispositions de l'article L. 761&#143;1 du code de justice administrative et de mettre à la charge de l'Etat le versement à M. A d'une somme de 5 000 euros au titre des frais exposés par lui, tant en première instance qu'en appel et en cassation ;
              
     
     <br/>				D E C I D E  :
              				--------------
              
Article 1er : Les articles 2 et 3 de l'arrêt de la cour administrative d'appel de Paris en date du 19 janvier 2005 sont annulés.
              
Article 2 : Le jugement du tribunal administratif de Paris en date du 9 avril 2002 est annulé.
              
Article 3 : L'Etat est condamné à verser à M. A la somme de 2 800 euros, assortie des intérêts au taux légal à compter du 24 décembre 1996. Les intérêts échus à la date du 5 août 2005, puis à chaque échéance annuelle à compter de cette date seront capitalisés pour produire eux&#143;mêmes intérêts.
              
Article 4 : L'Etat versera la somme de 5 000 euros à M. A au titre de l'article L. 761&#143;1 du code de justice administrative.
              
Article 5 : Le surplus des conclusions de la requête de M. A est rejeté.
              
Article 6 : La présente décision sera notifiée à M. Alain A et au ministre de la santé et des solidarités.
<br/>
</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
