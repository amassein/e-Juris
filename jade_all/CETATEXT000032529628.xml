<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032529628</ID>
<ANCIEN_ID>JG_L_2016_05_000000388962</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/52/96/CETATEXT000032529628.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère chambre, 13/05/2016, 388962, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-05-13</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>388962</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Marc Thoumelou</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Rémi Decout-Paolini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2016:388962.20160513</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
Procédure contentieuse antérieure<br/>
<br/>
              M. B...A...a demandé à la commission départementale d'aide sociale de l'Ain d'annuler la décision du 7 janvier 2013 par laquelle le président du conseil général de ce département lui a réclamé le remboursement d'une somme de 9 798,65 euros en raison de versements indus au titre de l'allocation compensatrice pour tierce personne du 1er janvier 2011 au 31 décembre 2012. Par une décision du 23 septembre 2013, la commission départementale d'aide sociale de l'Ain a rejeté sa demande.<br/>
<br/>
              Par une décision n° 130608 du 12 décembre 2014, la Commission centrale d'aide sociale, à la demande de M.A..., a ramené l'indu réclamé à 860 euros et réformé la décision de la commission départementale d'aide sociale de l'Ain du 23 septembre 2013 et la décision du président du conseil général de ce département du 7 janvier 2013 en ce qu'elles avaient de contraire à sa décision. <br/>
<br/>
Procédure devant le Conseil d'Etat<br/>
<br/>
              Par un pourvoi, enregistré le 24 mars 2015 au secrétariat du contentieux du Conseil d'Etat, le département de l'Ain demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler la décision de la Commission centrale d'aide sociale du 12 décembre 2014 ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter la demande de M. A...comme irrecevable.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de l'action sociale et des familles ; <br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Marc Thoumelou, maître des requêtes en service extraordinaire,  <br/>
<br/>
- les conclusions de M. Rémi Decout-Paolini, rapporteur public ;<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que, par une décision du 7 janvier 2013, le président du conseil général de l'Ain a réclamé à M. A...le remboursement d'un indu de 9 798,65 euros résultant du versement de l'allocation compensatrice pour tierce personne entre le 1er janvier 2011 et le 30 novembre 2012, alors qu'il percevait au cours de cette même période, de la Caisse nationale de retraites des agents des collectivités territoriales, une majoration pour tierce personne, en précisant lui accorder une remise gracieuse des sommes dues au titre de la période comprise entre le 1er avril et le 31 décembre 2010, pour tenir compte de sa bonne foi. M.A..., qui avait déclaré au département percevoir la majoration pour tierce personne dès 2010, et réitéré cette déclaration en 2011 et 2012, a alors saisi la commission départementale d'aide sociale de l'Ain d'une demande tendant à l'annulation de cette décision.<br/>
<br/>
              2. Saisie en appel par M.A..., la Commission centrale d'aide sociale a partiellement fait droit à sa demande en ramenant l'indu réclamé à 860 euros, au motif que l'administration, en lui versant à tort l'allocation compensatrice pour tierce personne, alors qu'elle avait été informée par ses soins de ce qu'il percevait une majoration pour tierce personne, avait commis une faute de nature à engager sa responsabilité et que le préjudice en découlant devait venir en déduction des sommes dont il était redevable à son égard.<br/>
<br/>
              3. Toutefois, la mise en jeu de la responsabilité des autorités administratives du fait des décisions qu'elles prennent en matière d'aide sociale, qui soulève un litige distinct d'une demande tendant à la réformation de ces décisions, relève des juridictions administratives de droit commun et non du juge de l'aide sociale. La circonstance que l'administration ait commis une faute de nature à engager sa responsabilité en procédant, pendant une longue période, au versement d'une allocation à laquelle, compte tenu de sa situation dont il l'avait dûment informée, le bénéficiaire ne pouvait pas prétendre, est par suite inopérante à l'appui de conclusions présentées devant les juridictions de l'aide sociale et tendant à l'annulation ou à la réformation d'une décision réclamant le remboursement d'un indu. Une telle circonstance, qui est de nature à révéler la bonne foi de l'intéressé, est seulement susceptible d'être invoquée à l'appui de conclusions tendant à l'annulation d'une décision du président du conseil général, devenu conseil départemental, rejetant expressément ou implicitement une demande du bénéficiaire de la prestation tendant à la remise ou à la modération, à titre gracieux, de l'obligation de reversement mise à sa charge.<br/>
<br/>
              4. Il résulte de ce qui précède que le département de l'Ain est fondé à soutenir que la Commission centrale d'aide sociale a commis une erreur de droit en réduisant le montant de la somme due par M. A...à hauteur du préjudice résultant pour lui de la faute de l'administration. Ce moyen suffisant à entraîner l'annulation de la décision de la Commission centrale, il n'est pas nécessaire d'examiner l'autre moyen du pourvoi. <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : La décision de la Commission centrale d'aide sociale du 12 décembre 2014 est annulée.<br/>
Article 2 : L'affaire est renvoyée à la Commission centrale d'aide sociale.<br/>
Article 3 : La présente décision sera notifiée au département de l'Ain et à M. B...A....<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
