<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000041782244</ID>
<ANCIEN_ID>JG_L_2020_03_000000420192</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/41/78/22/CETATEXT000041782244.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème chambre, 16/03/2020, 420192, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-03-16</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>420192</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MATUCHANSKY, POUPOT, VALDELIEVRE</AVOCATS>
<RAPPORTEUR>Mme Ophélie Champeaux</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emilie Bokdam-Tognetti</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2020:420192.20200316</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B... A... a demandé au tribunal administratif de Grenoble de prononcer la décharge des cotisations supplémentaires d'impôt sur le revenu et de contributions sociales auxquelles il a été assujetti au titre des années 2008, 2009 et 2010 ainsi que des pénalités correspondantes. Par un jugement nos 1401740, 1401738 du 12 novembre 2015, le tribunal a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 16LY00074 du 27 février 2018, la cour administrative d'appel de Lyon a prononcé un non-lieu partiel à statuer, accordé à M. A... une réduction des impositions supplémentaires et des pénalités mises à sa charge et rejeté le surplus des conclusions de sa requête d'appel. <br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et deux nouveaux mémoires, enregistrés les 27 avril et 27 juillet 2018 et les 15 avril et 31 décembre 2019 au secrétariat du contentieux du Conseil d'Etat, M. A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'article 4 de cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Ophélie Champeaux, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Emilie Bokdam-Tognetti, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Matuchansky, Poupot, Valdelièvre, avocat de M. A... ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que la SARL Pharmacie des Musées, dont M. A... était co-gérant et associé à hauteur de 50 %, a fait l'objet d'une vérification de comptabilité au titre de la période du 1er juillet 2007 au 30 juin 2010, à l'issue de laquelle l'administration fiscale a regardé sa comptabilité comme irrégulière et non probante pour l'ensemble de la période vérifiée et procédé à la reconstitution de son chiffre d'affaires ainsi qu'à la détermination du bénéfice imposable et de la taxe sur la valeur ajoutée (TVA). Les recettes omises ont été regardées comme des revenus distribués imposables, en application des dispositions du 1° du 1 de l'article 109 du code général des impôts, entre les mains de M. A... à hauteur de 50%. Par un jugement du 12 novembre 2015, le tribunal administratif de Grenoble a rejeté la demande de M. A... tendant à la décharge des cotisations supplémentaires d'impôt sur le revenu et de contributions sociales auxquelles il a été assujetti en conséquence de ces rectifications ainsi que des pénalités correspondantes. Par l'arrêt attaqué du 27 février 2018, la cour administrative d'appel de Lyon a prononcé la réduction des impositions et pénalités contestées et rejeté le surplus des conclusions d'appel du contribuable.<br/>
<br/>
              Sur les motifs de l'arrêt relatifs à la régularité de la procédure d'imposition :<br/>
<br/>
              2. En premier lieu, en jugeant, au point 6 de son arrêt, qu'à supposer qu'en soutenant que la "charte garantissant les droits du contribuable" n'impose pas à un contribuable d'avoir des connaissances en informatique, le requérant ait entendu se prévaloir de la méconnaissance, par l'administration, de la charte des droits et devoirs du contribuable vérifié, ce moyen n'était pas assorti de précisions suffisantes pour en apprécier le bien-fondé, la cour a suffisamment motivé son arrêt.<br/>
<br/>
              3. En second lieu, aux termes de l'article L. 57 du livre des procédures fiscales : " L'administration adresse au contribuable une proposition de rectification qui doit être motivée de manière à lui permettre de formuler ses observations ou de faire connaître son acceptation. (...) En cas d'application des dispositions de l'article L. 47 A, l'administration précise au contribuable la nature des traitements effectués ". Il résulte de ces dispositions que, pour être régulière, une proposition de rectification doit comporter la désignation de l'impôt concerné, de l'année d'imposition et de la base d'imposition, et énoncer les motifs sur lesquels l'administration entend se fonder pour justifier les redressements envisagés, de façon à permettre au contribuable de formuler ses observations de façon entièrement utile. S'agissant de revenus distribués, cette motivation peut résulter, soit de la reproduction de la teneur de la proposition de rectification adressée à la société distributrice, soit de la jonction de cette proposition de rectification en annexe du document adressé au bénéficiaire des distributions, dès lors du moins que le document concernant la société est lui-même suffisamment motivé. <br/>
<br/>
              4. D'une part, la cour a relevé que la proposition de rectification adressée à M. A... indiquait qu'elle portait, en matière d'impôt sur le revenu, au titre des années 2008, 2009 et 2010, sur des revenus de capitaux mobiliers résultant de revenus regardés comme lui ayant été distribués par la SARL Pharmacie des Musées et qu'elle précisait qu'à la suite de la vérification de comptabilité de cette pharmacie, des anomalies quant à l'utilisation du logiciel de gestion Alliance Premium avaient été constatées, que des omissions de recettes avaient été décelées et que le montant des recettes omises avait été évalué en faisant la moyenne de deux méthodes, succinctement décrites. La cour a également relevé que cette proposition renvoyait à celle adressée à la société Pharmacie des Musées qui avait été jointe et qu'elle mentionnait le montant des rehaussements relatifs à ces omissions de recettes ainsi que leurs conséquences financières pour le requérant. La cour en a déduit que si il n'avait pas été joint le CD-Rom annexé à la proposition de rectification adressée à la SARL et contenant les résultats des traitements effectués par l'administration, cette circonstance n'avait pas fait obstacle à ce que M. A... engage utilement, comme il l'avait fait, une discussion sur les rectifications notifiées. En statuant ainsi, la cour n'a pas commis d'erreur de droit. <br/>
<br/>
              5. D'autre part, si l'administration est tenue, lorsqu'elle adresse une proposition de rectification à une société qui a choisi, en application du c du II de l'article L. 47 A du livre des procédures fiscales, de mettre à sa disposition les copies des documents, données et traitements soumis à contrôle, de préciser dans cette proposition, les fichiers utilisés, la nature des traitements qu'elle a effectués sur ces fichiers et les modalités de détermination des éléments servant au calcul des rehaussements, elle n'a, en revanche, l'obligation de communiquer ni les algorithmes, logiciels ou matériels qu'elle a utilisés ou envisage de mettre en oeuvre pour effectuer ces traitements, ni les résultats de l'ensemble des traitements qu'elle a réalisés, que ce soit préalablement à la proposition de rectification ou dans le cadre de celle-ci. Par suite, c'est sans erreur de droit que la cour, après avoir relevé que la proposition de rectification adressée à la SARL Pharmacie des Musées, à laquelle étaient joints les fichiers exposant le résultat des traitements informatiques, expliquait la nature des traitements opérés par le vérificateur sur la comptabilité informatique de la société, détaillait les raisons pour lesquelles la comptabilité de cette dernière avait été écartée et exposait, de façon détaillée, les deux méthodes de reconstitution des recettes dissimulées de la SARL, a estimé, par une appréciation souveraine, que cette proposition de rectification était suffisamment motivée et en a déduit, sans commettre d'erreur de droit, que la proposition de rectification adressée à M. A..., à laquelle elle était jointe, était, par suite, elle-même suffisamment motivée.<br/>
<br/>
              Sur les motifs de l'arrêt relatifs au bien-fondé des impositions :<br/>
<br/>
              6. En premier lieu, la cour a jugé, par une appréciation souveraine non arguée de dénaturation, que l'administration apportait la preuve des graves irrégularités dont la comptabilité de la société Pharmacie des Musées était entachée dès lors qu'elle démontrait l'existence de ruptures de séquentialité dans la numérotation des ventes et des règlements. Elle n'a pas commis d'erreur de droit en jugeant que la seule circonstance que le mode de présentation de ces résultats, sans champ relatif à la date et dans un ordre non chronologique, rendait plus difficile leur analyse, n'était pas de nature à remettre en cause leur exactitude, dès lors que de telles ruptures de séquentialité avaient été mises en évidence au vu de la numérotation des ventes et des règlements et non de leur date.<br/>
<br/>
              7. En deuxième lieu, aux termes de l'article 109 du code général des impôts : " 1. Sont considérés comme revenus distribués : 1° Tous les bénéfices ou produits qui ne sont pas mis en réserve ou incorporés au capital ; ". Aux termes de l'article 110 du même code : " Pour l'application de l'article 109-1-1°, les bénéfices s'entendent de ceux qui ont été retenus pour l'assiette de l'impôt sur les sociétés ". Lorsque, pour procéder au rehaussement des bénéfices imposables d'une société, l'administration procède à la réintégration de la somme correspondant au montant hors taxes de recettes omises puis ajoute à ces bénéfices la somme correspondant au montant de la TVA due sur ces recettes calculées hors taxes, cette décomposition ne saurait avoir pour effet de remettre en cause le caractère de revenu distribué de la totalité des recettes. Par suite, l'administration est fondée à regarder comme distribuées les sommes correspondant, d'une part, au montant hors taxes des recettes omises et, d'autre part, au montant de la TVA due sur les recettes calculées hors taxes. <br/>
<br/>
              8. Après avoir écarté les deux méthodes de reconstitution du chiffre d'affaires de la société Pharmacie des Musées utilisées par le vérificateur, l'une comme radicalement viciée et l'autre comme excessivement sommaire, la cour a jugé qu'en l'absence de méthodes alternatives proposées par le requérant, il y avait lieu de fixer les nouvelles bases d'imposition au regard du nombre de règlements manquants de chaque exercice et du prix moyen du panier payé en espèces sur l'ensemble des tickets comptabilisés sur l'exercice. Elle en a déduit qu'il en résultait une insuffisance de déclaration du chiffre d'affaires hors taxe de la SARL Pharmacie des Musées de 25 431 euros au titre de l'exercice 2008 au lieu de 49 153 euros retenue par l'administration, de 22 500 euros au titre de l'exercice 2009 au lieu de 41 777 euros et de 25 028 euros au titre de l'exercice 2010 au lieu de 43 578 euros, et que le requérant était, dès lors, fondé à obtenir une réduction des cotisations supplémentaires d'impôt sur le revenu et de contributions sociales auxquelles il avait été assujetti, à raison d'une diminution de sa base d'imposition de 11 861 euros en 2008, de 9 638 euros en 2009 et de 9 275 euros en 2010.<br/>
<br/>
              9. Toutefois, en statuant ainsi alors qu'il résulte de ce qui a été dit au point 7 ci-dessus et qu'il ressort des pièces du dossier soumis à la cour, notamment de la proposition de rectification adressée au requérant, que les bénéfices regardés comme des revenus distribués correspondaient à la totalité des recettes omises, y compris le montant de la TVA due sur ces recettes, la cour a entaché son arrêt d'une erreur de droit.<br/>
<br/>
              10. Il résulte de ce qui précède que M. A... est seulement fondé à demander l'annulation de l'arrêt qu'il attaque en tant qu'il n'a pas réduit sa base imposable des montants correspondant à la TVA due sur les recettes omises.<br/>
<br/>
              11. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 1 000 euros à verser à M. A... au titre des dispositions de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'article 4 de l'arrêt de la cour administrative d'appel de Lyon du 27 février 2018 est annulé en tant qu'il n'a pas réduit la base imposable à l'impôt sur le revenu et aux contributions sociales de M. A..., au titre des années 2008, 2009 et 2010, des montants correspondant à la taxe sur la valeur ajoutée due sur les recettes omises.<br/>
<br/>
Article 2 : L'affaire est renvoyée, dans cette mesure, à la cour administrative d'appel de Lyon. <br/>
<br/>
Article 3 : L'Etat versera à M. A... la somme de 1 000 euros, au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : Le surplus des conclusions du pourvoi est rejeté.<br/>
<br/>
Article 5 : La présente décision sera notifiée à M. B... A... et au ministre de l'action et des comptes publics.<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
