<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033551447</ID>
<ANCIEN_ID>JG_L_2016_12_000000388141</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/55/14/CETATEXT000033551447.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère - 6ème chambres réunies, 07/12/2016, 388141</TITRE>
<DATE_DEC>2016-12-07</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>388141</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère - 6ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP CELICE, SOLTNER, TEXIDOR, PERIER</AVOCATS>
<RAPPORTEUR>M. Frédéric Puigserver</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Rémi Decout-Paolini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2016:388141.20161207</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B...C...et Me D...A..., agissant en qualité de mandataire judiciaire de la société Siana, ont demandé au tribunal administratif de Marseille d'annuler la décision du 4 avril 2008 par laquelle le préfet de la région Provence-Alpes-Côte-d'Azur a prononcé le rejet des dépenses de formation professionnelle de la société Siana pour un montant de 99 095,01 euros et l'a condamnée à verser la somme de 399 065,63 euros aux organismes de financement des formations, au titre des formations inexécutées, et la même somme au Trésor public, pour manoeuvres frauduleuses.<br/>
<br/>
              Par un jugement n° 0803949 du 28 juin 2011, le tribunal d'administratif de Marseille a annulé la décision du 4 avril 2008, d'une part, en tant qu'elle imposait à la société Siana le reversement aux cocontractants des sommes payées au titre des formations accomplies en 2006 auprès de diverses sociétés et, d'autre part, en tant qu'elle ordonnait le versement au Trésor public, pour manoeuvres frauduleuses, d'une somme correspondant aux prestations non réalisées, sauf en ce qui concerne cinq sociétés.<br/>
<br/>
              Par un arrêt n° 11MA03562du 19 décembre 2014, la cour administrative d'appel de Marseille a rejeté l'appel formé par M. C...et Me A...contre ce jugement du 28 juin 2011 en tant qu'il rejetait partiellement leurs conclusions.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 19 février 2015, 19 mai 2015 et 17 octobre 2016 au secrétariat du contentieux du Conseil d'Etat, M. C...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt de la cour administrative d'appel de Marseille du 19 décembre 2014 ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 7 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la convention internationale du travail n° 81 concernant l'inspection du travail dans l'industrie et le commerce, adoptée à Genève le 11 juillet 1947 ;<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - le code du travail ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Frédéric Puigserver, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Rémi Decout-Paolini, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Célice, Soltner, Texidor, Perier, avocat de M.C....<br/>
<br/>
              Vu la note en délibéré, enregistrée le 16 novembre 2016, présentée par M. C... ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que la société Siana, dont M. C...était le président et qui exerçait une activité de prestations de formation professionnelle continue, a fait l'objet, au cours de l'année 2007, d'un contrôle de l'administration du travail, au titre des années 2005 et 2006, réalisé dans les conditions prévues par les articles L. 991-1 et suivants, alors applicables, du code du travail ; que ce contrôle, effectué sur place et auprès de personnes bénéficiaires de la formation continue, a donné lieu à l'établissement d'un rapport en date du 28 août 2007, comportant plusieurs griefs ; qu'à la suite d'une réunion contradictoire organisée le 16 octobre 2007 et de divers échanges l'ayant suivie, le préfet de la région Provence-Alpes-Côte-d'Azur, estimant que la société Siana ne justifiait pas de la conformité de l'utilisation des fonds alloués à des actions de formation professionnelle ni de la réalité des dépenses engagées, a, par une décision du 29 novembre 2007, rejeté ses dépenses pour un montant de 99 095,01 euros, dont le versement au Trésor public a été mis solidairement à sa charge et à celle de ses dirigeants, et l'a condamnée à verser la somme de 399 065,63 euros aux organismes de financement des formations, au titre des formations considérées comme inexécutées, et la même somme au Trésor public, pour manoeuvres frauduleuses ; que, par une décision du 4 avril 2008, le préfet a rejeté le recours administratif préalable obligatoire exercé par la société en application de l'article R. 991-8 du code du travail et a confirmé sa décision initiale ; que le tribunal administratif de Marseille, saisi par M. C...et par le mandataire-liquidateur de la société Siana, a partiellement annulé cette décision en tant qu'elle le condamnait à verser des sommes aux organismes financeurs pour formations inexécutées et au Trésor public pour manoeuvres frauduleuses ; que M. C...se pourvoit en cassation contre l'arrêt par lequel la cour administrative d'appel de Marseille a rejeté son appel dirigé contre le jugement en ce qu'il rejetait partiellement sa demande ; <br/>
<br/>
              Sur l'arrêt attaqué, en tant qu'il statue sur la procédure de contrôle :<br/>
<br/>
              2. Considérant qu'aux termes de l'article L. 991-8 du code du travail, applicable au litige : " Les contrôles prévus au présent chapitre peuvent être opérés soit sur place, soit sur pièces. / Les résultats du contrôle sont notifiés à l'intéressé dans un délai ne pouvant dépasser trois mois à compter de la fin de la période d'instruction avec l'indication des procédures dont il dispose pour faire valoir ses observations. (...) / Les décisions de rejet de dépenses et de versement mentionnés au présent chapitre prises par l'autorité de l'Etat chargée de la formation professionnelle ne peuvent intervenir, après la notification des résultats du contrôle, que si la procédure contradictoire mentionnée au deuxième alinéa a été respectée. Ces décisions sont motivées et notifiées aux intéressés. (...) " ;<br/>
<br/>
              3. Considérant que le caractère contradictoire des contrôles menés conformément aux dispositions précitées de l'article L. 991-8 du code du travail, aujourd'hui reprises aux articles L. 6362-8 à L. 6362-10 de ce code, impose à l'autorité administrative de mettre l'intéressé à même de prendre connaissance du dossier le concernant ; que si l'administration entend se fonder sur des renseignements obtenus auprès de tiers, il lui incombe alors d'informer l'intéressé de l'origine et de la teneur de ces renseignements, avec une précision suffisante pour lui permettre, notamment, de discuter utilement leur provenance ou de demander, le cas échéant, la communication des documents qui les contiennent ; que toutefois, lorsque l'accès à ces renseignements serait de nature à porter gravement préjudice aux personnes qui en sont à l'origine, l'administration doit se limiter à informer l'intéressé, de façon suffisamment circonstanciée, de leur teneur ; qu'il revient au juge d'apprécier, au vu des échanges entre les parties et en ordonnant, le cas échéant, toute mesure d'instruction complémentaire, si le caractère contradictoire de la procédure a été respecté ;<br/>
<br/>
              4. Considérant, en premier lieu, que, pour juger que la procédure de contrôle en cause était régulière, la cour administrative d'appel de Marseille a relevé que l'administration avait communiqué aux intéressés, lors de l'entretien du 16 octobre 2007, la teneur des informations sur lesquelles elle se fondait et qu'ils n'avaient pas demandé à être confrontés aux témoins et ne les avaient pas eux-mêmes interrogés ; qu'en statuant ainsi, la cour n'a pas commis d'erreur de droit dans les règles gouvernant la charge de la preuve et n'a pas dénaturé les pièces du dossier soumis aux juges du fond, dont il ressort que le rapport de contrôle adressé à la société par courrier du 28 août 2007 faisait état de la teneur des informations détenues par l'administration ;<br/>
<br/>
              5. Considérant, en deuxième lieu, qu'il ressort des pièces du dossier soumis à la cour que n'a pas été soulevé devant elle le moyen tiré du défaut d'information préalable quant à l'origine des renseignements sur lesquels l'administration entendait se fonder ; que, par suite, le moyen soulevé en cassation tiré de ce que la cour aurait commis une erreur de droit en ne recherchant pas si cette information avait été donnée par l'administration à la requérante, qui n'est pas né de l'arrêt attaqué et n'est pas d'ordre public, ne peut être utilement soulevé à l'encontre de l'arrêt attaqué ;<br/>
<br/>
              6. Considérant, en troisième lieu, qu'aux termes du paragraphe 3 de l'article 6 de la convention européenne de sauvegarde des doits de l'homme et des libertés fondamentales : " Tout accusé a droit notamment à : / a. être informé, dans le plus court délai, dans une langue qu'il comprend et d'une manière détaillée, de la nature et de la cause de l'accusation portée contre lui ; (...) d. interroger ou faire interroger les témoins à charge et obtenir la convocation et l'interrogation des témoins à décharge dans les mêmes conditions que les témoins à charge ; (...) " ; que ces stipulations ne sont applicables, en principe, qu'aux procédures contentieuses suivies devant les juridictions lorsqu'elles statuent sur des accusations en matière pénale et ne peuvent être invoquées pour critiquer la régularité d'une procédure administrative, alors même qu'elle conduirait au prononcé d'une sanction ; qu'il ne peut en aller autrement que dans l'hypothèse où la procédure d'établissement de cette sanction pourrait, eu égard à ses particularités, emporter des conséquences de nature à porter atteinte de manière irréversible au caractère équitable d'une procédure ultérieurement engagée devant le juge ;<br/>
<br/>
              7. Considérant que l'origine des renseignements détenus par l'administration et la valeur des témoignages recueillis par elle pouvaient de nouveau être discutées devant les juges du fond, qui opèrent un entier contrôle sur les sanctions prononcées sur le fondement du II de l'article L. 991-5 et du second alinéa de L. 991-6 du code du travail, garantissant ainsi le caractère équitable de la procédure, et qu'ainsi le seul fait de ne pas avoir eu connaissance de ces témoignages dès le contrôle administratif ne compromettait pas les chances des requérants d'obtenir gain de cause devant le juge ; que, par suite, le moyen tiré de la méconnaissance des stipulations précitées doit être écarté ;<br/>
<br/>
              8. Considérant, en dernier lieu, que la convention internationale du travail n° 81 du 11 juillet 1947 régit le système d'inspection du travail dont doivent disposer les parties à la convention en vue d'assurer, dans leurs établissements industriels et commerciaux, aux termes, respectivement, de ses articles 2 et 23, " l'application des dispositions légales relatives aux conditions de travail et à la protection des travailleurs dans l'exercice de leur profession " ; que, dès lors, le moyen tiré de la violation de l'article 12 de cette convention, qui ne concerne que les conditions de travail et la protection des travailleurs, était inopérant à l'encontre des décisions attaquées ; qu'un tel motif, qui est d'ordre public et n'appelle l'appréciation d'aucune circonstance de fait, doit être substitué à celui retenu par la cour dans son arrêt, dont il justifie légalement le dispositif sur ce point ;<br/>
<br/>
              Sur l'arrêt attaqué, en tant qu'il statue sur le rejet des dépenses :<br/>
<br/>
              9. Considérant qu'aux termes de l'article L. 991-5 du code du travail, applicable au litige, dont les dispositions sont désormais reprises aux articles L. 6362-5 à L. 6362-7 de ce code : " I.- Les organismes mentionnés aux 2° et 3° de l'article L. 991-1 sont tenus, à l'égard des agents mentionnés à l'article L. 991-3 : / 1° De présenter les documents et pièces établissant l'origine des produits et des fonds reçus ainsi que la nature et la réalité des dépenses exposées pour l'exercice des activités conduites en matière de formation professionnelle continue ; / 2° De justifier le rattachement et le bien-fondé de ces dépenses à leurs activités ainsi que la conformité de l'utilisation des fonds aux dispositions législatives et réglementaires régissant ces activités. / A défaut de remplir les conditions prévues aux 1° et 2° ci-dessus, les organismes font, pour les dépenses considérées, l'objet de la décision de rejet prévue à l'article L. 991-8. / Les organismes prestataires d'actions entrant dans le champ de la formation professionnelle continue au sens de l'article L. 900-2 sont tenus, de même, de présenter tous documents et pièces établissant la réalité desdites actions. A défaut, celles-ci sont réputées inexécutées au sens de l'article L. 991-6. / II.- Les organismes prestataires d'actions entrant dans le champ de la formation professionnelle défini à l'article L. 900-2 doivent, solidairement avec leurs dirigeants de fait ou de droit, verser au Trésor public une somme égale au montant des dépenses ayant fait l'objet d'une décision de rejet en application du I. (...) " ; qu'il résulte de ces dispositions que l'obligation de versement au Trésor public à laquelle un organisme de formation professionnelle continue est tenu porte sur les dépenses qu'il a effectuées et pour lesquelles soit il ne produit pas de pièces établissant leur nature et leur réalité, soit il ne justifie pas leur rattachement à ses activités et leur bien-fondé ;<br/>
<br/>
              10. Considérant, en premier lieu, que la circonstance que, par son jugement du 28 juin 2011, non contesté sur ce point, le tribunal administratif de Marseille avait annulé la décision du 4 avril 2008 en tant qu'elle condamne la société Siana, sur le fondement du premier alinéa de l'article L. 991-6 du code du travail, à reverser aux organismes financeurs les sommes correspondant aux formations réalisées pour certaines sociétés, au motif que la réalité des prestations de formation pouvait être regardée comme établie, était sans incidence sur l'appréciation que la cour devait porter, pour l'application de l'article L. 991-5 du même code, sur le caractère non justifié de certaines dépenses ; que, par suite, le requérant n'est pas fondé à soutenir que la cour aurait commis une erreur de droit en s'abstenant de tenir compte de ce que le tribunal avait reconnu la réalité de certaines des formations litigieuses ;   <br/>
<br/>
              11. Considérant, en second lieu, que, pour juger que la société Siana était redevable, solidairement avec ses dirigeants, de la somme de 99 095,01 euros égale au montant des dépenses ayant fait l'objet d'une décision de rejet, la cour a relevé, d'une part, que la société, qui n'avait produit aucun emploi du temps permettant d'établir la concordance entre les actions de formation effectuées et les dépenses engagées, et qui s'était bornée à verser au dossier des notes établies sur papier libre, n'avait produit, ni en première instance, ni en appel, les pièces prévues par l'article L. 991-5 précité du code du travail ; que la cour a relevé, d'autre part, comme le tribunal l'avait lui-même jugé, que la société ne justifiait pas du rattachement de certaines dépenses à ses activités et de leur bien-fondé ; qu'en statuant ainsi, par un arrêt suffisamment motivé, la cour n'a pas méconnu les règles gouvernant la charge de la preuve ;<br/>
<br/>
              Sur l'arrêt attaqué, en tant qu'il statue sur les pénalités :<br/>
<br/>
              12. Considérant qu'aux termes des articles L. 920-9 puis L. 991-6 du code du travail, qui étaient en vigueur à la date des faits litigieux et leur sont demeurés applicables compte tenu de la portée de l'article L. 6362-7-2 de ce code : " Faute de réalisation totale ou partielle d'une prestation de formation, l'organisme prestataire doit rembourser au cocontractant les sommes indûment perçues de ce fait. / En cas de manoeuvres frauduleuses, le ou les contractants sont, de plus, assujettis à un versement d'égal montant au profit du Trésor public " ;<br/>
<br/>
              13. Considérant que, pour juger que des pénalités pour manoeuvres frauduleuses avaient pu légalement être infligées à la société Siana au titre de certaines actions de formations, sur le fondement des dispositions citées au point 12, la cour administrative d'appel de Marseille a relevé, d'une part, les témoignages des personnes présentées comme stagiaires de ces formations selon lesquels elles ne les avaient pas suivies, d'autre part, les nombreuses incohérences tenant aux lieux ou au volume de ces formations, ainsi qu'au contenu des feuilles d'émargement, et, enfin, l'absence d'élément apporté par la société attestant de sa bonne foi ; qu'en statuant ainsi, la cour a caractérisé non seulement l'élément matériel, mais également l'élément intentionnel définissant une manoeuvre frauduleuse ; qu'en outre, en formant sa conviction au vu du débat contradictoire entre les parties, elle n'a pas mis à la charge de la société la preuve de sa bonne foi ; que, dès lors, la cour administrative d'appel, qui n'a pas commis d'erreur de droit, a donné une exacte qualification juridique aux faits de l'espèce, tels qu'elle les a souverainement appréciés sans les dénaturer ;<br/>
<br/>
              14. Considérant que les dispositions des articles L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat, qui n'est pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de M. C...est rejeté.<br/>
Article 2 : La présente décision sera notifiée à M. B...C...et à la ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">66-09 TRAVAIL ET EMPLOI. FORMATION PROFESSIONNELLE. - CONTRÔLE DES DÉPENSES ET ACTIVITÉS DE FORMATION PROFESSIONNELLE CONTINUE - CARACTÈRE CONTRADICTOIRE - 1) MODALITÉS - MISE À MÊME DE PRENDRE CONNAISSANCE DU DOSSIER - 2) CAS DES RENSEIGNEMENTS ET TÉMOIGNAGES DES TIERS [RJ1].
</SCT>
<ANA ID="9A"> 66-09 1) Le caractère contradictoire des contrôles des dépenses et activités de formation professionnelle continue menés conformément aux dispositions précitées de l'article L. 991-8 du code du travail, aujourd'hui reprises aux articles L. 6362-8 à L. 6362-10 de ce code, impose à l'autorité administrative de mettre l'intéressé à même de prendre connaissance du dossier le concernant.... ,,2) Si l'administration entend se fonder sur des renseignements obtenus auprès de tiers, il lui incombe alors d'informer l'intéressé de l'origine et de la teneur de ces renseignements, avec une précision suffisante pour lui permettre, notamment, de discuter utilement leur provenance ou de demander, le cas échéant, la communication des documents qui les contiennent. Toutefois, lorsque l'accès à ces renseignements serait de nature à porter gravement préjudice aux personnes qui en sont à l'origine, l'administration doit se limiter à informer l'intéressé, de façon suffisamment circonstanciée, de leur teneur. Il revient au juge d'apprécier, au vu des échanges entre les parties et en ordonnant, le cas échéant, toute mesure d'instruction complémentaire, si le caractère contradictoire de la procédure a été respecté.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., s'agissant de l'enquête contradictoire préalable à une autorisation administrative de licenciement, CE, Section, 24 novembre 2006, Mme Rodriguez, n° 284208, p. 481 ; CE, 9 juillet 2007, Sangare, n° 288295, T. p. 651. Rappr., pour l'application de l'article 65 de la loi du 22 avril 1905, CE, 23 novembre 2016, M. Riquelme, n° 397733, à mentionner aux Tables.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
