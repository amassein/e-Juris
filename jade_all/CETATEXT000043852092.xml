<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043852092</ID>
<ANCIEN_ID>JG_L_2021_07_000000441646</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/85/20/CETATEXT000043852092.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème chambre, 22/07/2021, 441646, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-07-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>441646</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BARADUC, DUHAMEL, RAMEIX ; SCP DE NERVO, POUPET</AVOCATS>
<RAPPORTEUR>M. Charles-Emmanuel Airy</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Karin Ciavaldini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:441646.20210722</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une décision du 23 novembre 2020, le Conseil d'Etat, statuant au contentieux a prononcé l'admission des conclusions du pourvoi de la société par actions simplifiée (SAS) Mayotte Channel Gateway (MCG) dirigées contre l'ordonnance n° 2000606 du 29 juin 2020 du juge des référés du tribunal administratif de Mayotte en tant qu'elles sont dirigées contre l'article 3 de cette ordonnance. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code général de la propriété des personnes publiques ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Charles-Emmanuel Airy, auditeur,  <br/>
<br/>
              - les conclusions de Mme Karin Ciavaldini, rapporteure publique ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP de Nervo, Poupet, avocat de la société Mayotte Channel Gateway et à la SCP Baraduc, Duhamel, Rameix, avocat de la société CMA Terminals Mayotte ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces des dossiers soumis aux juges du fond que le juge des référés du tribunal administratif de Mayotte a, par une ordonnance du 29 juin 2020, suspendu l'exécution de la décision du 1er juin 2020 par laquelle la société Mayotte Channel Gateway (MCG), titulaire d'une délégation de service public pour l'exploitation du domaine public portuaire du Port de Longoni, prenant acte de l'échec des négociations engagées avec la société CMA Terminals Mayotte en vue la conclusion au profit de celle-ci d'une convention d'occupation temporaire du domaine public portuaire, lui a demandé de ne plus exercer ses activités dans le port à compter du 1er juin 2020 à minuit et d'évacuer le domaine portuaire avant le 15 juin 2020. Par l'article 3 de cette ordonnance, le juge des référés a, en outre, enjoint à la société MCG de délivrer provisoirement à la société CMA Terminals Mayotte, au plus tard dans les cinq jours suivant sa notification, sous réserve qu'elle respecte le règlement et le cahier des charges du Port de Longoni, une autorisation d'occupation temporaire lui permettant d'exercer son activité dans l'enceinte portuaire de Longoni jusqu'à ce qu'il soit statué au fond sur sa requête tendant à l'annulation de la décision du 1er juin 2020. Par une décision du 23 novembre 2020, le Conseil d'Etat, statuant au contentieux a admis les conclusions du pourvoi de la société MCG dirigées contre cette ordonnance en tant seulement qu'elles concernent cet article 3. <br/>
<br/>
              2. Après avoir estimé, dans l'ordonnance attaquée, devenue définitive sur ce point, que la condition d'urgence prévue par l'article L. 521-1 du code de justice administrative était satisfaite et qu'était de nature à faire naître un doute sérieux sur la légalité de la décision du 1er juin 2020 le moyen tiré de ce qu'elle était entachée de détournement de pouvoir, le juge des référés du tribunal administratif de Mayotte a suspendu l'exécution de cette décision. <br/>
<br/>
              3. Le juge des référés a en outre jugé que cette suspension impliquait nécessairement que la société MCG délivre provisoirement à la société CMA Terminals Mayotte, sous réserve qu'elle respecte le règlement et le cahier des charges du Port de Longoni, une autorisation d'occupation temporaire du domaine public lui permettant d'exercer son activité jusqu'à ce qu'il soit statué au fond sur sa requête. En statuant ainsi, alors que la société CMA Terminals Mayotte ne disposait, à la date de la décision contestée, d'aucun titre d'occupation du domaine public portuaire et que la suspension de l'exécution de la décision du 1er juin 2020, laquelle emportait notamment rejet de la demande de cette société tendant à la délivrance d'un titre d'occupation, impliquait seulement qu'il soit procédé à un réexamen de cette demande, le juge des référés du tribunal administratif de Mayotte a commis une erreur de droit.<br/>
<br/>
              4. Dans les circonstances de l'espèce, il y a lieu, en application des dispositions de l'article L. 821-2 du code de justice administrative, de régler l'affaire au titre de la procédure de référé engagée.<br/>
<br/>
              5. Ainsi qu'il a été dit au point 3, la suspension de l'exécution de la décision du 1er juin 2020 de la société MCG demandant à la société CMA Terminals Mayotte de ne plus exercer ses activités dans le port de Mayotte à compter du 1er juin 2020 à minuit et d'évacuer le domaine portuaire avant le 15 juin 2020, qui emporte refus de la demande de cette société tendant à la délivrance d'un titre d'occupation, implique seulement que la société MCG réexamine cette demande. Il y a lieu, par suite, d'enjoindre à la société MCG de procéder à ce réexamen.<br/>
<br/>
              6. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de la société MCG qui n'est pas, dans la présente instance, la partie perdante. Il n'y a par ailleurs pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par la société MCG à ce même titre. <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'article 3 de l'ordonnance du 29 juin 2020 du juge des référés du tribunal administratif de Mayotte est annulée.<br/>
Article 2 : Il est enjoint à la société MCG de réexaminer, dans un délai de 15 jours à compter de la notification de la présente décision, la demande de la société CMA Terminals Mayotte tendant à la délivrance d'une autorisation d'occupation du domaine public portuaire.<br/>
<br/>
Article 3 : Le surplus des conclusions du pourvoi de la société MCG est rejeté.<br/>
Article 4 : Les conclusions de la société CMA Terminals Mayotte présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée à la société par actions simplifiée Mayotte Channel Gateway, à la société CMA Terminals Mayotte, au préfet de Mayotte et au président du conseil départemental de Mayotte.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
