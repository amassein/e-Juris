<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043623015</ID>
<ANCIEN_ID>JG_L_2021_06_000000438607</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/62/30/CETATEXT000043623015.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème chambre, 04/06/2021, 438607, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-06-04</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>438607</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FOUSSARD, FROGER ; SCP THOUVENIN, COUDRAY, GREVY</AVOCATS>
<RAPPORTEUR>M. Philippe Ranquet</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Marie-Gabrielle Merloz</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:438607.20210604</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              M. B... A... a demandé au tribunal administratif de Lille d'annuler la délibération du 9 juin 2015 par laquelle la communauté d'agglomération Cap Calaisis a supprimé son emploi de chargé de mission et de la condamner à lui verser la somme de 119 457,14 euros en réparation des préjudices qu'il estime avoir subis à raison de l'illégalité de cette décision et de faits de harcèlement moral. Par un jugement n° 1510103 du 23 octobre 2018, le tribunal administratif de Lille a annulé la délibération attaquée et a rejeté le surplus des conclusions de la demande de M. A....<br/>
<br/>
              Par un arrêt n° 18DA02611 du 12 décembre 2019, la cour administrative d'appel de Douai, sur appel de M. A..., a annulé ce jugement en tant qu'il a rejeté ses conclusions indemnitaires et, après évocation, a rejeté ces mêmes conclusions.  <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 12 février et 22 mai 2020 au secrétariat du contentieux du Conseil d'Etat, M. A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt dans la mesure où il a rejeté ses conclusions ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande ;<br/>
<br/>
              3°) de mettre à la charge de la communauté d'agglomération Grand Calais Terres et Mers la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code général des collectivités territoriales ;<br/>
              - la loi n° 83-634 du 13 juillet 1983 ;<br/>
              - la loi n° 84-53 du 26 janvier 1984 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Philippe Ranquet, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Marie-Gabrielle Merloz, rapporteure publique ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Foussard, Froger, avocat de M. A... et à la SCP Thouvenin, Coudray, Grevy, avocat de la communauté d'agglomération du Calaisis ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que par un arrêté du 12 novembre 2014, le président de la communauté d'agglomération Cap Calaisis a mis fin au détachement de M. B... A..., ingénieur territorial en chef, dans l'emploi de directeur général des services de la communauté, et l'a affecté à un emploi de chargé de mission " Pôle métropolitain de la Côte d'Opale ". Par une délibération du 9 juin 2015, le conseil communautaire a supprimé cet emploi. M. A... a demandé au tribunal administratif de Lille d'annuler cette délibération et de condamner la communauté d'agglomération Cap Calaisis, aux droits de laquelle vient la communauté d'agglomération Grand Calais Terres et Mers, à lui verser la somme de 119 457,14 euros en réparation des préjudices qu'il estime avoir subis à raison de l'illégalité de cette décision et de faits de harcèlement moral. Par un jugement du 23 octobre 2018, le tribunal administratif de Lille a annulé la délibération attaquée et a rejeté les conclusions indemnitaires de M. A.... Ce dernier se pourvoit en cassation contre l'arrêt du 12 décembre 2019 par lequel la cour administrative d'appel de Douai, après avoir annulé sur son appel ce jugement dans la mesure où il lui faisait grief et statuant par évocation, a rejeté à son tour ses conclusions indemnitaires.<br/>
<br/>
              Sur les préjudices qui résulteraient de la délibération supprimant l'emploi de chargé de mission<br/>
<br/>
              2. La délibération du 9 juin 2015 a été annulée par le tribunal administratif de Lille, dont le jugement du 23 octobre 2018 est devenu définitif sur ce point, au motif que l'avis préalable du comité technique a été rendu dans des conditions irrégulières. Pour rejeter la demande de M. A... tendant à la réparation des conséquences dommageables de l'illégalité de cette délibération, la cour administrative d'appel a jugé que celle-ci n'était à l'origine d'aucun préjudice, dès lors que la délibération n'était illégale pour aucun autre motif que ce vice de procédure et que dans ces conditions, le conseil communautaire aurait pu légalement prendre la même décision au terme d'une procédure régulière.<br/>
<br/>
              3. En premier lieu, les moyens de M. A... tirés de ce que la cour aurait commis des erreurs de droit et dénaturé les pièces du dossier en ne jugeant pas la délibération illégale faute de consultation préalable de la commission administrative paritaire et d'information suffisante du conseil communautaire ne sont pas de nature, à les supposer fondés, à remettre en cause l'appréciation selon laquelle la délibération n'est entachée que d'un vice de procédure. Ils sont par suite, en tout état de cause, inopérants à l'encontre du rejet des conclusions indemnitaires.<br/>
<br/>
              4. En deuxième lieu, c'est sans méconnaître les dispositions de l'article 97 de la loi du 26 janvier 1984 portant dispositions statutaires relatives à la fonction publique territoriale ni se contredire que la cour a jugé que si ces dispositions font obligation à la collectivité publique de rechercher des possibilités de reclassement dès qu'un emploi est susceptible d'être supprimé, la légalité de la décision portant suppression de l'emploi ne dépend pas du respect de cette obligation.<br/>
<br/>
              5. En dernier lieu, une collectivité territoriale peut légalement, quel que soit l'état de ses finances, procéder à une suppression d'emploi par mesure d'économie. La cour a relevé qu'il ressortait des pièces du dossier, notamment du procès-verbal de la séance du conseil communautaire du 9 juin 2015 et d'un rapport de la chambre régionale des comptes, que la suppression d'emploi litigieuse procédait de la nécessité de maîtriser l'augmentation de la masse salariale ainsi que du choix, au regard des contraintes budgétaires, de confier les missions en cause à d'autres agents déjà en poste, et qu'en conséquence il n'était pas établi que la délibération contestée serait entachée d'un détournement de pouvoir et de procédure. Elle a également écarté, comme n'étant pas assorti d'éléments permettant d'en apprécier le bien-fondé, le moyen de M. A... invoquant une erreur d'appréciation sur ce point. En statuant ainsi, la cour n'a pas dénaturé les pièces du dossier qui lui était soumis.<br/>
<br/>
              Sur le harcèlement moral<br/>
<br/>
              6. Si M. A... soutenait avoir été victime de harcèlement moral dès lors qu'il aurait subi une " mise au placard " de la part de l'exécutif de la communauté d'agglomération quand ce dernier a eu le projet de mettre fin à ses fonctions de directeur général des services, qu'il aurait ensuite été affecté à un emploi destiné à être supprimé à brève échéance et qu'il aurait fait l'objet de propos de responsables de la communauté portant atteinte à son honneur et à son avenir professionnels, la cour administrative d'appel de Douai n'a pas dénaturé les pièces du dossier qui lui était soumis en estimant que ces allégations n'étaient pas suffisamment étayées pour permettre de faire présumer l'existence d'un harcèlement moral. De même, c'est sans donner aux faits de l'espèce une qualification inexacte qu'elle n'a retenu, comme étant de nature à caractériser des agissements de harcèlement moral, ni la circonstance que la communauté n'ait pas accordé à M. A... la protection fonctionnelle à la suite de propos de responsables syndicaux à son encontre, dès lors que le caractère diffamatoire de ces propos n'était pas établi, ni la suppression de l'emploi de chargé de mission, dès lors que celle-ci était justifiée par des considérations d'intérêt général ainsi qu'il a été dit ci-dessus, ni la perte de primes subie par M. A... quand il a été maintenu en surnombre après cette suppression, dès lors que la communauté était légalement tenue de mettre fin au versement des primes en cause, comme l'a jugé la même cour dans son arrêt n° 18DA02610 du même jour devenu définitif.<br/>
<br/>
              7. Il résulte de tout ce qui précède que M. A... n'est pas fondé à demander l'annulation de l'arrêt qu'il attaque.<br/>
<br/>
              8. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de la communauté d'agglomération Grand Calais Terres et Mers qui n'est pas, dans la présente instance, la partie perdante. Il n'y a pas lieu, dans les circonstances de l'espèce, de mettre à la charge de M. A... la somme demandée au même titre par la communauté d'agglomération Grand Calais Terres et Mers.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de M. A... est rejeté.<br/>
Article 2 : Les conclusions présentées par la communauté d'agglomération Grand Calais Terres et Mers au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La présente décision sera notifiée à M. B... A... et à la communauté d'agglomération Grand Calais Terres et Mers.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
