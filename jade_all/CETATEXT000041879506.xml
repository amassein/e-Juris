<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000041879506</ID>
<ANCIEN_ID>JG_L_2020_05_000000440460</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/41/87/95/CETATEXT000041879506.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 12/05/2020, 440460, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-05-12</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>440460</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2020:440460.20200512</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Par une requête, un nouveau mémoire et un mémoire en réplique, enregistrés les 7 et 10 mai 2020 et le 11 mai 2020 avant la clôture de l'instruction au secrétariat du contentieux du Conseil d'Etat, le Syndicat Jeunes médecins demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative :<br/>
<br/>
              1°) de suspendre l'exécution de la consigne d'isolement des enfants de soignants lors de leur retour à l'école à compter du 12 mai 2020, s'il est avéré qu'une telle consigne a été donnée par le Gouvernement ;<br/>
<br/>
              2°) en l'absence d'une telle consigne, d'enjoindre à l'Etat de diffuser largement, notamment aux recteurs et directeurs d'écoles élémentaires, au plus tard le 12 mai 2020, une information quant à la nécessité d'accueillir les enfants de personnels de santé de manière identique aux autres élèves et, lorsqu'ils n'appartiennent pas aux groupes d'élèves pour lesquels les écoles sont rouvertes, quant à la nécessité de les accueillir dans les conditions prévues à l'article 10 du décret n° 2020-545 du 11 mai 2020 ;<br/>
<br/>
              3°) d'enjoindre au Gouvernement de prendre les mesures règlementaires garantissant l'accueil des enfants des personnels prioritaires dans tous les communes.<br/>
<br/>
<br/>
<br/>
              Il soutient que :<br/>
              - la condition d'urgence est remplie dès lors que la reprise des classes a été décidée à partir du 12 mai 2020 ;<br/>
              - l'isolement des enfants de personnels soignants du reste des élèves est discriminatoire, injustifié et porte une atteinte grave et manifestement illégale à leur droit à un égal accès à l'éducation et à l'accès à une scolarisation adaptée, au droit de chacun à la liberté personnelle et au droit de ne pas subir une situation portant à atteinte à la santé mentale.<br/>
              Par deux mémoires en défense, enregistrés le 11 mai 2020 avant la clôture de l'instruction, le ministre de l'éducation nationale et de la jeunesse conclut au rejet de la requête. Il soutient que les moyens soulevés par le syndicat requérant ne sont pas fondés.<br/>
<br/>
              La requête a été communiquée au Premier ministre et au ministre des solidarités et de la santé qui n'ont pas produit d'observations.<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu :<br/>
              - la Constitution ;<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - le code de l'éducation ;<br/>
              - la loi n° 2020-290 du 23 mars 2020 ;<br/>
              - la loi n° 2020-546 du 11 mai 2020 ;<br/>
              - le décret n° 2020-293 du 23 mars 2020 ;<br/>
              - le décret n° 2020-545 du 11 mai 2020 ;<br/>
              - le décret n° 2020-548 du 11 mai 2020 ;<br/>
              - le code de justice administrative et l'ordonnance n° 2020-305 du 25 mars 2020 ;<br/>
<br/>
              Les parties ont été informées, sur le fondement de l'article 9 de l'ordonnance du 25 mars 2020 portant adaptation des règles applicables devant les juridictions de l'ordre administratif, de ce qu'aucune audience ne se tiendrait et de ce que la clôture de l'instruction était fixée au 11 mai 2020 à 19 heures, puis repoussée à 20 heures.<br/>
<br/>
<br/>
<br/>
<br/>
Considérant ce qui suit :<br/>
              1. Aux termes de l'article L. 521-2 du même code : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. Le juge des référés se prononce dans un délai de quarante-huit heures ". <br/>
<br/>
              Sur l'office du juge des référés :<br/>
<br/>
              2. Il résulte de la combinaison des dispositions des articles L. 511-1 et L. 521-2 du code de justice administrative qu'il appartient au juge des référés, lorsqu'il est saisi sur le fondement de l'article L. 521-2 et qu'il constate une atteinte grave et manifestement illégale portée par une personne morale de droit public à une liberté fondamentale, résultant de l'action ou de la carence de cette personne publique, de prescrire les mesures qui sont de nature à faire disparaître les effets de cette atteinte, dès lors qu'existe une situation d'urgence caractérisée justifiant le prononcé de mesures de sauvegarde à très bref délai et qu'il est possible de prendre utilement de telles mesures. Celles-ci doivent, en principe, présenter un caractère provisoire, sauf lorsque aucune mesure de cette nature n'est susceptible de sauvegarder l'exercice effectif de la liberté fondamentale à laquelle il est porté atteinte. Le caractère manifestement illégal de l'atteinte doit s'apprécier notamment en tenant compte des moyens dont dispose l'autorité administrative compétente et des mesures qu'elle a déjà prises.<br/>
<br/>
              Sur les circonstances :<br/>
<br/>
              3. L'émergence d'un nouveau coronavirus (covid-19), de caractère pathogène et particulièrement contagieux, et sa propagation sur le territoire français a conduit le ministre des solidarités et de la santé à prendre, par plusieurs arrêtés à compter du 4 mars 2020, des mesures sur le fondement des dispositions de l'article L. 3131-1 du code de la santé publique. En particulier, par un arrêté du 14 mars 2020, un grand nombre d'établissements recevant du public ont été fermés au public, les rassemblements de plus de 100 personnes ont été interdits et l'accueil des enfants dans les établissements les recevant et les établissements scolaires et universitaires a été suspendu, un accueil étant toutefois assuré, dans des conditions de nature à prévenir le risque de propagation du virus, pour les enfants de moins de seize ans des personnels indispensables à la gestion de la crise sanitaire, notamment dans les établissements d'enseignement scolaire. Puis, par un décret du 16 mars 2020 motivé par les circonstances exceptionnelles découlant de l'épidémie de covid-19, modifié par décret du 19 mars, le Premier ministre a interdit le déplacement de toute personne hors de son domicile, sous réserve d'exceptions limitativement énumérées et devant être dûment justifiées, à compter du 17 mars à 12h, sans préjudice de mesures plus strictes susceptibles d'être ordonnées par le représentant de l'Etat dans le département. Le ministre des solidarités et de la santé a pris des mesures complémentaires par des arrêtés des 17, 19, 20, 21 mars 2020. <br/>
<br/>
              4. Par l'article 4 de la loi du 23 mars 2020 d'urgence pour faire face à l'épidémie de covid-19, a été déclaré l'état d'urgence sanitaire pour une durée de deux mois sur l'ensemble du territoire national, prorogée jusqu'au 19 juillet 2020 inclus par la loi du 11 mai 2020 prorogeant l'état d'urgence sanitaire et complétant ses dispositions. Par un nouveau décret du 23 mars 2020 pris sur le fondement de l'article L. 3131-15 du code de la santé publique issu de la loi du 23 mars 2020, le Premier ministre a réitéré les mesures qu'il avait précédemment ordonnées tout en leur apportant des précisions ou restrictions complémentaires.<br/>
<br/>
              5. Enfin, par le décret n° 2020-545 du 11 mai 2020 ci-dessus visé, le Premier ministre a abrogé le décret du 23 mars 2020, à l'exception de son article 5-1 relatif aux collectivités d'outre-mer et à la Nouvelle-Calédonie, et a adopté de nouvelles dispositions. Il a notamment, au I de l'article 10 de ce décret, autorisé l'accueil des usagers dans les écoles maternelles et élémentaires ainsi que les classes correspondantes des établissements d'enseignement privé, dans des conditions de nature à permettre le respect des règles d'hygiène et de distanciation sociale définies au niveau national pour ces établissements en application de l'article 1er du même décret. En vertu du III du même article, un accueil demeure assuré par les mêmes établissements au profit des enfants des personnels indispensables à la gestion de la crise sanitaire et à la continuité de la vie de la Nation. Ces dispositions ont été reprises à l'article 12 du décret n° 2020-548 du 11 mai 2020.<br/>
<br/>
              Sur la demande en référé :<br/>
<br/>
              6. En premier lieu, si le Syndicat Jeunes médecins fait valoir que, dans certaines académies, notamment celles de Nice et de Toulouse, il a été fait état d'une consigne selon laquelle les enfants du personnel soignant devaient être accueillis séparément du reste des élèves, au sein d'une autre école ou d'une autre classe que celle d'origine, il ne résulte toutefois pas de l'instruction qu'une telle consigne aurait été donnée par le Gouvernement. En particulier, si la circulaire adressée à ses services le 4 mai 2020 par le ministre de l'éducation nationale et de la jeunesse, que ce dernier produit, permet la constitution de groupes multi-niveaux pour scolariser des élèves regardés comme prioritaires parce qu'ils sont en situation de handicap, parce qu'ils sont décrocheurs ou en risque de décrochage ou parce qu'ils sont enfants des personnels indispensables à la gestion de la crise sanitaire et à la continuité de la vie de la Nation, il résulte des termes mêmes de cette circulaire qu'elle ne l'envisage que lorsque les cours de ces enfants n'ont pas repris par ailleurs. Les conclusions du syndicat requérant tendant, dans l'hypothèse où il serait avéré qu'une consigne d'isolement des enfants de soignants lors de leur retour à l'école à compter du 12 mai 2020 aurait été donnée par le Gouvernement, à ce qu'en soit ordonnée la suspension sur le fondement de l'article L. 521-2 du code de justice administrative, ne peuvent par suite qu'être rejetées.<br/>
<br/>
              7. En deuxième lieu, si le ministre de l'éducation nationale et de la jeunesse a admis, au vu des pièces produites par le syndicat requérant, que des consignes différentes avaient pu être localement données au sein des académies de Toulouse et de Nice s'agissant de l'accueil des enfants du personnel soignant, il résulte de l'instruction que, dans chacune de ces académies, le recteur a, le 9 mai 2020, adressé un courrier aux inspecteurs d'académie afin de leur rappeler que toute consigne d'isolement des enfants des personnels indispensables à la gestion de la crise sanitaire des autres élèves accueillis était contraire aux modalités d'accueil arrêtées par le ministre, en leur demandant de s'assurer que chacune des écoles de leur département respectait les modalités d'accueil définies nationalement et de rectifier sans délai toute situation contraire. Ce courrier s'est accompagné, dans l'académie de Toulouse, d'un communiqué de presse. Le syndicat requérant n'établit pas, par la seule référence à un " tweet " d'un auteur non identifié, qu'une telle consigne existerait à ce jour à Mulhouse. Par suite, l'Etat ayant lui-même pris les dispositions propres à ce que, lorsqu'elle s'avérait nécessaire, soit rappelée l'information selon laquelle l'accueil dans les écoles élémentaires des enfants du personnel de santé doit être assuré de manière identique aux autres élèves, les conclusions du syndicat requérant tendant, sur le fondement de l'article L. 521-2 du code de justice administrative, à ce que soit prononcée une injonction ayant le même objet ne peuvent qu'être rejetées.<br/>
<br/>
              8. Enfin, les conclusions du syndicat requérant tendant à ce qu'il soit enjoint au Gouvernement, sur le fondement des mêmes dispositions, de prendre les mesures règlementaires garantissant l'accueil des enfants des personnels prioritaires dans tous les communes ne sont pas assorties des précisions permettant d'en apprécier le bien-fondé. Au demeurant, il résulte de ce qui a été dit aux points 5 et 6 que des mesures ont été prises afin que ces enfants puissent demeurer accueillis alors même que leurs cours n'auraient pas repris, sans que le syndicat requérant n'apporte aucun élément de nature à laisser supposer qu'il existe un risque que l'obligation que le III de l'article 10 du décret n° 2020-545 du 11 mai 2020 comporte à ce titre, réitérée par le III de l'article 12 du décret n° 2020-548 du même jour qui l'a remplacé, ne soit pas respectée. Il n'y a pas davantage lieu, dans ces conditions, de faire droit aux conclusions par lesquelles il demande que soit enjoint au Gouvernement de communiquer largement sur cette obligation d'accueil, qui figure parmi les dispositions de décrets publiés au Journal officiel de la République française.<br/>
<br/>
              9. Il résulte de tout ce qui précède que les conclusions présentées par le syndicat requérant doivent être rejetées.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête du Syndicat Jeunes médecins est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée au Syndicat Jeunes médecins et au ministre de l'éducation nationale et de la jeunesse.<br/>
Copie en sera adressée au Premier ministre et au ministre des solidarités et de la santé.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
