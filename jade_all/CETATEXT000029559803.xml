<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029559803</ID>
<ANCIEN_ID>JG_L_2014_10_000000373392</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/55/98/CETATEXT000029559803.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème SSJS, 08/10/2014, 373392, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-10-08</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>373392</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème SSJS</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BARTHELEMY, MATUCHANSKY, VEXLIARD, POUPOT</AVOCATS>
<RAPPORTEUR>M. Philippe Orban</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Rémi Keller</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2014:373392.20141008</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 20 novembre 2013 et 20 février 2014 au secrétariat du contentieux du Conseil d'Etat, présentés pour Mme A...B..., demeurant ... ; Mme B...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'ordonnance n° 1102816 du 20 septembre 2013 par laquelle le président de la 4ème chambre du tribunal administratif de Marseille a rejeté sa demande tendant, en premier lieu, à l'annulation du rejet implicite par le ministre de l'éducation nationale de sa demande en date du 16 décembre 2010 tendant à se voir octroyer le bénéfice de l'avantage spécifique d'ancienneté, à être rétablie dans ses droits en ce qui concerne sa carrière, ses droits à retraite et le montant de ses salaires, et à obtenir réparation de ses autres préjudices, en deuxième lieu, à ce qu'il soit enjoint au ministre de l'éducation nationale, au ministre chargé de la ville, au ministre chargé de la fonction publique et au ministre du budget d'inscrire le collège Jean-Claude Izzo sur la liste des écoles et établissements d'enseignement régie par le 2° de l'article 1er du décret n° 95-321 du 21 mars 1995, en troisième lieu, à ce qu'il soit enjoint au ministre de l'éducation nationale de la rétablir dans ses droits à avancement à traitement à compter du 1er septembre 2005, et, en quatrième lieu, à condamner l'Etat à lui verser la somme de 500 euros en réparation du préjudice moral et des troubles dans les conditions d'existence résultant de l'absence d'inscription du collège Jean-Claude Izzo sur la liste mentionnée précédemment ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
<br/>
              Vu la loi n° 84-16 du 11 janvier 1984 ;<br/>
<br/>
              Vu la loi n° 91-715 du 26 juillet 1991 ;<br/>
<br/>
              Vu le décret n° 95-313 du 21 mars 1995 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Philippe Orban, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Rémi Keller, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Barthélemy, Matuchansky, Vexliard, Poupot, avocat de Mme B...;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant que Mme B...a été affectée au collège Jean-Claude Izzo de Marseille à compter du 1er septembre 2005 en qualité de professeur certifiée en documentation ; que par un courrier du 16 décembre 2010, elle a demandé au ministre de l'éducation nationale le bénéfice de l'avantage spécifique d'ancienneté (ASA) prévu pour les fonctionnaires de l'Etat affectés dans des quartiers difficiles par l'article 11 de la loi du 26 juillet 1991 portant diverses dispositions relatives à la fonction publique ; que, dans le silence gardé par le ministre pendant deux mois, Mme B...a demandé au tribunal administratif de Marseille d'annuler le rejet implicite de sa demande et de condamner l'Etat à lui verser la somme de 500 euros en réparation du préjudice moral et des troubles dans les conditions d'existence subis de ce fait ; que, par une ordonnance du 20 septembre 2013, le président de la 4ème chambre du tribunal administratif de Marseille a rejeté ses conclusions à fin d'annulation comme manifestement irrecevables sur le fondement du 4° de l'article R. 222-1 du code de justice administrative et ses conclusions indemnitaires comme non assorties de précisions suffisantes sur le fondement du 7° du même article ; que Mme B...se pourvoit en cassation contre cette ordonnance ;<br/>
<br/>
              Sur les conclusions du pourvoi en tant qu'elles contestent le rejet de ses conclusions d'annulation pour excès de pouvoir :<br/>
<br/>
              2. Considérant que les arrêtés des 24 février et 7 avril 2014 du ministre chargé de l'éducation nationale accordant à Mme B...le bénéfice de l'ASA dans les conditions fixées par l'article 2 du décret du 21 mars 1995 et reconstituant son traitement en conséquence ont, postérieurement à l'introduction du pourvoi, retiré rétroactivement la décision attaquée ; que, par suite, les conclusions du pourvoi  de Mme B...relatives au rejet des conclusions à fins d'annulation de la décision qui lui refuse l'ASA sont devenues sans objet et qu'il n'y a pas lieu d'y statuer ;<br/>
<br/>
              Sur les conclusions du pourvoi en tant qu'elles contestent le rejet des conclusions indemnitaires de Mme B...:<br/>
<br/>
              3. Considérant qu'aux termes de l'article R. 222-1 du code de justice administrative : " Les présidents de tribunal administratif et de cour administrative d'appel, le vice-président du tribunal administratif de Paris et les présidents de formation de jugement des tribunaux et des cours peuvent, par ordonnance : (...) 7° Rejeter, après l'expiration du délai de recours ou, lorsqu'un mémoire complémentaire a été annoncé, après la production de ce mémoire, les requêtes ne comportant que (...) des moyens qui (...) ne sont manifestement pas assortis des précisions permettant d'en apprécier le bien-fondé " ; qu'il ressort des écritures que Mme B...n'a pas assorti ses moyens de précisions permettant d'en apprécier le bien-fondé ; que, par suite, le président de la 4ème chambre du tribunal administratif de Marseille n'a pas fait une inexacte application des dispositions précitées ni méconnu les stipulations de l'article 6 § 1 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales en faisant application de ces dispositions ;<br/>
<br/>
              Sur les conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              4. Considérant que, dans les circonstances de l'espèce, il y a lieu de mettre à la charge de l'Etat la somme de 500 euros au titre des frais exposés par Mme B...pour les besoins de la présente instance et non compris dans les dépens ;<br/>
<br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er : Il n'y a pas lieu de statuer sur les conclusions du pourvoi dirigées contre l'ordonnance du 20 septembre 2013 du président de la 4ème chambre du tribunal administratif de Marseille en tant qu'elle a rejeté les conclusions de Mme B...dirigées contre le rejet implicite de sa demande tendant au bénéfice de l'ASA.<br/>
<br/>
Article 2 : L'Etat versera à Mme B...une somme de 500 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : Le surplus des conclusions du pourvoi est rejeté.<br/>
Article 4 : La présente décision sera notifiée à Mme A...B...et à la ministre de l'éducation nationale, de l'enseignement supérieur et de la recherche.<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
