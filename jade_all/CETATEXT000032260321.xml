<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032260321</ID>
<ANCIEN_ID>JG_L_2016_03_000000383570</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/26/03/CETATEXT000032260321.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème SSJS, 16/03/2016, 383570, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-03-16</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>383570</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème SSJS</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DIDIER, PINET</AVOCATS>
<RAPPORTEUR>M. Jean-Luc Matt</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emilie Bokdam-Tognetti</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2016:383570.20160316</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La SAS Soleco, devenue Florette France GMS, a demandé au tribunal administratif de Caen de prononcer la décharge des cotisations supplémentaires de taxe professionnelle auxquelles elle a été assujettie au titre des années 2005 à 2008 dans les rôles de la commune de Lessay (Manche), ainsi que des intérêts de retard correspondants. Par un jugement n° 0901069 du 16 novembre 2010, le tribunal administratif de Caen a fait droit à sa demande en tant qu'elle portait sur les années 2005 et 2006.<br/>
<br/>
              Par un arrêt n° 10NT02718 du 17 novembre 2011, la cour administrative d'appel de Nantes a rejeté le recours formé contre ce jugement par le ministre du budget, des comptes publics et de la réforme de l'Etat.<br/>
<br/>
              Par une décision n° 355630 du 7 novembre 2013, le Conseil d'Etat statuant au contentieux a annulé cet arrêt et renvoyé l'affaire à la cour administrative d'appel de Nantes.<br/>
<br/>
              Par un arrêt n° 13NT03194 du 12 juin 2014, la cour administrative d'appel de Nantes a annulé l'article 2 du jugement du tribunal administratif de Caen du 16 novembre 2010 et rétabli la société Soleco dans les rôles de la commune de Lessay au titre des années 2005 et 2006 à raison de l'intégralité des cotisations supplémentaires de taxe professionnelle auxquelles elle avait été assujettie. <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 8 août et 10 novembre 2014 au secrétariat du contentieux du Conseil d'Etat, la société Florette France GMS demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - la loi n° 2006-1666 du 21 décembre 2006 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Luc Matt, maître des requêtes,<br/>
<br/>
              - les conclusions de Mme Emilie Bokdam-Tognetti, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Didier, Pinet, avocat de la société Florette France GMS ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que la société Florette France GMS a, par convention du 31 décembre 2004 prenant effet au 1er janvier 2004, fait apport à sa filiale à 100 %, la SAS Soleco, d'une branche complète d'activité. A l'issue de la vérification de comptabilité dont la SAS Soleco a fait l'objet, le vérificateur a remis en cause l'application par cette dernière, pour le calcul de ses cotisations de taxe professionnelle, de la valeur locative minimum prévue par l'article 1518 B du code général des impôts et procédé au rehaussement de la valeur locative des immobilisations apportées par la société Florette France GMS en retenant, sur le fondement du 3° quater de l'article 1469 du même code, le prix de revient de ces immobilisations avant l'opération d'apport partiel d'actif ci-dessus décrite. Par un jugement du 16 novembre 2010, le tribunal administratif de Caen a accordé à la SAS Soleco la décharge des cotisations supplémentaires de taxe professionnelle auxquelles elle a, en conséquence, été assujettie au titre des années 2005 et 2006 ainsi que des intérêts de retard correspondants. Par un arrêt du 17 novembre 2011, la cour administrative d'appel de Nantes a rejeté l'appel formé par le ministre du budget, des comptes publics, de la fonction publique et de la réforme de l'Etat contre ce jugement. Par une décision du 7 novembre 2013, le Conseil d'Etat statuant au contentieux a toutefois annulé cet arrêt et renvoyé l'affaire à la cour. Par l'arrêt attaqué du 12 juin 2014, la même cour a annulé l'article 2 du jugement du 16 novembre 2010 du tribunal administratif de Caen et rétabli la SAS Soleco dans les rôles de la commune de Lessay au titre des années 2005 et 2006 à raison de l'intégralité des cotisations supplémentaires de taxe professionnelle auxquelles elle avait été assujettie.<br/>
<br/>
              Sur la régularité de l'arrêt attaqué :<br/>
<br/>
              2. Lorsque le juge d'appel, saisi par le défendeur de première instance, censure le motif retenu par les premiers juges, il lui appartient, en vertu de l'effet dévolutif de l'appel, d'examiner l'ensemble des moyens opérants présentés par le demandeur en première instance qui n'ont pas été expressément abandonnés en appel. En l'espèce, il ressort des pièces du dossier soumis aux juges du fond qu'à l'appui de sa demande devant le tribunal administratif, la SAS Soleco, devenue Florette France GMF, s'est prévalue, sur le fondement de l'article L. 80 B du livre des procédures fiscales, de la décision de dégrèvement partiel prise, le 5 février 2008, par la direction des services fiscaux de Vaucluse s'agissant de la taxe professionnelle due, au titre de l'année 2005, à raison de son établissement de l'Isle-sur-la-Sorgue et que ce moyen, qui n'était pas inopérant, n'a pas été abandonné en appel. Or la cour, après avoir censuré le motif retenu par le tribunal administratif dans son jugement, a omis de répondre à ce moyen dans le cadre de l'effet dévolutif de l'appel. Elle a, ainsi, entaché son arrêt d'irrégularité. La société Florette France GMS est, dès lors, fondée à demander l'annulation, pour ce motif, de cet arrêt. Ce moyen suffisant, à lui seul, à justifier la cassation de l'arrêt, il n'y a pas lieu d'examiner l'autre moyen du pourvoi.<br/>
<br/>
              3. En application du second alinéa de l'article L. 821-2 du code de justice administrative aux termes duquel " Lorsque l'affaire fait l'objet d'un second pourvoi en cassation, le Conseil d'Etat statue définitivement sur cette affaire ", il y a lieu de régler l'affaire au fond.<br/>
<br/>
              4. L'appel formé par le ministre tend à l'annulation des articles 2 et 3 du jugement attaqué, par lesquels le tribunal administratif de Caen a, d'une part, déchargé la SAS Soleco, devenue Florette France GMS, des cotisations supplémentaires de taxe professionnelle et des intérêts de retard correspondants auxquels elle a été assujettie au titre des années 2005 et 2006 dans les rôle de la commune de Lessay et, d'autre part, mis une somme de 1 000 euros à la charge de l'Etat au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              5. Aux termes de l'article 1467 du code général des impôts, dans sa rédaction applicable aux impositions en litige : " La taxe professionnelle a pour base : 1° Dans le cas des contribuables autres que ceux visés au 2° : a. la valeur locative, telle qu'elle est définie aux articles 1469, 1518 A et 1518 B, des immobilisations corporelles dont le redevable a disposé pour les besoins de son activité professionnelle pendant la période de référence définie aux articles 1467 A et 1478, à l'exception de celles qui ont été détruites ou cédées au cours de la même période (...) ". Aux termes du 3° quater de l'article 1469 du même code, dans sa rédaction alors applicable : " Le prix de revient d'un bien cédé n'est pas modifié lorsque ce bien est rattaché au même établissement avant et après la cession et lorsque, directement ou indirectement : a. l'entreprise cessionnaire contrôle l'entreprise cédante ou est contrôlée par elle ; b. ou ces deux entreprises sont contrôlées par la même entreprise ". Aux termes, enfin, de l'article 1518 B du même code dans sa rédaction alors applicable : " A compter du 1er janvier 1980, la valeur locative des immobilisations corporelles acquises à la suite d'apports, de scissions, de fusions de sociétés ou de cessions d'établissements réalisés à partir du 1er janvier 1976 ne peut être inférieure aux deux tiers de la valeur locative retenue l'année précédant l'apport, la scission, la fusion ou la cession. Les dispositions du premier alinéa s'appliquent aux seules immobilisations corporelles directement concernées par l'opération d'apport, de scission, de fusion ou de cession, dont la valeur locative a été retenue au titre de l'année précédant l'opération (...) Pour les opérations mentionnées au premier alinéa réalisées à compter du 1er janvier 1992, la valeur locative des immobilisations corporelles ne peut être inférieure aux quatre cinquièmes de son montant avant l'opération (...) Les dispositions du présent article s'appliquent distinctement aux trois catégories d'immobilisations suivantes : terrains, constructions, équipements et biens mobiliers ".<br/>
<br/>
              6. Il résulte des termes mêmes du 3° quater de l'article 1469 du code général des impôts que les cessions de biens qu'il vise s'entendent des seuls transferts de propriété consentis entre un cédant et un cessionnaire. Ces dispositions, dont les termes renvoient à une opération définie et régie par le droit civil, ne sauraient, dès lors, s'entendre comme incluant toutes autres opérations qui, sans constituer des cessions proprement dites, ont pour conséquence une mutation patrimoniale. L'opération par laquelle une société apporte une partie de ses éléments d'actif à une autre société en échange de titres de cette dernière doit être regardée comme une cession à titre onéreux au sens du droit civil dès lors que cette opération manifeste la rencontre de deux volontés, celle du cessionnaire et celle du cédant, et s'applique à une situation où, après l'opération, ces deux personnes subsistent. L'apport partiel d'actifs réalisé par la société Florette au profit de la SAS Soleco entre ainsi dans les prévisions du 3° quater de l'article 1469 du code général des impôts, sans qu'y fassent obstacle les dispositions de l'article 1518 B de ce code. Si l'article 33 de la loi du 21 décembre 2006 de finances pour 2007 a ajouté à cet article un dernier alinéa disposant que " Sans préjudice des dispositions du 3° quater de l'article 1469, les dispositions de l'article 1518 B du code général des impôts s'appliquent distinctement aux trois catégories d'immobilisations suivantes : terrains, constructions, équipements et biens mobiliers ", cet ajout a seulement eu pour objet de préciser que la règle de la valeur locative plancher prévue à l'article 1518 B du code général des impôts ne faisait pas obstacle à l'application de la règle du maintien du prix de revient prévue au 3° quater de l'article 1469 du même code et ne saurait être interprétée comme ne permettant l'application de cette dernière règle aux apports partiels d'actif qu'à compter du 1er janvier 2007. Le ministre est, par suite, fondé à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Caen a jugé que l'administration ne pouvait pas remettre en cause, au titre des années 2005 et 2006, et faute de disposition législative expresse en ce sens, l'application de la valeur locative plancher prévue à l'article 1518 B du code général des impôts et lui substituer celle de la règle du maintien du prix de revient prévue au 3° quater de l'article 1469 du même code.<br/>
<br/>
              7. Il appartient toutefois au Conseil d'Etat, saisi par l'effet dévolutif de l'appel, d'examiner les autres moyens soulevés par la SAS Soleco devant le tribunal administratif de Caen.<br/>
<br/>
              8. Il résulte de ce qui a été dit au point 6 que les dispositions du 3° quater de l'article 1469 du code général des impôts sont applicables pour déterminer la valeur locative de biens cédés dans le cadre d'un apport partiel d'actifs. Cette interprétation de la loi fiscale, qui ne présente aucun caractère rétroactif, ne méconnait pas le principe de sécurité juridique ni, en tout état de cause, le principe de confiance légitime.<br/>
<br/>
              9. Aux termes du premier alinéa de l'article L. 80 A du livre des procédures fiscales : " Il ne sera procédé à aucun rehaussement d'impositions antérieures si la cause du rehaussement poursuivi par l'administration est un différend sur l'interprétation par le redevable de bonne foi du texte fiscal et s'il est démontré que l'interprétation sur laquelle est fondée la première décision a été, à l'époque, formellement admise par l'administration ". Aux termes de l'article L. 80 B du même livre : " La garantie prévue au premier alinéa de l'article L. 80 A est applicable : 1° Lorsque l'administration a formellement pris position sur l'appréciation d'une situation de fait au regard d'un texte fiscal (...) ". Les contribuables ne sont en droit d'invoquer, sur le fondement du premier alinéa de l'article L. 80 A ou de l'article L. 80 B du livre des procédures fiscales, lorsque l'administration procède à un rehaussement d'impositions antérieures, que des interprétations ou des appréciations antérieures à l'imposition primitive.<br/>
<br/>
              10. Si, à l'appui de sa demande, la société requérante se prévaut, sur le fondement de l'article L. 80 B du livre des procédures fiscales, de la décision de dégrèvement partiel prise, le 5 février 2008, par la direction des services fiscaux de Vaucluse s'agissant de la taxe professionnelle due, au titre de l'année 2005, à raison de son établissement de l'Isle-sur-la-Sorgue, cette décision est postérieure à l'établissement des cotisations primitives de taxe professionnelle mises à sa charge au titre des années 2005 à 2007 à raison de son établissement de Lessay. La société ne saurait dès lors, en tout état de cause, s'en prévaloir pour contester les impositions supplémentaires en litige.<br/>
<br/>
              11. Il résulte de tout ce qui précède que le ministre de l'économie et des finances est fondé à soutenir que c'est à tort que, par l'article 2 du jugement attaqué, le tribunal administratif de Caen a déchargé la SAS Soleco des cotisations supplémentaires de taxe professionnelle, et des intérêts de retard correspondants, auxquels elle a été assujettie au titre des années 2005 et 2006 dans les rôles de la commune de Lessay. Par voie de conséquence, il est également fondé à soutenir que c'est à tort que, par l'article 3 du même jugement, le tribunal administratif a mis une somme de 1 000 euros à la charge de l'Etat au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              12. Les dispositions de cet article font obstacle à ce qu'une somme soit mise, à ce titre, à la charge de l'Etat, qui n'est pas la partie perdante dans la présente instance. <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
Article 1er : L'arrêt du 12 juin 2014 de la cour administrative d'appel de Nantes est annulé.<br/>
Article 2 : Les articles 2 et 3 du jugement du 16 novembre 2010 du tribunal administratif de Caen sont annulés.<br/>
Article 3 : La SAS Soleco est rétablie dans les rôles de la commune de Lessay au titre des années 2005 et 2006 à raison de l'intégralité des cotisations supplémentaires de taxe professionnelle, et des intérêts de retard correspondants, auxquels elle a été assujettie.<br/>
Article 4 : Les conclusions présentées en appel par la SAS Soleco et en cassation par la société Florette France GMS au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée à la société Florette France GMS et au ministre des finances et des comptes publics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
