<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030223887</ID>
<ANCIEN_ID>JG_L_2015_02_000000376186</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/22/38/CETATEXT000030223887.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème SSJS, 11/02/2015, 376186, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-02-11</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>376186</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème SSJS</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BARADUC, DUHAMEL, RAMEIX</AVOCATS>
<RAPPORTEUR>Mme Laurence Marion</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Bertrand Dacosta</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2015:376186.20150211</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la décision du 27 août 2014 par laquelle le Conseil d'Etat statuant au contentieux a prononcé l'admission des conclusions du pourvoi de la société Vinci construction grands projets, de la société GTM génie civil et services et de la société Baudin Chateauneuf dirigées contre l'arrêt n° 10BX00160 du 7 janvier 2014 de la cour administrative d'appel de Bordeaux en tant que cet arrêt a rejeté leurs conclusions tendant à l'indemnisation des travaux supplémentaires relatifs, d'une part, aux appuis " Freyssinet " et, d'autre part, aux tiges de serrage des colliers ;  <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 30 janvier 2015, présentée pour la société Vinci construction grands projets et autres ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Laurence Marion, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Bertrand Dacosta, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Baraduc, Duhamel, Rameix, avocat de la société Vinci construction grands projets, de la société GTM génie civil et services et de la société Baudin Chateauneuf ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, le 30 mars 2000, l'Etat a conclu avec le groupement d'entreprises constitué par la société Dumez GTM, devenue la société Vinci construction grands projets, la société GTM construction, devenue la société GTM génie civil et services, et la société Baudin Chateauneuf, un marché pour les travaux de remplacement de la suspension du pont d'Aquitaine et d'élargissement du tablier, dont la maîtrise d'oeuvre a été confiée à la direction départementale de l'équipement de la Gironde ; que les entreprises chargées des travaux se pourvoient en cassation contre l'arrêt du 7 janvier 2014 par lequel la cour administrative d'appel de Bordeaux a rejeté leur requête tendant notamment à la réformation du jugement du tribunal administratif de Bordeaux du 17 novembre 2009 en tant qu'il ne faisait pas droit à certaines de leurs demandes d'indemnisation au titre des travaux supplémentaires réalisés dans le cadre du marché litigieux ; que, par une décision du 27 août 2014, le Conseil d'Etat statuant au contentieux a admis les conclusions du pourvoi dirigées contre l'arrêt attaqué en tant qu'il a rejeté les conclusions tendant à l'indemnisation des travaux supplémentaires relatifs, d'une part, aux appuis " Freyssinet ", d'autre part, aux tiges de serrage des colliers ; <br/>
<br/>
              2. Considérant que pour écarter les demandes d'indemnisation relatives, d'une part, aux travaux supplémentaires mis en oeuvre au titre de la nouvelle solution technique concernant les appuis " Freyssinet " qui servent de liaison entre les massifs d'ancrage des câbles et les poutres arrières, d'autre part, à la substitution d'un nouveau procédé de protection des tiges de serrage des colliers par galvanisation à chaud au procédé prévu par le cahier des clauses techniques particulières, la cour a notamment énoncé que si ces variantes, acceptées par le maître d'oeuvre, présentaient des avantages, elle n'entraînaient pas pour autant la réalisation de travaux indispensables à la bonne exécution des ouvrages compris dans les prévisions du marché ; qu'en recherchant ainsi si les travaux en cause étaient indispensables à la bonne exécution des ouvrages, alors qu'ils avaient donné lieu à des ordres de service établis par le maître d'oeuvre, la cour a commis une erreur de droit ; que son arrêt doit, par suite, être annulé en tant qu'il a rejeté les conclusions tendant à l'indemnisation des travaux supplémentaires relatifs aux appuis " Freyssinet " et aux tiges de serrage des colliers ;<br/>
<br/>
              3. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler, dans cette mesure, l'affaire au fond en application de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              4. Considérant, en premier lieu, que la nouvelle solution technique relative aux appuis " Freyssinet " proposée par le groupement d'entreprises a été acceptée par le maître d'oeuvre par un ordre de service n° 3860 du 5 juin 2001, aux termes duquel : " La solution technique proposée par le groupement concernant les appuis " Freyssinet " servant de liaison entre les poutres arrières et les massifs, étant meilleure en termes technique, financier et de délais, est acceptée " ; qu'il résulte des termes mêmes de cet ordre de service, dont il n'est pas allégué qu'il aurait donné lieu à des réserves de la part des entreprises, que la nouvelle solution a été acceptée notamment en tant qu'elle était moins coûteuse que celle prévue par le marché ; que les sociétés requérantes ne sont, par suite, pas fondées à demander une indemnité supérieure au montant de 24 135, 34 euros alloué par le tribunal administratif de Bordeaux au titre de l'application des prix du marché pour les dispositifs de vérinage et de glissement vertical ;<br/>
<br/>
              5. Considérant, en second lieu, qu'il résulte de l'instruction que la solution prévue au marché pour la protection des tiges de serrage des colliers pouvait être mise en oeuvre ; qu'il n'est pas sérieusement contesté que la solution alternative de galvanisation à chaud proposée par le groupement d'entreprises dans le cadre des études d'exécution et acceptée par la maîtrise d'oeuvre était plus simple à mettre en oeuvre que celle prévue au marché, impliquant une double métallisation avant et après pose ; que les sociétés requérantes ne sont, par suite, pas fondées à demander l'allocation d'un supplément de rémunération à ce titre ; <br/>
<br/>
              6. Considérant qu'il résulte de ce qui précède que les sociétés requérantes ne sont pas fondées à se plaindre de ce que le tribunal administratif de Bordeaux a rejeté leurs demandes de suppléments de rémunération relatifs aux nouvelles solutions techniques appliquées aux appuis " Freyssinet " et à la protection des tiges de serrage des colliers ; <br/>
<br/>
              7. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mise à la charge de l'Etat, qui n'est pas, dans la présente instance, la partie perdante, le versement aux sociétés Vinci construction grands projets, GTM génie civil et services et Baudin Chateauneuf de la somme qu'elles réclament à ce titre ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Bordeaux du 7 janvier 2014 est annulé en tant qu'il a rejeté les conclusions tendant à l'indemnisation des travaux supplémentaires relatifs aux appuis " Freyssinet " et aux tiges de serrage des colliers.<br/>
Article 2 : Les conclusions relatives à l'indemnisation des travaux supplémentaires relatifs aux appuis " Freyssinet " et aux tiges de serrage des colliers présentées par les sociétés Vinci construction grands projets, GTM génie civil et services et Baudin Chateauneuf devant la cour administrative d'appel de Bordeaux ainsi que leurs conclusions tendant à l'application de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La présente décision sera notifiée à la société Vinci construction grands projets, à la société GTM génie civil et services, à la société Baudin Chateauneuf et à la ministre de l'écologie, du développement durable et de l'énergie.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
