<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044359287</ID>
<ANCIEN_ID>JG_L_2021_11_000000448292</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/35/92/CETATEXT000044359287.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème chambre, 22/11/2021, 448292, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-11-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>448292</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FOUSSARD, FROGER</AVOCATS>
<RAPPORTEUR>Mme Rose-Marie Abel</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Marie-Gabrielle Merloz</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:448292.20211122</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Mme D... A... a demandé au tribunal administratif de Montreuil d'annuler les opérations électorales qui se sont déroulées le 15 mars 2020 dans la commune de Villetaneuse, de rejeter le compte de campagne de M. G... et de le déclarer inéligible et de prononcer l'inéligibilité de M. F... et de M. C.... Par un jugement n° 2003565 du 3 décembre 2020, le tribunal administratif de Montreuil a rejeté sa protestation.<br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés les 31 décembre 2020 et 4 juin 2021 au secrétariat du contentieux du Conseil d'Etat, Mme A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) de faire droit aux conclusions de sa protestation électorale ;<br/>
<br/>
              3°) de de mettre à la charge de l'ensemble de la liste conduite par M. G... la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code électoral;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Rose-Marie Abel, maître des requêtes en service extraordinaire,<br/>
<br/>
              - les conclusions de Mme Marie-Gabrielle Merloz, rapporteure publique ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Foussard, Froger, avocat de Mme A... ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. A l'issue des opérations électorales qui se sont déroulées le 15 mars 2020 à Villetaneuse, commune de plus de 1 000 habitants, en vue de l'élection des conseillers municipaux et du conseiller communautaire, la liste " Villetaneuse autrement " conduite par M. G... a obtenu 1 214 voix (50,33 % des suffrages exprimés) et a obtenu vingt-cinq des trente-trois sièges que compte le conseil municipal de cette commune et le siège de conseiller communautaire. Sept candidats de la liste " Villetaneuse en commun, pour une ville solidaire, humaine et écologique " conduite par Mme A..., qui a obtenu 946 voix (39,22 % des suffrages exprimés), ont été élus. Un candidat figurant sur la liste " Ensemble, faisons Villetaneuse ! " conduite par Mme B..., qui a obtenu 252 voix (10,44 % de suffrages exprimés), a également été élu. Mme A... fait appel du jugement du 3 décembre 2020 par lequel le tribunal administratif de Montreuil a rejeté sa protestation dirigée contre ces opérations électorales.<br/>
<br/>
              Sur la régularité du jugement attaqué :<br/>
<br/>
              2. Contrairement à ce que soutient Mme A..., le tribunal administratif de Montreuil a répondu, au point 6 de son jugement, au grief tiré de ce que trois personnes ont appelé à voter en faveur de la liste conduite par M. G... la veille et le jour du scrutin. Par ailleurs, en ne répondant pas au moyen tenant à ce que ces appels auraient méconnu le secret du vote, qui était inopérant dès lors qu'ils avaient pris la forme de messages publiés sur les réseaux sociaux, le tribunal n'a pas entaché son jugement d'une insuffisance de motivation.<br/>
<br/>
              Sur le bien-fondé du jugement attaqué :<br/>
<br/>
              En ce qui concerne l'éligibilité de M. F... et de M. C... :<br/>
<br/>
              3. Aux termes du deuxième alinéa de l'article L. 228 du code électoral : " Sont éligibles au conseil municipal tous les électeurs de la commune et les citoyens inscrits au rôle des contributions directes ou justifiant qu'ils devaient y être inscrits au 1er janvier de l'année de l'élection ". Aux termes de l'article L. 11 du même code : " I.- Sont inscrits sur la liste électorale de la commune, sur leur demande : 1° Tous les électeurs qui ont leur domicile réel dans la commune ou y habitent depuis six mois au moins (...) ". Il résulte de ces dispositions que, dès lors qu'un candidat est électeur d'une commune, la circonstance qu'il ne soit pas inscrit au rôle des impôts directs locaux dans cette dernière est sans incidence sur son éligibilité. Il n'appartient pas au juge de l'élection d'apprécier si un électeur inscrit sur les listes électorales remplit effectivement l'une des conditions prévues par l'article L. 11 du code électoral, mais seulement de rechercher si des manœuvres dans l'établissement de la liste électorale ont altéré la sincérité du scrutin. <br/>
<br/>
              4. Mme A... a soutenu devant le tribunal administratif de Montreuil que M. F... était inéligible dès lors, d'une part, qu'il avait été irrégulièrement inscrit sur la liste électorale de la commune de Villetaneuse faute d'y avoir son domicile réel et, d'autre part, que cette inscription résultait d'une manœuvre ayant altéré la sincérité du scrutin. Le tribunal administratif de Montreuil, pour rejeter ses conclusions tendant à ce que soit prononcée l'inéligibilité de M. F..., s'est à tort borné à constater que les attestations du ministère de l'intérieur en date des 18 février et 21 juin 2020 indiquaient qu'il était inscrit sur la liste électorale de la commune de Villetaneuse et était, par suite, éligible dans cette commune. Il résulte toutefois de l'instruction que Mme A... n'apporte pas d'éléments suffisamment probants de nature à établir que l'inscription de M. F... sur cette liste électorale revêt le caractère d'une manœuvre, alors que l'intéressé a fourni, pour justifier de son inscription sur cette liste, une attestation d'hébergement sur le territoire de la commune de Villetaneuse en date du 30 novembre 2018.<br/>
<br/>
              5. Aux termes de l'article L. 231 du code électoral : " (...) Les agents salariés communaux ne peuvent être élus au conseil municipal de la commune qui les emploie. (...). Aux termes du premier alinéa de l'article L. 237-1 du même code : " I. - Le mandat de conseiller municipal est incompatible avec l'exercice d'un emploi salarié au sein du centre communal d'action sociale de la commune ". Il résulte de ces dispositions que si l'exercice par un agent d'un emploi salarié au sein du centre communal d'action sociale de la commune d'élection est incompatible avec le mandat de conseiller municipal de cette commune, l'exercice d'un tel emploi ne le rend pas inéligible au conseil municipal de la commune. Il est constant que M. C... était agent du centre communal d'action sociale de Villetaneuse jusqu'à sa démission le 17 mars 2020. Dès lors et contrairement à ce que soutient Mme A..., il n'était pas inéligible au conseil municipal de la commune de Villetaneuse en application des dispositions précitées du code électoral.<br/>
<br/>
              En ce qui concerne la campagne et la propagande électorales :<br/>
<br/>
              6. Aux termes de l'article L. 48-1 du code électoral : " Les interdictions et restrictions prévues par le présent code en matière de propagande électorale sont applicables à tout message ayant le caractère de propagande électorale diffusé par tout moyen de communication au public par voie électronique ". Aux termes de l'article L. 48-2 du même code : " Il est interdit à tout candidat de porter à la connaissance du public un élément nouveau de polémique électorale à un moment tel que ses adversaires n'aient pas la possibilité d'y répondre utilement avant la fin de la campagne électorale. ". Aux termes de l'article L. 49 du même code dans sa rédaction applicable au litige : " A partir de la veille du scrutin à zéro heure, il est interdit de distribuer ou faire distribuer des bulletins, circulaires et autres documents. / A partir de la veille du scrutin à zéro heure, il est également interdit de diffuser ou de faire diffuser par tout moyen de communication au public par voie électronique tout message ayant le caractère de propagande électorale ".<br/>
<br/>
              7. En premier lieu, si Mme A... soutient que plusieurs manœuvres et irrégularités sont constitutives de violations des dispositions des articles L. 48-1 et L. 48-2 et L. 49 du code électoral et sont de nature à altérer la sincérité du scrutin au regard des huit voix ayant permis que l'élection soit acquise au premier tour, il résulte de l'instruction que les affiches diffusées par les associations " l'autre champ " et " le ver galant " dans des halls d'immeubles, bien qu'étant ouvertement hostiles à Mme A..., n'appelaient pas à voter pour la liste conduite par M. G... et ne comportaient pas des éléments nouveaux de polémique électorale, leur diffusion la vieille du scrutin n'étant au demeurant pas établie.<br/>
<br/>
              8. En deuxième lieu, si Mme A... fait valoir que quatre appels à voter pour la liste conduite par M. G... ont été diffusés sur les réseaux sociaux, elle n'établit, ni même n'allègue, que ces messages, dont il résulte de l'instruction que leur diffusion est restée limitée, auraient comporté un élément nouveau de polémique électorale.<br/>
<br/>
              9. En troisième lieu, si Mme A... soutient que des rumeurs liées à un cas de Covid-19 dans un collège de la commune de Villetaneuse ont dissuadé certains électeurs de se déplacer pour aller voter, elle n'établit pas que cette abstention, à la supposer même établie, aurait particulièrement affecté son électorat.<br/>
<br/>
              10. En quatrième lieu, si Mme A... soutient que la mobilisation des jeunes électeurs révèlerait des pressions effectuées sur ceux-ci, cette mobilisation ne peut, en tant que telle, révéler l'existence de telles pression. Par ailleurs, ses allégations selon lesquelles M. G... et ses colistiers ont versé des sommes d'argent en échange de leurs suffrages, ont fait des promesses d'emploi, de logement, d'essence gratuite à des électeurs et ont menacé des personnes de leur faire perdre leur emploi s'ils ne votaient pas pour leur liste, qui sont fondées sur des témoignages peu circonstanciés, ne sont corroborées par aucun élément probant.<br/>
<br/>
              En ce qui concerne le déroulement des opérations électorales :<br/>
<br/>
              11. Mme A... soutient que plusieurs irrégularités concernant le déroulement des opérations de dépouillement des votes ont eu une incidence sur la sincérité du scrutin. Toutefois, il résulte de l'instruction que la signature annulée par erreur au bureau n° 1 a bien été prise en compte, que la différence entre le nombre de votes figurant sur le compteur de l'urne et le nombre d'émargements au bureau n° 2 due à une mauvaise manipulation est restée sans incidence, puisque le nombre de bulletins trouvés dans l'urne est identique à celui des émargements et, enfin, que le bulletin de vote déchiré, qui l'a été par accident d'un scrutateur, pouvait être comptabilisé. Il résulte de ce qui précède que ces incidents n'ont eu aucune influence sur la sincérité du scrutin.<br/>
<br/>
              12. Par ailleurs, si Mme A... fait état de pressions exercées sur les électeurs au sein des bureaux de vote n° 4, n° 5 et n° 6, caractérisées par les circonstances que l'un des assesseurs désignés par M. G... aurait accueilli certains électeurs en déclarant : " G... ", qu'un deuxième assesseur aurait cherché à orienter le vote de jeunes électeurs et qu'un troisième assesseur faisait des clins d'œil, ces affirmations, qui ne s'appuient sur aucun fait précis, ne sont pas mentionnées au procès-verbal des opérations électorales de chacun des bureaux concernés. <br/>
<br/>
              En ce qui concerne le financement de la campagne électorale :<br/>
<br/>
              13. Aux termes du deuxième alinéa de l'article L. 52-8 du code électoral : " Les personnes morales, à l'exception des partis ou groupements politiques, ne peuvent participer au financement de la campagne électorale d'un candidat, ni en lui consentant des dons sous quelque forme que ce soit, ni en lui fournissant des biens, services ou autres avantages directs ou indirects à des prix inférieurs à ceux qui sont habituellement pratiqués ".<br/>
<br/>
              14. Si Mme A... soutient que M. G... a bénéficié du soutien financier de deux associations pour diffuser des affiches et pour l'organisation d'un goûter le 22 février 2020 et d'une réunion le 7 mars 2020 à l'occasion de laquelle une distribution de graines a été effectuée, il résulte toutefois de l'instruction que les affiches en cause n'appelaient pas à voter pour M. G... et que les deux événements ont été organisés dans l'intérêt propre des deux associations.<br/>
<br/>
              15. Par ailleurs, il résulte de l'instruction, notamment de la décision de la Commission nationale des comptes de campagne et des financements politiques en date du 1er octobre 2020 approuvant le compte de campagne de M. G..., que ce dernier a signé un contrat de location pour un local du 1er septembre 2019 au 31 mars 2020, pour un loyer de 280 euros par mois. Si Mme A... soutient que la mise à disposition de ce local par une entreprise commerciale n'aurait pas eu pour contrepartie le paiement du loyer mensuel fixé par le contrat et que M. G... aurait ainsi bénéficié d'un don de la part d'une personne morale, elle ne produit aucun élément de nature à étayer cette allégation. <br/>
<br/>
              16. Il résulte de tout ce qui précède, sans qu'il soit besoin d'examiner les fins de non-recevoir soulevées en défense, que Mme A... n'est pas fondée à se plaindre de ce que, par le jugement attaqué, le tribunal administratif de Montreuil a rejeté sa protestation tendant d'une part à l'annulation des opérations électorales qui se sont déroulées le 15 mars 2020 en vue de l'élection des conseillers municipaux et du conseiller communautaire de la commune de Villetaneuse et au rejet du compte de campagne de M. G....<br/>
<br/>
              17. Il résulte des points 3 à 6 et 14 à 16 que Mme A... n'est pas plus fondée à se plaindre de ce que le tribunal administratif de Montreuil a rejeté ses conclusions tendant à ce que soit prononcée l'inéligibilité de MM. F... et C....<br/>
<br/>
              Sur l'application de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              18. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de M. G... qui n'est pas, dans la présente instance, la partie perdante. Il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par M. G... sur le même fondement.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de Mme A... est rejetée.<br/>
Article 2 : Les conclusions présentées par M. G... sur le fondement de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La présente décision sera notifiée à Mme D... A... et à M. I... G.... <br/>
Copie en sera adressée la Commission nationale des comptes de campagne et des financements politiques et au ministre de l'intérieur.<br/>
              Délibéré à l'issue de la séance du 21 octobre 2021 où siégeaient : M. Stéphane Verclytte, conseiller d'Etat, présidant ; M. Christian Fournier, conseiller d'Etat et Mme Rose-Marie Abel, maître des requêtes en service extraordinaire-rapporteure. <br/>
<br/>
              Rendu le 22 novembre 2021.<br/>
<br/>
                 Le Président : <br/>
                 Signé : M. Stéphane Verclytte<br/>
 		La rapporteure : <br/>
      Signé : Mme Rose-Marie Abel<br/>
                 La secrétaire :<br/>
                 Signé : Mme E... H...<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
