<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000045118467</ID>
<ANCIEN_ID>JG_L_2022_02_000000439574</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/45/11/84/CETATEXT000045118467.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème chambre, 02/02/2022, 439574, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2022-02-02</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>439574</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SAS BOULLOCHE, COLIN, STOCLET ET ASSOCIÉS</AVOCATS>
<RAPPORTEUR>Mme Catherine Fischer-Hirtz</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Céline  Guibé</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2022:439574.20220202</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              L'Organisme de gestion de l'école catholique Ecole Sainte-Thérèse, ci-après désigné " OGEC Sainte Thérèse ", a demandé au tribunal administratif de Toulouse de prononcer la décharge de la cotisation de taxe d'habitation à laquelle il a été assujetti au titre de l'année 2017 pour un montant restant en litige de 2 129 euros. Par un jugement n° 1800583 du 14 janvier 2020, le tribunal administratif de Toulouse a rejeté sa demande.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 16 mars et 24 août 2020 et 9 septembre 2021 au secrétariat du contentieux du Conseil d'Etat, l'OGEC Sainte-Thérèse demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - le décret n° 2012-1246 du 7 novembre 2012 ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Catherine Fischer-Hirtz, conseillère d'Etat,  <br/>
<br/>
              - les conclusions de Mme Céline Guibé, rapporteure publique ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SAS Boulloche, Colin, Stoclet et Associés, avocat de l'Organisme de gestion de l'école catholique Ecole Sainte-Thérèse ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis au juge du fond que l'organisme de gestion de l'école catholique Ecole Sainte-Thérèse, ci-après désigné " OGEC Sainte-Thérèse ", a été assujetti à la taxe d'habitation dans les rôles de la commune de Saint-Gaudens au titre de l'année 2017 à raison de l'ensemble immobilier qu'il y exploite. Par un jugement du 14 janvier 2020, le tribunal administratif de Toulouse a rejeté la demande de l'OGEC Sainte-Thérèse tendant à la décharge de l'imposition laissée à sa charge. Il se pourvoit en cassation contre ce jugement.<br/>
<br/>
              2. D'une part, aux termes de l'article 1658 du code général des impôts : " Les impôts directs (...) sont recouvrés en vertu de rôles rendus exécutoires par arrêté du préfet. ". Ces rôles doivent comporter l'identification du contribuable, ainsi que le total par nature d'impôt et par année des sommes à acquitter. Ils ne sont en revanche pas soumis à l'obligation d'indiquer les bases de la liquidation imposée par les dispositions de l'article 24 du décret du 7 novembre 2012 relatif à la gestion budgétaire et comptable publique, qui ne portent que sur les créances non fiscales. <br/>
<br/>
              3. D'autre part, en vertu de l'article L. 104 du livre des procédures fiscales, les comptables chargés du recouvrement des impôts directs délivrent aux personnes qui en font la demande un extrait de rôle.<br/>
<br/>
              4. Il ressort des pièces du dossier soumis au juge du fond que l'OGEC Sainte-Thérèse a soutenu qu'ayant vainement demandé, sur le fondement de l'article L. 104 du livre des procédures fiscales, communication des rôles relatifs aux impositions litigieuses, il appartenait à l'administration d'établir devant le tribunal qu'ils " revêtent les mentions requises par la jurisprudence et par l'article 24 du décret du 7 novembre 2012 et qui conditionnent leur régularité (...) ". Par suite, après avoir écarté comme inopérant le moyen tiré de l'absence de mention des bases de liquidation, le tribunal administratif a commis une erreur de droit en s'abstenant de rechercher si les autres mentions visées au point 2 figuraient sur les rôles précités au seul motif que " le requérant ne soutient ni même n'allègue que le rôle litigieux serait dépourvu des mentions qui doivent y figurer ". Dès lors et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, son jugement doit être annulé. <br/>
<br/>
              5. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 1 000 euros à verser à l'OGEC Sainte-Thérèse au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du tribunal administratif de Toulouse du 14 janvier 2020 est annulé.<br/>
Article 2 : L'affaire est renvoyée au tribunal administratif de Toulouse. <br/>
Article 3 : L'Etat versera la somme de 1 000 euros à l'OGEC Sainte-Thérèse au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à l'organisme de gestion de l'école catholique Ecole Sainte-Thérèse et au ministre de l'économie, des finances et de la relance.<br/>
<br/>
              Délibéré à l'issue de la séance du 13 janvier 2022 où siégeaient : M. Frédéric Aladjidi, président de chambre, présidant ; M. Lionel Ferreira, maître des requêtes en service extraordinaire et Mme Catherine Fischer-Hirtz, conseillère d'Etat-rapporteure. <br/>
<br/>
              Rendu le 2 février 2022.<br/>
<br/>
<br/>
                 Le président : <br/>
                 Signé : M. Frédéric Aladjidi<br/>
 		La rapporteure : <br/>
      Signé : Mme Catherine Fischer-Hirtz<br/>
                 La secrétaire :<br/>
                 Signé : Mme B... A...<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
