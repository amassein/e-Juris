<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037089186</ID>
<ANCIEN_ID>JG_L_2018_06_000000411046</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/08/91/CETATEXT000037089186.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème et 6ème chambres réunies, 20/06/2018, 411046, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-06-20</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>411046</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème et 6ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DELAMARRE, JEHANNIN ; SCP GARREAU, BAUER-VIOLAS, FESCHOTTE-DESBOIS</AVOCATS>
<RAPPORTEUR>M. Jean-Dominique Langlais</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:411046.20180620</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme B...A...a demandé au tribunal administratif de Paris de condamner l'Etat à lui verser la somme de 10 000 euros en réparation du harcèlement moral qu'elle estime avoir subi et la somme de 1 000 euros en réparation des illégalités que l'administration aurait commises dans la gestion de sa carrière. Par un jugement n° 1410021/5-1 du 7 avril 2016, le tribunal administratif a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 16PA01614 du 28 mars 2017, la cour administrative d'appel de Paris a, sur la requête de MmeA..., annulé ce jugement en tant qu'il rejetait ses conclusions relatives à la réparation des préjudices ayant résulté pour elle de son harcèlement moral et condamné l'Etat à lui verser à ce titre la somme de 10 000 euros. <br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un nouveau mémoire, enregistrés les 30 mai, 30 août et 8 novembre 2017 au secrétariat du contentieux du Conseil d'Etat, le ministre de l'intérieur demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de MmeA....<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
<br/>
              - la loi n° 83-634 du 13 juillet 1983 ;<br/>
<br/>
              - la loi n° 84-16 du 11 janvier 1984 ;<br/>
<br/>
              - la loi n° 84-53 du 26 janvier 1984 ;<br/>
<br/>
              - le décret n° 87-602 du 30 juillet 1987 ;<br/>
<br/>
              - le décret n° 94-415 du 24 mai 1994 ;<br/>
<br/>
              - le code de justice administrative ; <br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Dominique Langlais, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Garreau, Bauer-Violas, Feschotte-Desbois, avocat de la Préfecture de police et à la SCP Delamarre, Jéhannin, avocat de MmeA....<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que MmeA..., agent de surveillance de Paris, a demandé au tribunal administratif de Paris de condamner l'Etat à l'indemniser des préjudices ayant résulté pour elle de faits de harcèlement moral et de décisions illégales dont elle aurait été victime dans le cadre de ses fonctions ; que le tribunal a rejeté cette demande par un jugement du 7 avril 2016 ; que le ministre de l'intérieur se pourvoit en cassation contre l'arrêt du 28 mars 2017 par lequel la cour administrative d'appel de Paris a annulé ce jugement en tant qu'il rejetait les conclusions de Mme A...relatives à la réparation des conséquences de son harcèlement moral et condamné l'Etat à lui verser à ce titre la somme de 10 000 euros ; <br/>
<br/>
              2. Considérant qu'aux termes de l'article 118 de la loi du 26 janvier 1984 : " I.- La commune et le département de Paris, ainsi que leurs établissements publics, disposent de fonctionnaires organisés en corps. Les personnels de ces collectivités et établissements sont soumis à un statut fixé par décret en Conseil d'Etat, qui peut déroger aux dispositions de la présente loi " ; qu'aux termes de l'article 2 du décret du 24 mai 1994 portant dispositions statutaires relatives aux personnels des administrations parisiennes pris pour l'application de ces dispositions : " Sont qualifiés de chefs des administrations parisiennes, au sens du présent décret, le maire de Paris, le président du conseil de Paris siégeant en formation de conseil départemental, le préfet de police pour les personnels placés sous son autorité et les présidents des établissements publics de la commune ou du département de Paris. / Le préfet de police est habilité à ester en justice pour les litiges concernant les personnels placés sous son autorité " ; qu'en vertu de l'article L. 532-1 du code de la sécurité intérieure, dans sa rédaction applicable au litige porté devant les juges du fond, les agents de surveillance de Paris, qui sont au nombre des agents des administrations parisiennes régis par les dispositions précitées, sont placés sous l'autorité du préfet de police ;<br/>
<br/>
              3. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que la demande indemnitaire que Mme A...a adressée au préfet de police portait sur des faits de harcèlement moral et sur des décisions illégales dont elle soutenait avoir été victime dans le cadre de ses fonctions, en particulier à l'occasion de son transfert à l'infirmerie psychiatrique de la préfecture de police et de son hospitalisation d'office, résultant de décisions des 8 et 9 septembre 2011 de son supérieur hiérarchique, le commissaire central du 10ème arrondissement, et du préfet de police, qui ont été annulées par le tribunal administratif de Paris, ainsi qu'à l'occasion de diverses décisions prises par l'administration sur ses demandes de reconnaissance de l'imputabilité au service de ses troubles d'anxiété réactionnels ; qu'elle invoquait ainsi des préjudices résultant d'agissements commis par l'administration dans le cadre de l'autorité hiérarchique exercée au nom du préfet de police en qualité de chef des administrations parisiennes et non d'autorité de l'Etat ; qu'il suit de là que seule la responsabilité de la ville de Paris pouvait être recherchée par l'intéressée ; qu'en faisant droit à ses conclusions dirigées contre l'Etat, la cour administrative d'appel a méconnu le principe selon lequel une personne morale de droit public ne saurait être condamnée à payer une somme qu'elle ne doit pas ; que, dans les circonstances particulières de l'espèce, s'agissant d'un recours indemnitaire faisant suite au rejet d'une réclamation préalable adressée au préfet de police, qui était compétent pour en connaître en tant qu'autorité de la ville de Paris, seule susceptible de voir sa responsabilité engagée, il appartenait aux juges du fond de regarder la demande dont ils étaient saisis comme dirigée contre cette collectivité publique et de l'appeler en la cause ;<br/>
<br/>
              4. Considérant qu'il résulte de ce qui précède que le ministre de l'intérieur est fondé à demander l'annulation de l'arrêt qu'il attaque ; <br/>
<br/>
              5. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à la charge de l'Etat, qui n'est pas, dans la présente instance, la partie perdante ; <br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 28 mars 2017 de la cour administrative d'appel de Paris est annulé.<br/>
<br/>
		Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Paris.<br/>
<br/>
Article 3 : Les conclusions présentées par Mme A...sur le fondement de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 4 : La présente décision sera notifiée au ministre d'Etat, ministre de l'intérieur et à Mme B... A.... <br/>
		Copie en sera adressée au préfet de police et à la ville de Paris.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
