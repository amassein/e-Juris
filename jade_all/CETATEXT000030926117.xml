<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030926117</ID>
<ANCIEN_ID>JG_L_2015_07_000000389779</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/92/61/CETATEXT000030926117.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème SSJS, 22/07/2015, 389779, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-07-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>389779</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème SSJS</FORMATION>
<TYPE_REC/>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MEIER-BOURDEAU, LECUYER ; SCP GASCHIGNARD</AVOCATS>
<RAPPORTEUR>M. Tristan Aureau</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Béatrice Bourgeois-Machureau</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2015:389779.20150722</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme B...A...a demandé au tribunal administratif de Paris, d'une part, d'annuler pour excès de pouvoir la décision du 15 septembre 2013 par laquelle le Centre national de la recherche scientifique (CNRS) a rejeté sa demande tendant à la communication des listes alphabétiques des bénéficiaires de la prime d'excellence scientifique pour 2009, 2010, 2011 et 2012 et, d'autre part, d'enjoindre au CNRS de lui communiquer ces listes dans un délai d'un mois suivant la décision à intervenir, sous astreinte. Par un jugement n° 1401940/5-3 du 4 mars 2015, le tribunal administratif a, d'une part, annulé pour excès de pouvoir la décision implicite refusant la communication et a, d'autre part, enjoint au CNRS de communiquer à Mme A...les listes en cause dans un délai de deux mois.<br/>
<br/>
              Par une requête, enregistrée le 27 avril 2015 au secrétariat du contentieux du Conseil d'Etat, le CNRS demande au Conseil d'Etat : <br/>
<br/>
              1°) d'ordonner qu'il soit sursis à l'exécution de ce jugement ;<br/>
<br/>
              2°) de mettre à la charge de Mme A...la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'éducation ;<br/>
              - la loi n° 78-753 du 17 juillet 1978 ;<br/>
              - le décret n° 2009-851 du 8 juillet 2009 ; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Tristan Aureau, auditeur, <br/>
<br/>
              - les conclusions de Mme Béatrice Bourgeois-Machureau, rapporteur public,<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Meier-Bourdeau, Lecuyer, avocat du Centre national de la recherche scientifique, et à la SCP Gaschignard, avocat de Mme A...; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1.	Considérant qu'aux termes du premier alinéa de l'article R. 821-5 du code de justice administrative : " La formation de jugement peut, à la demande de l'auteur du pourvoi, ordonner qu'il soit sursis à l'exécution d'une décision juridictionnelle rendue en dernier ressort si cette décision risque d'entraîner des conséquences difficilement réparables et si les moyens invoqués paraissent, en l'état de l'instruction, sérieux et de nature à justifier, outre l'annulation de la décision juridictionnelle, l'infirmation de la solution retenue par les juges du fond " ;<br/>
<br/>
              2.	Considérant, d'une part, que l'exécution du jugement attaqué implique la communication à Mme A...des documents dont le refus de communication constitue l'objet même du litige ; que cette communication revêtirait un caractère irréversible ;<br/>
<br/>
              3.	Considérant, d'autre part, que le moyen tiré de ce que le tribunal administratif a commis une erreur de droit, en estimant que la reconnaissance de la qualité du travail des personnels dont l'activité scientifique est jugée d'un niveau élevé et des personnels apportant une contribution exceptionnelle à la recherche n'était pas de nature à révéler une appréciation ou un jugement de valeur au sens du II de l'article 6 de la loi du 17 juillet 1978, paraît, en l'état de l'instruction, sérieux et de nature à justifier, outre l'annulation du jugement attaqué, l'infirmation de la solution retenue par les juges du fond ;<br/>
<br/>
              4.	Considérant que, dans ces conditions, il y a lieu d'ordonner le sursis à exécution du jugement du tribunal administratif de Paris du 4 mars 2015, jusqu'à ce qu'il soit statué sur les conclusions du pourvoi du Centre national de la recherche scientifique ; <br/>
<br/>
              5.	Considérant qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par le Centre national de la recherche scientifique au titre des dispositions de l'article L. 761-1 du code de justice administrative ; que les dispositions de cet article font obstacle à ce que soit accueillie la demande présentée au même titre par Mme A... ; <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : Jusqu'à ce qu'il ait été statué sur le pourvoi du Centre national de la recherche scientifique dirigé contre le jugement du 4 mars 2015 du tribunal administratif de Paris, il sera sursis à l'exécution de ce jugement.<br/>
<br/>
Article 2 : Les conclusions du Centre national de la recherche scientifique présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 3 : Les conclusions de Mme A...présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 4 : La présente décision sera notifiée au Centre national de la recherche scientifique et à Mme B...A....<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
