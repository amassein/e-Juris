<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028770811</ID>
<ANCIEN_ID>JG_L_2014_03_000000356141</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/77/08/CETATEXT000028770811.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère et 6ème sous-sections réunies, 24/03/2014, 356141, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-03-24</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>356141</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère et 6ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Julia Beurton</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Alexandre Lallet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:356141.20140324</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu 1°, sous le n° 356141, la requête, enregistrée le 25 janvier 2012 au secrétariat du contentieux du Conseil d'Etat, présentée par la fédération des associations régionales des allocataires de la Caisse autonome de retraite des médecins de France (CARMF), dont le siège est 79, rue de Tocqueville à Paris (75017), représentée par son président ; la fédération des associations régionales des allocataires de la CARMF demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret n° 2011-1644 du 25 novembre 2011 relatif au régime des prestations complémentaires de vieillesse des médecins libéraux prévu à l'article L. 645-1 du code de la sécurité sociale ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu 2°, sous le n° 356149, la requête sommaire et le mémoire complémentaire, enregistrés les 25 janvier et 24 avril 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour la Caisse autonome de retraite des médecins de France (CARMF), dont le siège est situé 46, rue Saint-Ferdinand à Paris (75841 cedex), représentée par son directeur, M. E... G..., demeurant..., M. C... F..., demeurant..., M. D... H..., demeurant ... et M. A...I..., demeurant... ; la Caisse autonome de retraite des médecins de France et autres demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret n° 2011-1644 du 25 novembre 2011 relatif au régime des prestations complémentaires de vieillesse des médecins libéraux prévu à l'article L. 645-1 du code de la sécurité sociale ou, subsidiairement, son article 4 ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat les dépens, y compris la contribution pour l'aide juridique mentionnée à l'article R. 761-1 du code de justice administrative ;<br/>
<br/>
			....................................................................................<br/>
<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu la Constitution, notamment son Préambule ;<br/>
<br/>
              Vu la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales et son premier protocole additionnel ;<br/>
<br/>
              Vu le code de la sécurité sociale ;<br/>
<br/>
              Vu la loi n° 2005-1579 du 19 décembre 2005 ;<br/>
<br/>
              Vu la loi n° 2012-1404 du 17 décembre 2012 ;<br/>
<br/>
              Vu la décision du 13 février 2013 par laquelle le Conseil d'Etat statuant au contentieux n'a pas renvoyé au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée pour la Caisse autonome de retraite des médecins de France et autres ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Julia Beurton, Auditeur,  <br/>
<br/>
              - les conclusions de M. Alexandre Lallet, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Foussard, avocat de la Caisse autonome de retraite des médecins de France et autres ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'en vertu de l'article L. 645-1 du code de la sécurité sociale, les médecins ayant exercé, pendant une certaine durée, une activité professionnelle non salariée dans le cadre de la convention organisant les rapports entre les organismes d'assurance maladie et les médecins bénéficient d'un régime de prestations complémentaires de vieillesse ; que les requêtes de la fédération des associations régionales des allocataires de la Caisse autonome de retraite des médecins de France (CARMF) et de la Caisse autonome de retraite des médecins de France et autres sont dirigées contre le même décret, relatif à ce régime ; qu'il y a lieu de les joindre pour statuer par une seule décision ;<br/>
<br/>
              Sur les interventions :<br/>
<br/>
              2. Considérant que la fédération française des médecins généralistes a intérêt au maintien du décret attaqué ; qu'ainsi son intervention est recevable ;<br/>
<br/>
              3. Considérant que MmeB..., médecin dont la retraite a été liquidée le 1er octobre 2011, a intérêt à l'annulation du décret attaqué ; qu'ainsi son intervention est recevable ;<br/>
<br/>
              Sur la régularité de la procédure :<br/>
<br/>
              4. Considérant qu'en vertu de l'article L. 645-3 du code de la sécurité sociale, une cotisation d'ajustement peut être appelée pour le régime de prestations complémentaires de vieillesse des médecins libéraux ; que cet article dispose que : " tout ou partie de cette cotisation peut ouvrir droit à des points supplémentaires dans des conditions fixées par décret, après avis des sections professionnelles des régimes mentionnés à l'article L. 645-1 (...) " ; qu'il est constant que le conseil d'administration de la Caisse autonome de retraite des médecins de France a été consulté sur le décret attaqué, notamment sur son article 3 pris pour l'application de ces dispositions, avant sa signature ; que, contrairement à ce que soutient la CARMF, il ne résulte pas de la seule circonstance que le ministre du travail, de l'emploi et de la santé n'a pas répondu aux objections soulevées par l'avis ainsi rendu que le Gouvernement n'en aurait pas pris connaissance avant d'adopter le décret attaqué ; que, par suite, le moyen tiré de ce que le décret attaqué aurait été pris au terme d'une procédure irrégulière doit être écarté ;<br/>
<br/>
              Sur la compétence de l'auteur du décret et sur le moyen tiré de la méconnaissance de l'article L. 645-5 du code de la sécurité sociale :<br/>
<br/>
              5. Considérant qu'aux termes de l'article L. 645-5 du code de la sécurité sociale, dans sa rédaction issue de la loi du 19 décembre 2005 de financement de la sécurité sociale pour 2006 : " La valeur de service du point de retraite pour les prestations de droit direct et les pensions de réversion liquidées antérieurement au 1er janvier 2006 est fixée par décret pour chacun des régimes. / Les points non liquidés et acquis antérieurement au 1er janvier 2006 ouvrent droit à un montant annuel de pension égal à la somme des produits du nombre de points acquis chaque année par une valeur de service du point. Cette valeur, fixée par décret, peut varier selon l'année durant laquelle les points ont été acquis et selon l'année de liquidation de la pension. / Les points acquis à compter du 1er janvier 2006 ouvrent droit à un montant annuel de pension égal au produit du nombre de points portés au compte de l'intéressé par la valeur de service du point. Cette valeur de service est fixée par décret " ; que la loi du 17 décembre 2012 de financement de la sécurité sociale pour 2013 a complété cet article par un alinéa ainsi rédigé : " Pour l'application du premier alinéa, une valeur de service plus favorable peut être prévue par décret pour les pensions de réversion, au titre d'un nombre de points n'excédant pas un seuil défini par décret " ;<br/>
<br/>
              6. Considérant, en premier lieu, que l'article L. 645-5 du code de la sécurité sociale, dans sa rédaction issue de la loi de financement de la sécurité sociale pour 2006, a prévu la possibilité de distinguer la valeur de service du point selon que les points ont été acquis et liquidés antérieurement au 1er janvier 2006, acquis mais non encore liquidés au 1er janvier 2006 ou acquis à compter du 1er janvier 2006 et, pour les points acquis mais non encore liquidés au 1er janvier 2006, en fonction notamment de l'année de liquidation de la pension ; qu'en tant qu'il fixe des valeurs de point différentes selon que les points acquis antérieurement au 1er janvier 2006 ont été liquidés avant le 1er janvier 2011 ou à compter de cette date, l'article 4 du décret attaqué n'est ainsi entaché d'aucune incompétence ni d'aucune méconnaissance des dispositions précitées de l'article L. 645-5 du code de la sécurité sociale ; <br/>
<br/>
              7. Considérant, en second lieu, que la fixation de la valeur de service du point de retraite ne touche à aucun principe fondamental du droit de la sécurité sociale dont l'article 34 de la Constitution réserve à la loi la détermination et ne relève pas du champ de la loi de financement de la sécurité sociale tel qu'il est défini par l'article LO 111-3 du code de la sécurité sociale ; qu'il résulte, par ailleurs, des dispositions citées au point 5 de la loi de financement de la sécurité sociale pour 2013, éclairée par les travaux préparatoires à son adoption, que le législateur a entendu expliciter la portée des distinctions qu'il avait envisagées en modifiant l'article L. 645-5 du code de la sécurité sociale par la loi du 19 décembre 2005 de financement de la sécurité sociale pour 2006 en précisant qu'il n'avait pas entendu alors exclure qu'une différence dans la valeur de service du point de retraite entre les prestations de droit direct et les pensions de réversion soit prévue par décret ; que, par suite, les moyens tirés de ce que l'article 5 du décret attaqué, qui fixe une valeur de service plus élevée pour les trois cents premiers points des pensions de réversion en cas de liquidation antérieurement au 1er janvier 2006, serait entaché d'incompétence et méconnaîtrait les dispositions de l'article L. 645-5 du code de la sécurité sociale doivent être écartés ;<br/>
<br/>
              Sur les moyens tirés de la méconnaissance de la garantie des droits et des principes de non-rétroactivité et " d'intangibilité " des droits liquidés :<br/>
<br/>
              8. Considérant, en premier lieu, que les dispositions contestées, qui modifient, à compter du 1er janvier 2012, la valeur de service du point de retraite applicable aux pensions, fussent-elles déjà liquidées, sont sans effet sur les arrérages servis avant leur publication ;<br/>
<br/>
              9. Considérant, en deuxième lieu, que si le décret attaqué ne pouvait légalement remettre en cause, dans un régime tel que celui des prestations complémentaires de vieillesse des médecins libéraux, le nombre des points acquis par les participants dont la retraite a déjà été liquidée, il lui appartenait en revanche de déterminer la valeur du point de façon, notamment, à assurer l'équilibre financier du régime ; que, par suite, la diminution de la valeur de service du point pour les pensions liquidées ne porte pas atteinte à des situations légalement acquises ; <br/>
<br/>
              10. Considérant, en troisième lieu, que le décret attaqué ne portant pas atteinte aux situations légalement acquises, le moyen tiré de la méconnaissance de la garantie des droits proclamée par l'article 16 de la Déclaration de 1789 doit également être écarté ;<br/>
<br/>
              11. Considérant, en dernier lieu, que les requérants ne sont, en tout état de cause, pas fondés à se prévaloir des dispositions de l'article R. 351-10 du code de la sécurité sociale, qui n'est pas applicable au régime des prestations complémentaires de vieillesse des médecins libéraux ;<br/>
<br/>
              Sur le moyen tiré de la méconnaissance de l'article 1er du premier protocole additionnel à la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales :<br/>
<br/>
              12. Considérant qu'aux termes de cet article : " Toute personne physique ou morale a droit au respect de ses biens. Nul ne peut être privé de sa propriété que pour cause d'utilité publique et dans les conditions prévues par la loi et les principes généraux du droit international. / Les dispositions précédentes ne portent pas atteinte au droit que possèdent les Etats de mettre en vigueur les lois qu'ils jugent nécessaires pour réglementer l'usage des biens conformément à l'intérêt général (...) " ;<br/>
<br/>
              13. Considérant que si les droits découlant du versement de cotisations à des régimes de retraite doivent être regardés comme des droits patrimoniaux au sens de ces stipulations, celles-ci ne sont pas méconnues lorsque les personnes ayant cotisé à un régime de retraite subissent une réduction raisonnable et proportionnée de leurs droits ;<br/>
<br/>
              14. Considérant que le décret attaqué fait passer la valeur de service du point de 15,55 à 14 ou à 13 euros, selon la date d'acquisition des points et la date de liquidation de la retraite ; que cette diminution, qui, même en prenant en compte les modalités d'indexation, ne peut être regardée comme portant atteinte à la substance même du droit à pension, était justifiée par l'objectif d'assurer la pérennité d'un régime dont l'équilibre financier était gravement compromis et correspond au choix fait, dans un souci d'équité entre les générations, de répartir l'effort nécessaire à son redressement entre les actifs et les pensionnés à hauteur respectivement des deux tiers et d'un tiers ; que la circonstance que le pouvoir réglementaire aurait pu prendre des mesures plus limitées si elles étaient intervenues plus tôt est sans incidence sur la légalité des mesures adoptées par le décret attaqué, au vu des prévisions disponibles en 2011 ; que si les requérants font en outre valoir que le régime a été conçu comme une contrepartie à la limitation des honoraires des médecins, la détermination de la valeur du point par le décret attaqué prend, en tout état de cause, en considération la participation de l'assurance maladie au financement du régime, par la prise en charge des deux tiers des cotisations des médecins exerçant en secteur à honoraires opposables ;<br/>
<br/>
              15. Considérant qu'il suit de là que la diminution de la valeur de service du point qu'opère le décret attaqué ne méconnaît pas l'article 1er du premier protocole additionnel à la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
<br/>
              Sur les moyens tirés de la méconnaissance du principe d'égalité et des stipulations des articles 14 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales et 1er de son premier protocole additionnel :<br/>
<br/>
              16. Considérant, en premier lieu, que le décret attaqué prévoit, à son article 4, que la valeur de service du point est réduite de 15,55 à 14 euros pour les points acquis antérieurement au 1er janvier 2006 et liquidés avant le 1er janvier 2011, tandis que cette valeur est réduite de 15,55 à 13 euros pour les points acquis à compter du 1er janvier 2006 ou acquis antérieurement mais liquidés à compter du 1er janvier 2011 ; que la distinction opérée selon que les points ont été acquis avant ou après le 1er janvier 2006 résulte directement des dispositions de l'article L. 645-5 du code de la sécurité sociale, dont la compatibilité avec la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales n'est pas contestée ; qu'il ne ressort pas des pièces du dossier que la distinction opérée selon que les points ont été liquidés avant ou après le 1er janvier 2011, dont le législateur avait ouvert la possibilité, ne présenterait pas un rapport raisonnable de proportionnalité entre la différence de traitement qui en résulte et l'objectif poursuivi ; <br/>
<br/>
              17. Considérant, en second lieu, que le décret attaqué prévoit, à son article 5, que, pour les pensions liquidées avant le 1er janvier 2006, la valeur de service reste égale à 15,55 euros pour les trois cents premiers points des pensions de réversion ; que la distinction ainsi faite entre les prestations de droit direct et les pensions de réversion et entre les pensions de réversion selon la date de leur liquidation, d'une part, répond à l'objectif légitime de limiter la baisse de la valeur de service pour les pensions de réversion, dont le montant n'est égal qu'à 50 % du montant d'une prestation de droit direct et prend en compte la date du 1er janvier 2006 retenue par la loi elle-même ; que, d'autre part, en choisissant le seuil de trois cents points, qui correspond au montant moyen des pensions de réversion, le pouvoir réglementaire a légitimement entendu permettre que cette mesure, tout en bénéficiant à l'ensemble des pensions de réversion, en cas de liquidation avant le 1er janvier 2006, préserve le niveau des pensions de réversion les plus modestes ; que les différences de traitement qui en résultent sont en rapport direct avec les objectifs poursuivis ; qu'il ne ressort pas des pièces du dossier qu'elles seraient manifestement disproportionnées au regard de ces objectifs ;<br/>
<br/>
              18. Considérant, enfin, que le moyen tiré de la différence de traitement qui résulterait du décret attaqué entre le régime des médecins et les régimes applicables aux autres professions n'est, en tout état de cause, pas assorti des précisions permettant d'en apprécier le bien-fondé ;<br/>
<br/>
              19. Considérant qu'il résulte de tout ce qui précède que les requérants ne sont pas fondés à demander l'annulation du décret qu'ils attaquent ; que, par voie de conséquence, leurs conclusions présentées au titre de l'article L. 761-1 du code de justice administrative doivent être également rejetées ; qu'il n'y pas lieu de faire droit à la demande de la CARMF et autres de mettre à la charge de l'Etat la contribution pour l'aide juridictionnelle ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Les interventions de la fédération française des médecins généralistes et de Mme B... sont admises.<br/>
Article 2 : Les requêtes de la fédération des associations régionales des allocataires de la CARMF et de la Caisse autonome de retraite des médecins de France et autres sont rejetées.<br/>
Article 3 : La contribution pour l'aide juridique est, sous le n° 356149, laissée à la charge de la Caisse autonome de retraite des médecins de France et autres.<br/>
Article 4 : La présente décision sera notifiée à la fédération des associations régionales des allocataires de la Caisse autonome de retraite des médecins de France, à la Caisse autonome de retraite des médecins de France, premier requérant dénommé pour la requête n° 356149, au Premier ministre, à la ministre des affaires sociales et de la santé, à la fédération française des médecins généralistes et à Mme J...B....<br/>
Les autres requérants ayant présenté la requête n° 356149 seront informés de la présente décision par Me Foussard, avocat au Conseil d'Etat et à la Cour de cassation, qui les représente devant le Conseil d'Etat.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
