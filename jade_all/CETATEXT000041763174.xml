<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000041763174</ID>
<ANCIEN_ID>JG_L_2020_03_000000429436</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/41/76/31/CETATEXT000041763174.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème - 2ème chambres réunies, 27/03/2020, 429436, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-03-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>429436</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème - 2ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GASCHIGNARD</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT>Mme Mireille Le Corre</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:429436.20200327</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme A... B... a demandé au tribunal administratif de Lyon d'annuler la décision implicite de rejet née du silence gardé par le ministre de la défense sur sa demande tendant à la prise en compte, pour son reclassement dans le corps des techniciens paramédicaux civils du ministère de la défense, de son ancienneté à compter de la date d'obtention de son brevet de préparateur en pharmacie obtenu le 5 juillet 2001. Par un jugement n° 1407661 du 24 mai 2017, le tribunal administratif de Lyon a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 17LY02782 du 4 février 2019, la cour administrative d'appel de Lyon a rejeté l'appel formé par Mme B... contre ce jugement. <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 4 avril et 5 juillet 2019 au secrétariat du contentieux du Conseil d'Etat, Mme B... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.  <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de la santé publique ;<br/>
              - le décret n° 2013-974 du 30 octobre 2013 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean Sirinelli, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Mireille Le Corre, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Gaschignard, avocat de Mme B... ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que Mme B... a été admise au concours sur titres des techniciens paramédicaux civils du ministère de la défense organisé au titre de l'année 2013, après avoir obtenu la même année le diplôme de préparateur en pharmacie hospitalière. Elle a été nommée technicienne paramédicale civile de classe normale stagiaire du ministère de la défense dans la spécialité de préparateur de pharmacie hospitalière par un arrêté du 15 novembre 2013. Par décision du 31 mars 2014, elle a été reclassée dans ce corps au 2ème échelon avec une ancienneté conservée de neuf mois et quinze jours. Le 20 avril 2014, elle a exercé un recours gracieux contre cette décision en demandant la prise en compte, pour son reclassement dans ce corps, des services antérieurs accomplis en qualité de préparateur en pharmacie depuis le 5 juillet 2001, date à laquelle elle a obtenu le brevet de préparateur en pharmacie. Par un jugement du 24 mai 2017, le tribunal administratif de Lyon a rejeté sa demande tendant à l'annulation de la décision implicite de rejet de ce recours. Par l'arrêt attaqué du 4 février 2019, la cour administrative d'appel de Lyon a rejeté l'appel formé par Mme B... contre ce jugement.<br/>
<br/>
              2. D'une part, selon l'article 2 du décret du 30 octobre 2013 portant statut particulier du corps des techniciens paramédicaux civils du ministère de la défense, le corps des techniciens paramédicaux civils du ministère de la défense " regroupe le personnel civil du ministère de la défense possédant les titres ou diplômes requis pour pouvoir exercer dans l'une des spécialités suivantes : / (...) / 8° Préparateur en pharmacie hospitalière ". Aux termes de l'article 10 du même décret : " I. - Les agents qui, à la date de leur nomination dans le corps des techniciens paramédicaux civils du ministère de la défense, justifient de services ou d'activités professionnelles accomplis, suivant le cas en qualité de fonctionnaire, de militaire ou d'agent public non titulaire ou en qualité de salarié, de bénévole, dans des fonctions correspondant à la spécialité dans laquelle ils sont nommés, sous réserve qu'ils justifient aussi de la détention des titres de formation, diplômes ou autorisations exigés pour l'exercice de ces fonctions, sont classés dans le grade de technicien paramédical civil de classe normale en prenant en compte la totalité de cette durée de services ou d'activités professionnelles. / II. - Les services ou activités professionnelles mentionnés au premier alinéa doivent avoir été accomplis dans les établissements ci-après : / (...) / 5° Pharmacie d'officine ".<br/>
<br/>
              3. D'autre part, aux termes de l'article L. 4241-13 du code de la santé publique : " Les préparateurs en pharmacie hospitalière sont autorisés à seconder le pharmacien chargé de la gérance de la pharmacie à usage intérieur ainsi que les pharmaciens qui l'assistent, en ce qui concerne la gestion, l'approvisionnement, la délivrance et la préparation des médicaments, produits et objets mentionnés à l'article L. 4211-1 ainsi que des dispositifs médicaux stériles. Ils exercent leurs fonctions sous la responsabilité et le contrôle effectif d'un pharmacien ". Aux termes de l'article L. 4241-1 du même code : " Les préparateurs en pharmacie sont seuls autorisés à seconder le titulaire de l'officine et les pharmaciens qui l'assistent dans la préparation et la délivrance au public des médicaments destinés à la médecine humaine et à la médecine vétérinaire (...) ". <br/>
<br/>
              4. Il résulte des dispositions citées aux points précédents que si le reclassement dans le corps des techniciens paramédicaux civils du ministère de la défense est subordonné à la condition que les agents justifient des titres de formations, diplômes ou autorisation exigés pour l'exercice des fonctions correspondant à la spécialité dans laquelle ils sont nommés, en l'occurrence le diplôme de préparateur en pharmacie hospitalière pour Mme B..., l'article 10 du décret du 30 octobre 2013 a entendu permettre, en application de son II, la prise en compte des services ou activités professionnelles accomplis dans des fonctions de préparateur en pharmacie d'officine pour le classement des agents dans le corps de technicien paramédical civil du ministère de la défense.<br/>
<br/>
              5. Il ressort des énonciations de l'arrêt attaqué que la cour administrative d'appel de Lyon a estimé que pour le reclassement dans le corps des techniciens paramédicaux du ministère de la défense dans la spécialité de préparateur en pharmacie hospitalière, il doit être tenu compte des seuls services accomplis en qualité de préparateur en pharmacie hospitalière, à l'exclusion des services ou activités professionnelles accomplis en qualité de préparateur en pharmacie d'officine. Il résulte de ce qui a été dit au point précédent qu'en rejetant, pour ce motif, la demande de Mme B... tendant à ce que soient prises en compte, pour son reclassement dans ce corps, ses activités professionnelles exercées en qualité de préparatrice en pharmacie d'officine depuis le 5 juillet 2001, la cour a commis une erreur de droit.<br/>
<br/>
              6. Il résulte de ce qui précède que Mme B... est fondée à demander l'annulation de l'arrêt attaqué.<br/>
<br/>
              7. Il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative.<br/>
<br/>
              8. Aux termes de l'article 38 du décret du 19 décembre 1991 portant application de la loi n° 91-647 du 10 juillet 1991 relative à l'aide juridique dans sa version applicable : " Lorsqu'une action en justice doit être intentée avant l'expiration d'un délai devant la juridiction du premier degré (...) l'action est réputée avoir été intentée dans le délai si la demande d'aide juridictionnelle s'y rapportant est adressée au bureau d'aide juridictionnelle avant l'expiration dudit délai et si la demande en justice est introduite dans un nouveau délai de même durée à compter (...) c) De la date à laquelle la décision d'admission ou de rejet de la demande est devenue définitive ". Il ressort des pièces du dossier que Mme B... a présenté une demande d'aide juridictionnelle le 13 juin 2014 qui a été rejetée par une décision du bureau d'aide juridictionnelle notifiée le 8 juillet 2014. Par suite, le ministre de la défense n'est pas fondé à soutenir que la requête de Mme B... dirigée contre la décision implicite du ministre rejetant sa demande du 20 avril 2014, enregistrée au greffe du tribunal administratif le 9 septembre 2014, serait tardive.<br/>
<br/>
              9. Il résulte de ce qui a été dit au point 4 de la présente décision que le ministre de la défense a commis une erreur de droit en rejetant la demande de Mme B... tendant à ce que soit prise en compte les activités professionnelles qu'elle a exercées à compter du 5 juillet 2001 en qualité de préparateur de pharmacie en officine pour son reclassement dans le corps des techniciens paramédicaux civils du ministère de la défense. Par suite, Mme B... est fondée à soutenir que c'est à tort que le tribunal administratif de Lyon a, par son jugement du 24 mai 2017, rejeté sa demande tendant à l'annulation de ce refus. <br/>
<br/>
              10. Il y a lieu d'enjoindre au ministre de la défense de réexaminer la demande de reclassement de Mme B... conformément aux motifs de la présente décision dans un délai de trois mois à compter de sa notification.<br/>
<br/>
              11. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 5 000 euros à verser à Mme B..., pour l'ensemble de la procédure, au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 4 février 2019 de la cour administrative d'appel de Lyon et le jugement du 24 mai 2017 du tribunal administratif de Lyon sont annulés. <br/>
Article 2 : Le refus du ministre de la défense de prendre en compte les activités professionnelles de Mme B... exercées à compter du 5 juillet 2001 en qualité de préparateur de pharmacie en officine pour son reclassement dans le corps des techniciens paramédicaux civils du ministère de la défense est annulée.<br/>
Article 3 : Il est enjoint au ministre de la défense de réexaminer la demande de reclassement de Mme B... dans les trois mois à compter de la notification de la présente décision.<br/>
Article 4 : L'Etat versera à Mme B... une somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 5 : La présente décision sera notifiée à Mme A... B... et à la ministre des armées.<br/>
Copie en sera adressée à la ministre des solidarités et de la santé.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
