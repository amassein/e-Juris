<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038384730</ID>
<ANCIEN_ID>JG_L_2019_04_000000420876</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/38/47/CETATEXT000038384730.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème - 3ème chambres réunies, 16/04/2019, 420876, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-04-16</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>420876</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème - 3ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GASCHIGNARD ; BALAT ; SCP DE NERVO, POUPET</AVOCATS>
<RAPPORTEUR>M. Olivier Gariazzo</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Romain Victor</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:420876.20190416</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Procédure contentieuse antérieure :<br/>
<br/>
              Les sociétés Procedim et Sinfimmo ont demandé au tribunal administratif de Grenoble, d'une part, d'annuler les décisions des 22 décembre 2010 et 26 mai 2011 du directeur du centre hospitalier spécialisé (CHS) de la Savoie rejetant leur offre d'achat de parcelles de terrain situées sur le territoire de la commune de Bassens pour les céder à la société CIS Promotion ainsi que leur recours gracieux contre cette décision de rejet et, d'autre part, d'enjoindre au directeur du CHS de la Savoie de résoudre la vente dans le délai d'un mois à compter de la notification du jugement à intervenir et de leur vendre les terrains concernés. Par un jugement n° 1104072 du 21 janvier 2014, le tribunal a rejeté ces demandes.<br/>
<br/>
              Par un arrêt n° 14LY00915 du 19 mars 2015, la cour administrative d'appel de Lyon a rejeté l'appel formé contre ce jugement par les sociétés Procedim et Sinfimmo.<br/>
<br/>
              Par une décision n° 390347 du 27 mars 2017, le Conseil d'Etat, statuant au contentieux sur le pourvoi des sociétés Procedim et Sinfimmo, a annulé cet arrêt et renvoyé l'affaire à la cour administrative d'appel de Lyon.<br/>
<br/>
              Par un arrêt n° 17LY01413 du 22 mars 2018, la cour, statuant sur renvoi du Conseil d'Etat, a rejeté la requête présentée le 21 mars 2014 par les sociétés Procedim et Sinfimmo contre le jugement du 21 janvier 2014 du tribunal administratif de Grenoble.<br/>
<br/>
<br/>
              Procédure devant le Conseil d'Etat : <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 24 mai et 27 août 2018 au secrétariat du contentieux du Conseil d'Etat, la société Sinfimmo, agissant en son nom propre et venant aux droits de la société Procedim avec laquelle elle a fusionné, demande au Conseil d'État :<br/>
<br/>
              1°) d'annuler l'article 2 de cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond dans cette mesure, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge du centre hospitalier spécialisé de la Savoie une somme de 8 000 euros au titre de l'article L. 761-1 du code de justice administrative.  <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Olivier Gariazzo, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Romain Victor, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Gaschignard, avocat de la société Procedim  et de la société Sinfimmo  et à la SCP de Nervo, Poupet, avocat du centre hospitalier spécialisé de la Savoie  ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que le centre hospitalier spécialisé (CHS) de la Savoie a organisé au mois de janvier 2009 la mise en concurrence de professionnels de l'immobilier sélectionnés par ses soins en vue de céder un ensemble de terrains relevant de son domaine privé et situés sur le territoire de la commune de Bassens. Dans le cadre de cette procédure, un dossier de consultation leur a été adressé, invitant les candidats à proposer un prix d'acquisition ainsi qu'un projet d'aménagement immobilier tenant compte des contraintes locales d'urbanisme. Onze candidats ont présenté une offre et cinq d'entre eux ont été invités à présenter leur projet devant un jury en juin 2009. Par un courrier du 11 septembre 2009, les sociétés Procedim et Sinfimmo, qui avaient présenté un projet commun, ont été informées du rejet de leur offre, le centre hospitalier ayant décidé de retenir celle qu'avait présentée la société CIS Promotion. À la suite d'une réclamation des sociétés Procedim et Sinfimmo, invoquant l'irrégularité de la procédure suivie, la consultation a été déclarée infructueuse le 12 octobre 2009 par la commission d'appel d'offres du centre hospitalier spécialisé et une nouvelle consultation a été engagée auprès des cinq candidats déjà entendus, sur le fondement d'un cahier des charges modifié. À l'issue de cette seconde consultation, la société CIS Promotion a été retenue comme cessionnaire par une décision du directeur du CHS du 11 décembre 2009, laquelle a été retirée pour vice de forme puis reprise, le 22 décembre 2010, après concertation avec le directoire de l'établissement et avis de son conseil de surveillance. Les sociétés Procedim et Sinfimmo ont demandé au tribunal administratif de Grenoble  d'annuler, d'une part, la décision du 22 décembre 2010 retenant la société CIS Promotion comme cessionnaire et, d'autre part, la décision de rejet du recours gracieux qu'elles avaient formé contre celle-ci. Elles ont relevé appel du jugement par lequel le tribunal administratif de Grenoble a rejeté leur demande. La société Sinfimmo, agissant en son nom propre et venant aux droits de la société Procedim avec laquelle elle a fusionné, se pourvoit en cassation contre l'article 2 de l'arrêt du 22 mars 2018 par lequel la cour administrative d'appel de Lyon, sur renvoi après annulation d'un premier arrêt du 19 mars 2015 par une décision du Conseil d'Etat du 27 mars 2017, a rejeté à nouveau sa requête d'appel.<br/>
<br/>
              2. La société CIS promotion, cessionnaire des parcelles en litige, justifie d'un intérêt suffisant au maintien de l'article 2 de l'arrêt attaqué. Son intervention au soutien des conclusions du CHS de la Savoie est, par suite, recevable.<br/>
<br/>
              3. En premier lieu, il ressort des pièces du dossier soumis aux juges du fond que les sociétés Procedim et Sinfimmo soulevaient à l'appui de leur demande d'annulation de la décision du 22 décembre 2010, par la voie de l'exception, un moyen tiré de ce que la consultation initialement conduite en vue de la cession des parcelles en litige avait été illégalement déclarée infructueuse par la décision du 12 octobre 2009. Après avoir relevé, par une appréciation souveraine non entachée de dénaturation, qu'à la suite de cette décision, une nouvelle consultation avait été mise en oeuvre, sur la base d'un nouveau cahier des charges, au cours de laquelle les candidats avaient pu de nouveau soumettre des offres, la cour a pu, sans erreur de droit, juger que la décision attaquée du 22 décembre 2010 n'avait pas été prise pour l'application de la décision du 12 octobre 2009, qui n'en constituait pas non plus la base légale, et écarter pour ce motif comme inopérante l'exception d'illégalité ainsi soulevée devant elle.<br/>
<br/>
              4. En deuxième lieu, si la cour a, au prix d'une dénaturation des pièces du dossier, relevé qu'à l'issue de cette seconde phase de consultation, la procédure avait de nouveau été déclarée infructueuse puis intégralement reprise, alors qu'ainsi qu'il a été dit ci-dessus, la décision prise le 11 décembre 2009 a seulement été retirée pour être reprise le 22 décembre 2010, sans organisation d'une troisième phase de consultation, cette erreur, qui affecte un motif de l'arrêt attaqué qui n'est pas le support nécessaire du raisonnement tenu par la cour, est demeurée sans incidence sur son bien-fondé. <br/>
<br/>
              5. En troisième et dernier lieu, aucune disposition législative ou réglementaire n'impose à une personne morale de droit public autre que l'État de faire précéder la vente d'une dépendance de son domaine privé d'une mise en concurrence préalable. Toutefois, lorsqu'une telle personne publique fait le choix, sans y être contrainte, de céder un bien de son domaine privé par la voie d'un appel à projets comportant une mise en concurrence, elle est tenue de respecter le principe d'égalité de traitement entre les candidats au rachat de ce bien. Il ne saurait cependant en découler qu'elle devrait respecter  les règles relatives à la commande publique, qui ne sont pas applicables à la cession d'un bien.  <br/>
<br/>
              6. La cour administrative d'appel n'a pas commis d'erreur de droit en jugeant que la circonstance que le centre hospitalier ait refusé de faire droit à une demande de communication de l'offre présentée par le candidat retenu ainsi que des procès-verbaux de la réunion de la commission ayant examiné les offres n'était pas de nature, à elle seule, à établir que la procédure de consultation organisée en vue de la cession des parcelles en litige, qui prévoyait seulement la communication aux candidats d'un dossier présentant les contraintes liées à l'aménagement du terrain et la possibilité pour eux de présenter une offre, sans comporter la fixation d'aucune règle relative au processus de choix du cessionnaire, se serait déroulée en méconnaissance du principe d'égalité de traitement entre les candidats.<br/>
<br/>
              7. Il résulte de tout ce qui précède que le pourvoi de la société Sinfimmo doit être rejeté, y compris ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              8. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la société Sinfimmo le versement au CHS de la Savoie et à la société CIS Promotion d'une somme  globale, à parts égales, de 3 000 euros au titre du même article.<br/>
<br/>
<br/>
<br/>                D E C I D E :<br/>
                              --------------<br/>
Article 1er : L'intervention de la société CIS promotion est admise.<br/>
<br/>
Article 2 : Le pourvoi de la société Sinfimmo est rejeté.<br/>
<br/>
Article 3 : La société Sinfimmo versera au centre hospitalier de la Savoie et à la société CIS Promotion, à parts égales, une somme globale de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : La présente décision sera notifiée à la société Sinfimmo, au centre hospitalier spécialisé de la Savoie et à la société CIS Promotion.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
