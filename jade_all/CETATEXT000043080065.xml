<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043080065</ID>
<ANCIEN_ID>JG_L_2021_01_000000445084</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/08/00/CETATEXT000043080065.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème chambre, 28/01/2021, 445084, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-01-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>445084</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Guillaume Leforestier</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Marc Pichon de Vendeuil</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:445084.20210128</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A... C... a, par deux protestations distinctes, demandé au tribunal administratif de Poitiers d'annuler le premier tour du scrutin qui s'est déroulé le 15 mars 2020 en vue de la désignation des conseillers municipaux de la commune de Saint-Georges-d'Oléron. Par un jugement n°s 2000759, 2000761 du 15 septembre 2020, le tribunal a rejeté ces protestations.<br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés les 5 octobre et 30 novembre 2020 au secrétariat du contentieux du Conseil d'Etat, M. C... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) de faire droit à son appel.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code électoral ;<br/>
              - la loi n° 2020-290 du 13 mars 2020 ; <br/>
              - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020.<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Guillaume Leforestier, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Marc Pichon de Vendeuil, rapporteur public ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. A l'issue du premier tour du scrutin organisé le 15 mars 2020 en vue du renouvellement des conseillers municipaux et communautaires de la commune de Saint-Georges d'Oléron, la liste conduite par Mme D... a obtenu 50,18 % des suffrages exprimés et remporté vingt-et-un sièges au conseil municipal et quatre sièges au conseil communautaire. La liste conduite par M. C... a obtenu 41,60 % des suffrages exprimés, soit cinq sièges au conseil municipal et un siège au conseil communautaire. La liste conduite par M. B... F... a obtenu 8,21 % des suffrages exprimés et un siège au conseil municipal. M. C... relève appel du jugement du 15 septembre 2020 par lequel le tribunal administratif de Poitiers a rejeté ses protestations tendant à l'annulation de ces opérations électorales.<br/>
<br/>
              Sur la régularité de la procédure devant le tribunal administratif :<br/>
<br/>
              2. Il résulte des dispositions combinées de l'article R. 773-1 du code de justice administrative et des articles R. 119 et R. 120 du code électoral que, par dérogation aux dispositions de l'article R. 611-1 du code de justice administrative, les tribunaux administratifs ne sont pas tenus d'ordonner la communication des mémoires en défense des conseillers municipaux dont l'élection est contestée aux auteurs des protestations, ni des autres mémoires ultérieurement enregistrés et qu'il appartient seulement aux parties, si elles le jugent utile, de prendre connaissance de ces défenses et mémoires ultérieurs au greffe du tribunal administratif. Par suite, M. C... n'est pas fondé à soutenir que le défaut de communication du mémoire produit le 26 août 2020 par le conseil de Mme D... entacherait la procédure d'irrégularité.<br/>
<br/>
              3. Contrairement à ce qui est par ailleurs soutenu, il ressort des énonciations du jugement attaqué que les éléments produits par M. C... à l'appui de sa protestation ont été pris en considération par le tribunal administratif.<br/>
<br/>
              Sur la régularité des opérations électorales :<br/>
<br/>
              4. En premier lieu, l'émergence d'un nouveau coronavirus, responsable de la maladie à coronavirus 2019 ou covid-19 et particulièrement contagieux, a été qualifiée d'urgence de santé publique de portée internationale par l'Organisation mondiale de la santé le 30 janvier 2020, puis de pandémie le 11 mars 2020. La propagation du virus sur le territoire français a conduit le ministre des solidarités et de la santé à prendre, à compter du 4 mars 2020, des mesures de plus en plus strictes destinées à réduire les risques de contagion. Dans ce contexte, le Premier ministre a adressé à l'ensemble des maires le 7 mars 2020 une lettre présentant les mesures destinées à assurer le bon déroulement des élections municipales et communautaires prévues les 15 et 22 mars 2020. Ces mesures ont été précisées par une circulaire du ministre de l'intérieur du 9 mars 2020 relative à l'organisation des élections municipales des 15 et 22 mars 2020 en situation d'épidémie de coronavirus covid-19, formulant des recommandations relatives à l'aménagement des bureaux de vote et au respect des consignes sanitaires, et par une instruction de ce ministre, du même jour, destinée à faciliter l'exercice du droit de vote par procuration. Après consultation par le Gouvernement du conseil scientifique mis en place pour lui donner les informations scientifiques utiles à l'adoption des mesures nécessaires pour faire face à l'épidémie de covid-19, les 12 et 14 mars 2020, le premier tour des élections municipales a eu lieu comme prévu le 15 mars 2020. A l'issue du scrutin, les conseils municipaux ont été intégralement renouvelés dans 30 143 communes ou secteurs. Le taux d'abstention a atteint 55,34 % des inscrits, contre 36,45 % au premier tour des élections municipales de 2014. <br/>
<br/>
              5. Au vu de la situation sanitaire, l'article 19 de la loi du 23 mars 2020 d'urgence pour faire face à l'épidémie de covid-19 a reporté le second tour des élections, initialement fixé au 22 mars 2020, au plus tard en juin 2020 et prévu que : " Dans tous les cas, l'élection régulière des conseillers municipaux et communautaires, des conseillers d'arrondissement, des conseillers de Paris et des conseillers métropolitains de Lyon élus dès le premier tour organisé le 15 mars 2020 reste acquise, conformément à l'article 3 de la Constitution ". Ainsi que le Conseil constitutionnel l'a jugé dans sa décision n° 2020-849 QPC du 17 juin 2020, ces dispositions n'ont ni pour objet ni pour effet de valider rétroactivement les opérations électorales du premier tour ayant donné lieu à l'attribution de sièges et ne font ainsi pas obstacle à ce que ces opérations soient contestées devant le juge de l'élection. <br/>
<br/>
              6. Aux termes de l'article L. 262 du code électoral, applicable aux communes de mille habitants et plus : " Au premier tour de scrutin, il est attribué à la liste qui a recueilli la majorité absolue des suffrages exprimés un nombre de sièges égal à la moitié du nombre des sièges à pourvoir, arrondi, le cas échéant, à l'entier supérieur lorsqu'il y a plus de quatre sièges à pourvoir et à l'entier inférieur lorsqu'il y a moins de quatre sièges à pourvoir. Cette attribution opérée, les autres sièges sont répartis entre toutes les listes à la représentation proportionnelle suivant la règle de la plus forte moyenne, sous réserve de l'application des dispositions du troisième alinéa ci-après. / Si aucune liste n'a recueilli la majorité absolue des suffrages exprimés au premier tour, il est procédé à un deuxième tour (...) ". Aux termes de l'article L. 273-8 du code électoral : " Les sièges de conseiller communautaire sont répartis entre les listes par application aux suffrages exprimés lors de cette élection des règles prévues à l'article L. 262. (...) ". <br/>
<br/>
              7. Ni par ces dispositions, ni par celles de la loi du 23 mars 2020 le législateur n'a subordonné à un taux de participation minimal la répartition des sièges au conseil municipal à l'issue du premier tour de scrutin dans les communes de mille habitants et plus, lorsqu'une liste a recueilli la majorité absolue des suffrages exprimés. Le niveau de l'abstention n'est ainsi, par lui-même, pas de nature à remettre en cause les résultats du scrutin, s'il n'a pas altéré, dans les circonstances de l'espèce, sa sincérité. <br/>
<br/>
              8. En l'espèce, M. C... se borne à relever que le taux d'abstention dans la commune a été supérieur à celui des élections de 2014 et à produire plusieurs attestations d'électeurs selon lesquelles ils se seraient abstenus de voter en raison du contexte sanitaire, sans invoquer aucune autre circonstance relative au déroulement de la campagne électorale ou du scrutin dans la commune qui montrerait, en particulier, qu'il aurait été porté atteinte au libre exercice du droit de vote ou à l'égalité entre les candidats. Dans ces conditions, le niveau de l'abstention constatée ne peut être regardé comme ayant altéré la sincérité du scrutin. <br/>
<br/>
              9. En deuxième lieu, si M. C... produit la photographie d'une inscription peinte en travers d'une chaussée aux termes de laquelle : " Voter C..., c'est voter FN ", il ne résulte toutefois pas de l'instruction que cette inscription, dont la date et l'emplacement exacts sont indéterminés et qui n'excède en tout état de cause pas les limites de ce qui peut être toléré dans le cadre de la polémique électorale, ait été de nature à altérer la sincérité du scrutin.<br/>
<br/>
              10. En troisième lieu, aux termes de l'article L. 49 du code électoral, dans sa version applicable aux faits de l'espèce : " A partir de la veille du scrutin à zéro heure, il est interdit de distribuer ou faire distribuer des bulletins, circulaires et autres documents. A partir de la veille du scrutin à zéro heure, il est également interdit de diffuser ou de faire diffuser par tout moyen de communication au public par voie électronique tout message ayant le caractère de propagande électorale ". Si M. C... soutient en premier lieu que Mme D... s'est rendue disponible la veille du scrutin pour répondre à d'éventuelles questions des électeurs, l'organisation d'une telle permanence n'était alors pas prohibée par les dispositions précitées. En second lieu, le requérant n'établit pas et il ne résulte pas de l'instruction que des bulletins, circulaires ou autres documents aient été distribués à cette occasion, en méconnaissance des mêmes dispositions. <br/>
<br/>
              11. En quatrième et dernier lieu, aux termes de l'article R. 76-1 du code électoral : " Le défaut de réception par le maire d'une procuration fait obstacle à ce que le mandataire participe au scrutin ". Il ressort des mentions portées au procès-verbal des opérations électorales que deux procurations n'ont pu, dans un premier temps, être admises faute d'avoir été préalablement reçues en mairie et que ce problème a par la suite été " résolu ", lesdites procurations ayant été adressées par erreur à Paris. Il résulte par ailleurs de l'instruction que les détenteurs des procurations litigieuses ont sollicité la gendarmerie qui en a vérifié et confirmé la validité auprès du bureau de vote. <br/>
<br/>
              12. Eu égard à l'impossibilité dans laquelle il se trouve de présumer l'identité des candidats en faveur desquels les deux électeurs ayant porté ces mentions ont exprimé leurs suffrages et alors même que ces irrégularités, à les supposer établies, ne seraient pas imputables à une manoeuvre des candidats élus, il appartient au juge de l'élection, pour en apprécier l'influence sur le scrutin, de placer les candidats dont l'élection est contestée dans la situation la plus défavorable et de retrancher deux voix du total obtenu par ces candidats et du total des suffrages exprimés. En application de cette méthode, le résultat obtenu par la liste de Mme D... devait être ramené à 969 voix, pour une majorité absolue de 967 voix. Il s'ensuit que les irrégularités alléguées par le requérant n'ont pu, en tout état de cause, altérer la sincérité du scrutin.<br/>
<br/>
              13. Il résulte de tout ce qui précède que M. C... n'est pas fondé à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Poitiers a rejeté sa protestation. <br/>
<br/>
              14. Il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par Mme D... au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>			D E C I D E :<br/>
              			--------------<br/>
<br/>
Article 1er : La requête de M. C... est rejetée.<br/>
Article 2 : Les conclusions présentées par Mme D... sur le fondement de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La présente décision sera notifiée à M. A... C..., Mme E... D... et au ministre de l'intérieur. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
