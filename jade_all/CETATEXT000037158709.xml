<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037158709</ID>
<ANCIEN_ID>JG_L_2018_07_000000407086</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/15/87/CETATEXT000037158709.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème - 8ème chambres réunies, 05/07/2018, 407086, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-07-05</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>407086</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème - 8ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP WAQUET, FARGE, HAZAN</AVOCATS>
<RAPPORTEUR>M. Sylvain Monteillet</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Vincent Daumas</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:407086.20180705</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme A...B...a demandé au tribunal administratif de Pau d'annuler la décision du 10 septembre 2012 par laquelle le préfet des Pyrénées-Atlantiques a prononcé la déchéance de ses droits aux primes au maintien du troupeau de vaches allaitantes et aides à la surface pour la campagne 2012, ainsi que la décision du 8 janvier 2013 rejetant son recours gracieux et la décision du ministre de l'agriculture, de l'agroalimentaire et de la forêt du 2 avril 2013 rejetant son recours hiérarchique.<br/>
<br/>
              Par un jugement n° 1300854 du 16 octobre 2014, le tribunal administratif de Pau a rejeté sa requête. <br/>
<br/>
              Par un arrêt n° 14BX03596 du 30 juin 2016, la cour administrative d'appel de Bordeaux a rejeté l'appel formé par Mme B...contre ce jugement.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 23 janvier et 24 avril 2017 au secrétariat du contentieux du Conseil d'Etat, Mme B... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat une somme de 4 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le règlement (CE) n° 73/2009 du Conseil du 19 janvier 2009 ;<br/>
              - le règlement (CE) n° 1122/2009 de la Commission du 30 novembre 2009 ; <br/>
              - le code rural et de la pêche maritime ;<br/>
              - l'arrêt de la Cour de justice de l'Union européenne du 16 juin 2011, Marija Omejc (C-536/09) ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Sylvain Monteillet, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Vincent Daumas, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Waquet, Farge, Hazan, avocat de Mme A...B...;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond qu'à la suite d'un contrôle sur place effectué de manière inopinée le 18 juin 2012, le préfet des Pyrénées-Atlantiques a prononcé, par décision du 10 septembre 2012, la déchéance des droits de Mme A...B...aux primes relatives à un troupeau de vaches allaitantes et aux aides à la surface pour la campagne 2012, au motif d'un refus de contrôle. Mme B... a demandé au tribunal administratif de Pau d'annuler cette décision ainsi que la décision du 8 janvier 2013 par laquelle le préfet des Pyrénées-Atlantiques a rejeté son recours gracieux et la décision du ministre de l'agriculture, de l'agroalimentaire et de la forêt du 2 avril 2013 rejetant son recours hiérarchique. Par un jugement du 16 octobre 2014, le tribunal administratif de Pau a rejeté sa demande. Mme B...se pourvoit en cassation contre l'arrêt du 30 juin 2016 par lequel la cour administrative d'appel de Bordeaux a rejeté l'appel qu'elle a interjeté à l'encontre de ce jugement.<br/>
<br/>
              2. Aux termes de l'article 26 du règlement (CE) n° 1122/2009 de la Commission du 30 novembre 2009 fixant les modalités d'application du règlement (CE) n° 73/2009 du Conseil en ce qui concerne la conditionnalité, la modulation et le système intégré de gestion et de contrôle dans le cadre des régimes de soutien direct en faveur des agriculteurs prévus par ce règlement ainsi que les modalités d'application du règlement (CE) n° 1234/2007 du Conseil en ce qui concerne la conditionnalité dans le cadre du régime d'aide prévu pour le secteur vitivinicole : " 1. Les contrôles administratifs et les contrôles sur place prévus par le présent règlement sont effectués de façon à assurer une vérification efficace du respect des conditions d'octroi des aides ainsi que des exigences et des normes applicables en matière de conditionnalité. / 2.  Les demandes concernées sont rejetées si l'agriculteur ou son représentant empêche la réalisation d'un contrôle sur place ".<br/>
<br/>
              3. La Cour de justice de l'Union européenne a dit pour droit, dans son arrêt du 16 juin 2011 Marija Omejc (C-536/09), que la notion de représentant constitue une notion autonome du droit de l'Union et recouvre, lors des contrôles sur place, toute personne adulte, dotée de la capacité d'exercice, qui réside dans l'exploitation agricole et à laquelle est confiée au moins une partie de la gestion de cette exploitation, pour autant que l'agriculteur a clairement exprimé sa volonté de lui donner mandat aux fins de le représenter et, partant, s'est engagé à assumer tous les actes et toutes les omissions de cette personne. <br/>
<br/>
              4. Pour juger que M. C...B...avait pu refuser au nom de Mme B... le contrôle diligenté le 18 juin 2012, la cour a relevé que Mme B...avait reçu notification de ce refus opposé par M.B..., pour son compte, lors d'une rencontre avec les agents chargés du contrôle. En statuant ainsi, sans rechercher si la requérante pouvait être regardée comme ayant exprimé sa volonté de donner mandat à M. B... préalablement au contrôle et si celui-ci pouvait être regardé comme résidant dans l'exploitation agricole de l'intéressée dont il aurait eu une partie de la gestion, la cour a commis une erreur de droit.<br/>
<br/>
              5. Il résulte de ce qui précède que, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, Mme B...est fondée à demander l'annulation de l'arrêt de la cour administrative d'appel de Bordeaux qu'elle attaque.<br/>
<br/>
              6. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat le versement à Mme B...d'une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt n° 14BX03596 de la cour administrative de Bordeaux du 30 juin 2016 est annulé.<br/>
<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Bordeaux. <br/>
<br/>
Article 3 : L'Etat versera à Mme B...une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : La présente décision sera notifiée à Mme A...B...et au ministre de l'agriculture et de l'alimentation.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
