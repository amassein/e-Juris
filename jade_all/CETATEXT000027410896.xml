<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027410896</ID>
<ANCIEN_ID>JG_L_2013_05_000000343051</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/41/08/CETATEXT000027410896.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  5ème sous-section jugeant seule, 15/05/2013, 343051, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-05-15</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>343051</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 5ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>RICARD</AVOCATS>
<RAPPORTEUR>M. Olivier Rousselle</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2013:343051.20130515</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 6 septembre 2010 et 23 mars 2011 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. A... C...et Mme B...E...D..., demeurant ... et pour la société Art Vie, dont le siège est 39, rue des Favorites à Paris (75015), représentée par son gérant en exercice ; M. C..., Mme E...D...et la société Art Vie demandent au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler l'arrêt n° 08PA05773 - 08PA06488 du 24 juin 2010 par lequel la cour administrative d'appel de Paris a rejeté leurs appels contre le jugement n° 0419342/6-1 du 3 octobre 2008 du tribunal administratif de Paris rejetant leur demande tendant à l'annulation de la décision du préfet de police accordant le concours de la force publique pour leur expulsion d'un local situé 69, rue des Entrepreneurs à Paris, ainsi qu'à la condamnation de l'Etat à leur verser la somme de 500 000 euros en réparation des préjudices qu'ils estiment avoir subis ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à leurs conclusions d'appel ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 2 000 euros à verser à Me Pierre Ricard, avocat de M.C..., de Mme E...D...et de la société Art Vie, au titre des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 ; <br/>
<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 22 avril 2013, présentée pour M. C..., Mme D...et la société Art Vie ;<br/>
<br/>
              Vu le code des procédures civiles ; <br/>
<br/>
              Vu la loi n° 91-647 du 10 juillet 1991 ;<br/>
<br/>
              Vu la loi n° 91-650 du 9 juillet 1991 ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Olivier Rousselle, Conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Ricard, avocat de M.C..., de Mme B...E...D...et de la SARL Art Vie ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, par un arrêt du 11 décembre 2001, la cour d'appel de Versailles a ordonné l'expulsion de M. C..., de Mme E...D...et de la société Art Vie d'un local situé 69, rue des Entrepreneurs à Paris dont ils étaient locataires ; que par une décision du 12 mai 2003 le préfet de police a accordé le concours de la force publique pour l'exécution de cet arrêt ; que, par un jugement du 3 octobre 2008, le tribunal administratif de Paris a rejeté la demande de M. C..., de Mme E...D...et de la société Art Vie tendant à l'annulation de cette décision ainsi qu'à la condamnation de l'Etat à les indemniser des préjudices qu'ils estiment avoir subis de ce fait ; que M.C..., Mme E...D...et la société Art Vie se pourvoient en cassation à l'encontre de l'arrêt du 24 juin 2010 par lequel la cour administrative d'appel de Paris a rejeté les appels formés contre ce jugement ; <br/>
<br/>
              2. Considérant que toute décision de justice ayant force exécutoire peut donner lieu à une exécution forcée, la force publique devant, si elle est requise, prêter main forte à cette exécution ; que, toutefois, des considérations impérieuses tenant à la sauvegarde de l'ordre public, ou à la survenance de circonstances postérieures à la décision judiciaire d'expulsion telles que l'exécution de celle-ci serait susceptible d'attenter à la dignité de la personne humaine, peuvent légalement justifier, sans qu'il soit porté atteinte au principe de la séparation des pouvoirs, le refus de prêter le concours de la force publique ; qu'en cas d'octroi de la force publique, il appartient au juge de rechercher si l'appréciation à laquelle s'est livrée l'administration sur la nature et l'ampleur des troubles à l'ordre public susceptibles d'être engendrés par sa décision ou sur les conséquences de l'expulsion des occupants compte tenu de la survenance de circonstances postérieures à la décision de justice l'ayant ordonnée n'est pas entachée d'une erreur manifeste d'appréciation ; qu'en jugeant que le risque de troubles à l'ordre public ne pouvait utilement être invoqué pour contester une décision d'octroi du concours de la force publique, la cour administrative d'appel a commis une erreur de droit ; que, par suite, sans qu'il soit besoin d'examiner l'autre moyen du pourvoi, son arrêt doit être annulé ; <br/>
<br/>
              3. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              4. Considérant que les requêtes d'appel n° 08PA05773 et n° 08PA06488 présentées respectivement par Mme E...D...et par M. C...devant la cour administrative d'appel de Paris sont dirigées contre le même jugement ; qu'il y a lieu de les joindre pour qu'il y soit statué par une seule décision ; <br/>
<br/>
              5. Considérant, en premier lieu, que les conclusions indemnitaires présentées par M.C..., Mme E...D...et la société Art Vie n'ont été précédées d'aucune demande préalable à l'administration, même en cours d'instance ; que, par suite, les requérants ne sont pas fondés à soutenir que c'est à tort que le tribunal administratif de Paris, après avoir constaté que le préfet de police n'avait pas défendu au fond sur leur demande, a rejeté comme irrecevables leurs conclusions indemnitaires ; <br/>
<br/>
              6. Considérant, en second lieu, qu'ainsi qu'il a été dit ci-dessus, il appartient au juge de rechercher si l'appréciation à laquelle s'est livrée l'administration sur la nature et l'ampleur des troubles à l'ordre public susceptibles d'être engendrés par sa décision d'octroi du concours de la force publique n'est pas entachée d'erreur manifeste d'appréciation ; qu'il ne ressort pas des pièces du dossier que M. C...et Mme E...D...se trouvaient dans une situation de précarité telle que leur expulsion devait être considérée par le préfet comme de nature à porter atteinte à l'ordre public ; qu'ainsi les requérants ne sont pas fondés à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Paris a estimé que le préfet de police n'avait pas commis d'erreur manifeste d'appréciation en accordant le concours de la force publique pour leur expulsion et a en conséquence rejeté leur demande tendant à l'annulation de cette décision d'octroi ; <br/>
<br/>
              7. Considérant que les dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt n° 08PA05773 - 08PA06488 de la cour administrative d'appel de Paris du 24 juin 2010 est annulé. <br/>
<br/>
Article 2 : Les requêtes présentées par M.C..., Mme E...D...et la société Art Vie devant la cour administrative d'appel de Paris, ainsi que leurs conclusions tendant à l'application des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 sont rejetées. <br/>
<br/>
Article 3 : La présente décision sera notifiée à M. A...C..., à Mme B... E...D..., à la société Art Vie et au ministre de l'intérieur. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
