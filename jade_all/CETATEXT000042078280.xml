<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042078280</ID>
<ANCIEN_ID>JG_L_2020_07_000000431101</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/07/82/CETATEXT000042078280.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème chambre, 02/07/2020, 431101, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-07-02</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>431101</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP NICOLAY, DE LANOUVELLE, HANNOTIN ; SCP HEMERY, THOMAS-RAQUIN, LE GUERER</AVOCATS>
<RAPPORTEUR>M. Olivier Rousselle</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Cécile Barrois de Sarigny</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2020:431101.20200702</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 27 mai et 6 août 2019 et le 5 juin 2020 au secrétariat du contentieux du Conseil d'Etat, M. A... B... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision du 20 mars 2019 par laquelle le conseil national de l'ordre des masseurs-kinésithérapeutes, statuant en formation restreinte, a suspendu son droit d'exercer la profession de masseur-kinésithérapeute pour une durée de 10 mois et a subordonné la reprise de son activité professionnelle à la tenue d'une nouvelle expertise ;<br/>
<br/>
              2°) de mettre à la charge du conseil national de l'ordre des masseurs-kinésithérapeutes et de l'agence régionale de santé d'Ile-de-France la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
              Vu : <br/>
<br/>
              - le code de la santé publique ;<br/>
              - le code de justice administrative et l'ordonnance n° 2020-305 du 25 mars 2020 ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Olivier Rousselle, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Cécile Barrois de Sarigny, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Nicolaÿ, de Lanouvelle, Hannotin, avocat de M. B... et à la SCP Hémery, Thomas-Raquin, Le Guerer, avocat du conseil national de l'ordre des masseurs-kinésithérapeutes ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article R. 4124-3 du code de la santé publique : " I. - Dans le cas d'infirmité ou d'état pathologique rendant dangereux l'exercice de la profession, la suspension temporaire du droit d'exercer est prononcée par le conseil régional ou interrégional pour une période déterminée, qui peut, s'il y a lieu, être renouvelée. / Le conseil est saisi à cet effet soit par le directeur général de l'agence régionale de santé soit par une délibération du conseil départemental ou du conseil national. (...) / II. - La suspension ne peut être ordonnée que sur un rapport motivé établi à la demande du conseil régional ou interrégional par trois médecins désignés comme experts, le premier par l'intéressé, le deuxième par le conseil régional ou interrégional et le troisième par les deux premiers experts. (...) ".<br/>
<br/>
              2. Il ressort des pièces du dossier que M. B..., masseur-kinésithérapeute, a fait l'objet d'une décision, prise le 6 juin 2018 par le directeur général de l'agence régionale de santé (ARS) d'Ile-de-France sur le fondement de l'article L. 4113-14 du code de la santé publique, prononçant la suspension de son activité pour une durée de 5 mois, au motif que la poursuite de celle-ci faisait courir un danger à la patientèle féminine eu égard aux condamnations judiciaires prononcées contre lui pour des faits d'agression sexuelle. Par une décision du 8 janvier 2019, la formation restreinte du conseil régional de l'ordre des masseurs-kinésithérapeutes d'Ile-de-France, saisie par l'ARS d'Ile-de-France, a suspendu M. B... du droit d'exercer sa profession pour une durée de 9 mois. Par une décision du 20 mars 2019, le conseil national de l'ordre, saisi par M. B..., a, après avoir annulé la décision du 8 janvier 2019, suspendu M. B... du droit d'exercer sa profession pour une durée de 10 mois à compter de la date de cette décision et subordonné la reprise de son activité à la tenue d'une nouvelle expertise psychiatrique.<br/>
<br/>
              3. Pour décider que M. B... devait être regardé comme présentant un état pathologique rendant dangereux l'exercice de sa profession, le conseil national de l'ordre des masseurs-kinésithérapeutes s'est fondé, d'une part, sur le rapport des trois experts psychiatres désignés dans le cadre de la procédure prévue par l'article R. 4124-3 du code de la santé publique et, d'autre part, sur la circonstance qu'il avait été reconnu coupable d'agression sexuelle sur une condisciple au cours de ses études par un arrêt de la cour d'appel de Paris du 8 octobre 2018 et condamné à trois ans de prison dont deux avec sursis et mise à l'épreuve, sur plusieurs témoignages, y compris du directeur de l'établissement, qui confirmaient un comportement déplacé et malsain à l'égard de ses condisciples féminines durant sa scolarité, ainsi que sur la circonstance que, dans une instance distincte, il avait été reconnu coupable de faits d'atteintes sexuelles sur trois de ses patientes par un jugement du tribunal de grande instance de Nanterre du 8 novembre 2018. En se fondant, pour prendre la décision attaquée, sur des faits et des témoignages confirmés par les condamnations prononcées par le juge pénal, qui révélaient un état pathologique rendant dangereux l'exercice de la profession de masseur-kinésithérapeute et en subordonnant la reprise d'activité à une nouvelle expertise, le conseil national de l'ordre, qui a seulement cherché à protéger la patientèle de M. B... par une mesure provisoire de suspension de son activité, n'a pas donné à sa décision le caractère d'une sanction. Par suite, le moyen tiré de ce que la décision de suspension constituerait une sanction déguisée et aurait été prise par une autorité incompétente et au terme d'une procédure irrégulière, faute de respecter les garanties procédurales attachées au contentieux disciplinaire et le principe selon lequel une sanction prononcée en première instance ne peut pas être aggravée sur appel de l'intéressé, ne peut qu'être écarté. <br/>
<br/>
              4. Le rapport d'expertise prévu par les dispositions citées ci-dessus de l'article R. 4124-3 du code de la santé publique a pour seul objet d'éclairer l'instance ordinale et ne la lie pas, pour l'appréciation qui lui incombe, de l'existence éventuelle d'un état pathologique rendant dangereux l'exercice de la médecine. Par suite, la circonstance que les auteurs du rapport ont conclu le 1er décembre 2018 à ce que l'expertise psychiatrique ne mettait pas en évidence une pathologie psychiatrique manifeste, tout en préconisant que la mesure de suspension se poursuive si les faits d'agressions sexuelles reprochés au praticien étaient avérés, ne faisait pas obstacle à ce que le conseil national de l'ordre prononce, par la décision attaquée, une suspension du droit d'exercer la profession de masseur-kinésithérapeute.<br/>
<br/>
              5. Enfin, il ressort des pièces du dossier qu'en estimant, au vu de l'ensemble des éléments d'information dont il disposait sur le comportement de M. B..., notamment des faits ayant donné lieu à plusieurs témoignages et condamnations, que l'état de santé de l'intéressé rendait dangereux l'exercice de celle-ci et justifiait une mesure de suspension d'une durée de dix mois, le conseil national de l'ordre des masseurs-kinésithérapeutes, qui, eu égard aux dangers présentés par l'état pathologique de M. B..., n'était pas tenu de limiter la suspension de l'exercice aux seules patientes féminines en raison de la difficulté exercer un contrôle sur ce point, alors même qu'une ordonnance du juge des libertés et de la détention prise dans le cadre de son contrôle judiciaire comportait une telle limitation, n'a pas fait une inexacte application des dispositions citées ci-dessus de l'article R. 4124-3 du code de la santé publique.<br/>
<br/>
              6. Il résulte de tout ce qui précède que M. B... n'est pas fondé à demander l'annulation de la décision qu'il attaque. Ses conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative doivent, par suite, également être rejetées. Il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par le conseil national de l'ordre des masseurs-kinésithérapeutes au titre des mêmes dispositions. <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
		Article 1er : La requête de M. B... est rejetée.<br/>
<br/>
Article 2 : Les conclusions présentées par le conseil national de l'ordre des masseurs-kinésithérapeutes au titre de l'article L. 761-1 du code de la justice administrative sont rejetées.<br/>
<br/>
Article 3 : La présente décision sera notifiée à M. A... B... et au conseil national de l'ordre des masseurs-kinésithérapeutes.<br/>
Copie en sera adressée au directeur de l'agence régionale de santé d'Ile-de-France et au conseil départemental des Hauts-de-Seine de l'ordre des masseurs-kinésithérapeutes.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
