<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034017897</ID>
<ANCIEN_ID>JG_L_2017_02_000000392271</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/01/78/CETATEXT000034017897.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème - 9ème chambres réunies, 08/02/2017, 392271, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-02-08</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>392271</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème - 9ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MATUCHANSKY, POUPOT, VALDELIEVRE</AVOCATS>
<RAPPORTEUR>M. Arno Klarsfeld</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Aurélie Bretonneau</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:392271.20170208</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              L'Union des coopératives agricoles Altitude a demandé au tribunal administratif de Clermont-Ferrand de prononcer la décharge de la taxe foncière à laquelle elle a été assujettie au titre des années 2009 à 2011 dans les rôles de la commune de Blesle (Haute-Loire). Par un jugement n° 1301037 du 2 juin 2015 le tribunal administratif de Clermont-Ferrand a rejeté sa demande<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire enregistrés les 3 août 2015 et 3 novembre 2015 au secrétariat du contentieux du Conseil d'Etat, l'Union des coopératives agricoles Altitude demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 4 500 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code général des impôts et le livre des procédures fiscales<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Arno Klarsfeld, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Aurélie Bretonneau, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Matuchansky, Poupot, Valdelièvre, avocat de l'Union des coopératives agricoles Altitude  ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que l'Union de coopératives agricoles Altitude, qui regroupe des sociétés coopératives territoriales de collecte de productions d'aliments pour le bétail, est propriétaire à Blesle (Haute-Loire) d'un établissement composé d'une usine de fabrication d'alimentation animale, d'une unité de stockage de l'alimentation et d'un bureau. Elle a demandé au tribunal administratif de Clermont-Ferrand de prononcer la décharge de la taxe foncière à laquelle elle a été assujettie au titre des années 2009 à 2011 dans les rôles de cette commune à raison de cet établissement. Par un jugement du 2 juin 2015 le tribunal administratif de Clermont-Ferrand a rejeté sa demande.<br/>
<br/>
              2. Aux termes de l'article 1382 du code général des impôts : " Sont exonérés de la taxe foncière sur les propriétés bâties : (...) 6° a) Les bâtiments qui servent aux exploitations rurales, tels que granges, écuries, greniers, caves, celliers, pressoirs et autres, destinés soit à loger les bestiaux des fermes et métairies ainsi que le gardien de ces bestiaux, soit à serrer les récoltes. (...) ; b) Dans les mêmes conditions qu'au premier alinéa du a, les bâtiments affectés à un usage agricole par les sociétés coopératives agricoles (...) ainsi que les unions de sociétés coopératives agricoles ou unions de coopératives agricoles et de coopératives de consommation constituées et fonctionnant conformément aux dispositions légales qui les régissent (...)".<br/>
<br/>
              3. En faisant expressément référence aux conditions de l'exonération de taxe foncière prévue au a) du 6° de l'article 1382 du code général des impôts, laquelle concerne les bâtiments servant aux exploitations rurales, les dispositions du b) du 6° du même article ont entendu donner à la notion d'usage agricole qu'elles mentionnent une signification visant les opérations qui sont réalisées habituellement par les agriculteurs eux-mêmes et qui ne présentent pas un caractère industriel. Pour l'application de ces dispositions, ne présentent pas un caractère industriel les opérations réalisées par une société coopérative agricole avec des moyens techniques qui n'excèdent pas les besoins collectifs de ses adhérents, quelle que soit l'importance de ces moyens. Dans le cas où, pour la réalisation de ses opérations, et sous réserve qu'elle fonctionne conformément aux dispositions légales qui la régissent, une société coopérative agricole procède de façon habituelle à des achats auprès de personnes autres que ses adhérents, il y a lieu, pour apprécier si les moyens techniques n'excèdent pas les besoins collectifs de ses adhérents, d'examiner si ces achats ont rendu nécessaires des investissements supérieurs à ceux qu'exige la satisfaction de ces besoins. Si tel est le cas, les bâtiments de cette société ne peuvent ouvrir droit au bénéfice de l'exonération de taxe foncière prévue par les dispositions précitées du 6° de l'article 1382 du code général des impôts. En revanche, lorsque la société peut exercer la totalité de son activité avec les moyens techniques dont elle dispose et qui sont proportionnés aux besoins collectifs de ses adhérents, la seule circonstance qu'elle procède à des opérations d'achat ou de vente auprès de non-adhérents ne lui fait pas nécessairement perdre le bénéfice de cette exonération.<br/>
<br/>
              4. Il ressort des pièces du dossier soumis au juge du fond que l'Union requérante fabrique des aliments pour bétail et dispose à cette fin d'une capacité de traitement d'environ 40 000 tonnes par an. Pour juger que l'administration avait légalement pu lui refuser le bénéfice de l'exonération de taxe foncière qu'elle sollicitait sur le fondement des dispositions du 6° de l'article 1382 du code général des impôts citées au point 2 ci-dessus, le tribunal administratif, après avoir relevé, d'une part, que les produits qu'elle fabriquait, en dépit de leur technicité, n'étaient pas d'une nature différente de ceux qui peuvent être réalisés par des agriculteurs disposant d'un matériel équivalent à partir de leur propre production, d'autre part, que l'établissement de Blesle cédait sa production à plus de 98 % à des adhérents de l'Union, s'est fondée sur ce que les céréales collectées par les coopératives de l'Union auprès de leurs adhérents, ne représentaient qu'entre 53 et 54 % des céréales traitées pour les années en litige, lesquelles ne représentaient que 40 à 43 % du total des produits traités, le solde étant composé de tourteaux et additifs acquis de tiers, pour en déduire que, l'installation en litige étant destinée à moins du quart de ses capacités à traiter les produits des adhérents des coopératives membres de l'union, l'investissement correspondant devait être regardé comme excédant les besoins collectifs de ces adhérents pour le traitement de leur production. <br/>
<br/>
              5. En déduisant ainsi le caractère industriel des opérations réalisées par l'établissement de l'Union requérante de la seule circonstance qu'elle recourait, pour assurer sa production, de manière majoritaire à des achats de matières premières auprès de personnes autres que les exploitations adhérentes de ses coopératives membres, de sorte que les capacités de l'établissement auraient excédé les besoins collectifs de ces exploitations, alors que l'Union n'avait pas pour fonction de commercialiser des produits finis sur un marché mais de fournir aux adhérents de ses membres des aliments pour bétail constituant des produits intermédiaires pour les besoins de leur propre production, et alors au surplus qu'il avait relevé que ces aliments étaient cédés en quasi-totalité à ces adhérents, le tribunal a entaché son jugement d'une erreur de droit. Il résulte de ce qui précède que le jugement attaqué doit être annulé.<br/>
<br/>
              6. Il y a lieu dans les circonstances de l'espèce de mettre à la charge de l'Etat la somme de 3 000 euros à verser à l'Union des coopératives agricoles Altitude au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1 : Le jugement du tribunal administratif de Clermont-Ferrand est annulé.<br/>
Article 2 : L'affaire est renvoyée au tribunal administratif de Clermont-Ferrand.<br/>
Article 3 : L'Etat versera la somme de 3 000 euros à l'Union des coopératives agricoles Altitude au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à l'Union des coopératives agricoles Altitude et au ministre de l'économie et des finances.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
