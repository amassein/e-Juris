<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027288068</ID>
<ANCIEN_ID>JG_L_2013_04_000000365832</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/28/80/CETATEXT000027288068.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème et 3ème sous-sections réunies, 08/04/2013, 365832, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-04-08</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>365832</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème et 3ème sous-sections réunies</FORMATION>
<TYPE_REC>Autres</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Jean-Marc Vié</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Benoît Bohnert</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:365832.20130408</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu l'arrêt n° 11/00079 du 6 février 2013, enregistré le 7 février 2013 au secrétariat du contentieux du Conseil d'Etat, par lequel la cour régionale des pensions de Montpellier, avant de statuer sur l'appel de Mme B... A...tendant à la réversion de la pension militaire d'invalidité de son ex-époux, a décidé, par application des dispositions de l'article 23-2 de l'ordonnance n° 58-1067 du 7 novembre 1958, de transmettre au Conseil d'Etat la question de la conformité aux droits et libertés garantis par la Constitution de l'article L. 43 du code des pensions militaires d'invalidité et des victimes de la guerre ; <br/>
<br/>
              Vu le mémoire, enregistré le 9 décembre 2011 au greffe de la cour régionale des pensions de Montpellier, présenté par Mme B...A..., en application de l'article L. 3-1 de l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la Constitution, notamment son Préambule et son article 61-1 ;<br/>
<br/>
              Vu l'ordonnance n° 58-1067 du 7 novembre 1958 ; <br/>
<br/>
              Vu le code des pensions militaires d'invalidité et des victimes de la guerre, notamment l'article L. 43 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Marc Vié, Maître des Requêtes, <br/>
<br/>
              - les conclusions de M. Benoît Bohnert, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il résulte des dispositions de l'article 23-4 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel que, lorsqu'une juridiction relevant du Conseil d'Etat a transmis à ce dernier, en application de l'article 23-2 de cette même ordonnance, la question de la conformité à la Constitution d'une disposition législative, le Conseil constitutionnel est saisi de cette question de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux ;<br/>
<br/>
              2. Considérant que l'article L. 43 du code des pensions militaires d'invalidité et victimes de guerre dispose que : " Ont droit à pension : 1° Les conjoints survivants des militaires et marins dont la mort a été causée par des blessures ou suites de blessures reçues au cours d'événements de guerre ou par des accidents ou suites d'accidents éprouvés par le fait ou à l'occasion du service ; 2° Les conjoints survivants des militaires et marins dont la mort a été causée par des maladies contractées ou aggravées par suite de fatigues, dangers ou accidents survenus par le fait ou à l'occasion du service, ainsi que les conjoints survivants de militaires et marins morts en jouissance d'une pension définitive ou temporaire correspondant à une invalidité égale ou supérieure à 85 % ou en possession de droits à cette pension ; 3° Les conjoints survivants des militaires et marins morts en jouissance d'une pension définitive ou temporaire correspondant à une invalidité égale ou supérieure à 60 % ou en possession de droits à cette pension. / Dans les trois cas, il y a droit à pension si le mariage est antérieur soit à l'origine, soit à l'aggravation de la blessure ou de la maladie, à moins qu'il ne soit établi qu'au moment du mariage l'état du conjoint pouvait laisser prévoir une issue fatale à brève échéance. / La condition d'antériorité du mariage ne sera pas exigée du conjoint survivant lorsqu'il aura eu un ou plusieurs enfants légitimes ou légitimés ou naturels reconnus dans les conditions prévues à l'article L. 64, ainsi que du conjoint survivant sans enfant qui pourrait prouver qu'il a eu une vie commune de trois ans avec le conjoint mutilé, quelle que soit la date du mariage. / En outre, les conjoints survivants d'une personne mutilée de guerre ou d'expéditions déclarés campagnes de guerre, atteint d'une invalidité égale ou supérieure à 80 %, ont droit, au cas où ils ne pourraient se réclamer des dispositions de l'alinéa qui précède, à une pension de réversion si le mariage a été contracté dans les deux ans de la réforme de leur conjoint mutilé ou de la cessation des hostilités, et si ce mariage a duré une année ou a été rompu par la mort accidentelle de du conjoint mutilé. / Peuvent également prétendre à une pension du taux de réversion les conjoints survivants visés aux alinéas 1° et 2° ci-dessus, si le mariage contracté postérieurement, soit à la blessure, soit à l'origine de la maladie, soit à l'aggravation, soit à la cessation de l'activité, a duré deux ans. / Le défaut d'autorisation militaire en ce qui concerne le mariage contracté par les militaires ou marins en activité de service, n'entraîne pas pour les ayants cause, perte du droit à pension " ;<br/>
<br/>
              3. Considérant que l'article L. 43 du code des pensions militaires d'invalidité et des victimes de la guerre est applicable au présent litige ; qu'il n'a pas déjà été déclaré conforme à la Constitution par le Conseil constitutionnel ; que le moyen tiré de ce que cet article, en excluant du droit à réversion les anciens conjoints divorcés des bénéficiaires d'une pension militaire d'invalidité, porte atteinte aux droits et libertés garantis par la Constitution, et notamment au principe d'égalité devant la loi garanti par la Déclaration des droits de l'homme et du citoyen de 1789, soulève une question présentant un caractère sérieux ; qu'ainsi, il y a lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité invoquée ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>			D E C I D E :<br/>
              			--------------<br/>
<br/>
Article 1er : La question de la conformité à la constitution de l'article L. 43 du code des pensions militaires d'invalidité et des victimes de la guerre est renvoyée au Conseil constitutionnel.<br/>
<br/>
Article 2 : La présente décision sera notifiée à Mme B... A...et au ministre de la défense. <br/>
Copie en sera adressée au Premier ministre et à la cour régionale des pensions de Montpellier.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
