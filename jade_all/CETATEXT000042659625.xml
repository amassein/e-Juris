<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042659625</ID>
<ANCIEN_ID>JG_L_2020_12_000000428059</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/65/96/CETATEXT000042659625.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème chambre, 10/12/2020, 428059, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-12-10</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>428059</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BENABENT</AVOCATS>
<RAPPORTEUR>M. Nicolas Agnoux</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emilie Bokdam-Tognetti</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2020:428059.20201210</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A... B... a demandé au tribunal administratif de Lyon de prononcer la décharge des cotisations supplémentaires d'impôt sur le revenu et des pénalités correspondantes auxquelles il a été assujetti au titre des années 2009 et 2010. Par un jugement n° 1406012 du 22 novembre 2016, le tribunal administratif de Lyon a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 17LY00284 du 13 décembre 2018, la cour administrative d'appel de Lyon a, sur appel formé par M. B... contre ce jugement, prononcé une décharge partielle des impositions en litige et rejeté le surplus de ses conclusions.  <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 15 février et 10 mai 2019 au secrétariat du contentieux du Conseil d'Etat, M. B... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'article 5 de cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Nicolas Agnoux, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Emilie Bokdam-Tognetti, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Bénabent, avocat de M. B... ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que M. B... exerce une activité de prestataire de services en ingénierie industrielle dans le cadre d'une entreprise individuelle. A la suite d'une procédure de visite et saisie engagée sur le fondement de l'article L. 16 B du livre des procédures fiscales, qui a concerné le domicile de M. B..., celui de ses parents, le siège de son entreprise individuelle et celui de son expert-comptable, l'administration fiscale a engagé, d'une part, une vérification de comptabilité de la société Harucci, installée à Gibraltar, dont elle a estimé qu'elle avait un établissement stable au siège de l'entreprise individuelle de M. B..., et un examen contradictoire de la situation fiscale personnelle de ce dernier, ces deux opérations de contrôle portant sur les années 2009 et 2010. Estimant que M. B... avait exercé son activité par l'intermédiaire de la société Harucci, l'administration a, sur le fondement de l'article 155 A du code général des impôts, imposé entre les mains du contribuable, dans la catégorie des bénéfices industriels et commerciaux, les sommes perçues par cette société. L'administration fiscale a également rectifié les bénéfices industriels et commerciaux déclarés par M. B... au titre de son entreprise individuelle. Le tribunal administratif de Lyon a rejeté la demande de M. B... tendant à la décharge des cotisations supplémentaires d'impôt sur le revenu au titre des années 2009 et 2010 résultant de ces rectifications. M. B... demande l'annulation de l'arrêt de la cour administrative d'appel de Lyon du 13 décembre 2018 en tant qu'il n'a pas fait intégralement droit à son appel. <br/>
<br/>
              2. En premier lieu, d'une part, aux termes du I de l'article L. 13 du livre des procédures fiscales : " Les agents de l'administration des impôts vérifient sur place, en suivant les règles prévues par le présent livre, la comptabilité des contribuables astreints à tenir et à présenter des documents comptables ". Aux termes de l'article L. 47 du même livre : " Un examen contradictoire de la situation fiscale personnelle d'une personne physique au regard de l'impôt sur le revenu, une vérification de comptabilité ou un examen de comptabilité ne peut être engagé sans que le contribuable en ait été informé par l'envoi ou la remise d'un avis de vérification ou par l'envoi d'un avis d'examen de comptabilité. / Cet avis doit préciser les années soumises à vérification et mentionner expressément, sous peine de nullité de la procédure, que le contribuable a la faculté de se faire assister par un conseil de son choix (...) ". L'administration procède à la vérification de comptabilité d'une entreprise ou d'un membre d'une profession non commerciale lorsqu'en vue d'assurer l'établissement d'impôts ou de taxes totalement ou partiellement éludés par les intéressés, elle contrôle sur place la sincérité des déclarations fiscales souscrites par cette entreprise ou ce contribuable en les comparant avec les écritures comptables ou les pièces justificatives dont elle prend alors connaissance et dont, le cas échéant, elle peut remettre en cause l'exactitude. L'exercice régulier du droit de vérification de comptabilité suppose le respect des garanties légales prévues en faveur du contribuable vérifié, au nombre desquelles figure notamment l'envoi ou la remise de l'avis de vérification auquel se réfère l'article L. 47 du même livre. <br/>
<br/>
              3. Il ressort des pièces du dossier soumis aux juges du fond qu'après avoir procédé, à compter du 25 novembre 2011, à la vérification de comptabilité de l'établissement stable de la société Harucci au siège de l'entreprise individuelle de M. B..., le vérificateur a déterminé, en comparant notamment les relevés bancaires de la société Harucci obtenus auprès de l'administration fiscale lettone avec la comptabilité de l'entreprise individuelle de M. B..., le montant des bénéfices réalisés par la société Harucci en France, qu'elle a rattachés aux bénéfices industriels et commerciaux réalisés par l'entreprise individuelle de M. B... sur le fondement de l'article 155 A du code général des impôts. Après avoir relevé que l'administration fiscale ne s'était pas livrée au contrôle des déclarations fiscales souscrites par M. B..., en tant qu'entrepreneur individuel, en les comparant, au terme d'un examen critique, avec les écritures comptables ou les pièces justificatives de son entreprise individuelle, la cour, qui n'a pas méconnu son office, a pu juger, sans commettre d'erreur de droit ni inexactement qualifier les faits ainsi rappelés, qu'il n'avait pas été procédé à une vérification irrégulière de la comptabilité de l'entreprise individuelle de M. B.... <br/>
<br/>
              4. En deuxième lieu, aux termes du I de l'article 155 A du code général des impôts : " Les sommes perçues par une personne domiciliée ou établie hors de France en rémunération de services rendus par une ou plusieurs personnes domiciliées ou établies en France sont imposables au nom de ces dernières : - soit, lorsque celles-ci contrôlent directement ou indirectement la personne qui perçoit la rémunération des services ; - soit, lorsqu'elles n'établissent pas que cette personne exerce, de manière prépondérante, une activité industrielle ou commerciale, autre que la prestation de services ; - soit, en tout état de cause, lorsque la personne qui perçoit la rémunération des services est domiciliée ou établie dans un Etat étranger ou un territoire situé hors de France où elle est soumise à un régime fiscal privilégié au sens mentionné à l'article 238 A ". Il résulte de ces dispositions que la possibilité qu'elles prévoient d'imposer entre les mains d'une personne qui rend des services les sommes correspondant à la rémunération de ces services lorsqu'elles sont perçues par une personne domiciliée ou établie hors de France n'est pas subordonnée, dans l'hypothèse, mentionnée au I de cet article, où la personne qui rend les services est domiciliée en France, à la condition que ces services aient été rendus en France. <br/>
<br/>
              5. Il en résulte que le requérant n'est pas fondé à soutenir que la cour aurait commis une erreur de droit et inexactement qualifié les faits de l'espèce en jugeant inopérante la circonstance que les prestations d'ingénierie prises en compte pour déterminer le montant imposable entre ses mains sur le fondement du I de l'article 155 A du code général des impôts avaient été réalisées hors de France.<br/>
<br/>
              6. En troisième lieu, après avoir relevé, par une appréciation souveraine des faits exempte de dénaturation, d'une part, que la société Harucci s'appuyait, pour son administration et sa direction, sur les moyens matériels de l'entreprise individuelle de M. B..., chez qui avaient été saisies les pièces relatives au fonctionnement de la société, qui disposait des codes de connexion au compte bancaire de celle-ci et gérait les opérations faites sur ce compte, et, d'autre part, que les contrats de la société Harucci étaient préparés et négociés par l'entreprise de M. B... qui, seule, disposait de l'expertise technique pour ce faire, pour en déduire que la société Harucci était contrôlée par l'entreprise de M. B..., la cour, qui n'a pas, en tout état de cause, jugé que la société Harucci était un " intermédiaire dissimulé ", n'a pas commis d'erreur de droit ni inexactement qualifié les faits. <br/>
<br/>
              7. En quatrième lieu, il ressort des pièces du dossier soumis aux juges du fond que, pour déterminer le montant imposable entre les mains de M. B..., l'administration a déduit du chiffre d'affaires réalisé par la société Harucci en France les frais supportés pour l'emploi des ingénieurs autres que M. B... ainsi que les frais de sous-traitance. En jugeant que, dès lors que le montant ainsi imposé correspondait aux sommes perçues par la société Harucci en rémunération de la fourniture, par l'entreprise individuelle de M. B..., des prestations d'ingénierie effectuées au profit de la société Air Liquide, l'administration pouvait retenir l'ensemble des prestations de vente facturées par la société Harucci ne trouvant aucune contrepartie réelle dans une intervention propre de cette dernière, sans se limiter aux seules prestations réalisées par l'entreprise individuelle de M. B... pour son compte, la cour, qui n'a pas dénaturé les pièces du dossier qui lui était soumis, n'a pas commis d'erreur de droit. <br/>
<br/>
              8. En cinquième lieu, si M. B... soutient que la cour a dénaturé les pièces du dossier en jugeant qu'il n'apportait pas la preuve de l'erreur commise par l'administration dans l'imputation à l'exercice clos en 2009 de factures émises en 2008, il n'assortit pas son moyen des précisions suffisantes permettant d'en apprécier le bien-fondé.<br/>
<br/>
              9. En sixième et dernier lieu, aux termes de l'article 1729 du code général des impôts : " Les inexactitudes ou les omissions relevées dans une déclaration ou un acte comportant l'indication d'éléments à retenir pour l'assiette ou la liquidation de l'impôt ainsi que la restitution d'une créance de nature fiscale dont le versement a été indûment obtenu de l'Etat entraînent l'application d'une majoration de : a. 40% en cas de manquement délibéré (...) c. 80 % en cas de manoeuvres frauduleuses (...). " Les pénalités pour manoeuvres frauduleuses ont pour objet de sanctionner des agissements destinés à égarer ou à restreindre le pouvoir de contrôle de l'administration.<br/>
<br/>
              10. Pour juger que l'administration établissait l'existence de manoeuvres frauduleuses, la cour a relevé que M. B... avait consulté un cabinet d'expertise comptable pour évaluer les avantages et risques découlant de la facturation de ses prestations par l'intermédiaire d'une société sise à Gibraltar et que l'étude réalisée par ce cabinet, qui mentionnait l'article 155 A du code général des impôts, indiquait notamment que localiser un bénéfice à Gibraltar n'était intéressant que s'il était réutilisé ou " dans l'éventualité de distributions occultes, les pouvoirs de l'administration étant limités du fait de l'absence de contrôle de comptes à Gibraltar et de l'absence de convention internationale France-Gibraltar ". En se bornant à relever l'existence de cette étude et de son contenu, sans caractériser en quoi le recours à la société Harucci, créée antérieurement à cette étude par M. B..., avait permis d'égarer ou restreindre le pouvoir de contrôle de l'administration, la cour a commis une erreur de droit.<br/>
<br/>
              11. Il résulte de tout ce qui précède que M. B... est seulement fondé à demander l'annulation de l'arrêt qu'il attaque en tant qu'il statue sur les pénalités pour manoeuvres frauduleuses qui lui ont été infligées.<br/>
<br/>
              12. Il y a lieu, dans les circonstances de l'espèce, de régler au fond l'affaire, dans la mesure de la cassation énoncée au point 11 ci-dessus, en application de l'article L. 821-2 du code de justice administrative.<br/>
<br/>
              13. Il résulte de ce qui a été dit au point 10 ci-dessus qu'en se bornant à invoquer l'étude réalisée par à un cabinet d'expertise comptable sur les avantages découlant de la création d'une société à Gibraltar, l'administration ne démontre pas l'existence de manoeuvres frauduleuses de la part de M. B.... <br/>
<br/>
              14. Toutefois, il appartient au juge, dans une telle hypothèse, alors même que l'administration ne le saisirait pas d'une demande en ce sens, de rechercher si les éléments qui étaient invoqués par l'administration pour justifier des pénalités pour manoeuvres frauduleuses permettent, à défaut d'établir ces dernières, de caractériser l'intention délibérée du contribuable d'éluder l'impôt et de substituer, au besoin d'office, à la majoration de 80 % appliquée par l'administration, la majoration de 40 % prévue par le a de l'article 1729 du code général des impôts. <br/>
<br/>
              15. Compte tenu des circonstances qui ont justifié le redressement litigieux, notamment du fait que M. B... ne pouvait pas ignorer qu'il contrôlait la société Harucci, l'administration établit l'intention délibérée du contribuable d'éluder l'impôt. Il y a lieu, par suite, de substituer d'office à la majoration de 80 %, la majoration de 40 % prévue au a de l'article 1729 du code général des impôts.<br/>
<br/>
              16. Il résulte de ce qui précède que M. B... est seulement fondé à soutenir que c'est à tort que, par le jugement qu'il attaque, le tribunal administratif de Lyon a rejeté l'intégralité de ses conclusions tendant à la décharge de la majoration de 80 % à laquelle il a été assujetti.<br/>
<br/>
              17. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat, pour l'ensemble de la procédure, la somme de 3 000 euros en application des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 13 décembre 2018 de la cour administrative d'appel de Lyon est annulé en tant qu'il statue sur les pénalités pour manoeuvres frauduleuses.<br/>
Article 2 : La pénalité de 40 % prévue par le a de l'article 1729 du code général des impôts est substituée à la pénalité au taux de 80 % dont ont été assorties les cotisations supplémentaires d'impôt sur le revenu auxquelles M. B... a été assujetti au titre des années 2009 et 2010.<br/>
Article 3 : Le jugement du tribunal administratif de Lyon du 22 novembre 2016 est réformé en ce qu'il a de contraire à l'article 2 ci-dessus.<br/>
Article 4 : Le surplus des conclusions du pourvoi de M. B... et le surplus des conclusions de sa requête d'appel en ce qui concerne les pénalités est rejeté.<br/>
Article 5 : L'Etat versera à M. B... la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 6 : La présente décision sera notifiée à M. A... B... et au ministre de l'économie, des finances et de la relance.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
