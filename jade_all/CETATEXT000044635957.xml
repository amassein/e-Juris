<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044635957</ID>
<ANCIEN_ID>JG_L_2021_12_000000445560</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/63/59/CETATEXT000044635957.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème - 6ème chambres réunies, 29/12/2021, 445560, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-12-29</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>445560</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème - 6ème chambres réunies</FORMATION>
<TYPE_REC/>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BOUZIDI, BOUHANNA</AVOCATS>
<RAPPORTEUR>Mme Pearl Nguyên Duy</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:445560.20211229</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              M. C... G... a demandé au tribunal administratif de Lyon d'annuler pour excès de pouvoir, d'une part, la décision du 7 octobre 2016 par laquelle le ministre de l'intérieur a refusé de lui délivrer un agrément de directeur responsable du casino ainsi que la décision du 28 février 2017 rejetant son recours gracieux et, d'autre part, l'arrêté du 28 février 2017 par lequel le ministre de l'intérieur a retiré ses agréments en qualité d'employé de jeux et de membre de comité de direction d'un casino. Par un jugement n° 1703373 du 20 décembre 2018, le tribunal administratif a annulé ces deux décisions.<br/>
<br/>
              Par un arrêt n° 19LY00826 du 25 août 2020, la cour administrative d'appel de Lyon a rejeté l'appel formé par le ministre de l'intérieur contre ce jugement.<br/>
<br/>
              Par un pourvoi et un nouveau mémoire, enregistrés le 22 octobre 2020 et le 31 mai 2021 au secrétariat du contentieux du Conseil d'Etat, le ministre de l'intérieur demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de la sécurité intérieure ; <br/>
              - l'arrêté du 14 mai 2007 relatif à la réglementation des jeux dans les casinos ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Pearl Nguyên Duy, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Bouzidi, Bouhanna, avocat de M. C... G....<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que M. G..., qui était alors titulaire d'un agrément d'employé de jeux délivré le 2 novembre 2004 par le ministre de l'intérieur et d'un agrément de membre du comité de direction d'un casino délivré par le même ministre le 16 novembre 2010, a été condamné à trois ans d'emprisonnement par un arrêt du 15 novembre 2012 de la cour d'appel de Grenoble, pour avoir commis, en 2003, des faits d'agression sexuelle sur mineur, cet arrêt étant devenu définitif à la suite du rejet du pourvoi de l'intéressé par un arrêt du 18 décembre 2013 de la chambre criminelle de la Cour de cassation. M. G... ayant fait spontanément état de cette condamnation à l'occasion d'une demande, en 2015, d'un agrément de directeur responsable de casino, le ministre de l'intérieur a rejeté sa demande par une décision du 7 octobre 2016 et, par un arrêté du 28 février 2017, lui a retiré les deux agréments dont il disposait déjà en qualité d'employé de jeux et de membre de comité de direction de casino.<br/>
<br/>
              2. Le ministre de l'intérieur se pourvoit en cassation contre l'arrêt du 25 août 2020 par lequel la cour administrative d'appel de Lyon a rejeté son appel dirigé contre le jugement du 20 décembre 2018 par lequel le tribunal administratif de Lyon a, à la demande de M. G..., annulé l'ensemble de ces décisions. Par les moyens qu'il invoque, le ministre doit être regardé comme ne demandant l'annulation de cet arrêt qu'en tant qu'il annule son arrêté du 28 février 2017 retirant à M. G... son agrément d'employé de jeux et son agrément de membre de comité de direction de casino.<br/>
<br/>
              3. Aux termes du dernier alinéa de l'article L. 321-4 du code de la sécurité intérieure : " Le directeur et les membres du comité de direction et les personnes employées à un titre quelconque dans les salles de jeux sont agréés par le ministre de l'intérieur ". Le premier alinéa de l'article R. 321-29 du même code dispose que : " Le directeur responsable et les membres du comité de direction des casinos sont tenus de se conformer aux clauses du cahier des charges. Ils veillent, en permanence, à la sincérité des jeux et à la régularité de leur fonctionnement " et l'article R. 321-39 du même code prévoit, dans sa rédaction alors applicable, que les modalités d'application de ces dispositions sont déterminées par arrêté conjoint du ministre de l'intérieur et du ministre chargé du budget. Enfin, aux termes du V de l'article 12 de l'arrêté interministériel du 14 mai 2007 relatif à la réglementation des jeux dans les casinos, dans sa rédaction applicable à l'espèce : " Le directeur responsable et les membres du comité de direction sont agréés par le ministre de l'intérieur sous réserve de ne point remplir des fonctions électives dans la commune siège de l'établissement. (...) Les décisions du ministre de l'intérieur comportant agrément, retrait d'agrément (...) du directeur responsable ou d'un membre du comité de direction sont notifiées en toute hypothèse au directeur responsable et, le cas échéant, aux intéressés ". <br/>
<br/>
              4. Il résulte des dispositions citées ci-dessus que le ministre de l'intérieur, qui assure la police des cercles de jeux et des casinos, peut, à ce titre, retirer tout agrément délivré par lui sur le fondement l'article L. 321-4 du code de la sécurité intérieure au directeur, aux membres du comité de direction et, plus généralement, aux personnes employées à un titre quelconque dans les casinos et salles de jeu, lorsque leur titulaire ne remplit plus les conditions mises à son octroi ou pour des motifs d'ordre public, ainsi que le prévoit d'ailleurs désormais l'article R. 321-32-1 du même code. Il en va notamment ainsi lorsque le titulaire de l'agrément ne présente plus les garanties de probité et de moralité requises pour l'exercice de fonctions de direction, de membre de comité de direction ou d'employé de jeux au sein d'un casino. <br/>
<br/>
              5. Il ressort des pièces du dossier soumis aux juges du fond que pour retirer, dans l'exercice des pouvoirs de police rappelés ci-dessus, les agréments dont M. G... était titulaire en qualité d'employé de jeux et de membre du comité de direction d'un casino, le ministre de l'intérieur s'est notamment fondé sur ce qu'il avait, tant lors de ses demandes d'agrément, en 2004 et 2010, que postérieurement à sa condamnation par l'arrêt du 15 novembre 2012 de la cour d'appel de Grenoble, volontairement dissimulé à l'administration les faits d'agression sexuelle dont il s'était rendu coupable en 2003.<br/>
<br/>
              6. En jugeant, pour rejeter l'appel du ministre de l'intérieur, que cette absence de déclaration spontanée de la part de M. G... ne méconnaissait pas les exigences mentionnées au point 4 et ne pouvait, par suite, légalement justifier à elle seule le retrait des deux agréments qui lui avaient été délivrés, la cour administrative d'appel, compte tenu de ce que, premièrement, s'agissant de la dissimulation reprochée lors des demandes d'agrément effectuées en 2004 et 2010, aucune disposition n'imposait la communication à l'administration des faits en cause et de ce que, deuxièmement, s'agissant de la dissimulation reprochée au moment où la condamnation était intervenue, aucun texte ni aucun principe n'imposaient qu'il la fasse connaître à l'administration, n'a pas inexactement qualifié les faits qui lui étaient soumis.<br/>
<br/>
              7. Par ailleurs, il ressort des pièces du dossier soumis aux juges du fond que le ministre de l'intérieur a demandé à la cour administrative d'appel de substituer un motif de " fraude " au motif de dissimulation volontaire retenu par la décision attaquée. Toutefois, la modification ainsi demandée n'ayant pas pour effet de changer le motif de la décision en litige, elle ne peut être regardée comme une demande de substitution de motifs. Par suite, la cour n'étant pas saisie d'une demande de substitution de motif, le moyen tiré de ce que, en refusant d'y faire droit au motif qu'une telle substitution aurait privé l'intéressé d'une garantie procédurale, elle aurait entaché son arrêt d'une erreur de droit, ne peut qu'être écarté.<br/>
<br/>
              8. Il résulte de tout ce qui précède, sans qu'il soit besoin de se prononcer sur la fin de non-recevoir soulevée par M. G... à l'encontre du mémoire du ministre de l'intérieur du 31 mai 2021, que le ministre de l'intérieur n'est pas fondé à demander l'annulation de l'arrêt qu'il attaque.<br/>
<br/>
              9. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 3 000 euros à verser à M. G... au titre des dispositions de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
		Article 1er : Le pourvoi du ministre de l'intérieur est rejeté.<br/>
<br/>
Article 2 : L'Etat versera à M. G... une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 3 : La présente décision sera notifiée au ministre de l'intérieur et à M. C... G....<br/>
              Délibéré à l'issue de la séance du 17 décembre 2021 où siégeaient : Mme Christine Maugüé, présidente adjointe de la section du contentieux, présidant ; M. B... I..., M. Fanien Raynaud, présidents de chambre ; M. N... D..., Mme F... M..., M. E... K..., M. A... L... et Mme Bénédicte Fauvarque-Cosson, conseillers d'Etat et Mme Pearl Nguyên Duy, maître des requêtes-rapporteure. <br/>
<br/>
              Rendu le 29 décembre 2021.<br/>
<br/>
<br/>
<br/>
                 La présidente : <br/>
                 Signé : Mme Christine Maugüé<br/>
 		La rapporteure : <br/>
      Signé : Mme Pearl Nguyên Duy<br/>
                 La secrétaire :<br/>
                 Signé : Mme J... H...<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
