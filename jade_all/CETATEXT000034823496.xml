<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034823496</ID>
<ANCIEN_ID>JG_L_2017_05_000000405083</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/82/34/CETATEXT000034823496.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème - 3ème chambres réunies, 29/05/2017, 405083, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-05-29</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>405083</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème - 3ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Vincent Uher</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Romain Victor</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:405083.20170529</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et un nouveau mémoire, enregistrés les 16 novembre 2016 et 8 février 2017 au secrétariat du contentieux du Conseil d'Etat, la société Vivendi demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir les énonciations figurant au paragraphe n° 270 du commentaire administratif publié au Bulletin officiel des finances publiques - Impôts sous la référence BOI-BIC-PVMV-30-10-20120912, à titre principal en tant qu'elles prévoient que l'inscription de titres ouvrant droit au régime des sociétés mères dans un compte de titres de participation constitue une présomption irréfragable qui matérialise une décision de l'entreprise opposable à celle-ci ; <br/>
<br/>
              2°) à titre subsidiaire, si les énonciations du paragraphe n° 270 étaient regardées comme indissociables d'autres énonciations de l'instruction publiée sous la référence BOI-BIC-PVMV-30-10-20120912, d'annuler ces énonciations dans leur ensemble ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat une somme de 15 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - la loi n° 95-885 de finances rectificative pour 1995 du 4 août 1995 ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Vincent Uher, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Romain Victor, rapporteur public.<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes du I de l'article 219 du code général des impôts, dans sa rédaction en vigueur à la date du commentaire attaqué : " Pour le calcul de l'impôt, le (...) taux normal de l'impôt est fixé à 33,1/3 %./ Toutefois : a. Le montant net des plus-values à long terme fait l'objet d'une imposition séparée au taux de 19 %, dans les conditions prévues au 1 du I de l'article 39 quindecies et à l'article 209 quater./ Pour les exercices ouverts à compter du 1er  janvier 2005, le taux d'imposition visé au premier alinéa est fixé à 15 %. / (...) a ter. Le régime des plus-values et moins-values à long terme cesse de s'appliquer au résultat de la cession de titres du portefeuille réalisée au cours d'un exercice ouvert à compter du 1er janvier 1994 à l'exclusion des parts ou actions de sociétés revêtant le caractère de titres de participation (...) / Pour les exercices ouverts à compter de la même date, le régime des plus ou moins-values à long terme cesse également de s'appliquer en ce qui concerne les titres de sociétés dont l'actif est constitué principalement par des titres exclus de ce régime ou dont l'activité consiste de manière prépondérante en la gestion des mêmes valeurs pour leur propre compte (...) / Pour l'application des premier et deuxième alinéas, constituent des titres de participation les parts ou actions de sociétés revêtant ce caractère sur le plan comptable. Il en va de même des (...) titres ouvrant droit au régime des sociétés mères ou, lorsque leur prix de revient est au moins égal à 22 800 000 euros, qui remplissent les conditions ouvrant droit à ce régime autres que la détention de 5 % au moins du capital de la société émettrice, si ces actions ou titres sont inscrits en comptabilité au compte de titres de participation ou à une subdivision spéciale d'un autre compte du bilan correspondant à leur qualification comptable (...) / a quinquies. Pour les exercices ouverts à compter du 1er janvier 2006, le montant net des plus-values à long terme afférentes à des titres de participation fait l'objet d'une imposition séparée au taux de 8 %. Ce taux est fixé à 0 % pour les exercices ouverts à compter du 1er janvier 2007. (...) / Les titres de participation mentionnés au premier alinéa sont les titres de participation revêtant ce caractère sur le plan comptable (...) et les titres ouvrant droit au régime des sociétés mères si ces actions ou titres sont inscrits en comptabilité au compte titres de participation ou à une subdivision spéciale d'un autre compte du bilan correspondant à leur qualification comptable (...) ".<br/>
<br/>
              2. Il résulte de ces dispositions, éclairées par les travaux préparatoires de l'article 2 de la loi de finances rectificative pour 1995 du 4 août 1995 duquel est issu le 3ème alinéa du a ter de l'article 219 du code général des impôts, que des titres ouvrant droit au régime des sociétés mères sans revêtir, sur le plan comptable, le caractère de titres de participation sont soumis au régime fiscal des plus-values et moins-values à long terme à la condition qu'ils soient inscrits en comptabilité dans une subdivision spéciale d'un compte du bilan - autre qu'un compte de titres de participation - correspondant à leur qualification comptable telle que, notamment, un sous-compte de plus-values à long terme dans un compte de valeurs de placement. Il en résulte qu'une telle inscription, qui matérialise une décision de l'entreprise d'opter pour la possibilité que lui ouvre la loi fiscale de soumettre le gain de cession de ces titres au régime fiscal des plus et moins-values à long terme, a la nature d'une décision de gestion et constitue une présomption irréfragable opposable à celle-ci, comme à l'administration. <br/>
<br/>
              3. En revanche, il résulte de la première phrase du 3ème alinéa du a ter du I de l'article 219 du code général des impôts, également éclairée par les travaux préparatoires de l'article 2 de la loi de finances rectificative pour 1995 du 4 août 1995, que les titres qui revêtent, sur le plan comptable, le caractère de titres de participation, c'est-à-dire ceux dont la possession durable est estimée utile à l'activité de l'entreprise, notamment parce qu'elle permet d'exercer une influence sur la société émettrice des titres ou d'en assurer le contrôle, une telle utilité pouvant notamment être caractérisée si les conditions d'achat des titres en cause révèlent l'intention de l'acquéreur d'exercer une influence sur la société émettrice et lui donnent les moyens d'exercer une telle influence, sont nécessairement soumis au régime des plus-values et moins-values à long terme, que ces titres ouvrent droit ou non au régime des sociétés mères. Il en résulte qu'une inscription de ces titres en comptabilité dans un compte de titres de participation, qui est commandée par le respect de la réglementation comptable à laquelle se réfère la loi fiscale, ne matérialise aucune décision de gestion de l'entreprise. Une telle écriture comptable peut, si la qualification de titres de participation retenue s'avère erronée, être corrigée tant à l'initiative de l'administration que, sous réserve que cette erreur ne revête pas un caractère délibéré, de l'entreprise.  <br/>
<br/>
              4. Il résulte de ce qui précède qu'en ce qu'ils prévoient, dans leur paragraphe n° 270, que " dès lors que les titres ouvrent droit au régime des sociétés mères, l'inscription dans un compte de titres de participation (...) constitue une présomption irréfragable qui matérialise une décision de l'entreprise opposable à celle-ci comme à l'administration ", les commentaires administratifs publiés sous la référence BOI-BIC-PVMV-30-10-20120912 méconnaissent les dispositions mentionnées au point 1. La société Vivendi est, dès lors, fondée à demander l'annulation pour excès de pouvoir de ce paragraphe. <br/>
<br/>
              5. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat le versement à  la société Vivendi de la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
                               D E C I D E :<br/>
                                --------------<br/>
<br/>
Article 1er : Le paragraphe n° 270 des commentaires administratifs publiés au Bulletin officiel des finances publiques - Impôts sous la référence BOI-BIC-PVMV-30-10-20120912 est annulé.<br/>
Article 2 : L'Etat versera à la société Vivendi la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée à la société Vivendi et au ministre de l'action et des comptes publics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
