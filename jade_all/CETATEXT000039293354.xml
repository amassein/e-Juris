<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039293354</ID>
<ANCIEN_ID>JG_L_2019_10_000000430062</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/29/33/CETATEXT000039293354.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème - 2ème chambres réunies, 21/10/2019, 430062, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-10-21</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>430062</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème - 2ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Jean-Yves Ollier</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Gilles Pellissier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:430062.20191021</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et un nouveau mémoire, enregistrés les 21 avril et 26 septembre 2019 au secrétariat du contentieux du Conseil d'Etat, le Conseil national des barreaux demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret n° 2018-919 du 26 octobre 2018 relatif à l'expérimentation d'un dispositif de médiation en cas de différend entre les entreprises et les administrations ; <br/>
<br/>
              2°) d'annuler pour excès de pouvoir la décision implicite de rejet résultant du silence gardé par le Premier ministre sur sa demande d'abrogation de ce décret et la décision du 7 mars 2019 par laquelle la garde des Sceaux, ministre de la justice a refusé de proposer au Premier ministre d'abroger ce décret; <br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la Constitution ;<br/>
              - la loi n° 2018-727 du 10 août 2018 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Yves Ollier, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Gilles Pellissier, rapporteur public ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Aux termes de l'article 36 de la loi du 10 août 2018 pour un Etat au service d'une société de confiance : " Sans préjudice des dispositifs particuliers qui peuvent être sollicités par les entreprises, il est créé, à titre expérimental et pour une durée de trois ans à compter de la publication du décret prévu au deuxième alinéa du présent article, un dispositif de médiation visant à résoudre les différends entre, d'une part, les entreprises et, d'autre part, les administrations et les établissements publics de l'Etat, les collectivités territoriales et les organismes de sécurité sociale. Cette médiation respecte les règles relatives aux délais de recours et de prescription prévues à l'article L. 213-6 du code de justice administrative. / Un décret fixe les modalités de cette expérimentation, en particulier les régions où elle est mise en oeuvre et les secteurs économiques qu'elle concerne ". <br/>
<br/>
              2. Le décret attaqué, relatif à l'expérimentation d'un dispositif de médiation en cas de différend entre les entreprises et les administrations, a été pris pour l'application de ces dispositions. <br/>
<br/>
              3. En premier lieu, aux termes de l'article 22 de la Constitution : " Les actes du Premier ministre sont contresignés, le cas échéant, par les ministres chargés de leur exécution ". S'agissant d'un acte réglementaire, les ministres chargés de son exécution sont ceux qui ont compétence pour signer ou contresigner les mesures que comporte nécessairement l'exécution du décret. Si le décret a été pris aux fins de favoriser le recours aux modes alternatifs de règlement des litiges, le garde des Sceaux, ministre de la justice, n'a pas pour autant, contrairement à ce que soutient le Conseil national des barreaux, à signer ou à contresigner des mesures que comporterait nécessairement l'exécution du décret attaqué. Le moyen tiré de l'absence de contreseing du garde des Sceaux, ministre de la justice, ne peut, par suite, qu'être écarté. <br/>
<br/>
              4. En deuxième lieu, les dispositions de l'article 1er du décret attaqué, qui confient au médiateur des entreprises le soin d'assurer le dispositif de médiation visant à résoudre les différends entre les entreprises et les administrations prévu par l'article 36 de la loi du 10 août 2018, ressortissent à la définition des modalités de l'expérimentation que la loi a confiée au pouvoir réglementaire. Par suite, le moyen tiré de l'incompétence du pouvoir réglementaire pour confier une fonction de médiation au médiateur des entreprises doit être écarté.<br/>
<br/>
              5. En troisième lieu, le médiateur des entreprises, service du ministère de l'économie et des finances, a pour objet de proposer gratuitement à tous les acheteurs et à toutes les entreprises, quelles que soient leurs ressources, et donc notamment à ceux disposant de moyens limités, un processus organisé afin de parvenir, avec son aide, à la résolution amiable de leurs différends. En élargissant à titre expérimental la possibilité pour les entreprises, les collectivités publiques et les organismes de sécurité sociale de recourir au service du médiateur des entreprises, le décret attaqué s'est borné à mettre en oeuvre la mission d'intérêt général, qui relève de l'Etat, de développer les modes alternatifs de règlement des litiges, corollaire d'une bonne administration de la justice. En outre, les dispositions en cause n'instituent aucunement un monopole au profit du médiateur des entreprises, les parties demeurant libres de recourir au médiateur de leur choix. Ainsi, aucune des attributions confiées au médiateur des entreprises par le décret attaqué n'emporte intervention sur un marché. Par suite, les dispositions du décret attaqué n'ont pas eu pour effet de méconnaître le principe de la liberté du commerce et de l'industrie et le droit de la concurrence.<br/>
<br/>
              6. Enfin, aucune disposition législative ou réglementaire ne fait obstacle à ce que le dispositif de médiation prévu par l'article 36 de la loi du 10 août 2018 soit confié à une autorité rattachée à un ministre. Le médiateur des entreprises est, comme toute autorité administrative, soumis au principe d'impartialité. Par suite, il ne saurait être sérieusement soutenu que la désignation de cette autorité pour assurer une mission de médiation méconnaîtrait une exigence d'indépendance ou le principe d'impartialité.<br/>
<br/>
              7. Il résulte de tout ce qui précède que la requête du Conseil national des barreaux doit être rejetée. Les dispositions de l'article L. 761-1 du code de justice administrative font par suite obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante. <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête du Conseil national des barreaux est rejetée.<br/>
Article 2 : La présente décision sera notifiée au Conseil national des barreaux, au Premier ministre, à la garde des Sceaux, ministre de la justice, au ministre de l'économie et des finances et au ministre de l'action et des comptes publics<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
