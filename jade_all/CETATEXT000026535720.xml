<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026535720</ID>
<ANCIEN_ID>JG_L_2012_10_000000353189</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/53/57/CETATEXT000026535720.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  4ème sous-section jugeant seule, 24/10/2012, 353189, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2012-10-24</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>353189</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 4ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Marc Dandelot</PRESIDENT>
<AVOCATS>SCP ODENT, POULET ; SCP CELICE, BLANCPAIN, SOLTNER</AVOCATS>
<RAPPORTEUR>M. Christophe Eoche-Duval</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Gaëlle Dumortier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2012:353189.20121024</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la requête sommaire et le mémoire complémentaire, enregistrés le 7 octobre 2011 et le 9 janvier 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour la société Sadef, dont le siège est  34 rue de Reuilly à Paris (75012) ; la société Sadef demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision du 11 juillet 2011 par laquelle la Commission nationale d'aménagement commercial a accordé à la société civile de construction vente du " Bon raisin " l'autorisation préalable requise en vue de l'extension de 11 560 m² de la surface de vente de l'ensemble commercial de Loches (Indre-et-Loire) portant la surface totale de vente à 16 910 m² ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat  et de la société civile de construction vente du " Bon raisin" la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de commerce ;<br/>
<br/>
              Vu la loi n° 73-1193 du 27 décembre 1973 ;<br/>
<br/>
              Vu la loi n° 2008-776 du 4 août 2008 ;<br/>
<br/>
              Vu le décret n° 2008-1212 du 24 novembre 2008 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Christophe Eoche-Duval, Conseiller d'Etat, <br/>
<br/>
              - les observations de la SCP Célice, Blancpain, Soltner, avocat de la société civile de construction vente du " Bon raisin " et de la SCP Odent, Poulet, avocat de la société Sadef,<br/>
<br/>
              - les conclusions de Mme Gaëlle Dumortier, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Célice, Blancpain, Soltner avocat de la société civile de construction vente du " Bon Raisin " et à la SCP Odent, Poulet, avocat de la société Sadef ;<br/>
<br/>
<br/>
<br/>
              Sur la procédure devant la commission nationale :<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier, et notamment du procès verbal de la réunion de la commission nationale du 11 septembre 2011, que l'avis du ministre de l'économie et des finances a été recueilli, puis présenté par le commissaire du gouvernement à la Commission nationale de l'aménagement commercial, et que, dès lors, le moyen tiré d'une absence de l'avis du ministre de l'économie, des finances, et de l'industrie manque en fait ;<br/>
<br/>
              Sur l'appréciation de la commission nationale : <br/>
<br/>
              2. Considérant qu'aux termes de l'article L. 752-6 du code de commerce, dans sa rédaction issue de la loi du 4 août 2008 : " Lorsqu'elle statue sur l'autorisation d'exploitation commerciale visée à l'article L. 752-1, la commission départementale d'aménagement commercial se prononce sur les effets du projet en matière d'aménagement du territoire, de développement durable et de protection des consommateurs. Les critères d'évaluation sont : / 1° En matière d'aménagement du territoire : / a) L'effet sur l'animation de la vie urbaine, rurale et de montagne ; / b) L'effet du projet sur les flux de transport ; / c) Les effets découlant des procédures prévues aux articles L. 303-1 du code de la construction et de l'habitation et L. 123-11 du code de l'urbanisme ; / 2° En matière de développement durable : / a) La qualité environnementale du projet ; / b) Son insertion dans les réseaux de transports collectifs. " ; <br/>
<br/>
              3. Considérant qu'il résulte de ces dispositions combinées que l'autorisation d'aménagement commercial ne peut être refusée que si, eu égard à ses effets, le projet contesté compromet la réalisation des objectifs énoncés par la loi ; qu'il appartient aux commissions d'aménagement commercial, lorsqu'elles statuent sur les dossiers de demande d'autorisation, d'apprécier la conformité du projet à ces objectifs, au vu des critères d'évaluation mentionnés à l'article L. 752-6 précité ; <br/>
<br/>
              En ce qui concerne la prise en compte de l'évolution démographique de la zone de chalandise : <br/>
<br/>
              4. Considérant que, si la Commission nationale d'aménagement commercial fait référence à l'évolution de la population de la zone de chalandise, il résulte des termes de sa décision que celle-ci est fondée sur l'appréciation des critères mentionnés plus haut du code de commerce et non sur des facteurs démographiques ; <br/>
<br/>
              En ce qui concerne le respect des critères de l'article L. 752-6 du code de commerce :<br/>
<br/>
              5. Considérant, en premier lieu, qu'il ressort des pièces du dossier, que l'offre résultant du projet sera complémentaire de l'offre offerte dans les magasins du centre-ville de Loches, notamment dans le domaine culturel ; que la Commission nationale d'aménagement commercial n'a pas commis d'erreur d'appréciation en retenant que l'offre commerciale résultant du projet sera de nature différente de celle du centre-ville, et que dès lors le projet contesté n'aura pas pour conséquence de porter atteinte à l'animation de la vie urbaine du centre-ville de Loches ; <br/>
<br/>
              6. Considérant, en deuxième lieu, que si la société Sadef soutient que le projet litigieux occasionnera un accroissement des flux de véhicules automobiles et ne sera pas desservi par un réseau local de transports, il ressort des pièces du dossier que la commission nationale n'a pas commis d'erreur d'appréciation en estimant que cet accroissement ne justifiait pas un rejet de la demande, dès lors qu'en l'espèce, des aménagements de nature à y faire face étaient prévus ; <br/>
<br/>
              7. Considérant, en troisième lieu, que si la société Sadef soutient que le projet aura des effets négatifs en termes d'intégration paysagère, de performance énergétique et de lutte contre les pollutions diverses, ils n'assortissent ces affirmations d'aucun élément permettant d'en apprécier le bien fondé ;<br/>
<br/>
              8. Considérant qu'il résulte de ce qui précède que la Commission nationale d'aménagement commercial a fait une exacte application des dispositions législatives et réglementaires rappelées ci-dessus, et que la société Sadef n'est pas fondée à demander l'annulation de sa décision ;<br/>
<br/>
              Sur les conclusions tendant à l'application des dispositions de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              9. Considérant que ces dispositions font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat et de la société civile de construction vente du "  Bon raisin " qui ne sont pas, dans la présente instance, les parties perdantes ; qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la société Sadef la somme de 3 000 euros à verser à la société civile de construction vente " Bon raisin ", au titre de ces dispositions ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de la société Sadef est rejetée.<br/>
Article 2 : La société Sadef versera à la société civile de construction vente du " Bon raisin " une somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée à la société Sadef, à la société civile de construction vente du " Bon raisin " et à la Commission nationale d'aménagement commercial.<br/>
Copie en sera adressée pour information à la ministre de l'artisanat, du commerce et du tourisme<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
