<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042671447</ID>
<ANCIEN_ID>JG_L_2020_12_000000433639</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/67/14/CETATEXT000042671447.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème chambre, 11/12/2020, 433639, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-12-11</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>433639</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BUK LAMENT - ROBILLOT</AVOCATS>
<RAPPORTEUR>M. Olivier Rousselle</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Cécile Barrois de Sarigny</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2020:433639.20201211</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              La société Casa a demandé au tribunal administratif de Nouvelle Calédonie de condamner l'Etat à lui verser, en réparation des préjudices causés par le refus de concours de la force publique qui lui a été opposé à compter du 13 mai 2015, une somme calculée sur la base d'un montant de 1 500 000 F CFP par mois. Par un jugement n° 1800356 du 16 mai 2019, le tribunal administratif a rejeté sa demande.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 16 août et 18 novembre 2020 au secrétariat du contentieux du Conseil d'Etat, la société Casa demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la loi n° 68-1250 du 31 décembre 1968 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Olivier Rousselle, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Cécile Barrois de Sarigny, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Buk Lament-Robillot, avocat de la société Casa.<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que, par une ordonnance du 17 janvier 2001, le juge des référés du tribunal de première instance de Nouméa a prononcé l'expulsion d'occupants sans droit ni titre d'un terrain propriété de la société Casa. Après avoir sans succès réclamé préalablement à l'Etat l'indemnisation du préjudice résultant pour elle du refus du concours de la force publique qui lui est opposé depuis le 13 mai 2015, la société Casa a saisi le tribunal administratif de Nouvelle-Calédonie d'une demande indemnitaire qui a, par un jugement du 16 mai 2019, été rejetée comme tardive. La société Casa se pourvoit en cassation contre ce jugement.<br/>
<br/>
              2. Il résulte du principe de sécurité juridique que le destinataire d'une décision administrative individuelle qui a reçu notification de cette décision ou en a eu connaissance dans des conditions telles que le délai de recours contentieux ne lui est pas opposable doit, s'il entend obtenir l'annulation ou la réformation de cette décision, saisir le juge dans un délai raisonnable, qui ne saurait, en règle générale et sauf circonstances particulières, excéder un an. Toutefois, cette règle ne trouve pas à s'appliquer aux recours tendant à la mise en jeu de la responsabilité d'une personne publique qui, s'ils doivent être précédés d'une réclamation auprès de l'administration, ne tendent pas à l'annulation ou à la réformation de la décision rejetant tout ou partie de cette réclamation mais à la condamnation de la personne publique à réparer les préjudices qui lui sont imputés. La prise en compte de la sécurité juridique, qui implique que ne puissent être remises en cause indéfiniment des situations consolidées par l'effet du temps, est alors assurée par les règles de prescription prévues par la loi du 31 décembre 1968 relative à la prescription des créances sur l'Etat, les départements, les communes et les établissements publics, rendue applicable à la Nouvelle-Calédonie par l'article 11 de cette loi.<br/>
<br/>
              3. Il ressort des termes mêmes du jugement attaqué que, pour rejeter la demande de la société Casa, le tribunal administratif de Nouvelle-Calédonie s'est fondé d'une part sur le principe de sécurité juridique selon lequel le destinataire d'une décision administrative individuelle doit, s'il entend obtenir l'annulation ou la réformation de cette décision, saisir le juge dans un délai raisonnable, qui ne saurait, en règle générale et sauf circonstances particulières, excéder un an et d'autre part sur la circonstance que la société ne pouvait valablement ignorer que le silence gardé pendant deux mois sur sa demande d'indemnisation avait donné naissance à une décision implicite de rejet. Il résulte de ce qui a été dit ci-dessus qu'en statuant ainsi, le tribunal administratif a commis une erreur de droit. La société requérante est, par suite, fondée à demander l'annulation du jugement qu'elle attaque.<br/>
<br/>
              4. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat une somme de 3 000 euros à verser à la société Casa au titre de l'article L.761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
              			D E C I D E :<br/>
              		 	--------------<br/>
<br/>
Article 1er : Le jugement du tribunal administratif de Nouvelle-Calédonie du 16 mai 2019 est annulé. <br/>
<br/>
Article 2 : L'affaire est renvoyée au tribunal administratif de Nouvelle-Calédonie.<br/>
<br/>
Article 3 : L'Etat versera à la société Casa la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : La présente décision sera notifiée à la société Casa, au ministre de l'intérieur et au ministre des outre-mer.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
