<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039230801</ID>
<ANCIEN_ID>JG_L_2019_10_000000421616</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/23/08/CETATEXT000039230801.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème - 7ème chambres réunies, 16/10/2019, 421616</TITRE>
<DATE_DEC>2019-10-16</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>421616</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème - 7ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BOULLOCHE</AVOCATS>
<RAPPORTEUR>M. Paul Bernard</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Guillaume Odinet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:421616.20191016</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme E... C... et M. A... D... ont demandé au tribunal administratif de Paris d'annuler la décision du 26 mai 2015 par laquelle la garde des sceaux, ministre de la justice a refusé le changement du nom de leur fils B..., né le 4 juin 2009, de " D... " en " C...-D... " et d'enjoindre au garde des sceaux d'autoriser ce changement de nom ou, à défaut, de réexaminer leur demande dans un délai de six mois à compter du jugement à intervenir.  <br/>
<br/>
              Par un jugement n° 1517971 du 19 janvier 2017, le tribunal administratif a rejeté leur demande. <br/>
<br/>
              Par un arrêt n° 17PA00953 du 12 avril 2018, la cour administrative d'appel de Paris a rejeté l'appel qu'ils ont formé contre ce jugement. <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 19 juin et 19 septembre 2018 au secrétariat du contentieux du Conseil d'Etat, Mme C... et M. D... demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à leur appel ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 4 000 euros sur le fondement des dispositions de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - le code civil ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Paul Bernard, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Guillaume Odinet, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Boulloche, avocat de Mme C... et de M. D... ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. Mme C... et M. D... ont, par une requête publiée au Journal officiel de la République française le 19 novembre 2010, saisi le garde des sceaux, ministre de la justice, d'une demande tendant à ce que soit adjoint au nom de leur fils, B... D..., né le 4 juin 2009, le nom de sa mère, C.... Cette demande a été rejetée par une décision du 26 mai 2015. Par un jugement du 19 janvier 2017, le tribunal administratif de Paris a rejeté la requête présentée par Mme C... et M. D... à l'encontre de cette décision. Mme C... et M. D... se pourvoient en cassation contre l'arrêt du 12 avril 2018 par lequel la cour administrative d'appel de Paris a rejeté l'appel qu'ils ont formé contre ce jugement. <br/>
<br/>
              2. L'article 61 du code civil dispose que : " Toute personne qui justifie d'un intérêt légitime peut demander à changer de nom (...). Le changement de nom est autorisé par décret ". Des motifs d'ordre affectif peuvent, dans des circonstances exceptionnelles, caractériser l'intérêt légitime requis par l'article 61 du code civil pour déroger aux principes de dévolution et de fixité du nom établis par la loi. <br/>
<br/>
              3. Selon les faits présentés à la cour par Mme C... et M. D..., dont l'inexactitude ne ressortait pas des pièces du dossier et dont le garde des sceaux, qui n'avait pas produit de mémoire malgré une mise en demeure, était réputé avoir admis l'exactitude, les requérants avaient décidé, avant la naissance de leur fils B..., que celui-ci se voie octroyer leurs deux noms accolés. A la suite d'un accouchement difficile, M. D... a déclaré l'enfant en lui attribuant son seul nom parce que l'officier d'état civil auprès duquel la déclaration de naissance a été effectuée l'avait fortement dissuadé d'opérer un tel choix, au regard notamment des importants " tracas administratifs " ultérieurs que celui-ci pourrait occasionner pour l'enfant. M. D... et Mme C... ont présenté une demande tendant au changement du nom de leur enfant environ un an et demi après la naissance, alors que les complications consécutives à l'accouchement avaient perduré durant plus d'un an et conduit Mme C... à subir plusieurs interventions pendant cette période. <br/>
<br/>
              4. En jugeant, sur la base de ces éléments, que Mme C... et M. D... ne justifiaient pas, faute de circonstances exceptionnelles, d'un intérêt légitime à demander que leur enfant porte leurs deux noms accolés en lieu et place du seul nom de son père, sous lequel il avait été déclaré à la naissance, la cour administrative d'appel de Paris a inexactement qualifié les faits de l'espèce. Par suite et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, l'arrêt attaqué doit être annulé. <br/>
<br/>
              5. Dans les circonstances de l'espèce, il y a lieu, en application des dispositions de l'article L. 821-2 du code de justice administrative, de régler l'affaire au fond.<br/>
<br/>
              6. Il ressort des pièces du dossier que, lors de la déclaration de naissance de son fils auprès des services de l'état civil de Nice, alors que sa volonté et celle de la mère de l'enfant, indiquée à de multiples reprises, notamment auprès du personnel de l'hôpital où l'accouchement a eu lieu, était de donner au jeune B... les noms de famille accolés de ses deux parents, M. D... en a été dissuadé par l'officier d'état civil et que, déstabilisé par l'insistance de ce dernier et les circonstances de l'accouchement difficile subi par Mme C..., il n'a pas déposé la déclaration conjointe rédigée en ce sens, ce qui a eu pour effet de conférer à l'enfant, en application de l'article 311-21 du code civil, le seul nom de son père. De telles circonstances doivent être regardées comme exceptionnelles et caractérisent, eu égard au motif invoqué, un intérêt légitime au sens de l'article 61 du code civil justifiant le changement du nom de l'enfant B... D... en " C...-D... ". Il suit de là que Mme C... et M. D... sont fondés à soutenir que c'est à tort que, par le jugement qu'ils contestent, le tribunal administratif de Paris a rejeté leur demande tendant à l'annulation de la décision du 26 mai 2015 par laquelle la garde des sceaux, ministre de la justice, a rejeté leur demande de changement de nom.<br/>
<br/>
              7. Il résulte de tout ce qui précède que le jugement du 19 janvier 2017 et la décision du 26 mai 2015 doivent être annulés. <br/>
<br/>
              8. Dans les circonstances de l'espèce, il y a lieu d'enjoindre à la garde des sceaux, ministre de la justice, de réexaminer la demande de changement de nom présentée par Mme C... et M. D... dans un délai de trois mois à compter de la présente décision.<br/>
<br/>
              9. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 3 000 euros à verser à Mme C... et M. D... au titre des dispositions de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 12 avril 2018 de la cour administrative d'appel de Paris, le jugement du 19 janvier 2017 du tribunal administratif de Paris et la décision du 26 mai 2015 de la garde des sceaux, ministre de la justice, sont annulés. <br/>
Article 2 : Il est enjoint à la garde des sceaux, ministre de la justice, de procéder, dans un délai de trois mois à compter de la notification de la présente décision, au réexamen de la demande de Mme C... et M. D....<br/>
Article 3 : L'Etat versera à Mme C... et à M. D... la somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative. <br/>
Article 4 : La présente décision sera notifiée à Mme E... C..., à M. A... D... et à la garde des sceaux, ministre de la justice. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">26-01-03 DROITS CIVILS ET INDIVIDUELS. ÉTAT DES PERSONNES. CHANGEMENT DE NOM PATRONYMIQUE. - MOTIFS D'ORDRE AFFECTIF CARACTÉRISANT, DANS DES CIRCONSTANCES EXCEPTIONNELLES, UN INTÉRÊT LÉGITIME AU CHANGEMENT DE NOM [RJ1] - EXISTENCE EN L'ESPÈCE [RJ2].
</SCT>
<ANA ID="9A"> 26-01-03 Des motifs d'ordre affectif peuvent, dans des circonstances exceptionnelles, caractériser l'intérêt légitime requis par l'article 61 du code civil pour déroger aux principes de dévolution et de fixité du nom établis par la loi.... ,,Les requérants avaient décidé, avant la naissance de leur fils Clément, que celui-ci se voie octroyer leurs deux noms accolés. A la suite d'un accouchement difficile, le père a déclaré l'enfant en lui attribuant son seul nom parce que l'officier d'état civil auprès duquel la déclaration de naissance a été effectuée l'avait fortement dissuadé d'opérer un tel choix, au regard notamment des importants  tracas administratifs  ultérieurs que celui-ci pourrait occasionner pour l'enfant. Les parents ont présenté une demande tendant au changement du nom de leur enfant environ un an et demi après la naissance, alors que les complications consécutives à l'accouchement avaient perduré durant plus d'un an et conduit la mère à subir plusieurs interventions pendant cette période.,,,De telles circonstances doivent être regardées comme exceptionnelles et caractérisent, eu égard au motif invoqué, un intérêt légitime au sens de l'article 61 du code civil justifiant le changement du nom de l'enfant afin de lui attribuer les noms de famille accolés de ses deux parents.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 31 janvier 2014, MM.,, n° 362444, p. 11 ; CE, 16 mai 2018, Mme,, n° 409656, T. p. 683.,,[RJ2] Rappr., pour une espèce proche dans laquelle un motif d'ordre affectif a été regardé comme caractérisant un intérêt légitime à changer de nom, CE, 12 décembre 2012, Garde des sceaux, ministre de la justice c/ Mlle,, n° 357865, T. p. 752.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
