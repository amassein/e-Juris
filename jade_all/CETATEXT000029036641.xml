<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029036641</ID>
<ANCIEN_ID>JG_L_2014_06_000000361281</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/03/66/CETATEXT000029036641.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème SSJS, 02/06/2014, 361281, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-06-02</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>361281</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème SSJS</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GARREAU, BAUER-VIOLAS, FESCHOTTE-DESBOIS</AVOCATS>
<RAPPORTEUR>M. Christophe Pourreau</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Marie-Astrid Nicolazo de Barmon</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2014:361281.20140602</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi, enregistré le 23 juillet 2012 au secrétariat du contentieux du Conseil d'Etat, présenté par la Garde des sceaux, ministre de la justice ; la Garde des sceaux, ministre de la justice demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 10MA02811 du 22 mai 2012 par lequel la cour administrative d'appel de Marseille, après avoir annulé le jugement n° 0900984 du 17 décembre 2009 par lequel le tribunal administratif de Nîmes a rejeté la requête de Mme B... tendant à l'annulation de l'arrêté du 7 janvier 2009 par lequel le directeur de l'administration pénitentiaire a infligé à cette dernière la sanction disciplinaire d'exclusion temporaire de ses fonctions pour une durée de 24 mois, a annulé cet arrêté ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à ses conclusions d'appel ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ; <br/>
<br/>
              Vu le code de procédure pénale ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Christophe Pourreau, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Marie-Astrid Nicolazo de Barmon, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Garreau, Bauer-Violas, Feschotte-Desbois, avocat de Mme A...B...;<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que Mme A...B..., adjointe administrative principale affectée en 2002 à la maison d'arrêt de Nîmes, a noué une relation avec un ancien détenu de cet établissement qu'elle a rencontré en juillet 2008 ; que, par un arrêté du 7 janvier 2009, la Garde des sceaux, ministre de la justice a prononcé à son encontre la sanction disciplinaire d'exclusion temporaire de ses fonctions pour une durée de 24 mois au motif qu'elle avait méconnu l'article D. 221 du code de procédure pénale ; que, le 6 avril 2009, Mme B... a demandé au tribunal administratif de Nîmes d'annuler cet arrêté ; que la Garde des sceaux, ministre de la Justice se pourvoit en cassation contre l'arrêt du 22 mai 2012 par lequel la cour administrative d'appel de Marseille, après avoir annulé le jugement du tribunal administratif de Nîmes du 17 décembre 2009 rejetant sa demande, a annulé l'arrêté du 7 janvier 2009 ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales : " 1. Toute personne a droit au respect de sa vie privée et familiale, de son domicile et de sa correspondance. / 2. Il ne peut y avoir ingérence d'une autorité publique dans l'exercice de ce droit que pour autant que cette ingérence est prévue par la loi et qu'elle constitue une mesure qui, dans une société démocratique, est nécessaire à la sécurité nationale, à la sûreté publique, au bien-être économique du pays, à la défense de l'ordre et à la prévention des infractions pénales, à la protection de la santé ou de la morale, ou à la protection des droits et libertés d'autrui. " ;<br/>
<br/>
              3. Considérant qu'aux termes de l'article D. 221 du code de procédure pénale : " Les membres du personnel et les personnes remplissant une mission dans l'établissement pénitentiaire ne peuvent entretenir avec les personnes placées ou ayant été placées par décision de justice sous l'autorité ou le contrôle de l'établissement ou du service dont ils relèvent, ainsi qu'avec leurs parents ou amis, des relations qui ne sont pas justifiées par les nécessités de leurs fonctions. " ;<br/>
<br/>
              4. Considérant que, si l'interdiction, pour les personnes physiques et les agents des personnes morales concourant au service public pénitentiaire, d'entretenir avec les personnes détenues, leurs parents ou amis, des relations qui ne seraient pas justifiées par les nécessités de leur mission répond à des impératifs tenant à la sécurité à l'intérieur de l'établissement et à l'égalité de traitement entre les personnes détenues ainsi qu'à la nécessité de protéger les droits et libertés de la personne détenue, placée, lorsqu'elle est en détention, dans une situation de vulnérabilité vis-à-vis des personnes concourant au service public pénitentiaire et si, par suite, l'article D. 221 du code de procédure pénale n'apporte pas, dans cette mesure, une ingérence excessive à l'exercice du droit au respect à la vie privée et familiale des personnes physiques et agents des personnes morales concourant au service public pénitentiaire, en revanche, en étendant cette interdiction aux personnes ayant été placées par décision de justice sous l'autorité ou le contrôle de l'établissement ou du service dont ils relèvent ainsi qu'avec leurs parents ou amis, l'article précité instaure une interdiction générale, de caractère absolu et sans aucune limitation de durée, qui impose des sujétions excessives au regard des stipulations de l'article 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ; qu'ainsi, la cour administrative d'appel de Marseille n'a pas commis d'erreur de droit en jugeant que les dispositions de l'article D. 221 du code de procédure pénale, en tant qu'elles interdisent de manière générale et absolue, sans limitation de durée, à un membre du personnel d'un établissement pénitentiaire toute relation personnelle avec un détenu ayant purgé sa peine dans cet établissement, ainsi qu'avec les parents et amis de ce détenu, méconnaissent ces stipulations ; que par suite le Garde des sceaux, ministre de la justice, n'est pas fondée à demander l'annulation de l'arrêt attaqué ;<br/>
<br/>
              5. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat une somme de 3 000 euros, à verser à Mme B..., au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de la Garde des sceaux, ministre de la justice est rejeté.<br/>
Article 2 : L'Etat versera à Mme B...une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
Article 3 : La présente décision sera notifiée à Mme A... B...et à la Garde des sceaux, ministre de la justice.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
