<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028077630</ID>
<ANCIEN_ID>JG_L_2013_10_000000359098</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/07/76/CETATEXT000028077630.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  6ème sous-section jugeant seule, 16/10/2013, 359098, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-10-16</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>359098</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 6ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP WAQUET, FARGE, HAZAN ; LE PRADO</AVOCATS>
<RAPPORTEUR>M. Marc Pichon de Vendeuil</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Suzanne Von Coester</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2013:359098.20131016</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 2 mai et 2 août 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. B...C..., demeurant ... ; M. C...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 08BX02785-10BX02787 du 28 février 2012 de la cour administrative d'appel de Bordeaux en tant qu'il a rejeté, d'une part, après avoir annulé l'ordonnance n° 0501519 du tribunal administratif de Bordeaux du 11 octobre 2005, ses conclusions tendant à l'annulation de la décision du 23 novembre 2004 par laquelle le maire de Mérignac a délivré à la SCI Mériland un permis de construire deux bâtiments ainsi que de la décision implicite de rejet de son recours gracieux formé contre cet arrêté, d'autre part, sa requête tendant à l'annulation du jugement n° 0503306 du 31 juillet 2008 par lequel le même tribunal a rejeté sa demande tendant à l'annulation du permis de construire modificatif du 1er  juillet 2005 ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à ses conclusions d'appel ;<br/>
<br/>
              3°) de mettre à la charge de la commune de Mérignac et de la SCI Mériland la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de l'urbanisme ;<br/>
<br/>
              Vu le décret n° 2006-958 du 31 juillet 2006 ;<br/>
<br/>
              Vu la décision du 23 novembre 2011 par laquelle le Conseil d'Etat statuant au contentieux n'a pas renvoyé au Conseil Constitutionnel la question prioritaire de constitutionalité soulevée par M.C... ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Marc Pichon de Vendeuil, Maître des Requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Suzanne von Coester, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Waquet, Farge, Hazan, avocat de M. C...et à Me Le Prado, avocat de la commune de Mérignac ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, par arrêté du 23 novembre 2004, le maire de Mérignac (Gironde) a délivré à la SCI Mériland un permis de construire deux bâtiments, dont une maison individuelle et un immeuble collectif composé de logements et d'un bureau ; qu'un permis de construire modificatif a été délivré à la SCI Mériland le 1er juillet 2005 ; que M. C...a successivement présenté au tribunal administratif de Bordeaux deux demandes tendant à l'annulation du permis de construire initial et du permis modificatif, qui ont respectivement été rejetées par une ordonnance du vice-président de ce tribunal du 11 octobre 2005 et par un jugement du 31 juillet 2008 ; que, par arrêt du 14 janvier 2008, la cour administrative d'appel de Bordeaux a rejeté l'appel formé par M. C... contre l'ordonnance du 11 octobre 2005 ; que par décision du 18 octobre 2010, le Conseil d'Etat a annulé l'arrêt du 14 janvier 2008 et a renvoyé l'affaire devant la cour administrative d'appel de Bordeaux ; que M. C...se pourvoit en cassation contre l'arrêt du 28 février 2012 par lequel la cour administrative d'appel de Bordeaux a rejeté, d'une part, après annulation de l'ordonnance du tribunal administratif de Bordeaux du 11 octobre 2005 et évocation, ses conclusions tendant à l'annulation de la décision du 23 novembre 2004 par laquelle le maire de Mérignac a délivré à la SCI Mériland un permis de construire, d'autre part, sa requête tendant à l'annulation du jugement du 31 juillet 2008 par lequel le même tribunal a rejeté sa demande tendant à l'annulation du permis de construire modificatif du 1er juillet 2005 ;<br/>
<br/>
              2. Considérant qu'aux termes du deuxième alinéa de l'article R. 741-2 du code de justice administrative, une décision " contient le nom des parties, l'analyse des conclusions et mémoires (...) " ; qu'il ressort de la procédure suivie devant la cour administrative d'appel de Bordeaux que, en méconnaissance de ces dispositions, celle-ci a omis de viser le mémoire, présenté devant elle par M. C...le 20 mai 2011, qui contenait des conclusions à fins de non-lieu auxquelles son arrêt n'a pas répondu ; que, par suite et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, M. C...est fondé à soutenir que l'arrêt de la cour administrative d'appel de Bordeaux est entaché d'irrégularité et à en demander l'annulation ;<br/>
<br/>
              3. Considérant qu'aux termes du second alinéa de l'article L. 821-2 du code de justice administrative : " Lorsque l'affaire fait l'objet d'un second pourvoi en cassation, le Conseil d'Etat statue définitivement sur cette affaire " ; qu'il y a lieu, par suite, de régler l'affaire au fond ;<br/>
<br/>
              4. Considérant que les requêtes n° 05BX02303 et 08BX02785 présentées par M. C...présentent à juger des questions semblables ; qu'il y a lieu de les joindre pour statuer par une seule décision ;<br/>
<br/>
              Sur la requête d'appel dirigée contre l'ordonnance du 11 octobre 2005 ayant rejeté la demande d'annulation du permis de construire délivré le 23 novembre 2004 :<br/>
<br/>
              En ce qui concerne la recevabilité de la requête :<br/>
<br/>
              5. Considérant qu'aux termes de l'article R. 600-1 du code de l'urbanisme, dans sa rédaction alors applicable : " En cas de déféré du préfet ou de recours contentieux à l'encontre d'un document d'urbanisme ou d'une décision relative à l'occupation ou l'utilisation du sol régie par le présent code, le préfet ou l'auteur du recours est tenu, à peine d'irrecevabilité, de notifier son recours à l'auteur de la décision et, s'il y a lieu, au titulaire de l'autorisation. Cette notification doit également être effectuée dans les mêmes conditions en cas de demande tendant à l'annulation ou à la réformation d'une décision juridictionnelle concernant un document d'urbanisme ou une décision relative à l'occupation ou l'utilisation du sol. L'auteur d'un recours administratif est également tenu de le notifier à peine d'irrecevabilité du recours contentieux qu'il pourrait intenter ultérieurement en cas de rejet du recours administratif. / La notification prévue au précédent alinéa doit intervenir par lettre recommandée avec accusé de réception, dans un délai de quinze jours francs à compter du dépôt du déféré ou du recours. / La notification du recours à l'auteur de la décision et, s'il y a lieu, au titulaire de l'autorisation est réputée accomplie à la date d'envoi de la lettre recommandée avec accusé de réception. Cette date est établie par le certificat de dépôt de la lettre recommandée auprès des services postaux. " ; <br/>
<br/>
              6. Considérant qu'il ressort des pièces du dossier que M. C...a notifié sa requête d'appel enregistrée le 29 novembre 2005 au greffe de la cour administrative d'appel de Bordeaux sous le n° 05BX02303 à la commune de Mérignac et à la SCI Mériland par courriers recommandés avec accusé de réception envoyés le même jour ; que, dès lors, la fin de non-recevoir opposée par la SCI Mériland doit être écartée ; <br/>
<br/>
              En ce qui concerne la régularité de l'ordonnance du 11 octobre 2005 :<br/>
<br/>
              7. Considérant qu'aux termes de l'article R. 222-1 du code de justice administrative, dans sa rédaction alors applicable : " Les présidents de tribunal administratif et de cour administrative d'appel, le vice-président du tribunal administratif de Paris et les présidents de formation de jugement des tribunaux et des cours peuvent, par ordonnance : / (...) 4° Rejeter les requêtes (...) entachées d'une irrecevabilité manifeste non susceptible d'être couverte en cours d'instance (...) " ;<br/>
<br/>
              8. Considérant qu'il résulte des termes mêmes du 4° de l'article R. 222-1 du code de justice administrative, dans sa rédaction applicable en l'espèce, que la possibilité de rejeter par ordonnance une requête manifestement irrecevable visait alors les irrecevabilités " non susceptibles d'être couvertes en cours d'instance " ; que même si, en réponse à la demande de régularisation que le greffe du tribunal administratif lui avait adressée, le requérant avait produit des documents qui ne justifiaient pas de l'accomplissement de la formalité requise par l'article R. 600-1 du code de l'urbanisme en ce qui concerne le permis qui faisait l'objet de sa demande, cette irrecevabilité pouvait être couverte par le requérant de sa propre initiative, jusqu'à la clôture de l'instruction, en produisant les documents en cause ; que par suite, le vice-président du tribunal administratif ne pouvait rejeter la demande formée devant lui par ordonnance sur le fondement des dispositions du 4° de l'article R. 222-1 du code de justice administrative, dans leur version alors applicable ; que, par suite, M. C...est fondé à soutenir que l'ordonnance du 11 octobre 2005 méconnaît les dispositions précitées de l'article R. 222-1 du code de justice administrative et à en demander l'annulation ; <br/>
<br/>
              9. Considérant qu'il y a lieu d'annuler l'ordonnance attaquée et de statuer immédiatement, par la voie de l'évocation, sur la demande présentée par M. C...devant le tribunal administratif ; <br/>
<br/>
              En ce qui concerne les conclusions à fin de non-lieu :<br/>
<br/>
              10. Considérant qu'aux termes du premier alinéa de l'article R. 421-32 du code de l'urbanisme dans sa rédaction en vigueur à la date d'édiction du permis litigieux : " Le permis de construire est périmé si les constructions ne sont pas entreprises dans le délai de deux ans à compter de la notification visée à l'article R. 421-34 ou de la délivrance tacite du permis de construire (...) " ; qu'aux termes du quatrième alinéa de ce même article, dans sa rédaction issue du décret du 31 juillet 2006 relatif aux règles de caducité du permis de construire et modifiant le code de l'urbanisme : " Lorsque le permis de construire fait l'objet d'un recours en annulation devant la juridiction administrative ou d'un recours devant la juridiction civile en application de l'article L. 480-13, le délai de validité de ce permis est suspendu jusqu'à la notification de la décision juridictionnelle irrévocable. " ; que l'article 2 du décret du 31 juillet 2006 prévoit que ces dispositions s'appliquent aux permis de construire en cours de validité à la date de sa publication ; que, dès lors que le permis de construire litigieux, délivré le 23 novembre 2004, était valide à la date d'entrée en vigueur de ces dispositions et faisait l'objet à cette même date d'un recours en annulation devant la juridiction administrative, son délai de validité doit être regardé comme ayant été suspendu jusqu'à la notification de la décision juridictionnelle irrévocable rendue sur ce recours ; que, par suite, les conclusions présentées en appel tendant à ce que le juge constate la caducité du permis de construire ne peuvent qu'être rejetées ;<br/>
<br/>
              En ce qui concerne la fin de non-recevoir opposée à la demande : <br/>
<br/>
              11. Considérant qu'il ressort des pièces du dossier que M. C...a notifié le 19 janvier 2005 à la SCI Mériland le recours gracieux qu'il a formé le même jour devant le maire de Mérignac contre le permis de construire du 23 novembre 2004 ; qu'il a également notifié sa demande présentée le 19 avril 2005 devant les premiers juges à la commune et à la SCI Mériland, qui en ont accusé réception les 22 et 21 avril 2005 ; que, dès lors, la fin de non recevoir tirée du non-respect de la formalité prévue par l'article R. 600-1 du code de l'urbanisme doit être écartée ;<br/>
<br/>
              En ce qui concerne la légalité externe :<br/>
<br/>
              12. Considérant, en premier lieu, que la circonstance qu'à la suite d'une simple erreur matérielle, la demande de permis de construire mentionne une superficie du terrain d'assiette de 2980 m², au lieu de 2395 m², n'est pas de nature à entacher d'illégalité le permis litigieux ; <br/>
<br/>
              13. Considérant, en deuxième lieu, qu'aux termes de l'article L. 421-2 du code de l'urbanisme, dans sa rédaction applicable en l'espèce : " A. Le dossier joint à la demande de permis de construire comporte : / 1° Le plan de situation du terrain ; / 2° Le plan de masse des constructions à édifier ou à modifier coté dans les trois dimensions, des travaux extérieurs à celles-ci et des plantations maintenues, supprimées ou créées ; / 3° Les plans des façades ; / 4° Une ou des vues en coupe précisant l'implantation de la construction par rapport au terrain naturel à la date du dépôt de la demande de permis de construire et indiquant le traitement des espaces extérieurs ; / 5° Deux documents photographiques au moins permettant de situer le terrain respectivement dans le paysage proche et lointain et d'apprécier la place qu'il y occupe. Les points et les angles des prises de vue seront reportés sur le plan de situation et le plan de masse ; / 6° Un document graphique au moins permettant d'apprécier l'insertion du projet de construction dans l'environnement, son impact visuel ainsi que le traitement des accès et des abords. Lorsque le projet comporte la plantation d'arbres de haute tige, les documents graphiques devront faire apparaître la situation à l'achèvement des travaux et la situation à long terme ; / 7° Une notice permettant d'apprécier l'impact visuel du projet. A cet effet, elle décrit le paysage et l'environnement existants et expose et justifie les dispositions prévues pour assurer l'insertion dans ce paysage de la construction, de ses accès et de ses abords (...) " ; <br/>
<br/>
              14. Considérant qu'il ressort des pièces du dossier que, si le dossier de demande de permis de construire déposée le 30 juin 2004 auprès des services de la mairie de Mérignac est lacunaire s'agissant de l'insertion du projet de construction dans son environnement, le dossier de demande de permis modificatif comprend une notice " volet paysager " qui décrit la situation du terrain, la nature et l'aspect des constructions et l'insertion du projet dans le paysage existant, accompagnée de photographies et dessins montrant la construction projetée et son insertion parmi les constructions environnantes ; qu'ainsi, la délivrance du permis de construire modificatif accordé le 1er juillet 2005 a eu pour effet de régulariser le permis initial ; <br/>
<br/>
              15. Considérant, en troisième lieu, que le moyen tiré de ce que l'avis de l'architecte des bâtiments de France aurait été rendu postérieurement à la délivrance du permis de construire est inopérant, dès lors qu'il ne ressort pas des pièces du dossier et qu'il n'est pas allégué que le permis de construire contesté devait être soumis à l'avis de l'architecte des bâtiments de France ;<br/>
<br/>
<br/>
              En ce qui concerne la légalité interne :<br/>
<br/>
              S'agissant de l'atteinte au caractère des lieux avoisinants :<br/>
<br/>
              16. Considérant qu'aux termes de l'article R. 111-21 du code de l'urbanisme : " Le permis de construire peut être refusé ou n'être accordé que sous réserve de l'observation de prescriptions spéciales si les constructions, par leur situation, leur architecture, leurs dimensions ou l'aspect extérieur des bâtiments ou ouvrages à édifier ou à modifier, sont de nature à porter atteinte au caractère ou à l'intérêt des lieux avoisinants, aux sites, aux paysages naturels ou urbains ainsi qu'à la conservation des perspectives monumentales. " ; <br/>
<br/>
              17. Considérant qu'il ne ressort pas des pièces du dossier que les constructions projetées seraient, par leur situation, leur architecture, leurs dimensions ou l'aspect extérieur des bâtiments, de nature à porter atteinte au caractère ou à l'intérêt des lieux avoisinants, lesquels ne présentent aucune particularité notable ; que, dans ces conditions, en délivrant le permis de construire litigieux, le maire de Mérignac n'a entaché l'appréciation à laquelle il s'est livré au regard des dispositions citées ci-dessus d'aucune erreur manifeste ;<br/>
<br/>
<br/>
<br/>
              S'agissant de l'implantation des constructions :<br/>
<br/>
              18. Considérant qu'aux termes de l'article UB c à g 7 du plan d'occupation des sols alors applicable : " Les parcelles inférieures ou égales à 10 m de largeur de façades seront construites obligatoirement en ordre continu. /1.1. En ordre continu et semi-continu : / Les constructions seront édifiées d'une limite latérale à l'autre sur une profondeur maximum de 16 m, parallèle à l'alignement, à partir de celui-ci ou du retrait imposé ; elles devront respecter un retrait minimum égal à leur hauteur diminuée de 4 m (A...- 4) avec un minimum de 4 m par rapport aux autres limites séparatives. (...) / Les constructions inférieures ou égales à 30 m² sont autorisées au-delà de la bande de 16 m, en contigüité des limites séparatives, elles ne devront pas excéder 3,50 m de hauteur sur celle-ci. " ; <br/>
<br/>
              19. Considérant, en premier lieu, qu'il ressort des dispositions précitées que l'obligation de construire en ordre continu n'est édictée que pour les parcelles inférieures ou égales à 10 mètres de largeur de façades ; qu'elle ne s'imposait dès lors pas à la construction projetée, dont la largeur de façade dépasse 11 mètres ; <br/>
<br/>
              20. Considérant, en deuxième lieu, que les dispositions rappelées ci-dessus du plan d'occupation des sols n'imposent pas une obligation de construction d'une limite latérale à l'autre, mais au contraire n'autorisent une telle construction que dans la limite d'une profondeur de 16 mètres ; qu'ainsi, M. C...n'est pas fondé à soutenir que la construction projetée méconnaîtrait l'obligation de construire d'une limite latérale à l'autre ;<br/>
<br/>
              21. Considérant, en troisième lieu, que si ces dispositions limitent, lorsqu'elles sont édifiées au-delà de la bande des 16 mètres et en contigüité des limites séparatives, les constructions à une surface de 30 m² et à une hauteur de 3,5 mètres, elles ne sont pas applicables à la construction litigieuse qui n'est pas édifiée en contigüité des limites séparatives ;<br/>
<br/>
              22. Considérant, enfin, que M. C...ne peut utilement invoquer les dispositions des articles UP 6 et 7 du règlement du plan local d'urbanisme, lequel n'était pas en vigueur à la date de l'arrêté contesté ; <br/>
<br/>
              S'agissant de la méconnaissance de l'article L. 123-6 du code de l'urbanisme :<br/>
<br/>
              23. Considérant qu'aux termes de l'article L. 123-6 du code de l'urbanisme : " A compter de la publication de la délibération prescrivant l'élaboration d'un plan local d'urbanisme, l'autorité compétente peut décider de surseoir à statuer, dans les conditions et délais prévus à l'article L. 111-8, sur les demandes d'autorisation concernant des constructions, installations ou opérations qui seraient de nature à compromettre ou à rendre plus onéreuse l'exécution du plan. " ; que la circonstance que le terrain d'assiette du projet serait classé dans le futur plan local d'urbanisme en zone urbaine pavillonnaire et que le projet litigieux n'y serait plus autorisé n'est pas de nature à compromettre ou à rendre plus onéreuse l'exécution du futur plan ; qu'il ne ressort pas des pièces du dossier que le terrain serait situé sur une servitude d'espace boisé classé du futur plan ; qu'ainsi, en ne décidant pas de surseoir à statuer sur la demande présentée par la SCI Mériland, le maire de Mérignac n'a pas commis d'erreur manifeste d'appréciation ; <br/>
<br/>
              24. Considérant qu'il résulte de tout ce qui précède que M. C...n'est pas fondé à demander l'annulation du permis de construire délivré le 23 novembre 2004 ; <br/>
<br/>
              Sur la requête d'appel dirigée contre le jugement du 31 juillet 2008 ayant rejeté la demande d'annulation du permis de construire modificatif du 1er juillet 2005 :<br/>
<br/>
              En ce qui concerne la fin de non-recevoir tirée de la méconnaissance de l'article R. 600-1 du code de l'urbanisme :<br/>
<br/>
              25. Considérant qu'il ressort des pièces du dossier que M. C...a notifié sa requête d'appel enregistrée le 7 novembre 2008 au greffe de la cour administrative d'appel de Bordeaux sous le n° 08BX02785 à la commune de Mérignac et à la SCI Mériland par courriers recommandés avec accusé de réception envoyés le 12 novembre 2008 ; que, dès lors, la fin de non-recevoir opposée par la SCI Mériland doit être écartée ; <br/>
<br/>
              En ce qui concerne la régularité du jugement du 31 juillet 2008 : <br/>
<br/>
              26. Considérant que le tribunal, pour écarter le moyen tiré de la violation de l'article UB7 du plan d'occupation des sols, a relevé que le terrain d'assiette des constructions autorisées dispose de deux façades sur voies publiques et que dès lors les dispositions en cause ne lui étaient pas applicables ; qu'en statuant ainsi, le tribunal n'a pas soulevé d'office un moyen, mais a seulement relevé que les conditions posées par le texte invoqué devant lui n'étaient pas remplies ; que le requérant n'est dès lors pas fondé à soutenir que le tribunal aurait méconnu l'article R. 611-7 du code de justice administrative en omettant de communiquer aux parties un moyen relevé d'office ; <br/>
<br/>
              En ce qui concerne l'arrêté du 1er juillet 2005 : <br/>
<br/>
              27. Considérant, en premier lieu, qu'il résulte des pièces du dossier, notamment de la demande du 18 février 2005, que l'arrêté du 1er juillet 2005 rectifie l'erreur de superficie du terrain d'assiette affectant le permis de construire initial, en indiquant une surface de 2395 m² au lieu de 2980 m², autorise la suppression d'une terrasse couverte et des modifications mineures des façades liées à cette suppression ainsi qu'une rotation vers l'ouest de l'implantation de la construction ; que les modifications projetées étant sans influence sur la conception générale du projet initial, l'arrêté litigieux doit être regardé, non comme un nouveau permis de construire, mais comme un permis de construire modificatif ; <br/>
<br/>
              28. Considérant, en second lieu, que si M. C...soutient que le dossier de demande de permis modificatif ne contenait pas l'ensemble des documents relatifs à la modification apportée, en méconnaissance de l'article R. 421-2 du code de l'urbanisme, il ressort des pièces du dossier que ladite demande contenait un plan de masse montrant la modification de l'orientation de la construction, ainsi que des plans de façades, et a ainsi permis au service instructeur de disposer d'éléments suffisants pour apprécier le bien-fondé de sa demande ; <br/>
<br/>
              29. Considérant qu'il résulte de ce qui précède que M. C...n'est pas fondé à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Bordeaux a rejeté sa demande d'annulation du permis de construire modificatif délivré le 1er juillet 2005 ; <br/>
<br/>
              Sur les conclusions tendant à l'application de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              30. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mise à la charge de la commune de Mérignac et de la SCI Mériland, qui ne sont pas, dans la présente instance, les parties perdantes, la somme que M. C... demande au titre des frais exposés par lui et non compris dans les dépens ; qu'en revanche il y a lieu, en application de ces dispositions, de mettre à la charge de ce dernier la somme de 1 000 euros à verser à chacune d'elles ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
  Article 1er : L'arrêt de la cour administrative d'appel de Bordeaux du 28 février 2012 est annulé.<br/>
  Article 2 : L'ordonnance du 11 octobre 2005 du vice-président du tribunal administratif de Bordeaux est annulée.<br/>
  Article 3 : Le surplus des conclusions d'appel et de première instance de M. C...est rejeté.<br/>
  Article 4 : M. C...versera à la commune de Mérignac et la SCI Mériland une somme de 1000 euros chacune au titre de l'article L. 761-1 du code de justice administrative.<br/>
  Article 5 : La présente décision sera notifiée à M. B...C..., à la commune de Mérignac et à la SCI Mériland.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
