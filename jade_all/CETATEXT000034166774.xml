<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034166774</ID>
<ANCIEN_ID>JG_L_2017_03_000000393050</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/16/67/CETATEXT000034166774.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème chambre, 10/03/2017, 393050, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-03-10</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>393050</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DELAPORTE, BRIARD</AVOCATS>
<RAPPORTEUR>Mme Manon Perrière</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Benoît Bohnert</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2017:393050.20170310</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Procédure antérieure : <br/>
<br/>
              La société par actions simplifiée Avis Financement Véhicules (AFV) a, sous le n° 1101199, demandé au tribunal administratif d'Amiens de prononcer la décharge des rappels de taxe différentielle sur les véhicules à moteur mis à sa charge au titre de la période allant du 1er mars 2006 au 14 août 2006 et des pénalités correspondantes.<br/>
<br/>
              La société AFV, venant aux droits de la société par actions simplifiée unipersonnelle Logistique Flottes Véhicules (LFV), a demandé, sous le n° 1101198, au tribunal administratif d'Amiens de prononcer la décharge des rappels de taxe différentielle sur les véhicules à moteur mis à la charge de cette dernière société au titre des périodes allant du 1er décembre 2003 au 30 novembre 2004, du 1er décembre 2004 au 30 novembre 2005 et du 1er décembre 2005 au 30 novembre 2006 ainsi que des pénalités correspondantes.<br/>
<br/>
              Par un jugement nos 1101198, 1101199 du 20 février 2014, le tribunal administratif a, d'une part, rejeté comme portées devant un ordre de juridiction incompétent pour en connaître les conclusions de la société AFV, venant aux droits de la société LFV, tendant à la décharge des rappels de taxe différentielle à moteur au titre des périodes allant du 1er décembre 2003 au 30 novembre 2004 et du 1er décembre 2004 au 28 février 2005, ainsi que des pénalités correspondantes, d'autre part, prononcé la décharge des rappels de taxe auxquels la société LFV a été assujettie au titre de la période allant du 1er mars au 30 novembre 2005 ainsi que des pénalités correspondantes et rejeté le surplus des conclusions de ces demandes.<br/>
<br/>
              Par un arrêt nos 14DA00658, 14DA00679 du 25 juin 2015, la cour administrative d'appel de Douai a, d'une part, sur recours du ministre des finances et des comptes publics, annulé ce jugement en tant qu'il prononce la décharge des rappels de taxe différentielle sur les véhicules à moteur auxquels la société LFV a été assujettie au titre de la période allant du 1er mars au 30 novembre 2005 ainsi que des pénalités correspondantes, remis ces rappels de taxe à la charge de la société et rejeté l'appel incident formé par la société et, d'autre part, rejeté l'appel formé par la société AFV, venant aux droits de la société LFV, tendant à l'annulation de ce jugement en tant qu'il a rejeté les conclusions de sa demande tendant à la décharge des rappels de taxe différentielle sur les véhicules à moteur au titre de la période du 1er décembre 2005 au 30 novembre 2006 et des pénalités correspondantes.<br/>
<br/>
              Procédure devant le Conseil d'Etat : <br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 28 août et 30 novembre 2015 et le 20 décembre 2016 au secrétariat du contentieux du Conseil d'Etat, la société AFV, venant aux droits de la société LFV, demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 6 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - le code de la route ;<br/>
              - la loi n° 2004-1484 du 30 décembre 2004 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Manon Perrière, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Benoît Bohnert, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Delaporte, Briard, avocat de la Société Avis Financement Véhicules (AFV) venant aux droits de la société Logistique Flottes Véhicules-LFV ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que la société Logistique Financement Véhicules (LFV) a fait l'objet, d'une part, d'une vérification de comptabilité au titre de la période du 9 décembre 2003 au 30 novembre 2005 et, d'autre part, d'un contrôle sur pièces pour la période du 1er décembre 2005 au 28 décembre 2006. La société Avis Financement Véhicules (AFV), qui vient aux droits et obligations de la société LFV, a fait l'objet, en ce qui la concerne, d'une part, d'une vérification de comptabilité au titre de la période du 22 novembre 2005 au 28 février 2006, d'autre part, d'un contrôle sur pièces pour la période du 1er mars au 14 août 2006. A l'issue de ces opérations de contrôle, l'administration fiscale a notifié à la société des rappels de taxe différentielle sur les véhicules à moteur. La société a contesté ces rappels devant le tribunal administratif d'Amiens, qui a rejeté sa demande pour les périodes allant du 1er décembre 2003 au 30 novembre 2004 et du 1er décembre 2004 au 28 février 2005 comme portée devant un ordre de juridiction incompétent pour en connaître et l'a déchargée des rappels de taxe au titre de la période du 1er mars au 30 novembre 2005. La société se pourvoit en cassation contre l'arrêt de la cour administrative d'appel de Douai du 25 juin 2015 qui, sur recours du ministre des finances et des comptes publics, a annulé ce jugement en tant qu'il prononçait la décharge partielle des rappels de taxe, a remis ceux-ci à la charge de la société au titre de la période du 1er mars au 30 novembre 2005 et rejeté le surplus des conclusions de la société.<br/>
<br/>
              Sur le bien-fondé de l'arrêt en ce qui concerne la procédure d'imposition :<br/>
<br/>
              2. Par l'article 42 de la loi du 30 décembre 2004 de finances pour 2005, le législateur a réformé le régime applicable à la taxe différentielle sur les véhicules à moteur prévue par les articles 1599 C et suivants du code général des impôts en modifiant les règles relatives à sa nature, à son exigibilité, à son régime déclaratif, à ses modalités de recouvrement et de contrôle ainsi qu'aux sanctions et aux garanties. En vertu du III de cet article, ces nouvelles dispositions sont entrées en vigueur le 1er mars 2005.<br/>
<br/>
              3. En premier lieu, aux termes de l'article L. 57 du livre des procédures fiscales : " L'administration adresse au contribuable une proposition de rectification qui doit être motivée de manière à lui permettre de formuler ses observations ou de faire connaître son acceptation (...) ". Aux termes de l'article R. 57-1 du même livre : " La proposition de rectification prévue par l'article L. 57 fait connaître au contribuable la nature et les motifs de la rectification envisagée. L'administration invite, en même temps, le contribuable à faire parvenir son acceptation ou ses observations dans un délai de trente jours à compter de la réception de la proposition (...) ". Il résulte de ces dispositions que, pour être régulière, une proposition de rectification doit comporter la désignation de l'impôt concerné, de l'année d'imposition et de la base d'imposition, et énoncer les motifs sur lesquels l'administration entend se fonder pour justifier les rectifications envisagées, de façon à permettre au contribuable de formuler utilement ses observations. En revanche, sa régularité ne dépend pas du bien-fondé de ces motifs. <br/>
<br/>
              4. Il ressort des pièces du dossier soumis aux juges du fond que l'administration a adressé à la société requérante, le 18 décembre 2007, une proposition de rectification lui indiquant qu'elle envisageait de mettre à sa charge des rappels de taxe différentielle sur les véhicules à moteur. Devant la cour, la société soutenait que la proposition de rectification était insuffisamment motivée au motif qu'elle ne distinguait pas les montants relatifs aux périodes d'imposition antérieures au 1er mars 2005 de ceux relatifs aux périodes d'imposition postérieures à cette date. Après avoir relevé par une appréciation souveraine non arguée de dénaturation que la proposition de rectification précisait l'impôt, les périodes d'imposition en cause, la base d'imposition ainsi que les motifs retenus par l'administration pour justifier les rappels de taxe, la cour n'a pas commis d'erreur de droit en jugeant que ce document était suffisamment motivé et mettait la société à même de présenter ses observations sur les rectifications envisagées par l'administration, alors même que les modalités de recouvrement et de réclamation de la taxe avaient changé. <br/>
<br/>
              5. En deuxième lieu, aux termes de l'article L. 256 du livre des procédures fiscales : " Un avis de mise en recouvrement est adressé par le comptable public compétent à tout redevable des sommes, droits, taxes et redevances de toute nature dont le recouvrement lui incombe lorsque le paiement n'a pas été effectué à la date d'exigibilité (...) ". Aux termes de l'article R. 256-1 du même livre, dans sa rédaction applicable à l'avis de mise en recouvrement émis en 2010 : " L'avis de mise en recouvrement prévu à l'article L. 256 indique pour chaque impôt ou taxe le montant global des droits, des pénalités et des intérêts de retard qui font l'objet de cet avis. / Lorsque l'avis de mise en recouvrement est consécutif à une procédure de rectification, il fait référence à la proposition prévue à l'article L. 57 ou à la notification prévue à l'article L. 76 et, le cas échéant, au document adressé au contribuable l'informant d'une modification des droits, taxes et pénalités résultant des rectifications (...) ".<br/>
<br/>
              6. Il ressort des pièces du dossier soumis aux juges du fond que l'avis de mise en recouvrement adressé le 8 novembre 2010 à la société comportait le montant des droits dus. Après avoir relevé, par une appréciation souveraine non arguée de dénaturation, que l'avis de mise en recouvrement se référait aux propositions de rectification du 18 décembre 2007, qui mentionnaient les impositions en cause et les périodes d'imposition, la cour n'a pas commis d'erreur de droit en écartant la circonstance qu'il ne distinguait pas les montants recouvrés relatifs aux périodes d'imposition antérieures au 1er mars 2005 de ceux relatifs aux périodes d'imposition postérieures à cette date et en déduisant de ces constatations que ce document était suffisamment motivé. <br/>
<br/>
              7. En troisième lieu, aux termes de l'article L. 13 du livre des procédures fiscales, dans sa rédaction applicable au litige : " Les agents de l'administration des impôts vérifient sur place, en suivant les règles prévues par le présent livre, la comptabilité des contribuables astreints à tenir et à présenter des documents comptables. / Lorsque la comptabilité est tenue au moyen de systèmes informatisés, le contrôle porte sur l'ensemble des informations, données et traitements informatiques qui concourent directement ou indirectement à la formation des résultats comptables ou fiscaux et à l'élaboration des déclarations rendues obligatoires par le code général des impôts ainsi que sur la documentation relative aux analyses, à la programmation et à l'exécution des traitements ". Aux termes de l'article L. 47 A du même livre, dans sa rédaction applicable au litige : " Lorsque la comptabilité est tenue au moyen de systèmes informatisés, les agents de l'administration fiscale peuvent effectuer la vérification sur le matériel utilisé par le contribuable. / Celui-ci peut demander à effectuer lui-même tout ou partie des traitements informatiques nécessaires à la vérification. Dans ce cas, l'administration précise par écrit au contribuable, ou à un mandataire désigné à cet effet, les travaux à réaliser ainsi que le délai accordé pour les effectuer (...) ".<br/>
<br/>
              8. Il ressort des pièces du dossier soumis aux juges du fond que l'administration a demandé à la société de réaliser, sur des logiciels de gestion commerciale, des traitements informatiques afin de déterminer la composition du parc de véhicules géré au cours de chacune des années couvrant la période vérifiée, ainsi que les lieux et dates de la première mise à disposition de ces véhicules. La cour, par une appréciation souveraine non arguée de dénaturation, a relevé que ces éléments avaient permis au vérificateur de déterminer le montant des rappels de taxe différentielle sur les véhicules à moteur mis à la charge de la société requérante. Il suit de là que c'est sans erreur de droit qu'elle a jugé que ces éléments devaient être regardés comme des informations concourant directement ou indirectement à l'élaboration des résultats comptables ou fiscaux et à l'élaboration des déclarations rendues obligatoires par le code général des impôts, au sens du deuxième alinéa de l'article L. 13 du livre des procédures fiscales, et entraient en conséquence dans le champ du contrôle des comptabilités tenues au moyen de systèmes informatisés, sans qu'y fasse obstacle la circonstance que ces logiciels de gestion commerciale n'étaient pas connectés au logiciel de comptabilité.<br/>
<br/>
              9. En quatrième lieu, en application de l'article L. 10 du livre des procédures fiscales, l'administration est en droit de contrôler les éléments ressortant des déclarations des contribuables avec toutes les données en sa possession. Il ressort des pièces du dossier soumis aux juges du fond que l'administration, à la suite d'un contrôle sur pièces, a adressé à la société requérante, le 18 décembre 2007, une seconde proposition de rectification relative à la période du 1er mars au 14 août 2006. La cour n'a pas commis d'erreur de droit en jugeant que l'administration n'était pas tenue de procéder à une nouvelle vérification de comptabilité, dès lors qu'elle s'est bornée à exploiter les renseignements en sa possession à la suite des vérifications de comptabilité ayant porté sur la période du 9 décembre 2003 au 28 février 2006, y compris les éléments obtenus en application de l'article L. 47 A du livre des procédures fiscales. Elle n'a en conséquence pas commis d'erreur de droit en en déduisant que la société n'avait été privée d'aucune des garanties attachées aux vérifications de comptabilité.<br/>
<br/>
              Sur le bien-fondé de l'arrêt en ce qui concerne le lieu d'imposition :<br/>
<br/>
              10. En premier lieu, aux termes de l'article R. 322-1 du code de la route, dans sa rédaction applicable au litige : " Tout propriétaire d'un véhicule à moteur, d'une remorque dont le poids total autorisé en charge est supérieur à 500 kilogrammes ou d'une semi-remorque, qui souhaite le mettre en circulation pour la première fois, doit adresser au préfet du département de son domicile une demande de certificat d'immatriculation en justifiant de son identité et en déclarant son domicile. / Toutefois, lorsque le propriétaire est une personne morale ou une entreprise individuelle, la demande de certificat d'immatriculation doit être adressée au préfet du département de l'établissement inscrit au registre du commerce et des sociétés ou au répertoire des métiers, auquel le véhicule doit être affecté à titre principal pour les besoins de cet établissement. Le propriétaire doit justifier de son identité et de l'adresse de l'établissement d'affectation du véhicule. / Pour un véhicule de location, la demande de certificat d'immatriculation doit être adressée au préfet du département de l'établissement où le véhicule est mis à la disposition du locataire, au titre de son premier contrat de location. Le propriétaire doit justifier de son identité et de l'adresse de l'établissement de mise à disposition. / Pour un véhicule faisant l'objet soit d'un contrat de crédit-bail, soit d'un contrat de location de deux ans ou plus, la demande de certificat d'immatriculation doit être adressée au préfet du département du domicile du locataire. Toutefois, lorsque ce véhicule doit être affecté à titre principal à un établissement du locataire pour les besoins de cet établissement, la demande doit être adressée au préfet du département de cet établissement. Le propriétaire doit justifier de son identité et déclarer, selon le cas, l'adresse du domicile du locataire ou celle de l'établissement d'affectation (...) ". Aux termes de l'article 1599 C du code général des impôts, dans sa rédaction applicable au litige : " Une taxe différentielle sur les véhicules à moteur est perçue au profit des départements dans lesquels les véhicules doivent être immatriculés (...) ". Aux termes de l'article 1599 E du même code, dans sa rédaction applicable au litige : " Le locataire d'un véhicule faisant l'objet soit d'un contrat de crédit-bail, soit d'un contrat de location de deux ans ou plus, est redevable de la taxe différentielle sur les véhicules à moteur, au lieu et place du propriétaire (...) ". Il résulte de ces dispositions qu'un véhicule faisant l'objet d'un contrat de location de moins de deux ans doit être immatriculé dans le département de l'établissement où il est matériellement mis à la disposition d'un locataire, au titre de son premier contrat de location, en vue d'une utilisation effective, et que la taxe différentielle sur les véhicules à moteur correspondante doit être acquittée dans ce département, sans qu'y fasse obstacle la circonstance que, dans le cadre de modalités de financement internes à un groupe ayant pour activité la location de véhicules et préalablement à sa première mise en service, le véhicule a été pris en crédit-bail par une des sociétés du groupe. Dans ce cas, le redevable de la taxe différentielle sur les véhicules à moteur est celui qui prend le véhicule en crédit-bail.<br/>
<br/>
              11. Il ressort des pièces du dossier soumis aux juges du fond que les sociétés AFV et LFV prenaient en crédit-bail des voitures que leur société-mère, la société Avis Location de Voitures, mettait à la disposition de ses clients pour des locations de courte durée. Dès lors, la cour n'a pas commis d'erreur de droit en jugeant que les véhicules, pris en crédit-bail par les sociétés AFV et LFV, devaient être immatriculés non dans le département de l'Oise, siège social de ces sociétés, mais dans le département de leur première utilisation au titre d'un contrat de location de courte durée.<br/>
<br/>
              12. En second lieu, la cour n'a pas commis d'erreur de droit en jugeant que la réponse ministérielle à M.A..., député, du 22 janvier 2001, dont se prévalait la société sur le fondement de l'article L. 80 A du livre des procédures fiscales ne comportait pas d'interprétation formelle de la loi fiscale différente de celle dont elle a fait application.<br/>
<br/>
              13. Il résulte de ce qui précède que la société AFV n'est pas fondée à demander l'annulation de l'arrêt qu'elle attaque. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er : Le pourvoi de la société AFV est rejeté.<br/>
Article 2 : La présente décision sera notifiée à société par actions simplifiée Avis Financement Véhicules et au ministre de l'économie et des finances.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
