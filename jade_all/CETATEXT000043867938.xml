<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043867938</ID>
<ANCIEN_ID>JG_L_2021_07_000000451084</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/86/79/CETATEXT000043867938.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème chambre, 27/07/2021, 451084, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-07-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>451084</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ZRIBI, TEXIER</AVOCATS>
<RAPPORTEUR>Mme Audrey Prince</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Mireille Le Corre</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:451084.20210727</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La Commission nationale des comptes de campagne et des financements politiques (CNCCFP) a saisi le tribunal administratif de Lille, en application de l'article L. 52-15 du code électoral, sur le fondement de sa décision du 16 décembre 2020 rejetant le compte de campagne de M. C... B..., candidat aux élections municipales et communautaires qui se sont déroulées le 15 mars 2020 dans la commune de Haubourdin (Nord) et refusant de lui accorder le remboursement de ses dépenses électorales. <br/>
<br/>
              Par un jugement n° 2009390 du 22 février 2021, le tribunal administratif de Lille a jugé que son compte de campagne avait été rejeté à bon droit par la Commission nationale des comptes de campagne et des financements politiques et qu'il n'avait pas droit au remboursement forfaitaire de l'Etat en vertu de l'article L. 52-11-1 du code électoral, l'a déclaré inéligible pendant un délai de neuf mois et démissionnaire d'office de son mandat de conseiller municipal d'Haubourdin à compter de la date du jugement, et a proclamé élu conseiller municipal d'Haubourdin le candidat venant immédiatement après le dernier élu sur sa liste.<br/>
<br/>
              Par une requête sommaire et un mémoire complémentaire, enregistrés les 25 mars et 26 avril 2021 au secrétariat du contentieux du Conseil d'Etat, M. B... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) de rejeter la saisine de la Commission nationale des comptes de campagne et des financements politiques ;<br/>
<br/>
              3°) de juger qu'il n'y a pas lieu de le déclarer inéligible ;<br/>
<br/>
              4°) de mettre à la charge de la Commission nationale des comptes de campagne et des financements politiques la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code électoral ;<br/>
              - la loi n° 2020-90 du 23 mars 2020 ; <br/>
              - la loi n° 2019-1269 du 2 décembre 2019 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Audrey Prince, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Mireille Le Corre, rapporteure publique ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Zribi et Texier, avocat de M. B... ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. A l'issue du premier tour de scrutin des élections municipales et communautaires qui s'est déroulé le 15 mars 2020 à Haubourdin (Nord), la liste conduite par M.   B..., " L'Haubourdinois gagnant ", a obtenu 14,84 % des suffrages exprimés et deux sièges au conseil municipal. Par une décision du 17 décembre 2020, la Commission nationale des comptes de campagne et des financements politiques a rejeté le compte de campagne présenté par M. B..., décidé qu'il n'avait pas droit au remboursement forfaitaire de l'Etat et saisi le juge de l'élection en application des dispositions de l'article L. 52-15 du code électoral. L'intéressé fait appel du jugement du 22 février 2021 par lequel le tribunal administratif de Lille a jugé que son compte de campagne avait été rejeté à bon droit par la Commission nationale des comptes de campagne et des financements politiques et qu'il n'avait pas droit au remboursement forfaitaire de l'Etat en vertu de l'article L. 52-11-1 du code électoral, l'a déclaré inéligible pendant un délai de neuf mois et démissionnaire d'office de son mandat de conseiller municipal d'Haubourdin à compter de la date du jugement et a proclamé élu conseiller municipal d'Haubourdin le candidat venant sur sa liste, immédiatement après le dernier élu.<br/>
<br/>
              Sur le rejet du compte de campagne :<br/>
<br/>
              2. Aux termes de l'article L. 52-4 du code électoral : " Tout candidat à une élection déclare un mandataire conformément aux articles L. 52-5 et L. 52-6 au plus tard à la date à laquelle sa candidature est enregistrée. Ce mandataire peut être une association de financement électoral, ou une personne physique dénommée " le mandataire financier ". Un même mandataire ne peut être commun à plusieurs candidats. / Le mandataire recueille, pendant l'année précédant le premier jour du mois de l'élection et jusqu'à la date du dépôt du compte de campagne du candidat, les fonds destinés au financement de la campagne. / Il règle les dépenses engagées en vue de l'élection et antérieures à la date du tour de scrutin où elle a été acquise, à l'exception des dépenses prises en charge par un parti ou groupement politique. Les dépenses antérieures à sa désignation payées directement par le candidat ou à son profit, ou par l'un des membres d'un binôme de candidats ou au profit de ce membre, font l'objet d'un remboursement par le mandataire et figurent dans son compte de dépôt (...) ". Le deuxième alinéa de l'article L. 52-6 du même code précise que le mandataire financier est tenu d'ouvrir un compte bancaire ou postal unique retraçant la totalité de ses opérations financières. <br/>
<br/>
              3. En premier lieu, eu égard à la finalité poursuivie par les dispositions de l'article L. 52-4 du code électoral, l'obligation de recourir à un mandataire financier, qui constitue une formalité substantielle à laquelle il ne peut être dérogé, implique nécessairement l'obligation tout aussi substantielle pour ledit mandataire d'ouvrir ès qualité, ainsi que l'exige le deuxième alinéa de l'article L. 52-6, un compte bancaire unique à son nom, qui retracera la totalité des opérations financières de la période courant du premier jour des douze mois précédant le mois d'une élection jusqu'au jour où cette élection sera acquise. <br/>
<br/>
              4. Il est constant que le mandataire financier désigné par M. B..., le 25 février 2020, n'a pas ouvert de compte de dépôt unique et que les opérations financières relatives à la campagne des élections municipales du 15 mars 2020 ont été réalisées à partir d'un compte ouvert par le mandataire financier désigné le 16 septembre 2019 par M. A..., alors candidat à ces mêmes élections, avant que ce dernier ne devienne, après la fusion de leurs listes, colistier de M. B.... Dans ces conditions, les circonstances que le mandataire financier de M. B... aurait été désigné tardivement en raison de la fusion tardive de ces listes, que le compte de dépôt ouvert par le mandataire financier de M. A... aurait retracé la totalité des dépenses relatives à la campagne électorale et que les opérations ainsi retracées résulteraient de faits générateurs antérieurs à la date du 25 février 2020 ne pouvaient justifier la méconnaissance des règles de financement des campagnes électorales citées au point 2. C'est donc à bon droit que la commission nationale des comptes de campagne et des financements politiques a rejeté le compte de campagne de M. B....<br/>
<br/>
              5. En second lieu, M. B... produit en appel, ainsi qu'il peut le faire, les pièces justificatives des versements personnels d'un montant de 4 348 euros qu'il a effectués auprès du mandataire financier de son colistier. Si le défaut de justification de ces versements, que la CNCCFP a également retenu pour rejeter le compte de campagne de M. B..., n'est ainsi plus de nature à justifier cette décision, le motif tiré de la méconnaissance des dispositions des articles L. 52-4 et L. 52-6 du code électoral, énoncé aux points précédents, suffit à lui seul à justifier le rejet du compte de campagne de M. B.... <br/>
<br/>
              6. Il résulte de tout ce qui précède que M. B... n'est pas fondé à se plaindre de ce que, par le jugement attaqué du 22 février 2021, le tribunal administratif de Lille a jugé que son compte de campagne avait été rejeté à bon droit par la Commission nationale des comptes de campagne et des financements politiques et qu'il n'avait pas droit au remboursement forfaitaire de l'Etat en vertu de l'article L. 52-11-1 du code électoral.<br/>
<br/>
              Sur l'inéligibilité :<br/>
<br/>
              7. Aux termes de l'article L. 118-3 du code électoral, dans sa rédaction issue de la loi du 2 décembre 2019 visant à clarifier diverses dispositions du droit électoral : " Lorsqu'il relève une volonté de fraude ou un manquement d'une particulière gravité aux règles de financement des campagnes électorales, le juge de l'élection, saisi par la Commission nationale des comptes de campagne et des financements politiques, peut déclarer inéligible : / (...) 3° Le candidat dont le compte de campagne a été rejeté à bon droit. (...) ". <br/>
<br/>
              8. En application de ces dispositions, en dehors des cas de fraude, le juge de l'élection ne peut prononcer l'inéligibilité d'un candidat sur le fondement de ces dispositions que s'il constate un manquement d'une particulière gravité aux règles de financement des campagnes électorales. Il lui incombe à cet effet de prendre en compte l'ensemble des circonstances de l'espèce et d'apprécier s'il s'agit d'un manquement caractérisé à une règle substantielle relative au financement des campagnes électorales et s'il présente un caractère délibéré. <br/>
<br/>
              9. Il résulte de ce qui a été dit au point 4 que c'est à bon droit que la Commission nationale des comptes de campagne et des financements politiques a rejeté le compte de campagne de M. B.... Si celui-ci a fait valoir devant la Commission nationale des comptes de campagne et des financements publics que l'irrégularité de son compte de campagne était dû à la fusion tardive de leurs listes et à la désignation très tardive de son mandataire financier, cette circonstance ne saurait justifier la méconnaissance des dispositions précitées du code électoral, qui constitue, eu égard à l'absence d'ambiguïté de la règle applicable, un manquement grave et délibéré à une obligation substantielle. Par suite, M. B... n'est pas fondé à soutenir que c'est à tort que le tribunal administratif de Lille l'a déclaré inéligible pendant un délai de neuf mois et démissionnaire d'office de son mandat de conseiller municipal d'Haubourdin à compter de la date à laquelle son jugement sera devenu définitif, puis a, par voie de conséquence, proclamé élu le candidat venant immédiatement après le dernier élu sur la liste conduite par M. B.... <br/>
<br/>
              10. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat, qui n'est pas, dans la présente instance, la partie perdante. <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de M. B... est rejetée.<br/>
Article 2 : La présente décision sera notifiée à M. C... B..., à la Commission nationale des comptes de campagne et des financements politiques et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
