<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032892419</ID>
<ANCIEN_ID>JG_L_2016_07_000000388777</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/89/24/CETATEXT000032892419.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère chambre, 13/07/2016, 388777, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-07-13</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>388777</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Marie Sirinelli</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Rémi Decout-Paolini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2016:388777.20160713</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu les procédures suivantes : <br/>
<br/>
              1° Par une  requête et un mémoire en réplique, enregistrés sous le n° 388777 les 17 mars et 4 septembre 2015 au secrétariat du contentieux du Conseil d'Etat, la Société nationale d'exploitation industrielle des tabacs et allumettes (SEITA) demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler pour excès de pouvoir l'article 3 de l'arrêté du ministre des affaires sociales, de la santé et des droits des femmes du 24 février 2015 modifiant l'arrêté du 15 avril 2010 relatif aux modalités d'inscription des avertissements de caractère sanitaire sur les unités de conditionnement des produits du tabac et insérant un pictogramme destiné aux femmes enceintes, en tant qu'il prévoit l'entrée en vigueur immédiate de l'obligation d'insertion de ce pictogramme et en tant qu'il limite à six mois la période de mise en oeuvre des dispositions transitoires destinées à éliminer les produits du tabac non conformes à la nouvelle réglementation ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              2° Par une  requête sommaire et un mémoire complémentaire, enregistrés sous le n° 389755 les 24 avril et 23 juillet 2015 au secrétariat du contentieux du Conseil d'Etat, la société Traditab demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler pour excès de pouvoir l'article 3 du même arrêté en tant qu'il limite à six mois la période pendant laquelle les emballages non conformes pourront être commercialisés et ne précise pas à quelles catégories de produits du tabac ces mesures transitoires s'appliquent ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu :<br/>
              - la directive 2014/40/UE du Parlement européen et du Conseil du 3 avril 2014 ; <br/>
              - le code général des impôts ;<br/>
              - le code de la santé publique ;<br/>
              - la décision n° 388777 du 30 juin 2015 par laquelle le Conseil d'Etat, statuant au contentieux, n'a pas renvoyé au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée par la Société nationale d'exploitation industrielle des tabacs et allumettes ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Marie Sirinelli, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Rémi Decout-Paolini, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Boutet, Hourdeaux, avocat de la Société Traditab ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'avant-dernier alinéa de l'article L. 3511-6 du code de la santé publique, en vigueur à la date de l'arrêté attaqué : " Toutes les unités de conditionnement du tabac et des produits du tabac ainsi que du papier à rouler les cigarettes portent, dans les conditions fixées par un arrêté du ministre chargé de la santé, un message général et un message spécifique de caractère sanitaire ". Sur le fondement de ces dispositions, le ministre des affaires sociales, de la santé et des droits des femmes a, par un arrêté du 24 février 2015, modifié l'arrêté du 15 avril 2010 relatif aux modalités d'inscription des avertissements de caractère sanitaire sur les unités de conditionnement des produits du tabac pour, d'une part, porter leur superficie à 65 % de la face correspondante de l'unité de conditionnement et, d'autre part, prévoir l'apposition d'un pictogramme destiné aux femmes enceintes. Par deux requêtes qu'il y a lieu de joindre, la Société nationale d'exploitation industrielle des tabacs et allumettes et la société Traditab demandent l'annulation pour excès de pouvoir des deux premiers alinéas de l'article 3 de cet arrêté du 24 février 2015 en tant, d'une part, qu'ils fixent l'entrée en vigueur de l'obligation d'apposer le pictogramme destiné aux femmes enceintes au lendemain de la publication de l'arrêté et, d'autre part, qu'ils limitent à six mois le délai pendant lequel les produits du tabac non conformes à cette prescription peuvent être mis à la consommation. <br/>
<br/>
              2. En premier lieu, ainsi que le Conseil d'Etat, statuant au contentieux, l'a relevé dans sa décision du 30 juin 2015, les dispositions précitées de l'avant-dernier alinéa de l'article L. 3511-6 du code de la santé publique renvoient à un arrêté du ministre chargé de la santé le soin de fixer les conditions dans lesquelles les unités de conditionnement du tabac et des produits du tabac ainsi que du papier à rouler les cigarettes portent des avertissements. En déterminant, par les dispositions attaquées, les modalités d'entrée en vigueur des modifications de ces avertissements auxquelles il procédait, le ministre des affaires sociales, de la santé et des droits des femmes n'a pas excédé les prévisions de l'avant-dernier alinéa de l'article L. 3511-6 du code de la santé publique. Par ailleurs, si la Société nationale d'exploitation industrielle des tabacs et allumettes soutient que les dispositions de ce même alinéa seraient contraires à la Constitution, le Conseil d'Etat statuant au contentieux, par cette même décision du 30 juin 2015, n'a pas renvoyé au Conseil constitutionnel la question prioritaire de constitutionnalité dirigée contre elles.<br/>
<br/>
              3. En deuxième lieu, les deux premiers alinéas de l'article 3 de l'arrêté attaqué disposent que : " L'article 1er entre en vigueur le lendemain de la date de publication du présent arrêté. / Toutefois, les produits du tabac non conformes aux dispositions de l'article 1er peuvent être mis à la consommation dans un délai de six mois suivant son entrée en vigueur ". <br/>
<br/>
              4. D'une part, l'article L. 3511-1 du code de la santé publique, dans sa rédaction en vigueur à la date de l'arrêté attaqué, prévoit que : " Sont considérés comme produits du tabac les produits destinés à être fumés, prisés, mâchés ou sucés, dès lors qu'ils sont, même partiellement, constitués de tabac, ainsi que les produits destinés à être fumés même s'ils ne contiennent pas de tabac, à la seule exclusion des produits qui sont destinés à un usage médicamenteux, au sens du troisième alinéa (2°) de l'article 564 decies du code général des impôts (...) ". L'article 1er de l'arrêté du 15 avril 2010, mentionné au point 1, qui n'est pas modifié par les dispositions attaquées, prévoit qu'il s'applique à " toutes les unités de conditionnement du tabac et des produits du tabac, à l'exception des tabacs à usage oral dont la commercialisation est autorisée et des autres produits du tabac sans combustion ". Les produits du tabac mentionnés par le deuxième alinéa de l'article 3 de l'arrêté attaqué doivent ainsi être déterminés par combinaison de l'article L. 3511-1 du code de la santé publique et de l'article 1er de l'arrêté du 15 avril 2010 et, dès lors, n'incluent pas les dispositifs électroniques de vapotage et les flacons de recharge qui leur sont associés. Par ailleurs, si cet alinéa se réfère aux " produits du tabac non conformes ", il vise ainsi, conformément à son objet, les unités de conditionnement de ces produits qui ne comportent pas un avertissement spécifique conforme aux dispositions de l'article 1er du même arrêté, dont il précise les modalités d'entrée en vigueur.<br/>
<br/>
              5. D'autre part, aux termes du 1° du 1 du I de l'article 302 D du code général des impôts applicable, par l'effet de l'article 575 C du même code, au droit de consommation sur les tabacs manufacturés institué par l'article 575 du même code : " (...) Le produit est mis à la consommation : / a. Lorsqu'il cesse de bénéficier du régime suspensif des droits d'accises prévu au II de l'article 302 G ou de l'entrepôt mentionné au 8° du I de l'article 570 ; / a bis) Lorsqu'il est fabriqué hors des régimes suspensifs mentionnés au a, sans bénéficier des exonérations prévues à l'article 302 D bis ; / b. Lorsqu'il est importé, à l'exclusion des cas où il est placé, au moment de l'importation, sous un régime suspensif des droits d'accises mentionné au a (...) ". En définissant la période transitoire de six mois qu'il prévoit par référence à la notion de mise à la consommation des produits, ainsi que le faisait au demeurant l'arrêté du 15 avril 2010 mentionné ci-dessus, le deuxième alinéa de l'article 3 de l'arrêté attaqué renvoie implicitement mais nécessairement à ces dispositions du 1° du 1 du I de l'article 302 D du code général des impôts, applicables au régime fiscal des tabacs manufacturés. <br/>
<br/>
              6. Par suite, le moyen tiré de la méconnaissance de l'objectif de valeur constitutionnelle de clarté et d'intelligibilité de la norme doit être écarté. Pour les mêmes motifs, le moyen tiré de ce que l'existence de sanctions pénales punissant, sur le fondement des articles L. 3512-2 et L. 3512-3 du code de la santé publique, la méconnaissance de l'avant-dernier alinéa de l'article L. 3511-6 du même code porterait atteinte au principe de légalité des délits et des peines, faute pour l'arrêté intervenu sur son fondement de comporter des dispositions suffisamment précises et des formules non équivoques, doit être également écarté.<br/>
<br/>
              7. En troisième lieu, l'exercice du pouvoir réglementaire implique pour son détenteur la possibilité de modifier à tout moment les normes qu'il définit sans que les personnes auxquelles sont, le cas échéant, imposées de nouvelles contraintes puissent invoquer un droit au maintien de la réglementation existante. En principe, les nouvelles normes ainsi édictées ont vocation à s'appliquer immédiatement, dans le respect des exigences attachées au principe de non-rétroactivité des actes administratifs. Toutefois, il incombe à l'autorité investie du pouvoir réglementaire, agissant dans les limites de sa compétence et dans le respect des règles qui s'imposent à elle, d'édicter, pour des motifs de sécurité juridique, les mesures transitoires qu'implique, s'il y a lieu, cette réglementation nouvelle. Il en va ainsi lorsque l'application immédiate de celle-ci entraîne, au regard de l'objet et des effets de ses dispositions, une atteinte excessive aux intérêts publics ou privés en cause. <br/>
<br/>
              8. D'une part, si l'article 3 de l'arrêté du 24 février 2015 ouvre un  délai de six mois à compter de son entrée en vigueur pendant lequel les produits du tabac dont les unités de conditionnement ne comportent pas le pictogramme destiné aux femmes enceintes peuvent être mis à la consommation et permet aux débitants de tabac d'écouler sans limitation de durée les produits mis à la consommation avant le 28 août 2015, il précise néanmoins que l'obligation issue de l'article 1er de l'arrêté " entre en vigueur le lendemain de la date de publication du présent arrêté ". Cette dernière disposition, en tant qu'elle imposait aux industriels concernés d'interrompre, dès le lendemain de la date de publication de l'arrêté attaqué, le conditionnement de produits du tabac dans des unités ne comportant pas le pictogramme destiné aux femmes enceintes, ne pouvait effectivement être mise en oeuvre, compte tenu des délais requis pour l'adaptation matérielle de leur processus de production, aussi limitée soit-elle, que par une suspension de la fabrication. S'il est constant que des consultations avec les professionnels du secteur ont été menées à ce sujet antérieurement à l'adoption de l'arrêté attaqué, ces consultations n'avaient pu les informer de la date exacte à laquelle ils seraient supposés avoir modifié leur processus de production. En conséquence, les sociétés requérantes sont fondées à soutenir que le premier alinéa de l'article 3 de l'arrêté du 24 février 2015 a, en l'absence de toute mesure transitoire, porté une atteinte excessive aux intérêts des entreprises intéressées et, par suite, à en demander l'annulation en tant qu'il n'a pas différé au 15 avril 2015 l'entrée en vigueur de l'article 1er du même arrêté. <br/>
<br/>
              9. D'autre part, les sociétés requérantes soutiennent que le délai de six mois, inférieur à celui qui avait été consenti à l'occasion des précédentes évolutions réglementaires relatives aux avertissements à caractère sanitaire à apposer sur les unités de conditionnement des produits du tabac, était insuffisant tant pour permettre aux fabricants et aux entrepositaires agréés, au sens de la législation fiscale, d'écouler leurs stocks d'emballages et de produits finis non conformes à cette nouvelle obligation que pour assurer l'approvisionnement du marché, en particulier dans les départements et collectivités d'outre-mer, avec des produits conformes à cette nouvelle obligation. Toutefois, il ressort des pièces des dossiers, et il n'est pas contesté par les sociétés requérantes, qu'en prévoyant de telles modalités d'entrée en vigueur, le ministre auteur de l'arrêté attaqué a entendu, au regard de la persistance en France d'un taux important de prévalence tabagique chez les femmes enceintes, prévenir la consommation par ces dernières de produits du tabac, en assurant une meilleure information sur les risques du tabagisme pour le déroulement des grossesses et le développement des enfants à naître. Il a ainsi poursuivi un objectif de santé publique. Si ce délai de six mois a pu empêcher la mise à la consommation sur le marché français d'une fraction de leurs stocks, notamment pour les produits à rotation lente, les éléments produits par les sociétés requérantes n'établissent pas la réalité de pertes significatives au regard de leur chiffre d'affaires ou d'atteintes aux relations contractuelles avec leurs fournisseurs et prestataires. Par ailleurs, si ce délai a effectivement conduit les fabricants à adapter leur processus de production à brève échéance au regard, en particulier, des contraintes qui prévalent pour assurer l'acheminement des produits dans les départements et collectivités d'outre-mer, il ne ressort pas des pièces des dossiers qu'au regard de l'objectif poursuivi, le ministre auteur de l'arrêté attaqué aurait porté une atteinte excessive aux intérêts des sociétés requérantes.<br/>
<br/>
              10. En quatrième lieu, la Société nationale d'exploitation industrielle des tabacs et allumettes ne peut, en tout état de cause, utilement se prévaloir, pour soutenir que le principe d'égalité aurait été méconnu, des mesures transitoires qui ont été retenues à l'occasion des précédentes évolutions réglementaires relatives aux avertissements à caractère sanitaire à apposer sur les unités de conditionnement des produits du tabac ou qui le sont par les dispositions du même article 3 de l'arrêté attaqué, s'agissant de la modification de la superficie de ces avertissements à caractère sanitaire.<br/>
<br/>
              11. En dernier lieu, l'application aux produits du tabac à fumer autres que les cigarettes, le tabac à rouler et le tabac à pipe à eau du taux de 65 %, déterminant la part de la face de l'unité de conditionnement couverte par l'avertissement, résulte de l'article 2 de l'arrêté du 24 février 2015, dont la société Traditab ne demande pas l'annulation. Par suite, cette société ne saurait utilement soutenir que l'application de ce taux à certains produits méconnaîtrait les objectifs fixés par l'article 11 de la directive 2014/40/UE du Parlement européen et du Conseil du 3 avril 2014 relative au rapprochement des dispositions législatives, réglementaires et administratives des Etats membres en matière de fabrication, de présentation et de vente des produits du tabac et des produits connexes.<br/>
<br/>
              12. Il résulte de tout ce qui précède que la Société nationale d'exploitation industrielle des tabacs et allumettes et la société Traditab sont seulement fondées à demander l'annulation des dispositions du premier alinéa de l'article 3 de l'arrêté attaqué, qui sont divisibles des autres dispositions de cet arrêté, en tant qu'il n'a pas différé au 15 avril 2015 l'entrée en vigueur de son article 1er.<br/>
<br/>
              13. Il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions que les sociétés requérantes présentent au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le premier alinéa de l'article 3 de l'arrêté du ministre des affaires sociales, de la santé et des droits des femmes du 24 février 2015 est annulé en tant qu'il n'a pas différé au 15 avril 2015 l'entrée en vigueur de l'article 1er du même arrêté. <br/>
Article 2 : Le surplus des conclusions des requêtes de la Société nationale d'exploitation industrielle des tabacs et allumettes et de la société Traditab est rejeté.<br/>
Article 3 : La présente décision sera notifiée à la Société nationale d'exploitation industrielle des tabacs et allumettes, à la société Traditab et à la ministre des  affaires sociales et de la santé.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
