<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044249849</ID>
<ANCIEN_ID>JG_L_2021_10_000000439721</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/24/98/CETATEXT000044249849.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème chambre, 25/10/2021, 439721, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-10-25</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>439721</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP CELICE, TEXIDOR, PERIER ; CABINET ROUSSEAU ET TAPIE</AVOCATS>
<RAPPORTEUR>Mme Dorothée Pradines</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Raphaël Chambon</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:439721.20211025</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B... D... a demandé au tribunal administratif de Rouen d'annuler pour excès de pouvoir la décision du 23 mars 2015 par laquelle le ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social, d'une part, a retiré sa décision implicite rejetant le recours hiérarchique de la société Akzo Nobel Packaging Coatings contre la décision du 18 juillet 2014 de l'inspecteur du travail de la 2ème section de l'unité territoriale de Seine-Maritime refusant d'autoriser son licenciement, et, d'autre part, a annulé cette décision et autorisé ce licenciement. Par un jugement n° 1501633 du 6 avril 2017, le tribunal administratif a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 17DA01075 du 23 janvier 2020, la cour administrative d'appel de Douai a rejeté l'appel formé par M. D... contre ce jugement.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un autre mémoire, enregistrés les 23 mars, 1er et 3 juillet 2020 au secrétariat de la section du contentieux du Conseil d'État, M. D... demande au Conseil d'État :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) de mettre à la charge de l'État et de la société Akzo Nobel Packaging Coatings la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code du travail ;<br/>
              - la loi n° 2000-321 du 12 avril 2000 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Dorothée Pradines, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Raphaël Chambon, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, au cabinet Rousseau et Tapie, avocat de M. D... et à la SCP Célice, Texidor, Perier, avocat de la société Akzo Nobel Packaging Coatings ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que M. D..., salarié de la société Akzo Nobel Packaging Coatings et membre titulaire de la délégation unique du personnel, était employé sur le site de Saint-Pierre-lès-Elbeuf, sur lequel étaient exclusivement fabriqués des revêtements alimentaires utilisant du bisphénol A. Son employeur a demandé l'autorisation de le licencier pour motif économique. Par décision du 18 juillet 2014, l'inspecteur du travail de la 2ème section de l'unité territoriale de Seine-Maritime a refusé d'accorder l'autorisation de licencier M. D.... Toutefois, par décision du 23 mars 2015, le ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social a retiré sa décision implicite rejetant le recours hiérarchique formé par la société Akzo Nobel Packaging Coatings contre la décision de l'inspecteur du travail, a annulé cette décision et a autorisé le licenciement de M. D.... Celui-ci se pourvoit en cassation contre l'arrêt du 23 janvier 2020 par lequel la cour administrative d'appel de Douai a rejeté son appel contre le jugement du tribunal administratif de Rouen du 6 avril 2017 ayant rejeté sa demande d'annulation pour excès de pouvoir de la décision du ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social du 10 avril 2015.<br/>
<br/>
              2. En vertu des dispositions du code du travail, le licenciement des salariés légalement investis de fonctions représentatives, qui bénéficient d'une protection exceptionnelle dans l'intérêt de l'ensemble des travailleurs qu'ils représentent, ne peut intervenir que sur autorisation de l'inspecteur du travail. Lorsque le licenciement d'un de ces salariés est envisagé, ce licenciement ne doit pas être en rapport avec les fonctions représentatives normalement exercées ou l'appartenance syndicale de l'intéressé. Dans le cas où la demande d'autorisation de licenciement présentée par l'employeur est fondée sur un motif de caractère économique, il appartient à l'inspecteur du travail et, le cas échéant, au ministre, de rechercher, sous le contrôle du juge de l'excès de pouvoir, si la situation de l'entreprise justifie le licenciement du salarié.<br/>
<br/>
              3. À ce titre, lorsque la demande d'autorisation de licenciement pour motif économique est fondée sur la cessation d'activité de l'entreprise, il appartient à l'autorité administrative de contrôler que cette cessation d'activité est totale et définitive. Il ne lui appartient pas, en revanche, de contrôler si cette cessation d'activité est justifiée par l'existence de mutations technologiques, de difficultés économiques ou de menaces pesant sur la compétitivité de l'entreprise. Il incombe ainsi à l'autorité administrative de tenir compte, à la date à laquelle elle se prononce, de tous les éléments de droit ou de fait recueillis lors de son enquête qui sont susceptibles de remettre en cause le caractère total et définitif de la cessation d'activité. Il lui incombe également de tenir compte de toute autre circonstance qui serait de nature à faire obstacle au licenciement envisagé, notamment celle tenant à une reprise, même partielle, de l'activité de l'entreprise impliquant un transfert du contrat de travail du salarié à un nouvel employeur en application de l'article L. 1224-1 du code du travail. Lorsque l'entreprise appartient à un groupe, la seule circonstance que d'autres entreprises du groupe aient poursuivi une activité de même nature ne fait pas, par elle-même, obstacle à ce que la cessation d'activité de l'entreprise soit regardée comme totale et définitive.<br/>
<br/>
              4. Il ressort des termes mêmes de l'arrêt attaqué que, pour juger que le ministre avait pu légalement retenir que la cessation totale et définitive d'activité de l'entreprise justifiait de faire droit à la demande d'autorisation de licenciement -que la cour, dont l'appréciation n'est sur ce point pas critiquée en cassation, a regardée comme fondée sur un tel motif-, la cour a retenu que la circonstance que l'entreprise ait poursuivi son activité dans d'autres sites était sans incidence sur l'appréciation du caractère total et définitif de la cessation d'activité du site de Saint-Pierre-lès-Elbeuf, dès lors qu'il n'était ni établi, ni même soutenu que cette activité s'exerçait dans le même secteur des revêtements alimentaires que celle menée sur ce site. Ce faisant, la cour, à qui il incombait de se prononcer sur le bien-fondé de l'appréciation de l'administration quant à la réalité de la cessation totale et définitive d'activité de l'entreprise et non du site, a entaché son arrêt d'erreur de droit. <br/>
<br/>
              5. Il résulte de ce qui précède, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, que M. D... est fondé à demander l'annulation de l'arrêt qu'il attaque.<br/>
<br/>
              6. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la société Akzo Nobel Packaging Coatings une somme de 3 000 euros à verser à M. D... au titre des dispositions de l'article L. 761-1 du code de justice administrative. Ces dispositions font en revanche obstacle à ce qu'une somme soit mise à ce titre à la charge de M. D... qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Douai est annulé.<br/>
Article 2 : La société Akzo Nobel Packaging Coatings versera à M. D... une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : Les conclusions de la société Akzo Nobel Packaging Coatings présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée à M. B... D..., à la ministre du travail, de l'emploi et de l'insertion et à la société Akzo Nobel Packaging Coatings.<br/>
<br/>
<br/>
              Délibéré à l'issue de la séance du 30 septembre 2021 où siégeaient : Mme Maud Vialettes, présidente de chambre, présidant ; Mme Carine Soulay, conseillère d'Etat et Mme Dorothée Pradines, maître des requêtes-rapporteure. <br/>
<br/>
              Rendu le 25 octobre 2021.<br/>
<br/>
                 La présidente : <br/>
                 Signé : Mme Maud Vialettes<br/>
<br/>
<br/>
 		La rapporteure : <br/>
      Signé : Mme Dorothée Pradines<br/>
<br/>
                 La secrétaire :<br/>
                 Signé : Mme C... A...<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
