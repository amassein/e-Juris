<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042545476</ID>
<ANCIEN_ID>JG_L_2020_11_000000434018</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/54/54/CETATEXT000042545476.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème - 2ème chambres réunies, 20/11/2020, 434018, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-11-20</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>434018</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème - 2ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MATUCHANSKY, POUPOT, VALDELIEVRE ; SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR>M. Alexis Goin</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Mireille Le Corre</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:434018.20201120</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A... B... a demandé au tribunal administratif de Pau de condamner la commune de Pau à lui verser une somme de 47 300 euros en réparation des préjudices qu'il estime avoir subis du fait d'un accident survenu en service le 16 octobre 2001. Par un jugement n° 1501033 du 8 juin 2017, le tribunal administratif de Pau a condamné la commune à verser à M. B... une indemnité de 1 500 euros, mis à la charge de la commune les frais d'expertise et rejeté le surplus de ses conclusions.<br/>
<br/>
              Par un arrêt n° 17BX02637 du 28 juin 2019, la cour administrative d'appel de Bordeaux a rejeté l'appel formé par M. B... contre ce jugement.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 28 août, 28 novembre 2019 et 22 juin 2020 au secrétariat du contentieux du Conseil d'Etat, M. B... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de la commune de Pau la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la loi n° 68-1250 du 31 décembre 1968 ;<br/>
              - la loi n° 83-634 du 13 juillet 1983 ;<br/>
              - la loi n° 84-53 du 26 janvier 1984 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Alexis Goin, auditeur,  <br/>
<br/>
              - les conclusions de Mme Mireille Le Corre, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Matuchansky, Poupot, Valdelièvre, avocat de M. B... et à la SCP Piwnica, Molinié, avocat de la commune de Pau ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que M. B..., agent technique de la commune de Pau affecté au service des espaces verts, a été victime, le 16 octobre 2001, d'un accident reconnu imputable au service dû à une explosion de déchets organiques. Estimant avoir subi différents préjudices liés à cet accident, M. B... a adressé, le 25 août 2007, une réclamation préalable au maire de Pau, avant de demander au tribunal administratif de Pau la condamnation de la commune de Pau à lui verser la somme de 47 300 euros en réparation de ces préjudices. Par un jugement du 8 juin 2017, le tribunal administratif a partiellement fait droit à la demande indemnitaire de M. B... en condamnant la commune de Pau à lui verser la somme de 1 500 euros au titre des troubles psychologiques subis du fait de l'accident de service et a rejeté le surplus de ses conclusions. M. B... se pourvoit en cassation contre l'arrêt du 28 juin 2019 par lequel la cour administrative d'appel de Bordeaux a rejeté l'appel qu'il avait formé contre ce jugement.<br/>
<br/>
              2. Aux termes de l'article 1er de la loi du 31 décembre 1968 relative à la prescription des créances sur l'Etat, les départements, les communes et les établissements publics : " Sont prescrites, au profit de l'Etat, des départements et des communes, (...) toutes créances qui n'ont pas été payées dans un délai de quatre ans à partir du premier jour de l'année suivant celle au cours de laquelle les droits ont été acquis ". <br/>
<br/>
              3. S'agissant d'une créance indemnitaire détenue sur une collectivité publique au titre d'un dommage corporel engageant sa responsabilité, le point de départ du délai de prescription prévu par ces dispositions est le premier jour de l'année suivant celle au cours de laquelle les infirmités liées à ce dommage ont été consolidées. Il en est ainsi pour tous les postes de préjudice, aussi bien temporaires que permanents, qu'ils soient demeurés à la charge de la victime ou aient été réparés par un tiers, tel qu'un organisme de sécurité sociale, qui se trouve subrogé dans les droits de la victime. <br/>
<br/>
              4. La consolidation de l'état de santé de la victime d'un dommage corporel fait courir le délai de prescription pour l'ensemble des préjudices directement liés au fait générateur qui, à la date à laquelle la consolidation s'est trouvée acquise, présentaient un caractère certain permettant de les évaluer et de les réparer, y compris pour l'avenir. Si l'expiration du délai de prescription fait obstacle à l'indemnisation de ces préjudices, elle est sans incidence sur la possibilité d'obtenir réparation de préjudices nouveaux résultant d'une aggravation directement liée au fait générateur du dommage et postérieure à la date de consolidation. Le délai de prescription de l'action tendant à la réparation d'une telle aggravation court à compter de la date à laquelle elle s'est elle-même trouvée consolidée.<br/>
<br/>
              5. Il ressort des énonciations de l'arrêt attaqué que la cour administrative d'appel de Bordeaux a, d'une part, retenu, s'agissant des préjudices d'ordre physiologique subis par le requérant du fait de l'accident de service de 2001, que l'état de santé de celui-ci devait être regardé comme consolidé au 11 avril 2002, pour en déduire que la prescription quadriennale avait commencé à courir, pour ces préjudices, le 1er janvier 2003 et que la demande indemnitaire présentée par M. B... le 25 août 2007 était prescrite. Elle a, d'autre part, retenu, s'agissant des troubles psychologiques invoqués par celui-ci, une autre date de consolidation le 30 juin 2010. En retenant ainsi deux dates de consolidation distinctes pour des infirmités résultant de l'accident survenu en 2001, sans rechercher si les troubles psychologiques allégués résulteraient d'une aggravation postérieure à la date de consolidation du 11 avril 2002, la cour a commis une erreur de droit. Par suite, et sans qu'il soit besoin de se prononcer sur les autres moyens du pourvoi, M. B... est fondé à demander l'annulation de l'arrêt qu'il attaque.<br/>
<br/>
              6. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la commune de Pau la somme de 3 000 euros à verser à M. B... au titre de l'article L. 761-1 du code de justice administrative. Les mêmes dispositions font obstacle à ce qu'une somme soit mise à la charge du requérant, qui n'est pas, dans la présente instance, la partie perdante. <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 28 juin 2019 de la cour administrative d'appel de Bordeaux est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Bordeaux.<br/>
Article 3 : La commune de Pau versera à M. B... une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. Ses conclusions présentées au même titre sont rejetées.<br/>
Article 4: La présente décision sera notifiée à M. A... B... et à la commune de Pau.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
