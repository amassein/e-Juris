<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044331898</ID>
<ANCIEN_ID>JG_L_2021_11_000000434742</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/33/18/CETATEXT000044331898.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème - 5ème chambres réunies, 15/11/2021, 434742</TITRE>
<DATE_DEC>2021-11-15</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>434742</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème - 5ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP PIWNICA, MOLINIE ; SCP BAUER-VIOLAS, FESCHOTTE-DESBOIS, SEBAGH</AVOCATS>
<RAPPORTEUR>Mme Catherine Moreau</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Olivier  Fuchs</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:434742.20211115</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              L'association Force 5 a demandé au tribunal administratif de Rennes d'annuler l'arrêté du 10 janvier 2013 par lequel le ministre de l'écologie, du développement durable et de l'énergie a autorisé la société Direct Energie Génération à exploiter une centrale de production d'électricité de type cycle combiné à gaz à Landivisiau (Finistère). Par un jugement n° 1301051 du 9 octobre 2015, le tribunal administratif de Rennes a rejeté sa demande. <br/>
<br/>
              Par un arrêt n° 19NT00848 du 19 juillet 2019, la cour administrative d'appel de Nantes a rejeté l'appel formé par l'association Force 5 contre ce jugement.  <br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 19 septembre et 19 décembre 2019 et 29 septembre 2021 au secrétariat du contentieux du Conseil d'Etat, l'association Force 5 demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la Constitution, notamment son article 62 et la Charte de l'environnement ;<br/>
              - la convention d'Aarhus sur l'accès à l'information, la participation du public au processus décisionnel et l'accès à la justice en matière d'environnement ;<br/>
              - le code de l'énergie ; <br/>
              - le code de l'environnement ;<br/>
              - la décision n° 2020-843 QPC du 28 mai 2020 du Conseil constitutionnel statuant sur la question prioritaire de constitutionnalité soulevée par l'association Force 5 ; <br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Catherine Moreau, conseillère d'Etat en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Olivier Fuchs, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Bauer-Violas, Feschotte-Desbois, Sebagh, avocat de l'association Force 5 et à la SCP Piwnica et Molinié, avocat de la société Total Direct Énergie et autre ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que par un arrêté du 10 janvier 2013, le ministre de l'écologie, du développement durable et de l'énergie a, en application de l'article L. 311-1 du code de l'énergie, autorisé la société Direct Énergie Génération à exploiter une centrale de production d'électricité de type cycle combiné à gaz à Landivisiau (Finistère). Par un arrêté du 5 avril 2013, cette autorisation a été transférée à la société Compagnie électrique de Bretagne. Par un jugement du 9 octobre 2015, le tribunal administratif de Rennes a rejeté comme irrecevable la demande de l'association Force 5 tendant à l'annulation de l'arrêté du 10 janvier 2013, au motif que cette association ne justifiait pas d'un intérêt lui donnant qualité pour agir contre cet arrêté. Par un arrêt du 15 mai 2017, la cour administrative d'appel de Nantes a rejeté l'appel formé par l'association Force 5 contre ce jugement. Par une décision n° 412493 du 25 février 2019, le Conseil d'Etat statuant au contentieux a annulé cet arrêt et renvoyé le litige devant la cour. L'association Force 5 se pourvoit en cassation contre l'arrêt du 19 juillet 2019 par lequel la cour administrative d'appel de Nantes a, après avoir annulé le jugement du 9 octobre 2015 du tribunal administratif de Rennes, rejeté sa demande. <br/>
<br/>
              2. Aux termes de l'article L. 311-1 du code de l'énergie, dans sa rédaction alors en vigueur : " L'exploitation d'une installation de production électrique est subordonnée à une autorisation administrative délivrée selon la procédure prévue aux articles L. 311-5 et L. 311-6 ou au terme d'un appel d'offres en application de l'article L. 311-10. (...) ". Aux termes de l'article L. 311-5 du même code : " L'autorisation d'exploiter une installation de production d'électricité est délivrée par l'autorité administrative en tenant compte des critères suivants : / 1° La sécurité et la sûreté des réseaux publics d'électricité, des installations et des équipements associés ; / 2° Le choix des sites, l'occupation des sols et l'utilisation du domaine public ; / 3° L'efficacité énergétique ; / 4° Les capacités techniques, économiques et financières du candidat ou du demandeur ; / 5° La compatibilité avec les principes et les missions de service public, notamment avec les objectifs de programmation pluriannuelle des investissements et la protection de l'environnement ; / 6° Le respect de la législation sociale en vigueur. (...) ". <br/>
<br/>
              3. Il résulte des dispositions du code de l'énergie citées au point 2 que l'autorisation administrative prévue par l'article L. 311-1 de ce code ne concerne pas seulement les installations de production d'électricité ayant fait l'objet de la procédure d'appel d'offres prévue à l'article L. 311-10 et n'a donc pas pour seul objet de désigner le ou les candidats retenus à l'issue de cette procédure, mais constitue l'autorisation d'exploiter une installation de production d'électricité et désigne non seulement le titulaire de cette autorisation, mais également le mode de production et la capacité autorisée ainsi que le lieu d'implantation de l'installation. Pour pouvoir exploiter cette installation, le bénéficiaire d'une telle autorisation doit par ailleurs obtenir les autorisations requises au titre d'autres législations avant la réalisation des travaux et la mise en service de l'installation, notamment, le cas échéant, en application de l'article L. 512-1 du code de l'environnement, après l'enquête publique prévue à l'article L. 512-2 du même code. <br/>
<br/>
              Sur l'information et la participation du public : <br/>
<br/>
              4. En premier lieu, aux termes de l'article 7 de la Charte de l'environnement : " Toute personne a le droit, dans les conditions et les limites définies par la loi, d'accéder aux informations relatives à l'environnement détenues par les autorités publiques et de participer à l'élaboration des décisions publiques ayant une incidence sur l'environnement ".  Aux termes des deuxième et troisième alinéas de l'article 62 de la Constitution : " Une disposition déclarée inconstitutionnelle sur le fondement de l'article 61-1 est abrogée à compter de la publication de la décision du Conseil constitutionnel ou d'une date ultérieure fixée par cette décision. Le Conseil constitutionnel détermine les conditions et limites dans lesquelles les effets que la disposition a produits sont susceptibles d'être remis en cause. / Les décisions du Conseil constitutionnel ne sont susceptibles d'aucun recours. Elles s'imposent aux pouvoirs publics et à toutes les autorités administratives et juridictionnelles ".<br/>
<br/>
              5. Par une décision n° 2020-843 QPC du 28 mai 2020, le Conseil constitutionnel a déclaré que les mots " par l'autorité administrative " figurant au premier alinéa de l'article L. 311-5 du code de l'énergie, dans sa rédaction issue de l'ordonnance du 9 mai 2011 portant codification de la partie législative du code de l'énergie, étaient contraires à l'article 7 de la Charte de l'environnement avant la date du 1er septembre 2013. Le dispositif de cette décision énonce que la déclaration d'inconstitutionnalité prend effet dans les conditions fixées aux paragraphes 15 et 16. Au paragraphe 15, le Conseil constitutionnel a relevé que " les dispositions déclarées contraires à la Constitution, dans leur rédaction contestée issue de l'ordonnance du 9 mai 2011, ne sont plus en vigueur " ; au paragraphe 16, il a précisé que " la remise en cause des mesures ayant été prises avant le 1er septembre 2013 sur le fondement des dispositions déclarées contraires à la Constitution avant cette date aurait des conséquences manifestement excessives. Par suite, ces mesures ne peuvent être contestées sur le fondement de cette inconstitutionnalité ".<br/>
<br/>
              6. Il résulte de ce qui a été dit au point précédent que le Conseil constitutionnel a entendu que ne puissent être remis en cause les effets que les dispositions déclarées contraires à la Constitution ont produits avant le 1er septembre 2013 sur le fondement de cette inconstitutionnalité. Par suite, eu égard à la portée de l'article 62 de la Constitution, le moyen tiré de ce que l'arrêté litigieux du 10 janvier 2013 méconnaît l'article 7 de la Charte de l'environnement ne peut qu'être écarté. <br/>
<br/>
              7. En deuxième lieu, l'association requérante ne saurait utilement invoquer l'illégalité de la décision qu'elle attaque au regard des objectifs de la directive du Conseil du 27 juin 1985 concernant l'évaluation des incidences de certains projets publics et privés sur l'environnement, dès lors que celle-ci a été entièrement transposée en droit interne et a d'ailleurs été abrogée par la directive du 13 décembre 2011 concernant l'évaluation des incidences de certains projets publics et privés sur l'environnement.<br/>
<br/>
              8. En troisième lieu, aux termes de l'article 6 de la convention d'Aarhus sur l'accès à l'information, la participation du public au processus décisionnel et l'accès à la justice en matière d'environnement : " 2. Lorsqu'un processus décisionnel touchant l'environnement est engagé, le public concerné est informé comme il convient, de manière efficace et en temps voulu, par un avis au public ou individuellement, selon le cas, au début du processus (...) / 3. Pour les différentes étapes de la procédure de participation du public, il est prévu des délais raisonnables laissant assez de temps pour informer le public conformément au paragraphe 2 ci-dessus et pour que le public se prépare et participe effectivement aux travaux tout au long du processus décisionnel en matière d'environnement. / 4. Chaque Partie prend des dispositions pour que la participation du public commence au début de la procédure, c'est-à-dire lorsque toutes les options et solutions sont encore possibles et que le public peut exercer une réelle influence. ". Ces stipulations doivent être regardées comme produisant des effets directs dans l'ordre juridique interne.<br/>
<br/>
              9. Pour écarter le moyen tiré de la méconnaissance de ces stipulations, la cour administrative d'appel a relevé, par une appréciation souveraine exempte de dénaturation, que, si les dispositions de l'article L. 311-5 du code de l'énergie alors en vigueur ne prévoyaient pas de procédure permettant l'information et la participation du public, il ressortait des pièces du dossier qui lui était soumis que le projet de création d'une nouvelle unité de production d'électricité en Bretagne ainsi que le type de centrale, sa puissance et sa localisation à Landivisiau avaient été présentés dans le cadre de la Conférence bretonne de l'énergie réunissant un grand nombre de partenaires économiques et associatifs de l'Etat et de la région, lors de plusieurs réunions organisées dès le mois de septembre 2010, ainsi que dans un dossier de presse exposant les caractéristiques et les impacts attendus de la centrale, et qu'une concertation avec les élus et le public avait été organisée en 2012, avec l'ouverture d'un espace participatif consacré au projet sur le site internet de la préfecture et des " rendez-vous de la concertation " en juin, septembre et novembre 2012. Elle a pu en déduire, sans entacher son arrêt d'erreur de droit, dès lors que, d'une part, à ce stade de la procédure, le projet autorisé au titre du code de l'énergie portait seulement, ainsi qu'il a été dit au point 3, sur le mode de production, la capacité autorisée et le lieu d'implantation de l'installation, à l'exclusion d'éléments plus précis sur la mise en œuvre de ce projet, et, d'autre part, qu'une enquête publique devait se tenir sur le projet de centrale, en vue de la délivrance de l'autorisation requise au titre de la législation sur les installations classées pour la protection de l'environnement, que la concertation, qui avait eu lieu à un stade précoce de la procédure, avait permis au public de faire valoir ses observations et ses avis en temps utile, alors que la décision d'autorisation n'était pas encore prise, et que les mesures prises en l'espèce suffisaient à assurer la mise en œuvre des objectifs fixés par les stipulations rappelées au point 8. <br/>
<br/>
              Sur le respect des critères fixés par l'article L. 311-5 du code de l'énergie : <br/>
              En ce qui concerne la protection de l'environnement : <br/>
<br/>
              10. La cour a relevé, par une appréciation souveraine des pièces du dossier, que le site choisi pour l'implantation de la centrale de production d'électricité se trouvait dans une zone industrielle, qu'il n'était pas situé dans une zone Natura 2000 ni dans une zone naturelle protégée et que la préservation de la zone humide qu'il comporte sera garantie lors du choix de l'emprise de l'installation. En se fondant sur ces constatations ainsi que sur les mesures envisagées pour atténuer l'impact du projet en termes d'émissions sonores, d'effluents atmosphériques, de gestion de l'eau et sur les paysages pour écarter le moyen tiré de ce que le projet serait incompatible avec la protection de l'environnement et ne permettrait pas, par suite, le respect de ce critère, fixé par le 5° de l'article L. 311-5 de l'énergie, la cour n'a pas dénaturé les pièces du dossier qui lui était soumis. <br/>
<br/>
              En ce qui concerne les objectifs de programmation pluriannuelle des investissements : <br/>
<br/>
              11. C'est également par une appréciation souveraine exempte de dénaturation que la cour, après avoir relevé qu'il ressortait des pièces du dossier qui lui était soumis, notamment du rapport remis au Parlement sur la programmation pluriannuelle des investissements pour la période 2009-2020, relevant que la région Bretagne ne produit que 7 % de l'énergie qu'elle consomme et que la mise en service d'un nouveau moyen de production dans la région de Saint-Brieuc était préconisée, a estimé que le projet d'implantation d'une installation de production d'électricité à Landivisiau n'était pas incompatible avec les objectifs de la programmation pluriannuelle des investissements et, par suite, ne méconnaissait pas cet autre critère fixé par le 5° de l'article L. 311-5 du code de l'énergie. <br/>
<br/>
              En ce qui concerne l'appréciation des capacités financières de la société Direct Energie Génération : <br/>
<br/>
              12. La cour a relevé, par une appréciation souveraine, qu'il ressortait des pièces du dossier qui lui était soumis que le dossier de demande de la société Direct Énergie Génération indiquait que le financement des principaux investissements, d'un montant de 490 millions d'euros, serait assuré par des fonds propres apportés par les actionnaires à hauteur de 30 % et par un financement bancaire pour 70 %. Elle n'a pas davantage entaché son arrêt de dénaturation en se fondant sur les lettres d'intention de plusieurs établissements de crédit bancaire d'assurer le recours à l'endettement pour un montant cumulé de 530 millions d'euros et sur la forte croissance du résultat net positif du groupe Direct Energie entre 2012 et 2015 pour estimer que le ministre n'avait pas commis d'erreur manifeste dans l'appréciation des capacités financières de la société, critère fixé par le 4° de l'article L. 311-5 du code de l'énergie.  <br/>
<br/>
              13. Il résulte de tout ce qui précède que l'association Force 5 n'est pas fondée à demander l'annulation de l'arrêt qu'elle attaque. <br/>
<br/>
              14. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'association Force 5 une somme de 3 000 euros à verser à la société Total Direct Énergie, venue aux droits de la société Direct Energie Génération, en application de l'article L. 761-1 du code de justice administrative. Ces dispositions font en revanche obstacle à ce qu'une somme soit mise à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante. <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de l'association Force 5 est rejeté. <br/>
Article 2 : L'association Force 5 versera la somme de 3 000 euros à la société Total Direct Énergie au titre de l'article L. 761-1 du code de justice administrative. <br/>
Article 3 : La présente décision sera notifiée à l'association Force 5, à la ministre de la transition écologique, à la société Total Direct Énergie et à la Compagnie électrique de Bretagne.<br/>
<br/>
<br/>
              Délibéré à l'issue de la séance du 8 octobre 2021 où siégeaient : Mme Christine Maugüé, présidente adjointe de la Section du contentieux, présidant ; M. A... F..., M. Fabien Raynaud, présidents de chambre ; Mme L... H..., M. J... C..., Mme E... I..., M. D... G..., M. Cyril Roger-Lacan, conseillers d'Etat et Mme Catherine Moreau, conseillère d'Etat en service extraordinaire-rapporteure. <br/>
<br/>
              Rendu le 15 novembre 2021.<br/>
<br/>
<br/>
<br/>
                 La Présidente : <br/>
                 Signé : Mme Christine Maugüé<br/>
 		La rapporteure : <br/>
      Signé : Mme Catherine Moreau<br/>
                 La secrétaire :<br/>
                 Signé : Mme K... B...<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-01-02-01 ACTES LÉGISLATIFS ET ADMINISTRATIFS. - DIFFÉRENTES CATÉGORIES D'ACTES. - ACCORDS INTERNATIONAUX. - APPLICABILITÉ. - CONVENTION D'AARHUS DU 25 JUIN 1998 - EFFET DIRECT [RJ1] - ARTICLE 6§4 - EXISTENCE [RJ2].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">44-006-01 NATURE ET ENVIRONNEMENT. - CONVENTION D'AARHUS DU 25 JUIN 1998 - EFFET DIRECT [RJ1] - ARTICLE 6§4 - EXISTENCE [RJ2].
</SCT>
<ANA ID="9A"> 01-01-02-01 Le paragraphe 4 de l'article 6 de la convention d'Aarhus du 25 juin 1998 doit être regardé comme produisant des effets directs dans l'ordre juridique interne.</ANA>
<ANA ID="9B"> 44-006-01 Le paragraphe 4 de l'article 6 de la convention d'Aarhus du 25 juin 1998 doit être regardé comme produisant des effets directs dans l'ordre juridique interne.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf., sur la notion d'effet direct, CE, Assemblée, 11 avril 2012, Groupe d'information et de soutien des immigrés et Fédération des associations pour la promotion et l'insertion par le logement, n° 322326, p. 142....[RJ2] Ab. jur., sur ce point, CE, 6 juin 2007, Commune de Groslay et autres, n°s 292942 293109 293158, p. 237. Rappr., s'agissant du paragraphe 1er, a) du même article, CE, 6 octobre 2021, Association PRIARTEM et autres, n°s 446302 et autres, à mentionner aux Tables ; s'agissant des paragraphes 2, 3, et 7 du même article, CE, 6 juin 2007, Commune de Groslay et autres, n°s 292942 293109 293158, p. 237.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
