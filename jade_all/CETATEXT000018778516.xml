<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000018778516</ID>
<ANCIEN_ID>JG_L_2008_05_000000305826</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/18/77/85/CETATEXT000018778516.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème et 7ème sous-sections réunies, 07/05/2008, 305826</TITRE>
<DATE_DEC>2008-05-07</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>305826</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème et 7ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Delarue</PRESIDENT>
<AVOCATS>SCP CELICE, BLANCPAIN, SOLTNER</AVOCATS>
<RAPPORTEUR>Mlle Sophie-Justine  Liéber</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Prada Bordenave Emmanuelle</COMMISSAIRE_GVT>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête sommaire et le mémoire complémentaire, enregistrés les 21 mai et 21 août 2007 au secrétariat du contentieux du Conseil d'Etat, présentés pour la SOCIETE AIR FRANCE, dont le siège est 45, rue de Paris, à Roissy-Charles-de-Gaulle, Cedex (95747), représentée par son président-directeur général en exercice ; la SOCIETE AIR FRANCE demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler la décision du 20 mars 2007 par laquelle l'Autorité de contrôle des nuisances sonores aéroportuaires (ACNUSA) lui a infligé une amende administrative de 6 000 euros ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 1 000 euros en application de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de l'aviation civile ;<br/>
<br/>
              Vu l'arrêté du 6 novembre 2003 portant interdiction entre 0 heure et 5 heures des décollages d'aéronefs non programmés pendant ladite période horaire sur l'aérodrome de Paris-Charles-de-Gaulle ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mlle Sophie-Justine Liéber, Maître des Requêtes,  <br/>
<br/>
              - les observations de la SCP Célice, Blancpain, Soltner, avocat de la SOCIETE AIR FRANCE, <br/>
<br/>
              - les conclusions de Mme Emmanuelle Prada Bordenave, Commissaire du gouvernement ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant qu'aux termes de l'article L. 227-4 du code de l'aviation civile : « Sur proposition de la Commission nationale de prévention des nuisances, l'Autorité de contrôle des nuisances sonores aéroportuaires prononce une amende administrative à l'encontre : - soit de la personne physique ou morale exerçant une activité de transport aérien public (...), dont l'aéronef ne respecte pas les mesures prises par le ministre chargé de l'aviation civile sur un aérodrome fixant (...) des restrictions permanentes ou temporaires d'usage de certains types d'aéronefs en fonction de la classification acoustique (...). / Durant la procédure suivie devant l'autorité et la commission, la personne concernée doit avoir connaissance de l'ensemble des éléments de son dossier. Elle doit pouvoir être entendue par la commission avant que celle-ci se prononce sur son cas et se faire représenter ou assister par la personne de son choix./ Les amendes administratives sont prononcées par l'autorité et ne peuvent excéder, par manquement constaté, un montant de 1 500 euros, pour une personne physique et de 20 000 euros pour une personne morale (...) » ;<br/>
<br/>
              Considérant que l'Autorité de contrôle des nuisances sonores aéroportuaires (ACNUSA) a infligé à la SOCIETE AIR FRANCE une amende de 6000 euros pour avoir méconnu les dispositions de l'article 1er de l'arrêté du 6 novembre 2003 susvisé, aux termes desquelles : « En vue de réduire les nuisances sonores autour de l'aérodrome de Paris-Charles-de-Gaulle, le décollage d'un aéronef de cette plate-forme entre 0 heure et 4 h 59, heures locales de départ de l'aire de stationnement, est interdit s'il n'a pas fait l'objet de l'attribution d'un créneau horaire de départ dans ladite plage horaire le jour en question » ; <br/>
<br/>
              Considérant que la décision de sanction, qui rappelle notamment les circonstances dans lesquelles le manquement est intervenu, comporte les considérations de droit et de fait sur lesquelles elle est fondée ; qu'elle est ainsi suffisamment motivée ;<br/>
<br/>
              Considérant que la rubrique « sanctions » publiée sur le site internet de l'ACNUSA, qui comporte des informations d'ordre général destinées à un large public et mentionnant succinctement des exemples pouvant entraîner l'exonération de sanctions, mais dont l'objet n'est ni de fixer une grille de sanctions, ni de définir une position destinée à guider les membres ou les services de l'Autorité lorsqu'elle doit prononcer de telles sanctions, n'a pas le caractère d'une directive ; que le moyen tiré de ce que l'ACNUSA n'aurait pas respecté une directive doit donc être écarté ;<br/>
<br/>
              Considérant que, contrairement à ce que soutient la SOCIETE AIR FRANCE, les mauvaises conditions météorologiques ayant retardé l'arrivée des agents de piste, si elles étaient extérieures à la compagnie et imprévisibles, n'étaient pas irrésistibles dès lors qu'elles n'avaient pas pour effet de contraindre l'appareil à décoller et ne peuvent être donc regardées comme relevant d'un cas de force majeure justifiant l'exonération de toute sanction ; <br/>
<br/>
              Considérant que l'article L. 227-4 du code de l'aviation civile précité prévoit que les sanctions maximum que l'Autorité de contrôle des nuisances sonores aéroportuaires peut prononcer sont de 20 000 euros pour une personne morale ; qu'en infligeant une amende de 6000 euros à la requérante, compte tenu de l'atteinte portée à la tranquillité des riverains imputable au dépassement de quinze minutes du début de la plage horaire de nuit, mais en tenant compte, pour moduler le montant de l'amende, des circonstances météorologiques défavorables et de la faible durée du dépassement, l'Autorité n'a pas pris une sanction disproportionnée à l'encontre de la requérante ; que la circonstance qu'à l'occasion d'infractions à la même réglementation, l'ACNUSA ait décidé, à une période antérieure et alors que les faits étaient d'ailleurs différents, de ne pas prononcer de sanction à l'encontre de la compagnie, est sans incidence sur la légalité de la sanction contestée ;<br/>
<br/>
              Considérant qu'il résulte de tout ce qui précède que la SOCIETE AIR FRANCE n'est pas fondée à demander l'annulation de la décision attaquée ;<br/>
<br/>
              Sur les conclusions tendant à l'application de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mise à la charge de l'Etat, qui n'est pas la partie perdante dans la présente instance, la somme que la SOCIETE AIR FRANCE demande au titre des frais exposés par elle et non compris dans les dépens ; qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par l'ACNUSA au même titre ; <br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de la SOCIETE AIR FRANCE est rejetée.<br/>
Article 2 : Les conclusions de l'Autorité de contrôle des nuisances sonores aéroportuaires tendant à l'application des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La présente décision sera notifiée à la SOCIETE AIR FRANCE, à l'Autorité de contrôle des nuisances sonores aéroportuaires et au ministre d'Etat, ministre de l'écologie, de l'énergie, du développement durable et de l'aménagement du territoire.<br/>
<br/>
<br/>
<br/>
</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-01-05 ACTES LÉGISLATIFS ET ADMINISTRATIFS. DIFFÉRENTES CATÉGORIES D'ACTES. ACTES ADMINISTRATIFS - NOTION. - DIRECTIVE - NOTION - RUBRIQUE FIGURANT SUR UN SITE INTERNET, COMPORTANT DES INFORMATIONS D'ORDRE GÉNÉRAL - ABSENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">44-05-01 NATURE ET ENVIRONNEMENT. AUTRES MESURES PROTECTRICES DE L'ENVIRONNEMENT. LUTTE CONTRE LE BRUIT. - A) DIRECTIVE - NOTION - RUBRIQUE FIGURANT SUR UN SITE INTERNET, COMPORTANT DES INFORMATIONS D'ORDRE GÉNÉRAL - ABSENCE -  B) FAITS CONSTITUTIFS DE CAS DE FORCE MAJEURE, POUVANT ENTRAÎNER L'EXONÉRATION DE SANCTIONS ADMINISTRATIVES - RETARDS DE DÉCOLLAGE - ABSENCE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">52-045 POUVOIRS PUBLICS ET AUTORITÉS ADMINISTRATIVES INDÉPENDANTES. AUTORITÉS ADMINISTRATIVES INDÉPENDANTES. - A) DIRECTIVE - NOTION - RUBRIQUE FIGURANT SUR UN SITE INTERNET, COMPORTANT DES INFORMATIONS D'ORDRE GÉNÉRAL - ABSENCE -  B) FAITS CONSTITUTIFS DE CAS DE FORCE MAJEURE, POUVANT ENTRAÎNER L'EXONÉRATION DE SANCTIONS ADMINISTRATIVES - RETARDS DE DÉCOLLAGE - ABSENCE.
</SCT>
<SCT ID="8D" TYPE="PRINCIPAL">65-03 TRANSPORTS. TRANSPORTS AÉRIENS. - A) DIRECTIVE - NOTION - RUBRIQUE FIGURANT SUR UN SITE INTERNET, COMPORTANT DES INFORMATIONS D'ORDRE GÉNÉRAL - ABSENCE -  B) FAITS CONSTITUTIFS DE CAS DE FORCE MAJEURE, POUVANT ENTRAÎNER L'EXONÉRATION DE SANCTIONS ADMINISTRATIVES - RETARDS DE DÉCOLLAGE - ABSENCE.
</SCT>
<ANA ID="9A"> 01-01-05 Amendes infligées à la société Air France pour décollages d'avions de Roissy pendant la plage horaire de nuit (de 0h à 4h59) lors de laquelle ces décollages sont prohibés. La rubrique sanctions publiée sur le site internet de l'Autorité de contrôle des nuisances sonores aéroportuaires, comportant des informations d'ordre général destinées à un large public et mentionnant succinctement des exemples pouvant entraîner l'exonération de sanctions mais dont l'objet n'est ni de fixer une échelle de sanctions, ni de définir à l'intention des membres ou des services de l'Autorité une position destinée à la guider lorsqu'elle doit prononcer de telles sanctions, ne peut être assimilée à une directive [RJ1].</ANA>
<ANA ID="9B"> 44-05-01 a) Amendes infligées à la société Air France pour décollages d'avions de Roissy pendant la plage horaire de nuit (de 0h à 4h59) lors de laquelle ces décollages sont prohibés. La rubrique sanctions publiée sur le site internet de l'Autorité de contrôle des nuisances sonores aéroportuaires, comportant des informations d'ordre général destinées à un large public et mentionnant succinctement des exemples pouvant entraîner l'exonération de sanctions mais dont l'objet n'est ni de fixer une échelle de sanctions, ni de définir à l'intention des membres ou des services de l'Autorité une position destinée à la guider lorsqu'elle doit prononcer de telles sanctions, ne peut être assimilée à une directive [RJ1].,,b) Alors même que les faits à l'origine du retard du décollage sont extérieurs à la compagnie aérienne et imprévisibles, ils ne sont pas irrésistibles dès lors qu'ils ne font pas par eux-mêmes obligation à cette compagnie de faire décoller ses appareils durant la plage horaire de nuit. Ils ne peuvent dès lors être regardés comme constitutifs d'un cas de force majeure justifiant l'exonération de toute sanction.</ANA>
<ANA ID="9C"> 52-045 a) Amendes infligées à la société Air France pour décollages d'avions de Roissy pendant la plage horaire de nuit (de 0h à 4h59) lors de laquelle ces décollages sont prohibés. La rubrique sanctions publiée sur le site internet de l'Autorité de contrôle des nuisances sonores aéroportuaires, comportant des informations d'ordre général destinées à un large public et mentionnant succinctement des exemples pouvant entraîner l'exonération de sanctions mais dont l'objet n'est ni de fixer une échelle de sanctions, ni de définir à l'intention des membres ou des services de l'Autorité une position destinée à la guider lorsqu'elle doit prononcer de telles sanctions, ne peut être assimilée à une directive [RJ1].,,b) Alors même que les faits à l'origine du retard du décollage sont extérieurs à la compagnie aérienne et imprévisibles, ils ne sont pas irrésistibles dès lors qu'ils ne font pas par eux-mêmes obligation à cette compagnie de faire décoller ses appareils durant la plage horaire de nuit. Ils ne peuvent dès lors être regardés comme constitutifs d'un cas de force majeure justifiant l'exonération de toute sanction.</ANA>
<ANA ID="9D"> 65-03 a) Amendes infligées à la société Air France pour décollages d'avions de Roissy pendant la plage horaire de nuit (de 0h à 4h59) lors de laquelle ces décollages sont prohibés. La rubrique sanctions publiée sur le site internet de l'Autorité de contrôle des nuisances sonores aéroportuaires, comportant des informations d'ordre général destinées à un large public et mentionnant succinctement des exemples pouvant entraîner l'exonération de sanctions mais dont l'objet n'est ni de fixer une échelle de sanctions, ni de définir à l'intention des membres ou des services de l'Autorité une position destinée à la guider lorsqu'elle doit prononcer de telles sanctions, ne peut être assimilée à une directive [RJ1].,,b) Alors même que les faits à l'origine du retard du décollage sont extérieurs à la compagnie aérienne et imprévisibles, ils ne sont pas irrésistibles dès lors qu'ils ne font pas par eux-mêmes obligation à cette compagnie de faire décoller ses appareils durant la plage horaire de nuit. Ils ne peuvent dès lors être regardés comme constitutifs d'un cas de force majeure justifiant l'exonération de toute sanction.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Comp., Section, 11 décembre 1970, Crédit foncier de France, n° 78880, p. 750.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
