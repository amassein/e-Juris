<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031537086</ID>
<ANCIEN_ID>JG_L_2015_11_000000381174</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/53/70/CETATEXT000031537086.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème / 4ème SSR, 27/11/2015, 381174, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-11-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>381174</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème / 4ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>LE PRADO ; SCP FOUSSARD, FROGER</AVOCATS>
<RAPPORTEUR>Mme Manon Perrière</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:381174.20151127</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A...B...a demandé au tribunal administratif de Marseille d'annuler l'arrêté du 26 janvier 2010 par lequel le maire de Marseille a délivré un permis de construire modificatif à la société civile immobilière (SCI) La Selika. Par un jugement n° 1004192 du 9 février 2012, le tribunal administratif a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 12MA01310 du 10 avril 2014, la cour administrative d'appel de Marseille a, sur la requête de M.B..., annulé ce jugement et l'arrêté du maire de Marseille. <br/>
<br/>
              Par un pourvoi, enregistré le 11 juin 2014 au secrétariat du contentieux du Conseil d'Etat, la SCI La Selika demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de M. B... ;<br/>
<br/>
              3°) de mettre à la charge de M. B...la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
<br/>
              - le code civil ;<br/>
<br/>
              - le code de l'urbanisme ;<br/>
<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Manon Perrière, auditeur,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Foussard, Froger, avocat de la SCI La Selika et à Me Le Prado, avocat de M. B...;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, par un arrêté du 26 janvier 2010, le maire de Marseille a délivré un permis de construire modificatif à la société civile immobilière (SCI) La Selika l'autorisant à augmenter la surface hors oeuvre nette d'une maison de retraite dont la construction avait été autorisée par un précédent arrêté du 13 septembre 2005 ; que, par un jugement du 9 février 2012, le tribunal administratif de Marseille a rejeté la demande de M. B...tendant à l'annulation de ce permis modificatif ; que, par l'arrêt du 10 avril 2014 contre lequel la SCI La Selika se pourvoit en cassation, la cour administrative d'appel de Marseille a annulé le jugement du tribunal administratif et le permis de construire modificatif contesté au motif que le projet méconnaissait l'article UC 7 du règlement du plan d'occupation des sols de Marseille ; <br/>
<br/>
              2. Considérant qu'aux termes de l'article 7 du règlement de la zone UC du plan d'occupation des sols de Marseille, relatif à l'implantation des constructions par rapport aux limites séparatives : " 1. La distance mesurée horizontalement de tout point d'une construction au point le plus proche des limites séparatives de la propriété est au moins égale à la différence d'altitude entre ces deux points diminuée de 3 mètres, sans être inférieure à 3 mètres, soit L &gt;= H - 3 &gt;= 3m. / (...) " ; que, pour juger que le permis litigieux avait été délivré en méconnaissance de ces dispositions, la cour administrative d'appel a estimé que, contrairement à ce que faisaient apparaître les plans joints à la demande de permis modificatif, le mur séparant le terrain d'assiette du projet de la parcelle de M. B...n'appartenait pas au pétitionnaire mais présentait le caractère d'un mur mitoyen, de sorte que la limite séparative passait en son milieu ; <br/>
<br/>
              3. Considérant, en premier lieu, que lorsque les éléments qui sont présentés devant lui révèlent que le projet autorisé méconnaît une règle d'urbanisme, le juge doit annuler le permis, même s'il a été délivré au vu d'un dossier qui, contenant des informations inexactes, ne faisait pas apparaître cette méconnaissance ; qu'il en va notamment ainsi lorsqu'il apparaît que, contrairement à ce qu'impliquaient les indications fournies, même de bonne foi, par le pétitionnaire dans le dossier de demande de permis sur la propriété d'un mur séparatif et l'emplacement des limites séparatives qui en résultait, les règles relatives à l'implantation des constructions par rapport à ces limites ont été méconnues ; qu'est  à cet égard sans incidence la circonstance que le pétitionnaire ait fourni l'attestation prévue à l'article R. 431-5 du code de l'urbanisme et doive ainsi être regardé comme ayant qualité pour construire sur le terrain en cause ; qu'ainsi, en jugeant que la circonstance que la société pétitionnaire avait, de bonne foi, estimé qu'elle était propriétaire du mur séparatif en cause ne faisait pas obstacle à ce que le permis litigieux soit jugé illégal pour les motifs mentionnés au point 2 ci-dessus, la cour administrative d'appel n'a pas commis d'erreur de droit ; <br/>
<br/>
              4. Considérant, en deuxième lieu, que les dispositions de l'article 653 du code civil établissent une présomption légale de copropriété des murs séparatifs de propriété ; qu'il ressort par ailleurs des pièces du dossier soumis aux juges du fond que les actes notariés produits par M. B...mentionnent le caractère mitoyen des murs de clôture de sa parcelle et font au surplus apparaître que le sommet du mur séparatif litigieux est légèrement arrondi, et permet ainsi l'écoulement des eaux de pluie sur les deux parcelles ; que la SCI La Selika n'a produit devant les juges du fond aucun élément de nature à établir que le mur séparatif n'était pas mitoyen ; qu'ainsi, c'est sans erreur de droit et sans erreur de qualification juridique des faits que la cour a jugé que, la question de la propriété du mur mitoyen ne présentant pas un caractère sérieux, il n'y avait pas lieu de poser une question préjudicielle au juge judiciaire, et que les documents produits par M. B...établissaient le caractère mitoyen du mur ;<br/>
<br/>
              5. Considérant, enfin, qu'il ressort des pièces du dossier soumis aux juges du fond, notamment des plans annexés à la demande de permis de construire, qu'en plusieurs points la distance de la construction à la limite séparative avec la propriété de M. B..., calculée par rapport au bord extérieur de ce mur, était de trois mètres ; que, calculée par rapport au milieu du mur, du fait du caractère mitoyen de ce dernier, elle était inférieure à trois mètres ; que, par suite, en déduisant du caractère mitoyen du mur situé en limite séparative des deux parcelles que le permis de construire modificatif déposé par la SCI La Selika méconnaissait les dispositions du plan d'occupation des sols de Marseille relatives aux règles de distance par rapport aux limites séparatives, la cour administrative d'appel, qui a suffisamment motivé son arrêt sur ce point, n'a pas commis d'erreur de droit ;<br/>
<br/>
              6. Considérant qu'il résulte de tout ce qui précède que la SCI La Selika n'est pas fondée à demander l'annulation de l'arrêt attaqué ;<br/>
<br/>
              7. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à la charge de M. B...qui n'est pas, dans la présente instance, la partie perdante ; qu'il y a lieu, dans les circonstances de l'espèce, de mettre la somme de 3 000 euros à la charge de la SCI La Selika au titre des mêmes dispositions ;<br/>
<br/>
<br/>
<br/>
<br/>			D E C I D E :<br/>
              			--------------<br/>
<br/>
Article 1er : Le pourvoi de la SCI La Selika est rejeté.<br/>
Article 2 : La SCI La Selika versera à M. B...la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée à la société civile immobilière La Selika et à M. A... B....<br/>
Copie en sera adressée à la commune de Marseille.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
