<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042538319</ID>
<ANCIEN_ID>JG_L_2020_11_000000431067</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/53/83/CETATEXT000042538319.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère chambre, 18/11/2020, 431067, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-11-18</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>431067</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>OCCHIPINTI</AVOCATS>
<RAPPORTEUR>Mme Manon Chonavel</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Marie Sirinelli</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2020:431067.20201118</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme B... C... a demandé à la commission départementale d'aide sociale de Saône-et-Loire d'annuler la mise en demeure émise le 15 janvier 2016 par le comptable public du centre des finances publiques de Mâcon pour le paiement de la somme de 8 585,04 euros correspondant au solde d'un indu de revenu minimum d'insertion pour la période du 1er août 2007 au 31 mai 2009 et de la décharger du paiement de cette somme. Par une décision du 2 mai 2017, la commission départementale d'aide sociale de Saône-et-Loire a rejeté cette demande.<br/>
<br/>
              Par une décision n° 170317 du 25 septembre 2018, la Commission centrale d'aide sociale a rejeté l'appel formé par Mme C... contre cette décision.  <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 24 mai et 29 juillet 2019 au secrétariat du contentieux du Conseil d'Etat, Mme C... demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler la décision de la Commission centrale d'aide sociale ;<br/>
<br/>
              2°) de mettre à la charge du département de Saône-et-Loire la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de l'action sociale et des familles ;<br/>
              - la loi n° 91-647 du 10 juillet 1991; <br/>
              - le décret n° 91-1266 du 19 décembre 1991 ;<br/>
              - le décret n° 2018-928 du 29 octobre 2018 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme A... D..., Auditrice,  <br/>
<br/>
              - les conclusions de Mme Marie Sirinelli, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Occhipinti, avocat de Mme C... ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis au juge du fond qu'à la suite d'un contrôle de la situation de Mme B... C..., la caisse d'allocations familiales de Saône-et-Loire a décidé le 26 août 2009 de récupérer à son encontre un indu de revenu minimum d'insertion pour la période allant d'août 2007 à mai 2009. Le 27 mai 2010, le département de Saône-et-Loire a émis un titre exécutoire pour le recouvrement de cet indu. Mme C... a demandé à la commission départementale d'aide sociale de Saône-et-Loire d'annuler la mise en demeure valant commandement de payer émise le 15 janvier 2016, sur le fondement du 4° de l'article L. 1617-5 du code général des collectivités territoriales, par le comptable public du centre des finances publiques de Mâcon pour le paiement de la somme de 8 585,04 euros, correspondant au solde de l'indu de revenu minimum d'insertion. Par une décision du 2 mai 2017, la commission départementale d'aide sociale de Saône-et-Loire a rejeté sa demande. Par une décision du 25 septembre 2018 contre laquelle Mme C... se pourvoit en cassation, la Commission centrale d'aide sociale a rejeté son appel formé contre cette décision.<br/>
<br/>
              Sur la fin de non-recevoir tirée de la tardiveté du pourvoi :<br/>
<br/>
              2. Aux termes de l'article 39 du décret du 19 décembre 1991 portant application de la loi n° 91-647 du 10 juillet 1991 relative à l'aide juridique : " En matière civile, lorsqu'une demande d'aide juridictionnelle en vue de se pourvoir devant la Cour de cassation (...) est adressée au bureau d'aide juridictionnelle établi près la Cour de cassation avant l'expiration du délai imparti pour le dépôt du pourvoi, (...) ce délai est interrompu. Un nouveau délai court à compter du jour de la réception par l'intéressé de la notification de la décision du bureau d'aide juridictionnelle ou, si elle est plus tardive, de la date à laquelle un auxiliaire de justice a été désigné. Ce dernier délai est interrompu lorsque le recours prévu à l'article 23 de la loi du 10 juillet 1991 est régulièrement formé par l'intéressé. (...) / Le délai alors imparti pour le dépôt du pourvoi (...) court à compter de la date de la réception par l'intéressé de la notification de la décision prise sur recours confirmant la décision déférée ou, si elle est plus tardive, de la date à laquelle un auxiliaire de justice a été désigné. / Les délais de recours sont interrompus dans les mêmes conditions lorsque l'aide juridictionnelle est sollicitée à l'occasion d'une instance devant le Conseil d'Etat (...) ". L'article 23 de la loi du 10 juillet 1991 permet à la personne à laquelle le bénéfice de l'aide juridictionnelle a été refusé de déférer la décision du bureau d'aide juridictionnelle institué auprès du Conseil d'Etat au président de la section du contentieux du Conseil d'Etat, dans un délai fixé par le premier alinéa de l'article 56 du décret du 19 décembre 1991 à quinze jours à compter de la notification qui lui a été faite de cette décision.<br/>
<br/>
              3. Il ressort des pièces de la procédure que dans le délai de deux mois courant à compter de la date de la notification de la décision de la Commission centrale d'aide sociale attaquée, Mme C... a présenté une demande d'aide juridictionnelle qui, par application des dispositions citées ci-dessus, a interrompu le délai du recours contentieux. Cette demande a été rejetée par une décision du bureau d'aide juridictionnelle du 28 janvier 2019, notifiée le 12 février suivant. Le 13 février 2019, soit dans le délai de quinze jours prévu à l'article 56 du décret du 19 décembre 1991, Mme C... a formé un recours contre cette décision, interrompant ainsi le nouveau délai de deux mois qui lui était imparti pour déposer un pourvoi. . Ce recours a été rejeté par une décision du 9 avril 2019, dont elle a reçu la notification le 16 avril suivant. Le pourvoi a été présenté le 24 mai 2019, avant l'expiration du délai de deux mois qui avait recommencé à courir à compter de cette notification. Par suite, le département de Saône-et-Loire n'est pas fondé à soutenir que le pourvoi de Mme C... serait tardif.<br/>
<br/>
              Sur le bien-fondé du pourvoi :<br/>
<br/>
              4. D'une part, aux termes de l'article L. 262-40 du code de l'action sociale et des familles, dans sa rédaction applicable au moment du versement de l'allocation, l'action du bénéficiaire pour le paiement de l'allocation de revenu minimum d'insertion " se prescrit par deux ans. Cette prescription est également applicable, sauf en cas de fraude ou de fausse déclaration, à l'action intentée par un organisme payeur en recouvrement des sommes indûment payées ". <br/>
<br/>
              5. D'autre part, aux termes de l'article 2262 du code civil, dans sa rédaction antérieure à l'entrée en vigueur de la loi du 17 juin 2008 portant réforme de la prescription en matière civile : " Toutes les actions, tant réelles que personnelles, sont prescrites par trente ans (...) ". En outre, le point de départ de la prescription de l'action en récupération de l'indu devait être reporté à la date où l'administration avait eu connaissance de la fraude commise par l'allocataire. La loi du 17 juin 2008 a réduit la durée de la prescription civile de droit commun pour prévoir, à l'article 2224 du code civil, que : " Les actions personnelles ou mobilières se prescrivent par cinq ans à compter du jour où le titulaire d'un droit a connu ou aurait dû connaître les faits lui permettant de l'exercer ". Aux termes du premier alinéa de l'article 2232 de ce code, dans sa rédaction issue de la même loi : " Le report du point de départ, la suspension ou l'interruption de la prescription ne peut avoir pour effet de porter le délai de la prescription extinctive au-delà de vingt ans à compter du jour de la naissance du droit ". Enfin, aux termes du II de l'article 26 de la loi du 17 juin 2008 : " Les dispositions de la présente loi qui réduisent la durée de la prescription s'appliquent aux prescriptions à compter du jour de l'entrée en vigueur de la présente loi, sans que la durée totale puisse excéder la durée prévue par la loi antérieure ".<br/>
<br/>
              6. Pour écarter le moyen tiré de ce que la créance du département de Saône-et-Loire était prescrite, la Commission centrale d'aide sociale a estimé que la prescription de l'action en récupération des sommes indument payées avait été interrompue par le titre exécutoire émis par le département le 26 mai 2010. En jugeant que ce titre exécutoire avait eu un effet interruptif de prescription sans rechercher si et à quelle date Mme C..., qui soutenait ne pas l'avoir reçu, en avait eu notification, la Commission centrale a insuffisamment motivé sa décision et commis une erreur de droit.<br/>
<br/>
              7. Il résulte de ce qui précède que Mme C... est fondée à demander pour ce motif l'annulation de la décision de la Commission centrale d'aide sociale qu'elle attaque. Le moyen retenu suffisant à entraîner cette annulation, il n'est pas nécessaire d'examiner les autres moyens de son pourvoi.<br/>
<br/>
              8. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge du département de Saône-et-Loire une somme de 2 000 euros à verser à Mme C... au titre de l'article L. 761-1 du code de justice administrative. Les dispositions de cet article font obstacle à ce que la somme demandée par le département de Saône-et-Loire soit mise à la charge de Mme C..., qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
Article 1er : La décision de la Commission centrale d'aide sociale du 25 septembre 2018 est annulée.<br/>
<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Paris.<br/>
Article 3 : Le département de Saône-et-Loire versera à Mme C... une somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
Article 4 : Les conclusions présentées par le département de Saône-et-Loire au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée à Mme B... C... et au département de Saône-et-Loire.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
