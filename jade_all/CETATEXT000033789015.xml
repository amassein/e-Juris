<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033789015</ID>
<ANCIEN_ID>JG_L_2016_12_000000395706</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/78/90/CETATEXT000033789015.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème - 4ème chambres réunies, 30/12/2016, 395706</TITRE>
<DATE_DEC>2016-12-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>395706</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème - 4ème chambres réunies</FORMATION>
<TYPE_REC/>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DE NERVO, POUPET</AVOCATS>
<RAPPORTEUR>M. Olivier Rousselle</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2016:395706.20161230</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B...A...a demandé au tribunal administratif de Cergy-Pontoise d'ordonner, en application de l'article L. 441-2-3-1 du code de la construction et de l'habitation, son logement par l'Etat dans un délai d'un mois à compter de la notification du jugement, sous astreinte de 500 euros par jour de retard à compter de cette date. Par une ordonnance n°1508491 du 26 octobre 2015, le tribunal administratif a rejeté sa demande.<br/>
<br/>
              Par un pourvoi et un mémoire complémentaire, enregistrés les 30 décembre 2015 et 29 mars 2016 au secrétariat du contentieux du Conseil d'Etat,  M. A...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande ;<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
<br/>
              - le code de la construction et de l'habitation ;<br/>
<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Olivier Rousselle, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP de Nervo, Poupet, avocat de M.A....<br/>
<br/>
<br/>
<br/>1.Considérant, en premier lieu, qu'aux termes du I de l'article L. 441-2-3-1 du code de la construction et de l'habitation : "  I.- Le demandeur qui a été reconnu par la commission de médiation comme prioritaire et comme devant être logé d'urgence et qui n'a pas reçu, dans un délai fixé par décret, une offre de logement tenant compte de ses besoins et de ses capacités peut introduire un recours devant la juridiction administrative tendant à ce que soit ordonné son logement ou son relogement. (...) " ; qu'aux termes de l'article R. 441-16-1 du même code, dans sa rédaction applicable au litige porté devant le juge du fond :  " A compter du 1er décembre 2008, le recours devant la juridiction administrative prévu au I de l'article L. 441-2-3-1 peut être introduit par le demandeur qui n'a pas reçu d'offre de logement tenant compte de ses besoins et capacités passé un délai de trois mois à compter de la décision de la commission de médiation le reconnaissant comme prioritaire et comme devant être logé d'urgence. Dans les départements d'outre-mer et, jusqu'au 1er janvier 2014, dans les départements comportant au moins une agglomération, ou une partie d'une agglomération, de plus de 300 000 habitants, ce délai est de six mois " ; <br/>
<br/>
              2. Considérant, en deuxième lieu, qu'aux termes de l'article R. 778-1 du code de justice administrative : "  Sont présentées, instruites et jugées selon les dispositions du présent code, sous réserve des dispositions particulières du code de la construction et de l'habitation et des dispositions du présent chapitre : 1° Les requêtes introduites par les demandeurs reconnus par la commission de médiation prévue à l'article L. 441-2-3 du code de la construction et de l'habitation comme prioritaires et devant se voir attribuer un logement en urgence, en application des dispositions du II du même article, et qui n'ont pas, passé le délai mentionné à l'article R. 441-16-1 du même code, reçu une offre de logement tenant compte de leurs besoins et de leurs capacités... " ; qu'aux termes de l'article R. 778-2 de ce code : " Les requêtes mentionnées à l'article R. 778-1 sont présentées dans un délai de quatre mois à compter de l'expiration des délais prévus aux articles R. 441-16-1, R. 441-17 et R. 441-18 du code de la construction et de l'habitation. Ce délai n'est toutefois opposable au requérant que s'il a été informé, dans la notification de la décision de la commission de médiation ou dans l'accusé de réception de la demande adressée au préfet en l'absence de commission de médiation, d'une part, de celui des délais mentionnés aux articles R. 441-16-1, R. 441-17 et R. 441-18 de ce code qui était applicable à sa demande et, d'autre part, du délai prévu par le présent article pour saisir le tribunal administratif./ A peine d'irrecevabilité, les requêtes doivent être accompagnées, sauf impossibilité justifiée, soit de la décision de la commission de médiation dont se prévaut le requérant, soit, en l'absence de commission, d'une copie de la demande adressée par le requérant au préfet " ; que l'article R. 441-18-2 du code de la construction et de l'habitation précise que, lorsque la commission de médiation reconnaît, en application de l'article L. 441-2-3, que le demandeur est prioritaire et doit se voir attribuer un logement en urgence, elle informe l'intéressé dans la notification de sa décision du délai prévu par l'article R. 441-16-1 dans lequel une offre de logement adaptée à ses besoins et à ses capacités ou une proposition d'accueil doit lui être faite et porte à sa connaissance le délai, prévu à l'article R. 778-2 du code de justice administrative, dans lequel il pourra exercer le recours contentieux mentionné à l'article L. 441-2-3-1 du code de la construction et de l'habitation, le tribunal administratif compétent, ainsi que l'obligation de joindre à la requête la décision de la commission ;<br/>
<br/>
              3. Considérant qu'il résulte de l'ensemble de ces dispositions que si la juridiction saisie sur le fondement du I de l'article L. 441-2-3-1 du code de la construction et de l'habitation peut exiger du demandeur qu'il régularise sa demande en produisant la décision de la commission de médiation et, en l'absence de régularisation, opposer l'irrecevabilité prévue au second alinéa de l'article R. 778-2 du code de justice administrative, elle ne peut exiger à peine d'irrecevabilité la production du document de notification comportant les mentions prévues par le premier alinéa du même article ;  <br/>
<br/>
              4. Considérant qu'il résulte de ce qui précède qu'en opposant une irrecevabilité à la demande de M. A...au motif que, malgré la demande qui lui avait été régulièrement adressée par le tribunal, il n'avait pas produit le verso de la décision du 20 septembre 2013, qui comportait les informations mentionnées au premier alinéa de l'article R. 778-2 du code de justice administrative, le tribunal administratif a entaché son ordonnance d'une erreur de droit ;<br/>
<br/>
              5. Considérant qu'il résulte de tout ce qui précède que M. A... est fondé à demander l'annulation de l'ordonnance du tribunal administratif de Cergy-Pontoise du 26 octobre 2015 ;  <br/>
<br/>
<br/>
<br/>
<br/>
              D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : L'ordonnance du tribunal administratif de Cergy-Pontoise du 26 octobre 2015 est annulée.<br/>
<br/>
		Article 2 : L'affaire est renvoyée au tribunal administratif de Cergy-Pontoise.<br/>
Article 3 : La présente décision sera notifiée à  M. B...A...et à la ministre du logement et de l'habitat durable.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">38-07-01 LOGEMENT. - SAISINE DU JUGE DU DALO - CONDITIONS DE RECEVABILITÉ - PRODUCTION DE LA DÉCISION DE LA COMMISSION DE MÉDIATION - EXISTENCE - PRODUCTION DU DOCUMENT DE NOTIFICATION CONTENANT LES MENTIONS FAISANT COURIR LE DÉLAI DE RECOURS - ABSENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-01-08 PROCÉDURE. INTRODUCTION DE L'INSTANCE. FORMES DE LA REQUÊTE. - CONTENTIEUX DU DALO - OBLIGATION DE JOINDRE LA DÉCISION ADMINISTRATIVE - EXISTENCE - OBLIGATION DE JOINDRE LE DOCUMENT DE NOTIFICATION DE CETTE DÉCISION CONTENANT LES MENTIONS FAISANT COURIR LES DÉLAIS DE RECOURS - ABSENCE.
</SCT>
<ANA ID="9A"> 38-07-01 Si la juridiction saisie sur le fondement du I de l'article L. 441-2-3-1 du code de la construction et de l'habitation peut exiger du demandeur qu'il régularise sa demande en produisant la décision de la commission de médiation et, en l'absence de régularisation, opposer l'irrecevabilité prévue au second alinéa de l'article R.778-2 du code de justice administrative, elle ne peut exiger à peine d'irrecevabilité la production du document de notification comportant les mentions prévues par le premier alinéa du même article.</ANA>
<ANA ID="9B"> 54-01-08 Si la juridiction saisie sur le fondement du I de l'article L. 441-2-3-1 du code de la construction et de l'habitation peut exiger du demandeur qu'il régularise sa demande en produisant la décision de la commission de médiation et, en l'absence de régularisation, opposer l'irrecevabilité prévue au second alinéa de l'article R.778-2 du code de justice administrative, elle ne peut exiger à peine d'irrecevabilité la production du document de notification comportant les mentions prévues par le premier alinéa du même article.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
