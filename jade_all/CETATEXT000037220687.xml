<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037220687</ID>
<ANCIEN_ID>JG_L_2018_07_000000407407</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/22/06/CETATEXT000037220687.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère chambre, 18/07/2018, 407407, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-07-18</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>407407</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FOUSSARD, FROGER ; SCP THOUIN-PALAT, BOUCARD</AVOCATS>
<RAPPORTEUR>Mme Cécile Chaduteau-Monplaisir</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Charles Touboul</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:407407.20180718</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme A...B...a demandé au tribunal administratif de Strasbourg, d'une part, d'annuler la décision du 18 octobre 2011 par laquelle le directeur général de l'office public d'aménagement et de construction de la Moselle, dénommé Moselis, a refusé de prendre à sa charge le versement de l'allocation d'aide au retour à l'emploi et, d'autre part, de condamner Moselis à lui verser une somme de 14 594,25 euros correspondant aux allocations d'aide au retour à l'emploi qu'elle estime lui être dues, augmentée des intérêts légaux à compter de sa demande. Par un jugement n° 1202678 du 20 novembre 2014, le tribunal administratif de Strasbourg a rejeté cette demande. <br/>
<br/>
              Par un arrêt n° 15NC00176 du 26 janvier 2017, enregistré le 30 janvier 2017 au secrétariat du contentieux du Conseil d'Etat, la cour administrative d'appel de Nancy a transmis au Conseil d'Etat, en application de l'article R. 351-2 du code de justice administrative, le pourvoi, enregistré le 27 janvier 2015 au greffe de cette cour, présenté par MmeB.... <br/>
<br/>
              Par ce pourvoi, ainsi que par un nouveau mémoire, un mémoire rectificatif et un mémoire en réplique, enregistrés les 19 juin et 4 juillet 2017 et le 11 juin 2018 au secrétariat du contentieux du Conseil d'Etat, Mme B...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler le jugement du tribunal administratif de Strasbourg du 20 novembre 2014 ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code du travail ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Cécile Chaduteau-Monplaisir, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Charles Touboul, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Foussard, Froger, avocat de MmeB..., et à la SCP Thouin-Palat, Boucard, avocat de l'office public d'aménagement et de construction de la Moselle ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que Mme B...a travaillé en qualité d'agent public au sein de l'office public d'habitations à loyer modéré du département de la Moselle, devenu l'office public d'aménagement et de construction de la Moselle (OPAC Moselis), du 1er septembre 1986 au 4 septembre 2010, date d'effet de sa démission, avant d'être recrutée par la commune de Metz dans le cadre d'un contrat unique d'insertion du 7 mars au 6 septembre 2011. Par lettre du 29 septembre 2011, le directeur de l'agence Pôle emploi de Metz l'a, d'une part, admise en qualité de demandeur d'emploi et a, d'autre part, refusé de la prendre en charge au titre du régime de l'assurance chômage au motif que son ancien employeur, l'OPAC Moselis, relève du secteur public et assure lui-même l'indemnisation de ses anciens salariés. Par une décision du 18 octobre 2011, le directeur général de l'OPAC Moselis a rejeté sa demande de versement de l'allocation d'aide au retour à l'emploi. Mme B...se pourvoit en cassation contre le jugement du 20 novembre 2014 par lequel le tribunal administratif de Strasbourg a rejeté sa demande tendant à l'annulation de cette dernière décision.  <br/>
<br/>
              2. D'une part, aux termes de l'article L. 5422-1 du code du travail : " Ont droit à l'allocation d'assurance les travailleurs involontairement privés d'emploi (...), aptes au travail et recherchant un emploi qui satisfont à des conditions d'âge et d'activité antérieure ". Aux termes de l'article L. 5422-2 de ce code : " L'allocation d'assurance est accordée pour des durées limitées qui tiennent compte de l'âge des intéressés et de leurs conditions d'activité professionnelle antérieure. (...) ". <br/>
<br/>
              3. D'autre part, l'article L. 5424-1 de ce code dispose que : " Ont droit à une allocation d'assurance dans les conditions prévues aux articles L. 5422-2 et L. 5422-3 : / 1° Les agents fonctionnaires et non fonctionnaires de l'Etat et de ses établissements publics administratifs, les agents titulaires des collectivités territoriales ainsi que les agents statutaires des autres établissements publics administratifs ainsi que les militaires ; / 2° Les agents non titulaires des collectivités territoriales et les agents non statutaires des établissements publics administratifs autres que ceux de l'Etat et ceux mentionnés au 4° ainsi que les agents non statutaires des groupements d'intérêt public ; (...) ". L'article L. 5422-13 du code du travail précise que : " Sauf dans les cas prévus à l'article L. 5424-1, dans lesquels l'employeur assure lui-même la charge et la gestion de l'allocation d'assurance, tout employeur assure contre le risque de privation d'emploi tout salarié (...) ". Enfin, aux termes de l'article R. 5424-2 de ce code, dans sa rédaction alors applicable : " Lorsque, au cours de la période retenue pour l'application de l'article L. 5422-2, la durée totale d'emploi accomplie pour le compte d'un ou plusieurs employeurs affiliés au régime d'assurance a été plus longue que l'ensemble des périodes d'emploi accomplies pour le compte d'un ou plusieurs employeurs relevant de l'article L. 5424-1, la charge de l'indemnisation incombe à l'institution mentionnée à l'article L. 5312-1 pour le compte de l'organisme mentionné à l'article L. 5427-1. / Dans le cas contraire, cette charge incombe à l'employeur relevant de l'article L. 5424-1, ou à celui des employeurs relevant de cet article qui a employé l'intéressé durant la période la plus longue ".<br/>
<br/>
              3. Pour l'application de ces dispositions, lorsqu'au cours de la période de référence retenue pour apprécier la condition d'activité professionnelle antérieure à laquelle est subordonné le versement de l'allocation d'assurance, la durée totale d'emploi a été accomplie par l'intéressé pour le compte de plusieurs employeurs publics relevant de l'article L. 5424-1 du code du travail, ou que la durée totale d'emploi accomplie pour le compte de tels employeurs a été plus longue que celle accomplie pour le compte d'un ou plusieurs employeurs affiliés au régime d'assurance, la charge de l'indemnisation incombe à celui de ces employeurs publics qui a employé l'intéressé durant la période la plus longue.<br/>
<br/>
              4. Par suite, en se fondant sur la circonstance que les deux employeurs successifs de MmeB..., l'OPAC Moselis et la commune de Metz, relevaient des dispositions particulières de l'article L. 5424-1 de ce code et assuraient eux-mêmes la charge et la gestion de l'allocation d'assurance pour en déduire que la durée d'emploi de Mme B...par chacun d'eux était sans incidence sur la détermination de celui auquel incombait la charge du versement de l'allocation d'aide au retour à l'emploi et juger que cette charge incombait au dernier d'entre eux, le tribunal administratif a commis une erreur de droit. <br/>
<br/>
              5. Il résulte de ce qui précède que Mme B...est fondée à demander l'annulation du jugement qu'elle attaque. Le moyen retenu suffisant à entraîner cette annulation, il n'est pas nécessaire d'examiner l'autre moyen de son pourvoi. <br/>
<br/>
              6. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à la charge de MmeB..., qui n'est pas, dans la présente instance, la partie perdante. <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du tribunal administratif de Strasbourg du 20 novembre 2014 est annulé. <br/>
Article 2 : L'affaire est renvoyée au tribunal administratif de Strasbourg. <br/>
Article 3 : Les conclusions de l'OPAC Moselis présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
Article 4 : La présente décision sera notifiée à Mme A...B...et à l'office public d'aménagement et de construction de la Moselle (OPAC Moselis).<br/>
Copie en sera adressée à la commune de Metz. <br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
