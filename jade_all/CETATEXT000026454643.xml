<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026454643</ID>
<ANCIEN_ID>JG_L_2012_10_000000346891</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/45/46/CETATEXT000026454643.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  2ème sous-section jugeant seule, 03/10/2012, 346891, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2012-10-03</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>346891</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 2ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Jacques-Henri Stahl</PRESIDENT>
<AVOCATS>BROUCHOT ; HAAS</AVOCATS>
<RAPPORTEUR>M. Yves Doutriaux</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Damien Botteghi</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2012:346891.20121003</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 21 février 2011 et 23 mai 2011 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. Christian B, demeurant au ... ; M. B demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler le jugement n° 0903755-7 du 21 décembre 2010 par lequel le tribunal administratif de Montpellier a rejeté sa demande tendant, d'une part, à la condamnation de La Poste à lui verser la somme de 6 947 euros en réparation du préjudice subi du fait de la réduction de son traitement durant une période de six mois depuis août 2007 jusqu'à son départ à la retraite ;<br/>
<br/>
              2°) réglant l'affaire au fond, de condamner La Poste à lui verser les sommes de 4 497,10 euros au titre du préjudice financier, 449,71 euros au titre de l'indemnité compensatrice de congés payés et 2 000 euros au titre du préjudice moral ; <br/>
<br/>
              3°) de mettre à la charge de La Poste le versement de la somme de 2 800 euros en application des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la loi n° 83-634 du 13 juillet 1983 ;<br/>
<br/>
              Vu la loi n° 84-16 du 11 janvier 1984 ;<br/>
<br/>
              Vu la loi n° 90-568 du 2 juillet 1990 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Yves Doutriaux, Conseiller d'Etat,<br/>
<br/>
              - les observations de Me Brouchot, avocat de M. B, et de Me Haas, avocat de La Poste,<br/>
<br/>
              - les conclusions de M. Damien Botteghi, Rapporteur public,<br/>
<br/>
              La parole ayant à nouveau été donnée à Me Brouchot, avocat de M. B, et de Me Haas, avocat de La Poste ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que M. B, cadre professionnel de La Poste, a demandé le 19 décembre 2006 à bénéficier, à compter du 1er janvier 2007, d'une cessation progressive d'activité, dont l'option est irrévocable, dans le cadre du dispositif " nouveau temps partiel d'accompagnement et de conseil " prévoyant un service de 70 %, jusqu'au 31 décembre 2007, date de sa mise à la retraite, assorti d'une rémunération réduite à 70 % de son ancienne rémunération, après avoir reçu une lettre du " conseiller mobilité " de La Poste, datée du 14 décembre 2006, lui confirmant " la possibilité d'utiliser son compte épargne temps pour obtenir un complément de salaire durant toute la durée de sa mesure d'âge " ; que, le 10 avril 2007, La Poste lui a confirmé par écrit que " d'après l'outil de simulation dont nous disposons vous serez indemnisé à hauteur de 100 % pendant toute la période du temps partiel " ; que cependant, la Poste lui a ultérieurement fait part, par une lettre du 12 juin 2007, qu'à compter du 1er août 2007 il ne percevrait plus que 70 % de sa rémunération ; que M. B a demandé à La Poste, par une lettre du 11 juillet 2007, le maintien de son traitement à hauteur de 100 % pour la période d'août à décembre 2007 ; que cette demande a été rejetée par une lettre du 31 juillet 2007 ; qu'en l'absence de réponse au recours administratif qu'il a formé le 19 mai 2009, M. B a saisi le tribunal administratif de Montpellier d'une demande tendant à l'indemnisation par La Poste du préjudice subi à hauteur de 4 497 euros au titre du traitement non perçu, de 449 euros au titre de l'indemnité compensatrice de congés payés et de 2 000 euros au titre du préjudice moral ;<br/>
<br/>
              2. Considérant que le tribunal administratif de Montpellier s'est fondé sur l'absence de préjudice matériel démontré pour rejeter la demande de M. B ; qu'en statuant ainsi, alors que M. B avait produit à l'appui de sa demande des éléments justifiant du préjudice matériel résultant de sa perte de rémunération à compter du mois d'août 2007, le tribunal administratif de Montpellier a dénaturé les pièces du dossier ; que, par suite et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, son jugement doit être annulé ; <br/>
<br/>
              3. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              4. Considérant qu'il résulte de l'instruction qu'après avoir indiqué à M. B par une lettre datée du 14 décembre 2006 qu'il aurait " la possibilité d'utiliser son compte épargne temps pour obtenir un complément de salaire durant toute la durée de sa mesure d'âge ", ce qui a conduit l'intéressé à opter de manière irrévocable pour une cessation progressive d'activité à compter du 1er janvier 2007, et lui avoir au demeurant confirmé par une lettre du 10 avril 2007 que " d'après l'outil de simulation dont nous disposons vous serez indemnisé à hauteur de 100 % pendant toute la durée du temps partiel ", La Poste a décidé, en informant M. B par une lettre du 12 juin 2007, qu'à compter du 1er août 2007, il ne percevrait plus que 70 % de sa rémunération ; qu'en donnant ainsi à M. B des assurances qui l'ont conduit à faire un choix irrévocable sans honorer les engagements qu'elle avait ainsi pris à compter du mois d'août 2007, La Poste a commis une faute de nature à engager sa responsabilité ; qu'il résulte de l'instruction que cette faute est la cause directe de la perte de rémunération subie par M. B entre août et décembre 2007 ; qu'il sera fait une juste appréciation du préjudice matériel subi par M. B en condamnant la Poste à lui verser une indemnité de 4 497 euros au titre du traitement non perçu et de 449 euros au titre de l'indemnité compensatrice de congés payés ; qu'il ne résulte en revanche pas de l'instruction que M. B aurait, dans les circonstances de l'espèce, subi un préjudice moral justifiant le versement d'une indemnité ; <br/>
<br/>
              5. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de La Poste la somme de 2 800 euros à verser à M. B, au titre des dispositions de l'article L. 761-1 du code de justice administrative ; que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de M. B qui n'est pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : Le jugement du tribunal administratif de Montpellier en date du 21 décembre 2010 est annulé.<br/>
<br/>
Article 2 : La Poste est condamnée à verser à M. B la somme de 4 946 euros.<br/>
<br/>
Article 3 : La Poste versera à M. B la somme de 2 800 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : Le surplus des conclusions de M. B est rejeté. <br/>
<br/>
Article 5 : La présente décision sera notifiée à M. Christian B et à La Poste.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
