<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038077324</ID>
<ANCIEN_ID>JG_L_2019_01_000000412251</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/07/73/CETATEXT000038077324.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème chambre, 30/01/2019, 412251, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-01-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>412251</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GADIOU, CHEVALLIER</AVOCATS>
<RAPPORTEUR>Mme Ophélie Champeaux</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emilie Bokdam-Tognetti</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2019:412251.20190130</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société par actions simplifiée (SAS) Nasa Automobiles a demandé au tribunal administratif de Nancy de prononcer la décharge des cotisations de taxe sur les surfaces commerciales qu'elle a acquittées au titre des années 2010, 2011 et 2012 à raison de ses deux établissements situés à Essey-lès-Nancy et Laxou (Meurthe-et-Moselle). Par un jugement nos 1400799, 1400979 du 8 octobre 2015, le tribunal a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 15NC02394 du 7 juillet 2017, enregistré au secrétariat du contentieux du Conseil d'Etat le 7 juillet 2017, la cour administrative d'appel de Nancy a, d'une part, transmis au Conseil d'Etat, en application de l'article R. 351-2 du code de justice administrative, le pourvoi présenté par la société Nasa Automobiles contre ce jugement en tant qu'il concerne les années 2011 et 2012 et, d'autre part, rejeté l'appel formé par la société contre ce jugement en tant qu'il concerne l'année 2010.<br/>
<br/>
              Par ce pourvoi, enregistré le 7 décembre 2015 au greffe de la cour administrative d'appel de Nancy, la société Nasa Automobiles demande :<br/>
<br/>
<br/>
              1°) d'annuler le jugement du tribunal administratif de Nancy en tant qu'il se prononce sur les impositions dues au titres des années 2011 et 2012 ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - la loi n° 72-657 du 13 juillet 1972 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Ophélie Champeaux, maître des requêtes en service extraordinaire,<br/>
<br/>
              - les conclusions de Mme Emilie Bokdam-Tognetti, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Gadiou, Chevallier, avocat de la société Nasa Automobiles.<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que la société Nasa Automobiles a sollicité la décharge des cotisations de taxe sur les surfaces commerciales acquittées au titre des années 2010, 2011 et 2012 à raison de ses deux établissements situés à Essey-lès-Nancy et Laxou (Meurthe-et-Moselle) au motif qu'elle disposait, pour l'exercice de son activité de vente de véhicules automobiles, d'une surface totale de vente n'excédant pas 400 m². Elle se pourvoit en cassation contre le jugement du 8 octobre 2015 par lequel le tribunal administratif de Nancy a rejeté sa demande en tant qu'elle tendait à la décharge des impositions dues au titre des années 2011 et 2012.<br/>
<br/>
              2. Aux termes de l'article 3 de la loi du 13 juillet 1972 instituant des mesures en faveur de certaines catégories de commerçants et artisans âgés, dans sa rédaction applicable aux années d'imposition en litige : " Il est institué une taxe sur les surfaces commerciales assise sur la surface de vente des magasins de commerce de détail, dès lors qu'elle dépasse 400 mètres carrés des établissements ouverts à partir du 1er janvier 1960 quelle que soit la forme juridique de l'entreprise qui les exploite. (...) / La surface de vente des magasins de commerce de détail, prise en compte pour le calcul de la taxe, et celle visée à l'article L. 720-5 du code de commerce, s'entendent des espaces affectés à la circulation de la clientèle pour effectuer ses achats, de ceux affectés à l'exposition des marchandises proposées à la vente, à leur paiement, et de ceux affectés à la circulation du personnel pour présenter les marchandises à la vente (...) ".<br/>
<br/>
              3. En premier lieu, les vices qui entachent soit la procédure d'instruction, par l'administration, de la réclamation préalable d'un contribuable, soit la décision par laquelle cette réclamation est rejetée, sont sans influence sur la régularité et le bien-fondé des impositions contestées. Il en résulte que le tribunal administratif n'a pas commis d'erreur de droit en jugeant que le moyen invoqué devant lui par la société Nasa Automobiles et tiré de la méconnaissance du principe du contradictoire lors de l'instruction de sa réclamation était inopérant.<br/>
<br/>
              4. En second lieu, le tribunal a relevé, par une appréciation souveraine non arguée de dénaturation, qu'il résultait de l'instruction que, bien que ne participant pas directement à la promotion commerciale de la société, l'aire de livraison des véhicules vendus ne constituait pas un espace fermé à la clientèle et n'était séparée de l'espace de vente que par des bornes amovibles, que le point d'accueil-vente chargé d'accueillir la clientèle n'était pas distinct de la surface d'exposition des marchandises et que, compte tenu de la configuration des lieux, les trois bureaux fermés devaient être regardés comme accessibles à la clientèle notamment pour finaliser le processus d'achat des véhicules. Il n'a entaché son jugement ni de contradiction de motifs ni d'erreur de droit en en déduisant que ces zones constituaient des surfaces affectées à la circulation de la clientèle pour effectuer ses achats et que par suite, elles entraient dans l'assiette de la taxe sur les surfaces commerciales et qu'eu égard aux superficies non contestées, la surface de vente excédait le seuil de 400 m².<br/>
<br/>
              5. Il résulte de ce qui précède, et sans qu'il soit besoin de statuer sur la fin de non-recevoir opposée par le ministre en défense, que le pourvoi de la société Nasa Automobiles ne peut qu'être rejeté, y compris ses conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de la société Nasa Automobiles est rejeté.<br/>
<br/>
Article 2 : La présente décision sera notifiée à la société par actions simplifiée Nasa Automobiles et au ministre de l'action et des comptes publics.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
