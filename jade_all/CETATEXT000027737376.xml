<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027737376</ID>
<ANCIEN_ID>JG_L_2013_07_000000362977</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/73/73/CETATEXT000027737376.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème et 10ème sous-sections réunies, 11/07/2013, 362977, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-07-11</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>362977</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème et 10ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP TIFFREAU, CORLAY, MARLANGE</AVOCATS>
<RAPPORTEUR>Mme Séverine Larere</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Aladjidi</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:362977.20130711</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu 1°, sous le n° 362977, le pourvoi, enregistré le 24 septembre 2012 au secrétariat du contentieux du Conseil d'Etat, présenté pour la société Castel Invest, dont le siège est chez M. E..., rue Pierre Chalon, L'houezel à Gosier (97190) ; la société Castel Invest demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'ordonnance n° 1200698 du 7 septembre 2012 par laquelle le vice-président, juge des référés du tribunal administratif de Basse-Terre, statuant sur le fondement de l'article L. 521-1 du code de justice administrative a, à la demande de M. C...F...et de Mme A...D..., suspendu l'exécution de l'arrêté du 11 octobre 2011 par lequel le maire du Lamentin lui a délivré un permis de construire ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter la demande présentée par M. F...et Mme D... ; <br/>
<br/>
              3°) de mettre à la charge, solidairement, de M. F...et Mme D...la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu 2°, sous le n° 363171, la requête, enregistrée le 2 octobre 2012 au secrétariat du contentieux du Conseil d'Etat, présentée pour la société Castel Invest ; la société Castel Invest demande au Conseil d'Etat d'ordonner le sursis à exécution de l'ordonnance n° 1200698 du 7 septembre 2012 visée ci-dessus ;<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu le code de l'urbanisme ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Séverine Larere, Maître des Requêtes,  <br/>
<br/>
              - les conclusions de M. Frédéric Aladjidi, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Tiffreau, Corlay, Marlange, avocat de la société Castel Invest ;<br/>
<br/>
<br/>
<br/>
              1. Considérant que le pourvoi et la requête de la société Castel Invest visés ci-dessus tendent à l'annulation et au sursis à exécution d'une même ordonnance ; qu'il y a lieu de les joindre pour statuer par une seule décision ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision.(...) " ;<br/>
<br/>
              3. Considérant qu'il ressort des pièces du dossier soumis au juge des référés que, par un arrêté du 11 octobre 2011, le maire de la commune du Lamentin a accordé à la société Castel Invest un permis de construire en vue d'édifier un ensemble de 9 bâtiments comprenant 78 logements locatifs ; que, par l'ordonnance attaquée du 7 septembre 2012, le vice-président, juge des référés du tribunal administratif de Basse-Terre a, à la demande de M. C...F...et de Mme A...D..., riverains de la construction projetée, suspendu, en application des dispositions précitées de l'article L. 521-1 du code de justice administrative, l'exécution de cet arrêté ; qu'après avoir écarté le moyen tiré de la tardiveté de la demande de M. F...et de Mme D...tendant à l'annulation de cet arrêté, il a jugé que la condition d'urgence prévue à l'article L. 521-1 du code de justice administrative était remplie et que plusieurs des moyens invoqués par les demandeurs à l'appui de leurs conclusions étaient propres, en l'état de l'instruction, à créer un doute sérieux sur la légalité de l'arrêté litigieux ;<br/>
<br/>
              Sur la recevabilité de la demande tendant à l'annulation de l'arrêté contesté :<br/>
<br/>
              4. Considérant qu'aux termes de l'article R. 600-2 du code de l'urbanisme : " Le délai de recours contentieux à l'encontre d'une décision de non-opposition à une déclaration préalable ou d'un permis de construire, d'aménager ou de démolir court à l'égard des tiers à compter du premier jour d'une période continue de deux mois d'affichage sur le terrain des pièces mentionnées à l'article R. 424-15 " ; que, selon l'article R. 424-15 du même code : " Mention du permis explicite ou tacite ou de la déclaration préalable doit être affichée sur le terrain, de manière visible de l'extérieur, par les soins de son bénéficiaire, dès la notification de l'arrêté (...) " ; que l'article A. 424-18 précise que : " Le panneau d'affichage doit être installé de telle sorte que les renseignements qu'il contient demeurent.lisibles de la voie publique ou des espaces ouverts au public pendant toute la durée du chantier " ; <br/>
<br/>
              5. Considérant que le juge des référés a jugé qu'il ressortait des pièces du dossier qui lui était soumis, notamment des photographies, du plan et des témoignages de riverains produits par les demandeurs, d'une part, que les panneaux d'affichage du permis de construire avaient été placés, du 20 octobre au 20 décembre 2011, non sur le terrain d'assiette du projet de construction mais à une distance de près de deux cents mètres, sur une voie privée non ouverte à la circulation publique et, d'autre part, qu'ils n'étaient lisibles ni d'une voie publique ni d'un espace ouvert au public ; qu'il en a déduit que cet affichage était irrégulier au regard des dispositions des articles R. 424-15 et A. 424-18 du code de l'urbanisme et qu'en conséquence la requête aux fins d'annulation présentée par M. F...et Mme D...n'était pas tardive ;<br/>
<br/>
              6. Considérant, en premier lieu, qu'en jugeant que l'affichage du permis de construire litigieux avait été réalisé non sur le terrain d'implantation du projet mais à une distance de près de deux cents mètres de ce terrain, le juge des référés, qui a suffisamment motivé son ordonnance sur ce point, a porté sur les faits qui lui étaient soumis une appréciation souveraine exempte de dénaturation ; <br/>
<br/>
              7. Considérant, en deuxième lieu, que l'ordonnance attaquée relève que les panneaux d'affichage du permis de construire étaient placés non pas au bord de la route mais au fond d'une impasse bordée de deux maisons privées sur une voie privée non ouverte à la circulation publique et utilisée par un riverain ; qu'en déduisant de ces circonstances que les panneaux n'étaient pas lisibles d'une voie publique ou d'un espace ouvert au public au sens de l'article A. 424-18 précité du code de l'urbanisme, le juge des référés, qui, contrairement à ce que soutient le pourvoi, ne s'est pas fondé sur le seul caractère privé de la " voie " sur laquelle les panneaux étaient implantés, s'est livré à une appréciation souveraine des faits qui lui étaient soumis  et n'a commis aucune erreur de droit ;<br/>
<br/>
              8. Considérant, enfin, que c'est également par une appréciation souveraine insusceptible d'être discutée en cassation que le juge des référés a estimé qu'il existait une contradiction entre les procès-verbaux, établis par le même cabinet d'huissier, produits par la société Castel Invest, d'une part, et par les demandeurs, d'autre part ; qu'il n'a commis aucune erreur de droit et a suffisamment motivé son ordonnance en se fondant sur des attestations de riverains du projet, dont il n'était ni établi ni même allégué devant lui  qu'ils auraient eu des liens avec les demandeurs ;<br/>
<br/>
              Sur l'existence d'un moyen propre à créer un doute sérieux sur la légalité de l'arrêté attaqué :<br/>
<br/>
              9. Considérant que l'ordonnance attaquée énonce que sont propres, en l'état de l'instruction, à créer un doute sérieux sur la légalité de l'arrêté attaqué les moyens tirés de la méconnaissance de dispositions du règlement du plan d'occupation des sols, de l'article R. 111-2 du code de l'urbanisme et des dispositions du plan de prévention des risques naturels de la commune du Lamentin et des articles R. 111-5 et R. 111-21 du code de l'urbanisme ;<br/>
<br/>
              10. Considérant, d'une part, que le juge des référés ne s'est pas borné, contrairement à ce que soutient le pourvoi, à énumérer les dispositions du code de l'urbanisme dont il estimait qu'elles étaient potentiellement méconnues par l'arrêté contesté mais a précisé en quoi ces dispositions lui paraissaient susceptibles d'avoir été méconnues ; que le moyen tiré de ce que l'ordonnance serait insuffisamment motivée doit, par suite, être écarté ;<br/>
<br/>
              11. Considérant, d'autre part, que le moyen tiré de ce que le juge des référés aurait commis une erreur de droit dans la désignation des moyens qu'il a regardés comme étant propres à créer, en l'état de l'instruction, un doute sérieux sur la légalité de l'arrêté attaqué n'est assorti d'aucune précision permettant d'en apprécier le bien-fondé ; qu'il ne peut, par suite, qu'être écarté ;<br/>
<br/>
              12. Considérant qu'il résulte de tout ce qui précède que la société Castel Invest n'est pas fondée à demander l'annulation de l'ordonnance qu'elle attaque ; que son pourvoi ne peut, par suite, qu'être rejeté ;<br/>
<br/>
              13. Considérant que, dès lors qu'il est statué, par la présente décision, sur le pourvoi n° 362977 de la société Castel Invest tendant à l'annulation de l'ordonnance du juge des référés du tribunal administratif de Basse-Terre du 7 septembre 2012, les conclusions de sa requête n° 363171 tendant à ce qu'il soit sursis à l'exécution de cette ordonnance sont dépourvues d'objet ; qu'il n'y a pas lieu, par suite, d'y statuer ;<br/>
<br/>
              14. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'il soit fait droit aux conclusions présentées à ce titre par la société Castel Invest ; qu'il y a lieu, en revanche, dans les circonstances de l'espèce, de mettre une somme de 2 000 euros à sa charge, à verser à M. F...et Mme D...;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : Le pourvoi n° 362977 de la société Castel Invest est rejeté. <br/>
Article 2 : Il n'y a pas lieu de statuer sur la requête n° 363171 de la société Castel Invest.<br/>
Article 3 : La société Castel Invest versera à M. F...et Mme D...une somme globale de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à la société Castel Invest, à la commune du Lamentin et à M. B... F...et Mme A... G...D.lisibles de la voie publique ou des espaces ouverts au public pendant toute la durée du chantier<br/>
Copie en sera adressée à la ministre de l'écologie, du développement durable et de l'énergie et à la ministre de l'égalité des territoires et du logement.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
