<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038546404</ID>
<ANCIEN_ID>JG_L_2019_06_000000424377</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/54/64/CETATEXT000038546404.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème - 1ère chambres réunies, 03/06/2019, 424377</TITRE>
<DATE_DEC>2019-06-03</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>424377</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème - 1ère chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GASCHIGNARD</AVOCATS>
<RAPPORTEUR>Mme Sara-Lou Gerber</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Dieu</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:424377.20190603</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Mme A...B...a demandé au tribunal administratif de Nîmes d'annuler pour excès de pouvoir la décision du 23 août 2017 par laquelle le maire de la commune de Buoux (Vaucluse) a refusé de lui délivrer l'attestation prévue à l'article R. 1234-9 du code du travail et de lui verser une indemnité compensatrice des jours de congés qu'elle estime lui être dus au titre de l'année 2017. Par un jugement n° 1702852 du 12 juillet 2018, le tribunal administratif a rejeté sa demande. <br/>
<br/>
              Par une ordonnance n° 18MA04233 du 18 septembre 2018, enregistrée le 20 septembre 2018 au secrétariat du contentieux du Conseil d'Etat, la présidente de la cour administrative d'appel de Marseille a transmis au Conseil d'Etat, en application de l'article R. 351-2 du code de justice administrative, l'appel formé par Mme B...contre ce jugement en tant qu'il rejette sa demande d'annulation de la décision attaquée en tant qu'elle lui refuse la délivrance de l'attestation prévue à l'article R. 1234-9 du code du travail. <br/>
<br/>
              Par cette requête et par un nouveau mémoire, enregistré le 4 mars 2019 au secrétariat du contentieux du Conseil d'Etat, Mme B...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement en tant qu'il rejette sa demande d'annulation de la décision du 23 août 2017 en tant qu'elle refuse la délivrance de l'attestation prévue à l'article R. 1234-9 du code du travail. ; <br/>
<br/>
              2°) de faire droit à sa demande et d'enjoindre à la commune de Buoux de lui délivrer l'attestation litigieuse ;<br/>
<br/>
              3°) de mettre à la charge de commune de Buoux la somme de 3 000 euros à verser  à  la  SCP  Gaschignard, avocat de MmeB..., au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991. <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la Constitution, notamment son article 61-1 ;<br/>
              - l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
              - la loi n° 83-634 du 13 juillet 1983 ; <br/>
              - la loi n° 84-53 du 26 janvier 1984 ; <br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - le code de l'action sociale et des familles ;<br/>
              - le code du travail ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Sara-Lou Gerber, auditeur,  <br/>
<br/>
              - les conclusions de M. Frédéric Dieu, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Gaschignard, avocat de Mme B...;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que le maire de la commune de Buoux a, le 25 juillet 2017, infligé à Mme B...la sanction d'exclusion temporaire des fonctions pour deux ans, sur le fondement de l'article 89 de la loi du 26 janvier 1984 portant dispositions statutaires relatives à la fonction publique territoriale. Par une seconde décision du 23 août 2017, le maire a refusé de faire droit aux demandes de Mme B...tendant, d'une part, au versement de l'indemnité compensatrice des jours de congés qu'elle estimait lui être due au titre de l'année 2017 et, d'autre part, à la délivrance de l'attestation qu'elle demandait en vue de solliciter, sur le fondement de l'article L. 5422-1 du code du travail, le bénéfice d'un revenu de remplacement. Par un jugement du 12 juillet 2018, le tribunal administratif de Nîmes a rejeté la demande d'annulation de cette seconde décision présentée par MmeB....<br/>
<br/>
              2. Par une requête, transmise au Conseil d'Etat par une ordonnance du 18 septembre 2018 de la présidente de la cour administrative d'appel de Marseille, prise sur le fondement de l'article R. 351-2 du code de justice administrative, Mme B...demande l'annulation de ce jugement en tant qu'il rejette sa demande d'annulation de la décision du 23 août 2017, en tant que celle-ci lui refuse la délivrance de l'attestation lui permettant de solliciter un revenu de remplacement.<br/>
<br/>
              3. A l'appui de cette requête transmise au Conseil d'Etat, Mme B...présente, devant le Conseil d'Etat, une question prioritaire de constitutionnalité dirigée contre l'article 89 de la loi du 26 janvier 1984, en ce que ses dispositions feraient obstacle à l'attribution du revenu de remplacement prévu par l'article L. 5422-1 du code du travail, aux termes duquel : " Ont droit à l'allocation d'assurance les travailleurs involontairement privés d'emploi (...) , aptes au travail et recherchant un emploi qui satisfont à des conditions d'âge et d'activité antérieure ".<br/>
<br/>
              Sur la question prioritaire de constitutionnalité :<br/>
<br/>
              4. Aux termes du premier alinéa de l'article 23-5 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel : " Le moyen tiré de ce qu'une disposition législative porte atteinte aux droits et libertés garantis par la Constitution peut être soulevé (...) à l'occasion d'une instance devant le Conseil d'Etat (...) ". Il résulte des dispositions de ce même article que le Conseil constitutionnel est saisi de la question prioritaire de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux. <br/>
<br/>
              5. L'article 89 de la loi du 26 janvier 1984 portant dispositions statutaires relatives à la fonction publique territoriale dispose que : " Les sanctions disciplinaires sont réparties en quatre groupes : / (...) Troisième groupe : / la rétrogradation ; l'exclusion temporaire de fonctions pour une durée de seize jours à deux ans ; (...) / L'exclusion temporaire de fonctions, qui est privative de toute rémunération, peut être assortie d'un sursis total ou partiel (...) ". Si une sanction d'exclusion temporaire prononcée, sur le fondement de ces dispositions, à l'encontre d'un agent de la fonction publique territoriale entraîne pour celui-ci la cessation provisoire de ses fonctions et la privation, pour la même durée, de la rémunération qui leur est attachée, elle n'a pas pour effet de le priver de son emploi, l'agent conservant son emploi pendant la période d'exclusion et étant réintégré dans ses fonctions au terme de cette période. Madame B...n'est, par suite, pas fondée à soutenir que les dispositions contestées méconnaissent les dispositions du cinquième alinéa du Préambule de la Constitution du 27 octobre 1946 aux termes desquelles : " Chacun a le devoir de travailler et le droit d'occuper un emploi (...) ". Est à cet égard sans incidence la circonstance que, ainsi qu'elle l'allègue, l'application de ces dispositions aurait pour effet de la priver du bénéfice des dispositions de l'article L. 5422-1 du code du travail citées ci-dessus.<br/>
<br/>
              6. Par ailleurs, alors même que l'agent exclu temporairement ne pourrait, ainsi qu'elle l'allègue, prétendre, pendant la période où court cette sanction, à l'allocation d'assurance prévue par l'article L. 5422-1 du code du travail, cette circonstance n'a pas, par elle-même, pour effet d'empêcher l'agent de percevoir un revenu pendant cette même période. L'exécution de la sanction ne fait, ainsi, notamment obstacle ni à ce que l'agent public exerce, tout en conservant son emploi public, un autre emploi, sous réserve du respect des obligations déontologiques qui s'imposent à lui, ni à ce qu'il sollicite, s'il s'y croit fondé, le bénéfice du revenu de solidarité active prévu par l'article L. 262-2 du code de l'action sociale et des familles. Mme B...n'est, par suite, pas fondée à soutenir que les dispositions contestées, telles qu'interprétées par la jurisprudence du Conseil d'Etat, méconnaissent le droit à des moyens convenables d'existence qui résulte des dixième et onzième alinéas du Préambule de la Constitution du 27 octobre 1946. <br/>
<br/>
              7. Il résulte de tout ce qui précède que la question soulevée, qui n'est pas nouvelle, ne présente pas un caractère sérieux. Par suite, il n'y a pas lieu de la renvoyer au Conseil constitutionnel. <br/>
<br/>
              Sur le pourvoi :<br/>
<br/>
              8. L'article R. 811-1 du code de justice administrative dispose que : " (...) le tribunal administratif statue en premier et dernier ressort : / 1° Sur les litiges relatifs aux prestations, allocations ou droits attribués au titre de l'aide ou de l'action sociale, du logement ou en faveur des travailleurs privés d'emploi, mentionnés à l'article R. 772-5, y compris le contentieux du droit au logement défini à l'article R. 778-1 (...) ".<br/>
<br/>
              9. Les articles R. 772-5 et suivants du code de justice administrative définissent des règles particulières à la présentation, à l'instruction et au jugement des requêtes relatives aux prestations, allocations ou droits attribués au titre de l'aide ou de l'action sociale, du logement ou en faveur des travailleurs privés d'emploi, destinées notamment à faciliter la saisine du juge administratif par le requérant, à permettre la poursuite à l'audience de la procédure contradictoire sur certains éléments et à favoriser un règlement rapide du litige. Relèvent ainsi de ces contentieux les litiges, y compris indemnitaires, portant sur l'attribution ou le versement d'une prestation ou d'une allocation ou la reconnaissance d'un droit au profit de la personne sollicitant le bénéfice de l'aide, de l'action sociale ou d'une aide en matière de logement ou encore du travailleur privé d'emploi, de même que sur les indus qui peuvent en résulter et les sanctions éventuellement prononcées à l'encontre du bénéficiaire.<br/>
<br/>
              10. La demande dont Mme B...a saisi le tribunal administratif de Nîmes ne porte pas sur ses droits à l'assurance chômage mais met seulement en cause ses relations avec la collectivité publique qui l'emploie, à laquelle elle reproche de ne pas s'être acquittée des obligations pesant sur l'employeur en application de l'article R. 1234-9 du code du travail, aux termes duquel : " L'employeur délivre au salarié, au moment de l'expiration ou de la rupture du contrat de travail, les attestations et justifications qui lui permettent d'exercer ses droits aux prestations mentionnées à l'article L. 5421-2 et transmet sans délai des mêmes attestations à Pôle emploi ". Par suite, cette demande ne peut être regardée comme relevant des litiges mentionnés à l'article R. 772-5 du code de justice administrative, sur lesquels le tribunal administratif statue en premier et dernier ressort en vertu de l'article R. 811-1 du même code.<br/>
<br/>
              11. Dès lors, la requête de Mme B...tendant à l'annulation du jugement du tribunal administratif de Nîmes du 12 juillet 2018 en tant qu'il rejette sa demande d'annulation du refus de lui délivrer l'attestation prévue par l'article R. 1234-9 du code du travail présente le caractère d'un appel. Il y a donc lieu d'en attribuer le jugement à la cour administrative d'appel de Marseille.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée par MmeB.... <br/>
<br/>
Article 2 : Le jugement de la requête de Mme B...est renvoyé à la cour administrative d'appel de Marseille.<br/>
<br/>
Article 3 : La présente décision sera notifiée à Mme A...B..., à la commune de Buoux, au ministre de l'intérieur et à la présidente de la cour administrative d'appel de Marseille.<br/>
Copie en sera adressée au Conseil constitutionnel, au Premier ministre et au ministre de l'action et des comptes publics.  <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-10 PROCÉDURE. - QPC PRÉSENTÉE AU SOUTIEN D'UNE REQUÊTE RESSORTISSANT À LA COMPÉTENCE D'UNE AUTRE JURIDICTION ADMINISTRATIVE - FACULTÉ, POUR LE CONSEIL D'ETAT, DE STATUER SUR LA TRANSMISSION DE LA QPC AVANT DE RENVOYER L'AFFAIRE À LA JURIDICTION COMPÉTENTE (SOL. IMPL.) [RJ1].
</SCT>
<ANA ID="9A"> 54-10 Le Conseil d'Etat, saisi d'une requête ressortissant à la compétence d'une autre juridiction administrative au soutien de laquelle est soulevée une question prioritaire de constitutionnalité (QPC), peut statuer sur la transmission de cette QPC au Conseil constitutionnel avant de renvoyer l'affaire à la juridiction compétente (sol. impl.).</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., sur l'absence d'obligation de statuer sur la recevabilité de la requête avant de se prononcer sur le renvoi de la QPC, CE, 21 novembre 2014, Société Mutuelle des Transports Assurances, n° 384353, T. p. 836. Comp., s'agissant d'une demande pour laquelle la juridiction administrative dans son ensemble est incompétente, CE, 13 juin 2018, Conseil national de l'ordre des infirmiers et autres, n°s 408325 409019 409045 409058, T. pp. 505-603-872.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
