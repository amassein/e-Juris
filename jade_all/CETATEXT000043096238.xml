<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043096238</ID>
<ANCIEN_ID>JG_L_2021_02_000000437797</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/09/62/CETATEXT000043096238.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème chambre, 03/02/2021, 437797, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-02-03</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>437797</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP KRIVINE, VIAUD</AVOCATS>
<RAPPORTEUR>M. Yohann Bouquerel</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Marc Pichon de Vendeuil</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:437797.20210203</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A... B... a demandé au tribunal administratif de Limoges d'annuler pour excès de pouvoir l'arrêté du 30 novembre 2018 par lequel le préfet de l'Indre a refusé de lui délivrer un titre de séjour, lui a fait obligation de quitter le territoire français sans délai à sa sortie du centre pénitentiaire de Châteauroux prévue le 26 mai 2019, a fixé le pays de renvoi et lui a interdit le retour sur le territoire français pour une durée de trois ans. Par une ordonnance n° 1801926 du 17 décembre 2018, le président du tribunal administratif de Limoges a rejeté sa demande pour tardiveté.<br/>
<br/>
              Par un arrêt n° 19BX00211 du 8 octobre 2019, la cour administrative d'appel de Bordeaux a rejeté l'appel formé par M. B... contre cette ordonnance.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 20 janvier et 21 mai 2020 au secrétariat du contentieux du Conseil d'État, M. B... demande au Conseil d'État :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 2 500 euros à verser à la SCP Krivine et Viaud, son avocat, au titre des dispositions des article L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991.<br/>
<br/>
<br/>
              	M. B... soutient que l'arrêt qu'il attaque est entaché :<br/>
              - d'une insuffisance de motivation en ce qu'il omet de préciser, pour juger que la notification de l'obligation de quitter le territoire français n'était pas ambigüe, qu'elle mentionnait bien que la présentation d'un recours administratif suspend le délai de recours contentieux ; <br/>
              - d'une insuffisance de motivation en ce que la cour s'est abstenue de vérifier que la notification de l'obligation de quitter le territoire français mentionnait la possibilité de former son recours contentieux entre les mains du chef de l'établissement pénitentiaire ;<br/>
              - d'une erreur de droit en ce qu'il juge que la notification de l'obligation de quitter le territoire français n'était pas ambigüe, alors qu'elle ne mentionnait pas que la présentation d'un recours administratif ne suspend pas le délai de recours contentieux ;<br/>
              - d'une erreur de droit en ce qu'il juge que le délai de recours de quarante-huit a couru, alors que la notification de l'obligation de quitter le territoire français ne mentionnait pas la possibilité de former le recours contentieux entre les mains du chef de l'établissement pénitentiaire ;<br/>
              - d'une erreur de droit en ce qu'en méconnaissance du droit à un recours effectif, il juge son recours tardif alors que le délai de recours de quarante-huit heures expirait le dimanche 2 décembre 2018 et qu'il ne pouvait être considéré comme matériellement en mesure de saisir directement le juge administratif ou de mandater un avocat à cette fin avant le lundi.<br/>
<br/>
              Par un mémoire en défense, enregistré le 18 septembre 2020, le ministre de l'intérieur conclut au rejet du pourvoi. Il soutient qu'aucun des moyens du pourvoi n'est fondé.<br/>
<br/>
              Par un mémoire en réplique, enregistré le 21 septembre 2020, M. B... reprend les conclusions de son pourvoi et les mêmes moyens. Il conclut, en outre, à ce que le mémoire en défense du ministre de l'intérieur soit écarté des débats dès lors qu'il a été produit trois heures seulement avant la clôture de l'instruction.<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Yohann Bouquerel, maître des requêtes en service extraordinaire, <br/>
<br/>
              - les conclusions de M. Marc Pichon de Vendeuil, rapporteur public,<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Viaud, Krivine, avocat de M. A... B... ;<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que le 13 mars 2018, M. B..., de nationalité serbe, détenu au centre pénitentiaire de Châteauroux depuis le 4 janvier 2018, a sollicité la délivrance d'un titre de séjour en qualité de salarié. Par un arrêté du vendredi 30 novembre 2018, notifié le jour même à 15h30, le préfet de l'Indre a rejeté sa demande, lui a fait obligation de quitter le territoire français sans délai à sa sortie du centre pénitentiaire prévue le 26 mai 2019, a fixé le pays de renvoi et lui a interdit le retour sur le territoire français pour une durée de trois ans. Par une ordonnance du 17 décembre 2018, le président du tribunal administratif de Limoges a rejeté pour tardiveté la demande de M. B..., enregistrée le lundi 3 décembre 2018 à 13h23, tendant à l'annulation pour excès de pouvoir de l'arrêté préfectoral du 30 novembre 2018. Par un arrêt du 8 octobre 2019, contre lequel M. B... se pourvoit en cassation, la cour administrative d'appel de Bordeaux a rejeté l'appel qu'il a formé contre cette ordonnance.<br/>
<br/>
              Sur les conclusions tendant à ce que le mémoire du ministre de l'intérieur soit écarté des débats :<br/>
<br/>
              2. La seule circonstance que le ministre de l'intérieur a produit son mémoire en défense trois heures seulement avant la clôture de l'instruction décidée par la présidente de la 7ème chambre de la Section du contentieux, pour regrettable qu'elle soit, n'est pas de nature à rendre irrecevable ce mémoire. Par suite, il n'y a pas lieu de faire droit aux conclusions de M. B... tendant à ce que le mémoire en défense du ministre de l'intérieur soit écarté des débats.<br/>
<br/>
              Sur le pourvoi :<br/>
<br/>
              3. Aux termes de l'article R. 421-5 du code de justice administrative : " Les délais de recours contre une décision administrative ne sont opposables qu'à la condition d'avoir été mentionnés, ainsi que les voies de recours, dans la notification de la décision ". L'article R. 776-1 du même code, dans sa rédaction applicable au litige, dispose que : " Sont présentées, instruites et jugées selon les dispositions de l'article L. 512-1 du code de l'entrée et du séjour des étrangers et du droit d'asile et celles du présent code, sous réserve des dispositions du présent chapitre, les requêtes dirigées contre : / 1° Les décisions portant obligation de quitter le territoire français, prévues au I de l'article L. 511-1 et à l'article L. 511-3-1 du code de l'entrée et du séjour des étrangers et du droit d'asile, ainsi que les décisions relatives au séjour notifiées avec les décisions portant obligation de quitter le territoire français ; / 2° Les décisions relatives au délai de départ volontaire prévues au II de l'article L. 511-1 du même code ; / 3° Les interdictions de retour sur le territoire français prévues au III du même article et les interdictions de circulation sur le territoire français prévues à l'article L. 511-3-2 du même code ; / 4° Les décisions fixant le pays de renvoi prévues à l'article L. 513-3 du même code ; / 6° Les décisions d'assignation à résidence prévues à l'article L. 561-2 du même code. / Sont instruites et jugées dans les mêmes conditions les conclusions tendant à l'annulation d'une autre mesure d'éloignement prévue au livre V du code de l'entrée et du séjour des étrangers et du droit d'asile, à l'exception des arrêtés d'expulsion, présentées en cas de placement en rétention ". L'article R. 776-19 du même code précise : " Si, au moment de la notification d'une décision mentionnée à l'article R. 776-1, l'étranger est retenu par l'autorité administrative, sa requête peut valablement être déposée, dans le délai de recours contentieux, auprès de ladite autorité administrative ". Il résulte par ailleurs des dispositions combinées des articles R. 776-29 et R. 776-31 du même code, issues du décret du 28 octobre 2016 pris pour l'application du titre II de la loi du 7 mars 2016 relative au droit des étrangers en France, que les étrangers ayant reçu notification d'une décision mentionnée à l'article R. 776-1 du code alors qu'ils sont en détention ont la faculté de déposer leur requête, dans le délai de recours contentieux, auprès du chef de l'établissement pénitentiaire. Il résulte de l'ensemble de ces dispositions qu'il incombe à l'administration d'indiquer, lors de la notification à un étranger détenu des décisions présentant les caractéristiques mentionnées ci-dessus, la possibilité de déposer une requête dans le délai de recours contentieux auprès du chef de l'établissement pénitentiaire.<br/>
<br/>
              4. En premier lieu, pour juger que l'arrêté litigieux était dépourvu d'ambiguïté et comportait une information suffisante pour permettre à M. B..., en l'absence de circonstances particulières susceptibles d'y faire obstacle, d'exercer son recours contentieux dans le délai de quarante-huit heures, la cour a relevé que si l'arrêté indique qu'il est possible de former un recours administratif dans un délai de deux mois, il précise que le recours en annulation devant la juridiction administrative doit être présenté dans un délai de quarante-huit heures et avertit lisiblement en lettres capitales que le recours juridictionnel n'est pas prorogé par la présentation préalable d'un recours administratif. M. B... n'est ainsi pas fondé à soutenir que l'arrêt qu'il attaque serait insuffisamment motivé sur ce point.<br/>
<br/>
              5. En deuxième lieu, aux termes de l'article R. 776-5 du code de justice administrative : " Les délais de quarante-huit heures mentionnés aux articles R. 776-2 et R. 776-4 et les délais de quinze jours mentionnés aux articles R. 776-2 et R. 776-3 ne sont susceptibles d'aucune prorogation ". Contrairement à ce que soutient M. B..., l'arrêté litigieux et sa feuille de notification n'étaient pas ambigus au motif qu'ils ne précisaient pas également que le délai de recours contentieux n'est pas suspendu. N'est ainsi pas fondé le moyen tiré de ce que la cour aurait entaché son arrêt d'une erreur de droit en jugeant que l'arrêté litigieux était dépourvu d'ambiguïté.<br/>
<br/>
              6. En troisième lieu, d'une part, si M. B... reproche à la cour administrative d'appel de Bordeaux de ne pas avoir vérifié si l'arrêté litigieux mentionnait la possibilité de déposer sa requête auprès du chef de l'établissement pénitentiaire, il ressort de ses écritures devant les juges du fond qu'il n'avait pas soulevé un tel moyen, qui est donc nouveau en cassation. D'autre part, la cour administrative d'appel, qui n'était pas tenue s'interroger d'office sur le bien-fondé de l'irrecevabilité opposée au requérant par le tribunal administratif, n'avait pas à rechercher, en l'absence de toute argumentation du requérant sur ce point, si l'arrêté litigieux ou sa notification mentionnait la possibilité de déposer sa requête auprès du chef de l'établissement pénitentiaire. Par suite, les moyens tirés de ce que l'arrêt attaqué serait entaché d'une insuffisance de motivation et d'une erreur de droit sur ce point ne peuvent qu'être écartés.<br/>
<br/>
              7. En dernier lieu, le moyen tiré de ce que le droit à un recours effectif garanti par l'article 6 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales aurait été méconnu en l'espèce, dès lors que M. B... ne pouvait être considéré comme matériellement en mesure de saisir directement le juge administratif ou de mandater un avocat avant la fin du délai de recours, n'a pas été soulevé devant la cour administrative d'appel et ne présente pas le caractère d'un moyen d'ordre public. Ce moyen, nouveau en cassation, est inopérant et ne peut qu'être écarté.<br/>
<br/>
              8. Il résulte de tout ce qui précède que M. B... n'est pas fondé à demander l'annulation de l'arrêt qu'il attaque.<br/>
<br/>
<br/>
<br/>			D E C I D E :<br/>
              --------------<br/>
Article 1er : Le pourvoi de M. B... est rejeté.<br/>
Article 2 : La présente décision sera notifiée à M. A... B... et au ministre de l'intérieur.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
