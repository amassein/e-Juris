<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038244680</ID>
<ANCIEN_ID>JG_L_2019_03_000000420366</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/24/46/CETATEXT000038244680.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème et 6ème chambres réunies, 18/03/2019, 420366, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-03-18</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>420366</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème et 6ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Alain Seban</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Cécile Barrois de Sarigny</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:420366.20190318</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés au secrétariat du contentieux du Conseil d'Etat les 4 mai et 28 décembre 2018, M. B... A...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la circulaire du ministre de l'intérieur du 3 avril 2018 concernant les mouvements de mutation des agents du corps d'encadrement et d'application de la police nationale en ce qu'elle diffère l'application de l'article 85 de la loi n° 2017-256 du 28 février 2017 de programmation relative à l'égalité réelle outre-mer et portant autres dispositions en matière sociale et économique (NOR : INTC1729576C) ;<br/>
<br/>
              2°) d'enjoindre au ministre de l'intérieur d'appliquer le critère de priorité lié au centre des intérêts matériels et moraux résultant des dispositions de cet article ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat une somme de 5000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
<br/>
              - le code civil ;<br/>
<br/>
              - la loi n° 84-16 du 11 janvier 1984 modifiée notamment par l'article 85 de la loi n° 2017-256 du 28 février 2017 ;<br/>
<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Alain Seban, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Cécile Barrois de Sarigny, rapporteur public.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Aux termes du premier alinéa de l'article 60 de la loi du 11 janvier 1984 portant dispositions statutaires relatives à la fonction publique de l'Etat, dans sa rédaction applicable avant l'entrée en vigueur de la loi du 28 février 2017 de programmation relative à l'égalité réelle outre-mer et portant autres dispositions en matière sociale et économique : " L'autorité compétente procède aux mouvements des fonctionnaires après avis des commissions administratives paritaires. / Dans les administrations ou services où sont dressés des tableaux périodiques de mutations, l'avis des commissions est donné au moment de l'établissement de ces tableaux. (...) " Aux termes du quatrième alinéa de ce même article dans cette même rédaction : " Dans toute la mesure compatible avec le bon fonctionnement du service, les affectations prononcées doivent tenir compte des demandes formulées par les intéressés et de leur situation de famille. Priorité est donnée aux fonctionnaires séparés de leur conjoint pour des raisons professionnelles, aux fonctionnaires séparés pour des raisons professionnelles du partenaire avec lequel ils sont liés par un pacte civil de solidarité (...), aux fonctionnaires handicapés relevant de l'une des catégories mentionnées aux 1°, 2°, 3°, 4°, 9°, 10° et 11° de l'article L. 5212-13 du code du travail et aux fonctionnaires qui exercent leurs fonctions, pendant une durée et selon des modalités fixées par décret en Conseil d'Etat, dans un quartier urbain où se posent des problèmes sociaux et de sécurité particulièrement difficiles ". Aux termes du dernier alinéa alors applicable : " Dans les administrations ou services mentionnés au deuxième alinéa du présent article, l'autorité compétente peut procéder à un classement préalable des demandes de mutation à l'aide d'un barème rendu public. Le recours à un tel barème constitue une mesure préparatoire et ne se substitue pas à l'examen de la situation individuelle des agents. Ce classement est établi dans le respect des priorités figurant au quatrième alinéa du présent article. Toutefois, l'autorité compétente peut édicter des lignes directrices par lesquelles elle définit, sans renoncer à son pouvoir d'appréciation, des critères supplémentaires établis à titre subsidiaire, notamment pour les fonctionnaires qui justifient du centre de leurs intérêts matériels et moraux dans une des collectivités régies par les articles 73 et 74 de la Constitution ainsi qu'en Nouvelle-Calédonie, dans des conditions prévues par décret en Conseil d'Etat ".<br/>
<br/>
              2. L'article 85 de la loi du 28 février 2017 de programmation relative à l'égalité réelle outre-mer et portant autres dispositions en matière sociale et économique a modifié ces dispositions. Il a, en premier lieu ajouté au quatrième alinéa une priorité donnée à l'examen des demandes de mutation des " fonctionnaires qui justifient du centre de leurs intérêts matériels et moraux dans une des collectivités régies par les articles 73 et 74 de la Constitution ainsi qu'en Nouvelle-Calédonie ". Il a, en second lieu, retiré du dernier aliéna la précision selon laquelle la situation de ces mêmes fonctionnaires peut faire l'objet de critères supplémentaires établis à titre subsidiaire par les lignes directrices édictées en application de cet alinéa.<br/>
<br/>
              3. Par la circulaire attaquée du 3 avril 2018 concernant les mouvements de mutation des agents du corps d'encadrement et d'application de la police nationale, le ministre de l'intérieur a toutefois prévu que : " Conformément aux dispositions introduites par l'article 85 de la loi n° 2017-256 du 28 février 2017 de programmation relative à l'égalité réelle outre-mer et portant autres dispositions en matière sociale et économique, le traitement des demandes de mutation vers l'outre-mer intègrera, après réunion des conditions nécessaires et concertation, le critère du centre des intérêts matériels et moraux (CIMM), à compter de l'année 2019 ". M. A... demande l'annulation de cette circulaire en tant qu'elle diffère l'application de la priorité prévue par les dispositions législatives ajoutées à l'article 60 de la loi du 11 janvier 1984 par l'article 85 de la loi du 28 février 2017.<br/>
<br/>
              4. Les dispositions de la loi du 28 février 2017 n'ont prévu aucun report de leur entrée en vigueur. Il ne ressort pas des pièces du dossier que leur entrée en vigueur aurait été manifestement impossible en l'absence de dispositions réglementaires fixant des modalités d'application, dispositions dont elles ne prévoient d'ailleurs pas l'intervention. Est sans incidence à cet égard la faculté donnée à l'administration d'adopter dans certains services des barèmes destinés à classer les demandes des agents ainsi que des lignes directrices pour fixer des critères subsidiaires applicables à l'examen de ces demandes, les lignes directrices ne pouvant au demeurant, depuis la loi du 28 février 2017, concerner la situation des fonctionnaires ayant le centre de leurs intérêts matériels et moraux outre-mer, qui bénéficient d'une priorité prévue par la loi. L'entrée en vigueur de ces dispositions législatives est dès lors intervenue, conformément à l'article 1er du code civil, au lendemain de leur publication. A compter de cette date, il appartenait à l'administration de tenir compte, dans l'examen des demandes de mutation, de la priorité accordée aux fonctionnaires qui justifient avoir le centre de leurs intérêts matériels et moraux dans une des collectivités régies par les articles 73 et 74 de la Constitution ainsi qu'en Nouvelle-Calédonie.<br/>
<br/>
              5. Il suit de là que le ministre n'est pas fondé à soutenir qu'il bénéficiait, pour mettre en oeuvre les dispositions issues de la loi du 28 février 2017, d'un délai raisonnable destiné à lui permettre d'adapter les barèmes ou d'arrêter des lignes directrices, délai raisonnable qui, au demeurant, aurait été dépassé à la date de la circulaire attaquée du 3 avril 2018. Il ne saurait davantage à cet égard se prévaloir ni de l'absence de consensus entre les organisations syndicales représentatives, ni de la complexité de la notion de centre des intérêts matériels et moraux, qu'il lui appartenait d'appliquer sous le contrôle du juge, ni enfin de la complexité alléguée des modifications techniques à introduire dans le système automatisé de traitement des demandes de mutation, qui ne pouvaient être regardées comme rendant manifestement impossible l'application de la loi. <br/>
<br/>
              6. Il résulte de ce qui précède que, sans qu'il soit besoin d'examiner l'autre moyen de la requête, M. A...est fondé à demander l'annulation de la circulaire qu'il attaque en tant qu'elle reporte à l'année 2019 la mise en oeuvre des dispositions de l'article 85 de la loi du 28 février 2017.<br/>
<br/>
              7. La présente décision n'appelle aucune mesure particulière d'exécution de la part du ministre de l'intérieur. Les conclusions à fin d'injonction de M. A...ne peuvent donc qu'être rejetées.<br/>
<br/>
              8. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 3 000 euros à verser à M. A...au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : La circulaire du ministre de l'intérieur du 3 avril 2018 concernant les mouvements de mutation des agents du corps d'encadrement et d'application de la police nationale est annulée en tant qu'elle reporte à l'année 2019 la mise en oeuvre des dispositions de l'article 85 de la loi n° 2017-256 du 28 février 2017.<br/>
Article 2 : L'Etat versera à M. A...une somme de 3000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : Le surplus des conclusions de la requête est rejeté.<br/>
Article 4 : La présente décision sera notifiée à M. B...A...et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
