<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030750274</ID>
<ANCIEN_ID>JG_L_2015_06_000000388457</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/75/02/CETATEXT000030750274.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème - 2ème SSR, 17/06/2015, 388457</TITRE>
<DATE_DEC>2015-06-17</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>388457</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème - 2ème SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP PIWNICA, MOLINIE ; SCP BARADUC, DUHAMEL, RAMEIX</AVOCATS>
<RAPPORTEUR>M. Frédéric Dieu</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Gilles Pellissier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:XX:2015:388457.20150617</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société Proxiserve a demandé au juge des référés du tribunal administratif de Melun, sur le fondement de l'article L. 551-13 du code de justice administrative, d'annuler le contrat ayant pour objet la pose, la location, la relève et l'entretien des compteurs d'eau du patrimoine de l'Office public de l'habitat (OPH) " Marne et Chantereine Habitat ".<br/>
<br/>
              Par une ordonnance n° 1500774 du 13 février 2015, le juge des référés du tribunal administratif de Melun a rejeté sa demande.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 4 et 19 mars et le 12 mai 2015 au secrétariat du contentieux du Conseil d'Etat, la société Proxiserve demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance ; <br/>
<br/>
              2°) statuant en référé, de faire droit à sa demande ; <br/>
<br/>
              3°) de mettre à la charge de l'Office public de l'habitat (OPH) " Marne et Chantereine Habitat " le versement de la somme de 4 000 euros en application des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - l'ordonnance n° 2005-649 du 6 juin 2005 ;<br/>
              - le décret n° 2005-1742 du 30 décembre 2005 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Frédéric Dieu, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Gilles Pellissier, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Piwnica, Molinié, avocat de la société Proxiserve, et à la SCP Baraduc, Duhamel, Rameix, avocat de l'Office public de l'habitat " Marne et Chantereine Habitat " ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis au juge des référés du tribunal administratif de Melun que, par un avis d'appel public à la concurrence publié le 10 septembre 2014 au Journal officiel de l'Union européenne, l'Office public de l'habitat (OPH) " Marne et Chantereine Habitat " a lancé, sur le fondement des dispositions des articles 28 et 29 du décret du 30 décembre 2005 fixant les règles applicables aux marchés passés par les pouvoirs adjudicateurs mentionnés à l'article 3 de l'ordonnance du 6 juin 2005, une procédure d'appel d'offres ouvert en vue de l'attribution d'un marché ayant pour objet la " pose, [la] location, [la] relève et [l']entretien des compteurs d'eau du patrimoine de Marne et Chantereine Habitat " ; que, le 5 janvier 2015, la commission d'appel d'offres a décidé de retenir l'offre de la société Ocea Smart Building ; que la société Proxiserve, dont l'offre a été rejetée, a saisi le juge du référé précontractuel du tribunal administratif de Melun, sur le fondement des dispositions de l'article L. 551-1 du code de justice administrative, d'une demande tendant à l'annulation de la procédure de passation de ce contrat ; qu'ayant appris au cours de l'instance que le marché litigieux avait été signé par l'OPH, la société Proxiserve, renonçant à ses conclusions initiales, a demandé au juge du référé contractuel du même tribunal, sur le fondement de l'article L. 551-13 du code de justice administrative, d'annuler ce marché ; que, par l'ordonnance attaquée, ce dernier a rejeté sa demande après l'avoir jugée irrecevable ; <br/>
<br/>
              2. Considérant, d'une part, qu'aux termes de l'article L. 551-13 du code de justice administrative : " Le président du tribunal administratif, ou le magistrat qu'il délègue, peut être saisi, une fois conclu l'un des contrats mentionnés aux articles L. 551-1 et L. 551-5, d'un recours régi par la présente section " et que selon l'article L. 551-14 du même code " (...) le recours régi par la présente section n'est pas ouvert au demandeur ayant fait usage du recours prévu à l'article L. 551-1 ou à l'article L. 551-5 dès lors que le pouvoir adjudicateur ou l'entité adjudicatrice a respecté la suspension prévue à l'article L. 551-4 ou à l'article L. 551-9 et s'est conformé à la décision juridictionnelle rendue sur ce recours. " ; <br/>
<br/>
              3. Considérant, d'autre part, qu'aux termes du 1° du I de l'article 46 du décret du 30 décembre 2005, dans sa rédaction applicable au présent litige : " Pour les marchés et accords cadres passés selon une procédure formalisée autre que celle du II de l'article 33, le pouvoir adjudicateur, dès qu'il a fait son choix pour une candidature ou une offre, notifie à tous les autres candidats le rejet de leur candidature ou de leur offre (...) Un délai d'au moins seize jours est respecté entre la date d'envoi de la notification prévue aux alinéas précédents et la date de conclusion du marché. Ce délai est réduit à au moins onze jours en cas de transmission électronique de la notification à l'ensemble des candidats intéressés. / La notification de l'attribution du marché ou de l'accord-cadre comporte l'indication de la durée du délai de suspension que le pouvoir adjudicateur s'impose, eu égard notamment au mode de transmission retenu. " ; qu'en application de ces dispositions, le pouvoir adjudicateur doit indiquer aux candidats non retenus, dans la notification du rejet de leur offre, la date à laquelle ou le délai au-delà duquel il signera le marché litigieux, cette date ou ce délai devant être fixés dans le respect du délai minimum de suspension prévu par les mêmes dispositions ;<br/>
<br/>
              4. Considérant que les dispositions de l'article L. 551-14 du code de justice administrative n'ont pas pour effet de rendre irrecevable un référé contractuel introduit par un concurrent évincé qui avait antérieurement présenté un référé précontractuel alors qu'il était dans l'ignorance du rejet de son offre et de la signature du marché, par suite d'un manquement du pouvoir adjudicateur au respect des dispositions prévoyant une information des concurrents évincés sur ce point, telles celles, citées ci-dessus, du décret du 30 décembre 2005 ; que les dispositions de l'article L. 551-14 ne sauraient non plus avoir pour effet de rendre irrecevable le référé contractuel du concurrent évincé ayant antérieurement présenté un référé précontractuel qui, bien qu'informé du rejet de son offre par le pouvoir adjudicateur, ne l'a pas été du délai de suspension que ce dernier s'imposait entre la date d'envoi de la notification du rejet de l'offre et la conclusion du marché, lorsqu'une telle information doit être donnée dans la notification du rejet ; qu'il en va de même lorsque cette notification indique un délai inférieur au délai minimum prévu par les dispositions applicables, alors même que le contrat aurait été finalement signé dans le respect de ce délai minimum ;<br/>
<br/>
              5. Considérant qu'il résulte de ce qui précède qu'en jugeant irrecevable le recours en référé contractuel de la société Proxiserve au motif que, si la notification du rejet de son offre indiquait un délai de signature inférieur au délai minimum institué par le I de l'article 46 du décret du 30 décembre 2005, le contrat n'avait été signé qu'à l'expiration de ce délai, le juge des référés du tribunal administratif de Melun a commis une erreur de droit ; que, par suite et sans qu'il soit besoin d'examiner les autres moyens de son pourvoi, la société Proxiserve est fondée à demander l'annulation de l'ordonnance attaquée ;<br/>
<br/>
              6. Considérant qu'il y a lieu, en application de l'article L. 821-2 du code de justice administrative, de régler l'affaire au titre de la procédure de référé engagée par la société Proxiserve ;<br/>
<br/>
              Sur la recevabilité des conclusions présentées sur le fondement de l'article L. 551-13 du code de justice administrative :<br/>
<br/>
              7. Considérant qu'il résulte de l'instruction que le courrier du 13 janvier 2015 par lequel l'OPH a informé la société Proxiserve de l'attribution du marché à la société Ocea Smart Building, mentionnait un délai de suspension de cinq jours avant la conclusion du marché, inférieur au délai minimum de seize jours imposé par les dispositions précitées du décret du 30 décembre 2005 ; qu'ainsi qu'il a été dit ci-dessus, la circonstance que le contrat a été signé le 3 février 2015, dans le respect du délai minimum, ne peut, en tout état de cause, être utilement opposée aux conclusions de la requérante présentées sur le fondement de l'article L. 551-13 du code de justice administrative ; que ces conclusions, présentées dans un mémoire distinct de celui visant, initialement, l'annulation de la procédure sur le fondement de l'article L. 551-1 de ce code, sont recevables, sans que puissent leur être opposées les dispositions du chapitre 1er du titre V du livre V du code de justice administrative, selon lesquelles les demandes formées devant le juge des référés sur le fondement de l'article L. 551-1 sont présentées et jugées selon des règles distinctes de celles applicables aux demandes présentées sur le fondement de l'article L. 551-13 ;<br/>
<br/>
              Sur les conclusions tendant à l'annulation du marché :<br/>
<br/>
              8. Considérant qu'en vertu des dispositions du troisième alinéa de l'article L. 551-18 du code de justice administrative, le juge du référé contractuel prononce la nullité du contrat " lorsque celui-ci a été signé avant l'expiration du délai exigé après l'envoi de la décision d'attribution aux opérateurs économiques ayant présenté une candidature ou une offre ou pendant la suspension prévue à l'article L. 551-4 ou à l'article L. 551-9 si, en outre, deux conditions sont remplies : la méconnaissance de ces obligations a privé le demandeur de son droit d'exercer le recours prévu par les articles L. 551-1 et L. 551-5, et les obligations de publicité et de mise en concurrence auxquelles sa passation est soumise ont été méconnues d'une manière affectant les chances de l'auteur du recours d'obtenir le contrat. " ; <br/>
<br/>
              9. Considérant qu'il résulte de l'instruction, en particulier du rapport d'analyse des offres, que, pour noter le critère du prix pondéré à 50 % selon le règlement de la consultation, le pouvoir adjudicateur a distingué les éléments des offres relatifs au " prix global ", à l'" investissement ", élément désignant le montant des dépenses d'installation des nouveaux compteurs à la charge du pouvoir adjudicateur, et au " prix mensuel moyen supporté par le locataire " ; que les deux premiers éléments ont été affectés, au moment de l'évaluation des offres, d'une pondération de 10 % et le troisième d'une pondération de 30 % ; qu'en s'abstenant de porter à la connaissance des candidats cette décomposition de la pondération du critère du prix, alors que les éléments d'appréciation relatifs à l'investissement et au prix mensuel moyen supporté par le locataire constituaient, eu égard à leur objet et à leur importance, des sous-critères assimilables à de véritables critères de jugement des offres, l'OPH " Marne et Chantereine Habitat " a manqué à ses obligations de publicité et de mise en concurrence ; qu'il résulte de l'instruction que si la société Proxiserve, dont l'offre était la moins chère, a obtenu la note maximale pour le sous-critère relatif au prix mensuel moyen supporté par le locataire, elle n'a obtenu, pour le sous-critère relatif à l'investissement, qu'une note de sept sur dix ; qu'eu égard à l'écart d'un point seulement entre la note globale attribuée à la société Proxiserve et celle attribuée à la société Ocea Smart Building, avec laquelle le marché a été conclu, le manquement tenant au défaut d'information des candidats doit être regardé comme ayant affecté les chances de la société Proxiserve d'obtenir le contrat ;<br/>
<br/>
              10. Considérant qu'il résulte de ce qui précède que la société Proxiserve est fondée à demander l'annulation du contrat sur le fondement du troisième alinéa de l'article L. 551-18 du code de justice administrative ; qu'aucun des éléments invoqués par l'OPH, qui ne sont pas étayés, tenant à l'impossibilité de procéder à des régularisations de charges, à la pénalisation des locataires et à la mise en péril des finances de l'office, ne constitue une raison impérieuse d'intérêt général justifiant le prononcé de l'une des mesures alternatives à l'annulation du contrat prévues par l'article L. 551-19 du même code ; que, cependant, compte tenu de la nécessité d'assurer la continuité des prestations de relevé et de maintenance des compteurs durant le délai nécessaire au lancement d'une nouvelle procédure de publicité et de mise en concurrence et à l'attribution du nouveau marché et de l'intérêt général qui s'attache à ce que cette continuité soit préservée, il y a lieu de ne prononcer l'annulation du marché qu'à l'expiration d'un délai de quatre mois à compter de la date de la présente décision ;<br/>
<br/>
              11. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que le versement d'une somme soit mis à la charge de la société Proxiserve qui n'est pas, dans la présente instance, la partie perdante ; qu'il y a lieu, en revanche, au titre des mêmes dispositions, de mettre à la charge de l'Office public de l'habitat " Marne et Chantereine Habitat " le versement à la société Proxiserve de la somme de 4 500 euros au titre des frais exposés par cette dernière et non compris dans les dépens pour la procédure suivie devant le Conseil d'Etat et le tribunal administratif de Melun ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'ordonnance du juge des référés du tribunal administratif de Melun du 13 février 2015 est annulée.<br/>
Article 2 : Le contrat conclu par l'Office public de l'habitat " Marne et Chantereine Habitat " le 3 février 2015 avec la société Ocea Smart Building est annulé. Cette annulation prendra effet à l'expiration d'un délai de quatre mois à compter de la date de la présente décision.<br/>
Article 3 : L'Office public de l'habitat " Marne et Chantereine Habitat " versera une somme de 4 500 euros à la société Proxiserve en application des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Les conclusions présentées par l'Office public de l'habitat " Marne et Chantereine Habitat " en application des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée à la société Proxiserve et à l'Office public de l'habitat " Marne et Chantereine Habitat ".<br/>
Copie en sera adressée pour information à la société Ocea Smart Building<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">39-08-01 MARCHÉS ET CONTRATS ADMINISTRATIFS. RÈGLES DE PROCÉDURE CONTENTIEUSE SPÉCIALES. RECEVABILITÉ. - RÉFÉRÉ CONTRACTUEL - IRRECEVABILITÉ LORSQU'UN RÉFÉRÉ PRÉCONTRACTUEL A ÉTÉ EXERCÉ (ART. L. 551-14 DU CJA) - EXCEPTIONS - 1) IGNORANCE DE LA SIGNATURE DU MARCHÉ [RJ1] - 2) IGNORANCE DU DÉLAI DE SUSPENSION [RJ2] OU NOTIFICATION INDIQUANT UN DÉLAI INFÉRIEUR AU DÉLAI MINIMUM.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">39-08-015-02 MARCHÉS ET CONTRATS ADMINISTRATIFS. RÈGLES DE PROCÉDURE CONTENTIEUSE SPÉCIALES. PROCÉDURES D'URGENCE. - IRRECEVABILITÉ LORSQU'UN RÉFÉRÉ PRÉCONTRACTUEL A ÉTÉ EXERCÉ (ART. L. 551-14 DU CJA) - EXCEPTIONS - 1) IGNORANCE DE LA SIGNATURE DU MARCHÉ [RJ1] - 2) IGNORANCE DU DÉLAI DE SUSPENSION [RJ2] OU NOTIFICATION INDIQUANT UN DÉLAI INFÉRIEUR AU DÉLAI MINIMUM.
</SCT>
<ANA ID="9A"> 39-08-01 1) Les dispositions de l'article L. 551-14 du code de justice administrative (CJA) n'ont pas pour effet de rendre irrecevable un référé contractuel introduit par un concurrent évincé qui avait antérieurement présenté un référé précontractuel alors qu'il était dans l'ignorance du rejet de son offre et de la signature du marché, par suite d'un manquement du pouvoir adjudicateur au respect des dispositions prévoyant une information des concurrents évincés sur ce point, telles celles du décret n°2005-1742 du 30 décembre 2005.... ,,2) Ces dispositions ne sauraient non plus avoir pour effet de rendre irrecevable le référé contractuel du concurrent évincé ayant antérieurement présenté un référé précontractuel qui, bien qu'informé du rejet de son offre par le pouvoir adjudicateur, ne l'a pas été du délai de suspension que ce dernier s'imposait entre la date d'envoi de la notification du rejet de l'offre et la conclusion du marché, lorsqu'une telle information doit être donnée dans la notification du rejet. Il en va de même lorsque cette notification indique un délai inférieur au délai minimum prévu par les dispositions applicables, alors même que le contrat aurait été finalement signé dans le respect de ce délai minimum.</ANA>
<ANA ID="9B"> 39-08-015-02 1) Les dispositions de l'article L. 551-14 du code de justice administrative (CJA) n'ont pas pour effet de rendre irrecevable un référé contractuel introduit par un concurrent évincé qui avait antérieurement présenté un référé précontractuel alors qu'il était dans l'ignorance du rejet de son offre et de la signature du marché, par suite d'un manquement du pouvoir adjudicateur au respect des dispositions prévoyant une information des concurrents évincés sur ce point, telles celles du décret n°2005-1742 du 30 décembre 2005.... ,,2) Ces dispositions ne sauraient non plus avoir pour effet de rendre irrecevable le référé contractuel du concurrent évincé ayant antérieurement présenté un référé précontractuel qui, bien qu'informé du rejet de son offre par le pouvoir adjudicateur, ne l'a pas été du délai de suspension que ce dernier s'imposait entre la date d'envoi de la notification du rejet de l'offre et la conclusion du marché, lorsqu'une telle information doit être donnée dans la notification du rejet. Il en va de même lorsque cette notification indique un délai inférieur au délai minimum prévu par les dispositions applicables, alors même que le contrat aurait été finalement signé dans le respect de ce délai minimum.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1]Cf. CE, 10 novembre 2010, Etablissement public national des produits de l'agriculture et de la mer (France Agrimer), n° 340944, T. p. 858., ,[RJ2]Cf. CE, 24 juin 2011, Office public de l'habitat interdépartemental de l'Essonne, du Val d'Oise et des Yvelines et société Seni, n°s 346665 346746, T. p. 1023.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
