<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032405464</ID>
<ANCIEN_ID>JG_L_2016_04_000000388712</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/40/54/CETATEXT000032405464.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème SSJS, 13/04/2016, 388712</TITRE>
<DATE_DEC>2016-04-13</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>388712</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème SSJS</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>RICARD ; BALAT</AVOCATS>
<RAPPORTEUR>M. Lionel Collet</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2016:388712.20160413</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Par trois décisions n° 13/010, 13/001 et 13/002 du 6 décembre 2013, la chambre disciplinaire de première instance de l'ordre des masseurs-kinésithérapeutes d'Ile-de-France et de La Réunion a prononcé la radiation de M. A...B...du tableau de l'ordre des masseurs-kinésithérapeutes.<br/>
<br/>
              Par une décision n° 003-2014/004-2014 du 10 février 2015, la chambre disciplinaire nationale de l'ordre des masseurs-kinésithérapeutes a rejeté la requête d'appel de M. B....<br/>
<br/>
              Par un pourvoi enregistré le 13 mars 2015 au secrétariat du contentieux du Conseil d'Etat, M. B...demande au Conseil d'Etat :<br/>
<br/>
               1°) d'annuler cette décision ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ; <br/>
<br/>
              3°) de mettre à la charge du Conseil national de l'ordre des masseurs-kinésithérapeutes la somme de 6 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
<br/>
              - le code de la santé publique ;<br/>
<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Lionel Collet, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Ricard, avocat de M. B... et à Me Balat, avocat du Conseil national de l'ordre des masseurs-kinésithérapeutes et du Conseil départemental de l'ordre des masseurs-kinésithérapeutes des Bouches-du-Rhône ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, par trois décisions du 6 décembre 2013, la chambre disciplinaire de première instance de l'ordre des masseurs-kinésithérapeutes d'Ile-de-France et de La Réunion, statuant respectivement sur une plainte déposée par le conseil départemental de l'ordre des masseurs-kinésithérapeutes des Bouches-du-Rhône et sur deux plaintes déposées conjointement par ce conseil départemental et par le conseil national de l'ordre, a infligé à M. B...la sanction de radiation du tableau de l'ordre des masseurs-kinésithérapeutes ; que l'intéressé se pourvoit en cassation contre la décision du 10 février 2015 par laquelle la chambre disciplinaire nationale de l'ordre a rejeté ses requêtes d'appel après les avoir jointes ;<br/>
<br/>
              2. Considérant, en premier lieu, qu'aux termes du cinquième alinéa de l'article R. 4126-1 du code de la santé publique : " Les plaintes sont signées par leur auteur et, dans le cas d'une personne morale, par une personne justifiant de sa qualité pour agir. Dans ce dernier cas, la plainte est accompagnée, à peine d'irrecevabilité, de la délibération de l'organe statutairement compétent pour autoriser la poursuite ou, pour le conseil départemental ou national, de la délibération signée par le président et comportant l'avis motivé du conseil " ; qu'aux termes de l'article R. 4323-3 du même code : " Les dispositions des articles R. 4126-1 à R. 4126-54 sont applicables aux masseurs-kinésithérapeutes et aux pédicures-podologues " ; qu'il résulte de la combinaison de ces dispositions que le conseil départemental ou national de l'ordre des masseurs-kinésithérapeutes ne peut valablement former une plainte disciplinaire qu'après en avoir délibéré de façon collégiale ; qu'en cas de consultation par voie électronique des membres d'une instance ordinale sur un projet de plainte, l'absence de confirmation de ce vote par une délibération collégiale de la même instance entache d'irrégularité la décision par laquelle elle dépose une plainte à l'encontre d'un masseur-kinésithérapeute ; qu'ainsi, en jugeant, pour écarter ce moyen qui, étant d'ordre public, était valablement soulevé pour la première fois en appel, que l'absence de confirmation, par une délibération ultérieure du conseil départemental des Bouches-du-Rhône, du vote électronique auquel a procédé ce conseil le 3 février 2013 pour décider de déposer plainte contre M. B... était sans incidence sur la recevabilité de cette plainte, la chambre de discipline nationale de l'ordre a commis une erreur de droit ;<br/>
<br/>
              3. Considérant, en second lieu, que pour répondre aux conclusions présentées le 12 décembre 2014 par M. B...et tendant à la récusation de quatre des six assesseurs appelés à siéger au sein de la formation de jugement aux côtés de sa présidente, la chambre disciplinaire nationale a estimé que cette demande de récusation, si elle était accueillie, la mettrait dans l'impossibilité de statuer sur le litige et qu'elle devait par suite être regardée comme une demande de renvoi pour cause de suspicion légitime, irrecevable devant une juridiction unique dont le ressort couvre l'ensemble du territoire national ; que, toutefois, il résulte des dispositions  de l'article R. 4321-39 du code de la santé publique que la chambre disciplinaire nationale comprend, outre son président, douze membres titulaires et douze membres suppléants ; qu'ainsi, le remplacement de quatre des six assesseurs qui composaient avec la présidente la formation de jugement appelée à statuer sur la plainte était possible ; qu'il suit de là qu'en retenant que la demande de récusation présentée, si elle était accueillie, la mettrait dans l'impossibilité de statuer, et en requalifiant en conséquence les conclusions dont elle était saisie, la chambre disciplinaire nationale de l'ordre a commis une erreur de droit ;<br/>
<br/>
              4. Considérant qu'il résulte de ce qui précède que la décision du 10 février 2015 de la chambre disciplinaire nationale de l'ordre des masseurs-kinésithérapeutes doit être annulée ;<br/>
<br/>
              5. Considérant que M. B...demande sur le fondement des dispositions de l'article L. 761-1 du code de justice administrative qu'une somme de 3 000 euros soit mise à la charge du Conseil national de l'ordre des masseurs-kinésithérapeutes qui a, ayant eu la qualité de plaignant devant la juridiction disciplinaire, est partie à l'instance devant le Conseil d'Etat ; qu'il y a lieu, dans les circonstances de l'espèce, de faire droit à ces conclusions ; que les mêmes dispositions font obstacle à ce qu'une somme soit mise à la charge de M. B... qui n'est pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La décision du 10 février 2015 de la chambre disciplinaire nationale de l'ordre des masseurs-kinésithérapeutes est annulée.<br/>
<br/>
Article 2 : L'affaire est renvoyée à la chambre disciplinaire nationale de l'ordre des masseurs-kinésithérapeutes.<br/>
<br/>
Article 3 : Le Conseil national de l'ordre des masseurs-kinésithérapeutes versera la somme de 3 000 euros à M. B...au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : Les conclusions présentées le Conseil national de l'ordre des masseurs-kinésithérapeutes et le conseil départemental de l'ordre des masseurs-kinésithérapeutes des Bouches-du-Rhône au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 5 : La présente décision sera notifiée à M. A... B..., au Conseil national de l'ordre des masseurs-kinésithérapeutes et au conseil départemental de l'ordre des masseurs-kinésithérapeutes des Bouches-du-Rhône.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">55-01-02-018 PROFESSIONS, CHARGES ET OFFICES. ORDRES PROFESSIONNELS - ORGANISATION ET ATTRIBUTIONS NON DISCIPLINAIRES. QUESTIONS PROPRES À CHAQUE ORDRE PROFESSIONNEL. - PLAINTE DISCIPLINAIRE D'UN CONSEIL DE L'ORDRE DES MASSEURS-KINÉSITHÉRAPEUTES - CONSULTATION DES MEMBRES PAR VOIE ÉLECTRONIQUE - NÉCESSITÉ D'UNE DÉLIBÉRATION COLLÉGIALE ULTÉRIEURE - EXISTENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">55-04-01-01 PROFESSIONS, CHARGES ET OFFICES. DISCIPLINE PROFESSIONNELLE. PROCÉDURE DEVANT LES JURIDICTIONS ORDINALES. INTRODUCTION DE L'INSTANCE. - PLAINTE DISCIPLINAIRE D'UN CONSEIL DE L'ORDRE DES MASSEURS-KINÉSITHÉRAPEUTES - CONSULTATION DES MEMBRES PAR VOIE ÉLECTRONIQUE - NÉCESSITÉ D'UNE DÉLIBÉRATION COLLÉGIALE ULTÉRIEURE - EXISTENCE.
</SCT>
<ANA ID="9A"> 55-01-02-018 Il résulte de la combinaison des articles R. 4126-1 et R. 4323-3 du code de la santé publique que le conseil départemental ou national de l'ordre des masseurs-kinésithérapeutes ne peut valablement former une plainte disciplinaire qu'après en avoir délibéré de façon collégiale. En cas de consultation par voie électronique des membres d'une instance ordinale sur un projet de plainte, l'absence de confirmation de ce vote par une délibération collégiale de la même instance entache d'irrégularité la décision par laquelle elle dépose une plainte à l'encontre d'un masseur-kinésithérapeute. Ce moyen est d'ordre public.</ANA>
<ANA ID="9B"> 55-04-01-01 Il résulte de la combinaison des articles R. 4126-1 et R. 4323-3 du code de la santé publique que le conseil départemental ou national de l'ordre des masseurs-kinésithérapeutes ne peut valablement former une plainte disciplinaire qu'après en avoir délibéré de façon collégiale. En cas de consultation par voie électronique des membres d'une instance ordinale sur un projet de plainte, l'absence de confirmation de ce vote par une délibération collégiale de la même instance entache d'irrégularité la décision par laquelle elle dépose une plainte à l'encontre d'un masseur-kinésithérapeute. Ce moyen est d'ordre public.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
