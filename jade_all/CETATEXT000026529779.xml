<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026529779</ID>
<ANCIEN_ID>JG_L_2012_10_000000330650</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/52/97/CETATEXT000026529779.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème et 10ème sous-sections réunies, 22/10/2012, 330650</TITRE>
<DATE_DEC>2012-10-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>330650</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème et 10ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP PEIGNOT, GARREAU, BAUER-VIOLAS ; SCP TIFFREAU, CORLAY, MARLANGE</AVOCATS>
<RAPPORTEUR>M. Philippe Josse</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Aladjidi</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2012:330650.20121022</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête sommaire et le mémoire complémentaire, enregistrés les 10 août et 5 novembre 2009 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. et Mme Alain B, demeurant ... et pour le groupement agricole d'exploitation en commun (GAEC) de la Vallée, dont le siège est situé lieudit Les Coins à Saint-Mathurin-sur-Loire (49250) ; M. et Mme B et le GAEC de la Vallée demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 08NT01929 du 23 avril 2009 par lequel la cour administrative d'appel de Nantes a rejeté leur requête tendant, d'une part, à l'annulation du jugement n°s 06-4487 et 06-4540 du tribunal administratif de Nantes du 6 mai 2008 et, d'autre part, à la condamnation de la commune de Saint-Mathurin-sur-Loire et de l'Etat à leur verser les sommes de 2 287 euros au titre de leur préjudice matériel lié à la désaffectation de leur stabulation et à sa mise en conformité, de 50 000 euros au titre des frais et condamnations dont ils ont fait l'objet et de 5 000 euros au titre de leur préjudice moral ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à leur requête d'appel ;<br/>
<br/>
              3°) de mettre à la charge de la commune et de l'Etat le versement d'une somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de l'urbanisme ;<br/>
<br/>
              Vu la loi n  68-1250 du 31 décembre 1968 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Philippe Josse, Conseiller d'Etat,  <br/>
<br/>
              - les observations de la SCP Peignot, Garreau, Bauer-Violas, avocat du GAEC de la Vallée et de M. et Mme B et de la SCP Tiffreau, Corlay, Marlange, avocat de la commune de Saint-Mathurin-sur-Loire,<br/>
<br/>
              - les conclusions de M. Frédéric Aladjidi, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Peignot, Garreau, Bauer-Violas, avocat du GAEC de la Vallée et de M. et Mme B et à la SCP Tiffreau, Corlay, Marlange, avocat de la commune de Saint-Mathurin-sur-Loire ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, par arrêté du 24 octobre 1987, le maire de la commune de Saint-Mathurin-sur-Loire a délivré à M. et Mme B un permis de construire une stabulation destinée à accueillir soixante bovins au lieudit " Les Coins " ; que le Conseil d'Etat a confirmé l'annulation de ce permis, sur appel d'un jugement du tribunal administratif de Nantes, par une décision du 31 juillet 1996 ; que, par un arrêt de la cour d'appel d'Angers du 17 avril 2001, signifié le 3 juillet 2001, M. et Mme B ont été condamnés, sur demande de M. C, voisin de la stabulation, à indemniser les troubles de voisinage qu'elle lui causait et, sous astreinte, à la démolir dans un délai de six mois ; qu'un nouveau permis de construire a été délivré à M. Patrice B, fils de M. et Mme B, par un arrêté du 8 décembre 2001, l'autorisant à étendre et transformer la stabulation litigieuse en un bâtiment affecté au stockage de matériel et de fourrage ; que, par un nouvel arrêt rendu le 1er mars 2004, la cour d'appel d'Angers a confirmé l'astreinte prononcée par le juge de l'exécution à l'encontre de M. et Mme B pour inexécution de son arrêt du 17 avril 2001 ; que le permis du 8 décembre 2001 a lui-même été annulé par un jugement du tribunal administratif de Nantes du 26 mai 2005 ; que M. et Mme B, ainsi que le GAEC de la Vallée, ont présenté, tant auprès de la commune de Saint-Mathurin-sur-Loire que de l'Etat, des demandes d'indemnisation des préjudices qu'ils estimaient avoir subis du fait de l'illégalité des permis qui leur avaient été délivrés les 24 octobre 1987 et 8 décembre 2001 ; que, par un jugement du 6 mai 2008, le tribunal administratif de Nantes a accueilli l'exception de prescription opposée à ces demandes par la commune de Saint-Mathurin-sur-Loire et par l'Etat ; que M. et Mme B et le GAEC de la Vallée se pourvoient en cassation contre l'arrêt du 23 avril 2009 par lequel la cour administrative d'appel de Nantes a confirmé le jugement du tribunal administratif et rejeté leur demande indemnitaire tendant en appel à l'octroi d'une somme de 2 287 euros au titre de leur préjudice matériel, correspondant aux frais engagés pour la transformation de la stabulation, d'une somme de 50 000 euros, destinée à couvrir tant les condamnations dont ils ont fait l'objet que les frais exposés lors des différentes instances juridictionnelles, et d'une somme de 5 000 euros au titre de leur préjudice moral ;<br/>
<br/>
              Sur le bien-fondé de l'arrêt attaqué en tant qu'il statue sur les préjudices invoqués par M. et Mme B au titre des frais de désaffectation de la stabulation et du préjudice moral lié à l'illégalité du permis de construire délivré le 24 octobre 1987 :<br/>
<br/>
              2. Considérant qu'aux termes du premier alinéa de l'article 1er de la loi du 31 décembre 1968 relative à la prescription des créances sur l'Etat, les départements, les communes et les établissements publics : " Sont prescrites, au profit de l'Etat, des départements et des communes, sans préjudice des déchéances particulières édictées par la loi, et sous réserve des dispositions de la présente loi, toutes créances qui n'ont pas été payées dans un délai de quatre ans à partir du premier jour de l'année suivant celle au cours de laquelle les droits ont été acquis " ; que l'article 2 du même texte dispose que : " La prescription est interrompue par : (...) Tout recours formé devant une juridiction, relatif au fait générateur, à l'existence, au montant ou au paiement de la créance, quel que soit l'auteur du recours et même si la juridiction saisie est incompétente pour en connaître, et si l'administration qui aura finalement la charge du règlement n'est pas partie à l'instance (...) Un nouveau délai de quatre ans court à compter du premier jour de l'année suivant celle au cours de laquelle a lieu l'interruption. Toutefois, si l'interruption résulte d'un recours juridictionnel, le nouveau délai court à partir du premier jour de l'année suivant celle au cours de laquelle la décision est passée en force de chose jugée " ; qu'aux termes de l'article L. 480-13 du code de l'urbanisme, dans sa rédaction alors applicable : " Lorsqu'une construction a été édifiée conformément à un permis de construire, le propriétaire ne peut être condamné par un tribunal de l'ordre judiciaire du fait de la méconnaissance des règles d'urbanisme ou des servitudes d'utilité publique que si, préalablement, le permis a été annulé pour excès de pouvoir ou son illégalité a été constatée par la juridiction administrative. L'action en responsabilité civile se prescrit, en pareil cas, par cinq ans après l'achèvement des travaux " ; qu'il résulte de ces dispositions que la créance détenue, le cas échéant, par le titulaire d'un permis de construire jugé illégal par la juridiction administrative, au titre du préjudice lié à la condamnation, par une juridiction judiciaire, à démolir le bâtiment litigieux ou à indemniser les préjudices qu'il a causés, se prescrit à compter du premier jour de l'année suivant celle au cours de laquelle la décision de la juridiction judiciaire est passée en force de chose jugée ;<br/>
<br/>
              3. Considérant que, par l'arrêt attaqué, la cour a jugé que M. et Mme B avaient eu connaissance des préjudices dont ils se prévalaient au plus tard lors de la notification de la décision du 31 juillet 1996 par laquelle le Conseil d'Etat avait confirmé l'annulation du permis de construire le bâtiment litigieux et que leur éventuelle créance indemnitaire se trouvait ainsi prescrite le 31 décembre 2000 ; qu'en statuant ainsi, alors que, ainsi qu'il a été dit, la créance que les époux B estimaient détenir à raison des frais de désaffectation de la stabulation et du préjudice moral lié à l'illégalité du permis de construire délivré le 27 avril 1987 n'avait acquis un caractère certain que du fait de la décision rendue par le juge judiciaire en application de l'article L. 480-13 du code de l'urbanisme et qu'elle ne se prescrivait qu'à compter de l'année suivant celle au cours de laquelle cette décision du juge judiciaire était passée en force de chose jugée, la cour a entaché son arrêt d'erreur de droit ; que celui-ci doit, par suite, et sans qu'il soit besoin d'examiner l'autre moyen du pourvoi, être annulé en tant qu'il statue sur ces chefs de préjudice ;<br/>
<br/>
              Sur le bien-fondé de l'arrêt attaqué en tant qu'il statue sur le préjudice lié aux frais de la procédure ayant conduit à l'annulation par le juge administratif du permis de construire délivré le 24 octobre 1987 :<br/>
<br/>
              4. Considérant, d'une part, qu'il ressort des pièces du dossier soumis aux juges du fond que le maire de la commune de Saint-Mathurin-sur-Loire a signé, conjointement avec l'avocat de la commune, le mémoire en défense, enregistré au greffe du tribunal administratif de Nantes le 4 janvier 2007, par lequel la commune a soulevé l'exception de prescription quadriennale de la créance dont se prévalaient M. et Mme B ; que, par suite, la cour n'a pas commis d'erreur de droit en jugeant que la prescription avait été régulièrement opposée ;<br/>
<br/>
              5. Considérant, d'autre part, que la cour n'a pas commis d'erreur de droit en jugeant que M. et Mme B avaient eu connaissance du préjudice résultant des frais de la procédure ayant conduit à l'annulation du permis de construire délivré le 24 octobre 1987 au plus tard lors de la notification de la décision du 31 juillet 1996 par laquelle le Conseil d'Etat a confirmé l'annulation de ce permis et qu'ainsi, la prescription quadriennale ayant couru à compter du 1er janvier 1997, elle se trouvait acquise à la date du 8 juin 2006 à laquelle M. et Mme B avaient présenté leur demande d'indemnisation ;<br/>
<br/>
              Sur le bien-fondé de l'arrêt attaqué en tant qu'il statue sur les préjudices liés à la condamnation à indemniser le voisin de M. et Mme B et aux frais de justice exposés devant les juridictions judiciaires :<br/>
<br/>
              6. Considérant qu'en jugeant que le préjudice lié à la condamnation à verser des dommages et intérêts à leur voisin prononcée par la cour d'appel d'Angers trouvait son origine dans le comportement fautif de M. et Mme B, qui avaient continué à exploiter leur stabulation en méconnaissance des prescriptions sanitaires de l'arrêté préfectoral du 17 septembre 1987 accordant une dérogation aux règles de distance prescrites par le règlement sanitaire départemental, la cour n'a pas dénaturé l'arrêt de la cour d'appel d'Angers du 17 avril 2001 ;<br/>
<br/>
              7. Considérant qu'il résulte de tout ce qui précède que les requérants sont fondés à demander l'annulation de l'arrêt qu'ils attaquent en tant seulement qu'il statue sur les préjudices invoqués par M. et Mme B au titre des frais de désaffectation de la stabulation et au titre du préjudice moral lié à l'illégalité du permis de construire délivré le 24 octobre 1987 ;<br/>
<br/>
              8. Considérant que, dans les circonstances de l'espèce, il y a lieu de régler l'affaire au fond dans cette mesure, en application de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              9. Considérant, d'une part, que M. et Mme B demandent réparation du préjudice matériel qu'ils estiment avoir subi du fait des frais engagés pour la transformation de leur stabulation en bâtiment de stockage ainsi que du préjudice moral qui résulterait de l'illégalité du permis de construire délivré le 24 octobre 1987 ; que si le fait générateur de la créance qu'ils sont susceptibles de détenir à ce titre trouve sa source dans l'illégalité du permis de construire, cette créance, à la supposer établie, ne peut avoir acquis un caractère certain que du fait de l'intervention de l'arrêt de la cour d'appel d'Angers, signifié le 3 juillet 2001, leur ordonnant de détruire la stabulation litigieuse, sans qu'y fasse obstacle la circonstance qu'en l'espèce, les époux B ont pu, en définitive, se borner à transformer la stabulation litigieuse ; que la prescription a couru à compter du 1er janvier 2002 et non, comme le soutiennent les requérants en se prévalant de la date à laquelle l'astreinte prononcée par la cour d'appel a commencé à courir, à compter du 1er janvier 2003 ; qu'elle était ainsi acquise, en application de la loi du 31 décembre 1968 précitée, le 8 juin 2006, date de la demande d'indemnisation de M. et Mme B ;<br/>
<br/>
              10. Considérant, d'autre part, que, ainsi qu'il a été dit ci-dessus, la commune a régulièrement opposé la prescription quadriennale à la demande d'indemnisation formée par M. et Mme B, avant que la juridiction saisie du litige au premier degré ne se soit prononcée sur le fond ;<br/>
<br/>
              11. Considérant qu'il suit de là, sans qu'il soit besoin  de statuer sur la fin de non-recevoir soulevée par la commune de Saint-Mathurin-sur-Loire, que les requérants ne sont pas fondés à soutenir que c'est à tort que le tribunal administratif de Nantes a rejeté leur demande de réparation des préjudices invoqués au titre des frais de désaffectation de la stabulation et du préjudice moral lié à l'illégalité du permis de construire délivré le 24 octobre 1987 ;<br/>
<br/>
              12. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de la commune de Saint-Mathurin-sur-Loire et de l'Etat, qui ne sont pas, dans la présente instance, les parties perdantes ; que, dans les circonstances de l'espèce, il n'y a pas lieu de faire droit aux conclusions présentées, à ce titre, par la commune de Saint-Mathurin-sur-Loire ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Nantes du 23 avril 2009 est annulé en tant qu'il statue sur les préjudices invoqués par M. et Mme B au titre des frais de désaffectation de la stabulation et du préjudice moral lié à l'illégalité du permis de construire délivré le 24 octobre 1987.<br/>
Article 2 : Les conclusions de la requête d'appel présentée par M. et Mme B et par le GAEC de la Vallée relatives aux préjudices mentionnés à l'article 1er sont rejetées.<br/>
Article 3 : Le surplus des conclusions du pourvoi de M. et Mme B et du GAEC de la Vallée est rejeté.<br/>
Article 4 : Les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative par la commune de Saint-Mathurin-sur-Loire sont rejetées.<br/>
Article 5 : La présente décision sera notifiée à M. et Mme Alain B, au GAEC de la Vallée et à la commune de Saint-Mathurin-sur-Loire.<br/>
Copie en sera transmise pour information à la ministre de l'égalité des territoires et du logement.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">18-04-02-04 COMPTABILITÉ PUBLIQUE ET BUDGET. DETTES DES COLLECTIVITÉS PUBLIQUES - PRESCRIPTION QUADRIENNALE. RÉGIME DE LA LOI DU 31 DÉCEMBRE 1968. POINT DE DÉPART DU DÉLAI. - TITULAIRE D'UN PERMIS DE CONSTRUIRE JUGÉ ILLÉGAL PAR LA JURIDICTION ADMINISTRATIVE - PRÉJUDICE NÉ DE LA CONDAMNATION PAR LE JUGE JUDICIAIRE À DÉMOLIR LE BÂTIMENT OU INDEMNISER LES PRÉJUDICES CAUSÉS - POINT DE DÉPART DE LA CRÉANCE DÉTENUE SUR L'ADMINISTRATION - PREMIER JOUR DE L'ANNÉE SUIVANT CELLE AU COURS DE LAQUELLE LA DÉCISION DE LA JURIDICTION JUDICIAIRE EST PASSÉE EN FORCE DE CHOSE JUGÉE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">60-02-05-01 RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. RESPONSABILITÉ EN RAISON DES DIFFÉRENTES ACTIVITÉS DES SERVICES PUBLICS. SERVICES DE L'URBANISME. PERMIS DE CONSTRUIRE. - TITULAIRE D'UN PERMIS DE CONSTRUIRE JUGÉ ILLÉGAL PAR LA JURIDICTION ADMINISTRATIVE - PRÉJUDICE NÉ DE LA CONDAMNATION PAR LE JUGE JUDICIAIRE À DÉMOLIR LE BÂTIMENT OU INDEMNISER LES PRÉJUDICES CAUSÉS - POINT DE DÉPART DE LA CRÉANCE DÉTENUE SUR L'ADMINISTRATION - PREMIER JOUR DE L'ANNÉE SUIVANT CELLE AU COURS DE LAQUELLE LA DÉCISION DE LA JURIDICTION JUDICIAIRE EST PASSÉE EN FORCE DE CHOSE JUGÉE.
</SCT>
<ANA ID="9A"> 18-04-02-04 La créance détenue, le cas échéant, par le titulaire d'un permis de construire jugé illégal par la juridiction administrative, au titre du préjudice lié à la condamnation, par une juridiction judiciaire, à démolir le bâtiment litigieux ou à indemniser les préjudices qu'il a causés, se prescrit à compter du premier jour de l'année suivant celle au cours de laquelle la décision de la juridiction judiciaire est passée en force de chose jugée.</ANA>
<ANA ID="9B"> 60-02-05-01 La créance détenue, le cas échéant, par le titulaire d'un permis de construire jugé illégal par la juridiction administrative, au titre du préjudice lié à la condamnation, par une juridiction judiciaire, à démolir le bâtiment litigieux ou à indemniser les préjudices qu'il a causés, se prescrit à compter du premier jour de l'année suivant celle au cours de laquelle la décision de la juridiction judiciaire est passée en force de chose jugée.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
