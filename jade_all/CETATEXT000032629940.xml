<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032629940</ID>
<ANCIEN_ID>JG_L_2014_12_000000385030</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/62/99/CETATEXT000032629940.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère / 6ème SSR, 23/12/2014, 385030, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-12-23</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>385030</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère / 6ème SSR</FORMATION>
<TYPE_REC>Autres</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR>M. Denis Rapone</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Alexandre Lallet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:385030.20141223</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Le syndicat UNSA des cadres et agents de direction du régime social des indépendants (UNSA RSI CAD) a demandé à la cour administrative d'appel de Paris d'annuler pour excès de pouvoir l'arrêté du ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social du 24 décembre 2013 fixant la liste des organisations syndicales reconnues représentatives dans la convention collective du personnel de direction du régime social des indépendants.<br/>
<br/>
              Par une ordonnance n° 14PA00964 du 1er octobre 2014, enregistrée le 7 octobre 2014 au secrétariat du contentieux du Conseil d'Etat, le président de la 3ème chambre de la cour administrative d'appel de Paris, avant qu'il soit statué sur la requête du syndicat UNSA RSI CAD, a décidé, par application des dispositions de l'article 23-2 de l'ordonnance n° 58-1067 du 7 novembre 1958, de transmettre au Conseil d'Etat la question de la conformité aux droits et libertés garantis par la Constitution des dispositions de l'article L. 2122-5 du code du travail.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier.<br/>
<br/>
              Vu :<br/>
              - la Constitution, notamment son Préambule, son article 34 et son article 61-1 ;<br/>
              - l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
              - l'article L. 2122-5 du code du travail ;<br/>
              - le code de justice administrative.<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Denis Rapone, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Alexandre Lallet, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Piwnica, Molinié, avocat du syndicat UNSA RSI CAD.<br/>
<br/>
              Vu la note en délibéré, enregistrée le 16 décembre 2014, présentée par le syndicat UNSA RSI CAD.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il résulte des dispositions de l'article 23-4 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel que, lorsqu'une juridiction relevant du Conseil d'Etat a transmis à ce dernier, en application de l'article 23-2 de cette même ordonnance, la question de la conformité à la Constitution d'une disposition législative, le Conseil constitutionnel est saisi de cette question de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article L. 2122-5 du code du travail : " Dans les branches professionnelles, sont représentatives les organisations syndicales qui : / 1° Satisfont aux critères de l'article L. 2121-1 ; / 2° Disposent d'une implantation territoriale équilibrée au sein de la branche ; / 3° Ont recueilli au moins 8 % des suffrages exprimés résultant de l'addition au niveau de la branche, d'une part, des suffrages exprimés au premier tour des dernières élections des titulaires aux comités d'entreprise ou de la délégation unique du personnel ou, à défaut, des délégués du personnel, quel que soit le nombre de votants, et, d'autre part, des suffrages exprimés au scrutin concernant les entreprises de moins de onze salariés dans les conditions prévues aux articles L. 2122-10-1 et suivants. La mesure de l'audience s'effectue tous les quatre ans " ;<br/>
<br/>
              3 Considérant que le syndicat UNSA des cadres et agents de direction du régime social des indépendants (UNSA RSI CAD) soutient que, faute de prévoir un dispositif spécifique pour déterminer la représentativité des organisations syndicales représentant les agents de direction des organismes de sécurité sociale, alors que leurs conditions de travail font l'objet d'une convention collective spéciale en vertu de l'article L. 123-2 du code de la sécurité sociale et qu'ils ne peuvent, pour une part importante d'entre eux, participer aux élections des institutions représentatives du personnel, les dispositions de l'article L. 2122-5 du code du travail portent atteinte aux principes de liberté syndicale, de participation des travailleurs à la détermination collective de leurs conditions de travail et d'égalité, respectivement garantis par les sixième et huitième alinéas du Préambule de la Constitution de 1946 et par l'article 6 de la Déclaration des droits de l'homme et du citoyen ;<br/>
<br/>
              4. Considérant, en premier lieu, qu'aux termes de l'article 6 de la Déclaration des droits de l'homme et du citoyen : " La Loi est l'expression de la volonté générale. Tous les Citoyens ont droit de concourir personnellement, ou par leurs Représentants, à sa formation. Elle doit être la même pour tous, soit qu'elle protège, soit qu'elle punisse (...) " ; que si, en règle générale, le principe d'égalité impose de traiter de la même façon des personnes qui se trouvent dans la même situation, il n'en résulte pas qu'il oblige à traiter différemment des personnes se trouvant dans des situations différentes ; qu'ainsi, il ne saurait, en tout état de cause, être fait grief à l'article L. 2122-5 du code du travail de méconnaître ce principe en ne prévoyant pas un dispositif spécifique pour déterminer la représentativité des organisations syndicales représentant les agents de direction des organismes de sécurité sociale, au motif que leur situation diffère de celle des autres agents de ces organismes en ce qu'ils sont susceptibles de se trouver, à raison des délégations d'autorité qui peuvent leur être consenties par l'employeur ou de la représentation de celui-ci devant les institutions représentatives du personnel, exclus de l'électorat et de l'éligibilité aux élections professionnelles ;<br/>
<br/>
              5. Considérant, en deuxième lieu, qu'aux termes du sixième alinéa du Préambule de la Constitution de 1946 : " Tout homme peut défendre ses droits et ses intérêts par l'action syndicale et adhérer au syndicat de son choix " ; qu'il ne saurait être fait grief à l'article L. 2122-5 du code du travail, qui  fixe un seuil d'audience au titre des critères de représentativité syndicale au niveau de la branche professionnelle, de méconnaître la liberté d'adhérer au syndicat de son choix ou de porter atteinte à la liberté d'action syndicale ;<br/>
<br/>
              6. Considérant, en dernier lieu, qu'aux termes du huitième alinéa du même Préambule : " Tout travailleur participe, par l'intermédiaire de ses délégués, à la détermination collective des conditions de travail ainsi qu'à la gestion des entreprises " ; que les dispositions de l'article L. 2122-5 du code du travail, qui tendent à assurer, dans les branches professionnelles, que la négociation collective soit conduite par des organisations syndicales dont la représentativité est notamment fondée sur une mesure de leur audience calculée en fonction des résultats obtenus aux élections professionnelles et qui fixent, afin d'éviter la dispersion de la représentation syndicale, le seuil de cette audience à 8 % des suffrages exprimés au premier tour des dernières élections professionnelles, ne méconnaissent pas, par elles-mêmes, le principe énoncé au huitième alinéa du Préambule de 1946 ; que si le syndicat requérant fait valoir que la mesure d'audience prévue par les dispositions contestées ne permet pas à un nombre important d'agents de direction des organismes de sécurité sociale de participer par l'intermédiaire de délégués élus à la détermination collective de leurs conditions de travail, alors que celles-ci font l'objet d'une convention collective spéciale en vertu de l'article L. 123-2 du code de la sécurité sociale, une telle situation trouve d'abord sa source dans les dispositions des articles L. 2314-16 et L. 2324-15 du code du travail telles qu'interprétées par l'autorité judiciaire et dans celles de l'article L. 123-2 du code de la sécurité sociale ; que le syndicat requérant, qui ne soulève pas la question de la conformité aux droits et libertés que la Constitution garantit de ces dispositions, ne peut utilement s'en prévaloir à l'appui de sa question prioritaire dirigée contre les seules dispositions de l'article L. 2122-5 du code du travail ; <br/>
<br/>
              7. Considérant qu'il résulte de tout ce qui précède que la question soulevée, qui n'est pas nouvelle, ne présente pas un caractère sérieux ; que, dès lors, il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité invoquée ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité transmise par le président de la 3ème chambre de la cour administrative d'appel de Paris.<br/>
Article 2 : La présente décision sera notifiée au syndicat UNSA des cadres et agents de direction du régime social des indépendants (UNSA RSI CAD), à la fédération CFDT Protection Sociale Travail Emploi (CFDT-PSTE) et au ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social.<br/>
Copie en sera adressée au Conseil constitutionnel, au Premier ministre ainsi qu'à la cour administrative d'appel de Paris.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
