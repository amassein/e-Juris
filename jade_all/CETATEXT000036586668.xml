<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036586668</ID>
<ANCIEN_ID>JG_L_2018_02_000000399838</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/58/66/CETATEXT000036586668.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème et 1ère chambres réunies, 07/02/2018, 399838</TITRE>
<DATE_DEC>2018-02-07</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>399838</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème et 1ère chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GATINEAU, FATTACCINI ; SCP THOUVENIN, COUDRAY, GREVY</AVOCATS>
<RAPPORTEUR>Mme Sara-Lou Gerber</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Sophie-Justine Lieber</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:399838.20180207</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B...A...a demandé au tribunal administratif de Lyon d'annuler pour excès de pouvoir la décision implicite d'homologation du document unilatéral fixant le contenu du plan de sauvegarde de l'emploi de la société Girus, née le 4 mai 2015 du silence gardé par le directeur régional des entreprises, de la concurrence, de la consommation, du travail et de l'emploi de Rhône-Alpes sur la demande de cette société. Par un jugement n° 1506558 du 20 octobre 2015, le tribunal administratif a rejeté sa demande. <br/>
<br/>
              Par un arrêt n° 15LY04094 du 15 mars 2016, la cour administrative d'appel de Lyon a, sur appel de M.A..., annulé ce jugement et la décision implicite du 4 mai 2015.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 17 mai et 17 juin 2016 au secrétariat du contentieux du Conseil d'Etat, la société Girus, la société Bauland, Carboni, Martinez et associés, agissant en qualité d'administrateur judiciaire de la société Girus et la société MDP mandataires judiciaires, agissant en qualité de mandataire judiciaire de la société Girus, demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de M.A... ;<br/>
<br/>
              3°) de mettre à la charge de M. A...une somme de 4 500 euros pour chacune des requérantes au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code du travail ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Sara-Lou Gerber, auditeur,  <br/>
<br/>
              - les conclusions de Mme Sophie-Justine Lieber, rapporteur public .<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Gatineau, Fattaccini, avocat de la société Girus et autres et à la SCP Thouvenin, Coudray, Grevy, avocat de M. A...;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que la société Girus, qui exerçait une activité de bureau d'études et d'ingénierie, a été placée, le 29 janvier 2015, en procédure de sauvegarde par le tribunal de commerce de Lyon pour une durée de dix mois ; qu'elle a alors soumis à l'administration une demande d'homologation de son document fixant, de manière unilatérale, le contenu d'un plan de sauvegarde de l'emploi de trente-huit salariés sur les cent soixante dix-sept qu'elle employait ; que le silence gardé pendant vingt-et-un jours sur le dossier complet accompagnant cette demande par le directeur régional des entreprises, de la concurrence, de la consommation, du travail et de l'emploi de Rhône-Alpes a fait naître, le 4 mai 2015, une décision implicite d'homologation ; que, par un jugement du 20 octobre 2015, le tribunal administratif de Lyon a rejeté la demande d'annulation de cette décision présentée par M.A..., salarié de la société Girus ; que la société Girus, la société Bauland, Carboni, Martinez et associés et la société MDP mandataires judiciaires se pourvoient en cassation contre l'arrêt du 15 mars 2016 par lequel la cour administrative d'appel de Lyon a annulé, d'une part, ce jugement et, d'autre part, la décision implicite d'homologation du 4 mai 2015 ; <br/>
<br/>
              2. Considérant que, l'arrêt attaqué ne se prononçant pas sur la recevabilité de la demande de première instance de M. A...en l'absence de moyen soulevé en appel sur ce point, les requérantes ne sont pas fondées à soutenir que la cour, en jugeant implicitement que M. A... avait intérêt pour agir en première instance, a déduit cet intérêt de sa seule qualité de salarié et commis, par suite, une erreur de droit ; qu'au demeurant, il ressort des pièces du dossier soumis aux juges du fond que M.A..., qui a été licencié à la suite de l'entrée en vigueur du plan homologué par la décision litigieuse, justifiait d'un intérêt lui donnant qualité pour agir contre cette décision ;<br/>
<br/>
              3. Considérant que l'article L. 1235-7-1 du code du travail dispose que le recours contentieux dirigé contre une décision d'homologation d'un plan de sauvegarde de l'emploi " (...) est présenté dans un délai de deux mois par l'employeur à compter de la notification de la décision de validation ou d'homologation, et par les organisations syndicales et les salariés à compter de la date à laquelle cette décision a été portée à leur connaissance conformément à l'article L. 1233-57-4 (...) " ; qu'aux termes de cet article L. 1233-57-4  du même code : " (...) Le silence gardé par l'autorité administrative pendant les délais prévus au premier alinéa vaut décision d'acceptation de validation ou d'homologation. Dans ce cas, l'employeur transmet une copie de la demande de validation ou d'homologation, accompagnée de son accusé de réception par l'administration, au comité d'entreprise et, si elle porte sur un accord collectif, aux organisations syndicales représentatives signataires. La décision de validation ou d'homologation ou, à défaut, les documents mentionnés au troisième alinéa et les voies et délais de recours sont portés à la connaissance des salariés par voie d'affichage sur leurs lieux de travail ou par tout autre moyen permettant de conférer date certaine à cette information " ; qu'il résulte de ces dispositions que le délai de recours contre une décision implicite d'homologation d'un plan de sauvegarde de l'emploi ne court, à l'égard des salariés de l'entreprise, qu'à compter du jour où, postérieurement à la naissance de cette décision implicite, ils ont été destinataires de la demande d'homologation présentée par l'employeur et de son accusé de réception par l'administration, soit par affichage de ces documents sur leurs lieux de travail, soit par tout autre moyen permettant de donner à cette information une date certaine ; <br/>
<br/>
              4. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que si la société Girus a affiché dans les lieux de travail de ses salariés une note d'information retraçant le déroulé de la procédure d'homologation du plan de sauvegarde de l'emploi et reproduisant les dispositions des articles L. 1233-57-4 et L. 1235-7-1 du code du travail, ainsi qu'un courrier électronique de l'administration confirmant la naissance d'une décision implicite d'homologation, elle n'a, en revanche, pas procédé à l'affichage du document qu'elle avait soumis à homologation ; qu'en jugeant que cette omission d'une formalité qui s'imposait à elle en vertu des dispositions citées ci-dessus de l'article L. 1235-7-1 du code du travail avait fait obstacle à ce que le délai de recours contre la décision implicite d'homologation puisse courir à l'égard de ses salariés, la cour administrative d'appel, qui a suffisamment motivé son arrêt sur ce point, n'a pas commis d'erreur de droit ; que si, par l'effet d'une erreur de plume, l'arrêt attaqué indique que la société n'établit pas avoir procédé " à l'affichage (...) pour le comité d'entreprise ", au lieu d'écrire " à l'affichage pour les salariés ", cette circonstance n'est pas davantage de nature à l'entacher d'insuffisance de motivation ni d'erreur de droit ;<br/>
<br/>
              5. Considérant que, l'affichage de cette simple note d'information étant ainsi, quelle que soit la date à laquelle il est intervenu, insusceptible de faire courir le délai de recours contentieux, le moyen tiré de ce que l'arrêt attaqué aurait dénaturé les pièces du dossier en mentionnant que la note d'information avait été affichée " à une date incertaine au siège de la société et au plus tôt le 21 mai 2015 pour certains établissements de la société ", qui conteste une considération dépourvue de toute incidence sur le raisonnement tenu par la cour administrative d'appel, est inopérant ;<br/>
<br/>
              6. Considérant qu'aux termes de l'article L. 1233-24-2 du code du travail, dans sa rédaction applicable à l'espèce : " L'accord collectif mentionné à l'article L. 1233-24-1 porte sur le contenu du plan de sauvegarde de l'emploi mentionné aux articles L. 1233-61 à L. 1233 63. / Il peut également porter sur : / 1° Les modalités d'information et de consultation du comité d'entreprise (...) ; / 2° La pondération et le périmètre d'application des critères d'ordre des licenciements mentionnés à l'article L. 1233-5 ; / 3° Le calendrier des licenciements ; / 4° Le nombre de suppressions d'emploi et les catégories professionnelles concernées ; / 5° Les modalités de mise en oeuvre des mesures de formation, d'adaptation et de reclassement prévues aux articles L. 1233-4 et L. 1233-4-1 " ; que l'article L. 1233-57-3 du même code prévoit qu'en l'absence d'accord collectif, ou en cas d'accord ne portant pas sur l'ensemble des points mentionnés aux 1° à 5° : " (...) l'autorité administrative homologue le document élaboré par l'employeur mentionné à l'article L. 1233-24-4, après avoir vérifié la conformité de son contenu aux dispositions législatives et aux stipulations conventionnelles relatives aux éléments mentionnés aux 1° à 5° de l'article L. 1233-24-2 (...) " ; qu'en vertu de ces dispositions, il appartient à l'administration, lorsqu'elle est saisie d'une demande d'homologation d'un document qui fixe les catégories professionnelles mentionnées au 4° de l'article L. 1233-24-2 cité ci-dessus, de s'assurer, au vu de l'ensemble des éléments qui lui sont soumis, notamment des échanges avec les représentants du personnel au cours de la procédure d'information et de consultation ainsi que des justifications qu'il appartient à l'employeur de fournir, que ces catégories regroupent, en tenant compte des acquis de l'expérience professionnelle qui excèdent l'obligation d'adaptation qui incombe à l'employeur, l'ensemble des salariés qui exercent, au sein de l'entreprise, des fonctions de même nature supposant une formation professionnelle commune ; qu'au terme de cet examen, l'administration refuse l'homologation demandée s'il apparaît que les catégories professionnelles concernées par le licenciement ont été déterminées par l'employeur en se fondant sur des considérations, telles que l'organisation de l'entreprise ou l'ancienneté des intéressés, qui sont étrangères à celles qui permettent de regrouper, compte tenu des acquis de l'expérience professionnelle, les salariés par fonctions de même nature supposant une formation professionnelle commune, ou s'il apparaît qu'une ou plusieurs catégories ont été définies dans le but de permettre le licenciement de certains salariés pour un motif inhérent à leur personne ou en raison de leur affectation sur un emploi ou dans un service dont la suppression est recherchée ;<br/>
<br/>
              7. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que l'organisation de la société Girus est structurée par projets, regroupés en cinq " domaines " d'activités correspondant aux différents marchés sur lesquels l'entreprise est présente, à savoir les domaines du " bâtiment ", de " l'énergie ", du " conseil-environnement ", de la " réalisation d'unités de traitement de déchets " et de " l'eau et environnement urbain ", auxquels s'ajoute un domaine " fonctions support " ; qu'en estimant, au vu des échanges entre l'employeur et les représentants du personnel, et compte tenu, d'une part, du nombre très élevé de catégories professionnelles auquel conduisaient les considérations retenues par l'employeur pour en fixer la liste, notamment dans le domaine du " bâtiment " et, d'autre part, de ce que les catégories professionnelles étaient systématiquement rattachées aux domaines d'activité de l'entreprise et définies en leur sein, que l'employeur s'était fondé sur des considérations tirées de l'organisation de l'entreprise, la cour a porté sur les faits de l'espèce une appréciation souveraine qui n'est pas entachée de dénaturation ; qu'en en déduisant que la détermination des catégories professionnelles ne reposait pas sur des considérations propres à regrouper les salariés par fonctions de même nature supposant une formation professionnelle commune et faisait, par suite, obstacle à l'homologation demandée, elle n'a pas commis d'erreur de droit ;<br/>
<br/>
              8. Considérant qu'il résulte de tout ce qui précède que la société Girus et autres ne sont pas fondées à demander l'annulation de l'arrêt qu'elles attaquent ;<br/>
<br/>
              9. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de M.A..., qui n'est pas la partie perdante dans la présente instance ; qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la société Girus, de la société Bauland, Carboni, Martinez et associés et de la société MDP mandataires judiciaires la somme de 1 000 euros chacune à verser à M. A...au titre de ces mêmes dispositions ;<br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er : Le pourvoi de la société Girus et autres est rejeté.<br/>
Article 2 : La société Girus, la société Bauland, Carboni, Martinez et associés et la société MDP mandataires judiciaires, agissant en qualité de mandataire de la société Girus verseront, chacune, une somme de 1 000 Euros à M. A...au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée à la société Girus, à la société Bauland, Carboni, Martinez et associés, à la société MDP mandataires judiciaires et à M. A....<br/>
Copie en sera adressée à la ministre du travail. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-07-01-04-01-01 PROCÉDURE. POUVOIRS ET DEVOIRS DU JUGE. QUESTIONS GÉNÉRALES. MOYENS. MOYENS D'ORDRE PUBLIC À SOULEVER D'OFFICE. ABSENCE. - MOYEN DE CASSATION - MOYEN TIRÉ DE CE QUE LE JUGE DU FOND AURAIT OMIS DE SE PRONONCER SUR LE CARACTÈRE SUFFISANT D'UN PSE (SOL. IMPL.) [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-08-02-02-01-03 PROCÉDURE. VOIES DE RECOURS. CASSATION. CONTRÔLE DU JUGE DE CASSATION. BIEN-FONDÉ. APPRÉCIATION SOUVERAINE DES JUGES DU FOND. - DÉFINITION DES CATÉGORIES PROFESSIONNELLES CONCERNÉES PAR LES SUPPRESSIONS D'EMPLOI D'UN DOCUMENT UNILATÉRAL FIXANT LE CONTENU D'UN PSE SOUMIS À HOMOLOGATION.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">66-07 TRAVAIL ET EMPLOI. LICENCIEMENTS. - HOMOLOGATION D'UN DOCUMENT UNILATÉRAL FIXANT LE CONTENU D'UN PSE - 1) POINT DE DÉPART DU DÉLAI DE RECOURS À L'ÉGARD DES SALARIÉS DE L'ENTREPRISE EN CAS DE DÉCISION IMPLICITE - DATE À LAQUELLE ILS ONT ÉTÉ, POSTÉRIEUREMENT À LA NAISSANCE DE LA DÉCISION IMPLICITE, DESTINATAIRES DE LA DEMANDE D'HOMOLOGATION ET DE SON ACCUSÉ DE RÉCEPTION - 2) CONTRÔLE DU JUGE DE CASSATION - A) OMISSION DU JUGE DU FOND DE SE PRONONCER SUR LE CARACTÈRE SUFFISANT DU PLAN [RJ1] - MOYEN DE CASSATION À SOULEVER D'OFFICE - ABSENCE (SOL. IMPL.) - B) DÉFINITION DES CATÉGORIES PROFESSIONNELLES CONCERNÉES PAR LES SUPPRESSIONS D'EMPLOI - APPRÉCIATION SOUVERAINE DES JUGES DU FOND, SOUS RÉSERVE DE LA DÉNATURATION ET DE L'ERREUR DE DROIT.
</SCT>
<ANA ID="9A"> 54-07-01-04-01-01 Le moyen de cassation tiré de ce que le juge du fond aurait omis de se prononcer sur le caractère suffisant d'un plan de sauvegarde de l'emploi (PSE) n'est pas un moyen d'ordre public à soulever d'office.</ANA>
<ANA ID="9B"> 54-08-02-02-01-03 Le juge de cassation laisse à l'appréciation souveraine des juges du fond le contrôle de la définition des catégories professionnelles concernées par les suppressions d'emploi du document unilatéral de l'employeur fixant le contenu d'un plan de sauvegarde de l'emploi (PSE) soumis à homologation, sous réserve de la dénaturation et de l'erreur de droit.</ANA>
<ANA ID="9C"> 66-07 1) Il résulte des articles L. 1235-7-1 et L. 1233-57-4 du code du travail que le délai de recours contre une décision implicite d'homologation d'un plan de sauvegarde de l'emploi (PSE) ne court, à l'égard des salariés de l'entreprise, qu'à compter du jour où, postérieurement à la naissance de cette décision implicite, ils ont été destinataires de la demande d'homologation présentée par l'employeur et de son accusé de réception par l'administration, soit par affichage de ces documents sur leurs lieux de travail, soit par tout autre moyen permettant de donner à cette information une date certaine....  ,,2) a) Le moyen de cassation tiré de ce que le juge du fond aurait omis de se prononcer sur le caractère suffisant du PSE n'est pas un moyen d'ordre public à soulever d'office....  ,,b) Le juge de cassation laisse à l'appréciation souveraine des juges du fond le contrôle de la définition des catégories professionnelles concernées par les suppressions d'emploi du document unilatéral de l'employeur soumis à homologation, sous réserve de la dénaturation et de l'erreur de droit.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 15 mars 2017, Ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social c/ Syndicat CGT de la société Bosal Le Rapide et Me Deltour et autre, n° 387728 387881, p. 92.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
