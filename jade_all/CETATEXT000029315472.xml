<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029315472</ID>
<ANCIEN_ID>JG_L_2014_07_000000358115</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/31/54/CETATEXT000029315472.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère SSJS, 30/07/2014, 358115, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-07-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>358115</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère SSJS</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DELAPORTE, BRIARD, TRICHET ; SCP LEVIS</AVOCATS>
<RAPPORTEUR>M. Laurent Cytermann</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Maud Vialettes</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2014:358115.20140730</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
Procédure contentieuse antérieure<br/>
<br/>
              La SNC Pharmacie de la Bédugue et M. et Mme D...F..., d'une part, la SELARL Pharmacie de la Rive gauche, Mme E...C..., M. A...H...et M. I... B..., associés co-gérants de la SELARL Pharmacie de la Rive gauche, d'autre part, ont demandé au tribunal administratif de Besançon d'annuler pour excès de pouvoir l'arrêté du 27 janvier 2010 par lequel le préfet du Jura a autorisé le transfert de l'officine de pharmacie exploitée par la SELARL Pharmacie du Val d'Amour, représentée par M.G..., du 29 rue de Besançon au 198 avenue du maréchal Juin à Dole. Par un jugement n° 1000404 et 1000413 du 26 mai 2011, le tribunal administratif de Besançon a rejeté les conclusions de M. et Mme F... et annulé l'arrêté du préfet du Jura du 27 janvier 2010.<br/>
<br/>
              Par un arrêt n° 11NC01196 du 30 janvier 2012, la cour administrative d'appel de Nancy, à la demande de la SELARL Pharmacie du Val d'Amour, a annulé le jugement du tribunal administratif de Besançon et rejeté les demandes de la SNC Pharmacie de la Bédugue et de la SELARL Pharmacie de la Rive gauche.<br/>
<br/>
Procédure devant le Conseil d'Etat<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 30 mars 2012, 15 juin 2012 et 11 septembre 2013 au secrétariat du contentieux du Conseil d'Etat, la SNC Pharmacie de la Bédugue et la SELARL Pharmacie de la Rive gauche demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt n° 11NC01196 de la cour administrative d'appel de Nancy du 30 janvier 2012 ; <br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de la SELARL Pharmacie du Val d'Amour ;<br/>
<br/>
              3°) de mettre à la charge de la SELARL Pharmacie du Val d'Amour, au profit de chacune d'entre elles, la somme de 2 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Vu :<br/>
              - les autres pièces du dossier ;<br/>
              - le code de la santé publique ;<br/>
              - le code de justice administrative.<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Laurent Cytermann, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Maud Vialettes, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Lévis, avocat de la SNC Pharmacie de la Bédugue et de la SELARL Pharmacie de la Rive gauche, et à la SCP Delaporte, Briard, Trichet, avocat de la SELARL Pharmacie du Val d'Armour.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes du premier alinéa de l'article L. 5125-3 du code de la santé publique : " Les créations, les transferts et les regroupements d'officines de pharmacie doivent permettre de répondre de façon optimale aux besoins en médicaments de la population résidant dans les quartiers d'accueil de ces officines. Les transferts et les regroupements ne peuvent être accordés que s'ils n'ont pas pour effet de compromettre l'approvisionnement nécessaire en médicaments de la population résidente de la commune ou du quartier d'origine ". <br/>
<br/>
              2. Il résulte de ces dispositions que l'autorité administrative ne peut autoriser le transfert d'une officine de pharmacie qu'à la double condition, d'une part, qu'il permette de répondre de façon optimale aux besoins en médicaments de la population résidant dans le quartier d'accueil et, d'autre part, qu'il n'ait pas pour effet de compromettre l'approvisionnement de la population résidente du quartier d'origine.<br/>
<br/>
              3. Pour annuler l'arrêté du 27 janvier 2010 du préfet du Jura autorisant le transfert de la pharmacie du Val d'Amour sur le territoire de la commune de Dole, le tribunal administratif de Besançon a jugé que, eu égard aux conditions satisfaisantes d'approvisionnement de son nouveau quartier d'implantation par les officines existantes, le déplacement de cette officine ne permettrait pas d'apporter une réponse optimale aux besoins des résidents de ce quartier. Saisie de l'appel de la SELARL Pharmacie du Val d'Amour, la cour administrative d'appel de Nancy a censuré ce motif en estimant que les premiers juges auraient dû, en outre, déterminer le quartier d'origine de l'officine et évaluer l'incidence de son transfert sur les conditions de desserte des résidents de cet autre quartier. En statuant ainsi, alors que, ainsi qu'il vient d'être dit, la circonstance que la condition relative aux conditions d'approvisionnement du quartier d'accueil n'était pas remplie suffisait à faire obstacle à ce transfert, la cour a commis une erreur de droit.<br/>
<br/>
              4. Il résulte de ce qui précède que les sociétés requérantes sont fondées à demander l'annulation de l'arrêt qu'elles attaquent. Le moyen tiré de l'erreur de droit ainsi commise suffisant à entraîner cette annulation, il n'est pas nécessaire d'examiner les autres moyens du pourvoi.<br/>
<br/>
              5. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la SELARL Pharmacie du Val d'Amour le versement, d'une part, à la SNC Pharmacie de la Bédugue et, d'autre part, à la SELARL Pharmacie de la Rive gauche d'une somme de 1 500 euros chacune au titre des dispositions de l'article L. 761-1 du code de justice administrative. Les dispositions de cet article font, en revanche, obstacle à ce qu'il soit fait droit aux conclusions de la SELARL Pharmacie du Val d'Amour présentées au même titre.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Nancy du 30 janvier 2012 est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Nancy.<br/>
Article 3 : La SELARL Pharmacie du Val d'Amour versera à la SNC Pharmacie de la Bédugue et à la SELARL Pharmacie de la Rive gauche une somme de 1 500 euros chacune au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Les conclusions de la SELARL Pharmacie du Val d'Amour présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.  <br/>
Article 5 : La présente décision sera notifiée à la SNC Pharmacie de la Bédugue, à la SELARL Pharmacie de la Rive gauche, à la SELARL Pharmacie du Val d'Amour et à la ministre des affaires sociales et de la santé.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
