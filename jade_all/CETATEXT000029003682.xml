<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029003682</ID>
<ANCIEN_ID>JG_L_2014_05_000000365208</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/00/36/CETATEXT000029003682.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème et 3ème sous-sections réunies, 28/05/2014, 365208, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-05-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>365208</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème et 3ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MONOD, COLIN, STOCLET</AVOCATS>
<RAPPORTEUR>M. Mathieu Herondart</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Benoît Bohnert</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:365208.20140528</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 15 janvier et 8 avril 2013 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. B... A...demeurant... ; il demande au Conseil d'Etat :<br/>
<br/>
               1°) d'annuler l'arrêt n°11BX00561 du 18 octobre 2012 par lequel la cour administrative d'appel de Bordeaux a rejeté sa requête tendant, d'une part, à l'annulation du jugement n° 0800693 du 21 octobre 2010 du tribunal administratif de Fort-de-France rejetant sa demande en décharge des cotisations supplémentaires d'impôt sur le revenu auxquelles il a été assujetti au titre des années 2003 et 2004, d'autre part, à la décharge des impositions litigieuses ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat une somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code général des impôts et le livre des procédures fiscales ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Mathieu Herondart, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Benoît Bohnert, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Monod, Colin, Stoclet, avocat de M. A...;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'aux termes du 1 de l'article 168 du code général des impôts : " En cas de disproportion marquée entre le train de vie d'un contribuable et ses revenus, la base d'imposition à l'impôt sur le revenu est portée à une somme forfaitaire déterminée en appliquant à certains éléments de ce train de vie le barème ci-après  (...) :  1. Valeur locative cadastrale de la résidence principale, déduction faite de celle s'appliquant aux locaux ayant un caractère professionnel (...) 2. Valeur locative cadastrale des résidences secondaires, déduction faite de celle s'appliquant aux locaux ayant un caractère professionnel (...) 4. Voitures automobiles destinées au transport des personnes (...) Toutefois, la base ainsi déterminée est réduite de moitié (...) pour les voitures qui sont affectées principalement à un usage professionnel. Cette réduction est limitée à un seul véhicule (...) " ;<br/>
<br/>
              2. Considérant qu'il résulte de ces dispositions que, lorsqu'elles sont exclusivement réservées à un usage professionnel et ne peuvent ainsi être regardées comme des éléments du " train de vie " du contribuable, les voitures automobiles, même destinées au transport de personnes, ne doivent pas être prises en compte dans la détermination des bases d'imposition prévues par ces dispositions ; que, par suite, la cour administrative d'appel de Bordeaux a commis une erreur de droit en jugeant que toute voiture automobile destinée au transport de personnes doit être prise en compte dans ces bases, même si elle est exclusivement affectée à un usage professionnel ; <br/>
<br/>
              3. Considérant qu'il résulte également de ces dispositions que des locaux utilisés à des fins professionnelles par le contribuable dans sa résidence principale ou dans ses résidences secondaires ne doivent pas non plus être pris en compte dans la détermination des bases d'imposition de l'article 168 du code général des impôts, même si le contribuable dispose de locaux professionnels distincts ; qu'en se fondant sur le fait que l'administration avait exclu des bases de l'imposition forfaitaire prévue par l'article 168 du code général des impôts des locaux affectés à un usage professionnel situés en dehors de la résidence principale de M.A..., pour juger que le premier niveau de cette résidence ne pouvait être exclu à son tour de ces bases d'imposition, sans rechercher si ces locaux étaient utilisés ou non à des fins professionnelles, la cour a également commis une erreur de droit ;<br/>
<br/>
              4. Considérant qu'il résulte de ce qui précède que, sans qu'il soit besoin d'examiner l'autre moyen du pourvoi, M. A...est fondé à demander l'annulation de l'arrêt attaqué ; qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat une somme de 3 000 euros à verser à M.A..., au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              		  D E C I D E :<br/>
                                   --------------<br/>
<br/>
 Article 1er : L'arrêt de la cour administrative d'appel de Bordeaux du 18 octobre 2012 est annulé.<br/>
<br/>
 Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Bordeaux.<br/>
<br/>
 Article 3 : L'Etat versera une somme de 3 000 euros à M. A...au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
 Article 4 : La présente décision sera notifiée à M. B...A...et au ministre des finances et des comptes publics.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
