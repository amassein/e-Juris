<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027990532</ID>
<ANCIEN_ID>JG_L_2013_09_000000370444</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/99/05/CETATEXT000027990532.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 09/09/2013, 370444, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-09-09</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>370444</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP WAQUET, FARGE, HAZAN</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2013:370444.20130909</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 23 juillet 2013 au secrétariat du contentieux du Conseil d'Etat, présentée par la société R Santé, dont le siège social est situé allée Augustin Cauchy à Avrille (49240) ; la société requérante demande au juge des référés du Conseil d'Etat : <br/>
<br/>
              1°) d'ordonner, sur le fondement de l'article L. 521-1 du code de justice administrative, la suspension de l'exécution de l'arrêté du 9 janvier 2013 de la ministre des affaires sociales et de la santé et du ministre chargé du budget portant modification des modalités d'inscription et de prise en charge du dispositif médical à pression positive continue pour le traitement de l'apnée du sommeil et prestations associées au chapitre 1er du titre Ier de la liste des produits et prestations remboursables prévue à l'article L. 165-1 du code de la sécurité sociale ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat le versement de la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
              elle soutient que :<br/>
              - la condition d'urgence est remplie dès lors que les dispositions contestées relatives à la télétransmission s'appliquent à compter du 1er juin 2013 et qu'elles peuvent entraîner une baisse de 10 % de son chiffre d'affaires ;<br/>
              - il existe un doute sérieux quant à la légalité de l'arrêté contesté ;<br/>
              - l'arrêté litigieux a été pris par une autorité qui n'était pas compétente pour le faire dans la mesure où son contenu relèverait du domaine réservé à la loi par l'article 34 de la Constitution ;<br/>
              - l'arrêté contesté méconnaît le droit au respect de la vie privée dès lors que les informations personnelles des utilisateurs sont transmises au fabricant et au prestataire ; <br/>
              - l'arrêté contesté méconnaît la liberté d'aller et venir des patients ; <br/>
              - l'arrêté contesté porte atteinte au droit à la protection de la santé en ce que les dispositions contestées peuvent priver un patient de soins sans accord préalable d'un médecin ;<br/>
<br/>
<br/>
      Vu l'arrêté dont la suspension de l'exécution est demandée ;<br/>
      Vu la copie de la requête à fin d'annulation de l'arrêté contesté ;<br/>
<br/>
              Vu le mémoire en défense, enregistré le 22 août 2013, présenté par la ministre des affaires sociales et de la santé, qui conclut au rejet de la requête ; elle soutient que : <br/>
              - la condition d'urgence n'est pas remplie dès lors que, d'une part, la mise en place d'un dispositif de télétransmission automatique de l'observance a été reportée au 1er octobre 2013 par l'arrêté du 30 avril 2013 et que, d'autre part, la société R Santé n'apporte pas la preuve d'un préjudice économique entraîné par la mise en place des dispositions prévues par l'arrêté litigieux ; <br/>
              - l'arrêté contesté ne porte pas atteinte au droit à la protection de la santé dès lors que le non-remboursement du dispositif est conditionné à un défaut d'observance du traitement par le patient et à l'information préalable de celui-ci sur l'arrêt à venir du remboursement ; <br/>
              - les dispositions de l'arrêté contesté relatives à la cessation du remboursement par la sécurité sociale n'interviendront qu'en juin 2014 au plus tôt, à une date où le Conseil d'Etat se sera prononcé au fond ; <br/>
              - la ministre des affaires sociales et de la santé et le ministre délégué auprès du ministre de l'économie et des finances, chargé du budget, étaient compétents pour signer l'arrêté contesté ; <br/>
              - l'arrêté contesté ne méconnaît pas le droit au respect de la vie privée en ce que les données collectées par le fabricant ne concernent que des appareils ne permettant pas d'identifier le patient concerné et qu'aucune donnée personnelle n'est transmise au prestataire et aux caisses d'assurance maladie ; <br/>
              - l'arrêté contesté ne méconnaît pas la liberté d'aller et venir dès lors que, d'une part, aucune utilisation quotidienne permanente n'est exigée et que, d'autre part, plusieurs dérogations sont prévues par l'arrêté ; <br/>
<br/>
              Vu le mémoire en réplique, enregistré le 27 août 2013, présenté par la société R Santé qui conclut aux mêmes fins par les mêmes moyens ; elle soutient en outre que la ministre des affaires sociales et de la santé n'apporte pas la preuve de ce que l'ensemble des fabricants d'appareils à pression positive continue propose une solution de télétransmission ; que la condition d'urgence est remplie dès lors que l'arrêté porte atteinte immédiatement aux prestataires de service concernés ; que les dispositions du code de la sécurité sociale citées par la ministre des affaires sociales et de la santé pour affirmer sa compétence sont inopérantes en l'espèce ; que le mémoire en défense de la ministre des affaires sociales et de la santé n'apporte pas de réponse à la protection des données personnelles des patients ; que les dérogations à l'utilisation du dispositif sont d'ordre médical et ne préviennent pas l'atteinte à la liberté d'aller et de venir des patients ; que le non-remboursement prévu par l'arrêté contesté entraîne une rupture du principe d'égalité devant l'accès aux soins ; <br/>
<br/>
              Vu le nouveau mémoire, enregistré le 2 septembre 2013, présenté par la société R Santé ;<br/>
<br/>
              Vu les observations complémentaires, enregistrées le 5 septembre 2013, présentées par la ministre des affaires sociales et de la santé, <br/>
<br/>
              Vu les pièces du dossier desquelles il résulte que la requête a été communiquée au ministre de l'économie et des finances et à la Haute autorité de santé, qui n'ont pas produit d'observations ; <br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, la société R Santé et, d'autre part, la ministre des affaires sociales et de la santé, ainsi que le ministre de l'économie et des finances et la Haute autorité de santé ; <br/>
<br/>
              Vu le procès-verbal de l'audience publique du 2 septembre 2013 à 10 heures au cours de laquelle ont été entendus : <br/>
<br/>
              - Me Claire Waquet, avocat au Conseil d'Etat et à la Cour de cassation, avocat de la société R Santé ;<br/>
<br/>
              - les représentants de la ministre des affaires sociales et de la santé ;<br/>
<br/>
              et à l'issue de laquelle le juge des référés a clôturé puis rouvert l'instruction jusqu'au 9 septembre à 11 heures ; <br/>
<br/>
              Vu les nouveaux mémoires de productions, enregistrés les 4, 6 et 9 septembre 2013, présentés par la société R Santé ;<br/>
<br/>
              Vu les observations complémentaires, enregistrées le 5 septembre 2013, présentées par la ministre de la santé et des affaires sociales ;<br/>
      Vu les autres pièces du dossier ; <br/>
<br/>
      Vu la Constitution ; <br/>
<br/>
      Vu le code civil ; <br/>
<br/>
      Vu le code de la santé publique ; <br/>
<br/>
      Vu le code de la sécurité sociale ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision " ;<br/>
<br/>
              2. Considérant que l'urgence justifie que soit prononcée la suspension d'un acte administratif lorsque l'exécution de celui-ci porte atteinte, de manière suffisamment grave et immédiate, à un intérêt public, à la situation du requérant ou aux intérêts qu'il entend défendre ; qu'il appartient au juge des référés d'apprécier concrètement, compte tenu des justifications fournies par le requérant, si les effets de l'acte litigieux sont de nature à caractériser une urgence justifiant que, sans attendre le jugement de la requête au fond, l'exécution de la décision soit suspendue ; que l'urgence doit être appréciée objectivement et compte tenu de l'ensemble des circonstances de l'affaire, à la date à laquelle le juge des référés se prononce ;<br/>
<br/>
              3. Considérant que la société requérante est une société prestataire de services qui met à la disposition des patients des appareils dits à pression permanente continue (PPC) conformément au traitement prescrit par leur médecin afin de traiter le syndrome d'apnée du sommeil dont ils sont atteints ainsi que de veiller à la bonne utilisation et au bon fonctionnement de ces matériels ; que cette prestation donne lieu au versement d'un forfait hebdomadaire à la charge de l'assurance maladie, fixé avant l'arrêté contesté à 20 euros ; que la société requérante soutient que l'arrêté litigieux portera une atteinte grave et immédiate à sa situation financière, qu'elle évalue à une baisse de l'ordre de 10 % de son chiffre d'affaires ; qu'il résulte toutefois de l'instruction que, si l'arrêté contesté prévoit que chaque appareil PPC devra être équipé d'un transmetteur pour le mettre en conformité avec les exigences qu'il prévoit représentant un coût hebdomadaire de 2 euros par patient équipé d'un tel appareil, il porte également le forfait hebdomadaire versé aux sociétés prestataires par patient de 20 à 21 euros ; qu'il en résulte, compte tenu du nombre de patients de la société requérante bénéficiant d'un tel appareil, que le surcoût causé par l'arrêté représentera une charge d'environ 5 % de son chiffre d'affaires ; qu'ainsi, l'arrêté contesté n'est pas susceptible de causer un préjudice grave et immédiat à la situation financière de la société requérante ;<br/>
<br/>
              4. Considérant qu'il résulte de ce qui précède que, en l'état de l'instruction et sans qu'il soit besoin de statuer sur l'existence d'un doute sérieux quant à la légalité de l'arrêté contesté, la condition d'urgence posée par l'article L. 521-1 du code de justice administrative ne peut être regardée, en l'espèce, comme remplie ; qu'il y a lieu, par suite, de rejeter la requête de la société R Santé, y compris ses conclusions tendant à l'application de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de la société R Santé est rejetée. <br/>
<br/>
Article 2 : L'ordonnance sera notifiée à la société R Santé, à la ministre des affaires sociales et de la santé, au ministre de l'économie et des finances et à la Haute autorité de santé. <br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
