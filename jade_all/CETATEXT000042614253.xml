<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042614253</ID>
<ANCIEN_ID>JG_L_2020_12_000000445999</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/61/42/CETATEXT000042614253.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 03/12/2020, 445999, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-12-03</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>445999</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2020:445999.20201203</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu les procédures suivantes :<br/>
<br/>
              I. Sous le n° 445999, par une ordonnance n° 2005501 du 5 novembre 2020, enregistrée au secrétariat du contentieux le 6 novembre 2020, la présidente du tribunal administratif de Toulouse a transmis au Conseil d'Etat, en application de l'article R. 351-2 du code de justice administrative, la requête que M. B... D... avait présentée à ce tribunal.<br/>
<br/>
              Par cette requête, enregistrée le 1er novembre 2020 au greffe du tribunal administratif de Toulouse, M. D... demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, d'ordonner la suspension de l'exécution de la décision d'obligation du port du masque pour les enfants âgés de 6 à 10 ans.<br/>
<br/>
<br/>
              Il soutient que :<br/>
              - la condition d'urgence est remplie dès lors que le port du masque comporte un certain nombre d'effets indésirables sur la santé des personnes, lesquels sont listés par l'Organisation mondiale de la santé ;<br/>
              - il est porté une atteinte grave et manifestement illégale à l'intérêt supérieur de l'enfant garanti par les stipulations de l'article 3-1 de la convention internationale des droits de l'enfant, par les dispositions de l'article L. 112-4 du code de l'action sociale et des familles et par celles de l'article 371-1 du code civil.<br/>
<br/>
<br/>
              II. Sous le n° 446001, par une ordonnance n° 2005530 du 5 novembre 2020, enregistrée au secrétariat du contentieux le 6 novembre 2020, la présidente du tribunal administratif de Toulouse a transmis au Conseil d'Etat, en application de l'article R. 351-2 du code de justice administrative, la requête que M. C... A... avait présentée à ce tribunal.<br/>
<br/>
              Par cette requête, enregistrée le 2 novembre 2020 au greffe du tribunal administratif de Toulouse, M. A... demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative d'ordonner la suspension de l'exécution de la décision d'obligation du port du masque pour les enfants âgés de 6 à 10 ans.<br/>
<br/>
<br/>
              Il soutient que :<br/>
              - la condition d'urgence est remplie dès lors que le port du masque comporte un certain nombre d'effets indésirables sur la santé des personnes, lesquels sont listés par l'Organisation mondiale de la santé ;<br/>
              - il est porté une atteinte grave et manifestement illégale à l'intérêt supérieur de l'enfant garanti par les stipulations de l'article 3-1 de la convention internationale des droits de l'enfant, par les dispositions de l'article L. 112-4 du code de l'action sociale et des familles et par celles de l'article 371-1 du code civil.<br/>
<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu : <br/>
              - la convention internationale relative aux droits de l'enfant ;<br/>
              - le code de l'action sociale et des familles ;<br/>
              - le code de la santé publique ;<br/>
              - le décret n° 2020-1257 du 14 octobre 2020 ;<br/>
              - le décret n° 2020-1310 du 29 octobre 2020 ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Les requêtes visées ci-dessus, qui sont présentées sur le fondement de l'article L. 521-2 du code de justice administrative, sont dirigées contre les mêmes dispositions. Il y a lieu de les joindre pour statuer par une seule ordonnance.<br/>
<br/>
              2. Aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. Le juge des référés se prononce dans un délai de quarante-huit heures ". Enfin, en vertu de l'article L. 522-3 du même code, le juge des référés peut, par une ordonnance motivée, rejeter une requête sans instruction ni audience lorsque la condition d'urgence n'est pas remplie ou lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable ou qu'elle est mal fondée.<br/>
<br/>
              Sur le cadre du litige :<br/>
<br/>
              3. Aux termes de l'article L. 3131-12 du code de la santé publique, issu de la loi du 23 mars 2020 d'urgence pour faire face à l'épidémie de Covid-19 : " L'état d'urgence sanitaire peut être déclaré sur tout ou partie du territoire métropolitain ainsi que du territoire des collectivités régies par les articles 73 et 74 de la Constitution et de la Nouvelle-Calédonie en cas de catastrophe sanitaire mettant en péril, par sa nature et sa gravité, la santé de la population ". L'article L. 3131-13 du même code précise que " L'état d'urgence sanitaire est déclaré par décret en conseil des ministres pris sur le rapport du ministre chargé de la santé. Ce décret motivé détermine la ou les circonscriptions territoriales à l'intérieur desquelles il entre en vigueur et reçoit application. Les données scientifiques disponibles sur la situation sanitaire qui ont motivé la décision sont rendues publiques. / (...) / La prorogation de l'état d'urgence sanitaire au-delà d'un mois ne peut être autorisée que par la loi, après avis du comité de scientifiques prévu à l'article L. 3131-19 ". Aux termes de l'article L. 3131-15 du même code : " Dans les circonscriptions territoriales où l'état d'urgence sanitaire est déclaré, le Premier ministre peut, par décret réglementaire pris sur le rapport du ministre chargé de la santé, aux seules fins de garantir la santé publique : / (...) 5° Ordonner la fermeture provisoire et réglementer l'ouverture, y compris les conditions d'accès et de présence, d'une ou plusieurs catégories d'établissements recevant du public. " Ces mesures doivent être " strictement proportionnées aux risques sanitaires encourus et appropriées aux circonstances de temps et de lieu. Il y est mis fin sans délai lorsqu'elles ne sont plus nécessaires. " <br/>
<br/>
              4. L'émergence d'un nouveau coronavirus, responsable de la maladie à coronavirus 2019 ou Covid-19 et particulièrement contagieux, a été qualifiée d'urgence de santé publique de portée internationale par l'Organisation mondiale de la santé le 30 janvier 2020, puis de pandémie le 11 mars 2020. La propagation du virus sur le territoire français a conduit le ministre chargé de la santé puis le Premier ministre à prendre, à compter du 4 mars 2020, des mesures de plus en plus strictes destinées à réduire les risques de contagion. Pour faire face à l'aggravation de l'épidémie, la loi du 23 mars 2020 a créé un régime d'état d'urgence sanitaire, défini aux articles L. 3131-12 à L. 3131-20 du code de la santé publique, et a déclaré l'état d'urgence sanitaire pour une durée de deux mois à compter du 24 mars 2020. La loi du 11 mai 2020 prorogeant l'état d'urgence sanitaire et complétant ces dispositions, a prorogé cet état d'urgence sanitaire jusqu'au 10 juillet 2020. L'évolution de la situation sanitaire a conduit à un assouplissement des mesures prises et la loi du 9 juillet 2020 a organisé un régime de sortie de cet état d'urgence.<br/>
<br/>
              5. Une nouvelle progression de l'épidémie au cours des mois de septembre et d'octobre, dont le rythme n'a cessé de s'accélérer au cours de cette période, a conduit le Président de la République à prendre le 14 octobre dernier, sur le fondement des articles L. 3131-12 et L. 3131-13 du code de la santé publique, un décret déclarant l'état d'urgence sanitaire à compter du 17 octobre sur l'ensemble du territoire national. Le 29 octobre 2020, le Premier ministre a pris, sur le fondement de l'article L. 3131-15 du code de la santé publique, le décret dont l'article 36 est ici contesté, prescrivant les mesures générales nécessaires pour faire face à l'épidémie de Covid-19 dans le cadre de l'état d'urgence sanitaire. L'article 1er de la loi du 14 novembre 2020 autorisant la prorogation de l'état d'urgence sanitaire et portant diverses mesures de gestion de la crise sanitaire a prorogé l'état d'urgence sanitaire jusqu'au 16 février 2021 inclus.<br/>
<br/>
              Sur l'impact du port du masque sur la santé des enfants :<br/>
<br/>
              6. Les deux requêtes font état de risques que créeraient le port du masque pour la santé de l'enfant. Elles produisent notamment une liste d'effets indésirables, émanant de l'OMS, au sein de laquelle sont notamment mentionnés les risques d'auto-contamination, de difficultés respiratoires et de lésions cutanées. Comme le relève le HCSP dans son Avis du 29 octobre 2020 relatif aux masques dans le cadre de la lutte contre la propagation du virus SARS-CoV-2, si l'efficacité intrinsèque des masques n'est pas modifiée chez l'enfant, l'ajustement du masque au visage est plus difficile et l'efficacité du port du masque serait réduite, voire même une auto-contamination pourrait être favorisée. Le HCSP relève également que l'adhésion au port d'un masque par des enfants d'âge primaire dépend de critères variés, du confort et de la respirabilité du masque comme chez l'adulte, mais aussi de critères plus subjectifs comme de son design. En outre, aux termes de l'avis émis conjointement le 14 septembre 2020 par l'OMS et l'UNICEF, intitulé Eléments à prendre en considération concernant les mesures de santé publique à mettre en place en milieu scolaire dans le cadre de l'épidémie de Covid-19 : " Dans les pays où les régions où la transmission communautaire du SARS-CoV-2 est intense et dans les contextes où il n'est pas possible de pratiquer l'éloignement physique, l'OMS et l'UNICEF recommandent aux décideurs d'appliquer les critères suivants concernant le port du masque dans les écoles (dans les salles de classe, les couloirs ou les espaces collectifs) lorsqu'ils élaborent les politiques nationales : / (...) Pour les enfants âgés de 6 à 11 ans, la décision concernant le port du masque doit reposer sur une approche fondée sur les risques qui tienne compte des aspects suivants : / (...) - la capacité des enfants à respecter le port approprié du masque et la disponibilité d'une supervision appropriée par des adultes ; / - l'impact potentiel du port du masque sur l'apprentissage et le développement psychosocial ; / - toutes considérations et tous ajustements spécifiques supplémentaires concernant des contextes particuliers tels que les activités sportives ou les enfants handicapés ou souffrant d'affections préexistantes. / (...) &#149; Les enseignants et le personnel auxiliaire peuvent être tenus de porter un masque lorsqu'ils ne peuvent pas maintenir une distance d'au moins 1 mètre vis-à-vis d'autrui ou en cas de transmission généralisée dans la région. / &#149; Tout doit être mis en oeuvre pour que le port du masque n'entrave pas l'apprentissage. ". Le HCSP relève également, dans son avis du 29 octobre, qu'il n'existe pas de vraie contre-indication au port du masque chez l'enfant de plus de trois ans. Il estime que le risque d'hypercapnie induite par le port prolongé d'un masque semble sans retentissement respiratoire ou neurologique et que si, chez des enfants ayant une pathologie respiratoire sévère, l'augmentation du travail respiratoire à travers le masque peut entraîner une gêne, leur état de santé les expose aux formes graves de Covid-19 et le port du masque est une des mesures essentielles pour les protéger. Le HCSP relève en outre que le port du masque peut entraîner une irritation de la peau de la face surtout en cas de port prolongé ou chez des personnes ayant une dermatose du visage préexistante. En outre, des élastiques trop tendus pourraient entraîner un inconfort et des lésions cutanées irritatives minimes rétro-auriculaires, mais aussi favoriser le décollement des oreilles en cas de port prolongé. Enfin, le HCSP estime que l'effet du port du masque sur l'anxiété des enfants est difficile à isoler des autres mesures potentiellement stressantes. Si les requêtes s'appuient pour étayer les risques allégués pour la santé de l'enfant sur un document émanant de l'OMS, le HCSP, conformément aux compétences que lui confie l'article L. 1411-4 du code de la santé publique et selon une démarche collégiale, a procédé à une analyse globale, au vu notamment d'une revue de la littérature scientifique, et en premier lieu des études relatives au port du masque chez l'enfant. Il résulte de tout ce qui précède que le risque pour la santé des enfants n'est pas établi, tant en ce qui concerne la toxicité que l'altération du système respiratoire, et qu'il appartient aux enseignants comme aux parents de s'assurer que le masque porté par l'enfant n'entraîne pas d'irritation ou de lésion.<br/>
<br/>
              7. En conséquence, dans le présent état de la connaissance scientifique et au vu de la circulation encore très intense du virus à la date de la présente ordonnance, l'obligation faite aux enfants de 6 à 10 ans de porter le masque à l'école et dans les lieux de loisirs périscolaires, ne porte pas d'atteinte grave et manifestement illégales aux libertés fondamentales des enfants. Les requêtes nos 445999 et 446001 doivent par suite être rejetées selon la procédure prévue à l'article L. 522-3 du code de justice administrative.<br/>
<br/>
<br/>
<br/>			O R D O N N E :<br/>
              			------------------<br/>
<br/>
Article 1er : Les requêtes de M. D... et M. A... sont rejetées.<br/>
Article 2 : La présente ordonnance sera notifiée à M. B... D... et à M. C... A.... Copie en sera transmise au Premier ministre et au ministre des solidarités et de la santé.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
