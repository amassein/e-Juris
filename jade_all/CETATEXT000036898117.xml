<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036898117</ID>
<ANCIEN_ID>JG_L_2018_05_000000411598</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/89/81/CETATEXT000036898117.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère chambre, 09/05/2018, 411598, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-05-09</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>411598</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GATINEAU, FATTACCINI ; SCP LYON-CAEN, THIRIEZ</AVOCATS>
<RAPPORTEUR>Mme Florence Marguerite</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Charles Touboul</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:411598.20180509</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A...B...a, d'une part, demandé au tribunal administratif de Lyon d'annuler la mise en demeure du 9 janvier 2014 par laquelle la caisse d'allocations familiales du Rhône a poursuivi le recouvrement d'indus d'aide exceptionnelle de fin d'année pour les années 2010, 2011 et 2012 et, d'autre part, formé opposition à la contrainte émise à son encontre le 18 avril 2014 par le directeur de cette caisse, d'un montant de 457,35 euros, correspondant aux mêmes indus. Par un jugement n° 1404051 du 17 janvier 2017, le tribunal administratif de Lyon a rejeté sa demande.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 19 juin et 19 septembre 2017 au secrétariat du contentieux du Conseil d'Etat, M. B... demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande ;<br/>
<br/>
              3°) de mettre à la charge de la caisse d'allocations familiales du Rhône la somme de 3 000 euros à verser à la SCP Lyon-Caen, Thiriez, son avocat, au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'action sociale et des familles ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - le décret n° 2010-1631 du 23 décembre 2010;<br/>
              - le décret n° 2011-1868 du 13 décembre 2011 ;<br/>
              - le décret n° 2012-168 du 27 décembre 2012 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Florence Marguerite, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Charles Touboul, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Lyon-Caen, Thiriez, avocat de M.B..., et à la SCP Gatineau, Fattaccini, avocat de la caisse d'allocations familiales du Rhône.<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis au juge du fond qu'à la suite d'un contrôle de la situation de l'intéressé en juillet 2013, la caisse d'allocations familiales du Rhône a décidé la récupération auprès de M.B..., bénéficiaire depuis octobre 2009 du revenu de solidarité active, d'un indu de cette allocation pour la période de septembre 2010 à juillet 2013, puis a mis en demeure l'intéressé, le 9 janvier 2014, de lui reverser trois indus d'aide exceptionnelle de fin d'année pour les années 2010, 2011 et 2012, d'un montant total de 457,35 euros, et, le 18 avril 2014, émis une contrainte à son encontre pour recouvrer cette somme. Par un jugement du 17 janvier 2017, le tribunal administratif de Lyon a rejeté comme irrecevable sa demande d'annulation de la mise en demeure du 9 janvier 2014 et comme non fondée l'opposition qu'il avait formée contre la contrainte du 18 avril 2014. Par son pourvoi en cassation, M. B...doit être regardé, eu égard aux moyens qu'il soulève, comme demandant l'annulation de ce jugement en tant seulement qu'il rejette son opposition à contrainte.<br/>
<br/>
              2. Il résulte des termes du jugement attaqué que, pour juger que M. B... n'avait pas droit à l'aide exceptionnelle de fin d'année pour les années 2010, 2011 et 2012 et rejeter l'opposition à contrainte que celui-ci avait formée, le tribunal administratif de Lyon s'est fondé sur la circonstance que, ainsi qu'il l'avait jugé par un jugement du 2 février 2016, le requérant ne pouvait prétendre au revenu de solidarité active pour la période allant de septembre 2010 à juillet 2013. Toutefois, ce jugement a été annulé par une décision du Conseil d'Etat, statuant au contentieux, n° 400978 du 17 novembre 2017. Par suite, M. B... est fondé à soutenir qu'en se fondant sur les motifs de son jugement du 2 février 2016, le tribunal a commis une erreur de droit et à demander pour ce motif l'annulation du jugement qu'il attaque, en tant qu'il rejette son opposition à contrainte et, par voie de conséquence, ses conclusions présentées au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991. <br/>
<br/>
              3. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de M.B..., qui n'est pas, dans la présente instance, la partie perdante. Ces mêmes dispositions font également obstacle à ce qu'une somme soit mise à la charge de la caisse d'allocations familiales du Rhône, qui, ayant pris les décisions en litige au nom de l'Etat, n'est pas partie à la procédure et n'a été appelée en la cause que pour produire des observations.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du tribunal administratif de Lyon du 17 janvier 2017 est annulé en tant qu'il rejette l'opposition à contrainte formée par M. B...et ses conclusions présentées au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991. <br/>
Article 2 : L'affaire est renvoyée au tribunal administratif de Lyon dans la mesure de la cassation prononcée. <br/>
Article 3 : Les conclusions de la caisse d'allocations familiales du Rhône présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée à M. A...B...et à la ministre des solidarités et de la santé.<br/>
Copie en sera adressée à la caisse d'allocations familiales du Rhône. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
