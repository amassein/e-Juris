<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000035991017</ID>
<ANCIEN_ID>JG_L_2017_11_000000404995</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/35/99/10/CETATEXT000035991017.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère chambre, 08/11/2017, 404995, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-11-08</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>404995</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FOUSSARD, FROGER</AVOCATS>
<RAPPORTEUR>Mme Florence Marguerite</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Rémi Decout-Paolini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2017:404995.20171108</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 10 novembre 2016, 10 février 2017 et 29 août 2017 au secrétariat du contentieux du Conseil d'Etat, la caisse autonome de retraite des médecins de France demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision implicite de rejet née du silence gardé par le ministre des affaires sociales et de la santé sur sa demande du 12 juillet 2016 tendant à l'abrogation de l'article 3 de l'arrêté du 4 juin 1959 modifié portant indemnités aux administrateurs de la caisse nationale d'assurance vieillesse des professions libérales et des sections professionnelles ;<br/>
<br/>
              2°) d'enjoindre au ministre d'abroger l'article 3 de cet arrêté ou, à titre subsidiaire, de lui enjoindre de mettre cet article 3 en conformité avec l'article L. 231-12 du code de la sécurité sociale dans un délai de trois mois à compter de la notification de la décision à venir ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de la sécurité sociale;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Florence Marguerite, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Rémi Decout-Paolini, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Foussard, Froger, avocat de la caisse autonome de retraite des médecins de France.<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Par un courrier du 12 juillet 2016, la caisse autonome de retraite des médecins de France a demandé au ministre des affaires sociales et de la santé d'abroger l'article 3 de l'arrêté du 4 juin 1959 portant indemnités aux administrateurs de la caisse nationale d'assurance vieillesse des professions libérales et des sections professionnelles, modifié en dernier lieu par un arrêté du 22 décembre 2010, qui prévoit que : " Les administrateurs perçoivent, à titre de frais de séjour, des indemnités égales à celles dont bénéficient les administrateurs de la caisse nationale et des caisses de base du régime social des indépendants, dans les mêmes conditions que ceux-ci ". La caisse autonome de retraite des médecins de France demande l'annulation pour excès de pouvoir de la décision implicite par laquelle le ministre a rejeté sa demande d'abrogation. <br/>
<br/>
              Sur la légalité de la décision attaquée :<br/>
<br/>
              2. Aux termes du premier alinéa de l'article L. 243-2 du code des relations entre le public et l'administration : " L'administration est tenue d'abroger expressément un acte réglementaire illégal ou dépourvu d'objet, que cette situation existe depuis son édiction ou qu'elle résulte de circonstances de droit ou de fait postérieures, sauf à ce que l'illégalité ait cessé ". <br/>
<br/>
              3. L'article L. 231-12 du code de la sécurité sociale dispose que : " Les organismes de sécurité sociale ne peuvent, en aucun cas, allouer un traitement à leurs membres du conseil ou administrateurs. Toutefois, ils leur remboursent leurs frais de déplacement. / Ils remboursent également aux employeurs des membres du conseil ou administrateurs salariés les salaires maintenus pour leur permettre d'exercer leurs fonctions pendant le temps de travail ainsi que les avantages et les charges sociales y afférents. / Les membres du conseil ou administrateurs des organismes de sécurité sociale ayant la qualité de travailleur indépendant peuvent percevoir des indemnités pour perte de leurs gains, fixées par arrêté ministériel ". L'article D. 231-25 du même code précise que l'arrêté prévu au troisième alinéa de l'article L. 231-12 est pris par le ministre chargé de la sécurité sociale.<br/>
<br/>
              4. Ni ces dispositions, qui habilitent le ministre chargé de la sécurité sociale à fixer les indemnités pour perte de leurs gains que peuvent percevoir les administrateurs des organismes de sécurité sociale ayant la qualité de travailleur indépendant, ni aucune autre disposition de la loi ou du décret ne donnent au ministre chargé de la sécurité sociale compétence pour définir les conditions et limites du remboursement des frais de déplacement des administrateurs de la caisse nationale d'assurance vieillesse des professions libérales et de ses sections professionnelles. Par suite, la caisse autonome de retraite des médecins de France est fondée à soutenir que l'article 3 de l'arrêté du 4 juin 1959 disposant que les administrateurs de la caisse nationale d'assurance vieillesse des professions libérales perçoivent, à titre de frais de séjour, des indemnités équivalentes à celles dont bénéficient les administrateurs de la caisse nationale et des caisses de base du régime social des indépendants a été pris par une autorité incompétente. <br/>
<br/>
              5. Il résulte de ce qui précède que la caisse requérante est fondée à demander l'annulation de la décision refusant d'abroger l'article 3 de l'arrêté du 4 juin 1959 qu'elle attaque. Le moyen d'incompétence retenu suffisant à entraîner cette annulation, il n'est pas nécessaire de se prononcer sur les autres moyens de la requête.<br/>
<br/>
              Sur les conclusions à fin d'injonction :<br/>
<br/>
              6. Aux termes de l'article L. 911-1 du code de justice administrative : " Lorsque sa décision implique nécessairement qu'une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public prenne une mesure d'exécution dans un sens déterminé, la juridiction, saisie de conclusions en ce sens, prescrit, par la même décision, cette mesure assortie, le cas échéant, d'un délai d'exécution ".<br/>
<br/>
              7. La présente décision implique nécessairement que le ministre chargé de la sécurité sociale abroge les dispositions contestées. Il y a lieu pour le Conseil d'Etat d'enjoindre au ministre des solidarités et de la santé de procéder à cette abrogation dans un délai de deux mois à compter de la notification de la présente décision.<br/>
<br/>
              Sur les frais exposés par les parties à l'occasion du litige :<br/>
<br/>
              8. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de  3 000 euros à verser à la caisse autonome de retraite des médecins de France au titre des dispositions de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La décision implicite par laquelle le ministre des affaires sociales et de la santé a refusé d'abroger l'article 3 de l'arrêté du 4 juin 1959 portant indemnités aux administrateurs de la caisse nationale d'assurance vieillesse des professions libérales et des sections professionnelles est annulée.<br/>
Article 2 : Il est enjoint au ministre des solidarités et de la santé d'abroger l'article 3 de l'arrêté du 4 juin 1959 dans un délai de deux mois à compter de la notification de la présente décision.<br/>
Article 3 : L'Etat versera à la caisse autonome de retraite des médecins de France la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à la caisse autonome de retraite des médecins de France et à la ministre des solidarités et de la santé.<br/>
Copie en sera adressée à la section du rapport et des études du Conseil d'Etat.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
