<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000025933987</ID>
<ANCIEN_ID>JG_L_2012_05_000000355594</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/25/93/39/CETATEXT000025933987.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème et 9ème sous-sections réunies, 30/05/2012, 355594</TITRE>
<DATE_DEC>2012-05-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>355594</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème et 9ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Philippe Martin</PRESIDENT>
<AVOCATS>FOUSSARD ; SPINOSI</AVOCATS>
<RAPPORTEUR>Mme Suzanne von Coester</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Julien Boucher</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2012:355594.20120530</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi, enregistré le 5 janvier 2012 au secrétariat du contentieux du Conseil d'Etat, présenté pour l'OFFICE FRANÇAIS DE PROTECTION DES REFUGIES ET APATRIDES (OFPRA), dont le siège est 201, rue Carnot à Fontenay-sous-Bois (94136) ; l'OFFICE FRANÇAIS DE PROTECTION DES REFUGIES ET APATRIDES demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'ordonnance n° 1109056/10 du 21 décembre 2011 par laquelle le juge des référés du tribunal administratif de Melun, statuant sur le fondement de l'article L. 521-1 du code de justice administrative, a, d'une part, suspendu l'exécution de la décision du 15 novembre 2011 par laquelle le directeur général de l'OFFICE FRANÇAIS DE PROTECTION DES REFUGIES ET APATRIDES a rejeté la demande d'asile de M. Abutalib A et, d'autre part, enjoint au directeur général de l'OFPRA de statuer sur cette demande dans un délai de quatre mois ;<br/>
<br/>
              2°) statuant en référé, de rejeter les conclusions de M. A ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
<br/>
              Vu la loi n° 91-647 du 10 juillet 1991 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Suzanne von Coester, Maître des Requêtes,<br/>
<br/>
              - les observations de Me Foussard, avocat de l'OFFICE FRANÇAIS DE PROTECTION DES REFUGIES ET APATRIDES et de Me Spinosi, avocat de M. A,<br/>
<br/>
              - les conclusions de M. Julien Boucher, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à Me Foussard, avocat de l'OFFICE FRANÇAIS DE PROTECTION DES REFUGIES ET APATRIDES et à Me Spinosi, avocat de M. A ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant que le juge des référés du tribunal administratif ne peut être régulièrement saisi d'une demande tendant à la mise en oeuvre de l'une des procédures régies par le livre V du code de justice administrative que pour autant que le litige principal auquel se rattache ou est susceptible de se rattacher la mesure d'urgence qu'il lui est demandé de prendre ressortit lui-même à la compétence du tribunal administratif ;<br/>
<br/>
              Considérant qu'aux termes de l'article L. 731-2 du code de l'entrée et du séjour des étrangers et du droit d'asile : " La Cour nationale du droit d'asile statue sur les recours formés contre les décisions de l'Office français de protection des réfugiés et apatrides, prises en application des articles L. 711-1, L. 712-1 à L. 712-3 et L. 723-1 à L. 723-3 (...) " ; que le 1° de l'article R. 733-6 de ce code précise que cette juridiction statue, notamment, sur les recours formés contre les décisions de l'office accordant ou refusant le bénéfice de l'asile ;<br/>
<br/>
              Considérant qu'il ressort des pièces du dossier soumis au juge des référés du tribunal administratif de Melun que, par une décision du 15 novembre 2011, le directeur général de l'OFFICE FRANÇAIS DE PROTECTION DES REFUGIES ET APATRIDES (OFPRA) a rejeté la demande d'asile déposée par M. A au motif que celui-ci, qui ne produisait aucun document d'identité ou de voyage, avait rendu volontairement impossible l'identification de ses empreintes digitales et qu'ainsi, il ne permettait pas à l'office de recueillir l'ensemble des éléments nécessaires à la reconnaissance du bien-fondé de sa demande ; que, ce faisant, le directeur général de l'OFPRA ne s'est pas borné à refuser d'enregistrer cette demande mais a expressément refusé à M. A, sur le fondement des dispositions mentionnées à l'article L. 731-2 du code de l'entrée et du séjour des étrangers et du droit d'asile, le bénéfice de l'asile, sans que la circonstance alléguée par M. A que cette décision serait intervenue en méconnaissance des dispositions régissant l'examen des demandes d'asile puisse avoir d'incidence sur la qualification de la décision qui lui a été opposée ; que dès lors, et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, l'OFPRA est fondé à soutenir qu'en estimant que la décision du 15 novembre 2011 ne constituait pas une décision refusant le bénéfice de l'asile à M. A au motif qu'elle n'avait pas été prise à l'issue d'un examen de sa demande d'asile, et en en déduisant qu'un recours contre cette décision relevait, non de la Cour nationale du droit d'asile, mais de la compétence du juge administratif de droit commun, le juge des référés du tribunal administratif de Melun a commis une erreur de droit ; que, par suite, l'ordonnance attaquée doit être annulée et la demande de M. A rejetée, en vertu des articles L. 821-2 et R. 522-8-1 du code de justice administrative, comme portée devant une juridiction incompétente pour en connaître ;<br/>
<br/>
              Considérant que les dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 font obstacle à ce qu'il soit fait droit aux conclusions présentées sur ce fondement par Me Spinosi, avocat de M. A ;<br/>
<br/>
<br/>
<br/>				D E C I D E  :<br/>
              				--------------<br/>
<br/>
Article 1er : L'ordonnance du 21 décembre 2011 du juge des référés du tribunal administratif de Melun est annulée.<br/>
<br/>
Article 2 : La demande de M. A est rejetée comme portée devant une juridiction incompétente pour en connaître.<br/>
<br/>
Article 3 : Les conclusions présentées par Me Spinosi au titre des dispositions de l'article 37 de la loi du 10 juillet 1991 sont rejetées.<br/>
<br/>
Article 4 : La présente décision sera notifiée à l'OFFICE FRANÇAIS DE PROTECTION DES REFUGIES ET APATRIDES et à M. Abutalib A.<br/>
Copie en sera adressée pour information au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">095-07-01-02 - RECOURS FORMÉS CONTRE LES DÉCISIONS DE L'OFPRA ACCORDANT OU REFUSANT LE BÉNÉFICE DE L'ASILE (1° DE L'ART. R. 733-6 DU CESEDA) - LITIGE RELATIF À UNE DÉCISION DU DIRECTEUR GÉNÉRAL DE L'OFPRA REFUSANT D'ENREGISTRER UNE DEMANDE D'ASILE ET REJETANT EXPRESSÉMENT CELLE-CI AU MOTIF QUE LE DEMANDEUR A RENDU VOLONTAIREMENT IMPOSSIBLE SON IDENTIFICATION - INCLUSION, SANS QU'AIT D'INCIDENCE LA CIRCONSTANCE ALLÉGUÉE QUE CETTE DÉCISION SERAIT INTERVENUE EN MÉCONNAISSANCE DES DISPOSITIONS RÉGISSANT L'EXAMEN DES DEMANDES D'ASILE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">17-05-01-01 COMPÉTENCE. COMPÉTENCE À L'INTÉRIEUR DE LA JURIDICTION ADMINISTRATIVE. COMPÉTENCE EN PREMIER RESSORT DES TRIBUNAUX ADMINISTRATIFS. COMPÉTENCE MATÉRIELLE. - EXCLUSION - LITIGE RELATIF À UNE DÉCISION DU DIRECTEUR GÉNÉRAL DE L'OFPRA REFUSANT D'ENREGISTRER UNE DEMANDE D'ASILE ET REJETANT EXPRESSÉMENT CELLE-CI AU MOTIF QUE LE DEMANDEUR A RENDU VOLONTAIREMENT IMPOSSIBLE SON IDENTIFICATION.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">17-05-04-02 COMPÉTENCE. COMPÉTENCE À L'INTÉRIEUR DE LA JURIDICTION ADMINISTRATIVE. COMPÉTENCE DES JURIDICTIONS ADMINISTRATIVES SPÉCIALES. JURIDICTION ADMINISTRATIVE DE DROIT COMMUN OU JURIDICTION ADMINISTRATIVE SPÉCIALISÉE. - COMPÉTENCE DE LA CNDA - INCLUSION - LITIGE RELATIF À UNE DÉCISION DU DIRECTEUR GÉNÉRAL DE L'OFPRA REFUSANT D'ENREGISTRER UNE DEMANDE D'ASILE ET REJETANT EXPRESSÉMENT CELLE-CI AU MOTIF QUE LE DEMANDEUR A RENDU VOLONTAIREMENT IMPOSSIBLE SON IDENTIFICATION - CONSÉQUENCE - COMPÉTENCE DU TRIBUNAL ADMINISTRATIF - ABSENCE.
</SCT>
<ANA ID="9A"> 095-07-01-02 Un litige relatif à une décision du directeur général de l'Office français de protection des réfugiés et apatrides (OFPRA) refusant d'enregistrer une demande d'asile et rejetant expressément celle-ci au motif que le demandeur a rendu volontairement impossible son identification, ressortit à la compétence de la Cour nationale du droit d'asile (CNDA), laquelle statue, en vertu du 1° de l'article R. 733-6 du code de l'entrée et du séjour des étrangers et du droit d'asile (CESEDA), sur les recours formés contre les décisions de l'office accordant ou refusant le bénéfice de l'asile. La circonstance alléguée par le demandeur que cette décision serait intervenue en méconnaissance des dispositions régissant l'examen des demandes d'asile et n'aurait pas été prise à l'issue d'un examen de sa demande d'asile, est sans incidence sur la qualification de la décision qui lui a été opposée. Par suite, incompétence du juge des référés du tribunal administratif pour connaître d'une requête tendant à la suspension de l'exécution de cette décision.</ANA>
<ANA ID="9B"> 17-05-01-01 Le juge des référés du tribunal administratif ne peut être régulièrement saisi d'une demande tendant à la mise en oeuvre de l'une des procédures régies par le livre V du code de justice administrative que pour autant que le litige principal auquel se rattache ou est susceptible de se rattacher la mesure d'urgence qu'il lui est demandé de prendre ressortit lui-même à la compétence du tribunal administratif. Tel n'est pas le cas d'un litige relatif à une décision du directeur général de l'Office français de protection des réfugiés et apatrides (OFPRA) refusant d'enregistrer une demande d'asile et rejetant expressément celle-ci au motif que le demandeur a rendu volontairement impossible son identification, dès lors qu'un tel litige ressortit à la compétence de la Cour nationale du droit d'asile (CNDA), laquelle statue, en vertu du 1° de l'article R. 733-6 du code de l'entrée et du séjour des étrangers et du droit d'asile (CESEDA), sur les recours formés contre les décisions de l'office accordant ou refusant le bénéfice de l'asile. La circonstance alléguée par le demandeur que cette décision serait intervenue en méconnaissance des dispositions régissant l'examen des demandes d'asile et n'aurait pas été prise à l'issue d'un examen de sa demande d'asile, est sans incidence sur la qualification de la décision qui lui a été opposée. Par suite, incompétence du juge des référés du tribunal administratif pour connaître d'une requête tendant à la suspension de l'exécution de cette décision.</ANA>
<ANA ID="9C"> 17-05-04-02 Un litige relatif à une décision du directeur général de l'Office français de protection des réfugiés et apatrides (OFPRA) refusant d'enregistrer une demande d'asile et rejetant expressément celle-ci au motif que le demandeur a rendu volontairement impossible son identification, ressortit à la compétence de la Cour nationale du droit d'asile (CNDA), laquelle statue, en vertu du 1° de l'article R. 733-6 du code de l'entrée et du séjour des étrangers et du droit d'asile (CESEDA), sur les recours formés contre les décisions de l'office accordant ou refusant le bénéfice de l'asile. La circonstance alléguée par le demandeur que cette décision serait intervenue en méconnaissance des dispositions régissant l'examen des demandes d'asile et n'aurait pas été prise à l'issue d'un examen de sa demande d'asile, est sans incidence sur la qualification de la décision qui lui a été opposée. Par suite, incompétence du juge des référés du tribunal administratif pour connaître d'une requête tendant à la suspension de l'exécution de cette décision.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
