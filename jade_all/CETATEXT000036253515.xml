<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036253515</ID>
<ANCIEN_ID>JG_L_2017_12_000000409693</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/25/35/CETATEXT000036253515.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème chambre, 20/12/2017, 409693, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-12-20</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>409693</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>BALAT ; SCP MARLANGE, DE LA BURGADE</AVOCATS>
<RAPPORTEUR>Mme Céline  Guibé</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emilie Bokdam-Tognetti</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2017:409693.20171220</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une décision n° 393721 du 15 avril 2016, le Conseil d'Etat, statuant au contentieux, saisi par l'association Vent de colère ! Fédération nationale, a prononcé une astreinte à l'encontre de l'Etat, s'il ne justifiait pas avoir, dans les six mois suivant la notification de cette décision, exécuté la décision n° 324852 du 28 mai 2014 par laquelle le Conseil d'Etat statuant au contentieux avait annulé l'arrêté du 17 novembre 2008 fixant les conditions d'achat de l'électricité produite par les installations utilisant l'énergie mécanique du vent et l'arrêté du 23 décembre 2008 le complétant.<br/>
<br/>
              Par une requête en tierce opposition, enregistrée le 10 avril 2017 au secrétariat du contentieux du Conseil d'Etat, les sociétés Innovent, FE Andrieu, FE Fiefs, Eoliennes de Bignan, Carbonne verre, FE Frénouville, FE Gaprée, FE Lamballe, FE Plechatel, FE Sains les Pernes, Verhaeghe gestion finance, FE Coquelles et Ferme éolienne de Croix Rault demandent au Conseil d'Etat :<br/>
<br/>
              1°) de déclarer non avenue sa décision n° 393721 du 15 avril 2016 ;<br/>
<br/>
              2°) statuant à nouveau, de ne pas prononcer d'astreinte en vue de l'exécution de sa décision n° 324852 du 28 mai 2014 ; <br/>
<br/>
              3°) de mettre à la charge de l'association Vent de colère ! Fédération nationale le versement de la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
              Vu :<br/>
              - le traité sur le fonctionnement de l'Union européenne ;<br/>
              - le règlement (CE) n° 794/2004 de la Commission du 21 avril 2004 concernant la mise en oeuvre du règlement (CE) n° 659/1999 du Conseil portant modalités d'application de l'article 93 du traité CE ; <br/>
              - les arrêts C-368/04 du 5 octobre 2006 et C-199/06 du 12 février 2008 de la Cour de justice des communautés européennes ;<br/>
              - la décision n° 324852 du 15 mai 2012 du Conseil d'Etat statuant au contentieux ;<br/>
              - l'arrêt C-262/12 du 19 décembre 2013 de la Cour de justice de l'Union européenne ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Céline Guibé, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Emilie Bokdam-Tognetti, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Balat, avocat des sociétés Innovent, FE Audrieu, FE Fiefs, Eoliennes de Bignan, Carbonne Vert, FE Renouville, FE Gapree, FE Lamballe, FE Plechatel, FE Sains Les Pernes, Verhaeghe Gestion Finances, FE Coquelles et Ferme Eolienne de Croix Rault et à la SCP Marlange, de la Burgade, avocat de l'association Vent de colère - Fédération nationale ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. En vertu des dispositions de l'article R. 832-1 du code de justice administrative, toute personne qui n'a été ni appelée, ni représentée dans l'instance peut former tierce opposition à une décision du Conseil d'Etat rendue en matière contentieuse. Cette voie de rétractation est ouverte à ceux qui se prévalent d'un droit auquel la décision entreprise aurait préjudicié.<br/>
<br/>
              2. Aux termes du paragraphe 1 de l'article 107 du traité sur le fonctionnement de l'Union européenne : " Sauf dérogations prévues par les traités, sont incompatibles avec le marché intérieur, dans la mesure où elles affectent les échanges entre Etats membres, les aides accordées par les Etats ou au moyen de ressources d'Etat sous quelque forme que ce soit qui faussent ou qui menacent de fausser la concurrence en favorisant certaines entreprises ou certaines productions " . Aux termes de l'article 108 du même traité : " 1. La Commission procède avec les Etats membres à l'examen permanent des régimes d'aides existant dans ces Etats (...) / 2. Si (...) la Commission constate qu'une aide accordée par un Etat ou au moyen de ressources d'Etat n'est pas compatible avec le marché intérieur (...), elle décide que l'Etat intéressé doit la supprimer ou la modifier dans le délai qu'elle détermine (...). /  3. La Commission est informée, en temps utile pour présenter ses observations, des projets tendant à instituer ou à modifier des aides. Si elle estime qu'un projet n'est pas compatible avec le marché intérieur, (...) elle ouvre sans délai la procédure prévue au paragraphe précédent. L'Etat membre intéressé ne peut mettre à exécution les mesures projetées, avant que cette procédure ait abouti à une décision finale ".<br/>
<br/>
              3. Par une décision du 28 mai 2014, le Conseil d'Etat statuant au contentieux a annulé l'arrêté du 17 novembre 2008 fixant les conditions d'achat de l'électricité produite par les installations utilisant l'énergie mécanique du vent et l'arrêté du 23 décembre 2008 le complétant. Il a prononcé cette annulation au motif qu'il résultait tant des motifs de sa propre décision n° 324852 du 15 mai 2012, que de l'arrêt C-262/12 du 19 décembre 2013 par lequel la Cour de justice de l'Union européenne s'est prononcée sur la question dont il l'avait saisie à titre préjudiciel, que l'achat de l'électricité produite par les installations utilisant l'énergie mécanique du vent à un prix supérieur à sa valeur de marché, dans les conditions définies par ces arrêtés, avait le caractère d'une aide d'Etat et que les arrêtés instituant cette aide avaient été pris en méconnaissance de l'obligation de notification préalable à la Commission européenne résultant de l'article 108, paragraphe 3 du traité sur le fonctionnement de l'Union européenne cité au point 2.<br/>
<br/>
              4. Par une décision du 15 avril 2016, le Conseil d'Etat statuant au contentieux a, en application des dispositions de l'article L. 911-5 du code de justice administrative, jugé que l'exécution de sa décision du 28 mai 2014 citée au point 3 ne serait complète qu'une fois que l'Etat aurait pris toutes les mesures nécessaires pour assurer le paiement, par chaque bénéficiaire de l'aide, des intérêts qu'il aurait acquittés s'il avait dû emprunter sur le marché le montant de l'aide accordée en application des arrêtés annulés dans l'attente de la décision de la Commission, que ces intérêts étaient dus sur les montants versés en application de l'arrêté du 17 novembre 2008, à proportion de la fraction de ces montants ayant la nature d'une aide, de la date de ce versement jusqu'à la date à laquelle la Commission a conclu à la compatibilité de l'aide avec le marché intérieur, soit le 27 mars 2014, et qu'ils devaient être calculés conformément au règlement (CE) n° 794/2004 de la Commission du 21 avril 2004. Constatant qu'à la date de sa décision, l'Etat n'avait pas pris les mesures propres à assurer l'exécution de la décision du 28 mai 2014, il a prononcé contre l'Etat, à défaut pour lui de justifier de cette exécution dans un délai de six mois à compter de la notification de sa décision, une astreinte de 10 000 euros par jour jusqu'à la date à laquelle la décision du 28 mai 2014 aurait reçu exécution.<br/>
<br/>
              5. A l'appui de leur tierce opposition, les sociétés requérantes soutiennent, en premier lieu, que lorsque le juge national a annulé un acte réglementaire pour défaut de notification préalable d'une aide d'Etat, et qu'il n'a pas assorti sa décision d'annulation de mesures tendant à l'exécution de celle-ci, il ne peut ultérieurement prononcer de telles mesures.<br/>
<br/>
              6. Toutefois, dans sa décision critiquée du 15 avril 2016, le Conseil d'Etat statuant au contentieux a relevé qu'il résulte des stipulations énoncées au point 2, telles qu'interprétées par la Cour de justice des Communautés européennes notamment dans ses arrêts du 5 octobre 2006, Transalpine Ölleitung in Österreich e.a., C-368/04, et de grande chambre du 12 février 2008, Centre d'exportation du livre français (CELF), C-199/06, que, s'il ressortit à la compétence exclusive de la Commission européenne de décider, sous le contrôle des juridictions de l'Union européenne, si une aide de la nature de celles qui sont mentionnées à l'article 107 du traité est ou non, compte tenu des dérogations prévues par le traité, compatible avec le marché intérieur, il incombe, en revanche, aux juridictions nationales de sauvegarder, jusqu'à la décision finale de la Commission, les droits des justiciables en cas de violation de l'obligation de notification préalable des aides d'Etat à la Commission prévue à l'article 108, paragraphe 3. Il a également relevé qu'il revient à ces juridictions de sanctionner, le cas échéant, l'illégalité de dispositions de droit national qui auraient institué ou modifié une telle aide en méconnaissance de l'obligation que ces stipulations imposent aux Etats membres d'en notifier le projet à la Commission préalablement à toute mise à exécution. Il a rappelé que lorsque la Commission européenne a adopté une décision devenue définitive constatant l'incompatibilité de cette aide avec le marché intérieur, la sanction de cette illégalité implique la récupération de l'aide mise à exécution en méconnaissance de cette obligation, mais que lorsque la Commission européenne a adopté une décision devenue définitive constatant la compatibilité de cette aide avec le marché intérieur, la sanction de cette illégalité implique seulement que soit mis à la charge des bénéficiaires de l'aide le paiement d'intérêts au titre de la période d'illégalité. <br/>
<br/>
              7. Dans la même décision, le Conseil d'Etat statuant au contentieux a jugé que si l'exécution d'un jugement par lequel un acte réglementaire a été annulé n'implique pas en principe que le juge, saisi sur le fondement de l'article L. 911-5 du code de justice administrative, enjoigne à l'administration de revenir sur les mesures individuelles prises en application de cet acte, la juridiction administrative, juge de droit commun du droit de l'Union, doit veiller à ce que toutes les conséquences d'une violation de l'article 108, paragraphe 3, du traité sur le fonctionnement de l'Union européenne soient tirées et que lorsque le Conseil d'Etat statuant au contentieux a annulé un acte réglementaire instituant une aide en méconnaissance de l'obligation de notification préalable à la Commission européenne, il incombe à l'Etat de prendre toutes les mesures nécessaires pour assurer le recouvrement auprès des bénéficiaires de l'aide, selon le cas, des aides versées sur le fondement de ce régime illégal ou des intérêts calculés sur la période d'illégalité. Dans cette même décision, le Conseil d'Etat statuant au contentieux a jugé que lorsqu'il constate que les mesures nécessaires n'ont pas été prises, le juge prescrit, sur le fondement des dispositions du livre IX du code de justice administrative, les mesures d'exécution impliquées par l'annulation de cet acte réglementaire, afin d'assurer la pleine effectivité du droit de l'Union. <br/>
<br/>
              8. Il résulte de ce qui précède que les mesures prescrites par le Conseil d'Etat dans sa décision critiquée ont été prises pour assurer la pleine effectivité du droit communautaire sans qu'y fasse obstacle la circonstance que ces mesures n'aient pas été prescrites dans la décision du 28 mai 2014, dès lors que le juge de l'excès de pouvoir n'est jamais tenu d'assortir sa décision d'une injonction pour en assurer l'exécution. Par suite, les sociétés requérantes ne sont pas fondées à critiquer la décision qu'elles attaquent pour ce motif, et ne peuvent pas davantage invoquer une atteinte aux principes de confiance légitime et de sécurité juridique, dès lors que l'absence de prononcé d'une injonction dans la décision du 28 mai 2014 n'excluait pas qu'une astreinte soit ultérieurement prononcée. <br/>
<br/>
              9. En deuxième lieu, les sociétés requérantes soutiennent que l'Etat ne pouvait ordonner la récupération des intérêts afférents à l'aide illégalement versée, en raison de circonstances exceptionnelles tenant au fait que les bénéficiaires de l'aide pouvaient, sur le fondement du principe de confiance légitime, déduire de plusieurs décisions juridictionnelles que le tarif d'achat institué par l'arrêté  du 17 novembre 2008 n'était pas constitutif d'une aide d'Etat. Toutefois, un Etat membre dont les autorités ont octroyé une aide en violation des règles prévues à l'article 108 du traité sur le fonctionnement de l'Union européenne ne saurait invoquer la confiance légitime des bénéficiaires pour se soustraire à l'obligation de prendre les mesures nécessaires en vue de l'exécution de la décision annulant l'acte réglementaire instituant cette aide pour défaut de notification à la Commission. Par suite, le moyen soulevé par les sociétés requérantes est inopérant à l'appui de la présente tierce opposition. <br/>
<br/>
              10. En troisième lieu, si le moyen tiré de la prescription partielle des créances de l'Etat à l'encontre d'un producteur éolien peut, le cas échéant, être invoqué à l'appui d'un recours contre le titre de recettes mettant à sa charge les intérêts dus au titre de la période d'illégalité de l'aide, un tel moyen ne peut être utilement invoqué pour critiquer le motif par lequel le Conseil d'Etat a jugé que les intérêts à récupérer devaient être calculés à compter de la date d'octroi des aides jusqu'à la date de la décision de la Commission européenne déclarant l'aide compatible avec les stipulations du Traité. Par suite, les sociétés requérantes ne peuvent davantage utilement soutenir à l'occasion du présent litige que l'application aux créances publiques d'un délai de prescription différent de celui qui s'applique aux créances des personnes privées serait contraire aux stipulations de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, telles qu'interprétées par la Cour européenne des droits de l'homme, notamment dans son arrêt du 25 juin 2009, Zouboulidis c/ Grèce, n° 36963/06. <br/>
<br/>
              11. Il résulte de tout ce qui précède, et sans qu'il soit besoin de se prononcer sur sa recevabilité, que la requête des sociétés requérantes ne peut qu'être rejetée. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à la charge de l'association Vent de colère ! Fédération nationale qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
              12. Il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par l'association Vent de colère ! Fédération nationale au titre des dispositions de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête des sociétés Innovent, FE Andrieu, FE Fiefs, Eoliennes de Bignan, Carbonne verre, FE Frénouville, FE Gaprée, FE Lamballe, FE Plechatel, FE Sains les Pernes, Verhaeghe gestion finance, FE Coquelles et Ferme éolienne de Croix Rault est rejetée.<br/>
Article 2 : Les conclusions présentées par l'association Vent de colère ! Fédération nationale au titre de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
Article 3 : La présente décision sera notifiée à la société Innovent, représentant désigné pour l'ensemble des sociétés requérantes, à l'association Vent de colère ! Fédération nationale, au ministre d'Etat, ministre de la transition écologique et solidaire et au Premier ministre.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
