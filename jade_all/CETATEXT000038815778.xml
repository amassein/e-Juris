<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038815778</ID>
<ANCIEN_ID>JG_L_2019_07_000000414663</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/81/57/CETATEXT000038815778.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème et 6ème chambres réunies, 24/07/2019, 414663, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-07-24</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>414663</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème et 6ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DIDIER, PINET</AVOCATS>
<RAPPORTEUR>M. Jean-Dominique Langlais</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Cécile Barrois de Sarigny</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:414663.20190724</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A...B...a demandé au tribunal administratif de Nantes d'annuler les décisions par lesquelles le ministre de l'intérieur a procédé à divers retraits de points sur son permis de conduire ainsi que la décision par laquelle il a constaté la perte de validité de ce titre pour solde de points nul et d'enjoindre au ministre de lui restituer ce titre affecté de l'intégralité des points retirés. Par un jugement n° 1406192 du 28 juillet 2017, le tribunal administratif a rejeté sa demande. <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 28 septembre et 28 décembre 2017 au secrétariat du contentieux du Conseil d'Etat, M. B... demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) de mettre à la charge du ministre de l'intérieur la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu <br/>
              - le code de la route ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Dominique Langlais, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Cécile Barrois de Sarigny, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Didier, Pinet, avocat de M.B....<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis au juge du fond que M.B..., ayant consulté le relevé figurant à son nom dans le système national des permis de conduire et constaté qu'il mentionnait une série d'infractions au code de la route, a demandé au tribunal administratif de Nantes d'annuler les décisions du ministre de l'intérieur procédant à des retraits de points consécutives à ces infractions et la décision constatant la perte de validité de son titre pour solde de points nul. Le ministre de l'intérieur a indiqué dans son mémoire en défense que le requérant n'avait jamais été titulaire d'un permis de conduire et que, par suite, aucune décision de retrait de points ni d'invalidation n'avait été prise. Par un jugement du 28 juillet 2017 contre lequel M. B...se pourvoit en cassation, le tribunal administratif a, au vu de ces indications, rejeté la requête dont il était saisi comme dépourvue d'objet et, par suite, irrecevable. <br/>
<br/>
              2. Il ressort également des pièces du dossier soumis au juge du fond qu'en réponse au mémoire en défense du ministre, M. B...a produit, le 27 février 2017, la copie d'un permis de conduire à son nom, délivré le 14 juin 1973 par la préfecture du Val-de-Marne sous le n° 94.567857. Si le ministre a présenté parallèlement, le 17 mars 2017, en réponse à une mesure supplémentaire d'instruction diligentée par le tribunal le 22 février, un second mémoire par lequel il affirmait à nouveau que le requérant n'était pas titulaire d'un permis de conduire, ce mémoire ne prenait pas parti sur l'authenticité du titre produit par l'intéressé. Le ministre n'a d'ailleurs pris connaissance de cette production que le 4 mai 2017 et s'est alors abstenu de présenter un nouveau mémoire. Dans ces conditions, le juge du fond s'est mépris sur la portée des écritures du défendeur en relevant dans son jugement que le ministre soutenait sans être démenti qu'aucun permis de conduire n'avait été délivré au requérant sous le n° 94.567857. Son jugement doit, par suite, être annulé.<br/>
<br/>
              3. Il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative. <br/>
<br/>
              4. Aux termes de l'article L. 225-1 du code de la route : " I.- Il est procédé, dans les services de l'Etat et sous l'autorité et le contrôle du ministre de l'intérieur, à l'enregistrement : / 1° De toutes informations relatives aux permis de conduire dont la délivrance est sollicitée ou qui sont délivrés en application du présent code, ainsi qu'aux permis de conduire délivrés par les autorités étrangères et reconnus valables sur le territoire national ; / 2° De toutes décisions administratives dûment notifiées portant restriction de validité, retrait, suspension, annulation et restriction de délivrance du permis de conduire, ainsi que des avertissements prévus par le présent code ; / 3° De toutes mesures de retrait du droit de faire usage du permis de conduire qui seraient communiquées par les autorités compétentes des territoires et collectivités territoriales d'outre-mer ; / 4° De toutes mesures de retrait du droit de faire usage du permis de conduire prises par une autorité étrangère et communiquées aux autorités françaises conformément aux accords internationaux en vigueur ; / 5° Des procès-verbaux des infractions entraînant retrait de points et ayant donné lieu au paiement d'une amende forfaitaire ou à l'émission d'un titre exécutoire de l'amende forfaitaire majorée ; / 6° De toutes décisions judiciaires à caractère définitif en tant qu'elles portent restriction de validité, suspension, annulation et interdiction de délivrance du permis de conduire, ou qu'elles emportent réduction du nombre de points du permis de conduire ainsi que de l'exécution d'une composition pénale ; / 7° De toute modification du nombre de points affectant un permis de conduire dans les conditions définies aux articles L. 223-1 à L. 223-8. / II.- Ces informations peuvent faire l'objet de traitements automatisés, soumis aux dispositions de la loi n° 78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés ". <br/>
<br/>
              5. La gestion du décompte des points retirés ou réattribués aux permis de conduire est assurée, sur le fondement des dispositions précitées de l'article L. 225-1 du code de la route, par un traitement automatisé d'informations à caractère nominatif dénommé " Système national des permis de conduire " (SNPC). Le relevé intégral d'information prévu à l'article L. 225-3 du code de la route, issu de ce traitement automatisé et relatif au permis de conduire de chaque conducteur, mentionne notamment les décisions par lesquelles le ministre de l'intérieur procède au retrait d'un ou plusieurs point sur ce permis ou constate sa perte de validité pour solde de points nul. <br/>
<br/>
              6. Il ressort des pièces du dossier que le relevé au nom de M. B...issu du SNPC comporte l'enregistrement d'infractions mais ne mentionne ni les références d'un permis de conduire ni l'intervention de décisions de retrait de points ou d'une décision d'invalidation pour solde de points nul. Par ailleurs, le requérant indique n'avoir jamais reçu notification de telles décisions. Enfin, le ministre de l'intérieur précise que, dans le cas où une personne ne détenant pas de permis de conduire commet des infractions au code de la route, celles-ci sont enregistrées dans le SNPC et figurent sur un relevé à son nom, sans pouvoir donner lieu à des retraits de points. Il résulte de l'ensemble de ces éléments que l'administration, n'ayant pas enregistré de permis de conduire au nom du requérant, n'a pas pris à son encontre les décisions dont l'annulation est demandée. Dans ces conditions, la requête, étant dirigée contre des décisions qui ne sont pas intervenues, est dépourvue d'objet et, par suite, irrecevable.  Il appartient toutefois au ministre, après vérification de l'authenticité du permis de conduire produit par l'intéressé, de procéder le cas échéant à l'enregistrement de ce permis dans le SNPC, sans préjudice de la possibilité de tirer les conséquences légales des infractions commises par son titulaire.<br/>
<br/>
              7. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à la charge de l'Etat, qui n'est pas, dans la présente instance, la partie perdante. <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : Le jugement du 28 juillet 2017 du tribunal administratif de Nantes est annulé. <br/>
Article 2 : La demande de M. B...est rejetée. <br/>
Article 3 : La présente décision sera notifiée à M. A...B...et au ministre de l'intérieur. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
