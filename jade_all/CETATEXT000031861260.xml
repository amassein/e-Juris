<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031861260</ID>
<ANCIEN_ID>JG_L_2015_12_000000383837</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/86/12/CETATEXT000031861260.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème SSJS, 23/12/2015, 383837, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-12-23</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>383837</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème SSJS</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MASSE-DESSEN, THOUVENIN, COUDRAY</AVOCATS>
<RAPPORTEUR>Mme Mireille Le Corre</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Xavier de Lesquen</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2015:383837.20151223</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 20 août et 20 novembre 2014 et 30 novembre 2015 au secrétariat du contentieux du Conseil d'Etat, Mme A...E...demande au Conseil d'Etat :<br/>
              1°) d'annuler la décision du 19 juin 2014 par laquelle la commission nationale d'indemnisation des avoués a rejeté " sa demande de prise en charge des éventuelles conséquences financières des décisions de justice relatives à la rupture de ses relations contractuelles avec Mme Renault-Gelot et M.B... " ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la loi n° 2011-94 du 25 janvier 2011 ;<br/>
              - le décret n° 2011-361 du 1er avril 2011 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Mireille Le Corre, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Xavier de Lesquen, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Masse-Dessen, Thouvenin, Coudray, avocat de Mme D...;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant que la loi du 25 janvier 2011 portant réforme de la représentation devant les cours d'appel a supprimé la profession d'avoué ; qu'aux termes des deux premiers alinéas de l'article 14 de cette loi : " Tout licenciement survenant en conséquence directe de la présente loi entre la publication de celle-ci et le 31 décembre 2012, ou le 31 décembre 2014 pour les personnels de la Chambre nationale des avoués près les cours d'appel, est réputé licenciement pour motif économique au sens de l'article L. 1233-3 du code du travail. / Dès lors qu'ils comptent un an d'ancienneté ininterrompue dans la profession, les salariés perçoivent du fonds d'indemnisation prévu à l'article 19 des indemnités calculées à hauteur d'un mois de salaire par année d'ancienneté dans la profession, dans la limite de trente mois. (...) " ;<br/>
<br/>
              2. Considérant qu'il résulte de l'instruction que Mme D...a exercé la profession d'avoué jusqu'au 1er janvier 2012 ; que deux de ses anciens collaborateurs, M. B..., salarié, et Mme Renault-Gelot, avocate sous le statut de collaboratrice libérale, ont engagé contre elle des actions indemnitaires devant le conseil de prud'hommes de Paris à la suite de la cessation de leurs fonctions ; que Mme D...a signalé, par une lettre du 30 novembre 2012, à la commission nationale d'indemnisation des avoués, que ces actions tendaient toutes deux à la mise en oeuvre de l'article 14 de la loi du 25 janvier 2011 cité au point précédent, qui est relatif à l'indemnisation des salariés des études d'avoué, et l'a informée de l'existence de ces deux procédures ; que par deux nouvelles lettres des 14 et 29 mai 2014, elle a informé la commission de l'évolution de ces procédures et notamment du fait que la cour d'appel de Paris avait jugé que Mme C...devait être regardée comme salariée de Mme D...pour la période courant du mois de février 1988 au 31 décembre 2011 ; qu'elle a conclu sa dernière lettre en demandant à la commission de lui indiquer si, compte tenu en particulier de la circonstance que Mme Renault-Gelot se réclamait de l'article 14 de la loi du 25 janvier 2011, elle " dev[ait] ou souhait[ait] intervenir dans ces procédures " ; qu'aucune de ces lettres, qui se bornent à communiquer à la commission nationale d'indemnisation des avoués des éléments relatifs à une procédure judiciaire en cours et à lui adresser une demande d'information sur la conduite qu'elle entend adopter vis-à-vis de cette procédure, ne saurait être regardée comme une demande d'indemnisation, ni pour elle-même, ni pour les intéressés ;  que, dès lors, à la date à laquelle la commission a pris la décision contestée du 19 juin 2014, la commission ne se trouvait pas saisie d'une demande en ce sens de cet ancien avoué ; que, par suite, en prenant la décision litigieuse, elle s'est fondée sur des faits matériellement inexacts ; que, dès lors et sans qu'il soit besoin d'examiner l'autre moyen de la requête, sa décision doit être annulée ;<br/>
<br/>
              3. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 3 000 euros à verser à MmeD..., au titre de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La décision du 19 juin 2014 de la commission nationale d'indemnisation des avoués est annulée.<br/>
Article 2 : L'Etat versera à Mme D...une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée à Mme A...E...et à la garde des sceaux, ministre de la justice.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
