<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033581193</ID>
<ANCIEN_ID>JG_L_2016_12_000000395388</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/58/11/CETATEXT000033581193.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème - 2ème chambres réunies, 09/12/2016, 395388, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-12-09</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>395388</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème - 2ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP HEMERY, THOMAS-RAQUIN</AVOCATS>
<RAPPORTEUR>M. Grégory Rzepski</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Gilles Pellissier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2016:395388.20161209</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. et Mme C...A...et M. B...A...ont demandé au tribunal administratif de Paris de condamner l'Etat à leur verser respectivement les sommes de 75 000 euros, 75 000 euros et 30 000 euros, assorties des intérêts au taux légal à compter du 11 mars 2011, et la capitalisation de ces intérêts, en réparation du préjudice que leur a causé le décès de leur fils et frère, M. D...A.... Par un jugement n° 1122978/6-2 du 26 novembre 2013, le tribunal administratif de Paris a condamné l'Etat à verser la somme de 12 500 euros chacun à M. et Mme C...A..., et la somme de 4 000 euros à M. B...A..., ainsi que les intérêts au taux légal à compter du 11 mars 2011 et la capitalisation de ces intérêts à compter du 11 mars 2012.<br/>
<br/>
              Par un arrêt n° 14PA00355 du 20 octobre 2015, la cour administrative d'appel de Paris a rejeté l'appel formé par M. et Mme C...A...et M. B...A...contre ce jugement. <br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 17 décembre 2015, 16 mars et 14 octobre 2016 au secrétariat du contentieux du Conseil d'Etat, M. et Mme C...A...et M. B...A...demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le décret n° 99-16 du 8 janvier 1999 ;<br/>
              - le décret n° 2005-796 du 15 juillet 2005 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Grégory Rzepski, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Gilles Pellissier, rapporteur public. <br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Hémery, Thomas-Raquin, avocat des consortsA....<br/>
<br/>
<br/>
<br/>1. Considérant, en premier lieu, qu'il ressort des énonciations de l'arrêt attaqué que le lieutenant Guillaume A...est décédé lors d'un accident aérien le 6 mai 2007 en Egypte, dans le cadre d'une mission de la Force Multinationale d'Observateurs au Sinaï ; que la cour administrative d'appel de Paris a estimé que la responsabilité de l'Etat était engagée du fait de négligences dans l'encadrement de l'unité de la victime, qui ont concouru au non respect des consignes de sécurité en vol, mais seulement à hauteur de la moitié des conséquences dommageables de l'accident, eu égard à l'absence de défaillance technique de l'appareil et au caractère délibéré d'un vol à très basse altitude qui, en l'absence de circonstance de nature à justifier une telle manoeuvre, était constitutif d'une faute partiellement imputable à M. A...en sa qualité de co-pilote de l'aéronef ; qu'en statuant ainsi, la cour a suffisamment motivé son arrêt ;<br/>
<br/>
              2. Considérant, en second lieu, qu'aux termes du point 2.3.1 de l'annexe au décret du 8 janvier 1999 portant réglementation de la circulation aérienne militaire, alors applicable : " Le commandant de bord est responsable de l'application par le personnel de conduite de l'aéronef des règles de l'air " ; qu'aux termes du point 2.3.2 de la même annexe : " Le pilote est responsable : / de la conduite de l'aéronef conformément aux ordres de vol et aux consignes d'utilisation ; / de l'application des règles de la CAM (circulation aérienne militaire) à la conduite de son aéronef ; il ne peut y déroger que s'il le juge nécessaire pour des motifs de sécurité ; (...) " ;  que ces dispositions, qui définissent le champ de la responsabilité du commandant de bord d'un aéronef militaire, n'excluent pas que puisse être également recherchée la responsabilité propre d'un pilote ou co-pilote, qui n'est pas le commandant de bord, en cas de violation, par celui-ci, des ordres ou des règles qu'elles mentionnent ; que, par suite, la cour n'a pas commis d'erreur de droit en imputant la faute de pilotage à la fois au pilote commandant de bord et à M. A...en sa qualité de co-pilote ; <br/>
<br/>
              3. Considérant qu'il résulte de ce qui précède que le pourvoi des consorts A...doit être rejeté ; que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de M. et Mme C...A...et de M. B...A...est rejeté.<br/>
Article 2 : La présente décision sera notifiée à M. et Mme C...A..., à M. B...A...et au ministre de la défense.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
