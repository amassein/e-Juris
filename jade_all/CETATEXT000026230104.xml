<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026230104</ID>
<ANCIEN_ID>JG_L_2012_07_000000340026</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/23/01/CETATEXT000026230104.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème et 9ème sous-sections réunies, 27/07/2012, 340026</TITRE>
<DATE_DEC>2012-07-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>340026</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème et 9ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Bernard Stirn</PRESIDENT>
<AVOCATS>SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR>M. Nicolas Labrune</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Edouard Crépey</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2012:340026.20120727</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête sommaire et le mémoire complémentaire, enregistrés les 28 mai et 30 août 2010 au secrétariat du contentieux du Conseil d'Etat, présentés pour la société AIS 2, dont le siège est 7, rue de la Baume à Paris (75008) ; la société AIS 2 demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler la délibération n° 2010-113 du 22 avril 2010 par laquelle la Commission nationale de l'informatique et des libertés (CNIL) lui a infligé un avertissement ;<br/>
<br/>
              2°) de mettre à la charge de la CNIL la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
<br/>
              Vu la loi n° 78-17 du 6 janvier 1978, modifiée notamment par la loi n° 2004-801 du 6 août 2004 ;<br/>
<br/>
              Vu le décret n° 2005-1309 du 20 octobre 2005 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Nicolas Labrune, Auditeur,<br/>
<br/>
              - les observations de la SCP Piwnica, Molinié, avocat de la société AIS 2,<br/>
<br/>
              - les conclusions de M. Edouard Crépey, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Piwnica, Molinié, avocat de la société AIS 2 ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant que, par une délibération du 22 avril 2010, la formation restreinte de la Commission nationale de l'informatique et des libertés (CNIL) a infligé à la société AIS 2 un avertissement rendu public pour avoir exploité deux traitements informatiques en méconnaissance des dispositions de la loi du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés, ainsi que, par une seconde délibération du même jour, mis celle-ci en demeure de cesser d'utiliser certaines données, de procéder aux formalités préalables auprès de la CNIL, de cesser de traiter des données non pertinentes, excessives ou inadéquates, de ne plus collecter d'informations relatives à la santé ou aux infractions concernant les candidats, enseignants et clients et enfin de définir une durée de conservation ; que la société AIS 2 demande l'annulation de cette première sanction ;<br/>
<br/>
              Sur la légalité externe de la décision attaquée :<br/>
<br/>
              En ce qui concerne la régularité de la procédure :<br/>
<br/>
              2. Considérant qu'aux termes de l'article 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales : " 1. Toute personne a droit au respect de sa vie privée et familiale, de son domicile et de sa correspondance. 2. Il ne peut y avoir ingérence d'une autorité publique dans l'exercice de ce droit que pour autant que cette ingérence est prévue par la loi et qu'elle constitue une mesure qui, dans une société démocratique, est nécessaire à la sécurité nationale, à la sûreté publique, au bien-être économique du pays, à la défense de l'ordre et à la prévention des infractions pénales, à la protection de la santé ou de la morale, ou à la protection des droits et libertés d'autrui. " ;<br/>
<br/>
              3. Considérant qu'en vertu des dispositions de l'article 44 de la loi du 6 janvier 1978, lorsque des membres ou agents de la CNIL opèrent un contrôle dans les locaux servant à la mise en oeuvre d'un traitement de données à caractère personnel, le procureur de la République territorialement compétent en est préalablement informé et que le responsable des lieux peut s'opposer à cette visite, qui ne peut alors se dérouler qu'avec l'autorisation du président du tribunal de grande instance dans le ressort duquel sont situés les locaux à visiter ou du juge délégué par lui ;<br/>
<br/>
              4. Considérant, d'une part, que si la société requérante soutient que les responsables des locaux ayant fait l'objet des contrôles sur place qui ont permis aux agents de la CNIL de constater les manquements sanctionnés par la délibération attaquée n'ont pas été informés de leur droit de s'opposer à ces visites, il ressort des pièces du dossier que M. A, qui doit être regardé comme le responsable des lieux au sens des dispositions de la loi du 6 janvier 1978, a signé un procès verbal de visite comportant la mention manuscrite de son accord pour ce contrôle et indiquant qu'il avait pris connaissance, avant le début des opérations, de la décision du président de la CNIL de faire procéder à une mission de vérification, de l'ordre de mission des agents ainsi que des dispositions de l'article 44 de la loi du 6 janvier 1978 ; qu'ainsi, dans les circonstances de l'espèce, M. A, dont la qualité de responsable des locaux n'est pas contestée, doit être regardé comme ayant été complément informé de son droit de s'opposer à ce contrôle tel qu'il découle de l'article 44 ; que, dès lors, ni les stipulations de l'article 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, ni les dispositions des articles 19 et 44 de la loi du 6 janvier 1978 n'ont été méconnues ; que ce moyen doit être écarté ;<br/>
<br/>
              5. Considérant, d'autre part, que si la société requérante soutient que le contrôle effectué par les agents de la CNIL serait intervenu dans des locaux de la société Acadomia SA situés à la même adresse que ceux de la société AIS 2, elle n'apporte aucun élément de nature à établir cette allégation ; qu'aux termes de l'article 19 de la loi du 6 janvier 1978 : " Ceux des agents qui peuvent être appelés à participer à la mise en oeuvre des missions de vérification mentionnées à l'article 44 doivent y être habilités par la commission ; cette habilitation ne dispense pas de l'application des dispositions définissant les procédures autorisant l'accès aux secrets protégés par la loi. " ; que l'article 64 du décret du 20 octobre 2005 dispose que : " les missions de contrôle sur place font l'objet d'un procès-verbal. Le procès-verbal énonce la nature, le jour, l'heure et le lieu des vérifications ou des contrôles effectués. Il indique également l'objet de la mission, les membres de celle-ci présents, les personnes rencontrées, le cas échéant, leurs déclarations, les demandes formulées par les membres de la mission ainsi que les éventuelles difficultés rencontrées. L'inventaire des pièces et documents dont les personnes chargées du contrôle ont pris copie est annexé au procès-verbal. Le procès-verbal est signé par les personnes chargées du contrôle qui y ont procédé et par le responsable des lieux ou par toute personne désignée par celui-ci. En cas de refus ou d'absence de celles-ci, mention en est portée au procès-verbal. Le procès-verbal est notifié au responsable des lieux et au responsable des traitements. " ; que lorsqu'un procès-verbal décrit le déroulement du contrôle et ses résultats, il appartient aux personnes qui en contestent les énonciations, en l'absence de dispositions leur attribuant la charge de la preuve contraire, de fournir néanmoins des éléments ou indications au soutien de leur contestation qui permettront au juge, dans le cadre du débat contradictoire, d'établir, le cas échéant, le caractère erroné ou incomplet des mentions du procès-verbal au regard de la réalité des faits ressortant du dossier issu du débat entre les parties ; qu'en l'espèce, aucun élément ne conduit à mettre en cause la validité du procès verbal, qui, par lui-même, ne permet pas de regarder le contrôle comme s'étant déroulé ailleurs que dans les locaux de la société société AIS 2 ; que ce moyen doit dès lors être écarté ;<br/>
<br/>
              En ce qui concerne le moyen tiré de l'insuffisance de motivation de la décision :<br/>
<br/>
              6. Considérant que la décision par laquelle la CNIL rend publique la sanction prononcée a le caractère d'une sanction complémentaire ; qu'aucune disposition non plus qu'aucun principe n'impose qu'elle fasse l'objet d'une motivation spécifique, distincte de la motivation d'ensemble de la sanction principale qu'elle complète ; que cette motivation d'ensemble ne saurait être regardée, dans la présente affaire, comme insuffisante, dès lors qu'elle expose l'ensemble des manquements constatés et établit leur gravité au regard des objectifs de la loi relative à l'informatique, aux fichiers et aux libertés ; qu'il suit de là que le moyen tiré de l'insuffisance de motivation de la décision attaquée doit être écarté ;<br/>
<br/>
              Sur le bien fondé de la sanction :<br/>
<br/>
              7. Considérant, en premier lieu, que si la société requérante soutient qu'elle ne saurait être regardée comme responsable des traitements incriminés au sens des dispositions des articles 3 et 45 de la loi du 6 janvier 1978, aux termes desquelles : " I. - Le responsable d'un traitement de données à caractère personnel est, sauf désignation expresse par les dispositions législatives ou réglementaires relatives à ce traitement, la personne, l'autorité publique, le service ou l'organisme qui détermine ses finalités et ses moyens. (...) ", il résulte de l'instruction que, si la société requérante est une filiale de la société Acadomia groupe SA, qu'elle exerce son activité sous la marque Acadomia et indique, sous le timbre de cette société, qu'après la sanction, des instructions correctives ont été données, elle exploite cependant elle-même les fichiers en cause, dont elle ne conteste pas être la propriétaire, pour l'exercice de sa propre activité commerciale, détermine effectivement le champ des données renseignées, la circonstance que d'autres filiales ou franchises les utilisent étant à cet égard sans incidence, et a en outre, d'une part, au cours de la procédure devant la CNIL, indiqué à la commission qu'elle entendait renoncer à la collecte de différentes données, d'autre part, devant le juge, soutenu qu'elle avait accompli les formalités nécessaires à la régularisation de l'exploitation des fichiers au regard des dispositions de la loi ;<br/>
<br/>
              8. Considérant, en deuxième lieu, que si la société requérante soutient que les faits sur lesquels la CNIL s'est fondée sont inexacts, dès lors qu'elle avait procédé à la déclaration de ces traitements, il résulte de l'instruction que différentes formalités dont se prévaut la société requérante ont été effectuées non par elle-même mais par des sociétés dont elle avait repris postérieurement l'activité ou par Acadomia Groupe SA, et que les déclarations faites par la société requérante elle-même indiquaient que les traitements en cause avaient pour objet de permettre le suivi de l'activité commerciale de soutien scolaire et n'avaient ni pour objet ni pour effet de déclarer l'exploitation dans les fichiers SEANET et SRANET des données dont le recueil et le traitement ont fait l'objet des constations de la CNIL ; que ce moyen doit être écarté ;<br/>
<br/>
              9. Considérant enfin, en troisième lieu, qu'aux termes de l'article 45 de la loi du 6 janvier 1978 : " I - La Commission nationale de l'informatique et des libertés peut prononcer un avertissement à l'égard du responsable d'un traitement qui ne respecte pas les obligations découlant de la présente loi. Elle peut également mettre en demeure ce responsable de faire cesser le manquement constaté dans un délai qu'elle fixe. Si le responsable d'un traitement ne se conforme pas à la mise en demeure qui lui est adressée, la commission peut prononcer à son encontre, après une procédure contradictoire, les sanctions suivantes : 1° Une sanction pécuniaire, dans les conditions prévues par l'article 47, à l'exception des cas où le traitement est mis en oeuvre par l'Etat " ; que l'article 46 de la même loi dispose que : " Les sanctions prévues au I et au 1° du II de l'article 45 sont prononcées sur la base d'un rapport établi par l'un des membres de la Commission nationale de l'informatique et des libertés, désigné par le président de celle-ci parmi les membres n'appartenant pas à la formation restreinte. Ce rapport est notifié au responsable du traitement, qui peut déposer des observations et se faire représenter ou assister. Le rapporteur peut présenter des observations orales à la commission mais ne prend pas part à ses délibérations. La commission peut entendre toute personne dont l'audition lui paraît susceptible de contribuer utilement à son information. La commission peut rendre publics les avertissements qu'elle prononce. Elle peut également, en cas de mauvaise foi du responsable du traitement, ordonner l'insertion des autres sanctions qu'elle prononce dans des publications, journaux et supports qu'elle désigne. Les frais sont supportés par les personnes sanctionnées. Les décisions prises par la commission au titre de l'article 45 sont motivées et notifiées au responsable du traitement. Les décisions prononçant une sanction peuvent faire l'objet d'un recours de pleine juridiction devant le Conseil d'Etat. " ; que lorsque la CNIL constate un manquement aux obligations prévues par la loi, elle peut infliger un avertissement ou adresser une mise en demeure pouvant, au terme du délai qu'elle fixe, la conduire à prononcer une sanction pécuniaire ou une injonction de cesser l'exploitation du traitement ; qu'il lui est loisible d'infliger un avertissement et d'adresser simultanément une mise en demeure sur des faits postérieurs à l'avertissement et distincts de ceux-ci ;<br/>
<br/>
              10. Considérant qu'il résulte de ce qui précède, tout d'abord, que la mise en demeure n'est pas une sanction et peut conduire à une sanction qui ne porte pas sur les mêmes faits ; qu'un avertissement pouvait être infligé en même temps qu'une mise en demeure était adoptée ; qu'ainsi, le moyen tiré de ce que les faits critiqués ne pouvaient faire l'objet d'un avertissement mais d'une simple mise en demeure est inopérant pour critiquer la décision d'infliger un avertissement ; qu'ensuite, si la sanction d'avertissement crée, lorsqu'elle est rendue publique, et, comme dans la présente affaire, largement reprise dans la presse, un dommage significatif à la personne qui en est l'objet, elle est en l'espèce proportionnée par son ampleur aux manquements multiples, durables et répétés par lesquels la société requérante, ayant pu intervenir sur un marché sans subir les coûts du contrôle interne des obligations légales auxquelles elle aurait dû s'astreindre, a exploité, en violation des règles légales de durée de conservation, des données relatives à la santé, aux condamnations, ou aux apparences de personnes physiques, en les assortissant de commentaires injurieux et méprisants ; que la circonstance qu'elle ait coopéré avec la CNIL depuis la révélation de ses agissements est sans incidence sur l'appréciation de la proportionnalité de la sanction infligée ; qu'est également sans incidence sur celle-ci, ainsi qu'il a été exposé ci-dessus, la circonstance qu'elle se soit conformée à la mise en demeure, qui portait nécessairement sur une période postérieure à celle en cause ; que, dès lors, le moyen tiré de ce que la décision attaquée méconnaîtrait le principe de proportionnalité des sanctions doit être écarté ;<br/>
<br/>
              11. Considérant qu'il résulte de tout ce qui précède que la société AIS 2 n'est pas fondée à demander l'annulation de la délibération de la CNIL du 22 avril 2010 ; que sa requête doit, par suite, être rejetée ;<br/>
<br/>
              Sur les conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              12. Considérant que ces dispositions font obstacle à ce que soit mise à la charge de l'Etat, qui n'est pas dans la présente instance la partie perdante, la somme demandée par la requérante au titre des frais exposés par elle et non compris dans les dépens ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>				D E C I D E  :<br/>
              				--------------<br/>
<br/>
Article 1er : La requête de la société AIS 2 est rejetée.<br/>
Article 2 : La présente décision sera notifiée à la société AIS 2 et à la Commission nationale de l'informatique et des libertés.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">26-07-04 DROITS CIVILS ET INDIVIDUELS. - PERSONNE RESPONSABLE DU TRAITEMENT - IDENTIFICATION - MÉTHODE DU FAISCEAU D'INDICES.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">26-07-10-02 DROITS CIVILS ET INDIVIDUELS. - PROCÈS-VERBAUX DES AGENTS DE LA CNIL (ART. 64 DU DÉCRET DU 20 OCTOBRE 2005) - CONTESTATION DU CARACTÈRE EXACT OU COMPLET DE LEURS ÉNONCIATIONS - CHARGE DE LA PREUVE PESANT SUR LES REQUÉRANTS - ABSENCE.
</SCT>
<ANA ID="9A"> 26-07-04 Recours à la méthode du faisceau d'indices pour identifier la personne responsable du traitement au sens des dispositions des articles 3 et 45 de la loi n° 78-17 du 6 janvier 1978.</ANA>
<ANA ID="9B"> 26-07-10-02 Lorsqu'un procès-verbal dressé, conformément aux dispositions de l'article 64 du décret n° 2005-1309 du 20 octobre 2005, par un agent de la CNIL dans le cadre d'un contrôle sur place décrit le déroulement du contrôle et ses résultats, il appartient aux personnes qui en contestent les énonciations, en l'absence de dispositions leur attribuant la charge de la preuve contraire, de fournir néanmoins des éléments ou indications au soutien de leur contestation qui permettront au juge, dans le cadre du débat contradictoire, d'établir, le cas échéant, le caractère erroné ou incomplet des mentions du procès-verbal au regard de la réalité des faits ressortant du dossier issu du débat entre les parties.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
