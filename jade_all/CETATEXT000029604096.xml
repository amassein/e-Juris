<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029604096</ID>
<ANCIEN_ID>JG_L_2014_10_000000349775</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/60/40/CETATEXT000029604096.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère et 6ème sous-sections réunies, 15/10/2014, 349775</TITRE>
<DATE_DEC>2014-10-15</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>349775</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère et 6ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FABIANI, LUC-THALER ; SCP GADIOU, CHEVALLIER ; SCP POTIER DE LA VARDE, BUK LAMENT</AVOCATS>
<RAPPORTEUR>M. Pascal Trouilly</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Maud Vialettes</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:349775.20141015</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 1er juin et 1er septembre 2011 au secrétariat du contentieux du Conseil d'Etat, présentés pour l'association de sauvegarde de l'environnement de la zone d'activité technologique du Plan du Bois à La Gaude (ASEZAT La Gaude), dont le siège est 25, avenue des Oliviers à La Gaude (06610), représentée par sa présidente ; l'association demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler les articles 2 à 4 de l'arrêt n° 09MA00638 du 31 mars 2011 par lequel la cour administrative d'appel de Marseille, après avoir donné acte du désistement de Mme A..., a rejeté sa requête tendant à l'annulation du jugement n° 0706822 du 15 décembre 2008 du tribunal administratif de Nice rejetant sa demande d'annulation de l'arrêté du 26 octobre 2007 par lequel le maire de la commune de La Gaude a délivré un permis de construire à la SARL Antigua ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ; <br/>
<br/>
              3°) de mettre à la charge de la commune de La Gaude et de la SARL Antigua la somme de 3 600 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de l'environnement ;<br/>
<br/>
              Vu le code de l'urbanisme ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Pascal Trouilly, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Maud Vialettes, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Gadiou, Chevallier, avocat de l'ASEZA.T. La Gaude, à la SCP Fabiani, Luc-Thaler, avocat de la commune de La Gaude et à la SCP Potier de la Varde, Buk Lament, avocat de la SARL Antigua ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que le maire de la commune de La Gaude a délivré le 26 octobre 2007 à la SARL Antigua un permis de construire pour la réalisation d'un ensemble de locaux à usage industriel destiné à la torréfaction et au conditionnement du café, sur un terrain situé dans la zone d'aménagement concerté (ZAC) du " Plan du Bois ", dite " ZAC IBM " ; que l'association de sauvegarde de l'environnement de la zone d'activité technologique du Plan du Bois à La Gaude (ASEZAT La Gaude) et certains voisins ont demandé l'annulation de ce permis de construire au tribunal administratif de Nice, qui a rejeté leur demande par un jugement du 15 décembre 2008 ; que l'association se pourvoit en cassation contre l'arrêt de la cour administrative d'appel de Marseille du 31 mars 2011 en tant qu'il rejette leur appel dirigé contre ce jugement ;<br/>
<br/>
              2. Considérant, en premier lieu, qu'aux termes de l'article L. 421-6 du code de l'urbanisme : " Le permis de construire ou d'aménager ne peut être accordé que si les travaux projetés sont conformes aux dispositions législatives et réglementaires relatives à l'utilisation des sols (...) " ; que, d'une part, en vertu de l'article R. 311-10-3 du code de l'urbanisme, applicable lors de l'approbation du règlement de la ZAC du Plan du Bois par arrêté du préfet des Alpes-Maritimes du 17 juin 1986, le règlement d'une zone d'aménagement concertée fixait notamment : " la surface de plancher développée hors oeuvre nette dont la construction est autorisée dans chaque îlot, en fonction, le cas échéant, de la nature et de l'affectation future des bâtiments " ; que le règlement de la ZAC du Plan du Bois est demeuré applicable en vertu de l'article L. 311-7 du code de l'urbanisme, en ayant les mêmes effets pour la zone intéressée qu'un plan local d'urbanisme ; qu'il résulte de l'article L. 123-3 du code de l'urbanisme, dans sa rédaction applicable à la décision litigieuse, que le plan local d'urbanisme peut, dans les ZAC, déterminer de la même façon que le faisait auparavant le règlement de la ZAC la surface dont la construction est autorisée dans chaque îlot ; que, d'autre part, aux termes de l'article L. 311-6 du même code, dans sa rédaction applicable à la décision litigieuse : " Les cessions ou concessions d'usage de terrains à l'intérieur des zones d'aménagement concerté font l'objet d'un cahier des charges qui indique le nombre de mètres carrés de surface hors oeuvre nette dont la construction est autorisée sur la parcelle cédée (...) / Le cahier des charges est approuvé lors de chaque cession ou concession d'usage par le maire ou le président de l'établissement public de coopération intercommunale, lorsque la création de la zone relève de la compétence du conseil municipal ou de l'organe délibérant de l'établissement public de coopération intercommunale, et par le préfet dans les autres cas (...) " ; que l'approbation du cahier des charges de cession de terrains par l'autorité administrative confère à ce document le caractère d'un acte réglementaire ;<br/>
<br/>
              3. Considérant qu'il résulte de ces dispositions que le nombre de mètres carrés de surface hors oeuvre nette dont la construction est autorisée sur une parcelle cédée au sein d'une ZAC est déterminé par le cahier des charges de la cession du terrain, approuvé par l'autorité administrative compétente ; qu'en l'absence d'une telle détermination, et à défaut de coefficient d'occupation des sols qui soit applicable, l'autorité chargée de la délivrance du permis de construire ne peut connaître la surface dont la construction est autorisée sur la parcelle et celle qui reste autorisée sur les autres parcelles du même îlot ; que, par suite, elle ne peut, en principe, légalement délivrer un permis de construire sur la parcelle considérée ; <br/>
<br/>
              4. Considérant toutefois, d'une part, qu'il ressort des pièces du dossier soumis aux juges du fond que le règlement de la ZAC du Plan du Bois a été approuvé par arrêté du préfet des Alpes-Maritimes du 17 juin 1986, soit antérieurement à la généralisation, par la loi du 13 décembre 2000 relative à la solidarité et au renouvellement urbains, de l'obligation d'assortir chaque cession d'un cahier des charges approuvé et que la SARL Antigua a acquis le terrain d'assiette, en 2006, auprès d'une société qui l'avait elle-même acquis auprès de l'aménageur de la zone ; que, d'autre part, il résulte des énonciations de l'arrêt attaqué que le maire disposait d'informations, notamment grâce à un certificat d'urbanisme délivré en 2005, qui lui permettaient de s'assurer que le projet de la société Antigua n'excédait pas la surface hors oeuvre nette dont la construction restait autorisée dans la ZAC ; que, dans ces conditions, la cour n'a pas commis d'erreur de droit en jugeant que le maire avait pu légalement délivrer le permis de construire alors même que la cession du terrain d'assiette n'avait pas fait l'objet d'un cahier des charges ;<br/>
<br/>
              5. Considérant, en deuxième lieu, que l'article 3 du titre I du règlement de la zone d'aménagement concerté du Plan du Bois précise que cette zone est " destinée à la construction de bureaux, de laboratoires et d'équipements d'accompagnement " ; que, toutefois, l'article UK 1 du titre II de ce même règlement, qui énumère les occupations et utilisations du sol admises dans cette zone, mentionne les " constructions à usage industriel " ainsi que les " installations classées à condition que soient mise en oeuvre toutes dispositions utiles pour les rendre compatibles avec les milieux environnant et permettre d'éviter les nuisances et les dangers éventuels " ; que, par suite, la cour n'a pas commis d'erreur de droit et ne s'est pas méprise sur la portée des dispositions de l'article 3 du titre I du règlement de cette zone d'aménagement concerté en jugeant que les auteurs de ce document ne pouvaient être regardés comme ayant voulu restreindre pour l'avenir les activités économiques susceptibles d'être accueillies dans cette zone aux seules installations nécessaires au fonctionnement de l'entreprise à l'origine de sa création et que l'autorisation délivrée à la SARL Antigua de construire un ensemble de locaux à usage industriel destiné à la torréfaction et au conditionnement du café n'était pas contraire à la réglementation de la zone d'aménagement concerté ;<br/>
<br/>
              6. Considérant, en troisième lieu, qu'il résulte de l'article R. 122-3 du code de l'environnement, dans sa rédaction applicable à la date du permis litigieux, que l'étude d'impact doit notamment contenir " (...) 1° Une analyse de l'état initial du site et de son environnement (...) ; / 2° Une analyse des effets directs et indirects, temporaires et permanents du projet sur l'environnement, et en particulier sur la faune et la flore, les sites et paysages, le sol, l'eau, l'air, le climat, les milieux naturels et les équilibres biologiques, sur la protection des biens et du patrimoine culturel et, le cas échéant, sur la commodité du voisinage (bruits, vibrations, odeurs, émissions lumineuses) ou sur l'hygiène, la santé, la sécurité et la salubrité publique (...) " ; qu'en écartant le moyen tiré du caractère insuffisant de l'étude d'impact jointe à la demande de permis de construire, après avoir estimé, au terme d'une appréciation souveraine non arguée de dénaturation, que cette étude décrivait et quantifiait de façon suffisante les conséquences sur l'environnement du fonctionnement de l'installation classée projetée, la cour n'a pas commis d'erreur de droit ; <br/>
<br/>
              7. Considérant qu'il résulte de tout ce qui précède que l'ASEZAT La Gaude n'est pas fondée à demander l'annulation de l'arrêt de la cour administrative d'appel de Marseille qu'elle attaque ; <br/>
<br/>
              8. Considérant que les conclusions de l'ASEZAT La Gaude présentées au titre de l'article L. 761-1 du code de justice administrative ne peuvent, en conséquence, qu'être rejetées ; qu'il n'y a pas lieu, dans les circonstances de l'espèce, de mettre à sa charge les sommes que la commune de La Gaude et la SARL Antigua demandent au titre des mêmes dispositions ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de l'association de sauvegarde de l'environnement de la zone d'activité technologique du Plan du Bois à La Gaude (ASEZAT La Gaude) est rejeté.<br/>
Article 2 : Les conclusions de la commune de La Gaude et de la SARL Antigua présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La présente décision sera notifiée à l'association de sauvegarde de l'environnement de la zone d'activité technologique du Plan du Bois à La Gaude, à la commune de La Gaude et à la SARL Antigua.<br/>
Copie en sera adressée à la ministre du logement, de l'égalité des territoires et de la ruralité.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">68-02-02-01 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. PROCÉDURES D'INTERVENTION FONCIÈRE. OPÉRATIONS D'AMÉNAGEMENT URBAIN. ZONES D'AMÉNAGEMENT CONCERTÉ (ZAC). - CAHIER DES CHARGES DE CESSION D'UN TERRAIN (APRÈS L'ENTRÉE EN VIGUEUR DE LA LOI SRU) - PORTÉE - DÉTERMINATION DU NOMBRE DE MÈTRES CARRÉS DE SURFACE DONT LA CONSTRUCTION EST AUTORISÉE SUR LA PARCELLE - CONSÉQUENCE DE L'ABSENCE DE CETTE DÉTERMINATION - IMPOSSIBILITÉ, EN PRINCIPE, EN L'ABSENCE DE COS, DE DÉLIVRER LÉGALEMENT UN PERMIS DE CONSTRUIRE SUR UNE TELLE PARCELLE.
</SCT>
<ANA ID="9A"> 68-02-02-01 Il résulte notamment des dispositions des articles L. 123-3 et L. 311-6 du code de l'urbanisme, dans leur rédaction issue de la loi n° 2000-1208 du 13 décembre 2000 relative à la solidarité et au renouvellement urbains (SRU), ainsi que de l'article L. 421-6 du même code, que le nombre de mètres carrés de surface hors oeuvre nette ou de surface de plancher dont la construction est autorisée sur une parcelle cédée au sein d'une ZAC est déterminé par le cahier des charges qui doit accompagner, y compris lorsque la ZAC a été créée avant l'entrée en vigueur de la loi SRU, la cession du terrain, et auquel l'approbation par l'autorité administrative compétente confère un caractère réglementaire.... ,,En l'absence d'une telle détermination, et à défaut de coefficient d'occupation des sols (COS) qui soit applicable, l'autorité chargée de la délivrance du permis de construire ne peut connaître la surface dont la construction est autorisée sur la parcelle et celle qui reste autorisée sur les autres parcelles du même îlot. Par suite, elle ne peut, en principe, légalement délivrer un permis de construire sur la parcelle considérée.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
