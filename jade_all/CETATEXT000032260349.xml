<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032260349</ID>
<ANCIEN_ID>JG_L_2016_03_000000394439</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/26/03/CETATEXT000032260349.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème SSJS, 18/03/2016, 394439, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-03-18</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>394439</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème SSJS</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MATUCHANSKY, VEXLIARD, POUPOT ; SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR>M. Jean-Baptiste de Froment</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Xavier de Lesquen</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2016:394439.20160318</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société Guy Dauphin Environnement a demandé au juge des référés du tribunal administratif de Caen, sur le fondement de l'article L. 521-1 du code de justice administrative, de suspendre l'exécution de l'arrêté du préfet de l'Orne du 25 septembre 2015 fixant des mesures d'urgence pour prévenir des dangers graves et imminents pour l'environnement et la santé publique sur le site de l'installation classée exploitée par la société à Nonant-le-Pin. Par une ordonnance n° 1501939 du 22 octobre 2015, le juge des référés a suspendu l'exécution de cette décision.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 6 novembre, 23 novembre 2015 et 18 janvier 2016 au secrétariat du contentieux du Conseil d'Etat, le ministre de l'écologie, du développement durable et de l'énergie demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) statuant en référé, de rejeter la demande de suspension de la société Guy Dauphin Environnement ;<br/>
<br/>
<br/>
<br/>
<br/>
              3°) de mettre à la charge de la société Guy Dauphin Environnement la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'environnement ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Baptiste de Froment, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Xavier de Lesquen, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Matuchansky, Vexliard, Poupot, avocat de la ministre de l'environnement, de l'énergie et de la mer, chargée des relations internationales sur le climat, et à la SCP Piwnica, Molinié, avocat de la société Guy Dauphin Environnement ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 25 février 2016, présentée par la société Guy Dauphin Environnement ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis au juge des référés que la société Guy Dauphin Environnement exploite à Nonant-le-Pin (Orne) une installation de stockage de déchets non dangereux et un centre de tri qui ont fait l'objet d'une autorisation d'exploiter délivrée par jugement du tribunal administratif de Caen du 18 février 2011, et dont les modalités de fonctionnement ont été fixées par un arrêté de prescriptions du préfet de l'Orne du 12 juillet 2011 ; qu'à la suite d'un rapport de l'inspection des installations classées pour la protection de 1'environnement du 22 septembre 2015, ayant relevé quinze non-conformités à l'arrêté de prescriptions concernant la décharge et une non-conformité concernant le centre de tri, le préfet de l'Orne a, par un arrêté du 25 septembre 2015, fixé des mesures d'urgence pour prévenir des dangers graves et imminents pour l'environnement et la sécurité publique, prévu que l'apport de déchets sur l'installation de Nonant-le-Pin était interdit et que cette interdiction prendrait fin lorsque la levée des non-conformités affectant les alvéoles de stockage des déchets aurait été constatée ; que, sur demande de la société exploitante, le juge des référés, a, par l'ordonnance attaquée, suspendu l'exécution de cet arrêté ;<br/>
<br/>
              2. Considérant qu'aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer en l'état de l'instruction un doute sérieux quant à la légalité de la décision. " ;<br/>
<br/>
              3. Considérant qu'il résulte de ces dispositions que la condition d'urgence à laquelle est subordonné le prononcé d'une mesure de suspension doit être regardée comme remplie lorsque la décision administrative contestée préjudicie de manière suffisamment grave et immédiate à un intérêt public, à la situation du requérant ou aux intérêts qu'il entend défendre ; qu'il appartient au juge des référés, saisi d'une demande tendant à la suspension d'une telle décision, d'apprécier concrètement, compte tenu des justifications fournies par le requérant, si les effets de celle-ci sur la situation de ce dernier ou, le cas échéant, des personnes concernées, sont de nature à caractériser une urgence justifiant que, sans attendre le jugement de la requête au fond, l'exécution de la décision soit suspendue ;<br/>
<br/>
              4. Considérant, d'une part, qu'il ressort des pièces du dossier soumis au juge des référés que si le fonctionnement de la plate-forme de tri n'est, selon le dossier de demande d'autorisation d'exploiter, qu'en partie lié à celui du centre de stockage de déchets, son exploitation est en réalité inséparable de celle du centre de stockage, dès lors que la moitié des déchets triés, ne pouvant être valorisée, est destinée à être acheminée vers le centre de stockage ; que l'activité de tri ne peut ainsi être poursuivie indépendamment de l'activité de stockage ; que, d'autre part, il ressort des pièces du dossier soumis au juge des référés que la réalisation de travaux sur l'installation de stockage constituait un préalable à toute reprise de l'activité sur cette installation et que le centre de tri ne pouvait, dès lors, pas non plus être remis en fonctionnement avant l'achèvement de ces travaux ; que, par suite, en estimant que la décision contestée avait pour effet d'empêcher la reprise de l'activité du centre de tri, le juge des référés a entaché son ordonnance de dénaturation des pièces du dossier ;<br/>
<br/>
              5. Considérant qu'il résulte de ce qui précède que, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, le ministre de l'écologie, du développement durable et de l'environnement est fondé à demander l'annulation de l'ordonnance qu'il attaque ; <br/>
<br/>
              6. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de statuer sur la demande en référé en application des dispositions de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              7. Considérant qu'il résulte de ce qui a été dit ci-dessus que l'interruption de l'activité sur le site Nonant-le-Pin est la conséquence immédiate non de l'arrêté litigieux, mais de la nécessité, pour la société requérante, de réaliser des travaux sur l'installation de stockage des déchets ; que, par suite, la condition d'urgence justifiant que, sans attendre que le tribunal juge la requête au fond, l'exécution de l'arrêté du 25 septembre 2015 soit suspendue ne peut être regardée, en l'espèce, comme remplie ; <br/>
<br/>
              8. Considérant qu'il résulte de ce qui précède, sans qu'il soit besoin de se prononcer sur l'existence d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de l'arrêté contesté, que la demande de suspension présentée par la société Guy Dauphin Environnement doit être rejetée ;<br/>
<br/>
              9. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat, qui n'est pas, dans la présente instance, la partie perdante ; qu'il y a lieu, en revanche, dans les circonstances de l'espèce, de mettre à la charge de la société Guy Dauphin Environnement, sur le fondement des mêmes dispositions, la somme de 3 000 euros à verser à l'Etat ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : L'ordonnance du juge des référés du tribunal administratif de Caen du 22 octobre 2015 est annulée.<br/>
<br/>
Article 2 : La demande de la société Guy Dauphin Environnement devant le tribunal administratif de Caen et ses conclusions sur le fondement de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 3 : La société Guy Dauphin Environnement versera à l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : La présente décision sera notifiée à la ministre de l'environnement, de l'énergie et de la mer, chargée des relations internationales sur le climat et à la société Guy Dauphin Environnement. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
