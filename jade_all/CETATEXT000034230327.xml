<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034230327</ID>
<ANCIEN_ID>JG_L_2017_03_000000388404</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/23/03/CETATEXT000034230327.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème - 9ème chambres réunies, 20/03/2017, 388404, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-03-20</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>388404</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème - 9ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LYON-CAEN, THIRIEZ ; SCP ROCHETEAU, UZAN-SARANO</AVOCATS>
<RAPPORTEUR>M. Vincent Villette</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Aurélie Bretonneau</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:388404.20170320</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Par une décision du 11 juillet 2016, le Conseil d'Etat statuant au contentieux, saisi sur renvoi de la cour administrative d'appel de Nancy en application de l'article R. 351-2 du code de justice administrative, de la requête de M. A...-C... B...tendant à l'annulation du jugement n° 1201724 du tribunal administratif de Strasbourg du 4 décembre 2013 en tant qu'il n'a que partiellement fait droit à sa demande d'annulation de la décision du 24 janvier 2012 par laquelle le président de l'office public de l'habitat (OPH) Moselis a refusé d'indemniser l'intégralité des frais de procédure qu'il a engagés pour se défendre dans le cadre d'une procédure pénale dirigée contre lui et d'une procédure pénale dans laquelle il a porté plainte avec constitution de partie civile, a sursis à statuer jusqu'à ce que le Tribunal des conflits ait tranché la question de savoir quel est l'ordre de juridiction compétent pour connaître de ce litige.<br/>
<br/>
              Par une décision n° 4070 du 14 novembre 2016, le Tribunal des conflits a déclaré la juridiction administrative seule compétente pour connaître de l'action relative aux conditions de mise en oeuvre de la protection fonctionnelle intentée par M. B...à l'encontre de l'OPH l'habitat Moselis. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier, y compris celles visées par la décision du Conseil d'Etat statuant au contentieux du 11 juillet 2016 ;<br/>
<br/>
              Vu :<br/>
              - le code général des collectivités territoriales ;<br/>
              - la loi n° 83-634 du 13 juillet 1983 ;<br/>
              - la loi du 24 mai 1872 relative au Tribunal des conflits ; <br/>
              - le décret n° 2015-233 du 27 février 2015 ; <br/>
              - le code de justice administrative ;<br/>
;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Vincent Villette, auditeur,  <br/>
<br/>
              - les conclusions de Mme Aurélie Bretonneau, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Lyon-Caen, Thiriez, avocat de M. A...-louis B...et à la SCP Rocheteau, Uzan-Sarano, avocat de l'office public de l'habitat Moselis ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que M. A...-C... B...a sollicité de l'office public de l'habitat (OPH) Moselis le bénéfice de la protection à laquelle les agents publics ont droit du fait de leurs fonctions, en raison de la procédure pénale qui avait été diligentée à son encontre pour des faits commis alors qu'il était président du conseil d'administration de l'office public d'aménagement et de construction (OPAC) de la Moselle, auquel l'OPH Moselis a succédé. Par un arrêt n° 10NC00924 du 4 août 2011, devenu définitif, la cour administrative d'appel de Nancy a annulé les décisions, implicites puis explicites, de l'OPH Moselis lui refusant le bénéfice de cette protection. A la suite de cet arrêt, M. B...a demandé à l'OPH de prendre en charge la somme de 105 498,66 euros au titre des frais engagés pour sa défense dans le cadre des poursuites pénales dont il avait fait l'objet ainsi qu'une somme de 50 686,48 euros au titre de la procédure pénale qu'il avait lui-même engagée contre quatre salariés de l'office. Il a saisi le tribunal administratif de Strasbourg d'une demande d'annulation pour excès de pouvoir de la décision du 24 janvier 2012 par laquelle le président de l'OPH a, d'une part, limité à 23 921,82 euros la prise en charge des frais qu'il avait exposés pour se défendre dans le cadre de la procédure pénale engagée contre lui, d'autre part, refusé toute prise en charge des frais afférents à la seconde procédure pénale, engagée à son initiative à l'encontre de quatre salariés de l'office. Par un jugement du 4 décembre 2013, le tribunal administratif de Strasbourg a annulé cette décision en tant seulement qu'elle refusait à M. B...le versement d'une somme de 2 392 euros correspondant aux frais d'avocat qu'il avait exposés devant la Cour de cassation dans le cadre de la procédure pénale engagée contre lui. M. B...conteste ce jugement en tant qu'il a rejeté le surplus de ses conclusions. Par une décision du 11 juillet 2016, le Conseil d'Etat, statuant au contentieux, a saisi le Tribunal des conflits de la question de compétence relative aux rapports entre un établissement public industriel et commercial et le président de son conseil d'administration. Par une décision du 14 novembre 2016, le Tribunal des conflits a jugé que les liens existant entre une personne publique et l'organe chargé de son administration sont des rapports de droit public justifiant la compétence de la juridiction administrative, sans qu'y fasse obstacle, en l'espèce, le fait que, comme l'OPH Moselis qui lui a succédé, l'OPAC de la Moselle a la nature d'un établissement public à caractère industriel et commercial.<br/>
<br/>
              Sur la compétence du Conseil d'Etat :<br/>
<br/>
              2. Aux termes de l'article R. 811-1 du code de justice administrative, dans sa rédaction applicable au litige : " Toute partie présente dans une instance devant le tribunal administratif (...) peut interjeter appel contre toute décision juridictionnelle rendue dans cette instance. / Toutefois, dans les litiges énumérés aux 1°, 4°, 5°, 6°, 7°, 8° et 9° de l'article R. 222-13, le tribunal administratif statue en premier et dernier ressort. Il en va de même pour les litiges visés aux 2° et 3° de cet article, sauf pour les recours comportant des conclusions tendant au versement ou à la décharge de sommes d'un montant supérieur au montant déterminé par les articles R. 222-14 et R. 222-15 ". Aux termes de l'article R. 222-13 du même code, dans sa rédaction applicable au litige : " Le président du tribunal administratif ou le magistrat qu'il désigne à cette fin et ayant atteint au moins le grade de premier conseiller ou ayant une ancienneté minimale de deux ans statue en audience publique et après audition du rapporteur public, sous réserve de l'application de l'article R. 732-1-1 : (...) / 2° Sur les litiges relatifs à la situation individuelle des fonctionnaires ou agents de l'Etat et des autres personnes ou collectivités publiques (...) à l'exception de ceux concernant l'entrée au service, la discipline et la sortie du service (...) ". <br/>
<br/>
              3. Pour renvoyer au Conseil d'Etat le jugement de la requête présentée par M. B..., la cour administrative d'appel de Nancy a considéré que celle-ci, qui tendait à l'annulation de la décision par laquelle l'office public de l'habitat Moselis a refusé de lui rembourser la totalité des frais exposés dans le cadre des poursuites pénales dont il a fait l'objet et des actions judiciaires qu'il a lui-même engagées, à la suite de sa mise en cause à raison de ses fonctions au sein de Moselis, avait trait à un litige portant sur la situation individuelle d'un agent public sans concerner l'entrée au service, la discipline, ni la sortie du service. Après avoir relevé que le requérant n'avait pas présenté de conclusions indemnitaires dans sa demande, elle en a déduit que, en vertu des dispositions rappelées au point 2, le tribunal administratif a statué en premier et dernier ressort.<br/>
<br/>
              4. Les dispositions de l'article R. 811-1 du code de justice administrative, dans leur rédaction applicable au litige, ouvraient la voie de l'appel à l'encontre des jugements statuant sur des recours qui, quoique relatifs à la situation individuelle des fonctionnaires ou agents de l'Etat et des autres personnes ou collectivités publiques sans concerner l'entrée au service, la discipline et la sortie du service, comportaient des conclusions tendant au versement ou à la décharge de sommes d'un certain montant. Il suit de là que la cour administrative d'appel de Nancy a commis une erreur de droit en subordonnant la contestation, par la voie de l'appel, d'un jugement portant sur un litige d'une telle nature à la présence de conclusions indemnitaires. Dès lors que le litige soumis par M. B...au tribunal administratif de Strasbourg ne portait pas sur le principe du droit au bénéfice de la protection fonctionnelle, qui lui avait été reconnu par un arrêt définitif du 4 août 2011 de la cour administrative d'appel de Nancy, mais uniquement sur le montant des sommes auxquelles il pouvait légalement prétendre à ce titre, ses conclusions doivent être regardées comme tendant au remboursement d'une somme. Eu égard au montant des sommes demandées, supérieur au seuil de 10 000 euros déterminé par l'article R. 222-14 du code de justice administrative dans sa rédaction alors applicable, le jugement du 4 décembre 2013 du tribunal administratif de Strasbourg, qui n'a pas été rendu en premier et dernier ressort, est susceptible d'appel. Il s'ensuit que la requête de M. B...dirigée contre ce jugement ne relève pas de la compétence du Conseil d'Etat, juge de cassation, mais de celle de la cour administrative d'appel de Nancy. Il y a lieu, dès lors, d'en attribuer le jugement à cette cour. <br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement de la requête de M. B...est attribué à la cour administrative d'appel de Nancy. <br/>
<br/>
Article 2 : La présente décision sera notifiée à M. A...-C... B...et à l'office public de l'habitat Moselis.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
