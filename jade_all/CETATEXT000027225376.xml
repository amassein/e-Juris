<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027225376</ID>
<ANCIEN_ID>JG_L_2013_03_000000343361</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/22/53/CETATEXT000027225376.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  4ème sous-section jugeant seule, 25/03/2013, 343361, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-03-25</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>343361</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 4ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FABIANI, LUC-THALER</AVOCATS>
<RAPPORTEUR>M. Hervé Guichon</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Gaëlle Dumortier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2013:343361.20130325</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi, enregistré le 17 septembre 2010 au secrétariat du contentieux du Conseil d'Etat, présenté par le ministre de l'éducation nationale, porte-parole du gouvernement ; le ministre demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler le jugement n° 0801893 du 22 juillet 2010 par lequel le tribunal administratif d'Amiens a, d'une part, annulé la décision du 30 mai 2008 du recteur de l'académie d'Amiens rejetant la demande de Mme A...Roll tendant à la prise en compte, au titre de l'ancienneté, de ses années de service accomplies en qualité de surveillant d'externat et de maître d'internat pour son reclassement dans l'échelle de rémunération des professeurs certifiés, et, d'autre part, enjoint au recteur de procéder au réexamen de la situation de l'intéressée ; <br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter la demande de Mme Roll ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de l'éducation ; <br/>
<br/>
              Vu le décret n° 51-1423 du 5 décembre 1951 ; <br/>
<br/>
              Vu le décret n° 64-217 du 10 mars 1964 ; <br/>
<br/>
              Vu le décret n° 85-1079 du 7 octobre 1985 ;<br/>
<br/>
              Vu le décret n° 90-1003 du 7 novembre 1990 ; <br/>
<br/>
                                                    Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Hervé Guichon, Maître des Requêtes en service extraordinaire,  <br/>
<br/>
              - les observations de la SCP Fabiani, Luc-Thaler, avocat de Mme Roll,<br/>
<br/>
              - les conclusions de Mme Gaëlle Dumortier, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Fabiani, Luc-Thaler, avocat de Mme Roll ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis au juge du fond que Mme Roll, maître contractuel de l'enseignement privé, a accédé, le 1er septembre 2003, par inscription sur une liste d'aptitude, à l'échelle de rémunération des professeurs certifiés en vertu de la procédure prévue par l'article 1er du décret du 7 novembre 1990, alors applicable et désormais repris à l'article R. 914-66 du code de l'éducation ; que, par un recours gracieux du 26 mai 2008, elle a demandé au recteur de l'académie d'Amiens la prise en compte dans son reclassement des années de service qu'elle avait accomplies, avant son entrée dans la fonction enseignante, en qualité de surveillante d'externat et de maîtresse d'internat, de 1979 à 1982 ; que par un jugement du 22 juillet 2010 contre lequel le ministre se pourvoit en cassation, le tribunal administratif d'Amiens a annulé la décision du 30 mai 2008 du recteur rejetant cette demande et a enjoint à ce dernier de procéder au réexamen de la situation de l'intéressée en prenant en compte son ancienneté acquise à ce titre ; <br/>
<br/>
              2. Considérant qu'aux termes du deuxième alinéa de l'article 10 du décret du 10 mars 1964 relatif aux maîtres contractuels et agréés des établissements d'enseignement privés sous contrat, alors applicable et désormais repris à l'article R. 914-60 du code de l'éducation : " Lorsqu'un maître change d'échelle de rémunération, il est reclassé selon les mêmes modalités que les agents exerçant dans l'enseignement public. " ; qu'aux termes de l'article 11 du décret du 5 décembre 1951 fixant les règles suivant lesquelles doit être déterminée l'ancienneté du personnel nommé dans l'un des corps de fonctionnaires de l'enseignement relevant du ministère de l'éducation nationale : " Sont considérés comme ayant été accomplis dans les grades indiqués dans le tableau suivant et entrent en ligne de compte pour le calcul de l'ancienneté dans leurs corps et grade, les services accomplis en qualité de : / (...) maître d'internat ou surveillant d'externat des lycées, collèges et établissements de formation (...) " ; <br/>
<br/>
              3. Considérant, toutefois, qu'aux termes de l'article 9 du décret du 7 novembre 1990 fixant les conditions exceptionnelles d'accès des maîtres des établissements d'enseignement privés sous contrat aux échelles de rémunération des professeurs certifiés, alors applicable et désormais repris au premier alinéa de l'article R. 914-74 du code de l'éducation : " Par dérogation aux dispositions du deuxième alinéa de l'article 10 du décret du 10 mars 1964 susvisé, les maîtres bénéficiant d'une promotion en application du présent décret sont classés dans leur nouvelle échelle de rémunération à l'échelon comportant un indice égal ou, à défaut, immédiatement supérieur à celui qu'ils détenaient dans leur échelle de rémunération d'origine. / Dans la limite de l'ancienneté exigée pour l'accès à l'échelon supérieur dans leur nouvelle échelle de rémunération, ils conservent l'ancienneté dans l'échelon qu'ils détenaient dans leur échelle de rémunération d'origine si leur promotion leur procure une augmentation de traitement inférieure à celle qu'entraînerait dans leur ancienne échelle la promotion à l'échelon supérieur ou, dans le cas où ils sont déjà à l'échelon terminal, à celle qui résultait de leur dernière promotion. " ; <br/>
<br/>
              4. Considérant que si les dispositions de l'article L. 914-1 du code de l'éducation étendent les mesures et règles générales applicables aux maîtres titulaires de l'enseignement public qu'elles énoncent, notamment en matière de promotion et d'avancement, aux maîtres ayant le même niveau de formation qui exercent dans l'enseignement privé sous contrat, elles n'ont ni pour objet ni pour effet de supprimer toute différence de traitement dans la gestion de la situation respective de ces deux catégories d'enseignants ; qu'ainsi, les dispositions de l'article 9 du décret du 7 novembre 1990 ont pu prévoir, sans porter atteinte à la règle générale d'égalisation des situations posée par l'article L. 914-1, les conditions exceptionnelles d'accès des maîtres des établissements d'enseignement privés sous contrat bénéficiant d'une promotion aux échelles de rémunération des professeurs certifiés mentionnées ci-dessus, ces conditions étant, au demeurant, équivalentes à celles prévues par le décret du 7 octobre 1985 pour le recrutement exceptionnel d'adjoints d'enseignement dans le corps des professeurs certifiés en dérogation aux règles habituelles de recrutement par promotion interne ; <br/>
<br/>
              5. Considérant que, dès lors, en déduisant de la combinaison des dispositions de l'article L. 914-1 et de celles de l'article 11 du décret du 5 décembre 1951 que les services effectués par Mme Roll en qualité de maîtresse d'internat et de surveillante d'externat devaient être pris en compte, au titre de l'ancienneté, à l'occasion de son reclassement dans l'échelle de rémunération des professeurs certifiés à laquelle elle avait eu accès par la voie de la liste d'aptitude prévue par l'article 1er du décret du 7 novembre 1990, alors que les dispositions de l'article 9 du même décret y faisaient obstacle, le tribunal administratif a commis une erreur de droit ; que le ministre est, par suite, fondé à demander l'annulation du jugement attaqué ; <br/>
<br/>
              6. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative ;  <br/>
<br/>
              7. Considérant qu'il résulte de ce qui précède que le recteur de l'académie d'Amiens n'a pas commis d'erreur de droit en se fondant sur les dispositions de l'article 9 du décret du 7 novembre 1990 pour refuser à Mme Roll la prise en compte dans son ancienneté, lors de son reclassement dans l'échelle de rémunération des professeurs certifiés, des services accomplis en qualité de maîtresse d'internat et de surveillante d'externat ; que Mme Roll n'est, dès lors, pas fondée à demander l'annulation pour excès de pouvoir de la décision attaquée ; que, par suite, ses conclusions à fins d'injonction doivent également être rejetées ; <br/>
<br/>
              8. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante ; <br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du 22 juillet 2010 du tribunal administratif d'Amiens est annulé.<br/>
Article 2 : La demande présentée par Mme Roll devant le tribunal administratif d'Amiens et ses conclusions présentées devant le Conseil d'Etat au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La présente décision sera notifiée au ministre de l'éducation nationale et à Mme A... Roll.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
