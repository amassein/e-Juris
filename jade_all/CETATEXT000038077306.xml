<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038077306</ID>
<ANCIEN_ID>JG_L_2019_01_000000408437</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/07/73/CETATEXT000038077306.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème chambre, 30/01/2019, 408437, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-01-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>408437</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP SEVAUX, MATHONNET</AVOCATS>
<RAPPORTEUR>Mme Ophélie Champeaux</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emilie Bokdam-Tognetti</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2019:408437.20190130</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A...B...a demandé au tribunal administratif de Lyon de prononcer la décharge des cotisations supplémentaires d'impôt sur le revenu et de contributions sociales auxquelles il a été assujetti au titre de l'année 2008. Par un jugement nos 1204506, 1301315 du 7 avril 2015, le tribunal a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 15LY01897 du 27 décembre 2016, la cour administrative d'appel de Lyon a rejeté l'appel formé par M. B...contre ce jugement.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 27 février 2017, 29 mai 2017 et 15 mai 2018 au secrétariat du contentieux du Conseil d'Etat, M. B...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L.761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code général des impôts ; <br/>
              - le code de justice administrative ; <br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Ophélie Champeaux, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Emilie Bokdam-Tognetti, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Sevaux, Mathonnet, avocat de M.B....<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que par deux actes des 14 et 25 mars 2008, la société La Malosse a vendu à M.B..., au prix unitaire de 15,23 euros, 3 221 titres de la société Loire Offset Plus, devenue Loire Offset Titoulet. A la suite de la vérification de comptabilité de la société La Malosse, l'administration a estimé que le prix payé avait été délibérément minoré, sans contrepartie, par rapport à la valeur vénale de ces titres, qu'elle a évaluée à 64,93 euros et a imposé, entre les mains de M.B..., la somme résultant de la différence entre ce dernier montant et celui retenu par les parties à la transaction, comme constituant une libéralité ayant donné lieu à une distribution occulte par la société La Malosse. M. B...se pourvoit en cassation contre l'arrêt du 27 décembre 2016 par lequel la cour administrative d'appel de Lyon a rejeté l'appel qu'il avait formé contre le jugement du 7 avril 2015 du tribunal administratif de Lyon rejetant sa demande tendant à la décharge des cotisations supplémentaires d'impôt sur le revenu et de contributions sociales auxquelles il a été assujetti au titre de l'année 2008. <br/>
<br/>
              2. En vertu du 3 de l'article 158 du code général des impôts, sont notamment imposables à l'impôt sur le revenu, dans la catégorie des revenus de capitaux mobiliers, les revenus considérés comme distribués en  application des articles 109 et suivants du même code. Aux termes de  l'article 111 du même code : " Sont notamment considérés comme revenus distribués:/ (...) c) Les rémunérations et avantages occultes (...) ". En cas d'acquisition par une société de titres à un prix que les parties ont délibérément majoré par rapport à la valeur vénale de l'objet de la transaction, ou, s'il s'agit d'une vente, délibérément minoré, sans que cet écart de prix comporte de contrepartie, l'avantage ainsi octroyé doit être requalifié comme une libéralité représentant un avantage occulte constitutif d'une distribution de revenus au sens des dispositions précitées du c) de l'article 111 du code général des impôts. La preuve d'une telle distribution occulte doit être regardée comme apportée par l'administration lorsqu'est établie l'existence, d'une part, d'un écart significatif entre le prix convenu et la valeur vénale du bien cédé, d'autre part, d'une intention, pour la société, d'octroyer, et, pour le cocontractant, de recevoir, une libéralité du fait des conditions de la cession.<br/>
<br/>
              3. La valeur vénale des titres non admis à la négociation sur un marché réglementé doit être appréciée compte tenu de tous les éléments dont l'ensemble permet d'obtenir un chiffre aussi voisin que possible de celui qui aurait résulté du jeu normal de l'offre et de la demande à la date à laquelle la cession est intervenue. Cette valeur doit être établie, en priorité, par référence à la valeur qui ressort de transactions portant, à la même époque, sur des titres de la société, dès lors que cette valeur ne résulte pas d'un prix de convenance.<br/>
<br/>
              4. Pour juger que l'administration avait pu à bon droit retenir le prix de 64,93 euros comme valeur de référence, la cour a relevé d'une part, qu'il s'agissait du prix auquel la société La Malosse avait acquis des titres de la même société Loire Offset Titoulet à une date se situant entre les deux cessions litigieuses et d'autre part, que le prix payé par M. B... avait été calculé par référence à un pacte d'actionnaires ne liant pas la société La Malosse et ne pouvant refléter la valeur de l'entreprise à la date de la transaction. En statuant ainsi, sans rechercher si le prix de 64,93 euros était aussi voisin que possible de celui qui aurait résulté du jeu normal de l'offre et de la demande ou si au contraire, comme le soutenait le requérant, il s'expliquait par les circonstances particulières de l'acquisition en cause qui lui conféraient le caractère d'un prix de convenance, la cour, dont l'arrêt est, sur ce point, insuffisamment motivé, a commis une erreur de droit. Dès lors, et sans qu'il soit besoin de statuer sur les autres moyens du pourvoi, M. B... est fondé à demander l'annulation de l'arrêt attaqué. <br/>
<br/>
              5. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 1 500 euros à verser à M. B...en application de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 27 décembre 2016 de la cour administrative d'appel de Lyon est annulé. <br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Lyon. <br/>
Article 3 : L'Etat versera à M. B...une somme de 1 500 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à M. A...B...et au ministre de l'action et des comptes publics. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
