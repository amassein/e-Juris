<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000035179887</ID>
<ANCIEN_ID>JG_L_2017_07_000000409475</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/35/17/98/CETATEXT000035179887.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère - 6ème chambres réunies, 12/07/2017, 409475</TITRE>
<DATE_DEC>2017-07-12</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>409475</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère - 6ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LYON-CAEN, THIRIEZ</AVOCATS>
<RAPPORTEUR>Mme Sandrine Vérité</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Rémi Decout-Paolini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:409475.20170712</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. C...D...a demandé au tribunal administratif de Toulouse d'annuler les opérations électorales qui se sont déroulées le 3 janvier 2017 en vue de l'élection de deux conseillers communautaires de la commune de Muret à la communauté de communes " le Muretain Agglo ", à l'issue desquelles ont été proclamés élus M. C...E...et Mme B...A.... Par un jugement n° 1700072 du 3 mars 2017, le tribunal administratif de Toulouse a annulé ces élections.<br/>
<br/>
              Par une requête sommaire et un mémoire complémentaire, enregistrés les 3 avril et 3 mai 2017 au secrétariat du contentieux du Conseil d'Etat, M.E..., Mme A...et la commune de Muret demandent au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler le jugement du 3 mars 2017 du tribunal administratif de Toulouse ;<br/>
<br/>
              2°) de rejeter la protestation de M.D... ;<br/>
<br/>
              3°) de mettre à la charge de ce dernier la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative, au titre des frais exposés tant en première instance qu'en appel.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code électoral ;<br/>
              - le code général des collectivités territoriales ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Sandrine Vérité, maître des requêtes en service extraordinaire, <br/>
<br/>
              - les conclusions de M. Rémi Decout-Paolini, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Lyon-Caen, Thiriez, avocat de M. C...E..., de Mme B...A...et de la commune de Muret.<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Par un arrêté du 24 novembre 2016, le préfet de la Haute-Garonne a prononcé la fusion de la communauté d'agglomération du Muretain, dont était membre la commune de Muret, avec la communauté de communes Axe sud et la communauté de communes rurales des coteaux du Savès et de l'Aussonnelle. Par un arrêté du 16 décembre suivant, le préfet a procédé à la répartition des sièges de conseillers communautaires entre les communes membres de la communauté " Le Muretain Agglo " ainsi créée, en fixant à douze le nombre de conseillers communautaires de la commune de Muret, qui était précédemment représentée par treize conseillers communautaires au sein de la communauté d'agglomération du Muretain. Les douze conseillers ont été élus par le conseil municipal de Muret le 16 décembre 2016, sur le fondement du c du 1° de l'article L. 5211-6-2 du code général des collectivités territoriales. Toutefois, le 30 décembre 2016, quatre conseillers municipaux ont démissionné de leur mandat, dont deux avaient été élus au conseil communautaire le 16 décembre précédent. Le 3 janvier 2017, le conseil municipal a procédé au remplacement de ces deux conseillers communautaires par une élection organisée selon les modalités prévues au b du 1° de l'article L. 5211-6-2, à l'issue de laquelle ont été proclamés élus M. E...et MmeA.... Ces derniers, ainsi que la commune de Muret, demandent l'annulation du jugement du 3 mars 2017 par lequel le tribunal administratif de Toulouse a annulé l'élection et enjoint à la commune de procéder à une nouvelle élection.<br/>
<br/>
              Sur l'élection des conseillers communautaires :<br/>
<br/>
              2. Aux termes de l'article L. 5211-6-2 du code général des collectivités territoriales, dans sa rédaction applicable à l'élection en litige : " Par dérogation aux articles L. 5211-6 et L. 5211-6-1, entre deux renouvellements généraux des conseils municipaux : / 1° En cas de création d'un établissement public de coopération intercommunale à fiscalité propre, de fusion entre plusieurs établissements publics de coopération intercommunale dont au moins l'un d'entre eux est à fiscalité propre, d'extension du périmètre d'un tel établissement par l'intégration d'une ou de plusieurs communes ou la modification des limites territoriales d'une commune membre ou d'annulation par la juridiction administrative de la répartition des sièges de conseiller communautaire, il est procédé à la détermination du nombre et à la répartition des sièges de conseiller communautaire dans les conditions prévues à l'article L.5211-6-1. / (...) / Dans les communes dont le conseil municipal est élu selon les modalités prévues au chapitre III du titre IV dudit livre Ier : / a) Si le nombre de sièges attribués à la commune est supérieur ou égal au nombre de conseillers communautaires élus à l'occasion du précédent renouvellement général du conseil municipal, les conseillers communautaires précédemment élus font partie du nouvel organe délibérant ; le cas échéant, les sièges supplémentaires sont pourvus par élection dans les conditions prévues au b ; / b) S'il n'a pas été procédé à l'élection de conseillers communautaires lors du précédent renouvellement général du conseil municipal ou s'il est nécessaire de pourvoir des sièges supplémentaires, les conseillers concernés sont élus par le conseil municipal parmi ses membres et, le cas échéant, parmi les conseillers d'arrondissement au scrutin de liste à un tour, sans adjonction ni suppression de noms et sans modification de l'ordre de présentation, chaque liste étant composée alternativement d'un candidat de chaque sexe.   La répartition des sièges entre les listes est opérée à la représentation proportionnelle à la plus forte moyenne. (...) ; / c) Si le nombre de sièges attribués à la commune est inférieur au nombre de conseillers communautaires élus à l'occasion du précédent renouvellement général du conseil municipal, les membres du nouvel organe délibérant sont élus par le conseil municipal parmi les conseillers communautaires sortants au scrutin de liste à un tour, sans adjonction ni suppression de noms et sans modification de l'ordre de présentation. La répartition des sièges entre les listes est opérée à la représentation proportionnelle à la plus forte moyenne. (...) / (...) / Le mandat des conseillers communautaires précédemment élus et non membres du nouvel organe délibérant de l'établissement public de coopération intercommunale à fiscalité propre prend fin à compter de la date de la première réunion de ce nouvel organe délibérant. / En cas de vacance pour quelque cause que ce soit, d'un siège de conseiller communautaire pourvu en application des b et c, il est procédé à une nouvelle élection dans les conditions prévues au b. (...) ".<br/>
<br/>
              3. Il résulte de ces dispositions qu'en cas de vacance d'un ou plusieurs sièges de conseiller communautaire, pour quelque cause que ce soit, pourvus en application des b et c du 1° de l'article L. 5211-6-2 du code général des collectivités territoriales, ces sièges sont pourvus dans les conditions prévues au b du 1° de cet article, en procédant à l'élection, par le conseil municipal, parmi ses membres et, le cas échéant, parmi les conseillers d'arrondissement, d'un nombre de conseillers communautaires égal au nombre de sièges vacants. Par suite, les requérants sont fondés à soutenir que c'est à tort que, pour annuler l'élection du 3 janvier 2017, le tribunal administratif de Toulouse a jugé que ces dispositions imposaient de procéder de nouveau à l'élection de l'ensemble des conseillers communautaires dont les sièges avaient été attribués à la commune.<br/>
<br/>
              4. Toutefois, en vertu des dispositions combinées de l'article L. 5211-2 du code général des collectivités territoriales, selon lesquelles " A l'exception de celles des deuxième à quatrième alinéas de l'article L. 2122-4, les dispositions du chapitre II du titre II du livre Ier de la deuxième partie relatives au maire et aux adjoints sont applicables au président et aux membres du bureau des établissements publics de coopération intercommunale, en tant qu'elles ne sont pas contraires aux dispositions du présent titre ", et des articles L. 2122-7 et L. 2122-7-1 du même code, applicables à l'élection du maire et des adjoints, l'élection des membres du conseil municipal au conseil d'une communauté d'agglomération sur le fondement de l'article L. 5211-6-2 du code général des collectivités territoriales se fait au scrutin secret.<br/>
<br/>
              5. Il résulte de l'instruction que les conseillers municipaux de Muret ont voté à leur place et que seule la liste de la majorité disposait de bulletins pré-imprimés. Si l'utilisation de tels bulletins ne constitue pas en elle-même une atteinte au secret du vote, et si aucune disposition ni aucun principe n'imposent la présence d'un isoloir, toutefois, eu égard à la configuration des lieux et à la nécessité, pour les conseillers municipaux qui souhaitaient s'écarter des bulletins pré-imprimés, d'inscrire leur choix de manière manuscrite, au vu des autres membres du conseil municipal et du public, le secret du vote n'a pas été assuré. Dans les circonstances de l'espèce, et quel que soit l'écart de voix entre les deux listes en présence, cette irrégularité a été de nature à altérer la sincérité du scrutin. <br/>
<br/>
              6. Il résulte de ce qui précède que M. E...et Mme A...ne sont pas fondés à se plaindre de ce que, par le jugement attaqué, le tribunal administratif de Toulouse a annulé l'élection de conseillers communautaires à laquelle le conseil municipal de Muret a procédé le 3 janvier 2017. <br/>
<br/>
              Sur les conclusions accessoires présentées par M. D...devant le tribunal administratif de Toulouse :<br/>
<br/>
              7. Si le tribunal administratif a, à bon droit, relevé qu'une nouvelle élection devait avoir lieu, il n'entrait pas dans son office, après avoir annulé les opérations électorales, d'enjoindre à la commune d'organiser de nouvelles élections. Par suite, les requérants sont fondés à demander l'annulation de l'article 2 du jugement  qu'ils attaquent.  <br/>
<br/>
              8. Une commune ne saurait avoir la qualité de partie devant le juge de l'élection saisi d'une contestation relative à l'élection de conseillers communautaires. Par suite, les requérants sont fondés à soutenir que le tribunal administratif ne pouvait, alors même que la commune de Muret s'était jointe aux observations présentées par les défendeurs, mettre à sa charge une somme au titre de l'article L. 761-1 du code de justice administrative. Il suit de là, et dès lors qu'une même somme a été mise globalement à la charge de M.E..., de Mme A...et de la commune de Muret, que ceux-ci sont fondés à demander l'annulation de l'article 3 du jugement  qu'ils attaquent.  <br/>
<br/>
              Sur les frais exposés par les parties à l'occasion du litige :<br/>
<br/>
              9. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mise à la charge de M.D..., qui n'est pas la partie perdante dans la présente instance, la somme que les requérants demandent à ce titre. Pour les motifs précisés au point 8, aucun versement ne saurait être mis à la charge de la commune de Muret au même titre. Enfin, il n'y a pas lieu, dans les circonstances de l'espèce, de mettre à la charge de M. E...et de Mme A...la somme que M. D...demande au titre des mêmes dispositions. <br/>
<br/>
<br/>
<br/>			D E C I D E :<br/>
              			--------------<br/>
<br/>
Article 1er : Les articles 2 et 3 du jugement du tribunal administratif de Toulouse du 3 mars 2017 sont annulés.<br/>
Article 2 : Le surplus des conclusions de la requête de M.E..., de Mme A...et de la commune de Muret est rejeté. <br/>
Article 3 : Les conclusions de M. D...présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
Article 4 : La présente décision sera notifiée à M. C...E..., à Mme B...A...et à M. C... D.... <br/>
Copie en sera adressée à la commune de Muret et au ministre de l'intérieur.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">135-05-01-01 COLLECTIVITÉS TERRITORIALES. COOPÉRATION. ÉTABLISSEMENTS PUBLICS DE COOPÉRATION INTERCOMMUNALE - QUESTIONS GÉNÉRALES. DISPOSITIONS GÉNÉRALES ET QUESTIONS COMMUNES. - ELECTION AU CONSEIL COMMUNAUTAIRE D'UN EPCI - 1) CAS OÙ DES SIÈGES POURVUS EN APPLICATION DES B ET C DU 1° DE L'ART. L. 5211-6-2 DU CGCT DEVIENNENT VACANTS - ELECTION PAR LE CONSEIL MUNICIPAL D'UN NOMBRE DE CONSEILLERS COMMUNAUTAIRES ÉGAL AU NOMBRE DE SIÈGES VACANTS - 2) ELECTION AU SCRUTIN SECRET - A) EXISTENCE - B) ESPÈCE - ATTEINTE AU SECRET DU SCRUTIN - CONSÉQUENCES.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">28 ÉLECTIONS ET RÉFÉRENDUM. - ELECTION AU CONSEIL COMMUNAUTAIRE D'UN EPCI - 1) CAS OÙ DES SIÈGES POURVUS EN APPLICATION DES B ET C DU 1° DE L'ART. L. 5211-6-2 DU CGCT DEVIENNENT VACANTS - ELECTION PAR LE CONSEIL MUNICIPAL D'UN NOMBRE DE CONSEILLERS COMMUNAUTAIRES ÉGAL AU NOMBRE DE SIÈGES VACANTS - 2) ELECTION AU SCRUTIN SECRET - A) EXISTENCE - B) ESPÈCE - ATTEINTE AU SECRET DU SCRUTIN - CONSÉQUENCES.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">28-08-05-04-02 ÉLECTIONS ET RÉFÉRENDUM. RÈGLES DE PROCÉDURE CONTENTIEUSE SPÉCIALES. POUVOIRS DU JUGE. ANNULATION D'UNE ÉLECTION. CONSÉQUENCES DE L'ANNULATION. - CAS OÙ UNE NOUVELLE ÉLECTION DOIT AVOIR LIEU - POUVOIR DU JUGE D'ENJOINDRE L'ORGANISATION DE NOUVELLES ÉLECTIONS - ABSENCE.
</SCT>
<ANA ID="9A"> 135-05-01-01 1) Il résulte de l'article L. 5211-6-2 du code général des collectivités territoriales (CGCT) qu'en cas de vacance d'un ou plusieurs sièges de conseiller communautaire, pour quelque cause que ce soit, pourvus en application des b et c du 1° de cet article, ces sièges sont pourvus dans les conditions prévues au b du 1° de cet article, en procédant à l'élection, par le conseil municipal, parmi ses membres et, le cas échéant, parmi les conseillers d'arrondissement, d'un nombre de conseillers communautaires égal au nombre de sièges vacants.,,,2) a) En vertu des dispositions combinées de l'article L. 5211-2 du CGCT et des articles L. 2122-7 et L. 2122-7-1 du même code, applicables à l'élection du maire et des adjoints, l'élection des membres du conseil municipal au conseil d'une communauté d'agglomération sur le fondement de l'article L. 5211-6-2 du CGCT se fait au scrutin secret.,,,b) Conseillers municipaux ayant voté à leur place, alors que seule la liste de la majorité disposait de bulletins pré-imprimés. Si l'utilisation de tels bulletins ne constitue pas en elle-même une atteinte au secret du vote, et si aucune disposition législative ou réglementaire ni aucun principe n'impose la présence d'un isoloir, toutefois, eu égard à la configuration des lieux et à la nécessité, pour les conseillers municipaux qui souhaitaient s'écarter des bulletins pré-imprimés, d'inscrire leur choix de manière manuscrite, au vu des autres membres du conseil municipal et du public, le secret du vote n'a pas été assuré. Dans les circonstances de l'espèce, et quel que soit l'écart de voix entre les deux listes en présence, cette irrégularité a été de nature à altérer la sincérité du scrutin.</ANA>
<ANA ID="9B"> 28 1) Il résulte de l'article L. 5211-6-2 du code général des collectivités territoriales (CGCT) qu'en cas de vacance d'un ou plusieurs sièges de conseiller communautaire, pour quelque cause que ce soit, pourvus en application des b et c du 1° de cet article, ces sièges sont pourvus dans les conditions prévues au b du 1° de cet article, en procédant à l'élection, par le conseil municipal, parmi ses membres et, le cas échéant, parmi les conseillers d'arrondissement, d'un nombre de conseillers communautaires égal au nombre de sièges vacants.,,,2) a) En vertu des dispositions combinées de l'article L. 5211-2 du CGCT et des articles L. 2122-7 et L. 2122-7-1 du même code, applicables à l'élection du maire et des adjoints, l'élection des membres du conseil municipal au conseil d'une communauté d'agglomération sur le fondement de l'article L. 5211-6-2 du CGCT se fait au scrutin secret.,,,b) Conseillers municipaux ayant voté à leur place, alors que seule la liste de la majorité disposait de bulletins pré-imprimés. Si l'utilisation de tels bulletins ne constitue pas en elle-même une atteinte au secret du vote, et si aucune disposition législative ou réglementaire ni aucun principe n'impose la présence d'un isoloir, toutefois, eu égard à la configuration des lieux et à la nécessité, pour les conseillers municipaux qui souhaitaient s'écarter des bulletins pré-imprimés, d'inscrire leur choix de manière manuscrite, au vu des autres membres du conseil municipal et du public, le secret du vote n'a pas été assuré. Dans les circonstances de l'espèce, et quel que soit l'écart de voix entre les deux listes en présence, cette irrégularité a été de nature à altérer la sincérité du scrutin.</ANA>
<ANA ID="9C"> 28-08-05-04-02 Si le tribunal administratif a à bon droit relevé qu'une nouvelle élection devait avoir lieu, il n'entrait pas dans son office, après avoir annulé les opérations électorales, d'enjoindre à la commune d'organiser de nouvelles élections.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
