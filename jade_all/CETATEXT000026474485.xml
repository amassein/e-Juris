<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026474485</ID>
<ANCIEN_ID>JG_L_2012_10_000000346979</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/47/44/CETATEXT000026474485.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème et 1ère sous-sections réunies, 08/10/2012, 346979</TITRE>
<DATE_DEC>2012-10-08</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>346979</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème et 1ère sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP HEMERY, THOMAS-RAQUIN ; SCP NICOLAY, DE LANOUVELLE, HANNOTIN</AVOCATS>
<RAPPORTEUR>M. Bruno Chavanat</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Xavier de Lesquen</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2012:346979.20121008</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi et le mémoire complémentaire, enregistrés les 23 février et 20 mai 2010 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. B...A...,  demeurant...,; M. A...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler le jugement n° 0903669, 0903873 et 0905068 du 21 décembre 2010 par lequel le tribunal administratif de Montpellier a rejeté ses demandes tendant à l'annulation, d'une part, des décisions du 19 août 2009 par lesquelles le président de la délégation spéciale de Saint-Cyprien a rejeté ses demandes de rémunération pendant son congé de maladie,  pour le mois de juillet 2009 et la période ultérieure et, d'autre part, des décisions des 22 septembre, 25 septembre et 6 novembre 2009 par lesquelles le maire de Saint-Cyprien l'a placé en congé de maladie sans traitement pour la période courant de juillet à novembre 2009 ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat le versement de la somme de 3000 euros au titre de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la loi n° 83-634 du 13 juillet 1983 ; <br/>
<br/>
              Vu la loi n° 84-53 du 26 janvier 1984 ;<br/>
<br/>
              Vu le décret n° 87-602 du 30 juillet 1987 ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Bruno Chavanat, Maître des Requêtes,  <br/>
<br/>
              - les observations de la SCP Hémery, Thomas-Raquin, avocat de M. A...et de la SCP Nicolaÿ, de Lanouvelle, Hannotin, avocat de la commune de saint Cyprien ;<br/>
<br/>
              - les conclusions de M. Xavier de Lesquen, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Hémery, Thomas-Raquin, avocat de M. A...et à la SCP Nicolaÿ, de Lanouvelle, Hannotin, avocat de la commune de saint Cyprien  ;<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis au juge du fond que M.A..., administrateur territorial et auparavant directeur général des services de la commune de Saint-Cyprien, a été placé en détention provisoire du 3 avril au 26 mai 2009 puis remis en liberté à compter de cette dernière date sous contrôle judiciaire, avec interdiction d'exercer toute activité dans sa collectivité et les établissements publics en lien avec celle-ci ; que l'intéressé a demandé à être placé en congé de maladie à compter du 1er juillet 2009 et à percevoir son traitement, en application de l'article 57 de la loi du 26 janvier 1984 portant dispositions statutaires relatives à la fonction publique territoriale ; que, par décisions du 19 août 2009, le président de la délégation spéciale de la commune de Saint-Cyprien a rejeté la demande de rémunération présentée par M. A...au titre du mois de juillet 2009 et de la période ultérieure; que par trois décisions des 22 septembre, 25 septembre et 6 novembre 2009, le maire de Saint-Cyprien a placé en congé de maladie ordinaire M.A..., sans versement de son  traitement ; que par un jugement du 21 décembre 2010, contre lequel M. A...se pourvoit en cassation, le tribunal administratif de Montpellier a rejeté les demandes de l'intéressé tendant à l'annulation de ces différentes décisions en ce qu'elles refusent ou ne prévoient pas le versement de son traitement; <br/>
<br/>
              2. Considérant qu'aux termes de l'article 20 de la loi du 13 juillet 1983 portant droits et obligations des fonctionnaires : " Les fonctionnaires ont droit, après service fait, à une rémunération comprenant le traitement, l'indemnité de résidence, le supplément familial de traitement ainsi que les indemnités instituées par un texte législatif ou réglementaire." ; que l'article 57 de la loi du 26 janvier 1984 portant dispositions statutaires relatives à la fonction publique territoriale dispose : " le fonctionnaire en activité a droit (...) 2° A des congés de maladie dont la durée totale peut atteindre un an pendant une période de douze mois consécutifs en cas de maladie dûment constatée mettant l'intéressé dans l'impossibilité d'exercer ses fonctions. Celui-ci conserve alors l'intégralité de son traitement pendant une durée de trois mois ; ce traitement est réduit de moitié pendant les neuf mois suivants. Le fonctionnaire conserve, en outre, ses droits à la totalité du supplément familial de traitement et de l'indemnité de résidence (...) " ; qu'enfin, aux termes de l'article 14 du décret du 30 juillet 1987 pris pour l'application de la loi du 26 janvier 1984 et relatif à l'organisation des comités médicaux, aux conditions d'aptitude physique et au régime des congés de maladie des fonctionnaires territoriaux : " (...) en cas de maladie dûment constatée et mettant le fonctionnaire dans l'impossibilité d'exercer ses fonctions, celui-ci est de droit mis en congé de maladie. " ; <br/>
<br/>
              3. Considérant que les dispositions de l'article 57 de la loi du 26 janvier 1984 selon lesquelles le fonctionnaire conserve, selon la durée du congé, l'intégralité ou la moitié de son traitement, ont pour seul objet de compenser la perte de rémunération due à la maladie en apportant une dérogation au principe posé par l'article 20 de la loi du 13 juillet 1983 subordonnant le droit au traitement au service fait ; qu'elles ne peuvent avoir pour effet d'accorder à un fonctionnaire bénéficiant d'un congé de maladie des droits à rémunération supérieurs à ceux qu'il aurait eus s'il n'en avait pas bénéficié ; <br/>
<br/>
              4. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que si l'intéressé n'avait pas été placé en congé de maladie, il n'aurait pu, en tout état de cause, percevoir son traitement en raison de l'interdiction professionnelle attachée à la mesure de contrôle judiciaire dont il était l'objet ; que le versement d'une rémunération au titre de son congé de maladie aurait eu pour effet, en méconnaissance de la règle ci-dessus énoncée, de lui accorder des droits supérieurs à ceux auxquels il aurait pu prétendre s'il n'avait pas bénéficié d'un tel congé ; que, par suite, en jugeant que M. A...n'avait pas droit au maintien de son traitement pour la période en cause, le tribunal administratif de Montpellier n'a pas entaché son jugement d'une erreur de droit ; que M. A...n'est, dès lors, pas fondé à en demander l'annulation ; <br/>
<br/>
               5. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mise à la charge de la commune de Saint-Cyprien, qui n'est pas la partie perdante dans la présente affaire, la somme que demande M. A...au titre des frais exposés par lui et non compris dans les dépens ; qu'il y a lieu, en revanche, de mettre à la charge de M. A...le versement à la commune de Saint-Cyprien d'une somme de 1500 euros au même titre ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de M. A...est rejeté.<br/>
<br/>
Article 2 : M. A...versera à la commune de Saint-Cyprien la somme de 1500 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
Article 3 : La présente décision sera notifiée à M. B...A...et à la commune de Saint-Cyprien. <br/>
	Copie en sera adressée pour information au ministre de l'intérieur.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">36-05-04-01-01 FONCTIONNAIRES ET AGENTS PUBLICS. POSITIONS. CONGÉS. CONGÉS DE MALADIE. QUESTIONS COMMUNES. - DISPOSITIONS PRÉVOYANT LE MAINTIEN DU TRAITEMENT - OBJET - COMPENSATION DE LA PERTE DE RÉMUNÉRATION DUE À LA MALADIE PAR DÉROGATION AU PRINCIPE SUBORDONNANT LE TRAITEMENT AU SERVICE FAIT - CONSÉQUENCE - IMPOSSIBILITÉ D'ACCORDER AU TITRE DU CONGÉ DE MALADIE DES DROITS À RÉMUNÉRATION SUPÉRIEURS À CEUX QUE LE FONCTIONNAIRE AURAIT EUS SANS CONGÉ.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">36-07-01-03 FONCTIONNAIRES ET AGENTS PUBLICS. STATUTS, DROITS, OBLIGATIONS ET GARANTIES. STATUT GÉNÉRAL DES FONCTIONNAIRES DE L'ÉTAT ET DES COLLECTIVITÉS LOCALES. DISPOSITIONS STATUTAIRES RELATIVES À LA FONCTION PUBLIQUE TERRITORIALE (LOI DU 26 JANVIER 1984). - DISPOSITIONS PRÉVOYANT LE MAINTIEN DU TRAITEMENT - OBJET - COMPENSATION DE LA PERTE DE RÉMUNÉRATION DUE À LA MALADIE PAR DÉROGATION AU PRINCIPE SUBORDONNANT LE TRAITEMENT AU SERVICE FAIT - CONSÉQUENCE - IMPOSSIBILITÉ D'ACCORDER AU TITRE DU CONGÉ DE MALADIE DES DROITS À RÉMUNÉRATION SUPÉRIEURS À CEUX QUE LE FONCTIONNAIRE AURAIT EUS SANS CONGÉ.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">36-08-02 FONCTIONNAIRES ET AGENTS PUBLICS. RÉMUNÉRATION. TRAITEMENT. - MAINTIEN DE L'INTÉGRALITÉ OU DE LA MOITIÉ DU TRAITEMENT PENDANT LE CONGÉ DE MALADIE - OBJET - COMPENSATION DE LA PERTE DE RÉMUNÉRATION DUE À LA MALADIE PAR DÉROGATION AU PRINCIPE SUBORDONNANT LE TRAITEMENT AU SERVICE FAIT - CONSÉQUENCE - IMPOSSIBILITÉ D'ACCORDER AU TITRE DU CONGÉ DE MALADIE DES DROITS À RÉMUNÉRATION SUPÉRIEURS À CEUX QUE LE FONCTIONNAIRE AURAIT EUS SANS CONGÉ.
</SCT>
<ANA ID="9A"> 36-05-04-01-01 Les dispositions de l'article 57 de la loi n° 84-53 du 26 janvier 1984 selon lesquelles le fonctionnaire territorial conserve, selon la durée de son congé maladie, l'intégralité ou la moitié de son traitement, ont pour seul objet de compenser la perte de rémunération due à la maladie en apportant une dérogation au principe posé à l'article 20 de la loi n° 83-624 du 13 juillet 1983 subordonnant le droit au traitement au service fait. Elles ne peuvent avoir pour effet d'accorder à un fonctionnaire bénéficiant d'un congé de maladie des droits à rémunération supérieurs à ceux qu'il aurait eus s'il n'en n'avait pas bénéficié (en l'espèce, fonctionnaire ne pouvant percevoir son traitement en raison de l'interdiction professionnelle attachée à la mesure de contrôle judiciaire dont il était l'objet).</ANA>
<ANA ID="9B"> 36-07-01-03 Les dispositions de l'article 57 de la loi n° 84-53 du 26 janvier 1984 selon lesquelles le fonctionnaire territorial conserve, selon la durée de son congé maladie, l'intégralité ou la moitié de son traitement, ont pour seul objet de compenser la perte de rémunération due à la maladie en apportant une dérogation au principe posé à l'article 20 de la loi n° 83-624 du 13 juillet 1983 subordonnant le droit au traitement au service fait. Elles ne peuvent avoir pour effet d'accorder à un fonctionnaire bénéficiant d'un congé de maladie des droits à rémunération supérieurs à ceux qu'il aurait eus s'il n'en n'avait pas bénéficié (en l'espèce, fonctionnaire ne pouvant percevoir son traitement en raison de l'interdiction professionnelle attachée à la mesure de contrôle judiciaire dont il était l'objet).</ANA>
<ANA ID="9C"> 36-08-02 Les dispositions de l'article 57 de la loi n° 84-53 du 26 janvier 1984 selon lesquelles le fonctionnaire territorial conserve, selon la durée de son congé maladie, l'intégralité ou la moitié de son traitement, ont pour seul objet de compenser la perte de rémunération due à la maladie en apportant une dérogation au principe posé à l'article 20 de la loi n° 83-624 du 13 juillet 1983 subordonnant le droit au traitement au service fait. Elles ne peuvent avoir pour effet d'accorder à un fonctionnaire bénéficiant d'un congé de maladie des droits à rémunération supérieurs à ceux qu'il aurait eus s'il n'en n'avait pas bénéficié (en l'espèce, fonctionnaire ne pouvant percevoir son traitement en raison de l'interdiction professionnelle attachée à la mesure de contrôle judiciaire dont il était l'objet).</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
