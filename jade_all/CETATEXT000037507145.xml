<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037507145</ID>
<ANCIEN_ID>JG_L_2018_10_000000424556</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/50/71/CETATEXT000037507145.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 12/10/2018, 424556, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-10-12</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>424556</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP SPINOSI, SUREAU</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2018:424556.20181012</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              M. A...B...a demandé au juge des référés du tribunal administratif de Mayotte, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, d'une part, de suspendre l'exécution de l'arrêté du 20 septembre 2018 par lequel le préfet de Mayotte l'a obligé à quitter sans délai le territoire français, a fixé le pays de destination et lui a interdit de revenir sur le territoire français pendant une durée d'un an et, d'autre part, d'enjoindre sous astreinte au préfet de réexaminer sa demande de titre de séjour dans un délai de huit jours et, dans l'attente, de lui délivrer une autorisation provisoire de séjour. Par une ordonnance<br/>
n° 1801272 du 24 septembre 2018, le juge des référés a rejeté sa demande.<br/>
<br/>
              Par une requête, enregistrée le 27 septembre 2018 au secrétariat du contentieux du Conseil d'Etat, M. B...demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative :<br/>
              1°) de l'admettre au bénéfice de l'aide juridictionnelle provisoire ;<br/>
<br/>
              2°) d'annuler cette ordonnance ;<br/>
<br/>
              3°) de surseoir à l'exécution de l'arrêté du 20 octobre 2018 ;<br/>
<br/>
              4°) de mettre à la charge de l'Etat la somme de 3 000 euros à verser à<br/>
M. B...au titre des dispositions combinées de l'article 37 de la loi du 10 juillet 1991 et de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Il soutient que : <br/>
              - la condition de l'urgence est remplie dès lors que la mesure d'éloignement est immédiatement exécutoire et que le recours formé contre elle n'est pas suspensif ;<br/>
              - il est porté une atteinte grave et manifestement illégale, d'une part, à son droit de mener une vie privée et familiale normale, protégée par l'article 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales et, d'autre part, à l'intérêt supérieur de l'enfant de son épouse consacré par l'article 3-1 de la convention relative aux droits des enfants ainsi qu'aux droits posés à l'article 9-1 de la même convention, dès lors qu'il établit la communauté de vie avec son épouse et sa participation à l'entretien et à l'éducation de cet enfant ;<br/>
              - la décision d'interdiction de retour sur le territoire français n'a pas été précédée d'un examen particulier de sa situation personnelle et est insuffisamment motivée. <br/>
              Par un mémoire en défense, enregistré le 8 octobre 2018, le ministre de l'intérieur conclut au rejet de la requête. Il soutient que les moyens soulevés par M. B... ne sont pas fondés et qu'en outre, la condition d'urgence n'est pas satisfaite s'agissant de la décision d'interdiction de retour sur le territoire français. <br/>
<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, M. B... et, d'autre part, le ministre de l'intérieur ; <br/>
<br/>
              Vu le procès-verbal de l'audience publique du 9 octobre 2018 à 15 heures au cours de laquelle ont été entendus : <br/>
              - Me Gury, avocat au Conseil d'Etat et à la Cour de cassation, avocat de<br/>
M. B...;<br/>
<br/>
              - la représentante du requérant ;<br/>
<br/>
              - les représentantes du ministre de l'intérieur ;<br/>
              et à l'issue de laquelle le juge des référés a clos l'instruction ; <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - la convention internationale relative aux droits de l'enfant du 26 janvier 1990 ;<br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - le code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. Le juge des référés se prononce dans un délai de quarante-huit heures " ; <br/>
<br/>
              2. Considérant qu'il résulte de l'instruction que, par un arrêté du 20 septembre 2018, le préfet de Mayotte a obligé M. A...B..., ressortissant comorien né le 27 novembre 1984, à quitter sans délai le territoire français, a fixé l'Union des Comores comme pays de destination et a prononcé à son encontre une interdiction de retour sur le territoire français pendant une durée d'un an ; que le préfet de Mayotte l'a, le même jour, placé en rétention administrative en vue de son éloignement ; que toutefois, le juge des libertés et de la détention près le tribunal de grande instance de Mamoudzou a ordonné la mainlevée de la mesure de rétention administrative le 22 septembre 2018 ; que M. B...relève appel de l'ordonnance du 24 septembre 2018 par laquelle le juge des référés du tribunal administratif de Mayotte, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, a rejeté sa demande tendant, notamment, à la suspension de l'exécution de l'arrêté du 20 septembre 2018 ;<br/>
<br/>
              Sur l'arrêté en tant qu'il lui fait obligation de quitter le territoire français :<br/>
<br/>
              3. Considérant qu'aux termes de l'article 8 de la convention de sauvegarde des droits de l'homme et des libertés fondamentales : " 1. Toute personne a droit au respect de sa vie privée et familiale, de son domicile et de sa correspondance. / 2. Il ne peut y avoir ingérence d'une autorité publique dans l'exercice de ce droit que pour autant que cette ingérence est prévue par la loi et qu'elle constitue une mesure qui, dans une société démocratique, est nécessaire à la sécurité nationale, à la sûreté publique, au bien-être économique du pays, à la défense de l'ordre et à la prévention des infractions pénales, à la protection de la santé ou de la morale, ou à la protection des droits et libertés d'autrui " ; qu'aux termes du paragraphe 1 de l'article 3 de la convention internationale relative aux droits de l'enfant : " Dans toutes les décisions qui concernent les enfants, qu'elles soient le fait des institutions publiques ou privées de protection sociale, des tribunaux, des autorités administratives ou des organes législatifs, l'intérêt supérieur de l'enfant doit être une considération primordiale " ; <br/>
<br/>
              4. Considérant que M. B...soutient qu'il est porté, par l'arrêté litigieux en tant qu'il lui fait l'obligation de quitter sans délai le territoire français, une atteinte grave et manifestement illégale aux droits protégés par les stipulations citées précédemment, en raison de sa communauté de vie avec Mme D...F...E..., ressortissante malgache vivant à Mayotte sous couvert d'une carte de séjour temporaire " mention vie privée et familiale ", et de sa participation à l'entretien et à l'éducation de l'enfant C...que cette dernière a eu, le<br/>
14 septembre 2011, avec un ressortissant français, dont elle s'est séparée en 2014 ; <br/>
<br/>
              5. Considérant toutefois qu'il résulte de l'instruction que M. B...ne justifie utilement qu'à compter de la fin de l'année 2015 de sa présence à Mayotte, de ses démarches en vue de la régularisation de son séjour et de ce qu'il vit avec MmeE..., avec laquelle il s'était marié le 11 août 2007 à Moroni (Union des Comores) puis dont il s'était séparé lorsqu'il était entré sur le territoire français ; qu'en outre, en l'état de l'instruction, il n'est pas établi que M. B...prendrait effectivement en charge l'éducation et l'entretien du jeune C...alors, d'ailleurs, que le juge aux affaires familiales près le tribunal de grande instance de Mamoudzou a, par un jugement du 11 mars 2015, relevé que l'enfant voyait son père et qu'il convenait de fixer sa résidence habituelle chez sa mère, tout en maintenant l'exercice commun de l'autorité parentale et en prévoyant un droit de visite et d'hébergement du père selon des modalités qu'il a définies ; que, dans ces conditions, il ne peut être retenu que l'obligation faite à M. B...de quitter sans délai le territoire français à destination de l'Union des Comores porterait une atteinte grave et manifestement illégale aux droits protégés par les stipulations citées au point 3, de même qu'en tout état de cause, aux stipulations du paragraphe 1 de l'article 9 de la convention internationale relative aux droits de l'enfant dont il entend se prévaloir également ; que, par suite, M. B...n'est pas fondé à soutenir que c'est à tort que le juge des référés du tribunal administratif de Mayotte a rejeté ses conclusions relatives à cette décision ; <br/>
<br/>
              Sur l'arrêté en tant qu'il lui interdit de revenir sur le territoire français pendant la durée d'un an :<br/>
<br/>
              6. Considérant qu'en ce que l'arrêté litigieux interdit à M. B...de revenir sur le territoire français pendant une durée d'une année, la condition d'urgence prévue à l'article L. 521-2 du code de justice administrative ne peut être regardée comme satisfaite à la date de la présente décision, dès lors qu'il n'est pas contesté que M.B..., qui n'est plus en rétention administrative, n'a pas été éloigné du territoire français ; que, par suite, sans qu'il y ait lieu d'examiner si l'autre condition posée à l'article L. 521-2 est satisfaite, M. B...n'est pas fondé à soutenir que c'est à tort que le juge des référés du tribunal administratif de Mayotte a rejeté ses conclusions dirigées contre l'arrêté litigieux en tant qu'il lui fait interdiction de revenir sur le territoire français pendant une durée d'un an ; <br/>
<br/>
              7. Considérant qu'il résulte de tout ce qui précède que, sans qu'il y ait lieu d'admettre M. B...au bénéfice de l'aide juridictionnelle provisoire, sa requête d'appel doit être rejetée, y compris en ce qu'elle comporte des conclusions présentées au titre de l'article 37 de la loi du 10 juillet 1991 et de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de M. B...est rejetée. <br/>
Article 2 : La présente ordonnance sera notifiée à M. A...B...et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
