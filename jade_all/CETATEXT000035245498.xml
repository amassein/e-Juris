<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000035245498</ID>
<ANCIEN_ID>JG_L_2017_07_000000384783</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/35/24/54/CETATEXT000035245498.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème - 9ème chambres réunies, 19/07/2017, 384783, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-07-19</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>384783</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème - 9ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP WAQUET, FARGE, HAZAN ; SCP GARREAU, BAUER-VIOLAS, FESCHOTTE-DESBOIS</AVOCATS>
<RAPPORTEUR>M. Vincent Villette</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Aurélie Bretonneau</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:384783.20170719</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              La SCA château de l'Arc, la SCI des hameaux du château de l'Arc, l'Association de défense de la Haute vallée de l'Arc et la société Mirabeau ont demandé au tribunal administratif de Marseille d'annuler, pour excès de pouvoir, la délibération en date du 30 mai 2011 par laquelle le conseil municipal de la Commune de Fuveau (Bouches du Rhône) a approuvé la modification n° 2 de son plan local d'urbanisme afin d'ouvrir à l'urbanisation une zone d'activités de 24 hectares précédemment classée en zone AU A2. Par un jugement n° 1105156 du 26 septembre 2013, le tribunal administratif a rejeté leur demande. <br/>
<br/>
              Par un arrêt n° 13MA04387 du 25 juillet 2014, la cour administrative d'appel de Marseille a rejeté l'appel que ces sociétés et cette association avaient formé contre ce jugement. <br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 25 septembre et 24 décembre 2014 et le 19 octobre 2015 au secrétariat du contentieux du Conseil d'Etat, la SCA château de l'Arc et la SCI des hameaux du château de l'Arc demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à leur appel ;<br/>
<br/>
              3°) de mettre à la charge de la commune de Fuveau une somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Vincent Villette, auditeur,  <br/>
<br/>
              - les conclusions de Mme Aurélie Bretonneau, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Garreau, Bauer-Violas, Feschotte-Desbois, avocat de la SCI des hameaux du château de l'Arc et de la SCA château de l'Arc et à la SCP Waquet, Farge, Hazan, avocat de la commune de Fuveau ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces de la procédure que la Commune de Fuveau a adressé à la cour administrative d'appel de Marseille une note en délibéré, enregistrée au greffe de cette cour le 7 juillet 2014, postérieurement à l'audience publique du 3 juillet 2014 au cours de laquelle le rapporteur public avait conclu à l'annulation de sa délibération du 30 mai 2011. Les juges d'appel ont nécessairement pris en compte, pour rejeter l'appel formé par les requérantes, les pièces nouvelles produites à l'appui de cette note, qu'ils ont par ailleurs omis de viser, et notamment sur celle établissant la date précise de remise des convocations aux conseillers municipaux, pour écarter le moyen tiré de ce que les délais de convocation prévus à l'article L. 2121-12 du code général des collectivités territoriales auraient été méconnus. Dans ces conditions, la cour d'appel aurait dû rouvrir l'instruction afin de mettre à même les sociétés requérantes de prendre connaissance de cette note et de discuter utilement de la portée des éléments nouveaux qu'elle contenait sur l'issue du litige. Il suit de là que l'arrêt attaqué est entaché d'irrégularité.<br/>
<br/>
              2. Il résulte de ce qui précède que, sans qu'il soit besoin d'examiner les autres moyens de leur pourvoi, les sociétés requérantes sont fondées à demander l'annulation de l'arrêt du 25 juillet 2014. Il n'y a pas lieu, dans les circonstances de l'espèce, de mettre une somme à la charge de la commune de Fuveau au titre de l'article L. 761-1 du code de justice administrative. Les dispositions de cet article font obstacle à ce qu'une somme soit mise, à ce titre, à la charge de la SCA château de l'Arc et de la SCI des hameaux du château de l'Arc, qui ne sont pas, dans la présente instance, la partie perdante. <br/>
<br/>
<br/>
<br/>
<br/>
                                        D E C I D E :<br/>
                                         --------------<br/>
<br/>
Article 1er : L'arrêt du 25 juillet 2014 de la cour administrative d'appel de Marseille est annulé.<br/>
Article 2 : L'affaire est renvoyée devant la cour administrative d'appel de Marseille.<br/>
Article 3 : Les conclusions présentées par la SCA château de l'Arc et à la SCI des hameaux du château de l'Arc ainsi que celles présentées par la commune de Fuveau au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée à la SCA château de l'Arc, à la SCI des hameaux du château de l'Arc et à la commune de Fuveau.<br/>
Copie sera transmise au ministre de la cohésion des territoires, à l'association de défense de la Haute vallée de l'Arc et à la société Mirabeau. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
