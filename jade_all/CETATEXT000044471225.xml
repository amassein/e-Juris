<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044471225</ID>
<ANCIEN_ID>JG_L_2021_12_000000439944</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/47/12/CETATEXT000044471225.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème - 10ème chambres réunies, 10/12/2021, 439944</TITRE>
<DATE_DEC>2021-12-10</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>439944</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème - 10ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR>Mme Catherine Fischer-Hirtz</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emilie Bokdam-Tognetti</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:439944.20211210</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés les 3 avril 2020 et 2 avril 2021, au secrétariat du contentieux du Conseil d'Etat, la société Hydroption demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler pour excès de pouvoir la délibération n° 2020-071 de la Commission de régulation de l'énergie (CRE) du 26 mars 2020 portant communication sur les mesures en faveur des fournisseurs prenant en compte des effets de la crise sanitaire sur les marchés d'électricité et de gaz naturel, en tant qu'elle méconnaît les dispositions des articles 10 et 13 de l'accord-cadre pour l'accès régulé à l'électricité nucléaire historique (ARENH) conclu le 26 octobre 2016 entre la société Hydroption et la société EDF, ensemble la décision du 31 mars 2020 rejetant le recours gracieux qu'elle avait formé à l'encontre de cette délibération ; <br/>
<br/>
              2°) d'enjoindre a` la CRE de reprendre une délibération s'agissant de l'application des articles 10 et 13 de l'accord-cadre ARENH dans un délai de deux semaines à compter de la notification de la décision a` venir et sous astreinte de 150 euros par jour de retard ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'énergie ; <br/>
              - l'arrêté du 12 mars 2019 portant modification de l'arrêté du 28 avril 2011 pris en application du II de l'article 4-1 de la loi n° 2000-108 relative à la modernisation et au développement du service public de l'électricité ;<br/>
              - le code de justice administrative ; <br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Catherine Fischer-Hirtz, conseillère d'Etat,  <br/>
<br/>
              - les conclusions de Mme Emilie Bokdam-Tognetti, rapporteure publique ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              Sur le cadre juridique :<br/>
<br/>
              1.  Aux termes de l'article L. 336-1 du code de l'énergie, issu de la loi du 7 décembre 2010 portant Nouvelle Organisation du Marché de l'Electricité : " Afin d'assurer la liberté de choix du fournisseur d'électricité tout en faisant bénéficier l'attractivité du territoire et l'ensemble des consommateurs de la compétitivité du parc électronucléaire français, un accès régulé et limité à l'électricité nucléaire historique, produite par les centrales nucléaires mentionnées à l'article L. 336-2, est ouvert, pour une période transitoire définie à l'article L. 336-8, à tous les opérateurs fournissant des consommateurs finals résidant sur le territoire métropolitain continental ou des gestionnaires de réseaux pour leurs pertes. / Cet accès régulé est consenti à des conditions économiques équivalentes à celles résultant pour Electricité de France de l'utilisation de ses centrales nucléaires mentionnées au même article L. 336-2 ".<br/>
<br/>
              2. L'article L. 336-2 du même code prévoit que, jusqu'au 31 décembre 2025, la société EDF cède de l'électricité aux fournisseurs qui en font la demande, pour un volume maximal déterminé par la Commission de régulation de l'énergie (CRE) en fonction des prévisions, fournies par les entreprises intéressées, de consommation des consommateurs finals et des gestionnaires de réseaux pour leurs pertes et en fonction de ce que représente la part de la production des centrales nucléaires dans la consommation totale des consommateurs finals. En vertu de l'article L. 337-13 du même code, le prix de l'électricité cédé par EDF en application de ces dispositions est arrêté par les ministres chargés de l'énergie et de l'économie, pris sur proposition de la CRE. En vertu d'un arrêté du 17 mai 2011, ce prix est fixé à 42 euros, hors taxes, par MWh à compter du 1er janvier 2012.<br/>
<br/>
              3. L'article L. 336-5 du même code prévoit que le fournisseur souhaitant exercer les droits qui découlent du mécanisme d'accès régulé à l'électricité nucléaire historique (ARENH) conclut avec EDF, dans le délai d'un mois suivant sa demande, un accord-cadre déterminant les modalités d'exercice de ces droits " par la voie de cessions d'une durée d'un an " et dont les stipulations sont conformes à un modèle déterminé par arrêté du ministre chargé de l'énergie pris sur proposition de la CRE. L'article R. 336-9 du même code prévoit que tout fournisseur ayant signé un accord-cadre avec EDF transmet à la CRE, au moins quarante jours avant le début de chaque période de livraison, un dossier de demande d'achat d'électricité au titre de l'ARENH. Aux termes de l'article R. 336-10 du même code : " La transmission d'un dossier de demande d'ARENH à la Commission de régulation de l'énergie vaut engagement ferme de la part du fournisseur d'acheter les quantités totales de produit qui lui seront cédées au cours de la période de livraison à venir calculées conformément à l'article R. 336-13 sur la base de sa demande et notifiées conformément à l'article R. 336-19 par la Commission de régulation de l'énergie ". Enfin, aux termes de l'article R. 336-19 du même code : " Au moins trente jours avant le début de chaque période de livraison, la Commission de régulation de l'énergie notifie simultanément : / 1o A chaque fournisseur, sur la base des éléments transmis dans le dossier de demande mentionné à l'article R. 336-9 et conformément aux méthodes mentionnées à l'article R. 336-13, les quantités et profils des produits que la société EDF lui cède sur la période de livraison à venir, les quantités étant celles définies à l'article R. 336-18 ; / 2o Au gestionnaire du réseau public de transport et à la société EDF la quantité d'électricité que cette société doit injecter chaque demi-heure de la période de livraison à venir au titre de l'ARENH ; / 3o Au gestionnaire du réseau public de transport la quantité d'électricité que reçoit, chaque demi-heure de la période de livraison à venir, chaque responsable d'équilibre des fournisseurs bénéficiaires de l'ARENH. (...) ".<br/>
<br/>
              4. Le modèle d'accord-cadre pour l'accès régulé à l'électricité nucléaire historique annexé à l'arrêté alors applicable du 12 mars 2019 portant modification de l'arrêté du 28 avril 2011 pris en application du II de l'article 4-1 de la loi n° 2000-108 relative à la modernisation et au développement du service public de l'électricité rappelle, dans son article 4 que : " A compter de la réception de la notification de cession annuelle d'électricité et de garanties de capacité, l'acheteur s'engage à prendre livraison de la totalité des produits cédés, objets de la notification ". L'article 13 du même modèle prévoit toutefois que : " L'exécution de l'accord-cadre pourra être suspendue, dans les cas de défaillance et suivant les modalités indiquées ci-après : / (...)/  - en cas de survenance d'un événement de force majeure, défini à l'article 10 de l'accord-cadre (3) " et que, dans cette hypothèse, " la suspension prend effet dès la survenance de l'événement de force majeure et entraîne de plein droit l'interruption de la cession annuelle d'électricité et de garanties de capacité ". Aux termes de l'article 10 de ce modèle : " La force majeure désigne un événement extérieur, irrésistible et imprévisible rendant impossible l'exécution des obligations des Parties dans des conditions économiques raisonnables/ (...)/ " La Partie souhaitant invoquer le bénéfice de la force majeure devra, dès connaissance de la survenance de l'événement de force majeure, informer l'autre Partie, la CDC et la CRE, par lettre recommandée avec accusé de réception, de l'apparition de cet événement et, dans la mesure du possible, leur faire part d'une estimation, à titre indicatif, de l'étendue et de la durée probable de cet événement. / La Partie souhaitant se prévaloir d'un événement de force majeure s'efforcera, dans des limites économiques raisonnables, de limiter les conséquences de l'événement de force majeure et devra, pendant toute la durée de cet événement, tenir régulièrement l'autre Partie informée de l'étendue et de la durée probable de cet événement. / Les obligations des Parties sont suspendues pendant la durée de l'événement de Force majeure ". Enfin, aux termes de l'article 19 de ce modèle : " En cas de litige survenu entre les parties, celles-ci s'engagent à se rencontrer en vue de chercher une solution amiable. Dans le cas où aucune solution amiable ne pourrait être trouvée dans les sept jours ouvrés suivant la survenance du litige, chacune des parties pourra saisir le tribunal de commerce de Paris. Les parties reconnaissent le tribunal de commerce de Paris comme l'unique juridiction compétente pour régler tout différend lié à l'interprétation ou à l'exécution du présent accord-cadre ".<br/>
<br/>
              Sur la demande de la société requérante : <br/>
<br/>
              5. Il ressort des pièces du dossier que la crise sanitaire liée à l'émergence d'un nouveau coronavirus (covid-19), de caractère pathogène et particulièrement contagieux ainsi que les mesures, notamment dites de confinement, prises pour limiter la propagation de ce virus, ont entraîné, à la fin du premier trimestre 2020, une importante baisse de la consommation d'électricité en France et une diminution du prix de l'électricité sur les marchés de gros à un niveau bien inférieur au montant de 42 euros mentionné au point 2. Certains fournisseurs d'électricité, estimant que cette situation était constitutive d'un " évènement de force majeure " au sens de l'article 10 précité du modèle annexé à l'arrêté du 12 mars 2019, ont demandé à mettre en œuvre les stipulations de l'article 13 de ces accords-cadres. <br/>
<br/>
              6. Par une délibération n° 2020-071 du 26 mars 2020 portant communication sur les mesures en faveur des fournisseurs prenant en compte des effets de la crise sanitaire sur les marchés d'électricité et de gaz naturel, la CRE, dans la partie intitulée " Evolution du cadre de l'ARENH ", a, dans les quatre premiers paragraphes, rappelé le contexte mentionné au point 5 et constaté le désaccord entre les fournisseurs alternatifs d' électricité et la société EDF, dans le paragraphe suivant, a donné son interprétation des dispositions précitées de l'article 10 du modèle d'accord-cadre annexé à l'arrêté du 12 mars 2019 et son analyse des conséquences sur le marché de l'électricité d'une suspension des accords-cadres sur l'ARENH et, dans le dernier paragraphe a conclu qu' " en conséquence, la CRE ne transmettra pas à RTE une évolution des volumes d'ARENH livrés par EDF aux fournisseurs concernés liée à une demande d'activation de la clause de force majeure ". <br/>
<br/>
              7. La société Hydroption, qui a conclu un accord-cadre sur l'ARENH en date du 26 octobre 2016 avec la société EDF et à qui cette dernière a refusé la mise en œuvre de la clause de force majeure par une décision du 23 mars 2020, demande, d'une part, l'annulation pour excès de pouvoir de la délibération mentionnée au point 6 en tant qu'elle méconnait les dispositions des articles 10 et 13 de cet accord ainsi que du rejet de son recours gracieux du 31 mars 2020 et, d'autre part, qu'il soit enjoint à la CRE de reprendre une délibération dans un délai de deux semaines sous astreinte de 150 euros par jour de retard. Eu égard aux moyens qu'elle invoque, les conclusions aux fins d'annulation doivent être regardées comme dirigées contre les énonciations du cinquième paragraphe de la délibération selon lesquelles la " force majeure ne trouverait à s'appliquer que si l'acheteur parvenait à démontrer que sa situation économique rendait totalement impossible l'exécution de l'obligation de paiement de l'ARENH ".<br/>
<br/>
              En ce qui concerne la fin de non-recevoir opposée par la Commission de régulation de l'énergie :<br/>
<br/>
              8. Les avis, recommandations, mises en garde et prises de position adoptés par les autorités de régulation dans l'exercice des missions dont elles sont investies, peuvent être déférés au juge de l'excès de pouvoir lorsqu'ils revêtent le caractère de dispositions générales et impératives ou lorsqu'ils énoncent des prescriptions individuelles dont ces autorités pourraient ultérieurement censurer la méconnaissance. Ces actes peuvent également faire l'objet d'un tel recours, introduit par un requérant justifiant d'un intérêt direct et certain à leur annulation, lorsqu'ils sont de nature à produire des effets notables, notamment de nature économique, ou ont pour objet d'influer de manière significative sur les comportements des personnes auxquelles ils s'adressent. <br/>
<br/>
              9. Il ressort des pièces du dossier que l'interprétation que la CRE a donnée des dispositions précitées de l'article 10 du modèle d'accord-cadre pour l'accès régulé à l'électricité nucléaire historique, alors même qu'elle ne saurait avoir pour effet de lier l'appréciation des juridictions qui ont été saisies des différends entre les fournisseurs d'électricité et la société EDF, a eu pour objet d'influer de manière significative sur le comportement des intéressés. Eu égard à sa qualité de fournisseur d'électricité, la société requérante justifie d'un intérêt direct et certain à l'annulation de cette prise de position, qui a été adoptée par la CRE dans le cadre de sa mission de régulation. Par suite, la fin de non-recevoir opposée par cette dernière doit être écartée.<br/>
<br/>
<br/>
              En ce qui concerne la légalité de la délibération attaquée :<br/>
<br/>
              10. En réservant l'application de la force majeure à l'hypothèse d'une impossibilité totale pour l'acheteur d'exécuter l'obligation de paiement de l'ARENH alors que les stipulations de l'article 10 de l'accord-cadre subordonnaient uniquement le bénéfice de cette clause à la condition qu'un événement extérieur, irrésistible et imprévisible rende impossible l'exécution des obligations des parties dans des conditions économiques raisonnables, la Commission de régulation de l'énergie a entaché la délibération attaquée d'une erreur de droit. <br/>
<br/>
              11. Il résulte de ce qui précède que la société Hydroption est fondée à demander l'annulation de la délibération attaquée en tant qu'elle énonce que " la force majeure ne trouverait à s'appliquer que si l'acheteur parvenait à démontrer que sa situation économique rendait totalement impossible l'exécution de l'obligation de paiement de l'ARENH ", ainsi que, dans cette mesure, du rejet de son recours gracieux. L'exécution de la présente décision n'implique pas qu'il soit enjoint à la CRE de prendre une nouvelle délibération.<br/>
<br/>
              12. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la CRE la somme de 3 000 euros à verser à la société requérante au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : La phrase de la délibération n° 2020-071 de la Commission de régulation de l'énergie du 26 mars 2020 énonçant que " La CRE considère néanmoins que la force majeure ne trouverait à s'appliquer que si l'acheteur parvenait à démontrer que sa situation économique rendait totalement impossible l'exécution de l'obligation de paiement de l'ARENH " et, dans cette même mesure, la décision du 31 mars 2020 sont annulées.<br/>
<br/>
Article 2 : La Commission de régulation de l'énergie versera à la société Hydroption la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 3 : Le surplus des conclusions de la requête de la société Hydroption est rejeté.<br/>
Article 4 : La présente décision sera notifiée à la société Hydroption, à la Commission de régulation de l'énergie et à la société Electricité de France.<br/>
Copie en sera adressée pour information à la ministre de la transition écologique et solidaire.<br/>
<br/>
Délibéré à l'issue de la séance du 26 novembre 2021 où siégeaient : M. Rémy Schwartz, président adjoint de la section du contentieux, présidant ; M. Bertrand Dacosta, M. Frédéric Aladjidi, présidents de chambre; Mme Anne Egerszegi, M. Thomas Andrieu, Mme Nathalie Escaut, M. Alexandre Lallet, conseillers d'Etat ; M. Nicolas Agnoux, maître des requêtes et Mme Catherine Fischer-Hirtz, conseillère d'Etat-rapporteure. <br/>
<br/>
              Rendu le 10 décembre 2021.<br/>
<br/>
<br/>
                 Le président : <br/>
                 Signé : M. Rémy Schwartz<br/>
 		La rapporteure : <br/>
      Signé : Mme Catherine Fischer-Hirtz<br/>
                 La secrétaire :<br/>
                 Signé : Mme Laurence Chancerel<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-01-05-02-02 ACTES LÉGISLATIFS ET ADMINISTRATIFS. - DIFFÉRENTES CATÉGORIES D'ACTES. - ACTES ADMINISTRATIFS - NOTION. - ACTES À CARACTÈRE DE DÉCISION. - ACTES NE PRÉSENTANT PAS CE CARACTÈRE. - ACTE DE DROIT SOUPLE - DÉLIBÉRATION DE LA CRE INTERPRÉTANT LA PORTÉE DE LA CLAUSE DE SUSPENSION POUR CAUSE DE FORCE MAJEURE DES ACCORDS-CADRES CONCLUS AVEC EDF POUR L'ARENH - ACTE SUSCEPTIBLE DE FAIRE L'OBJET D'UN RECOURS POUR EXCÈS DE POUVOIR [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">29-06 ENERGIE. - MARCHÉ DE L’ÉNERGIE. - DÉLIBÉRATION DE LA CRE INTERPRÉTANT LA PORTÉE DE LA CLAUSE DE SUSPENSION POUR CAUSE DE FORCE MAJEURE DES ACCORDS-CADRES CONCLUS AVEC EDF POUR L'ARENH  -1) RECOURS POUR EXCÈS DE POUVOIR D'UN FOURNISSEUR D'ÉLECTRICITÉ - RECEVABILITÉ - EXISTENCE [RJ1] - 2) LÉGALITÉ - ABSENCE, EU ÉGARD À SA PORTÉE RESTRICTIVE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">54-01-01-01 PROCÉDURE. - INTRODUCTION DE L'INSTANCE. - DÉCISIONS POUVANT OU NON FAIRE L'OBJET D'UN RECOURS. - ACTES CONSTITUANT DES DÉCISIONS SUSCEPTIBLES DE RECOURS. - ACTE DE DROIT SOUPLE - DÉLIBÉRATION DE LA CRE INTERPRÉTANT LA PORTÉE DE LA CLAUSE DE SUSPENSION POUR CAUSE DE FORCE MAJEURE DES ACCORDS-CADRES CONCLUS AVEC EDF POUR L'ARENH [RJ1].
</SCT>
<ANA ID="9A"> 01-01-05-02-02 L'article 10 du modèle d'accord-cadre annexé à l'arrêté du 12 mars 2019 portant modification de l'arrêté du 28 avril 2011 pris en application du II de l'article 4-1 de la loi n° 2000-108 du 10 février 2000 définit l'évènement de force majeure dont la survenance, en vertu de l'article 13 du même modèle, permet la suspension de l'exécution de l'accord-cadre que le fournisseur souhaitant exercer les droits qui découlent du mécanisme d'accès régulé à l'électricité nucléaire historique (ARENH) prévu à l'article L. 336-1 du code de l'énergie doit conclure avec la société EDF en vertu de l'article L. 336-5 du même code. Par une délibération portant communication sur les mesures en faveur des fournisseurs prenant en compte des effets de la crise sanitaire sur les marchés d'électricité et de gaz naturel, la Commission de régulation de l'énergie (CRE), dans la partie intitulée Evolution du cadre de l'ARENH, a donné son interprétation de l'article 10 du modèle d'accord-cadre en estimant que la force majeure ne trouverait à s'appliquer que si l'acheteur parvenait à démontrer que sa situation économique rendait totalement impossible l'exécution de l'obligation de paiement de l'ARENH.......Cette interprétation, alors même qu'elle ne saurait avoir pour effet de lier l'appréciation des juridictions qui ont été saisies des différends entre les fournisseurs d'électricité et la société EDF, a eu pour objet d'influer de manière significative sur le comportement des intéressés. Un fournisseur d'électricité justifie d'un intérêt direct et certain à l'annulation de cette prise de position, qui a été adoptée par la CRE dans le cadre de sa mission de régulation.</ANA>
<ANA ID="9B"> 29-06 L'article 10 du modèle d'accord-cadre annexé à l'arrêté du 12 mars 2019 portant modification de l'arrêté du 28 avril 2011 pris en application du II de l'article 4-1 de la loi n° 2000-108 du 10 février 2000 définit l'évènement de force majeure dont la survenance, en vertu de l'article 13 du même modèle, permet la suspension de l'exécution de l'accord-cadre que le fournisseur souhaitant exercer les droits qui découlent du mécanisme d'accès régulé à l'électricité nucléaire historique (ARENH) prévu à l'article L. 336-1 du code de l'énergie doit conclure avec la société EDF en vertu de l'article L. 336-5 du même code. Par une délibération portant communication sur les mesures en faveur des fournisseurs prenant en compte des effets de la crise sanitaire sur les marchés d'électricité et de gaz naturel, la Commission de régulation de l'énergie (CRE), dans la partie intitulée Evolution du cadre de l'ARENH, a donné son interprétation de l'article 10 du modèle d'accord-cadre en estimant que la force majeure ne trouverait à s'appliquer que si l'acheteur parvenait à démontrer que sa situation économique rendait totalement impossible l'exécution de l'obligation de paiement de l'ARENH.......1) Cette interprétation, alors même qu'elle ne saurait avoir pour effet de lier l'appréciation des juridictions qui ont été saisies des différends entre les fournisseurs d'électricité et la société EDF, a eu pour objet d'influer de manière significative sur le comportement des intéressés. ......Un fournisseur d'électricité justifie d'un intérêt direct et certain à l'annulation de cette prise de position, qui a été adoptée par la CRE dans le cadre de sa mission de régulation.......2) L'article 10 de l'accord-cadre subordonne uniquement le bénéfice de la clause de force majeure à la condition qu'un événement extérieur, irrésistible et imprévisible rende impossible l'exécution des obligations des parties dans des conditions économiques raisonnables. Dès lors, son application n'est pas réservée à l'hypothèse d'une impossibilité totale pour l'acheteur d'exécuter l'obligation de paiement de l'ARENH.......Par suite, annulation dans cette mesure de la délibération de la CRE.</ANA>
<ANA ID="9C"> 54-01-01-01 L'article 10 du modèle d'accord-cadre annexé à l'arrêté du 12 mars 2019 portant modification de l'arrêté du 28 avril 2011 pris en application du II de l'article 4-1 de la loi n° 2000-108 du 10 février 2000 définit l'évènement de force majeure dont la survenance, en vertu de l'article 13 du même modèle, permet la suspension de l'exécution de l'accord-cadre que le fournisseur souhaitant exercer les droits qui découlent du mécanisme d'accès régulé à l'électricité nucléaire historique (ARENH) prévu à l'article L. 336-1 du code de l'énergie doit conclure avec la société EDF en vertu de l'article L. 336-5 du même code. Par une délibération portant communication sur les mesures en faveur des fournisseurs prenant en compte des effets de la crise sanitaire sur les marchés d'électricité et de gaz naturel, la Commission de régulation de l'énergie (CRE), dans la partie intitulée Evolution du cadre de l'ARENH, a donné son interprétation de l'article 10 du modèle d'accord-cadre en estimant que la force majeure ne trouverait à s'appliquer que si l'acheteur parvenait à démontrer que sa situation économique rendait totalement impossible l'exécution de l'obligation de paiement de l'ARENH.......Cette interprétation, alors même qu'elle ne saurait avoir pour effet de lier l'appréciation des juridictions qui ont été saisies des différends entre les fournisseurs d'électricité et la société EDF, a eu pour objet d'influer de manière significative sur le comportement des intéressés. Un fournisseur d'électricité justifie d'un intérêt direct et certain à l'annulation de cette prise de position, qui a été adoptée par la CRE dans le cadre de sa mission de régulation.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ3] Cf., s'agissant du régime contentieux des actes de droit souple des autorités de régulation, CE, Assemblée, 21 mars 2016, Société Fairvesta International GmbH et autres, n°s 368082 368083 368084, p. 76 ; CE, Assemblée, 21 mars 2016, Société NC Numericable, n° 390023, p. 88.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
