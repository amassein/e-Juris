<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042752996</ID>
<ANCIEN_ID>JG_L_2020_12_000000442497</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/75/29/CETATEXT000042752996.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème chambre, 24/12/2020, 442497, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-12-24</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>442497</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FABIANI, LUC-THALER, PINATEL</AVOCATS>
<RAPPORTEUR>M. Didier Ribes</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Mireille Le Corre</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2020:442497.20201224</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B... A... a demandé au tribunal administratif de Nîmes, d'une part, de suspendre l'exécution de la décision 17 janvier 2020 par laquelle la ministre des armées l'a radié des cadres, d'autre part, de suspendre l'exécution de l'arrêté du 21 avril 2020 par lequel le ministre de l'intérieur a prononcé la cessation d'office de l'état militaire de sa carrière à compter du 22 avril 2020 et, enfin, d'enjoindre à la ministre des armées de le réintégrer et de reconstituer sa carrière depuis le 21 avril 2020, dans le délai d'un mois à compter de l'ordonnance du juge des référés, sous astreinte de 400 euros par jour de retard. Par une ordonnance n° 2001709 du 21 juillet 2020, le juge des référés de ce tribunal, statuant sur le fondement de l'article L. 521-1 du code de justice administrative, a décidé qu'il n'y avait pas lieu de statuer sur les conclusions dirigées contre la décision du 21 avril 2020 et rejeté le surplus des conclusions.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 5 et 18 août 2020 au secrétariat du contentieux du Conseil d'Etat, M. A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance en tant qu'elle a rejeté le surplus de ses conclusions ;<br/>
<br/>
              2°) statuant en référé, de faire droit dans cette mesure à ses demandes ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de la défense ;<br/>
              - le code de la sécurité intérieure ;<br/>
              - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Didier Ribes, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Mireille Le Corre, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Fabiani, Luc-Thaler, Pinatel, avocat de M. A... ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Aux termes de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision. / (...) ".<br/>
<br/>
              2. Il ressort des énonciations de l'ordonnance attaquée que l'épouse de M. A..., gendarme au peloton de sûreté maritime et portuaire militaire de Toulon, s'est présentée le 3 juin 2019 à la compagnie de gendarmerie départementale de La Valette-du-Var pour signaler des faits de violences commis sur elle par son conjoint dans la nuit du 1er au 2 juin, qu'une enquête de flagrance pour violences suivies d'incapacité n'excédant pas huit jours par conjoint ou concubin a été ouverte et que M. A... a été placé en garde à vue le 5 juin 2019 puis sous contrôle judiciaire du 7 juin au 7 novembre 2019. Il a été suspendu de ses fonctions par décision du 8 juin 2019 du général de brigade commandant la gendarmerie maritime, puis réintégré et muté à compter du 16 décembre 2019 au peloton de surveillance et d'intervention de gendarmerie de Pertuis. Par jugement correctionnel du 7 novembre 2019, le tribunal de grande instance de Toulon a condamné M. A... à une peine de dix mois d'emprisonnement avec sursis pour les faits de violences ayant entraîné une incapacité n'excédant pas huit jours commises par un conjoint, sans inscription au bulletin n° 2 du casier judiciaire. Après avoir recueilli l'avis du conseil d'enquête, la ministre des armées a, par décision du 17 janvier 2020, prononcé à son encontre la sanction disciplinaire de radiation des cadres. M. A... demande l'annulation de l'ordonnance du juge des référés du tribunal administratif de Nîmes du 21 juillet 2020, en tant qu'elle a rejeté sa demande de suspension de la décision de la ministre des armées.<br/>
<br/>
              Sur la procédure disciplinaire :<br/>
<br/>
              3. Aux termes de l'article R. 4137-66 du code de la défense : " L'envoi devant le conseil d'enquête est ordonné par le ministre de la défense ou par les autorités militaires dont la liste est fixée par arrêté du ministre de la défense. / L'ordre d'envoi devant le conseil d'enquête mentionne les faits à l'origine de la saisine du conseil et précise les circonstances dans lesquelles ils se sont produits. / L'avis du conseil d'enquête doit être remis à l'autorité habilitée à prononcer la sanction dans les trois mois qui suivent la date d'émission de l'ordre d'envoi. / Si aucun avis n'est rendu à l'issue de ce délai, le ministre de la défense met le conseil en demeure de se prononcer dans un délai déterminé qui ne peut être supérieur à un mois. S'il n'est pas fait droit à cette demande et sauf impossibilité matérielle pour le conseil de se réunir, l'autorité habilitée constate la carence du conseil et prononce la sanction, sans l'avis de ce conseil, après avoir invité le militaire à présenter sa défense. / (...) ".<br/>
<br/>
              4. Il ressort des pièces du dossier soumis au juge des référés que le conseil d'enquête, saisi le 12 juillet 2019, a été mis en demeure de se prononcer par la ministre des armées le 23 octobre et a rendu son avis le 28 novembre, soit cinq jours seulement après l'expiration du délai mentionné au troisième alinéa de l'article R. 4137-66 du code de la défense. Ce délai, qui a seulement pour objet de permettre à l'autorité habilitée de se prononcer valablement en l'absence d'avis émis par le conseil d'enquête, n'est pas prescrit à peine d'irrégularité de la procédure. Par suite, le moyen tiré de ce que le juge des référés du tribunal administratif de Nîmes aurait commis une erreur de droit en regardant le moyen tiré de la méconnaissance de ces dispositions comme impropre, en l'état de l'instruction, à créer un doute sérieux quant à la légalité de la décision attaquée doit être écarté.<br/>
<br/>
              5. Le juge des référés n'a pas davantage commis d'erreur de droit en écartant comme impropres, en l'état de l'instruction, à créer un doute sérieux quant à la légalité de la décision attaquée les moyens tirés de l'irrégularité de l'ordre d'envoi devant le conseil d'enquête et du manque d'impartialité de l'enquête administrative.<br/>
<br/>
              Sur la sanction prononcée :<br/>
<br/>
              6. Aux termes de l'article R. 434-12 du code de la sécurité intérieure : " Le policier ou le gendarme ne se départ de sa dignité en aucune circonstance. / En tout temps, dans ou en dehors du service, y compris lorsqu'il s'exprime à travers les réseaux de communication électronique sociaux, il s'abstient de tout acte, propos ou comportement de nature à nuire à la considération portée à la police nationale et à la gendarmerie nationale. Il veille à ne porter, par la nature de ses relations, aucune atteinte à leur crédit ou à leur réputation ".<br/>
<br/>
              7. Il ressort des pièces du dossier soumis au juge des référés qu'à la suite d'une dispute, M. A..., à qui son épouse barrait le passage pour l'empêcher de sortir du domicile conjugal, lui a fait une clé de bras autour du cou et l'a laissée inanimée sur le sol, avant que son fils, alerté par le bruit, ne la rejoigne et ne lui porte secours. Eu égard à l'office du juge des référés, le moyen tiré de ce que, compte tenu de la gravité des faits commis par M. A... et des fonctions exercées par l'intéressé, le juge des référés aurait commis une erreur de droit en considérant comme impropre, en l'état de l'instruction, à créer un doute sérieux quant à la légalité de cette décision le moyen tiré du caractère disproportionné de cette sanction doit être écarté.<br/>
<br/>
              8. Il résulte de ce qui précède que le pourvoi de M. A... contre l'ordonnance attaquée, qui est suffisamment motivée, doit être rejeté.<br/>
<br/>
              9. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de M. A... est rejeté.<br/>
Article 2 : La présente décision sera notifiée à M. B... A..., à la ministre des armées et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
