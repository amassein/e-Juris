<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037254073</ID>
<ANCIEN_ID>JG_L_2018_07_000000421136</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/25/40/CETATEXT000037254073.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème chambre, 26/07/2018, 421136, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-07-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>421136</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Olivier Gariazzo</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Romain Victor</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:421136.20180726</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, enregistrée le 1er juin 2018 au secrétariat du contentieux du Conseil d'État, M. A... B...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le troisième paragraphe du a du B du I et les dixième et onzième paragraphes du b du A du II de la circulaire du 22 mai 2014 de la garde des Sceaux, ministre de la justice, du ministre des finances et des comptes publics et du secrétaire d'État au budget relative à la lutte contre la fraude fiscale ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la Constitution, notamment son Préambule et son article 61-1 ;<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Olivier Gariazzo, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Romain Victor, rapporteur public.<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. M. B... demande l'annulation pour excès de pouvoir du troisième paragraphe du a du B du I et des dixième et onzième paragraphes du b du A du II de la circulaire du 22 mai 2014 relative à la lutte contre la fraude fiscale, par laquelle la garde des sceaux, ministre de la justice, le ministre des finances et des comptes publics et le secrétaire d'État au budget ont précisé les modalités de mise en oeuvre des nouvelles dispositions prévues par la loi organique du 6 décembre 2013 relative au procureur de la République financier et la loi du 6 décembre 2013 relative à la lutte contre la fraude fiscale et la grande délinquance économique et financière.<br/>
<br/>
              2. En premier lieu, le troisième paragraphe du a du B du I de la circulaire attaquée, inclus dans une sous-partie relative à l'élargissement du champ de la procédure d'enquête fiscale, énonce que " la poursuite, par l'autorité judiciaire, des fraudes fiscales comprises dans le champ de cette procédure est conditionnée à une plainte préalable de la Direction générale des finances publiques (DGFIP) et à un avis conforme de la commission des infractions fiscales qui se prononce sur l'existence de " présomptions caractérisées " de fraude fiscale. La procédure fiscale est conduite par la DGFIP en parallèle ou en aval de l'enquête judiciaire ". <br/>
<br/>
              3. M. B... conteste ce paragraphe en tant qu'il n'écarterait pas la possibilité de dépôt d'une plainte pour fraude fiscale dans le cas où l'administration entendrait se fonder sur des documents d'origine illicite. Toutefois, ce paragraphe, qui se borne à rappeler que l'engagement par l'administration de poursuites pénales pour fraude fiscale est subordonné à un avis conforme de la commission des infractions fiscales, n'a pas pour objet de commenter les conditions d'engagement des poursuites et de mise en oeuvre de l'article L. 228 du livre des procédures fiscales s'agissant des pièces ou documents utilisés par les services fiscaux et ne prend nullement position sur cette question. Ce paragraphe ne contient ainsi, pour ce qui concerne les documents sur lesquels l'administration fiscale est susceptible de se fonder au soutien d'un dépôt de plainte pour fraude fiscale, aucune disposition impérative à caractère général susceptible d'être déférée au juge de l'excès de pouvoir.<br/>
<br/>
              4. En second lieu, les dixième et onzième paragraphes du b du A du II de la circulaire attaquée énoncent que " dans la plupart des cas, l'application des pénalités fiscales suffit à sanctionner, par une réparation pécuniaire appropriée, les manquements aux obligations prescrites par le code général des impôts. / En revanche, dans certaines situations, les poursuites correctionnelles constituent le seul moyen adapté au préjudice financier mais aussi moral commis au détriment de la collectivité ainsi qu'en témoignent certaines affaires récentes. L'administration fiscale doit en outre être attentive à l'exemplarité que confère une condamnation pénale pour fraude fiscale, en particulier lorsque de nouvelles typologies de fraudes sont mises à jour, de manière à dissuader les fraudeurs potentiels d'y recourir ". <br/>
<br/>
              5. M. B... conteste ces paragraphes en ce qu'ils ne préciseraient pas que l'application simultanée des articles 1728 et 1741 du code général des impôts est réservée aux seuls cas les plus graves de fraude fiscale par omission volontaire de déclaration dans les délais prescrits et en ce qu'ils omettraient de préciser, d'une part, qu'un contribuable qui a obtenu la décharge de l'imposition pour un motif de fond par une décision juridictionnelle devenue définitive ne peut pas être condamné pour fraude fiscale et, d'autre part, que le montant global des sanctions encourues en application des articles 1728 et 1741 du code général des impôts ne peut excéder le montant le plus élevé de l'une des sanctions encourues. Toutefois, ces paragraphes, qui se bornent à rappeler la possibilité de sanctionner les manquements des contribuables aux obligations prescrites par le code général des impôts, d'une part, par des pénalités fiscales et, d'autre part, dans certaines situations, par des poursuites correctionnelles, n'a pas pour objet de commenter les conditions dans lesquelles les pénalités fiscales appliquées par l'administration et les peines correctionnelles pour fraude fiscale peuvent se cumuler pour un même comportement et ne prennent nullement position sur cette question. Ces paragraphes ne contiennent ainsi, pour ce qui concerne la possibilité d'appliquer de manière cumulative les articles 1728 et 1741 du code général des impôts, aucune disposition impérative à caractère général susceptible d'être déférée au juge de l'excès de pouvoir.<br/>
<br/>
              6. Il résulte de ce qui précède que la requête ne peut qu'être rejetée, sans qu'il soit besoin pour le Conseil d'État de se prononcer sur le renvoi au Conseil constitutionnel des questions prioritaires de constitutionnalité tirées de ce que l'article L. 228 du livre des procédures fiscales, d'une part, et les articles 1728 et 1741 du code général des impôts, d'autre part, porteraient atteinte aux droits et libertés garantis par la Constitution. <br/>
<br/>
              7. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'État qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>                             D E C I D E :<br/>
                                           --------------<br/>
<br/>
Article 1er : La requête de M. B... est rejetée.<br/>
<br/>
Article 2 : La présente décision sera notifiée à M. A... B..., au ministre de l'action et des comptes publics et au garde des sceaux, ministre de la justice.<br/>
Copie en sera adressée au Conseil constitutionnel et au Premier ministre.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
