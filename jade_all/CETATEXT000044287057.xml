<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044287057</ID>
<ANCIEN_ID>JG_L_2021_10_000000450358</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/28/70/CETATEXT000044287057.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème chambre, 25/10/2021, 450358, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-10-25</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>450358</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP THOUVENIN, COUDRAY, GREVY</AVOCATS>
<RAPPORTEUR>Mme Catherine Brouard-Gallet</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Raphaël Chambon</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:450358.20211025</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. P... G..., M. H... A... et M. E... D... ont demandé au tribunal administratif de Melun d'annuler les opérations électorales qui se sont déroulées les 15 mars et 28 juin 2020 pour l'élection des conseillers municipaux et communautaires de la commune d'Ivry-sur-Seine (Val-de-Marne). Par un jugement n° 2004927 du 4 février 2021, le tribunal administratif a rejeté leur protestation.<br/>
<br/>
              Par une requête, un mémoire complémentaire et un nouveau mémoire, enregistrés les 4 mars, 6 avril et 1er septembre 2021 au secrétariat du contentieux du Conseil d'Etat, M. G..., M. A... et M. D... demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) d'annuler les opérations électorales qui se sont déroulées les 15 mars et 28 juin 2020 dans la commune d'Ivry-sur-Seine.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code électoral ;<br/>
              - la loi n° 2020-290 du 23 mars 2020 ;<br/>
              - la décision QPC n° 2020-849 du 17 juin 2020 du Conseil constitutionnel ;<br/>
              - la décision du Conseil d'Etat n° 450358 du 13 juillet 2021 décidant de ne pas renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité posée par M. G... et autres ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Catherine Brouard-Gallet, conseillère d'Etat en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Raphaël Chambon, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Thouvenin, Coudray, Grévy, avocat de M. G... et autres ;<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. À l'issue du second tour des opérations électorales qui se sont déroulées les 15 mars et 28 juin 2020 pour l'élection des conseillers municipaux et communautaires d'Ivry-sur-Seine (Val-de-Marne), la liste " Ensemble pour Ivry " conduite par M. N... M..., maire sortant, a recueilli 5 618 voix, soit 65,55 % des suffrages exprimés et obtenu quarante-et-un sièges au conseil municipal et un siège au conseil communautaire, la liste " Ivry autrement " conduite par M. R... Q... a recueilli 1732 voix, soit 20,21 % des suffrages exprimés et obtenu cinq sièges au conseil municipal et la liste " Ivry c'est vous " conduite par Mme K... J... a recueilli 1 220 voix, soit 14,23 % des suffrages exprimés et obtenu trois sièges au conseil municipal. M. P... G... et M. H... A..., inscrits sur les listes électorales à Ivry-sur-Seine, et M. E... D..., candidat sur la liste " Ivry demain " présente au premier tour, relèvent appel du jugement du 4 février 2021 par lequel le tribunal administratif de Melun a rejeté leur protestation.<br/>
<br/>
              2. En premier lieu, aux termes de l'article L. 264 du code électoral : " Une déclaration de candidature est obligatoire pour chaque tour de scrutin. (...) / Seules peuvent se présenter au second tour les listes ayant obtenu au premier tour un nombre de suffrages au moins égal à 10 % du total des suffrages exprimés. Ces listes peuvent être modifiées dans leur composition pour comprendre des candidats ayant figuré au premier tour sur d'autres listes sous réserve que celles-ci ne se présentent pas au second tour et qu'elles aient obtenu au premier tour au moins 5 % des suffrages exprimés. En cas de modification de la composition d'une liste, l'ordre de présentation des candidats peut également être modifié. / Les candidats ayant figuré sur une même liste au premier tour ne peuvent figurer au second tour que sur une liste. Le choix de la liste sur laquelle ils sont candidats au second tour est notifié à la préfecture ou à la sous-préfecture par la personne ayant eu la qualité de responsable de la liste constituée par ces candidats au premier tour ". Aux termes de l'article L. 265 du même code : " La déclaration de candidature résulte du dépôt à la préfecture ou à la sous-préfecture d'une liste répondant aux conditions fixées aux articles L. 260, L. 263, L. 264 et LO. 265-1. Il en est délivré récépissé. / Elle est faite collectivement pour chaque liste par la personne ayant la qualité de responsable de liste. A cet effet, chaque candidat établit un mandat signé de lui, confiant au responsable de liste le soin de faire ou de faire faire, par une personne déléguée par lui, toutes déclarations et démarches utiles à l'enregistrement de la liste, pour le premier et le second tours (...) / ". Enfin, aux termes de l'article L. 267 du même code : " Les déclarations de candidatures doivent être déposées au plus tard : pour le premier tour, le troisième jeudi qui précède le jour du scrutin, à dix-huit heures ; pour le second tour, le mardi qui suit le premier tour, à dix-huit heures. Aucun retrait volontaire ou remplacement de candidat n'est accepté après le dépôt de la liste. Les retraits des listes complètes qui interviennent avant l'expiration des délais prévus à l'alinéa 1 du présent article pour le dépôt des déclarations de candidatures sont enregistrés ; ils comportent la signature de la majorité des candidats de la liste ".<br/>
<br/>
              3. Il résulte des dispositions des articles L. 264 et L. 265 du code électoral citées au point 2, qui prévoient que, d'une part, chaque candidat confie au responsable de liste, par mandat signé, le soin de faire ou de faire faire toutes déclarations et démarches utiles à l'enregistrement de la liste, pour le premier et le second tours et que, d'autre part, le choix de la liste sur laquelle les candidats ayant figuré sur une même liste au premier tour peuvent figurer au second tour est notifié à la préfecture ou à la sous-préfecture par la personne ayant eu la qualité de responsable de la liste constituée par ces candidats au premier tour, que le législateur a entendu confier au seul responsable de liste la capacité, entre les deux tours de scrutin, de choisir de fusionner la liste dont il est à la tête avec une ou plusieurs autres listes présentes au second tour afin de constituer une liste unique.<br/>
<br/>
              4. Il résulte de l'instruction qu'à l'issue du premier tour des opérations électorales en vue de l'élection des conseillers municipaux et communautaires de la commune d'Ivry-sur-Seine, la liste " Ivry demain ", conduite par Mme L..., a recueilli 2 531 voix, soit 22,10 % des suffrages exprimés, que cette liste n'a pas fait l'objet d'une notification à la préfecture du Val-de-Marne en vue de sa présentation au second tour de scrutin et enfin que M. M... a déclaré à la préfecture, à cet effet, la liste " Ensemble pour Ivry ", qui comprenait cinq candidats issus de la liste " Ivry demain ", dont la responsable de cette liste, Mme L.... Il résulte également de l'instruction que, conformément aux dispositions précitées de l'article L. 265 du code électoral, Mme L... avait reçu mandat des candidats de sa liste pour faire " toutes déclarations et démarches utiles à l'enregistrement de sa liste " pour le premier comme pour le second tour et qu'elle avait ainsi la qualité de responsable de sa liste. Par suite, d'une part, en l'absence d'enregistrement, auprès de la préfecture, de la liste " Ivry demain " en vue de sa candidature au second tour, M. G... et autres ne sauraient utilement soutenir que la notification auprès de la préfecture du Val-de-Marne, par 28 des 51 candidats de la liste " Ivry demain ", du retrait de leur candidature pour le second tour des opérations électorales devait être regardé comme le retrait d'une liste complète au sens de l'article L. 267 du code électoral, de sorte que cette circonstance ne faisait pas obstacle à ce que la responsable de la liste " Ivry demain " décidât de la fusion de cette liste avec la liste " Ensemble pour Ivry ", conduite par M. M.... D'autre part, les requérants ne sont pas fondés à soutenir que la fusion de la liste " Ivry demain " avec la liste " Ensemble pour Ivry ", bien que réalisée contre l'avis majoritaire des candidats de la liste " Ivry demain ", ainsi que la composition de la liste " Ensemble pour Ivry " présentée au second tour et issue de cette fusion, méconnaîtraient les articles L. 264 et L. 265 du code électoral ni, dans les circonstances de l'espèce, le pluralisme des courants d'idées et d'opinions et de participation équitable des partis et groupements politiques à la vie démocratique de la Nation. Dès lors, M. G... et autres ne sont pas fondés à soutenir que la composition de la liste " Ensemble pour Ivry " présentée au second tour serait irrégulière et caractériserait une manœuvre ayant altéré la sincérité du scrutin. <br/>
<br/>
              5. En deuxième lieu, M. G... et autres font valoir que le 29 janvier, le 16 février et le 26 février 2020, des affiches à caractère injurieux et diffamatoire ont été apposées dans la commune, circonstance qui a été de nature selon eux à altérer la sincérité du scrutin. Si, contrairement à ce qui est soutenu en défense, des irrégularités ayant affecté les opérations d'un premier tour de scrutin à l'issue duquel aucun candidat n'a été proclamé élu peuvent être invoqués à l'appui de conclusions dirigées contre les résultats du second tour, dès lors que ceux-ci ont pu être affectés par ces irrégularités, il résulte toutefois de l'instruction que ces affiches, qui dénonçaient, en des termes parfois véhéments, l'accord conclu entre le parti socialiste, " Europe-Ecologie Les Verts " et " La France Insoumise " ne peuvent être regardées, eu égard à leur teneur, comme ayant excédé les limites de la propagande électorale. Si certaines de ces affiches ont reproduit la photographie de Mme O... I..., candidate en troisième position sur la liste " Ivry demain ", tout en lui prêtant des opinions outrancières, il ne résulte pas de l'instruction que cet affichage, qui s'est déroulé un mois avant le premier tour, ait présenté un caractère massif, ni que Mme I... n'ait pas été en mesure d'y répondre utilement. Compte tenu en outre de l'écart important des voix qui a séparé les listes " Ivry ensemble " et " Ivry demain " arrivées en tête à l'issue du premier tour, ces affiches, pour regrettables qu'elles soient, ne peuvent être regardées comme ayant altéré la sincérité du scrutin.<br/>
<br/>
              6. En troisième et dernier lieu, l'émergence d'un nouveau coronavirus, responsable de la maladie à coronavirus 2019 ou covid-19 et particulièrement contagieux, a été qualifiée d'urgence de santé publique de portée internationale par l'Organisation mondiale de la santé le 30 janvier 2020, puis de pandémie le 11 mars 2020. La propagation du virus sur le territoire français a conduit le ministre des solidarités et de la santé à prendre, à compter du 4 mars 2020, des mesures de plus en plus strictes destinées à réduire les risques de contagion. Dans ce contexte, le Premier ministre a adressé à l'ensemble des maires le 7 mars 2020 une lettre présentant les mesures destinées à assurer le bon déroulement des élections municipales et communautaires prévues les 15 et 22 mars 2020. Ces mesures ont été précisées par une circulaire du ministre de l'intérieur du 9 mars 2020 relative à l'organisation des élections municipales des 15 et 22 mars 2020 en situation d'épidémie de coronavirus covid-19, formulant des recommandations relatives à l'aménagement des bureaux de vote et au respect des consignes sanitaires, et par une instruction de ce ministre, du même jour, destinée à faciliter l'exercice du droit de vote par procuration. Après consultation par le Gouvernement du conseil scientifique mis en place pour lui donner les informations scientifiques utiles à l'adoption des mesures nécessaires pour faire face à l'épidémie de covid-19, les 12 et 14 mars 2020, le premier tour des élections municipales a eu lieu comme prévu le 15 mars 2020. A l'issue du scrutin, les conseils municipaux ont été intégralement renouvelés dans 30 143 communes ou secteurs. Le taux d'abstention a atteint 55,34 % des inscrits, contre 36,45 % au premier tour des élections municipales de 2014.<br/>
<br/>
              7. Au vu de la situation sanitaire, l'article 19 de la loi du 23  mars 2020 d'urgence pour faire face à l'épidémie de covid-19 a reporté le second tour des élections, initialement fixé au 22 mars 2020, au plus tard en juin 2020 et prévu que : " Dans tous les cas, l'élection régulière des conseillers municipaux et communautaires, des conseillers d'arrondissement, des conseillers de Paris et des conseillers métropolitains de Lyon élus dès le premier tour organisé le 15 mars 2020 reste acquise, conformément à l'article 3 de la Constitution ". Ainsi que le Conseil constitutionnel l'a jugé dans sa décision n° 2020-849 QPC du 17 juin 2020, ces dispositions n'ont ni pour objet ni pour effet de valider rétroactivement les opérations électorales du premier tour ayant donné lieu à l'attribution de sièges et ne font ainsi pas obstacle à ce que ces opérations soient contestées devant le juge de l'élection.<br/>
<br/>
              8. Aux termes de l'article L. 262 du code électoral, applicable aux communes de mille habitants et plus : " Au premier tour de scrutin, il est attribué à la liste qui a recueilli la majorité absolue des suffrages exprimés un nombre de sièges égal à la moitié du nombre des sièges à pourvoir, arrondi, le cas échéant, à l'entier supérieur lorsqu'il y a plus de quatre sièges à pourvoir et à l'entier inférieur lorsqu'il y a moins de quatre sièges à pourvoir. Cette attribution opérée, les autres sièges sont répartis entre toutes les listes à la représentation proportionnelle suivant la règle de la plus forte moyenne, sous réserve de l'application des dispositions du troisième alinéa ci-après. / Si aucune liste n'a recueilli la majorité absolue des suffrages exprimés au premier tour, il est procédé à un deuxième tour (...) ". Aux termes de l'article L. 273-8 du code électoral : " Les sièges de conseiller communautaire sont répartis entre les listes par application aux suffrages exprimés lors de cette élection des règles prévues à l'article L. 262. (...) ".<br/>
<br/>
              9. Ni par ces dispositions ni par celles de la loi du 23 mars 2020 le législateur n'a subordonné à un taux de participation minimal la répartition des sièges au conseil municipal et au conseil communautaire à l'issue du premier tour de scrutin dans les communes de mille habitants et plus, lorsqu'une liste a recueilli la majorité absolue des suffrages exprimés. Le niveau de l'abstention n'est ainsi, par lui-même, pas de nature à remettre en cause les résultats du scrutin, s'il n'a pas altéré, dans les circonstances de l'espèce, sa sincérité.<br/>
<br/>
              10. En l'espèce, M. G... et autres font valoir que le taux d'abstention s'est élevé à 58,92 % au premier tour et à 68,72 % au second tour dans leur commune, sans invoquer aucune autre circonstance relative au déroulement de la campagne électorale ou du scrutin dans la commune qui montrerait, en particulier, qu'il aurait été porté atteinte au libre exercice du droit de votre ou à l'égalité entre les candidats. Dans ces conditions, le niveau de l'abstention constatée, au premier comme au second tour, ne peut être regardé comme ayant altéré la sincérité du scrutin.<br/>
<br/>
              11. Il résulte de tout ce qui précède que M. G..., M. A... et M. D... ne sont pas fondés à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Melun a rejeté leur protestation.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : La requête de M. G..., de M. A... et de M. D... est rejetée.<br/>
Article 2 : La présente décision sera notifiée à M. P... G..., à M. H... A..., à M. E... D..., à M. N... M..., à Mme K... C..., à M. R... Q..., à la Commission nationale des comptes de campagne et des financements politiques et au ministre de l'intérieur.<br/>
              Délibéré à l'issue de la séance du 30 septembre 2021 où siégeaient : Mme Maud Vialettes, présidente de chambre, présidant ; Mme Carine Soulay, conseillère d'Etat et Mme Catherine Brouard-Gallet, conseillère d'Etat en service extraordinaire-rapporteure. <br/>
<br/>
              Rendu le 25 octobre 2021.<br/>
<br/>
                 La présidente : <br/>
                 Signé : Mme Maud Vialettes<br/>
<br/>
<br/>
 		La rapporteure : <br/>
      Signé : Mme Catherine Brouard-Gallet<br/>
<br/>
<br/>
                 La secrétaire :<br/>
                 Signé : Mme F... B...<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
