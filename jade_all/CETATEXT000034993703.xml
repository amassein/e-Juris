<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034993703</ID>
<ANCIEN_ID>JG_L_2017_06_000000401482</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/99/37/CETATEXT000034993703.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème chambre, 22/06/2017, 401482, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-06-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>401482</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR>Mme Déborah Coricon</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Vincent Daumas</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2017:401482.20170622</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              M. et Mme B...A...ont demandé au tribunal administratif de Strasbourg de prononcer la décharge des cotisations supplémentaires d'impôt sur le revenu et des contributions sociales auxquelles ils ont été assujettis au titre des années 2007 et 2008 ainsi que des pénalités dont ces droits ont été assortis. Par un jugement n° 1204256, 1302276, 1302460 et 1302981 du 21 avril 2015, le tribunal administratif de Strasbourg a rejeté leurs demandes.<br/>
<br/>
              Par un arrêt n°15NC0134 du 12 mai 2016, la cour administrative d'appel de Nancy a rejeté l'appel formé par M. et Mme A...contre ce jugement.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés le 13 juillet et le 13 octobre 2016 au secrétariat du contentieux du Conseil d'Etat, M. et Mme A...demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
               2°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code général des impôts et le livre des procédures fiscales ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Déborah Coricon, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Vincent Daumas, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Piwnica, Molinié, avocat de M. et Mme B...A...;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. Aux termes de l'article L. 822-1 du code de justice administrative : " Le pourvoi en cassation devant le Conseil d'Etat fait l'objet d'une procédure préalable d'admission. L'admission est refusée par décision juridictionnelle si le pourvoi est irrecevable ou n'est fondé sur aucun moyen sérieux ". <br/>
<br/>
              2. Pour demander l'annulation de l'arrêt qu'ils attaquent, M. et Mme A...soutiennent que la cour administrative d'appel de Nancy :<br/>
              - a commis une erreur de droit en jugeant que dès lors que leur demande irrévocable de rencontrer le supérieur hiérarchique du vérificateur, en cas de maintien des redressements, était prématurée, l'administration pouvait ne pas y faire droit ;<br/>
              - l'a insuffisamment motivé et a commis une erreur de droit en jugeant, à propos des sommes imposées dans la catégorie des traitements et salaires, qu'elles n'étaient pas justifiées par des avances qu'ils auraient consenties à la société Lorraine Services ;<br/>
              - l'a insuffisamment motivé et a commis une erreur de droit en jugeant, à propos des sommes de 350 000 euros et 126 930 euros, qu'elles n'étaient pas justifiées par une cession de créance de M. A...à la SAS Lorraine Services qui les lui avaient versées ;<br/>
              - a dénaturé ses écritures, à propos de la somme de 119 000 euros, en relevant qu'ils soutenaient qu'elle était liée à une somme que M. A...devait rembourser à l'EURL Lorraine Services Acquisitions alors qu'il s'agissait de lui transférer des actions que celle-ci avait acquises pour ce montant et a commis une erreur de droit en estimant que ce transfert n'était pas établi par l'ordre de mouvement produit ;<br/>
              - l'a insuffisamment motivé et commis une erreur de droit, s'agissant des sommes de 990 000 euros, 514 729 euros et 528 193 euros,  en jugeant que M. A...ne justifiait pas, d'une part, que la créance qu'il avait détenue sur deux SCI avait été cédée à la société Lorraine Services et, d'autre part, que les sommes appréhendées ne correspondaient pas à des remboursements d'avances ;<br/>
              - a commis une erreur de droit  en rejetant la demande de restitution de l'impôt qu'ils avaient présentée au motif que les sommes en cause n'avaient pas été imposées sur le fondement du a mais du c de l'article 111 du code général des impôts ;<br/>
              - a omis de statuer sur son moyen relatif aux pénalités afférentes aux sommes de 350 000, 126 930, 990 000, 514 729 et 528 193 euros imposées en tant que revenus de capitaux mobiliers ;<br/>
              - a omis de statuer sur son moyen relatif aux pénalités afférentes aux sommes imposées comme traitement et salaires ;<br/>
              - l'a insuffisamment motivé et a commis une erreur de droit en jugeant, pour les pénalités de mauvaise foi relatives aux sommes imposées en traitement et salaires ainsi qu'au titre des frais de cadeaux, mission et déplacement, que l'administration avait établi son intention d'éluder l'impôt au seul motif que les redressements dont les droits étaient assortis de telles pénalités étaient fondés ; <br/>
              - a commis une erreur de droit en jugeant, s'agissant des majorations pour  manoeuvres frauduleuses, que l'administration avait établi la preuve de l'existence de telles manoeuvres sans rechercher si les avoirs consenties par la SAS Lorraine Services pour masquer les encaissements par M. A...de sommes de même montant l'avaient été avec la participation de celui-ci ou sur ses ordres. <br/>
<br/>
              3. Eu égard aux moyens soulevés, il y lieu d'admettre les conclusions du pourvoi qui sont dirigées contre l'arrêt attaqué en tant qu'il s'est prononcé sur les conclusions relatives aux pénalités appliquées aux droits dus au titre de l'imposition des sommes de 350 000, 126 930, 990 000, 514 729 et 528 193 euros dans la catégorie des revenus de capitaux mobiliers. En revanche, aucun des moyens n'est de nature à justifier l'admission du surplus des conclusions du pourvoi.<br/>
<br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er : Les conclusions du pourvoi de M. et Mme A... qui sont dirigées contre l'arrêt attaqué en tant qu'il s'est prononcé sur les conclusions relatives aux pénalités appliquées aux droits dus au titre de l'imposition des sommes de 350 000, 126 930, 990 000, 514 729 et 528 193 euros dans la catégorie des revenus de capitaux mobiliers sont admises.<br/>
<br/>
Article 2 : Le surplus des conclusions de pourvoi de M. et Mme A... n'est pas admis.<br/>
<br/>
Article 3 : La présente décision sera notifiée à M. et Mme A.... <br/>
Une copie en sera adressée pour information au ministre de l'action et des comptes publics<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
