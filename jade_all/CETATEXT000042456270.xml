<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042456270</ID>
<ANCIEN_ID>JG_L_2020_10_000000430526</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/45/62/CETATEXT000042456270.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère chambre, 21/10/2020, 430526, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-10-21</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>430526</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Pierre Boussaroque</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Vincent Villette</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2020:430526.20201021</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu les procédures suivantes :<br/>
<br/>
              1° Sous le n° 430526, par une requête et un mémoire en réplique, enregistrés les 6 mai et 3 décembre 2019 au secrétariat du contentieux du Conseil d'Etat, l'association de défense de la concurrence dans le secteur de l'identification des produits de consommation courante (ADCSIP) demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir, d'une part, le décret n° 2019-177 du 8 mars 2019 relatif aux identifiants pour la traçabilité des produits du tabac pris en application de l'article 2 de la loi n° 93-1419 du 31 décembre 1993 relative à l'Imprimerie nationale et, d'autre part, l'arrêté du 16 avril 2019 relatif à la désignation de l'entité de délivrance des identifiants prévus par l'article L. 3512-23 du code de la santé publique ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              2° Sous le n° 431830, par une requête, un mémoire complémentaire et un mémoire en réplique, enregistrés les 20 juin et 19 juillet 2019 et le 17 juillet 2020 au secrétariat du contentieux du Conseil d'Etat, l'association pour une nouvelle politique anti-tabac (PUNPAT) demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret n° 2019-187 du 13 mars 2019 relatif au dispositif de traçabilité des produits du tabac, la décision du Premier ministre rejetant sa demande de rapporter ce décret, ainsi que l'arrêté du 16 avril 2019 relatif à la désignation de l'entité de délivrance des identifiants prévus par l'article L. 3512-23 du code de la santé publique ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 1 500 euros au titre de l'article L. 761 1 du code de justice administrative.<br/>
<br/>
			....................................................................................<br/>
<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu :<br/>
              - la convention-cadre de l'Organisation mondiale de la santé pour la lutte antitabac et le protocole pour éliminer le commerce illicite des produits du tabac ;<br/>
              - le traité sur le fonctionnement de l'Union européenne ;<br/>
              - la directive 2014/40 du Parlement européen et du Conseil du 3 avril 2014;<br/>
              - le règlement d'exécution (UE) 2018/574 de la Commission du 15 décembre 2017 ; <br/>
              - le code de la santé publique ; <br/>
              - la loi n° 93-1419 du 31 décembre 1993 ; <br/>
              - le décret n° 2006-1436 du 24 novembre 2006 ;<br/>
              - le décret n° 2017-1082 du 24 mai 2017 ;<br/>
              - l'arrêté du 3 février 2017 portant nomination d'une personnalité indépendante en application du décret n° 2006-1436 du 24 novembre 2006 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Pierre Boussaroque, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Vincent Villette, rapporteur public ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 29 septembre 2020, présentée par l'association de défense de la concurrence dans le secteur de l'identification des produits de consommation courante.<br/>
<br/>
              Vu la note en délibéré, enregistrée le 29 septembre 2020, présentée par l'association pour une nouvelle politique anti-tabac ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              Sur le cadre juridique du litige :<br/>
<br/>
              1. D'une part, l'article 15 de la directive 2014/40/UE du Parlement européen et du Conseil du 3 avril 2014 relative au rapprochement des dispositions législatives, réglementaires et administratives des Etats membres en matière de fabrication, de présentation et de vente des produits du tabac et des produits connexes impose aux Etats membres de veiller à ce que chaque unité de conditionnement des produits du tabac porte un identifiant unique et renvoie à des actes d'exécution de la Commission, notamment, la détermination des normes techniques pour la mise en place et le fonctionnement du système d'identification et de traçabilité prévu à cet article, y compris le marquage à l'aide d'un identifiant unique. A ce titre, l'article 3 du règlement d'exécution (UE) 2018/574 de la Commission du 15 décembre 2017 relatif aux normes techniques pour la mise en place et le fonctionnement d'un système de traçabilité des produits du tabac prévoit que : " 1. Chaque État membre désigne une entité (" entité de délivrance des ID") chargée de la génération et de la délivrance des identifiants uniques (...). / 3. L'entité de délivrance des ID est indépendante et respecte les critères énoncés à l'article 35 (...) ". L'article L. 3512-23 du code de la santé publique, transposant cette directive, prévoit que : " I.- Les unités de conditionnement de produits du tabac fabriqués en France, importés d'un Etat non membre de l'Union européenne ou provenant d'un Etat membre de l'Union européenne, sont revêtus d'un identifiant unique, imprimé ou apposé de façon inamovible et indélébile. (...). II.- Les identifiants prévus au I sont délivrés par l'entité de délivrance des identifiants uniques répondant aux conditions de l'article 35 du règlement d'exécution (UE) 2018/574 de la Commission du 15 décembre 2017 précité. Lorsque l'Etat n'est pas l'entité de délivrance des identifiants uniques, le ministre chargé des douanes désigne une entité de délivrance des identifiants uniques dans les conditions prévues au 6° de l'article L. 3512-26 ", c'est-à-dire celles que doit fixer le décret en Conseil d'Etat auquel ces dispositions renvoient.<br/>
<br/>
              2. D'autre part, aux termes de l'article 2 de la loi du 31 décembre 1993 relative à l'Imprimerie nationale, cette société " est seule autorisée à réaliser les documents déclarés secrets ou dont l'exécution doit s'accompagner de mesures particulières de sécurité (...) ". L'article 1er du décret du 24 novembre 2006 pris pour l'application de ces dispositions prévoit que ces documents comprennent : " II. - (...) les documents administratifs dont l'exécution doit s'accompagner de mesures particulières de sécurité mentionnées au III et relevant des catégories suivantes : (...) 3° Cartes, titres ou permis attestant l'obtention par une personne d'une décision de l'Etat (...) lui ouvrant des droits ou lui accordant l'autorisation d'exercer certaines activités ; (...) /III. - Sont des mesures particulières de sécurité, en vue de l'application du II du présent article, la centralisation des opérations de réalisation des documents dans des locaux à accès contrôlé et protégés contre les intrusions ainsi que l'utilisation, dans la réalisation des documents, de procédés techniques destinés à empêcher les falsifications et les contrefaçons (...) /IV. - Des décrets pris sur le rapport du ministre compétent établissent, pour chaque ministère, la liste des documents mentionnés au II. Ces décrets sont pris et le cas échéant modifiés après avis d'une personnalité indépendante désignée par arrêté du Premier ministre. Cet avis est rendu public ".<br/>
<br/>
              3. Sur le fondement des dispositions mentionnées au point précédent, un décret du 8 mars 2019 a ajouté les identifiants uniques prévus par les dispositions mentionnées au point 1 à la liste des documents, mentionnés au II du décret du 24 novembre 2006, que l'Imprimerie nationale est seule autorisée à réaliser. Le décret du 13 mars 2019 relatif au dispositif de traçabilité des produits du tabac a inséré au code de la santé publique un article R. 3512-31 prévoyant que " I. L'entité de délivrance des identifiants uniques mentionnée au II de l'article L. 3512-23 est désignée par arrêté du ministre chargé des douanes ". Ce dernier a désigné l'Imprimerie nationale par un arrêté du 16 avril 2019.<br/>
<br/>
              4. Les associations requérantes demandent l'annulation pour excès de pouvoir de cet arrêté ainsi que, pour l'une, celle du décret du 8 mars 2019 et, pour l'autre, celle du décret du 13 mars 2019 et du refus du Premier ministre de le retirer. Il y a lieu de joindre leurs requêtes pour y statuer par une seule décision.<br/>
<br/>
              Sur les conclusions dirigées contre le décret du 8 mars 2019 :<br/>
<br/>
              En ce qui concerne la légalité externe :<br/>
<br/>
              5. En premier lieu, le décret attaqué a été pris sur le rapport du ministre de l'action et des comptes publics. Il résulte des dispositions du décret du 24 mai 2017 relatif aux attributions de ce ministre, alors applicable, que celui-ci était compétent en matière de douanes et droits indirects. Alors même que le décret attaqué ne poursuit pas seulement un objectif de lutte contre la contrefaçon des produits du tabac, mais également un objectif de santé publique, l'association de défense de la concurrence dans le secteur de l'identification des produits de consommation courante (ADCSIP) n'est pas fondée à soutenir que le décret attaqué n'aurait pas été pris sur le rapport du " ministre compétent " ainsi que l'impose sans autre précision le IV de l'article 1er du décret du 24 novembre 2006 cité au point 2.<br/>
<br/>
              6. En deuxième lieu, le décret attaqué a été pris après avis de M. A..., désigné le 3 février 2017 par arrêté du Premier ministre en qualité de personnalité indépendante en application du IV du décret du 24 novembre 2006, sans que la motivation succincte de cet avis permette d'en remettre en cause l'existence-même. D'une part, aucune des circonstances invoquées par la requérante, tenant à la carrière et aux fonctions de l'intéressé, agent de l'Etat actuellement médiateur des ministères économiques et financiers, n'est en tout état de cause de nature à faire douter de son indépendance, notamment vis-à-vis de la société Imprimerie nationale ou des concurrents de celle-ci, pas plus d'ailleurs que vis-à-vis de l'industrie du tabac ou du Premier ministre auquel est destiné l'avis prévu à l'article 1er du décret du 24 novembre 2006, pour rendre cet avis. D'autre part, si l'obligation faite au IV de l'article 1er du décret du 14 novembre 2006 de rendre son avis public impose que celui-ci soit diffusé et non pas seulement, contrairement à ce que soutient le ministre, qu'il soit communiqué sur demande, la circonstance que cette publicité n'aurait pas été assurée préalablement à l'intervention du décret attaqué n'est pas, dès lors qu'aucune disposition ne l'impose, de nature à entacher ce dernier d'illégalité. Il suit de là que l'ADCSIP n'est pas fondée à soutenir que le décret attaqué aurait été édicté en méconnaissance des dispositions du IV de l'article 1er du décret du 24 novembre 2006 relatives à l'avis d'une personnalité indépendante.<br/>
<br/>
              7. En troisième lieu, l'Imprimerie nationale ne retirant du monopole de réalisation des identifiants uniques dont elle est investie par le décret attaqué aucun avantage susceptible d'être qualifié d'aide d'Etat au sens de ce traité, l'ADCSIP n'est pas fondée à soutenir que le décret attaqué aurait dû être notifié à la Commission en application de l'article 108 du traité sur le fonctionnement de l'Union européenne.<br/>
<br/>
              En ce qui concerne la légalité interne :<br/>
<br/>
              8. D'une part, ainsi que le rappelle le I de L. 3512-23 du code de la santé publique, l'identifiant unique apposé sur chaque unité de conditionnement par les fabricants et les importateurs doit être conforme aux dispositions du règlement d'exécution (UE) 2018/574 de la Commission du 15 décembre 2017, auxquelles ces dispositions renvoient. L'article 5 de ce règlement d'exécution prévoit notamment que les fabricants et importateurs doivent, lorsqu'ils adressent une demande à l'entité compétente pour leur délivrer un identifiant unique, fournir les informations énumérées par ce règlement, parmi lesquelles divers numéros d'identification délivrés par l'administration attestant de leur qualité pour présenter une telle demande et de la régularité de leur situation au regard de la réglementation de leur activité. L'article L. 3515-4 du code de la santé publique punit en outre de 45 000 euros d'amende : " 3° Le fait d'importer, en vue de les vendre, distribuer ou offrir à titre gratuit, des produits du tabac fabriqués dans un Etat non membre de l'Union européenne ou provenant d'un Etat membre de l'Union européenne dont l'unité de conditionnement n'est pas revêtue de l'identifiant unique (...) ". D'autre part, il résulte des mêmes dispositions ainsi que de celles qui sont mentionnées au point 1 que la génération et la délivrance de cet identifiant doivent s'accompagner de mesures particulières de sécurité, tant par l'utilisation de procédés techniques destinés à en prévenir la falsification que par la centralisation des opérations dans des locaux à accès contrôlé protégés contre les intrusions et par le stockage sécurisé des données que le règlement d'exécution impose. Il suit de là que l'ADCSIP n'est pas fondée à soutenir que l'identifiant unique ne relèverait pas de la catégorie, définie au 3° du II de l'article 1er du décret du 24 novembre 2006, des titres ouvrant des droits ou autorisant à exercer certaines activités dont l'exécution doit s'accompagner de mesures particulières de sécurité, dont la réalisation peut être confiée à la seule Imprimerie nationale.<br/>
<br/>
              9. Il résulte de tout ce qui précède que l'ADCSIP n'est pas fondée à demander l'annulation du décret du 8 mars 2019, sans qu'il soit besoin de se prononcer sur les fins de non-recevoir soulevées par le ministre de l'action et des comptes publics.<br/>
<br/>
              Sur les conclusions dirigées contre le décret du 13 mars 2019 et contre le refus de le retirer :<br/>
<br/>
              10. Aux termes de l'article R. 421-1 du code de justice administrative : " La juridiction ne peut être saisie que par voie de recours formé contre une décision, et ce, dans les deux mois à partir de la notification ou de la publication de la décision attaquée ".<br/>
<br/>
              11. Il ressort des pièces du dossier que le décret du 13 mars 2019, dont l'association pour une nouvelle politique anti-tabac (PUNPAT) demande l'annulation pour excès de pouvoir, a été publié au Journal officiel le 15 mars 2019. Si cette association a adressé au Premier ministre le 10 juin 2019 une demande tendant à ce qu'il rapporte ce décret, cette demande, présentée après l'expiration du délai du recours contentieux, n'a pu prolonger ce délai. Ainsi, ses conclusions tendant à l'annulation de ce décret, enregistrées au secrétariat du contentieux du Conseil d'Etat le 20 juin 2019 ont été présentées tardivement et doivent, par suite, être rejetées comme irrecevables. En outre, le Premier ministre étant tenu de rejeter la demande tendant au retrait de dispositions réglementaires devenues définitives, les conclusions de la requérante tendant à l'annulation de ce refus ne peuvent qu'être également rejetées.<br/>
<br/>
              Sur les conclusions dirigées contre l'arrêté du 16 avril 2019 :<br/>
<br/>
              12. En premier lieu, le moyen tiré de ce que les dispositions de l'article L. 3512-23 du code de la santé publique, citées au point 1, fondant la compétence du ministre chargé des douanes pour désigner l'entité de délivrance des identifiants uniques, seraient incompatibles avec la directive 2014/40/UE du Parlement européen et du Conseil n'est pas assorti des précisions permettant d'en apprécier le bien-fondé.<br/>
<br/>
              13. En deuxième lieu, si l'ADCSIP soutient que les moyens qu'elle soulève à l'appui de ses conclusions dirigées contre le décret du 8 mars 2019 doivent également conduire à l'annulation de l'arrêté du 16 avril 2019, aucun de ces moyens n'a d'incidence sur la légalité de cet arrêté.<br/>
<br/>
              14. En dernier lieu, l'article 8 du protocole à la convention-cadre de l'Organisation mondiale de la santé pour éliminer le commerce illicite des produits du tabac adopté le 12 novembre 2012 stipule que : " Suivi et traçabilité (...) 2. Chaque partie instaure, conformément au présent article, un système de suivi et de traçabilité contrôlé par elle de tous les produits du tabac qui sont fabriqués ou importés sur son territoire en tenant compte de ses propres besoins nationaux ou régionaux spécifiques et en se fondant sur les meilleures pratiques existantes. / (...) 12. Les obligations auxquelles une Partie est tenue ne sont pas remplies par l'industrie du tabac et ne lui sont pas déléguées./ 13. Chaque Partie fait en sorte que ses autorités compétentes, dans le cadre de leur participation au régime de suivi et de traçabilité, n'aient de relations avec l'industrie du tabac et ceux qui représentent les intérêts de l'industrie du tabac que dans la mesure strictement nécessaire pour mettre en oeuvre le présent article ".  Ces stipulations, qui requièrent clairement l'intervention d'actes complémentaires pour produire des effets à l'égard des particuliers, ne peuvent utilement être invoquées à l'encontre de l'arrêté attaqué. <br/>
<br/>
              15. Il résulte de tout ce qui précède que les associations requérantes ne sont pas fondées à demander l'annulation de l'arrêté du 16 avril 2019, sans qu'il soit besoin de se prononcer sur les fins de non-recevoir soulevées par le ministre de l'action et des comptes publics.<br/>
<br/>
              Sur les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              16. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que les sommes demandées par les associations requérantes soient mises à la charge de l'Etat, qui n'est pas la partie perdante dans la présente instance. Il n'y a pas lieu, dans les circonstances de l'espèce, de mettre à la charge de l'association pour une nouvelle politique anti-tabac la somme que le ministre de l'action et des comptes publics demande au même titre.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Les requêtes de l'association pour la défense de la concurrence dans le domaine de l'identification des produits de consommation courante et de l'association pour une nouvelle politique anti-tabac sont rejetées.<br/>
Article 2 : Les conclusions présentées par le ministre de l'action et des comptes publics au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La présente décision sera notifiée à l'association pour la défense de la concurrence dans le secteur de l'identification des produits de consommation courante, à l'association pour une nouvelle politique anti-tabac et au ministre de l'économie, des finances et de la relance.<br/>
Copie en sera adressée au Premier ministre et au ministre des solidarités et de la santé.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
