<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036927152</ID>
<ANCIEN_ID>JG_L_2018_05_000000411835</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/92/71/CETATEXT000036927152.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  5ème chambre jugeant seule, 18/05/2018, 411835, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-05-18</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>411835</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 5ème chambre jugeant seule</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DIDIER, PINET ; SCP BARADUC, DUHAMEL, RAMEIX ; SCP SEVAUX, MATHONNET</AVOCATS>
<RAPPORTEUR>M. Olivier Rousselle</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Laurence Marion</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:411835.20180518</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Mme B...née A...a demandé au tribunal administratif de Montreuil de condamner l'Assistance publique-Hôpitaux de Paris (AP-HP) à l'indemniser du préjudice subi lors d'une intervention chirurgicale pratiquée le 23 juin 2009 à l'hôpital Avicenne. Par un jugement n° 1200465 du 18 juillet 2013, le tribunal administratif a condamné l'AP-HP à lui verser la somme de 62 500 euros avec intérêts au taux légal à compter du 22 mars 2011 et capitalisation des intérêts échus à compter du 22 mars 2012, ainsi qu'à chaque échéance annuelle à compter de cette date.<br/>
<br/>
              Par un arrêt n° 13VE02964 du 25 avril 2017 la cour administrative d'appel de Versailles a, sur appel de l'AP-HP, réformé ce jugement et ramené la somme que l'AP-HP a été condamnée à verser à Mme B...à 9 100 euros avec intérêts au taux légal à compter du 22 mars 2011 et capitalisation des intérêts échus à compter du 22 mars 2012, ainsi qu'à chaque échéance annuelle à compter de cette date.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés au secrétariat du contentieux du Conseil d'Etat le 26 juin et le 20 septembre 2017, Mme B... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de l'AP-HP et de faire droit à l'intégralité des conclusions de sa demande de première instance ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat une somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
<br/>
- le code de la santé publique ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Olivier Rousselle, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Laurence Marion, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Baraduc, Duhamel, Rameix, avocat de MmeB..., à la SCP Didier, Pinet, avocat de l'Assistance publique-Hôpitaux de Paris et à la SCP Sevaux, Mathonnet, avocat de l'Office national d'indemnisation des accidents médicaux, des affections iatrogènes et des infections nosocomiales ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que MmeB..., qui présente une maladie de Crohn associée à une spondylarthrite ankylosante, s'est vu diagnostiquer, à la suite d'examens réalisés en février 2009, une sténose de l'anastomose iléo-colique ; qu'en vue de prévenir un risque d'occlusion, une dilatation endoscopique de cette sténose a été réalisée à l'hôpital universitaire Avicenne de Bobigny, le 23 juin 2009 ; qu'à la suite de cette intervention, qui a occasionné une perforation sur l'angle colique gauche nécessitant une résection iléo-colique et une anastomose iléo-sigmoïdienne latéro-terminale, Mme B...s'est plainte d'une aggravation importante des signes digestifs associés à sa maladie de Crohn ; qu'elle a recherché devant la juridiction administrative la responsabilité de l'Assistance publique-Hôpitaux de Paris (AP-HP) ; qu'en première instance, le tribunal administratif de Montreuil, par un jugement du 18 juillet 2013, a retenu la responsabilité de l'AP-HP sur le fondement d'une erreur d'indication et alloué à la requérante une indemnité de 62 500 euros majorée des intérêts et des intérêts des intérêts en réparation des préjudices subis ; que, sur appel de l'AP-HP, la cour administrative d'appel de Paris a confirmé la responsabilité de l'AP-HP, ramené sa condamnation à 9 100 euros avec intérêts et capitalisation des intérêts et mis à la charge de Mme B... les frais d'expertise taxés à 6 424 euros ; que Mme B...se pourvoit en cassation contre cet arrêt en tant qu'il a écarté toute indemnisation des préjudices permanents et mis à sa charge les dépens de première instance et d'appel ; que l'AP-HP forme un pourvoi incident contre le même arrêt ;<br/>
<br/>
              Sur le pourvoi incident de l'Assistance publique-Hôpitaux de Paris :<br/>
<br/>
              Sur l'erreur d'indication :<br/>
<br/>
              2. Considérant qu'aux termes du premier alinéa du I de l'article L.1142-1 du code de la santé publique : " Hors le cas où leur responsabilité est encourue en raison d'un défaut d'un produit de santé, les professionnels de santé mentionnés à la quatrième partie du présent code, ainsi que tout établissement, service ou organisme dans lesquels sont réalisés des actes individuels de prévention, de diagnostic ou de soins ne sont responsables des conséquences dommageables d'actes de prévention, de diagnostic ou de soins qu'en cas de faute " ;<br/>
<br/>
              3. Considérant qu'il résulte des termes de l'arrêt attaqué que, pour estimer que la décision de procéder à une dilatation endoscopique de la sténose de la requérante était constitutive d'une faute engageant la responsabilité du service public sur le fondement des dispositions précitées, la cour administrative d'appel n'a pas omis de prendre en compte l'intérêt, mis en avant par l'AP-HP dans ses écritures, que présentait cette opération afin de permettre la réalisation d'une biopsie du colon, en vue du diagnostic d'éventuelles dysplasies ou d'un cancer, dont la probabilité est plus élevée chez les patients atteints de la maladie de Crohn ; que son arrêt n'est entaché sur ce point ni d'insuffisance de motivation ni d'erreur de droit ; que la cour s'est seulement fondée, pour juger que l'indication de dilatation endoscopique était erronée dans le cas de MmeB..., sur le risque élevé de perforation et de complications qu'elle comportait, et non sur le caractère inapproprié du choix de recourir à cette intervention à des fins diagnostiques ; le moyen tiré de ce que la cour aurait porté sur ce choix une appréciation entachée de dénaturation doit, par suite, être écarté ; <br/>
<br/>
              Sur le défaut d'information :<br/>
<br/>
              4. Considérant qu'en jugeant que l'AP-HP avait reconnu dans ses écritures de première instance que Mme B...n'avait pas été informée du fait que son intervention, d'une part, avait notamment pour objet de vérifier l'absence de dysplasie, et, d'autre part, pouvait, par la réduction de la surface intestinale, accélérer le transit et accroître les diarrhées dont elle souffrait, la cour administrative d'appel a dénaturé  les pièces du dossier ; que son arrêt doit être annulé sur ce point ;<br/>
<br/>
              Sur le pourvoi de MmeB... : <br/>
<br/>
              Sur l'indemnisation des préjudices permanents :<br/>
<br/>
              5. Considérant que pour refuser à la requérante toute indemnité à raison des préjudices permanents qu'elle invoquait, la cour administrative d'appel a jugé qu'il résultait de l'expertise que, compte tenu de l'état avancé de la maladie de Crohn dont était atteinte Mme B..., sa sténose colique, en l'absence de l'intervention litigieuse, aurait à court terme évolué vers une occlusion qui aurait nécessité une résection et augmenté les diarrhées en réduisant sa surface intestinale, de sorte que le lien de causalité entre les préjudices permanents allégués et la faute commise par l'AP-HP n'était pas établi ; qu'il ressort toutefois du rapport d'expertise du Docteur Biclet, commis par la cour administrative d'appel, que " Mme B...aurait vraisemblablement eu besoin d'une résection intestinale mais celle-ci a été anticipée de plusieurs années " ; qu'aucune des pièces du dossier soumis aux juges du fond ne permet de corroborer l'affirmation de l'arrêt attaqué citée ci-dessus selon laquelle la sténose colique aurait évolué à court terme vers une occlusion nécessitant une résection chirurgicale ; qu'il suit de là que la requérante est fondée à soutenir qu'en statuant ainsi, les juges du fond ont dénaturé les pièces du dossier ; que l'arrêt attaqué doit, pour ce motif, être annulé en tant qu'il a statué sur l'indemnisation des préjudices permanents ;<br/>
<br/>
              Sur les dépens :<br/>
<br/>
              6. Considérant, d'autre part, qu'aux termes de l'article R.761-1 du code de justice administrative : " Les dépens comprennent les frais d'expertise, d'enquête et de toute autre mesure d'instruction dont les frais ne sont pas à la charge de l'Etat. / Sous réserve de dispositions particulières, ils sont mis à la charge de toute partie perdante sauf si les circonstances particulières de l'affaire justifient qu'ils soient mis à la charge d'une autre partie ou partagés entre les parties " ; que la requérante n'ayant pas la qualité de partie perdante devant la cour, l'arrêt attaqué ne pouvait sans méconnaître ces dispositions mettre à sa charge les dépens de première instance et d'appel sans faire apparaître les circonstances particulières qui l'auraient justifié ; que Mme B...est par suite également fondée à demander l'annulation de l'arrêt qu'elle attaque sur ce point ;<br/>
<br/>
              7. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'AP-HP une somme de 3 000 euros à verser à Mme B...au titre des dispositions de l'article L.761-1 du code de justice administrative ; que ces dispositions font en revanche obstacle à ce qu'une somme soit mise, à ce titre, à la charge de Mme B...qui n'est pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Versailles du 25 avril 2017 est annulé en tant qu'il a rejeté les conclusions de Mme B...tendant à l'indemnisation de ses préjudices et mis à sa charge les dépens de première instance et d'appel.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Versailles dans la mesure de l'annulation prononcée à l'article 1er.<br/>
Article 3 : L'Assistance publique-Hôpitaux de Paris versera à Mme B...la somme de 3 000 euros au titre des dispositions de l'article L.761-1 du code de justice administrative.<br/>
Article 4 : Le pourvoi incident de l'Assistance publique-Hôpitaux de Paris est rejeté.<br/>
<br/>
Article 5 : La présente décision sera notifiée à MmeB..., à l'Assistance Publique-Hôpitaux de Paris, à la caisse primaire d'assurance-maladie du Val-de-Marne et à la ministre des solidarités et de la santé.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
