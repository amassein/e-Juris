<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030983345</ID>
<ANCIEN_ID>JG_L_2015_07_000000370878</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/98/33/CETATEXT000030983345.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème SSJS, 31/07/2015, 370878, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-07-31</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>370878</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème SSJS</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROCHETEAU, UZAN-SARANO ; SCP GARREAU, BAUER-VIOLAS, FESCHOTTE-DESBOIS</AVOCATS>
<RAPPORTEUR>Mme Dominique Chelle </RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2015:370878.20150731</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              	Vu la procédure suivante :<br/>
<br/>
              	Par une décision du 24 juin 2014, le Conseil d'Etat, statuant au contentieux, a prononcé l'admission des conclusions du pourvoi de M. D...B...dirigées contre l'arrêt n° 10BX02611 du 14 février 2013 de la cour administrative d'appel de Bordeaux en tant qu'il fixe le montant de l'indemnité mise à la charge du département de l'Aveyron en application des dispositions de l'article L. 121-11 du code rural et de la pêche maritime. <br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code rural et de la pêche maritime ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Dominique Chelle, Conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Rocheteau, Uzan-Sarano, avocat de M. B...et à la SCP Garreau, Bauer-Violas, Feschotte-Desbois, avocat du département de l'Aveyron ;<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond qu'à l'occasion de la construction de l'autoroute A75 une opération de remembrement a été menée sur le territoire de la commune de Séverac-le-Château (Aveyron) ; que, par une décision du 25 avril 1995, la commission départementale d'aménagement foncier de l'Aveyron a attribué à Mme C...B..., en compensation des parcelles ZP1 et ZK16, les parcelles VL5 et VH3 ; que, par un jugement du 24 janvier 2002 devenu définitif, le tribunal administratif de Toulouse a annulé cette décision en tant qu'elle rejetait la réclamation de MmeB... ; que, par une donation-partage du 30 juin 1999 M. D... B...est devenu propriétaire de ces parcelles ; que, le 21 novembre 2006, la commission départementale d'aménagement foncier, estimant qu'il n'était pas possible de compenser le défaut d'équivalence entre les apports et les attributions du compte de Mme B...par une modification des attributions, a accordé à M. B... une indemnité d'un montant de 23 939 euros ; que, par un jugement du 2 juillet 2010, le tribunal administratif de Toulouse a rejeté la demande de l'intéressé tendant à l'annulation de cette décision ; que, saisie d'un appel de M.B..., la cour administrative d'appel de Bordeaux a, par un arrêt du 14 février 2013, annulé ce jugement et, statuant par la voie de l'évocation, mis à la charge du département de l'Aveyron la somme de 41 208 euros ; que par décision du 24 juin 2014, le Conseil d'Etat, statuant au contentieux, a prononcé l'admission des conclusions du pourvoi formé par M. B...contre cet arrêt en tant qu'il fixe à 41 208 euros le montant de l'indemnité mise à la charge du département de l'Aveyron en application des dispositions de l'article L. 121-11 du code rural et de la pêche maritime ; <br/>
<br/>
              2. Considérant qu'aux termes de l'article L. 121-11 code rural et de la pêche maritime : " Lorsque la commission départementale, saisie à nouveau à la suite d'une annulation par le juge administratif, constate que la modification du parcellaire nécessaire pour assurer par des attributions en nature le rétablissement dans leurs droits des propriétaires intéressés aurait des conséquences excessives sur la situation d'autres exploitations, elle peut, par décision motivée, prévoir que ce rétablissement sera assuré par une indemnité à la charge du département, dont elle détermine le montant " ;<br/>
<br/>
              3. Considérant que, pour fixer à 41 208 euros le montant de l'indemnité due à M. B...par le département de l'Aveyron en application des dispositions précitées, la cour a tenu compte, d'une part, de la sous-évaluation de la parcelle ZP1 apportée au remembrement par MmeB..., dont elle a estimé qu'elle équivalait à une surface de 3 ha 46 a, et, d'autre part, de la surévaluation de la parcelle VL5 attribuée à l'intéressée, qu'elle a regardée comme équivalant à une surface de 2 ha 38 a compte non tenu des dégradations subies par cette parcelle postérieurement à son évaluation par la commission communale, du fait des travaux de réalisation de l'autoroute ; que la cour, qui a précisé qu'elle se fondait notamment sur le rapport établi par M. A...et qui n'était pas tenue de répondre à tous les arguments de M.B..., a suffisamment motivé son arrêt sur ce point ; qu'en estimant que la surévaluation de la parcelle VL5 dans le cadre des seules opérations de remembrement, compte non tenu de la perte de valeur de cette parcelle intervenue postérieurement du fait des travaux publics, équivalait à 2 ha 38 a, elle a porté sur les pièces du dossier qui lui était soumis une appréciation souveraine, exempte de dénaturation ; que M. B...n'est, par suite, pas fondé à demander l'annulation de l'arrêt attaqué ;<br/>
<br/>
              4. Considérant que les conclusions de M. B...tendant à ce qu'une somme soit mise à la charge de l'Etat, qui n'est pas la partie perdante dans la présente instance, au titre de l'article L. 761-1 du code de justice administrative, ne peuvent qu'être rejetées ; qu'il n'y a pas lieu, dans les circonstances de l'espèce, de mettre à ce titre une somme à la charge de M. B... ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>			D E C I D E :<br/>
              			--------------<br/>
<br/>
Article 1er : Le pourvoi de M. B...est rejeté. <br/>
<br/>
Article 2 : Les conclusions présentées par le département de l'Aveyron au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 3 : La présente décision sera notifiée à M. D...B..., au ministre de l'agriculture, de l'agroalimentaire et de la forêt et au département de l'Aveyron. <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
