<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029786318</ID>
<ANCIEN_ID>JG_L_2014_11_000000384089</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/78/63/CETATEXT000029786318.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  7ème sous-section jugeant seule, 21/11/2014, 384089, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-11-21</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>384089</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 7ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP JEAN-PHILIPPE CASTON ; SCP GADIOU, CHEVALLIER</AVOCATS>
<RAPPORTEUR>M. Vincent Montrieux</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Bertrand Dacosta</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2014:384089.20141121</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 1er et 15 septembre 2014 au secrétariat du contentieux du Conseil d'Etat, présentés pour la commune de Versailles, représentée par son maire ; la commune de Versailles demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'ordonnance n° 1405354 du 18 août 2014 par laquelle le juge des référés du tribunal administratif de Versailles, statuant en application de l'article L. 551-1 du code de justice administrative, a, à la demande de la société JFM Conseils, annulé la procédure de passation du marché de géomètre, géomètre expert, prestations de détection et récolement de réseaux de manière non intrusive pour le lot n° 3 " détection générale de réseaux ville existants et détections et récolements de réseaux dans le cadre d'investigations complémentaires par les moyens non intrusifs " au stade de l'analyse des offres et, en second lieu, enjoint à la commune de Versailles, si elle entend poursuivre la passation du contrat, de reprendre la procédure au stade de l'analyse des offres en y intégrant l'offre de la société JFM Conseils ; <br/>
<br/>
              2°) statuant en référé, de rejeter la requête de la société JFM Conseils ; <br/>
<br/>
              3°) de mettre à la charge de la société JFM Conseils le versement de la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code des marchés publics ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Vincent Montrieux, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Bertrand Dacosta, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Gadiou, Chevallier, avocat de la commune de Versailles, et à la SCP Jean-Philippe Caston, avocat de la société JFM Conseils ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article L. 551-1 du code de justice administrative dans sa version applicable à l'espèce : " Le président du tribunal administratif, ou le magistrat qu'il délègue, peut être saisi en cas de manquement aux obligations de publicité et de mise en concurrence auxquelles est soumise la passation des marchés publics (...). / Les personnes habilitées à agir sont celles qui ont un intérêt à conclure le contrat et qui sont susceptibles d'être lésées par ce manquement (...). / Le président du tribunal administratif peut être saisi avant la conclusion du contrat. Il peut ordonner à l'auteur du manquement de se conformer à ses obligations et suspendre la passation du contrat ou l'exécution de toute décision qui s'y rapporte. Il peut également annuler ces décisions et supprimer les clauses ou prescriptions destinées à figurer dans le contrat et qui méconnaissent lesdites obligations. Dès qu'il est saisi, il peut enjoindre de différer la signature du contrat jusqu'au terme de la procédure et pour une durée maximum de vingt jours. (...) " ;<br/>
<br/>
              2. Considérant que pour annuler, au stade de l'analyse des offres, la procédure de passation du lot n° 3 du marché des prestations de géomètre expert, de détection et recollement de réseaux manière non intrusive engagée le 22 janvier 2014 par le groupement de commandes constitué par la ville de Versailles, le centre communal d'action sociale de la commune de Versailles et la communauté d'agglomération de Versailles Grand Parc, le juge des référés du tribunal administratif de Versailles, statuant en application de l'article L. 551-1 du code de justice administrative, a estimé que la circonstance que le pouvoir adjudicateur avait analysé l'offre de la société JFM Conseils et lui avait posé des questions précises sur son bordereau des prix unitaires (BPU) et détail quantitatif estimatif (DQE) devait être regardée, dans les circonstances de l'espèce, comme constituant un " commencement de preuve " que la société JFM Conseils avait remis une offre complète ; qu'il ressort toutefois des pièces du dossier soumis au juge des référés que le rejet de l'offre de la société JFM Conseils reposait sur l'absence, dans l'offre présentée, de l'annexe 2 de la charte graphique signée et complétée, des originaux des BPU et DQE et du caractère " quasiment illisible " du BPU ; qu'au demeurant les échanges entre le pouvoir adjudicateur et la société JFM Conseils permettaient seulement de supposer que le pouvoir adjudicateur disposait d'une partie de l'offre déposée par ce candidat, mais en aucun cas que cette dernière était conforme aux prescriptions du règlement de la consultation et, notamment, qu'elle comportait les signatures dans les formes requises ; qu'ainsi la commune de Versailles est fondée à soutenir qu'en statuant ainsi le juge des référés du tribunal administratif de Versailles a dénaturé les pièces du dossier ; que, par suite, son ordonnance du 18 août 2014 doit être annulée, sans qu'il soit besoin d'examiner l'autre moyen du pourvoi ;<br/>
<br/>
              3. Considérant qu'il y a lieu, en application de l'article L. 821-2 du code de justice administrative, de régler l'affaire au titre de la procédure de référé engagée par la société JFM Conseils ;<br/>
<br/>
              4. Considérant qu'aux termes du I de l'article 35 du code des marchés publics : " 1° (...) Une offre irrégulière est une offre qui, tout en apportant une réponse au besoin du pouvoir adjudicateur, est incomplète ou ne respecte pas les exigences formulées dans l'avis d'appel public à la concurrence ou dans les documents de la consultation (...) " ; qu'aux termes du III de l'article 53 du même code : " Les offres inappropriées, irrégulières et inacceptables sont éliminées. (...) " ; qu'aux termes du I de l'article 59 du même code : " Il ne peut y avoir négociation avec les candidats. Il est seulement possible de demander aux candidats de préciser ou de compléter la teneur de leur offre " ;  <br/>
<br/>
              5. Considérant, en premier lieu, qu'il résulte de l'instruction que le règlement de la consultation du marché litigieux imposait aux candidats d'inclure dans leur offre un bordereau des prix unitaires et un détail quantitatif estimatif daté et signé, ainsi que l'annexe 2 de la charte graphique également datée et signée ; qu'il résulte de l'instruction que l'offre de la société JFM Conseils, ainsi qu'il a été dit ci-dessus, a été écartée au motif qu'elle ne comportait pas les originaux des pièces exigées, ni l'annexe 2 de la charte graphique signée et complétée et que le bordereau des prix unitaires était quasiment illisible ; que la société JFM Conseils, qui se borne à soutenir dans ses écritures que le pouvoir adjudicateur disposait nécessairement des pièces litigieuses dès lors qu'il a procédé à l'examen de son offre et lui a demandé de la préciser, n'apporte aucun autre élément de nature à justifier que l'offre qu'elle a déposée était complète et conforme aux prescriptions, y compris formelles, du règlement de la consultation ; <br/>
<br/>
              6. Considérant, en deuxième lieu, que les dispositions précitées  interdisent au pouvoir adjudicateur de modifier ou de rectifier lui-même une offre incomplète, comme telle irrégulière ; que la circonstance que le candidat ait été invité à préciser ou compléter son offre par le pouvoir adjudicateur, sans qu'il puisse alors en modifier la teneur, n'est pas de nature à régulariser une offre qui serait incomplète et que le pouvoir adjudicateur était, dès lors, tenue d'écarter ; <br/>
<br/>
              7. Considérant qu'il résulte de ce qui précède que la société JFM Conseils n'est pas fondée à soutenir que c'est à tort que son offre a été rejetée au motif qu'elle était incomplète et donc irrégulière ; que la lettre l'informant du rejet de son offre était, par ailleurs et en tout état de cause, suffisamment motivée ;<br/>
<br/>
              8. Considérant qu'il résulte de tout ce qui précède que la demande présentée  par la société JFM Conseils devant le juge des référés du tribunal administratif de Versailles doit être rejetée, ainsi que ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative ; qu'il y a lieu en revanche, dans les circonstances de l'espèce, de mettre, au même titre, à la charge de cette société le versement à la commune de Versailles d'une somme de 4 500 euros pour l'ensemble de la procédure ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'ordonnance du 18 août 2014 du juge des référés du tribunal administratif de Versailles est annulée.<br/>
Article 2 : La demande présentée par la société JFM Conseils devant le juge des référés du tribunal administratif de Versailles ainsi que ses conclusions tendant à l'application des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La société JFM Conseils versera une somme de 4 500 euros à la commune de Versailles au titre de l'article L. 761-1 du code de justice administrative.  <br/>
Article 4 : La présente décision sera notifiée à la commune de Versailles, à la société JFM Conseils et à la société Détect-Réseaux 92. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
