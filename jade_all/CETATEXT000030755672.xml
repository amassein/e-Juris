<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030755672</ID>
<ANCIEN_ID>JG_L_2015_06_000000370914</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/75/56/CETATEXT000030755672.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème / 4ème SSR, 19/06/2015, 370914</TITRE>
<DATE_DEC>2015-06-19</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>370914</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème / 4ème SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LEVIS ; HAAS ; SCP DIDIER, PINET</AVOCATS>
<RAPPORTEUR>M. Jean-Dominique Langlais</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:370914.20150619</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Le département de La Réunion a demandé au tribunal administratif de Saint-Denis, d'une part, d'annuler l'ordonnance du 13 mai 2008 du président de ce tribunal taxant et liquidant à la somme globale de 520 213,24 euros les frais et honoraires de l'expertise confiée à M.E..., assisté de cinq sapiteurs, pour examiner les désordres affectant le système de ventilation du bâtiment des archives départementales de La Réunion et, d'autre part, de fixer les rémunérations dues à une somme globale n'excédant pas 213 974,04 euros. Par un jugement n° 0801112 du 10 juillet 2012, le tribunal administratif a rejeté cette demande. <br/>
<br/>
              Par un arrêt n° 12BX02023 du 27 juin 2013, la cour administrative d'appel de Bordeaux, statuant sur l'appel du département de La Réunion, a réformé ce jugement en retenant que les travaux de MmeG..., sapiteur adjoint à M.E..., ne pouvaient être regardés comme ayant été utiles à la conduite de l'expertise, en déduisant en conséquence le montant des frais et honoraires de l'intéressée, évalués à 69 887,09 euros, de la somme mise à la charge du département de La Réunion et en mettant à la charge de M. E...le versement à ce département d'une somme de 1 500 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
              1°, sous le n° 370914, par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 6 août et 7 novembre 2013 au secrétariat du contentieux du Conseil d'Etat, M. E... demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) de mettre à la charge du département de La Réunion la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              2°, sous le n° 370916, par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 6 août et 7 novembre 2013 au secrétariat du contentieux du Conseil d'Etat, Mme G...demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler le même arrêt ;<br/>
<br/>
              2°) de mettre à la charge du département de La Réunion la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
<br/>
	Vu les autres pièces des dossiers ;<br/>
	Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Dominique Langlais, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Haas, avocat de M.E..., à la SCP Lévis, avocat du département de La Réunion, à la SCP Didier, Pinet, avocat de MmeG... ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, par une ordonnance du 5 septembre 2003, le président du tribunal administratif de Saint-Denis a désigné M. E...comme expert avec mission de se prononcer sur les désordres affectant le bâtiment des archives départementales de La Réunion ; que, par quatre ordonnances des 17 octobre 2003, 17 mars, 5 juillet et 4 août 2004, il l'a autorisé à s'adjoindre comme sapiteurs M.F..., M.H..., M. A...B..., Mme K...et Mme G..., cette dernière étant " chargée de la coordination des experts " ; que, par une ordonnance du 13 mai 2008, il a taxé les frais et honoraires de l'expertise aux sommes de 153 993 euros pour M. E..., 115 006 euros pour M.F..., 91 302,75 euros pour M. H..., 79 906,88 euros pour M. A...B..., 69 887,09 euros pour Mme G...et 4 111,58 euros pour MmeK..., soit une somme globale de 520 213,30 euros qu'il a mise à la charge du département de La Réunion ; que, par un jugement du 10 juillet 2012, le tribunal administratif de Saint-Denis a rejeté les conclusions du département de La Réunion tendant à la réduction des honoraires fixés par cette ordonnance ; que, par un arrêt du 27 juin 2013, la cour administrative d'appel de Bordeaux, faisant partiellement droit à l'appel du département, a annulé les articles 9 et 10 de l'ordonnance du 13 mai 2008 relatifs aux frais et honoraires de MmeG..., et réduit en conséquence la somme globale mise à la charge du département ; que la cour a, par ailleurs, mis à la charge de M. E...la somme de 1 500 euros à verser au département de La Réunion au titre de l'article L. 761-1 du code de justice administrative ; que M. E...et Mme G...présentent contre cet arrêt des pourvois en cassation qu'il y a lieu de joindre pour statuer par une seule décision ; que le département de La Réunion a présenté dans les deux instances des conclusions incidentes par lesquelles il demande l'annulation de l'arrêt du 27 juin 2013 en tant qu'il rejette le surplus de ses conclusions d'appel et confirme les montants mis à sa charge par l'ordonnance du 13 mai 2008 au titre des frais et honoraires de MM.E..., F..., H...et A...B... ;  <br/>
<br/>
              Sur le pourvoi de M.E... :<br/>
<br/>
              2. Considérant que l'arrêt du 27 juin 2013, qui confirme le montant des frais et honoraires taxés par l'ordonnance du 13 mai 2008 au profit de M. E...et rejette en conséquence l'appel du département en tant qu'il contestait ce montant, ne fait grief à M. E... qu'en ce que, par son article 3, il met à sa charge la somme de 1 500 euros au titre de l'article L. 761-1 du code de justice administrative ; qu'ainsi, le département de La Réunion est fondé à soutenir que M. E...ne justifie que dans cette mesure d'un intérêt à l'annulation de cet arrêt ;  <br/>
<br/>
              3. Considérant qu'aux termes de l'article L. 761-1 du code de justice administrative : " Dans toutes les instances, le juge condamne la partie tenue aux dépens ou, à défaut, la partie perdante, à payer à l'autre partie la somme qu'il détermine, au titre des frais exposés et non compris dans les dépens... " ; qu'en jugeant que M. E...avait, dans les circonstances de l'espèce, la qualité de partie perdante, alors qu'elle rejetait intégralement les conclusions du département de La Réunion dirigées contre l'ordonnance du 13 mai 2008 en tant qu'elle taxe les frais et honoraires dus à M.E..., la cour a inexactement qualifié les faits de l'espèce ; qu'il y a lieu, par suite, d'annuler l'article 3 de son arrêt ;  <br/>
<br/>
              Sur le pourvoi de MmeG... : <br/>
<br/>
              4. Considérant qu'aux termes de l'article R. 621-2 du code de justice administrative : " Il n'est commis qu'un seul expert à moins que la juridiction n'estime nécessaire d'en désigner plusieurs. Le président du tribunal administratif ou de la cour administrative d'appel, selon le cas, (...) choisit les experts parmi les personnes figurant sur l'un des tableaux établis en application de l'article R. 221-9. (...) / Lorsqu'il apparaît à un expert qu'il est nécessaire de faire appel au concours d'un ou plusieurs sapiteurs pour l'éclairer sur un point particulier, il doit préalablement solliciter l'autorisation du président du tribunal administratif ou de la cour administrative d'appel (...). La décision est insusceptible de recours " ; qu'aux termes de l'article R. 621-11 du même code : " Les experts et sapiteurs mentionnés à l'article R. 621-2 ont droit à des honoraires, sans préjudice du remboursement des frais et débours. / Chacun d'eux joint au rapport un état de ses vacations, frais et débours. / Dans les honoraires sont comprises toutes sommes allouées pour étude du dossier, frais de mise au net du rapport, dépôt du rapport et, d'une manière générale, tout travail personnellement fourni par l'expert ou le sapiteur et toute démarche faite par lui en vue de l'accomplissement de sa mission. / Le président de la juridiction, après consultation du président de la formation de jugement, (...) fixe par ordonnance, conformément aux dispositions de l'article R. 761-4, les honoraires en tenant compte des difficultés des opérations, de l'importance, de l'utilité et de la nature du travail fourni par l'expert ou le sapiteur et des diligences mises en oeuvre pour respecter le délai mentionné à l'article R. 621-2. Il arrête sur justificatifs le montant des frais et débours qui seront remboursés à l'expert. / S'il y a plusieurs experts, ou si un sapiteur a été désigné, l'ordonnance mentionnée à l'alinéa précédent fait apparaître distinctement le montant des frais et honoraires fixés pour chacun (...) " ;<br/>
<br/>
              5. Considérant que, pour refuser toute rémunération à Mme G...et décharger le département de La Réunion des frais et débours correspondant à son intervention, la cour administrative d'appel de Bordeaux a jugé qu'il ne résultait pas de l'instruction " que l'intervention de Mme G...en tant que sapiteur "chargée de la coordination des experts" par ordonnance du 4 août 2004 ait été indispensable à la conduite de l'expertise menée par des personnes particulièrement qualifiées dans chacun de leurs domaines et dont les travaux pouvaient être synthétisés sans difficulté particulière par M.E... " ; qu'en se fondant ainsi sur l'absence d'utilité de la désignation de Mme G...en qualité de sapiteur, alors qu'en application des dispositions précitées, il lui appartenait, non de contrôler les désignations faites en application de l'article R. 621-2 du code de justice administrative mais seulement de vérifier, au regard de l'article R. 621-11 du même code, la nature des travaux effectivement réalisés par Mme G...et de s'assurer que leur rémunération ainsi que le remboursement des frais et débours auxquels ils donnaient droit étaient fixés en fonction de leur difficulté, de leur importance et de leur utilité, la cour administrative d'appel a commis une erreur de droit ; qu'il y a lieu, par suite et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, d'annuler l'arrêt en tant qu'il décharge le département de La Réunion du paiement des honoraires de Mme G... et refuse à celle-ci le remboursement de ses frais et débours ; <br/>
<br/>
              Sur les conclusions incidentes du département de La Réunion :<br/>
<br/>
              6. Considérant, d'une part, qu'il résulte de ce qui a été dit au point 2 que le pourvoi de M. E... n'est recevable qu'en tant qu'il est dirigé contre l'article 3 de l'arrêt attaqué mettant à sa charge une somme au titre de l'article L. 761-1 du code de justice administrative ; que le pourvoi incident présenté par le département de La Réunion sous le n° 370914, qui tend à l'annulation de l'arrêt du 27 juin 2013 en tant que, par cet arrêt, la cour administrative d'appel de Bordeaux a confirmé les sommes mises à sa charge par l'ordonnance du 13 mai 2008 au titre de la rémunération de M. E...et de MM.F..., H...et A...B..., soulève un litige distinct de celui sur lequel portent les conclusions recevables du pourvoi principal ; qu'il doit, par suite, être rejeté ; <br/>
<br/>
              7. Considérant, d'autre part, que les conclusions incidentes présentées par le département de La Réunion sous le n° 370916, identiques à celles présentées en réponse au pourvoi de M.E..., ne tendent pas à l'annulation de l'arrêt attaqué en tant qu'il concerne MmeG..., auteur du pourvoi principal, mais en tant qu'il confirme le montant des sommes allouées aux autres personnes ayant participé aux opérations d'expertise ; que le département doit ainsi être regardé comme saisissant le juge de cassation d'un pourvoi provoqué ; que l'annulation partielle de l'arrêt de la cour administrative d'appel de Bordeaux prononcée au point 5 remet en cause la réduction des frais d'expertise qu'il avait obtenue en appel et aggrave ainsi sa situation ; que, par suite, son pourvoi provoqué est recevable ; <br/>
<br/>
              8. Considérant, en premier lieu, qu'en jugeant que la circonstance que les travaux de nature à remédier aux désordres en cause ont été d'un coût inférieur au coût total de l'expertise était sans incidence sur le bien-fondé de l'ordonnance du 13 mai 2008, la cour n'a pas commis d'erreur de droit ; qu'en se prononçant par ces motifs, exempts de dénaturation, elle a suffisamment motivé son arrêt sur ce point ; <br/>
<br/>
              9. Considérant, en deuxième lieu, qu'en jugeant qu'il ne résultait pas de l'instruction que l'expert et ses sapiteurs auraient excédé les limites de leur mission, dès lors notamment que la durée de cette mission se justifiait par la nécessité de réaliser plusieurs campagnes de prélèvements successives pour établir un diagnostic complet et proposer les réparations à apporter, la cour administrative d'appel n'a pas davantage dénaturé les faits et pièces du dossier qui lui était soumis ; qu'en se fondant sur ces motifs pour écarter le moyen par lequel le département soutenait que la durée de l'expertise avait été exagérément prolongée, elle a suffisamment fondé sa décision ; <br/>
<br/>
              10. Considérant, en troisième lieu, qu'en confirmant le montant des honoraires dus à M.E..., au motif qu'il ne résultait pas de l'instruction que son tarif horaire aurait été excessif ni qu'il n'aurait pas consacré à sa mission le nombre d'heures indiqué par l'ordonnance du 13 mai 2008, et en confirmant le montant des honoraires dus à MM.F..., H...et A...B..., sapiteurs, au motif qu'il ne résultait pas de l'instruction que leurs interventions, distinctes de celle de l'expert, n'auraient pas été utiles à l'accomplissement de la mission de celui-ci, la cour administrative d'appel, qui a souverainement apprécié les pièces du dossier qui lui était soumis, sans les dénaturer, a suffisamment répondu à l'argumentation d'appel par laquelle le département contestait la justification des montants retenus ; <br/>
<br/>
              11. Considérant, en quatrième lieu, qu'en confirmant le remboursement des dépenses engagées par l'expert et les sapiteurs au motif qu'il ne résultait pas de l'instruction que ces frais n'auraient pas eu de lien direct avec le déroulement de l'expertise ni d'utilité pour sa réalisation, la cour administrative d'appel, qui n'était pas tenue de procéder à l'examen distinct de la justification de chacun de ces frais, n'a pas commis d'erreur de droit ; qu'en se prononçant par ces motifs, exempts de dénaturation, elle a suffisamment justifié sa décision ; <br/>
<br/>
              12. Considérant qu'il suit de là que les conclusions du pourvoi provoqué du département de La Réunion doivent être rejetées ; <br/>
<br/>
              13. Considérant qu'il résulte de tout ce qui précède qu'il y a lieu d'annuler seulement les articles 1er à 3 de l'arrêt du 27 juin 2013 de la cour administrative d'appel de Bordeaux ;<br/>
<br/>
              Sur les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              14. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge du département de La Réunion la somme de 1 500 euros à verser à M. E...et la somme de 3 500 euros à verser à Mme G...au titre des dispositions de l'article L. 761-1 du code de justice administrative ; que ces dispositions font en revanche obstacle à ce qu'il soit fait droit aux conclusions présentées au même titre par le département de La Réunion ;<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Les articles 1er à 3 de l'arrêt de la cour administrative d'appel de Bordeaux du 27 juin 2013 sont annulés.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Bordeaux dans la limite de la cassation ainsi prononcée.<br/>
Article 3 : Le département de La Réunion versera la somme de 1 500 euros à M. E...et la somme de 3 500 euros à Mme G...au titre de l'article L. 761-1 du code de justice administrative. <br/>
Article 4 : Le surplus des conclusions de M. E...et de Mme G...est rejeté.<br/>
Article 5 : Les conclusions présentées par le département de La Réunion sont rejetées. <br/>
Article 6 : La présente décision sera notifiée à M. I...E..., à Mme J...G...et au département de La Réunion. <br/>
Copie en sera adressée à M. I...H..., à M. D...A...B...et à M. C...F....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-04-02-02-02 PROCÉDURE. INSTRUCTION. MOYENS D'INVESTIGATION. EXPERTISE. HONORAIRES DES EXPERTS. - FIXATION DES HONORAIRES, FRAIS ET DÉBOURS DES EXPERTS ET SAPITEURS - 1) CRITÈRES - EXCLUSION - UTILITÉ DE LA DÉSIGNATION - INCLUSION - UTILITÉ DES TRAVAUX RÉALISÉS - 2) POURVOI INCIDENT DE LA COLLECTIVITÉ PUBLIQUE DÉBITRICE FORMÉ CONTRE UN ARRÊT FIXANT CES MONTANTS À L'OCCASION DU POURVOI FORMÉ PAR UN SAPITEUR CONTRE CE MÊME ARRÊT EN TANT QU'IL NE LUI ALLOUE AUCUNE SOMME - NATURE - POURVOI PROVOQUÉ -  RECEVABILITÉ - EXISTENCE, LA DÉCISION DU JUGE DE CASSATION AYANT POUR EFFET D'AGGRAVER LA SITUATION DE LA COLLECTIVITÉ.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-08-02-004-01 PROCÉDURE. VOIES DE RECOURS. CASSATION. RECEVABILITÉ. RECEVABILITÉ DES POURVOIS. - FIXATION DES HONORAIRES, FRAIS ET DÉBOURS DES EXPERTS ET SAPITEURS - POURVOI INCIDENT DE LA COLLECTIVITÉ PUBLIQUE DÉBITRICE FORMÉ CONTRE UN ARRÊT FIXANT CES MONTANTS À L'OCCASION DU POURVOI FORMÉ PAR UN SAPITEUR CONTRE CE MÊME ARRÊT EN TANT QU'IL NE LUI ALLOUE AUCUNE SOMME - NATURE - POURVOI PROVOQUÉ -  RECEVABILITÉ - EXISTENCE, LA DÉCISION DU JUGE DE CASSATION AYANT POUR EFFET D'AGGRAVER LA SITUATION DE LA COLLECTIVITÉ.
</SCT>
<ANA ID="9A"> 54-04-02-02-02 1) Il appartient au juge, se prononçant en application de l'article R. 621-11 du code de justice administrative (CJA) sur le montant des sommes à allouer aux experts et sapiteurs, non de contrôler les désignations faites en application de l'article R. 621-2 du CJA mais seulement de vérifier, au regard de l'article R. 621-11, la nature des travaux effectivement réalisés et de s'assurer que les honoraires visant à les rémunérer ainsi que le remboursement des frais et débours auxquels ils donnent droit sont fixés en fonction de leur difficulté, de leur importance et de leur utilité.,,,2) Arrêt de cour administrative d'appel ayant réduit, sur appel de la collectivité publique, le montant des sommes allouées en première instance aux différents participants à une opération d'expertise au titre de leurs honoraires et du remboursement des frais et débours. A l'occasion du pourvoi en cassation formé par un participant à ces opérations, demandant l'annulation de cet arrêt en tant qu'il ne lui a alloué aucune somme, la collectivité publique forme un pourvoi incident contre cet arrêt en tant qu'il arrête les montants alloués aux autres participants.... ,,Ce pourvoi incident, qui n'est pas dirigé contre l'auteur du pourvoi principal, a le caractère d'un pourvoi provoqué. Il est recevable, dès lors que la cassation partielle de cet arrêt prononcée par le Conseil d'Etat remet en cause la réduction des frais que la collectivité publique avait obtenue en appel et aggrave ainsi sa situation.</ANA>
<ANA ID="9B"> 54-08-02-004-01 Arrêt de cour administrative d'appel ayant réduit, sur appel de la collectivité publique, le montant des sommes allouées en première instance aux différents participants à une opération d'expertise au titre de leurs honoraires et du remboursement des frais et débours. A l'occasion du pourvoi en cassation formé par un participant à ces opérations, demandant l'annulation de cet arrêt en tant qu'il ne lui a alloué aucune somme, la collectivité publique forme un pourvoi incident contre cet arrêt en tant qu'il arrête les montants alloués aux autres participants.... ,,Ce pourvoi incident, qui n'est pas dirigé contre l'auteur du pourvoi principal, a le caractère d'un pourvoi provoqué. Il est recevable, dès lors que la cassation partielle de cet arrêt prononcée par le Conseil d'Etat remet en cause la réduction des frais que la collectivité publique avait obtenue en appel et aggrave ainsi sa situation.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
