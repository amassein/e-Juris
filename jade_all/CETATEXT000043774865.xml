<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043774865</ID>
<ANCIEN_ID>JG_L_2021_07_000000409716</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/77/48/CETATEXT000043774865.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème - 8ème chambres réunies, 05/07/2021, 409716, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-07-05</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>409716</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème - 8ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>BALAT</AVOCATS>
<RAPPORTEUR>M. Géraud Sajust de Bergues</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Marie-Gabrielle Merloz</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:409716.20210705</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              La société Crédit industriel et commercial (CIC) a demandé au tribunal administratif de Montreuil de prononcer la décharge des cotisations supplémentaires d'impôt sur les sociétés ainsi que des pénalités correspondantes auxquelles elle a été assujettie au titre des exercices clos en 2002 et 2003, en sa qualité de société mère du groupe fiscalement intégré auquel appartenait la société Crédit Industriel d'Alsace-Lorraine. Par un jugement n° 0905910 du 27 mai 2010, le tribunal administratif de Montreuil a fait droit à sa demande et prononcé la décharge de ces impositions.<br/>
<br/>
              Par un arrêt n° 10VE03240 du 13 décembre 2011, la cour administrative d'appel de Versailles a rejeté l'appel formé par le ministre du budget, des comptes publics et de la réforme de l'Etat contre ce jugement.<br/>
<br/>
              Par une décision n° 357189 du 7 décembre 2015, le Conseil d'Etat statuant au contentieux a annulé cet arrêt et renvoyé l'affaire à la cour administrative d'appel de Versailles.<br/>
<br/>
              Par un second arrêt n° 15VE03818 du 28 février 2017, la cour administrative d'appel de Versailles, faisant droit à l'appel formé par le ministre du budget, des comptes publics et de la réforme de l'Etat, a annulé le jugement du tribunal administratif de Montreuil du 27 mai 2010 et remis à la charge de la société CIC les impositions en litige.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 11 avril et 11 juillet 2017 au secrétariat du contentieux du Conseil d'Etat, la société CIC demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce second arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel du ministre du budget, des comptes publics et de la réforme de l'Etat ;<br/>
<br/>
              3°) à titre subsidiaire, de renvoyer une question préjudicielle à la Cour de justice de l'Union européenne ;<br/>
<br/>
              4°) de mettre à la charge de l'Etat la somme de 25 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le traité sur le fonctionnement de l'Union européenne ;<br/>
              - la charte des droits fondamentaux de l'Union européenne ; <br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, son premier protocole additionnel et son protocole n° 12 ;<br/>
              - la convention signée le 5 octobre 1989 entre le Gouvernement de la République française et le Gouvernement de la République italienne en vue d'éviter les doubles impositions en matière d'impôts sur le revenu et sur la fortune et de prévenir l'évasion et la fraude fiscales ;<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - l'arrêt de la Cour de justice de l'Union européenne du 25 février 2021, Société Générale (C-403/19) ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Géraud Sajust de Bergues, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Marie-Gabrielle Merloz, rapporteure publique ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à Me Balat, avocat de la société Crédit industriel et commercial (CIC) ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que la société Crédit Industriel d'Alsace-Lorraine a réalisé pendant de courtes périodes, en 2002 et 2003, des opérations d'emprunts de titres d'une société italienne auprès de la banque britannique Goldman Sachs International. Conformément aux conventions de prêt, la société Crédit Industriel d'Alsace-Lorraine a encaissé les dividendes servis par la société italienne, diminués de la retenue à la source acquittée sur ces dividendes en Italie, puis a reversé immédiatement à la banque britannique une somme correspondant au montant brut de ces dividendes et acquitté les intérêts courus sur la durée du prêt. La société Crédit industriel d'Alsace-Lorraine a imputé sur le montant de l'impôt sur les sociétés afférent aux exercices clos en 2002 et 2003 les crédits d'impôt correspondant au montant des retenues à la source acquittées en Italie sur les dividendes encaissés pendant la période d'emprunt des titres. A l'issue d'une vérification de la comptabilité de cette société, l'administration a remis en cause cette imputation.<br/>
<br/>
              2. Par un jugement du 27 mai 2010, le tribunal administratif de Montreuil a prononcé la décharge des cotisations supplémentaires d'impôt sur les sociétés auxquelles la société Crédit Industriel et Commercial (CIC), en sa qualité de société mère du groupe fiscalement intégré auquel appartenait la société Crédit Industriel d'Alsace-Lorraine, a été assujettie au titre des exercices clos en 2002 et 2003 à l'issue de la vérification de la comptabilité de cette dernière. Par une décision du 7 décembre 2015, le Conseil d'Etat statuant au contentieux a annulé l'arrêt du 13 décembre 2011 par lequel la cour administrative d'appel de Versailles a rejeté l'appel formé par le ministre du budget, des comptes publics et de la réforme de l'Etat contre ce jugement. La société CIC se pourvoit contre l'arrêt du 28 février 2017 par lequel la cour administrative d'appel de Versailles, statuant après renvoi, a fait droit à l'appel formé par le ministre du budget, des comptes publics et de la réforme de l'Etat, annulé le jugement du tribunal administratif de Montreuil du 27 mai 2010 et remis à la charge de la société CIC les impositions en litige.<br/>
<br/>
              Sur le moyen tiré de la tardiveté de l'appel du ministre :<br/>
<br/>
              3. Aux termes de l'article R*. 200-18 du livre des procédures fiscales : " A compter de la notification du jugement du tribunal administratif qui a été faite au directeur du service de la direction générale des finances publiques ou de la direction générale des douanes et droits indirects qui a suivi l'affaire, celui-ci dispose d'un délai de deux mois pour transmettre, s'il y a lieu, le jugement et le dossier au ministre chargé du budget. / Le délai imparti pour saisir la cour administrative d'appel court, pour le ministre, de la date à laquelle expire le délai de transmission prévu à l'alinéa précédent ou de la date de la signification faite au ministre ". Il résulte de ces dispositions qu'en l'absence de signification du jugement du tribunal administratif par le contribuable au ministre, le délai imparti à ce dernier pour interjeter appel est de quatre mois à compter de la notification de ce jugement au directeur du service de l'administration des impôts, sans qu'il y ait lieu de rechercher à quelle date le jugement lui a été transmis.<br/>
<br/>
              4. Les dispositions de l'article R*. 200-18 du livre des procédures fiscales tiennent compte des nécessités particulières du fonctionnement de l'administration fiscale qui la placent dans une situation différente de celle des autres justiciables, en ménageant au ministre chargé du budget un délai d'appel qui peut excéder celui dont le contribuable dispose, en application de l'article R. 811-2 du code de justice administrative, pour saisir la cour administrative d'appel territorialement compétente d'une requête tendant à l'annulation d'un jugement de tribunal administratif, même lorsque le tribunal en cause a statué sur des pénalités fiscales. Le contribuable conserve néanmoins la faculté, y compris lorsque le ministre a saisi la cour après l'expiration du délai de deux mois prévu par l'article R. 811-2 du code de justice administrative, outre de présenter des observations en défense, de former un appel incident en vue de contester les pénalités qui étaient en litige devant le tribunal, quand bien même le ministre ne contesterait que les impositions dont ce tribunal aurait déchargé le contribuable. Par ailleurs, le contribuable est en mesure d'écourter le délai ouvert à l'administration, en application de l'article R*. 200-18 du livre des procédures fiscales, en signifiant directement au ministre, seul compétent pour faire appel, le jugement dont il a lui-même reçu notification.<br/>
<br/>
              5. Il résulte de ce qui précède que la cour administrative d'appel n'a pas commis d'erreur de droit en écartant le fin de non-recevoir opposée par la société CIC à l'appel que le ministre du budget, des comptes publics et de la réforme de l'Etat a formé contre le jugement du tribunal administratif de Montreuil du 27 mai 2010, au motif que le délai de recours supplémentaire de deux mois accordé au ministre par l'article R. 200-18 du livre des procédures fiscales était justifié par les nécessités du fonctionnement de l'administration et, par suite, ne portait atteinte ni au principe constitutionnel d'égalité devant la justice et d'égalité devant la loi, ni aux stipulations de l'article 6 § 1 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, ni, en tout état de cause, aux stipulations des articles 20 et 51 de la charte des droits fondamentaux de l'Union européenne. Par ailleurs, la société CIC ne peut utilement invoquer la méconnaissance des stipulations du protocole n° 12 à la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, qui n'a été ni signé ni ratifié par la France, non plus, par conséquent, que l'article 14 de cette convention, qui ne concerne que la jouissance des droits et libertés reconnus par celle-ci et n'est pas d'application autonome.<br/>
<br/>
              6. Enfin, il résulte des dispositions de l'article R*. 200-18 du livre des procédures fiscales que le délai imparti au ministre pour interjeter appel d'un jugement est de quatre mois à compter de la notification du jugement du tribunal administratif au directeur du service de l'administration des impôts. Par suite, la cour administrative d'appel n'a pas commis d'erreur de droit en ne recherchant pas à quelle date la direction des vérifications nationales et internationales, à qui le jugement du tribunal administratif de Montreuil avait été notifié le 9 juin 2010, l'avait transmis au ministre chargé du budget.<br/>
<br/>
              En ce qui concerne les autres moyens du pourvoi :<br/>
<br/>
              Sur le cadre juridique du litige :<br/>
<br/>
              7. Aux termes de l'article 39 du code général des impôts : "1. Le bénéfice net est établi sous déduction de toutes charges (...) ". Aux termes du premier alinéa du I de l'article 209 du même code, dans sa rédaction applicable aux années d'imposition en litige : "Sous réserve des dispositions de la présente section, les bénéfices passibles de l'impôt sur les sociétés sont déterminés d'après les règles fixées par les articles 34 à 45 (...) et en tenant compte uniquement des bénéfices réalisés dans les entreprises exploitées en France ainsi que de ceux dont l'imposition est attribuée à la France par une convention internationale'relative aux doubles impositions / (...) ". Aux termes du 1 de l'article 220 de ce code, dans sa rédaction applicable à ces mêmes années d'imposition : "a) Sur justifications, la retenue à la source à laquelle ont donné ouverture les revenus des capitaux mobiliers, visés aux articles 108 à 119, 238 septies B et 1678 bis, perçus par la société ou la personne morale est imputée sur le montant de l'impôt à sa charge en vertu du présent chapitre. / Toutefois, la déduction à opérer de ce chef ne peut excéder la fraction de ce dernier impôt correspondant au montant desdits revenus. / b) En ce qui concerne les revenus de source étrangère visés aux articles 120 à 123, l'imputation est limitée au montant du crédit correspondant à l'impôt retenu à la source à l'étranger ou à la décote en tenant lieu, tel qu'il est prévu par les conventions internationales / (...)". <br/>
<br/>
              8. Aux termes de l'article 10 de la convention conclue le 5 octobre 1989 entre la France et l'Italie en vue d'éviter les doubles impositions et de prévenir l'évasion et la fraude fiscales : "1. Les dividendes payés par une société qui est un résident d'un Etat à un résident de l'autre Etat sont imposables dans cet autre Etat. / 2. Toutefois, ces dividendes sont aussi imposables dans l'Etat dont la société qui paie les dividendes est un résident, et selon la législation de cet Etat (...)". Aux termes des stipulations de l'article 24 de la même convention : "1. La double imposition est évitée de la manière suivante : / (...) a) Les bénéfices et autres revenus positifs qui proviennent d'Italie et qui y sont imposables conformément aux dispositions de la convention, sont également imposables en France lorsqu'ils reviennent à un résident de France. L'impôt italien n'est pas déductible pour le calcul du revenu imposable en France. Mais le bénéficiaire a droit à un crédit d'impôt imputable sur l'impôt français dans la base duquel ces revenus sont compris. Ce crédit d'impôt est égal : / - pour les revenus visés aux articles 10, 11, 12, 16 et 17 (...) au montant de l'impôt payé en Italie, conformément aux dispositions de ces articles. Il ne peut toutefois excéder le montant de l'impôt français correspondant à ces revenus (...) ".<br/>
<br/>
              9. Les termes " bénéfices ", " revenus " et " autres revenus positifs " mentionnés à l'article 24 de la convention franco-italienne ne sont pas autrement définis par cette convention en ce qui concerne les dividendes. Ces termes doivent, dès lors, être interprétés selon le principe rappelé à l'article 3 de cette convention, en vertu duquel toute expression qui n'y est pas définie a le sens que lui attribue le droit de l'Etat cocontractant, concernant les impôts auxquels s'applique la convention, à moins que le contexte n'exige une interprétation différente. En l'absence d'élément exigeant une interprétation différente, les " bénéfices ", " revenus " et " autres revenus positifs " auxquels font référence les articles susmentionnés sont ceux déterminés selon les règles fixées par le code général des impôts.<br/>
<br/>
              10. A cet égard, il résulte des dispositions précitées du b) du 1 de l'article 220 du code général des impôts, qui définissent le régime applicable aux revenus de source étrangère auxquels cette disposition fait référence, que l'imputation sur l'impôt dû en France de la retenue à la source acquittée à l'étranger à raison de ces revenus est limitée au montant du crédit d'impôt correspondant à cette retenue à la source tel qu'il est prévu par les conventions internationales. En vertu des stipulations précitées de l'article 24 de la convention franco-italienne, lorsqu'une société soumise à l'impôt en France perçoit des dividendes d'une société étrangère soumis à une retenue à la source dans ces pays, elle est imposable en France sur ces dividendes, mais a droit à un crédit d'impôt imputable sur l'impôt sur les sociétés. Conformément à ces stipulations, ce crédit d'impôt ne peut excéder le montant de l'impôt français correspondant à ces revenus. Ce montant maximal doit être déterminé, en l'absence de toute stipulation contraire dans ces conventions fiscales, en appliquant aux dividendes qui ont fait l'objet de la retenue à la source dans ces pays, pour leur montant brut, l'ensemble des dispositions du code général des impôts relatives à l'impôt sur les sociétés, dont celles de l'article 39, applicables en matière d'impôt sur les sociétés en vertu de l'article 209, c'est-à-dire en déduisant du montant des dividendes distribués, avant toute retenue à la source, et sauf exclusion par des dispositions spécifiques, les charges justifiées, qui ne sont exposées que du fait de l'acquisition, de la détention ou de la cession des titres ayant donné lieu à la perception des dividendes, qui sont directement liées à cette perception et qui n'ont pas pour contrepartie un accroissement de l'actif.<br/>
<br/>
              Sur l'arrêt attaqué :<br/>
<br/>
              11. La cour administrative d'appel a jugé que l'administration fiscale avait pu à bon droit estimer que les sommes reversées par la société Crédit Industriel d'Alsace-Lorraine à la banque britannique prêteuse des titres et qui avaient donné lieu au prélèvement de retenues à la source en Italie devaient être déduites des dividendes perçus pour le calcul du revenu pris en compte en vue de déterminer le montant maximal des crédits d'impôt pouvant être imputés sur l'impôt sur les sociétés dû au titre des bénéfices de la société Crédit Industriel d'Alsace-Lorraine. <br/>
<br/>
              12. En premier lieu, il ressort des pièces du dossier soumis aux juges du fond qu'en vertu des conventions de prêts/emprunts de titres à court terme d'une société italienne qu'elle avait conclues avec la banque britannique Goldman Sachs International, la société Crédit Industriel d'Alsace-Lorraine était tenue de reverser immédiatement à cette banque une somme, majorée d'intérêts, correspondant au montant brut des dividendes attachés aux titres empruntés, déduction faite de la retenue à la source acquittée en Italie. Il résulte ainsi de ces conventions que le reversement de cette somme majorée d'intérêts par la société Crédit Industriel d'Alsace-Lorraine à la banque britannique Goldman Sachs International constituait une condition de la conservation des titres de la société italienne par la société Crédit Industriel d'Alsace-Lorraine et qu'il était directement lié à la perception de ces dividendes. Par suite, la cour administrative d'appel n'a pas inexactement qualifié les faits au regard des règles énoncées au point 10 en jugeant que le reversement effectué par la société Crédit Industriel d'Alsace-Lorraine devait être analysé, pour l'application de ces règles, comme une charge déductible du montant du revenu perçu. <br/>
<br/>
              13. En second lieu, la société CIC soutient que la cour administrative d'appel a commis une erreur de droit et méconnu la portée de ses écritures en écartant son moyen tiré de ce que l'application des règles énoncées au point 10 conduit à méconnaître la liberté de circulation des capitaux protégée par le droit de l'Union européenne. Elle soutient que les opérations portant sur des titres de sociétés étrangères réalisées par des sociétés soumises à l'impôt sur les sociétés en France sont désavantagées par rapport à celles qui portent sur des titres de sociétés françaises dès lors, d'une part, que les titres des sociétés étrangères sont soumis à une double imposition des dividendes à raison du plafonnement abusif du crédit d'impôt institué par les conventions fiscales conclues par la France en vue d'éliminer les double impositions et, d'autre part, que ce plafonnement équivaudrait à limiter, pour les titres étrangers, la déductibilité des charges à prendre en compte pour la détermination de l'impôt sur les sociétés dû en France.<br/>
<br/>
              14. Dans l'arrêt du 25 février 2021, Société Générale (C-403/19), par lequel elle s'est prononcée sur la question dont le Conseil d'Etat, statuant au contentieux, l'avait saisie à titre préjudiciel par sa décision n° 399952 du 24 avril 2019, la Cour de justice de l'Union européenne a dit pour droit que l'article 63 du traité sur le fonctionnement de l'Union européenne doit être interprété en ce sens qu'il ne s'oppose pas à une réglementation d'un Etat membre qui, dans le cadre d'un régime visant à compenser la double imposition de dividendes perçus par une société assujettie à l'impôt sur les sociétés de cet Etat membre dans lequel elle est établie, ayant fait l'objet d'un prélèvement par un autre Etat membre, accorde à une telle société un crédit d'impôt plafonné au montant que ce premier Etat membre recevrait si ces seuls dividendes étaient soumis à l'impôt sur les sociétés, sans compenser en totalité le prélèvement acquitté dans cet autre Etat membre. La Cour de justice a précisé, en ce qui concerne les modalités de calcul de ce crédit d'impôt, que les charges afférentes spécifiquement aux dividendes, déduites lors de ce calcul, conformément à la jurisprudence du Conseil d'Etat, doivent également être déduites du résultat global de la société résidente s'agissant des dividendes de source nationale.<br/>
<br/>
              15. Il résulte de l'interprétation ainsi donnée par la Cour de justice de l'Union européenne que les règles énoncées au point 10, qui prévoient notamment que les charges venant en déduction du montant des dividendes de source étrangère soumis à une retenue à la source sont également déduites pour la détermination de l'assiette de l'impôt sur les sociétés dû en France, ne méconnaissent pas la libre circulation des capitaux. Par suite, le moyen de la société CIC tiré de ce que la cour administrative d'appel a commis, à cet égard, une erreur de droit et une méconnaissance de la portée de ses écritures doit être écarté, de même que son moyen soulevé par voie de conséquence, tiré de l'erreur de droit qu'aurait commise la cour administrative d'appel en rejetant comme inopérant son moyen tiré de la violation de l'article 1er du premier protocole à la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales.<br/>
<br/>
              16. Il résulte de tout ce qui précède que le pourvoi de la société CIC doit être rejeté, y compris ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de la société CIC est rejeté.<br/>
Article 2 : La présente décision sera notifiée à la société Crédit Industriel et Commercial et au ministre de l'économie, des finances et de la relance.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
