<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043205059</ID>
<ANCIEN_ID>JG_L_2021_03_000000439964</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/20/50/CETATEXT000043205059.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème - 9ème chambres réunies, 01/03/2021, 439964, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-03-01</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>439964</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème - 9ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>CABINET COLIN - STOCLET</AVOCATS>
<RAPPORTEUR>M. Réda Wadjinny-Green</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Alexandre Lallet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:439964.20210301</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
      Vu la procédure suivante :<br/>
      Par une requête sommaire et un mémoire complémentaire, enregistrés les 6 avril et 3 juillet 2020 au secrétariat du contentieux du Conseil d'Etat, les communes de Faa'a et de Punaauia demandent au Conseil d'Etat : <br/>
<br/>
      1°) d'annuler pour excès de pouvoir le décret n° 2020-98 du 5 février 2020 relatif aux modalités de répartition de la dotation d'équipement des territoires ruraux en Polynésie française ;<br/>
<br/>
      2°) de mettre à la charge de l'Etat une somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.  <br/>
<br/>
<br/>
      Vu les autres pièces du dossier ; <br/>
<br/>
      Vu :<br/>
      - le code général des collectivités territoriales ;<br/>
      - le décret n° 2020-98 du 5 février 2020 ; <br/>
      - la décision n° 439964 du 28 septembre 2020 par laquelle le Conseil d'Etat statuant au contentieux n'a pas renvoyé au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée par les communes de Faa'a et de Punaauia ; <br/>
      - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
<br/>
<br/>
      Après avoir entendu en séance publique :<br/>
<br/>
      - le rapport de M. Réda Wadjinny-Green, auditeur,  <br/>
<br/>
      - les conclusions de M. Alexandre Lallet, rapporteur public ;<br/>
<br/>
      La parole ayant été donnée, après les conclusions, au Cabinet Colin - Stoclet, avocat de la commune de Faa'a et de la commune de Punaauia ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. L'article L. 2334-32 du code général des collectivités territoriales institue une dotation d'équipement des territoires ruraux " en faveur des établissements publics de coopération intercommunale à fiscalité propre et des communes répondant aux critères indiqués à l'article L. 2334-33 ". L'article L. 2334-33 définit les critères démographiques que doivent remplir les collectivités territoriales pour être éligibles à cette dotation. Sont notamment concernés : " 1° (...) b) Les établissements publics de coopération intercommunale à fiscalité propre dans les départements d'outre-mer et le Département de Mayotte qui ne forment pas un ensemble de plus de 150 000 habitants d'un seul tenant et sans enclave autour d'une ou de plusieurs communes centres de plus de 85 000 habitants et dont la densité de population est supérieure ou égale à 150 habitants au kilomètre carré, en prenant en compte la population issue du dernier recensement. (...) 2° Les communes : / a) Dont la population n'excède pas 2 000 habitants dans les départements de métropole et 3 500 habitants dans les départements d'outre-mer ; / b) Dont la population est supérieure à 2 000 habitants dans les départements de métropole et 3 500 habitants dans les départements d'outre-mer et n'excède pas 20 000 habitants dans les départements de métropole et 35 000 habitants dans les départements d'outre-mer et dont le potentiel financier par habitant est inférieur à 1,3 fois le potentiel financier moyen par habitant de l'ensemble des communes dont la population est supérieure à 2 000 habitants et n'excède pas 20 000 habitants ". Aux termes de l'article L. 2334-34 : " Les circonscriptions territoriales de Wallis-et-Futuna, les communes ainsi que leurs groupements des collectivités d'outre-mer et de Nouvelle-Calédonie bénéficient d'une quote-part de la dotation d'équipement des territoires ruraux dont le montant est calculé par application au montant total de cette dotation du rapport, majoré de 33 %, existant entre la population de chacune des collectivités et groupements intéressés et la population nationale, telle qu'elle résulte du dernier recensement de population. Le montant de cette quote-part évolue au moins comme la masse totale de la dotation d'équipement des territoires ruraux mise en répartition ". L'article L. 2334-38 précise que " les investissements pour lesquels les communes et leurs groupements à fiscalité propre sont susceptibles de recevoir des subventions de l'Etat dont la liste est fixée par voie réglementaire ne peuvent être subventionnés au titre de la dotation d'équipement des territoires ruraux ". Enfin, l'article L. 2573-54 du même code prévoit que " les articles L. 2334-32, L. 2334-33 et L. 2334-38 sont applicables aux communes de la Polynésie française ".<br/>
<br/>
              2. Antérieurement au décret attaqué du 5 février 2020 relatif aux modalités de répartition de la dotation d'équipement des territoires ruraux en Polynésie française, les crédits relatifs à la dotation d'équipement des territoires ruraux étaient versés aux communes de plus de 20 000 habitants de Polynésie française sous la forme d'une dotation libre d'emploi. Le décret du 5 février 2020 a modifié l'article R. 2573-54 du code général des collectivités territoriales, qui dispose désormais que : " Le haut-commissaire de la République attribue ces crédits aux communes et groupements de communes de Polynésie française sous forme de subventions dans les conditions prévues à l'article R. 2573-55 ", harmonisant ainsi le régime applicable aux communes de plus et de moins de 20 000 habitants en Polynésie française avec celui de la métropole. En revanche, le même décret réaffirme à l'article R. 234-5 du code des communes de la Nouvelle-Calédonie le principe d'une dotation libre d'emploi pour les communes de plus de 20 000 habitants situées en Nouvelle-Calédonie. Les communes de Faa'a et de Punaauia demandent l'annulation pour excès de pouvoir de ce décret. <br/>
<br/>
              3. L'article 34 de la Constitution dispose que : " La loi détermine les principes fondamentaux : / de la libre administration des collectivités territoriales, de leurs compétences et de leurs ressources " tandis qu'aux termes de son article 72 " Dans les conditions prévues par la loi, ces collectivités s'administrent librement par des conseils élus et disposent d'un pouvoir réglementaire pour l'exercice de leurs compétences ". Enfin, l'article 72-2 de la Constitution prévoit que " Les collectivités territoriales bénéficient de ressources dont elles peuvent disposer librement dans les conditions fixées par la loi ".<br/>
<br/>
              4. En premier lieu, il ressort des dispositions de l'article L. 2334-38 du code général des collectivités territoriales cité au point 1 et rendu applicable en Polynésie française en vertu de l'article L. 2573-54 du même code que le législateur a entendu permettre au gouvernement de prévoir que les crédits de la dotation d'équipement des territoires ruraux soient versés aux communes sous la forme de subventions. Il s'ensuit que le Premier ministre pouvait, sur le fondement des articles L. 2334-32, L. 2334-33 et L. 2334-38 applicables en Polynésie française et sans méconnaître sa compétence ni les principes de libre administration et d'autonomie financière des collectivités territoriales, prévoir que la dotation d'équipement des territoires ruraux serait versée aux communes de plus de 20 000 habitants de Polynésie française sous la forme de subventions de projets. <br/>
<br/>
              5. En deuxième lieu, le principe d'égalité ne s'oppose pas à ce que l'autorité investie du pouvoir réglementaire règle de façon différente des situations différentes ni à ce qu'elle déroge à l'égalité pour des raisons d'intérêt général, pourvu que dans l'un comme dans l'autre cas la différence de traitement qui en résulte soit en rapport avec l'objet de la norme qui l'établit. L'article L. 2334-38 précité n'est pas applicable en Nouvelle-Calédonie et aucune disposition du code général des collectivités territoriales ne prévoit que la dotation d'équipement des territoires ruraux puisse y être versée sous la forme de subventions. Eu égard aux spécificités du contexte néo-calédonien en termes de relations entre l'Etat et les collectivités territoriales, le décret attaqué pouvait, en tout état de cause, sans méconnaître le principe d'égalité entre les communes de plus de 20 000 habitants situées en Nouvelle-Calédonie, d'une part, et en Polynésie française, d'autre part, prévoir que la dotation d'équipement des territoires ruraux serait versée sous la forme d'une dotation libre d'emploi pour les premières et sous la forme de subventions de projets pour les secondes. Par ailleurs, le territoire de Wallis-et-Futuna ne comportant aucune commune de plus de 20 000 habitants, les communes requérantes ne sauraient utilement invoquer les règles qui seraient applicables aux communes de plus de 20 000 habitants sur ce territoire à l'appui du moyen tiré de la méconnaissance du principe d'égalité. <br/>
<br/>
              6. En troisième lieu, l'exercice du pouvoir réglementaire implique pour son détenteur la possibilité de modifier à tout moment les normes qu'il définit sans que les personnes auxquelles sont, le cas échéant, imposées de nouvelles contraintes, puissent invoquer un droit au maintien de la réglementation existante. En principe, les nouvelles normes ainsi édictées ont vocation à s'appliquer immédiatement, dans le respect des exigences attachées au principe de non-rétroactivité des actes administratifs. Il incombe toutefois à l'autorité investie du pouvoir réglementaire, agissant dans les limites de sa compétence et dans le respect des règles qui s'imposent à elle, d'édicter, pour des motifs de sécurité juridique, les mesures transitoires qu'implique, s'il y a lieu, cette réglementation nouvelle. Il en va ainsi lorsque l'application immédiate de celle-ci entraîne, au regard de l'objet et des effets de ses dispositions, une atteinte excessive aux intérêts publics ou privés en cause. <br/>
<br/>
              7. Les communes de Faa'a et de Punaauia soutiennent que la modification, pour l'année en cours, des modalités de versement des crédits relatifs à la dotation d'équipement des territoires ruraux pour les communes de plus de 20 000 habitants de Polynésie française les prive d'une dotation certaine et que le décret attaqué ne pouvait prévoir qu'aucune mesure transitoire ne s'appliquerait sans méconnaître le principe de sécurité juridique. Cependant, et en tout état de cause, la modification des règles relatives au versement de la dotation d'équipement des territoires ruraux par le décret attaqué n'emporte pas, par elle-même, une atteinte excessive aux intérêts des communes requérantes eu égard aux montants en cause et à la possibilité qui leur a été laissée de présenter des demandes de subvention pour les projets éligibles au titre de l'année en cours. Il s'ensuit que ce moyen doit être écarté.<br/>
<br/>
<br/>
              8. Il résulte de tout ce qui précède que la requête des communes de Faa'a et de Punaauia doit être rejetée, y compris les conclusions au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête des communes de Faa'a et de Punaauia est rejetée. <br/>
Article 2 : La présente décision sera notifiée aux communes de Faa'a et de Punaauia, au Premier ministre, au ministre des outre-mer et au président de la Polynésie française.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
