<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027752988</ID>
<ANCIEN_ID>JG_L_2013_07_000000363117</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/75/29/CETATEXT000027752988.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  7ème sous-section jugeant seule, 25/07/2013, 363117, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-07-25</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>363117</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 7ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Jean-Dominique Nuttens</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Bertrand Dacosta</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2013:363117.20130725</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 28 septembre 2012 au secrétariat du contentieux du Conseil d'Etat, présentée par la société Les Serres de la Marmande, dont le siège est 11T allée de la Vigne du Prince à Saint-Amand-Montrond (18200), représentée par son gérant en exercice, la société Agralys distribution, dont le siège est rue de Courtalain à La Chapelle-du-Noyer (28200), représentée par son président directeur général en exercice, la société Pauge, dont le siège est Les Noix Brûlées à Orval (18200), représentée par son président directeur général en exercice ; les sociétés demandent au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler la décision n° 1383 T du 27 juin 2012 par laquelle la Commission nationale d'aménagement commercial a accordé à la SCI Bianca d'Auvergnes l'autorisation préalable requise en vue de la création d'une jardinerie à l'enseigne " L'Esprit Jardiland ", d'une surface de vente de 3 968 m², à Saint-Amand-Montrond (Cher) ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 6 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 2 juillet 2013, présentée pour les sociétés Les Serres de la Marmande et autres ;<br/>
<br/>
              Vu le code de commerce ;<br/>
<br/>
              Vu le décret n° 2005-850 du 27 juillet 2005 ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Dominique Nuttens, Maître des Requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Bertrand Dacosta, rapporteur public ;<br/>
<br/>
<br/>
<br/>Sur la légalité externe de l'autorisation attaquée :<br/>
<br/>
              1. Considérant, en premier lieu, qu'il ressort des pièces du dossier que le commissaire du Gouvernement a recueilli l'avis de la ministre de l'égalité des territoires et du logement, de la ministre de l'écologie, du développement durable et de l'énergie et du ministre du redressement productif ; que ces avis ont été signés par des personnes dûment habilitées à le faire ; qu'en particulier, M. D...bénéficiait d'une délégation de signature du directeur général de la compétitivité, de l'industrie et des services à l'effet de signer tous actes, arrêtés et décisions, marchés ou conventions, dans la limite des attributions du service du tourisme, du commerce de l'artisanat et des services, qui n'est pas devenue caduque du fait du changement du ministre sous l'autorité duquel était placée la direction générale concernée ; que, dès lors, le moyen tiré de l'irrégularité de l'avis des ministres intéressés  doit être écarté ;<br/>
<br/>
              2. Considérant que si les sociétés requérantes font valoir que l'avis des ministres de l'égalité des territoires et du logement et de l'écologie, du développement durable et de l'énergie est entaché d'une erreur de fait, il ne ressort pas des pièces du dossier que l'avis ait été émis sans prendre en considération l'ensemble des documents pertinents ;<br/>
<br/>
              3. Considérant qu'il ne ressort pas des pièces du dossier que M. A...E..., nommé commissaire du Gouvernement auprès de la Commission nationale d'aménagement commercial par arrêté du 23 mars 2012 n'ait pas été absent ou empêché ; que le moyen tiré de l'exercice irrégulier des fonctions de commissaire du Gouvernement par M. B...C..., compétent pour exercer lesdites fonctions en cas d'absence ou d'empêchement de M.E..., ne peut qu'être écarté ; <br/>
<br/>
              Sur la légalité interne de l'autorisation attaquée :<br/>
<br/>
              En ce qui concerne la composition du dossier de la demande d'autorisation :<br/>
<br/>
              4. Considérant qu'aux termes de l'article R. 752-7 du code de commerce : " I. - La demande d'autorisation est  accompagnée : / 1° D'un plan indicatif faisant apparaître la surface de vente des commerces ; / 2° Des renseignements suivants : / a) Délimitation de la zone de chalandise du projet, telle que définie à l'article R. 752-8 (...) / II. - La demande est également accompagnée d'une étude destinée à permettre à la commission d'apprécier les effets prévisibles du projet au regard des critères prévus par l'article L. 752-6. Celle-ci comporte les éléments permettant d'apprécier les effets du projet sur : / 1° L'accessibilité de l'offre commerciale ; / 2° Les flux de voitures particulières et de véhicules de livraison ainsi que sur les accès sécurisés à la voie publique ; / 3° La gestion de l'espace ; / 4° Les consommations énergétiques et la pollution ; / 5° Les paysages et les écosystèmes " ; que si les sociétés requérantes font valoir que la demande de la SCI Bianca d'Auvergne était insuffisante en ce qui concerne les flux de véhicules, les accès sécurisés à la voie publique et les consommations énergétiques, il ressort des pièces du dossier que la Commission nationale a disposé d'éléments suffisants pour statuer sur ces différents points ; que, dès lors, le moyen tiré de la méconnaissance des dispositions de l'article R. 752-7 du code de commerce doit être écarté ; <br/>
<br/>
              En ce qui concerne l'appréciation de la Commission nationale d'aménagement commercial :<br/>
<br/>
              5. Considérant, d'une part, qu'aux termes du troisième alinéa de l'article 1er de la loi du 27 décembre 1973 : " Les pouvoirs publics veillent à ce que l'essor du commerce et de l'artisanat permette l'expansion de toutes les formes d'entreprises, indépendantes, groupées ou intégrées, en évitant qu'une croissance désordonnée des formes nouvelles de distribution ne provoque l'écrasement de la petite entreprise et le gaspillage des équipements commerciaux et ne soit préjudiciable à l'emploi " ; qu'aux termes de l'article L. 750-1 du code de commerce, dans sa rédaction issue de la loi du 4 août 2008 de modernisation de l'économie : " Les implantations, extensions, transferts d'activités existantes et changements de secteur d'activité d'entreprises commerciales et artisanales doivent répondre aux exigences d'aménagement du territoire, de la protection de l'environnement et de la qualité de l'urbanisme. Ils doivent en particulier contribuer au maintien des activités dans les zones rurales et de montagne ainsi qu'au rééquilibrage des agglomérations par le développement des activités en centre-ville et dans les zones de dynamisation urbaine. / Dans le cadre d'une concurrence loyale, ils doivent également contribuer à la modernisation des équipements commerciaux, à leur adaptation à l'évolution des modes de consommation et des techniques de commercialisation, au confort d'achat du consommateur et à l'amélioration des conditions de travail des salariés " ; <br/>
<br/>
              6. Considérant, d'autre part, qu'aux termes de l'article L. 752-6 du même code, issu de la même loi du 4 août 2008 : " Lorsqu'elle statue sur l'autorisation d'exploitation commerciale visée à l'article L. 752-1, la commission départementale d'aménagement commercial se prononce sur les effets du projet en matière d'aménagement du territoire, de développement durable et de protection des consommateurs. Les critères d'évaluation sont : / 1° En matière d'aménagement du territoire : / a) L'effet sur l'animation de la vie urbaine, rurale et de montagne ; / b) L'effet du projet sur les flux de transport ; / c) Les effets découlant des procédures prévues aux articles L. 303-1 du code de la construction et de l'habitation et L. 123-11 du code de l'urbanisme ; / 2° En matière de développement durable : / a) La qualité environnementale du projet ; / b) Son insertion dans les réseaux de transports collectifs. " ; <br/>
<br/>
              7. Considérant qu'il résulte de ces dispositions combinées que l'autorisation d'aménagement commercial ne peut être refusée que si, eu égard à ses effets, le projet contesté compromet la réalisation des objectifs énoncés par la loi ; qu'il appartient aux commissions d'aménagement commercial, lorsqu'elles statuent sur les dossiers de demande d'autorisation, d'apprécier la conformité du projet à ces objectifs, au vu des critères d'évaluation mentionnés à l'article L. 752-6 du code de commerce ; que, depuis l'entrée en vigueur de la loi du 4 août 2008, la densité d'équipement commercial de la zone de chalandise concernée ne figure plus au nombre de ces critères ; <br/>
<br/>
              8. Considérant, en premier lieu, que si les sociétés requérantes soutiennent que la décision attaquée a méconnu l'objectif fixé par le législateur en matière d'aménagement du territoire, il ressort des pièces du dossier que le projet contribuera à diversifier l'offre commerciale sur le territoire de la commune de Saint-Amand-Montrond sans porter atteinte à l'animation de la vie urbaine, qu'il prendra place sur une parcelle intégrée dans le tissu d'habitations et de commerces et activités situés le long de la route départementale 951, que ses effets sur les flux de circulation demeureront limités et que des accès sécurisés à la voie publique ont été prévus ; que la circonstance que le projet n'est pas desservi par des voies cyclables ou pédestres ne justifie pas, par elle-même, un refus de l'autorisation sollicitée ; qu'enfin, ainsi qu'il a été dit, la densité d'équipement commercial de la zone de chalandise n'avait pas à être prise en considération par la Commission nationale ; que, par suite, le moyen ne peut qu'être écarté ;<br/>
<br/>
              9. Considérant, en second lieu, que si les sociétés requérantes soutiennent que la décision attaquée a méconnu l'objectif fixé par le législateur en matière de développement durable, en ce qui concerne le respect de la réglementation thermique 2012, la consommation d'eau et les mesures d'économie, l'insertion dans l'environnement et l'insuffisance de la desserte par les transports collectifs, il ressort toutefois des pièces du dossier que des mesures d'économies d'énergie et de récupération des eaux pluviales seront prises, qu'une grande partie du terrain sera dédiée aux espaces verts et que le projet ne sera pas dépourvu de toute desserte par les transports collectifs ; qu'à la date de la décision prise par la Commission nationale, le pétitionnaire n'était pas tenu de s'engager à respecter la réglementation thermique 2012 ; que, par suite, le moyen ne peut qu'être écarté ; <br/>
<br/>
              10. Considérant qu'il résulte de tout ce qui précède que la requête des sociétés Les Serres de la Marmande et autres doit être rejetée, y compris les conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative ; qu'il y a lieu, en revanche, sur le fondement des mêmes dispositions, de mettre à la charge des sociétés requérantes le versement à la SCI Bianca d'Auvergne d'une somme de 1 000 euros chacune ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête présentée par les sociétés Les Serres de la Marmande, Agralys distribution et Pauge est rejetée. <br/>
Article 2 : les sociétés Les Serres de la Marmande, Agralys et Pauge verseront à la SCI Bianca d'Auvergne une somme de 1 000 euros chacune. <br/>
Article 3 : La présente décision sera notifiée à la société Les Serres de la Marmande, à la société Agralys distribution, à la société Pauge, à la société Bianca d'Auvergne et à la Commission nationale d'aménagement commercial.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
