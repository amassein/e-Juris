<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000035911888</ID>
<ANCIEN_ID>JG_L_2017_10_000000400412</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/35/91/18/CETATEXT000035911888.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème - 4ème chambres réunies, 25/10/2017, 400412, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-10-25</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>400412</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème - 4ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROCHETEAU, UZAN-SARANO</AVOCATS>
<RAPPORTEUR>Mme Catherine Bobo</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:400412.20171025</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête sommaire et un mémoire complémentaire, enregistrés les 6 juin et 5 septembre 2016 au secrétariat du contentieux du Conseil d'Etat, le Syndicat interprofessionnel des radios et télévisions indépendantes (SIRTI), le Syndicat professionnel des radiodiffuseurs généralistes privés (SRGP) et le Syndicat des réseaux radiophoniques nationaux (SRN) demandent au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret n° 2016-405 du 5 avril 2016 portant modification du cahier des charges de la société nationale de programme Radio France ;<br/>
<br/>
              2°) d'enjoindre au Premier ministre de plafonner les recettes publicitaires globales de Radio France et d'imposer la mise en place d'un système de traçabilité et de contrôle des pratiques publicitaires du groupe ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 10 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
<br/>
              - le traité sur le fonctionnement de l'Union européenne, notamment son article 102;<br/>
<br/>
              - le code de commerce ;<br/>
<br/>
              - la loi n° 86-1067 du 30 septembre 1986 ;<br/>
<br/>
              - le décret du 13 novembre 1987 portant approbation des cahiers des missions et des charges de la société Radio France et de l'Institut national de l'audiovisuel ;<br/>
<br/>
              - le décret n° 94-972 du 9 novembre 1994 ;<br/>
<br/>
              - le décret n° 2016-405 du 5 avril 2016 portant modification du cahier des charges de la société nationale de programme Radio  France;<br/>
<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Catherine Bobo, auditeur,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Rocheteau, Uzan-Sarano, avocat du SIRTI et autres.<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article 48 de la loi du 30 septembre 1986 relative à la liberté de communication : " Un cahier des charges fixé par décret définit les obligations de chacune des sociétés mentionnées à l'article 44 (...). / Les modalités de programmation des émissions publicitaires des sociétés nationales de programme sont précisées par les cahiers des charges. Ceux-ci prévoient en outre la part maximale de publicité qui peut provenir d'un même annonceur " ; que le cahier des missions et des charges de la société Radio France, approuvé par le décret du 13 novembre 1987, a été modifié par le décret du 5 avril 2016 visé ci-dessus afin d'autoriser certains services de radio de cette société, les stations France Inter, France Info et France Bleu, pour sa fréquence nationale et ses fréquences locales, à diffuser de la publicité de marque sous certaines conditions ; que les syndicats requérants demandent l'annulation pour excès de pouvoir des dispositions issues de ce décret du 5 avril 2016  ; <br/>
<br/>
              Sur la légalité externe du décret attaqué : <br/>
<br/>
              2. Considérant que les article 28, 31 et 42-3 de la loi du 30 septembre 1986 n'imposent pas au pouvoir réglementaire la réalisation d'une étude d'impact préalable à la modification par décret du cahier des charges de la société Radio France ; que les requérants ne peuvent utilement invoquer la méconnaissance des circulaires du Premier ministre du 17 février 2011 relative à la simplification des normes concernant les entreprises et les collectivités territoriales et du 17 juillet 2013 relative à la mise en oeuvre du gel de la réglementation qui prévoient une évaluation préalable de l'ensemble des projets de textes réglementaires applicables aux collectivités territoriales, aux entreprises ainsi qu'au public et qui se bornent à fixer des orientations pour l'organisation du travail gouvernemental ; que, par suite, le moyen tiré du défaut d'étude d'impact doit être écarté ;<br/>
<br/>
              3. Considérant que les requérants soutiennent que le décret attaqué a été adopté en méconnaissance du principe d'impartialité au motif qu'il a été préparé au sein de la direction générale des médias et des industries culturelles dont le directeur général est aussi membre du conseil d'administration de la société Radio-France et se trouverait ainsi en situation de conflit d'intérêts ; que, toutefois, la circonstance que l'Etat a désigné le directeur général des médias et des industries culturelles, en cette qualité, comme l'un de ses quatre représentants siégeant, en vertu de l'article 47-2 de la loi du 30 septembre 1986, au conseil d'administration de la société Radio France ne saurait conduire à regarder cet agent public comme ne présentant pas les garanties requises pour participer à l'élaboration des dispositions réglementaires fixant les obligations de cette société ;<br/>
<br/>
              4. Considérant qu'aux termes de l'article L. 462-2 du code de commerce, l'Autorité de la concurrence " est obligatoirement consultée par le Gouvernement sur tout projet de texte réglementaire instituant un régime nouveau ayant directement pour effet : / 1° De soumettre l'exercice d'une profession ou l'accès à un marché à des restrictions quantitatives (...) " ; que, contrairement à ce que soutiennent les requérants, en autorisant la société Radio France à diffuser des publicités pour des marques commerciales, le pouvoir réglementaire n'a pas instauré un nouveau régime ayant pour effet de soumettre à des restrictions quantitatives l'accès au marché de la publicité radiophonique ; que, par suite, le moyen tiré de ce que l'Autorité de la concurrence devait être consultée en vertu des dispositions précitées doit être écarté ; <br/>
<br/>
              Sur la légalité interne du décret attaqué :<br/>
<br/>
              5. Considérant, d'une part, qu'aux termes de l'article 102 du traité sur le fonctionnement de l'Union européenne : " Est incompatible avec le marché intérieur et interdit, dans la mesure où le commerce entre États membres est susceptible d'en être affecté, le fait pour une ou plusieurs entreprises d'exploiter de façon abusive une position dominante sur le marché intérieur ou dans une partie substantielle de celui-ci. / Ces pratiques abusives peuvent notamment consister à : / a) imposer de façon directe ou indirecte des prix d'achat ou de vente ou d'autres conditions de transaction non équitables, / b) limiter la production, les débouchés ou le développement technique au préjudice des consommateurs, / c) appliquer à l'égard de partenaires commerciaux des conditions inégales à des prestations équivalentes, en leur infligeant de ce fait un désavantage dans la concurrence, / d) subordonner la conclusion de contrats à l'acceptation, par les partenaires, de prestations supplémentaires qui, par leur nature ou selon les usages commerciaux, n'ont pas de lien avec l'objet de ces contrats " ; qu'aux termes de l'article L. 420-2 du code de commerce : " Est prohibée, dans les conditions prévues à l'article L. 420-1, l'exploitation abusive par une entreprise ou un groupe d'entreprises d'une position dominante sur le marché intérieur ou une partie substantielle de celui-ci. Ces abus peuvent notamment consister en refus de vente, en ventes liées ou en conditions de vente discriminatoires ainsi que dans la rupture de relations commerciales établies, au seul motif que le partenaire refuse de se soumettre à des conditions commerciales injustifiées. / Est en outre prohibée, dès lors qu'elle est susceptible d'affecter le fonctionnement ou la structure de la concurrence, l'exploitation abusive par une entreprise ou un groupe d'entreprises de l'état de dépendance économique dans lequel se trouve à son égard une entreprise cliente ou fournisseur. Ces abus peuvent notamment consister en refus de vente, en ventes liées, en pratiques discriminatoires visées au I de l'article L. 442-6 ou en accords de gamme " ; <br/>
<br/>
              6. Considérant, d'autre part, qu'aux termes de l'article 1er de la loi du 30 septembre 1986 : " La communication au public par voie électronique est libre. / L'exercice de cette liberté ne peut être limité que dans la mesure requise, (...) par les exigences de service public, ... " ; qu'en application de l'article 3-1 de la même loi, le Conseil supérieur de l'audiovisuel (CSA) doit veiller à favoriser la libre concurrence ; que le deuxième alinéa de l'article 17 de la loi l'habilite à saisir les autorités administratives ou judiciaires compétentes pour connaître des pratiques restrictives de la concurrence ; qu'aux termes de l'article 14 de la loi: " Le Conseil supérieur de l'audiovisuel exerce un contrôle, par tous moyens appropriés, sur l'objet, le contenu et les modalités de programmation des émissions publicitaires diffusées par les services de communication audiovisuelle en vertu de la présente loi. (...) " et qu'aux termes de son article 19: " Pour l'accomplissement des missions qui lui sont confiées par la présente loi, le Conseil supérieur de l'audiovisuel peut : / 1° Recueillir, sans que puissent lui être opposées d'autres limitations que celles qui résultent du libre exercice de l'activité des partis et groupements politiques mentionnés à l'article 4 de la Constitution : (...) / - auprès des administrations, des producteurs d'oeuvres audiovisuelles et cinématographiques, des personnes mentionnées à l'article 95 ainsi que des éditeurs et distributeurs de services de communication audiovisuelle, toutes les informations nécessaires pour s'assurer du respect des obligations qui sont imposées à ces derniers ; (...) / 2° Faire procéder auprès des administrations ou des éditeurs et distributeurs de services à des enquêtes (...) " ; <br/>
<br/>
              7. Considérant que les syndicats requérants font valoir que la société Radio France dispose, pour l'ensemble de ses radios, du tiers des fréquences disponibles sur l'ensemble du territoire national, qu'elle bénéficie d'un droit de réservation prioritaire des fréquences alors que les radios privées sont soumises à des appels à candidatures et qu'elle a en outre la possibilité d'obtenir l'attribution de nouvelles fréquences, auparavant mises à la disposition d'opérateurs privés, qu'enfin, elle dispose d'une puissance de diffusion plus grande sur chaque fréquence ; qu'ils font aussi valoir qu'elle est le premier groupe radiophonique, avec des parts d'audiences agrégées de ses différentes radios de plus de 23,5 %, alors que le groupe de radios privées le plus puissant atteint seulement un taux d'audiences cumulées de 18,5 % ; qu'ils soutiennent que le décret attaqué confère à la société Radio France de nouveaux avantages sur le marché de la publicité, dont le volume diminue depuis plusieurs années, alors que la publicité constitue la seule source de financement pour environ 850 radios privées et que les subventions publiques versées à Radio France représentent près de 92 % de ses ressources, lesquelles ont été en constante augmentation entre 2004 et 2014 ; <br/>
<br/>
              8. Considérant, en premier lieu, que les syndicats requérants n'apportent aucun élément de nature à établir que la société Radio France occuperait, sur l'ensemble du marché de la publicité radiophonique, une position dominante ni, en tout état de cause, que le décret attaqué la conduirait automatiquement à en abuser ; <br/>
<br/>
              9. Considérant, en deuxième lieu, que l'article 44 du cahier des missions et des charges introduit par le décret attaqué prévoit que, pour chacun des programmes, tant nationaux que locaux, de la société Radio France, le temps consacré à la diffusion de messages publicitaires ne peut excéder 17 minutes par jour en moyenne par trimestre civil, trente minutes pour un jour donné, trois minutes par jour en moyenne annuelle entre 7 heures et 9 heures, huit minutes pour un jour donné entre 7 heures et 9 heures, une minute et trente secondes pour chaque séquence de messages publicitaires entre 7 heures et 9 heures ; que, eu égard à ces limites ainsi qu'aux conditions et interdictions définies par les articles 43 et 45-1, il ne ressort pas des pièces du dossier, alors même qu'aucun plafond n'est fixé pour les ressources publicitaires de la société Radio France ni pour les parts de marché qu'elle peut détenir sur le marché global de la publicité radiophonique, que le décret attaqué serait, comme le soutiennent les syndicats requérants, de nature à mettre en péril le financement des radios privées, en portant ainsi atteinte au pluralisme des courants d'expression socioculturels et à la diversification des opérateurs, au respect desquels le CSA doit veiller, ou serait entaché d'une erreur manifeste d'appréciation ;<br/>
<br/>
              10. Considérant, en troisième lieu, que les sociétés requérantes font plus particulièrement valoir que le décret attaqué  fausserait le jeu de la concurrence sur le marché de la publicité locale en ne subordonnant pas l'accès de Radio France à ce marché à la réalisation de programmes d'intérêt local, alors que le décret du 9 novembre 1994 pris pour l'application du 1° de l'article 27 de la loi du 30 septembre 1986 relative à la liberté de communication et définissant les obligations relatives à l'accès à la publicité locale et au parrainage local des services de radiodiffusion sonore autorisés subordonne notamment l'accès des radios privées au marché publicitaire local à un minimum de 3 heures de programmes d'intérêt local et à une présence locale ; que, toutefois, il ressort des pièces du dossier que, pour mettre en oeuvre l'article 44 de la loi du 30 septembre 1986, qui lui impose de favoriser l'expression régionale sur ses antennes décentralisées sur l'ensemble du territoire, et l'article 25 de son cahier des charges, qui prévoit qu'elle conçoit et diffuse des programmes de stations locales privilégiant la proximité dans leur offre d'information, de services et de divertissements, la société Radio France assure, avec le réseau de ses radios locales, une présence locale qui va au-delà des exigences posées par le décret du 9 novembre 1994 ; qu'en outre les stations de radio France Bleu sont soumises au plafond de 17 minutes de diffusion de publicité par jour en moyenne sur un trimestre, alors que les radios locales privées peuvent consacrer jusqu'à 25 % de la durée des programmes d'intérêt local à la publicité locale, soit 45 minutes au minimum, ; que, par ailleurs, la publicité pour les opérations de promotion par le secteur de la distribution n'est pas autorisée sur les stations de la société Radio France ; qu'ainsi le moyen tiré de ce que le décret attaqué créerait par lui-même une distorsion de concurrence illégale sur le marché de la publicité locale ne peut qu'être écarté ;<br/>
<br/>
              11. Considérant, en quatrième lieu, que si l'article 8 du décret attaqué  abroge les dispositions du deuxième alinéa de l'article 45 du cahier des missions et des charges de la société Radio France, qui imposaient à cette société de soumettre à l'autorité de tutelle ses tarifs publicitaires, il résulte de la combinaison des dispositions citées au point 6 ci-dessus que le CSA demeure chargé de favoriser la libre concurrence et qu'il est en droit, à ce titre, de se faire communiquer par la société Radio France tous éléments utiles, notamment les tarifs publicitaires qu'elle pratique ; que la suppression, par l'article 2 du décret litigieux, du deuxième alinéa de l'article 32 du cahier des missions et des charges de la société Radio France, aux termes duquel " l'objet, le contenu et les modalités de programmation de ces messages sont soumis au contrôle de la Commission nationale de la communication et des libertés ", ne prive pas le CSA des pouvoirs de contrôle et d'investigation qu'il tient des dispositions précitées de la loi du 30 septembre 1986 ; qu'au demeurant, et afin de faciliter ce contrôle, le décret attaqué  insère à l'article 42 du cahier des missions et des charges un second alinéa qui impose que chaque séquence de messages publicitaires soit signalée par un indicatif sonore aisément identifiable par les auditeurs ou par une annonce d'animation appropriée ; que, dans ces conditions, les moyens tirés de ce que le décret attaqué serait illégal au motif qu'il abroge les dispositions mentionnées ci-dessus et qu'il prévoit un contrôle manifestement insuffisant du CSA doivent être écartés ;  <br/>
<br/>
              12. Considérant, enfin, que ni les dispositions de l'article 4 du décret, qui ont introduit en faveur des " échanges relatifs à des événements culturels ou sportifs " une exception à l'interdiction prévue par l'article 40 du cahier des charges de tout échange de services à caractère publicitaire, ni celles de son article 6, en ce qu'elles restreignent l'interdiction posée à l'article 43 de ce cahier de toute publicité en faveur de la distribution aux " opérations commerciales de promotion se déroulant entièrement ou principalement sur tout ou partie du territoire national ", ne méconnaissent l'objectif à valeur constitutionnelle de clarté et d'intelligibilité de la norme ; <br/>
<br/>
              13. Considérant qu'il résulte de tout ce qui précède que le syndicat interprofessionnel des radios et télévisions indépendantes, le syndicat professionnel des radiodiffuseurs généralistes privés et le syndicat des réseaux radiophoniques nationaux ne sont pas fondés à demander l'annulation du décret qu'ils attaquent ; que leurs conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative doivent, par voie de conséquence, être rejetées ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : La requête du syndicat interprofessionnel des radios et télévisions indépendantes, du syndicat professionnel des radiodiffuseurs généralistes privés et du syndicat des réseaux radiophoniques nationaux est rejetée.<br/>
Article 2 : La présente décision sera notifiée au syndicat interprofessionnel des radios et télévisions indépendantes, au syndicat professionnel des radiodiffuseurs généralistes privés, au syndicat des réseaux radiophoniques nationaux et à la ministre de la culture et de la communication.<br/>
Copie en sera adressée au Premier ministre et au CSA.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
