<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036157838</ID>
<ANCIEN_ID>JG_L_2017_12_000000411732</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/15/78/CETATEXT000036157838.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème - 2ème chambres réunies, 06/12/2017, 411732, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-12-06</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>411732</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème - 2ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Jean-Yves Ollier</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Gilles Pellissier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:411732.20171206</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, enregistrée au greffe du tribunal administratif de Versailles le 1er juin 2017, Mme B... A...a demandé au tribunal d'annuler pour excès de pouvoir l'arrêté du ministre de la défense du 26 avril 2017 fixant la liste des emplois de conseiller d'administration de la défense en tant qu'il ne fait pas figurer au nombre de ces emplois celui de chef de la division des ressources humaines du centre ministériel de gestion de Saint-Germain-en-Laye. <br/>
<br/>
              Par une ordonnance n° 1703763 du 14 juin 2017, enregistrée le 16 juin 2017 au secrétariat du contentieux du Conseil d'Etat, la présidente du tribunal administratif de Versailles a transmis cette requête au Conseil d'Etat, en application de l'article R. 351-2 du code de justice administrative.<br/>
<br/>
              Par un mémoire complémentaire et un mémoire en réplique, enregistrés les 2 octobre et 3 novembre 2017 au secrétariat du contentieux du Conseil d'Etat, Mme A...maintient ses conclusions devant le Conseil d'Etat.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la loi n° 83-634 du 13 juillet 1983 ;<br/>
              - la loi n° 84-16 du 11 janvier 1984 ;<br/>
              - le décret n° 2008-1314 du 12 décembre 2008 ;<br/>
              - le décret n° 2014-970 du 22 août 2014 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Yves Ollier, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Gilles Pellissier, rapporteur public.<br/>
<br/>
<br/>
<br/>1. Considérant, d'une part, qu'aux termes de l'article 2 du décret du 12 décembre 2008 relatif à l'emploi de conseiller d'administration de la défense : " Les fonctionnaires nommés dans l'emploi de conseiller d'administration de la défense sont chargés, au sein de l'administration centrale, dans les services à compétence nationale, dans les services déconcentrés du ministère de la défense ainsi que dans les établissements publics administratifs sous tutelle, de fonctions d'animation, de coordination, d'expertise ou de conseil comportant l'exercice de responsabilités particulièrement importantes. / Les conseillers d'administration occupant un emploi doté de l'échelon spécial (...) sont chargés d'assurer la direction de services ou d'exercer des fonctions d'animation, de coordination, de conseil ou d'expertise impliquant un haut niveau de qualification " ; qu'aux termes de l'article 3 du même décret : " Le nombre des emplois de conseiller d'administration de la défense est fixé par arrêté conjoint du ministre de la défense, du ministre chargé de la fonction publique et du ministre chargé du budget. / La liste de ces emplois est fixée par arrêté du ministre de la défense. Cette liste est révisée au moins tous les cinq ans (...) " ; qu'aux termes de son article 4 : " Peuvent être nommés dans un emploi de conseiller d'administration de la défense les fonctionnaires appartenant à un corps ou à un cadre d'emplois de catégorie A ou de niveau équivalent, dont l'indice brut terminal est au moins égal à l'indice brut 966, et justifiant d'au moins treize ans d'ancienneté dans un ou plusieurs corps, cadre d'emplois ou emplois de catégorie A ou de niveau équivalent dont quatre ans de services effectifs dans un grade d'avancement de ces corps ou cadres d'emplois " ; qu'un arrêté interministériel du 24 décembre 2010 a fixé à 89 le nombre d'emplois de conseiller d'administration de la défense, dont 64 classés dans les échelons compris entre le premier et le septième et 25 dotés de l'échelon spécial ; que la liste des emplois de conseiller d'administration de la défense a été fixée, en dernier lieu, par un arrêté du 26 avril 2017 du ministre de la défense ; que cette liste comporte un nombre d'emplois inférieur aux plafonds fixés par l'arrêté du 24 décembre 2010 ;<br/>
<br/>
              2. Considérant, d'autre part, qu'aux termes de l'article 2 du décret du 22 août 2014 relatif au statut d'emploi de conseiller technique de la défense : " Les fonctionnaires nommés dans l'emploi de conseiller technique de la défense assurent, au sein de l'administration centrale, dans les services à compétence nationale, dans les services déconcentrés du ministre de la défense ainsi que dans les établissements publics administratifs sous sa tutelle, soit la direction technique et administrative de bureaux opérant dans les domaines des travaux scientifiques, techniques ou industriels, soit des fonctions d'animation, de coordination, de conseil ou d'expertise comportant, dans ces mêmes domaines, l'exercice de responsabilités particulièrement importantes. / Les conseillers techniques de la défense occupant un emploi permettant l'accès à l'échelon spécial (...) sont chargés d'assurer la direction technique ou administrative de services particulièrement importants ou d'exercer, dans les domaines des travaux scientifiques, techniques ou industriels, des fonctions d'animation, de coordination, de conseil ou d'expertise impliquant un haut niveau de qualification " ; qu'aux termes de l'article 3 du même décret : " Le nombre des emplois de conseiller technique de la défense et le nombre des emplois permettant l'accès à l'échelon spécial mentionné à l'article 5 sont fixés par arrêté conjoint du ministre de la défense, du ministre chargé du budget et du ministre chargé de la fonction publique. / La liste et la localisation des emplois pouvant être occupés par un conseiller technique de la défense et, parmi ceux-ci, la liste et la localisation des emplois permettant l'accès à l'échelon spécial mentionné à l'article 5 sont fixées par arrêté du ministre de la défense. Ces listes sont révisées au moins tous les cinq ans (...) " ; qu'aux termes de son article 4 : " Peuvent être nommés dans un emploi de conseiller technique de la défense : / 1° Les ingénieurs divisionnaires d'études et de fabrications qui ont atteint au moins le 3e échelon de leur grade et comptent au moins quatre ans de services effectifs en qualité d'ingénieur divisionnaire ; / 2° Les autres fonctionnaires appartenant à un corps ou à un cadre d'emplois de catégorie A ou de niveau équivalent qui comptent au moins quatre ans de services effectifs dans un grade d'avancement dont l'indice brut terminal est au moins égal à l'indice brut 966 et qui ont atteint un échelon doté d'un indice au moins égal à l'indice brut 701. / Les fonctionnaires mentionnés aux 1° et 2° ci-dessus doivent en outre justifier d'au moins treize ans d'ancienneté dans un ou plusieurs corps, cadres d'emplois ou emplois de catégorie A ou de niveau équivalent " ; que ce décret fixe ainsi, en ce qui concerne le niveau des responsabilités donnant accès à ce statut d'emploi, des conditions analogues à celles prévues par le décret du 12 décembre 2008 relatif à l'emploi de conseiller d'administration de la défense ;<br/>
<br/>
              3. Considérant qu'il ressort des pièces du dossier que l'emploi de chef de la division des ressources humaines du centre ministériel de gestion de Saint-Germain-en-Laye figurait dans la liste des emplois de conseiller technique de la défense permettant l'accès à l'échelon spécial, fixée par un arrêté du ministre de la défense du 3 octobre 2014, lorsqu'il était occupé par un ingénieur divisionnaire d'études et de fabrications du ministère de la défense ; que l'accès à cet échelon spécial est réservé, en application du décret du 22 août 2014 relatif au statut d'emploi de conseiller technique de la défense, aux fonctionnaires en charge d'un service particulièrement important ou disposant d'un haut niveau de qualification ; que, par un arrêté du 5 décembre 2016, l'emploi de chef de la division de la gestion administrative et de la paye du même centre ministériel de gestion a été substitué à celui de chef de la division des ressources humaines dans la liste des emplois de conseiller technique de la défense ; qu'en revanche, l'emploi de chef de la division des ressources humaines, ainsi supprimé de la liste des emplois de conseiller technique de la défense, n'a pas été introduit dans la liste des emplois de conseiller d'administration de la défense par l'arrêté du 26 avril 2017 ; que MmeA..., attachée principale d'administration, affectée au poste de chef de la division des ressources humaines depuis le 1er décembre 2016, demande l'annulation de l'arrêté du 26 avril 2017 en tant qu'il ne comporte pas cet emploi ;<br/>
<br/>
              4. Considérant que l'emploi de chef de la division des ressources humaines du centre de gestion ministériel de Saint-Germain-en-Laye a ainsi été regardé par l'administration, jusqu'à l'intervention de l'arrêté du 5 décembre 2016, comme comportant des responsabilités particulièrement importantes ; que la ministre des armées ne justifie pas que cet emploi, qui selon la fiche de poste suppose l'animation et la coordination d'une équipe de soixante-quatre agents répartis en trois bureaux, n'impliquerait plus l'exercice de responsabilités comparables à celles qui sont confiées aux titulaires des autres emplois mentionnés dans l'arrêté du 26 avril 2017 ; qu'au demeurant, la même fiche de poste, à laquelle se réfère explicitement l'arrêté de nomination de MmeA..., présente cet emploi comme devant être occupé par un conseiller d'administration de la défense ; <br/>
<br/>
              5. Considérant qu'il résulte de ce qui précède que Mme A...est fondée à soutenir qu'en s'abstenant d'inclure l'emploi de chef de la division des ressources humaines du centre de gestion ministériel de Saint-Germain-en-Laye dans la liste des emplois de conseiller d'administration de la défense, le ministre de la défense a entaché l'arrêté du 26 avril 2017 d'une erreur manifeste d'appréciation et que celui-ci doit être, dans cette mesure, annulé ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêté du ministre de la défense du 26 avril 2017 fixant la liste des emplois de conseiller d'administration de la défense est annulé en tant qu'il n'inclut pas l'emploi de chef de la division des ressources humaines du centre ministériel de gestion de Saint-Germain-en-Laye.<br/>
Article 2 : La présente décision sera notifiée à Mme B...A...et à la ministre des armées.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
