<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037834560</ID>
<ANCIEN_ID>JG_L_2018_12_000000401813</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/83/45/CETATEXT000037834560.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème - 8ème chambres réunies, 19/12/2018, 401813</TITRE>
<DATE_DEC>2018-12-19</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>401813</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème - 8ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>BALAT ; SCP ROCHETEAU, UZAN-SARANO</AVOCATS>
<RAPPORTEUR>M. Géraud Sajust de Bergues</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emmanuelle Cortot-Boucher</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:401813.20181219</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Procédures contentieuses antérieures :<br/>
<br/>
              1° M. B...A...a demandé au tribunal administratif de Toulon, d'une part, d'annuler les décisions du président de la communauté d'agglomération Toulon Provence Méditerranée des 3 août et 4 octobre 2010 lui refusant un poste à temps complet et, d'autre part, d'enjoindre sous astreinte à la communauté d'agglomération de prendre un arrêté lui accordant un temps complet, soit seize heures de cours dans la discipline d'écriture musicale qu'il exercerait sur le site de Toulon, dans le délai d'un mois à compter de la notification du jugement, sous astreinte de 100 euros par jour de retard. <br/>
<br/>
              Par un jugement n° 1003041 du 6 janvier 2012, le tribunal administratif de Toulon a annulé ces décisions. <br/>
<br/>
              Par une décision n° 357484 du 26 mars 2014, rendue sur le pourvoi de M. A..., le Conseil d'Etat, statuant au contentieux, a annulé ce jugement et renvoyé l'affaire devant le tribunal administratif de Toulon. <br/>
<br/>
              Par un jugement n° 1003041 du 17 juillet 2014, le tribunal administratif de Toulon a rejeté la demande de M.A....<br/>
<br/>
              2° M. A...a demandé au tribunal administratif de Toulon, d'une part, d'annuler la décision de la communauté d'agglomération Toulon Provence Méditerranée du 26 mars 2012 rejetant sa demande d'attribution d'un temps plein ainsi que la décision de la communauté d'agglomération du 25 juillet 2012 rejetant son recours gracieux dirigé contre cette décision et, d'autre part, de condamner la communauté d'agglomération Toulon Provence Méditerranée à lui verser les sommes de 7 200 euros, à parfaire, au titre d'une perte de revenus et de 5 000 euros au titre du préjudice moral, augmentées des intérêts légaux et de la capitalisation de ces intérêts. <br/>
<br/>
              Par un jugement n° 1202510 du 5 décembre 2014, le tribunal administratif de Toulon a rejeté cette demande.<br/>
<br/>
              Par un arrêt n° 14MA03682, 15MA00505 du 24 mai 2016, la cour administrative d'appel de Marseille, après avoir joint les appels formés contre les jugements des 17 juillet et 5 décembre 2014, a annulé ces jugements ainsi que les décisions de la communauté d'agglomération Toulon Provence Méditerranée des 3 août et 4 octobre 2010 ainsi que des 26 mars et 25 juillet 2012, a enjoint à la communauté d'agglomération Toulon Provence Méditerranée de placer M. A...à temps complet dans le délai d'un mois à compter de la notification de cet arrêt, l'a condamnée à verser à celui-ci la somme de 20 000 euros et a rejeté le surplus des conclusions de la requête de M. A...dirigée contre le jugement du 5 décembre 2014.<br/>
<br/>
              Procédure devant le Conseil d'Etat :<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 25 juillet et 25 octobre 2016 au secrétariat du contentieux du Conseil d'Etat, la communauté d'agglomération Toulon Provence Méditerranée demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt en tant qu'il lui fait grief ; <br/>
<br/>
              2°) pour le cas où il déciderait de régler l'affaire au fond, de rejeter les requêtes présentées par M.A... devant la cour administrative d'appel de Marseille ; <br/>
<br/>
              3°) de mettre à la charge de M. A...la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la loi n° 84-53 du 26 janvier 1984 ;<br/>
              - le décret n° 91-298 du 20 mars 1991 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Géraud Sajust de Bergues, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Emmanuelle Cortot-Boucher, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Balat, avocat de la communauté d'agglomération Toulon Provence Méditerranée et à la SCP Rocheteau, Uzan-Sarano, avocat de M. A...;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond qu'après avoir été employé depuis 1993 en qualité d'agent contractuel à temps non complet, M. A... a été titularisé, par arrêté du président de la communauté d'agglomération Toulon Provence Méditerranée en date du 18 mai 2009, dans le cadre d'emploi des professeurs territoriaux d'enseignement artistique. Après sa titularisation, il a d'abord continué à assurer un service d'une durée hebdomadaire de dix heures. Cette durée a ensuite été portée à douze heures et enfin à treize heures. M. A...a demandé à bénéficier d'un poste à temps complet, soit seize heures de services hebdomadaires, mais ses demandes ont été rejetées par des décisions de son employeur en date des 3 août 2010, 4 octobre 2010 et 26 mars 2012. Sa demande tendant à l'octroi d'une indemnisation à hauteur de 11 000 euros au titre du préjudice moral et de la perte de salaire résultant du refus d'un poste à temps complet a été rejetée par une décision du 25 juillet 2012.<br/>
<br/>
              2. Par un jugement du 6 janvier 2012, le tribunal administratif de Toulon a annulé les décisions des 3 août et 4 octobre 2010. Sur pourvoi de la communauté d'agglomération Toulon Provence Méditerranée, le Conseil d'Etat, statuant au contentieux, a, par une décision du 26 mars 2014, annulé ce jugement et renvoyé l'affaire au tribunal administratif. Par un jugement du 17 juillet 2014, le tribunal administratif de Toulon a rejeté comme irrecevable la demande de M.A.... Le même tribunal administratif, par un jugement du 5 décembre 2014, a, par ailleurs, rejeté au fond une seconde demande de M.A..., présentée à la suite du jugement du tribunal administratif du 6 janvier 2012 tendant à l'annulation des décisions des 26 mars et 25 juillet 2012 et à la condamnation de la communauté d'agglomération Toulon Provence Méditerranée à lui verser des dommages-intérêts. La communauté d'agglomération se pourvoit en cassation contre l'arrêt du 24 mai 2016 en tant que, sur appel de M.A..., la cour administrative d'appel de Marseille, par l'article 1er, après avoir annulé le jugement du 17 juillet 2014, a annulé les décisions des 3 août et 4 octobre 2010, puis par voie de conséquence, par son article 2, a enjoint à la communauté d'agglomération Toulon Provence Méditerranée de placer M. A...à temps complet dans un délai d'un mois à compter de la notification de l'arrêt, par son article 3, a annulé le jugement du 5 décembre 2014 ainsi que les décisions des 26 mars et 25 juillet 2012, puis par son article 4, a condamné la communauté d'agglomération Toulon Provence Méditerranée à verser à M. A...la somme de 20 000 euros, et enfin par ses articles 6 et 7, s'est prononcée sur les demandes présentées au titre de l'article L.761-1 du code de justice administrative.<br/>
<br/>
              3. Aux termes de l'article 60 de la loi du 26 janvier 1984 portant dispositions statutaires relatives à la fonction publique territoriale : " Les fonctionnaires à temps complet, en activité ou en service détaché, qui occupent un emploi conduisant à pension du régime de la Caisse nationale de retraite des agents des collectivités locales ou du régime général de la sécurité sociale peuvent, sur leur demande, sous réserve des nécessités de la continuité et du fonctionnement du service et compte tenu des possibilités d'aménagement de l'organisation du travail, être autorisés à accomplir un service à temps partiel qui ne peut être inférieur au mi-temps. / (...) / A l'issue de la période de travail à temps partiel, les fonctionnaires sont admis de plein droit à occuper à temps plein leur emploi ou, à défaut, un autre emploi correspondant à leur grade. / (...) ". <br/>
<br/>
              4. La loi du 26 janvier 1984 comporte également un chapitre XII intitulé " Dispositions applicables aux fonctionnaires territoriaux nommés dans des emplois permanents à temps non complet ", qui comprend les articles 104 à 108. Aux termes de l'article 104 de cette loi, dans sa rédaction résultant de la loi du 27 décembre 1994 : " Les dispositions de la présente loi sont applicables aux fonctionnaires nommés dans des emplois permanents à temps non complet, sous réserve des dérogations prévues par décret en Conseil d'Etat rendues nécessaires par la nature de ces emplois. / (...) ". Aux termes de l'article 108 de la même loi : " Les fonctionnaires nommés dans des emplois permanents à temps non complet qui sont employés par une ou plusieurs collectivités ou établissements pendant une durée supérieure ou égale à la moitié de la durée légale du travail des fonctionnaires territoriaux à temps complet sont intégrés dans les cadres d'emplois. Un décret en Conseil d'Etat précise les conditions d'intégration de ces fonctionnaires dans la fonction publique territoriale ". Aux termes, enfin, de l'article 5-1 du décret du 20 mars 1991 portant dispositions statutaires applicables aux fonctionnaires territoriaux nommés dans des emplois permanents à temps non complet, dans sa version résultant du décret du 4 août 1993 : " Les communes, départements, syndicats intercommunaux, districts, syndicats et communautés d'agglomération nouvelles, communautés de communes et communautés de villes peuvent nonobstant les dispositions de l'article 4 du présent décret, créer des emplois à temps non complet pour l'exercice des fonctions relevant des cadres d'emplois suivants : professeurs d'enseignement artistique (...) ".<br/>
<br/>
              5. La communauté d'agglomération Toulon Provence Méditerranée est fondée à soutenir que la cour administrative d'appel de Marseille a commis une erreur de droit en retenant que M.A..., qui avait été recruté sur emploi à temps non complet, bénéficiait d'un droit à exercer ses fonctions à temps complet au titre de l'article 60 de la loi du 26 janvier 1984, alors que cet article est relatif à la possibilité, pour les fonctionnaires à temps complet, d'occuper leur emploi à temps partiel et de retrouver, à l'issue de cette période, leur emploi à temps plein.<br/>
<br/>
              6. Il résulte de ce qui précède, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, que la communauté d'agglomération Toulon Provence Méditerranée est fondée à demander l'annulation de l'article 1er de l'arrêt qu'elle attaque en tant qu'il a annulé les décisions des 3 août et 4 octobre 2010, ainsi que, par voie de conséquence, l'annulation des articles 2, 3, 4, 6 et 7 de cet arrêt. <br/>
<br/>
              7. Aux termes du second alinéa de l'article L. 821-2 du code de justice administrative : " Lorsque l'affaire fait l'objet d'un second pourvoi en cassation, le Conseil d'Etat statue définitivement sur cette affaire ". Le Conseil d'Etat étant saisi, s'agissant des décisions des 3 août et 4 octobre 2010, d'un second pourvoi en cassation, il lui incombe, compte tenu de l'annulation non contestée du jugement du tribunal administratif de Toulon du 17 juillet 2014, de statuer, par la voie de l'évocation, sur la demande présentée, à ce titre, par M. A... devant ce tribunal.<br/>
<br/>
              Sur les fins de non-recevoir opposées à la demande de M.A... :<br/>
<br/>
              8. Il ressort des pièces du dossier qu'outre la fin de non-recevoir dont le rejet par la cour administrative d'appel de Marseille n'a pas été contesté et qui était tiré de ce que la décision du 3 août 2010 rejetant la demande de M. A...d'exercer ses fonctions à temps complet aurait été retirée par une décision du 8 septembre 2010 portant sa durée hebdomadaire de service à 13 heures, il est soutenu en défense, d'une part, que la demande de M. A...était tardive faute pour celui-ci d'avoir attaqué, dans les délais, la décision du 8 septembre 2010 et, d'autre part, que la décision du 3 août 2010 ne faisait pas grief. Il résulte toutefois des motifs devenus définitifs de l'arrêt du 24 mai 2016 que la décision du 8 septembre 2010 n'a nullement retiré la décision du 3 août 2010 et est confirmative de celle-ci en tant qu'elle a refusé à M. A...d'exercer ses fonctions à temps complet. Par suite, M. A...avait intérêt à agir contre la décision du 3 août 2010 et, compte tenu du recours gracieux qu'il a formé contre cette décision et qui a été rejeté par la décision du 4 octobre 2010, le délai de recours de deux mois dont il disposait pour contester ces décisions n'était pas expiré lorsqu'il a saisi, le 1er décembre 2010, le tribunal administratif de Toulon.<br/>
<br/>
              Sur la légalité des décisions contestées :<br/>
<br/>
              9. Aux termes du troisième alinéa de l'article 3 de la loi du 26 janvier 1984, dans sa rédaction alors en vigueur : " Par dérogation au principe énoncé à l'article 3 du titre Ier du statut général, des emplois permanents peuvent être occupés de manière permanente par des agents contractuels dans les cas suivants : / 1° Lorsqu'il n'existe pas de cadre d'emplois de fonctionnaires susceptibles d'assurer les fonctions correspondantes ; 2° Pour les emplois du niveau de la catégorie A, lorsque les besoins des services ou la nature des fonctions le justifient / (...) ". <br/>
<br/>
              10. Ces dispositions sont applicables aux emplois permanents de professeur d'enseignement artistique, sans qu'aucune dérogation n'ait été prévue sur le fondement des dispositions citées au point 4 de l'article 104 de la loi du 26 janvier 1984. Il en résulte que lorsque des fonctionnaires de catégorie A ont été nommés dans de tels emplois à temps non complet, leur employeur ne peut, pour assurer des heures d'enseignement auxquelles ces fonctionnaires se sont portés candidats afin d'exercer leurs fonctions à temps complet, recruter un agent contractuel, que si les besoins du service ou la nature des fonctions en cause le justifient.<br/>
<br/>
              11. Il ressort des pièces du dossier que la communauté d'agglomération Toulon Provence Méditerranée a procédé, en vue de l'année scolaire 2010-2011, au recrutement d'un agent contractuel sur un emploi à temps non complet, d'une durée de huit heures par semaine, de professeur d'enseignement artistique dans la discipline de l'écriture musicale. La communauté d'agglomération soutient, sans que cela soit établi, que M.A..., même s'il enseignait déjà dans cette discipline, s'il avait déposé sa candidature sur ce poste et s'il avait demandé à exercer ses fonctions à temps complet, n'aurait pas été en mesure, compte tenu du lieu et des horaires des cours envisagés, d'assurer cet enseignement à hauteur des trois heures par semaine qui lui manquaient pour être à temps complet. Il n'est pas davantage établi par les pièces du dossier que le recrutement d'un agent contractuel se justifiait par la nécessité de répondre à des attentes différentes des élèves du conservatoire. Par suite, et sans qu'il soit besoin de se prononcer sur les autres moyens de la requête, M. A...est fondé à demander l'annulation des décisions de la communauté d'agglomération Toulon Provence Méditerranée des 3 août 2010 et 4 octobre 2010 lui refusant l'exercice d'un emploi à temps complet.<br/>
<br/>
              Sur les conclusions aux fins d'injonction :<br/>
<br/>
              12. Aux termes de l'article L. 911-1 du code de justice administrative : " Lorsque sa décision implique nécessairement qu'une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public prenne une mesure d'exécution dans un sens déterminé, la juridiction, saisie de conclusions en ce sens, prescrit, par la même décision, cette mesure assortie, le cas échéant, d'un délai d'exécution ".<br/>
<br/>
              13. Si les motifs de l'annulation des décisions attaquées impliquent nécessairement que M. A...ait pu exercer ses fonctions à temps complet depuis le mois de septembre 2010, ils n'impliquent pas que M. A...enseigne, ainsi qu'il le demande, une matière déterminée en un lieu déterminé. Ainsi, et dès lors que M. A...était le seul professeur titulaire d'enseignement artistique dans la discipline écriture musicale ayant demandé à exercer ses fonctions à temps complet et que la communauté d'agglomération Toulon Provence Méditerranée ne fait état d'aucun changement dans les circonstances de droit et de fait concernant la situation de l'intéressé, il y a lieu d'enjoindre à celle-ci de prononcer, dans le délai d'un mois à compter de la notification de la présente décision, la titularisation, à compter du mois de septembre 2010, de M. A...à temps complet.<br/>
<br/>
              Sur les conclusions tendant à l'application de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              14. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à la charge de M.A..., qui n'est pas, dans la présente instance, la partie perdante. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la communauté d'agglomération Toulon Provence Méditerranée la somme de 3 000 euros à verser à M. A...au titre de ces dispositions.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : L'article 1er, en tant qu'il a annulé les décisions de la communauté d'agglomération Toulon Provence Méditerranée du 3 août 2010 et du 4 octobre 2010, ainsi que les articles 2, 3, 4, 6 et 7 de l'arrêt du 24 mai 2016 de la cour administrative d'appel de Marseille sont annulés. <br/>
<br/>
Article 2 : Les décisions de la communauté d'agglomération Toulon Provence Méditerranée du 3 août et du 4 octobre 2010 sont annulées.<br/>
<br/>
Article 3 : Il est enjoint à la communauté d'agglomération Toulon Provence Méditerranée de prononcer, dans le délai d'un mois à compter de la notification de la présente décision, la titularisation, à compter du mois de septembre 2010, de M. A...à temps complet.<br/>
<br/>
Article 4 : La communauté d'agglomération Toulon Provence Méditerranée versera à M. A...la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 5 : Le surplus des conclusions de la demande présentée devant le tribunal administratif de Toulon et enregistrée sous le n° 1003041 est rejeté.<br/>
<br/>
Article 6 : Les conclusions de la requête d'appel formée par M. A...contre le jugement du tribunal administratif de Toulon du 5 décembre 2014 sont renvoyées à la cour administrative d'appel de Marseille.<br/>
<br/>
Article 7 : La présente décision sera notifiée à la communauté d'agglomération Toulon Provence Méditerranée et à M. B...A.... <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
