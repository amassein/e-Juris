<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043482328</ID>
<ANCIEN_ID>JG_L_2021_05_000000451743</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/48/23/CETATEXT000043482328.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, formation collégiale, 03/05/2021, 451743, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-05-03</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>451743</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés, formation collégiale</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BUK LAMENT - ROBILLOT</AVOCATS>
<RAPPORTEUR>M. Nicolas Boulouis</RAPPORTEUR>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2021:451743.20210503</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés les 16 et 1er mai 2021 au secrétariat du contentieux du Conseil d'Etat, l'association " Génération identitaire ", M. A... B... et Mme D... C... demandent au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative : <br/>
<br/>
              1°) d'ordonner la suspension de l'exécution du décret du Président de la République du 3 mars 2021 portant dissolution de l'association dénommée " Génération identitaire " ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 4 000 euros à verser à chacun des requérants au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Les requérants soutiennent que : <br/>
              - leur requête est recevable ; <br/>
              - la condition d'urgence est satisfaite dès lors que le décret contesté, qui prononce la dissolution de l'association Génération identitaire, porte nécessairement atteinte à la liberté d'association et crée, pour l'association et ses membres, une situation d'urgence qu'aucune circonstance particulière n'est de nature à remettre en cause ; <br/>
              - il existe un doute sérieux quant à la légalité du décret contesté ; <br/>
              - il est entaché de vice de procédure dès lors que l'association n'a pas disposé d'un délai suffisant pour présenter ses observations complètes sur la dissolution projetée, en méconnaissance du principe du caractère contradictoire de la procédure garanti par les articles L. 121-1, L. 122-1 et L. 122-2 du code des relations entre le public et l'administration ; <br/>
              - en retenant, au sens des 2° et 6° de l'article L. 212-1 du code de la sécurité intérieure, que l'association Génération identitaire, d'une part, incite à la haine raciale ou religieuse et, d'autre part, présente le caractère d'une milice privée, le décret contesté est entaché d'inexactitudes matérielles et d'erreurs de qualification juridique et d'appréciation ; <br/>
              - les faits reprochés à l'association dont la dissolution a été prononcée par le décret contesté, soit sont inexacts, soit ne constituent pas une provocation à la haine ou à la violence au sens du 6° de l'article L. 212-1 du code de la sécurité intérieure, dès lors que, en premier lieu, certains de ces faits ne sont pas contemporains, ont fait l'objet de relaxes sur le plan pénal ou leur invocation n'est pas assortie de précisions suffisantes permettant d'y répondre utilement, en deuxième lieu, l'association ne fait que relayer des données vérifiables, en troisième lieu, les propos et slogans des représentants et membres de l'association sont, par leurs contenus, proches des prises de position de certains médias et personnalités publiques, qui, pour autant, ne font pas l'objet de poursuites, en quatrième lieu, les propos reprochés à la porte-parole de l'association relèvent de sa liberté d'expression, en cinquième lieu, l'association ne peut être tenue pour responsable, conformément à l'article L. 212-1-1 du code de la sécurité intérieure, des propos et actes délictuels ou criminels commis par des personnes non-membres qui se seraient procuré du matériel marqué de son logo, ou qui lui auraient versé des dons, alors que, en outre, elle a publiquement condamné ces comportements et, en dernier lieu, le motif tiré de l'existence de liens entre l'association et des groupuscules extrémistes ou des individus appelant à la discrimination ou à la violence n'est pas étayé par des précisions probantes et est, partant, inopérant ; <br/>
              - l'association ne constitue pas un groupe de combat ou une milice privée au sens du 2° de l'article L. 212-1 du code de la sécurité intérieure et le décret contesté révèle un amalgame et une extrapolation de la part du gouvernement dès lors que, en premier lieu, l'association, eu égard à son objet, agit principalement comme un lanceur d'alerte par des opérations de communication symboliques, et non comme un groupe de combat, en deuxième lieu, elle n'est pas organisée selon une hiérarchie caractérisée par le culte d'un chef, l'attribution de grades en son sein ou la mise en place d'une discipline de type militaire, elle n'a pas de devise, son recrutement ne se fait pas sur des critères de force physique et les femmes n'en sont pas exclues, en troisième lieu, l'association ne détient ni n'utilise d'armes, en quatrième lieu, les vêtements distribués aux membres de l'association, en particulier une doudoune bleue, servent des considérations purement esthétiques d'identification, d'appartenance et de revendication politique communes aux organisations militantes activistes et ne peuvent être assimilés à des uniformes militaires, en cinquième lieu, l'association ne développe pas, par ses divers imprimés, slogans et chants traditionnels ou volontairement anachroniques ou politiques, ni par ses entrainements de boxe et d'auto-défense, une stratégie martiale ou paramilitaire, en sixième lieu, les opérations menées sur le terrain, notamment en Méditerranée en 2017, avaient un but médiatique, se sont déroulées sans violence, menace ou dégradation, ni organisation de défilés, et, en dernier lieu, l'emploi de drones ou d'hélicoptères lors de ces opérations n'est pas interdit et n'est pas caractéristique d'un groupe de combat ; <br/>
              - le décret contesté est entaché de disproportion et méconnaît les articles 10 et 11 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, qui garantissent la liberté d'expression ainsi que la liberté de réunion et d'association, d'une part, eu égard à la réalité du trouble à l'ordre public allégué et, d'autre part, dès lors que le prononcé de mesures moins sévères que la dissolution de l'association est possible ; <br/>
              - les dispositions de l'article L. 212-1 du code de la sécurité intérieure portent atteinte aux droits et libertés que la Constitution garantit et leur abrogation par le Conseil constitutionnel, dans le cadre d'une instance en question prioritaire de constitutionnalité, entachera le décret contesté d'un défaut de base légale. <br/>
<br/>
              Par un mémoire distinct, enregistré le 20 avril 2021, l'association Génération identitaire, M. B... et Mme C... demandent au Conseil d'Etat, en application de l'article 23-5 de l'ordonnance n° 58-1067 du 7 novembre 1958 et à l'appui de leur requête, de renvoyer au Conseil constitutionnel la question de conformité aux droits et libertés garantis par la Constitution des dispositions de l'article L. 212-1 du code de la sécurité intérieure. Les requérants soutiennent que ces dispositions sont applicables au litige, qu'elles n'ont jamais été déclarées conformes à la Constitution et que la question de leur conformité à l'article 6 de la Déclaration des droits de l'homme et du citoyen ainsi qu'à l'article 34 de la Constitution et à la liberté d'association présente un caractère sérieux. <br/>
<br/>
              Par deux mémoires en défense, enregistrés les 26 avril et 3 mai 2021, le ministre de l'intérieur conclut au rejet de la requête. Il soutient que la condition d'urgence n'est pas satisfaite et qu'aucun des moyens soulevés n'est de nature à faire naître un doute sérieux quant à la légalité de la décision contestée. <br/>
<br/>
              Par des observations en défense, enregistrées le 26 avril 2021, le ministre de l'intérieur fait valoir qu'il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée.<br/>
<br/>
              La requête a été communiquée au Président de la République et au Premier ministre qui n'ont pas produit d'observations. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la Constitution, notamment son Préambule ; <br/>
              - l'ordonnance n° 58-1067 du 7 novembre 1958 ; <br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ; <br/>
              - le code des relations entre le public et l'administration ; <br/>
              - le code de la sécurité intérieure ; <br/>
              - le code de justice administrative ; <br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, l'association Génération identitaire, M. B... et Mme C... et, d'autre part, le Président de la République, le Premier ministre et le ministre de l'intérieur ;<br/>
<br/>
              Ont été entendus lors de l'audience publique du 30 avril 2021, à 11 heures :<br/>
<br/>
              -  Me Robillot, avocat au Conseil d'Etat et à la Cour de cassation, avocat de l'association Génération identitaire ; <br/>
              - <br/>
              - le représentant de l'association Génération identitaire ; <br/>
<br/>
              -  Mme D... C... ;<br/>
<br/>
              - les représentants du ministre de l'intérieur ; <br/>
<br/>
              à l'issue de laquelle le juge des référés a prolongé l'instruction jusqu'au 3 mai à 12 heures.<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              Sur le cadre juridique applicable au litige : <br/>
<br/>
              1. Aux termes de l'article L. 212-1 du code de la sécurité intérieure " Sont dissous, par décret en conseil des ministres, toutes les associations ou groupements de fait (...) 2°)  (...) qui présentent, par leur forme et leur organisation militaires, le caractère de groupes de combat ou de milices privées 6° (...) qui, soit provoquent à la discrimination, à la haine ou à la violence envers une personne ou un groupe de personnes à raison de leur origine ou de leur appartenance ou de leur non-appartenance à une ethnie, une nation, une race ou une religion déterminée, soit propagent des idées ou théories tendant à justifier ou encourager cette discrimination, cette haine ou cette violence (...) ".<br/>
<br/>
              2. Sur le fondement des 2° et 6° de cet article, a été pris, le 3 mars 2021, un décret portant dissolution de l'association " Génération identitaire ", de l'exécution duquel cette association demande la suspension. <br/>
<br/>
              Sur la question prioritaire de constitutionnalité dirigée contre l'article L. 212-1 du code de la sécurité intérieure : <br/>
<br/>
              3. Aux termes du premier alinéa de l'article 23-5 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel : " Le moyen tiré de ce qu'une disposition législative porte atteinte aux droits et libertés garantis par la Constitution peut être soulevé (...) à l'occasion d'une instance devant le Conseil d'Etat (...) ". Il résulte des dispositions de ce même article que le Conseil constitutionnel est saisi de la question prioritaire de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux.<br/>
<br/>
              4. La requérante soutient, en premier lieu, que les dispositions du 6° de l'article L. 212-1 du code de la sécurité intérieure méconnaissent le principe d'égalité dès lors que les articles L.332-18 du code du sport pour les associations sportives et l'article L. 227-1 du code de sécurité intérieure pour les lieux de culte prévoient des mesures de police graduées, de fermeture ou de suspension provisoire, pour les mêmes faits de provocation à la discrimination, à la haine ou à la violence. Toutefois, d'une part, un tel grief ne peut être utilement soulevé en ce qui concerne la fermeture des lieux de culte eu égard à l'objet des dispositions législatives et à la différence de situation existant entre les associations et ces lieux de culte. D'autre part, la seule circonstance que le législateur ait prévu, par l'article L. 332-18 du code du sport, la possibilité de dissoudre mais aussi de suspendre pour une durée maximale de douze mois toute association ou groupement de fait ayant pour objet le soutien à une association sportive " dont des membres ont commis en réunion, en relation ou à l'occasion d'une manifestation sportive, des actes répétés ou un acte d'une particulière gravité et qui sont constitutifs de dégradations de biens, de violence sur des personnes ou d'incitation à la haine ou à la discrimination (...) " pour des faits qui, au demeurant, ne sont pas identiques à ceux auxquels se réfère l'article litigieux du code de la sécurité intérieure et eu égard à l'objet des dispositions législatives, ne saurait établir l'existence d'une méconnaissance du principe d'égalité.<br/>
<br/>
              5. Il est également soutenu que ces dispositions seraient entachées d'incompétence négative faute de définir de façon suffisamment précise les conditions et critères, nécessairement stricts, de dissolution d'une association, dans une mesure affectant la liberté d'association. Toutefois, alors que la décision de dissolution doit répondre, sous le contrôle du juge de l'excès de pouvoir, qui en contrôle l'adaptation, la nécessité et la proportionnalité, à la nécessité de sauvegarder l'ordre public, compte tenu de la gravité des troubles qui sont susceptibles de lui être portés par les associations et groupements visés par ces dispositions, le législateur a pu sans méconnaître sa propre compétence ne pas définir plus précisément les faits, mentionnés au 2° et 6° de l'article L. 212-1 du code de la sécurité intérieure, susceptibles de justifier une telle mesure, ni, s'agissant d'une mesure de police, la subordonner au constat et à la sanction préalables par le juge pénal d'infractions correspondant à ces faits.<br/>
<br/>
              6. Par suite, sans qu'il soit besoin de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité invoquée, le moyen tiré de ce que l'article L. 212-1 du code de la sécurité intérieure porterait atteinte aux droits et libertés que la Constitution garantit doit être écarté.<br/>
<br/>
              Sur le litige en référé : <br/>
<br/>
              7. Il ressort de la motivation du décret litigieux que, pour prononcer la dissolution, l'auteur du décret s'est fondé, sur le fait que l'association promouvait une idéologie provoquant à la haine, à la violence et à la discrimination des individus à raison de leur origine, de leur race ou de leur religion, et sur ce qu'elle employait dans sa communication comme dans son organisation, une symbolique et une rhétorique martiales, l'identifiant implicitement ou explicitement à une formation paramilitaire.<br/>
<br/>
              8. Aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision ".<br/>
<br/>
              9. En premier lieu, il résulte de l'instruction qu'informé du projet de dissolution le 12 février 2021, le président de l'association a produit des observations écrites le 21 février, préalablement à la mesure intervenue le 3 mars. La circonstance que certains faits venant à l'appui des motifs du projet de décision n'aient pu être contestés que brièvement n'est pas de nature à faire regarder le délai dont l'association a disposé pour présenter des observations comme insuffisant. N'est par suite pas de nature à créer un doute sérieux sur la légalité de la mesure la méconnaissance du principe des droits de la défense.<br/>
<br/>
              10. En deuxième lieu, il résulte de l'instruction que l'association " Génération identitaire ", créée en 2012, a pour objet " la défense et la promotion des identités locales, régionales, française et européenne (...) ". Cette association, sous couvert de contribuer, selon ses dires, au débat public sur l'immigration et de lutte contre le terrorisme islamiste, propage, depuis plusieurs années, des idées, par ses dirigeants, ses structures locales et ses militants tendant à justifier ou encourager la discrimination, la haine ou la violence envers les étrangers et la religion musulmane. Il en va ainsi notamment des slogans, messages ou prises de position tels que " Immigration, Racaille, Islamisation-Reconquête - Génération identitaire " ou encore des termes du " Pacte " que les sympathisants sont invités à signer. L'association organise en outre des évènements créant ou entretenant des sentiments xénophobes ou racistes comme l'occupation du toit de la Caisse d'allocations familiales de Bobigny en mars 2019, sur laquelle a été déployée une banderole portant le slogan " de l'argent pour les Français pas pour les étrangers " et incite régulièrement à l'occasion de faits divers à la violence en désignant les étrangers à la vindicte. Certains des agissements de militants de l'association ou d'individus qui s'en réclament ont au demeurant fait l'objet de poursuites ou de condamnations pénales alors que, contrairement à ce qui est soutenu, l'association ne s'en est pas systématiquement désolidarisée, ni n'a pris les mesures qu'imposaient ces agissements et qui peuvent dès lors lui être imputés.<br/>
<br/>
              11. Il résulte également de l'instruction que l'association souhaite " entrer en guerre ", utilise une imagerie et une rhétorique guerrières, organise des camps d'été au cours desquels des exercices de combat sont proposés et des actions imitant l'action des forces de l'ordre et leurs uniformes en mettant en oeuvre des moyens similaires en vue de faire constater leurs prétendues défaillances et d'apparaître comme un recours.<br/>
<br/>
              12. Par suite, en l'état de l'instruction, les moyens tirés de ce que la dissolution de l'association reposerait sur des faits matériellement inexacts et des erreurs de qualification juridique ne sont pas de nature à créer un doute sérieux sur sa légalité.  <br/>
<br/>
              13. Il en va de même, en dernier lieu, de ceux tirés de la disproportion de la mesure et de la méconnaissance des stipulations des articles 10 et 11 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, eu égard au nombre et à la gravité des faits relevés et à la gravité des dangers pour l'ordre public et la sécurité publique résultant des activités de l'association.<br/>
<br/>
              14. Il résulte de ce qui précède que, sans qu'il soit besoin de se prononcer sur l'urgence, les conclusions de la requête tendant à la suspension du décret litigieux doivent être rejetées. Il en va de même, par voie de conséquence, des conclusions présentées au titre de l'article L. 761-1 du code de justice administrative.  <br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
              ------------------<br/>
<br/>
Article 1er : La requête de l'association Génération identitaire, M. B... et Mme C... est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à l'association Génération identitaire, M. A... B... et Mme D... C..., ainsi qu'au ministre de l'intérieur. <br/>
Copie en sera adressée au Président de la République, au Premier ministre et au Conseil constitutionnel. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
