<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042885993</ID>
<ANCIEN_ID>JG_L_2020_12_000000438322</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/88/59/CETATEXT000042885993.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème chambre, 31/12/2020, 438322, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-12-31</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>438322</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>BOUTHORS ; SCP HEMERY, THOMAS-RAQUIN, LE GUERER</AVOCATS>
<RAPPORTEUR>M. Jean-Dominique Langlais</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Cécile Barrois de Sarigny</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2020:438322.20201231</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une décision n° 2018-004 du 5 juillet 2019, la chambre disciplinaire de première instance du conseil régional des Hauts-de-France de l'ordre des masseurs-kinésithérapeutes a prononcé contre Mme B... A... la sanction de l'avertissement.<br/>
<br/>
              Par une ordonnance n° 035-2019 du 6 décembre 2019, le président de la chambre disciplinaire national de l'ordre des masseurs-kinésithérapeutes a rejeté l'appel formé contre cette décision par Mme A....<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un nouveau mémoire, enregistrés les 6 février, 15 mai et 16 décembre 2020 au secrétariat du contentieux du Conseil d'Etat, Mme A... demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) de renvoyer l'affaire à la chambre disciplinaire nationale de l'ordre des masseurs-kinésithérapeutes ;<br/>
<br/>
              3°) de lui allouer la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de la santé publique ;<br/>
              - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Dominique Langlais, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Cécile Barrois de Sarigny, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à Me Bouthors, avocat de Mme A... et à la SCP Hémery, Thomas-Raquin, Le Guerer, avocat du Conseil national de l'ordre des masseurs-kinésithérapeutes.<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. Aux termes de l'article R. 4126-5 du code de la santé publique : " Dans toutes les instances, le président de la chambre disciplinaire de première instance et le président de la chambre disciplinaire nationale peuvent, par ordonnance motivée, sans instruction préalable : (...) 4° Rejeter les plaintes ou les requêtes manifestement irrecevables, lorsque la juridiction n'est pas tenue d'inviter leur auteur à les régulariser ou qu'elles n'ont pas été régularisées à l'expiration du délai imparti par une demande en ce sens. "<br/>
<br/>
              2. Mme A... demande l'annulation de l'ordonnance du 6 décembre 2019 par laquelle le président de la chambre disciplinaire nationale de l'ordre des masseurs-kinésithérapeutes a, sur le fondement des dispositions citées ci-dessus, rejeté comme irrecevable son appel formé contre la décision du 5 juillet 2019 par laquelle la chambre disciplinaire de première instance du conseil régional des Hauts-de-France de cet ordre lui a infligé la sanction de l'avertissement.<br/>
<br/>
              3. Aux termes de l'article R. 4126-44 du code de la santé publique, rendu applicable aux masseurs-kinésithérapeutes par l'article R. 4323-3 du même code, le délai d'appel contre une décision d'une chambre disciplinaire de première instance de l'ordre des masseurs-kinésithérapeutes " est de trente jours à compter de la notification de la décision ". Ce délai, qui ne se confond pas avec un délai d'un mois, présente le caractère d'un délai franc.<br/>
<br/>
              4. Il résulte des termes mêmes de l'ordonnance attaquée que, pour rejeter l'appel de Mme A..., elle se fonde sur la circonstance que, la décision de première instance lui ayant été régulièrement notifiée le 6 juillet 2019, son appel enregistré le 13 août 2019 était tardif.<br/>
<br/>
              5. Il ressort toutefois des pièces du dossier soumis au juge du fond que le pli comportant la notification de la décision de première instance avait été présenté au domicile de Mme A... le 6 juillet 2019 mais n'avait été effectivement retiré au bureau de poste de Valenciennes que le 12 juillet 2019. Par suite, Mme A... est fondée à soutenir que l'ordonnance attaquée a dénaturé les pièces du dossier soumis aux juges du fond et à en demander l'annulation, sans qu'il soit besoin de se prononcer sur l'autre moyen de son pourvoi.<br/>
<br/>
              6. Il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative.<br/>
<br/>
              7. Ainsi qu'il a été dit plus haut, il ressort des pièces du dossier que la décision de la chambre disciplinaire de première instance a été notifiée à Mme A... le 12 juillet 2019. Celle-ci disposait donc, pour interjeter appel de cette décision, d'un délai franc de trente jours courant jusqu'au dimanche 11 août 2019 à minuit, sa requête pouvant être régulièrement enregistrée durant toute la journée du lundi 12 août. Son appel enregistré le 13 août 2019 au greffe de la chambre disciplinaire nationale de l'ordre des masseurs kinésithérapeutes est, par suite, tardif, sans qu'y fasse obstacle la circonstance qu'il ait été expédié par l'intéressée le samedi 10 août 2019.<br/>
<br/>
              8. Les conclusions présentées par Mme A... au titre de l'article L. 761-1 du code de justice administrative, qui ne désignent pas la partie débitrice, ne peuvent, en tout état de cause, qu'être rejetées. Les dispositions de ce même article font obstacle à ce que la somme demandée à ce titre par le Conseil national de l'ordre des masseurs-kinésithérapeutes, qui n'a pas, en qualité d'intervenant, qualité de partie dans la présente instance, soit mise à la charge de Mme A....<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : L'ordonnance du 6 décembre 2019 du président de la chambre disciplinaire nationale de l'ordre des masseurs-kinésithérapeutes est annulée.<br/>
<br/>
		Article 2 : La requête de Mme A... est rejetée.<br/>
<br/>
Article 3 : Les conclusions présentées par Mme A... et par le Conseil national de l'ordre des masseurs-kinésithérapeutes au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 4 : La présente décision sera communiquée à Mme C... A... et au Conseil national de l'ordre des masseurs-kinésithérapeutes.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
