<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042353564</ID>
<ANCIEN_ID>JG_L_2020_09_000000428683</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/35/35/CETATEXT000042353564.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème - 8ème chambres réunies, 21/09/2020, 428683, Publié au recueil Lebon</TITRE>
<DATE_DEC>2020-09-21</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>428683</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème - 8ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BAUER-VIOLAS, FESCHOTTE-DESBOIS, SEBAGH</AVOCATS>
<RAPPORTEUR>M. Laurent-Xavier Simonel</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Laurent Cytermann</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:428683.20200921</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B... A... a demandé au tribunal administratif de Grenoble de condamner l'Etat à lui verser la somme de 54 053,23 euros correspondant au solde de l'indemnité de départ volontaire qu'il estimait lui être due ainsi que les intérêts au taux légal à compter du 5 septembre 2014 avec leur capitalisation annuelle. <br/>
<br/>
              Par un jugement n° 1506234 du 30 novembre 2017, le tribunal administratif de Grenoble a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 18LY00373 du 7 janvier 2019, la cour administrative d'appel de Lyon a rejeté l'appel formé par M. A... contre ce jugement. <br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés au secrétariat de la section du contentieux les 7 mars 2019, 7 juin 2019 et 26 mai 2020, M. A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, d'annuler le jugement du tribunal administratif de Grenoble du 30 novembre 2017 et de condamner l'Etat à lui payer la somme de 51 168,10 euros ainsi que les intérêts à compter du 5 septembre 2014 avec leur capitalisation ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales et son protocole additionnel ; <br/>
              - le code de l'éducation ;<br/>
              - le décret n° 2008-368 du 17 avril 2008 ;<br/>
              - le décret n° 2014-507 du 19 mai 2014 ;<br/>
              - la circulaire n° 2009-067 du 19 mai 2009 du ministre de l'éducation nationale ;<br/>
              - la circulaire n° 2014-156 du 27 novembre 2014 du ministre de l'éducation nationale et de la jeunesse ;<br/>
              - le code de justice administrative et l'ordonnance n° 2020-305 du 25 mars 2020 ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Laurent-Xavier Simonel, conseiller d'Etat en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Laurent Cytermann, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Bauer-Violas, Feschotte-Desbois, Sebagh, avocat de M. B... A... ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que M. B... A..., qui était professeur depuis 1992 dans un établissement d'enseignement privé sous contrat avec l'Etat, a présenté au recteur de l'académie de Grenoble, le 5 septembre 2014, une demande préalable tendant à connaître le montant de l'indemnité de départ volontaire auquel il pouvait prétendre en cas de démission pour reprise d'une entreprise. Par une première décision du 17 novembre 2014, le recteur lui a indiqué que ce montant s'élèverait à 22 000 euros si la démission intervenait au cours de l'année civile en cours, montant qui a été maintenu, à la suite d'une demande de précision, par une deuxième décision du 17 décembre 2014. Le 6 janvier 2015, M. A... a exercé un recours gracieux contre la première décision en demandant que le montant soit réévalué à 73 168,10 euros, puis a présenté, le 28 janvier 2015, sa démission à compter du 26 avril de la même année. Par un arrêté du 13 mars 2015, le recteur a accepté cette démission et a attribué à l'intéressé une indemnité de 22 000 euros. Par un jugement du 30 novembre 2017, le tribunal administratif de Grenoble a rejeté la demande de M. A... tendant à la condamnation de l'Etat à lui verser le solde de l'indemnité de départ volontaire auquel il estimait avoir droit, augmenté des intérêts au taux légal à compter du 5 septembre 2014, avec leur capitalisation annuelle. M. A... se pourvoit en cassation contre l'arrêt du 7 janvier 2019 par lequel la cour administrative d'appel de Lyon a rejeté l'appel qu'il a formé contre ce jugement.<br/>
<br/>
              Sur l'arrêt attaqué en tant qu'il s'est prononcé sur la régularité du jugement de première instance :<br/>
<br/>
              2. Le juge, auquel il incombe de veiller à la bonne administration de la justice, n'a aucune obligation, hormis le cas où des motifs exceptionnels tirés des exigences du débat contradictoire l'imposeraient, de faire droit à une demande de report de l'audience formulée par une partie. Il n'a pas davantage à motiver le refus qu'il oppose à une telle demande. En jugeant que la circonstance invoquée par l'avocat de M. A... à l'appui de ses demandes d'ajournement présentées les 2 et 10 novembre 2017 et qui tenait à la représentation, le jour de l'audience, d'un autre client devant une autre juridiction, ne constituait pas un tel motif exceptionnel, la cour n'a pas commis d'erreur de droit.<br/>
<br/>
              Sur l'arrêt attaqué en tant qu'il s'est prononcé sur le bien-fondé du jugement de première instance :<br/>
<br/>
              3. Aux termes de l'article 1er du décret du 17 avril 2008 instituant une indemnité de départ volontaire, dans sa rédaction applicable au litige : " Une indemnité de départ volontaire peut être attribuée aux fonctionnaires qui quittent définitivement la fonction publique de l'Etat à la suite d'une démission régulièrement acceptée en application du 2° de l'article 24 de la loi du 13 juillet 1983 susvisée et aux agents non titulaires de droit public recrutés pour une durée indéterminée qui démissionnent dans les conditions fixées par l'article 48 du décret du 17 janvier 1986 susvisé et dont le poste est supprimé ou fait l'objet d'une restructuration dans le cadre d'une opération de réorganisation du service. / L'agent qui souhaite bénéficier de l'indemnité de départ volontaire ne peut demander sa démission qu'à compter de la réception de la réponse de l'administration à la demande préalable de bénéfice de l'indemnité de départ volontaire ". Aux termes de l'article 3 du même décret, dans sa rédaction applicable au litige : " Nonobstant les dispositions de l'article 2 du présent décret, l'indemnité de départ volontaire peut être attribuée aux agents mentionnés à l'article 1er qui quittent définitivement la fonction publique de l'Etat pour créer ou reprendre une entreprise au sens de l'article L. 351-24 du code du travail. / Dans ce cas, les dispositions concernant la suppression du poste ou sa restructuration mentionnées à l'article 1er du présent décret ne s'appliquent pas. / (...) ". Aux termes de l'article 6 du même décret, dans sa rédaction applicable au litige : " Le montant de l'indemnité de départ volontaire ne peut excéder une somme équivalente à vingt-quatre fois un douzième de la rémunération brute annuelle perçue par l'agent au cours de l'année civile précédant celle du dépôt de sa demande de démission. Le montant de l'indemnité peut être modulé à raison de l'ancienneté de l'agent dans l'administration. (...) ". <br/>
<br/>
              4. Il résulte des dispositions du décret du 17 avril 2008 citées ci-dessus que l'attribution d'une indemnité de départ volontaire n'a pas le caractère d'un avantage statutaire. Le décret se borne à déterminer le plafond de cette indemnité et la possibilité d'en moduler le montant, sans fixer celui-ci. Chaque ministre est, ainsi, compétent, dans l'exercice de ses prérogatives d'organisation des services placés sous son autorité, pour établir, dans le respect des règles générales fixées par ces mêmes dispositions, la réglementation applicable au versement de cette indemnité au sein de son administration.<br/>
<br/>
              5. Par ailleurs, dans le cas où un texte prévoit l'attribution d'un avantage sans avoir défini l'ensemble des conditions permettant de déterminer à qui l'attribuer parmi ceux qui sont en droit d'y prétendre ou de fixer le montant à leur attribuer individuellement, l'autorité compétente peut, qu'elle dispose ou non en la matière du pouvoir réglementaire, encadrer l'action de l'administration, dans le but d'en assurer la cohérence, en déterminant, par la voie de lignes directrices, sans édicter aucune condition nouvelle, des critères permettant de mettre en oeuvre le texte en cause, sous réserve de motifs d'intérêt général conduisant à y déroger et de l'appréciation particulière de chaque situation. Dans ce cas, la personne en droit de prétendre à l'avantage en cause peut se prévaloir, devant le juge administratif, de telles lignes directrices si elles ont été publiées. En revanche, il en va autrement dans le cas où l'administration peut légalement accorder une mesure de faveur au bénéfice de laquelle l'intéressé ne peut faire valoir aucun droit. S'il est loisible, dans ce dernier cas, à l'autorité compétente de définir des orientations générales pour l'octroi de ce type de mesures, l'intéressé ne saurait se prévaloir de telles orientations à l'appui d'un recours formé devant le juge administratif.<br/>
<br/>
              6. Pour l'application des dispositions citées au point 3, le ministre chargé de l'éducation nationale a pris successivement deux circulaires, les 19 mai 2009 et 27 novembre 2014, qui ont prévu que les montants d'indemnité de départ volontaire attribués individuellement s'inscrivent " généralement " dans des fourchettes prédéfinies en fonction du nombre d'années d'ancienneté, avec la faculté pour les recteurs d'académie de s'en écarter dans le cadre de leur " pouvoir d'appréciation ". Si la première de ces circulaires indique qu'une telle faculté ne peut intervenir que " dans des cas exceptionnels ", elle précise pour les projets de création ou de reprise d'une entreprise, d'une part, que la demande sera " accueillie favorablement dans la mesure où il y a lieu d'encourager ce type d'initiative " et, d'autre part, que le montant de l'indemnité sera " généralement " fixé dans la partie haute de la fourchette. <br/>
<br/>
              7. Les circulaires des 19 mai 2009 puis du 27 novembre 2014, mentionnées au point 6, par lesquels le ministre chargé de l'éducation nationale s'est borné à encadrer l'action de l'administration dans le but d'en assurer la cohérence, en déterminant des critères permettant de mettre en oeuvre le décret du 17 avril 2008, sous réserve de motifs d'intérêt général conduisant à y déroger et de l'appréciation particulière de chaque situation, qui ont toutes deux été régulièrement publiées, constituent des lignes directrices. Dès lors, les maîtres contractuels des établissements d'enseignement privés sous contrat d'association, qui sont des agents de droit public auxquels l'article L. 914-1 du code de l'éducation rend applicables les conditions de cessation d'activité et les mesures sociales dont bénéficient les maîtres titulaires de l'enseignement public et qui peuvent, ainsi, prétendre à l'attribution d'une indemnité de départ volontaire même s'ils n'ont pas droit pour autant à obtenir un montant déterminé, peuvent se prévaloir, devant le juge administratif, des fourchettes de taux prévues par celle de ces lignes directrices qui leur sont applicables.<br/>
<br/>
              8. Par suite, en se bornant à juger que le recteur de l'académie de Grenoble avait pu, sans erreur manifeste d'appréciation, fixer le montant de l'indemnité de départ volontaire à verser à M. A... en appliquant un taux de 30 % au plafond prévu par l'article 6 du décret du 17 avril 2008, sans rechercher si ce taux s'inscrivait dans la fourchette de taux prévue, en fonction de l'ancienneté de service de l'agent, par la circulaire dont elle faisait application ou, à défaut, si un motif d'intérêt général ou des circonstances particulières tenant à la situation de l'intéressé permettaient de s'en écarter, la cour a commis une erreur de droit.<br/>
<br/>
              9. Il résulte de ce qui précède, sans qu'il soit besoin de se prononcer sur les autres moyens de son pourvoi, que M. A... est fondé à demander l'annulation de l'arrêt qu'il attaque en tant qu'il s'est prononcé sur le bien-fondé du jugement du 30 novembre 2017.<br/>
<br/>
              10. Il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative.<br/>
<br/>
              Sur le règlement au fond du litige :<br/>
<br/>
              11. En premier lieu, la circulaire du 19 mai 2009 prévoyait, d'une part, qu'" en cas de réponse positive, l'autorité compétente indiquera à l'agent le montant indemnitaire auquel il peut prétendre s'il démissionne (...) Cette notification constitue une décision susceptible de recours " mais, d'autre part, qu'" il sera précisé que le montant d'I.D.V. notifié n'est valable que dans l'hypothèse d'une démission intervenant dans le courant de l'année civile en cours et régulièrement acceptée par l'administration ". Si la circulaire indiquait, ensuite, que : " une démission présentée postérieurement à la fin de l'année civile donne lieu à un nouveau calcul de l'I.D.V. afin de prendre en compte le changement de l'année de référence ", cette précision ne pouvait faire obstacle à ce que les modifications intervenues dans les règles d'attribution de l'indemnité soient, en outre, prises en compte pour ce nouveau calcul.<br/>
<br/>
              12. Il résulte de l'instruction que la décision du 17 novembre 2014, par laquelle le recteur a indiqué, en réponse à la demande de M. A..., le montant indemnitaire auquel il pouvait prétendre s'il démissionnait, précisait que ce montant n'était valable quand dans le seul cas d'une démission intervenant dans le courant de l'année civile. Par suite et dès lors que la démission de l'intéressé n'a finalement été présentée que le 28 janvier 2015 avec effet au 26 avril de la même année, l'administration a pu, sans porter atteinte à la garantie des biens du requérant résultant de l'article 1er du protocole additionnel  à la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales et sans le priver d'une quelconque espérance légitime, faire légalement application, dans l'arrêté du 13 mars 2015 par laquelle elle a accepté sa démission et a fixé définitivement le montant de son indemnité, de la circulaire du 27 novembre 2014 qui avait été publiée entre temps. <br/>
<br/>
              13. En deuxième lieu, la circulaire du 27 novembre 2014 a remplacé les fourchettes qui étaient applicables en cas d'ancienneté supérieure à dix ans par une unique fourchette comprise entre 25 % et 50 % du plafond prévu par le décret du 17 avril 2008, sans règle particulière en cas de projet de création ou de reprise d'une entreprise. Par suite, en fixant le montant définitif de l'indemnité due à M. A... à 22 000 &#128;, c'est-à-dire à environ 30 % du plafond précité au regard de la rémunération qu'il avait perçue en 2014, l'administration n'a, en dépit de la valeur professionnelle de l'intéressé ainsi que de la pertinence et du besoin de financement de son projet de reprise d'entreprise, commis aucune erreur manifeste d'appréciation. <br/>
<br/>
              14. En troisième lieu, il ne résulte de l'instruction ni que l'administration a traité dans des délais anormalement longs la demande et les recours de M. A... ni qu'elle a délibérément différé l'intervention de ses décisions afin de pouvoir lui opposer les dispositions de la circulaire du 27 novembre 2014, qui lui sont applicables en raison de la date à laquelle il a choisi de présenter sa démission. Dès lors, l'existence d'une faute de l'administration n'est, en tout état de cause, pas établie. <br/>
<br/>
              15. Il résulte de ce qui précède que M. A... n'est pas fondé à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Grenoble a rejeté sa demande.<br/>
<br/>
              16. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise, à ce titre, à la charge de l'Etat, qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Lyon du 7 janvier 2019 est annulé en tant qu'il se prononce sur le bien-fondé du jugement du tribunal administratif de Grenoble du 30 novembre 2017.<br/>
<br/>
Article 2 : Les conclusions de la requête présentées à la cour administrative d'appel de Lyon par M. A... ainsi que le surplus de ses conclusions présentées devant le Conseil d'Etat sont rejetées.<br/>
<br/>
Article 3 : La présente décision sera notifiée à M. B... A... et au ministre de l'éducation nationale, de la jeunesse et des sports.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-01-05-03-03 ACTES LÉGISLATIFS ET ADMINISTRATIFS. DIFFÉRENTES CATÉGORIES D'ACTES. ACTES ADMINISTRATIFS - NOTION. INSTRUCTIONS ET CIRCULAIRES. DIRECTIVES ADMINISTRATIVES. - TEXTE PRÉVOYANT L'ATTRIBUTION D'UN AVANTAGE SANS AVOIR DÉFINI L'ENSEMBLE DES CONDITIONS PERMETTANT DE DÉTERMINER À QUI L'ATTRIBUER - POSSIBILITÉ POUR L'AUTORITÉ COMPÉTENTE D'ENCADRER L'ACTION DE L'ADMINISTRATION PAR DES LIGNES DIRECTRICES - EXISTENCE [RJ1], ALORS MÊME QUE CETTE AUTORITÉ DISPOSE DU POUVOIR RÉGLEMENTAIRE EN LA MATIÈRE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">01-02-02-01-03 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - COMPÉTENCE. RÉPARTITION DES COMPÉTENCES ENTRE AUTORITÉS DISPOSANT DU POUVOIR RÉGLEMENTAIRE. AUTORITÉS DISPOSANT DU POUVOIR RÉGLEMENTAIRE. MINISTRES. - TEXTE PRÉVOYANT L'ATTRIBUTION D'UN AVANTAGE SANS AVOIR DÉFINI L'ENSEMBLE DES CONDITIONS PERMETTANT DE DÉTERMINER À QUI L'ATTRIBUER - POSSIBILITÉ POUR L'AUTORITÉ COMPÉTENTE D'ENCADRER L'ACTION DE L'ADMINISTRATION PAR DES LIGNES DIRECTRICES - EXISTENCE [RJ1], ALORS MÊME QUE CETTE AUTORITÉ DISPOSE DU POUVOIR RÉGLEMENTAIRE EN LA MATIÈRE.
</SCT>
<ANA ID="9A"> 01-01-05-03-03 Dans le cas où un texte prévoit l'attribution d'un avantage sans avoir défini l'ensemble des conditions permettant de déterminer à qui l'attribuer parmi ceux qui sont en droit d'y prétendre ou de fixer le montant à leur attribuer individuellement, l'autorité compétente peut, qu'elle dispose ou non en la matière du pouvoir réglementaire, encadrer l'action de l'administration, dans le but d'en assurer la cohérence, en déterminant, par la voie de lignes directrices, sans édicter aucune condition nouvelle, des critères permettant de mettre en oeuvre le texte en cause, sous réserve de motifs d'intérêt général conduisant à y déroger et de l'appréciation particulière de chaque situation.</ANA>
<ANA ID="9B"> 01-02-02-01-03 Dans le cas où un texte prévoit l'attribution d'un avantage sans avoir défini l'ensemble des conditions permettant de déterminer à qui l'attribuer parmi ceux qui sont en droit d'y prétendre ou de fixer le montant à leur attribuer individuellement, l'autorité compétente peut, qu'elle dispose ou non en la matière du pouvoir réglementaire, encadrer l'action de l'administration, dans le but d'en assurer la cohérence, en déterminant, par la voie de lignes directrices, sans édicter aucune condition nouvelle, des critères permettant de mettre en oeuvre le texte en cause, sous réserve de motifs d'intérêt général conduisant à y déroger et de l'appréciation particulière de chaque situation.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, Section, 4 février 2015, Ministre de l'intérieur c/ M.,, n°s 383267 383268, p. 17.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
