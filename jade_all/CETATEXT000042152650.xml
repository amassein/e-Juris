<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042152650</ID>
<ANCIEN_ID>JG_L_2020_07_000000429336</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/15/26/CETATEXT000042152650.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème chambre, 10/07/2020, 429336, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-07-10</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>429336</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROCHETEAU, UZAN-SARANO ; SARL MEIER-BOURDEAU, LECUYER ET ASSOCIES</AVOCATS>
<RAPPORTEUR>M. Géraud Sajust de Bergues</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Marie-Gabrielle Merloz</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2020:429336.20200710</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              L'EARL Valette a demandé au tribunal administratif de Grenoble, à titre principal, d'annuler le titre de recettes émis le 13 juin 2016 par le directeur général de l'Etablissement national des produits de l'agriculture et de la mer (FranceAgriMer), à hauteur d'un montant de 34 135,07 euros, pour le recouvrement d'aides d'Etat versées entre 1998 et 2002 et, à titre subsidiaire, de condamner FranceAgriMer à lui verser la somme de 17 035,09 euros en réparation du préjudice qu'elle estime avoir subi du fait du versement d'une aide illégale. Par un jugement n° 1604456 du 27 avril 2018, le tribunal administratif de Grenoble a annulé le titre de recettes contesté.<br/>
<br/>
              Par un arrêt n° 18LY02462 du 4 février 2019, la cour administrative d'appel de Lyon a, sur appel de FranceAgriMer, annulé ce jugement et rejeté la demande de l'EARL Valette.  <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 1er avril et 28 juin 2019 au secrétariat du contentieux du Conseil d'Etat, l'EARL Valette demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel formé par FranceAgriMer ;<br/>
<br/>
              3°) de mettre à la charge de FranceAgriMer la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le traité instituant la Communauté européenne ;<br/>
              - le règlement (CEE) n° 1035/72 du Conseil du 18 mai 1972 ;<br/>
              - le règlement (CE) n° 2200/96 du Conseil du 28 octobre 1996 ;<br/>
              - le règlement (CE) n° 659/1999 du Conseil du 22 mars 1999 ;<br/>
              - le code rural et de la pêche maritime ;<br/>
              - la décision 2009/402/CE de la Commission européenne du 28 janvier 2009 ;<br/>
              - les arrêts du Tribunal de l'Union européenne du 27 septembre 2012, France c/ Commission (affaire T-139/09), Fédération de l'organisation économique fruits et légumes (Fedecom) c/ Commission (affaire T-243/09) et Producteurs de légumes de France c/ Commission (affaire T-328/09) ;<br/>
              - l'arrêt de la Cour de justice de l'Union européenne du 12 février 2015, Commission c/ France (affaire C-37/14) ;<br/>
              - le code de justice administrative et l'ordonnance n° 2020-305 du 25 mars 2020 modifiée ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Géraud Sajust de Bergues, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Marie-Gabrielle Merloz, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Rocheteau, Uzan-Sarano, avocat de l'EARL Valette et à la SARL Meier-Bourdeau, Lecuyer et associés, avocat de l'Etablissement national des produits de l'agriculture et de la mer (FranceAgrimer) ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 23 juin 2020, présentée par l'EARL Valette.<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que l'Office national interprofessionnel des fruits, des légumes et de l'horticulture (ONIFLHOR), aux droits duquel vient l'Etablissement national des produits de l'agriculture et de la mer (FranceAgriMer), a mis en place jusqu'en 2002 un régime d'aides d'Etat, dénommé " plans de campagne ", destiné à soutenir le marché national de fruits et légumes, sous la forme d'une aide financière à chaque campagne concernée, afin de prendre en compte les difficultés des marchés que traversaient certaines filières en raison d'une forte pression concurrentielle de la part des industries de transformation italiennes et espagnoles et des produits bruts importés des pays d'Europe de l'est. Les aides versées par l'ONIFLHOR transitaient par les comités économiques agricoles, qui reversaient les fonds à des groupements de producteurs en vue d'en faire bénéficier les producteurs adhérents. Saisie d'une plainte, la Commission européenne a, par une décision 2009/402/CE du 28 janvier 2009, concernant les " plans de campagne " dans le secteur des fruits et légumes mis à exécution par la France, énoncé que les aides versées au secteur des fruits et légumes français avaient pour but de faciliter l'écoulement des produits français en manipulant le prix de vente ou les quantités offertes sur les marchés, que de telles interventions constituaient des aides d'Etat instituées en méconnaissance du droit de l'Union européenne et prescrit leur récupération. Cette décision a été confirmée par trois arrêts du Tribunal de l'Union européenne, devenus définitifs, du 27 septembre 2012, France c/ Commission (T-139/09), Fédération de l'organisation économique fruits et légumes (Fedecom) c/ Commission (T-243/09) et Producteurs de légumes de France c/ Commission (T-328/09). Par un arrêt du 12 février 2015, Commission c/ France (C-37/14), la Cour de justice de l'Union européenne a jugé que les autorités françaises avaient, à cette date, manqué à leur obligation de procéder à la récupération des aides illégalement versées.<br/>
<br/>
              2. A la suite de ces arrêts et afin de procéder à la récupération des aides, FranceAgriMer a émis des titres de recettes à l'encontre de plusieurs producteurs de fruits et légumes, dont l'EARL Valette, le 13 juin 2016, pour un montant de 34 135,07 euros correspondant au montant des aides dont l'administration estime qu'elle a été bénéficiaire de 1998 à 2002 au titre des " plans de campagne ", augmenté des intérêts communautaires. Par un jugement du 27 avril 2018, le tribunal administratif de Grenoble a annulé le titre de recettes contesté. L'EARL Valette se pourvoit en cassation contre l'arrêt du 4 février 2019 par lequel la cour administrative d'appel de Lyon a annulé ce jugement et rejeté ses conclusions tendant, à titre principal, à l'annulation du titre de recettes et, à titre subsidiaire, à ce que FranceAgriMer soit condamné à lui verser la somme de 17 035,09 euros, égale au montant des intérêts communautaires, en réparation du préjudice qu'elle estime avoir subi du fait du versement d'une aide illégale.<br/>
<br/>
              3. En premier lieu, c'est par une appréciation souveraine exempte de dénaturation que la cour administrative d'appel a relevé qu'en l'espèce, la répartition entre producteurs adhérents du même groupement des montants dont la récupération doit être demandée s'est opérée en tenant compte de plusieurs éléments, dont le chiffre d'affaires réalisé par chaque producteur, et qu'il n'est pas démontré par l'EARL Valette que la méthode de répartition suivie ait abouti à une évaluation inadéquate des avantages dont elle avait bénéficié.<br/>
<br/>
              4. En second lieu, il résulte de la décision de la Commission européenne du 28 janvier 2009, qui s'impose aux autorités comme aux juridictions nationales dès lors que les recours exercés contre elle ont été définitivement rejetés, que le mécanisme des " plans de campagne " constituait une aide nouvelle soumise à l'obligation de notification en vertu des dispositions du paragraphe 3 de l'article 88 du traité instituant la Communauté européenne, reprises par les dispositions actuelles du paragraphe 3 de l'article 108 du traité sur le fonctionnement de l'Union européenne. Par suite, en jugeant que ce mécanisme constituait une aide existante au sens des dispositions du paragraphe 1 du même article, dont la mise en oeuvre avant l'intervention de la décision de la Commission n'était pas de nature à engager la responsabilité de FranceAgriMer venant aux droits de l'ONIFLHOR, la cour administrative d'appel a inexactement qualifié les aides en cause.<br/>
<br/>
              5. Il résulte toutefois de la même décision de la Commission, qui déclare ces aides incompatibles avec le marché commun, que les autorités nationales sont tenues de procéder à leur récupération auprès des bénéficiaires et ont l'obligation de leur faire supporter les intérêts communautaires afférents. Le paiement de ces intérêts communautaires ne saurait engager la responsabilité de la puissance publique, dès lors qu'ils ont pour seul objet de garantir l'effet utile du régime des aides d'Etat en compensant l'avantage financier et concurrentiel procuré par l'aide illégale entre l'octroi de celle-ci et sa récupération, y compris en cas de retard des autorités nationales à la récupérer. Ce motif, qui ne comporte l'appréciation d'aucune circonstance de fait et est de nature à justifier le rejet des conclusions de l'EARL Valette tendant à obtenir réparation du préjudice consistant, selon elle, en l'obligation de payer les intérêts communautaires, doit être substitué au motif erroné retenu par la cour. En outre et par suite, le moyen tiré de ce que ce motif aurait été relevé d'office par la cour sans qu'elle procède à l'information préalable des parties est inopérant.<br/>
<br/>
              6. Il résulte de tout ce qui précède que l'EARL Valette n'est pas fondée à demander l'annulation de l'arrêt qu'elle attaque.<br/>
<br/>
              7. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'EARL Valette la somme de 3 000 euros à verser à FranceAgriMer, au titre des dispositions de l'article L. 761-1 du code de justice administrative. Les mêmes dispositions font en revanche obstacle à ce qu'une somme soit mise à ce titre à la charge de FranceAgriMer qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : Le pourvoi de l'EARL Valette est rejeté.<br/>
Article 2 : L'EARL Valette versera à FranceAgriMer une somme de 3 000 euros, au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée à l'EARL Valette et à l'Etablissement national des produits de l'agriculture et de la mer (FranceAgriMer).<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
