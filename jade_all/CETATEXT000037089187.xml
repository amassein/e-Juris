<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037089187</ID>
<ANCIEN_ID>JG_L_2018_06_000000411182</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/08/91/CETATEXT000037089187.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  4ème chambre jugeant seule, 13/06/2018, 411182, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-06-13</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>411182</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 4ème chambre jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MATUCHANSKY, POUPOT, VALDELIEVRE</AVOCATS>
<RAPPORTEUR>Mme Françoise Tomé</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Sophie-Justine Lieber</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:411182.20180613</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              1° Sous le n° 411182, par une requête et un mémoire complémentaire, enregistrés les 6 juin et 6 septembre 2017 au secrétariat du contentieux du Conseil d'Etat, le Conseil national de l'ordre des médecins demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler pour excès de pouvoir l'ordonnance n° 2017-644 du 27 avril 2017 relative à l'adaptation des dispositions législatives relatives au fonctionnement des ordres des professions de santé ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              2° Sous le n° 411882, par une requête et un mémoire complémentaire enregistrés les 27 juin et 27 septembre 2017 au secrétariat du contentieux du Conseil d'Etat, le Conseil national de l'ordre des sages-femmes demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler pour excès de pouvoir l'ordonnance n° 2017-644 du 27 avril 2017 relative à l'adaptation des dispositions législatives relatives au fonctionnement des ordres des professions de santé ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
<br/>
              3° Sous le n° 411908, par une requête, un mémoire complémentaire et un mémoire en réplique enregistrés les 27 juin et 27 septembre 2017 et le 23 mars 2018 au secrétariat du contentieux du Conseil d'Etat, le Conseil national de l'ordre des pharmaciens demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler pour excès de pouvoir l'ordonnance n° 2017-644 du 27 avril 2017 relative à l'adaptation des dispositions législatives relatives au fonctionnement des ordres des professions de santé ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu : <br/>
              - la Constitution, notamment ses articles 38 et 62 ; <br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - la loi n° 2017-1841 du 30 décembre 2017 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Françoise Tomé, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Sophie-Justine Lieber, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Matuchansky, Poupot, Valdelièvre, avocat du Conseil national de l'ordre des médecins et du Conseil national de l'ordre des sages-femmes et à la SCP Célice, Soltner, Texidor, Perier, avocat du Conseil national de l'ordre des pharmaciens ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant que les requêtes du Conseil national de l'ordre des médecins, du Conseil national des sages-femmes et du Conseil national des pharmaciens, visées ci-dessus, présentent à juger les mêmes questions ; qu'il y a lieu de les joindre pour statuer par une seule décision ; <br/>
<br/>
              2. Considérant qu'il résulte des dispositions de l'article 38 de la Constitution que les ordonnances qu'elle prévoit ont, alors même qu'elles interviennent dans une matière ressortissants, en vertu de l'article 34 ou d'autres dispositions constitutionnelles, au domaine de la loi, le caractère d'actes administratifs ; que, cependant, dès lors que sa ratification est opérée par le législateur, une ordonnance acquiert valeur législative à compter de sa signature ; qu'il suit de là qu'en cas de ratification, la légalité d'une ordonnance ne peut plus, en principe, être utilement contestée devant la juridiction administrative ; qu'il ne pourrait en aller autrement que dans le cas où la loi de ratification s'avérerait incompatible, dans un domaine entrant dans le champ d'application de l'article 6 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, avec les stipulations de cet article, au motif qu'en raison des circonstances de son adoption, cette loi aurait eu essentiellement pour but de faire obstacle au droit de toute personne à un procès équitable ;<br/>
<br/>
              3. Considérant que, par son article 1er, la loi du 30 décembre 2017 ratifiant l'ordonnance n° 2017-644 du 27 avril 2017 relative à l'adaptation des dispositions législatives relatives au fonctionnement des ordres des professions de santé, a ratifié l'ordonnance attaquée ; que, contrairement à ce que soutient le Conseil national de l'ordre des pharmaciens, ni la circonstance que cette loi de ratification ait été déposée devant le Parlement postérieurement à l'enregistrement de sa requête, ni celle qu'elle n'aurait porté que sur la seule ordonnance du 27 avril 2017 ne sont, en tout état de cause, susceptibles de la faire regarder comme ayant eu essentiellement pour but de faire obstacle au droit de toute personne à un procès équitable, garanti par les stipulations de l'article 6 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ; que, par suite, la légalité de l'ordonnance attaquée n'est plus susceptible d'être discutée par la voie contentieuse ; que les conclusions des requêtes tenant à son annulation sont devenues sans objet ;<br/>
<br/>
              4. Considérant que, dans les circonstances de l'espèce, il n'y a pas lieu de mettre à la charge de l'Etat les sommes que demandent le Conseil national de l'ordre des médecins, le Conseil national de l'ordre des sages-femmes et le Conseil national de l'ordre des pharmaciens au titre de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er : Il n'y a pas lieu de statuer sur les conclusions des requêtes du Conseil national de l'ordre des médecins, du Conseil national de l'ordre des sages-femmes et du Conseil national de l'ordre des pharmaciens dirigées contre l'ordonnance n° 2017-644 du 27 avril 2017 relative à l'adaptation des dispositions législatives relatives au fonctionnement des ordres des professions de santé.<br/>
<br/>
Article 2 : Le surplus des conclusions de ces mêmes requêtes présenté au titre de l'article L. 761-1 du code de justice administrative est rejeté.<br/>
<br/>
Article 3 : La présente décision sera notifiée au Conseil national de l'ordre des médecins, au Conseil national de l'ordre des sages-femmes, au Conseil national de l'ordre des pharmaciens, à la ministre des solidarités et de la santé, à la ministre des outre-mer et au Premier ministre.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
