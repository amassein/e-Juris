<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000025040992</ID>
<ANCIEN_ID>JG_L_2011_12_000000316159</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/25/04/09/CETATEXT000025040992.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème et 10ème sous-sections réunies, 20/12/2011, 316159</TITRE>
<DATE_DEC>2011-12-20</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>316159</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème et 10ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GASCHIGNARD</AVOCATS>
<RAPPORTEUR>M. Philippe Josse</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Pierre Collin</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2011:316159.20111220</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés le 13 mai et le 18 août 2008 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. B... A..., demeurant..., ; M.  A...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 06PA00614 du 18 février 2008 par lequel la cour administrative d'appel de Paris a rejeté sa requête tendant, d'une part, à l'annulation du jugement n° 0411485/7 du 16 décembre 2005 du tribunal administratif de Paris rejetant sa demande tendant à l'annulation de la décision implicite du ministre des transports rejetant sa demande indemnitaire du 30 décembre 2003 et à la condamnation de l'Etat à lui verser la somme de 63 192 euros au titre du préjudice financier ainsi qu'une somme de 1 500 euros au titre de son préjudice moral en réparation des fautes commises lors du lancement du projet Eurotunnel et dans le contrôle de l'exécution du projet, ces sommes étant augmentées des intérêts légaux à compter du 30 décembre 2003, d'autre part, à la condamnation de l'Etat à lui verser les sommes et les intérêts mentionnés ci-dessus ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat le versement de la somme de 8 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le traité entre la République française et le Royaume-Uni de Grande-Bretagne et d'Irlande du Nord concernant la construction et l'exploitation par des sociétés privées concessionnaires d'une liaison fixe transmanche, signé à Cantorbéry le 12 février 1986 ;<br/>
<br/>
              Vu la concession concernant la conception, le financement, la construction et l'exploitation d'une liaison fixe à travers la Manche, établie à Paris le 14 mars 1986 entre, d'une part, le Gouvernement de la République française et le Gouvernement du Royaume-Uni de Grande-Bretagne et d'Irlande du Nord et, d'autre part, la société anonyme France-Manche et The Channel Tunnel Group Limited, approuvée en tant que de besoin par la loi n° 87-384 du 15 juin 1987 et amendée par la loi n° 99-589 du 12 juillet 1999 ;<br/>
<br/>
              Vu la loi n° 87-384 du 15 juin 1987 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Philippe Josse, Conseiller d'Etat,  <br/>
<br/>
              - les observations de la SCP Gaschignard, avocat de M. A..., <br/>
<br/>
              - les conclusions de M. Pierre Collin, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Gaschignard, avocat de M. A... ;<br/>
<br/>
<br/>
<br/>Considérant qu'aux termes du dernier alinéa de l'article R. 741-2 du code de justice administrative : "La décision fait apparaître la date de l'audience et la date à laquelle elle a été prononcée" ; que tant la minute que l'expédition de l'arrêt attaqué figurant au dossier portent des indications contradictoires quant à la date de sa lecture ; qu'ainsi, les mentions de cet arrêt ne permettent pas au Conseil d'Etat, juge de cassation, d'exercer son contrôle sur sa régularité ; que, dès lors, et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, l'arrêt attaqué doit être annulé ;<br/>
<br/>
              Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              Considérant qu'il résulte de l'instruction que la République française et le Royaume-Uni se sont engagés, par traité signé à Cantorbéry le 12 février 1986, à permettre la construction et l'exploitation d'une liaison fixe transmanche par des sociétés privées concessionnaires ; que par traité de concession quadripartite en date du 14 mars 1986, ils ont attribué aux deux sociétés France Manche S.A. et The Channel Group Ltd, retenues à la suite d'une consultation publique ouverte le 2 avril 1985, le droit et l'obligation d'assurer conjointement et solidairement la conception, la construction et l'exploitation d'une liaison qui comprend un double tunnel ferroviaire assorti d'un tunnel de service ; qu'un groupement dénommé "Eurotunnel" a été constitué par les sociétés Eurotunnel S. A. et Eurotunnel P.L.C., sociétés mères des deux sociétés concessionnaires ; que M. A..., agissant en tant qu'actionnaire de la société Eurotunnel S.A, a demandé  au tribunal administratif de Paris de condamner l'Etat à lui verser une indemnité de 63 192 euros en réparation du préjudice financier qu'il estime avoir subi du fait de la dépréciation des titres Eurotunnel dont il a fait l'acquisition à plusieurs occasions entre 1987 et 1996 et une indemnité de 1 500 euros au titre de son préjudice moral ; que, par un jugement du 16 décembre 2005, le tribunal administratif de Paris a rejeté cette demande ; que, pour engager la responsabilité de la puissance publique dans la formation des préjudices qu'il allègue, M.  A...fait valoir trois séries de fautes qu'aurait commises l'Etat, en premier lieu à l'occasion de la préparation du projet, en deuxième lieu à l'occasion des appels publics à l'épargne destinés à le financer, enfin à l'occasion de son déroulement ;<br/>
<br/>
              Sur la responsabilité de l'Etat du fait de fautes qui auraient été commises lors de la préparation du projet :<br/>
<br/>
              Considérant, en premier lieu, que  l'article 53 de la Constitution dispose : "Les traités de paix, les traités de commerce, les traités ou accords relatifs à l'organisation internationale, ceux qui engagent les finances de l'Etat, ceux qui modifient des dispositions de nature législative, ceux qui sont relatifs à l'état des personnes, ceux qui comportent cession, échange ou adjonction de territoire, ne peuvent être ratifiés ou approuvés qu'en vertu d'une loi./ Ils ne prennent effet qu'après avoir été ratifiés ou approuvés. (...)" ; que le premier alinéa de l'article 39 de la Constitution dispose : "L'initiative des lois appartient concurremment au Premier ministre et aux membres du Parlement." ; que les actes par lesquels est autorisée par le Parlement la ratification d'un engagement international et les actes de dépôt des projets et propositions de loi, ainsi que leur inscription à l'ordre du jour des assemblées parlementaires participent des relations entre le Gouvernement et le Parlement et que, par suite, n'est pas susceptible d'être arguée de faute devant la juridiction administrative l'éventuel retard, allégué par le requérant, qui aurait été mis à la présentation au Parlement et à l'adoption par celui-ci du projet de loi autorisant la ratification du traité de Cantorbéry ;<br/>
<br/>
              Considérant, en deuxième lieu, que le commencement des travaux n'était légalement conditionné ni par la ratification du traité de Cantorbéry, ni par l'approbation par le législateur du  traité de concession quadripartite, qui n'était d'ailleurs requise que pour celles des stipulations de cette concession qui contreviendraient au droit français ; qu'ainsi, doit être écarté le moyen selon lequel aurait été constitutif d'une faute de l'Etat français le fait que les travaux de construction du tunnel aient débuté avant que le Parlement autorise, par deux lois du 15 juin 1987, la ratification du traité de Cantorbéry et l'approbation, en tant que de besoin, de la concession concernant la conception, le financement, la construction et l'exploitation d'une liaison fixe à travers la Manche ;<br/>
<br/>
              Considérant, en troisième lieu, qu'aux termes du paragraphe 1 de l'article 1er du traité franco-britannique signé le 12 février 1986 à Cantorbéry : "(...) La liaison fixe transmanche sera financée sans qu'il soit fait appel à des fonds des Gouvernements ou à des garanties gouvernementales de nature financière ou commerciale" ; qu'ainsi le choix, par le Gouvernement, de la formule dite du "financement de projet", dans laquelle le financement de l'ouvrage est opéré par la mobilisation de capitaux privés rémunérés et d'emprunts remboursés par les recettes commerciales engendrées par l'exploitation ultérieure de celui-ci et que critique M. A..., s'inscrit dans le cadre d'un engagement international, qui n'est pas susceptible d'engager la responsabilité de l'Etat pour un motif autre que la rupture de l'égalité devant les charges publiques ;<br/>
<br/>
              Considérant, en quatrième lieu, que M.  A...n'établit pas de lien de causalité direct entre le calendrier des travaux, fixé par le traité de concession du 14 mars 1986, et la dépréciation qu'ont subie les titres qu'il détient ; qu'ainsi, M.  A...n'est pas fondé à demander réparation d'un préjudice qu'il aurait subi du fait de ce calendrier ;<br/>
<br/>
              Considérant qu'il résulte de tout ce qui précède que doit être écartée la mise en jeu de la responsabilité de l'Etat du fait de fautes qui auraient été commises à l'occasion de la préparation du projet ;<br/>
<br/>
              Sur la responsabilité de l'Etat du fait de fautes qui auraient été commises à l'occasion des appels publics à l'épargne destinés à financer le projet :<br/>
<br/>
              Considérant que la mise en jeu de la responsabilité de l'Etat à l'endroit des actionnaires du projet Eurotunnel pour avoir diffusé des informations qui, ensuite, se sont révélées erronées, suppose que soit établi que ce dernier a diffusé des informations qu'il savait inexactes à l'époque des faits ;<br/>
<br/>
              Considérant, en premier lieu, que la présence, tant au sein de l'appel d'offres du 20 mars 1985 que du traité de concession quadripartite du 14 mars 1986, de clauses tendant à assurer le respect des coûts et des délais ainsi que l'adéquation du volume des financements privés mobilisés aux besoins du projet ne saurait être considérée comme ayant eu pour objet de tromper les épargnants ;<br/>
<br/>
              Considérant, en deuxième lieu, que les déclarations du Président de la République à l'occasion de la présentation du projet, celles des ministres lors des débats parlementaires sur les projets de loi autorisant la ratification du traité de Cantorbéry, l'approbation, en tant que de besoin, de la concession concernant la conception, le financement, la construction et l'exploitation d'une liaison fixe à travers la Manche, les festivités ayant accompagné l'inauguration du tunnel le 6 mai 1994 ainsi que la mise en place, sur recommandation de la direction du Trésor, de diverses mesures favorables à la société, avaient pour objet d'exprimer le soutien des pouvoirs publics à un grand projet d'infrastructure et non pas d'inciter les épargnants à souscrire aux titres de la société Eurotunnel S.A ;<br/>
<br/>
              Considérant, en troisième lieu, que M.  A...n'établit pas le lien qu'il y aurait entre sa décision de souscrire des titres de la société Eurotunnel S.A et la note n° 233/87 de la trésorerie générale de Bourgogne, interne au réseau du Trésor public ;<br/>
<br/>
              Considérant qu'il résulte de tout ce qui précède que doit être écartée la mise en jeu de la responsabilité de l'Etat du fait de fautes qui auraient été commises à l'occasion des appels publics à l'épargne destinés à financer le projet ;<br/>
<br/>
              Sur la responsabilité de l'Etat du fait de fautes qui auraient été commises par défaut de surveillance du bon déroulement du projet :<br/>
<br/>
              Considérant qu'aux termes de l'article 10 du traité franco-britannique signé le 12 février 1986 à Cantorbéry : "(l) Une Commission intergouvernementale est mise en place pour suivre au nom des deux Gouvernements et par délégation de ceux-ci l'ensemble des questions liées à la construction et à l'exploitation de la Liaison Fixe. / (2) A l'égard des Concessionnaires, les deux Gouvernements exercent, par l'intermédiaire de la Commission intergouvernementale, leurs droits et obligations au titre de la Concession à l'exception de ceux concernant la modification, la prolongation, la suspension, la résiliation ou le transfert de cette dernière. / (3) Au titre de sa mission, la Commission intergouvernementale doit notamment : / (a) superviser la construction et l'exploitation de la Liaison Fixe ; / (b) entreprendre toutes consultations nécessaires avec les Concessionnaires ; / (c) prendre des décisions au nom des deux Gouvernements pour l'exécution de la Concession ; / (d) approuver les propositions du Comité de sécurité faites en application de l'article 11 ; / (e) élaborer ou participer à l'élaboration de tout règlement applicable à la Liaison Fixe, y compris en matière maritime et d'environnement, et en assurer le suivi ; / (f) examiner toute question qui lui serait soumise par les Gouvernements, le Comité de sécurité ou dont l'examen lui paraîtrait nécessaire ; / (g) émettre des avis et recommandations à l'égard des deux Gouvernements ou des Concessionnaires. (...)" ;<br/>
<br/>
              Considérant que l'article 2 de la concession quadripartite du 14 mars 1986 stipule : "Les concessionnaires ont le droit et l'obligation d'assurer (...) la conception, le financement, la construction et l'exploitation (...) d'une Liaison Fixe Transmanche. Les concessionnaires agissent à leurs risques et périls et sans appel à des fonds gouvernementaux ou à des garanties gouvernementales de nature financière ou commerciale, quelque soient les aléas rencontrés (...) les Gouvernements garantissent aux concessionnaires la liberté de fixer leur politique commerciale (...) les concédants n'interviennent pas dans la gestion ou l'exploitation de la Liaison Fixe" ; qu'aux termes de l'article 27 de la même concession quadripartite : "27.1. (...) la Commission intergouvernementale est chargée de superviser, au nom des concédants, la construction et l'exploitation de la Liaison Fixe par les concessionnaires. (...) / 27.5. Les Concessionnaires doivent suivre les instructions données par la Commission intergouvernementale et le Comité de sécurité dans l'exercice de leurs fonctions prévues à l'article 27.1 et 27.2. (...) / 27.7. Les Concédants s'assurent que, dans l'exercice de leurs fonctions, la Commission intergouvernementale et le Comité de sécurité prennent les mesures appropriées pour faciliter l'exécution de la Concession. (...)" ;<br/>
<br/>
              Considérant, en premier lieu, qu'il résulte des stipulations ainsi énoncées du traité de Cantorbéry et de la concession quadripartite que l'Etat n'avait, en sa qualité de concédant, aucune responsabilité directe ou par l'intermédiaire de la Commission intergouvernementale à l'égard des sociétés concessionnaires et de leurs actionnaires, dans la conception, le financement, la construction et l'exploitation de l'ouvrage, ni dans la composition du capital d'Eurotunnel, la désignation des membres de son conseil d'administration ou de sa direction ; qu'il suit de là que les carences alléguées de la Commission intergouvernementale, tant dans la surveillance des équilibres financiers du projet que dans celle de la passation des contrats nécessaires à sa réalisation ou dans l'alerte des autorités boursières quant à ses difficultés de mise en oeuvre, de même que le fait que les travaux de construction du tunnel aient débuté avant la mise en place effective de la Commission, ne sauraient être regardés comme de nature à engager la responsabilité de l'Etat ;<br/>
<br/>
              Considérant, en deuxième lieu, que M.  A...n'établit pas le lien qu'il y aurait entre la dépréciation enregistrée par les titres qu'il détient et le fait que l'Etat aurait manqué à ses obligations en prolongeant la période pendant laquelle des produits pouvaient être vendus en franchise de taxe à bord des ferrys assurant les liaisons transmanche et en tardant à réaliser certaines infrastructures de nature, le cas échéant, à améliorer la rentabilité du tunnel ; qu'au surplus, l'Etat français ne saurait être tenu pour responsable du retard mis par le Royaume-Uni à réaliser une ligne à grande vitesse entre Douvres et Londres ;<br/>
<br/>
              Considérant, en troisième lieu, que la qualité d'actionnaire de la société Eurotunnel S.A en laquelle M.  A...agit ne lui ouvre pas la possibilité d'engager la responsabilité de l'Etat pour des fautes que ce dernier aurait commises dans l'exercice de ses pouvoirs de police ou de ses pouvoirs généraux de surveillance de la réalisation de travaux d'infrastructures ;<br/>
<br/>
              Considérant qu'il résulte de tout ce qui précède que doit être écartée la mise en jeu de la responsabilité de l'Etat dans la formation des préjudices financier et moral que M.  A...estime avoir subis du fait de la dépréciation des titres Eurotunnel ; que doivent ainsi être rejetées ses conclusions tendant à l'annulation du jugement du tribunal administratif de Paris du 16 décembre 2005 et à la condamnation de l'Etat à lui verser une indemnité en réparation de ces préjudices ;<br/>
<br/>
              Sur les conclusions de M.  A...tendant à l'application des dispositions de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mis à la charge de l'Etat, qui n'est pas, dans la présente instance, la partie perdante, le versement d'une somme au titre des frais exposés par M.  A...en appel et en cassation et non compris dans les dépens ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : L'arrêt n° 06PA00614 de la cour administrative d'appel de Paris du 18 février 2008 est annulé.<br/>
Article 2 : Les conclusions de M.  A...tendant à l'annulation du jugement du tribunal administratif de Paris du 16 décembre 2005 et à la condamnation de l'Etat à lui verser une indemnité sont rejetées.<br/>
Article 3 : Les conclusions de M.  A...tendant à l'application des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée à M. B... A..., à la ministre de l'écologie, du développement durable, des transports et du logement et au ministre de l'économie, des finances et de l'industrie.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">60-01-02-01-01-01 RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. FAITS SUSCEPTIBLES OU NON D'OUVRIR UNE ACTION EN RESPONSABILITÉ. FONDEMENT DE LA RESPONSABILITÉ. RESPONSABILITÉ SANS FAUTE. RESPONSABILITÉ FONDÉE SUR L'ÉGALITÉ DEVANT LES CHARGES PUBLIQUES. RESPONSABILITÉ DU FAIT DE TRAITÉS OU DE CONVENTIONS INTERNATIONALES. - PROJET DE LIAISON FIXE TRANSMANCHE - FINANCEMENT PAR MOBILISATION DE CAPITAUX PRIVÉS - CHOIX S'INSCRIVANT DANS LE CADRE DU TRAITÉ FRANCO-BRITANNIQUE DE CANTORBÉRY DU 12 FÉVRIER 1986 - CONSÉQUENCE - RESPONSABILITÉ DE L'ETAT SUSCEPTIBLE D'ÊTRE ENGAGÉE SEULEMENT SUR LE FONDEMENT DE LA RUPTURE DE L'ÉGALITÉ DEVANT LES CHARGES PUBLIQUES.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">60-01-02-02 RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. FAITS SUSCEPTIBLES OU NON D'OUVRIR UNE ACTION EN RESPONSABILITÉ. FONDEMENT DE LA RESPONSABILITÉ. RESPONSABILITÉ POUR FAUTE. - MISE EN JEU DE LA RESPONSABILITÉ POUR FAUTE DE L'ETAT POUR DIFFUSION D'INFORMATIONS RELATIVES À LA RENTABILITÉ FUTURE D'UN PROJET ÉCONOMIQUE QUI SE SONT RÉVÉLÉES ERRONÉES - CONDITION - CONNAISSANCE PAR L'ETAT DU CARACTÈRE INEXACT DE CES INFORMATIONS À L'ÉPOQUE DES FAITS.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">60-01-03-02 RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. FAITS SUSCEPTIBLES OU NON D'OUVRIR UNE ACTION EN RESPONSABILITÉ. AGISSEMENTS ADMINISTRATIFS SUSCEPTIBLES D'ENGAGER LA RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. RENSEIGNEMENTS. - DIFFUSION PAR L'ETAT D'INFORMATIONS RELATIVES À LA RENTABILITÉ FUTURE D'UN PROJET ÉCONOMIQUE QUI SE SONT RÉVÉLÉES ERRONÉES - CONDITION - CONNAISSANCE PAR L'ETAT DU CARACTÈRE INEXACT DE CES INFORMATIONS À L'ÉPOQUE DES FAITS.
</SCT>
<ANA ID="9A"> 60-01-02-01-01-01 Le paragraphe 1 de l'article 1er du traité franco-britannique signé le 12 février 1986 à Cantorbéry stipule que la liaison fixe transmanche sera financée sans qu'il soit fait appel à des fonds des Gouvernements ou à des garanties gouvernementales de nature financière ou commerciale. Ainsi, le choix, par le Gouvernement, de la formule dite du « financement de projet », dans laquelle le financement de l'ouvrage est opéré par la mobilisation de capitaux privés rémunérés et d'emprunts remboursés par les recettes commerciales engendrées par l'exploitation ultérieure de celui-ci, critiqué par le requérant, s'inscrit dans le cadre d'un engagement international qui n'est pas susceptible d'engager la responsabilité de l'Etat pour un motif autre que la rupture de l'égalité devant les charges publiques. Par suite, la mise en jeu de la responsabilité de l'Etat pour faute liée au refus d'un mode de financement public ne peut qu'être écartée.</ANA>
<ANA ID="9B"> 60-01-02-02 La mise en jeu de la responsabilité de l'Etat, à l'endroit des actionnaires d'un projet économique, pour avoir diffusé des informations relatives à la rentabilité future de ce projet qui se sont par la suite révélées erronées, suppose que soit établi que l'Etat a diffusé des informations qu'il savait inexactes à l'époque des faits.</ANA>
<ANA ID="9C"> 60-01-03-02 La mise en jeu de la responsabilité de l'Etat, à l'endroit des actionnaires d'un projet économique, pour avoir diffusé des informations relatives à la rentabilité future de ce projet qui se sont par la suite révélées erronées, suppose que soit établi que l'Etat a diffusé des informations qu'il savait inexactes à l'époque des faits.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
