<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029751386</ID>
<ANCIEN_ID>JG_L_2014_11_000000362628</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/75/13/CETATEXT000029751386.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère et 6ème sous-sections réunies, 12/11/2014, 362628</TITRE>
<DATE_DEC>2014-11-12</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>362628</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère et 6ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GARREAU, BAUER-VIOLAS, FESCHOTTE-DESBOIS ; SCP COUTARD, MUNIER-APAIRE</AVOCATS>
<RAPPORTEUR>M. Rémi Decout-Paolini</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Maud Vialettes</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:362628.20141112</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 10 septembre et 10 décembre 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour Mme E...F..., demeurant..., ainsi que M. G...F..., Mme C...D...et Mme A...H..., demeurant... ; les requérants demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler la décision n° 110825 du 3 février 2012 par laquelle la commission centrale d'aide sociale, après avoir annulé la décision de la commission départementale d'aide sociale de Paris du 2 avril 2010, a, d'une part, rejeté comme portée devant une juridiction incompétente pour en connaître la demande formée par eux-mêmes et par Mme B... F...tendant à l'annulation de la décision du 23 novembre 2009 par laquelle le président du conseil de Paris a rejeté la demande de cette dernière de versement de l'allocation compensatrice pour tierce personne et, d'autre part, rejeté le surplus des conclusions de la requête, tendant au rétablissement de l'allocation compensatrice à compter du 1er juin 1997 et à ce qu'il soit enjoint au département de Paris, sous astreinte de 1 000 euros par jour de retard, de verser les sommes correspondantes assorties des intérêts moratoires ;<br/>
<br/>
              2°) de mettre à la charge du département de Paris et du département de la Seine-Saint-Denis la somme de 3 000 euros à verser à la SCP Peignot, Garreau, Bauer-Violas au titre des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de l'action sociale et des familles ;<br/>
<br/>
              Vu la loi n° 91-647 du 10 juillet 1991, notamment son article 37 ;<br/>
<br/>
              Vu la loi n° 2005-102 du 11 février 2005 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Rémi Decout-Paolini, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Maud Vialettes, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Garreau, Bauer-Violas, Feschotte-Desbois, avocat de Mme F...et autres et à la SCP Coutard, Munier-Apaire, avocat du département de la Seine-Saint-Denis ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que la commission des droits et de l'autonomie des personnes handicapées de Paris, par une décision du 15 septembre 2009, a accordé à Mme B...F..., qui en conservait le bénéfice en vertu de l'article 95 de la loi du 11 février 2005, l'allocation compensatrice pour tierce personne au taux de 80 % pour la période allant du 1er novembre 2007 au 31 octobre 2017 ; que celle-ci et M. G... F..., son fils et tuteur, ont alors saisi le département de Paris d'une demande de versement de l'allocation compensatrice ; que, par une décision du 23 novembre 2009, le président du conseil de Paris a rejeté cette demande en estimant qu'elle était sans objet et que le dossier devait être transmis au département dans lequel se situait le domicile de secours de Mme F... ; que celle-ci, son fils et d'autres proches ont demandé l'annulation de cette décision à la commission départementale d'aide sociale de Paris, qui a rejeté leur demande par une décision du 2 avril 2010, au motif que le département de Seine-Saint-Denis était compétent pour instruire la demande de renouvellement d'allocation compensatrice pour tierce personne de MmeF... ; que, saisie de l'appel de M. F...et des autres proches, la commission centrale d'aide sociale, par une décision du 3 février 2012, a annulé la décision de la commission départementale, rejeté, au motif que la juridiction de l'aide sociale n'avait pas compétence pour en connaître, les conclusions des requérants dirigées contre la décision du président du conseil de Paris, regardée comme portant sur la période courant à compter du 1er novembre 2007, et rejeté, au motif qu'elles portaient sur un litige distinct, le surplus de leurs conclusions, tendant au versement de l'allocation compensatrice pour tierce personne rétroactivement à compter du 1er juin 1997 ; que les requérants se pourvoient en cassation contre cette décision ;<br/>
<br/>
              Sur la régularité de la décision de la commission centrale d'aide sociale :<br/>
<br/>
              2. Considérant, en premier lieu, qu'en vertu des règles générales de procédure applicables, même sans texte, à toute juridiction administrative, la minute d'une décision rendue par la commission centrale d'aide sociale doit au moins être revêtue de la signature du président de la formation de jugement aux fins d'en attester la conformité au délibéré ; que rien ne fait par ailleurs obstacle à ce que la décision soit également signée par le rapporteur ; qu'en l'espèce, il résulte de l'examen de la minute de la décision attaquée que le moyen tiré de ce qu'elle n'est pas signée par le président de la formation de jugement manque en fait ; qu'il résulte de ce qui vient d'être dit que la circonstance que cette minute n'est pas revêtue de la signature du rapporteur, non plus que de celle d'un greffier d'audience, n'entache pas la décision attaquée d'irrégularité ;<br/>
<br/>
              3. Considérant, en second lieu, qu'il ressort des pièces de la procédure que le mémoire en défense du département de la Seine-Saint-Denis a été communiqué aux requérants le 13 janvier 2012, alors que l'audience était prévue le 20 janvier suivant ; que, toutefois, d'une part, les seules conclusions dirigées par les requérants contre ce département tendaient à ce que soit mis à sa charge le versement d'une somme au titre des frais exposés par eux et non compris dans les dépens et, d'autre part, la commission centrale, qui a rejeté les conclusions des requérants à fin d'annulation et d'injonction pour partie comme portées devant une juridiction incompétente pour en connaître et pour partie comme irrecevables, ne s'est pas fondée sur l'argumentation du département de la Seine-Saint-Denis ; que, dans ces conditions, le moyen tiré de la méconnaissance du principe du caractère contradictoire de la procédure doit être écarté ;<br/>
<br/>
              Sur le bien-fondé de la décision de la commission centrale d'aide sociale :<br/>
<br/>
              4. Considérant qu'aux termes de l'article L. 134-1 du code de l'action sociale et des familles : " A l'exception des décisions concernant l'attribution des prestations d'aide sociale à l'enfance ainsi que des décisions concernant le revenu de solidarité active, les décisions du président du conseil général et du représentant de l'Etat dans le département prévues à l'article L. 131-2 sont susceptibles de recours devant les commissions départementales d'aide sociale mentionnées à l'article L. 134-6 dans des conditions fixées par voie réglementaire " ; que l'article L. 131-2 du même code prévoit que : " La décision d'admission à l'aide sociale est prise par le représentant de l'Etat dans le département pour les prestations qui sont à la charge de l'Etat en application de l'article L. 121-7, à l'exception du revenu de solidarité active, et par le président du conseil général pour les autres prestations prévues au présent code " ; qu'en vertu des dispositions de l'article L. 245-2 du même code, applicables aux décisions relatives à l'allocation compensatrice en cas de maintien de son bénéfice après l'entrée en vigueur de la loi du 11 février 2005, si les décisions relatives à l'attribution de l'allocation par la commission des droits et de l'autonomie des personnes handicapées peuvent faire l'objet d'un recours devant la juridiction du contentieux technique de la sécurité sociale, les décisions du président du conseil général relatives au versement de l'allocation peuvent faire l'objet d'un recours devant les commissions départementales de l'aide sociale, dans les conditions et selon les modalités prévues aux articles L. 134-1 à L. 134-10 de ce code ; qu'enfin, le premier alinéa de l'article L. 122-4 du même code dispose que : " Lorsqu'il estime que le demandeur a son domicile de secours dans un autre département, le président du conseil général doit, dans le délai d'un mois après le dépôt de la demande, transmettre le dossier au président du conseil général du département concerné. Celui-ci doit, dans le mois qui suit, se prononcer sur sa compétence. Si ce dernier n'admet pas sa compétence, il transmet le dossier à la commission centrale d'aide sociale mentionnée à l'article L. 134-2 " ;<br/>
<br/>
              5. Considérant qu'il résulte de ces dispositions que la juridiction de l'aide sociale a seule compétence pour connaître, sous les réserves qu'elles mentionnent, des litiges relatifs à l'admission au bénéfice de l'aide sociale ; qu'est sans incidence sur la détermination de cette compétence la circonstance que le litige porte, en tout ou partie, sur la détermination du domicile de secours du demandeur, alors même que la juridiction ne serait pas saisie de cette question selon les modalités prévues par l'article L. 122-4 du code de l'action sociale et des familles ; que, par suite, en jugeant que la juridiction de l'aide sociale n'avait pas compétence pour connaître de la décision du président du conseil de Paris refusant de verser l'allocation compensatrice pour tierce personne à Mme F...et décidant de transmettre son dossier à un autre département, la commission centrale d'aide sociale a commis une erreur de droit ; <br/>
<br/>
              6. Considérant qu'il résulte de tout ce qui précède que les requérants sont fondés à demander l'annulation de la décision de la commission centrale d'aide sociale en tant seulement qu'elle porte sur le versement de l'allocation compensatrice pour tierce personne à Mme F...du 1er novembre 2007 à la date de son décès ;<br/>
<br/>
              Sur l'application des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 :<br/>
<br/>
              7. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à la charge des requérants, qui ne sont pas, dans la présente instance, la partie perdante ; qu'il n'y a pas lieu, dans les circonstances de l'espèce, de mettre à la charge du département de Paris et du département de la Seine-Saint-Denis la somme dont Mme E...F...et les autres requérants, qui ont obtenu le bénéfice de l'aide juridictionnelle, demandent le versement à leur avocat, au titre des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'article 1er de la décision de la commission centrale d'aide sociale du 3 février 2012, en tant qu'il annule la décision de la commission départementale d'aide sociale de Paris du 2 avril 2010 en ce qu'elle porte sur la période courant à compter du 1er novembre 2007, et l'article 2 de la même décision sont annulés.<br/>
Article 2 : L'affaire est renvoyée à la commission centrale d'aide sociale, dans la mesure de la cassation prononcée à l'article 1er.<br/>
Article 3 : Le surplus des conclusions du pourvoi est rejeté.<br/>
Article 4 : Les conclusions du département de la Seine-Saint-Denis présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée à Mme E...F..., première requérante dénommée, et aux départements de Paris et de la Seine-Saint-Denis.<br/>
Les autres requérants seront informés de la présente décision par la SCP Garreau, Bauer-Violas, avocat au Conseil d'Etat et à la Cour de cassation, qui les représente devant le Conseil d'Etat.<br/>
Copie en sera adressée pour information à la ministre des affaires sociales, de la santé et des droits des femmes.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">04-04-01-01 AIDE SOCIALE. CONTENTIEUX DE L'AIDE SOCIALE ET DE LA TARIFICATION. CONTENTIEUX DE L'ADMISSION À L'AIDE SOCIALE. COMMISSION CENTRALE D'AIDE SOCIALE. - RÈGLES DE SIGNATURE - SIGNATURE DU PRÉSIDENT - OBLIGATION - SIGNATURE DU RAPPORTEUR - FACULTÉ [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-06-04 PROCÉDURE. JUGEMENTS. RÉDACTION DES JUGEMENTS. - SIGNATURE D'UNE DÉCISION JURIDICTIONNELLE ADMINISTRATIVE - RÈGLES DE PROCÉDURE APPLICABLES SANS TEXTE - SIGNATURE DU PRÉSIDENT - OBLIGATION - SIGNATURE DU RAPPORTEUR - FACULTÉ [RJ1].
</SCT>
<ANA ID="9A"> 04-04-01-01 En vertu des règles générales de procédure applicables, même sans texte, à toute juridiction administrative, la minute d'une décision rendue par la commission centrale d'aide sociale doit au moins être revêtue de la signature du président de la formation de jugement aux fins d'en attester la conformité au délibéré. Rien ne fait par ailleurs obstacle à ce que la décision soit également signée par le rapporteur.</ANA>
<ANA ID="9B"> 54-06-04 En vertu des règles générales de procédure applicables, même sans texte, à toute juridiction administrative, la minute d'une décision rendue par la commission centrale d'aide sociale doit au moins être revêtue de la signature du président de la formation de jugement aux fins d'en attester la conformité au délibéré. Rien ne fait par ailleurs obstacle à ce que la décision soit également signée par le rapporteur.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., pour la procédure civile, CE, 16 janvier 1976, Perrimond et Ordre des avocats au barreau de Toulon et autres, n° 94169, p. 43.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
