<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032076968</ID>
<ANCIEN_ID>JG_L_2016_02_000000387586</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/07/69/CETATEXT000032076968.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème - 8ème SSR, 17/02/2016, 387586, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-02-17</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>387586</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème - 8ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>LE PRADO ; SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR>M. Christophe Pourreau</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emmanuelle Cortot-Boucher</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2016:387586.20160217</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, deux mémoires en réplique et quatre nouveaux mémoires, enregistrés les 2 février, 22 mai et 9 septembre 2015 et le 29 janvier 2016 au secrétariat du contentieux du Conseil d'Etat, la société Sodec et la SCI du Seignanx demandent au Conseil d'Etat d'annuler pour excès de pouvoir la décision n° 14-DCC-164 de l'Autorité de la concurrence du 13 novembre 2014 relative à la prise de contrôle exclusif par le groupe Carrefour d'un portefeuille de six galeries commerciales auprès de la société Unibail-Rodamco, en tant que cette décision porte sur le rachat de la galerie commerciale Bab 2 située à Bayonne.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
              Vu :<br/>
              - le code de commerce ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Christophe Pourreau, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Emmanuelle Cortot-Boucher, rapporteur public ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 9 février 2016, présentée par la société Sodec et la SCI du Seignanx ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier que, le 30 juillet 2014, la société Carmila, qui est contrôlée à titre exclusif par la société Carrefour, et la société Unibail-Rodamco ont annoncé le rachat par la société Carmila des galeries commerciales Bay 1 et Bay 2, situées en Ile-de-France, Bab 2, située à Bayonne, Labège 2, située à Toulouse, Cité Europe, située à Calais, et Place d'Arc, située à Orléans, jusque-là détenues ou codétenues par la société Unibail-Rodamco ; que, par une décision n° 14-DCC-164 du 13 novembre 2014, l'Autorité de la concurrence a autorisé cette opération, qui a le caractère d'une opération de concentration au sens du 2° du I de l'article L. 430-1 du code de commerce ; que la société Sodec et la SCI du Seignanx, qui développent le projet de centre commercial Les Allées Shopping, situé près de Bayonne, demandent l'annulation pour excès de pouvoir de la décision de l'Autorité de la concurrence en tant qu'elle a autorisé le rachat de la galerie commerciale Bab 2 ;<br/>
<br/>
              2. Considérant qu'il appartient à l'Autorité de la concurrence, à partir d'une analyse prospective tenant compte de l'ensemble des données pertinentes et en se fondant sur un scénario économique plausible, de caractériser les effets anticoncurrentiels d'une opération de concentration soumise à son autorisation et d'apprécier si ces effets sont de nature à porter atteinte au maintien d'une concurrence suffisante sur les marchés qu'elle affecte ;<br/>
<br/>
              3. Considérant qu'il ressort des pièces du dossier que l'opération de concentration contestée affecte le marché de la détention et de la gestion pour compte propre de grands centres commerciaux - entendus comme des centres commerciaux dont la surface commerciale utile est comprise entre 20 000 m² et 40 000 m² - dans l'agglomération bayonnaise, ainsi que le marché, plus large, de la détention et de la gestion pour compte propre de centres commerciaux dans cette agglomération ; qu'à la date de la décision attaquée, on comptait dans cette agglomération un seul grand centre commercial , Bab 2, d'une surface commerciale utile de 41 000 m², et deux petits centres commerciaux, Tarnos Océan, exploité par le groupe Carrefour, d'une surface commerciale utile de 6 500 m², et Leclerc Urrugne, exploité par le groupe Leclerc ; qu'étaient en outre prévues l'ouverture, au milieu de l'année 2015, du centre commercial régional Ametzondo, exploité par le groupe Ikea, d'une surface commerciale utile de 57 000 m², l'ouverture, fin 2015, du petit centre commercial Marinadour, exploité par le groupe Frey, d'une surface utile de 6 000 m², et l'ouverture, au cours de l'année 2016, du centre commercial régional Les Allées Shopping, d'une surface utile de 100 000 m² ;<br/>
<br/>
              Sur les effets concurrentiels horizontaux de l'opération :<br/>
<br/>
              4. Considérant que les sociétés requérantes soutiennent, en premier lieu, qu'en ne prenant pas en compte le projet d'extension de la galerie commerciale Bab 2, en prenant en compte, à l'inverse, le projet de centre commercial Azetmondo et, enfin, en estimant que le projet de centre commercial Les Allées Shopping n'était pas susceptible de concurrencer le centre commercial Bab 2, l'Autorité de la concurrence s'est fondée sur une analyse concurrentielle erronée du marché de la détention et de la gestion pour compte propre de centres commerciaux dans l'agglomération bayonnaise ;<br/>
<br/>
              5. Considérant, tout d'abord, qu'il ressort des pièces du dossier que, si le projet d'extension du centre commercial Bab 2 sur une surface commerciale utile d'environ 11 000 m² supplémentaires a bénéficié d'une autorisation de construire et d'une décision favorable de la Commission nationale d'aménagement commercial, cette autorisation et cette décision ont fait l'objet d'un recours en annulation devant le juge administratif ; que ces recours ont eu pour effet d'interrompre les travaux d'extension de la galerie commerciale, de sorte qu'à la date de la décision attaquée, ce projet n'était pas susceptible d'être achevé dans un délai suffisamment bref pour affecter la situation concurrentielle sur le marché de la détention et de la gestion pour compte propre de centres commerciaux dans l'agglomération bayonnaise ; qu'il ressort en tout état de cause des pièces du dossier que, eu égard à la surface commerciale utile considérée, ce projet n'était pas de nature à remettre en cause l'analyse concurrentielle ; que, par suite, le moyen tiré de ce que l'Autorité de la concurrence aurait commis une erreur de droit en ne prenant pas en compte ce projet dans l'analyse concurrentielle doit être écarté ;<br/>
<br/>
              6. Considérant, par ailleurs, qu'il ressort des pièces du dossier que, si le projet de modification de la répartition des surfaces intérieures de vente de la galerie commerciale du centre commercial Ametzondo a fait l'objet d'une décision négative de la Commission nationale d'aménagement commercial, contestée devant le juge administratif, cette décision ne remet pas en cause la réalisation du centre commercial et ne devrait pas retarder significativement la date de l'ouverture de la galerie commerciale ; qu'en outre, il ne ressort pas des pièces du dossier que la typologie des boutiques d'une galerie commerciale évolue de manière significative selon qu'elle est organisée autour d'une grande surface alimentaire ou autour d'une grande surface d'équipement de la maison ; qu'il ressort d'ailleurs des pièces du dossier que le centre commercial Ametzondo comprendra, non seulement un hypermarché Carrefour, mais aussi un nombre significatif de boutiques d'équipement de la personne et de restaurants ; que, par suite, le moyen tiré de ce que l'Autorité de la concurrence aurait commis une erreur de droit en prenant en compte le projet de centre commercial Azetmondo dans son analyse concurrentielle, au motif, d'une part, que sa réalisation ne serait pas certaine et, d'autre part, qu'il est organisé autour d'une grande surface d'équipement de la maison, doit être écarté ;<br/>
<br/>
              7. Considérant, enfin, que l'Autorité de la concurrence a mentionné au point 40 de sa décision, non pas que le centre commercial Les Allées Shopping n'était pas susceptible de concurrencer le centre commercial Bab 2, mais seulement qu'il n'était pas certain qu'il puisse ouvrir suffisamment rapidement pour constituer une source de pression concurrentielle sur le groupe Carrefour ; qu'en tout état de cause, la prise en compte de ce centre commercial, qui est développé par les sociétés requérantes, concurrentes du groupe Carrefour, n'aurait pu conduire qu'à une estimation inférieure du pouvoir de marché du groupe Carrefour à l'issue de l'opération contestée ; que, dans ces conditions, le moyen tiré de ce que l'Autorité de la concurrence aurait commis une erreur de droit en ne prenant pas en compte le projet de centre commercial Les Allées Shopping dans son analyse concurrentielle ne peut qu'être écarté ;<br/>
<br/>
              8. Considérant que les sociétés requérantes soutiennent, en second lieu, que l'Autorité de la concurrence a commis une erreur d'appréciation en estimant que l'opération n'avait pas d'effets anticoncurrentiels horizontaux excessifs sur le marché de la détention et de la gestion pour compte propre de centres commerciaux dans l'agglomération bayonnaise ;<br/>
<br/>
              9. Considérant qu'il ressort des pièces du dossier que l'opération de concentration contestée avait pour effet de substituer le groupe Carrefour à la société Unibail-Rodamco comme acteur dominant sur le segment de marché des grands centres commerciaux ; que, par ailleurs, sur le marché regroupant tous les centres commerciaux de l'agglomération bayonnaise, l'ouverture du centre commercial Ametzondo, dont la surface commerciale utile est supérieure à la surface commerciale utile cumulée des centres commerciaux Bab 2 et Tarnos Océan, devait avoir pour effet, à brève échéance, de faire perdre au groupe Carrefour sa place de leader, alors même qu'un hypermarché Carrefour devait être présent dans ce centre ; que, par suite, en estimant que l'opération de concentration ne faisait pas obstacle au maintien d'une concurrence suffisante sur le marché de la détention et de la gestion pour compte propre de centres commerciaux dans l'agglomération bayonnaise, l'Autorité de la concurrence n'a pas commis d'erreur d'appréciation ;<br/>
<br/>
              Sur les autres effets concurrentiels de l'opération :<br/>
<br/>
              10. Considérant qu'il ressort des pièces du dossier que les enseignes commerciales nationales, notamment les chaînes de magasins en franchise, ont un pouvoir de négociation important lié à l'effet de leur présence sur le niveau de fréquentation d'une galerie commerciale et sur l'orientation des visiteurs vers l'hypermarché autour duquel elle est organisée ; qu'il ne ressort pas des pièces du dossier que le groupe Carrefour aurait, du fait de l'opération contestée, le pouvoir de dissuader des enseignes commerciales de s'implanter dans des centres commerciaux concurrents ; qu'il ressort d'ailleurs des pièces du dossier que, sur les vingt-cinq enseignes commerciales qui ont annoncé ouvrir une boutique dans le centre commercial Ametzondo, huit disposent d'une boutique dans le centre commercial Bab 2 ; qu'il ne ressort pas des pièces du dossier que le rachat de la galerie commerciale Bab 2 pourrait dissuader le groupe Auchan d'ouvrir un hypermarché dans le centre commercial Les Allées Shopping, ni que l'exploitation par le groupe Carrefour d'un hypermarché dans le futur centre commercial Ametzondo lui permettrait d'influer sur le choix, par le groupe Ikea, des enseignes implantées dans la galerie commerciale qui l'entoure ; que, par suite, l'Autorité de la concurrence n'a pas commis d'erreur d'appréciation en ne retenant pas que, du fait de la présence du groupe Carrefour sur les marchés de la détention et de la gestion pour compte propre de centres commerciaux dans l'agglomération bayonnaise et de la distribution dans les galeries commerciales de cette même agglomération, l'opération de concentration présentait le risque anticoncurrentiel d'un verrouillage de l'accès des chaînes de distribution de biens alimentaires et de biens d'équipement aux centres commerciaux de l'agglomération bayonnaise ;<br/>
<br/>
              11. Considérant qu'il résulte de tout ce qui précède que la société Sodec et la SCI du Seignanx ne sont pas fondées à demander l'annulation pour excès de pouvoir de la décision de l'Autorité de la concurrence du 13 novembre 2014 relative à la prise de contrôle exclusif par le groupe Carrefour d'un portefeuille de six galeries commerciales auprès de la société Unibail-Rodamco en tant qu'elle a autorisé le rachat de la galerie commerciale Bab 2 ;<br/>
<br/>
              Sur les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              12. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la société Sodec et de la SCI du Seignanx une somme globale de 6 000 euros à verser à parts égales à la société Carrefour et à la société Unibail-Rodamco au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de la société Sodec et de la SCI du Seignanx est rejetée.<br/>
Article 2 : La société Sodec et la SCI du Seignanx verseront à parts égales à la société Carrefour et à la société Unibail-Rodamco la somme globale de 6 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée à la société Sodec et à la SCI du Seignanx, à la société Carrefour, à la société Unibail-Rodamco et à l'Autorité de la concurrence.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
