<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038244632</ID>
<ANCIEN_ID>JG_L_2019_03_000000414740</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/24/46/CETATEXT000038244632.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème - 9ème chambres réunies, 18/03/2019, 414740, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-03-18</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>414740</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème - 9ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FOUSSARD, FROGER ; SCP ZRIBI, TEXIER</AVOCATS>
<RAPPORTEUR>M. Arno Klarsfeld</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Aurélie Bretonneau</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:414740.20190318</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M.C..., Mme D...et M. A...B...ont demandé à la Cour nationale du droit d'asile d'annuler les décisions du 30 novembre 2016 par lesquelles l'Office français de protection des réfugiés et apatrides a rejeté leur demande d'asile et de leur reconnaître la qualité de réfugié ou, à défaut, de leur accorder le bénéfice de la protection subsidiaire. Par une décision n° 17011295, 17011296, 17011297 du 24 juillet 2017, la Cour nationale du droit d'asile leur a reconnu la qualité de réfugié.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 29 septembre et 29 décembre 2017 au secrétariat du contentieux du Conseil d'Etat, l'Office français de protection des réfugiés et apatrides demande au Conseil d'Etat d'annuler cette décision.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la convention de Genève du 28 juillet 1951 relative au statut des réfugiés et le protocole signé à New York le 31 janvier 1967 ;<br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Arno Klarsfeld, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Aurélie Bretonneau, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Foussard, Froger, avocat de l'Office français de protection des réfugiés et apatrides et à la SCP Zribi, Texier, avocat de M.C..., Mme D...et M. A...B...;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Par une décision du 30 novembre 2016, l'Office français de protection des réfugiés et apatrides a refusé de reconnaître à M.C..., à son épouse Mme D...et à son frère M. A...B..., tous trois de nationalité irakienne, la qualité de réfugié. Par une décision du 24 juillet 2017, contre laquelle l'Office français de protection des réfugiés et apatrides se pourvoit en cassation, la Cour nationale du droit d'asile leur a reconnu cette qualité à raison des risques de persécution auxquels ils seraient exposés en cas de retour en Irak, du fait des opinions politiques qui leur sont imputées.<br/>
<br/>
              2. Aux termes des stipulations du 2° du paragraphe A de l'article 1er de la convention de Genève relative au statut des réfugiés, la qualité de réfugié est reconnue à " toute personne qui, craignant avec raison d'être persécutée du fait de sa race, de sa religion, de sa nationalité, de son appartenance à un certain groupe social ou de ses opinions politiques, se trouve hors du pays dont elle a la nationalité et qui ne peut ou, du fait de cette crainte, ne veut se réclamer de la protection de ce pays " et aux termes de celles du paragraphe F du même article : " Les dispositions de cette Convention ne seront pas applicables aux personnes dont on aura des raisons sérieuses de penser : a) qu'elles ont commis un crime contre la paix, un crime de guerre ou un crime contre l'humanité, au sens des instruments internationaux élaborés pour prévoir des dispositions relatives à ces crimes ; b) qu'elles ont commis un crime grave de droit commun en dehors du pays d'accueil avant d'y être admises comme réfugiés ; c) qu'elles se sont rendues coupables d'agissements contraires aux buts et aux principes des Nations Unies ".<br/>
<br/>
              3. Il ressort des énonciations de la décision attaquée que, pour reconnaître à M. B... la qualité de réfugié, la Cour nationale du droit d'asile a notamment relevé qu'il s'était engagé en janvier 2013 au sein de la " brigade cinq " des peshmergas regroupant des combattants kurdes de différentes factions, que cet engagement impliquait l'adhésion à une certaine idéologie et qu'il s'était vu dispenser un enseignement politique. En retenant, au vu de l'ensemble des circonstances particulières de l'espèce, tenant aux conditions de l'enrôlement de l'intéressé et de son maintien au sein de la brigade en cause, dans un contexte de rivalité entre combattants kurdes, qu'il pouvait être tenu pour établi que cet engagement traduisait l'expression d'une opinion politique et que, par ailleurs, la demande de protection que l'intéressé avait présentée à sa hiérarchie en raison de menaces émanant de combattants de " l'Etat islamique " avait été assimilée à un acte de désobéissance politique, sa démarche ayant donné lieu à des accusations de trahison de la part de certains membres de sa hiérarchie, la Cour nationale du droit d'asile s'est livrée à une appréciation souveraine des faits de l'espèce qui n'est pas entachée de dénaturation. La Cour a, en outre, souverainement relevé que l'intéressé avait été identifié par des combattants de " l'Etat islamique " à raison de ses faits d'armes au sein des peshmergas et qu'il pouvait être tenu pour établi que les menaces de mort qui lui avaient été adressées étaient également animées par un motif politique. L'appréciation portée par la Cour sur le caractère fondé de ses craintes de persécution en cas de retour en Irak, tant à l'égard de l'armée kurde que des combattants de " l'Etat islamique ", est exempte de contradiction de motifs, de dénaturation et d'erreur de droit au regard des stipulations du 2° du A de l'article 1er de la convention de Genève. <br/>
<br/>
              4. Si, par ailleurs, il résulte de ses propres déclarations que M. B...admettait avoir pris part à des combats et avoir publié sur les réseaux sociaux, depuis son compte personnel, des photographies le mettant en scène à côté du cadavre d'un combattant de l' " Etat islamique ", la cour a souverainement relevé qu'il avait posé pour cette photographie et en avait assuré la diffusion sur ordre de sa hiérarchie, sans qu'il ait jamais été soutenu qu'il aurait pris part lui-même à des exactions. Au vu de ces constatations souveraines, la Cour a pu, sans entacher sa décision d'erreur de qualification juridique, estimer qu'il n'existait pas de raisons sérieuses de penser que M. B...se serait rendu coupable d'un crime grave de droit commun ou d'agissements contraires aux buts et aux principes des Nations unies et que ne pouvaient, en conséquence, lui être opposées les clauses d'exclusion résultant du b) ou du c) du paragraphe F de l'article 1er de la convention de Genève. Dès lors que l'application de ces clauses d'exclusion n'avait été soulevée par aucune partie devant la Cour nationale du droit d'asile, la Cour a pu, sans commettre d'erreur de droit, statuer sur les demandes dont elle était saisie sans se prononcer expressément sur l'application de ces clauses. <br/>
<br/>
              5. Enfin, la Cour n'a pas commis d'erreur de droit et a suffisamment motivé sa décision en se fondant sur les menaces de mort adressées à l'épouse et au frère de M.C..., vivant avec lui, pour juger que Mme D...et M. A...B...craignaient avec raison d'être persécutés en cas de retour dans leur pays et étaient, par suite, fondés à se voir reconnaître la qualité de réfugié.<br/>
<br/>
              6. Il résulte de ce qui précède que l'Office français de protection des réfugiés et apatrides n'est pas fondé à demander l'annulation de la décision qu'il attaque.<br/>
<br/>
              7. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Office français de protection des réfugiés et apatrides la somme de 3 000 euros à verser à la SCP Zribi, Texier, avocat des défendeurs, au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991, sous réserve que cette société renonce à percevoir la somme correspondant à la part contributive de l'Etat.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : Le pourvoi de l'Office français de protection des réfugiés et apatrides est rejeté.<br/>
<br/>
Article 2 : L'Office français de protection des réfugiés et apatrides versera la somme de 3 000 euros à la SCP Zribi, Texier au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991, sous réserve que cette société renonce à percevoir la somme correspondant à la part contributive de l'Etat.<br/>
Article 3 : La présente décision sera notifiée à l'Office français de protection des réfugiés et apatrides et à M.C..., Mme D...et M. A...B....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
