<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043890799</ID>
<ANCIEN_ID>JG_L_2021_07_000000439236</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/89/07/CETATEXT000043890799.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème chambre, 30/07/2021, 439236, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-07-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>439236</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP CELICE, TEXIDOR, PERIER</AVOCATS>
<RAPPORTEUR>Mme Flavie Le Tallec</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:439236.20210730</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête sommaire et un mémoire complémentaire, enregistrés les 2 mars et 17 juillet 2020 au secrétariat du contentieux du Conseil d'Etat, la commune de Châtel-Guyon et la communauté d'agglomération Riom Limagne et Volcans demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret n° 2019-1577 du 30 décembre 2019 pris pour l'application du III de l'article L. 302-5 du code de la construction et de l'habitation, en tant qu'il ne mentionne pas la commune de Châtel-Guyon ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 8 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de la construction et de l'habitation ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Flavie Le Tallec, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Célice, Texidor, Perier, avocat de la commune de Châtel-Guyon et de la communauté d'agglomération Riom Limagne et Volcans.<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. Aux termes du III de l'article L. 302-5 du code de la construction et de l'habitation : " Un décret fixe, au moins au début de chacune des périodes triennales mentionnées au I de l'article L. 302-8, la liste des communes appartenant aux agglomérations ou aux établissements publics de coopération intercommunale à fiscalité propre mentionnés au I du présent article, pour lesquelles la présente section n'est pas applicable ". La commune de Châtel-Guyon (Puy-de-Dôme) et la communauté d'agglomération Riom Limagne et Volcans demandent l'annulation du décret du 31 décembre 2019 pris sur le fondement de ces dispositions, en tant qu'il ne mentionne pas la commune de Châtel-Guyon dans la liste des communes exemptées des obligations de construction de logements sociaux. <br/>
<br/>
              Sur la légalité externe :<br/>
<br/>
              2. Les dispositions du III de l'article L. 302-5 du code de la construction et de l'habitation prévoient que la liste des communes exemptées par décret de leurs obligations relatives aux logements sociaux " est arrêtée sur proposition des établissements publics de coopération intercommunale auxquels elles appartiennent, après avis du représentant de l'Etat dans la région et de la commission nationale mentionnée aux II et III de l'article L. 302-9-1-1 ".<br/>
<br/>
              3. Il ressort des pièces du dossier que le préfet de la région Auvergne-Rhône-Alpes et la commission nationale mentionnée aux II et III de l'article L. 302-9-1-1 du code de la construction et de l'habitation ont émis, respectivement le 24 octobre 2019 et le 22 novembre 2019, leurs avis sur la proposition d'exemption de la commune de Châtel-Guyon qui avait été formulée le 24 septembre précédent par la communauté d'agglomération de Riom Limagne et Volcans et que ces avis ont été communiqués au Premier ministre et à la ministre de la cohésion des territoires et des relations avec les collectivités territoriales dans un délai leur permettant d'en tenir compte. Par suite, alors même que ces deux avis ne sont pas visés par le décret attaqué, le moyen tiré de ce qu'il n'aurait pas été précédé des avis requis par les dispositions rappelées au point précédent doit être écarté.<br/>
<br/>
              Sur la légalité interne :<br/>
<br/>
              4. Le même III de l'article L. 302-5 du code de la construction et de l'habitation dispose que la liste des communes exemptées par décret de leurs obligations relatives aux logements sociaux "  ne peut porter que sur des communes situées hors d'une agglomération de plus de 30 000 habitants et insuffisamment reliées aux bassins d'activités et d'emplois par le réseau de transports en commun, dans des conditions définies par le décret mentionné au premier alinéa du II du présent article, ou situées dans une agglomération de plus de 30 000 habitants dans laquelle le nombre de demandes de logements sociaux par rapport au nombre d'emménagements annuels, hors mutations internes dans le parc locatif social, se situe en-deçà d'un seuil fixé par ce même décret, ou sur des communes dont plus de la moitié du territoire urbanisé est soumis à une inconstructibilité (...) ". Pour déterminer si une commune est, pour l'application de ces dispositions, située hors d'une agglomération de plus de 30 000 habitants, il y a lieu de se référer à la notion d' " unité urbaine " retenue par l'Institut national de la statistique et des études économiques (INSEE), l'unité urbaine, telle que définie par l'INSEE à la date du décret attaqué, étant une commune ou un ensemble de communes présentant une zone de bâti continu, c'est-à-dire ne comportant pas de coupure de plus de 200 mètres entre deux constructions, qui compte au moins 2 000 habitants. <br/>
<br/>
              5. En premier lieu, si les requérantes soutiennent qu'il existe, entre la commune de Châtel-Guyon et chacune des communes de Riom, Enval et Mozac qui font partie de l'unité urbaine de Riom une distance entre les bâtis de plus de 200 mètres, il ressort des pièces du dossier, en particulier des documents annexés à la délibération du 24 septembre 2019 par laquelle la communauté d'agglomération de Riom Limagne Volcans a sollicité l'exemption de la commune de Châtel-Guyon, qu'il existe entre ces localités diverses constructions qui font obstacle à ce que soit constatée une coupure d'urbanisation de plus de 200 mètres, notamment entre la commune de Châtel-Guyon et celle d'Enval. Par suite, dès lors qu'il est constant que l'unité urbaine de Riom comporte plus de 30 000 habitants, les requérantes ne sont pas fondées à soutenir que la commune de Châtel-Guyon aurait dû figurer dans le décret attaqué au titre des communes qui sont situées hors d'une agglomération de plus de 30 000 habitants. <br/>
<br/>
              6. En deuxième lieu, par voie de conséquence de ce qui précède, les requérantes ne peuvent utilement soutenir que la commune de Châtel-Guyon n'est pas suffisamment reliée aux bassins d'activité et d'emploi par les services de transport public urbain, un tel critère d'exemption n'étant, aux termes des dispositions du III de l'article L. 302-5 du code de la construction et de l'habitation citées ci-dessus, applicable qu'aux seules communes situées hors d'une agglomération de plus de 30 000 habitants. <br/>
<br/>
              7. Enfin, les conditions posées par les dispositions du III de l'article L. 302-5 du code de la construction et de l'habitation s'imposant au pouvoir réglementaire, les requérantes, qui ne contestent pas que la commune de Châtel-Guyon ne remplissait pas les conditions d'exemption prévues par cet article pour les communes situées au sein d'une agglomération de plus de 30 000 habitants, ne sont pas fondées à soutenir que le pouvoir réglementaire aurait dû, à cet autre titre, exempter la commune de Châtel-Guyon de ses obligations.<br/>
<br/>
              8. Il résulte de tout ce qui précède que la commune de Châtel-Guyon et la communauté d'agglomération Riom Limagne et Volcans ne sont pas fondées à demander l'annulation du décret qu'elles attaquent. Leur requête doit, par suite, être rejetée, y compris, par voie de conséquence, ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de la commune de Châtel-Guyon et de la communauté d'agglomération Riom Limagne et Volcans est rejetée.<br/>
<br/>
Article 2 : La présente décision sera notifiée à la commune de Châtel-Guyon, à la communauté d'agglomération Riom Limagne et Volcans, à la ministre de la transition écologique et au Premier ministre.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
