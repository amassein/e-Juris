<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042175701</ID>
<ANCIEN_ID>JG_L_2020_07_000000432102</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/17/57/CETATEXT000042175701.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème chambre, 29/07/2020, 432102, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-07-29</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>432102</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP THOUVENIN, COUDRAY, GREVY</AVOCATS>
<RAPPORTEUR>M. Matias de Sainte Lorette</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Marie-Astrid Nicolazo de Barmon</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2020:432102.20200729</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. C... B... et Mme E... D... A... ont demandé au juge des référés du tribunal administratif de Lyon d'ordonner la suspension de l'exécution du commandement de payer valant saisie qui leur a été signifié le 4 janvier 2019 par le comptable du pôle de recouvrement spécialisé du Rhône pour le recouvrement de la somme de 378 244,51 euros, correspondant à des cotisations d'impôt sur le revenu et des pénalités auxquelles ils ont été assujettis au titre des années 2011 à 2013.<br/>
<br/>
              Par une ordonnance n° 1904571 du 14 juin 2019, le juge des référés du tribunal administratif de Lyon a rejeté leur demande.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un nouveau mémoire enregistrés les 1er et 16 juillet 2019 et le 3 juin 2020 au secrétariat du contentieux du Conseil d'Etat, M. B... et Mme D... A... demandent au Conseil d'État :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) de faire droit à leur demande de suspension ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 4 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Matias de Sainte Lorette, maître des requêtes,<br/>
<br/>
              - les conclusions de Mme Marie-Astrid Nicolazo de Barmon, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Thouvenin, Coudray, Grevy, avocat de M. C... B... et de Mme E... D... A... ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision ". L'article L. 522-3 de ce code prévoit que le juge des référés peut rejeter une requête par une ordonnance motivée, sans instruction contradictoire ni audience publique, lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable ou qu'elle est mal fondée.<br/>
<br/>
              2. Il résulte des énonciations de l'ordonnance attaquée que le juge des référés a rejeté la demande de suspension du commandement de payer valant saisie immobilière qui a été signifié aux requérants le 4 janvier 2019 pour le recouvrement de la somme de 378 244 euros, correspondant aux cotisations d'impôt sur le revenu mises à leur charge au titre des années 2011, 2012 et 2013, en l'absence de doute sérieux sur la légalité de l'obligation de payer résultant de ce commandement. Il a jugé que s'il résultait de l'instruction que le commandement en litige avait été émis pour le recouvrement d'impositions mises en recouvrement le 31 octobre 2015, par un rôle portant le numéro 929 A établi le 27 octobre 2015, il n'en résultait pas que ce rôle devait être regardé comme ayant été rapporté par le rôle portant le numéro 933 A établi le 11 décembre 2015, qui ne mettait aucune nouvelle imposition supplémentaire à la charge des requérants au titre de l'impôt sur le revenu pour les années 2011, 2012 et 2013. En statuant ainsi alors d'une part que ces rôles ne figuraient pas au dossier et d'autre part, que les deux avis d'imposition produits par les requérants et émis sur la base du rôle portant le numéro 933 A, faisaient état, au titre des impositions supplémentaires dues au titre des années 2012 et 2013, d'un montant à payer de 0 euro et portaient la mention " complète et remplace " les avis précédents, le juge des référés a insuffisamment motivé sa décision. Son ordonnance doit, pour ces motifs, être annulée.<br/>
<br/>
              3. Il y a lieu, dans les circonstances de l'espèce, de statuer sur la demande en référé, en application des dispositions de l'article L. 821-2 du code de justice administrative.<br/>
<br/>
              4. Ainsi qu'il a été dit au point 2 ci-dessus, il résulte de l'instruction que les avis d'imposition relatifs à l'impôt sur le revenu et aux prélèvements sociaux au titre des années 2012 et 2013, émis sur le fondement du rôle n° 933 A établi le 11 décembre 2015, font état d'un montant à payer fixé à 0 euro et portent la mention " le présent avis complète et remplace le précédent ". Par suite, il existe, en l'état de l'instruction et au regard des pièces produites devant le juge des référés, un doute sérieux sur la légalité de l'obligation de payer résultant du commandement de payer en litige à hauteur de la somme de 321 177,51 euros.<br/>
<br/>
              5. Pour vérifier si la condition d'urgence est satisfaite, le juge des référés doit apprécier la gravité des conséquences que pourraient entraîner, à brève échéance, l'obligation pour le contribuable de payer sans délai l'imposition ou les mesures mises en oeuvre par l'administration ou susceptibles de l'être pour le recouvrement de cette imposition, eu égard aux capacités du contribuable à acquitter les sommes qui lui sont demandées.<br/>
<br/>
              6. En l'espèce, les requérants font valoir que le commandement de payer en litige vaut saisie du bien immobilier dans lequel ils résident à titre principal. Il ne résulte pas de l'instruction qu'ils disposeraient, en plus de ce bien, d'un patrimoine susceptible de leur permettre d'acquitter le montant en litige. Le risque de dépossession ainsi évoqué doit dès lors être regardé comme pouvant entraîner des conséquences d'une ampleur telle qu'elle serait de nature à caractériser une situation d'urgence au sens de l'article L. 521-1 du code de justice administrative.<br/>
<br/>
              7. Il résulte de tout ce qui précède que M. B... et Mme D... A... sont fondés à demander la suspension de l'exécution du commandement de payer valant saisie émis à leur encontre le 4 janvier 2019 à hauteur de la somme de 321 177,51 euros.<br/>
<br/>
              8. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'ordonnance du 14 juin 2019 du juge des référés du tribunal administratif de Lyon est annulée.<br/>
<br/>
Article 2 : L'exécution du commandement de payer valant saisie émis le 4 janvier 2019 est suspendue à hauteur de la somme de 321 177,51 euros.<br/>
<br/>
Article 3 : L'Etat versera à M. B... et Mme D... A... la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : La présente décision sera notifiée à M. C... B..., premier requérant dénommé, et au ministre de l'économie, des finances et de la relance.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
