<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034081876</ID>
<ANCIEN_ID>JG_L_2017_02_000000397872</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/08/18/CETATEXT000034081876.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème - 8ème chambres réunies, 24/02/2017, 397872, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-02-24</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>397872</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème - 8ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BORE, SALVE DE BRUNETON</AVOCATS>
<RAPPORTEUR>M. Géraud Sajust de Bergues</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emmanuelle Cortot-Boucher</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:397872.20170224</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Le groupement agricole d'exploitation en commun (GAEC) des Marmottes a demandé au tribunal administratif de Clermont-Ferrand d'annuler la décision du 13 octobre 2011 par laquelle le préfet du Puy-de-Dôme a appliqué un taux de 20 % de réduction des aides soumises aux règles de la conditionnalité au titre de la campagne 2010 et prélevé le montant correspondant sur les aides versées au titre de la campagne 2011. Par un jugement n° 1300632 du 4 décembre 2014, le tribunal administratif de Clermont-Ferrand a rejeté cette demande.<br/>
<br/>
              Par un arrêt n° 15LY00393 du 12 janvier 2016, la cour administrative d'appel de Lyon a rejeté l'appel formé par le GAEC des Marmottes contre ce jugement.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 14 mars et 13 juin 2016 au secrétariat du contentieux du Conseil d'Etat, le GAEC des Marmottes demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat une somme de 4 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le règlement (CE) n° 73/2009 du Conseil, du 19 janvier 2009, établissant des règles communes pour les régimes de soutien direct en faveur des agriculteurs dans le cadre de la politique agricole commune et établissant certains régimes de soutien en faveur des agriculteurs ;<br/>
              - le code rural et de la pêche maritime ;<br/>
              - la loi n° 79-587 du 11 juillet 1979 ;<br/>
              - la loi n° 2000-321 du 12 avril 2000 ;<br/>
              - l'arrêté du 14 septembre 2010 relatif à la mise en oeuvre de la conditionnalité au titre de 2010 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Géraud Sajust de Bergues, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Emmanuelle Cortot-Boucher, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Boré, Salve de Bruneton, avocat du Gaec Les Marmottes ;<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que, lors d'un contrôle sur place en matière de santé et de protection animales dont le groupement agricole d'exploitation en commun (GAEC) des Marmottes, qui exploite un élevage bovin, a fait l'objet le 4 février 2010, les contrôleurs de l'Agence de services et de paiement ont constaté que deux bovins portaient le même numéro d'identification sur chacune des quatre boucles d'oreille, en méconnaissance des règles d'identification individuelle des bovins. La constatation du non respect de ces règles a conduit le préfet du Puy-de-Dôme à adresser au GAEC une lettre en date du 13 octobre 2011, intitulée " Conditionnalité 2010 - Lettre contradictoire de fin d'instruction ", lui rappelant l'anomalie constatée en matière d'identification individuelle des bovins, l'informant de l'application d'un taux de réduction de 20 % à toutes les aides soumises à la conditionnalité perçues par le GAEC au titre de la campagne 2010 et lui donnant un délai de 14 jours ouvrables pour produire ses observations. Une somme de 7 002,84 euros a ensuite été prélevée sur le montant des aides versées au titre de l'année 2011. Le GAEC se pourvoit en cassation contre l'arrêt du 12 janvier 2016 par lequel la cour administrative d'appel de Lyon a rejeté son appel du jugement du tribunal administratif de Clermont-Ferrand du 4 décembre 2014 rejetant sa demande tendant à l'annulation de cette décision du préfet du Puy-de-Dôme du 13 octobre 2011.<br/>
<br/>
              2. D'une part, le règlement n° 73/2009 du 19 janvier 2009 établissant des règles communes pour les régimes de soutien direct en faveur des agriculteurs dans le cadre de la politique agricole commune et établissant certains régimes en faveur des agriculteurs, dispose, dans son article 24 relatif aux modalités applicables aux réductions et exclusions en cas de non-respect des règles de la conditionnalité : " (...)/ 3. En cas de non respect délibéré, le pourcentage de réduction ne peut, en principe, pas être inférieur à 20 % et peut aller jusqu'à l'exclusion totale du bénéfice d'un ou de plusieurs régimes d'aide et s'appliquer à une ou plusieurs années civiles ". Aux termes de l'article D. 615-59 du code rural et de la pêche maritime : " (...) Lorsqu'un cas de non-conformité intentionnelle est constaté, le taux de réduction est fixé à 20 % (...) Un arrêté du ministre chargé de l'agriculture précise les cas dans lesquels une non-conformité est présumée intentionnelle. ". Il résulte des mentions figurant dans le tableau de l'annexe II à l'arrêté du 14 septembre 2010 relatif à la mise en oeuvre de la conditionnalité au titre de 2010 que l'anomalie consistant dans le port, par au moins deux animaux, du même numéro sur chacune des quatre boucles, revêt un caractère intentionnel et que la remise en conformité n'est pas possible. <br/>
<br/>
              3. D'autre part, aux termes de l'article 24 de la loi du 12 avril 2000 relative aux droits des citoyens dans leurs relations avec les administrations en vigueur à la date de l'application du taux de réduction en cause : " Exception faite des cas où il est statué sur une demande, les décisions individuelles qui doivent être motivées en application des articles 1er et 2 de la loi du 11 juillet 1979 n'interviennent qu'après que la personne intéressée a été mise à même de présenter des observations écrites et, le cas échéant, sur sa demande, des observations orales. (...) ". Aux termes de l'article 1er de la loi du 11 juillet 1979 relative à la motivation des actes administratifs et à l'amélioration des relations entre l'administration et le public,  également en vigueur à la date du litige : " Les personnes physiques ou morales ont le droit d'être informées sans délai des motifs des décisions administratives individuelles défavorables qui les concernent./ A cet effet, doivent être motivées les décisions qui : / (...) imposent des sujétions ; / (...) retirent ou abrogent une décision créatrice de droits ; (...) ".  <br/>
<br/>
              4. La décision par laquelle l'autorité administrative compétente impose au bénéficiaire d'une aide agricole régie par un texte de l'Union européenne de reverser tout ou partie du montant d'une aide et y procède par prélèvement sur le montant d'aides versées au titre d'une campagne suivante a le caractère d'une décision défavorable retirant une décision créatrice de droit au sens de l'article 1er de la loi du 11 juillet 1979, en tant qu'elle retire une aide financière qui avait été précédemment octroyée à son bénéficiaire. Elle revêt aussi le caractère d'une décision imposant une sujétion, au sens des mêmes dispositions, en tant qu'elle assujettit l'opérateur économique concerné, selon des modalités qu'elle définit, à l'obligation de reverser l'aide. Ainsi une telle décision, alors même qu'elle ne revêt pas une portée punitive, doit être motivée et précédée d'une procédure contradictoire. <br/>
<br/>
              5. Il ressort des énonciations de l'arrêt attaqué que la cour administrative d'appel a estimé que le GAEC n'alléguait ni n'établissait qu'il n'aurait pas bénéficié d'un délai suffisant pour présenter ses observations avant l'intervention de la décision préfectorale contestée. Ce faisant, elle a méconnu la portée des écritures devant le juge du fond dès lors qu'avait été précisément invoquée la méconnaissance des dispositions citées au point 3 relatives à la nécessité pour la personne intéressée d'être préalablement mise à même de produire des observations. Elle a également méconnu les dispositions combinées de l'article 24 de la loi du 12 avril 2000 et de l'article 1er de la loi du 11 juillet 1979 dès lors qu'elle a elle-même relevé que la lettre en date du 13 octobre 2011 par laquelle le préfet du Puy-de-Dôme avait informé le GAEC de son intention de réduire de 20 % toutes les aides soumises à la conditionnalité perçues au titre de la campagne 2010, réduction appliquée sur le montant des aides versées au titre de la campagne 2011, et par laquelle le préfet du Puy-de-Dôme lui avait imparti un délai de 14 jours ouvrables pour produire ses observations, avait été notifiée au GAEC le 4 février 2013. Dès lors, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, le GAEC des Marmottes est fondé à demander l'annulation de l'arrêt qu'il attaque.<br/>
<br/>
              6. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat le versement au GAEC des Marmottes d'une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Lyon du 12 janvier 2016 est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Lyon.<br/>
Article 3 : L'Etat versera au GAEC des Marmottes la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article4 : La présente décision sera notifiée au GAEC des Marmottes et au ministre de l'agriculture, de l'agroalimentaire et de la forêt. <br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
