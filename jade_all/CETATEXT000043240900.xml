<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043240900</ID>
<ANCIEN_ID>JG_L_2021_02_000000429890</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/24/09/CETATEXT000043240900.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème chambre, 11/02/2021, 429890, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-02-11</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>429890</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GATINEAU, FATTACCINI, REBEYROL</AVOCATS>
<RAPPORTEUR>Mme Cécile Nissen</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Céline  Guibé</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:429890.20210211</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. F... D... a demandé au tribunal administratif de Grenoble de prononcer la décharge des cotisations supplémentaires d'impôt sur le revenu et de contributions sociales auxquelles il a été assujetti au titre de l'année 2009 ainsi que la décharge des rappels de taxe sur la valeur ajoutée mis à sa charge au titre de la période du 1er janvier au 31 décembre 2009. Par un jugement n° 1601047 du 9 mai 2017, le tribunal a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 17LY02599 du 18 décembre 2018, la cour administrative d'appel de Lyon a rejeté l'appel formé par M. D... contre ce jugement.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 17 avril et 15 juillet 2019 et le 3 juin 2020 au secrétariat du contentieux du Conseil d'Etat, M. D... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 4500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la convention du 9 septembre 1966 modifiée, conclue entre la France et la Suisse en vue d'éliminer les doubles impositions en matière d'impôts sur le revenu et sur la fortune et de prévenir la fraude et l'évasion fiscales ;<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - le code du travail ;<br/>
              - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme B... E..., maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme C... A..., rapporteure publique ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Gatineau, Fattaccini, Rebeyrol, avocat de M. D... ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. M. D... a fait l'objet d'un examen contradictoire de sa situation fiscale personnelle au titre de l'année 2009, à l'issue duquel l'administration fiscale a retenu qu'il exerçait de manière occulte une profession d'artiste humoriste en France. Par suite, elle a évalué d'office, sur le fondement du 2° de l'article L. 73 du livre des procédures fiscales, les bénéfices non commerciaux tirés de cette activité, qui ont fait l'objet d'une taxation d'office sur le fondement du 3° de l'article L. 66 du même livre. L'administration fiscale a également notifié des rappels de taxe sur la valeur ajoutée à raison de la taxe non déclarée sur les recettes tirées de son activité d'artiste. Ces redressements ont enfin été assortis de la majoration de 80 % pour activité occulte prévue par le c de l'article 1728 du code général des impôts. M. D... se pourvoit en cassation contre l'arrêt du 18 décembre 2018 par lequel la cour administrative de Lyon a rejeté son appel contre le jugement du 9 mai 2017 du tribunal administratif de Grenoble rejetant sa demande tendant à la décharge des cotisations supplémentaires d'impôt sur le revenu auxquelles il a été assujetti au titre de l'année 2009, et à la décharge des rappels de taxe sur la valeur ajoutée qui lui ont été réclamés au titre de la période du 1er janvier au 31 décembre 2009 ainsi que des pénalités correspondantes.<br/>
<br/>
              Sur l'arrêt en tant qu'il statue sur les redressements en matière de TVA :<br/>
<br/>
              2. Il ressort des pièces de la procédure devant la cour administrative d'appel de Lyon que M. D... a contesté le bien-fondé des rappels de taxe sur la valeur ajoutée mis à sa charge, tant dans sa requête introductive que dans son mémoire en réplique, quand bien même ce chef de redressement n'était pas mentionné dans le récapitulatif de ses conclusions. Par suite, en retenant que le requérant n'avait pas repris en appel ses conclusions tendant à la décharge des rappels de taxe sur la valeur ajoutée, la cour a méconnu le sens de ses écritures. M. D... est donc fondé à demander l'annulation de l'arrêt en tant qu'il porte sur les rappels de taxe sur la valeur ajoutée et les pénalités correspondantes.<br/>
<br/>
              Sur l'arrêt en tant qu'il statue sur les redressements en matière d'impôt sur le revenu et de contributions sociales :<br/>
<br/>
              En ce qui concerne les redressements liés aux recettes tirées des spectacles et de la vente de CD, DVD et vidéos :<br/>
<br/>
              3. Aux termes des dispositions de l'article L. 762-1 du code du travail en vigueur avant le 1er mai 2008 et codifiées depuis lors aux articles L. 7121-2, L. 7121-3 et L. 7121-4 du code du travail : " Tout contrat par lequel une personne physique ou morale s'assure, moyennant rémunération, le concours d'un artiste du spectacle en vue de sa production, est présumé être un contrat de travail dès lors que cet artiste n'exerce pas l'activité, objet de ce contrat, dans des conditions impliquant son inscription au registre du commerce. / Cette présomption subsiste quels que soient le mode et le montant de la rémunération, ainsi que la qualification donnée au contrat par les parties. Elle n'est pas non plus détruite par la preuve que l'artiste conserve la liberté d'expression de son art, qu'il est propriétaire de tout ou partie du matériel utilisé ou qu'il emploie lui-même une ou plusieurs personnes pour le seconder, dès lors qu'il participe personnellement au spectacle. Sont considérés comme artistes du spectacle, notamment, (...) le chansonnier ". Aux termes de l'article 79 du code général des impôts : " Les traitements, indemnités, émoluments, salaires, pensions et rentes viagères concourent à la formation du revenu global servant de base à l'impôt sur le revenu ". Le 1 de l'article 92 du même code dispose que : " Sont considérés comme provenant de l'exercice d'une profession non commerciale ou comme revenus assimilés aux bénéfices non commerciaux, les bénéfices des professions libérales, des charges et offices dont les titulaires n'ont pas la qualité de commerçants et de toutes occupations, exploitations lucratives et sources de profits ne se rattachant pas à une autre catégorie de bénéfices ou de revenus. "<br/>
<br/>
              4. Il ressort des pièces du dossier soumis aux juges du fond que l'administration a reconstitué les revenus tirés des recettes des spectacles de M. D... et les a imposés dans la catégorie des bénéfices non commerciaux en application de l'article 92 du code général des impôts, alors qu'aucune pièce du dossier ne permettait de renverser la présomption, résultant des dispositions de l'article L. 762-1, recodifié aux articles L. 7121-2, L. 7121-3 et L. 7121-4 du code du travail, selon laquelle les sommes reçues à raison de ses prestations devaient être regardées comme un salaire d'artiste du spectacle et imposées dans la catégorie des traitements et salaires. Il appartenait à la cour administrative d'appel de relever d'office ce moyen, qui ressortait des pièces du dossier. En s'abstenant de le faire, la cour a entaché son arrêt d'erreur de droit. Par voie de conséquence, elle a également commis une erreur de droit en confirmant le bien-fondé de l'imposition des produits de la vente de CD, DVD et vidéos, qu'elle a regardés comme des recettes accessoires aux revenus tirés des recettes de spectacle et imposables, comme telles, dans la même catégorie. M. D... est donc fondé à demander l'annulation de l'arrêt en tant qu'il porte sur les cotisations supplémentaires d'impôt sur le revenu et de contributions sociales résultant de l'assujettissement des recettes tirées des spectacles et de la vente de CD, DVD et vidéos au titre de l'année 2009 et les pénalités correspondantes. <br/>
<br/>
              S'agissant des redressements relatifs aux droits d'auteur :<br/>
<br/>
              5. Aux termes de l'article 1er de la convention conclue le 9 septembre 1966 entre la France et la Suisse en vue d'éviter les doubles impositions en matière d'impôt sur le revenu et sur la fortune : " La présente convention s'applique aux personnes qui sont des résidents d'un Etat contractant ou de chacun des deux Etats ". Aux termes de l'article 13 de cette convention : " 1. Les redevances provenant d'un Etat contractant et payées à un résident de l'autre Etat contractant sont imposables dans cet autre Etat. / 2. Toutefois, les redevances peuvent être imposées dans l'Etat contractant d'où elles proviennent et selon la législation de cet Etat, mais l'impôt ainsi établi ne peut excéder 5 p. cent du montant brut des redevances. / 3. Le terme " redevances " employé dans le présent article désigne les rémunérations de toute nature payées pour l'usage ou la concession de l'usage d'un droit d'auteur sur une oeuvre littéraire, artistique ou scientifique (...). Aux termes de l'article 19 de cette convention : " 1. (...) les revenus qu'un résident d'un Etat contractant tire de ses activités personnelles exercées dans l'autre Etat contractant en tant qu'artiste du spectacle, tel qu'un artiste de théâtre, de cinéma, de la radio ou de la télévision, ou qu'un musicien, ou en tant que sportif, sont imposables dans cet autre Etat. (...) ".<br/>
<br/>
              6. La cour a jugé que la circonstance que M. D... disposait d'un foyer d'habitation en Suisse et qu'il y ait été inscrit au rôle des contribuables et assujetti de manière illimitée aux impôts cantonal, communal et fédéral direct dans le canton de Genève n'était pas de nature à faire obstacle à l'application, aux revenus qu'il tire de son activité indépendante exercée en France, des stipulations précitées de l'article 19 de la convention, pour en déduire que ces revenus étaient imposables en France. En statuant par ces motifs, alors qu'en application de l'article 13 de la convention, cette circonstance était susceptible, en caractérisant l'existence d'un foyer fiscal en Suisse, de faire obstacle à l'imposition en France des revenus tirés de ses droits d'auteur, ou à défaut de limiter à 5 % de leur montant brut la part imposable en France, la cour a commis une erreur de droit. M. D... est donc fondé à demander l'annulation de l'arrêt en tant qu'il porte sur les cotisations supplémentaires d'impôt sur le revenu et de contributions sociales résultant de l'assujettissement des droits d'auteur et les pénalités correspondantes, sans qu'il soit besoin de se prononcer sur les moyens du pourvoi qui se rattachent à ce chef de redressement.<br/>
<br/>
              7. Il résulte de tout ce qui précède que M. D... est fondé à demander l'annulation de la totalité de l'arrêt qu'il attaque.<br/>
<br/>
              Sur les frais d'instance :<br/>
<br/>
              8. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 3 000 euros à verser à M. D... au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Lyon du 18 décembre 2018 est annulé.<br/>
<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Lyon.<br/>
<br/>
Article 3 : L'Etat versera à M. D... une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
Article 4 : La présente décision sera notifiée à M. F... D... et au ministre de l'économie, des finances et de la relance.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
