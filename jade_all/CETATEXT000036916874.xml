<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036916874</ID>
<ANCIEN_ID>JG_L_2018_05_000000416153</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/91/68/CETATEXT000036916874.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  6ème chambre jugeant seule, 16/05/2018, 416153, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-05-16</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>416153</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 6ème chambre jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MARLANGE, DE LA BURGADE ; SCP BRIARD</AVOCATS>
<RAPPORTEUR>M. Vivien David</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Julie Burguburu</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:416153.20180516</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Les associations " Libre Horizon ", " Belle Normandie Environnement ", " Groupement régional des associations de protection de l'environnement ", " Robin des Bois ", " Vent de colère - Fédération nationale ", " Société pour la protection des paysages et de l'esthétique de la France ", " Fédération environnement durable " et " D-Day Héritage " ont demandé à la cour administrative d'appel de Nantes d'annuler l'arrêté du 8 juin 2016 par lequel le préfet du Calvados a, sur le fondement de l'article L. 214-3 du code de l'environnement, autorisé la société " Eoliennes Offshore du Calvados " à construire et exploiter un parc éolien en mer au large de la commune de Courseulles-sur-Mer.<br/>
<br/>
              Par un arrêt n° 16NT03382 du 2 octobre 2017, la cour administrative d'appel de Nantes a rejeté leur requête.<br/>
<br/>
              Par un pourvoi et un mémoire complémentaire, enregistrés les 1er décembre 2017 et 26 février 2018 au secrétariat du contentieux du Conseil d'Etat, les associations " Libre Horizon ", " Belle Normandie Environnement ", " Robin des Bois ", " Société pour la protection des paysages et de l'esthétique de la France " et " Fédération environnement durable " demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à leur requête ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat et de la société " Eoliennes Offshore du Calvados " le versement d'une somme de 4 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la Constitution, notamment la Charte de l'environnement ;<br/>
              - le code de l'énergie ;<br/>
              - le code de l'environnement ;<br/>
              - le code de l'urbanisme ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Vivien David, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Julie Burguburu, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Marlange, de la Burgade, avocat de l'association Libre Horizon et autres ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 12 avril 2018, présentée par l'association " Libre Horizon " et autres.<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 822-1 du code de justice administrative : " Le pourvoi en cassation devant le Conseil d'Etat fait l'objet d'une procédure préalable d'admission. L'admission est refusée par décision juridictionnelle si le pourvoi est irrecevable ou n'est fondé sur aucun moyen sérieux ".<br/>
<br/>
              2. Pour demander l'annulation de l'arrêt de la cour administrative de Nantes qu'elles attaquent, l'association " Libre Horizon " et autres soutiennent qu'il est entaché :<br/>
              - d'une erreur de droit en ce qu'il juge que les éléments complémentaires destinés à combler les insuffisances de l'étude d'impact fournis par le pétitionnaire après l'avis rendu par l'autorité de l'Etat compétente en matière d'environnement n'ont pas à être soumis à cette même autorité ;<br/>
              - d'une insuffisance de motivation faute de répondre au moyen tiré de ce que ces éléments complémentaires sont d'une importance telle que l'autorité environnementale ne peut, en leur absence, rendre un avis sur la demande d'autorisation ;<br/>
              - d'une dénaturation des pièces du dossier et d'une erreur de droit en ce qu'il juge que l'étude d'impact n'est pas entachée d'insuffisance au regard des dispositions de l'article R. 214-6 du code de l'environnement, que l'ouverture de l'enquête publique a donné lieu à une publicité suffisante et que sa durée a été suffisante eu égard à la complexité du dossier ;<br/>
              - d'une erreur de droit et d'une erreur de qualification juridique en ce qu'il juge que l'autorisation d'exploiter prévue par l'article L. 311-1 du code de l'énergie est accordée selon des critères distincts de ceux appliqués en vue de la délivrance de l'autorisation environnementale prévue à l'article L. 214-3 du code de l'environnement, qu'elle ne constitue pas une mesure d'application de la décision d'attribution prise à l'issue de l'appel d'offres et que ces deux décisions ne participent pas d'une opération administrative unique ;<br/>
              - d'une dénaturation des pièces du dossier et d'une insuffisance de motivation en ce qu'il juge que les modifications intervenues dans l'actionnariat de la société attributaire de l'autorisation d'exploiter ne peuvent pas être invoquées à l'appui du recours exercé contre l'autorisation environnementale, et omet de répondre au moyen tiré de l'erreur manifeste d'appréciation commise par le préfet du Calvados compte tenu de la procédure engagée en vue de l'inscription au patrimoine mondial de l'UNESCO du site des plages du Débarquement incluant le cimetière marin et les vestiges subaquatiques ;<br/>
              - d'une erreur de droit en ce qu'il juge que l'autorisation litigieuse ne porte pas atteinte aux exigences de la sécurité civile, et que le site du parc éolien maritime ne se situe pas sur le littoral au sens de l'article L. 121-1 du code de l'urbanisme ;<br/>
              - d'une dénaturation des pièces du dossier en ce qu'il juge que le principe de précaution n'a pas été méconnu en l'espèce.<br/>
<br/>
              3. Aucun de ces moyens n'est de nature à permettre l'admission du pourvoi.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de l'association " Libre Horizon " et autres n'est pas admis.<br/>
Article 2 : La présente décision sera notifiée à l'association " Libre Horizon ", première dénommée, pour l'ensemble des requérants.<br/>
Copie en sera adressée au ministre d'Etat, ministre de la transition écologique et solidaire, et à la société " Eoliennes Offshore du Calvados ".<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
