<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026555866</ID>
<ANCIEN_ID>JG_L_2012_10_000000352892</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/55/58/CETATEXT000026555866.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème et 4ème sous-sections réunies, 29/10/2012, 352892, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2012-10-29</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>352892</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème et 4ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Jacques Arrighi de Casanova</PRESIDENT>
<AVOCATS/>
<RAPPORTEUR>M. Charles Touboul</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Fabienne Lambolez</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2012:352892.20121029</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu, 1° sous le n° 352892, la requête, enregistrée le 23 septembre 2011 au secrétariat du contentieux du Conseil d'Etat, présentée par M. Guillaume C, demeurant ... ; M. C demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret n° 2011-876 du 25 juillet 2011 revalorisant le montant de l'amende forfaitaire pour certaines contraventions prévues par le code de la route en matière d'arrêt et de stationnement ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 1 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Vu, 2° sous le n° 355633, la requête, enregistrée le 6 janvier 2012 au secrétariat du contentieux du Conseil d'Etat, présentée par M. Guillaume C, demeurant 28, boulevard Haussmann à Paris (75009) ; M. C demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision implicite de rejet résultant du silence gardé par le Premier ministre sur sa demande tendant à l'abrogation des articles R. 48-1 à R. 49-8 du code de procédure pénale ;<br/>
<br/>
              2°) d'enjoindre au Premier ministre de réexaminer sa demande d'abrogation ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 1 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu la Constitution ;<br/>
<br/>
              Vu la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
<br/>
              Vu le code de procédure pénale ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Charles Touboul, Maître des Requêtes,  <br/>
<br/>
<br/>
<br/>
              - les conclusions de Mme Fabienne Lambolez, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant que M. C, qui a demandé au Premier ministre d'abroger les articles R. 48-1 à R. 49-8 du code de procédure pénale, constituant le chapitre II bis, relatif aux amendes forfaitaires et aux amendes forfaitaires majorées, du titre III du Livre II de ce code, défère au Conseil d'Etat le refus qui lui a été opposé ; qu'il demande par ailleurs l'annulation du décret du 25 juillet 2011 revalorisant le montant de l'amende forfaitaire pour certaines contraventions prévues par le code de la route en matière d'arrêt et de stationnement, qui a modifié les dispositions de l'article R. 49 du code de procédure pénale ;<br/>
<br/>
              2. Considérant que les requêtes de M. C présentent à juger des questions semblables ; qu'il y a lieu de les joindre pour statuer par une seule décision ;<br/>
<br/>
              Sur la légalité externe :<br/>
<br/>
              3. Considérant qu'aux termes de l'article 34 de la Constitution : " La loi fixe les règles concernant (...) / - la détermination des crimes et délits ainsi que les peines qui leur sont applicables ; la procédure pénale...  " ; qu'aux termes de l'article 37 de la Constitution : " Les matières autres que celles qui sont du domaine de la loi ont un caractère réglementaire(...) " ; <br/>
<br/>
              4. Considérant qu'aux termes de l'article 529 du code de procédure pénale, dans sa rédaction en vigueur à la date des dispositions réglementaires contestées : " Pour les contraventions des quatre premières classes dont la liste est fixée par décret en Conseil d'Etat l'action publique est éteinte par le paiement d'une amende forfaitaire qui est exclusive de l'application des règles de la récidive... " ; qu'aux termes de l'article 530-3 du même code : " Un décret en Conseil d'Etat fixe le montant des amendes et indemnités forfaitaires, des amendes forfaitaires minorées et des amendes forfaitaires majorées ainsi que des frais de constitution de dossier et précise les modalités d'application du présent chapitre, en déterminant notamment les conditions dans lesquelles les agents habilités à constater les infractions sont assermentés et perçoivent le montant des amendes forfaitaires et celui des transactions " ;<br/>
<br/>
              5. Considérant que les articles R. 48-1 à R. 49-8 du code de procédure pénale sont relatifs à la liste des contraventions pour lesquelles l'action publique est éteinte par le paiement d'une amende forfaitaire, au montant des amendes forfaitaires, à l'avis de contravention et à la carte de paiement, à la perception du montant des amendes par l'agent verbalisateur, au règlement par timbre, chèque, télépaiement ou timbre dématérialisé, à la forme de la requête en exonération, aux mentions figurant sur le titre exécutoire, à sa transmission au comptable du Trésor et à l'envoi de l'extrait de ce titre au contrevenant ; qu'en adoptant ces dispositions et en modifiant, par le décret contesté, celles de l'article R. 49, le Premier ministre s'est borné à préciser les modalités d'application des règles fixées par le législateur au chapitre II bis du titre III du Livre II du code de procédure pénale, sans définir lui-même aucune règle concernant la procédure pénale ; que, par suite, le moyen tiré de ce qu'il aurait empiété sur le domaine réservé à la loi par l'article 34 de la Constitution ne peut qu'être écarté ;<br/>
<br/>
              6. Considérant qu'aucune disposition ni aucun principe n'imposait que le décret contesté énonce les motifs ayant présidé à son adoption ; qu'il en allait de même des décrets dont sont issus les articles R. 48-1 à R. 49-8 du code de procédure pénale dont l'abrogation a été demandée par le requérant ;<br/>
<br/>
              Sur la légalité interne : <br/>
<br/>
              7. Considérant qu'aux termes du paragraphe 1 de l'article 6 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales : " Toute personne a droit à ce que sa cause soit entendue équitablement, publiquement, et dans un délai raisonnable, par un tribunal indépendant et impartial, établi par la loi, qui décidera, soit des contestations sur ses droits et obligations de caractère civil, soit du bien-fondé de toute accusation en matière pénale dirigée contre elle(...) " ; <br/>
<br/>
              8. Considérant que si, ainsi qu'en dispose l'article 530-1 du code de procédure pénale, en cas de condamnation, l'amende prononcée ne peut être inférieure, selon le cas, au montant de l'amende forfaitaire ou à celui de l'amende forfaitaire majorée, cette règle ne fait pas peser sur les contrevenants une contrainte de nature à les priver de l'accès à un tribunal, le refus de paiement de l'amende ne les exposant pas à un risque tel qu'ils puissent être dissuadés de soumettre au juge une contestation relative à la réalité de l'infraction ; qu'il suit de là que le moyen tiré de ce que le décret attaqué et les articles R. 48-1 à R. 49-8 du code de procédure pénale dont l'abrogation a été demandée par le requérant méconnaîtraient les stipulations de l'article 6 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ne peut, en tout état de cause, qu'être écarté ; <br/>
<br/>
              9. Considérant que le moyen tiré de ce que les dispositions réglementaires contestées méconnaîtraient les stipulations de l'article 7 de la même convention n'est pas assorti des précisions qui permettraient d'en apprécier le bien-fondé ;<br/>
<br/>
              10. Considérant, enfin, que le détournement de pouvoir allégué n'est pas établi ;<br/>
<br/>
              11. Considérant qu'il résulte de tout ce qui précède que M. C n'est pas fondé à demander l'annulation de la décision et du décret qu'il attaque ; que doivent être rejetées, par voie de conséquence, ses conclusions à fin d'injonction ainsi que ses conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
		Article 1er : Les requêtes de M. C sont rejetées. <br/>
<br/>
Article 2 : La présente décision sera notifiée à M. Guillaume C, au Premier ministre et au garde des sceaux, ministre de la justice. <br/>
Copie en sera adressée au ministre de l'intérieur et au ministre de l'économie et des finances.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
