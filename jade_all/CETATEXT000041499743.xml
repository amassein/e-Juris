<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000041499743</ID>
<ANCIEN_ID>J2_L_2020_01_000001802368</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/41/49/97/CETATEXT000041499743.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>CAA de LYON, 2ème chambre, 28/01/2020, 18LY02368, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-01-28</DATE_DEC>
<JURIDICTION>CAA de LYON</JURIDICTION>
<NUMERO>18LY02368</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème chambre</FORMATION>
<TYPE_REC>fiscal</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. PRUVOST</PRESIDENT>
<AVOCATS>ARBOR TOURNOUD PIGNIER WOLF</AVOCATS>
<RAPPORTEUR>Mme Camille  VINET</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme TERRADE</COMMISSAIRE_GVT>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
Procédure contentieuse antérieure<br/>
<br/>
       M. et Mme B... A... ont demandé au tribunal administratif de Grenoble de prononcer la décharge des cotisations supplémentaires d'impôt sur le revenu et de prélèvements sociaux auxquelles ils ont été assujettis au titre de l'année 2009, ainsi que des pénalités correspondantes.<br/>
<br/>
       Par un jugement n° 1605409 du 17 mai 2018, le tribunal administratif de Grenoble après avoir constaté un non-lieu à statuer à hauteur du dégrèvement prononcé en cours d'instance, a rejeté le surplus des conclusions de la demande.<br/>
<br/>
Procédure devant la cour<br/>
<br/>
       Par une requête enregistrée le 27 juin 2018, et un mémoire complémentaire enregistré le 25 octobre 2019, M. A..., représenté par Me G..., demande à la cour :<br/>
<br/>
       1°) d'annuler ce jugement du tribunal administratif de Grenoble du 17 mai 2018 ;<br/>
<br/>
       2°) de prononcer la décharge des impositions et pénalités maintenues à sa charge ;<br/>
<br/>
       3°) de mettre à la charge de l'Etat une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
       M. A... soutient que la proposition de rectification du 29 août 2012 lui notifiant les impositions complémentaires litigieuses est insuffisamment motivée.<br/>
<br/>
       Par un mémoire en défense, enregistré le 14 décembre 2018, le ministre de l'action et des comptes publics conclut au rejet de la requête.<br/>
<br/>
       Le ministre de l'action et des comptes publics soutient que la proposition de rectification litigieuse était suffisamment motivée dès lors que M. A..., gérant et unique associé de la SARL Restaurant Le Saray a reçu la proposition de rectification adressée à cette société.<br/>
       Vu les autres pièces du dossier ;<br/>
<br/>
       Vu :<br/>
       - le code général des impôts et le livre des procédures fiscales ;<br/>
       - le code de justice administrative ;<br/>
<br/>
       Les parties ayant été régulièrement averties du jour de l'audience ;<br/>
<br/>
       Après avoir entendu au cours de l'audience publique :<br/>
<br/>
       - le rapport de Mme C..., première conseillère,<br/>
       - et les conclusions de Mme F..., rapporteure publique ;<br/>
<br/>
<br/>
<br/>
       Considérant ce qui suit :<br/>
<br/>
       1. La SARL Restaurant Le Saray, qui exerce une activité de vente de produits alimentaires, a fait l'objet, au cours de l'année 2012, d'une vérification de comptabilité à l'issue de laquelle M. A..., son gérant et unique associé, a été considéré comme le bénéficiaire de revenus distribués et assujetti à des compléments d'impôt sur le revenu et de prélèvements sociaux au titre de l'année 2009, assortis de majorations et intérêts de retard. M. et Mme A... relèvent appel du jugement du tribunal administratif de Grenoble en tant qu'il a rejeté le surplus des conclusions de leur demande tendant à la décharge de ces impositions et des pénalités correspondantes.<br/>
<br/>
       2. Aux termes de l'article L. 57 du livre des procédures fiscales : " L'administration adresse au contribuable une proposition de rectification qui doit être motivée de manière à lui permettre de formuler ses observations ou de faire connaître son acceptation. / (...) ". Aux termes de l'article R. 57-1 du même livre : " La proposition de rectification prévue par l'article L. 57 fait connaître au contribuable la nature et les motifs de la rectification envisagée. / (...) ". Il résulte de ces dispositions que l'administration doit indiquer au contribuable, dans la proposition de rectification, les motifs et le montant des rehaussements envisagés, leur fondement légal et la catégorie de revenus dans laquelle ils sont opérés, ainsi que les années d'imposition concernées. En cas de motivation par référence, l'administration doit, en principe, annexer les documents auxquels elle se réfère dans la proposition de rectification ou en reprendre la teneur.<br/>
<br/>
       3. En l'espèce, si la proposition de rectification adressée à M. A... le 29 août 2012, dont il a accusé réception le 31 août 2012, indique le fondement légal et la catégorie de revenus dans laquelle les rehaussements sont opérés, ainsi que l'année d'imposition concernée, elle se borne à indiquer que la SARL Restaurant Le Saray a fait l'objet d'une vérification de comptabilité ayant donné lieu à une proposition de rectification adressée le 6 août 2012 à cette société et que la reconstitution de son chiffre d'affaires a permis de constater des omissions de recettes d'un montant de 292 074 euros. Il est constant que la proposition de rectification adressée à la SARL Restaurant Le Saray, dont l'administration n'a pas repris la teneur dans la proposition de rectification adressée à M. A..., n'était pas annexée à cette dernière. La circonstance que l'administration a notifié à la SARL Restaurant Le Saray, à l'adresse de cette société, la proposition de rectification la concernant par un pli distribué le 27 août 2012, ne permet pas de regarder M. A..., quand bien-même il était le gérant de cette société, comme ayant eu nécessairement connaissance des motifs des rehaussements envisagés à la date à laquelle il a reçu, à son adresse personnelle, la proposition de rectification le concernant. Ainsi, la proposition de rectification notifiant à M. A... des compléments d'impôt sur le revenu et de prélèvements sociaux au titre de l'année 2009 ne comportait pas les éléments de motivation lui permettant de la contester utilement. Si la proposition de rectification adressée à la SARL Restaurant Le Saray a finalement été envoyée, le 15 septembre 2012, à M. A... à son adresse personnelle, suite à une demande de sa part par laquelle il sollicitait dans le même temps l'octroi d'un délai de réponse supplémentaire, l'administration s'est bornée à lui octroyer ce délai supplémentaire sans l'informer de ce qu'il bénéficiait, du fait de l'envoi différé de cet élément de motivation, d'un nouveau délai initial de réponse à la proposition de rectification. Cet envoi n'ayant ainsi pas eu pour effet de rétablir la faculté du débat contradictoire dont la notification insuffisamment motivée du 29 août 2012 avait privé M. A..., l'imposition a été établie suivant une procédure irrégulière.<br/>
<br/>
       4. Il résulte de ce qui précède que M. A... est fondé à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Grenoble a rejeté le surplus des conclusions de sa demande.<br/>
<br/>
       5. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 1 500 euros au titre des au titre des frais exposés par M. A... dans l'instance en application des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
DECIDE :<br/>
<br/>
<br/>
<br/>
Article 1er : M. A... est déchargé des cotisations supplémentaires d'impôt sur le revenu et de prélèvements sociaux auxquelles il a été assujetti au titre de l'année 2009, ainsi que des pénalités correspondantes.<br/>
Article 2 : L'article 2 du jugement du tribunal administratif de Grenoble du 17 mai 2018 est annulé.<br/>
Article 3 : L'Etat versera à M. A... une somme de1 500 euros au titre de l'article L. 761-1 du code de justice administratif.<br/>
Article 4 : Le présent arrêt sera notifié à M. B... A... et au ministre de l'action et des comptes publics.<br/>
<br/>
<br/>
<br/>
Délibéré après l'audience du 7 janvier 2020, à laquelle siégeaient :<br/>
<br/>
M. Pruvost, président de chambre,<br/>
Mme D..., présidente-assesseure,<br/>
Mme C..., première conseillère.<br/>
<br/>
<br/>
Lu en audience publique le 28 janvier 2020.<br/>
2<br/>
N° 18LY02368<br/>
		gt<br/>
<br/>
<br/>
<br/>
</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">19-04-01-02 Contributions et taxes. Impôts sur les revenus et bénéfices. Règles générales. Impôt sur le revenu.
</SCT>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
