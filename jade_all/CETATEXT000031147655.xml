<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031147655</ID>
<ANCIEN_ID>JG_L_2015_09_000000389293</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/14/76/CETATEXT000031147655.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère SSJS, 11/09/2015, 389293, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-09-11</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>389293</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère SSJS</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP CELICE, BLANCPAIN, SOLTNER, TEXIDOR</AVOCATS>
<RAPPORTEUR>M. Yannick Faure</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Rémi Decout-Paolini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2015:389293.20150911</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              Par un mémoire, enregistré le 30 juin 2015 au secrétariat du contentieux du Conseil d'Etat, présenté en application de l'article 23-5 de l'ordonnance n° 58-1067 du 7 novembre 1958, le Groupement d'employeurs Agriplus demande au Conseil d'Etat, à l'appui de son pourvoi tendant à l'annulation de l'arrêt n° 13MA02971 du 17 mars 2015 par lequel la cour administrative d'appel de Marseille a annulé partiellement le jugement du tribunal administratif de Montpellier n° 1102112 du 21 mai 2013 et rejeté sa demande d'annulation de la décision de la directrice régionale adjointe des entreprises, de la concurrence, de la consommation, du travail et de l'emploi de Languedoc-Roussillon du 9 décembre 2010 mettant à sa charge le versement d'une somme de 33 075 euros au titre de la pénalité relative au non-respect de l'obligation d'emploi des travailleurs handicapés, de renvoyer au Conseil constitutionnel la question de la conformité aux droits et libertés garantis par la Constitution des dispositions du deuxième alinéa de l'article L. 5212-3 du code du travail ainsi que des articles L. 1111-2, L. 5212-14 et L. 5212-2 du même code.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la Constitution, notamment son Préambule et son article 61-1 ;<br/>
              - l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
              - le code du travail ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Yannick Faure, auditeur,  <br/>
<br/>
              - les conclusions de M. Rémi Decout-Paolini, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Célice, Blancpain, Soltner, Texidor, avocat du Groupement d'employeurs Agriplus ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Aux termes du premier alinéa de l'article 23-5 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel : " Le moyen tiré de ce qu'une disposition législative porte atteinte aux droits et libertés garantis par la Constitution peut être soulevé, y compris pour la première fois en cassation, à l'occasion d'une instance devant le Conseil d'Etat (...) ". Il résulte des dispositions de ce même article que le Conseil constitutionnel est saisi de la question prioritaire de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux.<br/>
<br/>
              2. Aux termes de l'article L. 5212-2 du code du travail : " Tout employeur emploie, dans la proportion de 6 % de l'effectif total de ses salariés, à temps plein ou à temps partiel, des travailleurs handicapés, mutilés de guerre et assimilés, mentionnés à l'article L. 5212-13 ". Il résulte des dispositions de l'article L. 1111-2 du même code que l'obligation d'emploi de travailleurs handicapés, mutilés de guerre et assimilés qui incombe à un employeur est calculée par référence à l'effectif total de ses salariés, les salariés titulaires d'un contrat de travail à durée indéterminée à temps plein étant pris intégralement en compte dans l'effectif de l'entreprise et les salariés titulaires d'un contrat de travail à durée déterminée étant pris en compte à due proportion de leur temps de présence au cours des douze mois précédents, sauf en cas de remplacement d'un salarié absent ou dont le contrat de travail est suspendu. Si le second alinéa de l'article L. 5212-3 du code du travail prévoit que les entreprises de travail temporaire ne sont pas assujetties à l'obligation d'emploi pour les salariés qu'elles mettent temporairement à disposition d'un client utilisateur pour l'exécution d'une mission, aucune dérogation comparable n'est prévue, s'agissant des groupements d'employeurs, pour les salariés qu'ils mettent à disposition de leurs membres. Or l'article L. 5212-14 du même code prévoit en revanche que : " Pour le calcul du nombre de bénéficiaires de l'obligation d'emploi, chaque personne est prise en compte à due proportion de son temps de présence dans l'entreprise au cours de l'année civile, quelle que soit la nature ou la durée de son contrat de travail (...) ". Il résulte de la combinaison de ces dispositions que l'obligation d'emploi de travailleurs handicapés, mutilés de guerre et assimilés qui incombe à un groupement d'employeurs est calculée en rapportant le nombre des bénéficiaires de cette obligation, comptabilisés à due proportion de leur temps de présence dans le groupement, à l'effectif total des salariés de ce même groupement, y compris lorsqu'ils sont mis à disposition de leurs membres. <br/>
<br/>
              3. Ces dispositions sont applicables au présent litige au sens et pour l'application de l'article 23-5 de l'ordonnance du 7 novembre 1958. Elles n'ont pas déjà été déclarées conformes à la Constitution par le Conseil constitutionnel. Le moyen tiré de ce qu'elles portent atteinte aux droits et libertés garantis par la Constitution, notamment au principe d'égalité, soulève une question présentant un caractère sérieux. Ainsi, il y a lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité invoquée.<br/>
<br/>
<br/>
<br/>			D E C I D E :<br/>
              			--------------<br/>
Article 1er : La question de la conformité à la Constitution des articles L. 1111-2, L. 5212-2, L. 5212-14, dans sa rédaction issue de la loi n° 2008-1249 du 1er décembre 2008, ainsi que du second alinéa de l'article L. 5212-3 du code du travail est renvoyée au Conseil constitutionnel.<br/>
<br/>
Article 2 : Il est sursis à statuer sur le pourvoi du Groupement d'employeurs Agriplus jusqu'à ce que le Conseil constitutionnel ait tranché la question de constitutionnalité ainsi soulevée. <br/>
<br/>
Article 3 : La présente décision sera notifiée au Groupement d'employeurs Agriplus, au Premier ministre et à la ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social.<br/>
.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
