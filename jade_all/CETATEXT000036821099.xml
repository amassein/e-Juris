<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036821099</ID>
<ANCIEN_ID>JG_L_2018_03_000000418820</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/82/10/CETATEXT000036821099.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 23/03/2018, 418820, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-03-23</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>418820</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>BALAT</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2018:418820.20180323</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B...A...a demandé au juge des référés du tribunal administratif de la Guyane, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, d'une part, d'ordonner qu'il soit mis fin à l'atteinte grave et manifestement illégale portée à ses libertés fondamentales et, d'autre part, de prononcer à cet effet toutes les mesures nécessaires à l'encontre du préfet de la Guyane. Par une ordonnance n° 1800100 du 2 février 2018, le juge des référés du tribunal administratif de la Guyane a rejeté sa demande.<br/>
<br/>
              Par une requête enregistrée le 7 mars 2018 au secrétariat du contentieux du Conseil d'Etat, M. A...demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) de faire droit à sa demande de première instance ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Il soutient que :<br/>
              - l'ordonnance contestée est entachée, d'une part, d'un vice de forme dès lors qu'elle ne mentionne pas la date de l'audience et, d'autre part, d'irrégularités dès lors que le juge des référés du tribunal administratif de la Guyane n'a pas communiqué au requérant les pièces transmises par la préfecture de la Guyane avant la clôture de l'instruction ;<br/>
              - l'arrêté préfectoral du 16 janvier 2018, par lequel le préfet de la Guyane lui a retiré son agrément en qualité d'armurier, est entaché d'une erreur de fait, d'une insuffisance de motivation et d'une erreur de droit ;<br/>
              - le juge des référés du tribunal administratif de la Guyane a commis, d'une part, une erreur de droit et, d'autre part, une erreur de qualification juridique des faits dès lors qu'il est porté une atteinte grave et manifestement illégale à sa liberté d'entreprendre et à son droit à la présomption d'innocence par l'arrêté litigieux du préfet de la Guyane prononçant le retrait de son agrément d'armurier.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ; <br/>
<br/>
              Vu :<br/>
              - le code de la sécurité intérieure ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. Aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. Le juge des référés se prononce dans un délai de quarante-huit heures ". En vertu de l'article L. 522-3 du même code, le juge des référés peut rejeter une requête par une ordonnance motivée, sans instruction contradictoire ni audience publique, lorsque la condition d'urgence n'est pas remplie ou lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable ou qu'elle est mal fondée. A cet égard, il appartient au juge d'appel de prendre en considération les éléments recueillis par le juge du premier degré dans le cadre de la procédure écrite et orale qu'il a diligentée.<br/>
<br/>
              2. Par un arrêté en date du 16 janvier 2018, le préfet de la Guyane a retiré l'agrément d'armurier qu'il a délivré à M. A...par un arrêté du 31 mai 2012. M. A... a saisi le juge des référés du tribunal administratif de la Guyane, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, d'une demande tendant, d'une part, à ordonner qu'il soit mis fin à l'atteinte grave et manifestement illégale portée à son droit à la présomption d'innocence et à sa liberté d'entreprendre et, d'autre part, de prononcer à cet effet toutes les mesures nécessaires à l'encontre du préfet de la Guyane. Par une ordonnance n° 1800100 du 2 février 2018, le juge des référés du tribunal administratif de la Guyane a rejeté sa demande. M. A... relève appel de cette ordonnance.<br/>
<br/>
              Sur la régularité de l'ordonnance attaquée :<br/>
<br/>
              3. D'une part, l'ordonnance attaquée ne mentionnant pas, contrairement aux exigences de l'article R. 741-2 du code de justice administrative, la date de l'audience, le requérant soutient que ce défaut l'entache d'irrégularité. Il résulte cependant des pièces du dossier que l'ordonnance a été rendue après une audience publique, ce qu'aucune partie ne conteste, et qu'elle s'est tenue le 1er février 2018, comme le requérant le mentionne lui-même dans la note en délibéré qu'il a produite après cette audience devant le juge des référés du tribunal administratif de la Guyane. Dans les circonstances de l'espèce, l'omission de la date de l'audience par l'ordonnance attaquée est restée sans incidence sur la régularité de la procédure suivie, et sans effet sur le contrôle exercé par le juge d'appel.<br/>
<br/>
              4. D'autre part, le requérant soutient que l'absence de communication au demandeur des pièces que le préfet de Guyane a versées après l'audience entache l'ordonnance attaquée d'irrégularité. Il apparaît néanmoins que cette circonstance est restée sans incidence sur la régularité de la procédure, dès lors qu'aucune de ces pièces n'a été le fondement des motifs de l'ordonnance attaquée.<br/>
<br/>
              Sur le bien fondé de l'ordonnance attaquée :<br/>
<br/>
              5. En regardant comme inopérants les moyens dirigés contre la régularité de la décision ou de la procédure y ayant conduit, après avoir exclu que la décision attaquée ait pu porter une atteinte grave et manifestement illégale à une liberté fondamentale, le juge des référés, qui devait examiner l'ensemble des moyens qui lui étaient soumis, s'est, sans erreur de droit, borné à écarter ceux qui n'auraient en tout état de cause pu le conduire à regarder comme réunies les conditions posées par l'article L 521-2 du code de justice administrative.<br/>
<br/>
              6. Le requérant soutient que le juge des référés du tribunal administratif de la Guyane a commis une erreur de droit en considérant que le retrait de l'agrément organisé par l'arrêté préfectoral du 16 janvier 2018 ne portait pas une atteinte grave et manifestement illégale à l'exercice de la liberté d'entreprendre. Cependant, la restriction de la liberté d'entreprendre résultant du retrait d'agrément étant organisée par des dispositions dont la légalité n'était pas contestée, le juge des référés n'a commis aucune erreur de droit. <br/>
<br/>
              7. Le requérant soutient également que le juge des référés du tribunal administratif de la Guyane a entaché sa décision d'une erreur de qualification juridique des faits en refusant de constater que l'arrêté préfectoral du 16 janvier 2018 avait porté une atteinte grave et manifestement illégale à sa présomption d'innocence. Cependant, le juge des référés n'a pas inexactement qualifié les faits en relevant que la prise en compte d'une condamnation qu'il n'a nullement regardée ou présentée comme définitive, ne portait, en dépit de la publication du retrait d'agrément qui en faisait état, aucune atteinte à la présomption d'innocence.<br/>
<br/>
              8. Les moyens articulés à l'encontre de l'ordonnance dont il est relevé appel ne peuvent donc qu'être écartés.<br/>
<br/>
              Sur la régularité de l'arrêté préfectoral du 16 janvier 2018<br/>
<br/>
              9. Aux termes de l'article L. 313-2 du code de la sécurité intérieure : " Nul ne peut exercer à titre individuel l'activité qui consiste, à titre principal ou accessoire, en la fabrication, le commerce, l'échange, la location, la réparation ou la transformation d'armes, d'éléments d'armes et de munitions ni diriger ou gérer une personne morale exerçant cette activité s'il n'est titulaire d'un agrément relatif à son honorabilité et à ses compétences professionnelles, délivré par l'autorité administrative ". Aux termes de l'article R. 313-5 du même code : " L'agrément peut être refusé lorsque le demandeur a été condamné à une peine d'emprisonnement avec ou sans sursis supérieure à trois mois, inscrite à son casier judiciaire ou, pour les ressortissants étrangers, dans un document équivalent au bulletin n° 2 du casier judiciaire ".<br/>
<br/>
              10. Contrairement à ce que soutient la requête d'appel, l'arrêté préfectoral critiqué n'énonçait pas que M. A...avait été l'objet d'une condamnation définitive par la justice brésilienne, mais se bornait à reproduire les mentions du jugement de condamnation de première instance mentionnant l'infliction d'une " peine définitive ".<br/>
<br/>
              11. Pour retirer l'agrément dont bénéficiait M.A..., le préfet, en relevant que la condamnation de ce dernier à dix années de prison conduisait à douter de sa moralité, n'a pas entaché d'erreur de droit son appréciation en la fondant sur un motif dubitatif, les faits étant clairement regardés comme établis et de nature à remettre en cause le respect de la condition légale de moralité à laquelle est subordonné l'octroi de l'agrément. En estimant que la condamnation à dix années de prison au Brésil pour violation de la législation locale sur les armes créait un trouble suffisant à l'ordre public pour justifier le retrait de l'agrément, le préfet a donné un fondement légal à son arrêté. Dans la mesure où l'intéressé pourra solliciter à nouveau l'agrément, dès qu'il s'estimera en droit de le faire, le moyen tiré de la disproportion de la mesure, faute quelle fixe un terme au retrait, ne peut qu'être écarté.<br/>
<br/>
              12. Dès lors qu'aucune atteinte grave et illégale à une liberté fondamentale ne peut être regardée comme établie, les conclusions de la requête ne peuvent qu'être rejetées, y compris celles tendant à ce que l'Etat soit condamné au versement d'une somme d'argent sur le fondement de l'article L. 761-1 du code de justice administrative, qui, l'Etat n'étant pas la partie perdante, y font obstacle.<br/>
<br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de M. A...est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à M. B...A....<br/>
Copie en sera adressée pour information au préfet de la Guyane et au ministre d'Etat, ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
