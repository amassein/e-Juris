<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027244292</ID>
<ANCIEN_ID>JG_L_2013_03_000000356552</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/24/42/CETATEXT000027244292.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  9ème sous-section jugeant seule, 28/03/2013, 356552, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-03-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>356552</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 9ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BARTHELEMY, MATUCHANSKY, VEXLIARD</AVOCATS>
<RAPPORTEUR>M. Matthieu Schlesinger</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Aladjidi</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2013:356552.20130328</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi, enregistré le 7 février 2012 au secrétariat du contentieux du Conseil d'Etat, présenté par le ministre du budget, des comptes publics et de la reforme de l'État ; le ministre demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 10VE01203 du 16 décembre 2011 par lequel la cour administrative d'appel de Versailles a, sur l'appel de M. A... B..., d'une part, annulé le jugement nos 0704111-0704179 du 16 février 2010 par lequel le tribunal administratif de Versailles a rejeté la demande de ce dernier tendant à la restitution des droits de taxe sur la valeur ajoutée qu'il a acquittés au titre de la période allant du 1er janvier 2004 au 31 décembre 2006, d'autre part, accordé la restitution demandée, enfin, mis à la charge de l'Etat la somme de 1 500 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter la requête d'appel de M. B... ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 6 mars 2013, présentée par le ministre de l'économie et des finances ;<br/>
<br/>
              Vu la sixième directive 77/388/CEE du Conseil du 17 mai 1977 ;<br/>
<br/>
              Vu le code général des impôts et le livre des procédures fiscales ;<br/>
<br/>
              Vu la loi n° 2002-303 du 4 mars 2002 ;<br/>
<br/>
              Vu le décret n° 2011-32 du 7 janvier 2011 ;<br/>
<br/>
              Vu le décret n° 2011-1127 du 20 septembre 2011 ;<br/>
<br/>
              Vu l'arrêté du 20 septembre 2011 relatif à la formation des chiropracteurs et à l'agrément des établissements de formation en chiropraxie ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Matthieu Schlesinger, Auditeur,  <br/>
<br/>
              - les observations de la SCP Barthélémy, Matuchansky, Vexliard, avocat de M. B...,<br/>
<br/>
              - les conclusions de M. Frédéric Aladjidi, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Barthélémy, Matuchansky, Vexliard, avocat de M. B... ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que M. B..., qui exerce l'activité de chiropracteur, a demandé la restitution des droits de taxe sur la valeur ajoutée qu'il a spontanément acquittés au titre de la période allant du 1er janvier 2004 au 31 décembre 2006, en estimant pouvoir bénéficier des dispositions de l'article 261 du code général des impôts relatives à l'exonération de cette taxe ; que le ministre se pourvoit en cassation contre l'arrêt du 16 décembre 2011 par lequel la cour administrative d'appel de Versailles a fait droit à la requête de M. B... tendant à l'annulation du jugement du 16 février 2010 du tribunal administratif de Versailles qui avait rejeté sa demande, en lui accordant la restitution des droits de taxe sur la valeur ajoutée litigieux ;<br/>
<br/>
              2. Considérant, en premier lieu, qu'aux termes de l'article R. 194-1 du livre des procédures fiscales : " Lorsque, ayant donné son accord à la rectification ou s'étant abstenu de répondre dans le délai légal à la proposition de rectification, le contribuable présente cependant une réclamation faisant suite à une procédure contradictoire de rectification, il peut obtenir la décharge ou la réduction de l'imposition, en démontrant son caractère exagéré. / Il en est de même lorsqu'une imposition a été établie d'après les bases indiquées dans la déclaration souscrite par un contribuable (...) " ; qu'il résulte de ces dispositions qu'un contribuable ne peut obtenir la restitution de droits de taxe sur la valeur ajoutée qu'il a déclarés et spontanément acquittés conformément à ses déclarations qu'à la condition d'en établir le mal-fondé ;<br/>
<br/>
              3. Considérant, en deuxième lieu, qu'aux termes de l'article 13, A, paragraphe 1 de la sixième directive du Conseil du 17 mai 1977 en matière d'harmonisation des législations des États membres relatives aux taxes sur le chiffre d'affaires : " Sans préjudice d'autres dispositions communautaires, les Etats membres exonèrent, dans les conditions qu'ils fixent en vue d'assurer l'application correcte et simple des exonérations prévues ci-dessous et de prévenir toute fraude, évasion et abus éventuels : / (...) c) les prestations de soins à la personne effectuées dans le cadre de l'exercice des professions médicales et paramédicales telles qu'elles sont définies par l'Etat membre concerné (...) " ; qu'en vertu du 1° du 4 de l'article 261 du code général des impôts, dans sa rédaction applicable à la période d'imposition en litige, sont exonérés de la taxe sur la valeur ajoutée : " Les soins dispensés aux personnes par les membres des professions médicales et paramédicales réglementées (...) " ; qu'en limitant l'exonération qu'elles prévoient aux soins dispensés par les membres des professions médicales et paramédicales soumises à réglementation, ces dispositions ne méconnaissent pas l'objectif poursuivi par l'article 13, A, paragraphe 1, sous c) de la sixième directive, précité, qui est de garantir que l'exonération s'applique uniquement aux prestations de soins à la personne fournies par des prestataires possédant les qualifications professionnelles requises ; qu'en effet, la directive renvoie à la réglementation interne des États membres la définition de la notion de professions paramédicales, des qualifications requises pour exercer ces professions et des activités spécifiques de soins à la personne qui relèvent de telles professions ; <br/>
<br/>
              4. Considérant toutefois que, conformément à l'interprétation des dispositions de la sixième directive qui résulte de l'arrêt rendu le 27 avril 2006 par la Cour de justice des Communautés européennes dans les affaires C-443/04 et C-444/04, l'exclusion d'une profession ou d'une activité spécifique de soins à la personne de la définition des professions paramédicales retenue par la réglementation nationale aux fins de l'exonération de la taxe sur la valeur ajoutée prévue à l'article 13, A, paragraphe 1, sous c) de cette directive serait contraire au principe de neutralité fiscale inhérent au système commun de taxe sur la valeur ajoutée s'il pouvait être démontré que les personnes exerçant cette profession ou cette activité disposent, pour la fourniture de telles prestations de soins, de qualifications professionnelles propres à assurer à ces prestations un niveau de qualité équivalente à celles fournies par des personnes bénéficiant, en vertu de la réglementation nationale, de l'exonération ;<br/>
<br/>
              5. Considérant, en troisième lieu, qu'aux termes de l'article 75 de la loi du 4 mars 2002 relative aux droits des malades et à la qualité du système de santé, dans sa version applicable au présent litige : " L'usage professionnel du titre d'ostéopathe ou de chiropracteur est réservé aux personnes titulaires d'un diplôme sanctionnant une formation spécifique à l'ostéopathie ou à la chiropraxie délivrée par un établissement de formation agréé par le ministre chargé de la santé dans des conditions fixées par décret. Le programme et la durée des études préparatoires et des épreuves après lesquelles peut être délivré ce diplôme sont fixés par voie réglementaire. (...) / Les praticiens en exercice, à la date d'application de la présente loi, peuvent se voir reconnaître le titre d'ostéopathe ou de chiropracteur s'ils satisfont à des conditions de formation ou d'expérience professionnelle analogues à celles des titulaires du diplôme mentionné au premier alinéa. Ces conditions sont déterminées par décret. (...) / Un décret établit la liste des actes que les praticiens justifiant du titre d'ostéopathe ou de chiropracteur sont autorisés à effectuer, ainsi que les conditions dans lesquelles ils sont appelés à les accomplir. / Ces praticiens ne peuvent exercer leur profession que s'ils sont inscrits sur une liste dressée par le représentant de l'État dans le département de leur résidence professionnelle, qui enregistre leurs diplômes, certificats, titres ou autorisations " ; <br/>
<br/>
              6. Considérant que le décret du 7 janvier 2011 relatif aux actes et aux conditions d'exercice de la chiropraxie n'a été publié que le 9 janvier 2011 et que le décret du 20 septembre 2011 relatif à la formation des chiropracteurs et à l'agrément des établissements de formation en chiropraxie, ainsi que l'arrêté du même jour pris en application de ces deux décrets, n'ont été publiés que le 21 septembre 2011 ; que, durant la période allant du 1er janvier 2004 au 31 décembre 2006, les actes dits de chiropraxie ne pouvaient être pratiqués que par les docteurs en médecine, et le cas échéant, pour certains actes seulement et sur prescription médicale, par les autres professionnels de santé habilités à les réaliser ;<br/>
<br/>
              7. Considérant qu'il résulte de ce qui précède que, pour décider de la restitution des droits de taxe sur la valeur ajoutée acquittés par M. B... sur ses prestations de chiropraxie, la cour devait vérifier que celui-ci démontrait disposer, pour la fourniture de ces prestations, de qualifications professionnelles propres à leur assurer un niveau de qualité équivalente à celles fournies, selon le cas, par un médecin ou par un membre d'une profession de santé réglementée ; qu'une telle appréciation ne peut être portée qu'au vu de la nature des actes accomplis sous la dénomination d'actes de chiropraxie et, s'agissant des actes susceptibles de comporter des risques en cas de contre-indication médicale, en considération des conditions dans lesquelles ils ont été effectués ;<br/>
<br/>
              8. Considérant qu'il appartenait, dès lors, à M. B..., pour mettre le juge à même de s'assurer que la condition tenant à la qualité des actes était remplie, de produire, d'une part, et sous réserve de l'occultation des noms des patients, des éléments relatifs à sa pratique permettant d'appréhender, sur une période significative, la nature des actes accomplis et les conditions dans lesquelles ils l'ont été et, d'autre part, tous éléments utiles relatifs à ses qualifications professionnelles ;<br/>
<br/>
              9. Considérant que, pour faire droit aux conclusions de M. B..., la cour administrative d'appel de Versailles a jugé que les dispositions du décret précité organisant, de façon transitoire, les modalités de délivrance de l'autorisation d'user du titre professionnel de chiropracteur aux praticiens en exercice à la date de sa publication devaient être regardées, dans le cadre du litige qui lui était soumis, comme définissant les conditions devant être remplies par les personnes pratiquant des actes de chiropraxie pour que ces actes soient regardés comme accomplis avec des garanties équivalentes à celles constatées pour des actes de même nature accomplis par des médecins ; qu'elle a ensuite estimé que les actes accomplis par l'intéressé pendant la période litigieuse étaient d'une qualité équivalente à ceux qui, s'ils avaient été effectués par un médecin, auraient été exonérés, dès lors que M. B... justifiait d'une formation équivalente à celle exigée par les dispositions réglementaires adoptées en 2011 ; qu'il résulte de ce qui a été dit ci-dessus qu'en statuant ainsi, sans apprécier la nature des actes ainsi accomplis, la cour a commis une erreur de droit ; qu'ainsi, le ministre est fondé à demander l'annulation de l'arrêt attaqué ;<br/>
<br/>
              10. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application de l'article L. 821-2 du code de justice administrative ; <br/>
<br/>
              11. Considérant qu'il résulte de tout ce qui a été dit ci-dessus aux points 1 à 8 que, pour obtenir la restitution des droits de taxe sur la valeur ajoutée qu'il a spontanément acquittés au titre de la période allant du 1er janvier 2004 au 31 décembre 2006 à raison de ses prestations de chiropraxie, M. B... doit, pour mettre le juge à même de s'assurer que la condition de l'exonération de taxe tenant à la qualité de ces prestations était remplie, produire, d'une part, et sous réserve de l'occultation des noms des patients, des éléments relatifs à sa pratique permettant d'appréhender, sur une période significative, la nature des actes accomplis et les conditions dans lesquelles ils l'ont été et, d'autre part, tous éléments utiles relatifs à ses qualifications professionnelles ;<br/>
<br/>
              12. Considérant que M. B... produit des éléments attestant, de manière suffisante, la qualité de la formation qu'il a suivie et du diplôme qu'il a obtenu ; que pour permettre au juge de se prononcer sur la nature des actes accomplis et les conditions dans lesquelles ils ont été effectués, il lui appartenait également de fournir des éléments permettant, sur une période significative d'au moins deux mois, de s'assurer que ces actes n'étaient pas interdits ou n'avaient pas été accomplis sans avis médical préalable lorsque celui-ci était requis ; qu'en l'espèce, M. B... ne produit aucune " fiche patient " et se borne à une description générale de son activité ; qu'ainsi M. B... n'établit pas, par les pièces qu'il produit, que les actes de chiropraxie qu'il a accomplis au cours de la période litigieuse puissent être regardés comme d'une qualité équivalente à ceux qui, s'ils avaient été effectués par un médecin pratiquant la chiropraxie, auraient été exonérés de taxe sur la valeur ajoutée ;<br/>
<br/>
              13. Considérant qu'il résulte de tout ce qui précède que M. B... n'est pas fondé à soutenir que c'est à tort que, par le jugement qu'il attaque, le tribunal administratif de Versailles a rejeté sa demande ; que par suite, les conclusions qu'il présente au titre de l'article L. 761-1 du code de justice administrative ne peuvent qu'être rejetées ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Versailles du 16 décembre 2011 est annulé.<br/>
Article 2 : La requête de M. B... présentée devant la cour administrative d'appel de Versailles et le surplus des conclusions du pourvoi de M. B... sont rejetés.<br/>
Article 3 : La présente décision sera notifiée au ministre de l'économie et des finances et à M. A... B....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
