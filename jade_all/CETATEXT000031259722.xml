<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031259722</ID>
<ANCIEN_ID>JG_L_2015_09_000000376331</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/25/97/CETATEXT000031259722.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème SSJS, 30/09/2015, 376331, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-09-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>376331</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème SSJS</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP NICOLAY, DE LANOUVELLE, HANNOTIN</AVOCATS>
<RAPPORTEUR>M. Mathieu Herondart</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Nathalie Escaut</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2015:376331.20150930</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. et Mme A...B...ont demandé au tribunal administratif de Versailles de les décharger des cotisations supplémentaires d'impôt sur le revenu et de contributions sociales ainsi que des pénalités correspondantes auxquelles ils ont été assujettis au titre de l'année 2004 et de mettre à la charge de l'Etat le paiement d'intérêts moratoires. <br/>
<br/>
              Par un jugement n° 0800049 du 14 février 2012, le tribunal administratif de Versailles a rejeté leur demande.<br/>
<br/>
              Par un arrêt n° 12VE01255 du 30 décembre 2013, la cour administrative d'appel de Versailles a, sur appel de M. et MmeB..., annulé ce jugement et les a déchargés des impositions et pénalités mises à leur charge. <br/>
<br/>
              Par un pourvoi, enregistré le 12 mars 2014 au secrétariat du contentieux du Conseil d'Etat, le ministre délégué, chargé du budget demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler les articles 1 à 3 de cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de M. et MmeB.... <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code général des impôts et le livre des procédures fiscales ; <br/>
              - la loi n° 2003-1311 du 30 décembre 2003 ;<br/>
              - le décret n° 2003-1384 du 31 décembre 2003 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Mathieu Herondart, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Nathalie Escaut, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Nicolaÿ, de Lanouvelle, Hannotin, avocat de M. et Mme B...;<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes du I de l'article 150 UB du code général des impôts, dans sa rédaction résultant de la loi du 30 décembre 2003 de finances pour 2004, applicable aux gains nets tirés de cessions à titre onéreux de valeurs mobilières et de droits sociaux de sociétés non cotées soumises à l'impôt sur les sociétés dont l'actif satisfait aux conditions qu'il prévoit, acquis avant le 21 novembre 2003 et cédés entre le 1er janvier 2004 et le 31 décembre 2004 : " Les gains nets retirés de cessions à titre onéreux de droits sociaux de sociétés ou groupements qui relèvent des articles 8 à 8 ter, dont l'actif est principalement constitué d'immeubles ou de droits portant sur ces biens, sont soumis exclusivement au régime d'imposition prévu au 1 et au 1° du II de l'article 150 U. Pour l'application de cette disposition, ne sont pas pris en considération les immeubles affectés par la société à sa propre exploitation industrielle, commerciale, agricole ou à l'exercice d'une profession non commerciale ";<br/>
<br/>
              2. Considérant qu'aux termes de l'article 74 SB de l'annexe II au code général des impôts, issu de l'article 1er du décret du 31 décembre 2003 pris pour l'application des articles 150 U et 150 VH et 244 bis A du code général des impôts et relatif aux plus-values réalisées par les particuliers et modifiant l'annexe II au code général des impôts, applicable aux impositions litigieuses : "Pour l'application de cette disposition, sont considérées comme sociétés à prépondérance immobilière les sociétés dont l'actif est, à la clôture des trois exercices qui précèdent la cession, constitué pour plus de 50 % de sa valeur réelle par des immeubles ou des droits portant sur des immeubles, non affectés par ces sociétés à leur propre exploitation industrielle, commerciale, agricole ou à l'exercice d'une profession non commerciale " ;<br/>
<br/>
              3. Considérant que le décret du 31 décembre 2003 a été pris sur le fondement du IX de l'article 10 de la loi du 30 décembre 2003 de finances pour 2004, prévoyant qu'un décret en Conseil d'Etat fixe les conditions d'application de ces articles et notamment les obligations déclaratives incombant aux contribuables et aux intermédiaires ; qu'en subordonnant ainsi, par les dispositions de l'article 74 SB, le bénéfice du régime d'imposition institué par l'article 150 UB à la condition que la société dont les titres sont cédés revête le caractère de prépondérance immobilière à la clôture des trois exercices précédant la cession, le pouvoir réglementaire ne s'est pas limité à préciser les conditions d'application de la loi fiscale mais a restreint le champ d'application d'un régime légal d'imposition en fixant une condition de durée de détention non prévue par la loi ; que, par suite, en jugeant que le pouvoir réglementaire avait fixé une règle relative à 1'assiette de 1' impôt qui relève de la seule compétence de la loi, la cour administrative d'appel de Versailles n'a pas commis d'erreur de droit ; qu'elle n'a pas davantage commis d'erreur de droit en jugeant qu'il ressortait des termes clairs de l'article 150 UB du code général des impôts que la condition de prépondérance immobilière ne pouvait s'apprécier qu'à la date de la cession des titres ; <br/>
<br/>
              4. Considérant qu'il résulte de ce qui précède que le ministre des finances et des comptes publics n'est pas fondé à demander l'annulation de l'arrêt qu'il attaque ;<br/>
<br/>
              5. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 3 000 euros à verser à M. et MmeB..., au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>                      D E C I D E :<br/>
                                    --------------<br/>
<br/>
Article 1er : Le pourvoi du ministre des finances et des comptes publics est rejeté.<br/>
<br/>
Article 2 : L'Etat versera une somme de 3 000 euros à M. et Mme B...au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 3 : La présente décision sera notifiée au ministre des finances et des comptes publics et à M. et MmeB....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
