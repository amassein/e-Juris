<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043415703</ID>
<ANCIEN_ID>JG_L_2021_04_000000446735</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/41/57/CETATEXT000043415703.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème chambre, 22/04/2021, 446735, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-04-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>446735</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MELKA-PRIGENT-DRUSCH ; SCP FOUSSARD, FROGER</AVOCATS>
<RAPPORTEUR>M. Jonathan  Bosredon</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Karin Ciavaldini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:446735.20210422</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une protestation enregistrée le 2 juillet 2020 au greffe du tribunal administratif de Versailles, M. G... A..., a demandé au tribunal, à titre principal, de réformer les résultats du second tour des élections municipales qui se sont déroulées le 28 juin 2020 dans la commune de Villennes-sur-Seine (Yvelines) en ajoutant 4 suffrages exprimés à la liste " Avenir Villennes ", de proclamer cette liste comme ayant obtenu le plus grand nombre de voix, de lui attribuer la majorité des sièges du conseil municipal, et, à titre subsidiaire, d'annuler l'ensemble des opérations électorales qui se sont déroulées dans cette commune.<br/>
<br/>
              Par un jugement n° 2004045 du 20 octobre 2020, ce tribunal a rejeté cette protestation.<br/>
<br/>
              Par une requête d'appel, enregistrée le 20 novembre 2020 au secrétariat du contentieux du Conseil d'Etat, M. A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) de faire droit à sa protestation.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code électoral ;<br/>
              - le code de justice administrative et le décret n° 20520-1406 du 18 novembre 2020 ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jonathan Bosredon, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Karin Ciavaldini, rapporteure publique ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Melka-Prigent-Drusch, avocat de M. A... et à la SCP Foussard, Froger, avocat de M. B... ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. A l'issue des opérations électorales qui se sont déroulées le 15 mars 2020 pour l'élection des conseillers municipaux de la commune de Villennes-sur-Seine (Yvelines), la liste " Avenir Villennes ", conduite par M. A..., a obtenu 698 voix, soit 32,74 % des suffrages exprimés, la liste " Bien vivre à Villennes ", conduite par M. D..., 570 voix, soit 26,74 % des suffrages exprimés, la liste " Villennes Autrement ", conduite par M. E..., 441 voix, soit 20,68 % des suffrages exprimés et la liste " Villennes Ensemble ", conduite par M. C..., 423 voix, soit 19,84 % des suffrages exprimés. A l'issue du second tour de scrutin, qui s'est déroulé le 28 juin 2020, la liste " Bien vivre à Villennes Autrement ", conduite par M. D..., est arrivée en tête avec 926 voix, soit 40 % des suffrages exprimés, et a obtenu 21 sièges, la liste " Avenir Villennes ", conduite par M. A..., a obtenu 924 voix, soit 39,91 % des suffrages, et 5 sièges et la liste " Villennes Ensemble ", conduite par M. C..., a obtenu 465 voix, soit 20,09 % des suffrages exprimés, et 3 sièges. M. A... relève appel du jugement du 20 octobre 2020 par lequel le tribunal administratif de Versailles a rejeté sa protestation tendant, à titre principal, à la rectification des résultats des opérations électorales et à ce que sa liste soit proclamée comme étant arrivée en tête à l'issue du second tour du scrutin et, à titre subsidiaire, à l'annulation de l'ensemble des opérations électorales.<br/>
<br/>
              Sur la campagne électorale :<br/>
<br/>
              2. Aux termes de l'article L. 52-1 du code électoral : " (...) A compter du premier jour du sixième mois précédant le mois au cours duquel il doit être procédé à des élections générales, aucune campagne de promotion publicitaire des réalisations ou de la gestion d'une collectivité ne peut être organisée sur le territoire des collectivités intéressées par le scrutin. Sans préjudice des dispositions du présent chapitre, cette interdiction ne s'applique pas à la présentation, par un candidat ou pour son compte, dans le cadre de l'organisation de sa campagne, du bilan de la gestion des mandats qu'il détient ou qu'il a détenus. Les dépenses afférentes sont soumises aux dispositions relatives au financement et au plafonnement des dépenses électorales contenues au chapitre V bis du présent titre ". L'article L. 52-8 du même code prévoit que : " (...) Les personnes morales, à l'exception des partis ou groupements politiques, ne peuvent participer au financement de la campagne électorale d'un candidat, ni en lui consentant des dons sous quelque forme que ce soit, ni en lui fournissant des biens, services ou autres avantages directs ou indirects à des prix inférieurs à ceux qui sont habituellement pratiqués. Les personnes morales, à l'exception des partis et groupements politiques ainsi que des établissements de crédit ou sociétés de financement ayant leur siège social dans un Etat membre de l'Union européenne ou partie à l'accord sur l'Espace économique européen, ne peuvent ni consentir des prêts à un candidat, ni lui apporter leur garantie pour l'obtention de prêts (...) ".<br/>
<br/>
              3. Il résulte en premier lieu de l'instruction que si la mairie de Villennes-sur-Seine a diffusé sur la page Facebook de la commune les 7, 20, 24 et 27 avril 2020 des messages accompagnés de photographies faisant état de la remise à titre gracieux de  deux mille masques au profit des personnes âgées et des résidents de maisons de retraite par une société dont le dirigeant était un membre de la liste conduite par M. D..., ces messages, diffusés dans le contexte de crise sanitaire, revêtaient un caractère purement informatif et ne comportaient pas de lien avec la campagne électorale. Par ailleurs, aucun élément ne permet d'établir que ce don aurait constitué un avantage matériel au profit de la liste conduite par M. D.... Par suite, les griefs tirés de ce que la publication de ces messages et les dons de masques seraient intervenus en méconnaissance des dispositions des articles L. 52-1 et L. 52-8 du code électoral ne peuvent qu'être écartés. <br/>
<br/>
              4. En deuxième lieu, si M. A... soutient que la publication le 21 mai 2020 sur la page Facebook de la commune d'un message faisant état de la réouverture prochaine du club de tennis communal constituait un soutien à la liste " bien vivre à Villennes " dont le maire sortant était proche, il résulte de l'instruction que ce message se bornait à faire état de la tenue d'une réunion entre le maire, l'adjoint à la sécurité, l'élu délégué aux sports et les responsables de ce club en vue d'arrêter des modalités de réouverture compatibles avec les contraintes sanitaires. Par suite, le grief tiré de ce que cette publication aurait constitué une irrégularité de nature à porter atteinte à la sincérité du scrutin ne peut qu'être écarté. <br/>
<br/>
              5. En troisième lieu, si M. A... soutient que l'équipe municipale sortante aurait dénigré l'action des candidats de la liste " Avenir Villennes " au travers de plusieurs messages publiés sur les réseaux sociaux les 23 avril, 19 mai et 16 juin 2020, il résulte de l'instruction que ces messages constituaient une réponse à des publications de cette liste, qui portaient au demeurant sur des sujets relatifs à la vie de la commune sans lien direct avec la campagne électorale, tels que la mise en place de protections en plastique sur les tables dans les écoles en prévision de la réouverture de ces dernières, le protocole sanitaire défini en vue de permettre la  réouverture du club de tennis municipal ou encore les conditions d'entretien de la voirie et des espaces verts d'un ensemble immobilier situé sur le plateau de Fauveau. Ces messages ne sauraient être regardés comme ayant introduit dans le débat électoral des éléments de polémique nouveaux, auxquels il n'aurait pas été possible de répondre en temps utile. De même, le message, publié le 18 juin 2020 sur la page Facebook de la commune, par lequel le maire exposait les raisons pour lesquelles il avait convoqué une séance du conseil municipal avant le second tour des élections municipales pour procéder à l'adoption du budget primitif de la commune pour 2020 constituait une réponse à la publication, sur le même réseau social, par un membre de la liste " Avenir Villennes ", d'un commentaire critique sur l'action de l'équipe sortante. Enfin, si une publication intervenue le 4 juin sur le compte Facebook de la commune, relative à l'état d'avancement d'un projet de construction initié en 2012 par l'ancien maire de la commune, faisait état de ce que l'intéressé était " actuellement membre de la liste électorale d'Olivier C... ", cette seule mention, qui ne revêt pas le caractère d'un élément de polémique électorale, ne saurait constituer une irrégularité de nature à porter atteinte à la sincérité du scrutin.<br/>
<br/>
              Sur le déroulement du scrutin :<br/>
<br/>
              6. Aux termes de l'article L. 241 du code électoral : " Des commissions, dont la composition et le fonctionnement sont fixés par décret, sont chargées, pour les communes de 2 500 habitants et plus, d'assurer l'envoi et la distribution des documents de propagande électorale ". L'article R. 34 du même code précise que " La commission de propagande reçoit du préfet le matériel nécessaire à l'expédition des circulaires et bulletins de vote et fait préparer les libellés d'envoi. / Elle est chargée : / - d'adresser, au plus tard le mercredi précédant le premier tour de scrutin et, en cas de ballottage, le jeudi précédant le second tour, à tous les électeurs de la circonscription, une circulaire et un bulletin de vote de chaque candidat, binôme de candidats ou liste (...) Si un candidat, un binôme de candidats ou une liste de candidats remet à la commission de propagande moins de circulaires ou de bulletins de vote que les quantités prévues ci-dessus, il peut proposer une répartition de ses circulaires et bulletins de vote entre les électeurs. A défaut de proposition ou lorsque la commission le décide, les circulaires demeurent à la disposition du candidat et les bulletins de vote sont distribués dans les bureaux de vote, à l'appréciation de la commission, en tenant compte du nombre d'électeurs inscrits (...) ".<br/>
<br/>
              7. Il résulte de l'instruction que les bulletins de vote de la liste " Avenir Villennes " n'étaient présents que dans environ la moitié des enveloppes distribuées aux électeurs de la commune par la commission électorale. Toutefois, il résulte également de l'instruction que M. A... a indiqué par erreur le 5 juin 2020 au prestataire chargé de la distribution, qui devait remettre une moitié des bulletins à la mairie en vue de leur mise à disposition dans les bureaux de vote et adresser l'autre moitié aux électeurs, qu'il lui en remettait 10 0000 exemplaires alors qu'il avait fourni directement à la mairie les 5 000 bulletins qui lui étaient destinés et n'en a remis que 5 000 à ce prestataire, qui en a adressé une moitié à la mairie et l'autre moitié aux électeurs. M. A... n'a pas davantage signalé, lors de la réunion de la commission électorale du 10 juin 2020 chargée d'examiner la conformité de bulletins de vote, à laquelle il participait, que ce nombre de 10 000 bulletins incluait les exemplaires livrés directement en mairie par ses soins. Dans ces conditions, celui-ci ne saurait se prévaloir, au soutien de sa protestation, des dysfonctionnements dans la distribution des documents de propagande aux électeurs, auxquels il n'est pas étranger. Au demeurant, M. A..., averti de cette situation par les services de la préfecture dès le 19 juin 2020, a été en mesure de faire procéder directement à la distribution des bulletins de vote manquants aux électeurs avant le scrutin et il résulte de l'instruction que les bureaux de vote ont disposé d'un nombre suffisant de bulletins de la liste " Avenir Villennes ".  Par suite, ces dysfonctionnements n'ont pas été de nature à altérer la sincérité du scrutin et M. A... ne saurait soutenir que les suffrages exprimés en sa faveur au seul moyen d'une profession de foi auraient dû, dans ces circonstances particulières, être regardés comme régulièrement exprimés. <br/>
<br/>
              8. Il résulte de tout ce qui précède que M. A... n'est pas fondé à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Versailles a rejeté sa protestation. <br/>
<br/>
              Sur les frais exposés et non compris dans les dépends :<br/>
<br/>
              9. Il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par M. D... et autres sur le fondement de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de M. A... est rejetée.<br/>
Article 2 : Les conclusions présentées par M. D... et autres sur le fondement de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La présente décision sera notifiée à M. G... A..., à M. F... D..., premier dénommé, pour l'ensemble des défendeurs et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
