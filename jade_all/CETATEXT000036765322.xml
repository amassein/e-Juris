<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036765322</ID>
<ANCIEN_ID>JG_L_2018_03_000000407413</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/76/53/CETATEXT000036765322.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème chambre, 23/03/2018, 407413, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-03-23</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>407413</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP CELICE, SOLTNER, TEXIDOR, PERIER</AVOCATS>
<RAPPORTEUR>M. Etienne de Lageneste</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Romain Victor</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:407413.20180323</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B...A...a demandé au tribunal administratif de Grenoble de prononcer la décharge des cotisations supplémentaires d'impôt sur le revenu auxquelles il a été assujetti au titre des années 2005, 2006, 2007 et 2008. Par un jugement nos 0904531-1101084 du 20 septembre 2013, ce tribunal a réduit la cotisation supplémentaire due au titre de l'année 2005 et a rejeté le surplus des conclusions de cette demande.<br/>
<br/>
              Par un arrêt n° 13LY02945 du 12 novembre 2014, la cour administrative de Lyon a rejeté l'appel formé par M. A...contre l'article 2 de ce jugement.<br/>
<br/>
              Par une décision n° 387058 du 16 mars 2016 le Conseil d'Etat, statuant au contentieux sur le pourvoi de M.A..., a annulé cet arrêt et renvoyé l'affaire à la cour.<br/>
<br/>
              Par un arrêt n° 16LY01043 du 1er décembre 2016, la cour administrative de Lyon a rejeté l'appel formé par M. A...contre le jugement du tribunal administratif de Grenoble du 20 septembre 2013.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 1er février et 2 mai 2017 au secrétariat du contentieux du Conseil d'Etat, M. A...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code des assurances ;<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - la loi n° 89-1009 du 31 décembre 1989, notamment son article 11 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Etienne de Lageneste, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Romain Victor, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Célice, Soltner, Texidor, Perier, avocat de M.A.libre de prévoir l'affiliation obligatoire de tous les salariés pour autant que ceux qui peuvent se prévaloir des dispositions de l'article 11 de loi du 31 décembre 1989 ne supportent pas de cotisations à ce titre<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que M. A...a été embauché le 3 janvier 1991 en qualité de cadre par la société Bernard Dufaur Développement. En application d'un contrat d'assurance de groupe souscrit par cette société le 12 mars 1991, il a perçu, à compter du 1er septembre 1999, une pension complémentaire d'invalidité qu'il n'a pas déclarée au titre de l'impôt sur le revenu. L'administration fiscale, regardant ce contrat comme relevant d'un régime à adhésion obligatoire, a assujetti M. A...à des cotisations supplémentaires d'impôt sur le revenu au titre des années 2005 à 2008, procédant de la réintégration de ces sommes dans son revenu imposable. Par un jugement du 20 septembre 2013, le tribunal administratif de Grenoble a, d'une part, réduit la cotisation supplémentaire d'impôt sur le revenu due au titre de l'année 2005, d'autre part, rejeté le surplus des conclusions de M. A... tendant à la décharge de ces cotisations supplémentaires. Par un arrêt du 12 novembre 2014, la cour administrative d'appel de Lyon a rejeté l'appel formé par M. A...contre l'article 2 de ce jugement. Par une décision du 16 mars 2016, le Conseil d'Etat, statuant au contentieux, a annulé cet arrêt et renvoyé cette affaire à la cour. M. A...se pourvoit en cassation contre l'arrêt du 1er décembre 2016 par lequel la cour administrative d'appel a rejeté son appel.<br/>
<br/>
              2. En premier lieu, l'article 11 de la loi du 31 décembre 1989 renforçant les garanties offertes aux personnes assurées contre certains risques dispose : " Aucun salarié employé dans une entreprise avant la mise en place, à la suite d'une décision unilatérale de l'employeur, d'un système de garanties collectives contre le risque décès, les risques portant atteinte à l'intégrité physique de la personne ou liés à la maternité ou les risques d'incapacité de travail ou d'invalidité ne peut être contraint à cotiser contre son gré à ce système ".<br/>
<br/>
              3. Devant la cour, M. A...se prévalait, sur le fondement des dispositions de l'article L. 80 A du livre des procédures fiscales, du bénéfice de l'interprétation de la loi fiscale donnée au paragraphe 25 de la documentation administrative 5 F-1132 du 10 février 1999, reprise au paragraphe 230 des commentaires publiés au bulletin officiel des finances publiques (BOFiP) - impôts sous la référence BOI-RSA-CHAMP-20-30-20, selon laquelle les rentes d'invalidité servies en exécution d'un contrat d'assurance souscrit par un salarié ou d'un contrat d'assurance de groupe auquel un salarié a adhéré en vue de compléter son régime légal de protection sociale sont exclues du champ d'application de l'impôt sur le revenu dès lors que la souscription ou l'adhésion est facultative. Il soutenait à cet égard que le contrat d'assurance de groupe souscrit le 12 mars 1991 par son employeur était, pour lui, en application de l'article 11 de loi du 31 décembre 1989, un contrat à adhésion facultative dès lors qu'il était entré dans l'entreprise avant cette souscription. Toutefois, ces dispositions, qui ont seulement pour objet de faire obstacle à ce que l'employeur puisse contraindre un salarié déjà employé à la date de mise en place d'un système de garanties collectives à cotiser contre son gré à ce système, sont sans incidence sur le caractère facultatif ou obligatoire de l'adhésion, l'employeur demeurant.libre de prévoir l'affiliation obligatoire de tous les salariés pour autant que ceux qui peuvent se prévaloir des dispositions de l'article 11 de loi du 31 décembre 1989 ne supportent pas de cotisations à ce titre Par suite, c'est sans erreur de droit que la cour a écarté comme inopérant le moyen soulevé devant elle par M.A.libre de prévoir l'affiliation obligatoire de tous les salariés pour autant que ceux qui peuvent se prévaloir des dispositions de l'article 11 de loi du 31 décembre 1989 ne supportent pas de cotisations à ce titre<br/>
<br/>
              4. En deuxième lieu, c'est sans erreur de droit et sans méconnaître les règles de dévolution de la charge de la preuve que la cour a jugé que n'étaient pas de nature à faire obstacle à ce que le contrat d'assurance de groupe en litige soit regardé comme un contrat à adhésion obligatoire les circonstances, à les supposer établies, que l'employeur de M. A...n'avait aucune obligation de souscrire un tel contrat ou de ce que M. A...aurait pu en demander la résiliation, dès lors qu'il représentait à lui seul la majorité des salariés concernés.<br/>
<br/>
              5. En troisième lieu, en jugeant que le seul bulletin de salaire versé au dossier ne permettait pas d'apprécier si les cotisations versées avaient été prises en charge par M. A...et déduites pour la détermination de son revenu net imposable, la cour s'est bornée à répondre à l'argumentation soulevée par l'intéressé devant elle, tirée de ce que les cotisations qu'il avait acquittées n'avaient pas été déduites de son revenu imposable. Le requérant n'est donc pas fondé à soutenir que la cour aurait, ce faisant, relevé un moyen d'office sans inviter les parties en débattre, en méconnaissance des dispositions de l'article R. 611-7 du code de justice administrative. <br/>
<br/>
              6. Il résulte de ce qui précède que M. A...n'est pas fondé à demander l'annulation de l'arrêt qu'il attaque. Par suite, son pourvoi doit être rejeté, y compris ses conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de M. A...est rejeté.<br/>
Article 2 : La présente décision sera notifiée à M. B...A...et au ministre de l'action et des comptes publics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
