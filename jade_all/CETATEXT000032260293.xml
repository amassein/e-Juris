<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032260293</ID>
<ANCIEN_ID>JG_L_2016_03_000000375436</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/26/02/CETATEXT000032260293.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème SSJS, 16/03/2016, 375436, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-03-16</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>375436</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème SSJS</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MONOD, COLIN, STOCLET</AVOCATS>
<RAPPORTEUR>Mme Isabelle Lemesle</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Edouard Crépey</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2016:375436.20160316</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par un jugement n° 1000770 du 14 juin 2011, le tribunal administratif de Montpellier a rejeté la requête de M. A...B...tendant à l'annulation de la décision du 29 décembre 2009 par laquelle le préfet de l'Hérault a rejeté sa demande tendant au bénéfice du secours exceptionnel de l'Etat prévu à l'article 41-1 du décret du 10 mars 1962 relatif aux mesures prises pour l'accueil et le reclassement professionnel et social des bénéficiaires de la loi du 26 décembre 1961.<br/>
<br/>
              Par un arrêt n° 11MA03351 du 8 octobre 2013, la cour administrative d'appel de Marseille a rejeté l'appel formé par M. B...tendant à l'annulation du jugement du 14 juin 2011 du tribunal administratif de Montpellier.<br/>
<br/>
              Par un pourvoi sommaire et deux mémoires complémentaires, enregistrés les 13 février 2014, 12 mai et 10 juin 2014 au secrétariat du contentieux du Conseil d'Etat, M. B... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel et d'enjoindre au préfet de l'Hérault de réexaminer sa situation ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de commerce ;<br/>
              - le code général des impôts ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - le décret n° 62-261 du 10 mars 1962 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Isabelle Lemesle, conseiller d'Etat,<br/>
<br/>
              - les conclusions de M. Edouard Crépey, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Monod, Colin, Stoclet, avocat de M. A...B...;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. L'article 61-1 du décret du 10 mars 1962 relatif aux mesures prises pour l'accueil et le reclassement professionnel et social des bénéficiaires de la loi n° 61-1439 du 26 décembre 1961 dispose que : " Le représentant de l'Etat dans le département peut accorder des secours exceptionnels : / - au bénéfice des personnes ayant la qualité de "rapatrié" au regard de l'article 1er de la loi n° 61-1439 du 26 décembre 1961 ; (...) / Ces secours peuvent être accordés : / - lorsque les demandeurs rencontrent de graves difficultés économiques et financières liées à des dettes, à l'exception des dettes fiscales, contractées avant le 31 juillet 1999, qui, à défaut d'aide de l'Etat, les obligeraient de manière certaine et imminente à vendre leur résidence principale ; (...) / Le représentant de l'Etat dans le département apprécie s'il y a lieu ou non d'accorder un secours exceptionnel, au vu des circonstances de l'espèce. Il examine la situation au regard notamment des procédures de traitement du surendettement prévues au titre III du livre III du code de la consommation et de l'article L. 526-1 du code de commerce. / Le représentant de l'Etat dans le département fixe le montant du secours exceptionnel nécessaire au regard de la dette et des ressources de l'intéressé. En tout état de cause, ce montant ne peut pas être supérieur à la valeur de la résidence principale estimée par le trésorier-payeur général. / L'aide n'est accordée et versée que si le bénéficiaire justifie de la régularité de sa situation fiscale. Elle est réglée directement aux créanciers ou au mandataire en cas de procédure collective ".<br/>
<br/>
              2. Par une décision du 29 septembre 2009, le préfet de l'Hérault a refusé le bénéfice des dispositions précitées de l'article 61-1 du décret du 10 mars 1962 à M. B...au motif qu'il était personnellement redevable des dettes fiscales de son entreprise. Pour rejeter l'appel de M. B...à l'encontre du jugement du 14 juin 2011 par lequel le tribunal administratif de Montpellier a rejeté la requête dirigée contre cette décision de refus, la cour administrative d'appel de Marseille s'est bornée à relever que celui-ci était personnellement redevable des dettes fiscales de son entreprise personnelle. Ce faisant, elle a, en l'absence de précisions sur la forme exacte de la société, sur son régime d'imposition ou la nature de ses dettes, entaché son arrêt d'insuffisance de motivation. Il résulte de ce qui précède que, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, M. B...est fondé à demander l'annulation de l'arrêt attaqué. <br/>
<br/>
              3. M. B...a obtenu le bénéfice de l'aide juridictionnelle. Par suite, son avocat peut se prévaloir des dispositions des articles L.  761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991. Il y a lieu, dans les circonstances de l'espèce, et sous réserve que la SCP Monod, Colin, Stoclet, avocat du requérant, renonce à percevoir la somme correspondant à la part contributive de l'Etat, de mettre à la charge de ce dernier la somme de 2 000 euros à verser à la SCP Monod, Colin, Stoclet.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Marseille du 8 octobre 2013 est annulé.<br/>
<br/>
Article 2 : L'affaire est renvoyée à la cour administrative de Marseille.<br/>
<br/>
Article 3 : L'Etat versera à la SCP Monod, Colin, Stoclet, avocat de M. A...B..., une somme de 2 000 euros en application des dispositions du deuxième alinéa de l'article 37 de la loi du 10 juillet 1991, sous réserve que cette société renonce à percevoir la somme correspondant à la part contributive de l'Etat.<br/>
<br/>
Article 4 : La présente décision sera notifiée à M. A...B....<br/>
Copie en sera adressée au Premier ministre (mission interministérielle aux rapatriés).<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
