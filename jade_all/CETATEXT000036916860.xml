<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036916860</ID>
<ANCIEN_ID>JG_L_2018_05_000000410343</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/91/68/CETATEXT000036916860.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  6ème chambre jugeant seule, 16/05/2018, 410343, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-05-16</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>410343</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 6ème chambre jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Vivien David</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Julie Burguburu</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:410343.20180516</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, un mémoire en réplique et un nouveau mémoire, enregistrés les 5 mai 2017, 2 mars et 6 avril 2018 au secrétariat du contentieux du Conseil d'Etat, M. Jacques Rousseau demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision du 6 février 2017 par laquelle la première présidente de la cour d'appel de Saint-Denis de La Réunion lui a délivré un avertissement ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat  la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - l'ordonnance n° 58-1270 du 22 décembre 1958 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Vivien David, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Julie Burguburu, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. M. Jacques Rousseau, conseiller à la cour d'appel de Saint-Denis de La Réunion, a fait l'objet, le 6 février 2017, d'un avertissement de la part de la première présidente de cette cour en raison des nombreux courriels qu'il a adressés à plusieurs de ses collègues les 19 et 22 août 2016. Il en demande l'annulation. <br/>
<br/>
              2. Aux termes de l'article 43 de l'ordonnance du 22 décembre 1958 portant loi organique relative au statut de la magistrature : " Tout manquement par un magistrat aux devoirs de son état, à l'honneur, à la délicatesse ou à la dignité, constitue une faute disciplinaire./ Constitue un des manquements aux devoirs de son état la violation grave et délibérée par un magistrat d'une règle de procédure constituant une garantie essentielle des droits des parties, constatée par une décision de justice devenue définitive. (...) ". Aux termes de l'article 44 de la même ordonnance : " En dehors de toute action disciplinaire, l'inspecteur général, chef de l'inspection générale de la justice, les premiers présidents, les procureurs généraux et les directeurs ou chefs de service à l'administration centrale ont le pouvoir de donner un avertissement aux magistrats placés sous leur autorité. / Le magistrat à l'encontre duquel il est envisagé de délivrer un avertissement est convoqué à un entretien préalable. Dès sa convocation à cet entretien, le magistrat a droit à la communication de son dossier et des pièces justifiant la mise en oeuvre de cette procédure. Il est informé de son droit de se faire assister de la personne de son choix. / Aucun avertissement ne peut être délivré au-delà d'un délai de deux ans à compter du jour où l'inspecteur général, chef de l'inspection générale de la justice, le chef de cour, le directeur ou le chef de service de l'administration centrale a eu une connaissance effective de la réalité, de la nature et de l'ampleur des faits susceptibles de justifier une telle mesure. En cas de poursuites pénales exercées à l'encontre du magistrat, ce délai est interrompu jusqu'à la décision définitive de classement sans suite, de non-lieu, d'acquittement, de relaxe ou de condamnation. Passé ce délai et hormis le cas où une procédure disciplinaire a été engagée à l'encontre du magistrat avant l'expiration de ce délai, les faits en cause ne peuvent plus être invoqués dans le cadre d'une procédure d'avertissement. / L'avertissement est effacé automatiquement du dossier au bout de trois ans si aucun nouvel avertissement ou aucune sanction disciplinaire n'est intervenu pendant cette période. ". L'article 45 de la même ordonnance définit les sanctions disciplinaires applicables aux magistrats.<br/>
<br/>
              3. S'il ne constitue pas une sanction disciplinaire au sens de l'article 45 de l'ordonnance du 22 décembre 1958 portant loi organique relative au statut de la magistrature, un avertissement est une mesure prise en considération de la personne et est mentionné au dossier du magistrat dont il ne peut être effacé automatiquement que si aucun nouvel avertissement ou aucune sanction disciplinaire n'est intervenue pendant les trois années suivantes. L'avertissement doit, dès lors, respecter les droits de la défense et le principe d'impartialité. <br/>
<br/>
              4. En premier lieu, il ne ressort pas des pièces du dossier que la première présidente de la cour d'appel de Saint-Denis de La Réunion, qui dispose de la compétence pour infliger un avertissement à l'intéressé en vertu de l'article 44 de l'ordonnance du 22 décembre 1958 portant loi organique relative au statut de la magistrature précité, ait fait preuve de partialité à l'égard de M.A.... De même, ni les termes utilisés dans le courrier de convocation à l'entretien préalable, qui visait seulement à porter à la connaissance de l'intéressé les faits reprochés, ni le courrier lui indiquant qu'il n'était pas en droit d'exiger une expertise préalable, qui se bornait à rappeler le cadre procédural de la mesure envisagée, ne révèlent un parti pris défavorable au requérant. D'autre part, la circonstance que le procès-verbal de l'entretien ait été dressé par la conseillère chargée du secrétariat général de la première présidence, destinataire de courriels ayant motivé l'avertissement, est sans incidence dès lors que seule l'autorité hiérarchique est l'auteur de l'avertissement. Au surplus, il ressort de la décision attaquée que M. A... a été informé de ce que ses déclarations seraient consignées par écrit par cette personne et a indiqué ne pas y être opposé. Il résulte de tout ce qui précède que M. A...n'est pas fondé à soutenir que la décision attaquée aurait été prise au terme d'une procédure irrégulière, en méconnaissance des droits de la défense et du principe d'impartialité.<br/>
<br/>
              5. En deuxième lieu, la seule circonstance que la première présidente aurait estimé que les faits commis étaient de nature à constituer des manquements disciplinaires est sans incidence dès lors que la seule mesure envisagée lors de la convocation à l'entretien et décidée après ce dernier est un avertissement pris sur le fondement de l'article 44 de l'ordonnance du 22 décembre 1958 cité au point 1. La procédure préalable à une sanction disciplinaire mentionnée à l'article 45 de la même ordonnance ne trouvait, dès lors, pas à s'appliquer. Le moyen tiré de ce que la mesure serait entachée de détournement de procédure doit être écarté.<br/>
<br/>
              6. En troisième lieu, l'avertissement contesté a été prononcé à raison de courriels adressés par M. A...à plusieurs de ses collègues les 19 et 22 août 2016, dont il ne conteste ni la matérialité ni la teneur. Eu égard tant à leur contenu et à leur ton qu'à leur multiplication en quelques heures seulement, la première présidente de la cour d'appel de Saint-Denis de La Réunion n'a pas fait une inexacte application des dispositions précitées de l'article 43 de l'ordonnance du 22 décembre 1958 portant loi organique relative au statut de la magistrature en estimant que les faits ainsi reprochés à M. A...étaient constitutifs d'un manquement de ce magistrat aux devoirs de son état, notamment au devoir de délicatesse à l'égard de son supérieur hiérarchique et de collègues magistrats et fonctionnaires, et en lui infligeant, pour ce motif, un avertissement, alors même que, comme le soutient M.A..., les faits en cause auraient été isolés, n'auraient pas porté préjudice à l'institution judiciaire et qu'il s'en serait excusé auprès de ses collègues. En outre, si M. A...soutient, sans être contredit, que ces faits auraient été la conséquence de son surmenage professionnel et de son état de santé mental, il ne ressort pas des pièces du dossier que l'altération de son état de santé ait été de nature à faire obstacle à ce qu'il puisse être sanctionné en raison des actes en cause.<br/>
<br/>
              7. Enfin, le détournement de pouvoir allégué n'est pas établi.  <br/>
<br/>
              8. Il résulte de tout ce qui précède que M. A...n'est pas fondé à demander l'annulation de la décision qu'il attaque. Par suite, sa requête doit être rejetée, y compris les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : La requête de M. A...est rejetée.<br/>
Article 2 : La présente décision sera notifiée à M. Jacques Rousseau et à la garde des sceaux, ministre de la justice.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
