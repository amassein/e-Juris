<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029069573</ID>
<ANCIEN_ID>JG_L_2014_06_000000362284</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/06/95/CETATEXT000029069573.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème / 9ème SSR, 11/06/2014, 362284, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-06-11</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>362284</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème / 9ème SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BARADUC, DUHAMEL, RAMEIX</AVOCATS>
<RAPPORTEUR>M. Frédéric Bereyziat</RAPPORTEUR>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CESSR:2014:362284.20140611</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 28 août et 26 novembre 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour la SA Sefival, dont le siège est route de Saint-Just au Plessis-Saint-Just (60130) ; la SA Sefival demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler les articles 1er, 3 et 4 de l'arrêt n° 11DA00515 du 21 juin 2012 par lesquels la cour administrative d'appel de Douai, faisant partiellement droit à l'appel du ministre chargé du budget, a remis à sa charge, en droits et intérêts de retard, les cotisations supplémentaires d'impôt sur les sociétés et de contributions additionnelles à cet impôt auxquelles elle a été assujettie au titre des exercices clos de 2003 à 2005 et réformé, dans cette mesure, le jugement n° 0900253 du 15 mars 2011 par lequel le tribunal administratif d'Amiens avait accueilli sa demande en décharge dirigée contre ces impositions et contre les majorations exclusives de bonne foi dont ces dernières avaient été assorties ; <br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter les conclusions d'appel du ministre restant en litige ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code général des impôts et le livre des procédures fiscales ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Frédéric Béreyziat, maître des requêtes,<br/>
<br/>
              - les conclusions de Mme Delphine Hedary, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Baraduc, Duhamel, Rameix, avocat de la SA Sefival ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que la SA Equilibre, devenue le 9 janvier 2004 la SA Sefival, a fait l'objet d'une vérification de comptabilité portant sur la période du 1er janvier 2003 au 31 décembre 2005 ; qu'à l'issue des opérations de contrôle, l'administration a remis en cause le droit de la société de reporter, sur les résultats des exercices vérifiés, les déficits constatés avant le 30 juin 2003, au motif qu'était intervenue à cette date une cessation d'entreprise, au sens des dispositions du 5 de l'article 221 du code général des impôts ; que la société a ainsi été assujettie à des cotisations supplémentaires d'impôt sur le revenu et de contribution additionnelle à cet impôt, au titre des exercices clos de 2003 à 2005 ; que ces impositions ont été assorties de la majoration pour manquement délibéré prévue au a) de l'article 1729 du code général des impôts ; que la société a demandé au juge de l'impôt la décharge, en droits et pénalités, de ces suppléments d'imposition ; que le tribunal administratif d'Amiens a fait droit à cette demande par un jugement du 15 mars 2011 ; que la société se pourvoit en cassation contre les articles 1er, 3 et 4 de l'arrêt du 21 juin 2012 par lesquels la cour administrative d'appel de Douai, faisant partiellement droit au recours du ministre chargé du budget, a remis à sa charge les droits supplémentaires et intérêts de retard qui lui avaient été réclamés, réformé dans cette mesure le jugement du tribunal administratif d'Amiens et rejeté ses conclusions tendant à ce que soit mis à la charge de l'Etat, sur le fondement des dispositions de l'article R. 761-1 du code de justice administrative, les frais d'huissier qu'elle avait engagés pour signifier à l'administration fiscale le jugement frappé d'appel ; <br/>
<br/>
              Sur l'arrêt attaqué en tant qu'il statue sur les conclusions en décharge :<br/>
<br/>
              2. Considérant qu'aux termes du I de l'article 209 du code général des impôts relatif à la détermination de la base de l'impôt sur les sociétés, dans sa rédaction applicable au litige : " (...) en cas de déficit subi pendant un exercice, ce déficit est considéré comme une charge de l'exercice suivant et déduit du bénéfice réalisé pendant ledit exercice (...). Si ce bénéfice n'est pas suffisant pour que la déduction puisse être intégralement opérée, l'excédent du déficit est reporté (...) sur les exercices suivants (...) " ; qu'aux termes de l'article 221 du même code, dans sa rédaction issue de l'article 8 de la loi du 30 décembre 1985 de finances pour 1986 : " (...) 5. Le changement de l'objet social ou de l'activité réelle d'une société emporte cessation d'entreprise (...) " ; qu'il résulte de la combinaison de ces dispositions que la mise en oeuvre du droit au report déficitaire est notamment subordonnée à la condition que la société qui s'en prévaut n'ait pas subi, dans son activité réelle, de transformations telles qu'elle ne serait plus, en réalité, la même ; <br/>
<br/>
              3. Considérant qu'il ressort des énonciations de l'arrêt attaqué que, pour écarter le moyen tiré de ce que les changements intervenus dans l'activité de la société n'avaient pas constitué une cessation d'entreprise, au sens des dispositions citées au point 2, la cour a successivement relevé que cette société, créée en 1993 sous sa dénomination ancienne de SA Equilibre, avait exercé jusqu'au 1er juillet 2000 une activité, dont elle tirait plus de 99 pour cent de son chiffre d'affaires, consistant à acheter et à revendre, auprès notamment d'éleveurs et de haras, des produits et matériels destinés aux chevaux ; que cette société avait été rachetée en 1998 par la SA Sogal, qui avait pour activité la fabrication de produits du même type ; que l'activité d'achat-revente de la société contribuable avait été mise en sommeil du 1er juillet 2000 au 30 juin 2003 ; qu'à compter de cette seconde date, la société contribuable s'était engagée, par une convention de prestation de services conclue avec la société Sogal, à mettre son personnel à la disposition de cette dernière, en contrepartie d'une redevance égale au coût réel d'emploi des salariés ainsi mis à disposition, majoré  d'une marge ; que la société contribuable avait en outre cédé, le 22 octobre 2003, la marque " Equilibre " à la société Sogal ; qu'ainsi, à compter du 30 juin 2003 et pour toute la période vérifiée, la société contribuable avait tiré l'intégralité de son chiffre d'affaires de la redevance versée en exécution de cette convention et n'avait plus procédé, pour son compte propre, à l'achat ou à la revente de produits et matériels pour chevaux ; <br/>
<br/>
              4. Considérant qu'en analysant ainsi la nature des opérations réalisées par la société contribuable, la part des différentes activités dans son chiffre d'affaires ainsi que l'évolution des conditions d'exercice de ces activités et en relevant, notamment, le passage d'une activité commerciale exercée aux risques de la société vers une activité de sous-traitance garantissant sur le long terme la totalité du chiffre d'affaires de l'intéressée, pour juger qu'était intervenue, au 30 juin 2003, une cessation d'entreprise au sens des dispositions du 5 de l'article 221 du code général des impôts, la cour n'a pas, contrairement à ce que soutient la société Sefival, méconnu ces dernières dispositions ni donné aux faits qui lui étaient soumis une qualification juridique inexacte ; <br/>
<br/>
              Sur l'arrêt attaqué en tant qu'il statue sur les conclusions relatives aux dépens : <br/>
<br/>
              5. Considérant qu'aux termes de l'article R. 761-1 du code de justice administrative : " Les dépens comprennent les frais d'expertise, d'enquête et de toute autre mesure d'instruction dont les frais ne sont pas à la charge de l'Etat (...) " ;<br/>
<br/>
              6. Considérant qu'en jugeant que les frais d'huissier engagés par la société Sefival pour signifier à l'administration fiscale le jugement du tribunal administratif d'Amiens faisant droit à ses conclusions en décharge n'entraient pas dans le champ des dispositions réglementaires citées ci-dessus et ne pouvaient, dès lors, être mis à la charge de l'Etat au titre de ces dispositions, la cour n'a pas commis d'erreur de droit ;<br/>
<br/>
              7. Considérant qu'il résulte de tout ce qui précède que la société Sefival n'est pas fondée à demander l'annulation des articles 1er, 3 et 4 de l'arrêt qu'elle attaque ;<br/>
<br/>
              Sur les conclusions de la société Sefival présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              8. Considérant que ces dispositions font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : Le pourvoi de la société Sefival est rejeté.<br/>
 Article 2: La présente décision sera notifiée à la société anonyme Sefival et au ministre des finances et des comptes publics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
