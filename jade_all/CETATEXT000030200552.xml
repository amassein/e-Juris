<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030200552</ID>
<ANCIEN_ID>JG_L_2015_01_000000369727</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/20/05/CETATEXT000030200552.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème SSJS, 16/01/2015, 369727, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-01-16</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>369727</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème SSJS</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>OCCHIPINTI</AVOCATS>
<RAPPORTEUR>M. Alain Méar</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emmanuelle Cortot-Boucher</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2015:369727.20150116</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
Procédure contentieuse antérieure<br/>
<br/>
              M. A... C...a demandé au tribunal administratif de Nîmes de prononcer la décharge des cotisations supplémentaires d'impôt sur le revenu et de contributions sociales auxquelles il a été assujetti au titre des années 2005, 2006 et 2007. Par un jugement n° 0903613 du 28 juin 2010, le tribunal administratif de Nîmes a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 10MA03391 du 26 avril 2013, la cour administrative d'appel de Marseille a dit n'y avoir pas lieu à statuer hauteur d'un dégrèvement prononcé par l'administration puis, faisant partiellement droit à l'appel de M.C..., a prononcé la décharge des cotisations supplémentaires d'impôt sur le revenu et de contributions sociales mises à la charge de M. C...au titre des années 2005 à 2007 correspondant à la réintégration dans ses revenus des sommes de 16 027 euros en 2005, 36 443 euros en 2006 et 11 754 euros en 2007, et réformé en ce sens le jugement du 28 juin 2010 du tribunal administratif.<br/>
<br/>
Procédure devant le Conseil d'Etat<br/>
<br/>
              Par un pourvoi enregistré le 27 juin 2013 au secrétariat du contentieux du Conseil d'Etat, le ministre délégué, chargé du budget demande au Conseil d'Etat d'annuler les articles 2, 3 et 4 de l'arrêt de la cour administrative d'appel de Marseille du 26 avril 2013.<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - le code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Alain Méar, Conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Emmanuelle Cortot-Boucher, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Occhipinti, avocat de M. C...;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond qu'à la suite d'une vérification de comptabilité de la SARL GCM, dont M. C...était associé à 98,99 %, portant sur les exercices clos en 2005, 2006 et 2007, l'administration fiscale a remis en cause la déduction, d'une part, de commissions versées à MmeD..., d'autre part, de frais de déplacement et de restauration remboursés à M.C... ; qu'ayant estimé que ce dernier était gérant de fait de la société et seul maître de l'affaire, elle a réintégré les sommes litigieuses dans ses revenus imposables au titre des années 2005 à 2007 ; que, par un jugement du 28 juin 2010, le tribunal administratif de Nîmes a rejeté la demande de M. C...tendant à la décharge des cotisations supplémentaires d'impôt sur le revenu et de contributions sociales résultant de ces réintégrations ; que le ministre délégué, chargé du budget se pourvoit en cassation contre l'arrêt du 26 avril 2013 de la cour administrative d'appel de Marseille en tant que, faisant droit à l'appel de M.C..., il prononce la décharge de ces impositions supplémentaires et réforme dans cette mesure le jugement du tribunal administratif de Nîmes ;<br/>
<br/>
              2. Considérant, en premier lieu, qu'aux termes de l'article 111 du code général des impôts : " Sont notamment considérés comme revenus distribués : / (...) / c. Les rémunérations et avantages occultes ; / (...) " ; que l'octroi d'un avantage sans contrepartie doit être requalifié comme une libéralité représentant un avantage occulte constitutif d'une distribution de bénéfices au sens de ces dispositions, alors même que l'opération est portée en comptabilité et y est assortie de toutes les justifications concernant son objet apparent et l'identité du destinataire, dès lors que cette comptabilisation ne révèle pas, par elle-même, la libéralité en cause ; qu'ainsi, la cour administrative d'appel de Marseille a commis une erreur de droit en jugeant qu'une commission ne pouvait être qualifiée d'avantage occulte dès lors qu'elle avait été portée en charge avec l'indication de son objet, de son montant et de son destinataire, sans rechercher si ces indications pouvaient dissimuler l'octroi d'une libéralité ; que le ministre est donc fondé à demander pour ce motif l'annulation de l'arrêt attaqué en tant qu'il statue sur le bien-fondé des impositions supplémentaires résultant de la réintégration dans les revenus de M. C...des commissions accordées par la SARL GCM à MmeD... ; <br/>
<br/>
              3. Considérant, en second lieu, que les remboursements de frais, même non justifiés, consentis à un gérant majoritaire de SARL, qui sont susceptibles d'être regardés comme un complément de rémunération trouvant son origine dans l'exercice des fonctions de l'intéressé, constituent ainsi, en principe, un élément de sa rémunération imposable, en application de l'article 62 du code général des impôts, dans la catégorie des rémunérations alloués aux gérants majoritaires de SARL, sauf si leur montant, ajouté aux autres éléments de la rémunération, a pour effet de porter le total de celle-ci à un niveau excessif ; qu'ainsi, la cour administrative d'appel de Marseille a commis une erreur de droit en jugeant que les sommes correspondant aux frais de déplacements et de restauration non justifiés devaient être imposées entre les mains de M. C...dans la catégorie des revenus de capitaux mobiliers au motif qu'elles n'avaient pu être déduites des bénéfices sociaux de l'entreprise, sans rechercher si le montant de ces sommes, ajouté aux autres éléments de la rémunération de M.C..., avait pour effet de porter le total de cette rémunération à un niveau excessif ; que le ministre est donc fondé à demander pour ce motif l'annulation de l'arrêt attaqué en tant qu'il statue sur le bien-fondé des impositions supplémentaires résultant de la réintégration dans les revenus de M. C...des frais de déplacements et de restauration non justifiés qui lui avaient été versés par la SARL GCM ;<br/>
<br/>
              4. Considérant qu'il résulte de ce qui précède que le ministre est fondé à demander l'annulation des articles 2, 3 et 4 de l'arrêt attaqué ; <br/>
<br/>
              5. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 font obstacle à ce qu'il soit fait droit aux conclusions présentées sur le fondement de ces dispositions par l'avocat de M.C...;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Les articles 2, 3 et 4 de l'arrêt de la cour administrative d'appel de Marseille du 26 avril 2013 sont annulés.<br/>
Article 2 : L'affaire est renvoyée, dans cette mesure, à la cour administrative d'appel de Marseille.<br/>
Article 3 : Les conclusions présentées par Me B...Occhipinti, avocat de M.C..., au titre des dispositions de l'article L. 761-1 du code de justice administrative et de l'article 37 de la loi du 10 juillet 1991 sont rejetées. <br/>
Article 4 : La présente décision sera notifiée au ministre des finances et des comptes publics et à Monsieur A...C....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
