<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026589669</ID>
<ANCIEN_ID>JG_L_2012_11_000000360252</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/58/96/CETATEXT000026589669.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème et 2ème sous-sections réunies, 07/11/2012, 360252</TITRE>
<DATE_DEC>2012-11-07</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>360252</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème et 2ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DE CHAISEMARTIN, COURJON ; SCP BARTHELEMY, MATUCHANSKY, VEXLIARD</AVOCATS>
<RAPPORTEUR>M. Jean-Dominique Nuttens</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Bertrand Dacosta</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2012:360252.20121107</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 15 juin et 2 juillet 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour la Polynésie française, représentée par son président domicilié BP 2551 à Papeete (98713) ; la Polynésie française demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'ordonnance n° 1200255 du 30 mai 2012 par laquelle le juge des référés du tribunal administratif de la Polynésie française, statuant en application de l'article L. 551-24 du code de justice administrative et sur la requête de la société JL Polynésie, a annulé la procédure de passation du marché public de travaux relatif à l'aménagement du carrefour dénivelé de la mairie de la commune de Punaauia ; <br/>
<br/>
              2°) statuant en référé, de rejeter la demande de la société JL Polynésie ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la loi organique n° 2004-192 du 27 février 2004 ;<br/>
<br/>
              Vu la délibération de l'Assemblée de la Polynésie française n° 84-20 du 1er mars 1984 modifiée portant code des marchés publics de toute nature passés au nom de la Polynésie française et de ses établissements publics ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Dominique Nuttens, Maître des Requêtes en service extraordinaire,  <br/>
<br/>
              - les observations de la SCP de Chaisemartin, Courjon, avocat de la Polynésie française et de la SCP Barthélemy, Matuchansky, Vexliard, avocat de la société JL Polynésie,<br/>
<br/>
              - les conclusions de M. Bertrand Dacosta, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP de Chaisemartin, Courjon, avocat de la Polynésie française et à la SCP Barthélemy, Matuchansky, Vexliard, avocat de la société JL Polynésie ;<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'aux termes de l'article L. 551-24 du code de justice administrative : " (...) en Polynésie française (...), le président du tribunal administratif, ou le magistrat qu'il délègue, peut être saisi en cas de manquement aux obligations de publicité et de mise en concurrence auxquelles est soumise la passation des marchés et contrats publics en vertu de dispositions applicables localement. / Les personnes habilitées à agir sont celles qui ont un intérêt à conclure le contrat et qui sont susceptibles d'être lésées par ce manquement, ainsi que le haut-commissaire de la République dans le cas où le contrat est conclu ou doit être conclu par une collectivité territoriale ou un établissement public local. / Le président du tribunal administratif peut être saisi avant la conclusion du contrat. Il peut ordonner à l'auteur du manquement de se conformer à ses obligations et suspendre la passation du contrat ou l'exécution de toute décision qui s'y rapporte. Il peut également annuler ces décisions et supprimer les clauses ou prescriptions destinées à figurer dans le contrat et qui méconnaissent lesdites obligations. Dès qu'il est saisi, il peut enjoindre de différer la signature du contrat jusqu'au terme de la procédure et pour une durée maximum de vingt jours. / Le président du tribunal administratif ou son délégué statue en premier et dernier ressort en la forme des référés " ;<br/>
<br/>
              2. Considérant qu'il ressort des pièces du dossier soumis au juge des référés que, par un avis d'appel public à la concurrence du 6 octobre 2011, la Polynésie française a engagé une procédure d'appel d'offres ouvert en vue de la passation d'un marché de travaux relatif à l'aménagement du carrefour de la mairie de Punaauia ; que l'offre du groupement d'entreprises dont la société JL Polynésie était le mandataire a été rejetée par décision du 7 mai 2012 du ministre de l'équipement et des transports terrestres de la Polynésie française ; que, par l'ordonnance attaquée du 30 mai 2012, le juge des référés du tribunal administratif de la Polynésie française a annulé la procédure de passation de ce marché ;<br/>
<br/>
              3. Considérant, en premier lieu, que pour faire droit aux conclusions présentées par la société JL Polynésie, le juge des référés a relevé que le manquement ayant consisté à ne pas informer les candidats de l'existence d'un critère de choix des offres d'une particulière importance était constitutif d'un manquement aux obligations de publicité et de concurrence ayant lésé la société JL Polynésie ; que, ce faisant, il a implicitement mais nécessairement répondu au moyen tiré par la Polynésie française de ce que la société JL Polynésie n'était pas habilitée à agir sur le fondement de l'article L. 551-24 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              4. Considérant, en deuxième lieu, qu'aux termes de l'article 20 du code des marchés publics de toute nature passés au nom du territoire de la Polynésie française et de ses établissements publics : " (...) L'avis d'appel d'offres fait connaître au moins : / (...) 7) Le cas échéant, les éléments de l'offre dont il sera particulièrement tenu compte lors du dépouillement pour l'attribution du marché (...) " ; qu'aux termes de l'article 25 du même code : " (...) l'autorité compétente choisit librement l'offre qu'elle juge la plus intéressante, en tenant compte du prix des prestations, de leur coût d'utilisation, de leur valeur technique, des garanties professionnelles et financières présentées par chacun des candidats, du délai d'exécution ainsi que du plan de charge des entreprises. L'autorité compétente peut décider que d'autres considérations entrent en ligne de compte ; dans ce cas, elles devront avoir été spécifiées dans l'avis d'appel d'offres. (...) " ; <br/>
<br/>
              5. Considérant, toutefois, qu'aux termes de l'article 28-1 de la loi organique du 27 février 2004 portant statut d'autonomie de la Polynésie française : " La Polynésie française fixe les règles applicables à la commande publique de la Polynésie française et de ses établissements publics dans le respect des principes de liberté d'accès, d'égalité de traitement des candidats, de transparence des procédures, d'efficacité de la commande publique et de bon emploi des deniers publics " ; que, pour assurer le respect de ces principes, l'information appropriée des candidats sur les critères d'attribution d'un marché public est nécessaire, dès l'engagement de la procédure d'attribution du marché, dans l'avis d'appel public à concurrence ou le cahier des charges tenu à la disposition des candidats ; qu'en Polynésie française, où les dispositions en vigueur prévoient l'application d'au moins six critères énumérés par l'article 25 du code applicable localement, susceptibles d'être complétés par d'autres critères, le respect des principes fondamentaux de la commande publique implique, dans tous les cas, que le pouvoir adjudicateur fournisse aux candidats l'information appropriée sur les conditions de mise en oeuvre des critères d'attribution, y compris lorsque les six critères prévus par le code applicable localement ne sont pas complétés par d'autres critères spécifiques au marché en cause, en indiquant la hiérarchisation ou la pondération de ces critères, même lorsque leur est attribuée une égale importance ; qu'il suit de là que le juge des référés a pu énoncer, sans commettre d'erreur de droit, que l'autorité compétente devait indiquer aux candidats la hiérarchisation des différents critères lorsqu'une telle hiérarchisation était retenue pour le jugement des offres ;<br/>
<br/>
              6. Considérant, en dernier lieu, que le juge des référés a pu, par une appréciation souveraine exempte de dénaturation et en motivant suffisamment son ordonnance sur ce point, estimer que le délai d'exécution du marché, dont la place parmi les critères d'attribution du marché n'avait pas été communiquée aux candidats, avait constitué un élément de l'offre dont l'autorité compétente avait particulièrement tenu compte ;<br/>
<br/>
              7. Considérant qu'il résulte de ce qui précède que le pourvoi de la Polynésie française doit être rejeté, y compris ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative ; qu'il y a lieu, en application des mêmes dispositions, de mettre à la charge de la Polynésie française le versement d'une somme de 3 000 euros à la société JL Polynésie ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi présenté par la Polynésie française est rejeté.<br/>
Article 2 : La Polynésie française versera à la société JL Polynésie la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
Article 3 : La présente décision sera notifiée à la Polynésie française et à la société JL Polynésie.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">46-01-02-02 OUTRE-MER. DROIT APPLICABLE. STATUTS. POLYNÉSIE FRANÇAISE. - ARTICLE 28-1 DE LA LOI ORGANIQUE DU 27 FÉVRIER 2004 DONNANT COMPÉTENCE À LA POLYNÉSIE FRANÇAISE POUR FIXER LES RÈGLES APPLICABLES À LA COMMANDE PUBLIQUE DANS LE RESPECT DES PRINCIPES DE LIBERTÉ D'ACCÈS, D'ÉGALITÉ DE TRAITEMENT DES CANDIDATS ET DE TRANSPARENCE DES PROCÉDURES - CONSÉQUENCES - 1) OBLIGATION D'INFORMER LES CANDIDATS SUR LES CRITÈRES D'ATTRIBUTION DÈS L'ENGAGEMENT DE LA PROCÉDURE, DANS L'AVIS D'APPEL PUBLIC À CONCURRENCE OU LE CAHIER DES CHARGES TENU À DISPOSITION DES CANDIDATS [RJ1] - 2) ETENDUE DE L'OBLIGATION D'INFORMATION, COMPTE TENU DU DROIT EN VIGUEUR EN POLYNÉSIE FRANÇAISE.
</SCT>
<ANA ID="9A"> 46-01-02-02 Aux termes de l'article 28-1 de la loi organique n° 2004-192 du 27 février 2004 portant statut d'autonomie de la Polynésie française : « La Polynésie française fixe les règles applicables à la commande publique de la Polynésie française et de ses établissements publics dans le respect des principes de liberté d'accès, d'égalité de traitement des candidats, de transparence des procédures, d'efficacité de la commande publique et de bon emploi des deniers publics ».,,1) Pour assurer le respect de ces principes, l'information appropriée des candidats sur les critères d'attribution d'un marché public est nécessaire, dès l'engagement de la procédure d'attribution du marché, dans l'avis d'appel public à concurrence ou le cahier des charges tenu à la disposition des candidats.,,2) En Polynésie française, où les dispositions en vigueur prévoient l'application d'au moins six critères énumérés par l'article 25 du code des marchés publics applicable localement, susceptibles d'être complétés par d'autres critères, le respect des principes fondamentaux de la commande publique implique, dans tous les cas, que le pouvoir adjudicateur fournisse aux candidats l'information appropriée sur les conditions de mise en oeuvre des critères d'attribution, y compris lorsque les six critères prévus par le code applicable localement ne sont pas complétés par d'autres critères spécifiques au marché en cause, en indiquant la hiérarchisation ou la pondération de ces critères, même lorsque leur est attribuée une égale importance.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, Section, 30 janvier 2009, Agence nationale pour l'emploi (ANPE), n° 290236, p. 3.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
