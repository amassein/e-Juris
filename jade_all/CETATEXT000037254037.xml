<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037254037</ID>
<ANCIEN_ID>JG_L_2018_07_000000417650</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/25/40/CETATEXT000037254037.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème chambre, 26/07/2018, 417650, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-07-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>417650</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MARLANGE, DE LA BURGADE ; SCP GATINEAU, FATTACCINI</AVOCATS>
<RAPPORTEUR>Mme Dominique Chelle </RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Cécile Barrois de Sarigny</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:417650.20180726</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme B...A...a demandé au tribunal administratif de Lyon d'annuler la décision du 27 mars 2017 par laquelle le directeur de la caisse d'allocations familiales du Rhône a rejeté, sur avis de la commission de recours amiable, sa demande tendant au bénéfice de l'aide personnalisée au logement pour la période comprise entre le 1er janvier 2014 et le 31 mars 2015. Par une ordonnance n° 1704242 du 5 septembre 2017, prise sur le fondement de l'article R. 222-1 du code de justice administrative, le président de la 8ème chambre du tribunal administratif de Lyon a rejeté sa demande. <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés au secrétariat du contentieux du Conseil d'Etat les 26 janvier et 5 avril 2018, Mme A...demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) de renvoyer l'affaire au tribunal administratif de Lyon ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 000 euros à verser à la SCP Marlange, de la Burgade, son avocat, au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la loi n° 91-647 du 10 juillet 1991 ; <br/>
              - le décret n° 91-1266 du 19 décembre 1991 ; <br/>
              -  le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Dominique Chelle, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Cécile Barrois de Sarigny, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Marlange, de la Burgade, avocat de Mme A...et à la SCP Gatineau, Fattaccini, avocat de la caisse d'allocations familiales du Rhône.<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant que la loi du 10 juillet 1991 relative à l'aide juridique prévoit, en son article 2, que les personnes physiques dont les ressources sont insuffisantes pour faire valoir leurs droits en justice peuvent bénéficier d'une aide juridictionnelle et, en son article 25, que le bénéficiaire de l'aide juridictionnelle a droit à l'assistance d'un avocat choisi par lui ou, à défaut, désigné par le bâtonnier de l'ordre des avocats ; qu'il résulte des articles 76 et 77 du décret du 19 décembre 1991 portant application de cette loi que si la personne qui demande l'aide juridictionnelle ne produit pas de document attestant l'acceptation d'un avocat choisi par lui, l'avocat peut être désigné sur-le-champ par le représentant de la profession qui siège au bureau d'aide juridictionnelle, à condition qu'il ait reçu délégation du bâtonnier à cet effet ; <br/>
<br/>
              2. Considérant qu'il ressort des pièces du dossier soumis au juge du fond que, par un courrier du 10 mai 2017, présenté sans le ministère d'un avocat, Mme A...a saisi le tribunal administratif de Lyon d'une demande tendant à l'annulation de la décision du 27 mars 2017 par laquelle le directeur de la caisse d'allocations familiales du Rhône avait rejeté, sur avis de la commission de recours amiable, sa demande tendant au bénéfice de l'aide personnalisée au logement pour la période comprise entre le 1er janvier 2014 et le 31 mars 2015 ; qu'elle a, parallèlement, sollicité le bénéfice de l'aide juridictionnelle prévue par les dispositions précitées ; que, par une décision du 30 juin 2017, le président du bureau d'aide juridictionnelle près le tribunal de grande instance de Lyon lui a accordé le bénéfice de l'aide juridictionnelle totale et a désigné un avocat aux fins de la représenter ; que cet avocat a demandé le 28 juillet 2017 la communication des éléments du dossier au greffe du tribunal administratif de Lyon, qui lui a adressé ces éléments le 9 août 2017 ; que, par une ordonnance prise dès le 5 septembre 2017 sur le fondement de l'article R. 222-1 du code de justice administrative, le président de la 8ème chambre a rejeté la requête de Mme A... au motif que l'unique moyen contenu dans son courrier du 10 mai 2017 était inopérant ; <br/>
<br/>
              3. Considérant qu'en se prononçant ainsi, alors que l'avocat désigné au titre de l'aide juridictionnelle n'avait pas encore produit de mémoire et sans l'avoir mis en demeure de le faire en lui impartissant un délai à cette fin, l'auteur de l'ordonnance attaquée n'a pas assuré à la requérante le respect effectif du droit qu'elle tirait de la loi du 10 juillet 1991 ; que, par suite, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, l'ordonnance a été rendue au terme d'une procédure irrégulière et doit être annulée ; <br/>
<br/>
              4. Considérant qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions de Mme A...présentées au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 ; que les dispositions de l'article L. 761-1 font obstacle à ce que la somme que la caisse d'allocations familiales du Rhône demande sur leur fondement soit mise à la charge de Mme A...qui n'est pas, dans la présente espèce, la partie perdante ;<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : L'ordonnance du 5 septembre 2017 du président de la 8e chambre du tribunal administratif de Lyon est annulée.<br/>
<br/>
		Article 2 : L'affaire est renvoyée au tribunal administratif de Lyon.<br/>
<br/>
Article 3 : Les conclusions de Mme A...présentées au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 sont rejetées.<br/>
<br/>
Article 4 : Les conclusions présentées par la caisse d'allocations familiales du Rhône au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée à Mme B...A...et à la caisse d'allocations familiales du Rhône.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
