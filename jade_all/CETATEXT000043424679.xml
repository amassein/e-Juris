<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043424679</ID>
<ANCIEN_ID>JG_L_2021_04_000000450874</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/42/46/CETATEXT000043424679.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 22/04/2021, 450874, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-04-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>450874</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2021:450874.20210422</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et trois mémoires complémentaires, enregistrés les 19 mars, 30 mars, 6 avril et 9 avril 2021 au secrétariat du contentieux du Conseil d'Etat, le syndicat Action et Démocratie demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative :<br/>
<br/>
              1°) d'ordonner la suspension de l'exécution du décret n° 2021-209 du 25 février 2021 relatif à l'organisation de l'examen du baccalauréat général et technologique de la session 2021 pour l'année scolaire 2020-2021 ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 1 000 euros au titre l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Il soutient que :<br/>
              - sa requête est recevable ;<br/>
              - il justifie, eu égard à son objet social, d'un intérêt lui donnant qualité pour agir ;<br/>
              - la condition d'urgence est satisfaite dès lors, d'une part, que les épreuves du baccalauréat auront lieu dans moins de trois mois et, d'autre part, que le décret attaqué porte préjudice tant aux enseignants qu'aux élèves, ces derniers n'ayant aucun intérêt à se voir délivrer un diplôme dans des conditions qui en dénaturent la valeur ;<br/>
              - il existe un doute sérieux quant à la légalité du décret contesté ;<br/>
              - le décret contesté méconnaît les principes de sécurité juridique et de non-rétroactivité des lois et règlements, dès lors qu'en substituant aux épreuves écrites de spécialité et d'évaluations communes la prise en compte des notes de contrôle continu depuis septembre 2020, il modifie tardivement les modalités d'évaluation du baccalauréat et porte atteinte de manière rétroactive à la signification et la valeur de la notation trimestrielle ; <br/>
              - la prise en compte des notes du contrôle continu ne saurait être justifiée par la nécessité d'adapter les conditions d'obtention du baccalauréat, pour la session 2021, à l'impossibilité d'organiser l'examen dans des conditions de sécurité sanitaire, dès lors, d'une part, que les épreuves de spécialité et les évaluations communes avaient d'ores et déjà été reportées au mois de juin et qu'il n'est pas établi que la situation sanitaire en fin d'année scolaire rendrait impossible la tenue de ces épreuves dans des conditions de nature à assurer la sécurité des élèves, des enseignants et des personnels appelés à assurer la surveillance des candidats ;<br/>
              - le décret contesté méconnaît les dispositions de l'arrêté du 16 juillet 2018, qui prévoit que les moyennes annuelles représentent 10 % de la note globale pour l'obtention du baccalauréat, dès lors qu'il a pour effet de conférer à ces moyennes annuelles un poids pouvant aller jusqu'à 32 % de la note globale ;<br/>
              - les articles 2 et 3 de ce décret ont pour effet de faire peser sur l'évaluation formative à laquelle procèdent les enseignants la responsabilité de la réussite ou de l'échec à l'examen de leurs propres élèves, en méconnaissance des dispositions de l'article D. 334-9 du code de l'éducation qui interdit à l'enseignant d'être à la fois intéressé à la réussite de ses élèves dont il est partie prenante et juge de celle-ci ; <br/>
              - le décret contesté méconnaît le principe de liberté pédagogique des enseignants consacré par l'article L. 921-1-1 du code de l'éducation en ce qu'il prévoit que les notes de contrôle continu pourront être harmonisées par une commission, au mépris de la souveraineté des notes initialement attribuées par les enseignants ; <br/>
              - il méconnaît le principe d'égalité dès lors qu'en l'absence de normes précises encadrant les modalités de notation dans le cadre du contrôle continu, ces dernières varient considérablement selon le type d'établissement et le niveau d'exigence des enseignants ;<br/>
              - le décret contesté méconnaît le principe mentionné à l'article D. 334-9 du code de l'éducation selon lequel la notation d'un candidat à un examen ne peut être réalisée par l'enseignant ayant eu le candidat comme élève au cours de l'année scolaire ;<br/>
              - dès lors que les conditions sanitaires permettent l'ouverture des établissements scolaires et l'accueil des élèves, les aménagements prévus par le décret du 25 février 2021 ne sont pas justifiés.  <br/>
<br/>
              Par un mémoire en défense et deux nouveaux mémoires, enregistrés les 1er, 2 et 8 avril 2021, le ministre de l'éducation nationale, de la jeunesse et des sports conclut au rejet de la requête. Il soutient que la requête n'est pas recevable eu égard à l'absence d'intérêt à agir du syndicat requérant, que la condition d'urgence n'est pas satisfaite et qu'aucun moyen de la requête n'est de nature à créer un doute sérieux quant à la légalité du décret contesté.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la Constitution, et notamment son Préambule ;<br/>
              - le code de l'éducation ;<br/>
              - la loi n° 2020-290 du 23 mars 2020 ;<br/>
              - la loi n° 2020-1379 du 14 novembre 2020 ;<br/>
              - la loi n° 2021-160 du 15 février 2021 ; <br/>
              - l'ordonnance n° 2020-1694 du 24 décembre 2020 ;<br/>
              - le décret n° 2018-614 du 16 juillet 2018 ;<br/>
              - le décret n° 2020-1310 du 29 octobre 2020 ;<br/>
              - le décret n° 2021-209 du 25 février 2021 ;<br/>
              - l'arrêté du 16 juillet 2018 modifié relatif aux épreuves du baccalauréat général à compter de la session de 2021 ;<br/>
              - l'arrêté du 16 juillet 2018 relatif aux modalités d'organisation du contrôle continu pour l'évaluation des enseignements dispensés dans les classes conduisant au baccalauréat général et technologique ; <br/>
              - l'arrêté du 11 octobre 2019 modifiant l'arrêté du 16 juillet 2018 relatif aux modalités d'organisation du contrôle continu pour l'évaluation des enseignements dispensés dans les classes conduisant au baccalauréat général et au baccalauréat technologique ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, le syndicat Action et Démocratie, et d'autre part, le ministre de l'éducation nationale, de la jeunesse et des sports ;<br/>
<br/>
              Ont été entendus lors de l'audience publique du 2 avril 2021, à 15 heures : <br/>
<br/>
              - les représentants du syndicat requérant ; <br/>
<br/>
              - les représentants du ministre de l'éducation nationale, de la jeunesse et des sports ; <br/>
<br/>
              à l'issue de laquelle le juge des référés a différé la clôture de l'instruction au 9 avril à 18 heures ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision ". <br/>
<br/>
              Sur le cadre juridique du litige :<br/>
<br/>
              2. L'article 20 du décret du 16 juillet 2018 relatif aux enseignements conduisant au baccalauréat général et aux formations technologiques conduisant au baccalauréat technologique a modifié l'article D. 334-4 du code de l'éducation en disposant, pour ce qui est du baccalauréat général, que : " L'évaluation des enseignements obligatoires repose sur des épreuves terminales et sur des évaluations de contrôle continu tout au long du cycle terminal " et qu'" Un arrêté du ministre chargé de l'éducation nationale définit les modalités d'organisation du contrôle continu pour le baccalauréat général et les conditions dans lesquelles est attribuée une note de contrôle continu aux candidats qui ne suivent les cours d'aucun établissement, aux candidats inscrits dans un établissement d'enseignement privé hors contrat, aux candidats scolarisés au Centre national d'enseignement à distance et aux sportifs de haut niveau (...) ".<br/>
<br/>
              3. Pour l'application de ces dispositions, l'arrêté du 16 juillet 2018 relatif aux modalités d'organisation du contrôle continu pour l'évaluation des enseignements dispensés dans les classes conduisant au baccalauréat général et technologique a fixé, par ses articles 1er à 8, les modalités d'organisation de ce contrôle pour les candidats scolarisés dans les établissements publics d'enseignement et dans les établissements d'enseignement privés sous contrat. Les articles 1er et 2 de l'arrêté du 16 juillet 2018 prévoient, d'une part, que les candidats scolarisés dans les établissements d'enseignement publics ou privés sous contrat subissent trois sessions d'épreuves de contrôle continu, deux en classe de première et une en classe de terminale et, d'autre part, que la note de contrôle continu attribuée aux candidats au baccalauréat qui sont scolarisés dans les établissements publics d'enseignement et dans les établissements d'enseignement privés sous contrat compte pour quarante pour cent des coefficients attribués pour l'examen et est fixée, pour une part de trente pour cent, sur la base de trois sessions d'épreuves dites " évaluations communes " et, pour une part de dix pour cent, sur la base de l'évaluation des résultats de l'élève au cours du cycle terminal, telle qu'elle résulte des notes attribuées par ses professeurs. Le I de l'article 9 de de l'arrêté du 16 juillet 2018 prévoit que, pour les candidats scolarisés dans les établissements privés hors contrat, ceux-ci sont convoqués à une évaluation ponctuelle pour l'enseignement de spécialité ne donnant pas lieu à une épreuve terminale et à une évaluation ponctuelle pour chacun des autres enseignements faisant l'objet d'évaluations communes de contrôle continu, la note de contrôle continu mentionnée à l'article 1er  étant fixée, conformément au II de l'article 9, en tenant compte des notes obtenues aux évaluations ponctuelles prévues au I de ce même article. Enfin, par un arrêté du 11 octobre 2019, le ministre de l'éducation nationale, de la jeunesse et des sports a modifié l'article 2 et le I de l'article 9 de l'arrêté du 16 juillet 2018 en reportant du deuxième au troisième trimestre de l'année de terminale la série d'épreuves communes de contrôle continu passées en classe de terminale, tant par les candidats scolarisés dans l'enseignement public et dans l'enseignement privé sous contrat que pour les autres candidats.<br/>
<br/>
              4. Par ailleurs, aux termes de l'article 3 de l'ordonnance du 24 décembre 2020 relative à l'organisation des concours et examens pendant la crise sanitaire née de l'épidémie de covid-19, prise sur le fondement de l'article 11 de la loi du 23 mars 2020 d'urgence pour faire face à l'épidémie de covid-19 et de l'article 10 de la loi du 14 novembre 2020 autorisant la prorogation de l'état d'urgence sanitaire : " Nonobstant toute disposition législative ou réglementaire contraire, les autorités compétentes pour la détermination des modalités d'accès aux formations de l'enseignement supérieur dispensées par les établissements relevant des livres IV et VII du code de l'éducation ainsi que pour la détermination des modalités de délivrance des diplômes de l'enseignement supérieur, y compris le baccalauréat, peuvent apporter à ces modalités les adaptations nécessaires à leur mise en oeuvre. / S'agissant des épreuves des examens ou concours, ces adaptations peuvent porter, dans le respect du principe d'égalité de traitement des candidats, sur leur nature, leur nombre, leur contenu, leur coefficient ou leurs conditions d'organisation, qui peut notamment s'effectuer de manière dématérialisée. " <br/>
<br/>
              5. Sur le fondement des dispositions citées au point 5, les articles 2 et 3 du décret du 25 février 2021 relatif à l'organisation de l'examen du baccalauréat général et technologique de la session 2021 ont prévu que les notes retenues au titre des épreuves terminales des enseignements de spécialité et des évaluations communes de la classe de terminale sont les moyennes annuelles de la classe de terminale inscrites dans le livret scolaire pour les enseignements concernés, pour les candidats scolarisés dans les établissements publics, dans les établissements privés sous contrat ou dans les établissements scolaires français à l'étranger qui figurent sur la liste prévue à l'article R. 451-2 du code de l'éducation pour le cycle terminal du lycée général et technologique. <br/>
<br/>
              Sur la demande en référé : <br/>
<br/>
              6. Le syndicat Action et Démocratie demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative, d'ordonner la suspension de l'exécution du décret du 25 février 2021 relatif à l'organisation de l'examen du baccalauréat général et technologique de la session 2021 pour l'année scolaire 2020-2021, en ce qu'il substitue aux épreuves écrites de spécialité et d'évaluations communes la prise en compte des notes obtenues par les élèves des établissements publics et privés sous contrat depuis le mois de septembre 2020 dans le cadre du contrôle continu. <br/>
<br/>
              7. En premier lieu, si les articles 2 et 3 du décret du 25 février 2021 conduisent à retenir, ainsi qu'il a été dit au point 6, les moyennes annuelles de la classe de terminale inscrites dans le livret scolaire au titre des épreuves terminales des enseignements de spécialité et pour les évaluations communes de la classe de terminale sanctionnant les enseignements concernés, ces dispositions se bornent à dispenser les candidats au baccalauréat de la session 2021 scolarisés dans les établissements publics, les établissements privés sous contrat et les établissements scolaires français à l'étranger figurant sur la liste prévue à l'article R. 451-2 du code de l'éducation de subir les épreuves terminales prévues dans ces matières par l'arrêté du 16 juillet 2018 relatif aux épreuves du baccalauréat général à compter de la session de 2021 et n'ont ni pour objet, ni pour effet d'instituer de nouvelles épreuves ou modalités d'évaluation impliquant une préparation spécifique de la part des élèves de terminale concernés par cette mesure. Par suite, le moyen tiré de ce qu'en substituant en cours d'année scolaire le contrôle continu aux épreuves prévues par les textes pour les candidats qui se présenteront au baccalauréat en 2021, le décret contesté priverait ces derniers de la possibilité de disposer d'un délai raisonnable pour s'adapter aux nouvelles modalités d'organisation de cet examen et porterait ainsi atteinte au principe de sécurité juridique n'est pas propre à créer un doute sérieux quant à sa légalité.<br/>
<br/>
              8. En deuxième lieu, s'il est soutenu que les dispositions du décret contesté porteraient atteinte à la liberté pédagogique des enseignants en ce que le poids accru du contrôle continu dans la délivrance du baccalauréat exposerait ces derniers à un risque de contestation des évaluations de leurs élèves, voire de pressions de la part des élèves et de leurs parents, il résulte des dispositions de l'article L. 912-1-1 du code l'éducation que la liberté pédagogique de l'enseignant, qui s'exerce dans le respect des programmes et des instructions du ministre chargé de l'éducation nationale et dans le cadre du projet d'école ou d'établissement, concerne à titre principal la manière dont l'enseignement est délivré, et ne fait pas obstacle à ce que les pouvoirs publics modifient, à titre exceptionnel pour tenir compte du contexte de crise sanitaire, le déroulement ou les modalités d'évaluations des épreuves du baccalauréat. Il s'ensuit que le moyen tiré de ce que le décret du 25 février 2021 méconnaîtrait ce principe en modifiant rétroactivement la signification et la valeur des évaluations attribuées par les enseignants au cours des premiers et deuxième trimestres de l'année scolaire 2020-2021 n'est pas de nature à faire naître, en l'état de l'instruction, un doute sérieux quant à sa légalité.<br/>
<br/>
              9. En troisième lieu, l'harmonisation des notes du contrôle continu, prévue par l'article 6 du décret du 25 février 2021, est destinée à garantir l'égalité dans l'évaluation des candidats. Aux termes de ces dispositions, le jury doit s'assurer qu'il n'existe pas de discordance manifeste entre les notes issues des moyennes annuelles des livrets scolaires retenues au titre des évaluations communes de la classe de terminale et des épreuves terminales des enseignements de spécialité. Il peut procéder à une harmonisation des notes issues des moyennes annuelles des livrets scolaires retenues au titre des évaluations communes de la classe de terminale et des épreuves terminales des enseignements de spécialité, en s'appuyant, le cas échéant, sur les moyennes annuelles du livret scolaire des élèves de terminale des années scolaires antérieures dans les enseignements comparables et sur les notes obtenues par les candidats des sessions 2018 et 2019 aux épreuves terminales à ces mêmes enseignements. Il s'ensuit qu'au vu des objectifs fixés par l'article 6 du décret et des différents éléments d'appréciation mis à la disposition du jury pour procéder à cette harmonisation des notes, les moyens tirés, d'une part, de la méconnaissance par ce texte du principe d'égalité entre les candidats, en ce qu'il prévoit la prise en compte au titre des épreuves du baccalauréat de notations continu dont le niveau peut varier selon l'établissement et les exigences des professeurs et, d'autre part, du caractère arbitraire du processus d'harmonisation des notes qu'il institue ne peuvent être regardés, en l'état de l'instruction, comme sérieux et de nature à faire naître un doute sérieux quant à la légalité des dispositions contestées. Il en va de même du moyen tiré de ce que le poids accru donné aux notes obtenues par les élèves en cours d'année scolaire méconnaîtrait la règle fixée à l'article D. 334-9 du code de l'éducation selon laquelle les examinateurs ne peuvent évaluer leurs élèves de l'année en cours, dès lors qu'aucun principe n'interdit la prise en compte des évaluations issues du contrôle continu dans le cadre d'un examen tel que le baccalauréat. <br/>
<br/>
              10. En quatrième et dernier lieu, il résulte de l'instruction et des échanges lors de l'audience que la diminution de la capacité d'accueil d'une partie des lycées, afin d'assurer le respect des règles sanitaires imposées par le décret du 29 octobre 2020 prescrivant les mesures générales nécessaires pour faire face à l'épidémie de covid-19 dans le cadre de l'état d'urgence sanitaire ne permettait pas d'envisager l'organisation dans des conditions normales les évaluations communes et les épreuves terminales portant sur les enseignements de spécialités pour les quelques 550 000 candidats à la session 2021 du baccalauréat scolarisés dans les établissements d'enseignement publics ou privés sous contrat. Qui plus est, la reprise de la diffusion de l'épidémie s'est traduite au cours de la période récente par une aggravation significative sur l'ensemble du territoire national de la diffusion des différents variants du virus, conduisant les pouvoirs publics à annoncer la généralisation des mesures jusque-là imposées à un nombre limité de départements et la suspension de l'accueil des élèves dans les établissements d'enseignement secondaire du 5 avril jusqu'au 3 mai 2021. Par suite, le moyen tiré de ce que les aménagements apportés par le décret du 25 février 2021 aux modalités d'évaluation des épreuves du baccalauréat ne seraient pas justifiés par l'état actuel de la situation sanitaire n'est pas de nature à créer un doute sérieux quant à la légalité des dispositions contestées. <br/>
<br/>
              11. Il résulte de tout ce qui précède que, sans qu'il soit besoin de se prononcer, d'une part, sur la fin de non-recevoir soulevée en défense par le ministre de l'éducation, de la jeunesse et des sports et, d'autre part, sur la condition d'urgence, la requête du syndicat Action et Démocratie doit être rejetée, y compris ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
              ------------------<br/>
<br/>
Article 1er : La requête du syndicat Action et Démocratie est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée au syndicat Action et Démocratie et au ministre de l'éducation nationale, de la jeunesse et des sports.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
