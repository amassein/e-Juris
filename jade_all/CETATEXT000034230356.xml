<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034230356</ID>
<ANCIEN_ID>JG_L_2017_03_000000407206</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/23/03/CETATEXT000034230356.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème chambre, 20/03/2017, 407206, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-03-20</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>407206</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Sophie-Caroline de Margerie</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Béatrice Bourgeois-Machureau</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2017:407206.20170320</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par deux mémoires, enregistrés les 26 janvier et 20 février 2017 au secrétariat du contentieux du Conseil d'Etat, l'association ALCALY (Alternative au contournement routier de Lyon) demande au Conseil d'Etat, en application de l'article 23-5 de l'ordonnance n° 58-1067 du 7 novembre 1958 et à l'appui de sa requête tendant à l'annulation pour excès de pouvoir des décisions par lesquelles le Premier ministre et la ministre de l'environnement, de l'énergie et de la mer, chargée des relations internationales sur le climat ont refusé de faire droit à sa demande d'abrogation du décret du 16 juillet 2008 déclarant d'utilité publique les travaux de construction de l'autoroute A 45 entre Saint-Etienne et Lyon sur le territoire des communes de Cellieu, Chagnon, Genilac, L'Horme, La Fouillouse, La Talaudière, La Tour-en-Jarez, L'Etrat, Saint-Chamond, Saint-Jean-Bonnefonds, Saint-Joseph, Saint-Martin-la-Plaine, Saint-Romain-en-Jarez, Sorbiers, Valfleury dans le département de la Loire et de Brignais, Chassagny, Montagny, Mornant, Orliénas, Saint-Andéol-le-Château, Saint-Jean-de-Touslas, Saint-Maurice-sur-Dargoire, Taluyers et Vourles dans le département du Rhône et portant mise en compatibilité des plans locaux d'urbanisme des communes de Cellieu, Chagnon, Genilac, L'Horme, La Fouillouse, La Talaudière, La Tour-en-Jarez, L'Etrat, Saint-Chamond, Saint-Jean-Bonnefonds, Saint-Joseph, Saint-Martin-la-Plaine, Sorbiers dans le département de la Loire et de Brignais, Chassagny, Montagny, Mornant, Orliénas, Saint-Andéol-le-Château, Saint-Jean-de-Touslas, Saint-Maurice-sur-Dargoire, Taluyers et Vourles dans le département du Rhône, de renvoyer au Conseil constitutionnel la question de la conformité aux droits et libertés garantis par la Constitution de l'article L. 111-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ; <br/>
<br/>
              Vu : <br/>
              - la Constitution, notamment son article 61-1 et son Préambule ; <br/>
              - l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
              - le code de justice administrative, notamment son article L. 111-1 ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Sophie-Caroline de Margerie, conseiller d'Etat,<br/>
<br/>
              - les conclusions de Mme Béatrice Bourgeois-Machureau, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1.	Considérant qu'aux termes du premier alinéa de l'article 23-5 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel : " Le moyen tiré de ce qu'une disposition législative porte atteinte aux droits et libertés garantis par la Constitution peut être soulevé (...) à l'occasion d'une instance devant le Conseil d'Etat (...) " ; qu'il résulte des dispositions de ce même article que le Conseil constitutionnel est saisi de la question prioritaire de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux ;<br/>
<br/>
              2.	Considérant que si l'association ALCALY soutient que les dispositions de l'article L. 111-1 du code de justice administrative méconnaîtraient le droit à un procès équitable et le droit à un recours effectif garantis par l'article 16 de la Déclaration des droits de l'homme et du citoyen, ces dispositions, qui ont pour seul objet de placer le Conseil d'Etat au sommet de l'un des deux ordres de juridiction que la Constitution reconnaît, ne portent, par elles-mêmes, aucune atteinte au droit à un procès équitable et à un recours effectif garantis par l'article 16 de la Déclaration des droits de l'homme et du citoyen ; que par suite, la question soulevée, qui n'est pas nouvelle, ne présente pas un caractère sérieux ; qu'ainsi, sans qu'il soit besoin de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité invoquée, le moyen tiré de ce que l'article L. 111-1 du code de justice administrative porterait atteinte aux droits et libertés garantis par la Constitution doit être écarté ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : Il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée par l'association ALCALY.<br/>
Article 2 : La présente décision sera notifiée à l'association ALCALY.<br/>
Copie en sera adressée au Conseil constitutionnel, à la ministre de l'environnement, de l'énergie et de la mer, chargée des relations internationales sur le climat et au Premier ministre. <br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
