<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032008488</ID>
<ANCIEN_ID>JG_L_2016_02_000000382016</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/00/84/CETATEXT000032008488.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème / 2ème SSR, 10/02/2016, 382016, Publié au recueil Lebon</TITRE>
<DATE_DEC>2016-02-10</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>382016</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème / 2ème SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BOUZIDI, BOUHANNA</AVOCATS>
<RAPPORTEUR>M. Grégory Rzepski</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Olivier Henrard</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2016:382016.20160210</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante:<br/>
<br/>
              La communauté d'agglomération Côte basque - Adour a demandé au tribunal administratif de Pau de condamner la société Exedra Sud Aquitaine à lui verser la somme de 4 460,64 euros correspondant aux frais de l'expertise judiciaire réalisée le 10 mars 2011.<br/>
<br/>
              Par un jugement n° 1202244 du 25 avril 2014, le magistrat désigné par le président du tribunal administratif de Pau a rejeté cette demande.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 30 juin et 30 septembre 2014 au secrétariat du contentieux du Conseil d'Etat, la communauté d'agglomération Côte basque - Adour demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce  jugement ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande tendant à la condamnation de la société Exedra Sud Aquitaine ;<br/>
<br/>
              3°) de mettre à la charge de la société Exedra Sud Aquitaine le versement à la communauté d'agglomération Côte basque - Adour de la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Grégory Rzepski, maître des requêtes en service extraordinaire,<br/>
<br/>
              - les conclusions de M. Olivier Henrard, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Bouzidi, Bouhanna, avocat de la communaute d'agglomeration Côte basque - Adour ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que la communauté d'agglomération Bayonne-Anglet-Biarritz a conclu le 10 octobre 2008 avec la société Exedra Sud Aquitaine un marché relatif à la fourniture et à l'installation d'un dispositif de récupération et de traitement des eaux usées des bateaux stationnés au port de plaisance d'Anglet ; que le dispositif de récupération des eaux ne fonctionnant pas, la communauté d'agglomération Bayonne-Anglet-Biarritz a saisi d'une demande d'expertise le juge des référés du tribunal administratif de Pau qui y a fait droit par ordonnance du 24 novembre 2010 ; que, par ordonnance de taxe du 25 mars 2011, le président du tribunal administratif de Pau a fixé à 4 460,64 euros TTC les frais et honoraires de l'expertise et les a mis à la charge de la communauté d'agglomération Bayonne-Anglet-Biarritz ; que la communauté d'agglomération Côte basque - Adour, qui vient aux droits de la communauté d'agglomération Bayonne-Anglet-Biarritz, a demandé au tribunal administratif de Pau de condamner la société Exedra Sud Aquitaine à l'indemniser, sur le fondement de la responsabilité contractuelle, des préjudices subis et tenant au paiement de ces frais et honoraires ; que, par le jugement attaqué du 25 avril 2014, le tribunal administratif de Pau a rejeté cette demande ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article R. 621-13 du code de justice administrative : " Lorsque l'expertise a été ordonnée sur le fondement du titre III du livre V, le président du tribunal ou de la cour, après consultation, le cas échéant, du magistrat délégué, ou, au Conseil d'Etat, le président de la section du contentieux en fixe les frais et honoraires par une ordonnance prise conformément aux dispositions des articles R. 621-11 et R. 761-4. Cette ordonnance désigne la ou les parties qui assumeront la charge de ces frais et honoraires (...). Elle peut faire l'objet, dans le délai d'un mois à compter de sa notification, du recours prévu à l'article R. 761-5./ Dans le cas où les frais d'expertise mentionnés à l'alinéa précédent sont compris dans les dépens d'une instance principale, la formation de jugement statuant sur cette instance peut décider que la charge définitive de ces frais incombe à une partie autre que celle qui a été désignée par l'ordonnance mentionnée à l'alinéa précédent ou par le jugement rendu sur un recours dirigé contre cette ordonnance " ; qu'il résulte de ces dispositions que lorsque le président du tribunal administratif a pris une ordonnance fixant les frais et honoraires de l'expertise et désignant la partie qui en assumera la charge, celle-ci, en l'absence d'instance principale engagée à l'issue de l'expertise, ne peut remettre en cause la taxation des frais et honoraires que dans les conditions fixées par les articles R. 621-13 et R. 761-5 du code de justice administrative ; qu'elle n'est, dès lors, pas recevable à former un recours indemnitaire ayant pour objet la condamnation d'une autre partie à lui verser les sommes correspondantes ;<br/>
<br/>
              3. Considérant que la demande de la communauté d'agglomération Côte basque - Adour tendant à la condamnation de société Exedra Sud Aquitaine, présentée alors que l'ordonnance du président du tribunal administratif de Pau du 25 mars 2011 liquidant les frais d'expertise n'avait pas fait l'objet d'un recours prévu à l'article R. 761-5 du code de justice administrative, était irrecevable ; que ce motif doit être substitué à celui, tiré de l'absence de faute pouvant être reprochée à la société Exedra Sud Aquitaine dans l'exécution du marché, qu'a retenu le jugement du tribunal administratif de Pau, dont il justifie légalement le dispositif ;<br/>
<br/>
              4. Considérant qu'il résulte de ce qui précède que, sans qu'il soit besoin d'examiner les moyens du pourvoi, la communauté d'agglomération Côte basque - Adour n'est pas fondée à demander l'annulation du jugement qu'elle attaque ; que ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative ne peuvent, par suite, qu'être rejetées ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de la communauté d'agglomération Côte basque - Adour est rejeté.<br/>
<br/>
Article 2 : La présente décision sera notifiée à la communauté d'agglomération Côte basque - Adour et à la société Exedra Sud Aquitaine.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-01-03 PROCÉDURE. INTRODUCTION DE L'INSTANCE. EXCEPTION DE RECOURS PARALLÈLE. - EXISTENCE - RECOURS INDEMNITAIRE TENDANT AU REMBOURSEMENT DE FRAIS D'EXPERTISE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-03-011 PROCÉDURE. PROCÉDURES DE RÉFÉRÉ AUTRES QUE CELLES INSTITUÉES PAR LA LOI DU 30 JUIN 2000. RÉFÉRÉ TENDANT AU PRONONCÉ D'UNE MESURE D'EXPERTISE OU D'INSTRUCTION. - RÉFÉRÉ INSTRUCTION - VOIES DE CONTESTATION DE LA RÉPARTITION DES FRAIS D'EXPERTISE - RECOURS CONTRE L'ORDONNANCE DE TAXATION OU CONTESTATION DANS L'INSTANCE PRINCIPALE - CONSÉQUENCE - IRRECEVABILITÉ D'UN RECOURS INDEMNITAIRE TENDANT AU REMBOURSEMENT DE CES FRAIS.
</SCT>
<ANA ID="9A"> 54-01-03 Il résulte de l'article R. 621-13 du code de justice administrative (CJA) que lorsque le président du tribunal administratif a pris, sur le fondement du titre III du livre V du CJA, une ordonnance fixant les frais et honoraires de l'expertise et désignant la partie qui en assumera la charge, celle-ci, en l'absence d'instance principale engagée à l'issue de l'expertise, ne peut remettre en cause la taxation des frais et honoraires que dans les conditions fixées par les articles R. 621-13 et R. 761-5 du CJA, c'est-à-dire en contestant l'ordonnance de taxation. Une partie n'est, dès lors, pas recevable à former un recours indemnitaire ayant pour objet la condamnation d'une autre partie à lui verser les sommes correspondantes.</ANA>
<ANA ID="9B"> 54-03-011 Il résulte de l'article R. 621-13 du code de justice administrative (CJA) que lorsque le président du tribunal administratif a pris, sur le fondement du titre III du livre V du CJA, une ordonnance fixant les frais et honoraires de l'expertise et désignant la partie qui en assumera la charge, celle-ci, en l'absence d'instance principale engagée à l'issue de l'expertise, ne peut remettre en cause la taxation des frais et honoraires que dans les conditions fixées par les articles R. 621-13 et R. 761-5 du CJA, c'est-à-dire en contestant l'ordonnance de taxation. Une partie n'est, dès lors, pas recevable à former un recours indemnitaire ayant pour objet la condamnation d'une autre partie à lui verser les sommes correspondantes.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
