<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029998431</ID>
<ANCIEN_ID>JG_L_2014_12_000000372322</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/99/84/CETATEXT000029998431.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème / 9ème SSR, 30/12/2014, 372322</TITRE>
<DATE_DEC>2014-12-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>372322</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème / 9ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Romain Godet</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Edouard Crépey</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:372322.20141230</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 21 septembre 2013 au secrétariat du contentieux du Conseil d'Etat, présentée par l'association dissoute " L'&#140;uvre française ", dont le siège est au 4 bis rue Caillaux à Paris (75013), représentée par son président, et par M. A...E..., demeurant au ... ; les requérants demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret du 25 juillet 2013 portant dissolution de l'association " L'&#140;uvre française " ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 2 500 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
<br/>
              Vu le code de la sécurité intérieure ;<br/>
<br/>
              Vu la loi n° 2000-321 du 12 avril 2000 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Romain Godet, maître des requêtes en service extraordinaire,<br/>
<br/>
              - les conclusions de M. Edouard Crépey, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article L. 212-1 du code de la sécurité intérieure : " Sont dissous, par décret en conseil des ministres, toutes les associations ou groupements de fait : / (...) 2° (...) qui présentent, par leur forme et leur organisation militaires, le caractère de groupes de combat ou de milices privées ; (...) / 5° Ou qui ont pour but soit de rassembler des individus ayant fait l'objet de condamnation du chef de collaboration avec l'ennemi, soit d'exalter cette collaboration ; / 6° (...) qui, soit provoquent à la discrimination, à la haine ou à la violence envers une personne ou un groupe de personnes à raison de leur origine ou de leur appartenance ou de leur non-appartenance à une ethnie, uneC..., une race ou une religion déterminée, soit propagent des idées ou théories tendant à justifier ou encourager cette discrimination, cette haine ou cette violence ; (...) " ; que, sur le fondement de ces dispositions, le décret attaqué du 25 juillet 2013 a prononcé la dissolution de l'association " L'&#140;uvre française " ;<br/>
<br/>
              2. Considérant qu'il ressort des pièces du dossier que les requérants ont eu connaissance de l'identité, de la qualité et de l'adresse administrative tant du fonctionnaire ayant signé le courrier les informant de l'intention du Gouvernement de prononcer la dissolution de l'association " L'&#140;uvre française " que de celles du fonctionnaire chargé de recueillir leurs observations orales ; que les dispositions de l'article 24 de la loi du 12 avril 2000 n'imposent ni la tenue d'un procès-verbal contradictoire ni sa communication aux parties à la procédure ; que le décret contesté a pu, sans insuffisance de motivation, ne pas mentionner les arguments invoqués par les requérants dans le cadre de leurs observations orales ; que, par suite, les moyens tirés de la méconnaissance des articles 4 et 24 de la loi du 12 avril 2000 doivent être écarté ;<br/>
<br/>
              3. Considérant, en deuxième lieu, d'une part, que pour regarder l'association " L'&#140;uvre française " comme ayant pour but d'exalter la collaboration avec l'ennemi, le décret attaqué retient sa participation aux commémorations de la mort de Philippe Pétain, l'organisation de camps d'été placés sous la haute figure de celui-ci, la commémoration ou la référence dans les écrits et publications de l'association à des personnalités favorables à la collaboration avec l'ennemi pendant la seconde guerre mondiale, le choix d'emblèmes rappelant ceux utilisés par le régime de l'Etat Français, ainsi que la participation à certains événements organisés par l'association d'individus condamnés pour délit de négationnisme ; que les requérants se bornent à contester la matérialité de ces éléments, sans apporter d'éléments probants à cet effet ; que si, pour certains d'entre eux, ils n'ont pas été directement organisés par l'association, ils l'ont néanmoins été à son instigation et ont permis d'y véhiculer son idéologie propre ; que, compte tenu de leur caractère précis et concordant, les éléments ainsi retenus caractérisent l'existence de faits mentionnés au 5° de l'article L. 212-1 ; <br/>
<br/>
              4. Considérant, d'autre part, que si les requérants font valoir que ni les statuts de l'association " L'&#140;uvre française " ni les orientations que celle-ci s'est fixées en 2000 dans le cadre d'une " charte " ne comportent des idées ou théories tendant à justifier ou encourager la discrimination, la haine ou la violence au sens du 6° de l'article L. 212-1, il ressort des pièces du dossier que des articles ou entretiens, parus dans la presse, de membres dirigeants de l'association ou d'anciens dirigeants ayant conservé une forte influence dans le fonctionnement de cette association, ainsi que des communiqués de l'association diffusés sur son site internet, comportaient soit directement, soit indirectement, notamment par les références faites aux auteurs de théories ou de publications à caractère raciste ou antisémite, des éléments provoquant à la discrimination, à la haine ou à la violence ou justifiant cette discrimination, haine ou violence au sens du 6° de l'article L. 212-1 ; que, si le décret prend en considération certains événements dont l'association n'est pas formellement l'organisatrice, il ressort néanmoins des pièces du dossier que, eu égard à l'étroite imbrication entre cette association et les organisateurs de ces événements, les activités qui s'y sont tenues peuvent être imputées à l'association elle-même ; qu'à supposer que M. E...n'ait pas lui-même tenu les propos expressément antisémites qui lui sont prêtés par le décret et quoiqu'il ne soit pas établi par les pièces du dossier que les articles publiés dans l'hebdomadaire Rivarol par MM. B...et D...seraient imputables à l'association " L'&#140;uvre française ", ces circonstances ne suffisent pas à infirmer le caractère précis et concordant des éléments retenus pas le décret ; <br/>
<br/>
              5. Considérant, enfin, que les requérants contestent la prise en considération par le décret de l'existence " de camps de formation paramilitaire, physique et idéologique ", alors que l'association n'en est pas l'organisatrice ; qu'il ressort néanmoins des pièces du dossier que, lors de ces camps, est arboré par les participants l'emblème de l'association " L'&#140;uvre française " et que la publication " Jeune C...", qui organise ces camps, constitue l'émanation de cette association ; qu'en outre, si les requérants soutiennent que l'association dispose d'un simple service d'ordre, il ressort également des pièces du dossier que l'organisation de celui-ci fait, en tout état de cause, apparaître l'existence d'un encadrement et d'une aptitude à l'action de force tels, eu égard aux exercices d'entraînement pratiqués au cours des camps de formation, que l'association " L'&#140;uvre française " pouvait être regardée comme une milice privée au sens du 2° de l'article L. 212-1 ; <br/>
<br/>
              6. Considérant que, par suite, les moyens tendant à contester la matérialité et la qualification des faits retenus par le décret attaqué doivent être écartés ;<br/>
<br/>
              7. Considérant, en troisième lieu, que, eu égard aux considérations de fait et de droit sur lesquelles la mesure de dissolution est fondée, le décret attaqué ne méconnaît pas les articles 9, 10 et 11 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentale ; qu'en particulier, si la dissolution critiquée constitue une restriction à l'exercice des libertés d'expression et d'association, celle-ci est justifiée par la gravité des dangers pour l'ordre public et la sécurité publique résultant des activités de l'association en cause ; qu'il s'ensuit que le moyen doit être écarté ;<br/>
<br/>
              8. Considérant, en dernier lieu, que si, en faisant état d'un " détournement de procédure ", les requérants invoquent en réalité un détournement de pouvoir, son existence n'est pas établie ;<br/>
<br/>
              9. Considérant qu'il résulte de tout ce qui précède que les requérants ne sont pas fondés à demander l'annulation du décret qu'ils attaquent ; que les dispositions de l'articles L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de l'association " L'&#140;uvre française " et de M. E...est rejetée.<br/>
Article 2 : La présente décision sera notifiée à l'association " L'&#140;uvre française ", à M. A...E..., au Premier ministre et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">10-01-04-01 ASSOCIATIONS ET FONDATIONS. QUESTIONS COMMUNES. DISSOLUTION. ASSOCIATIONS ET GROUPEMENTS DE FAIT - LOI DU 10 JANVIER 1936. - MOTIF DE DISSOLUTION - EXALTATION DE LA COLLABORATION AVEC L'ENNEMI (5° DE L'ARTICLE L. 212-1 DU CODE DE LA SÉCURITÉ INTÉRIEURE) - MÉTHODE D'IDENTIFICATION - FAISCEAU D'INDICES - ILLUSTRATION EN L'ESPÈCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">49-05-13 POLICE. POLICES SPÉCIALES. POLICE DES ASSOCIATIONS ET GROUPEMENTS DE FAIT (LOI DU 10 JANVIER 1936) (VOIR : ASSOCIATIONS ET FONDATIONS). - DISSOLUTION D'UNE ASSOCIATION OU D'UN GROUPEMENT DE FAIT - MOTIF - EXALTATION DE LA COLLABORATION AVEC L'ENNEMI (5° DE L'ARTICLE L. 212-1 DU CODE DE LA SÉCURITÉ INTÉRIEURE) - MÉTHODE D'IDENTIFICATION - FAISCEAU D'INDICES - ILLUSTRATION EN L'ESPÈCE.
</SCT>
<ANA ID="9A"> 10-01-04-01 Pour regarder une association comme ayant pour but d'exalter la collaboration avec l'ennemi, le décret qui procède à sa dissolution retient sa participation aux commémorations de la mort de Philippe Pétain, l'organisation de camps d'été placés sous la haute figure de celui-ci, la commémoration ou la référence dans les écrits et publications de l'association à des personnalités favorables à la collaboration avec l'ennemi pendant la seconde guerre mondiale, le choix d'emblèmes rappelant ceux utilisés par le régime de l'Etat Français, ainsi que la participation à certains événements organisés par l'association d'individus condamnés pour délit de négationnisme. Les requérants se bornent à contester la matérialité de ces éléments, sans apporter d'éléments probants à cet effet. Si, pour certains d'entre eux, ils n'ont pas été directement organisés par l'association, ils l'ont néanmoins été à son instigation et ont permis d'y véhiculer son idéologie propre. Compte tenu de leur caractère précis et concordant, les éléments ainsi retenus caractérisent l'existence de faits mentionnés au 5° de l'article L. 212-1 du code de la sécurité intérieure en vertu duquel peuvent être dissous les associations ou groupements de fait qui ont pour but soit de rassembler des individus ayant fait l'objet de condamnation du chef de collaboration avec l'ennemi, soit d'exalter cette collaboration.</ANA>
<ANA ID="9B"> 49-05-13 Pour regarder une association comme ayant pour but d'exalter la collaboration avec l'ennemi, le décret qui procède à sa dissolution retient sa participation aux commémorations de la mort de Philippe Pétain, l'organisation de camps d'été placés sous la haute figure de celui-ci, la commémoration ou la référence dans les écrits et publications de l'association à des personnalités favorables à la collaboration avec l'ennemi pendant la seconde guerre mondiale, le choix d'emblèmes rappelant ceux utilisés par le régime de l'Etat Français, ainsi que la participation à certains événements organisés par l'association d'individus condamnés pour délit de négationnisme. Les requérants se bornent à contester la matérialité de ces éléments, sans apporter d'éléments probants à cet effet. Si, pour certains d'entre eux, ils n'ont pas été directement organisés par l'association, ils l'ont néanmoins été à son instigation et ont permis d'y véhiculer son idéologie propre. Compte tenu de leur caractère précis et concordant, les éléments ainsi retenus caractérisent l'existence de faits mentionnés au 5° de l'article L. 212-1 du code de la sécurité intérieure en vertu duquel peuvent être dissous les associations ou groupements de fait qui ont pour but soit de rassembler des individus ayant fait l'objet de condamnation du chef de collaboration avec l'ennemi, soit d'exalter cette collaboration.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
