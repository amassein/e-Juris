<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034272782</ID>
<ANCIEN_ID>JG_L_2017_03_000000403768</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/27/27/CETATEXT000034272782.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème - 2ème chambres réunies, 17/03/2017, 403768</TITRE>
<DATE_DEC>2017-03-17</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>403768</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème - 2ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Thomas Odinot</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Olivier Henrard</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:403768.20170317</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu les procédures suivantes :<br/>
<br/>
              1° Sous le n° 403768, par une requête, enregistrée le 26 septembre 2016 au secrétariat du contentieux du Conseil d'Etat, M. B...A...demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret n° 2016-360 du 25 mars 2016 relatif aux marchés publics, ainsi que la décision implicite du Premier ministre de rejet de sa demande de retrait du décret ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              2° Sous le n° 403817, par une requête et un mémoire en réplique, enregistrés les 27 septembre 2016 et 2 mars 2017 au secrétariat du contentieux du Conseil d'Etat, l'Ordre des avocats de Paris demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler pour excès de pouvoir l'article 142 du même décret ;<br/>
<br/>
              2°) d'enjoindre au Premier ministre de modifier les dispositions attaquées dans un délai de quatre mois, sous astreinte de 500 euros par jour de retard ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu :<br/>
              - la Constitution, notamment son article 34 ;<br/>
              - le code civil ;<br/>
              - le code de commerce ; <br/>
              - l'ordonnance n° 2015-899 du 23 juillet 2015 ;<br/>
              - le décret n° 2010-1525 du 8 décembre 2010 ;<br/>
              - le décret n° 2015-1163 du 17 septembre 2015 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Thomas Odinot, auditeur,  <br/>
<br/>
              - les conclusions de M. Olivier Henrard, rapporteur public. <br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Coutard, Munier-Apaire, avocat de l'Ordre des avocats de Paris.  <br/>
<br/>
              Vu la note en délibéré, enregistrée le 7 mars 2017 présentée par l'Ordre des avocats de Paris.<br/>
<br/>
<br/>
<br/>1. Considérant que les requêtes de M. A...et de l'Ordre des avocats de Paris sont dirigées contre le même décret ; qu'il y a lieu de les joindre pour statuer par une seule décision ;<br/>
<br/>
              2. Considérant que M. A...doit être regardé comme sollicitant l'annulation pour excès de pouvoir des seules dispositions de l'article 29 et du 8° du I de l'article 30 du décret du 25 mars 2016 relatif aux marchés publics ; que l'Ordre des avocats de Paris conteste les dispositions de l'article 142 du même décret en tant qu'elles sont relatives au médiateur des entreprises ; <br/>
<br/>
              Sur les conclusions dirigées contre les articles 29 et 30 du décret :<br/>
<br/>
              3. Considérant, en premier lieu, que l'article 29 du décret prévoit que ses dispositions, à l'exception des articles 2, 4, 5, 12, 20 à 23, 30, 48 à 55, 60, 107, 108 et du titre IV de la première partie, ne s'appliquent pas aux marchés publics de " services juridiques de représentation légale d'un client par un avocat dans le cadre d'une procédure juridictionnelle, devant les autorités publiques ou les institutions internationales ou dans le cadre d'un mode alternatif de règlement des conflits ", non plus qu'aux marchés de " services de consultation juridique fournis par un avocat en vue de la préparation de toute procédure visée à l'alinéa précédent ou lorsqu'il existe des signes tangibles et de fortes probabilités que la question sur laquelle porte la consultation fera l'objet d'une telle procédure " ; que, toutefois, ces deux catégories de services juridiques ne sont pas au nombre de celles qui sont exclues du champ d'application de l'ordonnance du 23 juillet 2015 relative aux marchés publics par le 10° de son article 14 ; que leur sont donc applicables les dispositions de l'article 1er de l'ordonnance, aux termes desquelles : " I. - Les marchés publics soumis à la présente ordonnance respectent les principes de liberté d'accès à la commande publique, d'égalité de traitement des candidats et de transparence des procédures " ; que l'article 29 du décret attaqué précise que la passation des marchés de services juridiques entrant dans l'une ou l'autre de ces deux catégories est soumise à une obligation de publicité et de mise en concurrence, et que, s'il appartient à l'acheteur d'en définir librement les modalités, celles-ci doivent être déterminées en fonction du montant et des caractéristiques du marché ; que, par suite, M. A...n'est pas fondé à soutenir que l'article 29 exonérerait les marchés en cause du respect des règles générales rappelées à l'article 1er de l'ordonnance du 23 juillet 2015 ; <br/>
<br/>
              4. Considérant, en second lieu, qu'en vertu des dispositions du 8° du I de l'article 30 du décret attaqué, les marchés publics répondant à un besoin dont la valeur estimée est inférieure à 25 000 euros HT peuvent être négociés sans publicité ni mise en concurrence préalable ; que, d'une part, cette faculté ouverte aux acheteurs se justifie par la nécessité d'éviter que ne leur soit imposé, pour des marchés d'un montant peu élevé, le recours à des procédures dont la mise en oeuvre ne serait pas indispensable pour assurer l'efficacité de la commande publique et la bonne utilisation des derniers publics et qui pourraient même, en certains cas, dissuader des opérateurs économiques de présenter leur candidature ; que la définition d'un seuil portant sur la valeur estimée du besoin constitue un critère objectif de nature à renforcer la sécurité juridique de la passation du marché pour l'acheteur et le candidat ; que, d'autre part, en précisant que, pour les marchés publics passés sans publicité ni mise en concurrence en application du 8° du I de l'article 30, l'acheteur veille à choisir une offre pertinente, à faire une bonne utilisation des deniers publics et à ne pas contracter systématiquement avec un même opérateur économique lorsqu'il existe une pluralité d'offres susceptibles de répondre au besoin, les dispositions attaquées prévoient des garanties encadrant l'usage de cette possibilité ; que, dès lors, sans qu'il soit besoin de statuer sur la recevabilité des conclusions de la requête, le moyen tiré de ce que les dispositions attaquées auraient méconnu les principes de liberté d'accès à la commande publique, d'égalité de traitement des candidats et de transparence des procédures doit être écarté ;<br/>
<br/>
              Sur les conclusions dirigées contre l'article 142 du décret :<br/>
<br/>
              5. Considérant qu'aux termes de l'article 142 du décret attaqué : " En cas de différend concernant l'exécution des marchés publics, les acheteurs et les titulaires peuvent recourir au médiateur des entreprises ou aux comités consultatifs de règlement amiable des différends ou litiges relatifs aux marchés publics dans des conditions fixées par décret (...). / Le médiateur des entreprises agit comme tierce partie, sans pouvoir décisionnel, afin d'aider les parties, qui en ont exprimé la volonté, à trouver une solution mutuellement acceptable à leur différend. / La saisine du médiateur des entreprises ou d'un comité consultatif de règlement amiable interrompt le cours des différentes prescriptions et les délais de recours contentieux jusqu'à la notification du constat de clôture de la médiation ou la notification de la décision prise par l'acheteur sur l'avis du comité (...) " ; <br/>
<br/>
              6. Considérant que l'Ordre des avocats de Paris soutient que ces dispositions sont illégales en tant qu'elles concernent le médiateur des entreprises, au motif qu'elles instituent à son seul bénéfice un régime juridique nouveau, dont sont exclues les autres personnes exerçant une activité de médiation ;<br/>
<br/>
              En ce qui concerne la compétence du Premier ministre pour édicter des règles relatives à l'interruption de délais de prescription :<br/>
<br/>
              7. Considérant que les dispositions de l'article 142 instituent un régime de prescription pour l'action en paiement d'une créance pour les entreprises qui saisissent le médiateur des entreprises ou un comité consultatif de règlement amiable ; qu'il résulte toutefois de l'article 34 de la Constitution qu'il n'appartient qu'au législateur de déterminer les principes fondamentaux des obligations civiles, au nombre desquels figure la fixation d'un délai de prescription pour l'action en paiement d'une créance ; que, par suite, et sans qu'il soit besoin d'examiner sur ce point les moyens de la requête, l'article 142 du décret du 25 mars 2016 est entaché d'illégalité en tant qu'il dispose que la saisine du médiateur des entreprises ou d'un comité consultatif de règlement amiable interrompt le cours des différentes prescriptions ; que, toutefois, eu égard à la portée des conclusions de la requête, il n'y a lieu d'annuler ces dispositions qu'en tant qu'elles sont relatives à la saisine du médiateur des entreprises ;<br/>
<br/>
              En ce qui concerne les moyens de la requête :<br/>
<br/>
              8. Considérant, en premier lieu, qu'aux termes de l'article L. 462-2 du code de commerce : " L'Autorité est obligatoirement consultée par le Gouvernement sur tout projet de texte réglementaire instituant un régime nouveau ayant directement pour effet : (...) 2° D'établir des droits exclusifs dans certaines zones " ; que l'Ordre des avocats de Paris soutient que la règle selon laquelle la saisine du médiateur des entreprises interrompt le cours des délais contentieux est constitutive d'un droit exclusif au sens de ces dispositions ; que, toutefois, il est loisible aux parties à un marché public de prévoir l'intervention, préalablement à la saisine du juge, en cas de différend, d'un organisme chargé d'une mission de conciliation et de médiation et qu'elles peuvent ainsi, si elles le souhaitent, donner un effet interruptif des délais de recours à la saisine du médiateur de leur choix ; qu'au surplus, un tel effet était déjà attaché de plein droit, avant l'intervention du décret contesté, à la saisine d'un comité consultatif de règlement amiable des différends, organisme également placé auprès du ministre chargé de l'économie, les dispositions en cause se bornant à étendre cette règle au médiateur des entreprises ; qu'elles ne peuvent être regardées comme ayant pour objet ou pour effet d'instituer un " régime nouveau " établissant un droit exclusif au sens des dispositions précitées du code de commerce ; qu'elles n'avaient pas, par suite, à être soumises à l'avis de l'Autorité de la concurrence ;<br/>
<br/>
              9. Considérant, en deuxième lieu, que les dispositions attaquées, contrairement à ce que soutient l'Ordre des avocats de Paris, n'instituent aucunement un monopole au profit du médiateur des entreprises, les cocontractants d'un marché public demeurant libres de recourir au médiateur de leur choix; qu'ainsi le moyen tiré de ce que seul le législateur aurait été compétent pour prévoir l'intervention du médiateur des entreprises ne peut qu'être écarté ; <br/>
<br/>
              10. Considérant, en troisième lieu, que les personnes publiques sont chargées d'assurer les activités nécessaires à la réalisation des missions de service public dont elles sont investies et bénéficient à cette fin de prérogatives de puissance publique ; qu'en outre, si elles entendent, indépendamment de ces missions, prendre en charge une activité économique, elles ne peuvent légalement le faire que dans le respect tant de la liberté du commerce et de l'industrie que du droit de la concurrence ; qu'à cet égard, pour intervenir sur un marché, elles doivent, non seulement agir dans la limite de leurs compétences, mais également justifier d'un intérêt public, lequel peut résulter notamment de la carence de l'initiative privée ; qu'une fois admise dans son principe, une telle intervention ne doit pas se réaliser suivant des modalités telles qu'en raison de la situation particulière dans laquelle se trouverait cette personne publique par rapport aux autres opérateurs agissant sur le même marché, elle fausserait le libre jeu de la concurrence sur celui-ci ;<br/>
<br/>
              11. Considérant que le médiateur des entreprises, service du ministère de l'économie et des finances, a pour objet de proposer gratuitement à tous les acheteurs et à toutes les entreprises, quelles que soient leurs ressources, et donc notamment à ceux disposant de moyens limités, un processus organisé afin de parvenir, avec son aide, à la résolution amiable de leurs différends ; qu'en donnant aux acheteurs et aux entreprises la possibilité de recourir au service du médiateur des entreprises, l'article 142 du décret attaqué s'est borné à mettre en oeuvre la mission d'intérêt général, qui relève de l'Etat, de développer les modes alternatifs de règlement des litiges, corollaire d'une bonne administration de la justice ; qu'en outre, comme il a été dit au point 9, les dispositions en cause n'instituent aucunement un monopole au profit du médiateur des entreprises ; qu'ainsi, aucune des attributions confiées au médiateur des entreprises n'emporte intervention sur un marché ; que par suite, les dispositions de l'article 142 du décret attaqué n'ont eu ni pour objet, ni pour effet de méconnaître le principe de la liberté du commerce et de l'industrie et le droit de la concurrence ; <br/>
<br/>
              12. Considérant qu'il résulte de tout ce qui précède qu'il n'y a lieu d'annuler le décret du 25 mars 2016 qu'en tant seulement qu'il dispose que la saisine du médiateur des entreprises interrompt le cours des différentes prescriptions ; que, par suite, les conclusions de la requête de M. A...et le surplus des conclusions de la requête de l'ordre des avocats de Paris, y compris ses conclusions aux fins d'injonction, doivent être rejetés ;<br/>
<br/>
              Sur les conclusions présentées au titre  des dispositions de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              13. Considérant que les dispositions de l'article L . 761-1 du code de justice administrative font obstacle à ce que la somme demandée à ce titre par M. A...soit mise à la charge de l'Etat qui n'est pas la partie perdante dans le litige qui l'oppose à ce requérant ; qu''il n'y a pas lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat les sommes que l'Ordre des avocats de Paris demandent au même titre ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le quatrième alinéa de l'article 142 du décret du 25 mars 2016 relatif aux marchés publics est annulé en tant qu'il prévoit que la saisine du médiateur des entreprises interrompt le cours des différentes prescriptions.<br/>
Article 2 : La requête de M. A...et le surplus de la requête de l'Ordre des avocats de Paris sont rejetés.<br/>
Article 3 : La présente décision sera notifiée à M. B...A..., à l'Ordre des avocats de Paris, au Premier ministre et au ministre de l'économie et des finances.<br/>
Copie sera adressée au garde des sceaux, ministre de la justice.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-04 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - VIOLATION DIRECTE DE LA RÈGLE DE DROIT. - PRINCIPES DE LA COMMANDE PUBLIQUE - DÉCRET FIXANT LE SEUIL EN DEÇÀ DUQUEL LES MARCHÉS PUBLICS PEUVENT ÊTRE NÉGOCIÉS SANS PUBLICITÉ NI MISE EN CONCURRENCE PRÉALABLE - VIOLATION - ABSENCE, DÈS LORS QUE DES GARANTIES ENCADRENT L'USAGE DE CETTE POSSIBILITÉ [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">14-01 COMMERCE, INDUSTRIE, INTERVENTION ÉCONOMIQUE DE LA PUISSANCE PUBLIQUE. PRINCIPES GÉNÉRAUX. - INTERVENTION DES PERSONNES PUBLIQUES SUR UN MARCHÉ - 1) CONDITIONS - JUSTIFICATION DU PRINCIPE DE CETTE INTERVENTION PAR UN INTÉRÊT PUBLIC - ABSENCE DE MISE EN CAUSE DU LIBRE JEU DE LA CONCURRENCE [RJ2] - 2) ABSENCE EN L'ESPÈCE - ARTICLE 142 DU DÉCRET DU 25 MARS 2016 RELATIF AUX MARCHÉS PUBLICS INSTITUANT UN MÉDIATEUR DES ENTREPRISES.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">39-02-005 MARCHÉS ET CONTRATS ADMINISTRATIFS. FORMATION DES CONTRATS ET MARCHÉS. FORMALITÉS DE PUBLICITÉ ET DE MISE EN CONCURRENCE. - DÉCRET FIXANT LE SEUIL EN DEÇÀ DUQUEL LES MARCHÉS PUBLICS PEUVENT ÊTRE NÉGOCIÉS SANS PUBLICITÉ NI MISE EN CONCURRENCE PRÉALABLE - PRINCIPES DE LA COMMANDE PUBLIQUE - VIOLATION - ABSENCE, DÈS LORS QUE DES GARANTIES ENCADRENT L'USAGE DE CETTE POSSIBILITÉ [RJ1].
</SCT>
<ANA ID="9A"> 01-04 Article 30 du décret n° 2016-360 du 25 mars 2016 relatif aux marchés publics fixant à 25 000 euros le seuil en deçà duquel les marchés publics peuvent être négociés sans publicité ni mise en concurrence préalable.... ,,D'une part, cette faculté ouverte aux acheteurs se justifie par la nécessité d'éviter que ne leur soit imposé, pour des marchés d'un montant peu élevé, le recours à des procédures dont la mise en oeuvre ne serait pas indispensable pour assurer l'efficacité de la commande publique et la bonne utilisation des deniers publics et qui pourraient même, en certains cas, dissuader des opérateurs économiques de présenter leur candidature. La définition d'un seuil portant sur la valeur estimée du besoin constitue un critère objectif de nature à renforcer la sécurité juridique de la passation du marché pour l'acheteur et le candidat.,,,D'autre part, en précisant que, pour les marchés publics passés sans publicité ni mise en concurrence en application du 8° du I de l'article 30 du décret du 25 mars 2016, l'acheteur veille à choisir une offre pertinente, à faire une bonne utilisation des deniers publics et à ne pas contracter systématiquement avec un même opérateur économique lorsqu'il existe une pluralité d'offres susceptibles de répondre au besoin, ces dispositions prévoient des garanties encadrant l'usage de cette possibilité.... ,,Dès lors, le 8° du I de l'article 30 du décret du 25 mars 2016 ne méconnaît pas les principes de liberté d'accès à la commande publique, d'égalité de traitement des candidats et de transparence des procédures,,.</ANA>
<ANA ID="9B"> 14-01 1) Les personnes publiques sont chargées d'assurer les activités nécessaires à la réalisation des missions de service public dont elles sont investies et bénéficient à cette fin de prérogatives de puissance publique. En outre, si elles entendent, indépendamment de ces missions, prendre en charge une activité économique, elles ne peuvent légalement le faire que dans le respect tant de la liberté du commerce et de l'industrie que du droit de la concurrence. A cet égard, pour intervenir sur un marché, elles doivent, non seulement agir dans la limite de leurs compétences, mais également justifier d'un intérêt public, lequel peut résulter notamment de la carence de l'initiative privée. Une fois admise dans son principe, une telle intervention ne doit pas se réaliser suivant des modalités telles qu'en raison de la situation particulière dans laquelle se trouverait cette personne publique par rapport aux autres opérateurs agissant sur le même marché, elle fausserait le libre jeu de la concurrence sur celui-ci.... ,,2) Le médiateur des entreprises, service du ministère de l'économie et des finances, a pour objet de proposer gratuitement à tous les acheteurs et à toutes les entreprises, quelles que soient leurs ressources, et donc notamment à ceux disposant de moyens limités, un processus organisé afin de parvenir, avec son aide, à la résolution amiable de leurs différends. En donnant aux acheteurs et aux entreprises la possibilité de recourir au service du médiateur des entreprises, l'article 142 du décret n° 2016-360 du 25 mars 2016 s'est borné à mettre en oeuvre la mission d'intérêt général, qui relève de l'Etat, de développer les modes alternatifs de règlement des litiges, corollaire d'une bonne administration de la justice. En outre, aucun monopole n'est institué au profit du médiateur.  Ainsi, aucune des attributions confiées au médiateur des entreprises n'emporte intervention sur un marché. Par suite, l'article 142 du décret du 25 mars 2016 n'a eu ni pour objet, ni pour effet de méconnaître le principe de la liberté du commerce et de l'industrie et le droit de la concurrence.</ANA>
<ANA ID="9C"> 39-02-005 Article 30 du décret n° 2016-360 du 25 mars 2016 fixant à 25 000 euros le seuil en deçà duquel les marchés publics peuvent être négociés sans publicité ni mise en concurrence préalable.... ,,D'une part, cette faculté ouverte aux acheteurs se justifie par la nécessité d'éviter que ne leur soit imposé, pour des marchés d'un montant peu élevé, le recours à des procédures dont la mise en oeuvre ne serait pas indispensable pour assurer l'efficacité de la commande publique et la bonne utilisation des deniers publics et qui pourraient même, en certains cas, dissuader des opérateurs économiques de présenter leur candidature. La définition d'un seuil portant sur la valeur estimée du besoin constitue un critère objectif de nature à renforcer la sécurité juridique de la passation du marché pour l'acheteur et le candidat.,,,D'autre part, en précisant que, pour les marchés publics passés sans publicité ni mise en concurrence en application du 8° du I de l'article 30 du décret du 25 mars 2016, l'acheteur veille à choisir une offre pertinente, à faire une bonne utilisation des deniers publics et à ne pas contracter systématiquement avec un même opérateur économique lorsqu'il existe une pluralité d'offres susceptibles de répondre au besoin, les dispositions attaquées prévoient des garanties encadrant l'usage de cette possibilité.... ,,Dès lors, le 8° du I de l'article 30 du décret du 25 mars 2016 ne méconnaît pas les principes de liberté d'accès à la commande publique, d'égalité de traitement des candidats et de transparence des procédures.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Comp., dans un cas où aucune garantie n'était prévue, CE, 10 février 2010, Perez, n° 329100, p. 17., ,[RJ2] Cf. CE, Assemblée, 31 mai 2006, Ordre des avocats au barreau de Paris, n° 275531, p. 272.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
