<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027091618</ID>
<ANCIEN_ID>JG_L_2013_02_000000341196</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/09/16/CETATEXT000027091618.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème sous-section jugeant seule, 20/02/2013, 341196, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-02-20</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>341196</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BARADUC, DUHAMEL</AVOCATS>
<RAPPORTEUR>Mme Anne Berriat</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Delphine Hedary</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2013:341196.20130220</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 5 juillet et 5 octobre 2010 au secrétariat du contentieux du Conseil d'Etat, présentés pour la Société Via Paris immobilier, dont le siège est 32, boulevard de Sébastopol à Paris (75004), représentée par son gérant en exercice ; la Société Via Paris immobilier demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 09VE01854 du 4 mai 2010 par lequel la cour administrative d'appel de Versailles a rejeté sa requête tendant, d'une part, à l'annulation du jugement n° 0605108 du 9 avril 2009 du tribunal administratif de Cergy-Pontoise rejetant partiellement sa demande tendant à la décharge, en droits et intérêts de retard, des cotisations supplémentaires d'impôt sur les sociétés et de contribution additionnelle de 10 % auxquelles elle a été assujettie au titre des années 2000, 2001 et 2002 et, d'autre part, à la décharge de ces impositions ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code général des impôts et le livre des procédures fiscales ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Anne Berriat, Maître des Requêtes en service extraordinaire,  <br/>
<br/>
              - les observations de la SCP Baraduc, Duhamel, avocat de la Société Via Paris immobilier,<br/>
<br/>
              - les conclusions de Mme Delphine Hedary, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Baraduc, Duhamel, avocat de la Société Via Paris immobilier ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que la société Via Paris immobilier, qui exerce une activité de marchand de biens, a fait l'objet d'une vérification de comptabilité portant sur les exercices 2000, 2001 et 2002 au terme de laquelle l'administration fiscale a réintégré dans ses résultats, d'une part, des abandons de loyer pour les trois exercices et, d'autre part, une somme inscrite au passif qu'elle a estimée injustifiée au titre de l'exercice de 2000 ; que la société se pourvoit en cassation contre l'arrêt du 4 mai 2010 par lequel la cour administrative d'appel de Versailles a rejeté sa requête tendant à l'annulation du jugement du tribunal administratif de Cergy-Pontoise du 9 avril 2009, en tant qu'il n'a pas fait droit à la totalité de sa demande de décharge ;  <br/>
<br/>
              2. Considérant, en premier lieu, que la Société Via Paris immobilier soutient que la cour n'a pas répondu au moyen tiré de ce que l'administration n'a pas justifié le montant des loyers qu'elle a réintégrés dans les résultats de l'entreprise en produisant des éléments de comparaison avec les loyers d'autres appartements ; que, cependant, la cour a estimé, par une appréciation des faits qui n'est pas arguée de dénaturation, que, contrairement à ce que soutenait la société qui estimait qu'elles correspondaient à des charges, les sommes d'un montant de 5 500 francs enregistrées dans la comptabilité de la société pendant deux mois correspondaient à des loyers et que ce montant pouvait ainsi être retenu comme loyer mensuel pour les trois exercices ; que, dès lors, le moyen tiré de ce que l'administration ne justifiait pas le choix de ce montant de loyer par une comparaison avec d'autres loyers était inopérant ; que, par suite, la cour n'a pas entaché son arrêt d'une insuffisance de motivation en n'y répondant pas ;<br/>
<br/>
              3. Considérant, en second lieu, que la société requérante soutient que la cour a insuffisamment motivé son arrêt en ne répondant pas au moyen tiré de ce que la somme de 2 204 000 francs réintégrée dans les résultats imposables comme passif injustifié correspondant à des sommes inscrites à un compte courant d'associé ne devait pas être inscrite en totalité au passif du bilan de la société à la clôture de l'exercice mais aurait dû être diminuée du montant cumulé de sommes inscrites au crédit de ce compte courant d'associé, résultant notamment de prêts bancaires à cet associé ; que, toutefois, la cour a estimé, par une appréciation des faits non arguée de dénaturation, que la nature des sommes portées au crédit de ce compte courant d'associé n'était pas justifiée par les éléments fournis par la société ; que, dès lors, elle a répondu implicitement mais nécessairement au moyen tiré de ce que la somme de 2 204 000 francs ne devait pas être inscrite en totalité au passif du bilan de la société et ne devait donc pas être réintégrée au titre de passif injustifié ; que, par suite, la cour n'a pas entaché son arrêt d'insuffisance de motivation sur ce point ;  <br/>
<br/>
              4. Considérant qu'il résulte de ce qui précède que la Société Via Paris immobilier n'est pas fondée à demander l'annulation de l'arrêt qu'elle attaque ; que son pourvoi doit, par suite, être rejeté, y compris ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>				D E C I D E  :<br/>
              				--------------<br/>
<br/>
Article 1er : Le pourvoi de la Société Via Paris immobilier est rejeté.<br/>
Article 2 : La présente décision sera notifiée à la Société Via Paris immobilier et au ministre de l'économie et des finances.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
