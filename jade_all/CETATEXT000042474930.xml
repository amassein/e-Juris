<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042474930</ID>
<ANCIEN_ID>JG_L_2020_10_000000435693</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/47/49/CETATEXT000042474930.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème chambre, 28/10/2020, 435693, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-10-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>435693</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Françoise Tomé</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Raphaël Chambon</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2020:435693.20201028</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
              Par un mémoire distinct, enregistré le 8 août 2020 au secrétariat du contentieux du Conseil d'Etat, M. B... C... demande au Conseil d'Etat, en application de l'article 23-5 de l'ordonnance n° 58-1067 du 7 novembre 1958 et à l'appui de sa requête tendant à l'annulation du décret du Président de la République du 1er septembre 2019 portant nomination, titularisation et affectation (enseignement supérieur) et à ce qu'il soit enjoint au ministre de l'enseignement supérieur, de la recherche et de l'innovation de reprendre la procédure du concours d'agrégation, sous astreinte, de renvoyer au Conseil constitutionnel la question de la conformité aux droits et libertés garantis par la Constitution des dispositions des articles L. 131-7, L. 132-2, L. 132-4 et L. 136-4 du code de justice administrative, de l'article L. 952-6 du code de l'éducation et de l'article 25 septies de la loi n° 83-634 du 13 juillet 1983 portant droits et obligations des fonctionnaires.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
<br/>
              - la Constitution, notamment son article 61-1 ;<br/>
              - l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
              - le code de l'éducation ;<br/>
              - la loi n° 83-634 du 13 juillet 1983 ;<br/>
              - le code de justice administrative, notamment le second alinéa de l'article R.  771-15 ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme D... A..., conseillère d'Etat,<br/>
<br/>
              - les conclusions de M. Raphaël Chambon, rapporteur public ;<br/>
<br/>
              et après en avoir délibéré hors de la présence du rapporteur public ; <br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              Sur la demande de renvoi du jugement de la question prioritaire de constitutionnalité à une autre juridiction : <br/>
<br/>
<br/>
              1. Les conclusions présentées par M. C... tendant à la récusation de l'ensemble des membres de la section du contentieux soit pour conclure en qualité de rapporteur public sur sa question prioritaire de constitutionnalité, soit pour y statuer doivent être regardées comme tendant, en réalité, au renvoi du jugement de sa question prioritaire de constitutionnalité devant une autre juridiction, ainsi qu'il l'indique d'ailleurs lui-même. Or si tout justiciable est recevable à demander à la juridiction immédiatement supérieure qu'une affaire dont est saisie la juridiction compétente soit renvoyée devant une autre juridiction du même ordre si, pour des causes dont il appartient à l'intéressé de justifier, la juridiction compétente est suspecte de partialité, une telle demande ne peut être mise en oeuvre pour s'opposer à ce que le Conseil d'Etat, qui n'a pas de juridiction supérieure, juge une affaire portée devant lui. Par suite, la demande formée en ce sens par M. C... ne peut, en tout état de cause, qu'être rejetée ainsi que, par voie de conséquence, les conclusions accessoires dont est assortie cette demande. <br/>
<br/>
              Sur la question prioritaire de constitutionnalité :<br/>
<br/>
              2. Aux termes du premier alinéa de l'article 23-5 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel : " Le moyen tiré de ce qu'une disposition législative porte atteinte aux droits et libertés garantis par la Constitution peut être soulevé (...) à l'occasion d'une instance devant le Conseil d'Etat (...) ". Il résulte des dispositions de ce même article que le Conseil constitutionnel est saisi de la question prioritaire de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux.<br/>
<br/>
              3. En premier lieu, si M. C... soutient que l'article L. 132-4 du code de justice administrative est contraire aux droits et libertés que garantit la Constitution, aucun article L. 132-4 ne figure au code de justice administrative. <br/>
<br/>
              4. En deuxième lieu, les dispositions des articles L. 131-7, L. 132-2 et L. 136-4 du code de justice administrative relatives aux compétences de la commission supérieure du Conseil d'Etat, aux sanctions pouvant être prononcées à l'encontre des membres du Conseil d'Etat et aux conditions dans lesquelles ils doivent déposer une déclaration d'intérêts et les dispositions de l'article 25 septies de la loi du 13 juillet 1983 portant droits et obligations des fonctionnaires relatives aux conditions dans lesquelles les fonctionnaires peuvent exercer une activité privée lucrative sont sans incidence sur le jugement de la requête par laquelle M. C... demande que le Conseil d'Etat annule le décret du Président de la République du 1er septembre 2019 portant nomination, titularisation et affectation (enseignement supérieur) et enjoigne au ministre de l'enseignement supérieur, de la recherche et de l'innovation de reprendre la procédure du concours d'agrégation, sous astreinte. Par suite, elles ne peuvent être regardées comme applicables au litige ou à la procédure au sens et pour l'application de l'article 23-5 de l'ordonnance du 7 novembre 1958. <br/>
<br/>
              5. En dernier lieu, si M. C... conteste également la constitutionnalité des dispositions de l'article L. 952-6 du code de l'éducation, qui sont relatives à la qualification des enseignants-chercheurs par une instance nationale et aux conditions dans lesquelles les questions individuelles relatives à leur recrutement, leur affectation et leur carrière doivent être examinées et soutient, à cet égard, que ces dispositions sont entachées d'incompétence négative dans des conditions de nature à affecter les principes d'impartialité et d'égal accès aux emplois publics et le principe fondamental reconnu par les lois de la République de l'indépendance des enseignants-chercheurs, il se borne à faire valoir au soutien de sa critique que les dispositions réglementaires prises pour l'application de ces dispositions législatives en auraient méconnu la portée et que la pratique des universités est contraire à ce qu'elles prévoient. Dans ces conditions, sa question prioritaire de constitutionnalité, en tant qu'elle porte sur l'article L. 952-6 du code de l'éducation, n'est pas sérieuse. Elle ne soulève pas davantage une question nouvelle.<br/>
<br/>
              6. Il résulte de ce qui précède qu'il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée par M. C....<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La demande de renvoi pour cause de suspicion légitime présentée par M. C... est rejetée. <br/>
Article 2 : Il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée par M. C....<br/>
Article 3 : La présente décision sera notifiée à M. B... C.... <br/>
Copie en sera adressée au garde des sceaux, ministre de la justice, à la ministre de l'enseignement supérieur, de la recherche et de l'innovation, à la ministre de la transformation et de la fonction publiques, au Conseil constitutionnel et au Premier ministre.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
