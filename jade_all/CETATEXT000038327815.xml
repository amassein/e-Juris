<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038327815</ID>
<ANCIEN_ID>JG_L_2019_04_000000416540</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/32/78/CETATEXT000038327815.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère et 4ème chambres réunies, 01/04/2019, 416540, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-04-01</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>416540</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère et 4ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GASCHIGNARD</AVOCATS>
<RAPPORTEUR>M. Jean-Luc Nevache</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Charles Touboul</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:416540.20190401</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, enregistrée le 14 décembre 2017 au secrétariat du contentieux du Conseil d'Etat, la société Innov'sa, la société Vermeiren France et la société Drive devilbiss demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir l'arrêté du ministre des solidarités et de la santé et du ministre de l'action et des comptes publics du 17 octobre 2017 portant modification des modalités de prise en charge des " sièges coquilles de série " au titre Ier de la liste prévue à l'article L. 165-1 du code de la sécurité sociale ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 6 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de la sécurité sociale ;<br/>
              - le code de l'action sociale et des familles ;<br/>
              - le décret n° 2015-848 du 9 juillet 2015 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Luc Nevache, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Charles Touboul, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Gaschignard, avocat de la société Vermeiren France, de la société Innov'sa et de la société Drive Devilbiss ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 165-1 du code de sécurité sociale, dans sa rédaction applicable à la date de l'acte attaqué : " Le remboursement par l'assurance maladie des dispositifs médicaux à usage individuel (...) est subordonné à leur inscription sur une liste établie après avis d'une commission de la Haute Autorité de santé mentionnée à l'article L. 161-37. L'inscription est effectuée soit par la description générique de tout ou partie du produit concerné, soit sous forme de marque ou de nom commercial. L'inscription sur la liste peut elle-même être subordonnée au respect de spécifications techniques, d'indications thérapeutiques ou diagnostiques et de conditions particulières de prescription et d'utilisation. (...) ". Par un arrêté conjoint du 17 octobre 2017, dont les sociétés Innov'sa, Vermeiren France et Drive devilbiss demandent l'annulation pour excès de pouvoir, les ministres des solidarités et de la santé et de l'action et des comptes publics ont modifié les conditions de prise en charge des " sièges coquilles de série " inscrits au titre Ier de la liste des produits et prestations remboursables prévue à l'article L. 165-1 du code de la sécurité sociale. <br/>
<br/>
              Sur les avis de la commission nationale d'évaluation des dispositifs médicaux et des technologies de santé : <br/>
<br/>
              2. En premier lieu, aux termes de l'article R. 165-20 du code de la sécurité sociale, relatif à la commission nationale d'évaluation des dispositifs médicaux et des technologies de santé : "  La commission se réunit sur convocation de son président. / La commission élabore son règlement intérieur (...) ". L'article III-2 de ce règlement intérieur prévoit que le bureau a délégation de la commission pour " décider de recourir à des experts externes et les choisir, au regard de leurs compétences et après avoir examiné leurs liens d'intérêts ". Il ressort des pièces du dossier que les douze membres du groupe de travail multidisciplinaire, constitué en octobre 2014 à la suite d'un appel à candidatures public et de la sollicitation d'experts, ont été choisis par le bureau de la commission après que celui-ci eut procédé à l'analyse de leurs liens d'intérêts et de leur compétence dans le domaine d'étude et que ces personnes eurent rempli une déclaration d'intérêts, qui a été publiée sur le site de la Haute Autorité de santé. Il en résulte que le moyen tiré de ce que les membres du groupe de travail constitué par la commission nationale d'évaluation des dispositifs médicaux et des technologies de santé n'auraient pas été nommés par le bureau, que leur identité n'aurait pas été connue non plus que leur compétence et leurs éventuels liens d'intérêt doit être écarté. <br/>
<br/>
              3. En deuxième lieu, aux termes de l'article III-3 du règlement intérieur de la commission : "  La Commission se réunit sur convocation de son Président qui arrête l'ordre du jour. / A cette fin, le secrétariat envoie une convocation par voie électronique au plus tard cinq jours calendaires avant la séance. / Cette convocation est accompagnée de : / - l'ordre du jour, / - des documents relatifs aux points inscrits à l'ordre du jour ou des liens électroniques permettant d'y accéder ". Il ressort des pièces du dossier que la commission s'est prononcée sur la modification des conditions d'inscription des sièges coquilles sur la liste des produits et prestations remboursables, à la suite du recueil des observations des fabricants et des distributeurs, en procédant à cinq votes successifs au cours de sa séance du 8 novembre 2016, qui a été régulièrement convoquée. Si la diffusion, le 18 novembre 2016, de la convocation à la réunion du 22 novembre 2016 de la commission, avec l'ordre du jour et les documents correspondants, n'a pas respecté le délai de cinq jours calendaires prévu par le règlement intérieur de la commission, il ressort toutefois des pièces du dossier que cette réunion, s'agissant des sièges coquilles de série, s'est bornée à adopter le procès-verbal de la séance du 8 novembre 2016, formalisant l'avis alors émis, et que sa convocation tardive n'a pas été susceptible d'exercer une influence sur la teneur de cet avis et n'a privé les sociétés requérantes d'aucune garantie.   <br/>
<br/>
              4. En troisième lieu, jusqu'à leur modification par le décret du 9 juillet 2015 relatif à la composition de la commission de la transparence et de la Commission nationale d'évaluation des dispositifs médicaux et des technologies de santé, les dispositions du premier alinéa de l'article R. 165-19 du code de la sécurité sociale prévoyaient que : " Les délibérations de la commission ne sont valables que si au moins neuf de ses membres ayant voix délibérative sont présents " et elles prévoient, après cette modification, que : " Les délibérations de la commission ne sont valables que si au moins treize de ses membres ayant voix délibérative sont présents ".  Toutefois, l'article 5 du décret du 9 juillet 2015 dispose que : " Dans un délai de quatre mois suivant la publication du présent décret, le collège de la Haute Autorité de santé procède, dans les conditions prévues par ce décret, à la nomination de l'ensemble des membres de la commission de la transparence et de la Commission nationale d'évaluation des dispositifs médicaux et des technologies de santé. Par exception aux dispositions du 1° de l'article R 163-15 et du A du I de l'article R. 165-18 du code de la sécurité sociale relatives à la durée des mandats des membres des commissions, le mandat en cours des membres de ces commissions prend fin à la date d'installation de la nouvelle commission. / Jusqu'à l'intervention de ce renouvellement, les commissions continuent de siéger dans leur composition antérieure au présent décret ; leurs avis définitifs sont valides pour la prise des décisions en vue desquelles ils ont été sollicités. / Lorsque, antérieurement à ce renouvellement, une commission a communiqué un projet d'avis en application des articles R. 163-16 ou R. 165-12 du code de la sécurité sociale, l'audition permise par ces articles et l'adoption des avis définitifs peuvent être assurées par les commissions composées conformément aux dispositions du présent décret ". En application de ce texte, le collège de la Haute Autorité de santé a procédé à la nomination de l'ensemble des membres de la commission nationale d'évaluation des dispositifs médicaux et des technologies de santé dans sa nouvelle composition le 4 novembre 2015. Il résulte de l'ensemble de ces éléments que le quorum des réunions de la commission était de neuf membres avant le 4 novembre 2015 et de treize membres à compter de cette date. Par suite, contrairement à ce que soutiennent les sociétés requérantes, le quorum requis était atteint lors des réunions des 21 juillet 2015, 8 septembre 2015, 8 novembre 2016 et 22 novembre 2016, au cours desquelles la commission a délibéré sur les conditions d'inscription des " sièges coquilles de série " sur la liste des produits et prestations remboursables. <br/>
<br/>
              5. En dernier lieu, aux termes de l'article R. 165-11 du code de la sécurité sociale : " L'avis rendu par la Commission nationale d'évaluation des dispositifs médicaux et des technologies de santé, en vue d'une inscription ou d'une modification des conditions d'inscription, comporte notamment : (...) / 2° L'appréciation du bien-fondé, au regard du service attendu du produit ou de la prestation, de l'inscription sur la liste prévue à l'article L. 165-1. Cette évaluation conduit à considérer le service attendu comme suffisant ou insuffisant pour justifier l'inscription au remboursement. Elle est réalisée pour chaque indication thérapeutique, diagnostique ou de compensation du handicap en distinguant, le cas échéant, des groupes de population et précise les seules indications pour lesquelles la commission estime l'inscription fondée ; (...) ". Il résulte de ces dispositions que la motivation des avis rendus par la commission doit préciser, pour chaque indication thérapeutique, diagnostique ou de compensation du handicap, si le service attendu du produit ou de la prestation est suffisant pour permettre de l'inscrire sur la liste prévue à l'article L. 165-1 du même code, en distinguant, le cas échéant, des groupes de population et en précisant les indications pour lesquelles la commission estime l'inscription fondée.<br/>
<br/>
              6. La commission estime dans son avis du 8 septembre 2015 " que le service rendu des sièges coquilles de série est suffisant pour le renouvellement d'inscription sur la liste des produits et prestations prévue à l'article L. 165-1 du code de la sécurité sociale (...) dans l'indication suivante : adulte ayant une impossibilité de se maintenir en position assise sans un système de soutien, évalué GIR 1-2 dans le cadre de l'obtention de l'allocation personnalisée d'autonomie (...) ". Elle constate, dans son avis définitif du 22 novembre 2016, après examen des observations recueillies pendant la phase contradictoire prévue par  l'article R. 165-9 du code de la sécurité sociale, que " pour la majorité des sièges actuellement pris en charge sous les descriptions génériques en vigueur des sièges coquilles de série, l'incapacité à assurer la fonction principale d'assistance à la posture en position assise a été soulignée ". Elle indique ensuite que " seuls les sièges coquilles ayant un intérêt médical pour la population visée, c'est-à-dire assurant une véritable fonction d'aide au positionnement en position assise sont proposés pour la prise en charge ". Elle ajoute que : " Ce type de sièges assure un positionnement passif. Pour éviter un risque de grabatisation des utilisateurs de sièges coquilles, leur utilisation doit être réservée, à titre exceptionnel, à une population gériatrique dans l'impossibilité de se maintenir en position assise sans un système de soutien, généralement confinée au lit ou au fauteuil, et dépendantes (GIR 1-2). Les personnes ayant une capacité à se déplacer ne doivent pas être destinataires de ce type de siège ". Il en résulte que la commission a motivé ses avis en définissant les indications précises et les groupes de population pour lesquels les sièges coquilles devaient être recommandés et en excluant, au contraire, les patients pour lesquels cette prescription était non seulement inutile mais pouvait même se révéler contre indiquée. Par suite, les sociétés requérantes ne sont pas fondées à soutenir que les avis de la commission des 8 septembre 2015 et 22 novembre 2016 ne seraient pas suffisamment motivés au regard des prescriptions de l'article R. 165-11 du code de la sécurité sociale. <br/>
<br/>
              Sur la légalité des restrictions apportées au remboursement des sièges coquilles de série : <br/>
<br/>
              7. En premier lieu, il ressort des pièces du dossier que, à la suite des travaux confiés à un groupe de travail composé essentiellement de professionnels de santé et en l'absence de littérature documentant l'intérêt clinique de ces dispositifs, la commission nationale d'évaluation des dispositifs médicaux et des technologies de santé a préconisé que le recours aux sièges coquilles, susceptibles, en cas de mésusage, d'accélérer la " grabatisation " des patients, soit réservé, dans une indication gériatrique, aux personnes ayant perdu leur autonomie motrice, tandis que d'autres dispositifs ou aides techniques devaient être privilégiés pour les patients plus jeunes ou ayant conservé une certaine autonomie. <br/>
<br/>
              8. Par l'arrêté attaqué, les ministres ont soumis le remboursement des sièges coquilles de série par l'assurance maladie à l'entente préalable de l'organisme de prise en charge, donnée après avis du médecin-conseil, en application de l'article R. 165-23 du code de la sécurité sociale, pour une prescription à l'adulte " de plus de 60 ans ayant une impossibilité de se maintenir en position assise sans un système de soutien, évalué dans les groupes iso-ressources (GIR) 1-2 selon la grille AGGIR ". S'ils pouvaient légalement, dans le but d'éviter la prescription de ces dispositifs médicaux aux personnes ayant une capacité à se déplacer et de prévenir le risque de grabatisation induit par usage prolongé de ce type de siège, se référer à  la grille AGGIR, destinée, en application des dispositions de l'article R. 232-3 du code de l'action sociale et des familles, à apprécier le degré de perte d'autonomie dans l'accomplissement des actes de la vie quotidienne des demandeurs de l'allocation personnalisée d'autonomie, ils ne pouvaient en revanche, sans erreur manifeste d'appréciation, subordonner le remboursement par l'assurance maladie à l'évaluation du patient selon cette grille, qui repose sur de très nombreuses variables étrangères à la capacité à se déplacer, ni, eu égard à la diversité de situations, quant à leurs fonctions locomotrices, des personnes relevant d'un même groupe iso-ressources, à son classement en groupe iso-ressources 1 ou 2, alors qu'ils n'ont pas justifié, par les éléments versés au dossier, que cette condition n'excédait pas le but recherché,  qui est d'exclure de la prise en charge par l'assurance maladie les sièges coquilles prescrits à des personnes ayant conservé une autonomie de déplacement.  <br/>
<br/>
              9. En second lieu, en vertu du point 1.3.2 du paragraphe consacré au siège coquille de série au titre Ier de la liste des produits et prestations remboursables, dans sa rédaction issue de l'arrêté attaqué, la prise en charge d'un siège coquille exclut celle d'un coussin de série d'aide à la prévention des escarres ou d'un véhicule pour personne handicapée. Le 1.1.4 du même paragraphe, également issu de l'arrêté attaqué, prévoit que : " (...) Le siège doit être réalisé dans un matériau utilisé pour la réalisation de coussins de siège d'aide à la prévention des escarres (...) " et son 1.1.14 que : " Les " sièges coquilles de série " peuvent être considérés comme des fauteuils roulants manuels (...) ". Il résulte de ces dispositions que les spécifications techniques des sièges coquilles de série intègrent désormais les besoins liés à la prévention des escarres et prévoient les conditions de leur usage comme fauteuil roulant. Ce faisant, elles rendent inutiles le cumul de prescription avec des équipements ayant la même finalité. Il en résulte que l'arrêté contesté n'est pas entaché d'erreur manifeste en ce qu'il exclut le cumul de prise en charge d'un siège coquille de série avec celle d'un coussin de série d'aide à la prévention des escarres ou d'un véhicule pour personne handicapée. Il ne méconnaît pas plus le principe d'égalité en ce qu'il comporte cette restriction pour les sièges coquilles de série et non pour les sièges de série modulables et évolutifs, adaptables aux mesures du patient, lesquels répondent à un besoin thérapeutique différent, dans le but d'assurer le maintien en position assise en dépit de déficits posturaux graves de l'enfant ou de l'adulte.<br/>
<br/>
              Sur la méconnaissance du principe de sécurité juridique : <br/>
<br/>
              10. Aux termes de l'article L. 221-5 du code des relations entre le public et l'administration : " L'autorité administrative investie du pouvoir réglementaire est tenue, dans la limite de ses compétences, d'édicter des mesures transitoires dans les conditions prévues à l'article L. 221-6 lorsque l'application immédiate d'une nouvelle réglementation est impossible ou qu'elle entraîne, au regard de l'objet et des effets de ses dispositions, une atteinte excessive aux intérêts publics ou privés en cause. / Elle peut également y avoir recours, sous les mêmes réserves et dans les mêmes conditions, afin d'accompagner un changement de réglementation ".<br/>
<br/>
              11. L'article 2 de l'arrêté attaqué prévoit que : " I. - Les dispositions du présent arrêté entrent en vigueur le 1er janvier 2018. / II. - Toutefois, à titre transitoire et dérogatoire, les codes 1277270 (siège coquille de série, amovible, modèle simple) et 1283365 (siège coquille de série, amovible, modèle avec cales et maintien auto-accrochables) sont maintenus en vigueur, au titre de la liste prévue à l'article L. 165-1 du code de la sécurité sociale, jusqu'au 1er juillet 2018. A compter du 1er janvier 2018, la prise en charge desdits dispositifs médicaux est subordonnée à leur prescription dans les indications de prise en charge définies au paragraphe 1.2 de la nouvelle sous-section 7 et par ailleurs, en application de l'article R. 165-23 du même code, à un accord préalable rempli par le médecin prescripteur lors de la première prescription et à chaque renouvellement ". Il résulte de ces dispositions que la date d'entrée en vigueur de l'arrêté, publié au Journal officiel de la République française du 24 octobre 2017, a été reportée au 1er janvier 2018 pour ce qui concerne, outre les indications de prise en charge annulées par la présente décision, l'accord préalable prévu à l'article R. 165-23 et au 1er juillet 2018 pour ce qui concerne les spécifications techniques des sièges coquilles de série. Compte tenu de l'intérêt de santé publique qui s'attache à ce qu'il soit mis fin à une utilisation inappropriée des sièges coquilles, qui aggravait la perte d'autonomie de certains patients et entraînait des dépenses injustifiées pour l'assurance maladie, et eu égard au fait que les industriels et les sociétés commercialisant ces matériels avaient été avertis de la modification de la réglementation les concernant, à la suite de l'avis de la commission nationale d'évaluation des dispositifs médicaux et des technologies de santé rendu public le 8 septembre 2015, par l'avis de projet de modification des modalités de prise en charge des ministres chargés de la santé et de la sécurité sociale publié au Journal officiel de la République française du 2 août 2016 puis par l'avis de la commission rendu public le 22 novembre 2016, l'arrêté attaqué n'a pas méconnu le principe de sécurité juridique, rappelé à l'article L. 221-5 du code des relations entre le public et l'administration.<br/>
<br/>
              12. Il résulte de tout ce qui précède que les sociétés requérantes sont seulement fondées à demander l'annulation du point 1.2 du paragraphe 1 de la nouvelle sous-section 7 introduite par l'arrêté attaqué au titre Ier de la liste des produits et des prestations remboursables, qui détermine l'indication de prise en charge des sièges coquilles de série et des deuxième et troisième alinéas de son point 1.3.1 relatif à la durée de prise en charge, qui n'en sont pas divisibles.<br/>
              Sur les frais liés au litige :<br/>
<br/>
              13. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat une somme de 1 000 euros à verser à chacune des sociétés Innov'sa, Vermeiren France et Drive devilbiss, au titre des dispositions de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : Le point 1.2 et les deuxième et troisième alinéas du point 1.3.1 du paragraphe 1 de la nouvelle sous-section 7 introduite au titre Ier de la liste prévue à l'article L. 165-1 du code de la sécurité sociale par l'arrêté du 17 octobre 2017 portant modification des modalités de prise en charge des " sièges coquilles de série " sont annulés.<br/>
Article 2 : L'Etat versera à la société Innov'sa, à la société Vermeiren France et à la société Drive devilbiss une somme de 1 000 euros chacune, au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : Le surplus des conclusions de la requête est rejeté.<br/>
Article 4 : La présente décision sera notifiée, pour l'ensemble des sociétés requérantes, à la société Innov'sa, première dénommée, et à la ministre des solidarités et de la santé.<br/>
Copie en sera adressée à la Haute Autorité de santé. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
