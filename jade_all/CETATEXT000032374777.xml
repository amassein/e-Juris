<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032374777</ID>
<ANCIEN_ID>JG_L_2016_04_000000386696</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/37/47/CETATEXT000032374777.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème - 7ème chambres réunies, 04/04/2016, 386696, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-04-04</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>386696</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème - 7ème chambres réunies</FORMATION>
<TYPE_REC>Action en astreinte</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP NICOLAY, DE LANOUVELLE, HANNOTIN</AVOCATS>
<RAPPORTEUR>M. Camille Pascal</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Béatrice Bourgeois-Machureau</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2016:386696.20160404</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A...B...a demandé à la cour administrative d'appel de Versailles de définir les mesures d'exécution de l'arrêt n° 10VE03164 du 8 novembre 2012 par lequel elle avait annulé pour excès de pouvoir l'arrêté du 20 juin 2006 du ministre de l'économie, des finances et de l'industrie ayant prononcé à son encontre une sanction disciplinaire de déplacement d'office ainsi que l'arrêté du 26 septembre 2008 du ministre du budget, des comptes publics et de la fonction publique maintenant cette sanction disciplinaire. Il a demandé à ce titre à la cour d'enjoindre à l'Etat de le réintégrer dans son emploi de chef de poste à la trésorerie de Saint-Martin et Saint-Barthélemy ou dans un emploi équivalent et de reconstituer sa carrière à compter du 1er novembre 2006, dans un délai de huit jours et sous astreinte de 2 000 euros par jour de retard.<br/>
<br/>
              Par un arrêt n° 14VA01495 du 13 novembre 2014, la cour administrative d'appel de Versailles a rejeté cette demande d'exécution.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 24 décembre 2014, 24 mars et 17 novembre 2015 au secrétariat du contentieux du Conseil d'Etat, M. B...demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler ce dernier arrêt ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Camille Pascal, conseiller d'Etat,<br/>
<br/>
              - les conclusions de Mme Béatrice Bourgeois-Machureau, rapporteur public,<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Nicolaÿ, de Lanouvelle, Hannotin, avocat de M. B...; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1.	Considérant qu'aux termes de l'article L. 911-4 du code de justice administrative : " En cas d'inexécution d'un jugement ou d'un arrêt, la partie intéressée peut demander au tribunal administratif ou à la cour administrative d'appel qui a rendu la décision d'en assurer l'exécution. / Toutefois, en cas d'inexécution d'un jugement frappé d'appel, la demande d'exécution est adressée à la juridiction d'appel. / Si le jugement ou l'arrêt dont l'exécution est demandée n'a pas défini les mesures d'exécution, la juridiction saisie procède à cette définition. Elle peut fixer un délai d'exécution et prononcer une astreinte " ;<br/>
<br/>
              2.	Considérant qu'il ressort des pièces du dossier soumis au juge du fond que M.B..., receveur-percepteur du Trésor public, a fait l'objet, alors qu'il était affecté comme chef de poste de la trésorerie de Saint-Martin et chargé des fonctions de trésorier par intérim de Saint-Barthélemy, d'une sanction disciplinaire de déplacement d'office par un arrêté du 20 juin 2006 du ministre de l'économie, des finances et de l'industrie, sanction ultérieurement confirmée, après avis de la commission de recours du conseil supérieur de la fonction publique de l'Etat, par un arrêté du ministre du budget, des comptes publics et de la fonction publique en date du 26 septembre 2008 ; qu'en conséquence de cette sanction, M. B...a été affecté par arrêté du 8 août 2006 à la trésorerie générale des Yvelines comme chargé de mission auprès du trésorier payeur général, à compter du 1er novembre 2006 ; qu'il a été placé en congé de longue maladie à compter du 21 septembre 2006, puis en congé de longue durée jusqu'au 31 octobre 2009 avant d'être réintégré à la trésorerie des Yvelines, à compter du 1er novembre 2009, par une décision du 21 octobre 2009 ; <br/>
<br/>
              3.	Considérant que la cour administrative d'appel de Versailles, par arrêt du 8 novembre 2012, a annulé pour excès de pouvoir les arrêtés du 20 juin 2006 et du 26 septembre 2008 prononçant et maintenant la sanction disciplinaire de déplacement d'office ; que, saisie par M. B...aux fins d'assurer l'exécution de cette annulation pour excès de pouvoir, la cour administrative d'appel de Versailles, par un nouvel arrêt du 13 novembre 2014, a rejeté cette demande ; que M. B...se pourvoit en cassation contre cet arrêt en tant qu'il a refusé d'enjoindre à l'Etat, sous astreinte, de le réintégrer dans son emploi de chef de poste à la trésorerie de Saint-Martin et Saint-Barthélemy ou dans un emploi équivalent ; <br/>
<br/>
              4.	Considérant que, pour rejeter la demande d'exécution présentée par M. B..., la cour administrative d'appel de Versailles s'est fondée sur la circonstance que l'intéressé avait été placé en congé de longue durée entre le 21 septembre 2006 et le 31 octobre 2009 et en a déduit que la contestation de son affectation à la trésorerie des Yvelines à l'issue de ce congé soulevait un litige distinct de l'exécution de l'annulation de la sanction disciplinaire de mutation d'office ; <br/>
<br/>
              5.	Considérant, toutefois, que l'annulation de la décision ayant illégalement muté un agent public oblige l'autorité compétente à replacer l'intéressé, à la date de sa mutation, dans l'emploi qu'il occupait précédemment et à reprendre rétroactivement les mesures nécessaires pour le placer dans une position régulière ; que, par suite, en jugeant, ainsi qu'il vient d'être dit, que, l'intéressé ayant été placé en congé de longue durée puis réintégré à l'issue de ce congé à la trésorerie des Yvelines, sa demande soulevait un litige distinct de l'exécution de l'annulation de la sanction disciplinaire de mutation, alors que l'administration n'avait pas pris de mesures pour le placer rétroactivement et régulièrement dans l'emploi de chef de poste à la trésorerie de Saint Martin qu'il occupait à la date de cette mutation d'office, la cour a commis une erreur de droit ; <br/>
<br/>
              6.	Considérant qu'il résulte de ce qui précède, sans qu'il soit besoin de se prononcer sur les autres moyens du pourvoi, que l'arrêt de la cour administrative d'appel de Versailles du 13 novembre 2014 doit être annulé en tant qu'il a rejeté les conclusions de M. B... tendant à ce qu'il soit rétabli dans ses fonctions ou dans un emploi équivalent ; <br/>
<br/>
              7.	Considérant qu'il y a lieu dans les circonstances de l'espèce de mettre à la charge de l'Etat la somme de 3 000 euros à verser à M. B...au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Versailles du 13 novembre 2014 est annulé en tant qu'il a rejeté les conclusions de M. B...tendant à ce qu'il soit rétabli dans ses fonctions ou dans un emploi équivalent.<br/>
<br/>
Article 2 : L'affaire est, dans cette mesure, renvoyée devant la cour administrative d'appel de Versailles.<br/>
<br/>
Article 3 : L'Etat versera à M. B...la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : La présente décision sera notifiée à M. A...B...et au ministre des finances et des comptes publics.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
