<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000025913413</ID>
<ANCIEN_ID>JG_L_2012_05_000000341259</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/25/91/34/CETATEXT000025913413.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème et 1ère sous-sections réunies, 09/05/2012, 341259</TITRE>
<DATE_DEC>2012-05-09</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>341259</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème et 1ère sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Christian Vigouroux</PRESIDENT>
<AVOCATS>SPINOSI ; SCP TIFFREAU, CORLAY, MARLANGE</AVOCATS>
<RAPPORTEUR>M. Didier Ribes</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Cyril Roger-Lacan</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2012:341259.20120509</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 7 juillet et 7 octobre 2010 au secrétariat du contentieux du Conseil d'État, présentés pour la COMMUNE DE TOMINO, représentée par son maire en exercice, et pour la COMMUNE DE MERIA, représentée par son maire en exercice ; la COMMUNE DE TOMINO et la COMMUNE DE MERIA demandent au Conseil d'État :<br/>
<br/>
              1°) d'annuler l'arrêt n° 08MA01778 du 7 mai 2010 par lequel la cour administrative d'appel de Marseille a rejeté leur demande tendant à l'annulation du jugement n° 0601270 du 7 février 2008 par lequel le tribunal administratif de Bastia a annulé, à la demande de M. Claude A, les arrêtés des 20 et 22 mai 2006 pris par les maires de ces deux communes, refusant de délivrer un permis de construire à M. A ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à leurs conclusions d'appel ;<br/>
<br/>
              3°) de mettre à la charge de M. A le versement de la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de l'urbanisme ;<br/>
<br/>
              Vu la loi n° 2000-1208 du 13 décembre 2000 ;<br/>
<br/>
              Vu la loi n° 2009-526 du 12 mai 2009 ;<br/>
<br/>
              Vu la loi n° 2010-788 du 12 juillet 2010 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Didier Ribes, Maître des Requêtes,  <br/>
<br/>
              - les observations de Me Spinosi, avocat de la COMMUNE DE TOMINO et de la COMMUNE DE MERIA et de la SCP Tiffreau, Corlay, Marlange, avocat de M. A, <br/>
<br/>
              - les conclusions de M. Cyril Roger-Lacan, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à Me Spinosi, avocat de la COMMUNE DE TOMINO et de la COMMUNE DE MERIA et à  la SCP Tiffreau, Corlay, Marlange, avocat de M. A, <br/>
<br/>
<br/>
<br/>
<br/>
              Considérant qu'il ressort des pièces du dossier soumis aux juges du fond qu'après avoir obtenu un permis de construire délivré par le maire de Tomino le 23 novembre 1968, M. Claude A a fait édifier une maison d'habitation sur des parcelles situées à la fois sur le territoire de cette commune et sur le territoire de la COMMUNE DE MERIA ;  que le bâtiment a été détruit en 1976 par un attentat à l'explosif ; qu'en 2006, M. A a sollicité de la COMMUNE DE TOMINO et de la COMMUNE DE MERIA un permis de construire afin de pouvoir reconstruire à l'identique sa maison sur le fondement de l'article L. 111-3 du code de l'urbanisme ; que, par deux arrêtés des 20 et 22 mai 2006, les maires des deux communes ont rejeté sa demande ; que, par un jugement du 7 février 2008, le tribunal administratif de Bastia a annulé les arrêtés des maires des deux communes ; que, par un arrêt du 7 mai 2010 contre lequel la COMMUNE DE TOMINO et la COMMUNE DE MERIA se pourvoient en cassation, la cour administrative d'appel de Marseille a rejeté leur recours formé contre le jugement du tribunal administratif ; <br/>
<br/>
              Sans qu'il soit besoin de se prononcer sur les autres moyens du pourvoi ;<br/>
<br/>
              Considérant qu'aux termes de l'article L. 111-3 du code de l'urbanisme, dans sa rédaction alors applicable : " La reconstruction à l'identique d'un bâtiment détruit par un sinistre est autorisée nonobstant toute disposition d'urbanisme contraire, sauf si la carte communale ou le plan local d'urbanisme en dispose autrement, dès lors qu'il a été régulièrement édifié (...) " ; que, s'il résulte de ces dispositions que les bâtiments construits sans autorisation ou en méconnaissance de celle-ci, ainsi que ceux édifiés sur le fondement d'une autorisation annulée par le juge administratif ou retirée par l'administration, doivent être regardés comme n'ayant pas été " régulièrement édifiés ", en revanche, un permis de construire délivré sur le fondement des dispositions de cet article ne saurait utilement être contesté au motif de l'illégalité du permis de construire initial ;<br/>
<br/>
              Considérant qu'une construction constituée d'un ensemble immobilier unique implanté sur le territoire de deux communes doit, en principe, faire l'objet d'un seul permis de construire, délivré conjointement par les deux maires compétents ; que, dès lors, la cour administrative d'appel de Marseille a pu juger que le moyen soulevé par les communes requérantes, qui faisaient valoir que la partie du bâtiment construite sur le territoire de la COMMUNE DE MERIA n'avait pas été autorisée par le maire de cette commune, n'était pas tiré du caractère irrégulier de la construction initiale mais de l'illégalité du permis de construire délivré en 1968 par le maire de la seule COMMUNE DE TOMINO pour l'ensemble du bâtiment ;<br/>
<br/>
              Considérant, toutefois, qu'il résulte de l'article 207 de la loi du 13 décembre 2000 relative à la solidarité et au renouvellement urbains, dont est issu l'article L. 111-3 du code de l'urbanisme dans sa rédaction applicable au litige, que le législateur n'a pas entendu instituer un droit illimité dans le temps pour tout type de construction ; qu'en effet, le dispositif mis en place par la loi du 13 décembre 2000 était initialement destiné à faciliter la réparation des dégâts causés aux constructions par la tempête de décembre 1999 ; qu'ainsi, le droit reconnu n'a pas pour objet et ne saurait avoir pour effet de permettre aux propriétaires d'un bâtiment détruit de le reconstruire au-delà d'un délai raisonnable afin d'échapper à l'application de règles d'urbanisme devenues contraignantes ; qu'il est ouvert le temps nécessaire à l'obtention par le propriétaire de l'indemnisation par les assureurs ou les personnes responsables du sinistre ; que, d'ailleurs, la loi du 12 mai 2009 de simplification et de clarification du droit et d'allègement des procédures a limité à dix ans l'ancienneté de la destruction du bâtiment dont la reconstruction à l'identique peut être autorisée ; <br/>
<br/>
              Considérant que l'opération envisagée par M. A, consistant à reconstruire, en 2006, une construction détruite en 1976 ne saurait être regardée comme la reconstruction d'un bâtiment après sinistre au sens de l'article L. 111-3 du code de l'urbanisme ; que, par suite, en jugeant, pour confirmer le jugement du tribunal administratif de Bastia, que les maires de Tomino et de Meria ne pouvaient légalement refuser le permis de construire sollicité par M. A, la cour administrative d'appel a entaché son arrêt d'une erreur de droit ; que, dès lors, la COMMUNE DE TOMINO et la COMMUNE DE MERIA sont fondées à en demander l'annulation ;<br/>
<br/>
              Considérant qu'en application de l'article L. 821-2 du code de justice administrative,  il y a lieu de régler l'affaire au fond ;<br/>
<br/>
              Considérant qu'il résulte de ce qui a été dit ci-dessus que la COMMUNE DE TOMINO et  la COMMUNE DE MERIA sont fondées à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Bastia a annulé les arrêtés des maires de la COMMUNE DE TOMINO et de la COMMUNE DE MERIA refusant le permis de construire sollicité par M. A ; que, pour les mêmes motifs, M. A n'est pas fondé à demander l'annulation des arrêtés des maires de Tomino et Meria ;<br/>
<br/>
              Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de M. A le versement à la COMMUNE DE TOMINO et à la COMMUNE DE MERIA de la somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative ; que les mêmes dispositions font obstacle à ce que soit mis à la charge de ces communes, qui ne sont pas la partie perdante dans la présente instance, le remboursement des frais exposés par M. A et non compris dans les dépens ;<br/>
<br/>
<br/>
<br/>
<br/>
              			D E C I D E :<br/>
              			--------------<br/>
<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Marseille du 7 mai 2010 et le jugement du tribunal administratif de Bastia du 7 février 2008 sont annulés.<br/>
<br/>
Article 2 : La requête de M. A devant le tribunal administratif de Bastia est rejetée.<br/>
<br/>
Article 3 : M. A versera à la COMMUNE DE TOMINO et à la COMMUNE DE MERIA une somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : Les conclusions de M. A tendant à l'application des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 5 : La présente décision sera notifiée à la COMMUNE DE TOMINO, à la COMMUNE DE MERIA et à M. Claude A.<br/>
Copie en sera adressée pour information au Premier ministre, ministre de l'écologie, du développement durable, des transports et du logement.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">68-001-01 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. RÈGLES GÉNÉRALES D'UTILISATION DU SOL. RÈGLES GÉNÉRALES DE L'URBANISME. - DROIT À LA RECONSTRUCTION À L'IDENTIQUE D'UN BÂTIMENT DÉTRUIT PAR UN SINISTRE (ART. L. 111-3 DU CODE DE L'URBANISME) - LIMITE - DROIT NE POUVANT ÊTRE EXERCÉ AU-DELÀ D'UN DÉLAI RAISONNABLE.
</SCT>
<ANA ID="9A"> 68-001-01 Il résulte de l'article 207 de la loi n° 2000-1208 du 13 décembre 2000 relative à la solidarité et au renouvellement urbains, dont est issu l'article L. 111-3 du code de l'urbanisme, que le droit qu'il instaure à la reconstruction à l'identique d'un bâtiment détruit par un sinistre nonobstant toute disposition d'urbanisme contraire n'a pas pour objet et ne saurait avoir pour effet de permettre aux propriétaires d'un bâtiment détruit de le reconstruire au-delà d'un délai raisonnable afin d'échapper à l'application des règles d'urbanisme devenues contraignantes. Il n'est donc ouvert que le temps nécessaire à l'obtention par le propriétaire de l'indemnisation par les assureurs ou les personnes responsables du sinistre.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
