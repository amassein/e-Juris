<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027031733</ID>
<ANCIEN_ID>JG_L_2013_02_000000357016</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/03/17/CETATEXT000027031733.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  7ème sous-section jugeant seule, 04/02/2013, 357016, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-02-04</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>357016</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 7ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>CARBONNIER ; SCP CELICE, BLANCPAIN, SOLTNER</AVOCATS>
<RAPPORTEUR>M. Fabrice Aubert</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Bertrand Dacosta</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2013:357016.20130204</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la décision du 1er août 2012 par laquelle le Conseil d'Etat, statuant au contentieux, a prononcé l'admission des conclusions du pourvoi de l'établissement public Voies navigables de France (VNF) dirigé contre l'arrêt n° 08MA04141 du 19 décembre 2011 de la cour administrative d'appel de Marseille en tant qu'il a statué sur la réclamation tenant au surcoût de démolition des digues existantes, aux surcoûts liés aux difficultés d'extraction dans les travaux d'enlèvement des passes et sur l'application des intérêts moratoires au titre de l'indemnisation des travaux de reprise de la brèche ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code des marchés publics ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Fabrice Aubert, Auditeur,  <br/>
<br/>
              - les observations de Me Carbonnier, avocat de l'établissement public Voies navigables de France (VNF), et de la SCP Célice, Blancpain, Soltner, avocat des sociétés <br/>
Razel-Bec, DTP Terrassement, Entreprises Morillon Corvol Courbot et Entreprise Chagnaud,<br/>
<br/>
              - les conclusions de M. Bertrand Dacosta, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à Me Carbonnier, avocat de l'établissement public Voies navigables de France (VNF), et à la SCP Célice, Blancpain, Soltner, avocat des sociétés Razel-Bec, DTP Terrassement, Entreprises Morillon Corvol Courbot et Entreprise Chagnaud ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, par un acte d'engagement du 26 février 2000, l'établissement public Voies navigables de France (VNF) a confié à un groupement de sociétés, dont la société Razel-Bec est mandataire, un marché ayant pour objet la construction d'une digue fluvio-maritime sur le territoire de la commune de Sète ; qu'un litige est né entre les parties dans le cadre de l'exécution du marché ; que, par un jugement du 27 juin 2008, le tribunal administratif de Montpellier a fait partiellement droit aux demandes du groupement en condamnant VNF à lui verser la somme de 1 218 842,18 euros assortie des intérêts et de leur capitalisation ; que, par arrêt du 19 décembre 2011, la cour administrative d'appel de Marseille a partiellement annulé et réformé ce jugement en condamnant VNF à verser au groupement les sommes de 777 947,65 euros hors taxes et 6 292 715,18 euros hors taxes assorties des intérêts et de leur capitalisation ; que, par une décision du 1er août 2012, le Conseil d'Etat a admis les conclusions du pourvoi de VNF dirigé contre cet arrêt en tant qu'il a statué sur la réclamation tenant au surcoût de démolition des digues existantes, aux surcoûts liés aux difficultés d'extraction dans les travaux d'enlèvement des passes et sur l'application des intérêts moratoires au titre de l'indemnisation des travaux de reprise de la brèche ; <br/>
<br/>
              2. Considérant, en premier lieu, que, pour accorder la somme de 662 249 euros hors taxes au groupement au titre de sa réclamation tenant au surcoût de démolition des digues existantes, la cour s'est fondée sur ce que le groupement d'entreprises a dû faire face à des contraintes d'exécution imprévues ; qu'elle lui a ainsi accordé une indemnisation au titre de sujétions imprévues ; que cependant, elle n'a pas recherché si les difficultés matérielles rencontrées lors de l'exécution du marché présentaient un caractère exceptionnel, si ces difficultés étaient imprévisibles lors de la conclusion du contrat et si leur cause était extérieure aux parties, conditions qui seules permettent de faire droit à une demande d'indemnisation au titre de sujétions imprévues ; que, par suite, elle a commis une erreur de droit ; que son arrêt doit dès lors être annulé en tant qu'il a statué sur cette réclamation ;<br/>
<br/>
              3. Considérant, en deuxième lieu, que, pour accorder la somme de 240 492 euros hors taxes au groupement au titre des surcoûts liés aux difficultés d'extraction dans les travaux d'enlèvement des passes, la cour ne s'est pas fondée sur l'existence de sujétions imprévues, mais sur la faute commise par le maître d'ouvrage en délivrant, dans le dossier de la consultation, des informations erronées ou incomplètes sur les caractéristiques des matériaux à retirer ; que, par suite, VNF ne peut utilement soutenir que la cour aurait entaché son arrêt d'erreur de droit en ne recherchant pas si les difficultés d'extraction étaient imprévisibles et extérieures aux parties ; que la cour n'a pas davantage commis d'erreur de droit en jugeant, par un arrêt suffisamment motivé sur ce point, que la demande d'indemnisation du groupement n'était pas forclose, dès lors que, contrairement à ce que soutient VNF, les dispositions de l'article 50.21 du CCAG-travaux applicable ne faisaient pas obstacle à ce que le groupement fasse parvenir son mémoire complémentaire de réclamation à la personne responsable du marché avant que soit né une décision implicite de rejet de sa réclamation initiale ; <br/>
<br/>
<br/>
<br/>
<br/>
              4. Considérant, enfin, que la cour a retenu que le groupement a droit aux intérêts moratoires sur la somme de 777 947,65 euros qu'elle a accordée au groupement au titre des travaux de réparation de la brèche, augmentée de la taxe sur la valeur ajoutée, à compter du 14 janvier 2005 avec capitalisation à compter du 3 avril 2008 ; qu'il résulte de l'arrêt de la cour en sa partie devenue définitive compte tenu de la décision du Conseil d'Etat du 1er aout 2012 statuant sur l'admission du pourvoi, que cette somme ne correspond pas au règlement de sommes dues dans le cadre du décompte général du marché mais est relative au règlement de travaux de reprise faits à la demande de VNF, dans le cadre de la garantie de parfait achèvement, suite à des désordres non imputables au groupement ; que s'agissant ainsi du règlement de sommes distinctes du décompte général du marché, VNF ne peut soutenir que la cour aurait commis une erreur de droit, s'agissant du point de départ des intérêts moratoires dus au titre de cette somme, en méconnaissant les dispositions de l'article 178 du code des marchés publics alors en vigueur, et de l'arrêté du 17 janvier 1991, dès lors que ces dispositions ne sont applicables qu'au règlement des acomptes et du solde du marché retracé par le décompte général ; que par suite, les conclusions de VNF dirigées contre l'arrêt en tant qu'il applique des intérêts moratoires au titre de l'indemnisation des travaux de reprise de la brèche doivent être rejetés ;<br/>
<br/>
              5. Considérant qu'il résulte de tout ce qui précède que l'arrêt du 19 décembre 2011 de la cour administrative d'appel de Marseille doit être annulé en tant seulement qu'il a statué sur la réclamation tenant au surcoût de démolition des digues existantes ;<br/>
<br/>
              6. Considérant que dans les circonstances de l'espèce, il n'y a pas lieu de faire droit aux conclusions des parties tendant à l'application des dispositions de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 19 décembre 2011 de la cour administrative d'appel de Marseille est annulé en tant qu'il a statué sur la réclamation tenant au surcoût de démolition des digues existantes.<br/>
Article 2 : L'affaire est renvoyée devant la cour administrative d'appel de Marseille dans cette mesure.<br/>
Article 3 : Le surplus des conclusions du pourvoi et les conclusions des sociétés Razel-Bec, DTP Terrassement, Entreprises Morillon Corvol Courbot et Entreprise Chagnaud présentées sur le fondement des dispositions de l'article L. 761-1 du code de justice administrative sont rejetés.<br/>
Article 4 : La présente décision sera notifiée à l'établissement public Voies navigables de France et aux sociétés Razel-Bec, DTP Terrassement, Entreprises Morillon Corvol Courbot et Entreprise Chagnaud.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
