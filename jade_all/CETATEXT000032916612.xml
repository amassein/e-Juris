<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032916612</ID>
<ANCIEN_ID>JG_L_2016_07_000000398718</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/91/66/CETATEXT000032916612.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème chambre, 19/07/2016, 398718</TITRE>
<DATE_DEC>2016-07-19</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>398718</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Jacques Reiller</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Edouard Crépey</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2016:398718.20160719</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une décision du 31 mars 2016, enregistrée le 11 avril 2016 au secrétariat du contentieux du Conseil d'Etat, la Commission nationale des comptes de campagne et des financements politiques (CNCCFP), après avoir constaté l'absence de dépôt du compte de campagne de M. A...B..., candidat tête de liste de l'Union populaire républicaine aux élections régionales qui se sont déroulées les 6 et 13 décembre 2015 dans la région Provence-Alpes-Côte d'Azur, a saisi le Conseil d'Etat, juge de l'élection, en application des dispositions de l'article L. 52-15 du code électoral.  <br/>
<br/>
              Cette saisine a été communiquée à M.B..., qui n'a pas produit d'observations.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code électoral ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jacques Reiller, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Edouard Crépey, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Aux termes de l'article L. 52-12 du code électoral : " Chaque candidat ou candidat tête de liste soumis au plafonnement prévu à l'article L. 52-11 et qui a obtenu au moins 1 % des suffrages exprimés est tenu d'établir un compte de campagne retraçant, selon leur origine, l'ensemble des recettes perçues et, selon leur nature, l'ensemble des dépenses engagées ou effectuées en vue de l'élection, hors celles de la campagne officielle par lui-même ou pour son compte, au cours de la période mentionnée à l'article L. 52-4. La même obligation incombe au candidat ou au candidat tête de liste dès lors qu'il a bénéficié de dons de personnes physiques conformément à l'article L. 52-8 du présent code selon les modalités prévues à l'article 200 du code général des impôts. (...) Le candidat estime et inclut, en recettes et en dépenses, les avantages directs ou indirects, les prestations de services et dons en nature dont il a bénéficié. (...) / Au plus tard avant 18 heures le dixième vendredi suivant le premier tour de scrutin, chaque candidat ou candidat tête de liste présent au premier tour dépose à la Commission nationale des comptes de campagne et des financements politiques son compte de campagne et ses annexes accompagné des justificatifs de ses recettes ainsi que des factures, devis et autres documents de nature à établir le montant des dépenses payées ou engagées par le candidat ou pour son compte. Le compte de campagne est présenté par un membre de l'ordre des experts-comptables et des comptables agréés ; celui-ci met le compte de campagne en état d'examen et s'assure de la présence des pièces justificatives requises. Cette présentation n'est pas nécessaire lorsqu'aucune dépense ou recette ne figure au compte de campagne. Dans ce cas, le mandataire établit une attestation d'absence de dépense et de recette. Cette présentation n'est pas non plus nécessaire lorsque le candidat ou la liste dont il est tête de liste a obtenu moins de 1 % des suffrages exprimés et qu'il n'a pas bénéficié de dons de personnes physiques selon les modalités prévues à l'article 200 du code général des impôts. (...) ". En application des dispositions du troisième alinéa de l'article L. 52-15 du code électoral : " Lorsque la commission a constaté que le compte de campagne n'a pas été déposé dans le délai prescrit (...), la commission saisit le juge de l'élection. ".<br/>
<br/>
              2. Il appartient au juge de l'élection, pour apprécier s'il y a lieu de faire usage de la faculté ouverte par les dispositions précitées de l'article L. 118-3 du code électoral, de déclarer inéligible un candidat qui n'a pas déposé son compte de campagne, contrairement aux prescriptions de l'article L. 52-12 du même code, de tenir compte, eu égard à la nature des règles méconnues, du caractère délibéré du manquement ainsi que de l'ensemble des circonstances de l'espèce.<br/>
<br/>
              3. Il résulte de l'instruction que M.B..., candidat tête de liste de l'Union populaire républicaine pour les élections régionales en Provence-Alpes-Côte d'Azur, a obtenu 0,61 % des voix au premier tour de scrutin qui s'est déroulé le 6 décembre 2015. Il est constant qu'il n'a pas déposé son compte de campagne et qu'il n'a pas davantage produit une attestation d'absence de recette et de dépense établie par son mandataire financier. Par lettre recommandée avec accusé de réception signé le 18 mars 2016, la Commission nationale des comptes de campagne et des financements politiques a informé M. B...que, dès lors qu'il n'avait pas restitué les carnets de reçus-dons délivrés à son mandataire financier par les services de l'administration compétente, elle ne pouvait s'assurer s'il avait bénéficié de dons consentis par des personnes physiques et s'il était en conséquence tenu ou non de déposer un compte de campagne en application des dispositions de l'article L. 52-12 du code électoral. <br/>
<br/>
              4. L'absence de restitution par le candidat des carnets de reçus-dons fait présumer de la perception de dons de personnes physiques visées à l'article L. 52-8. Si cette présomption peut être combattue par tous moyens, M. B...n'a pas répondu à la demande de régularisation adressée par la Commission. Il n'a pas davantage produit dans le cadre de la présente instance. <br/>
<br/>
              5. Dans ces conditions, M. B...doit être regardé comme ayant méconnu, de manière délibérée, une règle substantielle de financement des campagnes électorales. Il a ainsi commis un manquement d'une particulière gravité aux règles de financement des campagnes électorales qui justifie qu'il soit déclaré inéligible, en application de l'article L. 118-3 du code électoral, pour une durée d'un an à compter de la date de la présente décision. <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              ---------------<br/>
<br/>
Article 1er : M. A...B...est déclaré inéligible à toutes les élections pour une durée d'un an à compter de la présente décision. <br/>
<br/>
Article 2 : La présente décision sera notifiée à la Commission nationale des comptes de campagne et des financements politiques et à M. A...B.... <br/>
Copie en sera adressée au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">28-005-04-02 ÉLECTIONS ET RÉFÉRENDUM. DISPOSITIONS GÉNÉRALES APPLICABLES AUX ÉLECTIONS. FINANCEMENT ET PLAFONNEMENT DES DÉPENSES ÉLECTORALES. COMPTE DE CAMPAGNE. - DISPENSE DE DÉPÔT DE COMPTE DE CAMPAGNE QUAND LE CANDIDAT OU LA LISTE A OBTENU MOINS DE 1% DES VOIX ET N'A PAS BÉNÉFICIÉ DE DONS DE PERSONNES PHYSIQUES - CANDIDAT AYANT OBTENU MOINS DE 1% DES VOIX MAIS N'AYANT PAS PRODUIT LES CARNETS DE REÇUS-DONS PERMETTANT DE S'ASSURER QU'IL N'A PERÇU AUCUN DON - CONSÉQUENCE - DÉFAUT DE PRÉSENTATION DU COMPTE DE CAMPAGNE ENTRAINANT L'INÉLIGIBILITÉ.
</SCT>
<ANA ID="9A"> 28-005-04-02 Candidat tête d'une liste ayant obtenu moins de 1 % des voix au premier tour de scrutin, n'ayant pas déposé son compte de campagne et n'ayant pas davantage produit une attestation d'absence de recette et de dépense établie par son mandataire financier. Par lettre recommandée avec accusé de réception, la Commission nationale des comptes de campagne et des financements politiques (CNCCFP) l'a informé que, dès lors qu'il n'avait pas restitué les carnets de reçus-dons délivrés à son mandataire financier par les services de l'administration compétente, elle ne pouvait s'assurer s'il avait bénéficié de dons consentis par des personnes physiques et s'il était en conséquence tenu ou non de déposer un compte de campagne en application des dispositions de l'article L. 52-12 du code électoral.... ,,L'absence de restitution par le candidat des carnets de reçus-dons fait présumer de la perception de dons de personnes physiques visées à l'article L. 52-8. Si cette présomption peut être combattue par tous moyens, le candidat n'a pas répondu à la demande de régularisation adressée par la Commission. Il n'a pas davantage produit au cours de l'instance.... ,,Dans ces conditions, le candidat doit être regardé comme ayant méconnu, de manière délibérée, une règle substantielle de financement des campagnes électorales. Il a ainsi commis un manquement d'une particulière gravité aux règles de financement des campagnes électorales qui justifie qu'il soit déclaré inéligible, en application de l'article L. 118-3 du code électoral, pour une durée d'un an.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
