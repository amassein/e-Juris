<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039710198</ID>
<ANCIEN_ID>JG_L_2019_11_000000435816</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/71/01/CETATEXT000039710198.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 14/11/2019, 435816, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-11-14</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>435816</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2019:435816.20191114</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, enregistrée le 6 novembre 2019 au secrétariat du contentieux du Conseil d'Etat, l'Association philanthropique d'action contre l'anarchie urbaine vecteur d'incivilités demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative :<br/>
<br/>
              1°) d'ordonner la suspension de l'exécution du décret n° 2019-1082 du 23 octobre 2019 relatif à la règlementation des engins de déplacement personnel ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Elle soutient que :<br/>
              - la condition d'urgence est remplie dès lors que l'exécution du décret litigieux porte une atteinte grave et immédiate à l'intérêt public qu'elle entend défendre et que constitue l'impératif de sécurité routière ;<br/>
              - il existe un doute sérieux quant à la légalité du décret attaqué ;<br/>
              - il est entaché d'incompétence dès lors qu'il n'est pas établi que les pouvoirs publics ont saisi le Conseil d'Etat préalablement à sa promulgation, en application de l'article L. 112-1 du code de justice administrative ;<br/>
              - il méconnaît l'article 4 de la directive n° 2006/126/CE du 20 décembre 2006 et l'article R. 211-2 du code la route dès lors que les trottinettes électriques sont, d'une part, des cyclomoteurs et doivent être, à ce titre, soumises à un régime d'autorisation administrative préalable et, d'autre part, utilisables à un âge inférieur à l'âge minimal prévu par ces textes ; <br/>
              - ses dispositions ne sont pas de nature à réduire le nombre d'accidents graves et sont dès lors insuffisantes au regard des exigences de sécurité publique et pour garantir le droit à la vie et à ne pas subir de traitements inhumains et dégradants garanti par les articles 2 et 3 de la Convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la Convention européenne de sauvegarde des droits de l'homme et des libertés fondamentale ; <br/>
              - la directive du 20 décembre 2006 ; <br/>
              - le code de la route ;  <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision ". Aux termes de l'article L. 522-3 du même code : " Lorsque la demande ne présente pas un caractère d'urgence ou lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable ou qu'elle est mal fondée, le juge des référés peut la rejeter par ordonnance motivée sans qu'il y ait lieu d'appliquer les deux premiers alinéas de l'article L. 522-1 ".<br/>
<br/>
              2. L'Association philanthropique d'action contre l'anarchie urbaine vecteur d'incivilités (APACAUVI) a été créée le 28 juin 2019 dans le but de s'opposer au développement des trottinettes électriques. Un décret du 23 octobre 2019 définit les caractéristiques techniques et les conditions de circulation de ces engins. Par la présente requête, l'Association philanthropique d'action contre l'anarchie urbaine vecteur d'incivilités demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative, de suspendre l'exécution de ce décret. <br/>
<br/>
              3. L'association requérante soutient que le Conseil d'Etat, dont l'avis est visé par le décret, n'en aurait en réalité pas été saisi. Ce moyen entièrement hypothétique que ne corrobore aucun commencement d'élément matériel est à l'évidence dénué de tout caractère sérieux.<br/>
<br/>
              4. L'association estime ensuite que les dispositions contestées méconnaîtraient les exigences de l'article 4 de la directive 2006/126/CE du 20 décembre 2006, qui subordonnent à la détention d'un permis de conduire le pilotage des cyclomoteurs, définis, entre autres critères, comme des engins motorisés ayant une vitesse supérieure à 25 km/h, auxquels les engins de déplacement personnel doivent être assimilés. L'article 3 du décret attaqué ayant précisément pour objet de définir les engins de déplacement personnels qu'il régit comme ne dépassant pas la vitesse de 25 km/h, le moyen est dépourvu de tout caractère sérieux.<br/>
<br/>
              5. Enfin, le moyen purement spéculatif sur l'insuffisance des dispositions attaquées pour garantir la sécurité publique, assorti de considérations générales sur les risques que font courir les engins réglementés, ne peut, devant le juge des référés, être regardé, en l'état, comme un moyen sérieux.<br/>
<br/>
              6. Il résulte de ce qui précède que, faute que l'une des conditions de l'article L 521-1 soit présente et sans qu'il soit besoin d'apprécier l'urgence alléguée ou la recevabilité de la requête, il y a lieu de rejeter les conclusions de l'APACAUVI à fin de suspension du décret attaqué, ainsi que celles tendant au versement d'une somme sur le fondement des dispositions de l'article L 761-1 du code de justice administrative, qui y font obstacle, dès lors que l'Etat n'est pas la partie perdante.<br/>
<br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de l'Association philanthropique d'action contre l'anarchie urbaine vecteur d'incivilités est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à l'Association philanthropique d'action contre l'anarchie urbaine vecteur d'incivilités et au ministre de l'intérieur.<br/>
Copie en sera adressée au Premier ministre. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
