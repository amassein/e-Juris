<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034698330</ID>
<ANCIEN_ID>JG_L_2017_05_000000396335</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/69/83/CETATEXT000034698330.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème - 8ème chambres réunies, 12/05/2017, 396335</TITRE>
<DATE_DEC>2017-05-12</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>396335</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème - 8ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP POTIER DE LA VARDE, BUK LAMENT, ROBILLOT ; SCP ORTSCHEIDT</AVOCATS>
<RAPPORTEUR>M. Christian Fournier</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Vincent Daumas</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:396335.20170512</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme B...A...a demandé au tribunal administratif de Marseille d'annuler la délibération en date du 4 juillet 2011 en tant que son nom ne figure pas sur la liste des candidats admis à l'examen professionnel d'accès au grade d'attaché territorial principal au titre de l'année 2011 ainsi que la décision en date du 16 septembre 2011 par laquelle le président du centre de gestion de la fonction publique territoriale des Bouches-du-Rhône a rejeté son recours gracieux, d'enjoindre au centre de gestion de statuer à nouveau sur son admission et de le condamner à lui verser la somme de 40 000 euros en réparation du préjudice subi.<br/>
<br/>
              Par un jugement n° 1107000 du 14 mai 2014, le tribunal administratif de Marseille a rejeté la requête de MmeA....<br/>
<br/>
              Par un arrêt n° 14MA03151 du 24 novembre 2015, la cour administrative d'appel de Marseille, sur l'appel formé par Mme A...contre ce jugement, a, d'une part, annulé le jugement du tribunal et, d'autre part, annulé la délibération du 4 juillet 2010 par laquelle a été établie la liste des candidats admis à l'examen professionnel d'accès au grade d'attaché principal territorial au titre de l'année 2011 en tant que n'y figure pas le nom de MmeA..., enjoint au président du centre de gestion de se prononcer de nouveau sur l'admission de Mme A... dans un délai d'un mois, condamné le centre de gestion à verser à Mme A... la somme de 7 000 euros en réparation du préjudice subi et rejeté le surplus des conclusions de MmeA....<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 22 janvier et 5 avril 2016 au secrétariat du contentieux du Conseil d'Etat, le centre de gestion de la fonction publique territoriale des Bouches-du-Rhône demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler les articles 1, 2, 3, 4, 5, 7 et 8 de cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire du fond, de rejeter l'appel de MmeA... ;<br/>
<br/>
              3°) de mettre à la charge de Mme A...la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
            Vu les autres pièces du dossier ;<br/>
<br/>
            Vu :<br/>
            - la loi n° 84-53 du 26 janvier 1984 ;<br/>
            - le décret n° 85-1229 du 20 novembre 1985 ;<br/>
            - le décret n° 87-1099 du 30 décembre 1987 ;<br/>
            - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Christian Fournier, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Vincent Daumas, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Ortscheidt, avocat du centre de gestion de la fonction publique territoriale des Bouches-du-Rhône et à la SCP Potier de la Varde, Buk Lament, Robillot, avocat de Mme B...A...;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 19 avril 2017, présentée par MmeA... ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que Mme A...s'est présentée à l'examen professionnel d'accès au grade d'attaché principal territorial organisé par le centre de gestion de la fonction publique territoriale des Bouches-du-Rhône au titre de l'année 2011. Par une lettre en date du 8 juillet 2011, ce centre de gestion a informé Mme A...qu'elle avait obtenu une moyenne de 10,25 sur 20 mais qu'elle ne figurait pas sur la liste des candidats admis qui avait été arrêtée par la délibération du jury du 4 juillet 2011, dès lors que le seuil d'admission correspondait à la note de 11 sur 20. Mme A...a formé, le 27 juillet 2011, un recours gracieux contre la délibération du jury en tant que son nom n'y figurait pas, lequel a été rejeté le 16 septembre 2011. Par un jugement du 14 mai 2014, le tribunal administratif de Marseille a rejeté la requête de Mme A...tendant à l'annulation de la délibération et du rejet de son recours gracieux ainsi qu'à la réparation du préjudice qui aurait résulté pour elle de leur illégalité. Par un arrêt du 24 novembre 2015, sur l'appel de MmeA..., la cour administrative d'appel de Marseille a annulé le jugement de première instance ainsi que les actes attaqués, a enjoint au président du centre de gestion de se prononcer de nouveau sur les droits de l'intéressée dans un délai d'un mois et a fait droit partiellement à ses conclusions indemnitaires. Le centre de gestion de la fonction publique territoriale des Bouches-du-Rhône se pourvoit en cassation contre cet arrêt, à l'exception de son article 6 qui a rejeté le surplus des conclusions de la requête d'appel.<br/>
<br/>
              2. Aux termes des deux premiers alinéas de l'article 15 du décret du 20 novembre 1985 relatif aux conditions générales de recrutement des agents de la fonction publique territoriale, alors applicables au déroulement des concours et examens professionnels : " Le jury est souverain. / (...) A l'issue des épreuves d'admission, le jury arrête, dans la limite des places mises au concours, la liste des candidats admis. (...) ".<br/>
<br/>
              3. Lorsque l'arrêté fixant les modalités d'organisation d'un examen professionnel se borne à prévoir, à l'instar de l'article 4 de l'arrêté du 17 mars 1988 relatif à l'accès au grade d'attaché principal territorial, d'une part que toute note inférieure à 5 sur 20 à l'une des épreuves entraîne l'élimination du candidat et, d'autre part, qu'un candidat ne peut être déclaré admis si la moyenne de ses notes aux épreuves est inférieure à 10 sur 20, il est loisible au jury de cet examen, dans l'exercice de son pouvoir souverain d'appréciation des mérites des candidats, d'arrêter, après examen des résultats des épreuves, un seuil d'admission supérieur au seuil minimal fixé par cet arrêté. L'autorité organisatrice de l'examen peut informer les candidats du seuil d'admission correspondant à la moyenne des notes en dessous de laquelle aucun d'entre eux n'a, ainsi, pu être admis.<br/>
<br/>
              4. Dès lors, en jugeant que la détermination de la note minimale exigée des candidats pour être admis à l'issue des épreuves d'un examen professionnel est un élément de l'organisation de cet examen et que le jury de l'examen professionnel d'accès au grade d'attaché professionnel n'était pas compétent pour fixer cette note sans rechercher au préalable si ce jury ne s'était pas borné, dans l'exercice de son pouvoir souverain d'appréciation des mérites des candidats, à arrêter, après examen des résultats des épreuves, un seuil d'admission supérieur au seuil minimal fixé par la réglementation de l'examen, la cour a commis une erreur de droit. Le centre de gestion requérant est, par suite, fondé à demander l'annulation de l'arrêt attaqué.<br/>
<br/>
              5. Il n'y a pas lieu, dans les circonstances de l'espèce, de mettre à la charge de Mme A...la somme que le centre de gestion de la fonction publique territoriale des Bouches-du-Rhône demande au titre de l'article L. 761-1 du code de justice administrative. Ces dispositions font obstacle à ce qu'une somme soit mise à la charge de ce centre de gestion qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Les articles 1 à 5, 7 et 8 de l'arrêt du 24 novembre 2015 de la cour administrative d'appel de Marseille sont annulés.<br/>
Article 2 : L'affaire est renvoyée, dans cette mesure, devant la cour administrative d'appel de Marseille.<br/>
Article 3 : Les conclusions présentées par le centre de gestion de la fonction publique territoriale des Bouches-du-Rhône et par Mme A...au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée au centre de gestion de la fonction publique territoriale des Bouches-du-Rhône et à Mme B...A....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">36-03-02 FONCTIONNAIRES ET AGENTS PUBLICS. ENTRÉE EN SERVICE. CONCOURS ET EXAMENS PROFESSIONNELS. - POUVOIRS DU JURY - ARRÊTÉ PORTANT ORGANISATION DE L'EXAMEN PROFESSIONNEL PRÉVOYANT QUE TOUTE NOTE INFÉRIEURE À UN SEUIL ENTRAÎNE L'ÉLIMINATION D'UN CANDIDAT ET QU'UN CANDIDAT NE PEUT ÊTRE ADMIS SI LA MOYENNE DE SES NOTES EST INFÉRIEURE À UN SEUIL - FACULTÉ POUR LE JURY D'ARRÊTER UN SEUIL D'ADMISSION SUPÉRIEUR AU SEUIL MINIMAL FIXÉ PAR L'ARRÊTÉ - EXISTENCE [RJ1].
</SCT>
<ANA ID="9A"> 36-03-02 Lorsque l'arrêté fixant les modalités d'organisation d'un examen professionnel se borne à prévoir, d'une part, que toute note inférieure à 5 sur 20 à l'une des épreuves entraîne l'élimination du candidat et, d'autre part, qu'un candidat ne peut être déclaré admis si la moyenne de ses notes aux épreuves est inférieure à 10 sur 20, il est loisible au jury de cet examen, dans l'exercice de son pouvoir souverain d'appréciation des mérites des candidats, d'arrêter, après examen des résultats des épreuves, un seuil d'admission supérieur au seuil minimal fixé par cet arrêté. L'autorité organisatrice de l'examen peut informer les candidats du seuil d'admission correspondant à la moyenne des notes en dessous de laquelle aucun d'entre eux n'a, ainsi, pu être admis.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1]Rappr., s'agissant d'un concours, CE, 23 juin 1950, Sieurs Chauliat, p. 386 ; CE, 7 novembre 1986, Muckenhirn, n° 77932, T. p. 577 ; CE, 11 juin 2001,,, n° 220599, T. pp. 1005-1035. Comp., s'agissant de la fixation ab initio de la note minimale que doivent obtenir les candidats, CE, 12 mai 1976, Demoiselle Tanguy, n° 97598, p. 243 ; CE, 13 octobre 1976, Fédération autonome de l'aviation civile, n° 00504, p. 936.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
