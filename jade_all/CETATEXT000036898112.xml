<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036898112</ID>
<ANCIEN_ID>JG_L_2018_05_000000408895</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/89/81/CETATEXT000036898112.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème chambre, 09/05/2018, 408895, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-05-09</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>408895</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GASCHIGNARD</AVOCATS>
<RAPPORTEUR>Mme Laurence Franceschini</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Louis Dutheillet de Lamothe</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:408895.20180509</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Le préfet des Bouches-du-Rhône a déféré au tribunal administratif de Marseille l'arrêté du 8 juillet 2013 par lequel le maire d'Arles a délivré un permis de construire à M. A... -C...B.... Par un jugement n° 1307451 du 26 mars 2015, le tribunal administratif de Marseille a fait droit à cette demande.<br/>
<br/>
              Par un arrêt n° 15MA02090 du 12 janvier 2017, la cour administrative d'appel de Marseille a rejeté l'appel formé par M. B...contre ce jugement.  <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 14 mars et 15 juin 2017 au secrétariat du contentieux du Conseil d'Etat, M. B... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
                    Vu les autres pièces du dossier ;<br/>
<br/>
                    Vu : <br/>
                    - le code de l'urbanisme ;<br/>
                    - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Laurence Franceschini, Conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Louis Dutheillet de Lamothe, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Gaschignard, avocat de M. A...-pierreB....<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que M. B...a déposé le 18 décembre 2012 une demande de permis de construire pour édifier un bâtiment destiné à abriter des véhicules agricoles et des produits phytosanitaires à usage agricole sur un terrain situé en zone à vocation agricole NC du plan d'occupation des sols de la commune d'Arles (Bouches-du-Rhône). Par un arrêté du 8 juillet 2013, le maire de la commune d'Arles a accordé le permis de construire. M. B... se pourvoit en cassation contre l'arrêt de la cour administrative d'appel de Marseille qui a confirmé le jugement du tribunal administratif de Marseille qui, à la demande du préfet des Bouches-du-Rhône, a annulé cet arrêté.<br/>
<br/>
              2. L'article NC 1 du règlement du plan d'occupation des sols de la commune d'Arles autorise en zone NC " dans l'intérêt de l'exploitation agricole ", la " construction de bâtiments nouveaux (...) à caractère fonctionnel, autres qu'à usage d'habitation lorsqu'ils sont directement liés ou nécessaires à l'exploitation agricole (...) ".<br/>
<br/>
              3. Il ressort des énonciations de l'arrêt attaqué que M. B...donne à bail des terres pour l'exploitation de Foin de Crau bénéficiant d'une appellation d'origine contrôlée par un fermier qui loue par ailleurs un bâtiment situé sur une parcelle voisine pour l'entrepôt de son matériel et le stockage du foin. Après avoir relevé que M. B...ne démontre pas que le régime des baux ruraux entraîne, pour le fermier qui exploite cette dernière parcelle, une situation de précarité qui justifient la construction sur la parcelle de M. B...de l'entrepôt autorisé par le permis contesté, la cour en a déduit que la construction projetée n'était pas nécessaire à l'exploitation agricole en cause et ne remplissait dès lors pas la condition posée par l'article NC 1 précité. En se fondant ainsi sur le seul fait que le fermier de M. B...louait un entrepôt sur une autre propriété pour en déduire que la construction projetée n'était pas nécessaire à son exploitation agricole, la cour administrative d'appel de Marseille a entaché son arrêt d'une erreur de droit. Ainsi, sans qu'il soit besoin d'examiner l'autre moyen du pourvoi, M. B...est fondé à en demander l'annulation. <br/>
<br/>
              5. Il y a lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par M. B...au titre des dispositions de l'article L. 761-1 du code de justice administrative et de mettre à la charge de l'Etat la somme de 3 000 euros qu'il demande.<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
-------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Marseille du 12 janvier 2017 est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Marseille.<br/>
Article 3 : L'Etat versera à M. B...une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à M. A... -C...B..., au ministre de la cohésion des territoires et à la commune d'Arles. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
