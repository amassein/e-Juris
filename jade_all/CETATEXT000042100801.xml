<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042100801</ID>
<ANCIEN_ID>JG_L_2020_07_000000425229</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/10/08/CETATEXT000042100801.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème - 6ème chambres réunies, 08/07/2020, 425229</TITRE>
<DATE_DEC>2020-07-08</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>425229</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème - 6ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP SEVAUX, MATHONNET ; SCP ROCHETEAU, UZAN-SARANO ; LE PRADO</AVOCATS>
<RAPPORTEUR>M. Alain Seban</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Cécile Barrois de Sarigny</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:425229.20200708</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A... B... a demandé au tribunal administratif de Versailles de condamner l'Office national d'indemnisation des accidents médicaux, des affections iatrogènes et des infections nosocomiales (ONIAM) et, à titre subsidiaire, le centre hospitalier Sud francilien (CHSF), à lui verser la somme de 212 371,89 euros en réparation des préjudices qu'il estime avoir subis à la suite d'une opération dans cet établissement. Par un jugement n° 1304093 du 13 octobre 2015, le tribunal administratif a condamné le CHSF à verser à M. B... la somme de 98 029,93 euros.<br/>
<br/>
              Par un arrêt n° 16VE00156 du 16 octobre 2018, la cour administrative d'appel de Versailles a, sur appel du CHSF et appel incident de M. B..., ramené cette indemnisation à 31 981 euros et rejeté le surplus des conclusions des parties.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 6 novembre 2018 et 4 février 2019 au secrétariat du contentieux du Conseil d'Etat, M. B... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt en tant qu'il fait partiellement droit à l'appel du CHSF et rejette le surplus de ses conclusions ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel du centre hospitalier et de faire droit à son appel incident ;<br/>
<br/>
              3°) de mettre à la charge du CHSF et de l'ONIAM le versement d'une somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de la santé publique ;<br/>
              - le code de justice administrative et l'ordonnance n° 2020-305 du 25 mars 2020 ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Alain Seban, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Cécile Barrois de Sarigny, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Rocheteau, Uzan-Sarano, avocat de M. B..., à Me Le Prado, avocat du centre hospitalier Sud francilien et à la SCP Sevaux, Mathonnet, avocat de l'Office national d'indemnisation des accidents médicaux ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que M. B..., qui souffrait de la maladie de Dupuytren, a subi le 23 février 2010 au centre hospitalier Sud francilien (CHSF) une intervention chirurgicale, à la suite de laquelle il a ressenti une gêne fonctionnelle, qui n'a cessé de s'aggraver et s'est accompagnée d'une algodystrophie en raison de laquelle il a, en grande partie, perdu l'usage de la main droite. Il a demandé au tribunal administratif de Versailles de condamner l'Office national d'indemnisation des accidents médicaux, des affections iatrogènes et des infections nosocomiales (ONIAM) et, subsidiairement, le CHSF à réparer les préjudices qu'il estimait avoir subis. Par un jugement du 13 octobre 2015, le tribunal administratif a condamné le CHSF à lui verser la somme de 98 029,93 euros. Par un arrêt du 6 octobre 2018, la cour administrative d'appel de Versailles a, sur appel du CHSF et sur appel incident de M. B..., ramené la condamnation du CHSF à 31 981 euros et écarté tout engagement de la responsabilité de l'ONIAM au titre de la solidarité nationale.<br/>
<br/>
              2. M. B... se pourvoit en cassation contre cet arrêt. Par la voie du pourvoi provoqué, l'ONIAM conclut, dans l'hypothèse où l'arrêt serait cassé en tant qu'il écarte sa responsabilité au titre de la solidarité nationale, à sa cassation en tant qu'il fixe l'indemnité due par le CHSF. <br/>
<br/>
              Sur l'arrêt en tant qu'il statue sur la responsabilité de l'ONIAM au titre de la solidarité nationale <br/>
<br/>
              3. Il résulte des dispositions du II de l'article L. 1142-1 du code de la santé publique que l'ONIAM doit assurer, au titre de la solidarité nationale, la réparation des dommages résultant directement d'actes de prévention, de diagnostic ou de soins à la double condition qu'ils présentent un caractère d'anormalité au regard de l'état de santé du patient comme de l'évolution prévisible de cet état et que leur gravité excède le seuil défini à l'article D. 1142-1. La condition d'anormalité du dommage prévue par ces dispositions doit toujours être regardée comme remplie lorsque l'acte médical a entraîné des conséquences notablement plus graves que celles auxquelles le patient était exposé de manière suffisamment probable en l'absence de traitement. <br/>
<br/>
              4. En jugeant, alors que, d'une part, il n'était pas contesté devant elle qu'en l'absence d'intervention chirurgicale, l'évolution prévisible de la maladie de Dupuytren dont souffrait M. B... ne l'exposait qu'à une gêne fonctionnelle modérée et que, d'autre part, il résultait de ses propres constatations que les suites de l'opération se caractérisaient, pour l'intéressé, par de vives douleurs et un déficit fonctionnel important, que le dommage ne présentait pas un caractère anormal au regard de l'état de santé du patient comme de l'évolution prévisible de celui-ci, la cour administrative d'appel a inexactement qualifié les faits qui lui étaient soumis. M. B... est, dès lors, fondé à demander l'annulation, sur ce point, de l'arrêt qu'il attaque.<br/>
<br/>
              Sur l'arrêt en tant qu'il statue sur la responsabilité pour faute du CHSF<br/>
<br/>
              En ce qui concerne le pourvoi principal<br/>
<br/>
              5. Dans l'hypothèse où un accident médical non fautif est à l'origine de conséquences dommageables mais où une faute commise par une personne mentionnée au I de l'article L. 1142-1 du code de la santé publique a fait perdre à la victime une chance d'échapper à l'accident ou de se soustraire à ses conséquences, un tel accident ouvre droit à réparation au titre de la solidarité nationale si ses conséquences remplissent les conditions posées au II de l'article L. 1142-1 du même code, mais l'indemnité due par l'ONIAM est réduite du montant de celle mise, le cas échéant, à la charge du responsable de la perte de chance, égale à une fraction du dommage corporel correspondant à l'ampleur de la chance perdue.<br/>
<br/>
              6. Après avoir souverainement apprécié, d'une part, que le défaut d'information de M. B... sur les risques d'algodystrophie lors de l'opération lui avait fait perdre une chance de 25 % de se soustraire à celle-ci et, d'autre part, que les fautes commises lors de cette intervention lui avaient fait perdre une chance de 25% d'éviter l'algodystrophie, la cour administrative d'appel a jugé que le taux de perte de chance d'éviter le dommage devait être fixé à 25 %.<br/>
<br/>
              7. Or il incombait à la cour, pour fixer le taux de la perte de chance subie par M. B..., d'additionner, d'une part, le taux de sa perte de chance de se soustraire à l'opération, c'est-à-dire la probabilité qu'il ait refusé l'opération s'il avait été informé du risque d'algodystrophie qu'elle comportait et, d'autre part, le taux de sa perte de chance résultant de la faute médicale commise lors de l'opération, ce taux étant multiplié par la probabilité qu'il ait accepté l'opération s'il avait été informé du risque d'algodystrophie qu'elle comportait. Compte tenu des taux de perte de chance, rappelés ci-dessus, que la cour avait souverainement appréciés, il devait en résulter un taux global de 25 % + (25 % x 75 %) = 43,75 %. Par suite, en statuant ainsi qu'il a été dit au point précédent, la cour a entaché son arrêt d'une erreur de droit. M. B... est dès lors fondé à en demander l'annulation sur ce point.<br/>
<br/>
              En ce qui concerne le pourvoi provoqué de l'ONIAM<br/>
<br/>
              8. La présente décision annulant l'arrêt du 16 octobre 2018 de la cour administrative d'appel de Versailles, les conclusions par lesquelles l'ONIAM demande la cassation de cet arrêt en tant qu'il fixe l'indemnisation due par le CHSF ont perdu leur objet. Il n'y a pas lieu d'y statuer.<br/>
<br/>
              Sur les conclusions présentées au titre de l'article L.761-1 du code de justice administrative<br/>
<br/>
              9. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'ONIAM et du CHSF une somme de 1 500 euros à verser, chacun, à M. B... au titre des dispositions de l'article L. 761-1 du code de justice administrative. Ces dispositions font obstacle à ce que soit mise à la charge de M. B..., qui n'est pas, dans la présente instance, la partie perdante, la somme que demande l'ONIAM au même titre. Enfin, il n'y a pas lieu de mettre à la charge du CHSF la somme que demande l'ONIAM au titre des mêmes dispositions.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Versailles du 16 octobre 2018 est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Versailles.<br/>
Article 3 : Il n'y a pas lieu de statuer sur les conclusions du pourvoi provoqué de l'ONIAM dirigées contre l'arrêt du 16 octobre 2018.<br/>
Article 4 : Les conclusions présentées par l'ONIAM au titre de l'article L.761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : L'ONIAM et le centre hospitalier Sud francilien verseront à M. B... une somme de 1 500 euros chacun, au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 6 : La présente décision sera notifiée à M. A... B..., au centre hospitalier Sud francilien et à l'Office national d'indemnisation des accidents médicaux, des affections iatrogènes et des infections nosocomiales.<br/>
Copie en sera adressée à la Caisse primaire d'assurance maladie de l'Essonne.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">60-02-01-01-01 RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. RESPONSABILITÉ EN RAISON DES DIFFÉRENTES ACTIVITÉS DES SERVICES PUBLICS. SERVICE PUBLIC DE SANTÉ. ÉTABLISSEMENTS PUBLICS D'HOSPITALISATION. RESPONSABILITÉ POUR FAUTE SIMPLE : ORGANISATION ET FONCTIONNEMENT DU SERVICE HOSPITALIER. - CUMUL D'UN DÉFAUT D'INFORMATION SUR LES RISQUES LIÉS À UNE OPÉRATION ET D'UNE FAUTE MÉDICALE AYANT ENTRAÎNÉ UNE PERTE DE CHANCE D'ÉVITER LA RÉALISATION DU RISQUE - MODALITÉS DE CALCUL DU TAUX DE PERTE DE CHANCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">60-02-01-01-02 RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. RESPONSABILITÉ EN RAISON DES DIFFÉRENTES ACTIVITÉS DES SERVICES PUBLICS. SERVICE PUBLIC DE SANTÉ. ÉTABLISSEMENTS PUBLICS D'HOSPITALISATION. RESPONSABILITÉ POUR FAUTE MÉDICALE : ACTES MÉDICAUX. - CUMUL D'UN DÉFAUT D'INFORMATION SUR LES RISQUES LIÉS À UNE OPÉRATION ET D'UNE FAUTE MÉDICALE AYANT ENTRAÎNÉ UNE PERTE DE CHANCE D'ÉVITER LA RÉALISATION DU RISQUE - MODALITÉS DE CALCUL DU TAUX DE PERTE DE CHANCE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">60-04-03 RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. RÉPARATION. ÉVALUATION DU PRÉJUDICE. - CUMUL D'UN DÉFAUT D'INFORMATION SUR LES RISQUES LIÉS À UNE OPÉRATION ET D'UNE FAUTE MÉDICALE AYANT ENTRAÎNÉ UNE PERTE DE CHANCE D'ÉVITER LA RÉALISATION DU RISQUE - MODALITÉS DE CALCUL DU TAUX DE PERTE DE CHANCE.
</SCT>
<ANA ID="9A"> 60-02-01-01-01 Défaut d'information sur les risques encourus lors d'une intervention chirurgicale, ayant fait perdre à l'intéressé une chance de se soustraire à celle-ci. Fautes commises lors de l'intervention lui ayant par ailleurs fait perdre une chance d'éviter la réalisation de ce risque.,,,Pour fixer le taux de la perte de chance subie par l'intéressé, il incombe au juge d'additionner, d'une part le taux de sa perte de chance de se soustraire à l'opération, c'est-à-dire la probabilité qu'il ait refusé l'opération s'il avait été informé du risque qu'elle comportait et, d'autre part, le taux de sa perte de chance résultant de la faute médicale commise lors de l'opération, ce taux étant multiplié par la probabilité qu'il ait accepté l'opération s'il avait été informé du risque qu'elle comportait.</ANA>
<ANA ID="9B"> 60-02-01-01-02 Défaut d'information sur les risques encourus lors d'une intervention chirurgicale, ayant fait perdre à l'intéressé une chance de se soustraire à celle-ci. Fautes commises lors de l'intervention lui ayant par ailleurs fait perdre une chance d'éviter la réalisation de ce risque.,,,Pour fixer le taux de la perte de chance subie par l'intéressé, il incombe au juge d'additionner, d'une part le taux de sa perte de chance de se soustraire à l'opération, c'est-à-dire la probabilité qu'il ait refusé l'opération s'il avait été informé du risque qu'elle comportait et, d'autre part, le taux de sa perte de chance résultant de la faute médicale commise lors de l'opération, ce taux étant multiplié par la probabilité qu'il ait accepté l'opération s'il avait été informé du risque qu'elle comportait.</ANA>
<ANA ID="9C"> 60-04-03 Défaut d'information sur les risques encourus lors d'une intervention chirurgicale, ayant fait perdre à l'intéressé une chance de se soustraire à celle-ci. Fautes commises lors de l'intervention lui ayant par ailleurs fait perdre une chance d'éviter la réalisation de ce risque.,,,Pour fixer le taux de la perte de chance subie par l'intéressé, il incombe au juge d'additionner, d'une part le taux de sa perte de chance de se soustraire à l'opération, c'est-à-dire la probabilité qu'il ait refusé l'opération s'il avait été informé du risque qu'elle comportait et, d'autre part, le taux de sa perte de chance résultant de la faute médicale commise lors de l'opération, ce taux étant multiplié par la probabilité qu'il ait accepté l'opération s'il avait été informé du risque qu'elle comportait.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
