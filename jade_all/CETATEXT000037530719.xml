<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037530719</ID>
<ANCIEN_ID>JG_L_2018_10_000000412542</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/53/07/CETATEXT000037530719.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème - 7ème chambres réunies, 25/10/2018, 412542</TITRE>
<DATE_DEC>2018-10-25</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>412542</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème - 7ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FOUSSARD, FROGER ; SCP POTIER DE LA VARDE, BUK LAMENT, ROBILLOT</AVOCATS>
<RAPPORTEUR>Mme Louise Bréhier</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Sophie Roussel</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:412542.20181025</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La SCI Finanz a demandé au tribunal administratif de Montreuil d'annuler l'arrêté en date du 16 novembre 2015 par lequel le maire de Montreuil (Seine-Saint-Denis) a accordé à la SA HLM Antin Résidences un permis de construire un ensemble immobilier de 164 logements sur un terrain situé 127-127 bis rue Etienne Marcel. Par un jugement n° 1603398 du 18 mai 2017, le tribunal administratif a annulé cet arrêté. <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 18 juillet et 18 octobre 2017 au secrétariat du contentieux du Conseil d'Etat, la commune de Montreuil demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler ce jugement ; <br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter la demande de la SCI Finanz ; <br/>
<br/>
              3°) de mettre à la charge de la SCI Finanz la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'urbanisme ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Louise Bréhier, auditrice, <br/>
<br/>
              - les conclusions de Mme Sophie Roussel, rapporteur public,<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Foussard, Froger, avocat de la commune de Montreuil, et à la SCP Potier de La Varde, Buk Lament, Robillot, avocat de la SCI Finanz ; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1.	Considérant qu'il ressort des pièces du dossier soumis aux juges du fond qu'un permis de construire a été délivré à la SA HLM Antin Résidences par un arrêté du maire de Montreuil du 16 novembre 2015 pour la réalisation d'un ensemble de 164 logements d'habitation ; que la commune de Montreuil se pourvoit en cassation contre le jugement du 18 mai 2017 par lequel le tribunal administratif de Montreuil a annulé ce permis  à la demande de la SCI Finanz ; <br/>
<br/>
              2.	Considérant qu'aux termes de l'article R. 431-16 du code de l'urbanisme, dans sa version alors en vigueur, " Le dossier joint à la demande de permis de construire comprend en outre, selon les cas : (...) e) Lorsque la construction projetée est subordonnée par un plan de prévention des risques naturels prévisibles ou un plan de prévention des risques miniers approuvés, ou rendus immédiatement opposables en application de l'article L. 562-2 du code de l'environnement, ou par un plan de prévention des risques technologiques approuvé, à la réalisation d'une étude préalable permettant d'en déterminer les conditions de réalisation, d'utilisation ou d'exploitation, une attestation établie par l'architecte du projet ou par un expert certifiant la réalisation de cette étude et constatant que le projet prend en compte ces conditions au stade de la conception " ; <br/>
<br/>
              3.	Considérant qu'il appartient au juge, saisi d'un moyen tiré de la méconnaissance de cette disposition de s'assurer de la production, par le pétitionnaire, d'un document établi par l'architecte du projet ou par un expert attestant qu'une étude a été menée conformément aux exigences de la règlementation et que ses résultats ont été pris en compte au stade de la conception du projet ; qu'il ne saurait en revanche dans ce cadre porter une appréciation sur le contenu de l'étude et son caractère suffisant au regard des exigences des plans de prévention des risques qui en imposent la réalisation ;  <br/>
<br/>
              4.	Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que la construction autorisée par le permis du 16 novembre 2015 est classée dans la zone F du plan de prévention des risques " mouvements de terrains " de la commune de Montreuil, approuvé par un arrêté du préfet de Saint-Denis du 22 avril 2011, correspondant, selon le règlement du plan " aux zones soumises aux seuls aléas " moyen " et " faible " relatifs au risque lié au retrait-gonflement des sols argileux " ; qu'en vertu du point 5.2.1.1 du règlement, une telle construction est soumise, au choix, à la réalisation d'une série d'études géotechniques ou à l'application de certaines mesures techniques prescrites par le plan ; qu'au titre des études, le plan exige la réalisation d'études portant " sur l'ensemble de la parcelle ou sur la surface au sol du projet augmentée de 2 mètres à sa périphérie, dont l'objectif est de définir les dispositions constructives et environnementales nécessaires pour assurer la stabilité des bâtiments vis à vis du risque de tassement différentiel et couvrant les missions géotechniques de type G 12 (étude géotechnique d'avant-projet), G2 (étude géotechnique de projet) et G3 (étude et suivi géotechnique d'exécution) au sens de la norme géotechnique NF P 94-500 " ; <br/>
<br/>
              5.	Considérant que le dossier de permis de construire déposé par la SA HLM Antin Résidences comprenait une attestation, établie le 22 juillet 2015 par un bureau d'ingénierie en géotechnique, faisant état de la réalisation d'une étude géotechnique de conception en phase d'avant-projet (G2 APV) au mois de mars 2015 dans le cadre du projet qui a fait l'objet de la demande de permis et attestant la prise en compte par l'étude de sol, du risque de mouvement de terrain ; qu'il résulte de ce qui a été dit ci-dessus qu'en jugeant que le dossier de permis de construire n'était pas complet au regard des exigences du e) de l'article R. 431-16 du code de l'urbanisme, au motif que l'attestation produite ne permettait pas de s'assurer que le projet prenait en compte, dès sa conception, les conditions d'utilisation et d'exploitation des constructions déterminées par l'étude, le tribunal administratif de Montreuil a commis une erreur de droit ; <br/>
<br/>
              6.	Considérant qu'il résulte de ce qui précède que la commune de Montreuil est fondée, sans qu'il soit besoin d'examiner les autres moyens de son pourvoi, à demander l'annulation du jugement qu'elle attaque ; <br/>
<br/>
              7.	Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              8.	Considérant, en premier lieu, que la requérante ne peut utilement soutenir que l'attestation du 22 juillet 2015 serait insuffisante ; <br/>
<br/>
              9.	Considérant, en deuxième lieu, qu'aux termes de l'article R. 431-8 du code de l'urbanisme : " Le projet architectural comprend une notice précisant : 1° L'état initial du terrain et de ses abords indiquant, s'il y a lieu, les constructions, la végétation et les éléments paysagers existants ; 2° Les partis retenus pour assurer l'insertion du projet dans son environnement et la prise en compte des paysages, faisant apparaître, en fonction des caractéristiques du projet : (...) b) L'implantation, l'organisation, la composition et le volume des constructions nouvelles, notamment par rapport aux constructions ou paysages avoisinants ; c) Le traitement des constructions, clôtures, végétations ou aménagements situés en limite de terrain ; (...) " ; que, contrairement à ce que soutient la requérante, il ressort des pièces du dossier que la notice contenue dans le projet architectural comporte une description de l'état initial du terrain et de ses abords et indique les partis retenus pour assurer l'insertion du projet dans son environnement, notamment les partis relatifs à son implantation et à son traitement architectural ; que le moyen tiré de la méconnaissance de l'article R. 431-8 du code de l'urbanisme ne peut, par suite, qu'être écarté ;  <br/>
<br/>
              10.	Considérant, en troisième lieu, qu'aux termes de l'article L. 311-4 du code de l'urbanisme : " Lorsqu'une construction est édifiée sur un terrain n'ayant pas fait l'objet d'une cession, location ou concession d'usage consentie par l'aménageur de la zone, une convention conclue entre la commune ou l'établissement public de coopération intercommunale et le constructeur précise les conditions dans lesquelles celui-ci participe au coût d'équipement de la zone. La convention constitue une pièce obligatoire du dossier de permis de construire ou de lotir. " ; que si le projet litigieux se trouve sur l'emprise de la zone d'aménagement concerté " Fraternité ", il ressort des pièces du dossier qu'à la date de dépôt de la demande de permis de construire, le dossier de réalisation et le programme des équipements publics de la zone d'aménagement concerté n'avaient pas été approuvés ; que cette circonstance faisait obstacle à la conclusion de la convention requise par l'article L. 311-4 du code de l'urbanisme ; que la requérante ne peut par suite utilement soutenir que le dossier de demande de permis de construire ne comportait pas la convention requise par cet article ; <br/>
<br/>
              11.	Considérant, en dernier lieu, qu'aux termes de l'article R. 111-21 du même code, dans sa rédaction applicable au litige : " Le projet peut être refusé ou n'être accepté que sous réserve de l'observation de prescriptions spéciales si les constructions, par leur situation, leur architecture, leurs dimensions ou l'aspect extérieur des bâtiments ou ouvrages à édifier ou à modifier, sont de nature à porter atteinte au caractère ou à l'intérêt des lieux avoisinants, aux sites, aux paysages naturels ou urbains ainsi qu'à la conservation des perspectives monumentales. " ; qu'eu égard à la teneur de ces dispositions et à la marge d'appréciation qu'elles laissent à l'autorité administrative pour accorder un permis de construire, le juge de l'excès de pouvoir ne peut censurer une autorisation de construire que si l'appréciation portée par l'autorité administrative, au regard de ces dispositions, est entachée d'une erreur manifeste ; qu'il ressort des pièces du dossier que le maire de Montreuil a pu, sans commettre d'erreur manifeste d'appréciation, estimer que la construction projetée pouvait s'insérer dans le cadre existant, alors même qu'elle présentait une volumétrie plus importante que les constructions voisines ; <br/>
<br/>
              12.	Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la SCI Finanz une somme de 1 500 euros à verser à la commune de Montreuil et une somme de 1 500 euros à verser à la SA HLM Antin résidence au titre de l'article L. 761-1 du code de justice administrative ; que les dispositions de cet article font, en revanche, obstacle à ce qu'une somme soit mise à ce titre à la charge de la commune de Montreuil et de la SA HLM Antin résidence, qui ne sont pas, dans la présente instance, les parties perdantes ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : Le jugement du tribunal administratif de Montreuil du 18 mai 2017 est annulé. <br/>
<br/>
Article 2 : La demande présentée par la SCI Finanz devant le tribunal administratif de Montreuil est rejetée. <br/>
<br/>
Article 3 : La SCI Finanz versera une somme de 1 500 euros à la commune de Montreuil et une somme de 1 500 euros à la SA HLM Antin résidence au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : La présente décision sera notifiée à la commune de Montreuil, à la SCI Finanz et à la SA HLM Antin Résidences.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">68-03-02-01 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. PERMIS DE CONSTRUIRE. PROCÉDURE D'ATTRIBUTION. DEMANDE DE PERMIS. - COMPOSITION DU DOSSIER - OBLIGATION DE PRODUIRE L'ATTESTATION ÉTABLIE PAR L'ARCHITECTE DU PROJET OU PAR L'EXPERT CERTIFIANT LA RÉALISATION DE L'ÉTUDE PRÉALABLE PRÉVUE PAR LE E DE L'ARTICLE R. 431-16 DU CODE DE L'URBANISME ET SA PRISE EN COMPTE DANS LE PROJET - CONTRÔLE PAR LE JUGE DU CONTENU DE L'ÉTUDE ET DE SON CARACTÈRE SUFFISANT - ABSENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">68-03-03-01-05 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. PERMIS DE CONSTRUIRE. LÉGALITÉ INTERNE DU PERMIS DE CONSTRUIRE. LÉGALITÉ AU REGARD DE LA RÉGLEMENTATION NATIONALE. DIVERSES DISPOSITIONS LÉGISLATIVES OU RÉGLEMENTAIRES. - PLAN DE PRÉVENTION DES RISQUES NATURELS PRÉVISIBLES (PPRNP) OU PLAN DE PRÉVENTION DES RISQUES MINIER (PPRM) OU PLAN DE PRÉVENTION DES RISQUES TECHNOLOGIQUES (PPRT) - OBLIGATION DE PRODUIRE L'ATTESTATION ÉTABLIE PAR L'ARCHITECTE DU PROJET OU PAR L'EXPERT CERTIFIANT LA RÉALISATION DE L'ÉTUDE PRÉALABLE PRÉVUE PAR LE E DE L'ARTICLE R. 431-16 DU CODE DE L'URBANISME ET SA PRISE EN COMPTE DANS LE PROJET - CONTRÔLE PAR LE JUGE DU CONTENU DE L'ÉTUDE ET DE SON CARACTÈRE SUFFISANT - ABSENCE.
</SCT>
<ANA ID="9A"> 68-03-02-01 Il appartient au juge, saisi d'un moyen tiré de la méconnaissance de l'article R. 431-16 du code de l'urbanisme de s'assurer de la production, par le pétitionnaire, d'un document établi par l'architecte du projet ou par un expert attestant qu'une étude a été menée conformément aux exigences de la règlementation et que ses résultats ont été pris en compte au stade de la conception du projet. Il ne saurait en revanche dans ce cadre porter une appréciation sur le contenu de l'étude et son caractère suffisant au regard des exigences des plans de prévention des risques qui en imposent la réalisation.</ANA>
<ANA ID="9B"> 68-03-03-01-05 Il appartient au juge, saisi d'un moyen tiré de la méconnaissance de l'article R. 431-16 du code de l'urbanisme de s'assurer de la production, par le pétitionnaire, d'un document établi par l'architecte du projet ou par un expert attestant qu'une étude a été menée conformément aux exigences de la règlementation et que ses résultats ont été pris en compte au stade de la conception du projet. Il ne saurait en revanche dans ce cadre porter une appréciation sur le contenu de l'étude et son caractère suffisant au regard des exigences des plans de prévention des risques qui en imposent la réalisation.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
