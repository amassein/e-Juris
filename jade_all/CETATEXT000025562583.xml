<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000025562583</ID>
<ANCIEN_ID>JG_L_2012_02_000000355850</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/25/56/25/CETATEXT000025562583.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 20/02/2012, 355850, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2012-02-20</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>355850</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MASSE-DESSEN, THOUVENIN</AVOCATS>
<RAPPORTEUR>Mme Pascale Fombeur</RAPPORTEUR>
<COMMISSAIRE_GVT/>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 16 janvier 2012 au secrétariat du contentieux du Conseil d'Etat, présentée pour la CONFÉDÉRATION GÉNÉRALE DU TRAVAIL, dont le siège est situé 263 rue de Paris à Montreuil (93516), représentée par son représentant légal ; la CONFÉDÉRATION GÉNÉRALE DU TRAVAIL demande au juge des référés du Conseil d'Etat :<br/>
<br/>
              1°) d'ordonner, sur le fondement de l'article L. 521-1 du code de justice administrative, la suspension de l'exécution de l'arrêté du 11 août 2011 relatif à la délivrance, sans opposition de la situation de l'emploi, des autorisations de travail aux étrangers non ressortissants d'un État membre de l'Union européenne, d'un autre État partie à l'espace économique européen ou de la Confédération suisse ;<br/>
<br/>
              2°) de mettre à la charge de l'État le versement de la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              elle soutient qu'il existe un doute sérieux quant à la légalité de la décision contestée, qui a été adoptée à l'issue d'une procédure irrégulière, dès lors que les organisations syndicales n'ont pas été consultées ; que faute de préciser les métiers et les zones géographiques caractérisés par des difficultés de recrutement, l'arrêté contesté est entaché d'erreur de droit au regard de l'article L. 313-10 du code de l'entrée et du séjour des étrangers et du droit d'asile et de l'article R. 5221-21 du code du travail ; que les auteurs de l'arrêté ont commis une erreur manifeste d'appréciation de la situation de l'emploi en retirant de la liste des métiers ouverts aux étrangers non européens sans opposition de la situation du travail notamment des métiers de l'informatique et du BTP et en n'y inscrivant aucun métier du secteur de la santé et des services aux particuliers ; qu'en n'excluant pas de son champ d'application les ressortissants algériens et tunisiens et les ressortissants de certains Etats d'Afrique sub-saharienne, l'arrêté litigieux est entaché d'illégalité au regard des stipulations de l'article 7 de l'accord franco-algérien du 27 décembre 1968 et de celles de l'article 3 de l'accord franco-tunisien du 17 mars 1988 ainsi que des accords de gestion concertée des flux migratoires conclus par la France ; que la condition d'urgence est remplie dès lors que la décision contestée porte une atteinte grave et immédiate aux intérêts des étrangers résidant et travaillant en France en dépit des besoins économiques des secteurs considérés, comme le montrent les difficultés rencontrées dans le secteur du BTP et les difficultés que connaissent les jeunes diplômés étrangers ingénieurs ou informaticiens, et porte préjudice à l'économie française ;<br/>
<br/>
<br/>
              Vu l'arrêté dont la suspension de l'exécution est demandée ;<br/>
<br/>
              Vu la copie de la requête à fin d'annulation ; <br/>
<br/>
              Vu le mémoire en défense, enregistré le 2 février 2012, présenté par le ministre du travail, de l'emploi et de la santé, qui conclut au rejet de la requête ; il soutient que la condition d'urgence n'est pas remplie ; qu'en effet, la confédération requérante ne fait, en l'espèce, état d'aucune circonstance particulière de nature à justifier une atteinte grave et immédiate à l'exercice des libertés syndicales ; que l'urgence ne peut reposer sur la simple allégation d'une atteinte aux libertés d'aller et venir et d'exercer une activité professionnelle ; que le critère de la situation de l'emploi prévu par le code du travail est inopérant dans le cadre d'un renouvellement d'un titre de séjour ; que le coût économique pour les entreprises n'est pas démontré ; qu'un délai particulièrement long s'est écoulé entre l'arrêté et la demande de suspension, contredisant l'allégation d'urgence de la situation ; qu'il n'existe aucun doute sérieux quant à la légalité de l'arrêté contesté ; que les organisation syndicales d'employeurs et de salariés représentatives ont bien été consultées le 6 juillet 2011 ; que le pouvoir réglementaire pouvait définir la zone géographique comme couvrant l'ensemble de la France métropolitaine ; que pour les demande d'autorisation de travail présentées pour l'un des métiers figurant dans la liste annexée, les services n'ont pas à prendre en considération le bassin d'emploi, dans la mesure où ce critère est inopérant ; que le référentiel mentionné par la confédération requérante n'est pas opposable juridiquement et sa mention dans une liste ne saurait constituer une obligation ; que la liste de métiers en tension a été actualisée au regard des besoins en main d'oeuvre et des possibilités de formation des demandeurs d'emploi ; que les métiers concernant des professions réglementées n'ont pas vocation à être inscrits sur la liste ; que les ressortissants algériens et tunisiens étant régis par des conventions bilatérales spécifiques, ils n'entrent pas dans le champ d'application de l'arrêté et que les ressortissants d'Etats ayant conclu des accords de gestion concertée des flux migratoires bénéficient le cas échéant des stipulations plus favorables de ces accords ; <br/>
              Vu le mémoire en réplique, enregistré le 8 février 2012, présenté pour la CONFÉDÉRATION NATIONALE DU TRAVAIL, qui reprend les conclusions de sa requête et les mêmes moyens ; elle soutient en outre que les conséquences de l'arrêté contesté sur la situation des travailleurs étrangers auxquels la situation de l'emploi devient opposable portent atteinte à des situations juridiquement protégées et sont ainsi de nature à caractériser une situation d'urgence ; que, dans leur immense majorité, les travailleurs concernés n'auront d'autre alternative que de se retourner vers d'autres pays, ce qui portera préjudice à la réactivité concurrentielle, à la croissance et à l'attractivité des entreprises ; que le décalage entre la publication de l'arrêté au Journal officiel et le dépôt de la requête tient aux circonstances que les effets de l'arrêté, qui caractérisent la situation d'urgence, n'ont pu être constatés plus tôt ; que la procédure suivie n'a pas permis un débat loyal sur le projet d'arrêté ; que le ministre ne produit pas les justifications relatives aux besoins de main d'oeuvre ; que la zone géographique doit être prise en considération par les services préfectoraux ; que les auteurs, en prenant en considération les possibilités de formation, ont ajouté incompétemment à la loi un critère qu'elle ne prévoit pas ; <br/>
<br/>
              Vu les autres pièces du dossier ; <br/>
<br/>
              Vu le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              Vu le code du travail ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, la CONFÉDÉRATION GÉNÉRALE DU TRAVAIL et, d'autre part, le ministre de l'intérieur, de l'outre-mer, des collectivités territoriales et de l'immigration ainsi que le ministre du travail, de l'emploi et de la santé ;<br/>
<br/>
              Vu le procès-verbal de l'audience publique du 9 février 2012 à 10 h, au cours de laquelle ont été entendus :<br/>
<br/>
              - Me Masse-Dessen, avocat au Conseil d'Etat et à la Cour de cassation, avocat de la CONFÉDÉRATION GÉNÉRALE DU TRAVAIL ;<br/>
<br/>
              - les représentants de la CONFÉDÉRATION GÉNÉRALE DU TRAVAIL ;<br/>
<br/>
              - les représentants du ministre de l'intérieur, de l'outre-mer, des collectivités territoriales et de l'immigration et du ministre du travail, de l'emploi et de la santé ;<br/>
              et à l'issue de laquelle l'instruction a été prolongée jusqu'au 15 février 2012 à 12 heures ;<br/>
              Vu les pièces complémentaires produites le 10 février 2012 par le ministre du travail, de l'emploi et de la santé ;<br/>
<br/>
              Vu le nouveau mémoire, enregistré le 15 février 2012, présenté pour la CONFÉDÉRATION GÉNÉRALE DU TRAVAIL, qui reprend les conclusions et moyens de sa requête et produit des pièces complémentaires ;<br/>
<br/>
<br/>
<br/>
              Considérant qu'aux termes de l'article L. 521-1 du code de justice  administrative : "Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de  l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision" ; que l'urgence justifie que soit prononcée la suspension d'un acte administratif lorsque l'exécution de celui-ci porte atteinte, de manière suffisamment grave et immédiate, à un intérêt public, à la situation du requérant ou aux intérêts qu'il entend défendre ;<br/>
<br/>
              Considérant qu'aux termes de l'article L. 313-10 du code de l'entrée et du séjour des étrangers et du droit d'asile : " La carte de séjour temporaire autorisant l'exercice d'une activité professionnelle est délivrée : / 1° A l'étranger titulaire d'un contrat de travail visé conformément aux dispositions de l'article L. 341-2 du code du travail. / Pour l'exercice d'une activité professionnelle salariée dans un métier et une zone géographique caractérisés par des difficultés de recrutement et figurant sur une liste établie au plan national par l'autorité administrative, après consultation des organisations syndicales d'employeurs et de salariés représentatives, l'étranger se voit délivrer cette carte sans que lui soit opposable la situation de l'emploi sur le fondement du même article L. 341-2 (...) " ; que l'arrêté dont la suspension est demandée, pris sur le fondement des dispositions précitées, a pour effet de restreindre, par rapport à l'arrêté du 18 janvier 2008 antérieurement applicable, la liste des métiers pour lesquels une autorisation de travail peut être délivrée à un étranger non ressortissant d'un État membre de l'Union européenne, d'un autre État partie à l'Espace économique européen ou de la Confédération suisse sans que lui soit opposable la situation de l'emploi ; que, par suite, le préfet doit désormais, pour les métiers considérés, prendre en considération, en vertu du 1° de l'article R. 5221-20 du code du travail, " la situation de l'emploi dans la profession et dans la zone géographique pour lesquelles la demande est formulée, compte tenu des spécificités requises pour le poste de travail considéré, et les recherches déjà accomplies par l'employeur auprès des organismes de placement concourant au service public du placement pour recruter un candidat déjà présent sur le marché du travail " ; que ces dispositions s'appliquent en cas de première demande d'autorisation de travail, nécessaire à l'exercice en France d'une activité professionnelle salariée par un étranger résidant hors de France ou résidant régulièrement en France mais non titulaire d'un titre de séjour l'autorisant à travailler comme salarié, ainsi que, en vertu de l'article R. 5221-35 du code du travail, lors du premier renouvellement de l'autorisation si l'étranger demande à occuper un emploi dans un métier ou une zone géographique différents de ceux qui étaient mentionnés sur l'autorisation de travail initiale ; que, s'agissant des étrangers qui ont achevé avec succès, dans un établissement d'enseignement supérieur habilité au plan national, un cycle de formation conduisant à un diplôme au moins équivalent au master, il résulte des dispositions de l'article L. 311-11 du code de l'entrée et du séjour des étrangers et du droit d'asile que la situation de l'emploi ne leur est pas opposable en cas de recrutement dans un emploi en relation avec leur formation et répondant à certaines autres conditions, en vue de l'acquisition d'une première expérience professionnelle, mais le redevient ensuite ;<br/>
<br/>
              Considérant que, à l'appui de sa demande, la CONFÉDÉRATION GÉNÉRALE DU TRAVAIL invoque, d'une part, les répercussions de l'arrêté litigieux sur la situation de nombreux salariés étrangers du secteur du bâtiment et des travaux publics, dont les emplois seraient artificiellement déqualifiés par leurs employeurs et la rémunération diminuée ; que, toutefois, la suppression des métiers de chargé d'études techniques, chef de chantier et conducteur de travaux du BTP de la liste des métiers pour lesquels la situation de l'emploi n'est pas opposable n'est pas la cause directe des comportements décrits ; que la confédération requérante invoque, d'autre part, les incidences de l'arrêté pour de nombreux jeunes diplômés étrangers très qualifiés, notamment dans le secteur de l'informatique, qui se heurteraient à des refus de renouvellement de leur autorisation de travail et de leur titre de séjour, ainsi que ses conséquences pour les entreprises désireuses de les embaucher eu égard aux difficultés qu'elles rencontrent pour pourvoir certains postes ; que, toutefois, l'arrêté n'a pas pour effet d'interdire les recrutements envisagés mais seulement, si la situation de l'emploi justifie ces recrutements, d'imposer à l'employeur de faire la preuve de l'échec des recherches accomplies auprès des organismes concourant au service public du placement pour recruter un candidat déjà présent sur le marché du travail ; qu'une partie notable des difficultés relevées par la confédération requérante, d'ailleurs également pour des recrutements dans des emplois qui ne figuraient pas sur la liste annexée à l'arrêté du 18 janvier 2008, résulte non de l'arrêté litigieux mais de la plus grande rigueur dans l'instruction des demandes d'autorisation de travail recommandée par la circulaire du 31 mai 2011 du ministre de l'intérieur, de l'outre-mer, des collectivités territoriales et de l'immigration et du ministre du travail, de l'emploi et de la santé relative à la maîtrise de l'immigration professionnelle ; que, par ailleurs, le préjudice qui résulterait de l'application de l'arrêté litigieux pour l'économie française n'est pas établi ; qu'ainsi, il n'apparaît pas, en l'état de l'instruction, que l'exécution de l'arrêté litigieux porte atteinte, de manière suffisamment grave et immédiate, aux intérêts que la confédération requérante entend défendre pour caractériser une situation d'urgence justifiant qu'en soit prononcée la suspension dans l'attente du jugement de la requête au fond ;<br/>
<br/>
              Considérant qu'il résulte de ce qui précède que les conclusions de la CONFÉDÉRATION GÉNÉRALE DU TRAVAIL tendant à la suspension de l'exécution de l'arrêté du 11 août 2011 relatif à la délivrance, sans opposition de la situation de l'emploi, des autorisations de travail aux étrangers non ressortissants d'un État membre de l'Union européenne, d'un autre État partie à l'espace économique européen ou de la Confédération suisse doivent être rejetées, ainsi que, par voie de conséquence, ses conclusions tendant à l'application de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de la CONFÉDÉRATION GÉNÉRALE DU TRAVAIL est rejetée.<br/>
 Article 2 : La présente ordonnance sera notifiée à la CONFÉDÉRATION GÉNÉRALE DU TRAVAIL, au ministre de l'intérieur, de l'outre-mer, des collectivités territoriales et de l'immigration et au ministre du travail, de l'emploi et de la santé.<br/>
<br/>
<br/>
</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
