<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026719826</ID>
<ANCIEN_ID>JG_L_2012_12_000000346504</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/71/98/CETATEXT000026719826.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème et 3ème sous-sections réunies, 03/12/2012, 346504, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2012-12-03</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>346504</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème et 3ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP HEMERY, THOMAS-RAQUIN</AVOCATS>
<RAPPORTEUR>M. Jean-Marc Anton</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Benoît Bohnert</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2012:346504.20121203</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 8 février 2011 et 9 mai 2011 au secrétariat du contentieux du Conseil d'Etat, présentés pour la société Rhodia S.A, dont le siège est 110 esplanade Charles de Gaulle, Immeuble Coeur Défense Tour A à Courbevoie (92400) ; elle demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler l'article 4 de l'arrêt n° 10VE00755 du 23 novembre 2010 par lequel la cour administrative d'appel de Versailles n'a que partiellement fait droit à sa requête tendant à l'annulation du jugement n° 0612677 du 7 janvier 2010 du tribunal administratif de Versailles rejetant sa demande tendant à la décharge des rappels de taxe sur la valeur ajoutée mis à sa charge au titre de la période 1er janvier au 31 décembre 1998 et à la décharge de ces impositions ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la sixième directive 77/388/CEE du Conseil en date du 17 mai 1977, en matière d'harmonisation des législations des Etats membres relatives aux taxes sur le chiffre d'affaires ;<br/>
<br/>
              Vu le code général des impôts et le livre des procédures fiscales ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Marc Anton, Maître des Requêtes,  <br/>
<br/>
              - les observations de la SCP Hémery, Thomas-Raquin, avocat de la société Rhodia,<br/>
<br/>
- les conclusions de M. Benoît Bohnert, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Hémery, Thomas-Raquin, avocat de la société Rhodia ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond qu'à la suite d'une vérification de comptabilité portant sur la période du 1er janvier 1997 au 31 décembre 1998, l'administration a notifié à la société Rhodia, le 20 décembre 2000, des rappels de taxe sur la valeur ajoutée qu'elle a mis en recouvrement le 15 novembre 2001 ; qu'après le rejet de sa réclamation tendant à leur décharge partielle, la société a saisi le tribunal administratif de Versailles d'une demande qui a été rejetée par jugement du 7 janvier 2010 ; qu'elle se pourvoit en cassation contre l'article 4 de l'arrêt du 23 novembre 2010 par lequel la cour administrative d'appel de Versailles, après avoir fait partiellement droit à sa requête par l'article 1er en la déchargeant à hauteur d'une somme de 94 213 euros des rappels de taxe sur la valeur ajoutée au titre de la période du 1er janvier au 31 décembre 1998 ainsi que des intérêts correspondants, a rejeté le surplus de ses conclusions ; que, par la voie d'un pourvoi incident, le ministre du budget, des comptes publics et de la réforme de l'Etat, porte-parole du Gouvernement demande l'annulation de cet article 1er et de l'article 2 réformant le jugement en ce qu'il a de contraire à l'article 1er ;<br/>
<br/>
<br/>
              Sur le pourvoi principal :<br/>
<br/>
              En ce qui concerne l'étendue du litige :<br/>
<br/>
              2. Considérant que, par une décision du 14 décembre 2011, postérieure à l'introduction du pourvoi, la société Rhodia a bénéficié d'un dégrèvement à hauteur d'une somme de 159 960,18 euros en principal, correspondant à la prise en compte, par compensation pour ce montant avec une fraction des rappels en litige, d'un crédit de taxe sur la valeur ajoutée dont elle disposait au 31 décembre 1998 ; que, dès lors, il n'y a pas lieu de statuer sur les conclusions du pourvoi principal à hauteur de cette somme ; <br/>
<br/>
              En ce qui concerne la régularité de l'arrêt : <br/>
<br/>
              3. Considérant qu'aux termes de l'article L. 57 du livre des procédures fiscales, dans sa rédaction applicable à la présente procédure : " L'administration adresse au contribuable une notification de redressement qui doit être motivée de manière à lui permettre de formuler ses observations ou de faire connaître son acceptation (...) Lorsque l'administration rejette les observations du contribuable sa réponse doit également être motivée " ;<br/>
<br/>
              4. Considérant, en premier lieu, que la cour a relevé que le tribunal avait répondu au moyen tiré de l'insuffisante motivation de la notification de redressement en rappelant les conditions de régularité de ce document et les éléments de fait et de droit qu'il contenait, en écartant le moyen tiré de la contradiction de motifs alléguée et en indiquant que le tribunal n'était pas tenu de répondre à tous les arguments soulevés par les parties ; que, par suite, le moyen tiré de l'insuffisante motivation de l'arrêt s'agissant de la régularité du jugement doit être écarté ; <br/>
<br/>
              5. Considérant, en deuxième lieu, que dès lors que plusieurs chefs de redressement procèdent de l'application d'une même règle de droit, une notification de redressement peut comporter une motivation commune à ces chefs de redressement ; qu'il ressort des termes de la notification de redressement du 20 décembre 2000 que l'administration a justifié son refus d'admettre la déduction de la taxe sur la valeur ajoutée acquittée au titre de prestations correspondant, d'une part, à des cessions de titres des sociétés Setila, Rexor et Tergal et, d'autre part, à l'introduction des titres de la société à la bourse de New-York au motif que ces opérations étaient situées hors du champ d'application de la taxe sur la valeur ajoutée ; que, dans ces conditions, alors que la notification pouvait ne pas réitérer, pour chacune des deux catégories d'opération, le fondement légal de l'imposition, la cour a suffisamment motivé son arrêt en relevant que ce document indiquait la nature, l'origine, le motif, le montant et les modalités de calcul des rappels et comportait les précisions suffisantes pour éclairer la société requérante sur la nature et les motifs des rappels envisagés et lui permettre d'en discuter le bien-fondé ;<br/>
<br/>
              6. Considérant, en troisième lieu, qu'en jugeant suffisantes les précisions contenues dans la réponse aux observations du contribuable, en date du 17 avril 2001, relatives aux raisons pour lesquelles l'administration contestait le caractère d'activité économique de la prise de participation de la société requérante dans les filiales dont les titres avaient été cédés et en relevant que, parmi ces raisons, figurait notamment la faible vraisemblance de ce caractère économique compte tenu de la courte durée de détention des titres alléguée par l'administration, la cour a suffisamment répondu au moyen tiré de ce que ce courrier ne pouvait, sans être entaché d'insuffisance de motivation, se borner à alléguer cette faible durée ;<br/>
<br/>
              En ce qui concerne le bien-fondé de l'arrêt :  <br/>
<br/>
              7. Considérant qu'aux termes du 1 du I de l'article 271 du code général des impôts : " La taxe sur la valeur ajoutée qui a grevé les éléments du prix d'une opération imposable est déductible de la taxe sur la valeur ajoutée applicable à cette opération (...) " ; qu'il résulte de ces dispositions, interprétées à la lumière des paragraphes 1 et 2, 3 et 5 de l'article 17 de la sixième directive du Conseil du 17 mai 1977, que l'existence d'un lien direct et immédiat entre une opération particulière en amont et une ou plusieurs opérations en aval ouvrant droit à déduction est, en principe, nécessaire pour qu'un droit à déduction de la taxe sur la valeur ajoutée en amont soit reconnu à l'assujetti et pour déterminer l'étendue d'un tel droit ; que le droit à déduction de la taxe sur la valeur ajoutée grevant l'acquisition de biens ou de services en amont suppose que les dépenses effectuées pour acquérir ceux-ci fassent partie des éléments constitutifs du prix des opérations taxées en aval ouvrant droit à déduction ; qu'en l'absence d'un tel lien, un assujetti est toutefois fondé à déduire l'intégralité de la taxe sur la valeur ajoutée ayant grevé des biens et services en amont, lorsque les dépenses liées à l'acquisition de ces biens et services font partie de ses frais généraux et sont, en tant que telles, des éléments constitutifs du prix des biens produits ou des services fournis par cet assujetti ; <br/>
<br/>
              8. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que la société requérante exerçait une activité de holding du groupe Rhodia, qu'elle n'avait aucune activité industrielle et commerciale et ne rendait pas de prestations de services à titre onéreux à ses filiales ; <br/>
<br/>
              9. Considérant, d'une part, que la cour, qui a relevé ces faits non contestés devant elle, n'a pas commis d'erreur de droit en en déduisant que la taxe sur la valeur ajoutée correspondant aux frais de conseils et d'études acquittés à l'occasion de cessions de titres des sociétés Setila, Rexor et Tergal, opérations exonérées de cette taxe, n'était pas déductible au motif que la société n'exerçait pas d'activité industrielle ou commerciale ; <br/>
<br/>
              10. Considérant, d'autre part, que la société Rhodia a soutenu devant la cour qu'elle s'immisçait dans la gestion de ses filiales dans le cadre de la détermination d'une stratégie industrielle dont procéderaient les cessions de titres et qu'elle était, par suite, fondée à soutenir que la taxe sur la valeur ajoutée devait être déduite au titre de ses frais généraux ; que la cour qui a écarté ce moyen en estimant, par une appréciation souveraine non arguée de dénaturation et sans méconnaître les règles relatives à la dévolution de la charge de la preuve, que la société Rhodia ne l'établissait pas par des documents probants et une argumentation circonstanciée, s'est ainsi fondée sur l'absence de preuve apportée par la société et non sur la seule circonstance, relevée par l'administration, d'une courte durée de détention des titres ; qu'en déduisant de ces constatations que la société ne pouvait déduire la taxe sur la valeur ajoutée, elle n'a ni méconnu les dispositions de l'article 271 du code général des impôts ni entaché son arrêt de contradiction de motifs ;<br/>
<br/>
              11. Considérant qu'il résulte de ce qui précède que la société Rhodia n'est pas fondée à demander l'annulation de l'article 4 de l'arrêt attaqué en ce qu'il a maintenu à sa charge le surplus des impositions contestées ; <br/>
<br/>
              Sur le pourvoi incident :<br/>
<br/>
              12. Considérant qu'il résulte des dispositions du 4° de l'article 259 B du code général des impôts et du 2 de l'article 283 du même code, dans leur rédaction applicable au litige, que s'agissant des prestations des conseillers, ingénieurs, bureaux d'études dans tous les domaines et les prestations des experts-comptables, la taxe doit être acquittée par le preneur ;<br/>
<br/>
              13. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que la société requérante a acquitté en 1998 à un cabinet britannique d'audit et de conseil des frais correspondant à des prestations de services rendues à l'acquéreur suisse de sa filiale Setila qu'elle s'était engagée à prendre à sa charge à concurrence de 50% des frais exposés ; que la facture qui lui a été adressée a été établie hors taxe sur la valeur ajoutée ; que l'administration a estimé que la somme qui était demandée à la société devait être assujettie à la taxe à la valeur ajoutée sur le fondement de ces dispositions ; <br/>
<br/>
              14. Considérant que le moyen tiré de ce que la société Rhodia devait être regardée comme le preneur intermédiaire ou apparent des prestations d'audit et de conseil relatives à la cession de la société Setila, qui n'a pas été soulevé par le ministre devant la cour et n'est pas d'ordre public, est sans influence sur le bien-fondé de l'arrêt attaqué ; que, par suite, le moyen tiré de ce que la cour, dont les constatations ne sont pas arguées de dénaturation, aurait commis une erreur de droit et inexactement qualifié les faits en jugeant que la société requérante n'était pas le preneur de la prestation de services rendue par le cabinet d'audit et de conseil à l'acquéreur suisse doit être écarté ; <br/>
<br/>
              15. Considérant qu'il résulte de ce qui précède que le pourvoi incident du ministre du budget, des comptes publics et de la réforme de l'Etat, porte-parole du Gouvernement doit être rejeté ;<br/>
<br/>
              16. Considérant qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées au titre de l'article L. 761-1 du code de justice administrative par la société Rhodia ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
Article 1er : Il n'y a pas lieu de statuer sur les conclusions de la société Rhodia à hauteur de la somme de 159 960,18 euros.<br/>
Article 2 : Le surplus des conclusions du pourvoi de la société Rhodia est rejeté.<br/>
Article 3 : Le pourvoi incident du ministre du budget, des comptes publics et de la réforme de l'Etat, porte-parole du Gouvernement, est rejeté.<br/>
Article 4 : La présente décision sera notifiée à la société Rhodia et au ministre de l'économie et des finances.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
