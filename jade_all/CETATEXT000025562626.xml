<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000025562626</ID>
<ANCIEN_ID>JG_L_2012_03_000000341562</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/25/56/26/CETATEXT000025562626.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème et 2ème sous-sections réunies, 19/03/2012, 341562, Publié au recueil Lebon</TITRE>
<DATE_DEC>2012-03-19</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>341562</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème et 2ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BOUTET ; FOUSSARD ; SCP BOUZIDI, BOUHANNA</AVOCATS>
<RAPPORTEUR>M. Fabrice Aubert</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Boulouis</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2012:341562.20120319</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 15 juillet et 15 octobre 2010 au secrétariat du contentieux du Conseil d'Etat, présentés pour la SA GROUPE PARTOUCHE, dont le siège est au 141 bis rue de Saussure à Paris (75017), représenté par son président directeur général ; la société demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 08DA00104 du 11 mai 2010 par lequel la cour administrative d'appel de Douai a rejeté sa requête tendant à l'annulation du jugement n° 0607378 du 20 novembre 2007 du tribunal administratif de Lille rejetant sa demande tendant, premièrement, à l'annulation de la délibération du 9 octobre 2006 par laquelle le conseil municipal de la ville de Lille a autorisé son maire à signer le contrat de délégation de service public du casino de Lille avec le groupe Lucien Barrière, deuxièmement, à ce qu'il soit enjoint à la commune de résilier la convention ou de saisir le juge du contrat afin qu'il prononce sa nullité, sous astreinte de 1 000 euros par jour de retard, et, troisièmement, à ce que soit mis à la charge de la commune la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à ses conclusions d'appel ;<br/>
<br/>
              3°) de mettre à la charge de la commune de Lille une somme de 5 000 euros en application des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la loi du 15 juin 1907 ; <br/>
<br/>
              Vu le code général des collectivités territoriales ;<br/>
<br/>
              Vu le code général de la propriété des personnes publiques ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Fabrice Aubert, Auditeur,  <br/>
<br/>
              - les observations de la SCP Bouzidi, Bouhanna, avocat de la SA GROUPE PARTOUCHE, de la SCP Boutet, avocat de la ville de Lille et de Me Foussard, avocat de la Société Lilloise d'Animation Touristique, <br/>
<br/>
              - les conclusions de M. Nicolas Boulouis, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Bouzidi, Bouhanna, avocat de la SOCIÉTÉ GROUPE PARTOUCHE, à la SCP Boutet, avocat de la ville de Lille et à Me Foussard, avocat de la Société Lilloise d'Animation Touristique ;<br/>
<br/>
<br/>
<br/>Considérant qu'il ressort des pièces du dossier soumis au juge du fond que la commune de Lille a lancé, le 30 décembre 2005, une procédure de mise en concurrence pour l'attribution d'une délégation de service public portant sur la réalisation et l'exploitation d'un casino, d'une salle de spectacles de 1 200 places, d'un hôtel de catégorie quatre étoiles d'une capacité de 148 chambres, de trois restaurants et divers bars, ainsi que d'un parc de stationnement de 680 places ; que, par délibération du 9 octobre 2006, le conseil municipal de Lille a autorisé son maire à signer le contrat de délégation de service public avec le groupe Lucien Barrière ; que par l'arrêt attaqué, la cour administrative d'appel de Douai a rejeté l'appel de la SA GROUPE PARTOUCHE, candidat évincé, contre le jugement du tribunal administratif de Lille du 20 novembre 2007 rejetant sa demande dirigée contre la délibération du 9 octobre 2006 ;<br/>
<br/>
              Considérant qu'il résulte des dispositions de la loi du 15 juin 1907 relative aux casinos, ainsi que des travaux parlementaires qui ont précédé son adoption et de ses modifications successives, que le législateur, tout en soumettant à une surveillance particulière les jeux de casino, a entendu que ces activités concourent aux objectifs de développement touristique, économique et culturel des communes autorisées à les accueillir ; qu'ainsi, en vertu de l'article 2 de la loi du 15 juin 1907 modifiée, les jeux de casino sont autorisés par arrêté du ministre de l'intérieur, sur avis conforme du conseil municipal de la commune concernée ; que ces dispositions imposent à la commune, d'une part, de conclure à cette fin avec le titulaire de l'autorisation une convention et, d'autre part, d'assortir celle-ci d'un cahier des charges fixant des obligations au cocontractant, relatives notamment à la prise en charge du financement d'infrastructures et de missions d'intérêt général en matière de développement économique, culturel et touristique ; que si ces jeux de casinos ne constituent pas, par eux-mêmes, une activité de service public, les conventions obligatoirement conclues pour leur installation et leur exploitation, dès lors que le cahier des charges impose au cocontractant une participation à ces missions et que sa rémunération est substantiellement assurée par les résultats de l'exploitation, ont le caractère de délégation de service public ; <br/>
<br/>
              Considérant, en premier lieu, que pour rejeter le moyen tiré de l'insuffisance d'information des membres du conseil municipal sur la durée d'amortissement proposée par chacun des candidats à la délégation pour le projet hôtelier réalisé à titre accessoire, la cour a relevé, d'une part, que le procès-verbal de la commission de délégation de service public comportait la durée de l'amortissement prévu pour l'hôtel par chaque candidat et a jugé, d'autre part, que la société requérante ne fournissait pas d'élément concret permettant d'évaluer, en l'espèce, l'incidence des conditions d'amortissement différentes sur les conditions d'appréciation des offres ; que ce faisant, la cour, qui a mis le juge de cassation en mesure d'exercer son contrôle, n'a pas entaché son arrêt d'insuffisance de motivation ; <br/>
<br/>
              Considérant, en deuxième lieu, que pour écarter le moyen tiré de la partialité de la présidente de la commission de délégation de service public à l'égard de la société Lucien Barrière, la cour a recherché si les liens de subordination professionnelle ayant existé entre elle et la personne choisie par le groupe Lucien Barrière pour le conseiller sur sa candidature étaient de nature, eu égard à leur ancienneté et leur intensité, à faire porter, par eux-mêmes, un doute sur l'impartialité de la commission ; que, par suite, la cour n'a pas commis d'erreur de droit en jugeant que la procédure devant la commission de délégation de service public n'était pas entachée de partialité ; <br/>
<br/>
              Considérant, en troisième lieu, que la cour n'a pas davantage commis d'erreur de droit en ne se prononçant pas sur le point de savoir si la présidente de la commission de délégation de service public avait été régulièrement habilitée pour représenter le maire de Lille, dès lors que le moyen tiré de son défaut d'habilitation n'était pas soulevé devant les juges du fond par la société requérante ; que de même, la société requérante n'ayant pas soulevé devant les juges du fond le moyen tiré de ce que la construction d'une annexe hôtelière serait de nature à rompre l'égalité entre les candidats en favorisant les groupes hôteliers, la cour n'a pas commis l'erreur de droit alléguée en ne répondant pas à un tel moyen ; <br/>
<br/>
              Considérant, en quatrième lieu, que l'article L. 1411-2 du code général des collectivités territoriales dispose que : " (...) les conventions de délégation de service public ne peuvent contenir de clauses par lesquelles le délégataire prend à sa charge l'exécution de services ou de paiements étrangers à l'objet de la délégation " ; que ces dispositions ne font pas obstacle à ce qu'une convention de délégation de service public mette à la charge du cocontractant des prestations accessoires dès lors qu'elles présentent un caractère complémentaire à l'objet de la délégation ; que la cour, par une appréciation souveraine, ayant estimé que l'activité hôtelière permise par la délégation, en complément de l'exploitation du casino, présentait un caractère accessoire à l'activité de jeux, elle n'a pas, en conséquence,  commis d'erreur de droit en jugeant que la délégation litigieuse ne méconnaissait pas les dispositions de l'article L. 1411-2 du code général des collectivités territoriales ;<br/>
<br/>
              Considérant, enfin, qu'il résulte des dispositions de l'article L. 2333-54 du code général des collectivités territoriales que les communes dotées d'un casino peuvent instituer sur le produit brut des jeux un prélèvement dont le taux ne peut dépasser 15 % de ce produit ; qu'indépendamment de ce prélèvement fiscal, les conventions d'occupation du domaine public peuvent prévoir le versement d'une redevance par le cocontractant en contrepartie des avantages que lui procurent l'occupation du domaine, et qui excèdent le seul produit des jeux ; que cette redevance, si elle affecte les revenus que tire des jeux le délégataire, comme les autres charges qu'il doit supporter, n'est pas légalement exclue de l'assiette du prélèvement prévu par l'article L. 2333-54 du code général des collectivités territoriales, lequel s'applique sur le produit brut des jeux ; qu'ainsi, contrairement à ce que soutient la société requérante, la cour n'a pas commis d'erreur de droit en jugeant que la redevance domaniale demandée à un casino ne pouvait être regardée comme incluse dans le plafond de 15 % fixé pour les prélèvements sur le produit brut des jeux ;<br/>
<br/>
              Considérant qu'il résulte de tout ce qui précède que la SA GROUPE PARTOUCHE n'est pas fondée à demander l'annulation de l'arrêt attaqué ; que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mis à la charge de la commune de Lille, qui n'est pas la partie perdante, la somme demandée par la SA GROUPE PARTOUCHE ; qu'en revanche, il y a lieu, sur le fondement de ces dispositions, de mettre à la charge de la SA GROUPE PARTOUCHE le versement de la somme de 3 000 euros à la Société Lilloise d'Animation Touristique ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de la SA GROUPE PARTOUCHE est rejeté.<br/>
Article 2 : La SA GROUPE PARTOUCHE versera à la Société Lilloise d'Animation Touristique une somme de 3 000 euros en application de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée à la SA GROUPE PARTOUCHE, à la commune de Lille et à la Société Lilloise d'Animation Touristique.<br/>
Copie pour information en sera transmise au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">135-02-03-03 COLLECTIVITÉS TERRITORIALES. COMMUNE. ATTRIBUTIONS. SERVICES COMMUNAUX. - 1) CONVENTIONS CONCLUES POUR L'INSTALLATION ET L'EXPLOITATION DE CASINOS - QUALIFICATION - DÉLÉGATION DE SERVICE PUBLIC, EN RAISON DES OBLIGATIONS DE PARTICIPATION AUX MISSIONS DE SERVICE PUBLIC ET DE LA RÉMUNÉRATION SUBSTANTIELLEMENT ASSURÉE PAR LES RÉSULTATS DE L'EXPLOITATION [RJ1] - 2) PRÉLÈVEMENT SUR LE PRODUIT BRUT DES JEUX (ART. L. 2333-54 DU CGCT) - ASSIETTE - REDEVANCE D'OCCUPATION DU DOMAINE PUBLIC QUE LE COCONTRACTANT PEUT ÊTRE CONDUIT À VERSER À LA COMMUNE - INCLUSION.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">39-01-03-03 MARCHÉS ET CONTRATS ADMINISTRATIFS. NOTION DE CONTRAT ADMINISTRATIF. DIVERSES SORTES DE CONTRATS. DÉLÉGATIONS DE SERVICE PUBLIC. - CONVENTIONS CONCLUES POUR L'INSTALLATION ET L'EXPLOITATION DE CASINOS - INCLUSION, EN RAISON DES OBLIGATIONS DE PARTICIPATION AUX MISSIONS DE SERVICE PUBLIC ET DE LA RÉMUNÉRATION SUBSTANTIELLEMENT ASSURÉE PAR LES RÉSULTATS DE L'EXPLOITATION [RJ1].
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">63-02 SPORTS ET JEUX. CASINOS. - 1) CONVENTIONS CONCLUES POUR L'INSTALLATION ET L'EXPLOITATION DE CASINOS - QUALIFICATION - DÉLÉGATION DE SERVICE PUBLIC, EN RAISON DES OBLIGATIONS DE PARTICIPATION AUX MISSIONS DE SERVICE PUBLIC ET DE LA RÉMUNÉRATION SUBSTANTIELLEMENT ASSURÉE PAR LES RÉSULTATS DE L'EXPLOITATION [RJ1] - 2) PRÉLÈVEMENT SUR LE PRODUIT BRUT DES JEUX (ART. L. 2333-54 DU CGCT) - ASSIETTE - REDEVANCE D'OCCUPATION DU DOMAINE PUBLIC QUE LE COCONTRACTANT PEUT ÊTRE CONDUIT À VERSER À LA COMMUNE - INCLUSION.
</SCT>
<ANA ID="9A"> 135-02-03-03 1) Si les jeux de casinos ne constituent pas, par eux-mêmes, une activité de service public, les conventions obligatoirement conclues pour leur installation et leur exploitation, dès lors que le cahier des charges impose au cocontractant une participation à des missions de service public et que sa rémunération est substantiellement assurée par les résultats de l'exploitation, ont le caractère de délégation de service public.,,2) Il résulte des dispositions de l'article L. 2333-54 du code général des collectivités territoriales (CGCT) que les communes dotées d'un casino peuvent instituer sur le produit brut des jeux un prélèvement dont le taux ne peut dépasser 15 % de ce produit. Indépendamment de ce prélèvement fiscal, les conventions d'occupation du domaine public peuvent prévoir le versement d'une redevance par le cocontractant en contrepartie des avantages que lui procurent l'occupation du domaine, et qui excèdent le seul produit des jeux. Cette redevance, si elle affecte les revenus que tire des jeux le délégataire, comme les autres charges qu'il doit supporter, n'est pas légalement exclue de l'assiette du prélèvement prévu par l'article L. 2333-54 du CGCT, lequel s'applique sur le produit brut des jeux.</ANA>
<ANA ID="9B"> 39-01-03-03 Si les jeux de casinos ne constituent pas, par eux-mêmes, une activité de service public, les conventions obligatoirement conclues pour leur installation et leur exploitation, dès lors que le cahier des charges impose au cocontractant une participation à des missions du service public et que sa rémunération est substantiellement assurée par les résultats de l'exploitation, ont le caractère de délégation de service public.</ANA>
<ANA ID="9C"> 63-02 1) Si les jeux de casinos ne constituent pas, par eux-mêmes, une activité de service public, les conventions obligatoirement conclues pour leur installation et leur exploitation, dès lors que le cahier des charges impose au cocontractant une participation à des missions de service public et que sa rémunération est substantiellement assurée par les résultats de l'exploitation, ont le caractère de délégation de service public.,,2) Il résulte des dispositions de l'article L. 2333-54 du code général des collectivités territoriales (CGCT) que les communes dotées d'un casino peuvent instituer sur le produit brut des jeux un prélèvement dont le taux ne peut dépasser 15 % de ce produit. Indépendamment de ce prélèvement fiscal, les conventions d'occupation du domaine public peuvent prévoir le versement d'une redevance par le cocontractant en contrepartie des avantages que lui procurent l'occupation du domaine, et qui excèdent le seul produit des jeux. Cette redevance, si elle affecte les revenus que tire des jeux le délégataire, comme les autres charges qu'il doit supporter, n'est pas légalement exclue de l'assiette du prélèvement prévu par l'article L. 2333-54 du CGCT, lequel s'applique sur le produit brut des jeux.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 25 mars 1966, Ville de Royan et société anonyme de Royan et Couzinet, n° 46504 et autres, p. 237 ; Section de l'intérieur, avis, 4 avril 1995, n° 357274, Rapport public 1995, p. 414 ; CE, Section, 10 mars 2006, Commune d'Houlgate, n° 264098 et autres, p. 138.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
