<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036744001</ID>
<ANCIEN_ID>JG_L_2018_03_000000413235</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/74/40/CETATEXT000036744001.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème chambre, 26/03/2018, 413235, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-03-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>413235</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP THOUVENIN, COUDRAY, GREVY</AVOCATS>
<RAPPORTEUR>M. Marc Firoud</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Gilles Pellissier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:413235.20180326</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme A...B..., veuveD..., a demandé au tribunal administratif de Marseille d'annuler la décision du 8 février 2012 par laquelle le ministre de la défense a rejeté sa demande, présentée en qualité d'ayant droit de son époux décédé, tendant au bénéfice de la loi du 5 janvier 2010 relative à la reconnaissance et à l'indemnisation des victimes des essais nucléaires français. Par un jugement n° 1201892 du 2 octobre 2014, le tribunal administratif de Marseille a rejeté sa demande. <br/>
<br/>
              Par un arrêt n° 14MA04707 du 7 mars 2017, la cour administrative d'appel de Marseille a rejeté l'appel formé par Mme B...contre ce jugement.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 9 août et 12 octobre 2017 au secrétariat du contentieux du Conseil d'Etat, Mme B...demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la loi n° 2010-2 du 5 janvier 2010 ;<br/>
              - la loi n° 2017-256 du 28 février 2017 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Marc Firoud, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Gilles Pellissier, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Thouvenin, Coudray, Grevy, avocat de MmeB....<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que M. C...D..., né le 12 janvier 1931, militaire de carrière, a été affecté du 7 mai 1969 au 4 mai 1970 au sein de la 115ème compagnie de marche du génie de l'air, laquelle était en charge de la réalisation des travaux d'infrastructures aériennes et routières sur l'atoll de Hao (Polynésie française) ; qu'il est décédé le 27 septembre 1994 des suites d'un cancer du cerveau qui lui a été diagnostiqué la même année ; que Mme D...a présenté, en sa qualité d'ayant droit, une demande d'indemnisation sur le fondement de la loi n° 2010-2 du 5 janvier 2010 relative à la reconnaissance et à l'indemnisation des victimes des essais nucléaires français ; que, par une décision du 8 février 2012, le ministre de la défense a rejeté cette demande, au motif que le risque imputable aux essais nucléaires dans la survenue de la maladie de M. D...pouvait être qualifié de négligeable ; que Mme B...a demandé au tribunal administratif de Marseille d'annuler cette décision ; que, par l'arrêt attaqué, la cour administrative d'appel de Marseille a rejeté l'appel formé par Mme B...contre le jugement du 2 octobre 2014 par lequel le tribunal administratif de Marseille a rejeté sa demande ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article 1er de la loi du 5 janvier 2010 relative à la reconnaissance et à l'indemnisation des victimes des essais nucléaires français : " Toute personne souffrant d'une maladie radio-induite résultant d'une exposition à des rayonnements ionisants dus aux essais nucléaires français et inscrite sur une liste fixée par décret en Conseil d'Etat conformément aux travaux reconnus par la communauté scientifique internationale peut obtenir réparation intégrale de son préjudice dans les conditions prévues par la présente loi. / Si la personne est décédée, la demande de réparation peut être présentée par ses ayants droit " ; qu'aux termes de l'article 2 de cette même loi : " La personne souffrant d'une pathologie radio-induite doit avoir résidé ou séjourné : / 1° Soit entre le 13 février 1960 et le 31 décembre 1967 au Centre saharien des expérimentations militaires, ou entre le 7 novembre 1961 et le 31 décembre 1967 au Centre d'expérimentations militaires des oasis ou dans les zones périphériques à ces centres ; / 2° Soit entre le 2 juillet 1966 et le 31 décembre 1998 en Polynésie française./ (...) " ; qu'aux termes de l'article 4 de cette même loi, dans sa rédaction antérieure à la loi du 28 février 2017 de programmation relative à l'égalité réelle outre-mer et portant autres dispositions en matière sociale et économique, disposait : " I. - Les demandes individuelles d'indemnisation sont soumises au comité d'indemnisation des victimes des essais nucléaires (...) / V. - Ce comité examine si les conditions de l'indemnisation sont réunies. Lorsqu'elles le sont, l'intéressé bénéficie d'une présomption de causalité à moins qu'au regard de la nature de la maladie et des conditions de son exposition le risque attribuable aux essais nucléaires puisse être considéré comme négligeable. Le comité le justifie auprès de l'intéressé (...) " ; qu'enfin, aux termes de l'article 113 de la loi du 28 février 2017 : " I.- Au premier alinéa du V de l'article 4 de la loi n° 2010-2 du 5 janvier 2010 relative à la reconnaissance et à l'indemnisation des victimes des essais nucléaires français, les mots et la phrase : " à moins qu'au regard de la nature de la maladie et des conditions de son exposition le risque attribuable aux essais nucléaires puisse être considéré comme négligeable. Le comité le justifie auprès de l'intéressé. " sont supprimés. / II.- Lorsqu'une demande d'indemnisation fondée sur les dispositions du I de l'article 4 de la loi n° 2010-2 du 5 janvier 2010 relative à la reconnaissance et à l'indemnisation des victimes des essais nucléaires français a fait l'objet d'une décision de rejet par le ministre de la défense ou par le comité d'indemnisation des victimes des essais nucléaires avant l'entrée en vigueur de la présente loi, le comité d'indemnisation des victimes des essais nucléaires réexamine la demande s'il estime que l'entrée en vigueur de la présente loi est susceptible de justifier l'abrogation de la précédente décision. Il en informe l'intéressé ou ses ayants droit s'il est décédé qui confirment leur réclamation et, le cas échéant, l'actualisent. Dans les mêmes conditions, le demandeur ou ses ayants droit s'il est décédé peuvent également présenter une nouvelle demande d'indemnisation, dans un délai de douze mois à compter de l'entrée en vigueur de la présente loi. / III.- Une commission composée pour moitié de parlementaires et pour moitié de personnalités qualifiées propose, dans un délai de douze mois à compter de la promulgation de la présente loi, les mesures destinées à réserver l'indemnisation aux personnes dont la maladie est causée par les essais nucléaires. Elle formule des recommandations à l'attention du Gouvernement " ; <br/>
<br/>
              3. Considérant que l'entrée en vigueur des dispositions précitées du I de l'article 113 de la loi du 28 février 2017 n'est pas manifestement impossible en l'absence de mesures d'application ; qu'elle est dès lors intervenue le lendemain de la publication de cette loi au Journal officiel de la République française, soit le 2 mars 2017 ; que ces dispositions sont applicables aux instances en cours à cette date ; qu'il suit de là qu'en ne faisant pas application de ces nouvelles dispositions à la demande d'indemnisation présentée par MmeB..., la cour a commis une erreur de droit ; que son arrêt doit donc être annulé, sans qu'il soit besoin d'examiner les autres moyens du pourvoi ;<br/>
<br/>
              4. Considérant que, dans les circonstances de l'espèce, il y a lieu de régler l'affaire au fond, en application des dispositions de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              5. Considérant qu'il résulte des dispositions citées au point 2 ci-dessus que la décision du 8 février 2012 par laquelle le ministre de la défense a rejeté la demande d'indemnisation présentée par Mme B...au titre de la loi n° 2010-2 du 5 janvier 2010 relative à la reconnaissance et à l'indemnisation des victimes des essais nucléaires français, au motif que le risque imputable aux essais nucléaires dans la survenue de la maladie de M. D... pouvait être considéré comme négligeable, est illégale ; qu'il y a lieu par suite d'annuler le jugement du tribunal administratif de Marseille et de renvoyer Mme B...devant le comité d'indemnisation des victimes des essais nucléaires pour que celui-ci procède au réexamen de sa demande ;<br/>
<br/>
              6. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 3 500 euros à verser à Mme B...au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 7 mars 2017 de la cour administrative d'appel de Marseille et le jugement du 2 octobre 2014 du tribunal administratif de Marseille sont annulés.<br/>
Article 2 : La décision du ministre de la défense en date du 8 février 2012 est annulée.<br/>
Article 3 : Mme B...est renvoyée devant le comité d'indemnisation des victimes des essais nucléaires pour que celui-ci procède au réexamen de sa demande.<br/>
Article 4 : L'Etat versera à Mme B...une somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 5 : La présente décision sera notifiée à Mme A...B..., épouseD..., à la ministre des armées et au comité d'indemnisation des victimes des essais nucléaires.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
