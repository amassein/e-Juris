<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026230079</ID>
<ANCIEN_ID>JG_L_2012_07_000000325371</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/23/00/CETATEXT000026230079.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème et 9ème sous-sections réunies, 27/07/2012, 325371</TITRE>
<DATE_DEC>2012-07-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>325371</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème et 9ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Bernard Stirn</PRESIDENT>
<AVOCATS>SCP RICHARD</AVOCATS>
<RAPPORTEUR>M. Nicolas Labrune</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Edouard Crépey</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2012:325371.20120727</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 18 février 2009 au secrétariat du contentieux du Conseil d'Etat, présentée par la Société France Quick, dont le siège est Immeuble Rostand 22, avenue des Nations, ZAC Paris Nord II à Villepinte (93420), représentée par son président directeur général en exercice ; la Société France Quick demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler la décision du 16 décembre 2008 par laquelle la Commission d'accès aux documents administratifs l'a condamnée au paiement d'une amende de 50 000 euros pour réutilisation et dénaturation d'informations publiques, ainsi qu'à la publication de cette sanction dans les journaux ou magazines ayant servi de support à sa campagne publicitaire ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la loi n° 78-753 du 17 juillet 1978 ;<br/>
<br/>
              Vu le décret n° 2005-1755 du 30 décembre 2005 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Nicolas Labrune, Auditeur,<br/>
<br/>
              - les observations de la SCP Richard, avocat de la Société France Quick,<br/>
<br/>
              - les conclusions de M. Edouard Crépey, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Richard, avocat de la Société France Quick ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article 12 de la loi du 17 janvier 1978 : " Sauf accord de l'administration, la réutilisation des informations publiques est soumise à la condition que ces dernières ne soient pas altérées, que leur sens ne soit pas dénaturé et que leurs sources et la date de leur dernière mise à jour soient mentionnées. " ; qu'en vertu de l'article 18 de la même loi, la Commission d'accès aux documents administratifs (CADA) peut infliger à toute personne qui a réutilisé des informations publiques en méconnaissance des dispositions précitées de l'article 12 une amende ; que, lorsque les informations publiques ont été réutilisées à des fins commerciales, le montant de cette amende, qui ne peut excéder 150 000 euros lorsqu'il s'agit de sanctionner un premier manquement, doit être proportionné à la gravité du manquement commis et aux avantages tirés de ce manquement ; que la CADA peut, à la place ou en sus de l'amende, interdire à l'auteur d'une infraction la réutilisation d'informations publiques pendant une durée, qui ne peut excéder deux ans lorsqu'il s'agit de sanctionner un premier manquement ; qu'enfin la CADA " peut également ordonner la publication de la sanction aux frais de celui qui en est l'objet selon des modalités fixées par décret en Conseil d'Etat. " ;<br/>
<br/>
              2. Considérant que la Société France Quick demande l'annulation de la décision du 16 décembre 2008 par laquelle la CADA, ayant constaté qu'elle avait utilisé à l'occasion de deux campagnes publicitaires des données publiques provenant de l'Agence française de sécurité sanitaire des aliments en violation des dispositions de la loi du 17 juillet 1978, l'a condamnée, en application des dispositions de l'article 18 de cette même loi, d'une part, au paiement d'une amende de 50 000 euros et, d'autre part, à la publication de cette sanction dans les journaux ou magazines ayant servi de support à ses actions publicitaires ;<br/>
<br/>
              3. Considérant, en premier lieu, que ni les dispositions de la loi du 17 janvier 1978, ni celles de son décret d'application, ni aucun principe général du droit n'imposent à la CADA d'adresser une mise en demeure à la personne qui a réutilisé des informations publiques en violation des prescriptions de la loi préalablement au prononcé de la sanction qu'elles prévoient ; que, par suite, la Société France Quick n'est pas fondée à soutenir que la sanction que lui a infligée la CADA ne pouvait régulièrement intervenir sans mise en demeure préalable ;<br/>
<br/>
              4. Considérant, en deuxième lieu, qu'il résulte de l'instruction et qu'il n'est pas contesté que la Société France Quick a réutilisé les données d'un " tableau synthétique des apports nutritionnels conseillés en acides gras chez l'homme adulte ", publié en annexe d'un avis de l'Agence française de sécurité sanitaire des aliments, pour vanter, au cours de deux campagnes publicitaires dans la presse, les bienfaits de l'huile de friture servant à la préparation des produits servis dans ses établissements ; qu'elle faisait valoir dans cette publicité que " sa teneur en acides gras saturés est de 7,5 %, soit trois fois moins que le taux recommandé par l'Agence française de sécurité sanitaire (24 %) " ; que pourtant l'Agence française de sécurité sanitaire des aliments n'a jamais fait de recommandations relatives à la teneur en acides gras saturés des huiles de friture ; que le tableau précité, publié en annexe de l'un de ses avis, indiquait uniquement que les apports nutritionnels conseillés en acides gras sont, en ce qui concerne les acides gras saturés, de 8 % de l'apport énergétique total et que ce chiffre ne concernait que l'homme adulte, ce que la publicité omettait de préciser ; qu'en imposant à la Société France Quick, qui ne conteste pas avoir ainsi gravement dénaturé, tant en ce qui concerne les chiffres que leur objet et leur source, les données publiées par l'Agence française de sécurité sanitaire des aliments, afin d'induire en erreur les consommateurs sur les effets diététiques de ses produits, la publication de la sanction dans les journaux ou magazines qui ont servi de support à la campagne publicitaire au cours de laquelle elle a ainsi réutilisé ces informations publiques, la CADA n'a pas prononcé une sanction disproportionnée aux manquements qui viennent d'être décrits ci-dessus, quelles que soient, dans les circonstances de l'espèce, les diligences, au demeurant incomplètes et imparfaites, que la requérante aurait effectuées pour y mettre fin ;<br/>
<br/>
              5. Considérant, en troisième lieu, qu'aux termes de l'article 26 du décret du 30 décembre 2005 : " Lorsque la commission décide de faire publier la sanction qu'elle prononce, elle en détermine les modalités dans sa décision en fixant le délai de publication et en désignant le support de celle-ci. La publication intervient dans un délai maximum d'un mois à compter du jour où la décision est devenue définitive. Elle est proportionnée à la gravité de la sanction prononcée et adaptée à la situation de l'auteur de l'infraction. " ; que, d'une part, en précisant que la commission devait désigner le support de la publication de la sanction qu'elle prononçait, les dispositions précitées n'ont pas pour objet et ne sauraient avoir pour effet de limiter cette publication à un seul support ; que, d'autre part, en faisant obligation à la Société France Quick de justifier de l'exécution de la publication de la sanction " dans les trois mois suivant la notification de la présente décision ", la CADA n'a pas entendu imposer cette publication avant que sa décision ne fût devenue définitive, en méconnaissance des dispositions précitées de l'article 26 du décret du 30 décembre 2005, mais a seulement ordonné la production des éléments justificatifs de la publication de sa décision au cas où, n'ayant pas fait l'objet d'un recours contentieux, celle-ci serait devenue définitive à l'expiration du délai de recours contentieux ;<br/>
<br/>
              6. Considérant qu'il résulte de l'ensemble de ce qui précède que la Société France Quick n'est pas fondée à demander l'annulation de la sanction prononcée à son encontre par la CADA le 16 décembre 2008 ; que sa requête ne peut, par suite, qu'être rejetée y compris, par voie de conséquence, ses conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>				D E C I D E  :<br/>
              				--------------<br/>
<br/>
Article 1er : La requête de la Société France Quick est rejetée.<br/>
Article 2 : La présente décision sera notifiée à la Société France Quick, à la Commission d'accès aux documents administratifs, à l'Agence française de sécurité sanitaire des aliments, au ministre des affaires sociales et de la santé, au ministre de l'agriculture, de l'agroalimentaire et de la forêt et au ministre du redressement productif.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">26-06-01 DROITS CIVILS ET INDIVIDUELS. ACCÈS AUX DOCUMENTS ADMINISTRATIFS. ACCÈS AUX DOCUMENTS ADMINISTRATIFS AU TITRE DE LA LOI DU 17 JUILLET 1978. - RÉUTILISATION - POUVOIRS DE SANCTION DE LA CADA (ART. 18 DE LA LOI DU 17 JUILLET 1978) - 1) OBLIGATION DE MISE EN DEMEURE PRÉALABLE AVANT LE PRONONCÉ DE LA SANCTION - ABSENCE - 2) PUBLICATION DE LA DÉCISION DE SANCTION SUBORDONNÉE À SON CARACTÈRE DÉFINITIF (ART. 26 DU DÉCRET DU 30 DÉCEMBRE 2005).
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">59-02-02-02 RÉPRESSION. DOMAINE DE LA RÉPRESSION ADMINISTRATIVE RÉGIME DE LA SANCTION ADMINISTRATIVE. LÉGALITÉ EXTERNE. - RÉUTILISATION - POUVOIRS DE SANCTION DE LA CADA (ART. 18 DE LA LOI DU 17 JUILLET 1978) - 1) OBLIGATION DE MISE EN DEMEURE PRÉALABLE AVANT LE PRONONCÉ DE LA SANCTION - ABSENCE - 2) PUBLICATION DE LA DÉCISION DE SANCTION SUBORDONNÉE À SON CARACTÈRE DÉFINITIF (ART. 26 DU DÉCRET DU 30 DÉCEMBRE 2005).
</SCT>
<ANA ID="9A"> 26-06-01 En vertu de l'article 18 de la loi n° 78-753 du 17 juillet 1978, la Commission d'accès aux documents administratifs (CADA) peut infliger une amende à toute personne qui a réutilisé des informations publiques en méconnaissance des dispositions de l'article 12....  ...1) Ni les dispositions de la loi du 17 juillet 1978, ni celles de son décret d'application, ni aucun principe général du droit n'imposent à la CADA d'adresser une mise en demeure à la personne qui a réutilisé des informations publiques en violation des prescriptions de la loi préalablement au prononcé de la sanction qu'elles prévoient.,,2) Pour l'application de l'article 26 du décret n° 2005-1755 du 30 décembre 2005, la CADA ne peut ordonner la publication de sa décision qu'une fois qu'elle est devenue définitive à l'expiration du délai de recours contentieux.</ANA>
<ANA ID="9B"> 59-02-02-02 En vertu de l'article 18 de la loi n° 78-753 du 17 juillet 1978, la Commission d'accès aux documents administratifs (CADA) peut infliger une amende à toute personne qui a réutilisé des informations publiques en méconnaissance des dispositions de l'article 12....  ...1) Ni les dispositions de la loi du 17 juillet 1978, ni celles de son décret d'application, ni aucun principe général du droit n'imposent à la CADA d'adresser une mise en demeure à la personne qui a réutilisé des informations publiques en violation des prescriptions de la loi préalablement au prononcé de la sanction qu'elles prévoient.,,2) Pour l'application de l'article 26 du décret n° 2005-1755 du 30 décembre 2005, la CADA ne peut ordonner la publication de sa décision qu'une fois qu'elle est devenue définitive à l'expiration du délai de recours contentieux.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
