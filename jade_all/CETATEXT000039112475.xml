<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039112475</ID>
<ANCIEN_ID>JG_L_2019_09_000000433896</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/11/24/CETATEXT000039112475.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 03/09/2019, 433896, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-09-03</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>433896</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FABIANI, LUC-THALER, PINATEL</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2019:433896.20190903</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              M. A... B... et Mme D... C... ont demandé au juge des référés du tribunal administratif de Montreuil, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, d'enjoindre à l'Office français de l'immigration et de l'intégration (OFII) de leur rétablir le bénéfice des conditions matérielles d'accueil et de procéder au versement de l'allocation pour demandeur d'asile, dans un délai de quarante-huit heures, sous astreinte de<br/>
100 euros par jour de retard. Par une ordonnance n° 1908932 du 16 août 2019, le juge des référés du tribunal administratif de Montreuil a rejeté leur demande. <br/>
              Par une requête et un mémoire en réplique, enregistrés les 26 et 29 août 2019 au secrétariat du contentieux du Conseil d'Etat, M. B... et Mme C... demandent au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative :<br/>
<br/>
              1°) de les admettre au bénéfice de l'aide juridictionnelle provisoire ;<br/>
<br/>
              2°) d'annuler cette ordonnance ;<br/>
<br/>
              3°) d'enjoindre à l'OFII de leur rétablir, ainsi qu'à leurs deux enfants, les conditions matérielles d'accueil et de procéder au versement de l'allocation pour demandeur d'asile, dans un délai de quarante-huit heures et sous astreinte de 100 euros par jour de retard ;<br/>
<br/>
              4°) de mettre à la charge de l'OFII la somme de 1 500 euros au bénéfice de leur conseil au titre des dispositions de l'article 37 de la loi du 10 juillet 1991 et de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
              Ils soutiennent que : <br/>
              - la condition d'urgence est remplie dès lors que la privation des conditions matérielles d'accueil les place dans une situation d'extrême précarité ;<br/>
              - la décision de l'OFII porte une atteinte grave et manifestement illégale à leur droit d'asile et à leur droit d'accès à des conditions matérielles d'accueil décentes ; <br/>
              - elle est entachée d'une erreur de droit dès lors que l'OFII ne pouvait mettre fin aux conditions matérielles d'accueil, en se fondant sur le 3° de l'article L. 744-8 du code de l'entrée et du séjour des étrangers et du droit d'asile, dès lors qu'elles leur avaient été accordées ; <br/>
              - elle est entachée d'une erreur manifeste d'appréciation dès lors que l'OFII n'a pas pris en compte la vulnérabilité des requérants et de leurs deux enfants mineurs ;<br/>
              - l'OFII ne pouvait se fonder sur le 3° de l'article L. 744-8 du code de l'entrée et du séjour des étrangers et du droit d'asile dès lors que le délai pris pour présenter leur demande d'asile ne leur était pas imputable.<br/>
              Par un mémoire en défense, enregistré le 29 août 2019 au secrétariat du contentieux du Conseil d'Etat, l'Office français de l'immigration et de l'intégration conclut au rejet de la requête. Il soutient que la condition d'urgence n'est pas remplie et que les moyens invoqués ne sont pas fondés.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la directive n° 2013/33/UE du 26 juin 2013 du Parlement européen et du Conseil ;<br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, M. B... et Mme C... et, d'autre part, le directeur de l'Office français de l'immigration et de l'intégration ;<br/>
<br/>
              Vu le procès-verbal de l'audience publique du 30 août 2019 à 10 heures au cours de laquelle ont été entendus : <br/>
<br/>
              - Me Pinatel, avocat au Conseil d'Etat et à la Cour de cassation, avocat de M. B... et Mme C... ;<br/>
<br/>
- le représentant de M. B... et de Mme C... ;<br/>
<br/>
- M. B... ;<br/>
<br/>
              - les représentants de l'Office français de l'immigration et de l'intégration ;<br/>
<br/>
              et à l'issue de laquelle le juge des référés a clos l'instruction.<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
              1. Aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. Le juge des référés se prononce dans un délai de quarante-huit heures ". <br/>
<br/>
              2. Il résulte de l'instruction diligentée par le juge des référés du tribunal administratif de Montreuil que M. B... et Mme C..., de nationalité syrienne, se sont vu proposer et ont accepté le bénéfice des conditions matérielles d'accueil le 25 octobre 2018, date à laquelle leurs demandes d'asile respectives ont été enregistrées par la préfecture de<br/>
Seine-Saint-Denis, en procédure dite " Dublin ". Les intéressés ont, ensuite, perçu l'allocation pour demandeur d'asile au titre des mois d'octobre et novembre 2018. Toutefois, après la requalification, par la préfecture de Seine-Saint-Denis, de leur demande d'asile en procédure dite " accélérée ", en application du 3° du III de l'article L. 723-2 du code de l'entrée et du séjour des étrangers et du droit d'asile, au motif que leur demande d'asile n'avait pas été présentée, sans motif légitime, dans le délai de cent vingt jours à compter de leur entrée en France, le directeur territorial de l'Office français de l'immigration et de l'intégration (OFII) leur a notifié, le 11 décembre 2018, son intention de leur refuser des conditions matérielles d'accueil en application du 3° de l'article L. 744-8 du même code, puis a pris une décision en ce sens le<br/>
20 mars 2019. Après avoir formé un recours administratif préalable contre cette décision, le<br/>
17 mai 2019, qui a été implicitement rejeté, les intéressés ont demandé, le 14 août 2019, au juge des référés du tribunal administratif de Montreuil d'enjoindre à l'OFII, sur le fondement de l'article L. 521-2 du code de justice administrative, de leur rétablir le bénéfice des conditions matérielles d'accueil et de procéder au versement de l'allocation pour demandeur d'asile. Ils relèvent appel de l'ordonnance du 16 août 2019, par laquelle leur demande a été rejetée.<br/>
<br/>
              Sur les dispositions applicables :<br/>
<br/>
              3. Aux termes du premier alinéa de l'article L744-1 du code de l'entrée et du séjour des étrangers et du droit d'asile : " Les conditions matérielles d'accueil du demandeur d'asile, au sens de la directive 2013/33/UE du Parlement européen et du Conseil, du 26 juin 2013, établissant des normes pour l'accueil des personnes demandant la protection internationale, sont proposées à chaque demandeur d'asile par l'Office français de l'immigration et de l'intégration après l'enregistrement de la demande d'asile par l'autorité administrative compétente, en application du présent chapitre. Les conditions matérielles d'accueil comprennent les prestations et l'allocation prévues au présent chapitre ". Aux termes de l'article L. 744-8 du même code, dans sa rédaction résultant de la loi du 29 juillet 2015 relative à la réforme du droit d'asile applicable au litige : " Le bénéfice des conditions matérielles d'accueil peut être : / (...) /  3° Refusé si le demandeur présente une demande de réexamen de sa demande d'asile ou s'il n'a pas sollicité l'asile, sans motif légitime, dans le délai prévu au 3° du III de l'article L. 723-2 / (...) / La décision de suspension, de retrait ou de refus des conditions matérielles d'accueil est écrite et motivée. Elle prend en compte la vulnérabilité du demandeur. (...) ". Aux termes du second alinéa de l'article L. 744-9 du même code, relatif à l'allocation de demandeur d'asile : " Pour les personnes qui obtiennent la qualité de réfugié prévue à l'article L. 711-1 ou le bénéfice de la protection subsidiaire prévue à l'article L. 712-1, le bénéfice de l'allocation prend fin au terme du mois qui suit celui de la notification de la décision ". <br/>
<br/>
              Sur l'office du juge des référés :<br/>
<br/>
              4. D'une part, les dispositions de l'article L. 521-2 du code de justice administrative confèrent au juge administratif des référés le pouvoir d'ordonner toute mesure dans le but de faire cesser une atteinte grave et manifestement illégale portée à une liberté fondamentale par une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public. Il résulte tant des termes de cet article que du but dans lequel la procédure qu'il instaure a été créée que doit exister un rapport direct entre l'illégalité relevée à l'encontre de l'autorité administrative et la gravité de ses effets au regard de l'exercice de la liberté fondamentale en cause.<br/>
<br/>
              5. D'autre part, si la privation du bénéfice des mesures prévues par la loi afin de garantir aux demandeurs d'asile des conditions matérielles d'accueil décentes, jusqu'à ce qu'il ait été statué sur leur demande, est susceptible de constituer une atteinte grave et manifestement illégale à la liberté fondamentale que constitue le droit d'asile, le caractère grave et manifestement illégal d'une telle atteinte s'apprécie en tenant compte des moyens dont dispose l'autorité administrative compétente et de la situation du demandeur. Ainsi, le juge des référés ne peut faire usage des pouvoirs qu'il tient de l'article L. 521-2 du code de justice administrative en adressant une injonction à l'administration que dans le cas où, d'une part, le comportement de celle-ci fait apparaître une méconnaissance manifeste des exigences qui découlent du droit d'asile et où, d'autre part, il résulte de ce comportement des conséquences graves pour le demandeur d'asile, compte tenu notamment de son âge, de son état de santé ou de sa situation familiale. Il incombe au juge des référés d'apprécier, dans chaque situation, les diligences accomplies par l'administration en tenant compte des moyens dont elle dispose ainsi que de l'âge, de l'état de santé et de la situation familiale de la personne intéressée. <br/>
<br/>
              Sur l'urgence :<br/>
<br/>
              6. Il résulte de l'instruction diligentée par le juge des référé du Conseil d'Etat, et notamment de l'audience publique, que si, ainsi que le soutient l'OFII, M. B... et<br/>
Mme C... se sont vu accorder le bénéfice de la protection subsidiaire par une décision du directeur général de l'office français de protection des réfugiés et apatrides du 19 août 2019, s'ils pourront ainsi prochainement avoir accès au marché de l'emploi ainsi qu'à diverses prestations sociales et familiales et ne pourront, en revanche, plus bénéficier de l'allocation de demandeur d'asile, en application du deuxième alinéa de l'article L. 744-9 du code de l'entrée et du séjour des étrangers et du droit d'asile, au terme du mois suivant la notification de la décision précitée, il n'est pas contesté qu'à ce jour, cette notification n'est pas intervenue et que les intéressés, qui ont deux enfants de trois et cinq ans, n'ont ni hébergement ni ressource. Dans ces conditions et nonobstant le délai mis à présenter leur demande de référé, ils sont fondés à soutenir que la condition d'urgence prévue à l'article L. 521-2 du code de justice administrative est remplie.<br/>
<br/>
              Sur l'atteinte grave et manifestement illégale à une liberté fondamentale :<br/>
<br/>
              7. Il est constant que si M. B... et Mme C... sont entrés en France le<br/>
1er juin 2018 et si le délai prévu par le 3° du III de l'article L. 723-2 du code de l'entrée et du séjour des étrangers et du droit d'asile expirait ainsi, en principe, à la fin du mois de septembre 2018, cette période a été marquée, d'une part, par un engorgement très important des services chargés de l'enregistrement des demande d'asile, en particulier en région parisienne où a été mise en place une plateforme téléphonique qui a changé de numéro en août et, d'autre part, par l'indisponibilité pour cause de congés d'été d'un certain nombre d'intervenants dont fait partie l'association qui accompagnait les intéressés. Dans ces conditions et eu égard à la vulnérabilité de la famille tenant à la présence de deux enfants de trois et cinq ans, l'OFII a porté une atteinte manifestement illégale au droit d'asile des requérants, en les privant totalement des conditions matérielles d'accueil, au motif que leur demande d'asile avait été présentée le 25 octobre 2018.<br/>
<br/>
              8. S'agissant de l'allocation pour demandeur d'asile, la privation manifestement illégale de cette allocation, alors que les intéressés sont dépourvus de ressources, est de nature à porter une atteinte grave au droit d'asile, qui justifie qu'il soit enjoint à l'OFII de rétablir, pour l'avenir, le versement de cette allocation, dans un délai de quarante-huit heures à compter de la présente ordonnance. Il n'y a pas lieu, en revanche, de prononcer d'astreinte.<br/>
<br/>
              9. S'agissant du bénéfice d'un hébergement, M. B... et Mme C..., qui n'ont pas présenté de conclusions expresses aux fins d'injonction en ce sens et à qui la protection subsidiaire ouvre des droits nouveaux, ne contestent pas les indications de l'OFII selon lesquelles 43 familles de demandeurs d'asile avec deux enfants sont actuellement en attente de place dans le seul département de Seine-Saint-Denis. Il n'y a, dès lors et en tout état de cause, pas lieu d'ordonner de mesure au titre de la procédure engagée. <br/>
<br/>
              10. Il résulte de tout ce qui précède, sans qu'il soit besoin de se prononcer sur l'autre moyen de leur requête, que M. B... et Mme C... sont fondés à soutenir que c'est à tort que, par l'ordonnance attaquée, le juge des référés du tribunal administratif de Montreuil a rejeté leur demande tendant à ce qu'il soit enjoint à l'OFII de leur rétablir le versement de l'allocation pour demandeur d'asile, dans un délai de quarante-huit heures à compter de la présente ordonnance. <br/>
<br/>
              11. Il y a lieu, dans les circonstances de l'espèce, et sans qu'il soit besoin d'admettre M. B... et Mme C... au bénéfice de l'aide juridictionnelle provisoire, de mettre à la charge de l'OFII et au profit de ces derniers le versement d'une somme de<br/>
1 500 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : L'ordonnance n° 1908932 du 16 août 2019 du juge des référés du tribunal administratif de Montreuil est annulée.<br/>
Article 2 : Il est enjoint à l'Office français de l'immigration et de l'intégration de rétablir à M. B... et Mme C... le versement de l'allocation pour demandeur d'asile, dans un délai de quarante-huit heures à compter de la présente ordonnance. <br/>
Article 3 : L'Office français de l'immigration et de l'intégration versera la somme de<br/>
1 500 euros à M. B... et Mme C..., en application de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Le surplus des conclusions de la requête est rejeté.<br/>
Article 5 : La présente ordonnance sera notifiée à M. A... B..., à Mme D... C... et à l'Office français de l'immigration et de l'intégration.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
