<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037158688</ID>
<ANCIEN_ID>JG_L_2018_07_000000393194</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/15/86/CETATEXT000037158688.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème et 1ère chambres réunies, 04/07/2018, 393194, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-07-04</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>393194</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème et 1ère chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Françoise Tomé</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Dieu</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:393194.20180704</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>1° Sous le n° 393194, par une requête et un mémoire en réplique, enregistrés le 4 septembre 2015 et le 8 février 2016 au secrétariat du contentieux du Conseil d'Etat, Mme D... A...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la délibération du 16 avril 2015 par laquelle le conseil d'administration du Muséum national d'histoire naturelle a fixé la composition du comité de sélection chargé d'examiner les candidatures au poste de professeur de cosmochimie n° 4119 ; <br/>
<br/>
              2°) d'annuler la délibération du 10 juillet 2015 de ce même conseil d'administration fixant la liste des candidats, classés par ordre de préférence, qu'il retenait pour ce poste ;<br/>
<br/>
              3°) d'enjoindre au Muséum national d'histoire naturelle de reprendre les opérations de concours et de désigner un nouveau comité de sélection comportant au minimum 40 % de femmes.<br/>
<br/>
<br/>
              2° Sous le n° 398085, par une requête et un mémoire en réplique, enregistrés les 18 mars et 7 septembre 2016 au secrétariat du contentieux du Conseil d'Etat, Mme A...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret du Président de la République du 21 janvier 2016 en tant qu'il nomme et titularise M. C... B... dans le corps des professeurs du Muséum national d'histoire naturelle sur le poste n° 4119 ; <br/>
<br/>
              2°) d'annuler pour excès de pouvoir la délibération du 16 avril 2015 par laquelle le conseil d'administration du Muséum national d'histoire naturelle a fixé la composition du comité de sélection chargé d'examiner les candidatures au poste de professeur de cosmochimie n° 4119 ; <br/>
<br/>
              3°) d'annuler la délibération du 10 juillet 2015 de ce même conseil d'administration fixant la liste des candidats, classés par ordre de préférence, qu'il retenait pour ce poste ;<br/>
<br/>
              4°) d'enjoindre au Muséum national d'histoire naturelle de reprendre les opérations de concours et de désigner un nouveau comité de sélection comportant au minimum quarante pour cent de femmes.<br/>
<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu : <br/>
              - le code de l'éducation ;<br/>
              - la loi n° 2012-347 du 12 mars 2012 ;<br/>
              - la loi n° 2013-660 du 22 juillet 2013 ;<br/>
              - le décret n° 84-431 du 6 juin 1984 ;<br/>
              - le décret n° 92-1178 du 2 novembre 1992 ; <br/>
              - le décret n° 2001-916 du 3 octobre 2001 ;<br/>
              - le décret n° 2014-11007 du 1er octobre 2014 ;<br/>
              - le décret n° 2014-1627 du 26 décembre 2014 ;<br/>
              - le décret n° 2015-455 du 21 avril 2015 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Françoise Tomé, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Frédéric Dieu, rapporteur public ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 22 juin 2018, présentée par MmeA....<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant que les deux requêtes visées ci-dessus, relatives à un même recrutement de professeur au Muséum national d'histoire naturelle, présentent à juger les mêmes questions ; qu'il y a lieu de les joindre pour statuer par une seule décision ;<br/>
<br/>
              2. Considérant que Mme A...s'est portée candidate au concours de recrutement du poste de professeur de cosmochimie n° 4119 au Muséum national d'histoire naturelle ; qu'elle demande l'annulation de la délibération du 16 avril 2015 par laquelle le conseil d'administration de cet établissement a approuvé la composition du comité de sélection constitué pour ce concours, l'annulation de la délibération du 10 juillet 2015 par laquelle ce même conseil d'administration a proposé, en première position, la candidature de M. B...et enfin, par voie de conséquence, l'annulation du décret du Président de la République du 21 janvier 2016 en tant qu'il nomme M. B...à ce poste ;<br/>
<br/>
              Sur la délibération du 16 avril 2015 fixant la composition du comité de sélection :<br/>
<br/>
              3. Considérant qu'aux termes de l'article 10 du décret du 2 novembre 1992 portant statut du corps des professeurs du Muséum national d'histoire naturelle et du corps des maîtres de conférences du Muséum national d'histoire naturelle : " En application des dispositions de l'article L. 952-6-1 du code de l'éducation, un comité de sélection est créé pour pourvoir chaque emploi d'enseignant-chercheur du Muséum national d'histoire naturelle créé ou déclaré vacant (...) / Le comité de sélection est créé par délibération du conseil d'administration siégeant en formation restreinte aux représentants élus des enseignants-chercheurs du Muséum national d'histoire naturelle, des enseignants-chercheurs et personnels assimilés. (...) / Les membres du comité de sélection sont proposés par le président du Muséum d'histoire naturelle (...), après avis du conseil scientifique. (...) / Le conseil d'administration en formation restreinte statue par un vote sur la liste des noms qui lui sont proposés par le président du Muséum national d'histoire naturelle " ;<br/>
<br/>
              4. Considérant, en premier lieu, que Mme A...soutient que la composition du comité de sélection constitué pour le concours litigieux, qui ne comportait que deux femmes parmi ses huit membres, était illégale, faute de respecter les dispositions de l'article 55 de la loi du 12 mars 2012 relative à l'accès à l'emploi titulaire et à l'amélioration des conditions d'emploi des agents contractuels dans la fonction publique, à la lutte contre les discriminations et portant diverses dispositions relatives à la fonction publique, qui dispose : " A compter du 1er janvier 2015, pour la désignation des membres des jurys et des comités de sélection constitués pour le recrutement ou la promotion de fonctionnaires relevant de la fonction publique de l'Etat, (...) l'autorité administrative chargée de l'organisation du concours, de l'examen ou de la sélection respecte une proportion minimale de 40 % de personnes de chaque sexe./ A titre exceptionnel, les statuts particuliers peuvent, compte tenu des contraintes de recrutement et des besoins propres des corps ou cadres d'emplois, fixer des dispositions dérogatoires à la proportion minimale prévue au premier alinéa (...) " ; <br/>
<br/>
              5. Considérant, toutefois, qu'il résulte des dispositions de l'article L. 911-1 du code de l'éducation que les dispositions statutaires de la fonction publique de l'Etat s'appliquent aux membres des corps de fonctionnaires du service public de l'éducation sous réserve des dispositions du livre neuvième de ce code ; qu'à ce titre, l'article L. 952-6-1 de ce code, pour l'application duquel ont été prises les dispositions du décret du 2 novembre 1992 citées au point 3, fixe les règles propres aux comités de sélection composés pour le recrutement des enseignants-chercheurs ; que dans sa rédaction, applicable au litige, issue de la loi du 22 juillet 2013 sur l'enseignement supérieur et la recherche, cet article dispose ainsi, à son deuxième alinéa, que la composition de ces comités doit concourir " (...) à une représentation équilibrée entre les femmes et les hommes lorsque la répartition entre les sexes des enseignants de la discipline le permet (...) " ; que cette règle de représentation équilibrée déroge, par suite, à la règle statutaire fixée, pour l'ensemble de la fonction publique de l'Etat, par les dispositions citées ci-dessus de l'article 55 de la loi du 12 mars 2012, lesquelles ne sont, par suite, pas applicables au concours litigieux ; qu'ainsi MmeA..., qui ne conteste pas la composition du comité de sélection au regard des dispositions de l'article L. 952-6-1 du code de l'éducation, ne peut utilement soutenir que les dispositions de l'article 55 de la loi du 12 mars 2012 ont été méconnues ;<br/>
<br/>
              6. Considérant, par ailleurs, que le décret du 21 avril 2015 " fixant des dispositions dérogatoires à la proportion de quarante pour cent de chaque sexe dans la composition des comités de sélection des concours de recrutement des professeurs des universités " n'est pas applicable aux enseignants-chercheurs du Muséum national d'histoire naturelle, lesquels sont exclusivement régis par les dispositions du décret du 2 novembre 1992, mentionné ci-dessus et pris sur le même fondement de l'article L. 952-6-1 du code de l'éducation ; que la composition du comité de sélection litigieux n'a, par suite, pas été prise en application de ce décret du 21 avril 2015 ; que Mme A...ne saurait ainsi utilement en invoquer l'illégalité par voie d'exception ;<br/>
<br/>
              7. Considérant, en second lieu, que Mme A...soutient que l'avis du conseil scientifique du Muséum national d'histoire naturelle sur la composition du comité de sélection, prévu par les dispositions citées ci-dessus de l'article 10 du décret du 2 novembre 1992, est entaché d'irrégularité en ce qu'il résulte d'une consultation des membres de ce conseil effectuée par voie électronique ; <br/>
<br/>
              8. Considérant qu'il résulte des termes mêmes de l'article 1er du décret du 26 décembre 2014 relatif aux modalités d'organisation des délibérations à distance des instances administratives à caractère collégial que ce texte ne s'applique que sous réserve de dispositions propres aux organismes en cause ; que, par suite, contrairement à ce que soutient en défense la ministre de l'éducation nationale, de l'enseignement supérieur et de la recherche, les modalités de délibération par voie électronique des instances du Muséum national d'histoire naturelle sont exclusivement régies par les dispositions particulières de l'article 27-1 du décret du 3 octobre 2001 relatif au Muséum national d'histoire naturelle qui, dans sa rédaction applicable à l'espèce, dispose que " (...) le règlement intérieur du Muséum précise notamment les règles de quorum, les modalités de délibérations des conseils et de représentation de leurs membres, les modalités de convocation, d'établissement et d'envoi de l'ordre du jour des conseils (...) / Il peut en outre prévoir que les membres des conseils participent aux séances par des moyens de visioconférence ou de communication électronique permettant l'identification de leurs membres et leur participation effective à une délibération collégiale et satisfaisant à des caractéristiques techniques garantissant la transmission continue et simultanée des débats et la confidentialité des votes lorsque le scrutin est secret (...) / Il peut également prévoir, pour les matières qu'il définit et en cas d'urgence avérée, les conditions dans lesquelles la délibération est prise après consultation écrite des membres, y compris par voie électronique (...) " ;<br/>
<br/>
              9. Considérant que le règlement intérieur du Muséum national d'histoire naturelle, dans sa rédaction en vigueur aux dates auxquelles le conseil scientifique a été consulté sur la composition du comité de sélection, ne comportait aucune disposition relative à la délibération de ce conseil par voie de communication électronique ; que si, comme le soutient en défense l'établissement, les dispositions du règlement intérieur du conseil scientifique permettaient un tel mode de consultation de ses membres, de telles dispositions ne pouvaient compétemment se substituer aux règles qu'il appartenait au seul règlement intérieur du Muséum national d'histoire naturelle de fixer ; que Mme A...est, par suite, fondée à soutenir que l'avis émis au terme du recueil, par voie électronique, de la position individuelle des différents membres du conseil scientifique, a été rendu en méconnaissance des dispositions citées ci-dessus de l'article 27-1 du décret du 3 octobre 2001 relatif au Muséum national d'histoire naturelle ; <br/>
<br/>
              10. Considérant, toutefois, que si les actes administratifs doivent être pris selon les formes et conformément aux procédures prévues par les lois et règlements, un vice affectant le déroulement d'une procédure administrative préalable, suivie à titre obligatoire ou facultatif, n'est de nature à entacher d'illégalité la décision prise que s'il ressort des pièces du dossier qu'il a été susceptible d'exercer, en l'espèce, une influence sur le sens de la décision prise ou qu'il a privé les intéressés d'une garantie ; qu'en l'espèce, il ressort des pièces du dossier que les modalités retenues par le conseil scientifique du Muséum national d'histoire naturelle pour rendre son avis sur la composition du comité de sélection, qui n'ont privé la requérante d'aucune garantie, n'ont pas eu d'incidence sur le sens de cet avis et, a fortiori, sur celui de la délibération litigieuse ; que, par suite, MmeA..., qui conteste seulement le principe du recours à une consultation par voie électronique, n'est pas fondée à soutenir que le vice ayant affecté cet avis entacherait d'irrégularité la délibération du 16 avril 2015 du conseil d'administration du Muséum national d'histoire naturelle ;<br/>
<br/>
              11. Considérant, en troisième lieu, qu'il ressort des pièces du dossier que, ainsi que le soutient MmeA..., les membres du comité de sélection ont été proposés au conseil d'administration du Muséum national d'histoire naturelle par son directeur général, et non par le président de cet établissement comme le prévoient pourtant les dispositions, citées au point 3, de l'article 10 du décret du 2 novembre 1992, dans sa rédaction issue du décret du 1er octobre 2014 modifiant le décret n° 2001-916 du 3 octobre 2001 relatif au Muséum national d'histoire naturelle ; que toutefois, à la date de cette proposition, étaient également applicables les dispositions transitoires de l'article 30 de ce dernier décret, aux termes desquelles : " Pour la période courant de la date de publication du présent décret à celle de la nomination du président du Muséum, la gestion de l'établissement est assurée par le directeur général en fonctions (...) " ; que ces dispositions conféraient par suite compétence au directeur général du Muséum pour proposer au conseil d'administration, ainsi que le prévoyait d'ailleurs le décret du 2 novembre 1992 dans sa rédaction antérieure au décret du 1er octobre 2014, la composition du comité de sélection ;<br/>
<br/>
              Sur la délibération du 10 juillet 2015 ayant statué sur la liste des candidats retenus par le comité de sélection :<br/>
<br/>
              12. Considérant, en premier lieu, qu'aux termes de l'article 10-1 du décret, déjà mentionné ci-dessus, du 2 novembre 1992 : " Le comité de sélection examine les dossiers des candidats au détachement, à l'intégration directe et au recrutement par concours. Au vu de rapports pour chaque candidat présentés par deux de ses membres, le comité établit la liste des candidats qu'il souhaite entendre (...) / Le président du comité de sélection convoque les candidats et fixe l'ordre du jour de la réunion (...) " ; qu'il ressort des pièces du dossier que la réunion au cours de laquelle le comité de sélection a décidé de la liste des candidats à auditionner et celle au cours de laquelle il a procédé aux auditions et établi la liste de candidats soumise, pour sa délibération litigieuse, au conseil d'administration, se sont tenues, séparément et successivement, au cours de la même journée du 17 juin 2015 ; que, par suite, le moyen par lequel Mme A...se borne à soutenir que la procédure est irrégulière au motif que le comité de sélection n'a tenu qu'une unique réunion manque en fait ;<br/>
<br/>
              13. Considérant, en deuxième lieu, qu'aux termes de l'article 8 du décret du 3 octobre 2001 : " Lorsque le président du Muséum ne peut présider une séance du conseil d'administration, celui-ci élit en son sein un président de séance à la majorité des membres présents ou représentés " ; que Mme A...n'est, par suite, pas fondée à soutenir qu'en l'absence du président du Muséum, la séance du 10 juillet 2015 du conseil d'administration aurait dû être présidée par son doyen d'âge ;<br/>
<br/>
              14. Considérant, enfin, qu'il ressort des pièces du dossier qu'au cours de la séance du 11 juillet 2015 étaient présents trois membres de l'administration du Muséum national d'histoire naturelle qui n'étaient pas membres du conseil d'administration et dont la présence n'est prévue ni par le décret du 3 octobre 2001 relatif au Muséum national d'histoire naturelle ni par le règlement intérieur alors applicable ; qu'il ressort toutefois des pièces du dossier que ces trois personnes, qui appartenaient aux services administratifs concernés par les points à l'ordre du jour, se sont bornées à intervenir en réponse aux questions posées par les membres du conseil d'administration, n'ont pas pris part aux débats sur les mérites des candidats et n'ont pas pris part aux votes ; que, dès lors, leur présence, qui, contrairement à ce que soutient MmeA..., ne portait pas par elle-même atteinte au principe d'indépendance des enseignants-chercheurs, n'a, dans les circonstances de l'espèce, pas eu d'influence sur le sens de la délibération litigieuse ; qu'elle ne l'a, par suite, pas entachée d'irrégularité ;<br/>
<br/>
              15. Considérant qu'il résulte de tout ce qui précède que Mme A... n'est pas fondée à demander l'annulation des délibérations des 16 avril 2015 et 10 juillet 2015 du conseil d'administration du Muséum national d'histoire naturelle ; que, par suite, les conclusions par lesquelles elle demande, par voie de conséquence, l'annulation du décret du 21 janvier 2016 du Président de la République en tant qu'il nomme M.B..., ne peuvent qu'être également rejetées ; qu'il en va de même de ses conclusions à fin d'injonction ; <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Les requêtes n° 393194 et n° 398085 de Mme A...sont rejetées.<br/>
Article 2 : La présente décision sera notifiée à Mme D...A..., au Muséum national d'histoire naturelle, à M. C...B..., à la ministre de l'enseignement supérieur, de la recherche et de l'innovation et au Premier ministre. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
