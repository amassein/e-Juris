<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043215963</ID>
<ANCIEN_ID>JG_L_2021_02_000000449214</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/21/59/CETATEXT000043215963.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 23/02/2021, 449214, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-02-23</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>449214</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2021:449214.20210223</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B... A... a demandé au juge des référés du tribunal administratif de Cergy-Pontoise, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, en premier lieu, de déclarer responsable l'ordre des avocats au barreau des Hauts-de-Seine à raison de la décision de son bâtonnier de relever de sa commission d'office Me C..., en deuxième lieu, d'annuler la décision de relève de Me C... prononcée par ce bâtonnier, en troisième lieu, d'enjoindre à ce bâtonnier de confirmer la désignation d'office de Me C... dans la défense de ses intérêts devant la cour d'appel de Versailles et le tribunal judiciaire de Nanterre, dans des délais compatibles avec la tenue de l'audience prévue le 18 février 2021 et, en dernier lieu, de transmettre au procureur de la République de Paris les faits susceptibles de recevoir la qualification pénale de faux et d'usages de faux en écritures publiques ainsi que de tentatives d'escroqueries au jugement, en application de l'article 40 du code de procédure pénale. Par une ordonnance n° 2101087 du 27 janvier 2021, le juge des référés a rejeté sa requête.<br/>
<br/>
              Par une requête, deux mémoires complémentaires et un mémoire en réplique, enregistrés le 30 janvier et les 12, 15 et 16 février 2021 au secrétariat du contentieux du Conseil d'Etat, M. A... demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative :<br/>
<br/>
              1°) d'annuler l'ordonnance du 27 janvier 2021 du tribunal administratif de Cergy-Pontoise ;<br/>
<br/>
              2°) d'enjoindre au bâtonnier de l'ordre des avocats au barreau des Hauts-de-Seine de confirmer la désignation d'office de Me C... dans la défense de ses intérêts devant la cour d'appel de Versailles et le tribunal judiciaire de Nanterre, dans des délais compatibles avec la tenue de l'audience prévue le 18 février 2021 ;<br/>
<br/>
              3°) de porter à la connaissance du procureur de la République, en application de l'article 40 du code de procédure pénale, les faits susceptibles d'être qualifiés de faux et usage de faux en écritures publiques et de tentatives d'escroqueries au jugement.<br/>
<br/>
<br/>
              Il soutient que :<br/>
              - le Conseil d'Etat est compétent pour connaître de ce litige ;<br/>
              - l'ordonnance du juge des référés du tribunal administratif de Cergy-Pontoise est entachée d'irrégularité en ce qu'elle mentionne à tort la participation à l'audience du bâtonnier de l'ordre des avocats au barreau des Hauts-de-Seine ; <br/>
              - elle méconnaît la portée de sa demande, qui est dirigée contre le refus du bâtonnier de désigner un nouvel avocat à la suite de la relève d'un avocat qu'il avait initialement désigné à sa demande, et non contre une première décision de refus de désignation d'avocat ;<br/>
              - ses conclusions tendant à la mise en oeuvre des dispositions de l'article 40 du code de procédure pénale sont recevables et assorties d'éléments probants quant aux faits invoqués ;<br/>
              - la condition d'urgence est satisfaite ;<br/>
              - la décision contestée, qui revient sans motif sur la décision du 15 juin 2018 commettant d'office Me C..., en méconnaissance tant de l'alinéa 2 de l'article 419 du code de procédure civile que des termes du contrat qui le lie à cette avocate et des principes d'indépendance et de secret professionnel de l'avocat, porte une atteinte grave et manifestement illégale à son droit à un procès équitable.<br/>
<br/>
              En application des dispositions de l'article R. 611-7 du code de justice administrative, les parties ont été informées que la décision du Conseil d'Etat était susceptible d'être fondée sur le moyen, relevé d'office, tiré de l'incompétence de la juridiction administrative pour statuer sur le litige.<br/>
<br/>
              Par un mémoire en défense, enregistré le 16 février 2021 au secrétariat du contentieux du Conseil d'Etat, l'ordre des avocats au barreau des Hauts-de-Seine conclut au rejet de la requête et à ce que la somme de 3 000 euros soit mise à la charge du requérant au titre de l'article L. 761-1 du code de justice administrative. Il soutient que les moyens ne sont pas fondés.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la Constitution, et notamment son préambule ;<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - le code de procédure civile ;<br/>
              - le code de procédure pénale ; <br/>
              - le code de justice administrative et l'ordonnance n° 2020-1402 du 18 novembre 2020 ;<br/>
<br/>
<br/>
              Les parties ont été informées, sur le fondement de l'article 3 de l'ordonnance n° 2020-1402 du 18 novembre 2020 portant adaptation des règles applicables aux juridictions administratives, de ce qu'aucune audience ne se tiendrait et de ce que la clôture de l'instruction serait fixée le 17 février 2021 à 18 heures.<br/>
<br/>
              Vu la note en délibéré, enregistrée le 19 février 2021, présentée par M. A... ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 521-2 du même code : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. (...) ". Le juge administratif des référés ne peut être saisi d'une demande tendant à la mise en oeuvre de l'une des procédures régies par le livre V du code de justice administrative que pour autant que le litige principal auquel se rattache ou est susceptible de se rattacher la mesure d'urgence qu'il lui est demandé de prescrire n'échappe pas manifestement à la compétence de la juridiction administrative.<br/>
<br/>
              2. La demande présentée devant le juge des référés du tribunal administratif de Cergy-Pontoise par M. A..., à la demande duquel le bâtonnier de l'ordre des avocats au barreau des Hauts-de-Seine avait désigné le 15 juin 2018 une avocate pour l'assister dans la défense de ses intérêts devant la cour d'appel de Versailles et le tribunal judiciaire de Nanterre, était dirigée contre la décision par laquelle ce bâtonnier a relevé cette avocate de cette désignation et tendait à ce qu'il lui soit enjoint de désigner à nouveau cette avocate pour l'assister.<br/>
<br/>
              3. Toutefois, il n'appartient pas aux juridictions administratives de connaître du litige né de cette action relative à la désignation d'un avocat par le bâtonnier de l'ordre des avocats. Dès lors, il y a lieu d'annuler l'ordonnance du 27 janvier 2021 par laquelle le juge des référés s'est reconnu compétent pour connaître de la demande de M. A..., et, statuant par voie d'évocation, de rejeter cette demande comme portée devant une juridiction incompétente pour en connaître.<br/>
<br/>
              4. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de M. A... la somme de 2 000 euros, à verser à l'ordre des avocats au barreau des Hauts-de-Seine au titre des dispositions de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
              ------------------<br/>
<br/>
Article 1er : L'ordonnance du 27 janvier 2021 du juge des référés du tribunal de Cergy-Pontoise est annulée.<br/>
Article 2 : La demande présentée par M. A... devant le juge des référés du tribunal administratif de Cergy-Pontoise est rejetée comme portée devant une juridiction incompétente pour en connaître.<br/>
Article 3 : M. A... versera à l'ordre des avocats au barreau des Hauts-de-Seine la somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
Article 4 : La présente ordonnance sera notifiée à M. B... A... et à l'ordre des avocats au barreau des Hauts-de-Seine.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
