<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000023958620</ID>
<ANCIEN_ID>JG_L_2011_05_000000321357</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/23/95/86/CETATEXT000023958620.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème et 8ème sous-sections réunies, 04/05/2011, 321357</TITRE>
<DATE_DEC>2011-05-04</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>321357</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème et 8ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP WAQUET, FARGE, HAZAN ; SCP GASCHIGNARD</AVOCATS>
<RAPPORTEUR>Mme Anne Egerszegi</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Edouard Geffray</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2011:321357.20110504</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 6 octobre 2008 et 6 janvier 2009 au secrétariat du contentieux du Conseil d'Etat, présentés pour la COMMUNE DE FONDETTES (Indre-et-Loire), représentée par son maire ; la COMMUNE DE FONDETTES demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 07NT01961 du 24 juin 2008 par lequel la cour administrative d'appel de Nantes, faisant droit à l'appel de la société civile immobilière (SCI) Chatigny, de M. D... A...et de Mme C...B..., a annulé le jugement du 9 mai 2007 du tribunal administratif de d'Orléans ainsi que l'arrêté du 11 juillet 2005 du maire de Fondettes délivrant à la commune un permis de construire en vue d'aménager une aire d'accueil pour les gens du voyage au lieudit la Prairie d'Islate ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter la requête d'appel de la SCI Chatigny, de M.  A...et de MmeB... ;<br/>
<br/>
              3°) de mettre à la charge de la SCI Chatigny, de M.  A...et de Mme B...la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de l'environnement ;<br/>
<br/>
              Vu le code de l'urbanisme ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Anne Egerszegi, Maître des Requêtes,<br/>
<br/>
              - les observations de la SCP Waquet, Farge, Hazan, avocat de la COMMUNE DE FONDETTES et de la SCP Gaschignard, avocat de la SCI Chatigny, de M. A...et de MmeB...,<br/>
<br/>
              - les conclusions de M. Edouard Geffray, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Waquet, Farge, Hazan, avocat de la COMMUNE DE FONDETTES et à la SCP Gaschignard, avocat de la SCI Chatigny, de M. A...et de MmeB...,; <br/>
<br/>
<br/>
<br/>
              Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, par un arrêté du 11 juillet 2005, le maire de Fondettes (Indre-et-Loire) a délivré à la commune un permis de construire en vue de la réalisation, au lieudit " La Prairie d'Islate ", d'une aire de stationnement pour gens du voyage, d'une superficie de 7 211 m², comportant six bornes sanitaires dont une accessible aux personnes à mobilité réduite, un bâtiment destiné au gardien d'une surface hors oeuvre nette totale de 186 m² ainsi que vingt-quatre emplacements de stationnement ; que, par un jugement du 9 mai 2007, le tribunal administratif d'Orléans a rejeté la demande de la société civile immobilière (SCI) Chatigny et de deux habitants de la commune tendant à l'annulation de cet arrêté ; que la COMMUNE DE FONDETTES se pourvoit en cassation contre l'arrêt du 24 juin 2008 par lequel la cour administrative d'appel de Nantes a annulé le jugement du 9 mai 2007 du tribunal administratif d'Orléans et l'arrêté du 11 juillet 2005 du maire de Fondettes ;<br/>
<br/>
              Sans qu'il soit besoin d'examiner les autres moyens du pourvoi ;<br/>
<br/>
              Considérant qu'aux termes de l'article R. 111-2 du code de l'urbanisme, dans sa rédaction en vigueur à la date de délivrance du permis de construire contesté : " Le permis de construire peut être refusé ou n'être accordé que sous réserve de l'observation de prescriptions spéciales si les constructions, par leur situation ou leurs dimensions, sont de nature à porter atteinte à la salubrité ou à la sécurité publique. Il en est de même si les constructions projetées, par leur implantation à proximité d'autres installations, leurs caractéristiques ou leur situation, sont de nature à porter atteinte à la salubrité ou à la sécurité publique. " ; qu'aux termes de l'article L. 562-4 du code de l'environnement : " Le plan de prévention des risques naturels prévisibles approuvé vaut servitude d'utilité publique. Il est annexé au plan d'occupation des sols, conformément à l'article L. 126-1 du code de l'urbanisme (...) " ; <br/>
<br/>
              Considérant que les prescriptions d'un plan de prévention des risques naturels prévisibles, destinées notamment à assurer la sécurité des personnes et des biens exposés aux risques d'inondation et valant servitude d'utilité publique, s'imposent directement aux autorisations de construire, sans que l'autorité administrative soit tenue de reprendre ces prescriptions dans le cadre de la délivrance du permis de construire ; qu'il incombe toutefois à l'autorité compétente pour délivrer une autorisation d'urbanisme, si les particularités de la situation qu'il lui appartient d'apprécier l'exigent, de préciser dans l'autorisation, le cas échéant, les conditions d'application d'une prescription générale contenue dans le plan ou de subordonner, en application des dispositions précitées de l'article R. 111-2 du code de l'urbanisme, la délivrance du permis de construire sollicité à d'autres prescriptions spéciales, si elles lui apparaissent nécessaires, que celles du plan de prévention des risques naturels prévisibles ;<br/>
<br/>
              Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que les parcelles faisant l'objet du permis de construire contesté sont classées en zone d'aléa fort (A3) par le plan de prévention des risques naturels d'inondation de la Loire, approuvé par arrêté préfectoral du 29 janvier 2001 ; que le règlement de ce plan autorise, en zone A3 d'aléa fort, l'implantation de terrains d'accueil des gens du voyage, ainsi que des sanitaires et éventuellement du local de gardien nécessaires à ces terrains d'accueil, sous réserve des prescriptions qu'il édicte, notamment celle qui prévoit que ces constructions et installations doivent être " aptes à résister structurellement aux remontées de nappes et à une inondation dont le niveau serait égal aux plus hautes eaux connues " ; <br/>
<br/>
              Considérant qu'il résulte de ce qui précède qu'en jugeant que les dispositions du règlement du plan de prévention des risques naturels prévisibles d'inondation de la Loire impliquaient que le maire de Fondettes assortît le permis de construire sollicité de prescriptions propres à assurer la sécurité des personnes et des biens exposés aux risques d'inondation, sans indiquer quelles atteintes à la sécurité publique ou quels risques appelaient l'édiction de précisions ou de prescriptions complémentaires, et en en déduisant que le maire avait entaché l'autorisation litigieuse d'une erreur manifeste d'appréciation au regard des dispositions de l'article R. 111-2 du code de l'urbanisme, la cour administrative d'appel de Nantes a insuffisamment motivé son arrêt et commis une erreur de droit ; que la COMMUNE DE FONDETTES est donc fondée à demander l'annulation de l'arrêt qu'elle attaque ;<br/>
<br/>
              Considérant qu'il y a lieu, dans les circonstances de l'espèce, de faire application des dispositions de l'article L. 761-1 du code de justice administrative et de mettre à la charge de la SCI Chatigny, de M. A...et de MmeB..., le versement à la COMMUNE DE FONDETTES d'une somme de 1 000 euros, chacun, au titre des frais exposés par cette dernière et non compris dans les dépens ; qu'en revanche, ces dispositions font obstacle à ce que soit mis à la charge de la COMMUNE DE FONDETTES qui n'est pas, dans la présente instance, la partie perdante, le versement d'une somme au titre des frais exposés par la SCI Chatigny, M.  A...et Mme B...et non compris dans les dépens ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Nantes du 24 juin 2008 est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Nantes.<br/>
Article 3 : La SCI Chatigny, M. A...et Mme B...verseront  à la COMMUNE DE FONDETTES une somme de 1 000 euros, chacun, au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Les conclusions présentées par la SCI Chatigny, M. A...et Mme B...au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée à la COMMUNE DE FONDETTES, à la SCI Chatigny, à M. D... A...et à Mme C...B....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">68-01 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. PLANS D'AMÉNAGEMENT ET D'URBANISME. - PLAN DE PRÉVENTION DES RISQUES NATURELS PRÉVISIBLES - OBLIGATION D'EN REPRENDRE LES PRESCRIPTIONS DANS LES AUTORISATIONS D'URBANISME - 1) ABSENCE - 2) LIMITE - OBLIGATION, SI NÉCESSAIRE, DE PRÉCISER DANS L'AUTORISATION D'URBANISME LES CONDITIONS D'APPLICATION D'UNE PRESCRIPTION GÉNÉRALE DU PLAN OU D'EN SOUMETTRE LA DÉLIVRANCE À D'AUTRES PRESCRIPTIONS SPÉCIALES COMPLÉMENTAIRES.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">68-01-01-02-02-02 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. PLANS D'AMÉNAGEMENT ET D'URBANISME. PLANS D`OCCUPATION DES SOLS (POS) ET PLANS LOCAUX D'URBANISME (PLU). APPLICATION DES RÈGLES FIXÉES PAR LES POS OU LES PLU. RÈGLES DE FOND. TYPES D'OCCUPATION OU D'UTILISATION DU SOL SOUMIS À DES CONDITIONS SPÉCIALES. - PLAN DE PRÉVENTION DES RISQUES NATURELS PRÉVISIBLES - OBLIGATION D'EN REPRENDRE LES PRESCRIPTIONS DANS LES AUTORISATIONS D'URBANISME - 1) ABSENCE - 2) LIMITE - OBLIGATION, SI NÉCESSAIRE, DE PRÉCISER DANS L'AUTORISATION D'URBANISME LES CONDITIONS D'APPLICATION D'UNE PRESCRIPTION GÉNÉRALE DU PLAN OU D'EN SOUMETTRE LA DÉLIVRANCE À D'AUTRES PRESCRIPTIONS SPÉCIALES COMPLÉMENTAIRES.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">68-03-025-02-02 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. PERMIS DE CONSTRUIRE. NATURE DE LA DÉCISION. OCTROI DU PERMIS. PERMIS ASSORTI DE RÉSERVES OU DE CONDITIONS. - OBLIGATION DE REPRENDRE, DANS LE CADRE DE LA DÉLIVRANCE DU PERMIS, LES PRESCRIPTIONS D'UN PLAN DE PRÉVENTION DES RISQUES NATURELS PRÉVISIBLES - 1) ABSENCE - 2) LIMITE - OBLIGATION, SI NÉCESSAIRE, DE PRÉCISER DANS LE PERMIS LES CONDITIONS D'APPLICATION D'UNE PRESCRIPTION GÉNÉRALE DU PLAN OU D'EN SOUMETTRE LA DÉLIVRANCE À D'AUTRES PRESCRIPTIONS SPÉCIALES COMPLÉMENTAIRES.
</SCT>
<ANA ID="9A"> 68-01 1) Les prescriptions d'un plan de prévention des risques naturels prévisibles s'imposent au demandeur d'une autorisation d'urbanisme sans qu'il soit besoin de les reprendre dans cette autorisation. 2) Il incombe toutefois à l'autorité compétente pour délivrer l'autorisation, le cas échéant, d'y préciser les conditions d'application d'une prescription générale contenue dans le plan, ainsi que d'en subordonner l'octroi au respect d'autres prescriptions spéciales complémentaires qui lui apparaissent nécessaires en application de l'article R. 111-2 du code de l'urbanisme.</ANA>
<ANA ID="9B"> 68-01-01-02-02-02 1) Les prescriptions d'un plan de prévention des risques naturels prévisibles s'imposent au demandeur d'une autorisation d'urbanisme sans qu'il soit besoin de les reprendre dans cette autorisation. 2) Il incombe toutefois à l'autorité compétente pour délivrer l'autorisation, le cas échéant, d'y préciser les conditions d'application d'une prescription générale contenue dans le plan, ainsi que d'en subordonner l'octroi au respect d'autres prescriptions spéciales complémentaires qui lui apparaissent nécessaires en application de l'article R. 111-2 du code de l'urbanisme.</ANA>
<ANA ID="9C"> 68-03-025-02-02 1) Les prescriptions d'un plan de prévention des risques naturels prévisibles s'imposent au demandeur d'un permis de construire sans qu'il soit besoin de les reprendre dans ce dernier. 2) Il incombe toutefois à l'autorité compétente pour délivrer le permis, le cas échéant, d'y préciser les conditions d'application d'une prescription générale contenue dans le plan, ainsi que d'en subordonner l'octroi au respect d'autres prescriptions spéciales complémentaires qui lui apparaissent nécessaires en application de l'article R. 111-2 du code de l'urbanisme.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
