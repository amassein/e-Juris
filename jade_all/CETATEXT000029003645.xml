<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029003645</ID>
<ANCIEN_ID>JG_L_2014_05_000000351237</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/00/36/CETATEXT000029003645.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  5ème sous-section jugeant seule, 28/05/2014, 351237, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-05-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>351237</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 5ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DIDIER, PINET ; SCP LYON-CAEN, THIRIEZ</AVOCATS>
<RAPPORTEUR>M. Gérald Bégranger</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2014:351237.20140528</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 26 juillet et 13 octobre 2011 au secrétariat du contentieux du Conseil d'Etat, présentés pour l'Assistance publique-Hôpitaux de Paris, dont le siège est 3 avenue Victoria à Paris (75184) ; l'Assistance publique-Hôpitaux de Paris demande au Conseil d'Etat d'annuler l'arrêt n° 10PA01468 du 26 mai 2011 par lequel la cour administrative d'appel de Paris a, sur appel des consortsA..., annulé les jugements des 16 novembre 2005 et 6 juillet 2006 du tribunal administratif de Melun et l'a condamnée à verser, en réparation des préjudices ayant résulté d'une intervention subie par M. A...le 9 septembre 1998 à l'hôpital Henri Mondor de Créteil, à M. A...une somme de 736 256,30 euros ainsi qu'une rente trimestrielle d'un montant de 19 079,28 euros au titre de l'assistance d'une tierce personne, à Mme C...A..., en son nom propre, la somme de 60 000 euros et, au nom de ses deux enfants, la somme de 8 000 euros chacun ; <br/>
<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
	Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Gérald Bégranger, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Didier, Pinet, avocat de l'Assistance publique-Hôpitaux de Paris et à la SCP Lyon-Caen, Thiriez, avocat des consortsA... ;<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond qu'à la suite du traitement d'un cancer des amygdales, M. A...a subi les 31 août et 9 septembre 1998 à l'hôpital Henri Mondor de Créteil deux interventions chirurgicales pour la reconstruction de sa mandibule ; qu'à l'issue de la dernière intervention, il a présenté une hémiplégie droite massive, attribuée à un accident vasculaire cérébral consécutif à une occlusion de la carotide interne gauche ; que le tribunal administratif de Melun a, par un jugement du 16 novembre 2005, rejeté son recours indemnitaire dirigé contre l'Assistance publique-Hôpitaux de Paris, dont dépend l'hôpital Henri Mondor, ainsi que, par un jugement du 6 juillet 2006, celui que son épouse avait présenté tant en son nom propre qu'au nom de leurs enfants ; que les appels présentés par les consorts A...contre ces jugements ont été rejetés par un arrêt de la cour administrative d'appel de Paris du 26 novembre 2007 qui a été annulé par une décision du Conseil d'Etat statuant au contentieux du 19 mars 2010 ; que l'Assistance publique-Hôpitaux de Paris se pourvoit en cassation contre l'arrêt du 26 mai 2011 par lequel la cour administrative d'appel de Paris s'est de nouveau prononcée sur les deux appels et a retenu la responsabilité sans faute de l'établissement au titre de la survenue de l'accident vasculaire cérébral ;<br/>
<br/>
              2. Considérant que lorsqu'un acte médical nécessaire au diagnostic ou au traitement du malade présente un risque dont l'existence est connue mais dont la réalisation est exceptionnelle et dont aucune raison ne permet de penser que le patient y soit particulièrement exposé, la responsabilité du service public hospitalier est engagée si l'exécution de cet acte est la cause directe de dommages sans rapport avec l'état initial du patient comme avec l'évolution prévisible de cet état, et présentant un caractère d'extrême gravité ; <br/>
<br/>
              3. Considérant que pour juger que le dommage subi par M. A... présentait un caractère d'extrême gravité, la cour s'est bornée à relever qu'à la suite de l'accident vasculaire cérébral l'intéressé était resté atteint de très graves séquelles consistant en une hémiplégie droite, une aphasie globale, des troubles intellectuels, des troubles de déglutition et une surdité bilatérale et que ces conséquences présentaient une exceptionnelle gravité, sans répondre à l'argumentation de l'Assistance publique-Hôpitaux de Paris qui faisait valoir que si ces séquelles entraînaient une incapacité permanente partielle de 80 %, celle-ci était imputable à hauteur de 50 % à l'accident vasculaire cérébral et de 30 % à sa pathologie cancéreuse ; qu'elle a ainsi entaché son arrêt d'une insuffisance de motivation ; que, par suite, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, l'Assistance publique-Hôpitaux de Paris est fondée à demander l'annulation de l'arrêt du 26 mai 2011 ; <br/>
<br/>
              4. Considérant qu'aux termes du second alinéa de l'article L. 821-2 du code de justice administrative : " Lorsque l'affaire fait l'objet d'un second pourvoi en cassation, le Conseil d'Etat statue définitivement sur cette affaire " ; qu'il y a lieu, par suite, de régler l'affaire au fond ;<br/>
<br/>
              5. Considérant que les deux appels formés par les consorts A...contre les jugements du tribunal administratif de Melun des 16 novembre 2005 et 6 juillet 2006 présentent à juger les mêmes questions ; qu'il y a lieu de les joindre pour statuer par une seule décision ;<br/>
<br/>
              Sur les fins de non-recevoir opposées par l'Assistance publique-Hôpitaux de Paris :<br/>
<br/>
              6. Considérant, d'une part, que l'Assistance publique-Hôpitaux de Paris soutient que les conclusions indemnitaires présentées par Mme A...en son nom personnel et pour le compte de ses deux enfants mineurs sont irrecevables dès lors que Mme A...et ses enfants n'ont pas été parties à la procédure de première instance ; qu'il résulte toutefois des pièces du dossier que Mme A...a adressé à l'Assistance publique-Hôpitaux de Paris le 10 janvier 2006 une demande préalable en son nom personnel et pour le compte de ses deux enfants, rejetée expressément le 10 mars 2006, puis a saisi le tribunal administratif, qui a rejeté sa requête par le jugement du 6 juillet 2006 dont elle relève appel ; qu'ainsi, la fin de non-recevoir opposée par l'Assistance publique-Hôpitaux de Paris à la demande de Mme A...et de ses enfants doit être écartée ;<br/>
<br/>
              7. Considérant, d'autre part, que la caisse primaire d'assurance maladie de Paris s'est bornée, devant les juges de première instance, à produire un mémoire, enregistré le 9 mars 2002, pour demander " qu'il lui soit donné acte de son intervention et de ses réserves " ; qu'elle n'a, ce faisant, pas présenté devant les premiers juges de conclusions à fin de remboursement des frais qu'elle a exposés ; qu'elle n'est pas recevable à demander pour la première fois en appel que les dépenses qu'elle a exposées au titre des frais médicaux, pharmaceutiques et d'appareillage soient mises à la charge de l'Assistance publique-Hôpitaux de Paris ; que, par suite, cet établissement public est fondé à demander le rejet des conclusions de la caisse primaire d'assurance maladie de Paris ;<br/>
<br/>
              Sur la responsabilité sans faute de l'Assistance publique-Hôpitaux de Paris : <br/>
<br/>
              8. Considérant qu'il résulte de l'instruction, et notamment des constatations des experts, que malgré sa pathologie, M. A... n'était pas particulièrement exposé au risque connu mais exceptionnel d'accident vasculaire cérébral au cours d'une intervention chirurgicale ; que l'accident a entraîné une hémiplégie droite entravant la marche, une aphasie globale et des troubles intellectuels ; que ces dommages présentent un caractère d'extrême gravité sans commune mesure avec les troubles liés à la pathologie initiale de l'intéressé et à son traitement ; qu'ils sont sans rapport avec l'état initial du patient comme avec l'évolution prévisible de cet état ; que, dans ces conditions, la responsabilité de l'Assistance publique-Hôpitaux de Paris est engagée au titre des conséquences dommageables de l'accident ; que les consorts A...sont, par suite, fondés à soutenir que c'est à tort que le tribunal administratif de Melun a rejeté leurs conclusions à fin d'indemnisation ;<br/>
<br/>
              9. Considérant que si les consorts A...ont droit à ce que soient réparés tous les préjudices causés par l'accident vasculaire cérébral, les préjudices liés au cancer dont M. A... souffrait avant cet accident et aux effets de son traitement n'ont pas à être mis à la charge de l'Assistance publique-Hôpitaux de Paris ; qu'il convient, dès lors, de déterminer, pour chaque poste de préjudice, à laquelle de ces pathologies il doit être rattaché ; que, pour les postes de préjudices pour lesquels une telle détermination n'est pas possible, la part imputable à l'accident vasculaire cérébral doit être fixée au cinq huitièmes, soit 62,5%, l'expertise réalisée le 7 mars 2005 à la demande du tribunal administratif de Melun ayant retenu que, sur le taux d'incapacité permanente partielle de 80 % présenté par l'intéressé, un taux d'incapacité de 50 % résultait de l'accident vasculaire cérébral ;<br/>
<br/>
              Sur les préjudices de M.A... :<br/>
<br/>
              10. Considérant que la date de consolidation de l'état de M. A...a été fixée au 9 septembre 2003 par l'expertise du 7 mars 2005 mentionnée ci-dessus ; qu'après une aggravation de son état de santé en lien direct avec l'accident vasculaire cérébral, M. A... est décédé le 14 juillet 2011 à l'âge de 60 ans ; qu'il y a lieu d'évaluer jusqu'à cette date les préjudices causés par cet accident éprouvés par l'intéressé et dont ses ayants droit demandent l'indemnisation ; <br/>
<br/>
              En ce qui concerne les préjudices patrimoniaux de M.A... :<br/>
<br/>
              Quant aux dépenses de santé :<br/>
<br/>
              11. Considérant que, contrairement à ce qu'affirme l'Assistance publique-Hôpitaux de Paris, les dépenses de santé restées à la charge de M. A...concernant une ceinture abdominale, un fauteuil roulant, un panier porte-flacon, un bassin de lit et une pommade pour engelures sont dans leur totalité consécutives à la survenue de l'accident vasculaire cérébral ; qu'elles doivent ainsi être entièrement supportées par l'Assistance publique-Hôpitaux de Paris ; que ces dépenses s'élèvent au montant non contesté de 524 euros ;<br/>
<br/>
              12. Considérant que, compte tenu de leurs liens tant avec l'accident vasculaire cérébral qu'avec les effets du traitement du cancer subis par M.A..., l'Assistance publique-Hôpitaux de Paris ne doit supporter que 62,5 % des dépenses de santé restées à la charge de M. A... relatives aux appareillages auditifs, à la fourniture d'alimentation entérale et de trachéotomie, à la fourniture de Nutilis pour la déglutition et à un réducteur de mâchoire ; qu'au vu des factures présentées par les ayants droit de M. A..., la somme due à ce titre s'élève à 3 469 euros ; <br/>
<br/>
              13. Considérant que si les ayants droit de M. A...démontrent qu'une partie de sa rééducation s'est effectuée grâce à des logiciels spécifiques, ils n'apportent pas de justifications suffisantes à l'appui de leurs conclusions tendant au remboursement des frais d'équipement en ordinateur ; que les frais de lunettes ne sont pas davantage en lien direct avec l'accident vasculaire cérébral ;<br/>
<br/>
              Quant à la perte de gains professionnels de M.A... :<br/>
<br/>
              14. Considérant, s'agissant de la perte de gains professionnels avant consolidation, que si M. A... souffrait d'une incapacité temporaire totale entre le 9 septembre 1998 et le 9 septembre 2003, date de sa mise à la retraite, il résulte de l'instruction que son employeur a continué à lui verser un plein traitement pendant trois ans, puis un demi-traitement pendant deux ans et que 40 699,55 euros ont été pris en charge par la mutuelle Intériale durant la même période ; que ses ayants droit n'établissent pas qu'il aurait perçu durant cette période des revenus inférieurs à ceux qui auraient été les siens en l'absence d'accident vasculaire cérébral ;<br/>
<br/>
              15. Considérant, s'agissant de la perte de gains professionnels après consolidation, que M.A..., alors âgé de 52 ans, a été admis à faire valoir ses droits à la retraite pour invalidité à compter du 9 septembre 2003 ; que ses ayants droit demandent, au titre de la perte de gains subie durant cette période, une indemnité de 90 748 euros correspondant à la différence entre le montant de la pension de retraite qu'il a perçue et celui du traitement qui lui aurait été versé durant les huit ans qui le séparaient de l'âge légal de départ à la retraite ; que si l'Assistance publique-Hôpitaux de Paris soutient que la pathologie cancéreuse de l'intéressé ne lui aurait pas permis, en toute hypothèse, de travailler normalement, elle n'établit pas que cette affection, à l'origine d'une incapacité permanente partielle de 30 %, aurait fait obstacle à l'exercice d'une activité professionnelle ; que, par suite, il y a lieu d'accorder aux ayants droit de M. A...la somme de 90 748 euros demandée ;<br/>
<br/>
              Quant à l'incidence professionnelle :<br/>
<br/>
              16. Considérant que les ayants droit de M. A... n'apportent aucun élément permettant de justifier la somme de 100 000 euros qu'ils réclament au titre de l'incidence professionnelle de l'accident de M.A... ; que, par suite, il n'y a pas lieu de leur accorder une somme à ce titre ;<br/>
<br/>
              Quant aux frais de logement adapté :<br/>
<br/>
              17. Considérant que les ayants droit de M. A...réclament 27 878 euros au titre des frais engagés pour réaliser des travaux afin d'adapter leur logement aux handicaps de M. A... consécutifs à la survenue d'un accident vasculaire cérébral ; que si, comme le soutient l'Assistance publique - Hôpitaux de Paris, ils ne présentent qu'un devis pour les 1 463,29 euros liés à l'installation d'un store, le reste de ces dépenses, dûment justifié, soit 26 415 euros, doit leur être remboursé ;<br/>
<br/>
              Quant à l'assistance par tierce personne :<br/>
<br/>
              18. Considérant qu'il résulte de l'instruction qu'après le retour de M. A... à son domicile, son état de santé consécutif à la survenue de l'accident vasculaire cérébral a nécessité, avant consolidation, l'assistance d'une tierce personne à raison de douze heures par jour d'avril à juin 2001, dix heures par jour de juillet 2001 à novembre 2002 et huit heures par jour de décembre 2002 à août 2003 ; que, compte tenu du salaire horaire moyen d'une aide à domicile et aux éléments d'évaluation produits par les requérants, il sera fait une juste appréciation de ce poste de préjudice, en leur allouant, au titre de ce poste de préjudice, pour la période courant jusqu'au 31 août 2003, un capital de 75 787 euros correspondant aux charges exposées à ce titre et non prises en charge par la caisse primaire d'assurance maladie de Paris ; que, par ailleurs, la nécessité de la présence d'une tierce personne 24 heures sur 24 en raison de la dégradation de l'état de santé de M. A...n'est établie qu'à compter du début de l'année 2009 ; qu'il y a lieu, par suite, en ce qui concerne les frais liés à l'assistance d'une tierce personne pour la période postérieure à août 2003, de retenir une présence de huit heures par jour et d'allouer à l'intéressé, sur les mêmes bases de calcul que pour la période comprise entre décembre 2002 et août 2003, la somme de 8 480 euros pour les quatre derniers mois de l'année 2003 et celle de 127 195 euros pour les années 2004 à 2008 ; que, sur la base d'une présence 24 heures sur 24 à partir de l'année 2009, les ayants droit de M. A... peuvent prétendre à une indemnité de 193 125 euros pour la période du 1er janvier 2009 au 14 juillet 2011 ; que la somme totale correspondant à l'assistance par tierce personne s'élève à 404 587 euros ;<br/>
<br/>
              Quant aux frais divers :<br/>
<br/>
              19. Considérant que les ayants droit de M. A...réclament 1 504 euros au titre des dépenses engagées pour la location d'un poste de télévision et l'ouverture d'une ligne téléphonique lors des nombreuses hospitalisations consécutives à la survenue de l'accident vasculaire cérébral ; que rien n'indique, contrairement à ce que soutient l'Assistance publique-Hôpitaux de Paris, que ces dépenses, pour lesquelles les consorts A...produisent des factures, auraient été prises en charge par un tiers ni que les périodes d'hospitalisation concernées aient été imputables à la pathologie cancéreuse de l'intéressé ; que, par suite, il sera alloué 1 504 euros au titre de ce chef de préjudice ; <br/>
<br/>
              En ce qui concerne les préjudices extrapatrimoniaux de M.A... :<br/>
<br/>
              Quant aux préjudices temporaires :<br/>
<br/>
              20. Considérant que si M. A...a souffert d'une incapacité temporaire totale jusqu'à sa date de consolidation, une part de cette incapacité était imputable à la pathologie cancéreuse dont il souffrait avant son accident vasculaire cérébral ; que par suite, il sera fait dans les circonstances de l'espèce une juste appréciation de son déficit fonctionnel temporaire en l'évaluant à 250 euros par mois, soit 30 000 euros ;<br/>
<br/>
              21. Considérant que le pretium doloris de M. A...a été évalué à 6/7 mais que l'expert a précisé que la part imputable à l'accident vasculaire cérébral était de 4/7 ; que, par suite, 6 000 euros seront alloués aux requérants au titre des souffrances endurées par M.A... ;<br/>
<br/>
              Quant aux préjudices permanents :<br/>
<br/>
              22. Considérant que M. A...a souffert après consolidation d'une incapacité permanente partielle de 80 %, celle-ci étant imputable à hauteur de 50 % à l'accident vasculaire cérébral ; qu'il sera fait dans les circonstances de l'espèce une juste appréciation de son déficit fonctionnel permanent en allouant aux ayants droit de M. A...une somme de 86 500 euros à ce titre ;<br/>
<br/>
              23. Considérant que le préjudice esthétique de M. A...a été évalué à 4/7, dont 2/7 imputable à l'accident vasculaire cérébral ; qu'il y a lieu d'allouer à ses ayants droit une somme de 1 700 euros pour ce chef de préjudice ;<br/>
<br/>
              24. Considérant qu'en dépit de sa pathologie cancéreuse, M. A... était avant son accident un homme très actif, titulaire d'un brevet de pilotage, pratiquant le tennis et la moto, jouant de la guitare et voyageant ; que l'expert ayant retenu que l'essentiel du préjudice d'agrément de M. A...était imputable à l'accident vasculaire cérébral, il sera fait une juste appréciation de ce chef de préjudice en lui allouant la somme de 15 000 euros ;<br/>
<br/>
              25. Considérant qu'il sera fait une juste appréciation du préjudice sexuel de M. A... consécutif à l'accident vasculaire cérébral dont il a été victime en le fixant à 10 000 euros ;<br/>
<br/>
              26. Considérant qu'il résulte de ce qui précède que les ayants droit de M. A... sont fondés à demander l'annulation du jugement du 15 novembre 2005 du tribunal administratif de Paris et la condamnation de l'Assistance publique-Hôpitaux de Paris à leur verser la somme de 676 447 euros au titre des préjudices subis par M. A...entre le 9 septembre 1998 et le 14 juillet 2011 du fait de l'accident dont il a été victime ; <br/>
<br/>
              Sur les préjudices de Mme A...et de ses enfants :  <br/>
<br/>
              27. Considérant que Mme A...demande, en son nom propre et au nom de ses enfants Tiffany et Jay-Lloyd, la réparation des préjudices qu'ils ont subis en raison de la maladie puis du décès de leur mari et père ; que TiffanyA..., majeure à compter du 7 décembre 2013, a repris en son nom propre les conclusions présentées par sa mère en son nom ;<br/>
<br/>
              En ce qui concerne les préjudices patrimoniaux :<br/>
<br/>
              Quant aux préjudices économiques subis par Mme A...avant le décès de son époux :<br/>
<br/>
              28. Considérant que Mme A...demande 50 000 euros pour les préjudices économiques qu'elle a subis du fait de la maladie son mari  jusqu'à son décès ; que si Mme A... a fait le choix de s'arrêter de travailler pour s'occuper de son époux, elle ne démontre pas avoir subi un préjudice économique supérieur aux frais liés à l'assistance par tierce personne faisant par ailleurs l'objet d'une indemnisation ;<br/>
<br/>
              Quant aux préjudices économiques subis par les consorts A...postérieurement au décès de M.A... :<br/>
<br/>
              29. Considérant qu'il résulte de l'instruction que les revenus du foyer s'élevaient, avant le décès de M.A..., à 57 326 euros par an ; qu'il convient de déduire de ces revenus, dès lors que le foyer comportait deux enfants mineurs, 15 % pour la part de consommation personnelle de M.A... ; que les revenus postérieurs à cette date s'élèvent, compte tenu de la pension de réversion touchée par MmeA..., de ses revenus personnels et de la pension versée aux orphelins, à 37 660 euros ; que la perte de revenus des proches de M. A... peut être évaluée à 11 067 euros par an ; qu'il convient de convertir cette somme en capital et de lui appliquer, compte tenu de l'âge de M. A...au moment de son décès, un coefficient de capitalisation de 18, 368 ; qu'ainsi il sera versé aux consorts A...un capital de 203 279 euros au titre des pertes de revenus du fait du décès de leur mari et père ; que ce capital sera versé, ainsi qu'ils le demandent, à hauteur de 70 % à MmeA..., soit 142 295,30 euros, et à hauteur 15 % à chacun des enfants, soit 30 491,85 euros chacun ;  <br/>
<br/>
              Quant aux frais d'obsèques :<br/>
<br/>
              30. Considérant que Mme A...demande la prise en charge des frais d'obsèques qu'elle a assumés et qui s'élèvent à 4 368 euros ; qu'il y a lieu de les lui accorder ;<br/>
<br/>
              En ce qui concerne les préjudices extrapatrimoniaux :<br/>
<br/>
              31. Considérant qu'il sera fait une juste appréciation du préjudice d'accompagnement subi par les consorts A...du fait de la maladie de leur mari et père en octroyant 400 euros par mois à Mme A...et 300 euros par mois à chacun des enfants pour la période comprise entre le 9 septembre 1998 et le 14 juillet 2011, soit 61 600 euros à Mme A...et 46 200 euros à chacun des enfants ; <br/>
<br/>
              32. Considérant que, dans les circonstances de l'espèce, il sera fait une juste appréciation du préjudice d'affection subi par les consorts A...du fait du décès de leur mari et père en leur octroyant 20 000 euros à chacun ; <br/>
<br/>
              33. Considérant qu'il résulte de ce qui précède que les requérants sont fondés à demander l'annulation du jugement du 6 juillet 2006 du tribunal administratif de Paris et la condamnation de l'Assistance publique-Hôpitaux de Paris à verser à Mme A...la somme de 228 263,30 euros en son nom propre et la somme de 96 691,85 euros en sa qualité de représentante légale de son fils, et à Tiffany A...la somme de 96 691,85 euros, en réparation des préjudices qu'ils ont personnellement subis du fait de l'accident survenu à leur mari et père ;  <br/>
<br/>
              Sur les intérêts et la capitalisation des intérêts :<br/>
<br/>
              34. Considérant, d'une part, que les ayants droit de M. A...ont droit aux intérêts au taux légal sur la somme de 676 447 euros, correspondant aux frais exposés et aux préjudices personnels, à compter de la date de réception de sa demande préalable du 27 avril 2000 pour les créances nées antérieurement à cette date et à compter de leur naissance pour les autres ; que la capitalisation des intérêts a été demandée le 23 septembre 2010 ; qu'à cette date, il était dû plus d'une année d'intérêts ; que, dès lors, conformément aux dispositions de l'article 1154 du code civil, il y a lieu de faire droit à cette demande, puis d'accorder la capitalisation à chaque échéance annuelle à compter de cette date ;<br/>
<br/>
              35. Considérant, d'autre part, que MmeA..., en son nom propre et en qualité de représentante légale de son fils, ainsi que TiffanyA..., ont droit aux intérêts au taux légal sur les sommes dues par l'Assistance publique-Hôpitaux de Paris à compter de la date de réception de leur demande préalable du 10 janvier 2006 pour les créances nées antérieurement à cette date et à compter de leur naissance pour les autres ; que la capitalisation des intérêts a été demandée le 23 septembre 2010 ; qu'à cette date, il était dû plus d'une année d'intérêts ; que, dès lors, conformément aux dispositions de l'article 1154 du code civil, il y a lieu de faire droit à cette demande, puis d'accorder la capitalisation à chaque échéance annuelle à compter de cette date ; <br/>
<br/>
              Sur les frais d'expertise :<br/>
<br/>
              36. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Assistance publique-Hôpitaux de Paris les frais d'expertise taxés et liquidés à la somme de 4 000 euros par l'ordonnance du président du tribunal administratif de Melun du 15 avril 2005 ;<br/>
<br/>
              Sur les conclusions tendant à l'application de l'article L. 761-1 du code de justice administrative : <br/>
<br/>
              37. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Assistance publique-Hôpitaux de Paris, la somme de 5 000 euros à verser aux consortsA..., au titre des dispositions de l'article L. 761-1 du code de justice administrative ; que ces dispositions font obstacle à ce que la somme demandée à ce titre par l'Assistance publique-Hôpitaux de Paris soit mise à la charge des consorts A...qui ne sont pas, dans la présente instance, la partie perdante ; qu'elles font également obstacle à ce qu'il soit fait droit aux conclusions présentées par la caisse primaire d'assurance maladie de Paris à l'encontre de l'Assistance publique-Hôpitaux de Paris au titre de l'instance d'appel, la demande d'indemnité présentée par la caisse n'étant pas accueillie par la présente décision ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Paris du 26 mai 2011 est annulé. <br/>
<br/>
Article 2 : Les jugements du tribunal administratif de Melun des 16 novembre 2005 et 6 juillet 2006 sont annulés.<br/>
<br/>
Article 3 : L'Assistance publique-hôpitaux de Paris est condamnée à verser aux ayants droit de M. A...la somme de 676 447 euros assortie des intérêts au taux légal à compter de la date de réception de sa demande préalable du 27 avril 2000 pour les créances nées antérieurement à cette date et à compter de leur naissance pour les autres, les intérêts étant capitalisés pour produire eux-mêmes intérêts au 23 septembre 2010 et à chaque échéance annuelle ultérieure.<br/>
<br/>
Article 4 : L'Assistance publique-Hôpitaux de Paris est condamnée à verser à Mme A... la somme de 228 263,30 euros en son nom propre et la somme de 96 691,85 euros en sa qualité de représentante légale de son fils Jay-LloydA..., et à Mlle B...A...une somme de 96 691,85 euros, ces sommes étant assorties des intérêts au taux légal à compter de la date de réception de leur demande préalable du 10 janvier 2006, les intérêts étant capitalisés pour produire eux-mêmes intérêts au 23 septembre 2010 et à chaque échéance annuelle ultérieure.<br/>
<br/>
Article 5 : Les frais et honoraires d'expertise taxés et liquidés à la somme de 4 000 euros sont mis à la charge de l'Assistance publique-Hôpitaux de Paris.<br/>
<br/>
Article 6 : L'Assistance publique-Hôpitaux de Paris versera aux consorts A...la somme de 5 000 euros en application de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 7 : Les conclusions présentées par la caisse primaire d'assurance maladie de Paris devant la cour administrative d'appel de Paris sont rejetées.<br/>
Article 8 : Les conclusions de l'Assistance publique-Hôpitaux de Paris présentées sur le fondement de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 9 : La présente décision sera notifiée à l'Assistance publique-Hôpitaux de Paris, à Mme C...A..., à Mlle B...A...et à la caisse primaire d'assurance maladie de Paris.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
