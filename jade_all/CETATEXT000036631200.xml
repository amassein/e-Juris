<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036631200</ID>
<ANCIEN_ID>JG_L_2018_02_000000402109</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/63/12/CETATEXT000036631200.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème et 5ème chambres réunies, 21/02/2018, 402109</TITRE>
<DATE_DEC>2018-02-21</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>402109</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème et 5ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>LE PRADO</AVOCATS>
<RAPPORTEUR>Mme Airelle Niepce</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Julie Burguburu</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:402109.20180221</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société civile de construction vente (SCCV) Les Balcons de l'Arly a demandé au tribunal administratif de Grenoble d'annuler pour excès de pouvoir l'arrêté du 17 septembre 2012 par lequel le maire de Crest-Voland a opposé un sursis à statuer à sa demande tendant à obtenir un permis de construire modificatif au permis initialement délivré le 12 avril 2007. Par un jugement n° 1205907 du 4 juillet 2014, le tribunal administratif a annulé cet arrêté et enjoint au maire de Crest-Voland de réexaminer, dans un délai de trois mois à compter de la notification de son jugement, la demande de permis de construire modificatif présentée par la SCCV Les Balcons de l'Arly.<br/>
<br/>
              Par un arrêt n° 14LY02741 du 14 juin 2016, la cour administrative d'appel de Lyon a rejeté l'appel formé par la commune de Crest-Voland contre ce jugement.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 3 août et 21 octobre 2016 au secrétariat du contentieux du Conseil d'Etat, la commune de Crest-Voland demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ; <br/>
<br/>
              2°) de mettre à la charge de la SCCV Les Balcons de l'Arly la somme de 3 500 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              -	le code de l'urbanisme ;<br/>
              -	le décret n° 2008-1353 du 19 décembre 2008 ;<br/>
              -	le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Airelle Niepce, maître des requêtes ;<br/>
<br/>
              - les conclusions de Mme Julie Burguburu, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Le Prado, avocat de la commune de Crest-Voland ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1.	Il ressort des pièces du dossier soumis aux juges du fond que, le 12 avril 2007, le maire de Crest-Voland a délivré au groupe Rémy Loisirs un permis de construire pour la réalisation d'une résidence de tourisme au lieudit La Cottuaz, qui a été transféré à la société civile de construction vente (SCCV) Les Balcons de l'Arly par un arrêté du 24 juillet 2007. Cette société a déposé, le 4 avril 2008, une demande de permis de construire modificatif qui a été rejetée par une décision du maire de Crest-Voland du 12 novembre 2008. Par un jugement du 23 février 2012, le tribunal administratif de Grenoble a, d'une part, annulé ce refus, d'autre part, enjoint au maire de Crest-Voland de réexaminer la demande de permis modificatif dans un délai de trois mois à compter de la notification de son jugement. En exécution de cette injonction, le maire de Crest-Voland a, par des décisions des 22 et 23 mai 2012, décidé de surseoir à statuer sur cette demande. La SCCV Les Balcons de l'Arly a, par trois courriers distincts du 16 juillet 2012, reçus en mairie le 18 juillet 2012, d'une part, demandé le retrait des décisions des 22 et 23 mai 2012, d'autre part, sollicité, en application des dispositions de l'article L. 600-2 du code de l'urbanisme, le réexamen de sa demande de permis modificatif au regard des dispositions d'urbanisme applicables au 12 novembre 2008. Par un arrêté du 17 septembre 2012, le maire de Crest-Voland lui a opposé un nouveau sursis à statuer. Par un jugement du 4 juillet 2014, le tribunal administratif de Grenoble a, d'une part, annulé cet arrêté, d'autre part, enjoint au maire de Crest-Voland de statuer de nouveau sur la demande de la société pétitionnaire, dans un délai de deux mois, en faisant application des dispositions de l'article L. 600-2 du code de l'urbanisme. La commune de Crest-Voland se pourvoit en cassation contre l'arrêt du 14 juin 2016 par lequel la cour administrative d'appel de Lyon a rejeté son appel contre ce jugement. <br/>
<br/>
              2.	D'une part, aux termes du premier alinéa de l'article R* 424-17 du code de l'urbanisme, dans sa rédaction applicable à la date de délivrance du permis de construire initial : " Le permis de construire, d'aménager ou de démolir est périmé si les travaux ne sont pas entrepris dans le délai de deux ans à compter de la notification mentionnée à l'article R. 424-10 ou de la date à laquelle la décision tacite est intervenue. "  En application du décret du 19 décembre 2008 prolongeant le délai de validité des permis de construire, d'aménager ou de démolir et des décisions de non-opposition à une déclaration préalable, ce délai a été porté à trois ans pour les permis de construire délivrés avant le 31 décembre 2010. Lorsque le permis de construire est périmé, un permis modificatif ne peut légalement être délivré. <br/>
<br/>
              3.	D'autre part, aux termes du premier alinéa de l'article R* 424-19 du code de l'urbanisme : " En cas de recours devant la juridiction administrative contre le permis ou contre la décision de non-opposition à la déclaration préalable ou de recours devant la juridiction civile en application de l'article L. 480-13, le délai de validité prévu à l'article R. 424-17 est suspendu jusqu'au prononcé d'une décision juridictionnelle irrévocable. " <br/>
<br/>
              4.	La cour administrative d'appel de Lyon a jugé que le délai de validité du permis de construire dont était titulaire la SCCV Les Balcons de l'Arly et pour lequel elle demandait la délivrance d'un permis modificatif avait été, en application des dispositions de l'article R* 424-19 du code de l'urbanisme citées au point 3, suspendu pendant la durée du recours formé par la société contre le refus de lui délivrer le permis de construire modificatif. En statuant ainsi, alors que les dispositions de l'article R* 424-19 du code de l'urbanisme ne sont pas applicables en cas de recours du bénéficiaire d'un permis de construire contre le refus de lui délivrer un permis de construire modificatif, la cour administrative d'appel de Lyon a entaché son arrêt d'une erreur de droit. Par suite, sans qu'il soit besoin de se prononcer sur les autres moyens du pourvoi, son arrêt doit être annulé.<br/>
<br/>
              5.	Il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par la commune de Crest-Voland au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt n° 14LY02741 du 14 juin 2016 de la cour administrative d'appel de Lyon est annulé.<br/>
<br/>
      Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Lyon.<br/>
<br/>
Article 3 : Les conclusions présentées par la commune de Crest-Voland au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 4 : La présente décision sera notifiée à la commune de Crest-Voland et à la société civile de construction vente Les Balcons de l'Arly.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">68-03-04-01 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. PERMIS DE CONSTRUIRE. RÉGIME D'UTILISATION DU PERMIS. PÉREMPTION. - RECOURS CONTRE LE REFUS DE DÉLIVRER UN PERMIS DE CONSTRUIRE MODIFICATIF - EFFET SUSPENSIF SUR LE DÉLAI DE VALIDITÉ DU PERMIS DE CONSTRUIRE INITIAL (ART. R - . 424-19 DU CODE DE L'URBANISME) - ABSENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">68-06 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. RÈGLES DE PROCÉDURE CONTENTIEUSE SPÉCIALES. - RECOURS CONTRE LE REFUS DE DÉLIVRER UN PERMIS DE CONSTRUIRE MODIFICATIF - EFFET SUSPENSIF SUR LE DÉLAI DE VALIDITÉ DU PERMIS DE CONSTRUIRE INITIAL (ART. R - . 424-19 DU CODE DE L'URBANISME) - ABSENCE.
</SCT>
<ANA ID="9A"> 68-03-04-01 L'article R*. 424-19 du code de l'urbanisme n'est pas applicable en cas de recours du bénéficiaire d'un permis de construire contre le refus de lui délivrer un permis de construire modificatif. Le délai de validité du permis de construire initial n'est donc pas suspendu pendant la durée du recours formé par le titulaire de ce permis contre le refus de lui délivrer un permis de construire modificatif.</ANA>
<ANA ID="9B"> 68-06 L'article R*. 424-19 du code de l'urbanisme n'est pas applicable en cas de recours du bénéficiaire d'un permis de construire contre le refus de lui délivrer un permis de construire modificatif. Le délai de validité du permis de construire initial n'est donc pas suspendu pendant la durée du recours formé par le titulaire de ce permis contre le refus de lui délivrer un permis de construire modificatif.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
