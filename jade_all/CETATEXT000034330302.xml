<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034330302</ID>
<ANCIEN_ID>JG_L_2017_03_000000385108</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/33/03/CETATEXT000034330302.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème - 3ème chambres réunies, 31/03/2017, 385108, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-03-31</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>385108</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème - 3ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MASSE-DESSEN, THOUVENIN, COUDRAY</AVOCATS>
<RAPPORTEUR>M. Jean-Marc Anton</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Romain Victor</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:385108.20170331</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. E...C...a demandé au tribunal administratif de Fort-de-France de le décharger des rappels de taxe sur la valeur ajoutée qui lui ont été réclamés pour la période du 1er janvier 2000 au 31 décembre 2002 ainsi que des pénalités et intérêts de retard correspondants. Par un jugement n° 0701086 du 23 décembre 2011, ce tribunal a rejeté cette demande.<br/>
<br/>
              Par un arrêt n° 12BX00525 du 10 juillet 2014, la cour administrative d'appel de Bordeaux a annulé ce jugement et rejeté l'appel de M.C....<br/>
<br/>
              Par un pourvoi sommaire, complété par des observations rectificatives, un mémoire complémentaire et un nouveau mémoire, enregistrés les 13 octobre et 14 octobre 2014, 13 janvier 2015 et 16 avril 2016, M.C..., MeF..., en sa qualité d'administrateur judiciaire de M.C..., et MeD..., en sa qualité de mandataire judiciaire du redressement judiciaire de M.C..., demandent au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt en tant qu'il est relatif à la revente de lots issus de la division d'un terrain que M. C...avait acquis en 1997 ;<br/>
<br/>
              2°) réglant dans cette mesure l'affaire au fond, de faire droit à l'appel de M. C... ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 4 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Marc Anton, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Romain Victor, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Masse-Dessen, Thouvenin, Coudray, avocat de M.C..., de Me F...et de Me D...;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond qu'à la suite d'un examen de la situation fiscale personnelle de M. et Mme C...portant sur les années 2000 à 2002, M. C... a été assujetti, à la suite de la requalification en bénéfices industriels et commerciaux de revenus issus de la vente d'immeubles à bâtir provenant de la division d'un terrain lui appartenant, à des rappels de taxe sur la valeur ajoutée au titre de cette période, assortis de pénalités pour exercice d'une activité occulte en application de l'article 1728 du code général des impôts.<br/>
<br/>
              Sur le bien-fondé des rappels de taxe sur la valeur ajoutée :  <br/>
<br/>
              2. Aux termes de l'article 35 du code général des impôts : " I. Présentent également le caractère de bénéfices industriels et commerciaux, pour l'application de l'impôt sur le revenu, les bénéfices réalisés par les personnes physiques désignées ci-après : / (...) 3° Personnes qui procèdent à la cession d'un terrain divisé en lots destinés à être construits lorsque le terrain a été acquis à cet effet (...) ". Aux termes de l'article 257 du même code, dans sa rédaction applicable aux impositions en litige : " Sont également soumis à la taxe sur la valeur ajoutée : / (...) 6° Les opérations qui portent sur des immeubles, des fonds de commerce ou des actions ou parts de sociétés immobilières et dont les résultats doivent être compris dans les bases de l'impôt sur le revenu au titre des bénéfices industriels ou commerciaux (...) ". La cour administrative d'appel de Bordeaux a relevé que M. C...s'était vu consentir en septembre 1991 une promesse de vente d'un terrain d'une superficie de 10 137 m² sur la commune du Diamant, sous condition suspensive de l'obtention d'un arrêté de lotissement, que l'autorisation de lotir, sollicitée par M. C...en janvier et septembre 1992, avait été obtenue le 1er juillet 1996, que le transfert définitif de propriété était intervenu, au profit de M. C..., par actes des 30 mai et 9 juin 1997, que le terrain avait alors fait l'objet de travaux de viabilisation et d'aménagement et avait été divisé en huit lots,  que trois lots avaient été vendus en 2000 pour un montant total de 303 696 euros et deux lots en 2001 pour un montant total de 334 909 euros et, enfin, que M. C...n'établissait pas que ses huit frères et soeurs se seraient désengagés de ce projet en raison du délai pour l'obtention de l'arrêté de lotir. Elle a  pu déduire de ces constatations exemptes de dénaturation que le terrain avait été acquis, dès l'origine, dans l'intention de le revendre après division en huit lots et obtention d'une autorisation de lotir, et non de l'utiliser pour réaliser un lotissement familial destiné aux huit membres de sa fratrie. Par suite, c'est sans erreur de droit que la cour a jugé que, les bénéfices tirés de la vente des lots issus de la division de ce terrain relevaient du champ d'application des dispositions précitées de l'article 35 du code général des impôts et que l'opération en cause était soumise à la taxe sur la valeur ajoutée. <br/>
<br/>
              Sur les pénalités :<br/>
<br/>
              3. Aux termes du 3 de l'article 1728 du code général des impôts dans sa rédaction applicable à la présente espèce, dont la rédaction a été reprise par le c du 1 de l'actuel article 1728 : "  Le défaut de production dans les délais prescrits d'une déclaration ou d'un acte comportant l'indication d'éléments à retenir pour l'assiette ou la liquidation de l'impôt entraîne l'application, sur le montant des droits mis à la charge du contribuable ou résultant de la déclaration ou de l'acte déposé tardivement, d'une majoration de : (...) c. 80 % en cas de découverte d'une activité occulte. ".  Il résulte de ces dispositions, éclairées par les travaux parlementaires qui ont précédé l'adoption de la loi de laquelle elles sont issues, que dans le cas où un contribuable n'a ni déposé dans le délai légal les déclarations qu'il était tenu de souscrire, ni fait connaître son activité à un centre de formalités des entreprises ou au greffe du tribunal de commerce, l'administration doit être réputée apporter la preuve, qui lui incombe, de l'exercice occulte de l'activité professionnelle si le contribuable n'est pas lui même en mesure d'établir qu'il a commis une erreur justifiant qu'il ne se soit acquitté d'aucune de ces obligations déclaratives. <br/>
<br/>
              4. En jugeant que la majoration de 80 % prévue par ces dispositions en cas de découverte d'une activité occulte pouvait être retenue aux deux seules conditions que le contribuable n'ait pas fait connaître son activité à un centre de formalités des entreprises ou au greffe du tribunal de commerce, ni souscrit les déclarations fiscales qui lui incombent et en considérant sans incidence l'erreur dont se prévalaient M. et MmeC..., qui soutenaient avoir déclaré les revenus en cause dans une mauvaise catégorie d'imposition, la cour a commis une erreur de droit. <br/>
<br/>
              5. Il résulte de ce qui précède que M. C...est seulement fondé à demander l'annulation de l'arrêt qu'il attaque en tant qu'il rejette sa demande de décharge de la majoration de 80 % mise à sa charge à raison du caractère occulte de son activité de lotisseur.<br/>
<br/>
              6. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat le versement à M. C...et autres de la somme globale de 1 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>                        D E C I D E :<br/>
                                       --------------<br/>
<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Bordeaux du 10 juillet 2014 est annulé en tant qu'il porte sur les pénalités au titre des activités de lotisseur de M.C....<br/>
<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Bordeaux dans la limite de la cassation ainsi prononcée. <br/>
<br/>
Article 3 : L'Etat versera à M. C...et autres une somme globale de 1 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : Le surplus des conclusions de M. C...et autres est rejeté.<br/>
<br/>
Article 5 : La présente décision sera notifiée à M. E...C..., à Maître B...F..., à Maître A...D...et au ministre de l'économie et des finances.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
