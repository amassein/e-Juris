<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000024815385</ID>
<ANCIEN_ID>JG_L_2011_11_000000353801</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/24/81/53/CETATEXT000024815385.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 08/11/2011, 353801, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2011-11-08</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>353801</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>FOUSSARD ; SCP GADIOU, CHEVALLIER</AVOCATS>
<RAPPORTEUR>M. Jacques Arrighi de Casanova</RAPPORTEUR>
<COMMISSAIRE_GVT/>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la requête, enregistrée le 2 novembre 2011 au secrétariat du contentieux du Conseil d'État, présentée par M. Henri A, ... ; M. A demande au juge des référés du Conseil d'État :<br/>
<br/>
              1°) d'annuler l'ordonnance n° 1118048/9 du 17 octobre 2011 par laquelle le juge des référés du tribunal administratif de Paris, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, a rejeté comme portée devant un ordre de juridiction incompétent pour en connaître sa demande tendant à ce qu'il soit enjoint à la Ville de Paris de procéder à la rétrocession du bien situé 53 rue de l'Église, sur lequel elle a exercé son droit de préemption ;<br/>
<br/>
              2°) de faire droit à sa demande de première instance ; <br/>
<br/>
              3°) de mettre à la charge de la Ville de Paris le versement d'une somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              il soutient que la juridiction administrative est compétente pour connaître d'un refus de rétrocéder un bien préempté, acte pris par une personne morale de droit public, et que sa requête est recevable ; que la condition d'urgence est remplie ; que la Ville de Paris a porté atteinte aux libertés fondamentales que constituent le droit de propriété et le droit de disposer de ses biens ; que ces atteintes sont graves et manifestement illégales, dès lors que la Ville de Paris méconnaît les dispositions des article L. 213-14 et R. 213-12 du code de l'urbanisme ; <br/>
<br/>
<br/>
              Vu l'ordonnance attaquée ;<br/>
<br/>
              Vu le mémoire en défense et le nouveau mémoire, enregistrés les 4 et 7 novembre 2011, présentés pour la Ville de Paris, qui conclut au rejet de la requête et à ce que la somme de 2 500 euros soit mise à la charge de M. A au titre de l'article L. 761-1 du code de justice administrative ; elle soutient que l'incompétence du juge administratif doit être confirmée, dès lors qu'elle a procédé à la consignation des sommes dues dans le délai de six mois prévu à l'article L. 213-14 du code de l'urbanisme et que le litige porte sur la régularité ou le caractère abusif de cette consignation, sur laquelle, conformément à l'article R. 13-39 du code de l'expropriation, il appartient au juge de l'expropriation de se prononcer ; que, subsidiairement, la décision implicite contestée n'existe pas ; que les conditions requises par l'article L. 521-2 du code de justice administrative ne sont pas remplies, dès lors que l'urgence n'est pas caractérisée, que l'exercice du droit de préemption ne saurait constituer une atteinte grave au droit de propriété et qu'aucune illégalité manifeste ne saurait être retenue ; <br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 8 novembre 2011, présentée pour la Ville de Paris ;<br/>
<br/>
              Vu le code de l'expropriation pour cause d'utilité publique ; <br/>
<br/>
              Vu le code de l'urbanisme ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, M. A et, d'autre part, la Ville de Paris ;<br/>
<br/>
              Vu le procès-verbal de l'audience publique du 7 novembre 2011 à 12 heures au cours de laquelle ont été entendus :<br/>
<br/>
              - Me Chevallier, avocat au Conseil d'Etat et à la Cour de cassation, avocat de M. A ;<br/>
<br/>
              - le représentant de M. A ; <br/>
<br/>
              - Me Foussard, avocat au Conseil d'Etat et à la Cour de cassation, avocat de la Ville de Paris ;<br/>
<br/>
              - les représentants de la Ville de Paris ;<br/>
<br/>
              et à l'issue de laquelle l'instruction a été close ;<br/>
<br/>
<br/>
<br/>
              Considérant que le juge administratif ne peut être saisi d'une requête tendant à la mise en oeuvre de l'une des procédures régies par le livre V du code de justice administrative que pour autant que le litige principal auquel se rattache ou est susceptible de se rattacher la mesure d'urgence qu'il lui est demandé de prescrire n'échappe pas manifestement à la compétence de la juridiction administrative ; <br/>
<br/>
              Considérant qu'il résulte des dispositions de l'article L. 213-4 du code de l'urbanisme que, lorsque le titulaire du droit de préemption urbain a exercé ce droit en application de l'article L. 213-2 du même code, le prix d'acquisition est, à défaut d'accord amiable, fixé par la juridiction judiciaire compétente, selon les règles applicables en matière d'expropriation ; que, selon l'article L. 213-14 de ce code, le prix doit être réglé par le titulaire du droit de préemption ou, en cas d'obstacle au paiement, consigné, dans les six mois qui suivent la décision définitive de cette juridiction, faute de quoi le titulaire du droit de préemption est tenu, sur demande de l'ancien propriétaire, de lui rétrocéder le bien acquis par voie de préemption ;<br/>
<br/>
              Considérant que le litige opposant M. A à la Ville de Paris, et qui a conduit le requérant à saisir le juge des référés du tribunal administratif de Paris d'une demande sur le fondement des dispositions de l'article L. 521-2 du code de justice administrative, porte sur les modalités d'exécution d'un jugement du 16 février 2010, devenu définitif à la suite de l'arrêt rendu le 22 septembre 2011 par la cour d'appel de Paris, par lequel le juge de l'expropriation a fixé le prix d'un bien sur lequel la ville a exercé son droit de préemption urbain ; que, faisant grief à cette dernière d'avoir procédé à la consignation des sommes dues, au lieu de régulariser la vente dans les conditions fixées par ce juge, le requérant a demandé au juge administratif des référés d'ordonner à la Ville de Paris de procéder à la rétrocession prévue par les dispositions de l'article L. 213-14 du code de l'urbanisme ; <br/>
<br/>
              Considérant qu'ainsi que l'a jugé à bon droit le juge des référés du tribunal administratif de Paris, un tel litige, qui conduit à statuer sur les conditions de réalisation de la cession et sur le respect des modalités définies à cet effet par le juge de l'expropriation, notamment sur le bien-fondé de la consignation réalisée par la Ville de Paris, n'est pas détachable des procédures engagées devant l'autorité judiciaire à la suite du différend portant sur le prix du bien préempté ; qu'il est ainsi manifestement étranger à la compétence de la juridiction administrative ; que, par suite, M. A n'est pas fondé à soutenir que c'est à tort que, par l'ordonnance attaquée, le juge des référés du tribunal administratif de Paris a rejeté sa demande comme portée devant une juridiction incompétente pour en connaître ; que la requête, y compris ses conclusions tendant à l'application des dispositions de l'article L. 761-1 du code de justice administrative, doit en conséquence être rejetée ;<br/>
<br/>
              Considérant qu'il n'y pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par la Ville de Paris au titre de ces mêmes dispositions ;<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de M. A est rejetée.<br/>
Article 2 : Les conclusions présentées par la Ville de Paris au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La présente ordonnance sera notifiée à M. Henri A et à la Ville de Paris.<br/>
<br/>
<br/>
</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
