<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038462152</ID>
<ANCIEN_ID>JG_L_2019_05_000000428574</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/46/21/CETATEXT000038462152.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère chambre, 10/05/2019, 428574, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-05-10</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>428574</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Sandrine Vérité</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Rémi Decout-Paolini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2019:428574.20190510</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Par une requête, enregistrée le 3 mars 2019 au secrétariat du contentieux du Conseil d'Etat, la SAS Ecole internationale des vocations équestres, Mme A...E..., M. C... F...et Mme D...B...demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir l'arrêté du ministre du travail du 27 décembre 2018 portant enregistrement au répertoire national des certifications professionnelles, en tant qu'il désigne l'organisme délivrant la certification professionnelle " technicien dentaire équin " ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 1 000 euros à verser à chacun d'eux au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de l'éducation ; <br/>
              - le code rural et de la pêche maritime ;<br/>
              - le code du travail ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Sandrine Vérité, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Rémi Decout-Paolini, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes du II de l'article L. 335-6 du code de l'éducation, en vigueur à la date de la décision attaquée : " Il est créé un répertoire national des certifications professionnelles. Les diplômes et les titres à finalité professionnelle y sont classés par domaine d'activité et par niveau. / Les diplômes et titres à finalité professionnelle peuvent y être enregistrés à la demande des organismes ou instances les ayant créés et après avis de la Commission nationale de la certification professionnelle. (...) ". Aux termes de l'article R. 335-12 du même code, alors en vigueur : " Le répertoire national des certifications professionnelles contribue à faciliter l'accès à l'emploi, la gestion des ressources humaines et la mobilité professionnelle. Il permet de tenir à la disposition des personnes et des entreprises une information constamment mise à jour sur les diplômes et les titres à finalité professionnelle ainsi que sur les certificats de qualification établis par les commissions paritaires nationales de l'emploi des branches professionnelles. / Les certifications enregistrées dans le répertoire sont reconnues sur l'ensemble du territoire national. / L'enregistrement dans le répertoire national concerne la seule certification proprement dite ". Aux termes de l'article R. 335-13 du même code, alors en vigueur : " Les diplômes et titres à finalité professionnelle sont classés dans le répertoire national des certifications professionnelles par domaine d'activité et par niveau. (...) / Les certificats de qualification sont classés séparément par domaine d'activité. Le répertoire précise en outre leurs correspondances éventuelles avec des diplômes ou des titres professionnels. / Le répertoire mentionne les correspondances entre les certifications, ainsi que, lorsqu'elles sont explicitement prévues par les autorités qui les délivrent, les reconnaissances mutuelles, partielles ou totales. / (...) ". Enfin, aux termes de l'article R. 335-20 du même code, alors en vigueur : " L'enregistrement dans le répertoire national des diplômes, titres ou certificats de qualification mentionnés à l'article R. 335-16, leur modification éventuelle et le renouvellement ou la suppression de l'enregistrement sont prononcés par arrêté du ministre chargé de la formation professionnelle ".<br/>
<br/>
              2. Bien que participant de la politique publique de la formation professionnelle, la décision, dépourvue de caractère général et impersonnel, par laquelle le ministre chargé de la formation professionnelle, sur le fondement de l'article R. 335-20 du code de l'éducation, enregistre un diplôme, un titre ou un certificat de qualification dans le répertoire national des certifications professionnelles n'a pas, par elle-même, pour objet l'organisation d'un service public et ne revêt donc pas un caractère réglementaire. <br/>
<br/>
              3. Par suite, l'arrêté du ministre du travail du 27 décembre 2018 portant enregistrement au répertoire national des certifications professionnelles n'entre pas dans le champ du 2° de l'article R. 311-1 du code de justice administrative qui donne compétence au Conseil d'Etat pour connaître en premier et dernier ressort des recours dirigés contre les actes réglementaires des ministres. Aucune autre disposition du code de justice administrative ne donne compétence au Conseil d'Etat pour connaître en premier et dernier ressort des conclusions de la SAS Ecole internationale des vocations équestres et des autres requérants tendant à l'annulation de cette décision. Il y a lieu, en application de l'article R. 351-1 du code de justice administrative, d'en attribuer le jugement au tribunal administratif de Paris, compétent pour en connaître en vertu de l'article R. 312-1 du même code.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement de la requête de la SAS Ecole internationale des vocations équestres et des autres requérants est attribué au tribunal administratif de Paris. <br/>
Article 2 : La présente décision sera notifiée, pour l'ensemble des requérants, à la SAS Ecole internationale des vocations équestres, première dénommée, à la ministre du travail et au président du tribunal administratif de Paris.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
