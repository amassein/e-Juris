<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037158694</ID>
<ANCIEN_ID>JG_L_2018_07_000000401566</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/15/86/CETATEXT000037158694.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème chambre, 04/07/2018, 401566, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-07-04</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>401566</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MATUCHANSKY, POUPOT, VALDELIEVRE ; SCP GASCHIGNARD</AVOCATS>
<RAPPORTEUR>Mme Céline Roux</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Sophie-Justine Lieber</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:401566.20180704</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme B...A...a demandé au tribunal administratif de Rennes d'annuler pour excès de pouvoir l'arrêté du président de l'université de Bretagne-Sud du 17 septembre 2012 décidant son reclassement au 6ème échelon de la classe normale du corps des maîtres de conférences en tant qu'il fixe la date d'effet de ce reclassement au 1er janvier 2010. Par un jugement n° 1205025 du 12 mai 2014, le tribunal administratif a fait droit à sa demande et enjoint au président de cette université de fixer la date d'effet de son reclassement au 1er septembre 2009.<br/>
<br/>
              Par un arrêt n° 14NT01836 du 17 mai 2016, la cour administrative d'appel de Nantes a, sur appel de l'université de Bretagne-Sud, annulé ce jugement et rejeté la demande de Mme A....<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 18 juillet et 18 octobre 2016 et le 5 janvier 2018 au secrétariat du contentieux du Conseil d'Etat, Mme A...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de l'université de Bretagne-Sud ;<br/>
<br/>
              3°) de mettre à la charge de l'université de Bretagne-Sud la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'éducation ;<br/>
              - la loi n° 2009-1673 du 30 décembre 2009 ;<br/>
              - la loi n° 2010-1658 du 29 décembre 2010 ;<br/>
              - le décret n° 84-431 du 6 juin 1984 ;<br/>
              - le décret n° 2009-462 du 23 avril 2009 ;<br/>
              - l'arrêté du 10 février 2012 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Céline Roux, maître des requêtes en service extraordinaire,<br/>
<br/>
              - les conclusions de Mme Sophie-Justine Lieber, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Matuchansky, Poupot, Valdelièvre, avocat de Mme A...et à la SCP Gaschignard, avocat de l'université de Bretagne-Sud ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 25 juin 2018, présentée par MmeA....<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que Mme A..., qui a été titularisée dans le corps des maîtres de conférences en 2004, a demandé au président de l'université de Bretagne-Sud son reclassement dans ce corps par application des dispositions de l'article 125 de la loi du 30 décembre 2009 de finances pour 2010 ; que, par un arrêté du 17 septembre 2012, le président de cette université l'a reclassée au 6ème échelon de la classe normale de ce corps à compter du 1er janvier 2010 ; que Mme A...se pourvoit en cassation contre l'arrêt du 17 mai 2016 par lequel la cour administrative d'appel de Nantes a, sur appel de l'université de Bretagne-Sud, annulé le jugement du 12 mai 2014 par lequel le tribunal administratif de Rennes a annulé cet arrêté en tant qu'il ne prenait effet qu'au 1er janvier 2010 et a enjoint au président de l'université de procéder au reclassement de l'intéressée à compter du 1er septembre 2009 et rejeté sa demande ;<br/>
<br/>
              2. Considérant, en premier lieu, qu'aux termes de l'article 125 de la loi du 30 décembre 2009 : " Les maîtres de conférences régis par le décret n° 84-431 du 6 juin 1984 relatif au statut des enseignants-chercheurs de l'enseignement supérieur (...), titularisés dans leur corps avant le 1er septembre 2009, classés dans le premier grade et en fonctions à la date de publication de la présente loi, peuvent bénéficier, sur leur demande, d'une proposition de reclassement établie par application du décret n° 2009-462 du 23 avril 2009 (...), la durée des services accomplis depuis la date de leur recrutement et jusqu'au 31 août 2009 étant prise en compte dans la limite d'un an. Toutefois, l'ancienneté de service des intéressés dans leur corps continue à être décomptée à partir de la date à laquelle ils y ont accédé. / La demande doit être présentée dans un délai de six mois à compter de la date de publication de la présente loi (...) / L'administration leur communique une proposition de nouveau classement. Ils disposent alors d'un délai de deux mois pour faire connaître leur décision " ; que ces dispositions sont dépourvues de portée rétroactive ; qu'ainsi, la cour administrative d'appel de Nantes n'a pas commis d'erreur de droit en jugeant que les reclassements auxquels il a été procédé en application de l'article 125 de la loi du 30 décembre 2009 de finances pour 2010 ne pouvaient prendre effet qu'à compter du 1er janvier 2010, date d'entrée en vigueur de la loi ;<br/>
<br/>
              3. Considérant, en deuxième lieu, qu'en jugeant que Mme A...n'établissait pas que, pris dans son ensemble, le dispositif de reclassement était défavorable aux maîtres de conférence qui en sollicitaient l'application, en raison d'une prise d'effet de ce reclassement à compter du 1er janvier 2010 et non du 1er septembre 2009, la cour ne s'est pas fondée, contrairement à ce que soutient la requérante, sur des circonstances inopérantes et n'a, par suite, pas commis d'erreur de droit ;<br/>
<br/>
              4. Considérant, en troisième lieu, que la cour, qui a suffisamment motivé son arrêt, ne s'est pas méprise sur la portée des écritures des parties en écartant le moyen que Mme A... avait invoqué en première instance, tiré de la violation du principe d'égalité de traitement entre agents appartenant à un même corps ;<br/>
<br/>
              5. Considérant, en quatrième lieu, que si la requérante reproche à la cour d'avoir jugé que les dispositions législatives citées au point 2 n'ont pas pour effet d'inverser l'ordre d'ancienneté des fonctionnaires déjà en fonction, ce moyen est inopérant, dès lors qu'il porte sur un motif surabondant de l'arrêt de la cour ;<br/>
<br/>
              6. Considérant, en dernier lieu, que la cour, en jugeant que la requérante ne pouvait utilement se prévaloir, à l'appui de son moyen tiré de la méconnaissance du principe d'égalité entre fonctionnaires du corps auquel elle appartient, des dispositions de l'article 104 de la loi du 29 décembre 2010 de finances rectificative pour 2010, qui sont applicables aux seuls maîtres de conférences régis par le décret du 21 février 1992 portant statuts particuliers des corps d'enseignants-chercheurs des établissements d'enseignement supérieur publics relevant du ministre chargé de l'agriculture, n'a commis aucune erreur de droit ;<br/>
<br/>
              7. Considérant qu'il résulte de ce qui précède que les conclusions aux fins d'annulation de Mme A...doivent être rejetées ainsi, par voie de conséquence, que ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative ; qu'en outre, les dispositions de cet article font obstacle à ce que soient accueillies les conclusions tendant à leur application présentées par l'université de Bretagne-Sud, dès lors qu'elle n'est pas partie au litige ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de Mme A...est rejeté.<br/>
Article 2 : Les conclusions de l'université de Bretagne-Sud présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La présente décision sera notifiée à Mme B...A..., à la ministre de l'enseignement supérieur, de la recherche et de l'innovation et à l'université de Bretagne-Sud.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
