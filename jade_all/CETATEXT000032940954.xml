<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032940954</ID>
<ANCIEN_ID>JG_L_2016_07_000000391939</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/94/09/CETATEXT000032940954.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème chambre, 27/07/2016, 391939, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-07-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>391939</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Mireille Le Corre</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Xavier de Lesquen</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2016:391939.20160727</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme A... D...a demandé au tribunal administratif de Paris d'annuler pour excès de pouvoir l'arrêté du 30 octobre 2013 par lequel le préfet de police lui a retiré la carte de séjour temporaire dont elle était titulaire, a prononcé à son encontre une obligation de quitter le territoire français et a fixé le pays de destination. Par un jugement n° 1403138 du 30 juin 2014, le tribunal administratif a annulé cet arrêté.<br/>
<br/>
              Par un arrêt n° 14PA03646 du 19 mai 2015, la cour administrative d'appel de Paris a, sur l'appel du préfet de police, d'une part, annulé ce jugement, d'autre part, annulé l'arrêté du 30 octobre 2013.<br/>
<br/>
              Par un pourvoi, enregistré le 21 juillet 2015 au secrétariat du contentieux du Conseil d'Etat, le ministre de l'intérieur demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à l'appel de l'Etat.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code civil ;<br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Mireille Le Corre, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Xavier de Lesquen, rapporteur public ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des énonciations de l'arrêt attaqué que Mme A...D..., ressortissante nigériane née en 1984 qui, selon ses déclarations, serait entrée en France en août 2009, y a donné naissance en 2011 à un enfant dont la paternité a été reconnue par M. C... B..., qui est de nationalité française ; que Mme D...a obtenu en mai 2012 la délivrance d'une carte de séjour temporaire portant la mention " vie privée et familiale " en qualité de parent d'enfant français, renouvelée en mai 2013, sur le fondement des dispositions du 6° de l'article L. 313-11 du code de l'entrée et du séjour des étrangers et du droit d'asile ; que, toutefois, par un arrêté du 30 octobre 2013, le préfet de police a retiré le titre de séjour délivré en mai 2013, au motif que l'admission au séjour de Mme D... avait été obtenue par fraude et l'a obligée à quitter le territoire dans le délai de trente jours, en fixant le pays à destination duquel elle pouvait être éloignée d'office ; que, saisi par Mme D... d'un recours pour excès de pouvoir dirigé contre cet arrêté, le tribunal administratif de Paris, par un jugement du 30 juin 2014, l'a annulé et a enjoint au préfet de police de réexaminer la situation de l'intéressée ; que par un arrêt du 19 mai 2015, contre lequel le ministre de l'intérieur se pourvoit en cassation, la cour administrative d'appel de Paris a, après avoir annulé le jugement du 30 juin 2014, annulé l'arrêté préfectoral contesté ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article L. 313-11 du code de l'entrée et du séjour des étrangers et du droit d'asile : " Sauf si sa présence constitue une menace pour l'ordre public, la carte de séjour temporaire portant la mention " vie privée et familiale " est délivrée de plein droit : / (...) 6° A l'étranger ne vivant pas en état de polygamie, qui est père ou mère d'un enfant français mineur résidant en France, à la condition qu'il établisse contribuer effectivement à l'entretien et à l'éducation de l'enfant dans les conditions prévues par l'article 371-2 du code civil depuis la naissance de celui-ci ou depuis au moins deux ans, sans que la condition prévue à l'article L. 311-7 soit exigée (...) " ; qu'aux termes de l'article L. 623-1 du même code : " Le fait de contracter un mariage ou de reconnaître un enfant aux seules fins d'obtenir, ou de faire obtenir, un titre de séjour ou le bénéfice d'une protection contre l'éloignement, ou aux seules fins d'acquérir, ou de faire acquérir, la nationalité française est puni de cinq ans d'emprisonnement et de 15 000 euros d'amende (...). / Ces mêmes peines sont applicables en cas d'organisation ou de tentative d'organisation d'un mariage ou d'une reconnaissance d'enfant aux mêmes fins. / Elles sont portées à 10 ans d'emprisonnement et à 750 000 euros d'amende lorsque l'infraction est commise en bande organisée. " ;<br/>
<br/>
              3. Considérant que si un acte de droit privé opposable aux tiers est en principe opposable dans les mêmes conditions à l'administration tant qu'il n'a pas été déclaré nul par le juge judiciaire, il appartient cependant à l'administration, lorsque se révèle une fraude commise en vue d'obtenir l'application de dispositions de droit public, d'y faire échec même dans le cas où cette fraude revêt la forme d'un acte de droit privé ; que ce principe peut conduire l'administration, qui doit exercer ses compétences sans pouvoir renvoyer une question préjudicielle à l'autorité judiciaire, à ne pas tenir compte, dans l'exercice de ces compétences, d'actes de droit privé opposables aux tiers ; que tel est le cas pour la mise en oeuvre des dispositions du 6° de l'article L. 313-11 du code de l'entrée et du séjour des étrangers et du droit d'asile, qui n'ont pas entendu écarter l'application des principes ci-dessus rappelés ; que, par conséquent, si la reconnaissance d'un enfant est opposable aux tiers, en tant qu'elle établit un lien de filiation et, le cas échéant, en tant qu'elle permet l'acquisition par l'enfant de la nationalité française, dès lors que cette reconnaissance a été effectuée conformément aux conditions prévues par le code civil, et s'impose donc en principe à l'administration tant qu'une action en contestation de filiation n'a pas abouti, il appartient néanmoins au préfet, s'il est établi, lors de l'examen d'une demande de titre de séjour présentée sur le fondement du 6° de l'article L. 313-11 du code de l'entrée et du séjour des étrangers et du droit d'asile, que la reconnaissance de paternité a été souscrite dans le but de faciliter l'obtention de la nationalité française ou d'un titre de séjour, de faire échec à cette fraude et de refuser, sous le contrôle du juge de l'excès de pouvoir, tant que la prescription prévue par les articles 321 et 335 du code civil n'est pas acquise, la délivrance de la carte de séjour temporaire sollicitée par la personne se présentant comme père ou mère d'un enfant français ; <br/>
<br/>
              4. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond, et notamment des rapports d'enquête de police transmis à l'autorité judiciaire, que M.B..., auteur de la reconnaissance de paternité de l'enfant de MmeD..., a reconnu, au fil des années, la paternité de 24 enfants ; que 6 des 7 mères de ces enfants qui ont pu être entendues par la police, sur la vingtaine de femmes concernées, ont reconnu le caractère frauduleux de ces déclarations de paternité, obtenues en échanges de rémunérations ou de relations sexuelles, aux fins d'obtenir une régularisation de leur situation administrative sur le territoire français ; que MmeD..., qui est au nombre des femmes interrogées, a indiqué se livrer à la prostitution et ne pas savoir qui était le père biologique de son enfant ; qu'elle a déclaré que la reconnaissance de celui-ci par M. B...avait été obtenue en échange de relations sexuelles, ainsi que l'ont d'ailleurs relevé les juges du fond ; que, par suite, en jugeant, en dépit de ces éléments précis et concordants, que le caractère frauduleux de la reconnaissance de paternité de l'enfant de Mme D... par M. B... n'était pas établi, la cour administrative d'appel de Paris a dénaturé les pièces du dossier ; que le ministre de l'intérieur est fondé, pour ce motif, à demander l'annulation de l'arrêt qu'il attaque ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : L'arrêt du 19 mai 2015 de la cour administrative d'appel de Paris est annulé.<br/>
<br/>
		Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Paris.<br/>
Article 3 : La présente décision sera notifiée au ministre de l'intérieur et à Mme A...D....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
