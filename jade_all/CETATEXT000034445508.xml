<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034445508</ID>
<ANCIEN_ID>JG_L_2017_04_000000398473</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/44/55/CETATEXT000034445508.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème - 2ème chambres réunies, 19/04/2017, 398473, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-04-19</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>398473</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème - 2ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP WAQUET, FARGE, HAZAN</AVOCATS>
<RAPPORTEUR>M. Thomas Odinot</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Gilles Pellissier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:398473.20170419</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B...A...a demandé au tribunal administratif de Melun d'annuler la décision du 22 novembre 2013 par laquelle le préfet du Val-de-Marne a refusé de faire droit à sa demande de regroupement familial au profit de son épouse, ensemble la décision du 15 janvier 2014 par laquelle le ministre de l'intérieur a rejeté son recours hiérarchique. Par un jugement n° 1401733 du 7 mai 2015, le tribunal administratif a rejeté cette demande.<br/>
<br/>
              Par une ordonnance n°s 15PA02398, 15PA04441 du 21 décembre 2015, le président de la 8ème chambre de la cour administrative d'appel de Paris a rejeté l'appel de M. A... tendant à l'annulation de ce jugement.<br/>
<br/>
              Par un pourvoi et un mémoire complémentaire, enregistrés les 4 avril et 10 mai 2016 au secrétariat du contentieux du Conseil d'Etat, M. A...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'ordonnance attaquée ;<br/>
<br/>
              2°) de renvoyer l'affaire devant la cour administrative d'appel de Paris ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 000 euros à verser à la SCP Waquet, Farge, Hazan, avocat de M.A..., au titre des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991. <br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - le décret n° 91-1266 du 19 décembre 1991 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Thomas Odinot, auditeur,  <br/>
<br/>
              - les conclusions de M. Gilles Pellissier, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Waquet, Farge, Hazan, avocat de M.A....<br/>
<br/>
<br/>
<br/>
              1. Considérant, d'une part, qu'aux termes de l'article R. 222-1 du code de justice administrative : " Les présidents de tribunal administratif et de cour administrative d'appel, le vice-président du tribunal administratif de Paris et les présidents de formation de jugement des tribunaux et des cours peuvent, par ordonnance : (...) / 4° Rejeter les requêtes manifestement irrecevables, lorsque la juridiction n'est pas tenue d'inviter leur auteur à les régulariser ou qu'elles n'ont pas été régularisées à l'expiration du délai imparti par une demande en ce sens (...) " ; que l'article R. 612-1 du même code précise que : " Lorsque des conclusions sont entachées d'une irrecevabilité susceptible d'être couverte après l'expiration du délai de recours, la juridiction ne peut les rejeter en relevant d'office cette irrecevabilité qu'après avoir invité leur auteur à les régulariser. / Toutefois, la juridiction d'appel ou de cassation peut rejeter de telles conclusions sans demande de régularisation préalable pour les cas d'irrecevabilité tirés de la méconnaissance d'une obligation mentionnée dans la notification de la décision attaquée conformément à l'article R. 751-5 (...) " ; que l'article R. 811-7 du même code dispose que : " Les appels ainsi que les mémoires déposés devant la cour administrative d'appel doivent être présentés, à peine d'irrecevabilité, par l'un des mandataires mentionnés à l'article R. 431- 2 (...) " ;<br/>
<br/>
              2. Considérant, d'autre part, que la loi du 10 juillet 1991 relative à l'aide juridique prévoit, en son article 2, que les personnes physiques dont les ressources sont insuffisantes pour faire valoir leurs droits en justice peuvent bénéficier d'une aide juridictionnelle et, en son article 25, que le bénéficiaire de l'aide juridictionnelle a droit à l'assistance d'un avocat choisi par lui ou, à défaut, désigné par le bâtonnier de l'ordre des avocats ; qu'il résulte des articles 76 et 77 du décret du 19 décembre 1991 portant application de cette loi que si la personne qui demande l'aide juridictionnelle ne produit pas de document attestant l'acceptation d'un avocat choisi par elle, l'avocat peut être désigné sur-le-champ par le représentant de la profession qui siège au bureau d'aide juridictionnelle, à condition qu'il ait reçu délégation du bâtonnier à cet effet ; qu'enfin, l'article 39 du même décret dispose que, lorsque l'aide juridictionnelle a été sollicitée à l'occasion d'une instance devant une juridiction administrative statuant à charge de recours devant le Conseil d'Etat avant l'expiration du délai imparti pour le dépôt des mémoires, ce délai est interrompu et " un nouveau délai court à compter du jour de la réception par l'intéressé de la notification de la décision du bureau d'aide juridictionnelle ou, si elle est plus tardive, de la date à laquelle un auxiliaire de justice a été désigné " ;<br/>
<br/>
              3. Considérant qu'il ressort des pièces de la procédure d'appel que M. A..., qui avait été régulièrement informé par la lettre de notification du jugement attaqué de l'obligation de recourir au ministère d'avocat pour faire appel de ce jugement, a introduit sa requête dans le délai de recours sans le ministère d'un avocat en demandant le bénéfice de l'aide juridictionnelle ; que le bureau d'aide juridictionnelle près le tribunal de grande instance de Paris a fait droit à sa demande et désigné un avocat pour le représenter ; que si cet avocat a produit un mémoire après l'expiration du nouveau délai de deux mois prévu par l'article 39 du décret du 19 décembre 1991, la requête de M. A...avait, quant à elle, été introduite dans le délai d'appel de deux mois prévu par les dispositions de l'article L. 811-2 du code de justice administrative ; qu'ainsi, en rejetant sa requête comme tardive, le président de la 8ème chambre de la cour administrative d'appel de Paris a commis une erreur de droit ; que son ordonnance doit dès lors être annulée ;<br/>
<br/>
              4. Considérant que M. A...a obtenu devant le Conseil d'Etat le bénéfice de l'aide juridictionnelle ; que, par suite, son avocat peut se prévaloir des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 ; qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 2 000 euros à verser à la SCP Waquet, Farge, Hazan, sous réserve qu'elle renonce à percevoir la somme correspondant à la part contributive de l'Etat.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'ordonnance du président de la 8ème chambre de la cour administrative d'appel de Paris du 21 décembre 2015 est annulée.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Paris. <br/>
Article 3 : L'Etat versera à la SCP Waquet, Farge, Hazan, avocat de M.A..., une somme de 2 000 euros en application des dispositions du deuxième alinéa de l'article 37 de la loi du 10 juillet 1991, sous réserve que cette société renonce à percevoir la somme correspondant à la part contributive de l'Etat. <br/>
Article 4 : La présente décision sera notifiée à M. B...A...et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
