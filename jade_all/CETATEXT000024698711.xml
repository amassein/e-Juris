<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000024698711</ID>
<ANCIEN_ID>JG_L_2011_10_000000339154</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/24/69/87/CETATEXT000024698711.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème et 9ème sous-sections réunies, 19/10/2011, 339154</TITRE>
<DATE_DEC>2011-10-19</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>339154</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème et 9ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Christian Vigouroux</PRESIDENT>
<AVOCATS>SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR>M. Tanneguy Larzul</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Delphine Hedary</COMMISSAIRE_GVT>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête sommaire et le mémoire complémentaire, enregistrés les 3 mai et 15 juillet 2010 au secrétariat du contentieux du Conseil d'Etat, présentés pour la SOCIETE APPLE INC, dont le siège est 1 Infinite Loop à Ca 95014 Cupertino, Etats-Unis et la SOCIETE I TUNES SARL, dont le siège est 8 rue Heinrich Heine à Luxembourg (L 1720), Grand Duché du Luxembourg ; la SOCIETE APPLE INC et la SOCIETE I TUNES SARL demandent au Conseil d'Etat d'annuler le décret n° 2009-1773 du 29 décembre 2009 relatif à l'organisation de la Haute Autorité pour la diffusion des oeuvres et la protection des droits sur internet (HADOPI) ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le traité sur le fonctionnement de l'Union européenne, notamment ses articles 34 et 56 ;<br/>
<br/>
              Vu la directive 91/250/CEE du Conseil, du 14 mai 1991, concernant la protection juridique des programmes d'ordinateur ;<br/>
<br/>
              Vu la directive 98/34/CE du Parlement européen et du Conseil, du 22 juin 1998 prévoyant une procédure d'information dans le domaine des normes et règlementations techniques ;<br/>
<br/>
              Vu la directive n° 2001/29/CE du Parlement européen et du Conseil du 22 mai 2001 sur l'harmonisation de certains aspects du droit d'auteur et des droits voisins dans la société de l'information ;<br/>
<br/>
              Vu le code de la propriété intellectuelle ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Tanneguy Larzul, Conseiller d'Etat,<br/>
<br/>
              - les observations de la SCP Piwnica, Molinié, avocat de la SOCIETE APPLE INC et de la SOCIETE I TUNES SARL,<br/>
<br/>
              - les conclusions de Mme Delphine Hedary, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Piwnica, Molinié, avocat de la SOCIETE APPLE INC et de la SOCIETE I TUNES SARL ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant que l'article L. 331-12 du code de la propriété intellectuelle dispose que : " La Haute Autorité pour la diffusion des oeuvres et la protection des droits sur internet est une autorité publique indépendante. A ce titre, elle est dotée de la personnalité morale " ; qu'en application de l'article L. 331-13, la Haute autorité, qui se substitue à l'Autorité de régulation des mesures techniques, assure notamment une mission " de régulation et de veille dans le domaine des mesures techniques de protection et d'identification des oeuvres et des objets protégés par un droit d'auteur ou par un droit voisin " ; que pour garantir l'interopérabilité des systèmes et des services existants, dans le respect des droits des parties, l'article L. 331-32 du même code confère à la Haute autorité des pouvoirs de médiation entre d'une part les éditeur de logiciel, les fabricants de système techniques et les exploitants de service, et d'autre part, les titulaires des droits sur les mesures techniques ; qu'à défaut d'accord entre les parties, la Haute autorité peut prononcer " une injonction prescrivant, au besoin sous astreinte, les conditions dans lesquelles le demandeur peut obtenir l'accès aux informations essentielles à l'interopérabilité et les engagements qu'il doit respecter pour garantir l'efficacité et l'intégrité de la mesure technique, ainsi que les conditions d'accès et d'usage du contenu protégé " ; que la Haute autorité a également le pouvoir d'infliger " une sanction pécuniaire applicable soit en cas d'inexécution de ses injonctions, soit en cas de non-respect des engagements qu'elle a acceptés " ; qu'enfin en application de l'article L. 331-30 du code de la propriété intellectuelle dans sa rédaction issue de l'article 12 de la loi n° 2009-1311 du 28 octobre 2009, le décret attaqué énonce les règles applicables à la procédure et à l'instruction des dossiers devant le collège et la commission de protection des droits de la Haute autorité ;<br/>
<br/>
              Sur le moyen tiré de l'irrégularité de la consultation du Conseil d'Etat :<br/>
<br/>
              Considérant que le décret attaqué a été pris après consultation de la section de l'intérieur du Conseil d'Etat ; qu'il ressort des pièces du dossier que ce décret ne contient aucune disposition différant à la fois de celles qui figuraient dans le projet soumis par le gouvernement au Conseil d'Etat et de celles qui ont été adoptées par le Conseil d'Etat; que dès lors, les sociétés requérantes ne sont pas fondées à soutenir que la consultation du Conseil d'Etat sur le décret attaqué aurait été entachée d'irrégularité ;<br/>
<br/>
              Sur la méconnaissance de l'obligation de transmission à la Commission prévue par la directive 98-34 du Parlement européen et du Conseil du 22 juin 1998 :<br/>
<br/>
              Considérant qu'aux termes de l'article 8-1 de la directive 98/34/CE du 22 juin 1998 : " Sous réserve de l'article 10, les Etats membres communiquent immédiatement à la Commission tout projet de règle technique, sauf s'il s'agit d'une simple transposition intégrale d'une norme internationale ou européenne, auquel cas une simple information quant à la norme concernée suffit " ; qu'il résulte de l'article 1 paragraphe 9 de la directive qu'une " règle technique " est une " une spécification technique ou autre exigence, y compris les dispositions administratives qui s'y appliquent, dont l'observation est obligatoire, de jure ou de facto, pour la commercialisation ou l'utilisation dans un Etat membre ", et du paragraphe 10 du même article 1, que doit être regardé comme un " projet de règle technique " " le texte d'une spécification technique ou d'une autre exigence, y compris les dispositions administratives, qui est, avec l'intention de l'établir ou de la faire finalement établir comme une règle technique et qui se trouve à un stade de préparation où il est encore possible d'y apporter des amendements substantiels " ; que le décret contesté qui se borne, d'une part, à prescrire les dispositions relatives à l'agrément des personnes habilitées à procéder à des constatations permettant de caractériser une infraction aux dispositions protégeant le droit d'auteur et les droits voisins, et d'autre part, à fixer les règles relatives à l'organisation de la Haute autorité, ne comporte aucune règle technique et ne constitue pas par lui même un projet de " règle technique " au sens de la directive précitée, dont les dispositions de l'article 8-1 n'ont pas été méconnues ; que la circonstance que les lois du 1er août 2006 et du 12 juin 2009 n'auraient elles mêmes pas fait l'objet de la notification prévue à l'article 8-1, est, en tout état de cause, sans incidence sur la légalité du décret attaqué ;<br/>
<br/>
              Sur la violation des articles L. 331-37 du code de la propriété intellectuelle et 21 de la Constitution :<br/>
<br/>
              Considérant que les dispositions attaquées du 13°) du I de l'article R. 331-4 se bornent, conformément à l'habilitation législative de l'article L. 331-30, à préciser qu'il revient au collège de la Haute Autorité pour la diffusion des oeuvres et la protection des droits sur internet (HADOPI) d'adopter les règles de procédures gouvernant sa saisine dans le cadre de l'article L. 331-32 du code ; que ces dispositions n'ont ainsi ni pour effet ni pour objet d'édicter des règles de fond en matière d'interopérabilité ou d'en confier l'élaboration au collège de la HADOPI ; que dès lors le moyen tiré de ce que les dispositions attaquées méconnaîtraient l'article 21 de la Constitution ou l'habilitation donnée par l'article L. 331-37 ne peut qu'être écartés ;<br/>
<br/>
              Sur la violation de la directive 2001/29/CE du 22 mai 2001 et de la directive du 14 mai 1991 du Parlement européen et du Conseil, concernant la protection juridique des programmes d'ordinateur :<br/>
<br/>
              Considérant que les dispositions attaquées n'ayant ni pour objet ni pour effet d'assurer la mise en oeuvre des mesures adoptées par le législateur à l'article L. 331-32 du code de la propriété intellectuelle pour imposer l'interopérabilité des mesures techniques de protection, mais seulement d'attribuer au collège de la Haute autorité compétence pour adopter des règles procédurales permettant l'exercice des pouvoirs reconnus par le législateur en cas de désaccord entre parties sur l'interopérabilité des mesures techniques, le moyen tiré de ce que l'article L. 331-32 du code de la propriété intellectuelle définissant les devoirs des différentes parties en matière d'interopérabilité méconnait les articles 5 et 6 de la directive du 22 mai 2001 et la directive du 14 mai 1991 est sans incidence sur la légalité du décret attaqué ;<br/>
<br/>
              Sur la méconnaissance des stipulations des articles 56 et 34 du traité sur le fonctionnement de l'Union européenne :<br/>
<br/>
              Considérant que les dispositions attaquées, relatives aux compétences d'une autorité interne pour mettre en oeuvre les dispositions assurant l'exacte transposition d'une directive, ne méconnaissent en rien les articles 34 et 56 du traité sur le fonctionnement de l'Union européenne ;<br/>
<br/>
              Considérant qu'il résulte de ce qui précède que les requérants ne sont pas fondés à demander l'annulation du décret attaqué ;<br/>
<br/>
<br/>
<br/>				D E C I D E  :<br/>
              				--------------<br/>
<br/>
Article 1er : La requête de la SOCIETE APPLE INC et de la SOCIETE I TUNES SARL est rejetée.<br/>
<br/>
Article 2 : La présente décision sera notifiée à la SOCIETE APPLE INC, à la SOCIETE I TUNES SARL, au Premier ministre, au ministre de la culture et de la communication, au garde des sceaux, ministre de la justice et des libertés, au ministre de l'économie, des finances et de l'industrie et à la Haute autorité pour la diffusion des oeuvres et la protection des droits sur internet.<br/>
<br/>
</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">15-05-01-04 COMMUNAUTÉS EUROPÉENNES ET UNION EUROPÉENNE. RÈGLES APPLICABLES. LIBERTÉ DE CIRCULATION. LIBRE PRESTATION DE SERVICES. - RÈGLE TECHNIQUE - NOTION - EXCLUSION - DISPOSITIONS D'UN DÉCRET SE RAPPORTANT À L'ORGANISATION OU AU FONCTIONNEMENT D'UN ORGANISME CONNAISSANT DE RÈGLES TECHNIQUES.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">26-04-03 DROITS CIVILS ET INDIVIDUELS. DROIT DE PROPRIÉTÉ. PROPRIÉTÉ LITTÉRAIRE ET ARTISTIQUE. - DÉCRET RELATIF À L'ORGANISATION DE L'HADOPI - CARACTÈRE DE RÈGLE TECHNIQUE DEVANT ÊTRE COMMUNIQUÉE À LA COMMISSION EUROPÉENNE - ABSENCE.
</SCT>
<ANA ID="9A"> 15-05-01-04 Un décret qui se rapporte à l'organisation ou au fonctionnement d'un organisme (en l'espèce, la Haute Autorité pour la diffusion des oeuvres et la protection des droits sur internet) connaissant de règles techniques ne comporte pas, de ce seul fait, de règles techniques et ne constitue pas par lui-même un projet de règle technique au sens de la directive 98/34/CE du Parlement européen et du Conseil du 22 juin 1998. Il n'a donc pas à être communiqué, préalablement à son adoption, à la Commission européenne en application de l'article 8-1 de cette directive.</ANA>
<ANA ID="9B"> 26-04-03 Le décret n° 2009-1773 relatif à l'organisation de la Haute Autorité pour la diffusion des oeuvres et la protection des droits sur internet (HADOPI) qui se borne, d'une part, à prescrire les dispositions relatives à l'agrément des personnes habilitées à procéder à des constatations permettant de caractériser une infraction aux dispositions protégeant le droit d'auteur et les droits voisins et, d'autre part, à fixer les règles relatives à l'organisation de la Haute autorité, ne comporte aucune règle technique et ne constitue pas par lui même un projet de « règle technique » au sens de la directive 98/34/CE du Parlement européen et du Conseil du 22 juin 1998. Le projet de décret n'avait donc pas à être communiqué à la Commission européenne en application de l'article 8-1 de cette directive.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
