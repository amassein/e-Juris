<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043113680</ID>
<ANCIEN_ID>JG_L_2021_02_000000448983</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/11/36/CETATEXT000043113680.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 01/02/2021, 448983, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-02-01</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>448983</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2021:448983.20210201</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, enregistrée le 21 janvier 2021 au secrétariat du contentieux du Conseil d'Etat, l'Union nationale des syndicats CGT de la protection judiciaire de la jeunesse demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative :<br/>
<br/>
              1°) de " prononcer la nullité " de la note du 24 décembre 2020 de la secrétaire générale du ministère de la justice relative au report de jours de congés non pris au titre de l'année 2020 en tant qu'elle dispose que le seuil des 20 jours de congés qui doivent avoir été pris dans l'année pour pouvoir alimenter un compte épargne-temps concerne les congés annuels et les jours de fractionnement ;<br/>
<br/>
              2°) d'enjoindre au ministre de la justice de rétablir les agents dans leurs droits en leur donnant la possibilité d'inscrire rétroactivement, au titre de l'année 2020, sur leur compte épargne-temps le maximum de 20 jours prévu par l'arrêté du 11 mai 2020 relatif à la mise en oeuvre de dispositions temporaires en matière de compte épargne-temps dans la fonction publique de l'Etat et dans la magistrature afin de faire face aux conséquences de l'épidémie de covid-19.<br/>
<br/>
<br/>
              Il soutient que :<br/>
              - il justifie d'un intérêt pour agir au titre de sa mission statutaire ;<br/>
              - la condition d'urgence est satisfaite dès lors, d'une part, que la limitation par la note litigieuse de la nature des jours de congés devant avoir été pris au cours de l'année 2020 pour pouvoir alimenter un compte épargne-temps porte une atteinte grave et immédiate aux intérêts des agents de la protection judiciaire de la jeunesse exerçant leurs missions en dispositifs d'hébergement ou de détention du fait de l'iniquité de traitement entre fonctionnaires d'un même corps qui en résulte concernant le bénéfice du plafond dérogatoire de 20 jours prévu par l'arrêté du 11 mai 2020 et, d'autre part, que l'adoption et la transmission tardives de cette mesure sont susceptibles d'entraîner la perte de jours de congés pour certains agents si des dispositions contraires ne sont pas adoptées avant le 31 janvier 2021 ;<br/>
              - en précisant que le seuil de 20 jours de congés ouvrant droit à l'alimentation du compte épargne-temps concerne les congés annuels et les jours de fractionnement, et non les congés de toute nature comme indiqué dans une précédente note du 9 décembre 2020, il est porté, par la note contestée, une atteinte grave et manifestement illégale au principe d'égalité ;<br/>
              - la restriction de la nature des congés pouvant être pris en compte, alors que les textes réglementaires applicables ne comportent pas de précisions en la matière, lèse en effet les agents de la protection judiciaire de la jeunesse exerçant en hébergements et en services éducatifs auprès d'établissements pénitentiaires pour mineurs, faute de pouvoir inscrire sur leur compte épargne-temps jusqu'à 20 jours alors même qu'ils ont été davantage mobilisés et donc empêchés de prendre des congés du fait de la mise en oeuvre du plan de continuité de l'activité lors de la première période de confinement.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le décret n° 2002-634 du 29 avril 2002 ;<br/>
              - l'arrêté du 28 août 2009 pris pour l'application du décret n° 2002-634 du 29 avril 2002 modifié portant création du compte épargne-temps dans la fonction publique de l'Etat et dans la magistrature ;<br/>
              - l'arrêté du 30 décembre 2009 relatif à la mise en oeuvre du compte épargne-temps pour les agents du ministère de la justice et des libertés, de la grande chancellerie de la Légion d'honneur et pour les magistrats de l'ordre judiciaire ;<br/>
              - l'arrêté du 11 mai 2020 relatif à la mise en oeuvre de dispositions temporaires en matière de compte épargne-temps dans la fonction publique de l'Etat et dans la magistrature afin de faire face aux conséquences de l'épidémie de covid-19 ;<br/>
              - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ; <br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 521-2 du même code : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. (...) ". En vertu de l'article L. 522-3 du même code, le juge des référés peut, par une ordonnance motivée, rejeter une requête sans instruction ni audience lorsque la condition d'urgence n'est pas remplie ou lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable ou qu'elle est mal fondée.<br/>
<br/>
              2. D'une part, aux termes du premier alinéa de l'article 3 du décret du 29 avril 2002 portant création du compte épargne-temps dans la fonction publique de l'Etat et dans la magistrature, le compte épargne-temps est alimenté par le report de jours de réduction du temps de travail et par le report de congés annuels " sans que le nombre de jours de congés pris dans l'année puisse être inférieur à 20 ". D'autre part, en vertu de l'arrêté du 11 mai 2020 relatif à la mise en oeuvre de dispositions temporaires en matière de compte épargne-temps dans la fonction publique de l'Etat et dans la magistrature afin de faire face aux conséquences de l'épidémie de covid-19, le plafond annuel de jours pouvant être inscrits sur un compte épargne-temps est porté, au titre de l'année 2020, de 10 à 20 jours.<br/>
<br/>
              3. Si par la note litigieuse du 24 décembre 2020, la secrétaire générale du ministère de la justice a entendu rappeler les principales règles et dérogations applicables en matière de report de jours de congés non pris au titre de l'année 2020, elle précise que le seuil de 20 jours de congés qui doivent avoir été pris dans l'année pour ouvrir droit à l'alimentation d'un compte épargne-temps, tel que prévu par les dispositions précitées de l'article 3 du décret du 29 avril 2002, concerne les " congés annuels et les jours de fractionnement ". L'Union nationale des syndicats CGT de la protection judiciaire de la jeunesse, qui fait valoir qu'une précédente note du 9 décembre 2020 mentionnait les " congés de toute nature ", soutient que la limitation de la nature des congés pouvant être pris en compte pour apprécier la condition de seuil lèse les agents de la protection judiciaire de la jeunesse exerçant au sein de services d'hébergements et de services éducatifs auprès d'établissements pénitentiaires pour mineurs, en raison de l'impossibilité, du fait de ce mode de décompte, de pouvoir inscrire sur leur compte épargne-temps jusqu'à 20 jours comme l'autorise, pour l'année 2020, l'arrêté du 11 mai 2020.<br/>
<br/>
              4. A l'appui de sa requête, l'organisation requérante se borne à invoquer l'atteinte portée au principe d'égalité. Par ailleurs, selon ses propres déclarations, l'atteinte alléguée consisterait dans la limitation à 15 jours maximum du nombre de jours pouvant être inscrits par les agents concernés sur leur compte épargne-temps au titre de l'année 2020, sans pouvoir aller, comme envisagé pour d'autres agents relevant du même ministère, jusqu'aux 20 jours permis par dérogation au plafond de 10 jours normalement applicable. Ces éléments ne permettent pas de caractériser une atteinte " grave " à une " liberté fondamentale " de nature à justifier que le juge des référés fasse usage, à très bref délai, des pouvoirs qu'il tient de l'article L. 521-2 du code de justice administrative.<br/>
<br/>
              5. Il résulte de tout ce qui précède que la requête de l'Union nationale des syndicats CGT de la protection judiciaire de la jeunesse doit être rejetée selon la procédure prévue par l'article L. 522-3 de ce code.<br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
              ------------------<br/>
<br/>
Article 1er : La requête de l'Union nationale des syndicats CGT de la protection judiciaire de la jeunesse est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à l'Union nationale des syndicats CGT de la protection judiciaire de la jeunesse.<br/>
Copie en sera adressée au garde des sceaux, ministre de la justice.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
