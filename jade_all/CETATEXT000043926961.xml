<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043926961</ID>
<ANCIEN_ID>JG_L_2021_07_000000439195</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/92/69/CETATEXT000043926961.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème - 7ème chambres réunies, 09/07/2021, 439195, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-07-09</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>439195</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème - 7ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Sophie-Caroline de Margerie</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Sophie Roussel</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:439195.20210709</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés les 28 février 2020 et 15 juin 2021 au secrétariat du contentieux du Conseil d'Etat, l'association de Défense de l'Environnement des Riverains de l'aéroport de Beauvais-Tillé, l'association Regroupement des organismes de sauvegarde de l'Oise et l'association contre les nuisances de l'aéroport de Tillé demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir l'article 1er de l'arrêté du secrétaire d'Etat auprès de la ministre de la transition écologique et solidaire, chargé des transports, du 26 décembre 2019 relatif aux restrictions d'exploitation de l'aérodrome de Beauvais-Tillé ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat le versement d'une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu :<br/>
              - le règlement (UE) n° 598/2014 du Parlement européen et du Conseil du 16 avril 2014 ;<br/>
              - le code de l'aviation civile ;   <br/>
              - le code de l'environnement ;    <br/>
              - le code des transports ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme B... A..., conseillère d'Etat, <br/>
<br/>
              - les conclusions de Mme Sophie Roussel, rapporteure publique ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 22 juin 2021, présentée par la ministre de la transition écologique.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Par un arrêté du 25 avril 2002, le ministre de l'équipement, des transports et du logement a instauré des restrictions d'exploitation de l'aérodrome de Beauvais-Tillé interdisant notamment à tout aéronef d'atterrir ou de décoller entre 0 heure et 5 heures, heures locales de départ ou d'arrivée sur l'aire de stationnement, et aux avions les plus bruyants d'atterrir ou décoller entre 22 heures et 7 heures du matin. Modifiant ces dispositions, l'arrêté du 26 décembre 2019 a prévu, en son article 1er, que le ministre chargé de l'aviation civile peut, au cas par cas, autoriser des dérogations à cette interdiction d'atterrissage nocturne, dans les conditions qu'il fixe. L'association de Défense de l'Environnement des Riverains de l'aéroport de Beauvais-Tillé, l'association Regroupement des organismes de sauvegarde de l'Oise et l'association contre les nuisances de l'aéroport de Tillé doivent être regardées comme demandant l'annulation pour excès de pouvoir de l'article 1er de cet arrêté.<br/>
<br/>
              2. Aux termes du II de l'article L. 110-1 du code de l'environnement, les autorités s'inspirent, dans le cadre des lois qui en définissent la portée, du " principe de non-régression, selon lequel la protection de l'environnement, assurée par les dispositions législatives et réglementaires relatives à l'environnement, ne peut faire l'objet que d'une amélioration constante, compte tenu des connaissances scientifiques et techniques du moment ".<br/>
<br/>
              3. Les dispositions de l'arrêté attaqué donnent au ministre chargé de l'aviation civile le pouvoir d'accorder, au cas par cas, aux aéronefs effectuant des vols réguliers de transport de passagers et performants d'un point de vue acoustique, dont le dernier atterrissage était prévu entre 21 heures et 23 heures et dont le décollage est prévu le lendemain après 5 heures, le droit d'atterrir la nuit sur l'aéroport de Beauvais, par dérogation à l'interdiction posée par l'arrêté du 25 avril 2002, sans que soit limité le nombre de ces autorisations dérogatoires. Faute pour l'administration, d'une part, d'avoir encadré le surcroît du trafic aérien nocturne qui pourrait résulter de l'octroi de ces dérogations et d'autre part, d'avoir indiqué les motifs d'intérêt général qui pourraient le cas échéant les justifier, les associations requérantes sont fondées à soutenir que l'arrêté attaqué méconnaît les dispositions du 9° du II de l'article L. 110-1 du code de l'environnement. posant le principe de non-régression de la protection de l'environnement.<br/>
<br/>
              4. Il résulte de ce qui précède, sans qu'il soit besoin d'examiner les autres moyens de la requête, que les associations requérantes sont fondées à demander l'annulation pour excès de pouvoir de l'article 1er de l'arrêté qu'elles attaquent.<br/>
<br/>
              5. Il y a lieu dans les circonstances de l'espèce, de mettre à la charge de l'Etat le versement d'une somme de 700 euros à chacune des associations requérantes au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : L'article 1er de l'arrêté du secrétaire d'Etat auprès de la ministre de la transition écologique et solidaire, chargé des transports, du 26 décembre 2019 relatif aux restrictions d'exploitation de l'aérodrome de Beauvais-Tillé est annulé.<br/>
Article 2 : L'Etat versera à l'association de Défense de l'Environnement des Riverains de l'aéroport de Beauvais-Tillé, à l'association Regroupement des organismes de sauvegarde de l'Oise et à l'association contre les nuisances de l'aéroport de Tillé la somme de 700 euros chacune au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée à l'association de Défense de l'Environnement des Riverains de l'aéroport de Beauvais-Tillé, à l'association Regroupement des organismes de sauvegarde de l'Oise, à l'association contre les nuisances de l'aéroport de Tillé et à la ministre de la transition écologique. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
