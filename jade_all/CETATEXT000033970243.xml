<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033970243</ID>
<ANCIEN_ID>JG_L_2017_02_000000396900</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/97/02/CETATEXT000033970243.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème chambre, 01/02/2017, 396900, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-02-01</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>396900</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MASSE-DESSEN, THOUVENIN, COUDRAY</AVOCATS>
<RAPPORTEUR>M. Jean Sirinelli</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Olivier Henrard</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2017:396900.20170201</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              Mme B...A...a demandé au tribunal administratif de Lyon, d'une part, d'annuler la décision implicite de rejet résultant du silence gardé par le ministre du budget, des comptes publics et de la fonction publique sur sa demande du 26 avril 2010 tendant à la reconnaissance de l'imputabilité au service de la détérioration de son état de santé et, d'autre part, de condamner l'Etat à lui verser la somme de 120 000 000 euros en réparation des préjudices qu'elle estime avoir subis du fait des fautes commises par l'administration à l'occasion de son admission à la retraite pour invalidité non imputable au service. Par un jugement n° 1004976 du 3 mai 2012, le tribunal administratif de Lyon a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 12LY02948 du 24 septembre 2013, la cour administrative d'appel de Lyon a rejeté l'appel de Mme A...contre ce jugement. <br/>
<br/>
              Par une décision n° 380702 du  27 juillet 2015, le Conseil d'Etat statuant au contentieux a annulé cet arrêt et renvoyé l'affaire à la cour administrative d'appel de Lyon. <br/>
<br/>
              Par un arrêt n° 15LY02734 du 10 décembre 2015, la cour administrative d'appel de Lyon a annulé le jugement du tribunal administratif de Lyon du 3 mai 2012 puis rejeté la demande présentée devant celui-ci par MmeA.... <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 10 février et 9 mai 2016 au secrétariat du contentieux du Conseil d'Etat, Mme A...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt en tant qu'il a rejeté sa demande présentée devant le tribunal administratif ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande présentée devant le tribunal administratif ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code des pensions civiles et militaires de retraite ;  <br/>
              - la loi n° 68-1250 du 31 décembre 1968 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean Sirinelli, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Olivier Henrard, rapporteur public. <br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Masse-Dessen, Thouvenin, Coudray, avocat de MmeA.... <br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, par un arrêté du 29 décembre 1989, modifié par un arrêté 1er juin 1990, MmeA..., professeur adjoint d'éducation physique et sportive, a été admise à la retraite, à compter du 12 décembre 1989, pour invalidité non imputable au service ; que ces arrêtés ayant été annulés par un jugement du tribunal administratif de Lille du 28 juin 1994, Mme A...a été à nouveau admise à la retraite, pour le même motif, par un arrêté du 25 septembre 1996 ; qu'une pension civile de retraite a été initialement concédée à Mme A...par arrêté du 11 décembre 1995 mais jamais mise en paiement ; que cette pension a été révisée par un arrêté du 9 juin 1997 puis, à la demande de l'intéressée, par un arrêté du 24 novembre 1997, avec effet au 25 septembre 1996 ; que ce dernier arrêté n'a pas été notifié par l'administration à Mme A...et ne l'a été que le 9 novembre 2007 par l'intermédiaire d'une assistante sociale sollicitée par l'intéressée ; que les arrérages de la pension civile de retraite ont été versés à cette dernière en mars 2009, pour la période comprise entre le 1er janvier 2003 et le 31 décembre 2008 ; qu'ils lui ont également été versés pour la période du 25 septembre 1996 au 31 décembre 2002, après la décision du ministre du budget, des comptes publics, de la fonction publique et de la réforme de l'Etat de ne pas opposer à la créance détenue par Mme A...la prescription prévue par l'article 1er de la loi du 31 décembre 1968 ; que Mme A...a contesté la légalité de l'arrêté du 24 novembre 1997, par une demande enregistrée le 23 juin 2008, rejetée par un jugement du tribunal administratif de Lyon du 24 mars 2011, devenu définitif ; que, par un jugement du 3 mai 2012, le tribunal administratif de Lyon a également rejeté sa demande tendant à la réparation des préjudices résultant du retard mis par l'administration à lui notifier son titre de pension du 24 novembre 1997 et à lui verser sa pension de retraite ; que, par un arrêt du 24 septembre 2013, la cour administrative d'appel de Lyon a rejeté l'appel formé par Mme A...contre ce jugement ; que, par une décision du 27 juillet 2015, le Conseil d'Etat statuant au contentieux a, d'une part, annulé cet arrêt et, d'autre part, renvoyé devant la cour le jugement de cette affaire ; que, par un arrêt du 10 décembre 2015, la cour administrative d'appel de Lyon a annulé le jugement du tribunal administratif de Lyon du 3 mai 2012 et, après avoir évoqué l'affaire, a rejeté les conclusions indemnitaires de Mme A...en jugeant que si l'administration avait commis une faute de nature à engager sa responsabilité en s'abstenant de lui notifier le titre de pension émis le 24 novembre 1997, ce qui avait fait obstacle à la mise en paiement de sa pension civile de retraite, les négligences fautives de Mme A...étaient toutefois de nature à exonérer totalement l'administration de sa responsabilité ; que la requérante se pourvoit en cassation contre cet arrêt en tant qu'il a rejeté la demande qu'elle a présentée devant le tribunal administratif de Lyon ; <br/>
<br/>
              2. Considérant qu'en jugeant que le fait que Mme A...se soit abstenue de fournir les déclarations nécessaires à la mise en paiement de la pension était constitutif d'une faute d'une gravité justifiant que l'administration soit exonérée de toute responsabilité dans le préjudice subi par l'intéressée en raison du retard de l'administration à lui notifier son titre de pension révisé et à lui payer les arrérages de sa pension de retraite, la cour administrative d'appel de Lyon a inexactement qualifié les faits qui lui étaient soumis ; <br/>
<br/>
              3. Considérant qu'il résulte de ce qui précède, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, que Mme A...est fondée à demander l'annulation de l'article 2 de l'arrêt qu'elle attaque ; <br/>
<br/>
              4. Considérant qu'aux termes du second alinéa de l'article L. 821-2 du code de justice administrative : " Lorsque l'affaire fait l'objet d'un second pourvoi en cassation, le Conseil d'Etat statue définitivement sur cette affaire " ; qu'il y a lieu, par suite, de régler l'affaire au fond ;<br/>
<br/>
              5. Considérant, d'une part, que les recours formés par Mme A...contre l'arrêté du ministre de l'éducation nationale du 25 octobre 1995 l'admettant à la retraite et le titre de pension du 24 novembre 1997 ont été rejetés respectivement par un arrêt de la cour administrative d'appel de Douai du 13 décembre 2005 et un jugement du tribunal administratif de Lyon du 24 mars 2011, devenus définitifs ; que Mme A...n'est dès lors pas fondée à demander l'indemnisation des préjudices que lui aurait causés la prétendue illégalité de ces décisions ; qu'elle ne l'est pas davantage à demander l'indemnisation de la perte de revenus que lui a causée l'édiction tardive du titre de pension du 11 décembre 1995 et l'absence de mise en paiement de sa pension jusqu'au 24 septembre 1996 dès lors que sa demande, ayant le même objet, a été rejetée par le même arrêt du 13 décembre 2005 ;<br/>
<br/>
              6. Considérant, d'autre part, qu'il résulte de l'instruction que l'administration, en ne notifiant pas à Mme A...le titre de pension du 24 novembre 1997 et en tardant, par suite, à lui verser sa pension de retraite, a commis une faute de nature à engager sa responsabilité ; <br/>
<br/>
              7. Considérant, ainsi qu'il a été dit au point 1, que Mme A...a perçu l'intégralité des arrérages de la pension concédée par l'arrêté du 24 novembre 1997 ; qu'il sera fait, dans les circonstances de l'espèce et eu égard aux justifications apportées par la requérante, une juste appréciation du préjudice matériel et moral subi par celle-ci, en raison des troubles de toute nature dans ses conditions d'existence et son état de santé occasionnés par le retard mis par l'administration à lui notifier son titre de pension et à procéder au versement de cette dernière, en l'évaluant à 3 000 euros ; que toutefois, MmeA..., en s'abstenant de fournir à l'administration les déclaration nécessaires à la mise en paiement de sa pension et d'engager toute démarche pour obtenir celle-ci durant plus de dix ans,  doit être regardée comme ayant commis une négligence de nature à exonérer l'administration à hauteur de la moitié de sa responsabilité ; qu'il y a lieu, par suite, de condamner l'Etat à lui verser une somme de 1 500 euros ;<br/>
<br/>
              8. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 3 000 euros à verser à MmeA..., au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'article 2 de l'arrêt de la cour administrative d'appel de Lyon du 10 décembre 2015 est annulé.<br/>
<br/>
Article 2 : L'Etat est condamné à verser à Mme A...la somme de 1 500 euros. <br/>
Article 3 : L'Etat versera une somme de 3 000 euros à Mme A...au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Le surplus de la demande présentée par Mme A...devant le tribunal administratif de Lyon est rejeté. <br/>
Article 5 : La présente décision sera notifiée à Mme B...A...et au ministre de l'économie et des finances.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
