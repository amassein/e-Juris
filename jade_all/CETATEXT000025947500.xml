<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000025947500</ID>
<ANCIEN_ID>JG_L_2012_05_000000354186</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/25/94/75/CETATEXT000025947500.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère et 6ème sous-sections réunies, 31/05/2012, 354186, Publié au recueil Lebon</TITRE>
<DATE_DEC>2012-05-31</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>354186</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère et 6ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LYON-CAEN, THIRIEZ ; SCP MASSE-DESSEN, THOUVENIN</AVOCATS>
<RAPPORTEUR>M. Rémi Decout-Paolini</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Claire Landais</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2012:354186.20120531</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 21 novembre et 6 décembre 2011 au secrétariat du contentieux du Conseil d'Etat, présentés pour la SOCIETE AVIS LOCATION DE VOITURES, dont le siège est 5-6, place de l'Iris, La Défense 2, à Paris-La Défense (92095), représentée par son président ; la société demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'ordonnance n° 1108330 du 3 novembre 2011 par laquelle le juge des référés du tribunal administratif de Cergy-Pontoise, statuant sur le fondement de l'article L. 521-1 du code de justice administrative, a rejeté sa demande tendant à la suspension de l'exécution des deux décisions du 7 juillet 2011 par lesquelles le directeur régional des entreprises, de la concurrence, de la consommation, du travail et de l'emploi (DIRECCTE) d'Ile-de-France a déterminé le nombre et le périmètre des établissements distincts de la société pour l'élection des délégués du personnel et des représentants du personnel au comité d'entreprise ;<br/>
<br/>
              2°) statuant en référé, de faire droit à sa demande de suspension ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat le versement de la somme de 6 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code du travail, modifié notamment par l'article 4 de la loi n° 2008-789 du 20 août 2008 ;<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Rémi Decout-Paolini, Maître des Requêtes,  <br/>
<br/>
              - les observations de la SCP Lyon-Caen, Thiriez, avocat de la SOCIETE AVIS LOCATION DE VOITURES,<br/>
<br/>
              - les conclusions de Mme Claire Landais, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Lyon-Caen, Thiriez, avocat de la SOCIETE AVIS LOCATION DE VOITURES ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>Considérant qu'aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision " ;<br/>
<br/>
              Considérant que les articles L. 2314-3 et L. 2324-4 du code du travail, relatifs à l'organisation des élections professionnelles des délégués du personnel et des représentants du personnel au comité d'entreprise, imposent à l'employeur d'inviter les organisations syndicales à négocier un protocole d'accord préélectoral ; qu'aux termes des dispositions identiques des articles L. 2314-3-1 et L. 2324-4-1 du même code : " La validité du protocole d'accord préélectoral conclu entre l'employeur et les organisations syndicales intéressées est subordonnée à sa signature par la majorité des organisations syndicales ayant participé à sa négociation, dont les organisations syndicales représentatives ayant recueilli la majorité des suffrages exprimés lors des dernières élections professionnelles ou, lorsque ces résultats ne sont pas disponibles, la majorité des organisations représentatives dans l'entreprise " ; que les articles L. 2314-25 et L. 2324-23 du même code disposent que les contestations relatives à la régularité des opérations électorales tendant à la désignation des délégués du personnel et des représentants du personnel au comité d'entreprise sont de la compétence du juge judiciaire ; qu'aux termes de l'article L. 2314-31 du même code, relatif à l'élection des délégués du personnel : " Dans chaque entreprise, à défaut d'accord entre l'employeur et les organisations syndicales intéressées conclu selon les conditions de l'article L. 2314-3-1, le caractère d'établissement distinct est reconnu par l'autorité administrative " ; qu'aux termes de l'article L. 2322-5 du même code, relatif à l'élection des représentants du personnel au comité d'entreprise : " Dans chaque entreprise, à défaut d'accord entre l'employeur et les organisations syndicales intéressées conclu selon les conditions de l'article L. 2324-4-1, l'autorité administrative du siège de l'entreprise a compétence pour reconnaître le caractère d'établissement distinct " ; <br/>
<br/>
              Considérant qu'il résulte de l'ensemble de ces dispositions que l'autorité administrative est compétente pour déterminer, dans le cadre de l'engagement d'un processus électoral et sur saisine de l'une des parties à la négociation du protocole d'accord préélectoral, le nombre d'établissements distincts d'une entreprise, dès lors qu'aucun protocole n'a été conclu sur ce point ou qu'un tel protocole ne satisfait manifestement pas à la double condition de majorité prévue aux articles L. 2314-3-1 et L. 2324-4-1 du code du travail ; que toutefois, l'édiction de cet acte préparatoire aux élections professionnelles en vue desquelles l'autorité administrative a été saisie ne peut, en tout état de cause, intervenir qu'avant la tenue de ces élections ;<br/>
<br/>
              Considérant qu'il ressort des pièces du dossier soumis au juge des référés que, si le directeur régional des entreprises, de la concurrence, de la consommation, du travail et de l'emploi d'Ile-de-France a été saisi le 16 mai 2011 par un représentant du syndicat CFTC dans le cadre du processus engagé le mois précédent en vue des élections professionnelles organisées au sein de la SOCIETE AVIS LOCATION DE VOITURES pour renouveler les mandats des délégués du personnel et des représentants du personnel au comité d'entreprise, ces élections se sont déroulées du 3 au 28 juin 2011 ; que, par suite, les deux décisions du 7 juillet 2011, par lesquelles il a déterminé au sein de la société dix-sept établissements distincts pour l'élection des délégués du personnel et cinq établissements distincts pour l'élection des représentants du personnel au comité d'entreprise, étaient dépourvues d'effet ; qu'ainsi, la demande de la société tendant à ce que l'exécution de ces décisions soit suspendue était sans objet et, par suite, irrecevable ; que ce motif de pur droit, exclusif de toute appréciation de fait de la part du juge de cassation, doit être substitué au motif de l'ordonnance attaquée dont il justifie légalement le dispositif ; qu'il y a lieu, par suite, de rejeter les conclusions du pourvoi de la SOCIETE AVIS LOCATION DE VOITURES, y compris celles présentées au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de la SOCIETE AVIS LOCATION DE VOITURES est rejeté. <br/>
Article 2 : La présente décision sera notifiée à la SOCIETE AVIS LOCATION DE VOITURES et au ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social.<br/>
Copie en sera adressée à la FGMM-CFDT.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-035-02-02 PROCÉDURE. PROCÉDURES INSTITUÉES PAR LA LOI DU 30 JUIN 2000. RÉFÉRÉ SUSPENSION (ART. L. 521-1 DU CODE DE JUSTICE ADMINISTRATIVE). RECEVABILITÉ. - DEMANDE DE SUSPENSION D'UNE DÉCISION DÉPOURVUE D'EFFET - DÉCISION DE L'AUTORITÉ ADMINISTRATIVE DÉTERMINANT, FAUTE D'ACCORD PRÉÉLECTORAL EN CE SENS, LE NOMBRE D'ÉTABLISSEMENTS DISTINCTS D'UNE ENTREPRISE POUR LA PRÉPARATION DES ÉLECTIONS PROFESSIONNELLES - DÉCISION INTERVENUE APRÈS LA TENUE DES ÉLECTIONS - CONSÉQUENCE - IRRECEVABILITÉ.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">66-04-01-02 TRAVAIL ET EMPLOI. INSTITUTIONS REPRÉSENTATIVES DU PERSONNEL. COMITÉS D'ENTREPRISE. ORGANISATION DES ÉLECTIONS. - DÉTERMINATION DU NOMBRE D'ÉTABLISSEMENTS DISTINCTS D'UNE ENTREPRISE - 1) COMPÉTENCE DE L'AUTORITÉ ADMINISTRATIVE POUR Y PROCÉDER - EXISTENCE - CONDITIONS DE FOND - ABSENCE DE PROTOCOLE D'ACCORD PRÉÉLECTORAL OU ACCORD NE SATISFAISANT MANIFESTEMENT PAS À LA RÈGLE DE DOUBLE MAJORITÉ - 2) DÉLAI DANS LEQUEL L'ADMINISTRATION DOIT STATUER - AVANT LA TENUE DES ÉLECTIONS.
</SCT>
<ANA ID="9A"> 54-035-02-02 Lorsqu'elle détermine, dans le cadre de l'engagement des élections professionnelles des délégués du personnel ou des représentants du personnel au comité d'entreprise, et sur saisine de l'une des parties à la négociation du protocole d'accord préélectoral, le nombre d'établissements distincts d'une entreprise, l'autorité administrative édicte un acte préparatoire aux élections professionnelles qui ne peut intervenir qu'avant la tenue de ces élections. Une telle décision, lorsqu'elle intervient après la tenue de ces élections, est dépourvue d'effets juridiques et la demande tendant à la suspension de son exécution est par conséquent irrecevable.</ANA>
<ANA ID="9B"> 66-04-01-02 1) Il résulte des dispositions du code du travail relatives à l'organisation des élections professionnelles des délégués du personnel et des représentants du personnel au comité d'entreprise, que l'autorité administrative est compétente pour déterminer, dans le cadre de l'engagement d'un processus électoral et sur saisine de l'une des parties à la négociation du protocole d'accord préélectoral, le nombre d'établissements distincts d'une entreprise, dès lors qu'aucun protocole n'a été conclu sur ce point ou qu'un tel protocole ne satisfait manifestement pas à la double condition de majorité prévue aux articles L. 2314-3-1 et L. 2324-4-1 du code du travail.... ...2) Toutefois, l'édiction de cet acte préparatoire aux élections professionnelles en vue desquelles l'autorité administrative a été saisie ne peut, en tout état de cause, intervenir qu'avant la tenue de ces élections.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
