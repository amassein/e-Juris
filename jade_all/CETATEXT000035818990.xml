<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000035818990</ID>
<ANCIEN_ID>JG_L_2017_10_000000406373</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/35/81/89/CETATEXT000035818990.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème - 10ème chambres réunies, 06/10/2017, 406373</TITRE>
<DATE_DEC>2017-10-06</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>406373</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème - 10ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP THOUIN-PALAT, BOUCARD</AVOCATS>
<RAPPORTEUR>Mme Séverine Larere</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Yohann Bénard</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:406373.20171006</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société Reunisolis a demandé au juge des référés du tribunal administratif de Paris :<br/>
<br/>
              1°) d'ordonner, sur le fondement des dispositions de l'article L. 521-1 du code de justice administrative, la suspension de l'exécution de la décision du 27 juin 2016 par laquelle la directrice de l'énergie a rejeté son offre relative à son projet " Centrale de la Gabarre " dans le cadre de l'appel d'offres portant sur la réalisation et l'exploitation d'installations de production d'électricité à partir de techniques de conversion du rayonnement solaire d'une puissance supérieure à 100 kWc et situées dans les zones non interconnectées et de la décision du 18 octobre 2016, par laquelle le directeur adjoint de l'énergie du ministère de l'écologie, du développement durable et de l'énergie a rejeté son recours gracieux ; <br/>
<br/>
              2°) d'ordonner, par voie de conséquence, la suspension de l'exécution de la liste des lauréats de la famille 2 de l'appel d'offres 2015/S 0093-166551 portant sur la réalisation et l'exploitation d'installations de production d'électricité à partir de techniques de conversion du rayonnement solaire d'une puissance supérieure à 100 kWc et situées dans les zones non interconnectées ;<br/>
<br/>
              3°) d'enjoindre au ministre de l'écologie, du développement durable et de l'énergie de procéder à une nouvelle instruction de son offre dans un délai de huit jours à compter du prononcé de la présente ordonnance, sous astreinte de 500 euros par jour de retard ou, à titre subsidiaire, d'inscrire le projet " Centrale de la Gabarre " sur la liste des lauréats de la famille 2 retenus à l'issue de l'appel d'offres.<br/>
<br/>
              Par une ordonnance n° 1621207/9 du 13 décembre 2016, le juge des référés du tribunal administratif de Paris a rejeté sa demande.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 28 décembre 2016, 11 janvier 2017 et 15 juin 2017 au secrétariat du contentieux du Conseil d'Etat, la société Reunisolis demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) statuant en référé, de faire droit à sa demande ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'énergie ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Séverine Larere, maître des requêtes,<br/>
<br/>
              - les conclusions de M. Yohann Bénard, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Thouin-Palat, Boucard, avocat de la société Reunisolis ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis au juge des référés du tribunal administratif de Paris que, par un avis publié au Journal officiel de l'Union européenne le 15 mai 2015, la ministre de l'écologie, du développement durable et de l'énergie a lancé, en application des dispositions de l'article L. 311-10 du code de l'énergie, un appel d'offres portant sur la réalisation et l'exploitation d'installations de production d'électricité à partir de techniques de conversion du rayonnement solaire d'une puissance supérieure à 100 kWc et situées dans les zones non interconnectées. Après instruction des candidatures reçues par la commission de régulation de l'énergie (CRE), la liste des candidats retenus par la ministre a été publiée sur le site internet du ministère le 11 juin 2016. Par courrier du 27 juin 2016, la société Reunisolis a été informée que son offre concernant le projet " centrale de la Gabarre", situé en Guadeloupe n'était pas retenue. Elle a formé contre cette décision, le 8 août 2016, un recours gracieux qui a été rejeté le 18 octobre 2016. Elle a saisi, le 7 décembre 2016, le tribunal administratif de Paris, d'une part, d'une requête tendant à l'annulation de la décision du 27 juin 2016 rejetant son offre et de la décision de rejet de son recours gracieux du 18 octobre 2016 ainsi que, par voie de conséquence, de la liste des lauréats de la famille 2 de l'appel d'offres, et, d'autre part, d'une demande de suspension de ces mêmes décisions sur le fondement de l'article L. 521-1 du code de justice administrative. Cette dernière demande a été rejetée par l'ordonnance attaquée du juge des référés du 13 décembre 2016.<br/>
<br/>
              2. Aux termes de l'article L. 522-3 du code de justice administrative : " Lorsque la demande ne présente pas un caractère d'urgence ou lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable ou qu'elle est mal fondée, le juge des référés peut la rejeter par une ordonnance motivée (...) ".<br/>
<br/>
              3. Pour rejeter, sur le fondement de ces dispositions, la demande de suspension présentée par la société Reunisolis, le juge des référés a jugé que cette demande était " manifestement irrecevable " du fait de l'irrecevabilité manifeste des demandes d'annulation présentées par la société Reunisolis. Il a estimé, d'une part, par des motifs non contestés de son ordonnance, qu'eu égard au caractère indivisible de la liste des lauréats établie par la ministre de l'écologie, du développement durable et de l'énergie, les conclusions de la société requérante dirigées contre le courrier du 27 juin 2016, qui se bornait à l'informer que sa candidature n'avait pas été retenue, et contre la décision du 18 octobre 2016 rejetant son recours gracieux étaient irrecevables et, d'autre part, que ses conclusions dirigées contre la décision fixant la liste des lauréats étaient tardives. Sur ce dernier point, il a jugé que le délai de recours contentieux ouvert aux candidats non retenus pour contester la liste des lauréats avait commencé à courir à compter de la notification aux intéressés du rejet de leur offre, cette information les mettant en mesure de demander la communication de la liste dans son intégralité. Il en a déduit que la requérante, qui avait reçu au plus tard le 8 août 2016, date de son recours gracieux, le courrier daté du 27 juin 2016 l'informant du rejet de son offre, avait jusqu'au 10 octobre 2016 pour demander l'annulation de la décision fixant la liste des lauréats.<br/>
<br/>
              4. En premier lieu, il ressort de la copie de la minute de l'ordonnance attaquée que celle-ci comporte la signature du juge des référés qui l'a rendue. Par suite, le moyen tiré de ce que cette ordonnance aurait été rendue en méconnaissance des dispositions de l'article R. 742-5 du code de justice administrative, qui prévoient que la minute de l'ordonnance est signée du magistrat qui l'a rendue, manque en fait.<br/>
<br/>
              5. En deuxième lieu, il ressort des motifs de l'ordonnance attaquée que celle-ci comporte l'énoncé des considérations de fait et de droit qui ont conduit le juge des référés à rejeter la demande de suspension dont il était saisi. Cette ordonnance est ainsi suffisamment motivée, sans qu'y fasse obstacle la circonstance qu'elle ne mentionne pas la date d'enregistrement de la demande d'annulation des actes attaqués, alors même que le juge des référé s'est fondé, pour rejeter la demande de suspension, sur la tardiveté d'une partie des conclusions de cette demande, dès lors que la société requérante avait nécessairement connaissance de cette date. De même, le juge des référés a pu, pour juger irrecevables les conclusions à fin d'annulation dirigées contre la décision de rejet de la candidature de la société, se  fonder sur le caractère indivisible de la liste des lauréats sans s'en expliquer davantage.<br/>
<br/>
              6. En troisième lieu, si l'article L. 511-1 du code de justice administrative prévoit que " le juge des référés statue par des mesures qui présentent un caractère provisoire " et qu'" il n'est pas saisi du principal ", ces dispositions ne font pas obstacle à ce que le juge des référés constate le caractère manifestement irrecevable de la demande présentée au principal et en tire les conséquences sur la demande dont il est saisi. Le moyen tiré de la méconnaissance de ces dispositions doit, par suite, être écarté.<br/>
<br/>
              7. En quatrième lieu, lorsque la demande tendant à l'annulation du ou des actes administratifs dont la suspension est demandée est irrecevable, la demande de suspension est manifestement mal fondée au sens de l'article L. 522-3 du code de justice administrative cité au point 2 ci-dessus. Le juge des référés a expressément rappelé ce principe au point 2 de l'ordonnance attaquée. Par suite, si par une erreur de plume il a indiqué, au point 7 de son ordonnance, que la requête à fin de suspension était manifestement " irrecevable " du fait de l'irrecevabilité manifeste des conclusions à fin d'annulation dirigées contre les mêmes actes, cette erreur est demeurée sans conséquence sur la régularité ou le bien-fondé de l'ordonnance attaquée.<br/>
<br/>
              8. En cinquième lieu, dès lors qu'il estimait que la demande dont il était saisi était manifestement mal fondée du fait de l'irrecevabilité des conclusions présentées au fond, le juge des référés pouvait la rejeter par ordonnance en application des dispositions de l'article L. 522-3 du code de justice administrative. Le moyen tiré de l'irrégularité de la procédure suivie doit, dès lors, être écarté.<br/>
<br/>
              9. En sixième lieu, le juge des référés n'a commis ni erreur de droit ni dénaturation des faits en jugeant que le délai de recours contentieux ouvert aux candidats non retenus à l'issue de l'appel d'offres pour contester la liste des lauréats avait commencé à courir à compter de la notification aux intéressés du rejet de leur offre, cette information les mettant en mesure de demander communication de la liste dans son intégralité, et en en déduisant que la société requérante, qui avait reçu au plus tard le 8 août 2016, date de son recours gracieux, le courrier daté du 27 juin 2016 l'informant du rejet de son offre, avait jusqu'au 10 octobre 2016 pour demander l'annulation de cette liste. Il n'a pas davantage commis d'erreur de droit en jugeant que la circonstance que le courrier du 27 juin 2016 ait indiqué à tort que le rejet de l'offre de la société requérante constituait une décision susceptible de recours dans un délai de deux mois à compter de sa notification était sans incidence sur l'opposabilité du délai de recours contre la décision fixant la liste des lauréats. Par suite, le juge des référés n'a pas inexactement qualifié les faits, qu'il n'a pas dénaturés, en jugeant que les conclusions tendant à l'annulation de la liste des lauréats, présentées après l'expiration du délai de recours, étaient tardives et, par suite, irrecevables. Ce faisant, il n'a pas méconnu les droits garantis par les articles 6 et 13 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales.  <br/>
<br/>
              10. Il résulte de ce qui précède que la société Reunisolis n'est pas fondée à demander l'annulation de l'ordonnance qu'elle attaque. Les dispositions de l'article L. 761-1 du code de justice administrative font dès lors obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat qui n'est pas la partie perdante.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de la société Reunisolis est rejeté. <br/>
Article 2 : La présente décision sera notifiée à la société Reunisolis et au ministre d'Etat, ministre de la transition écologique et solidaire.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">39-02-02-03 MARCHÉS ET CONTRATS ADMINISTRATIFS. FORMATION DES CONTRATS ET MARCHÉS. MODE DE PASSATION DES CONTRATS. APPEL D'OFFRES. - CONTESTATION PAR UN CANDIDAT ÉVINCÉ DE LA LISTE DES LAURÉATS D'UN APPEL D'OFFRES - COURRIER ÉMANANT DE L'ADMINISTRATION MENTIONNANT À TORT LA POSSIBILITÉ POUR CE DERNIER DE FORMER UN RECOURS CONTRE LA DÉCISION DE REJET DE SON OFFRE - CIRCONSTANCE SANS INCIDENCE SUR L'OPPOSABILITÉ DU DÉLAI DE RECOURS CONTRE LA DÉCISION FIXANT LA LISTE DES LAURÉATS.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-01-07-02-01 PROCÉDURE. INTRODUCTION DE L'INSTANCE. DÉLAIS. POINT DE DÉPART DES DÉLAIS. NOTIFICATION. - NOTIFICATION À UNE SOCIÉTÉ D'UNE DÉCISION DE REJET DE L'OFFRE QU'ELLE A PRÉSENTÉE DANS LE CADRE D'UN APPEL D'OFFRES, CONSTITUANT LE POINT DE DÉPART DU DÉLAI DE RECOURS CONTRE LA LISTE DES LAURÉATS - MENTION ERRONÉE DE LA POSSIBILITÉ DE FORMER UN RECOURS CONTRE LA DÉCISION DE REJET DE L'OFFRE - CIRCONSTANCE SANS INCIDENCE SUR L'OPPOSABILITÉ DU DÉLAI DE RECOURS CONTRE LA DÉCISION FIXANT LA LISTE DES LAURÉATS.
</SCT>
<ANA ID="9A"> 39-02-02-03 Société ayant reçu de la part de l'administration un courrier l'informant du rejet de l'offre qu'elle avait présentée dans le cadre d'un appel d'offres et indiquant à tort que cette décision de rejet constituait une décision susceptible de recours dans un délai de deux mois à compter de sa notification, alors que seule la liste des lauréats était susceptible d'un tel recours dans ce délai. Une telle erreur est sans incidence sur l'opposabilité à cette société du délai de recours contre la décision fixant la liste des lauréats.</ANA>
<ANA ID="9B"> 54-01-07-02-01 Société ayant reçu de la part de l'administration un courrier l'informant du rejet de l'offre qu'elle avait présentée dans le cadre d'un appel d'offres et indiquant à tort que cette décision de rejet constituait une décision susceptible de recours dans un délai de deux mois à compter de sa notification, alors que seule la liste des lauréats était susceptible d'un tel recours dans ce délai. Une telle erreur est sans incidence sur l'opposabilité à cette société du délai de recours contre la décision fixant la liste des lauréats.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
