<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038104921</ID>
<ANCIEN_ID>JG_L_2019_02_000000427504</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/10/49/CETATEXT000038104921.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 06/02/2019, 427504, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-02-06</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>427504</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP WAQUET, FARGE, HAZAN ; SCP HEMERY, THOMAS-RAQUIN, LE GUERER</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2019:427504.20190206</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu les procédures suivantes :<br/>
<br/>
              1° Sous le n° 427504, par une requête, enregistrée le 30 janvier 2019 au secrétariat du contentieux du Conseil d'Etat, la Ligue pour la protection des oiseaux demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative : <br/>
<br/>
              1°) de suspendre l'exécution de l'arrêté du 30 janvier 2019 du ministre de la transition écologique et solidaire relatif au prélèvement autorisé de l'oie cendrée, de l'oie rieuse et de l'oie des moissons au cours du mois de février 2019 ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Elle soutient que : <br/>
              - sa requête est recevable dès lors qu'elle a intérêt à agir contre l'arrêté attaqué ;<br/>
              - la condition d'urgence est remplie dès lors que l'exécution de l'arrêté litigieux porte une atteinte suffisamment grave et immédiate à la préservation et à la défense des oiseaux, intérêts qu'elle entend défendre, en ce qu'il va entraîner la destruction irréversible de nombreux oiseaux migrateurs, pour certains pendant la période de migration prénuptiale ; <br/>
              - il existe un doute sérieux quant à la légalité de l'arrêté contesté ;<br/>
              - l'arrêté litigieux méconnaît le principe de protection complète des espèces migratrices prévu par les dispositions des articles 7 de la directive n° 2009/147/CE du 30 novembre 2009 et L. 424-2 du code de l'environnement dès lors qu'il autorise le prélèvement d'espèces migratrices pendant la période de retour vers leur lieu de nidification ; <br/>
              - il méconnaît les dispositions de l'article L. 424-2 et de l'article 9 de la directive n° 2009/14/CE du 30 novembre 2009 dès lors, d'une part, que les conditions permettant de déroger au principe de protection complète de ces espèces migratrices ne sont pas remplies et, d'autre part, que le nombre de prélèvements maximal n'est pas fixé par chasseur en méconnaissance des dispositions de l'article L. 425-14 auquel renvoie l'article L. 424-2. <br/>
<br/>
<br/>
              Par un mémoire en défense, enregistré le 4 février 2019, le ministre d'Etat, ministre de la transition écologique et solidaire conclut au rejet de la requête. Il soutient que la condition d'urgence n'est pas remplie et que les moyens soulevés ne sont pas de nature à faire naître un doute sérieux quant à la légalité de l'arrêté contesté. <br/>
<br/>
              Par une intervention, enregistrée le 4 février 2019, la Fédération nationale des chasseurs demande que le Conseil d'Etat rejette la requête. Elle s'en remet à la sagesse du Conseil d'Etat s'agissant de la condition d'urgence et soutient que les moyens soulevés ne sont pas de nature à faire naître un doute sérieux quant à la légalité de l'arrêté contesté. <br/>
<br/>
<br/>
<br/>
              2° Sous le n° 427520, par une requête, enregistrée le 31 janvier 2019 au secrétariat du contentieux du Conseil d'Etat, l'association France nature environnement demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative : <br/>
<br/>
              1°) de suspendre l'exécution de l'arrêté du 30 janvier 2019 du ministre de la transition écologique et solidaire relatif au prélèvement autorisé de l'oie cendrée, de l'oie rieuse et de l'oie des moissons au cours du mois de février 2019 ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 1 500 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Elle soutient que : <br/>
              - sa requête est recevable dès lors qu'elle a intérêt à agir contre l'arrêté du 30 janvier 2019 ; <br/>
              - la condition d'urgence est remplie dès lors que la prolongation d'un mois de la chasse à l'oie cendrée en février et de dix jours de la chasse aux oies rieuses et des moissons porte une atteinte suffisamment grave et immédiate à la protection de ces espèces animales, intérêt qu'elle entend défendre ; <br/>
              - il existe un doute sérieux quant à la légalité de l'arrêté contesté ; <br/>
              - l'arrêté litigieux a été pris en méconnaissance des articles L. 424-2 et L. 425-14 du code de l'environnement dès lors qu'il ne fixe pas le nombre maximal d'oies cendrées qu'un chasseur est autorisé à prélever ;<br/>
              - il a été pris en méconnaissance de la directive n° 2009/147/CE du 30 novembre 2009 dès lors que les conditions pour déroger au principe de protection complète ne sont pas remplies en ce que, d'une part, il n'est pas démontré clairement la nécessité d'y déroger en l'absence de dommages importants causés en France aux cultures agricoles par ces espèces migratrices et, d'autre part, il n'est pas attesté qu'il existerait d'autres solutions satisfaisantes que la chasse ; <br/>
              - il est entaché d'illégalité dès lors qu'il prévoit des dates de fermeture différentes de la chasse pour ces trois espèces d'oies alors qu'il existe un risque de confusion entre ces espèces d'apparence similaire ;<br/>
              - il a été pris en méconnaissance de l'avis défavorable de la Commission européenne relatif à la prolongation de la chasse des oies. <br/>
<br/>
              Par un mémoire en défense, enregistré le 4 février 2019, le ministre d'Etat, ministre de la transition écologique et solidaire conclut au rejet de la requête. Il soutient que la condition d'urgence n'est pas remplie et que les moyens soulevés ne sont pas de nature à faire naître un doute sérieux quant à la légalité de l'arrêté contesté. <br/>
<br/>
              Par une intervention, enregistrée le 4 février 2019, la Fédération nationale des chasseurs demande que le Conseil d'Etat rejette la requête. Elle s'en remet à la sagesse du Conseil d'Etat s'agissant de la condition d'urgence et soutient que les moyens soulevés ne sont pas de nature à faire naître un doute sérieux quant à la légalité de l'arrêté contesté. <br/>
<br/>
<br/>
<br/>
              3° Sous le n° 427544, par une requête, enregistrée le 31 janvier 2019 au secrétariat du contentieux du Conseil d'Etat, l'association Humanité et biodiversité demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative : <br/>
<br/>
              1°) de suspendre l'exécution de l'arrêté du 30 janvier 2019 du ministre de la transition écologique et solidaire relatif au prélèvement autorisé de l'oie cendrée, de l'oie rieuse et de l'oie des moissons au cours du mois de février 2019 ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 1 500 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Elle soulève les mêmes moyens que la requête n° 427520.<br/>
<br/>
<br/>
              Par un mémoire en défense, enregistré le 4 février 2019, le ministre d'Etat, ministre de la transition écologique et solidaire conclut au rejet de la requête. Il soutient que la condition d'urgence n'est pas remplie et que les moyens soulevés ne sont pas de nature à faire naître un doute sérieux quant à la légalité de l'arrêté contesté. <br/>
<br/>
              Par une intervention, enregistrée le 4 février 2019, la Fédération nationale des chasseurs demande que le Conseil d'Etat rejette la requête. Elle s'en remet à la sagesse du Conseil d'Etat s'agissant de la condition d'urgence et soutient que les moyens soulevés ne sont pas de nature à faire naître un doute sérieux quant à la légalité de l'arrêté contesté. <br/>
<br/>
<br/>
<br/>
              4° Sous le n° 427549, par une requête et un mémoire en réplique, enregistrés les 31 janvier et 5 février 2019 au secrétariat du contentieux du Conseil d'Etat, l'association One Voice demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative ; <br/>
<br/>
              1°) de suspendre l'exécution de l'arrêté du 30 janvier 2019 du ministre de la transition écologique et solidaire relatif au prélèvement autorisé de l'oie cendrée, de l'oie rieuse et de l'oie des moissons au cours du mois de février 2019 ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Elle soutient que : <br/>
              - sa requête est recevable ; <br/>
              - la condition d'urgence est remplie dès lors que l'exécution de l'arrêté litigieux est susceptible de porter une atteinte suffisamment grave et immédiate à la protection de la biodiversité en France en ce qu'elle est susceptible de causer la destruction irréversible d'un nombre élevé d'oies en période de migration prénuptiale ;<br/>
              - il existe un doute sérieux quant à la légalité de l'arrêté contesté ; <br/>
              - l'arrêté litigieux a été pris au terme d'une procédure irrégulière dès lors que la synthèse des observations et propositions du public prévue à l'alinéa 7 de l'article L. 123-19-1 du code de l'environnement n'a pas été publiée ;<br/>
              - il a été pris en méconnaissance de l'article L. 424-2 du code de l'environnement et des articles 7 et 9 de la directive 2009/147/CE du 30 novembre 2009 concernant la conservation des oiseaux sauvages dès lors que, d'une part, les conditions pour déroger à l'interdiction de la chasse des oiseaux migrateurs pendant leur période de migration ou de nidification ne sont pas remplies, et d'autre part, il ne fixe pas de nombre maximal de prélèvements par jour ou par zone ;<br/>
              - il a été pris en méconnaissance de l'article L. 420-1 du code de l'environnement dès lors qu'il contrevient à la gestion durable du patrimoine faunique ; <br/>
              - il a été pris en méconnaissance de l'article 6 de la Charte de l'environnement dès lors qu'il ne permet pas la conciliation nécessaire entre le caractère récréatif de la chasse et la protection de l'environnement.<br/>
<br/>
<br/>
              Par un mémoire en défense, enregistré le 4 février 2019, le ministre d'Etat, ministre de la transition écologique et solidaire conclut au rejet de la requête. Il soutient que la condition d'urgence n'est pas remplie et que les moyens soulevés ne sont pas de nature à faire naître un doute sérieux quant à la légalité de l'arrêté contesté. <br/>
<br/>
              Par une intervention, enregistrée le 4 février 2019, la Fédération nationale des chasseurs demande que le Conseil d'Etat rejette la requête. Elle s'en remet à la sagesse du Conseil d'Etat s'agissant de la condition d'urgence et soutient que les moyens soulevés ne sont pas de nature à faire naître un doute sérieux quant à la légalité de l'arrêté contesté. <br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, la Ligue pour la protection des oiseaux, l'association France nature environnement, l'association Humanité et biodiversité et l'association One Voice et, d'autre part, le ministre d'Etat, ministre de la transition écologique et solidaire, l'Office national de la chasse et de la faune sauvage et la Fédération nationale des chasseurs ; <br/>
<br/>
              Vu le procès-verbal de l'audience publique du 5 février 2019 à 11 heures au cours de laquelle ont été entendus : <br/>
<br/>
              - Me Le Guerer, avocat au Conseil d'Etat et à la Cour de cassation, avocat de la Ligue pour la protection des oiseaux ;<br/>
<br/>
              - les représentants des associations de la Ligue pour la protection des oiseaux, France nature environnement, Humanité et biodiversité et One Voice ; <br/>
<br/>
              - les représentants du ministre d'Etat, ministre de la transition écologique et solidaire ; <br/>
<br/>
              - Me Farge, avocat au Conseil d'Etat et à la Cour de cassation, avocat de la Fédération nationale des chasseurs ; <br/>
<br/>
              - les représentants de la Fédération nationale des chasseurs ; <br/>
<br/>
              et à l'issue de laquelle le juge des référés a différé la clôture de l'instruction au 5 février 2019 à 16 heures.<br/>
<br/>
<br/>
              Vu les notes en délibéré, enregistrées le 5 février 2019, présentées par le ministre de la transition écologique et solidaire et par la Fédération nationale des chasseurs ;<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu :<br/>
              - la Constitution, et notamment la Charte de l'environnement ;<br/>
              - la directive n° 2009/147/CE du Parlement européen et du Conseil du 30 novembre 2009 concernant la conservation des oiseaux sauvages ;<br/>
              - le code de l'environnement ;<br/>
              - l'arrêté du 19 janvier 2009 du ministre d'Etat, ministre de l'écologie, de l'énergie, du développement durable et de l'aménagement du territoire relatif aux dates de fermeture de la chasse aux oiseaux de passage et au gibier d'eau ; <br/>
              - le code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
Considérant ce qui suit :<br/>
<br/>
              1. Les requêtes visées ci-dessus tendent à la suspension de l'exécution du même arrêté. Il y a donc lieu de les joindre pour statuer par une seule ordonnance.<br/>
<br/>
              2. Les associations requérantes demandent au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative, de suspendre l'exécution de l'arrêté du 30 janvier 2019 par lequel le ministre d'Etat, ministre de le la transition écologique et solidaire a, d'une part, reporté au 10 février 2019 la date de fermeture de la chasse de l'oie rieuse et de l'oie des moissons et, d'autre part, autorisé, dans les conditions qu'il précise, le prélèvement de 4 000 oies cendrées au cours du mois de février 2019. <br/>
<br/>
              Sur l'intervention en défense de la Fédération nationale des chasseurs : <br/>
<br/>
              3. La Fédération nationale des chasseurs a intérêt au maintien de l'arrêté attaqué. Par suite, son intervention en défense est recevable.<br/>
<br/>
              Sur les conclusions aux fins de suspension : <br/>
              4. Aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision ". Selon le deuxième alinéa de l'article R. 522-13 du même code, le juge des référés peut décider que sa décision sera exécutoire aussitôt qu'elle aura été rendue. <br/>
<br/>
              5. Il ressort de l'article L. 424-2 du code de l'environnement que : " Nul ne peut chasser en dehors des périodes d'ouverture de la chasse fixées par l'autorité administrative selon des conditions déterminées par décret en Conseil d'État. / Les oiseaux ne peuvent être chassés ni pendant la période nidicole ni pendant les différents stades de reproduction et de dépendance. Les oiseaux migrateurs ne peuvent en outre être chassés pendant leur trajet de retour vers leur lieu de nidification. / Toutefois, pour permettre, dans des conditions strictement contrôlées et de manière sélective, la capture, la détention ou toute autre exploitation judicieuse de certains oiseaux migrateurs terrestres et aquatiques en petites quantités, conformément aux dispositions de l'article L. 425-14, des dérogations peuvent être accordées. / Un décret en Conseil d'Etat fixe les modalités d'application de cette disposition ". Il appartient au ministre chargé de la chasse, compétent en vertu de l'article R. 424-9 du même code pour fixer les dates d'ouverture et de fermeture de la chasse aux oiseaux de passage et au gibier d'eau, de se conformer à l'interprétation que la Cour de justice de l'Union européenne a donnée des dispositions des articles 7 § 4 et 9 de la directive du 30 novembre 2009 concernant la conservation des oiseaux sauvages, dont l'article L. 424-2 assure la transposition. L'article 9 de la directive autorise toutefois les Etats membres à déroger à ces dispositions " s'il n'existe pas d'autre solution satisfaisante " pour un certain nombre de motifs, et notamment " pour permettre, dans des conditions strictement contrôlées et de manière sélective, la capture, la détention ou toute autre exploitation judicieuse de certains oiseaux en petites quantités ". L'article 9 prévoit également, en son paragraphe 2, que les dérogations doivent mentionner les espèces concernées, les moyens, installations ou méthodes de capture ou de mise à mort autorisés, les conditions de risque et les circonstances de temps et de lieu dans lesquelles ces dérogations peuvent être prises, l'autorité habilitée à déclarer que les conditions exigées sont réunies, à décider quels moyens, installations ou méthodes peuvent être mis en oeuvre, dans quelles limites et par quelles personnes, enfin les contrôles qui seront opérés.<br/>
<br/>
              6. En premier lieu, eu égard à l'objet de l'arrêté dont la suspension est demandée et aux dates qu'il fixe pour l'autorisation de prélèvement respectivement d'oies cendrées et d'oies rieuses et des moissons, la condition d'urgence requise par l'article L. 521-1 du code de justice administrative doit être regardée comme remplie. <br/>
<br/>
              7. En second lieu, il ressort des pièces du dossier que l'arrêté attaqué est fondé sur la dérogation prévue au troisième alinéa de l'article L. 424-2 du code de l'environnement, transposant le c du paragraphe 1 de l'article 9 de la directive du 30 novembre 2009. Le ministre fait état des risques, au regard notamment de l'équilibre des écosystèmes, que présenterait la prolifération des espèces d'oies visées par l'arrêté en litige, en particulier au Nord de l'Europe. Il se fonde principalement sur le " plan de gestion international sur l'oie cendrée ", adopté en décembre 2018 par les Etats signataires de l'accord sur la conservation des oiseaux d'eau migrateurs d'Afrique et d'Eurasie (" AEWA "). Cependant, il n'établit pas, par les éléments qu'il a produits tant à l'appui de ses mémoires qu'au cours de l'audience, qu'il n'existerait aucune autre solution satisfaisante, qui pourrait notamment être mise en oeuvre dans les Etats européens les plus concernés par les risques allégués, ni que les prélèvements par tir autorisés par l'arrêté contesté constitueraient une " exploitation judicieuse de certains oiseaux en petites quantités " lui permettant de déroger au principe de protection complète des espèces migratrices pendant leur trajet de retour vers leur lieu de nidification. Par suite, le moyen tiré de la méconnaissance des dispositions du troisième alinéa de l'article L. 424-2 du code de l'environnement et du paragraphe 1 de l'article 9 de la directive du 30 novembre 2009 est de nature, en l'état de l'instruction, à faire naître un doute sérieux quant à la légalité de l'arrêté litigieux. <br/>
<br/>
              8. Il résulte de ce qui précède, sans qu'il soit besoin d'examiner les autres moyens des requêtes, que la suspension de l'exécution de cet arrêté doit être ordonnée. Compte tenu de l'urgence, il y a lieu de décider, sur le fondement de l'article R. 522-13 du code de justice administrative, que la présente ordonnance est immédiatement exécutoire.<br/>
              Sur les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative : <br/>
<br/>
              9. Il y a lieu de mettre à la charge de l'Etat, au titre de ces dispositions, le versement d'une somme, d'une part, de 500 euros chacune aux associations France nature environnement et Humanité et biodiversité et, d'autre part, de 1 000 euros chacune à la Ligue pour la protection des oiseaux et à l'association One Voice.  <br/>
<br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
------------------<br/>
Article 1er : L'intervention de la Fédération nationale des chasseurs est admise. <br/>
Article 2 : L'exécution de l'arrêté du ministre d'Etat, ministre de la transition écologique et solidaire du 30 janvier 2019 relatif au prélèvement autorisé de l'oie cendrée, de l'oie rieuse et de l'oie des moissons au cours du mois de février 2019 est suspendue.<br/>
Article 3 : En application de l'article R. 522-13 du code de justice administrative, la présente ordonnance est immédiatement exécutoire.<br/>
Article 4 : L'Etat versera la somme de 500 euros chacune aux associations France nature environnement et Humanité et biodiversité et la somme de 1 000 euros chacune à la Ligue pour la protection des oiseaux et à l'association One Voice au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
Article 5 : La présente ordonnance sera notifiée à la Ligue pour la protection des oiseaux, aux associations France nature environnement, Humanité et biodiversité et One Voice, au ministre d'Etat, ministre de la transition écologique et solidaire et à la Fédération nationale des chasseurs.<br/>
Copie en sera adressée à l'Office national de la chasse et de la faune sauvage.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
