<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033404338</ID>
<ANCIEN_ID>JG_L_2016_11_000000387576</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/40/43/CETATEXT000033404338.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème - 10ème chambres réunies, 16/11/2016, 387576, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-11-16</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>387576</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème - 10ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP CELICE, SOLTNER, TEXIDOR, PERIER</AVOCATS>
<RAPPORTEUR>Mme Marie-Gabrielle Merloz</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Marie-Astrid Nicolazo de Barmon</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2016:387576.20161116</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société Havas SA a demandé au tribunal administratif de Montreuil de prononcer le rétablissement de ses résultats déficitaires des exercices clos en 2002 et 2004, à concurrence des sommes de 271 351 000 euros et de 75 965 000 euros. Par un jugement n° 1009522 du 29 mars 2012, le tribunal administratif de Montreuil a rejeté sa demande tendant au rétablissement de ses résultats déficitaires de l'exercice clos en 2002 à raison de la réintégration de l'insuffisance de valorisation des dividendes versés sous forme d'actions au cours de cet exercice et, pour le surplus, ordonné une expertise contradictoire en vue de déterminer la pertinence des méthodes sur la base desquelles l'administration a fondé les rehaussements relatifs aux moins-values.<br/>
<br/>
              Par un arrêt n° 12VE01924 du 2 décembre 2014, la cour administrative d'appel de Versailles a rejeté l'appel formé par la société contre l'article 1er de ce jugement rejetant sa demande tendant au rétablissement de ses résultats déficitaires de l'exercice clos en 2002 à raison de la réintégration de l'insuffisance de valorisation des dividendes d'actions reçus au cours de cet exercice. <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 2 février 2015 et 4 mai 2015 au secrétariat du contentieux du Conseil d'Etat, la société Havas SA demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rétablir son résultat déficitaire de l'exercice clos en 2002 à concurrence de la somme de 79 951 000 euros ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 10 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - le code de justice administrative.<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Marie-Gabrielle Merloz, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Marie-Astrid Nicolazo de Barmon, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Célice, Soltner, Texidor, Perier, avocat de la Société Havas SA ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 4 novembre 2016, présentée pour la société Havas SA ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              Sur les motifs de l'arrêt relatifs au bénéfice du régime des sociétés mères :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que la société Havas a apporté au cours de l'année 1999 à la société de droit espagnol ISP les titres qu'elle détenait dans le capital de treize sociétés françaises et étrangères, non cotées en bourse, en échange desquels elle a reçu des actions représentant 45 % du capital social de la société ISP. Par une décision du 1er  septembre 1999, le ministre a accordé à la société Havas les agréments prévus aux articles 210 B et 210 C du code général des impôts, qui lui permettaient de placer cette opération d'apport partiel d'actifs sous le régime de faveur prévu à l'article 210 A du même code. En 2001, la société Havas est devenue actionnaire unique de la société ISP, désormais dénommée NMPG. Cette dernière a décidé, au cours de l'année 2002, de procéder, au profit de la société Havas, à la distribution de dividendes sous forme d'actions, lesquelles correspondaient aux titres apportés au cours de l'année 1999, à l'exclusion des titres de la société Médiapolis Espagne. L'engagement prévu dans l'agrément du 1er septembre 1999 de conservation des titres reçus par la société espagnole n'ayant ainsi pas été respecté, la société Havas a présenté, par un courrier du 29 novembre 2002, une nouvelle demande d'agrément par laquelle elle s'engageait, notamment, afin de ne pas perdre les avantages fiscaux attachés au premier agrément, à renoncer au bénéfice du régime des sociétés mères prévu aux articles 145 et 216 du code général des impôts et à soumettre en conséquence à l'impôt sur les sociétés au taux de droit commun le produit de la distribution en cause. Par une décision du 15 juillet 2003, le ministre des finances et de l'industrie a formalisé l'accord ainsi intervenu.<br/>
<br/>
              2. Aux termes de l'article 216 du code général des impôts, dans sa rédaction applicable à l'espèce : " I. Les produits nets des participations, ouvrant droit à l'application du régime des sociétés mères et visées à l'article 145, touchés au cours d'un exercice par une société mère, peuvent être retranchés du bénéfice net total de celle-ci, défalcation faite d'une quote-part de frais et charges. (...) ". Il résulte de ces dispositions que le bénéfice du régime des sociétés mères est une option ouverte aux sociétés qui, en remplissant les conditions légales, sont libres de l'exercer ou non.<br/>
<br/>
              3. En premier lieu, il ressort des énonciations de l'arrêt attaqué que c'est par un motif surabondant que, pour écarter le moyen de la société Havas tiré de ce que sa renonciation au bénéfice du régime des sociétés mères ne constituait pas une décision de gestion qui lui était opposable, la cour administrative d'appel de Versailles a mentionné qu'il n'était pas établi que cette renonciation ait été inspirée par les énonciations de l'instruction référencée 4 I-1-00 n° 4, publiée le 14 février 2000, selon laquelle la rupture de l'engagement de conservation des titres entraîne la déchéance rétroactive du régime spécial appliqué à l'opération d'apport partiel d'actifs prévu à l'article 210 B du code général des impôts. Par suite, ce motif ne peut être utilement critiqué devant le juge de cassation. <br/>
<br/>
              4. En second lieu, la cour n'a pas commis d'erreur de droit en jugeant que la société Havas avait, en renonçant au bénéfice du régime des sociétés mères institué par les articles 145 et 216 du code général des impôts, pris une décision de gestion qui lui était opposable, alors même qu'il ressortirait des pièces du dossier qui lui était soumis, ainsi que le soutient la société, que cette décision, prise dans le cadre d'un accord formalisé par la décision du 15 juillet 2003 mentionnée au point 1 ci-dessus, aurait été motivée par l'interprétation administrative alors donnée des dispositions combinées des articles 210 A et 210 B du code général des impôts, laquelle a été ultérieurement infirmée par le Conseil d'Etat, statuant au contentieux dans sa décision n°289658 du 15 juillet 2007.  <br/>
<br/>
              Sur les motifs de l'arrêt relatifs à la valorisation des titres :<br/>
<br/>
              5. La valeur vénale des actions d'une société non admises à la négociation sur un marché réglementé doit être appréciée en tenant compte de tous les éléments dont l'ensemble permet d'obtenir un chiffre aussi voisin que possible de celui qu'aurait entraîné le jeu normal de l'offre et de la demande à la date où la cession est intervenue.<br/>
<br/>
              6. Il ressort des énonciations de l'arrêt attaqué que, suivant les conclusions d'un rapport d'expertise établi le 28 mai 1999, la société Havas a évalué les titres qu'elle a apportés à la société de droit espagnol ISP au cours de l'année 1999, mentionnés au point 1 ci-dessus, en multipliant par six les résultats d'exploitation avant impôts, amortissements et provisions des sociétés en cause et en majorant le résultat ainsi obtenu de leur trésorerie moyenne disponible. Il ressort également des pièces du dossier soumis à la cour que, conformément à un rapport d'expertise du 28 décembre 2002, le montant des dividendes distribués par la société Havas au cours de l'année 2002 sous forme d'actions, lesquelles correspondaient à une exception près, comme il a été indiqué au point 1, aux titres qu'elle avait apportés en 1999, a été évalué à 135 269 000 euros, selon la même méthode mais en multipliant par cinq et demi les résultats d'exploitation avant impôts, amortissements et provisions des sociétés et en majorant le résultat ainsi obtenu de la moitié seulement de leur trésorerie moyenne disponible. Il ressort enfin de ces pièces que, à la suite de la vérification de comptabilité dont la société a fait l'objet, l'administration a estimé qu'il y avait lieu de retenir, comme en 1999, la totalité de la trésorerie moyenne disponible des sociétés concernées et porté en conséquence l'évaluation des actions litigieuses à la somme de 215 220 000 euros. <br/>
<br/>
              7. Après avoir rappelé que le principe même de la méthode spontanément mise en oeuvre par la société requérante n'a jamais été discuté par les parties et que l'administration a uniquement estimé infondé le coefficient de 0,5 appliqué à la trésorerie moyenne disponible, la cour administrative d'appel a constaté, d'une part, que, dans son premier rapport, l'expert mandaté par la société Havas avait justifié la prise en compte de la totalité de la trésorerie moyenne disponible pour évaluer les actions apportées en 1999 par l'existence d'une trésorerie structurellement très positive des sociétés concernées, tandis que, dans son second rapport, bien qu'insistant sur le caractère essentiel de " la permanence de la méthode... pour assurer une comparabilité acceptable des valorisations obtenues ", il n'avait fait état d'aucune circonstance particulière justifiant la prise en compte de seulement la moitié de la trésorerie moyenne disponible. La cour a relevé, d'autre part, que cette diminution ne pouvait être justifiée, contrairement à ce que soutenait la société requérante, par la conjoncture économique dans le secteur de la communication entre 1999 et 2002, dès lors que celle-ci avait déjà été prise en compte par la diminution du multiplicateur du résultat d'exploitation et qu'il n'était pas établi que le multiplicateur retenu était encore trop élevé. Elle a également relevé que la société requérante ne faisait état d'aucun changement qui serait intervenu dans les conditions d'exploitation ou la situation financière des sociétés en cause. Elle a indiqué, enfin, que la circonstance que l'estimation retenue par l'administration conduisait à constater une augmentation de 57 % de la valeur des titres en trois ans était, par elle-même, sans incidence sur le bien-fondé de cette estimation. <br/>
<br/>
              8. Il ressort, en premier lieu, des énonciations de l'arrêt attaqué, ainsi rappelées, que la cour n'a pas exigé de la société requérante qu'elle démontre le caractère excessif de l'évaluation de l'administration mais s'est bornée à confronter les arguments présentés dans le cadre de l'instruction par les deux parties en présence. La société Havas étant seule en mesure de faire état de circonstances particulières justifiant la modification des coefficients qu'elle a retenus pour l'évaluation des actions litigieuses, la cour n'a pas commis d'erreur de droit en jugeant que l'administration devait être regardée comme apportant la preuve que la société Havas, qui n'avait pas apporté une telle justification,  avait sous-évalué ces actions.<br/>
<br/>
              9. En second lieu, la cour n'a pas commis d'erreur de droit en jugeant que l'administration pouvait remettre en cause cette évaluation en comparant les paramètres retenus dans le cadre d'une méthode identique, dont la pertinence n'a jamais été contestée, à l'occasion d'une précédente transaction portant sur les mêmes titres, dès lors qu'elle a recherché, en se livrant à une appréciation souveraine des faits non arguée de dénaturation, si le contexte et les conditions dans lesquelles ces opérations ont été effectuées pouvaient justifier que soient retenus des paramètres différents. <br/>
<br/>
              10. Il résulte de tout ce qui précède que la société Havas n'est pas fondée à demander l'annulation de l'arrêt qu'elle attaque. <br/>
<br/>
              11. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de la société Havas SA est rejeté.<br/>
Article 2 : La présente décision sera notifiée à la société Havas SA et au ministre de l'économie et des finances.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
