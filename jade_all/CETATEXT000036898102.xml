<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036898102</ID>
<ANCIEN_ID>JG_L_2018_05_000000402013</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/89/81/CETATEXT000036898102.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème chambre, 09/05/2018, 402013, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-05-09</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>402013</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Cyrille Beaufils</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Louis Dutheillet de Lamothe</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:402013.20180509</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête enregistrée le 29 juillet 2016 au secrétariat du contentieux du Conseil d'Etat, le collectif national de préservation des activités agropastorales et rurales (CNPAAR) et M. A...B...demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision implicite de rejet née du silence gardé par le Premier ministre sur leur demande du 29 mars 2016 tendant, d'une part, à ce que l'Etat demande d'engager la révision de la convention, signée à Berne le 19 septembre 1979, relative à la conservation de la vie sauvage et du milieu naturel ainsi que de la directive 92/43/CEE du Conseil du 21 mai 1992 concernant la conservation des habitats naturels ainsi que de la faune et de la flore sauvages, afin de retirer au loup son statut d'espèce protégée, d'autre part, à ce que l'Etat procède en urgence à des mesures d'abattage des loups, à ce qu'il recense les loups sur le territoire national, à ce qu'il publie les résultats des analyses biologiques qui auront été réalisées, à ce qu'il renforce les tirs d'abattage et améliore la prise en charge des dispositifs de protection des troupeaux ;<br/>
<br/>
              2°) de prescrire toute mesure d'instruction utile ;<br/>
<br/>
              3°) de prononcer à l'égard du Gouvernement diverses mesures d'injonction tendant à la révision de la réglementation européenne et nationale, à la mise en place d'un plan national permettant de réguler effectivement les loups, à un recensement de ces animaux, et à ce que soient rendus publics les résultats des analyses réalisées, sous astreinte de 1 000 euros par jour de retard à compter de la décision à intervenir ;  <br/>
<br/>
              4°) de mettre à la charge de l'Etat la somme 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Le Collectif national de préservation des activités agropastorales et rurales et autre soutiennent que le Premier ministre a commis une erreur manifeste d'appréciation en rejetant leur demande, compte tenu de ce que divers éléments suggèrent que le nombre de loups est largement sous estimé,  de ce que l'administration minore la fréquence des attaques de loups, et de ce que la règlementation actuelle, notamment la convention de Berne relative à la conservation de la vie sauvage et du milieu naturel, la directive concernant la conservation des habitats naturels ainsi que de la faune et de la flore sauvages du 21 mai 1992, dite directive " habitats " et les dispositions du code de l'environnement, comme le plan loup, sont inadaptés. <br/>
<br/>
              Par un mémoire en défense, enregistré le 10 mars 2017, le ministre d'Etat, ministre de la transition écologique et solidaire, conclut au rejet de la requête.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la convention relative à la conservation de la vie sauvage et du milieu naturel, signée à Berne, le 19 septembre 1979 ;<br/>
              - la directive 92/43/CEE du Conseil du 21 mai 1992 ;<br/>
              - le code de l'environnement ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Cyrille Beaufils, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Louis Dutheillet de Lamothe, rapporteur public.<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Le Collectif national de préservation des activités agropastorales et rurales et autre demandent l'annulation pour excès de pouvoir de la décision implicite de rejet née du silence gardé par le Premier ministre sur leur demande du 29 mars 2016 tendant à ce que l'Etat demande aux autorités compétentes d'engager la révision de la convention relative à la conservation de la vie sauvage et du milieu naturel, signée à Berne le 19 septembre 1979, et la directive 92/43/CEE du Conseil du 21 mai 1992 concernant la conservation des habitats naturels ainsi que de la faune et de la flore sauvages, afin de retirer au loup son statut d'espèce protégée, à ce que l'Etat procède à des mesures d'abattage des loups, à ce qu'il recense effectivement les loups sur le territoire national, à ce qu'il publie les résultats des analyses biologiques qui auront été réalisées, et à ce qu'il améliore la prise en charge des dispositifs de protection des troupeaux. <br/>
<br/>
              2. Eu égard à la nature et à l'objet du litige, le syndicat Coordination rurale-Union nationale justifie d'un intérêt suffisant pour présenter une intervention. <br/>
<br/>
              3. En premier lieu, le refus implicite opposé par le Premier ministre à la demande du Collectif national de préservation des activités agropastorales et rurales et autre tendant à ce que l'Etat demande aux autorités compétentes d'engager la révision de la convention relative à la conservation de la vie sauvage et du milieu naturel et de la directive 92/43/CEE constitue une décision qui n'est pas détachable de l'exercice des pouvoirs du Gouvernement dans la conduite des relations internationales. Par suite, les conclusions correspondantes soulèvent des questions qui ne sont pas susceptibles, par leur nature, d'être portées devant la juridiction administrative. Le ministre d'Etat, ministre de la transition énergétique et solidaire, est dès lors fondé à soutenir que ces conclusions doivent être rejetées comme portées devant une juridiction incompétente pour en connaître. <br/>
<br/>
              4. En second lieu, il ressort des pièces du dossier que, pour déterminer la réglementation applicable au loup, qui comporte notamment, en application des articles L. 411-2 et des articles R. 411-1 et suivants du code de l'environnement, un arrêté fixant les conditions et limites dans lesquelles des dérogations à l'interdiction de destruction des loups peuvent être accordées par les préfets et un autre arrêté, de périodicité annuelle, fixant le nombre maximum de loups dont la destruction pourra être autorisée, ainsi que les mesures concernant cette espèce, les autorités compétentes s'appuient sur une méthodologie, reposant principalement sur trois paramètres, soit le nombre de zones de présence permanente du loup (ZPP), qui sont délimitées au vu d'un recueil d'indices de présence de cet animal opéré, de façon régulière, par les agents de l'Office national de la chasse et de la faune sauvage et d'autres observateurs formés à cet effet, l'estimation d'un effectif minimum retenu (EMR), fondée sur un relevé hivernal des traces de loup dans chaque ZPP, enfin l'estimation du nombre total de spécimens présents par une modélisation dite " capture, marquage, recapture " (CMR), qui utilise des indices de présence biologique (excréments, poils, urines, sang...) récoltés sur le terrain, y compris en dehors des ZPP, et tenant compte à la fois des animaux sédentarisés et dispersés. Il ne ressort pas des pièces du dossier, eu égard aux critiques formulées en termes généraux par les requérants qui se bornent à soutenir que la réglementation applicable aux loups serait inadaptée, que les nombres de loups et d'attaques de troupeaux par ces animaux seraient largement sous estimés et que le " plan loup " serait fondé sur des " ratios erronés ", sans indiquer notamment ni quelles dispositions ils entendent précisément contester ni en quoi la méthode présentée ci-dessus ne serait pas pertinente pour apprécier l'évolution de la population de loups et de sa répartition géographique, que le Premier Ministre aurait entaché sa décision d'une erreur manifeste d'appréciation.<br/>
<br/>
              5. Il résulte de ce qui précède que les conclusions tendant à l'annulation de la décision attaquée doivent être rejetées, de même, par voie de conséquence, que les conclusions tendant à l'édiction de diverses mesures d'injonction sous astreinte et que celles présentées au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : L'intervention du syndicat Coordination rurale-Union nationale est admise.<br/>
Article 2 : La requête du Collectif national de préservation des activités agropastorales et rurales et autre est rejetée.<br/>
Article 3 : La présente décision sera notifiée au Collectif national de préservation des activités agropastorales et rurales, premier dénommé, pour l'ensemble des requérants, au syndicat Coordination rurale-Union nationale et au ministre d'Etat, ministre de la transition écologique et solidaire. <br/>
Copie en sera adressée au ministre de l'agriculture et de l'alimentation. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
