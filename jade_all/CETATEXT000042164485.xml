<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042164485</ID>
<ANCIEN_ID>JG_L_2020_07_000000441728</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/16/44/CETATEXT000042164485.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 13/07/2020, 441728, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-07-13</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>441728</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2020:441728.20200713</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
              Par une requête, enregistrée le 9 juillet 2020 au secrétariat du contentieux du Conseil d'Etat, la société Presse Actu LTD demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative :<br/>
              1°) d'ordonner la suspension de l'exécution de la décision de l'Autorité de régulation des communications électroniques et des Postes (ARCEP) de déclarer applicable l'article 5 de la loi n° 47-585 du 2 avril 1947 jusqu'à ce qu'intervienne la décision du Conseil constitutionnel statuant sur sa question prioritaire de constitutionnalité ;<br/>
<br/>
              2°) d'ordonner toutes mesures nécessaires à la sauvegarde de la liberté de diffusion de la presse, la libre communication des pensées et opinions, la liberté d'entreprendre et la liberté du commerce et de l'industrie ;<br/>
<br/>
              3°) d'enjoindre à l'ARCEP de retirer sa décision en l'absence de cadre juridique pour encadrer la mise en oeuvre du refus d'assortiment par les kiosquiers, sous astreinte de 5 000 euros par jour de retard, dans un délai de 15 jours à compter de la notification de l'ordonnance à intervenir ; <br/>
<br/>
              4°) d'enjoindre à l'ARCEP de procéder à l'établissement d'un cadre juridique permettant la liberté de diffusion des premiers numéros d'un périodique ainsi que la diffusion des titres en attente d'une décision favorable de la Commission paritaire des publications et agences de presse (CPPAP) ou tant qu'un refus n'est pas devenu définitif, sans possibilité de refus par les kiosquiers, dans un délai de huit jours à compter de la notification de l'ordonnance sous astreinte de 5 000 euros par jour de retard ; <br/>
<br/>
              5°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Elle soutient que :<br/>
              - sa requête est recevable dès lors que, en premier lieu, la décision de l'ARCEP est constitutive d'un comportement qui justifie l'intervention du juge des référés, en deuxième lieu, la décision attaquée résulte du comportement d'une autorité administrative indépendante et donc une personne morale de droit public et, en troisième lieu, cette décision attaquée a pour effet de lui porter un préjudice en permettant aux kiosquiers de refuser la diffusion de tout titre de presse non agréé par la CPPAP ;<br/>
              - la condition de l'urgence est satisfaite dès lors que, en premier lieu, dix de ses titres sont en attente du renouvellement de leur agrément depuis le 2 août 2019 et risquent à tout moment de ne plus être diffusés par les kiosquiers et, en second lieu, l'incertitude de la diffusion du nouveau titre qu'elle envisage pour la date du 20 juillet 2020 lui fait courir un risque financier important ;<br/>
              - la décision attaquée méconnaît les dispositions combinées des articles 1er et 5 de la loi du 29 juillet 1881 et de l'article 1er de la loi du 2 avril 1947 dès lors qu'elle subordonne la distribution des titres de presse au bon-vouloir des points-de-vente et porte ainsi une atteinte disproportionnée à la liberté de la presse ;<br/>
              - elle porte une atteinte disproportionnée à la libre communication des pensées et opinions rappelée notamment par les stipulations de l'article 10 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales compte tenu du droit octroyé aux kiosquiers et distributeurs de refuser la diffusion de titres ; <br/>
              - elle méconnaît le principe d'égal accès aux distributeurs et le principe d'impartialité des distributeurs vis-à-vis des éditeurs ;<br/>
              - elle méconnaît les dispositions des articles 6 et 11 de la Déclaration des droits de l'homme et du citoyen de 1789 en portant atteinte à la liberté d'expression des éditeurs, à la libre communication des pensées et des opinions et au pluralisme de la presse ;<br/>
              - elle porte une atteinte disproportionnée à la liberté d'entreprendre et à la liberté du commerce et de l'industrie dès lors que, d'une part, elle fait peser un risque économique majeur sur les sociétés éditrices de presse et, d'autre part, elle a pour effet de restreindre l'ouverture du réseau des distributeurs.<br/>
<br/>
              Par un mémoire distinct, enregistré le 10 juillet 2020, présenté en application de l'article 23-5 de l'ordonnance du 7 novembre 1958, la société Presse Actu LTD demande au juge des référés du Conseil d'Etat, à l'appui de sa requête, de renvoyer au Conseil constitutionnel la question de la conformité aux droits et libertés garantis par la Constitution des dispositions de l'article 5 de la loi n° 47-585 du 2 avril 1947. Elle soutient que ces dispositions, applicables au litige et n'ayant pas été déclarées conformes à la Constitution, posent une question nouvelle et sérieuse en ce qu'elles méconnaissent l'article 11 de la Déclaration des droits de l'homme et du citoyen de 1789 qui consacre la libre communication des pensées et des opinions, l'objectif de valeur constitutionnelle de pluralisme des courants de pensées et d'opinions, la liberté d'expression et l'interdiction des régimes d'autorisation préalable. En outre, elle soutient que les dispositions contestées sont entachées d'une incompétence négative et qu'elles sont à l'origine d'une rupture d'égalité devant la loi.<br/>
<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu :<br/>
              - la Constitution et notamment son Préambule ;<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - la loi du 29 juillet 1881 sur la liberté de la presse ;<br/>
              - la loi n° 47-585 du 2 avril 1947 ;<br/>
              - la loi n° 2019-1063 du 18 octobre 2019 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. Le juge des référés se prononce dans un délai de quarante-huit heures ". En vertu de l'article L. 522-3 du même code, le juge des référés peut, par une ordonnance motivée, rejeter une requête sans instruction ni audience lorsque la condition d'urgence n'est pas remplie ou lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable ou qu'elle est mal fondée.<br/>
              2. La société Presse actu LTD demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, en premier lieu, de renvoyer au Conseil constitutionnel la question de la conformité aux droits et libertés garantis par la Constitution des dispositions de l'article 5 de la loi du 2 avril 1947, en deuxième lieu, d'ordonner, sans attendre, la suspension de l'exécution de la décision par laquelle l'ARCEP a déclaré applicable ledit article 5 de la loi précitée ainsi que toutes mesures nécessaires à la sauvegarde des libertés fondamentales en question et, en dernier lieu, qu'il soit fait injonction, sous astreinte, à l'ARCEP de retirer sa décision et de procéder à l'établissement d'un cadre juridique permettant la liberté de diffusion des périodiques et des titres de presse.<br/>
<br/>
              3. Il résulte de la combinaison des dispositions de l'ordonnance n° 58-1067 du 7 novembre 1958 avec celles du livre V du code de justice administrative qu'une question prioritaire de constitutionnalité peut être soulevée devant le juge administratif des référés statuant sur le fondement de l'article L. 521-2 de ce code. Le juge des référés peut en toute hypothèse, y compris lorsqu'une question prioritaire de constitutionnalité est soulevée devant lui, rejeter une requête qui lui est soumise pour incompétence de la juridiction administrative, irrecevabilité ou défaut d'urgence.<br/>
              4. L'usage par le juge des référés des pouvoirs qu'il tient des dispositions de l'article L. 521-2 du code de justice administrative est subordonné à la condition qu'une urgence particulière rende nécessaire l'intervention dans les quarante-huit heures d'une mesure de sauvegarde d'une liberté fondamentale.<br/>
<br/>
              5. Pour justifier de l'urgence à prononcer les mesures demandées, la société Presse Actu LTD se borne à soutenir que les titres qu'elle édite risquent à tout moment de ne plus être diffusés compte tenue de l'absence de renouvellement de leur agrément et que l'incertitude de la diffusion de son nouveau titre à venir lui fait courir un risque financier important. Toutefois, ces circonstances ne sauraient, par elles-mêmes, caractériser l'existence d'une situation d'urgence particulière justifiant l'intervention du juge des référés dans le très bref délai prévu par les dispositions de l'article L. 521-2 du code de justice administrative.<br/>
<br/>
              6. Par suite il y a lieu, sans qu'il soit besoin de se prononcer sur la transmission au Conseil constitutionnel de la question prioritaire de constitutionnalité soulevée, de rejeter l'ensemble des conclusions de la requête, y compris celles présentées au titre de l'article L. 761-1 du code de justice administrative, selon la procédure prévue par l'article L. 522-3 du même code.<br/>
<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de la société Presse Actu LTD est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à la société Presse Actu LTD.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
