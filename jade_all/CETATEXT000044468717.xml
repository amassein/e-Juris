<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044468717</ID>
<ANCIEN_ID>JG_L_2021_12_000000449810</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/46/87/CETATEXT000044468717.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème chambre, 09/12/2021, 449810, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-12-09</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>449810</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR>M. Olivier Guiard</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Céline  Guibé</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:449810.20211209</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société ITD a demandé au tribunal administratif de Lille de prononcer la décharge des cotisations supplémentaires d'impôt sur les sociétés auxquelles elle a été assujettie au titre des exercices clos de 2008 à 2010, ainsi que des rappels de taxe sur la valeur ajoutée mis à sa charge au titre de la période du 1er janvier 2008 au 31 décembre 2010 et des pénalités correspondantes. Par un jugement n° 1508977 du 10 juillet 2018, ce tribunal a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 18DA01839 du 17 décembre 2020, la cour administrative d'appel de Douai a rejeté l'appel formé par la société ITD contre ce jugement.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 17 février et 17 mai 2021 au secrétariat du contentieux du Conseil d'Etat, la société ITD demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Olivier Guiard, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Céline Guibé, rapporteure publique ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Piwnica, Molinié, avocat de la société ITD ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 822-1 du code de justice administrative : " Le pourvoi en cassation devant le Conseil d'Etat fait l'objet d'une procédure préalable d'admission. L'admission est refusée par décision juridictionnelle si le pourvoi est irrecevable ou n'est fondé sur aucun moyen sérieux ".<br/>
<br/>
              2. Pour demander l'annulation de l'arrêt qu'elle attaque, la société ITD soutient que la cour administrative d'appel de Douai :<br/>
              - a commis une triple erreur de droit en jugeant que l'administration fiscale établissait la réalité des ventes non comptabilisées à partir des seuls renseignements obtenus auprès de tiers, en ne tenant pas compte du solde net entre les insuffisances de déclaration de stock à hauteur de 25 038 euros et les excédents de ces mêmes déclarations à hauteur de 21 200 euros, et en confirmant les rappels de taxe sur la valeur ajoutée alors qu'il y avait une insuffisance de déclaration des stocks ;<br/>
              - a dénaturé les pièces du dossier, inexactement qualifié les faits, entaché sa décision d'une contradiction de motifs et commis une erreur de droit en se fondant sur des critères dépourvus de pertinence pour juger que la location d'un bien immobilier constituait un acte anormal de gestion et refuser la déductibilité des charges sans rechercher si les dépenses exposées avaient été engagées dans l'intérêt de l'entreprise, alors qu'elle admettait que l'immeuble en cause avait effectivement été utilisé pour les besoins de son activité professionnelle ;<br/>
              - a commis une erreur de droit en refusant la déduction de la taxe sur la valeur ajoutée grevant les dépenses qu'elle avait exposées dans le cadre de l'aménagement de ses locaux professionnels, au motif que les dépenses en cause étaient constitutives d'un acte anormal de gestion ;<br/>
              - a commis une erreur de droit en jugeant que l'administration fiscale était fondée à remettre en cause ses frais de restaurant à concurrence d'une quote-part de 40 % ;<br/>
              - a commis une erreur de qualification juridique des faits et entaché les motifs de son arrêt de contradiction en jugeant que l'administration fiscale établissait le caractère délibéré des manquements justifiant les pénalités mises à sa charge sur le fondement de l'article 1729 du code général des impôts. <br/>
<br/>
              3. Eu égard aux moyens soulevés, il y a lieu d'admettre les conclusions du pourvoi qui sont dirigées contre l'arrêt attaqué en tant que celui-ci a statué sur le rappel de taxe sur la valeur ajoutée relatif aux dépenses d'aménagement de l'immeuble de Roubaix ainsi que sur la pénalité correspondante. En revanche, s'agissant du surplus des conclusions du pourvoi, aucun des moyens soulevés n'est de nature à permettre leur admission. <br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Les conclusions du pourvoi de la société ITD qui sont dirigées contre l'arrêt attaqué en tant qu'il a statué sur le rappel de taxe sur la valeur ajoutée relatif aux dépenses d'aménagement de l'immeuble de Roubaix ainsi que sur la pénalité correspondante, sont admises.<br/>
Article 2 : Le surplus des conclusions du pourvoi de la société ITD n'est pas admis.<br/>
Article 3 : La présente décision sera notifiée à la société ITD.<br/>
Copie en sera adressée au ministre de l'économie, des finances et de la relance.<br/>
              Délibéré à l'issue de la séance du 25 novembre 2021 où siégeaient : M. Frédéric Aladjidi, président de chambre, présidant ; M. Thomas Andrieu, conseiller d'Etat et M. Olivier Guiard, maître des requêtes-rapporteur. <br/>
<br/>
              Rendu le 9 décembre 2021.<br/>
<br/>
<br/>
                 Le président : <br/>
                 Signé : M. Frédéric Aladjidi<br/>
 		Le rapporteur : <br/>
      Signé : M. Olivier Guiard<br/>
                 La secrétaire :<br/>
                 Signé : Mme A... B...<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
