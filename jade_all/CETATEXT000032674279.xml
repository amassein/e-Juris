<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032674279</ID>
<ANCIEN_ID>JG_L_2016_06_000000383638</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/67/42/CETATEXT000032674279.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème - 9ème chambres réunies, 08/06/2016, 383638</TITRE>
<DATE_DEC>2016-06-08</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>383638</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème - 9ème chambres réunies</FORMATION>
<TYPE_REC/>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BARADUC, DUHAMEL, RAMEIX</AVOCATS>
<RAPPORTEUR>M. Vincent Villette</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Edouard Crépey</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2016:383638.20160608</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
Procédure contentieuse antérieure<br/>
<br/>
              M. B...C...a demandé au tribunal administratif de Pau d'annuler les décisions des 18 janvier et 25 avril 2008, par lesquelles le maire de la commune de Lees-Athas a accordé au nom de l'Etat deux permis de construire à M. et MmeA.... Par un jugement n° 0802254, 0802255 du 4 mai 2010, le tribunal administratif a fait droit à sa demande.<br/>
<br/>
              Par un arrêt n° 10BX02035 du 7 juin 2011, la cour administrative d'appel de Bordeaux, faisant droit à l'appel formé par le ministre de l'écologie, du développement durable, des transports et du logement, a annulé ce jugement du 4 mai 2010 du tribunal administratif de Pau.<br/>
<br/>
              Par une décision n° 351538 du 4 novembre 2013, le Conseil d'Etat statuant au contentieux, faisant droit au pourvoi formé par M.C..., a annulé cet arrêt du 7 juin 2011 de la cour administrative d'appel de Bordeaux et renvoyé l'affaire devant cette cour.<br/>
<br/>
              Par un arrêt n° 13BX03110 du 17 juin 2014, la cour administrative d'appel de Bordeaux, statuant sur renvoi après cassation par le Conseil d'Etat, a rejeté l'appel formé par le ministre de l'écologie, du développement durable, des transports et du logement contre le jugement du tribunal administratif de Pau du 4 mai 2010.<br/>
<br/>
Procédure devant le Conseil d'Etat<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 12 août et 12 novembre 2014 au secrétariat du contentieux du Conseil d'Etat, la ministre du logement, de l'égalité des territoires et de la ruralité demande au Conseil d'Etat d'annuler cet arrêt de la cour administrative d'appel de Bordeaux.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code rural et de la pêche maritime, notamment son article L. 111-3 ;<br/>
              - le code de l'urbanisme ;<br/>
              - l'arrêté du 7 février 2005 du ministre de l'écologie et du développement durable, fixant les règles techniques auxquelles doivent satisfaire les élevages de bovins, de volailles et/ou de gibier à plumes et de porcs soumis à déclaration au titre du livre V du code de l'environnement et ses annexes I et II ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Vincent Villette, auditeur,  <br/>
<br/>
              - les conclusions de M. Edouard Crépey, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Baraduc, Duhamel, Rameix, avocat de M. C...;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes du premier alinéa de l'article L. 111-3 du code rural et de la pêche maritime : " Lorsque des dispositions législatives ou réglementaires soumettent à des conditions de distance l'implantation ou l'extension de bâtiments agricoles vis-à-vis des habitations et immeubles habituellement occupés par des tiers, la même exigence d'éloignement doit être imposée à ces derniers à toute nouvelle construction et à tout changement de destination précités à usage non agricole nécessitant un permis de construire, à l'exception des extensions de constructions existantes " ; que l'article 2.1.1 de l'annexe I de l'arrêté du 7 février 2005 visé ci-dessus dispose que : " les bâtiments d'élevage et leurs annexes sont implantés à au moins 100 mètres des habitations des tiers " ; que cet arrêté précise en son article 2 : " Les dispositions de l'annexe I sont applicables dans un délai de quatre mois à compter de la publication du présent arrêté au Journal officiel. / Pour les installations existantes, déclarées au plus tard quatre mois après la publication du présent arrêté au Journal officiel, les dispositions mentionnées à l'annexe II sont applicables dans les délais suivants : (...) au plus tard le 31 décembre 2010 " ; que l'annexe II de cet arrêté prévoit que les règles d'implantation fixées à l'annexe I sont applicables dans les délais prévus à l'article 2 ; qu'enfin, aux termes du 2.1.4 de l'annexe I de cet arrêté : " Les dispositions du 2.1.1 (...) ne s'appliquent, dans le cas des extensions des élevages en fonctionnement régulier, qu'aux nouveaux bâtiments d'élevage ou à leurs annexes nouvelles " ;<br/>
<br/>
              2. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que M. et Mme A...ont obtenu les 18 janvier et 25 avril 2008 du maire de la commune de Lees-Athas, agissant au nom de l'Etat, deux permis de construire pour la réalisation de maisons à usage d'habitation, situées à 50 mètres des bâtiments d'élevage de bovins de l'exploitation agricole de M.C..., déclarée au titre des dispositions du livre V du code de l'environnement ; que, statuant sur renvoi après cassation par le Conseil d'Etat de son arrêt du 7 juin 2011, la cour administrative d'appel de Bordeaux a rejeté l'appel formé par la ministre de l'écologie, du développement durable, des transports et du logement contre le jugement du 14 mai 2010 par lequel le tribunal administratif de Pau a fait droit à la demande de M. C... tendant à l'annulation des permis de construire délivrés à M. et Mme A... ;<br/>
<br/>
              3. Considérant que, eu égard à l'objet des dispositions précitées de l'article L. 111-3 du code rural et de la pêche maritime, notamment du parallélisme qu'elles établissent entre les exigences qui pèsent sur l'implantation ou l'extension des bâtiments agricoles et sur les nouvelles constructions à usage non agricole, la circonstance que les dispositions de l'arrêté du 7 février 2005 prévoient, pour les bâtiments d'élevage existants, une application différée des règles de distance est sans incidence sur les conditions d'application, en vertu des dispositions précitées de l'article L. 111-3 du code rural et de la pêche maritime, des règles de distance aux nouvelles constructions à usage non agricole ; que, dès lors, c'est sans erreur de droit que la cour administrative d'appel de Bordeaux a fait application, à la date à laquelle les permis de construire ont été accordés aux épouxA..., de l'exigence d'éloignement de 100 mètres posée par l'arrêté du 7 février 2005 et a jugé que les constructions litigieuses ne respectaient pas les dispositions combinées de l'article L. 111-3 du code rural et de la pêche maritime et de cet arrêté ; qu'il suit de là que le pourvoi de la ministre du logement, de l'égalité des territoires et de la ruralité doit être rejeté ;<br/>
<br/>
              4. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat une somme de 3 000 euros à verser à M. C...au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de la ministre du logement, de l'égalité des territoires et de la ruralité est rejeté.<br/>
Article 2 : L'Etat versera à M. C...une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée à la ministre du logement et de l'habitat durable et à M. B...C....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">44-02-01-01 NATURE ET ENVIRONNEMENT. INSTALLATIONS CLASSÉES POUR LA PROTECTION DE L'ENVIRONNEMENT. CHAMP D'APPLICATION DE LA LÉGISLATION. INDÉPENDANCE À L'ÉGARD D'AUTRES LÉGISLATIONS. - EXIGENCES D'ÉLOIGNEMENT DES BÂTIMENTS AGRICOLES PESANT SYMÉTRIQUEMENT SUR LES NOUVEAUX BÂTIMENTS AGRICOLES ET LES NOUVELLES CONSTRUCTIONS À USAGE NON AGRICOLE (ART. L. 111-3 DU CRPM) [RJ1] - APPLICATION DIFFÉRÉE DES RÈGLES DE DISTANCE POUR DES BÂTIMENTS AGRICOLES EXISTANTS - INCIDENCE SUR L'APPLICATION DES RÈGLES DE DISTANCE AUX NOUVELLES CONSTRUCTIONS À USAGE NON AGRICOLE - ABSENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">68-03-03-01-05 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. PERMIS DE CONSTRUIRE. LÉGALITÉ INTERNE DU PERMIS DE CONSTRUIRE. LÉGALITÉ AU REGARD DE LA RÉGLEMENTATION NATIONALE. DIVERSES DISPOSITIONS LÉGISLATIVES OU RÉGLEMENTAIRES. - EXIGENCES D'ÉLOIGNEMENT DES BÂTIMENTS AGRICOLES PESANT SYMÉTRIQUEMENT SUR LES NOUVEAUX BÂTIMENTS AGRICOLES ET LES NOUVELLES CONSTRUCTIONS À USAGE NON AGRICOLE (ART. L. 111-3 DU CRPM) [RJ1] - APPLICATION DIFFÉRÉE DES RÈGLES DE DISTANCE POUR DES BÂTIMENTS AGRICOLES EXISTANTS - INCIDENCE SUR L'APPLICATION DES RÈGLES DE DISTANCE AUX NOUVELLES CONSTRUCTIONS À USAGE NON AGRICOLE - ABSENCE.
</SCT>
<ANA ID="9A"> 44-02-01-01 Eu égard à l'objet des dispositions de l'article L. 111-3 du code rural et de la pêche maritime (CRPM), notamment du parallélisme qu'elles établissent entre les exigences d'éloignement qui pèsent sur l'implantation ou l'extension des bâtiments agricoles et sur les nouvelles constructions à usage non agricole, la circonstance que les dispositions de l'arrêté du 7 février 2005 du ministre de l'écologie et du développement durable, fixant les règles techniques auxquelles doivent satisfaire les élevages de bovins, de volailles et/ou de gibier à plumes et de porcs soumis à déclaration au titre du livre V du code de l'environnement et ses annexes I et II prévoient, pour les bâtiments d'élevage existants, une application différée des règles de distance est sans incidence sur les conditions d'application, en vertu des dispositions de l'article L. 111-3 du code rural et de la pêche maritime, des règles de distance aux nouvelles constructions à usage non agricole.</ANA>
<ANA ID="9B"> 68-03-03-01-05 Eu égard à l'objet des dispositions de l'article L. 111-3 du code rural et de la pêche maritime (CRPM), notamment du parallélisme qu'elles établissent entre les exigences d'éloignement qui pèsent sur l'implantation ou l'extension des bâtiments agricoles et sur les nouvelles constructions à usage non agricole, la circonstance que les dispositions de l'arrêté du 7 février 2005 du ministre de l'écologie et du développement durable, fixant les règles techniques auxquelles doivent satisfaire les élevages de bovins, de volailles et/ou de gibier à plumes et de porcs soumis à déclaration au titre du livre V du code de l'environnement et ses annexes I et II prévoient, pour les bâtiments d'élevage existants, une application différée des règles de distance est sans incidence sur les conditions d'application, en vertu des dispositions de l'article L. 111-3 du code rural et de la pêche maritime, des règles de distance aux nouvelles constructions à usage non agricole.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf., sur l'application de ces règles de distance aux constructions à usage non agricole, CE, 24 février 2016, EARL Enderlin Marcel, n° 380556, à mentionner aux Tables.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
