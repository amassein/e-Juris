<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037802631</ID>
<ANCIEN_ID>JG_L_2018_11_000000421443</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/80/26/CETATEXT000037802631.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème chambre, 28/11/2018, 421443, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-11-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>421443</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>LE PRADO ; SCP DIDIER, PINET</AVOCATS>
<RAPPORTEUR>M. Jean-Dominique Langlais</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:421443.20181128</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. et Mme E...A..., agissant tant en leur nom propre qu'en qualité de représentants légaux de leurs enfants mineurs F...etB..., ont demandé au tribunal administratif de Marseille, à titre principal, d'ordonner une nouvelle expertise sur les conditions de prise en charge de leur fils C...par l'Assistance publique - Hôpitaux de Marseille (AP-HM) et, à titre subsidiaire, de condamner l'AP-HM à leur verser la somme de 133 686 euros en réparation des préjudices ayant résulté pour eux du décès de l'enfant. Par un jugement n° 1308339 du 29 juin 2015, le tribunal administratif de Marseille a condamné l'AP-HM à verser la somme de 2 001,50 euros à M. et Mme A...en qualité d'ayants droit de leur fils, au prorata des droits détenus par eux dans la succession deC..., au titre des préjudices propres de celui-ci et, sous réserve de déduction des provisions déjà versées, les sommes de 2 000 euros chacun à M. et Mme A...et de 1 400 euros chacun à leurs enfants mineurs, au titre de leur préjudice d'affection. <br/>
<br/>
              Par un arrêt n° 15MA03876 du 18 janvier 2018, la cour administrative d'appel de Marseille a, sur appel des consortsA..., porté ces sommes à, respectivement, 2 100 euros, 2 500 euros et 2 000 euros. <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 12 juin et 13 septembre 2018 au secrétariat du contentieux du Conseil d'Etat, M. et Mme A...demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) de mettre à la charge de l'AP - HM la somme de 3 000 euros à verser à la SCP Didier, Pinet, leur avocat, au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Dominique Langlais, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Didier, Pinet, avocat de M. et MmeA....<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'aux termes de l'article L. 822-1 du code de justice administrative : " Le pourvoi en cassation devant le Conseil d'Etat fait l'objet d'une procédure préalable d'admission. L'admission est refusée par décision juridictionnelle si le pourvoi est irrecevable ou n'est fondé sur aucun moyen sérieux " ; <br/>
<br/>
              2. Considérant que, pour demander l'annulation de l'arrêt de la cour administrative d'appel de Marseille qu'ils attaquent, M. et Mme A...soutiennent que la cour :<br/>
              - a dénaturé les pièces du dossier et insuffisamment motivé son arrêt en retenant, sans s'en expliquer, qu'un second contrôle du rythme cardiaque de leur enfant C...et la vérification de sa tension artérielle le 3 janvier 2011 n'auraient pas nécessairement conduit à pratiquer, en l'absence de troubles hémodynamiques patents ou de signes de détresse respiratoire, des examens complémentaires qui auraient permis de diagnostiquer l'infection ;<br/>
              - a commis une erreur de droit en s'abstenant de rechercher, pour évaluer la chance perdue d'échapper au décès, si le jeune C...aurait dû être gardé en observation et, dans l'affirmative, si son infection aurait pu être diagnostiquée et prise en charge plus tôt ;<br/>
              - a commis une erreur de droit en refusant d'indemniser la perte de revenus résultant des congés pris pour les obsèques de leur enfant au Maroc ;<br/>
              - a commis une erreur de droit et méconnu son office en rejetant la demande d'indemnisation des frais d'obsèques du jeune C...sans apprécier elle-même le montant du préjudice, au besoin en faisant usage de ses pouvoirs d'instruction ;<br/>
<br/>
              3. Considérant qu'eu égard aux moyens soulevés, il y a lieu d'admettre les conclusions du pourvoi qui sont dirigées contre l'arrêt en tant qu'il s'est prononcé sur les conclusions relatives à l'indemnisation des frais d'obsèques de l'enfant ; qu'en revanche, s'agissant des conclusions dirigées contre l'arrêt attaqué en tant qu'il s'est prononcé sur les autres conclusions des requérants, aucun des moyens soulevés n'est de nature à permettre l'admission de ces conclusions ;<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Les conclusions du pourvoi de M. et Mme A...qui sont dirigées contre l'arrêt attaqué en tant qu'il s'est prononcé sur leurs conclusions relatives à l'indemnisation des frais d'obsèques de leur fils sont admises.<br/>
Article 2 : Le surplus des conclusions du pourvoi de M. et Mme A...n'est pas admis.<br/>
<br/>
Article 3 : La présente décision sera notifiée à M. E...A...et Mme D...A....<br/>
Copie en sera adressée à l'Assistance publique - Hôpitaux de Marseille, à la caisse primaire d'assurance maladie de Marseille et à la ministre des solidarités et de la santé.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
