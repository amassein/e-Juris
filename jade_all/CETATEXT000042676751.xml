<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042676751</ID>
<ANCIEN_ID>JG_L_2020_12_000000446716</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/67/67/CETATEXT000042676751.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 04/12/2020, 446716, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-12-04</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>446716</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC/>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2020:446716.20201204</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, enregistrée le 20 novembre 2020 au secrétariat du contentieux du Conseil d'Etat, Mme B... A... demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, de suspendre l'exécution de l'article 4 du décret n° 2020-1310 du 29 octobre 2020.<br/>
<br/>
<br/>
              Elle soutient que : <br/>
              - la condition d'urgence est satisfaite ; <br/>
              - il est porté une atteinte grave et manifestement illégale à la liberté d'aller et venir ainsi qu'au droit à la vie privée et familiale, à la liberté individuelle, à la liberté de réunion, au principe de primauté du droit de l'Union européenne et à l'égalité devant la loi ;<br/>
              - l'article 4 méconnaît le droit de libre circulation au sein de l'Union européenne; <br/>
              - il méconnaît par son imprécision le principe d'égalité devant la loi ;<br/>
              - il méconnaît la liberté d'aller et venir, le droit à une vie privée et familiale, la liberté de réunion et la liberté individuelle en raison du confinement qu'il instaure ;<br/>
              -il est entaché d'erreur manifeste d'appréciation.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de la santé publique ;<br/>
              - le décret n° 2020-1257 du 14 octobre 2020 ;<br/>
              - le décret n° 2020-1310 du 29 octobre 2020 ;<br/>
              - le décret n° 2020-1454 du 27 novembre 2020 ; <br/>
              - le code de justice administrative ;<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. Le juge des référés se prononce dans un délai de quarante-huit heures. ". En vertu de l'article L. 522-3 du même code, le juge des référés peut, par une ordonnance motivée, rejeter une requête sans instruction ni audience lorsque la condition d'urgence n'est pas remplie ou lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable ou qu'elle est mal fondée.<br/>
<br/>
              2. Aux termes de l'article L. 3131-12 du code de la santé publique, issu de la loi du 23 mars 2020 d'urgence pour faire face à l'épidémie de covid-19 : " L'état d'urgence sanitaire peut être déclaré sur tout ou partie du territoire métropolitain ainsi que du territoire des collectivités régies par les articles 73 et 74 de la Constitution et de la Nouvelle-Calédonie en cas de catastrophe sanitaire mettant en péril, par sa nature et sa gravité, la santé de la population ". L'article L. 3131-13 du même code précise que " L'état d'urgence sanitaire est déclaré par décret en conseil des ministres pris sur le rapport du ministre chargé de la santé. Ce décret motivé détermine la ou les circonscriptions territoriales à l'intérieur desquelles il entre en vigueur et reçoit application. Les données scientifiques disponibles sur la situation sanitaire qui ont motivé la décision sont rendues publiques. / (...) / La prorogation de l'état d'urgence sanitaire au-delà d'un mois ne peut être autorisée que par la loi, après avis du comité de scientifiques prévu à l'article L. 3131-19 ". Aux termes de l'article L. 3131-15 du même code : " Dans les circonscriptions territoriales où l'état d'urgence sanitaire est déclaré, le Premier ministre peut, par décret réglementaire pris sur le rapport du ministre chargé de la santé, aux seules fins de garantir la santé publique " prendre un certain nombre de mesures de restriction ou d'interdiction des déplacements, activités et réunions, notamment " Interdire aux personnes de sortir de leur domicile, sous réserve des déplacements strictement indispensables aux besoins familiaux ou de santé (...) " à condition d'être " strictement proportionnées aux risques sanitaires encourus et appropriées aux circonstances de temps et de lieu ".<br/>
<br/>
              3. L'émergence d'un nouveau coronavirus, responsable de la maladie à coronavirus 2019 ou covid-19 et particulièrement contagieux, a été qualifiée d'urgence de santé publique de portée internationale par l'Organisation mondiale de la santé le 30 janvier 2020, puis de pandémie le 11 mars 2020. La propagation du virus sur le territoire français a conduit le ministre chargé de la santé puis le Premier ministre à prendre, à compter du 4 mars 2020, des mesures de plus en plus strictes destinées à réduire les risques de contagion. Pour faire face à l'aggravation de l'épidémie, la loi du 23 mars 2020 a créé un régime d'état d'urgence sanitaire, défini aux articles L. 3131-12 à L. 3131-20 du code de la santé publique, et a déclaré l'état d'urgence sanitaire pour une durée de deux mois à compter du 24 mars 2020. La loi du 11 mai 2020 prorogeant l'état d'urgence sanitaire et complétant ces dispositions, a prorogé cet état d'urgence sanitaire jusqu'au 10 juillet 2020. L'évolution de la situation sanitaire a conduit à un assouplissement des mesures prises et la loi du 9 juillet 2020 a organisé un régime de sortie de cet état d'urgence. <br/>
<br/>
              4. Une nouvelle progression de l'épidémie au cours des mois de septembre et d'octobre, dont le rythme n'a cessé de s'accélérer au cours de cette période, a conduit le Président de la République à prendre le 14 octobre dernier, sur le fondement des articles L. 3131-12 et L. 3131-13 du code de la santé publique, un décret déclarant l'état d'urgence à compter du 17 octobre sur l'ensemble du territoire national. Le 29 octobre 2020, le Premier ministre a pris, sur le fondement de l'article L. 3131-15 du code de la santé publique, le décret contesté, prescrivant les mesures générales nécessaires pour faire face à l'épidémie de covid-19 dans le cadre de l'état d'urgence sanitaire.<br/>
<br/>
              5. Il résulte des données scientifiques publiées que la circulation du virus sur le territoire métropolitain s'est fortement amplifiée au cours des dernières semaines, malgré les mesures prises, conduisant à une situation particulièrement dangereuse pour la santé de l'ensemble de la population française. Ainsi, à la date du 16 novembre 2020, plus de 1 991 233 cas ont été confirmés positifs à la covid-19, en augmentation de près de 9 406 dans les dernières vingt-quatre heures, le taux d'incidence national étant de 257 cas pour 100 000 habitants contre 246 au 20 octobre et 118 au 28 septembre, le taux de positivité des tests réalisés étant de 16,4 % au 16 novembre contre 13,2 % au 18 octobre et 9 % au 28 septembre, 45 054 décès de la covid-19 sont à déplorer au 16 novembre 2020, en hausse de 508 cas en vingt-quatre heures. Enfin, la France a atteint le nombre inégalé au 16 novembre 2020 de 33 497 personnes hospitalisées, mettant sous tension l'ensemble du système de santé et rendant nécessaire, au cours des derniers jours, des transferts de patients entre régions et avec des pays voisins ainsi que des déprogrammations d'hospitalisations non urgentes.<br/>
<br/>
              6. Pour faire face à cette situation d'urgence sanitaire, le gouvernement, en prenant les mesures détaillées par le décret du 29 octobre 2020, a fait le choix d'une politique qui cherche à casser la dynamique actuelle de progression du virus par la stricte limitation des déplacements de personnes hors de leur domicile. A cette fin, il a, à l'article 4 du décret, interdit tout déplacement des personnes hors de leur lieu de résidence et fixé une liste limitative des exceptions à cette interdiction.<br/>
<br/>
              7. Dans ces conditions, eu égard, d'une part, à la gravité actuelle de la situation sanitaire, marquée par l'existence de risques importants pesant sur l'ensemble de la population à la suite de la très large diffusion du virus sur tout le territoire national, d'autre part, à la tension très forte pesant sur le système de santé compte tenu des capacités de soins déjà mobilisées et susceptibles de l'être, enfin aux assouplissements aux mesures de confinement apportés par le décret du 27 novembre 2020 qui a modifié l'article 4 dont la suspension est demandée, aucun des moyens soulevés par la requérante n'est de nature à caractériser une atteinte grave et manifestement illégale portée aux libertés fondamentales. <br/>
<br/>
              8. Il résulte de ce qui précède, sans qu'il soit besoin de se prononcer sur la condition d'urgence, que la requête de Mme A... doit être rejetée, selon la procédure prévue à l'article L. 522-3 du code de justice administrative.<br/>
<br/>
<br/>
<br/>			O R D O N N E :<br/>
              			------------------<br/>
<br/>
Article 1er : La requête de Mme A... est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à Mme B... A....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
