<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030625087</ID>
<ANCIEN_ID>JG_L_2015_05_000000388273</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/62/50/CETATEXT000030625087.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère sous-section jugeant seule, 22/05/2015, 388273, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-05-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>388273</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère sous-section jugeant seule</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Philippe Combettes</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Alexandre Lallet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:388273.20150522</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
Procédure contentieuse antérieure<br/>
<br/>
              La Commission nationale des comptes de campagne et des financements politiques, sur le fondement de l'article L. 52-15 du code électoral, a saisi le tribunal administratif de Versailles de la décision du 20 novembre 2014 par laquelle elle a constaté l'absence de dépôt du compte de campagne de M. G...F..., élu conseiller municipal de Grigny (Essonne) et conseiller communautaire de Grigny à la communauté d'agglomération les Lacs de l'Essonne le 23 mars 2014. <br/>
<br/>
              Par un jugement n° 1408222 du 3 février 2015, le tribunal administratif de Versailles a déclaré M. F...inéligible pour une durée d'un an et démissionnaire d'office de ses mandats de conseiller municipal et de conseiller communautaire et a proclamé Mme D...M'A... élue conseillère municipale et conseillère communautaire. <br/>
<br/>
Procédure devant le Conseil d'Etat<br/>
<br/>
              Par une requête et par un mémoire en réplique, enregistrés les 25 février et 27 avril 2015 au secrétariat du contentieux du Conseil d'Etat, M. F...demande au Conseil d'Etat d'annuler ce jugement du tribunal administratif de Versailles du 3 février 2015. <br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code électoral ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Philippe Combettes, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Alexandre Lallet, rapporteur public.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              Sur la régularité du jugement attaqué :<br/>
<br/>
              1. Il ressort des pièces de la procédure devant le tribunal administratif de Versailles et des mentions du jugement attaqué que M. F...a été régulièrement convoqué à l'audience du 20 janvier 2015, à laquelle il n'a pas été présent. Par suite, il n'est pas fondé à soutenir que le tribunal, faute de l'avoir mis en mesure de s'expliquer, aurait statué au terme d'une procédure irrégulière. <br/>
<br/>
              Sur l'inéligibilité de M.F... :<br/>
<br/>
              2. L'article L. 52-12 du code électoral dispose que : " Chaque candidat ou candidat tête de liste soumis au plafonnement prévu à l'article L. 52-11 et qui a obtenu au moins 1 % des suffrages exprimés est tenu d'établir un compte de campagne (...) / Au plus tard avant 18 heures le dixième vendredi suivant le premier tour de scrutin, chaque candidat ou candidat tête de liste présent au premier tour dépose à la Commission nationale des comptes de campagne et des financements politiques son compte de campagne et ses annexes accompagné des justificatifs de ses recettes ainsi que des factures, devis et autres documents de nature à établir le montant des dépenses payées ou engagées par le candidat ou pour son compte. (...) Cette présentation n'est pas nécessaire lorsqu'aucune dépense ou recette ne figure au compte de campagne. Dans ce cas, le mandataire établit une attestation d'absence de dépense et de recette (...) ". L'article L. 52-15 du même code prévoit que : " La Commission nationale des comptes de campagne et des financements politiques approuve et, après procédure contradictoire, rejette ou réforme les comptes de campagne (...) / (...) Lorsque la commission a constaté que le compte de campagne n'a pas été déposé dans le délai prescrit, (...) la commission saisit le juge de l'élection (...) ". Enfin, selon l'article L. 118-3, dans sa rédaction alors applicable : " Saisi par la commission instituée par l'article L. 52-14, (...) le juge de l'élection peut déclarer inéligible le candidat qui n'a pas déposé son compte de campagne dans les conditions et le délai prescrits à l'article L. 52-12. / (...) L'inéligibilité (...) est prononcée pour une durée maximale de trois ans et s'applique à toutes les élections. Toutefois, elle n'a pas d'effet sur les mandats acquis antérieurement à la date de la décision (...) ".<br/>
<br/>
              3. Il appartient au juge de l'élection, pour apprécier s'il y a lieu de faire usage de la faculté, donnée par les dispositions de l'article L. 118-3 du code électoral, de déclarer inéligible un candidat qui n'a pas déposé son compte de campagne dans les conditions et délai prescrits à l'article L. 52-12 du même code, de tenir compte, eu égard à la nature de la règle méconnue, du caractère délibéré ou non du manquement, de l'existence éventuelle d'autres motifs d'irrégularité du compte, du montant des sommes en cause ainsi que de l'ensemble des circonstances de l'espèce. <br/>
<br/>
              4. Il est constant que M. F..., candidat tête de la liste " Rassembler pour changer Grigny ", élu conseiller municipal de Grigny et conseiller communautaire de Grigny à la communauté d'agglomération les Lacs de l'Essonne le 23 mars 2014, n'a pas déposé son compte de campagne dans le délai imparti par l'article L. 52-12 du code électoral. Il a, ainsi, méconnu une obligation substantielle. Pour expliquer ce manquement, il se contente d'invoquer la modestie des recettes encaissées et des dépenses exposées, inférieures à 4 300 euros, de faire état de l'indisponibilité de son mandataire financier en raison de problèmes personnels et d'un déplacement à l'étranger et d'affirmer, sans d'ailleurs apporter de justificatif à l'appui de ses allégations, que son compte de campagne a été déposé ultérieurement. Il ne fait ainsi état d'aucune circonstance propre à justifier la méconnaissance de l'obligation résultant de l'article L. 52-12 du code électoral, alors qu'il reconnaît avoir engagé des dépenses de campagne.<br/>
<br/>
              5. Il résulte de ce qui précède que  M. F... n'est pas fondé à soutenir que c'est à tort que le tribunal administratif de Versailles l'a déclaré inéligible pour une durée d'un an et démissionnaire d'office de ses mandats de conseiller municipal et de conseiller communautaire. <br/>
<br/>
              Sur les conséquences à tirer de cette inéligibilité :<br/>
<br/>
              6. Le premier alinéa de l'article L. 273-6 du code électoral dispose que : " Les conseillers communautaires représentant les communes de 1 000 habitants et plus au sein des organes délibérants des communautés de communes, des communautés d'agglomération, des communautés urbaines et des métropoles sont élus en même temps que les conseillers municipaux et figurent sur la liste des candidats au conseil municipal ". Le premier alinéa de l'article L. 273-8 du même code prévoit que : " Les sièges de conseiller communautaire sont répartis entre les listes par application aux suffrages exprimés lors de cette élection des règles prévues à l'article L. 262. Pour chacune des listes, les sièges sont attribués dans l'ordre de présentation des candidats ". Enfin, il résulte des premier et dernier alinéas de l'article L. 273-10 du même code que le juge qui constate l'inéligibilité d'un candidat annule son élection et proclame en conséquence l'élection du " candidat de même sexe élu conseiller municipal (...) suivant sur la liste des candidats aux sièges de conseiller communautaire sur laquelle le conseiller à remplacer a été élu ". <br/>
<br/>
              7. Il résulte de l'instruction que M.F..., MmeC..., M. E... et Mme M'A..., placés respectivement en première, deuxième, troisième et quatrième positions sur la liste " Rassembler pour changer Grigny ", étaient candidats tant au conseil municipal qu'au conseil communautaire. La liste ayant remporté 18,01 % des suffrages exprimés, elle s'est vu attribuer trois sièges au conseil municipal et deux sièges au conseil communautaire. Si c'est à bon droit que le tribunal administratif de Versailles, après avoir déclaré M. F...démissionnaire d'office de ses mandats de conseiller municipal et de conseiller communautaire, a proclamé Mme M'A... élue conseillère municipale, c'est, en revanche, à tort qu'il l'a proclamée élue conseillère communautaire. Il y a lieu, par application des dispositions de l'article L. 273-10 du code électoral, de proclamer élu conseiller communautaire non Mme M'A... mais M. E...et de réformer sur ce point le jugement du tribunal administratif de Versailles.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : La requête de M. F...est rejetée.<br/>
Article 2 : M. B...E...est proclamé élu conseiller communautaire de la commune de Grigny à la communauté d'agglomération les Lacs de l'Essonne en lieu et place de M.F....<br/>
Article 3 : Le jugement du tribunal administratif de Versailles du 3 février 2015 est réformé en ce qu'il a de contraire à la présente décision.<br/>
Article 4 : La présente décision sera notifiée à M. G...F..., à la Commission nationale des comptes de campagne et des financements politiques et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
