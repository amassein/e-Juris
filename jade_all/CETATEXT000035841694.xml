<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000035841694</ID>
<ANCIEN_ID>JG_L_2017_10_000000398615</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/35/84/16/CETATEXT000035841694.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère - 6ème chambres réunies, 20/10/2017, 398615</TITRE>
<DATE_DEC>2017-10-20</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>398615</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère - 6ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FABIANI, LUC-THALER, PINATEL</AVOCATS>
<RAPPORTEUR>Mme Marie Sirinelli</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Rémi Decout-Paolini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:398615.20171020</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, enregistrée le 8 avril 2016 au secrétariat du contentieux du Conseil d'Etat, la SELARL Docteur Dominique Debray et la SELARL Docteur Jean-Jacques Piétriga demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision implicite par laquelle le Premier ministre a rejeté leur demande du 4 novembre 2015 tendant à ce qu'il prononce, sur le fondement de l'article L. 1151-3 du code de la santé publique, l'interdiction des actes de lipoaspiration à visée esthétique ;<br/>
<br/>
              2°) d'enjoindre au Premier ministre de prononcer l'interdiction sollicitée dans un délai de six mois à compter de la notification de la décision à intervenir, sous astreinte de 200 euros par jour de retard ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 4 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de la santé publique ;<br/>
              - le décret n° 2011-382 du 11 avril 2011 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Marie Sirinelli, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Rémi Decout-Paolini, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Fabiani, Luc-Thaler, Pinatel, avocat de la SEARL Docteur Dominique Debray et de la SELARL Docteur Jean-Jacques Pietriga.<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier que, par une lettre adressée au Premier ministre le 4 novembre 2015, la SELARL Docteur Dominique Debray et la SELARL Docteur Jean-Jacques Piétriga ont demandé à celui-ci de faire usage des pouvoirs qu'il tire de l'article L. 1151-3 du code de la santé publique pour interdire les actes de lipoaspiration à visée esthétique. Ces sociétés demandent l'annulation pour excès de pouvoir de la décision implicite de rejet née du silence gardé par le Premier ministre sur leur demande.<br/>
<br/>
              2. En premier lieu, aux termes, d'une part, de l'article L. 1151-3 du code de la santé publique : " Les actes à visée esthétique dont la mise en oeuvre présente un danger grave ou une suspicion de danger grave pour la santé humaine peuvent être interdits par décret après avis de la Haute Autorité de santé. Toute décision de levée de l'interdiction est prise en la même forme ". <br/>
<br/>
              3. Aux termes, d'autre part, de l'article L. 6322-1 du code de la santé publique : " Une intervention de chirurgie esthétique, y compris dans les établissements de santé mentionnés au livre Ier, ne peut être pratiquée que dans des installations satisfaisant à des conditions techniques de fonctionnement. Celles-ci font l'objet d'une certification dans les conditions prévues à l'article L. 6113-3. / La création de ces installations est soumise à l'autorisation de l'autorité administrative territorialement compétente. L'autorisation, qui entraîne la possibilité de fonctionner, est accordée pour une durée limitée renouvelable. Elle est subordonnée au résultat d'une visite de conformité sollicitée par la personne autorisée et menée par l'autorité administrative compétente. / (...) L'autorisation peut être suspendue totalement ou partiellement, ou peut être retirée par l'autorité administrative compétente pour les motifs et dans les conditions prévues à l'article L. 6122-13. (...) ". Aux termes de l'article L. 6322-3 du même code : " Les conditions d'autorisation des installations mentionnées à l'article L. 6322-1 sont fixées par décret en Conseil d'Etat. Les conditions techniques de leur fonctionnement et la durée du délai prévu à l'article L. 6322-2 sont fixées par décret ". Sur ce fondement, les articles D. 6322-31 et suivants de ce code définissent notamment les conditions auxquelles les installations de chirurgie esthétique doivent satisfaire et la composition de l'équipe médicale y pratiquant les interventions de chirurgie esthétique.<br/>
<br/>
              4. Il ressort des pièces du dossier que les actes à visée esthétique de lipoaspiration constituent des interventions de chirurgie esthétique qui, réalisées suivant des techniques mises au point dès les années soixante-dix et régulièrement améliorées depuis lors, ne peuvent être pratiquées que dans les conditions fixées par les articles L. 6322-1 et suivants du code de la santé publique ainsi que par les dispositions réglementaires prises pour leur application. Au regard de cet encadrement législatif et réglementaire, dont l'insuffisance n'est ni démontrée ni même alléguée, et alors même que de tels actes de chirurgie peuvent exceptionnellement entraîner des complications ayant des conséquences graves, le Premier ministre n'a pas commis d'erreur manifeste d'appréciation en estimant que la mise en oeuvre des actes de lipoaspiration ne présentait pas un danger grave ou une suspicion de danger grave justifiant qu'il soit fait application des dispositions de l'article L. 1151-3 précité du code de la santé publique pour en interdire la pratique.<br/>
<br/>
              5. En second lieu, il résulte de l'interprétation donnée par la Cour de justice de l'Union européenne des stipulations des articles 49 et 56 du traité sur le fonctionnement de l'Union européenne, en particulier par l'arrêt rendu le 19 mai 2009 dans les affaires C-171/07 et C-172/07, que la liberté d'établissement et la libre prestation de services peuvent faire l'objet de restrictions justifiées par des raisons impérieuses d'intérêt général, dès lors que ces mesures s'appliquent de manière non discriminatoire, sont propres à garantir la réalisation de l'objectif qu'elles poursuivent et ne vont pas au-delà de ce qui est nécessaire pour l'atteindre. En l'espèce, toutefois, si les requérants soutiennent que la décision contestée méconnaît les articles 49 et 56 du traité sur le fonctionnement de l'Union européenne, dès lors que, combinée avec les dispositions du décret du 11 avril 2011 relatif à l'interdiction de la pratique d'actes de lyse adipocytaire à visée esthétique, elle laisse subsister une réglementation nationale qui n'est ni cohérente, ni systématique, une telle décision, refusant d'interdire la pratique de certains actes, ne saurait être regardée comme une restriction à la liberté d'établissement ni à la libre prestation de services. Par suite, le moyen tiré de la méconnaissance des principes de liberté d'établissement et de libre prestation de services du droit de l'Union européenne ne peut qu'être écarté.<br/>
<br/>
              6. Il résulte de ce qui précède, sans qu'il soit besoin de statuer sur la fin de non-recevoir opposée par le ministre des affaires sociales et de la santé, que la SELARL Docteur Dominique Debray et la SELARL Docteur Jean-Jacques Piétriga ne sont pas fondées à demander l'annulation de la décision implicite par laquelle le Premier ministre a rejeté leur demande tendant à ce qu'il prenne, sur le fondement des dispositions de l'article L. 1151-3 du code de la santé publique, un décret interdisant les actes de lipoaspiration à visée esthétique. <br/>
<br/>
              7. Il suit de là que les conclusions des sociétés requérantes à fin d'injonction ainsi que celles qu'elles présentent au titre de l'article L. 761-1 du code de justice administrative doivent être également rejetées.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de la SELARL Docteur Dominique Debray et de la SELARL Docteur Jean-Jacques Piétriga est rejetée. <br/>
Article 2 : La présente décision sera notifiée à la SELARL Docteur Dominique Debray, à la SELARL Docteur Jean-Jacques Piétriga, au Premier ministre et à la ministre des solidarités et de la santé.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">61-01-01 SANTÉ PUBLIQUE. PROTECTION GÉNÉRALE DE LA SANTÉ PUBLIQUE. POLICE ET RÉGLEMENTATION SANITAIRE. - CHIRURGIE ESTHÉTIQUE - ACTES À VISÉE ESTHÉTIQUE DONT LA MISE EN OEUVRE PRÉSENTE UN DANGER GRAVE OU UNE SUSPICION DE DANGER GRAVE POUR LA SANTÉ HUMAINE POUVANT ÊTRE INTERDITS PAR DÉCRET APRÈS AVIS DE LA HAS (ART. L. 1151-3 DU CODE DE LA SANTÉ PUBLIQUE) - ACTES DE LIPOASPIRATION - EXCLUSION.
</SCT>
<ANA ID="9A"> 61-01-01 Les actes à visée esthétique de lipoaspiration constituent des interventions de chirurgie esthétique qui, réalisées suivant des techniques mises au point dès les années soixante-dix et régulièrement améliorées depuis lors, ne peuvent être pratiquées que dans les conditions fixées par les articles L. 6322-1 et suivants du code de la santé publique (CSP) ainsi que par les dispositions réglementaires prises pour leur application. Au regard de cet encadrement législatif et réglementaire et alors même que de tels actes de chirurgie peuvent exceptionnellement entraîner des complications ayant des conséquences graves, le Premier ministre n'a pas commis d'erreur manifeste d'appréciation en estimant que la mise en oeuvre des actes de lipoaspiration ne présentait pas un danger grave ou une suspicion de danger grave justifiant qu'il soit fait application des dispositions de l'article L. 1151-3 du CSP pour en interdire la pratique.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
