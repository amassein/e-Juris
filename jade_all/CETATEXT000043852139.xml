<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043852139</ID>
<ANCIEN_ID>JG_L_2021_07_000000454006</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/85/21/CETATEXT000043852139.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème chambre, 22/07/2021, 454006, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-07-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>454006</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème chambre</FORMATION>
<TYPE_REC>Autres</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROCHETEAU, UZAN-SARANO</AVOCATS>
<RAPPORTEUR>Mme Dominique Agniau-Canel</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Laurent Domingo</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:454006.20210722</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              M. C... A... et M. B... D... ont demandé au tribunal administratif de Grenoble, d'une part, d'annuler le refus du maire d'Arvillard de leur communiquer l'ensemble des mails échangés avec les élus concernant les délibérations d'octobre et novembre 2016 et, d'autre part, d'enjoindre, sous astreinte, au maire d'Arvillard de leur communiquer les documents demandés.<br/>
<br/>
              Par un jugement n° 1804016 du 5 mars 2021, le tribunal administratif de Grenoble a annulé la décision refusant la communication de ces documents et a enjoint au maire d'Arvillard de communiquer dans un délai de deux mois les courriels échangés entre lui et les élus municipaux à propos des délibérations des 21 novembre 2016 et 27 novembre 2017 relatives aux microcentrales du Bens et du Jourdron, après avoir occulté les adresses de messagerie des expéditeurs et des destinataires des messages ainsi que, le cas échéant, toute autre mention susceptible de porter atteinte à l'un des secrets protégés par les dispositions des articles L. 311-5 et L. 311-6 du code des relations entre le public et l'administration. <br/>
<br/>
              Par une requête enregistrée le 28 juin 2021 au secrétariat du contentieux du Conseil d'Etat, la commune d'Arvillard demande au Conseil d'Etat qu'il soit sursis à l'exécution de ce jugement, contre lequel elle s'est pourvue en cassation sous le n° 452218.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code des relations entre le public et l'administration ;<br/>
              - le code général des collectivités territoriales ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Dominique Agniau-Canel, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Laurent Domingo, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Rocheteau, Uzan-Sarano, avocat de la Commune D'arvillard ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. Aux termes du premier alinéa de l'article R. 821-5 du code de justice administrative : " La formation de jugement peut, à la demande de l'auteur du pourvoi, ordonner qu'il soit sursis à l'exécution d'une décision juridictionnelle rendue en dernier ressort si cette décision risque d'entraîner des conséquences difficilement réparables et si les moyens invoqués paraissent, en l'état de l'instruction, sérieux et de nature à justifier, outre l'annulation de la décision juridictionnelle, l'infirmation de la solution retenue par les juges du fond ".<br/>
<br/>
              2. En premier lieu, l'exécution du jugement du 5 mars 2021 implique la communication à M. A... et M. D... de documents dont le refus de communication constitue l'objet même du litige. Cette communication, indépendamment du contenu des documents en cause, revêtirait un caractère irréversible. Il s'ensuit que la condition tenant au risque que le jugement entraîne des conséquences difficilement réparables doit être regardée comme remplie.<br/>
<br/>
              3. En second lieu, les moyens tirés de ce que le tribunal administratif de Grenoble a entaché son jugement d'erreur de droit et d'erreur de qualification juridique des faits en jugeant que les courriels échangés entre le maire et les élus municipaux à propos des délibérations relatives aux microcentrales du Bens et du Jourdron devaient être regardés comme des documents administratifs au sens de l'article L. 300-2 du code des relations entre le public et l'administration paraissent, en l'état de l'instruction, sérieux et de nature à justifier, outre l'annulation de ce jugement, l'infirmation de la solution retenue par les juges du fond.<br/>
<br/>
              4. Il résulte de ce qui précède qu'il y a lieu d'ordonner qu'il soit sursis à l'exécution du jugement du 5 mars 2021 du tribunal administratif de Grenoble jusqu'à ce qu'il ait été statué sur les conclusions du pourvoi de la commune d'Arvillard.<br/>
<br/>
              5. Il n'y a pas lieu, dans les circonstances de l'espèce, de mettre à la charge de M. A... et M. D..., la somme que la commune d'Arvillard demande au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
                                   D E C I D E :<br/>
                                   ----------------<br/>
<br/>
Article 1er : Jusqu'à ce qu'il ait été statué sur le pourvoi de la commune d'Arvillard contre le jugement du tribunal administratif de Grenoble du 5 mars 2021, il sera sursis à l'exécution de ce jugement.<br/>
Article 2 : Les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative par la commune d'Arvillard sont rejetées.<br/>
Article 3 : La présente décision sera notifiée à la commune d'Arvillard.<br/>
Copie en sera adressée à MM. Olivier A... et Mathieu D....<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
