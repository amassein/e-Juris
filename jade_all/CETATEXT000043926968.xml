<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043926968</ID>
<ANCIEN_ID>JG_L_2021_07_000000454295</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/92/69/CETATEXT000043926968.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 19/07/2021, 454295, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-07-19</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>454295</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>GALY ; SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2021:454295.20210719</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et un nouveau mémoire, enregistrés les 6 et 13 juillet 2021 au secrétariat du contentieux du Conseil d'Etat, la société Oui Energy demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative :<br/>
<br/>
              1°) de suspendre l'exécution de la délibération n° 2021-125 de la Commission de régulation de l'énergie du 6 mai 2021 portant décision sur la méthode de répartition des volumes d'accès régulé à l'électricité nucléaire historique (ARENH) en cas de dépassement du plafond prévu par la loi et de la décision de la Commission de régulation de l'énergie du 26 mai 2021 l'informant qu'aucun volume d'ARENH et de garanties de capacité ne lui serait livré sur la période de livraison commençant en juillet 2021 en application de cette délibération ; <br/>
<br/>
              2°) d'enjoindre à la Commission de régulation de l'énergie de prendre une nouvelle décision accordant à la société Oui Energy les volumes maximaux d'accès régulé à l'électricité nucléaire qu'elle a sollicités dans sa demande au titre du guichet de mai 2021 pour la période de livraison du 1er juillet 2021 au 30 juin 2022, dans un délai de dix jours à compter de la notification de l'ordonnance à intervenir sous astreinte de 1 000 euros par jour de retard ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              La société soutient que :<br/>
              - la condition d'urgence est satisfaite dès lors que la délibération de la Commission de régulation de l'énergie du 6 mai 2021 ainsi que la décision de rejet subséquente du 28 mai 2021 ont pour effet de la priver de tout accès à l'ARENH à compter du 1er juillet 2021 et de la forcer à avoir recours au marché pour acheter son électricité à un prix beaucoup plus élevé, ce qui l'expose à une perte de 14,5 millions d'euros et à une situation de cessation de paiements ; <br/>
              - il existe un doute sérieux quant à la légalité des décisions contestées ; <br/>
              - elles méconnaissent les articles L. 336-1, L. 336-9 et L. 336-8 du code de l'énergie, qui prévoient la liberté de choix du fournisseur d'électricité, l'accès transparent, équitable et non discriminatoire à l'électricité produite par les centrales nucléaires et le développement de la concurrence, dès lors que, en tirant la conséquence du dépassement du volume maximal pour la période de livraison du 1er janvier 2021 au 31 décembre 2021 sur la période de livraison du 1er juillet 2021 au 30 juin 2022, la commission de régulation de l'énergie avantage les fournisseurs qui ont obtenu de l'ARENH au titre de la période de livraison commençant au 1er janvier 2021 en privant les autres fournisseurs de tout accès à l'ARENH du 1er juillet 2021 au 31 décembre 2021 et en leur opposant un écrêtement pour les six derniers mois allant du 1er janvier 2022 au 30 juin 2022 alors qu'aucun fournisseur ayant obtenu de l'ARENH au titre de la période de livraison commençant au 1er janvier 2021 n'aura de droit à ce titre ; <br/>
              - l'indication de la Commission de régulation de l'énergie selon laquelle la société Oui Energy pourra candidater au guichet de novembre 2021 pour la période de livraison commençant le 1er janvier 2022 méconnaît l'article R. 336-2 du code de l'énergie et pose un problème de visibilité du marché, la société n'étant pas capable de se projeter sur le guichet de novembre pour une période courant jusqu'au 31 décembre 2022.<br/>
<br/>
              Par un mémoire en défense et un nouveau mémoire, enregistrés les 12 et 13 juillet 2021, la Commission de régulation de l'énergie conclut au rejet de la requête. Elle soutient que la condition d'urgence n'est pas satisfaite, et que les moyens soulevés ne sont pas fondés. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'énergie ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, la société Oui Energy, et d'autre part, la ministre de la transition écologique, Electricité de France et la Commission de régulation de l'énergie ;<br/>
<br/>
              Ont été entendus lors de l'audience publique du 13 juillet 2021, à 10 heures : <br/>
<br/>
              - Me A..., avocate au Conseil d'Etat et à la Cour de cassation, avocate de la société Oui Energy ;<br/>
<br/>
              - les représentants de la société Oui Energy ; <br/>
<br/>
              - les représentants de la Commission de régulation de l'énergie ; <br/>
<br/>
              à l'issue de laquelle le juge des référés a reporté la clôture de l'instruction au 13 juillet 2021 à 18 heures puis au 16 juillet 2021 à 14 heures.<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision ".<br/>
<br/>
              2. L'article L. 336-1 du code de l'énergie, issu de la loi du 7 décembre 2010 portant Nouvelle Organisation du Marché de l'Electricité, prévoit que : " Afin d'assurer la liberté de choix du fournisseur d'électricité tout en faisant bénéficier l'attractivité du territoire et l'ensemble des consommateurs de la compétitivité du parc électronucléaire français, un accès régulé et limité à l'électricité nucléaire historique, produite par les centrales nucléaires mentionnées à l'article L. 336-2, est ouvert, pour une période transitoire définie à l'article L. 336-8, à tous les opérateurs fournissant des consommateurs finals résidant sur le territoire métropolitain continental ou des gestionnaires de réseaux pour leurs pertes. / Cet accès régulé est consenti à des conditions économiques équivalentes à celles résultant pour Electricité de France de l'utilisation de ses centrales nucléaires mentionnées au même article L. 336-2 ".<br/>
<br/>
              3. L'article L. 336-2 du même code prévoit ainsi que, jusqu'au 31 décembre 2025, Electricité de France cède de l'électricité aux fournisseurs qui en font la demande, pour un volume maximal déterminé, pour chaque opérateur, en application des dispositions des articles L. 336-3 et L. 336-4 et dans les conditions définies à l'article L. 336-5 du même code. En application du deuxième alinéa de cet article, " Le volume global maximal d'électricité nucléaire historique pouvant être cédé est déterminé par arrêté des ministres chargés de l'économie et de l'énergie pris après avis de la Commission de régulation de l'énergie, en fonction notamment du développement de la concurrence sur les marchés de la production d'électricité et de la fourniture de celle-ci à des consommateurs finals et dans l'objectif de contribuer à la stabilité des prix pour le consommateur final. Ce volume global maximal, qui demeure strictement proportionné aux objectifs poursuivis, ne peut excéder 100 térawattheures par an jusqu'au 31 décembre 2019 et 150 térawattheures par an à compter du 1er janvier 2020 ". Ce volume global maximal reste néanmoins fixé à 100 TWh par an en application d'un arrêté du 28 avril 2011.<br/>
<br/>
              4. L'article L. 336-3 du même code prévoit que le volume maximal cédé à un fournisseur est calculé pour une année par la Commission de régulation de l'électricité en fonction des prévisions, fournies par les entreprises intéressées, de consommation des consommateurs finals et des gestionnaires de réseaux pour leurs pertes et en fonction de ce que représente la part de la production des centrales nucléaires dans la consommation totale des consommateurs finals. En application des deuxième et troisième alinéas de cet article : " Si la somme des volumes maximaux définis à l'alinéa précédent pour chacun des fournisseurs excède le volume global maximal fixé en application de l'article L. 336-2, la Commission de régulation de l'énergie répartit ce dernier entre les fournisseurs de manière à permettre le développement de la concurrence sur l'ensemble des segments du marché de détail. / La Commission de régulation de l'énergie fixe, selon une périodicité infra-annuelle, le volume cédé à chaque fournisseur et le lui notifie (...) ".<br/>
<br/>
              5. L'article L. 336-5 du même code prévoit que le fournisseur souhaitant exercer les droits qui découlent du mécanisme d'accès régulé à l'électricité nucléaire historique (ARENH) conclut avec EDF, dans le délai d'un mois suivant sa demande, un accord-cadre déterminant les modalités d'exercice de ces droits " par la voie de cessions d'une durée d'un an " et dont les stipulations sont conformes à un modèle déterminé par arrêté du ministre chargé de l'énergie pris sur proposition de la CRE.<br/>
<br/>
              6. Aux termes de l'article L. 336-9 du même code : " Afin de garantir un accès transparent, équitable et non discriminatoire à l'électricité produite par les centrales nucléaires mentionnées à l'article L. 336-2, pour les fournisseurs d'électricité, y compris le propriétaire de ces centrales, la Commission de régulation de l'énergie propose les prix, calcule les droits et contrôle l'accès régulé à l'électricité nucléaire historique prévu par l'article L. 336-1 (... )".<br/>
<br/>
              7. L'article R. 336-1 du même code prévoit que " l'électricité est cédée par la société EDF aux fournisseurs d'électricité autorisés sous la forme de produits livrés par périodes d'une durée d'un an, caractérisés par une quantité et un profil ". Aux termes de l'article R. 336-2 du même code : " Les périodes de livraison commencent le 1er janvier et le 1er juillet. La quantité d'un produit, exprimée en mégawatts, est la puissance moyenne d'électricité délivrée pendant la période de livraison de ce produit. ". Aux termes de l'article R. 336-3 du même code : " Le profil du produit est la chronique demi-heure par demi-heure de la puissance délivrée pendant la période de livraison ". L'article R. 336-10 prévoit que la transmission d'un dossier de demande d'ARENH à la Commission de régulation de l'énergie vaut engagement ferme de la part du fournisseur d'acheter les quantités totales de produit qui lui seront cédées au cours de la période de livraison en application de la décision de la Commission de régulation de l'énergie. <br/>
<br/>
              8. L'article R. 336-18 du même code prévoit que lorsque la somme totale des quantités de produit maximales pouvant être cédées aux fournisseurs d'électricité dépasse le plafond de 100 TWh, les quantités de produit cédées aux fournisseurs sont recalculées de telle sorte que la somme des quantités de produit cédées soit égale au plafond à partir d'une méthode de répartition du plafond répartie par la Commission de régulation de l'électricité.<br/>
<br/>
              9. Il résulte de l'instruction que les demandes d'ARENH des fournisseurs d'électricité ont dépassé le plafond de 100 TWh au titre de la période de livraison commençant le 1er janvier 2021. Ces demandes ont fait l'objet d'un écrêtement pour atteindre le plafond de 100 TWh. Par une délibération du 6 mai 2021, applicable aux demandes pouvant être présentées au titre de la période de livraison commençant le 1er juillet 2021, la Commission de régulation de l'électricité a décidé, comme les années précédentes, que les livraisons correspondant aux demandes d'ARENH pour la période de livraison commençant en janvier 2021 ne seraient pas écrêtées. Ainsi, tout fournisseur n'ayant pas formulé de nouvelle demande d'ARENH pour la période de livraison commençant en juillet 2021 a conservé l'intégralité des quantités d'ARENH obtenues à compter du 1er janvier 2021 jusqu'à la fin de l'année 2021. La société Oui Energy n'a pu présenter de demande d'ARENH au titre de la période de livraison commençant en janvier 2021. Le plafond de 100 TwH étant atteint jusqu'en décembre 2021, la Commission de régulation de l'électricité lui a notifiée, le 28 mai 2021, qu'aucun volume d'ARENH ne lui serait livré sur la période de livraison commençant en juillet 2021. La société Oui Energy demande au juge des référés du Conseil d'Etat, sur le fondement de l'article L. 521-1 du code de justice administrative, de suspendre l'exécution de la délibération de la Commission de la régulation de l'électricité du 6 mai 2021 et de la décision subséquente du 28 mai 2021. <br/>
<br/>
              10. La société requérante soutient que la délibération de la Commission de régulation de l'énergie méconnaît les dispositions des articles L. 336-1, L. 336-3 et L. 336-9, citées aux points 2, 4 et 6, qui prévoient des objectifs de liberté de choix du fournisseur d'électricité, d'accès transparent, équitable et non discriminatoire à l'électricité produite pas les centrales nucléaires et de développement de la concurrence. Toutefois, d'une part, les objectifs énoncés à ces articles ne sauraient avoir pour effet de permettre à la Commission de régulation de remettre en cause les cessions accordées du 1er janvier au 31 décembre 2021 qui ont donné lieu à un engagement ferme d'achat de la part du fournisseur, en application des dispositions citées au point 7. D'autre part, il résulte des dispositions citées aux points 4, 5 et 7 que les cessions d'électricité en application du dispositif d'ARENH reposent sur des livraisons d'électricité pour une quantité et un profil déterminés sur une période d'un an. Elles ne permettent donc pas de prévoir une cession d'électricité au titre de la période de livraison allant du 1er juillet 2021 et 30 juin 2022 qui ne commencerait qu'en janvier 2022, date à laquelle le plafond de 100 TWh ne sera plus impacté par les décisions prises par la Commission de régulation au titre de la période de livraison allant du 1er janvier au 31 décembre 2021. Il sera au demeurant loisible à la société de soumettre une demande au titre de la période de livraison allant du 1er janvier au 31 décembre 2022. Il s'ensuit que le moyen tiré de ce que les décisions litigieuses méconnaîtraient les dispositions des articles L. 336-1, L. 336-3 et L. 336-9 ne paraît pas, en l'état de l'instruction, de nature à créer un doute sérieux quant à leur légalité. <br/>
<br/>
              11. Il résulte de ce qui précède que, sans qu'il soit besoin de se prononcer sur la condition d'urgence, la requête de la société Oui Energy doit être rejetée, y compris ses conclusions aux fins d'injonction et ses conclusions tendant à l'application de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
              ------------------<br/>
<br/>
Article 1er : La requête de la société Oui Energy est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à la société Oui Energy et à la Commission de régulation de l'énergie.<br/>
Copie en sera adressée à la ministre de la transition écologique et à Electricité de France.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
