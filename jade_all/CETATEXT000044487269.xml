<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044487269</ID>
<ANCIEN_ID>JG_L_2021_12_000000459010</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/48/72/CETATEXT000044487269.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 09/12/2021, 459010, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-12-09</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>459010</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2021:459010.20211209</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, enregistrée le 30 novembre 2021 au secrétariat du contentieux du Conseil d'Etat, M. B... A... demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative :<br/>
<br/>
              1°) d'enjoindre au Conseil supérieur de l'audiovisuel de prendre sans délai une délibération fixant provisoirement les règles d'élaboration des sondages par les chaînes de radio et de télévision se trouvant sous son contrôle, dans l'attente de la décision au fond à intervenir, afin de préciser que les sondages doivent inclure tous les candidats publiquement déclarés à l'élection, dans l'ordre alphabétique, et que les sondages ne peuvent pas inclure des personnes qui n'ont pas publiquement déclaré leur candidature à l'élection, et ce sous astreinte de 10 000 euros par jour de retard ;  <br/>
<br/>
              2°) d'enjoindre au Conseil supérieur de l'audiovisuel, dans l'attente de la décision au fond à intervenir, d'assurer provisoirement un temps minimal d'expression de tous les candidats publiquement déclarés à la prochaine élection présidentielle dans les médias se trouvant sous son contrôle à des heures de grande audience, soit quinze minutes par semaine ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 1 800 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Il soutient que :<br/>
              - la condition d'urgence est satisfaite dès lors que la période de pré-campagne électorale est déjà ouverte et que chaque jour qui passe sans que les règles sur les sondages ne soient précisées favorise un peu plus l'infidélité du prochain scrutin présidentiel, tout en laissant perdurer une situation contraire aux droits fondamentaux des candidats à l'élection présidentielle ; <br/>
              - en ne précisant pas, dans sa délibération n° 2017-62 du 22 novembre 2017 relative au principe de pluralisme politique dans les services de radio et de télévision, les critères de validité des sondages d'opinion susceptibles d'être pris en compte pour apprécier le caractère équitable des temps d'intervention des candidats au regard de leur représentativité, le Conseil supérieur de l'audiovisuel ne garantit pas le respect, par les services de radio et de télévision, de leurs obligations en matière de pluralisme politique, découlant de l'article 11 de la Déclaration des droits de l'homme et du citoyen du 26 août 1789, de l'article 10 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales et des articles 1er, 3-1 et 13 de la loi du 30 septembre 1986 relative à la liberté de communication ;<br/>
              - l'inaction du Conseil supérieur de l'audiovisuel à compléter sa délibération, entachée d'incompétence négative, crée une grave rupture d'égalité entre les candidats et porte une atteinte grave aux libertés politiques.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la Constitution, notamment son Préambule et ses titres Ier et II ; <br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - la loi n° 86-1067 du 30 septembre 1986 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1.  Aux termes de l'article L. 521-2 du même code : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. (...) ". En vertu de l'article L. 522-3 du même code, le juge des référés peut, par une ordonnance motivée, rejeter une requête sans instruction ni audience lorsque la condition d'urgence n'est pas remplie ou lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable ou qu'elle est mal fondée.<br/>
<br/>
              2. Le requérant qui saisit le juge des référés sur le fondement des dispositions de l'article L. 521-2 du code de justice administrative doit justifier des circonstances particulières caractérisant la nécessité pour lui de bénéficier à très bref délai d'une mesure de la nature de celles qui peuvent être ordonnées sur le fondement de cet article.<br/>
<br/>
              3. M. A..., qui a fait connaître sa volonté de se porter candidat à la prochaine élection présidentielle, a demandé au Conseil supérieur de l'audiovisuel de modifier sa délibération du 22 novembre 2017 relative au principe de pluralisme politique dans les services de radio et de télévision, afin d'y préciser les règles d'élaboration des sondages d'opinion susceptibles d'être pris en compte pour apprécier le caractère équitable des temps d'intervention des candidats au regard de leur représentativité. Dans l'attente de la décision au fond sur le recours pour excès de pouvoir qu'il a introduit contre la décision implicite de rejet qui serait née du silence sur sa demande, il demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, d'enjoindre au Conseil supérieur de l'audiovisuel de prendre une délibération fixant provisoirement de telles règles et d'assurer provisoirement un temps minimal de quinze minutes par semaine d'expression de tous les candidats publiquement déclarés à la prochaine élection présidentielle dans les médias se trouvant sous son contrôle à des heures de grande audience.<br/>
<br/>
              4. Pour justifier de l'urgence à prononcer les mesures demandées, le requérant se borne à faire valoir, de façon générale et comme il l'avait fait au soutien d'une précédente demande ayant le même objet, présentée sur le fondement de l'article L. 521-3 du code de justice administrative, que l'absence de précision par le Conseil supérieur de l'audiovisuel quant aux règles d'élaboration des sondages porte chaque jour davantage atteinte à la sincérité du prochain scrutin en vue de l'élection du Président de la République et aux droits fondamentaux des candidats à cette élection. Ces seuls éléments, à quatre mois du scrutin et alors que la délibération critiquée comme entachée d'incompétence négative est en vigueur depuis 2017 et que les règles propres à la prochaine élection présidentielle sont fixées par la délibération du Conseil supérieur de l'audiovisuel n° 2011-1 du 4 janvier 2011 relative au principe de pluralisme politique dans les services de radio et de télévision en période électorale, complétée par la recommandation n° 2021-03 du 6 octobre 2021 aux services de communication audiovisuelle en vue de l'élection du Président de la République, ne suffisent pas à caractériser l'existence d'une situation d'urgence particulière justifiant l'intervention du juge des référés dans le très bref délai prévu par les dispositions de l'article L. 521-2 du code de justice administrative. <br/>
<br/>
              5. Il résulte de tout ce qui précède que la requête de M. A... doit être rejetée, y compris ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative, selon la procédure prévue à l'article L. 522-3 du code de justice administrative.<br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
              ------------------<br/>
<br/>
Article 1er : La requête de M. A... est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à M. B... A....<br/>
Copie en sera adressée au Conseil supérieur de l'audiovisuel.<br/>
Fait à Paris, le 9 décembre 2021<br/>
Signé : Anne Courrèges<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
