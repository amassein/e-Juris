<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030064084</ID>
<ANCIEN_ID>JG_L_2015_01_000000369924</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/06/40/CETATEXT000030064084.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère sous-section jugeant seule, 07/01/2015, 369924, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-01-07</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>369924</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère sous-section jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Laurent Cytermann</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Maud Vialettes</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:369924.20150107</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une ordonnance n° 1102022 du 25 juin 2013, enregistrée le 4 juillet 2013 au secrétariat du contentieux du Conseil d'Etat, le président du tribunal administratif de Pau a transmis au Conseil d'Etat, en application de l'article R. 351-2 du code de justice administrative, la requête, enregistrée le 21 septembre 2011 au greffe de ce tribunal, présentée par la Fédération des entreprises de boulangeries et pâtisseries françaises.<br/>
<br/>
              Par cette requête, la Fédération des entreprises de boulangeries et pâtisseries françaises demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision implicite de rejet née du silence gardé par le ministre du travail et de la solidarité sur sa demande du 28 juin 2010 tendant à l'abrogation de l'arrêté du préfet des Pyrénées-Atlantiques du 22 décembre 1993 relatif à la fermeture hebdomadaire des boulangeries de ce département ; <br/>
<br/>
              2°) d'enjoindre au ministre chargé du travail de procéder à un réexamen de sa demande dans un délai de trois mois ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
              Vu les autres pièces du dossier.<br/>
<br/>
              Vu :<br/>
              - le code du travail ;<br/>
              - le code de justice administrative.<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Laurent Cytermann, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Maud Vialettes, rapporteur public.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 3132-29 du code du travail, reprenant les dispositions du premier alinéa de l'ancien article L. 221-17 du code du travail : " Lorsqu'un accord est intervenu entre les organisations syndicales de salariés et les organisations d'employeurs d'une profession et d'une zone géographique déterminées sur les conditions dans lesquelles le repos hebdomadaire est donné aux salariés, le préfet peut, par arrêté, sur la demande des syndicats intéressés, ordonner la fermeture au public des établissements de la profession ou de la zone géographique concernée pendant toute la durée de ce repos (...) ". Pour l'application de ces dispositions, la fermeture au public des établissements d'une profession ne peut légalement être ordonnée sur la base d'un accord syndical que dans la mesure où cet accord correspond pour la profession à la volonté de la majorité indiscutable de tous ceux qui exercent cette profession à titre principal ou accessoire et dont l'établissement ou partie de celui-ci est susceptible d'être fermé. Aux termes de l'article R. 3132-22 du même code : " Lorsqu'un arrêté préfectoral de fermeture au public, pris en application de l'article L. 3132-29, concerne des établissements concourant d'une façon directe à l'approvisionnement de la population en denrées alimentaires, il peut être abrogé ou modifié par le ministre chargé du travail après consultation des organisations professionnelles intéressées. / Cette décision ne peut intervenir qu'après l'expiration d'un délai de six mois à compter de la mise en application de l'arrêté préfectoral ".<br/>
<br/>
              2. A la suite de l'accord intervenu le 8 décembre 1993 entre certains syndicats d'employeurs et de travailleurs concernés, le préfet des Pyrénées-Atlantiques a, par arrêté du 22 décembre 1993, prescrit la fermeture un jour par semaine des établissements, parties d'établissements, magasins, dépôts ou locaux de quelque nature qu'ils soient, couverts ou découverts, sédentaires ou ambulants, dans lesquels s'effectue la vente ou la distribution des produits de boulangerie, pâtisserie, viennoiserie et dérivés de ces activités. La fédération requérante demande l'annulation pour excès de pouvoir de la décision implicite par laquelle le ministre chargé du travail a rejeté sa demande du 28 juin 2010 tendant à l'abrogation de l'arrêté du préfet des Pyrénées-Atlantiques du 22 décembre 1993.<br/>
<br/>
              3. L'autorité compétente, saisie d'une demande tendant à l'abrogation d'un règlement illégal, est tenue d'y déférer, soit que ce règlement ait été illégal dès sa signature, soit que l'illégalité résulte de circonstances de droit ou de fait postérieures à cette date.<br/>
<br/>
              4. En premier lieu, si, en vertu des dispositions de l'article L. 221-17, reprises à l'article L. 3132-29 du code du travail, le préfet doit vérifier l'existence d'un accord entre une ou plusieurs organisations d'employeurs et une ou plusieurs organisations de salariés, résultant d'échanges et de discussions menés simultanément et collectivement entre les parties, les conditions de négociation de cet accord, lequel ne doit d'ailleurs pas nécessairement prendre la forme d'un document écrit et signé dans les conditions alors prévues au titre III du livre I du code du travail, sont par elles-mêmes sans incidence sur la légalité de l'arrêté. En l'espèce, il ressort des pièces du dossier que l'accord du 8 décembre 1993 a été précédé de deux réunions organisées les 12 février et 8 décembre 1993 auxquelles l'ensemble des organisations intéressées ont été conviées. Par suite, la fédération requérante n'est pas fondée à soutenir que l'accord n'aurait pas été précédé de négociations entre les représentants des différentes professions concernées et que l'arrêté aurait été pris à la suite d'une simple " consultation " des organisations intéressées. Par ailleurs, la circonstance que cet accord ne fasse pas mention de telles négociations est sans incidence sur la légalité de l'arrêté du 22 décembre 1993.<br/>
<br/>
              5. En deuxième lieu, si, ainsi qu'il a été dit au point 1, l'arrêté préfectoral de fermeture hebdomadaire doit être précédé d'un accord entre des organisations syndicales intéressées du département qui corresponde à la volonté d'une majorité indiscutable de tous ceux qui exercent la profession à titre principal ou accessoire et dont l'établissement ou partie de celui-ci est susceptible d'être fermé, ces dispositions n'imposent pas que les organisations signataires de l'accord soient elles-mêmes représentatives de cette majorité. Par suite, la circonstance que ni la Fédération des entreprises de boulangeries et pâtisseries françaises ni d'autres organisations représentatives des professionnels de la boulangerie industrielle n'aient signé l'accord du 8 décembre 1993 est sans incidence sur la légalité de l'arrêté du 22 décembre 1993. De même, la circonstance que le préfet n'ait pas consulté la Fédération des entreprises de boulangeries et pâtisseries françaises n'est pas, en tout état de cause, de nature à établir par elle-même que l'accord du 8 décembre 1993 n'aurait pas exprimé, à la date de cet arrêté, la volonté de la majorité indiscutable des établissements concernés. <br/>
<br/>
              6. En troisième lieu, d'une part, il ne ressort ni des éléments allégués par la fédération requérante, qui se borne à soutenir que l'arrêté litigieux a été pris sur la base du seul accord des syndicats représentant la profession de boulanger artisan, ni des autres pièces du dossier que l'accord du 8 décembre 1993 conclu entre la fédération départementale de la boulangerie-pâtisserie des Pyrénées-Atlantiques et l'union départementale Force ouvrière et l'union départementale CFTC, qui sont, contrairement à ce qui est soutenu, nommément désignées dans cet accord, n'aurait pas reflété l'opinion de la majorité indiscutable des établissements concernés. En particulier, l'affirmation de la fédération requérante selon laquelle la fédération départementale de la boulangerie-pâtisserie des Pyrénées-Atlantiques n'aurait pas eu un caractère représentatif n'est assortie d'aucun élément permettant d'en apprécier la pertinence. <br/>
<br/>
              7. D'autre part, si la fédération requérante soutient que les boulangeries artisanales sont aujourd'hui minoritaires dans le département, cette circonstance, à la supposer exacte, n'est pas par elle-même de nature à établir que se serait produit dans l'opinion d'un nombre important des établissements intéressés un changement susceptible de modifier la volonté de la majorité d'entre eux. Au demeurant, il ressort des pièces du dossier que le préfet des Pyrénées-Atlantiques a procédé, en février 2003, à une nouvelle consultation des organisations professionnelles intéressées, à laquelle la fédération requérante a été conviée mais n'a pas participé, qui a permis d'établir que l'arrêté litigieux correspondait encore à la volonté de la majorité indiscutable des établissements concernés dans le département. Il ne ressort pas des pièces du dossier que se serait produit depuis lors, dans l'opinion d'un nombre important des commerçants intéressés, un changement susceptible de modifier la volonté de la majorité d'entre eux. Par suite, le moyen tiré de ce qu'il n'est pas établi que l'accord au vu duquel l'arrêté du 22 décembre 1993 a été pris corresponde à une majorité indiscutable des établissements concernés à la date de cet arrêté et à la date de la décision implicite du ministre doit être écarté.<br/>
<br/>
              8. Il résulte de tout ce qui précède que la Fédération des entreprises de boulangeries et pâtisseries françaises n'est pas fondée à demander l'annulation de la décision implicite par laquelle le ministre chargé du travail a rejeté sa demande du 28 juin 2010 tendant à l'abrogation de l'arrêté du préfet des Pyrénées-Atlantiques du 22 décembre 1993. Par suite, ses conclusions tendant à ce qu'il soit enjoint au ministre de procéder à un réexamen de sa demande ainsi que celles présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative ne peuvent qu'être également rejetées. <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de la Fédération des entreprises de boulangeries et pâtisseries françaises est rejetée. <br/>
Article 2 : La présente décision sera notifiée à la Fédération des entreprises de boulangeries et pâtisseries françaises et au ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
