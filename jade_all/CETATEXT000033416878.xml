<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033416878</ID>
<ANCIEN_ID>JG_L_2016_11_000000398262</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/41/68/CETATEXT000033416878.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème - 3ème chambres réunies, 16/11/2016, 398262</TITRE>
<DATE_DEC>2016-11-16</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>398262</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème - 3ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Manon Perrière</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Benoît Bohnert</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2016:398262.20161116</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. F..., MmeG..., M.B..., MmeR..., Mme L...et M. P... ont demandé au tribunal administratif de Versailles, à titre principal, de suspendre, pour ce qui concerne la commune de Coignières, l'exécution de l'arrêté du 16 décembre 2015 par lequel le préfet de la région d'Ile-de-France a fixé le nombre et la répartition des sièges au sein du conseil communautaire de la communauté d'agglomération de Saint-Quentin-en-Yvelines jusqu'à ce que le Conseil constitutionnel ait statué sur les questions prioritaires de constitutionnalité soulevées et, à titre subsidiaire, d'annuler le résultat des opérations électorales qui se sont déroulées le 19 décembre 2015 pour la désignation des conseillers communautaires au sein de ce nouvel établissement public de coopération intercommunale. <br/>
<br/>
              Par un jugement n° 1508478 du 23 février 2016, le tribunal administratif a rejeté leur protestation.<br/>
<br/>
              Par une requête, enregistrée le 24 mars 2016 au secrétariat du contentieux du Conseil d'Etat, M. F..., MmeG..., M.B..., MmeR..., Mme L...et M. P...demandent au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler ce jugement en tant qu'il a rejeté leur protestation relative à la désignation des conseillers communautaires qui s'est déroulée le 19 décembre 2015 et refusé de transmettre au Conseil constitutionnel la question de la conformité du c) du 1° de l'article L. 5211-6-2 du code général des collectivités territoriales aux droits et libertés garantis par la Constitution ; <br/>
<br/>
              2°) de faire droit à leur protestation ;<br/>
<br/>
              3°) d'enjoindre au préfet des Yvelines de procéder à la réinstallation des trois conseillers communautaires de Coignières dont les mandats ont été supprimés du fait de la tenue de ce scrutin ;<br/>
<br/>
              4°) à titre subsidiaire, de proclamer élu M. I...après avoir annulé l'élection de M.K... ;<br/>
<br/>
              5°) de mettre à la charge de M. K...et de Mme H...la somme de 1 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la Constitution ;<br/>
              - l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
              - le code électoral ; <br/>
              - le code général des collectivités territoriales ;<br/>
              - la loi n° 2014-58 du 27 janvier 2014 modifiée par la loi n° 2015-991 du 7 août 2015 ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Manon Perrière, auditeur,  <br/>
              - les conclusions de M. Benoît Bohnert, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant que, par la loi du 27 janvier 2014 de modernisation de l'action publique territoriale et d'affirmation des métropoles, le législateur a souhaité rationaliser l'organisation des structures intercommunales, notamment en Ile-de-France, en regroupant des établissements publics de coopération intercommunale (EPCI) au sein de structures au périmètre plus étendu et dotées de compétences renforcées ; que, dans ce cadre, le préfet de la région d'Ile-de-France a arrêté, le 4 mars 2015, le schéma régional de coopération intercommunale ; que, par arrêté du 18 mai 2015, le préfet des Yvelines a défini le projet de périmètre de fusion de la communauté d'agglomération de Saint-Quentin-en-Yvelines et de la communauté de communes de l'Ouest Parisien, étendu aux communes de Coignières et Maurepas ; que, par différentes délibérations intervenues entre le 23 novembre et le 11 décembre 2015, les conseils municipaux des communes de Montigny-le-Bretonneux, de Plaisir, de Villepreux, de Voisins-le-Bretonneux, des Clayes-sous-Bois, d'Elancourt, de Coignières et de Maurepas ont donné leur accord en ce qui concerne le nombre et la répartition des sièges au sein du nouvel EPCI ; que, par arrêté du 16 décembre 2015, le préfet de la région d'Ile-de-France a fixé le nombre et la répartition des sièges au sein du conseil communautaire du nouvel EPCI de Saint-Quentin-en-Yvelines à compter du 1er janvier 2016 ; <br/>
<br/>
              2. Considérant qu'il résulte de l'instruction que, le 18 décembre 2015, le conseil municipal de Coignières s'est réuni pour procéder à l'élection de deux conseillers communautaires ; que deux listes ont été présentées au suffrage des conseillers municipaux, une première liste composée de M. K...et de MmeH..., une seconde liste composée de M. I... et de MmeH... ; qu'à l'issue du scrutin qui s'est tenu le 19 décembre 2015, la liste de M. K... et MmeH... a remporté vingt suffrages et la liste de M. I...et Mme H... un seul suffrage ; que M. F..., MmeG..., M.B..., Mme R..., Mme L...et M.P..., conseillers municipaux de la commune de Coignières, ont contesté la légalité de cette désignation ; qu'ils interjettent appel du jugement du 23 février 2016 en tant que, par ce jugement, le tribunal administratif de Versailles a refusé de transmettre la question prioritaire de constitutionnalité portant sur le c) du 1° de l'article L. 5211-6-2 du code général des collectivités territoriales et rejeté leur protestation ; qu'en outre, par un mémoire distinct enregistré le 12 septembre 2016, ils demandent au Conseil d'Etat de transmettre au Conseil constitutionnel la question de la conformité aux droits et libertés garantis par la Constitution de ces mêmes dispositions ;<br/>
<br/>
              Sur les interventions de M.I... :<br/>
<br/>
              3. Considérant que M. I...doit être regardé, en l'état du dossier, comme justifiant, en sa qualité de candidat non élu à l'issue du scrutin qui s'est déroulé le 19 décembre 2015, d'un intérêt lui donnant qualité pour intervenir au soutien de la requête de M. F..., Mme G..., M.B..., MmeR..., Mme L...et M. P...; que, dès lors, son intervention au soutien de la question prioritaire de constitutionnalité soulevée par ces mêmes personnes à l'appui de cette requête, présentée par un mémoire distinct, doit être admise pour l'examen de cette question prioritaire de constitutionnalité ; <br/>
<br/>
              Sur la question prioritaire de constitutionnalité :<br/>
<br/>
              4. Considérant qu'aux termes de l'article 23-1 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel : " Devant les juridictions relevant du Conseil d'Etat ou de la Cour de cassation, le moyen tiré de ce qu'une disposition législative porte atteinte aux droits et libertés garantis par la Constitution est, à peine d'irrecevabilité, présenté dans un écrit distinct et motivé " ; qu'aux termes de l'article 23-2 de la même ordonnance : " (...) Le refus de transmettre la question ne peut être contesté qu'à l'occasion d'un recours contre la décision réglant tout ou partie du litige " ; qu'aux termes du premier alinéa de l'article R. 771-16 du code de justice administrative : " Lorsque l'une des parties entend contester devant le Conseil d'Etat, à l'appui d'un appel ou d'un pourvoi en cassation formé contre la décision qui règle tout ou partie du litige, le refus de transmission d'une question prioritaire de constitutionnalité précédemment opposé, il lui appartient, à peine d'irrecevabilité, de présenter cette contestation avant l'expiration du délai de recours dans un mémoire distinct et motivé, accompagné d'une copie de la décision de refus de transmission " ;<br/>
<br/>
              5. Considérant qu'il résulte de ces dispositions que, lorsqu'un tribunal administratif a refusé de transmettre au Conseil d'Etat la question prioritaire de constitutionnalité qui lui a été soumise, il appartient à l'auteur de cette question de contester ce refus, à l'occasion de l'appel formé contre le jugement qui statue sur le litige, dans le délai de recours contentieux et par un mémoire distinct et motivé, que le refus de transmission précédemment opposé l'ait été par une décision distincte du jugement, dont il joint alors une copie, ou directement par ce jugement ; que les dispositions de l'article 23-5 de l'ordonnance du 7 novembre 1958 n'ont ni pour objet ni pour effet de permettre à celui qui a déjà présenté une question prioritaire de constitutionnalité devant une juridiction statuant en dernier ressort de s'affranchir des conditions, définies par les dispositions citées plus haut de la loi organique et du code de justice administrative, selon lesquelles le refus de transmission peut être contesté devant le juge d'appel ; <br/>
<br/>
              6. Considérant qu'il résulte de l'instruction que les requérants soutenaient, devant le tribunal administratif, que l'arrêté préfectoral du 16 décembre 2015 fixant le nombre et la répartition des sièges au sein du conseil communautaire du nouvel EPCI de Saint-Quentin-en-Yvelines avait été pris sur le fondement du c) du 1° de l'article L. 5211-6-2 du code général des collectivités territoriales et était illégal en tant que ces dispositions étaient contraires à la Constitution ; que, pour demander au tribunal administratif de renvoyer au Conseil d'Etat la question de la conformité aux droits et libertés garantis par la Constitution de ces dispositions, les requérants soutenaient qu'elles étaient inconstitutionnelles en ce qu'elles réduisaient le nombre de conseillers communautaires et supprimaient des mandats de délégués régulièrement élus au suffrage universel et méconnaissaient ainsi les dispositions de l'article 72 de la Constitution, qui font du principe électif l'une des conditions essentielles de la décentralisation ; que le tribunal administratif a jugé qu'il n'y avait pas lieu de renvoyer cette question prioritaire de constitutionnalité au Conseil d'Etat dès lors que les dispositions contestées ne pouvaient être regardées comme étant applicables au litige, l'arrêté préfectoral n'ayant pas été pris pour leur  application ; <br/>
<br/>
              7. Considérant que si les requérants formulent en appel par mémoire distinct une question prioritaire de constitutionnalité portant sur la conformité des mêmes dispositions législatives aux mêmes droits et libertés garantis par la Constitution, en soutenant désormais qu'elles constituent le fondement légal des opérations électorales et non celui de l'arrêté préfectoral du 16 décembre 2015, il ne peut être fait droit à leur demande dès lors qu'il leur appartenait de soulever ce moyen dans le cadre d'une contestation en appel du refus de transmission par le tribunal administratif ; <br/>
<br/>
              Sur la contestation du refus de transmission des questions prioritaires de constitutionnalité :<br/>
<br/>
              8. Considérant qu'aux termes de l'article R. 771-12 du code de justice administrative : " Lorsque, en application du dernier alinéa de l'article 23-2 de l'ordonnance n° 58-1067 du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel, l'une des parties entend contester, à l'appui d'un appel formé contre la décision qui règle tout ou partie du litige, le refus de transmission d'une question prioritaire de constitutionnalité opposé par le premier juge, il lui appartient, à peine d'irrecevabilité, de présenter cette contestation avant l'expiration du délai d'appel dans un mémoire distinct et motivé, accompagné d'une copie de la décision de refus de transmission./ La contestation du refus de transmission par la voie du recours incident doit, de même, faire l'objet d'un mémoire distinct et motivé, accompagné d'une copie de la décision de refus de transmission " ; que, faute d'avoir été présentée par un mémoire distinct dans le délai d'appel, ainsi qu'en dispose cet article, cette contestation est irrecevable ; <br/>
<br/>
<br/>
              Sur la régularité du jugement attaqué :<br/>
<br/>
              9. Considérant qu'il résulte de l'instruction qu'un avis d'audience du 29 décembre 2015 a averti les parties de ce que l'audience était fixée au 11 février 2016 ; que le tribunal administratif a adressé aux défendeurs, le 9 février 2016, l'intervention de M. I...ainsi qu'un mémoire en réplique de M. F...et trois mémoires distincts présentés par ce dernier soulevant des questions prioritaires de constitutionnalité ; que les défendeurs ont produit un nouveau mémoire en défense le 10 février 2016 ; que s'il a été communiqué par voie postale aux requérants le même jour, il résulte des énonciations de la " fiche requête " du tribunal administratif que ce mémoire en défense a été également remis le jour de l'audience aux requérants ; qu'il résulte en outre des dispositions combinées de l'article R. 773-1 du code de justice administrative et des articles R. 119 et R. 120 du code électoral que, par dérogation aux prescriptions de  l'article R. 611-1 du code de justice administrative, le tribunal administratif n'est pas tenu d'ordonner la communication des mémoires en défense aux auteurs de la protestation ; que, par conséquent, la circonstance que les requérants n'auraient pas été en mesure de répondre aux mémoires en défense produits le 10 février qui ne leur seraient parvenus par voie postale qu'après la date de l'audience et qu'ils n'auraient été avertis qu'après l'audience de la réouverture antérieure de l'instruction n'est pas de nature à entacher d'irrégularité la procédure à l'issue de laquelle le jugement attaqué a été rendu ; que le moyen tiré de ce qu'une erreur aurait été commise dans le délai de réponse donné aux requérants pour répondre au dernier mémoire en défense n'est, en tout état de cause, pas assorti des précisions permettant d'en apprécier le bien-fondé ;<br/>
<br/>
              Sur le bien-fondé du jugement attaqué :<br/>
<br/>
              En ce qui concerne le moyen relatif à l'illégalité de la délibération du 19 décembre 2015 constatant le résultat des élections :<br/>
<br/>
              10. Considérant que les requérants soutiennent que la délibération du 19 décembre 2015 constatant le résultat des élections serait illégale du fait de l'illégalité de l'arrêté du préfet de la région d'Ile-de-France du 16 décembre 2015 fixant le nombre et la répartition des sièges au sein du conseil communautaire et se prévalent à nouveau par voie d'exception, concernant ce dernier arrêté, de l'illégalité, d'une part, de l'arrêté du préfet de la région d'Ile-de-France du 4 mars 2015 adoptant le schéma régional de coopération intercommunale et, d'autre part, de l'arrêté du préfet du département des Yvelines en date du 18 mai 2015 définissant le projet de périmètre de fusion de la communauté d'agglomération de Saint-Quentin-en-Yvelines et de la communauté de communes de l'Ouest Parisien étendu aux communes de Maurepas et de Coignières ;<br/>
<br/>
              11. Considérant qu'aux termes du second alinéa du VI de l'article 11 de la loi du 27 janvier 2014 de modernisation de l'action publique territoriale et d'affirmation des métropoles : " Le représentant de l'Etat dans la région constate la composition de l'organe délibérant de l'établissement public de coopération intercommunale à fiscalité propre fixée selon les modalités prévues au premier alinéa du présent VI. A défaut de délibération des conseils municipaux dans le délai prévu au même alinéa, la composition de l'organe délibérant est arrêtée par le représentant de l'Etat dans la région, selon les modalités prévues aux II et III de l'article L. 5211-6-1 du code général des collectivités territoriales. " ; <br/>
<br/>
              12. Considérant que l'illégalité d'un acte administratif, qu'il soit ou non réglementaire, ne peut être utilement invoquée à l'appui de conclusions dirigées contre une décision administrative que si cette dernière a été prise pour son application ou s'il en constitue la base légale ; que l'arrêté préfectoral du 16 décembre 2015 fixant, sur le fondement des dispositions législatives précitées, le nombre et la répartition des sièges au sein d'un conseil communautaire n'est pas pris pour l'application du schéma régional de coopération intercommunale et du projet de périmètre de fusion, lesquels n'en constituent pas, en tout état de cause, sa base légale ; que, par suite, la circonstance que les arrêtés du 4 mars 2015 et du 18 mai 2015 soient illégaux, à la supposer établie, est sans incidence sur la légalité de l'arrêté du préfet de la région d'Ile-de-France du 16 décembre 2015 ; qu'ainsi, le moyen doit être écarté ;<br/>
<br/>
              En ce qui concerne les moyens relatifs à la désignation des conseillers communautaires du 19 décembre 2015 :<br/>
<br/>
              S'agissant du moyen tiré de la date retenue pour l'organisation de cette désignation :<br/>
<br/>
              13. Considérant qu'aux termes de l'article L. 5211-6-2 du code général des collectivités territoriales, dans sa version applicable au litige : " Par dérogation aux articles L. 5211-6 et L. 5211-6-1, entre deux renouvellements généraux des conseils municipaux : 1° En cas de création d'un établissement public de coopération intercommunale à fiscalité propre, de fusion entre plusieurs établissements publics de coopération intercommunale dont au moins l'un d'entre eux est à fiscalité propre, d'extension du périmètre d'un tel établissement par l'intégration d'une ou de plusieurs communes ou la modification des limites territoriales d'une commune membre ou d'annulation par la juridiction administrative de la répartition des sièges de conseiller communautaire, il est procédé à la détermination du nombre et à la répartition des sièges de conseiller communautaire dans les conditions prévues à l'article L. 5211-6-1. / (...) Dans les communes dont le conseil municipal est élu selon les modalités prévues au chapitre III du titre IV dudit livre Ier : / (...) c) Si le nombre de sièges attribués à la commune est inférieur au nombre de conseillers communautaires élus à l'occasion du précédent renouvellement général du conseil municipal, les membres du nouvel organe délibérant sont élus par le conseil municipal parmi les conseillers communautaires sortants au scrutin de liste à un tour, sans adjonction ni suppression de noms et sans modification de l'ordre de présentation. La répartition des sièges entre les listes est opérée à la représentation proportionnelle à la plus forte moyenne. Si le nombre de candidats figurant sur une liste est inférieur au nombre de sièges qui lui reviennent, le ou les sièges non pourvus sont attribués à la ou aux plus fortes moyennes suivantes. / (...) Le mandat des conseillers communautaires précédemment élus et non membres du nouvel organe délibérant de l'établissement public de coopération intercommunale à fiscalité propre prend fin à compter de la date de la première réunion de ce nouvel organe délibérant. " ;<br/>
<br/>
              14. Considérant qu'il résulte de l'instruction que, sur le fondement de l'arrêté du 16 décembre 2015 par lequel le préfet de la région d'Ile-de-France a fixé, conformément aux dispositions de l'article L. 5211-6-2 du code général des collectivités territoriales précitées, le nombre et la répartition des sièges au sein du conseil communautaire du nouvel EPCI de Saint-Quentin-en-Yvelines à compter du 1er janvier 2016, le conseil municipal de Coignières a procédé le 19 décembre 2015 aux opérations électorales en vue de désigner les deux conseillers communautaires ayant vocation à représenter la commune au sein de ce nouvel établissement public ; que si le préfet des Yvelines n'a pris l'arrêté portant fusion des deux EPCI et création du nouvel EPCI que le 24 décembre 2015, aucune disposition législative ou réglementaire, ni aucun principe général du droit ne fait obstacle à ce que la désignation des conseillers communautaires n'intervienne avant que n'ait une existence légale l'EPCI au sein duquel ils sont appelés à siéger conformément au cadre légal fixé par les arrêtés préfectoraux cités au point 10 ; qu'ainsi, le moyen  tiré de ce que le conseil municipal ne pouvait légalement procéder aux opérations électorales, alors que le nouvel EPCI n'avait encore aucune existence légale au jour du scrutin, doit être écarté ;<br/>
<br/>
              S'agissant du moyen relatif à la double candidature de Mme H...:<br/>
<br/>
              15. Considérant qu'il résulte de l'instruction que, le 19 décembre 2015, lorsque le scrutin a été organisé, M. K..., maire de Coignières a indiqué que lui-même et Mme H... étaient candidats sur une même liste ; que M. I...s'est alors porté candidat en indiquant diriger une liste comportant également en seconde position MmeH..., en dépit du refus exprès de cette dernière ; qu'à l'issue du scrutin, la liste de M. K...et Mme H... a remporté vingt suffrages et la liste de M. I... et Mme H...un seul suffrage ; que les requérants soutiennent que la double candidature de Mme H...rend nulle l'élection de la liste ayant remporté le suffrage ; qu'il résulte toutefois de l'instruction que la circonstance que Mme H...a été inscrite contre son gré sur la liste de M. I...a été, dans les circonstances de l'espèce et eu égard à l'écart de voix, sans incidence sur le résultat du scrutin ; qu'ainsi, le moyen doit être écarté ;<br/>
<br/>
              En ce qui concerne les autres moyens de la requête :<br/>
<br/>
              16. Considérant que, pour demander l'annulation de la désignation des conseillers communautaires du 19 décembre 2015 et la réinstallation des trois conseillers communautaires de Coignières dont les mandats ont été supprimés du fait de la tenue de ce scrutin, les requérants se bornent à reprendre les moyens soulevés en première instance tirés de ce que le scrutin électoral organisé le 19 décembre 2015 au sein du conseil municipal devait respecter l'ordre de présentation des conseillers communautaires désignés par le suffrage universel direct à l'occasion des élections municipales du 23 mars 2014, en application du c) du 1° de l'article L. 5211-6-2 du code général des collectivités territoriales et de ce que le secret du scrutin aurait été méconnu dès lors qu'au moment du dépouillement, un scrutateur a reconnu son bulletin et a indiqué le sens de son vote ; qu'il y a lieu, en l'absence d'éléments nouveaux, d'écarter ces moyens par adoption des motifs retenus par les premiers juges ; <br/>
<br/>
              17. Considérant qu'il résulte de ce qui précède que les conclusions à fin d'annulation des opérations électorales du 19 décembre 2015 et les conclusions tendant à la réinstallation des trois conseillers communautaires de Coignières ne peuvent qu'être rejetées ;<br/>
<br/>
              Sur les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              18. Considérant que ces dispositions font obstacle à ce qu'une somme soit mise à la charge de M. K...et de Mme H...qui ne sont pas, dans la présente instance, la partie perdante ; qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par M. K...et Mme H...au titre des mêmes dispositions ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : Il n'y a pas lieu de renvoyer au Conseil constitutionnel la question de la conformité aux droits et libertés garantis par la Constitution des dispositions du c) du 1° de l'article L. 5211-6-2 du code général des collectivités territoriales.<br/>
<br/>
Article 2 : La requête de M. F..., MmeG..., M.B..., MmeR..., Mme L... et M. P...est rejetée.<br/>
<br/>
Article 3 : Les conclusions de M. K...et de Mme H...présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 4 : La présente décision sera notifiée à M. E...F..., Mme O...G..., M. C... B..., MmeJ... R..., Mme M...L..., M. A...P..., M. Q...K..., à Mme N...H...et au ministre de l'intérieur.<br/>
Copie en sera adressée à M. D... I... et à la Commission nationale des comptes de campagne et des financements politiques.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-08-01 ACTES LÉGISLATIFS ET ADMINISTRATIFS. APPLICATION DANS LE TEMPS. ENTRÉE EN VIGUEUR. - ELECTION AU CONSEIL COMMUNAUTAIRE D'UN NOUVEL EPCI - POSSIBILITÉ POUR UNE COMMUNE DE PROCÉDER À L'ÉLECTION DES CONSEILLERS COMMUNAUTAIRES AVANT LA CRÉATION DE L'EPCI SI SON CADRE LÉGAL EST DÉJÀ FIXÉ - EXISTENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">135-05-01-01 COLLECTIVITÉS TERRITORIALES. COOPÉRATION. ÉTABLISSEMENTS PUBLICS DE COOPÉRATION INTERCOMMUNALE - QUESTIONS GÉNÉRALES. DISPOSITIONS GÉNÉRALES ET QUESTIONS COMMUNES. - ELECTION AU CONSEIL COMMUNAUTAIRE D'UN NOUVEL EPCI - POSSIBILITÉ POUR UNE COMMUNE DE PROCÉDER À L'ÉLECTION DES CONSEILLERS COMMUNAUTAIRES AVANT LA CRÉATION DE L'EPCI SI SON CADRE LÉGAL EST DÉJÀ FIXÉ - EXISTENCE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">54-10-02 PROCÉDURE. - 1) POSSIBILITÉ DE FORMER EN APPEL UNE QPC CONTRE LES MÊMES DISPOSITIONS QUE CELLES VISÉES PAR UNE QPC POSÉE EN PREMIÈRE INSTANCE - ABSENCE, DÈS LORS QU'IL CONVIENT DE CONTESTER LE REFUS DE TRANSMISSION [RJ1] - 2) APPLICATION À UN CAS OÙ LA QPC DE PREMIÈRE INSTANCE EST ADOSSÉE À UN MOYEN NON REPRIS EN APPEL - EXISTENCE.
</SCT>
<SCT ID="8D" TYPE="PRINCIPAL">54-10-10 PROCÉDURE. - 1) POSSIBILITÉ DE FORMER EN APPEL UNE QPC CONTRE LES MÊMES DISPOSITIONS QUE CELLES VISÉES PAR UNE QPC POSÉE EN PREMIÈRE INSTANCE - ABSENCE, DÈS LORS QU'IL CONVIENT DE CONTESTER LE REFUS DE TRANSMISSION [RJ1] - 2) APPLICATION À UN CAS OÙ LA QPC DE PREMIÈRE INSTANCE EST ADOSSÉE À UN MOYEN NON REPRIS EN APPEL - EXISTENCE.
</SCT>
<ANA ID="9A"> 01-08-01 Aucune disposition législative ou réglementaire, ni aucun principe général du droit ne fait obstacle à ce que la désignation par une commune de ses conseillers communautaires au sein d'un nouvel EPCI n'intervienne avant que le préfet n'ait pris l'arrêté portant création du nouvel EPCI au sein duquel ces conseillers sont appelés à siéger, conformément à un cadre légal déjà fixé à la date de cette désignation par des arrêtés préfectoraux ayant arrêté le schéma de coopération intercommunale, le périmètre de fusion des deux EPCI et le nombre et la répartition des sièges au sein du nouveau conseil communautaire.</ANA>
<ANA ID="9B"> 135-05-01-01 Aucune disposition législative ou réglementaire, ni aucun principe général du droit ne fait obstacle à ce que la désignation par une commune de ses conseillers communautaires au sein d'un nouvel EPCI n'intervienne avant que le préfet n'ait pris l'arrêté portant création du nouvel EPCI au sein duquel ces conseillers sont appelés à siéger, conformément à un cadre légal déjà fixé à la date de cette désignation par des arrêtés préfectoraux ayant arrêté le schéma de coopération intercommunale, le périmètre de fusion des deux EPCI et le nombre et la répartition des sièges au sein du nouveau conseil communautaire.</ANA>
<ANA ID="9C"> 54-10-02 1) Il résulte des article 23-1 et 23-2 de l'ordonnance n° 58-1067 du 7 novembre 1958 et de l'article R. 771-16 du code de justice administrative que, lorsqu'un tribunal administratif a refusé de transmettre au Conseil d'Etat la question prioritaire de constitutionnalité (QPC) qui lui a été soumise, il appartient à l'auteur de cette question de contester ce refus à l'occasion de l'appel formé contre le jugement qui statue sur le litige, dans le délai de recours contentieux et par un mémoire distinct et motivé, que le refus de transmission précédemment opposé l'ait été par une décision distincte du jugement, dont il joint alors une copie, ou directement par ce jugement....  ,,2) En l'espèce, impossibilité de faire valoir devant le juge d'appel, dans le cadre d'une nouvelle QPC, l'inconstitutionnalité des mêmes dispositions législatives ayant fait l'objet d'une QPC en première instance, alors même que, devant le tribunal administratif, cette inconstitutionnalité était invoquée au soutien d'un moyen critiquant un arrêté, pris pour l'application de la loi visée par la QPC, que le tribunal a jugé inapplicable au litige pour rejeter la QPC et que, en appel, le requérant n'invoque plus cet arrêté.</ANA>
<ANA ID="9D"> 54-10-10 1) Il résulte des article 23-1 et 23-2 de l'ordonnance n° 58-1067 du 7 novembre 1958 et de l'article R. 771-16 du code de justice administrative que, lorsqu'un tribunal administratif a refusé de transmettre au Conseil d'Etat la question prioritaire de constitutionnalité (QPC) qui lui a été soumise, il appartient à l'auteur de cette question de contester ce refus à l'occasion de l'appel formé contre le jugement qui statue sur le litige, dans le délai de recours contentieux et par un mémoire distinct et motivé, que le refus de transmission précédemment opposé l'ait été par une décision distincte du jugement, dont il joint alors une copie, ou directement par ce jugement....  ,,2) En l'espèce, impossibilité de faire valoir devant le juge d'appel, dans le cadre d'une nouvelle QPC, l'inconstitutionnalité des mêmes dispositions législatives ayant fait l'objet d'une QPC en première instance, alors même que, devant le tribunal administratif, cette inconstitutionnalité était invoquée au soutien d'un moyen critiquant un arrêté, pris pour l'application de la loi visée par la QPC, que le tribunal a jugé inapplicable au litige pour rejeter la QPC et que, en appel, le requérant n'invoque plus cet arrêté.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 1er février 2011, SARL Prototype Technique Industrie (Prototech), n° 342536, p. 24. Comp. CE, 1er février 2012, Région Centre, n° 351795, T. p. 957.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
