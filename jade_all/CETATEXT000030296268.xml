<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030296268</ID>
<ANCIEN_ID>JG_L_2015_02_000000382557</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/29/62/CETATEXT000030296268.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème et 7ème sous-sections réunies, 27/02/2015, 382557, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-02-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>382557</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème et 7ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP TIFFREAU, MARLANGE, DE LA BURGADE</AVOCATS>
<RAPPORTEUR>M. Tristan Aureau</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Xavier Domino</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:382557.20150227</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu 1°, sous le n° 382557, le pourvoi, enregistré le 11 juillet 2014 au secrétariat du contentieux du Conseil d'Etat, de la ministre de l'écologie, du développement durable et de l'énergie, qui demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 13LY01526 du 14 mai 2014 par lequel la cour administrative d'appel de Lyon a, sur la requête de Mme B...A...et autres, d'une part, annulé le jugement n° 1202046 du 10 avril 2013 par lequel le tribunal administratif de Lyon a rejeté leur demande tendant à l'annulation de l'arrêté n° 2012-758 du préfet du Rhône du 23 janvier 2012 déclarant d'utilité publique le projet d'aménagement de l'échangeur n° 7 sur la route nationale 346 pour la desserte du Grand Stade à Décines-Charpieu, sur les communes de Chassieu, Décines-Charpieu et Meyzieu, et emportant mise en compatibilité du plan local d'urbanisme de la communauté urbaine de Lyon sur ces communes et, d'autre part, annulé cet arrêté ; <br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de Mme A...et autres ;<br/>
<br/>
<br/>
<br/>
              Vu 2°, sous le n° 382632, le pourvoi sommaire et le mémoire complémentaire, enregistrés les 15 juillet et 15 octobre 2014 au secrétariat du contentieux du Conseil d'Etat, présentés pour la communauté urbaine de Lyon, qui demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler le même arrêt du 14 mai 2014 de la cour administrative d'appel de Lyon ; <br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de Mme A...et autres ; <br/>
<br/>
              3°) de mettre à la charge de Mme A...et autres la somme de 4 000 euros sur le fondement de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
			....................................................................................<br/>
<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu les notes en délibéré, enregistrées le 16 février 2015, présentées pour Mme A... et autres ;<br/>
<br/>
              Vu la directive 85/337/CEE du Conseil du 27 juin 1985 modifiée ;<br/>
<br/>
              Vu le code de l'environnement ;<br/>
<br/>
              Vu le code de l'expropriation pour cause d'utilité publique ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Tristan Aureau, auditeur, <br/>
<br/>
              - les conclusions de M. Xavier Domino, rapporteur public,<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Spinosi, Sureau, avocat de la communauté urbaine de Lyon (Grand Lyon), et à la SCP Tiffreau, Marlange, de la Burgade, avocat de Mme A...et autres ; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1.	Considérant que les pourvois de la ministre de l'écologie, du développement durable et de l'énergie et de la communauté urbaine de Lyon sont dirigés contre le même arrêt de la cour administrative d'appel de Lyon ; qu'il y a lieu de les joindre pour statuer par une seule décision ; <br/>
<br/>
              2.	Considérant qu'il ressort des énonciations des arrêts attaqués que, pour assurer la desserte du projet de Grand Stade prévu sur le territoire de la commune de Décines-Charpieu, a été notamment prévu d'aménager l'échangeur n° 7 de la route nationale 346 (rocade est) afin d'assurer la desserte du site par les véhicules particuliers, les navettes bus mises en place les soirs d'événement et les services de secours ; que ce projet a été soumis à une enquête publique entre les 14 juin et 18 juillet 2011, et déclaré d'utilité publique par un arrêté du 23 janvier 2012 du préfet du Rhône ; que le tribunal administratif de Lyon a rejeté le recours pour excès de pouvoir formé contre cet arrêté par un jugement du 10 avril 2013 ; que la cour administrative d'appel de Lyon, par un arrêt du 14 mai 2014, a annulé ce jugement ainsi que l'arrêté du 23 janvier 2012 du préfet du Rhône ; que la ministre de l'écologie, du développement durable et de l'énergie et la communauté urbaine de Lyon se pourvoient en cassation contre cet arrêt ;<br/>
<br/>
              3.	Considérant qu'aux termes de l'article L. 123-1 du code de l'environnement dans sa version alors applicable : " I - La réalisation d'aménagements, d'ouvrages ou de travaux exécutés par des personnes publiques ou privées est précédée d'une enquête publique soumise aux prescriptions du présent chapitre, lorsqu'en raison de leur nature, de leur consistance ou du caractère des zones concernées, ces opérations sont susceptibles d'affecter l'environnement. (...) " ; qu'aux termes de l'article R. 123-13 du même code, dans sa rédaction alors applicable : " Le préfet, après consultation du commissaire enquêteur ou du président de la commission d'enquête, précise par arrêté : / 1° L'objet de l'enquête, la date à laquelle celle-ci sera ouverte et sa durée, qui ne peut ni être inférieure à un mois ni, sauf prorogation d'une durée maximum de quinze jours décidée par le commissaire enquêteur ou par la commission d'enquête, excéder deux mois ; / 2° Les lieux, ainsi que les jours et heures où le public pourra consulter le dossier d'enquête et présenter ses observations sur le registre ouvert à cet effet (...) ; / 3° Les noms et qualités du commissaire enquêteur ou des membres de la commission d'enquête et de leurs suppléants éventuels ; / 4° Les lieux, jours et heures où le commissaire enquêteur ou un membre de la commission d'enquête se tiendra à la disposition du public pour recevoir ses observations ; / 5° Les lieux où, à l'issue de l'enquête, le public pourra consulter le rapport et les conclusions du commissaire enquêteur ou de la commission d'enquête ; / 6° Si le projet a fait l'objet d'une étude d'impact ou d'une notice d'impact dans les conditions prévues par les articles R. 122-1 à R. 122-16, la mention de la présence de ce document dans le dossier d'enquête ; / 7° L'information selon laquelle, le cas échéant, le dossier d'enquête publique est transmis à un autre Etat ; / 8° L'identité de l'autorité compétente pour prendre la décision d'autorisation ou d'approbation et la nature de celle-ci ; / 9° L'identité de la personne responsable du projet ou l'autorité auprès de laquelle des informations peuvent être demandées " ; que, selon l'article R. 123-14 du même code : " Un avis portant ces indications à la connaissance du public est, par les soins du préfet, publié en caractères apparents quinze jours au moins avant le début de l'enquête et rappelé dans les huit premiers jours de celle-ci dans deux journaux régionaux ou locaux diffusés dans le ou les départements concernés (...) " ; qu'en vertu des dispositions de l'article R. 123-6 du code de l'environnement, l'étude d'impact, lorsqu'elle est requise, fait partie intégrante du dossier soumis à enquête publique ;<br/>
<br/>
              4.	Considérant que s'il appartient à l'autorité administrative de procéder à l'ouverture de l'enquête publique et à la publicité de celle-ci dans les conditions fixées par les dispositions du code de l'environnement précédemment citées, la méconnaissance de ces dispositions n'est toutefois de nature à vicier la procédure et donc à entraîner l'illégalité de la décision prise à l'issue de l'enquête publique que si elle n'a pas permis une bonne information de l'ensemble des personnes intéressées par l'opération ou si elle a été de nature à exercer une influence sur les résultats de l'enquête et, par suite, sur la décision de l'autorité administrative ;<br/>
<br/>
              5.	Considérant que, pour annuler l'arrêté contesté devant elle, la cour administrative d'appel de Lyon, après avoir relevé que l'arrêté du préfet du Rhône prescrivant l'ouverture de l'enquête publique et l'avis au public relatif à cette enquête avaient omis de mentionner que le projet avait fait l'objet d'une étude d'impact et que ce document faisait partie du dossier soumis à l'enquête, a estimé que cette méconnaissance des dispositions des articles R. 123-13 et R. 123-14 avait été de nature à nuire à l'information des personnes intéressées par le projet et justifiait l'annulation de l'arrêté portant déclaration d'utilité publique ; <br/>
<br/>
              6.	Considérant que la cour admettait toutefois que l'étude d'impact, qu'elle qualifiait au demeurant de particulièrement volumineuse, figurait dans le dossier d'enquête et avait pu être consultée par le public lors des permanences de la commission d'enquête ; qu'elle relevait, en outre, le nombre d'observations recueillies au cours de l'enquête, ainsi que le fait que le programme du Grand Stade avait été largement couvert par les médias, la circonstance que le dossier de permis de construire le stade avait été soumis à enquête publique avec mention de l'existence de l'étude d'impact et la circonstance que la direction régionale de l'environnement, de l'aménagement et du logement avait émis un avis sur l'étude d'impact disponible par voie électronique ; <br/>
<br/>
              7.	Considérant qu'en se fondant sur la seule circonstance qu'avait été omise la mention relative à l'existence de l'étude d'impact dans l'arrêté d'ouverture de l'enquête publique et l'avis au public pour estimer que la procédure avait été viciée, alors que ce seul élément, en l'absence d'autres circonstances, n'est pas de nature à faire obstacle, faute d'information suffisante, à la participation effective du public à l'enquête ou à exercer une influence sur les résultats de l'enquête, la cour administrative d'appel a commis une erreur de droit ;<br/>
<br/>
              8.	Considérant qu'il résulte de ce qui précède que, sans qu'il soit besoin d'examiner les autres moyens des pourvois, la ministre de l'écologie, du développement durable et de l'énergie et la communauté urbaine de Lyon sont fondées à demander l'annulation des arrêts qu'ils attaquent ;<br/>
<br/>
              9.	Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat ou de la communauté urbaine de Lyon qui ne sont pas, dans la présente instance, des parties perdantes ; qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par la communauté urbaine de Lyon au titre de ces mêmes dispositions ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : L'arrêt n° 13LY01526 de la cour administrative d'appel de Lyon du 14 mai 2014 est annulé.<br/>
<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Lyon.<br/>
<br/>
Article 3 : Le surplus des conclusions du pourvoi de la communauté urbaine de Lyon est rejeté.<br/>
<br/>
Article 4 : Les conclusions présentées par Mme A...et autres au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
<br/>
Article 5 : La présente décision sera notifiée à la ministre de l'écologie, du développement durable et de l'énergie, à la métropole de Lyon et à Mme B...A..., premier défendeur nommé. Les autres défendeurs seront informés de la présente décision par la SCP Tiffreau, Marlange, de la Burgade, avocat au Conseil d'Etat et à la Cour de cassation, qui les représente devant le Conseil d'Etat et est chargée, à ce titre, de leur donner connaissance de cette décision. Copie en sera adressée au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
