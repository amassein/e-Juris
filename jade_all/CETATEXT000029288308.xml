<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029288308</ID>
<ANCIEN_ID>JG_L_2014_07_000000374526</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/28/83/CETATEXT000029288308.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  2ème sous-section jugeant seule, 23/07/2014, 374526, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-07-23</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>374526</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 2ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LE GRIEL</AVOCATS>
<RAPPORTEUR>M. Tristan Aureau</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Béatrice Bourgeois-Machureau</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2014:374526.20140723</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi, enregistré le 10 janvier 2014 au secrétariat du contentieux du Conseil d'Etat, présenté pour M. A...B..., demeurant ...; M. B...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'ordonnance n° 1305504 du 29 novembre 2013 par laquelle le juge des référés du tribunal administratif de Montpellier a rejeté sa demande tendant, d'une part, à la suspension, sur le fondement des dispositions de l'article L. 521-1 du code de justice administrative, de l'exécution de la décision du 16 septembre 2013 par laquelle le préfet des Pyrénées-Orientales a refusé le renouvellement de son titre de séjour et, d'autre part, à ce qu'il soit enjoint au préfet de réexaminer sa demande et de lui délivrer un récépissé dans l'attente de ce réexamen ;<br/>
<br/>
              2°) statuant en référé, de faire droit à sa demande ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat le versement de la somme de 3 000 euros à la SCP Le Griel, son avocat, en application des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 ; <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de l'entrée et du séjour des étrangers et du droit d'asile ; <br/>
<br/>
              Vu la loi n° 91-647 du 10 juillet 1991 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Tristan Aureau, auditeur, <br/>
<br/>
              - les conclusions de Mme Béatrice Bourgeois-Machureau, rapporteur public,<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Le Griel, avocat de M. B...; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision " ;<br/>
<br/>
              2. Considérant que l'urgence justifie que soit prononcée la suspension d'un acte administratif lorsque l'exécution de celui-ci porte atteinte, de manière suffisamment grave et immédiate, à un intérêt public, à la situation du requérant ou aux intérêts qu'il entend défendre ; qu'il appartient au juge des référés, saisi d'une demande de suspension d'une décision refusant la délivrance d'un titre de séjour, d'apprécier si la condition d'urgence est remplie compte tenu de l'incidence immédiate du refus de titre de séjour sur la situation concrète de l'intéressé ; que cette condition d'urgence est en principe satisfaite dans le cas d'un refus de renouvellement ou d'un retrait du titre de séjour ; que, dans les autres cas, il appartient au requérant de justifier de circonstances particulières caractérisant la nécessité pour lui de bénéficier à très bref délai d'une mesure provisoire dans l'attente d'une décision juridictionnelle statuant  sur la légalité de la décision litigieuse ;<br/>
<br/>
              3. Considérant que, pour rejeter la demande de M. B...tendant à la suspension de l'exécution de la décision du préfet des Pyrénées-Orientales du 16 septembre 2013 refusant le renouvellement de son titre de séjour, le juge des référés du tribunal administratif de Montpellier s'est fondé sur le motif que le demandeur n'établissait pas la nécessité de bénéficier à très bref délai d'une mesure provisoire ; que, toutefois, ainsi qu'il a été dit, dans le cas de décisions refusant le renouvellement d'un titre de séjour, la condition d'urgence doit en principe être regardée comme remplie ; que, dès lors, le juge des référés ne pouvait, sans erreur de droit, exiger du requérant qu'il justifie de circonstances particulières pour caractériser une situation d'urgence ; que, par suite, et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, l'ordonnance attaquée doit être annulée ;<br/>
<br/>
              4. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au titre de la procédure de référé en application des dispositions de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              5. Considérant que M. B...fait valoir, à l'appui de sa demande de suspension de l'exécution de la décision du 16 septembre 2013 du préfet des Pyrénées-Orientales refusant le renouvellement du titre de séjour dont il était titulaire, que ce refus méconnaît les dispositions du 11° de l'article L. 313-11 du code  de l'entrée et du séjour des étrangers et du droit d'asile ; qu'il est entaché d'erreur manifeste d'appréciation ; qu'il aurait dû être précédé de la consultation de la commission du titre de séjour ; qu'il avait droit à la délivrance d'un titre de séjour en qualité de retraité ; qu'aucun de ces moyens n'est, toutefois, en l'état de l'instruction, de nature à créer un doute sérieux quant à la légalité de la décision contestée ; que les conclusions à fin de suspension ne peuvent, dès lors, qu'être rejetées ;<br/>
<br/>
              6. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'il soit fait droit aux conclusions présentées sur le fondement de l'article 37 de la loi du 10 juillet 1991 ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
                --------------<br/>
Article 1er : L'ordonnance du juge des référés du tribunal administratif de Montpellier du 29 novembre 2013 est annulée.<br/>
<br/>
Article 2 : La demande présentée par M. B...devant le tribunal administratif de Montpellier et les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
<br/>
Article 3 : La présente décision sera notifiée à M. A...B...et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
