<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034267047</ID>
<ANCIEN_ID>JG_L_2017_03_000000387218</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/26/70/CETATEXT000034267047.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème chambre, 22/03/2017, 387218, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-03-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>387218</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BARADUC, DUHAMEL, RAMEIX ; LE PRADO ; SCP FOUSSARD, FROGER ; SCP SEVAUX, MATHONNET</AVOCATS>
<RAPPORTEUR>M. Charles Touboul</RAPPORTEUR>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CECHS:2017:387218.20170322</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu les procédures suivantes :<br/>
<br/>
              Mme A...B...a demandé au tribunal administratif de Versailles de condamner le centre hospitalier André Mignot de Versailles à réparer les préjudices ayant résulté d'une intervention chirurgicale qu'elle a subie dans cet établissement le 1er avril 2004. L'Office national d'indemnisation des accidents médicaux, des affections iatrogènes et des infections nosocomiales (ONIAM) a demandé que les indemnités qu'il avait versées à l'intéressée au titre de certains postes de préjudice soit mises à la charge du centre hospitalier. La caisse primaire d'assurance maladie (CPAM) de la Gironde a demandé le remboursement de ses débours. Par un jugement n° 0901637, 1007705 du 31 juillet 2013, le tribunal administratif a condamné le centre hospitalier à verser 377 422,48 euros à MmeB..., 65 000 euros à l'ONIAM et 343 758,38 euros à la CPAM de la Gironde.<br/>
<br/>
              Par un arrêt n° 13VE03138 du 18 novembre 2014, la cour administrative d'appel de Versailles, faisant droit à l'appel du centre hospitalier André Mignot, a annulé ce jugement et rejeté l'ensemble des demandes présentées devant le tribunal administratif. <br/>
<br/>
              1° Sous le n° 387218, par un pourvoi sommaire et un mémoire complémentaire enregistrés les 19 janvier et 2 avril 2015 au secrétariat du contentieux du Conseil d'Etat, Mme B... demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) de renvoyer le litige à la cour administrative d'appel de Versailles ; <br/>
<br/>
              3°) de mettre à la charge du centre hospitalier André Mignot la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              2° Sous le n° 387301, par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 21 janvier et 16 avril 2015 au secrétariat du contentieux du Conseil d'Etat, la CPAM de la Gironde demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler l'arrêt du 18 novembre 2014;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel; <br/>
<br/>
              3°) de mettre à la charge du centre hospitalier André Mignot la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu :<br/>
<br/>
              - le code de la santé publique ;<br/>
<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Charles Touboul, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Baraduc, Duhamel, Rameix, avocat de MmeB..., à Me Le Prado, avocat du centre hospitalier André Mignot de Versailles, à la SCP Foussard, Froger, avocat de la caisse primaire d'assurance maladie de la Gironde et à la SCP Sevaux, Mathonnet, avocat de l'Office national d'indemnisation des accidents médicaux, des affections iatrogènes et des infections nosocomiales.<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que MmeB..., qui présentait une endométriose sévère entraînant des douleurs et une infertilité, a subi le 1er avril 2004 au centre hospitalier André Mignot de Versailles une intervention chirurgicale destinée à traiter cette affection ; qu'en raison d'une lésion du nerf pudendal survenue au cours de l'intervention, elle est demeurée atteinte d'une incontinence urinaire et sphinctérienne ; que la commission régionale de conciliation et d'indemnisation des accidents médicaux (CRCI) d'Ile-de-France ayant émis l'avis que le dommage était la conséquence d'une faute engageant la responsabilité du centre hospitalier, l'Office national d'indemnisation des accidents médicaux, des affections iatrogènes et des affections nosocomiales (ONIAM) a fait à la victime, en application des dispositions de l'article L. 1142-15 du code de la santé publique, une offre d'indemnisation en lieu et place de l'assureur de l'établissement qui s'y était refusé ; que Mme B...a accepté cette offre pour certains postes de préjudice et a demandé au tribunal administratif de Versailles de condamner le centre hospitalier à réparer les autres postes ou, subsidiairement, d'en mettre la réparation à la charge de l'ONIAM au titre de la solidarité nationale, sur le fondement des dispositions du II de l'article L. 1142-1 du code de la santé publique ; que l'ONIAM, au titre des indemnités qu'il avait versées à la victime, et la caisse primaire d'assurance maladie (CPAM) de la Gironde, au titre des frais exposés par elle à la suite de l'accident, ont exercé des actions subrogatoires contre le centre hospitalier ; que, par un jugement du 31 juillet 2013, le tribunal administratif a estimé que les médecins avaient commis une faute en pratiquant une exérèse profonde des lésions d'endométriose rectales et pelviennes au lieu de se borner à une exérèse des lésions présentes sur le site de fécondation, faisant ainsi courir à la patiente un risque injustifié dès lors que l'intervention avait pour objet de remédier à une infertilité ; que le tribunal a, en conséquence, condamné l'établissement à indemniser tant Mme B...que l'ONIAM et la CPAM de la Gironde ; que, par un arrêt du 18 novembre 2014, la cour administrative d'appel de Versailles, faisant droit à l'appel du centre hospitalier, a annulé le jugement et rejeté l'ensemble des demandes de première instance ; que, sous le n° 387301, Mme B...se pourvoit en cassation contre cet arrêt en tant qu'il rejette ses conclusions dirigées contre le centre hospitalier et contre l'ONIAM ; que l'ONIAM demande par la voie d'un pourvoi provoqué que si l'arrêt est annulé en tant qu'il rejette les conclusions dirigées à son encontre, il soit également annulé en tant qu'il rejette ses propres conclusions dirigées contre le centre hospitalier ; que, sous le n° 387301, la CPAM de la Gironde demande que l'arrêt soit annulé en tant qu'il rejette ses conclusions dirigées contre le centre hospitalier ; qu'il y a lieu de joindre les pourvois pour statuer par une seule décision ; <br/>
<br/>
              Sur la responsabilité du centre hospitalier André Mignot :<br/>
<br/>
              En ce qui concerne l'indication opératoire et la conduite de l'intervention :<br/>
<br/>
              2. Considérant que, pour écarter l'existence d'une faute dans l'indication opératoire comme dans la conduite de l'intervention, la cour administrative d'appel a estimé qu'il résultait de l'instruction et, notamment, des conclusions des experts désignés par la CRCI d'Ile-de-France et par le tribunal administratif de Versailles, que l'intervention était justifiée non seulement par le désir de grossesse de Mme B...mais aussi par la gravité des lésions liées à l'endométriose, que les complications hémorragiques survenues ne présentaient pas un caractère anormal dans ce type d'opérations et imposaient de poursuivre l'intervention pour y mettre fin, que seule une exérèse complète des lésions liées à l'endométriose de la patiente était de nature à permettre d'atteindre le résultat recherché et que, selon les deux experts, l'intervention avait été conduite dans les règles de l'art par un chirurgien expérimenté ; <br/>
<br/>
              3. Considérant, en premier lieu, que, contrairement à ce que soutient la CPAM de la Gironde, il ne ressort pas des termes de l'arrêt que la cour se soit crue tenue de suivre les conclusions des experts, commettant ainsi une erreur de droit ;<br/>
<br/>
              4. Considérant, en second lieu, qu'en retenant que l'intervention avait eu pour objet non seulement de permettre à la patiente d'avoir un second enfant, mais aussi de remédier aux douleurs importantes résultant de l'endométriose sévère dont elle était atteinte, la cour administrative d'appel, dont l'arrêt est suffisamment motivé sur ce point, n'a pas dénaturé les faits qui lui étaient soumis ; qu'en estimant que le fait de procéder à une exérèse profonde des lésions d'endométriose, seule de nature à permettre d'atteindre le but recherché, ne présentait pas le caractère d'une faute médicale, elle n'a pas inexactement qualifié les faits de l'espèce ; <br/>
<br/>
              5. Considérant, enfin, que la cour n'a pas davantage entaché son arrêt d'une erreur de qualification juridique en écartant l'existence d'une faute dans la conduite de l'intervention, après avoir retenu, dans le cadre d'une appréciation souveraine exempte de dénaturation et par une motivation suffisante, que les complications hémorragiques survenues au début de celle-ci ne justifiaient pas de l'interrompre mais exigeaient au contraire de la poursuivre afin de mettre fin à ces complications ; <br/>
<br/>
              En ce qui concerne l'information donnée à la patiente :<br/>
<br/>
              6. Considérant que la cour a estimé, par une motivation suffisante et sans dénaturer les éléments qui lui étaient soumis, qu'il résultait de l'instruction que les risques connus liés à l'intervention, notamment ceux relatifs aux atteintes possibles aux organes digestifs et à l'urètre, avaient été portés à la connaissance de la patiente et que l'absence d'information sur un risque de trouble neurologique s'expliquait par le fait que ce risque n'était pas répertorié à l'époque ; qu'eu égard à ces éléments, la cour n'a pas commis d'erreur de qualification juridique en ne retenant pas un manquement des médecins à leur devoir d'information ; que si elle a également estimé qu'en tout état de cause l'intéressée n'avait pas perdu une chance de refuser l'intervention dès lors que l'intervention chirurgicale était la seule voie thérapeutique possible pour traiter les lésions dont elle était atteinte, ce motif présente un caractère surabondant ; que, par suite, le moyen tiré de l'erreur de droit commise par la cour en se fondant sur l'utilité de l'intervention pour nier l'existence d'une perte de chance, sans avoir constaté qu'elle ne disposait d'aucune possibilité raisonnable de la refuser, n'est pas opérant ;<br/>
<br/>
              7. Considérant qu'il résulte de ce qui précède que MmeB..., l'ONIAM et la CPAM de la Gironde ne sont pas fondés à demander l'annulation de l'arrêt du 18 novembre 2014 de la cour administrative d'appel de Versailles en tant qu'il se prononce sur la responsabilité du centre hospitalier André Mignot ;  <br/>
<br/>
              Sur la réparation du dommage par l'ONIAM au titre de la solidarité nationale :<br/>
<br/>
              8. Considérant qu'aux termes du II de l'article L. 1142-1 du code de la santé publique, dans sa rédaction applicable au litige : " Lorsque la responsabilité d'un professionnel, d'un établissement, service ou organisme mentionné au I ou d'un producteur de produits n'est pas engagée, un accident médical, une affection iatrogène ou une infection nosocomiale ouvre droit à la réparation des préjudices du patient et, en cas de décès, de ses ayants droit au titre de la solidarité nationale, lorsqu'ils sont directement imputables à des actes de prévention, de diagnostic ou de soins et qu'ils ont eu pour le patient des conséquences anormales au regard de son état de santé comme de l'évolution prévisible de celui-ci et présentent un caractère de gravité, fixé par décret, apprécié au regard de la perte de capacités fonctionnelles et des conséquences sur la vie privée et professionnelle mesurées en tenant notamment compte du taux d'atteinte permanente à l'intégrité physique ou psychique, de la durée de l'arrêt temporaire des activités professionnelles ou de celle du déficit fonctionnel temporaire " ; que l'article D. 1142-1 du même code définit le seuil de gravité prévu par ces dispositions législatives ;<br/>
<br/>
              9. Considérant qu'il résulte de ces dispositions que l'ONIAM doit assurer, au titre de la solidarité nationale, la réparation des dommages résultant directement d'actes de prévention, de diagnostic ou de soins à la double condition qu'ils présentent un caractère d'anormalité au regard de l'état de santé du patient comme de l'évolution prévisible de cet état et que leur gravité excède le seuil défini à l'article D. 1142-1 du même code ; que la condition d'anormalité du dommage prévue par ces dispositions doit toujours être regardée comme remplie lorsque l'acte médical a entraîné des conséquences notablement plus graves que celles auxquelles le patient était exposé de manière suffisamment probable en l'absence de traitement ; que, lorsque les conséquences de l'acte médical ne sont pas notablement plus graves que celles auxquelles le patient était exposé par sa pathologie en l'absence de traitement, elles ne peuvent être regardées comme anormales sauf si, dans les conditions où l'acte a été accompli, la survenance du dommage présentait une probabilité faible ; qu'ainsi, elles ne peuvent être regardées comme anormales au regard de l'état du patient lorsque la gravité de cet état a conduit à pratiquer un acte comportant des risques élevés dont la réalisation est à l'origine du dommage ;<br/>
<br/>
              10. Considérant que, pour juger que l'accident survenu à la suite de l'intervention pratiquée le 1er avril 2004 n'avait pas eu des conséquences anormales au regard de l'état de santé initial de Mme B...comme de l'évolution prévisible de cet état, la cour s'est bornée à estimer que l'intervention chirurgicale était indispensable en raison de la sévérité de l'endométriose et de l'étendue des lésions et que cette intervention était délicate et risquée ; qu'en se prononçant ainsi, sans rechercher si les conséquences de l'accident n'étaient pas notablement plus graves que celles auxquelles l'intéressée était exposée par sa pathologie en l'absence de ce traitement chirurgical, la cour a commis une erreur de droit ; <br/>
<br/>
              11. Considérant qu'il résulte de ce qui précède que Mme B...est fondée à demander l'annulation de l'arrêt du 28 novembre 2014 en tant qu'il rejette ses conclusions dirigées contre l'ONIAM ; <br/>
<br/>
              Sur l'application des dispositions de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              12. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soient mises à la charge  du centre hospitalier André Mignot, qui n'est pas dans la présente instance la partie perdante, les sommes demandées sur leur fondement par MmeB..., la CPAM de Gironde et l'ONIAM ; que ces dispositions font également obstacle à ce que la somme demandée par l'ONIAM soit mise à la charge de Mme B..., qui n'est pas la partie perdante ; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 18 novembre 2014 de la cour administrative d'appel de Versailles est annulé en tant qu'il rejette les conclusions de Mme B...dirigées contre l'ONIAM.<br/>
<br/>
Article 2 : L'affaire est renvoyée dans cette mesure à la cour administrative d'appel de Versailles.<br/>
<br/>
		Article 3 : Le surplus des conclusions du pourvoi de Mme B...est rejeté.<br/>
<br/>
		Article 4 : Le pourvoi de la CPAM de la Gironde est rejeté.<br/>
<br/>
Article 5 : Le pourvoi provoqué de l'ONIAM, ainsi que ses conclusions présentées au titre de dispositions de l'article L. 761-1 du code de justice administrative, sont rejetées.<br/>
<br/>
Article 6 : Les conclusions présentées par la CPAM de la Gironde au titre de l'article L.  761-1 du code de justice administrative sont rejetées. <br/>
<br/>
Article 7 : La présente décision sera notifiée à Mme A...B..., au centre hospitalier André Mignot de Versailles, à l'Office national d'indemnisation des accidents médicaux, des affections iatrogènes et des infections nosocomiales et à la caisse primaire d'assurance maladie de la Gironde.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
