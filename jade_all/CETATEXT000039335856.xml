<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039335856</ID>
<ANCIEN_ID>JG_L_2019_11_000000420794</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/33/58/CETATEXT000039335856.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème chambre, 06/11/2019, 420794, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-11-06</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>420794</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LYON-CAEN, THIRIEZ ; SCP MATUCHANSKY, POUPOT, VALDELIEVRE</AVOCATS>
<RAPPORTEUR>Mme Françoise Tomé</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Raphaël Chambon</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2019:420794.20191106</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête enregistrée au secrétariat du contentieux du Conseil d'Etat le 22 mai 2018, M. B... A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision du 15 septembre 2015 du Conseil national de l'ordre des médecins, statuant en formation restreinte, le suspendant du droit d'exercer la médecine et subordonnant la reprise de son activité à la constatation de son aptitude par une expertise effectuée dans les conditions prévues à l'article R. 4124-3 du code de la santé publique ; <br/>
<br/>
              2°) de mettre à la charge du Conseil national de l'ordre des médecins la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de la santé publique ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Françoise Tomé, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Raphaël Chambon, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Lyon-Caen, Thiriez, avocat de M. A... et à la SCP Matuchansky, Poupot, Valdelièvre, avocat du Conseil national de l'ordre des médecins ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article R. 4124-3 du code de la santé publique " I. Dans le cas d'infirmité ou d'état pathologique rendant dangereux l'exercice de la profession, la suspension temporaire du droit d'exercer est prononcée par le conseil régional ou interrégional pour une période déterminée, qui peut, s'il y a lieu, être renouvelée. (...) / II. - La suspension ne peut être ordonnée que sur un rapport motivé établi à la demande du conseil régional ou interrégional par trois médecins désignés comme experts, le premier par l'intéressé, le deuxième par le conseil régional ou interrégional et le troisième par les deux premiers experts. (...) / IV. - Les experts procèdent ensemble, sauf impossibilité manifeste, à l'expertise. (....) / Si l'intéressé ne se présente pas à la convocation fixée par les experts, une seconde convocation lui est adressée. En cas d'absence de l'intéressé aux deux convocations, les experts établissent un rapport de carence à l'intention du conseil régional ou interrégional, qui peut alors suspendre le praticien pour présomption d'infirmité ou d'état pathologique rendant dangereux l'exercice de la profession. (...) / VI. - Si le conseil régional ou interrégional n'a pas statué dans le délai de deux mois à compter de la réception de la demande dont il est saisi, l'affaire est portée devant le Conseil national de l'ordre (...) ". Aux termes de l'article R. 4124-3-3 du même code, la décision de suspension prononcée par le Conseil national, statuant en formation restreinte, est notifiée par lettre recommandée avec demande d'avis de réception au praticien intéressé et " susceptible d'un recours pour excès de pouvoir devant le Conseil d'Etat dans le délai de deux mois ". <br/>
<br/>
              2. Il ressort des pièces du dossier que le pli recommandé, qui contenait la décision du 15 septembre 2015 du Conseil national de l'ordre des médecins, statuant en formation restreinte, par laquelle M. A... a vu son droit d'exercer la médecine suspendu jusqu'à la constatation de son aptitude par une expertise effectuée dans les conditions prévues à l'article R. 4124-3, a été notifié à M. A... le 16 octobre 2015, ainsi qu'en attestent les mentions figurant sur l'avis de réception. En outre, il n'est pas contesté que la lettre de notification figurant dans ce pli mentionnait les voies et délais de recours contre cette décision. Par suite, ainsi que le soutient le Conseil national de l'ordre des médecins, la requête par laquelle M. A... demande l'annulation pour excès de pouvoir de cette décision, enregistrée au Conseil d'Etat le 22 mai 2018, soit après l'expiration du délai de recours contentieux, est tardive. Ses conclusions à fins d'annulation ne peuvent, dès lors, qu'être rejetées comme irrecevables, sans qu'il soit besoin d'examiner les moyens présentés à leur soutien. <br/>
<br/>
              3. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mise à la charge du Conseil national de l'ordre des médecins, qui n'est pas dans la présente instance la partie perdante, la somme que demande M. A... au titre des frais exposés par lui et non compris dans les dépens. Dans les circonstances de l'espèce, il n'y a pas lieu de faire droit aux conclusions présentées au même titre par le Conseil national de l'ordre des médecins. <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de M. A... est rejetée.<br/>
Article 2 : Les conclusions présentées par le Conseil national de l'ordre des médecins au titre de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
Article 3 : La présente décision sera notifiée à M. B... A... et au Conseil national de l'ordre des médecins.<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
