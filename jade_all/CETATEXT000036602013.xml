<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036602013</ID>
<ANCIEN_ID>JG_L_2018_02_000000417482</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/60/20/CETATEXT000036602013.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 05/02/2018, 417482, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-02-05</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>417482</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2018:417482.20180205</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B...A...a demandé au juge des référés du tribunal administratif de Montreuil, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, d'ordonner à l'administration et particulièrement à la police de l'air et des frontières, la restitution de son passeport sous 48 heures et de mettre à la charge de l'Etat la somme de 1 500 euros en application de l'article L. 761-1 du code de justice administrative. Par une ordonnance n° 1800178 du 10 janvier 2018, le juge des référés du tribunal administratif de Montreuil a rejeté sa demande. <br/>
<br/>
<br/>
              Par une requête, enregistrée le 19 janvier 2018 au secrétariat du contentieux du Conseil d'Etat, M. A...demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, d'annuler cette ordonnance, d'ordonner à l'administration, et particulièrement à la police de l'air et des frontières, la restitution de son passeport sous 48 heures et de mettre à la charge de l'Etat la somme de 1 500 euros en application de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Il soutient que : <br/>
              - son titre de voyage délivré par les autorités italiennes a été remis au chef de poste au centre de rétention en juillet 2017 et ne lui a pas été restitué à l'occasion de son départ pour l'Italie le 28 août 2017 ; <br/>
              - il y a urgence à mettre fin à cette situation qui perdure depuis le 28 août 2017 ; <br/>
              - elle porte une atteinte grave et manifestement illégale à son droit à la liberté et à la sûreté prévus à l'article 2 de la déclaration des droits de l'homme, à l'article 66 de la Constitution, et à l'article 5 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales qui constitue une liberté fondamentale ; <br/>
              - son document de voyage ne pouvait être retenu que pour la durée strictement proportionnée aux besoins de l'autorité administrative.<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
	Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
              1. Aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. Le juge des référés se prononce dans un délai de quarante-huit heures ". En vertu de l'article L. 522-3 du même code, le juge des référés peut rejeter une requête par une ordonnance motivée, sans instruction contradictoire ni audience publique, lorsque la condition d'urgence n'est pas remplie ou lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable ou qu'elle est mal fondée. A cet égard, il appartient au juge d'appel de prendre en considération les éléments recueillis par le juge du premier degré dans le cadre de la procédure écrite et orale qu'il a diligentée.<br/>
              2. M.A..., ressortissant togolais, bénéficiaire d'un titre de séjour délivré par les autorités italiennes au titre de la protection subsidiaire accordée aux étrangers exposés à des risques dans leur pays d'origine, a fait l'objet d'une obligation de quitter le territoire français pris par le préfet de la Seine-Saint-Denis le 20 juillet 2017. Pour l'exécution de cette décision, il a été placé en rétention. A cette occasion, différents documents d'identité et de voyage dont un titre de voyage valant passeport, délivré par les autorités italiennes, ont été retenus par les services de police. Au moment de son départ pour l'Italie le 28 août 2017, la carte d'identité et le permis de séjour délivrés par les autorités italiennes lui ont été restitués, mais non le titre de voyage n° MD0022397 qu'il justifie avoir remis au chef de poste du centre de rétention administrative de Paris le 23 juillet 2017. Ayant vainement demandé la restitution de ce titre de voyage par lettre du 24 octobre 2017, M. A...a saisi, sur le fondement de l'article L. 521-2 du code de justice administrative, le juge des référés du tribunal administratif de Montreuil afin qu'il enjoigne à l'administration de le lui restituer en invoquant l'atteinte à son droit à la liberté et à la sûreté. Par ordonnance n° 1800178 du 10 janvier 2018, le juge des référés du tribunal administratif de Montreuil a rejeté sa demande faute d'élément propre à établir l'existence d'une situation d'urgence justifiant l'intervention du juge des référés dans un délai très bref. Par la présente requête, M. A...fait appel de cette ordonnance. <br/>
              3. Pour rejeter la demande de M.A..., le juge des référés du tribunal administratif de Montreuil, tout en relevant le caractère inexpliqué de l'absence de restitution de ce titre, a estimé que la condition d'urgence à quarante-huit heures exigée par l'article L. 521-2 du code de justice administrative n'était pas remplie dans la mesure où les pièces du dossier faisaient apparaître que la privation de son passeport, bien que rendant plus difficiles ses déplacements à l'étranger, n'avait pas fait obstacle à ce qu'il se rende en France depuis lors. En appel, M. A...se borne à reproduire sa demande de première instance sans critiquer directement les motifs de l'ordonnance qui écartent la condition d'urgence et n'apporte en appel aucun élément de nature à infirmer l'appréciation ainsi portée. Ainsi que le juge des référés du tribunal administratif de Montreuil l'a constaté à bon droit et pour les motifs qu'il a retenus, la condition d'urgence requise par l'article L. 521-2 du code de justice administrative pour justifier l'injonction demandée ne peut pas être regardée comme remplie.<br/>
<br/>
              4. Il résulte de ce qui précède, sans qu'il soit besoin d'examiner l'existence d'une atteinte grave et manifestement illégale à une liberté fondamentale, qu'il est manifeste que l'appel de M. A...ne peut être accueilli. Par suite, il y a lieu, dans les circonstances de l'espèce de rejeter sa requête selon la procédure prévue par l'article L. 522-3 du code de justice administrative, y compris les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de M. A...est rejetée. <br/>
Article 2 : La présente ordonnance sera notifiée à M. B...A...et au ministre d'Etat, ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
