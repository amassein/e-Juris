<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031938387</ID>
<ANCIEN_ID>JG_L_2016_01_000000387106</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/93/83/CETATEXT000031938387.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème SSJS, 22/01/2016, 387106, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-01-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>387106</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème SSJS</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP SPINOSI, SUREAU ; SCP ROCHETEAU, UZAN-SARANO</AVOCATS>
<RAPPORTEUR>M. Camille Pascal</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Béatrice Bourgeois-Machureau</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2016:387106.20160122</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
         M. A...C...a demandé au tribunal administratif de Grenoble, à titre principal, de dire et juger que le permis de construire délivré à M. B...le 13 novembre 2002 par le préfet de la Drôme en vue de l'édification de deux éoliennes sur le territoire de la commune de La Répara-Auriples avait été retiré et, à titre subsidiaire, d'annuler pour excès de pouvoir ce permis ainsi que les arrêtés des 12 mai 2003, 12 décembre 2007 et 3 janvier 2008 par lesquels le préfet a transféré ce permis de construire. Par un jugement n° 1005020 du 14 mai 2013, le tribunal administratif a rejeté ces demandes.<br/>
<br/>
         Par un arrêt n° 13LY01881 du 13 novembre 2014, la cour administrative d'appel de Lyon, sur appel de M.C..., d'une part, a annulé ce jugement en tant qu'il portait sur le permis tacite du 13 novembre 2002 mais, statuant par la voie de l'évocation, a rejeté les conclusions tendant à l'annulation de ce permis et, d'autre part, rejeté les conclusions d'appel relatives aux autres arrêtés.<br/>
<br/>
         Par un pourvoi sommaire et deux mémoires complémentaires, enregistrés les 13 janvier, 13 avril et 1er juillet 2015 au secrétariat du contentieux du Conseil d'Etat, M. C...demande au Conseil d'Etat : <br/>
<br/>
         1°) d'annuler les articles 2 et 3 de cet arrêt ; <br/>
<br/>
         2°) réglant l'affaire au fond, de faire droit à son appel ; <br/>
<br/>
         3°) de mettre à la charge de l'Etat et de la société Bellane Energie la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
         Vu les autres pièces du dossier ;<br/>
<br/>
         Vu : <br/>
         - la directive 85/337/CEE du Conseil du 27 juin 1985 modifiée par la directive 97/11/CE du Conseil du 3 mars 1997 ;<br/>
         - le code de l'environnement ;<br/>
         - le code de l'urbanisme ;<br/>
         - le décret  n° 77-629 du 12 octobre 1977 ;<br/>
         - le code de justice administrative ; <br/>
<br/>
<br/>
<br/>
<br/>
         Après avoir entendu en séance publique :<br/>
<br/>
         - le rapport de M. Camille Pascal, conseiller d'Etat,<br/>
<br/>
         - les conclusions de Mme Béatrice Bourgeois-Machureau, rapporteur public,<br/>
<br/>
         La parole ayant été donnée, avant et après les conclusions, à la SCP Spinosi, Sureau, avocat de M.C..., et à la SCP Rocheteau, Uzan-Sarano, avocat de la société Bellane Energie ; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1.	Considérant qu'il ressort des énonciations de l'arrêt attaqué que le préfet de la Drôme a délivré le 13 novembre 2002 à M. B...un permis de construire deux éoliennes sur le territoire de la commune de La Répara-Auriples ; que, par un arrêté du 12 mai 2003, le préfet a transféré ce permis à la société Albatros Energie ; que, cette société ayant présenté une demande en vue de la construction d'une seule éolienne sur un terrain inclus dans le terrain d'assiette de ce permis tacite, un nouveau permis de construire tacite est né le 23 octobre 2004 ; que, par deux arrêtés des 12 décembre 2007 et 3 janvier 2008, le préfet a transféré le permis du 13 novembre 2002 à la société Bellane Industrie, puis à la société Bellane Energie ; que M. C...a demandé au tribunal administratif de Grenoble d'annuler le permis de construire tacite du 13 novembre 2002, ainsi que les arrêtés de transfert des 12 mai 2003, 12 décembre 2007 et 3 janvier 2008 ; que, par un jugement du 14 mai 2013, le tribunal a jugé qu'il n'y avait pas lieu de statuer sur les conclusions dirigées contre les arrêtés de transfert des 12 mai 2003 et 12 décembre 2007 et que les conclusions dirigées contre le permis tacite du 13 novembre 2002 et l'arrêté de transfert du 3 janvier 2008 étaient tardives ; <br/>
<br/>
              2.	Considérant que la cour administrative d'appel de Lyon, par l'arrêt attaqué, a rejeté les conclusions d'appel de M. C...en tant qu'elles portaient sur la légalité des arrêtés de transfert du permis de construire des 12 mai 2003, 12 décembre 2007 et 3 janvier 2008 et en tant qu'elles demandaient à la juridiction administrative de " dire et juger " que le permis du 13 novembre 2002 avait été retiré et était devenu caduc ; que la cour, en revanche, a annulé le jugement du tribunal administratif de Grenoble en tant qu'il avait rejeté comme irrecevables les conclusions tendant à l'annulation du permis du 13 novembre 2002 mais, statuant sur ces conclusions par la voie de l'évocation, les a rejetées ; que M. C...se pourvoit en cassation contre l'arrêt rendu par la cour administrative d'appel, en ce que cet arrêt a rejeté les conclusions à fin d'annulation du permis de construire délivré le 13 novembre 2002 ; <br/>
<br/>
              3.	Considérant qu'il résulte des termes du 2 de l'article 6 de la directive du 27 juin 1985 modifiée par la directive du 3 mars 1997 que, lorsqu'un Etat membre décide de soumettre un projet à une évaluation de ses incidences sur l'environnement, il doit veiller à ce que la demande d'autorisation de ce projet et le contenu de l'étude d'impact soient mis à la disposition du public dans un délai raisonnable afin de lui permettre d'exprimer son avis " avant que l'autorisation ne soit délivrée " ; qu'à la date du permis de construire du 13 novembre 2002, ni le décret du 12 octobre 1977 ni aucune autre disposition législative ou réglementaire n'avaient transposé les objectifs poursuivis par l'article 6 de la directive, alors que cette transposition devait intervenir avant le 14 mars 1999 ; <br/>
<br/>
              4.	Considérant que la cour administrative d'appel a relevé que si aucune disposition législative ou réglementaire ne prévoyait, à la date du permis de construire contesté, qu'il devait être précédé d'une étude d'impact, une telle étude avait été en l'espèce réalisée à la demande de l'administration ; qu'après avoir jugé, conformément à ce qui vient d'être dit, que les objectifs de la directive du 27 juin 1985 impliquaient de mettre à la disposition du public toute évaluation faite de l'impact du projet sur l'environnement, la cour a estimé que l'absence de mise à disposition du public du document en cause n'avait privé le public d'aucune garantie, n'avait pas eu d'incidence sur le sens de la décision prise ou n'avait pas eu pour effet de nuire à l'information de l'ensemble des personnes intéressées ; que la cour ne pouvait, sans erreur de droit, juger que l'absence de mise à disposition du public de l'étude d'impact avant la délivrance du permis de construire, et alors qu'elle ne relevait aucune autre circonstance de fait à cet égard, n'avait pas nui à l'information de l'ensemble des personnes intéressées ; que, par suite, sans qu'il soit besoin d'examiner les autres moyens du pourvoi qui sont dirigés contre la même partie de l'arrêt, M. C...est fondé à demander l'annulation de l'arrêt qu'il attaque en tant qu'il a statué sur les conclusions tendant à l'annulation du permis de construire délivré le 13 novembre 2002 ; <br/>
<br/>
              5.	Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat et la société Bellane Energie chacun la somme de 2 000 euros à verser à M. C..., au titre des dispositions de l'article L. 761-1 du code de justice administrative ; que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de M. C...qui n'est pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'article 2 de l'arrêt de la cour administrative d'appel de Lyon en date du 13 novembre 2014 est annulé.<br/>
Article 2 : L'affaire est renvoyée dans cette mesure à la cour administrative d'appel de Lyon.<br/>
Article 3 : L'Etat et la société Bellane Energie verseront chacun la somme de 2 000 euros à M. C...en application des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à M. A...C..., à la société Bellane Energie et  à la ministre du logement, de l'égalité des territoires et de la ruralité.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
