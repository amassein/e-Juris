<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043726657</ID>
<ANCIEN_ID>JG_L_2021_06_000000435466</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/72/66/CETATEXT000043726657.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème - 2ème chambres réunies, 29/06/2021, 435466, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-06-29</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>435466</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème - 2ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GATINEAU, FATTACCINI, REBEYROL</AVOCATS>
<RAPPORTEUR>M. Thomas Pez-Lavergne</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Marc Pichon de Vendeuil</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:435466.20210629</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              1° Sous le n° 435466, par une requête sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 21 octobre 2019, 20 janvier 2020 et 1er février 2021 au secrétariat du contentieux du Conseil d'Etat, le syndicat CFE-CGC réseaux consulaires demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret n° 2019-867 du 21 août 2019 relatif aux modalités de traitement des agents publics refusant l'engagement proposé par le repreneur d'une activité exercée par leur chambre de commerce et d'industrie d'affectation ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 4 500 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              2° Sous le n° 435483, par une requête et un mémoire en réplique, enregistrés les 21 octobre 2019 et 12 janvier 2021 au secrétariat du contentieux du Conseil d'Etat, le syndicat national des chambres de commerce et de l'industrie - CFDT demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le même décret ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
              3° Sous le n° 435486, par une requête, enregistrée le 21 octobre 2019 au secrétariat du contentieux du Conseil d'Etat, le syndicat CGT du personnel des chambres de commerce et d'industrie de Paris et d'Ile-de-France demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le même décret ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu : <br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales et son premier protocole additionnel ;<br/>
              - le code de commerce ;<br/>
              - le code du travail ;<br/>
              - la loi n° 52-1311 du 10 décembre 1952 ;<br/>
              - la loi n° 2019-486 du 22 mai 2019 ;<br/>
              - l'arrêté du 25 juillet 1997 relatif au statut du personnel de l'assemblée des chambres françaises de commerce et d'industrie, des chambres régionales de commerce et d'industrie, des chambres de commerce et d'industrie et des groupements interconsulaires ;<br/>
              - le code de justice administrative ; <br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Thomas Pez-Lavergne, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Marc Pichon de Vendeuil, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Gatineau, Fattaccini, Rebeyrol, avocat du syndicat CFE-CGC réseaux consulaires ; <br/>
<br/>
              Vu la note en délibéré, enregistrée le 9 juin 2021, présentée sous le n° 435466 par le syndicat CFE-CGC réseaux consulaires ; <br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Les requêtes visées ci-dessus sont dirigées contre le même décret. Il y a lieu de les joindre pour statuer par une seule décision.<br/>
<br/>
              2. Aux termes de l'article L. 712-11-1 du code de commerce : " Sans préjudice des dispositions législatives particulières, lorsqu'une personne de droit privé ou de droit public reprend tout ou partie de l'activité d'une chambre de commerce et d'industrie, quelle que soit la qualification juridique de la transformation de ladite activité, elle propose aux agents de droit public employés par cette chambre pour l'exercice de cette activité un contrat de droit privé ou un engagement de droit public. / Le contrat de travail ou l'engagement proposé reprend les éléments essentiels du contrat ou de l'engagement dont l'agent de droit public est titulaire, en particulier ceux qui concernent la rémunération. Les services accomplis au sein de la chambre de commerce et d'industrie sont assimilés à des services accomplis au sein de la personne privée ou publique d'accueil. / En cas de refus de l'agent public d'accepter le contrat ou l'engagement, la chambre de commerce et d'industrie employeur applique, selon des modalités prévues par décret, les dispositions relatives à la rupture de la relation de travail prévues par le statut du personnel administratif des chambres de commerce et d'industrie mentionné à l'article 1er de la loi n° 52-1311 du 10 décembre 1952 relative à l'établissement obligatoire d'un statut du personnel administratif des chambres d'agriculture, des chambres de commerce et des chambres de métiers ".<br/>
<br/>
              3. Le décret attaqué du 21 août 2019, pris pour l'application de l'article L. 712-11-1 du code de commerce précité, a inséré dans ce code un article D. 712-11-2 relatif aux modalités de traitement des agents publics en cas de refus du nouveau contrat de travail de droit privé ou de l'engagement de droit public proposé par le repreneur de tout ou partie de l'activité auparavant exercée par leur chambre de commerce et d'industrie d'affectation.<br/>
<br/>
<br/>
<br/>
<br/>
              Sur la légalité externe du décret attaqué :<br/>
<br/>
              En ce qui concerne la compétence du Premier ministre :<br/>
<br/>
              4. Aux termes de l'article 1er de la loi du 10 décembre 1952 relative à l'établissement obligatoire d'un statut du personnel administratif des chambres d'agriculture, des chambres de commerce et des chambres de métiers : " La situation du personnel administratif des chambres d'agriculture, des chambres de commerce et des chambres de métiers de France est déterminée par un statut établi par des commissions paritaires nommées, pour chacune de ces institutions, par le ministre de tutelle ". Il résulte de ces dispositions que la commission paritaire chargée d'établir le statut du personnel des chambres de commerce et d'industrie est, en principe, seule compétente pour fixer les règles statutaires applicables aux personnels de ces établissements ayant la qualité d'agents de droit public, notamment celles relatives à leur licenciement.<br/>
<br/>
              5. Toutefois, il résulte des dispositions, citées au point 2, de l'article L. 712-11-1 du code de commerce, créé par la loi du 22 mai 2019 relative à la croissance et la transformation des entreprises, que le législateur a entendu déroger à la loi du 10 décembre 1952 en permettant au pouvoir réglementaire, en cas de refus d'un agent public employé par une chambre de commerce et d'industrie d'accepter le contrat ou l'engagement proposé par le repreneur de ses activités, de déterminer, par décret, les modalités selon lesquelles la chambre employeur applique les dispositions relatives à la rupture de la relation de travail prévues par le statut du personnel administratif des chambres de commerce et d'industrie. Dès lors, nonobstant la compétence de droit commun de la commission paritaire mentionnée au point 4, le Premier ministre était compétent pour définir la procédure de licenciement pour refus de transfert au repreneur des activités de la chambre de commerce et d'industrie en se fondant, tout en les adaptant, sur les dispositions relatives à la rupture des relations de travail en cas de refus de mutation géographique qui figurent à l'article 4 de l'annexe 5 à l'article 28 du statut.<br/>
<br/>
              En ce qui concerne la consultation du Conseil d'Etat :<br/>
<br/>
              6. Il résulte des dispositions citées au point 2 que l'article L. 712-11-1 du code de commerce renvoie à un décret simple l'adoption des mesures réglementaires qu'il prévoit. Aucune autre disposition n'impose l'adoption de ces mesures par décret en Conseil d'Etat. Dès lors, le syndicat CGT du personnel des chambres de commerce et d'industrie de Paris et d'Ile-de-France n'est pas fondé à soutenir que le Conseil d'Etat aurait dû être consulté préalablement à l'édiction du décret attaqué.<br/>
<br/>
              Sur la légalité interne du décret attaqué :<br/>
<br/>
              En ce qui concerne le mode de calcul de l'indemnité de rupture pour refus de transfert selon les règles applicables au licenciement pour refus de mutation géographique :<br/>
<br/>
              7. Ni l'article L. 712-11-1 du code de commerce ni aucune autre disposition n'imposaient au Premier ministre de rattacher le régime d'indemnisation des agents licenciés pour refus de transfert au régime d'indemnisation des licenciements pour suppression de poste prévu à l'article 35-2 du statut du personnel administratif des chambres de commerce. Dès lors, le Premier ministre n'a pas commis d'erreur de droit ni d'erreur d'appréciation en fixant le mode de calcul de l'indemnité de rupture du contrat de ces agents conformément au régime d'indemnisation des licenciements pour refus de mutation géographique, prévu à l'article 4 de l'annexe 5 à l'article 28 de ce statut, la mutation géographique au sein d'une chambre ayant en commun avec le transfert au repreneur des activités d'une chambre de ne pas entraîner la suppression du poste de l'agent concerné et de donner à cet agent la possibilité de poursuivre son activité professionnelle.<br/>
<br/>
              En ce qui concerne le principe général du droit relatif aux obligations de reclassement interne :<br/>
<br/>
              8. Il résulte des dispositions de l'article L. 712-11-1 du code de commerce citées au point 2 que le législateur a entendu instituer une procédure spécifique de transfert des agents de droit public des chambres de commerce et d'industrie en cas de reprise de tout ou partie de l'activité de la chambre qui les emploie par une personne de droit privé ou de droit public. Par suite, alors que le poste de l'agent transféré n'est pas supprimé mais transféré dans la nouvelle entité, le syndicat CFE-CGC réseaux consulaires ne peut utilement soutenir que le décret attaqué, qui a été pris pour l'application de ces dispositions législatives, méconnaîtrait le principe général du droit imposant à un employeur de rechercher à reclasser un agent de droit de public dont le poste est supprimé.<br/>
<br/>
              En ce qui concerne le droit au respect des biens :<br/>
<br/>
              9. La circonstance que l'indemnité de rupture prévue à l'article 4 de l'annexe 5 de l'article 28 du statut serait calculée de manière proportionnelle à l'ancienneté ne saurait la faire regarder comme n'assurant pas l'indemnisation de la perte du statut du personnel administratif des chambres de commerce et d'industrie par l'agent refusant son transfert au repreneur de tout ou partie de l'activité de la chambre de commerce et d'industrie qui l'emploie pour l'exercice de cette activité. Dès lors, le syndicat CFE-CGC réseaux consulaires n'est, en tout état de cause, pas fondé à soutenir que le décret attaqué serait incompatible avec les stipulations de l'article 1er du premier protocole additionnel à la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales en ce qu'il n'assurerait pas l'indemnisation intégrale du préjudice résultant de la perte du statut du personnel administratif des chambres de commerce et d'industrie.<br/>
<br/>
              En ce qui concerne le principe de sécurité juridique :<br/>
<br/>
              10. Selon le VI de l'article 40 de la loi du 22 mai 2019 relative à la croissance et la transformation des entreprises : " Les agents de droit public relevant du statut du personnel administratif des chambres de commerce et d'industrie établi sur le fondement de la loi n° 52-1311 du 10 décembre 1952 précitée peuvent demander que leur soit proposé par leur employeur un contrat de travail de droit privé dans un délai de douze mois suivant l'agrément de la convention collective mentionné au II du présent article. Les conditions dans lesquelles sont transférés les droits et les avantages des agents ayant opté pour un contrat de droit privé sont fixées par ladite convention collective. / Les agents mentionnés au premier alinéa du présent VI qui n'ont pas opté dans ce délai pour un contrat de droit privé, demeurent régis, pour leur situation particulière, par le statut du personnel administratif des chambres de commerce et d'industrie établi en application de la loi n° 52-1311 du 10 décembre 1952 précitée ".<br/>
<br/>
              11. Le syndicat CFE-CGC réseaux consulaires soutient que le décret attaqué, en ne prévoyant pas une entrée en vigueur différée, méconnaîtrait le principe de sécurité juridique en portant une atteinte excessive à la situation des agents des chambres de commerce et d'industrie en les empêchant d'exercer le droit d'option pour un contrat de droit privé que leur reconnaissent les dispositions du VI de l'article 40 de la loi du 22 mai 2019 pendant une durée d'un an après l'agrément de la convention collective des personnels des chambres de commerce et d'industrie. Ce décret se borne toutefois à faire application des dispositions rappelées au point 2 prévoyant la rupture des relations de travail entre la chambre de commerce et d'industrie et l'agent refusant l'engagement de droit public ou le contrat de droit privé proposé par le repreneur d'une activité exercée par sa chambre. Ainsi, en tout état de cause, en cas de reprise de l'activité par un tiers, l'agent ne peut continuer d'être employé par cette chambre et ne peut, en conséquence, bénéficier du dispositif prévu par le VI de l'article 40 de la loi du 22 mai 2019 précité et se voir proposer, par sa chambre, un contrat de travail de droit privé. Par suite, le syndicat CFE-CGC réseaux consulaires n'est pas fondé à soutenir que l'entrée en vigueur immédiate du décret attaqué priverait les agents publics des chambres de commerce et d'industrie de la possibilité de bénéficier du dispositif prévu par le VI de l'article 40 de la loi du 22 mai 2019 précité.<br/>
<br/>
              12. Il résulte de tout ce qui précède que les syndicats requérants ne sont pas fondés à demander l'annulation du décret qu'ils attaquent.<br/>
<br/>
              13. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Les requêtes du syndicat CFE-CGC réseaux consulaires, du syndicat national des chambres de commerce et de l'industrie - CFDT et du syndicat CGT du personnel des chambres de commerce et d'industrie de Paris et d'Ile-de-France sont rejetées.<br/>
Article 2 : La présente décision sera notifiée au syndicat CFE-CGC réseaux consulaires, au syndicat national des chambres de commerce et de l'industrie - CFDT, au syndicat CGT du personnel des chambres de commerce et d'industrie de Paris et d'Ile-de-France, au Premier ministre et au ministre de l'économie, des finances et de la relance. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
