<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044367645</ID>
<ANCIEN_ID>JG_L_2021_11_000000434892</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/36/76/CETATEXT000044367645.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème chambre, 24/11/2021, 434892, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-11-24</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>434892</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP THOUIN-PALAT, BOUCARD</AVOCATS>
<RAPPORTEUR>M. Edouard Solier</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Dieu</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:434892.20211124</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête sommaire, un mémoire complémentaire et un nouveau mémoire, enregistrés les 25 septembre et 26 décembre 2019 et le 11 mai 2020 au secrétariat du contentieux du Conseil d'Etat, M. D... B... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision du 15 juillet 2019 par laquelle le Conseil national de l'ordre des chirurgiens-dentistes a refusé de reconnaître le diplôme d'université d'esthétique buccale délivré par l'université Nice-Sophia-Antipolis au titre de l'année universitaire 2016-2017 ;<br/>
<br/>
              2°) d'enjoindre au Conseil national de l'ordre des chirurgiens-dentistes de reconnaître ce diplôme dans un délai d'un mois à compter de la notification de la décision du Conseil d'Etat ; <br/>
<br/>
              3°) de mettre à la charge du Conseil national de l'ordre des chirurgiens-dentistes la somme de 10 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de la santé publique ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Edouard Solier, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Frédéric Dieu, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Thouin-Palat, Boucard, avocat de M. B... ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 8 octobre 2021, présentée par M. B... ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
<br/>
              1. Aux termes de l'article R. 4127-216 du code de la santé publique : " Les seules indications que le chirurgien-dentiste est autorisé à mentionner sur ses imprimés professionnels, notamment ses feuilles d'ordonnances, notes d'honoraires et cartes professionnelles, sont : (...) / 3° Les titres et fonctions reconnus par le Conseil national de l'ordre (...) ". L'article R. 4127-218 du même code dispose quant à lui : " Les seules indications qu'un chirurgien-dentiste est autorisé à faire figurer sur une plaque professionnelle à la porte de son immeuble ou de son cabinet sont (...) les diplômes, titres ou fonctions reconnus par le Conseil national de l'ordre ". Ces dispositions ayant notamment pour finalité d'assurer la pertinence des informations portées à la connaissance des patients par les plaques et imprimés professionnels des praticiens, le Conseil national de l'ordre des chirurgiens-dentistes peut légalement refuser de reconnaître des diplômes qui ne présentent pas d'intérêt pour les soins délivrés par le praticien ou qui ne sont pas compatibles avec les dispositions du code de déontologie des chirurgiens-dentistes, au nombre desquelles figurent, notamment, celles de l'article R. 4127-215, qui dispose : " La profession dentaire ne doit pas être pratiquée comme un commerce (...) ". <br/>
<br/>
              2. Par la décision attaquée du 15 juillet 2019, prise sur le fondement des dispositions citées ci-dessus, le Conseil national de l'ordre des chirurgiens-dentistes a refusé de reconnaître le diplôme d'université d'esthétique buccale délivré par l'université Nice-Sophia-Antipolis au titre de l'année universitaire 2016-2017. Il ressort des pièces du dossier que cette décision a été prise au motif que l'absence de formation clinique, au regard des objectifs poursuivis, enlève l'intérêt qu'elle pourrait avoir dans la pratique quotidienne, tant pour ce dernier que pour le patient.<br/>
<br/>
              3. En premier lieu, il ressort des pièces du dossier que le diplôme dont M. B... a entendu obtenir la reconnaissance prévoyait un versant théorique de cent-quatre heures ainsi qu'un versant pratique de quarante heures enseigné sous forme de travaux dirigés, et faisait l'objet d'une validation par un examen écrit et la présentation de cas cliniques. Si son programme mentionnait des enseignements relatifs à des cas cliniques, il ne s'agissait pas pour autant, pour les professionnels en formation, d'une formation clinique, consistant en la mise en œuvre de points théoriques et pratiques, sous le contrôle et la supervision d'enseignants. Par suite, le moyen tiré de ce que la décision attaquée est entachée d'erreur de fait en ce qu'elle retient que la formation en esthétique buccale ne comporte pas de volet clinique doit être écarté. <br/>
<br/>
              4. En deuxième lieu, après avoir relevé que la formation préalable à la délivrance du diplôme d'université d'esthétique buccale par l'université Nice-Sophia-Antipolis au titre de l'année universitaire 2016-2017 ne comportait pas de volet clinique, le Conseil national de l'ordre des chirurgiens-dentistes n'a pas commis d'erreur d'appréciation ni fait une inexacte application des dispositions mentionnées au point 1 en refusant de reconnaître ce diplôme au motif que l'absence de volet clinique de cette formation enlevait l'intérêt qu'elle pourrait avoir dans la pratique quotidienne du praticien, tant de son point de vue que du point de vue du patient. La circonstance, invoquée par M. B..., que son expérience de praticien puisse valoir au titre de la formation clinique est sans incidence sur la nécessité, exigée par le Conseil national de l'ordre des chirurgiens-dentistes, de pratiquer les soins sur des patients sous la supervision d'enseignants dans le cadre du diplôme litigeux. <br/>
<br/>
              5. En dernier lieu, il ressort des pièces du dossier que le diplôme universitaire d'esthétique buccale délivré en 2014 par l'université de Nice-Sophia-Antipolis au titre de l'année universitaire 2013-2014, qui a fait l'objet d'une reconnaissance par le Conseil national de l'ordre des chirurgiens-dentistes, comportait, à la différence du diplôme universitaire d'esthétique buccale délivré par la même université au titre de l'année universitaire 2016-2017, une mise en pratique des savoirs acquis sur des patients au centre hospitalier universitaire. Par suite, le moyen tiré de ce que la décision attaquée méconnaîtrait le principe d'égalité doit, en tout état de cause, être écarté.<br/>
<br/>
              6. Il résulte de tout ce qui précède que la requête de M. B... tendant à l'annulation de la décision attaquée doit être rejetée, y compris ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de M. B... est rejetée.<br/>
Article 2 : La présente décision sera notifiée à M. D... B... et au Conseil national de l'ordre des chirurgiens-dentistes.<br/>
              Délibéré à l'issue de la séance du 8 octobre 2021 où siégeaient : Mme Maud Vialettes, présidente de chambre, présidant ; Mme Fabienne Lambolez, conseillère d'Etat et M. Edouard Solier, maître des requêtes-rapporteur. <br/>
<br/>
<br/>
              Rendu le 24 novembre 2021.<br/>
                 La présidente : <br/>
                 Signé : Mme Maud Vialettes<br/>
<br/>
<br/>
 		Le rapporteur : <br/>
      Signé : M. Edouard Solier<br/>
<br/>
<br/>
                 La secrétaire :<br/>
                 Signé : Mme A... C...<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
