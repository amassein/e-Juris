<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031128761</ID>
<ANCIEN_ID>JG_L_2015_07_000000391454</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/12/87/CETATEXT000031128761.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 09/07/2015, 391454, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-07-09</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>391454</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>HAAS</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2015:391454.20150709</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>M. Alaa B...a demandé au juge des référés du tribunal administratif de Nantes, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, d'enjoindre au ministre de l'intérieur de lui délivrer le visa d'entrée et de court séjour qu'il a demandé ou, à défaut, de réexaminer sa demande dans un délai de 24 heures suivant la décision à intervenir. Par une ordonnance n° 1505201 du 24 juin 2015, le juge des référés du tribunal administratif de Nantes a rejeté sa demande. <br/>
              Par une requête et un mémoire de production, enregistrés les 2 et 6 juillet 2015 au secrétariat du contentieux du Conseil d'Etat, M. B...demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative :<br/>
<br/>
              1°) d'annuler cette ordonnance ; <br/>
<br/>
              2°) de faire droit à sa demande de première instance et d'enjoindre au ministre de l'intérieur de réexaminer sa demande de visa après saisine de l'Office français de protection des réfugiés et apatrides (OFPRA) pour avis ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Il soutient que : <br/>
              - la condition d'urgence est remplie, dès lors qu'il vit seul au Liban dans des conditions précaires et difficiles et qu'en cas de refoulement en Syrie, il peut être appelé au service militaire à tout moment ;<br/>
              - en refusant le visa sollicité, le consul général de France à Beyrouth a porté une atteinte manifestement illégale à son droit constitutionnel d'asile, au droit à la vie ainsi qu'au droit de ne pas subir la torture ou des traitements humains et dégradants ;<br/>
              - la circonstance qu'il n'ait pas motivé sa demande initiale de visa au titre de l'asile est sans incidence dès lors que la décision de rejet de la commission se substitue à la décision initiale du consul général de France à Beyrouth ;<br/>
              - il convient de tenir compte de l'existence des membres de sa famille résidant en France.<br/>
              Par un mémoire en défense et un nouveau mémoire, enregistrés le 6 juillet 2015, le ministre de l'intérieur conclut au rejet de la requête. <br/>
<br/>
              Il soutient que :<br/>
              - la condition d'urgence n'est pas remplie dès lors que le requérant, qui ne réside plus en Syrie, a tardé pour introduire son référé et n'établit pas que ses craintes sont avérées ;<br/>
              - le refus de délivrance d'un visa d'entrée sur le territoire français ne porte pas une atteinte grave et manifestement illégale à une liberté fondamentale ; en effet, le droit constitutionnel d'asile n'implique pas un droit à la délivrance d'un visa d'entrée en France pour y solliciter l'asile.<br/>
              Par une intervention, enregistrée le 3 juillet 2015, la Cimade demande que le Conseil d'Etat fasse droit aux conclusions de M.B....<br/>
<br/>
<br/>
              Vu :<br/>
              - la convention de Genève du 28 juillet 1951 ;<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, M. B... et la Cimade et, d'autre part, le ministre de l'intérieur ;<br/>
<br/>
              Vu le procès-verbal de l'audience publique du 7 mai 2015 à 9 heures 30 au cours de laquelle ont été entendus : <br/>
<br/>
              - Me Haas, avocat au Conseil d'Etat et à la Cour de Cassation, avocat de M. B... ;<br/>
<br/>
              - le représentant de la Cimade ;<br/>
<br/>
- les représentants du ministre de l'intérieur ;<br/>
<br/>
              et à l'issue de laquelle le juge des référés a clos l'instruction ; <br/>
<br/>
<br/>
<br/>
              1. Considérant que la Cimade justifie d'un intérêt suffisant pour intervenir au soutien de la requête ; que son intervention est, par suite, recevable ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. " ; que le requérant qui saisit le juge des référés sur le fondement de ces dispositions doit justifier de circonstances particulières caractérisant la nécessité pour lui de bénéficier à très bref délai d'une mesure visant à sauvegarder une liberté fondamentale ;<br/>
<br/>
              3. Considérant qu'il résulte de l'instruction que, le 29 décembre 2014, M. A...B..., de nationalité syrienne, a sollicité auprès du consulat général de France à Beyrouth, un visa de court séjour afin de rendre visite aux membres de sa famille qui résident en France ; qu'après s'être vu opposer un refus, il a saisi la commission de recours contre les décisions de refus de visa d'entrée en France en invoquant, pour la première fois au soutien de sa demande, son intention de déposer une demande d'asile en France ; que, par une décision du 29 mai 2015, la commission a rejeté son recours ; que, s'estimant toujours saisie d'une demande de visa de court séjour, elle a fondé sa décision sur le risque de détournement de l'objet du visa à des fins migratoires ; que l'intéressé a saisi le juge des référés du tribunal administratif de Nantes, sur le fondement de l'article L. 521-2 du code de justice administrative, de conclusions tendant à la délivrance du visa sollicité ou, à défaut, au réexamen de sa demande ; qu'il relève appel de l'ordonnance du 24 juin 2015 par laquelle le juge des référés du tribunal administratif de Nantes a rejeté sa demande pour défaut d'urgence ;<br/>
<br/>
              4. Considérant que, sauf circonstances particulières, le refus des autorités consulaires de délivrer un visa d'entrée en France ne constitue pas une situation d'urgence caractérisée rendant nécessaire l'intervention dans les quarante-huit heures du juge des référés ; que, s'il est vrai que M.B..., qui est âgé de 19 ans, vit seul, sans ressources personnelles ni logement et que plusieurs membres de sa famille résident en France en qualité de réfugié, il ne résulte toutefois pas de l'instruction que les conditions dans lesquelles il réside actuellement au Liban où il se trouve placé sous la protection du Haut Commissariat aux réfugiés seraient constitutives d'une situation d'urgence caractérisée de nature à justifier l'intervention du juge des référés dans les conditions d'urgence particulière prévues par l'article L. 521-2 du code de justice administrative ; qu'au demeurant, rien ne fait obstacle à ce que l'intéressé, s'il s'y croit fondé, saisisse le consul général de France à Beyrouth d'une nouvelle demande de visa fondée sur son intention de déposer une demande d'asile en France ; <br/>
<br/>
              5. Considérant qu'il résulte de ce qui précède que M. B...n'est pas fondé à soutenir que c'est à tort que, par l'ordonnance attaquée, le juge des référés du tribunal administratif de Nantes a rejeté sa demande ; que les conclusions qu'il a présentées au titre de l'article L. 761-1 du code de justice administrative ne peuvent, par suite, qu'être rejetées ;<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : L'intervention de la Cimade est admise. <br/>
Article 2 : La requête de M. B...est rejetée.<br/>
Article 3 : La présente ordonnance sera notifiée à M. A...B..., à la Cimade et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
