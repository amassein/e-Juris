<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000020288735</ID>
<ANCIEN_ID>JG_L_2009_02_000000306511</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/20/28/87/CETATEXT000020288735.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère sous-section jugeant seule, 18/02/2009, 306511, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2009-02-18</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>306511</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère sous-section jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Arrighi de Casanova</PRESIDENT>
<AVOCATS>LE PRADO</AVOCATS>
<RAPPORTEUR>Mme Laure  Bédier</RAPPORTEUR>
<COMMISSAIRE_GVT>Mlle Courrèges Anne</COMMISSAIRE_GVT>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête sommaire et le mémoire complémentaire, enregistrés les 13 juin et 13 septembre 2007 au secrétariat du contentieux du Conseil d'Etat, présentés pour la SOCIETE COLAS, dont le siège est 7, place René Clair à Boulogne-Billancourt (92653) ; la SOCIETE COLAS demande au Conseil d'Etat d'annuler pour excès de pouvoir le décret n° 2007-546 du 11 avril 2007 relatif aux droits des cotisants et au recouvrement des cotisations et contributions sociales et modifiant le code de la sécurité sociale en tant que, par son article 4, il introduit dans ce code un article R. 243-59-2 instituant une méthode de vérification par échantillonnage et extrapolation ;<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de la sécurité sociale ;<br/>
<br/>
              Vu le code du travail ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Laure Bédier, Maître des Requêtes,  <br/>
<br/>
              - les observations de Me Le Prado, avocat de la SOCIETE COLAS, <br/>
<br/>
              - les conclusions de Mlle Anne Courrèges, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>Sur la légalité externe du décret attaqué :<br/>
<br/>
              Considérant, en premier lieu, que le moyen tiré de l'irrégularité de la composition des conseils d'administration de l'agence centrale des organismes de sécurité sociale et de la caisse nationale du régime social des indépendants n'est pas assorti de précisions suffisantes pour permettre au Conseil d'Etat d'en apprécier le bien-fondé ; <br/>
<br/>
              Considérant, en second lieu, que si la procédure de contrôle instaurée par l'article R. 243-59-2 du code de la sécurité sociale introduit dans ce code par le décret attaqué, peut conduire à une régularisation des cotisations d'assurance-chômage, il ne résulte toutefois ni de ce décret, ni d'aucune autre disposition législative ou réglementaire que son exécution nécessite l'intervention de mesures réglementaires ou individuelles que le ministre de l'emploi, de la cohésion sociale et du logement aurait compétence pour signer ; que l'absence de contreseing du ministre délégué à l'emploi, au travail et à l'insertion professionnelle des jeunes ne saurait en tout état de cause affecter la régularité de ce décret ; que la société requérante n'est dès lors pas fondée à soutenir que, faute d'avoir été contresigné par ce ministre et ce ministre délégué, le décret attaqué a été pris en méconnaissance de l'article 22 de la Constitution aux termes duquel : « Les actes du Premier ministre sont contresignés, le cas échéant, par les ministres chargés de leur exécution » ;<br/>
<br/>
              Sur la légalité interne de la procédure de contrôle par échantillonnage et extrapolation introduite par le décret attaqué :<br/>
<br/>
              Considérant que l'article R. 243-59-2 du code de la sécurité sociale, dans sa rédaction issue du II de l'article 4 du décret attaqué, prévoit que les inspecteurs du recouvrement des cotisations sociales peuvent, lorsqu'ils effectuent un contrôle, proposer à l'employeur d'utiliser une méthode de vérification par échantillonnage et extrapolation ; que l'employeur peut s'opposer, par écrit, au recours à cette méthode, dans les quinze jours suivant la remise par l'inspecteur du recouvrement d'un document l'informant du contenu et des différentes étapes de cette procédure ;<br/>
<br/>
              Considérant, en premier lieu, que l'article L. 242-1 du code de la sécurité sociale définit les sommes versées aux travailleurs sur la base desquelles sont calculées les cotisations dues au titre des assurances sociales, des accidents du travail et des allocations familiales ; que l'article L. 243-7 du même code confie aux organismes chargés du recouvrement des cotisations du régime général le contrôle de l'application par les employeurs de la législation relative aux cotisations et contributions sociales ; que ces articles, non plus qu'aucune autre disposition législative ni aucun principe, n'ont pas pour objet ni pour effet d'exclure le recours à une procédure de contrôle et de redressement par échantillonnage et extrapolation, sur la base de données fournies par l'employeur contrôlé, telle que celle introduite par le nouvel article R. 243-59-2 du code de la sécurité sociale ; que, contrairement à ce que soutient la société requérante, cette procédure de contrôle n'a pas le caractère d'une taxation forfaitaire ;<br/>
<br/>
              Considérant, en deuxième lieu, qu'en application de ce même article R. 243-59-2, à l'issue de la procédure de contrôle, l'inspecteur du recouvrement remet à l'employeur un document dans lequel il précise notamment les populations faisant l'objet des vérifications, les critères retenus pour procéder au tirage des échantillons, leur contenu, les cas atypiques qui en ont été exclus, les résultats obtenus pour chacun des échantillons, la méthode d'extrapolation appliquée et les résultats ainsi obtenus ; que l'employeur peut ensuite procéder lui-même au calcul des sommes supplémentaires dont il est redevable ou au contraire de celles qu'il a indûment versées pour la totalité des salariés concernés par chacune des anomalies constatées sur chacun des échantillons utilisés ; qu'à défaut, l'organisme de recouvrement y procède ; <br/>
<br/>
              Considérant que, les cotisations et contributions sociales dues au titre des rémunérations versées aux salariés étant établies et versées selon un régime de déclaration et de précompte par l'employeur, il incombe à celui-ci, qui dispose ainsi d'éléments suffisants pour déterminer la nature et le montant des redressements à opérer, de tirer les conséquences, pour chacun des salariés concernés, du contrôle par échantillonnage et extrapolation dont les résultats lui ont été notifiés ; que, par suite, la société requérante n'est pas fondée à soutenir que les dispositions attaquées sont illégales faute de précisions relatives à la régularisation du calcul des cotisations sociales de chaque salarié ;<br/>
<br/>
              Considérant, en troisième lieu, qu'il résulte des dispositions du même article R. 243-59-2 que l'inspecteur du recouvrement peut proposer à tout employeur d'utiliser la procédure de vérification par échantillonnage et extrapolation ; qu'ainsi qu'il a été dit ci-dessus, le recours à cette méthode ne fait pas obstacle à une régularisation de la situation de chaque salarié à la suite du contrôle ; que, par suite, le moyen tiré de la rupture du principe d'égalité entre entreprises et entre salariés qui résulterait de la mise en oeuvre de cette méthode de contrôle doit être écarté ; <br/>
<br/>
              Considérant, en quatrième lieu, que l'employeur peut se faire assister du conseil de son choix ; que l'inspecteur du recouvrement est tenu, en application de l'article R. 243-59-2, d'informer l'employeur des critères utilisés pour définir l'échantillonnage et de la méthode d'extrapolation choisie lorsque cette procédure de contrôle est acceptée par l'employeur ; que celui-ci peut, tout au long de la vérification, faire part de ses observations et désaccords à l'inspecteur du recouvrement, qui doit y répondre ; que, contrairement à ce que soutient la société requérante, l'inspecteur du recouvrement est tenu de prendre en compte l'opposition de l'employeur au recours à la procédure de contrôle par échantillonnage ou extrapolation ; qu'en pareil cas, il indique à l'employeur le lieu dans lequel les éléments nécessaires au contrôle exhaustif sur pièces doivent être remis ainsi que les critères de classement de ces pièces ; qu'à l'issue d'un délai de quinze jours au cours duquel l'employeur peut faire valoir ses observations, l'inspecteur du recouvrement lui notifie le lieu et les critères retenus ; que la mise à disposition des documents nécessaires au contrôle doit se faire dans un délai déterminé d'un commun accord entre l'inspecteur et l'employeur, qui ne peut toutefois être supérieur à soixante jours ; que ce n'est que lorsque l'employeur refuse de présenter les documents nécessaires au contrôle dans le lieu et en vertu des critères ainsi définis que l'inspecteur du recouvrement peut passer outre à son opposition au recours à la méthode de vérification par échantillonnage ou extrapolation ; qu'ainsi, compte tenu de ces garanties de procédure, et eu égard à l'intérêt général qui s'attache à ce qu'un contrôle puisse être en toute hypothèse réalisé, la société requérante n'est pas fondée à soutenir que le principe de sécurité juridique et le caractère contradictoire de la procédure de contrôle seraient méconnus par les dispositions attaquées, ni que la possibilité d'opposition à la méthode par échantillonnage et extrapolation serait dépourvue d'effet utile en raison de ses conséquences pour l'employeur ;<br/>
<br/>
              Considérant, enfin, que les redressements opérés en matière de recouvrement des cotisations sociales peuvent être contestés, quelle que soit la méthode de contrôle utilisée, devant le juge compétent pour régler les différends auxquels donne lieu l'application des législations et réglementations de sécurité sociale ; que, dès lors, le moyen tiré de la méconnaissance du principe de l'accès au juge ne peut qu'être écarté ; <br/>
<br/>
              Considérant qu'il résulte de tout ce qui précède que la requête de la SOCIETE COLAS doit être rejetée, y compris ses conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de la SOCIETE COLAS est rejetée. <br/>
Article 2 : La présente décision sera notifiée à la SOCIETE COLAS, au Premier ministre et à la ministre de la santé et des sports.<br/>
<br/>
</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
