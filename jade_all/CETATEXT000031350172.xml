<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031350172</ID>
<ANCIEN_ID>JG_L_2015_10_000000392748</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/35/01/CETATEXT000031350172.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème SSJS, 21/10/2015, 392748, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-10-21</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>392748</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème SSJS</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP PIWNICA, MOLINIE ; SCP GATINEAU, FATTACCINI</AVOCATS>
<RAPPORTEUR>Mme Charline Nicolas</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Gilles Pellissier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2015:392748.20151021</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              Le département des Hauts-de-Seine a demandé au juge des référés du tribunal administratif de Cergy-Pontoise, sur le fondement de l'article L. 521-3 du code de justice administrative, d'enjoindre à la société Sequalum, en premier lieu, de lui communiquer l'ensemble des documents, données et informations non encore fournis visés dans la mise en demeure du 11 mai 2015, la lettre du 10 juin 2015 et le tableau figurant à l'annexe au procès-verbal du comité de suivi du 19 juin 2015 ainsi que l'ensemble des dossiers d'ouvrages exécutés des SR01 et des comptes-rendus de mise à disposition et tout autre document, information ou donnée nécessaire à l'exploitation du service public portant sur l'établissement et l'exploitation d'un réseau de communications électroniques à très haut débit sur le territoire départemental, dans un délai de cinq jours ouvrés sous astreinte de 500 euros par jour de retard, et, en second lieu, de lui restituer, dans le même délai et sous la même astreinte, les badges, clés et procédures permettant l'exploitation et la maintenance de ce réseau.<br/>
<br/>
              Par une ordonnance n° 1505656 du 3 août 2015, le juge des référés du tribunal administratif de Cergy-Pontoise a, en premier lieu, enjoint à la société Sequalum de répondre, dans un délai de quinze jours à compter de la notification de l'ordonnance, aux mesures demandées par le département des Hauts-de-Seine référencés sous les n°s 1, 10, 14, 15, 17, 19, 24, 30, 33, 36, 38, 39, 41, 42, 45, sous astreinte de 500 euros par jour de retard et, en second lieu, rejeté le surplus de ses conclusions.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 18 août, 1er et 24 septembre 2015 au secrétariat du contentieux du Conseil d'Etat, le département des Hauts-de-Seine demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance en tant qu'elle a rejeté le surplus de sa demande ;<br/>
<br/>
              2°) statuant en référé, de faire droit dans cette mesure à sa demande ;<br/>
<br/>
              3°) de mettre à la charge de la société Sequalum le versement d'une somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code général des collectivités territoriales ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Charline Nicolas, auditeur,  <br/>
<br/>
              - les conclusions de M. Gilles Pellissier, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Piwnica, Molinié, avocat du département des Hauts-de-Seine, et à la SCP Gatineau, Fattaccini, avocat de la société Sequalum ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 12 octobre 2015, présentée par la société Sequalum ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article L. 521-3 du code de justice administrative : " En cas d'urgence et sur simple requête qui sera recevable même en l'absence de décision administrative préalable, le juge des référés peut ordonner toutes mesures utiles sans faire obstacle à l'exécution d'aucune mesure administrative " ;<br/>
<br/>
              2. Considérant qu'il ressort des pièces du dossier soumis au juge des référés que le département des Hauts-de-Seine a demandé au juge des référés du tribunal administratif de Cergy-Pontoise d'enjoindre à la société Sequalum, en premier lieu, de lui communiquer l'ensemble des documents, données et informations non encore fournis et nécessaires à l'exploitation du service public portant sur l'établissement et l'exploitation d'un réseau de communications électroniques à très haut débit sur le territoire départemental et, en second lieu, de lui délivrer les badges, clés et procédures permettant l'exploitation et la maintenance de ce réseau ; que le juge des référés a fait droit à la première de ces demandes et, dans le dispositif de l'ordonnance attaquée, rejeté le surplus de la demande du département des Hauts-de-Seine ; que, toutefois, le juge des référés, en ne se prononçant pas, dans les motifs de cette ordonnance, sur le bien-fondé de la seconde demande du département tendant à ce qu'il soit enjoint à la société Sequalum de lui délivrer les badges, clés et procédures permettant l'exploitation et la maintenance de ce réseau, a entaché son ordonnance d'une insuffisance de motivation ; que, par suite et sans qu'il soit besoin d'examiner les autres moyens de son pourvoi, le département des Hauts-de-Seine est fondé à demander l'annulation de l'ordonnance attaquée en tant qu'elle a, à son article 2, rejeté le surplus de ses conclusions ;<br/>
<br/>
              3. Considérant que dans les circonstances de l'espèce, il y a lieu, en application de l'article L. 821-2 du code de justice administrative, de régler l'affaire au titre de la procédure de référé engagée par le département des Hauts-de-Seine ;<br/>
<br/>
              4. Considérant, d'une part, que la condition d'urgence est remplie, dès lors que le département des Hauts-de-Seine a repris en régie l'exploitation du réseau très haut débit Seine à compter du 1er juillet 2015 et doit assurer la continuité et le bon fonctionnement du service public ; <br/>
<br/>
              5. Considérant, d'autre part, que, le département, compte tenu des éléments remis par la société Sequalum en cours d'instance, a précisé le dernier état de sa demande dans un mémoire enregistré au greffe du tribunal administratif le 30 juillet 2015 ; que devant le Conseil d'Etat, la société Sequalum conteste la teneur de la demande précédente mais ne discute pas le contenu de ce mémoire du 30 juillet 2015 ; que dans ces conditions, eu égard à l'utilité des mesures sollicitées pour la continuité du fonctionnement du service public dans le dernier état de la demande du département, il y a lieu d'enjoindre à la société Sequalum de lui délivrer soixante-deux badges supplémentaires permettant l'accès à tous les noeuds de raccordements optiques (NRO), deux jeux de clés pour tous les NRO pour lesquels la société Sequalum n'est pas le titulaire direct du droit d'occupation, les cartes permettant de reproduire sans autorisation préalable les clefs des NRO fournies, deux jeux de clés pour chacun des sous-répartiteurs optiques de niveau 1 (SRO1) ou, en cas de clé unique, un jeu de dix clés accompagné des cartes permettant de les reproduire sans autorisation préalable, un double des clés des armoires hébergeant les boîtiers des sous-répartiteurs optiques de niveau 2 (SRO2) et vingt clés supplémentaires pour l'ouverture des boîtes à clés de chaque site lorsque cela est nécessaire à l'accès au NRO, SRO1 et SRO2 ; qu'il y a lieu également d'enjoindre à la société de restaurer le fonctionnement de tous les badges dont dispose le département sur les NRO puis de déconnecter le système de contrôle à distance par Sequalum ; qu'il y a lieu d'assortir cette injonction d'une astreinte de 500 euros par jour de retard à compter de l'expiration d'un délai de quinze jours à compter de la notification de la présente décision ;<br/>
<br/>
              6. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mise à la charge du département des Hauts-de-Seine, qui n'est pas, dans la présente instance, la partie perdante, la somme que demande la société Sequalum au titre des frais exposés par elle et non compris dans les dépens ; qu'en revanche, il y a lieu, dans les circonstances de l'espèce, de mettre une somme de 3 000 euros à la charge de cette dernière au titre des frais exposés par le département des Hauts-de-Seine ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'article 2 de l'ordonnance du juge des référés du tribunal administratif de Cergy-Pontoise du 3 août 2015 est annulé.<br/>
Article 2 : Il est enjoint à la société Sequalum de mettre en oeuvre les mesures indiquées au point 5 de la présente décision, ce, dans un délai de quinze jours à compter de la notification de cette décision et sous une astreinte de 500 euros par jour de retard.<br/>
Article 3 : La société Sequalum versera au département des Hauts-de-Seine une somme de 3 000 euros en application des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Les conclusions présentées par la société Sequalum en application des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée au département des Hauts-de-Seine et à la société Sequalum.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
