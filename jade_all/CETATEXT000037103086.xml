<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037103086</ID>
<ANCIEN_ID>JG_L_2018_06_000000415698</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/10/30/CETATEXT000037103086.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème - 3ème chambres réunies, 25/06/2018, 415698, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-06-25</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>415698</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème - 3ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MARLANGE, DE LA BURGADE</AVOCATS>
<RAPPORTEUR>M. Alexandre  Koutchouk</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Benoît Bohnert</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:415698.20180625</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société anonyme à responsabilité limitée (SARL) Zara France a demandé au tribunal administratif de Lyon de lui accorder la restitution de la taxe sur les surfaces commerciales qu'elle a acquittée au titre des années 2010 à 2012, à raison de son établissement situé au n° 71 rue de la République à Lyon (Rhône). Par un jugement n° 1303607 du 26 janvier 2016, ce tribunal a rejeté sa demande. <br/>
<br/>
              Par un arrêt n° 16LY01038 du 28 septembre 2017, la cour administrative d'appel de Lyon, après  avoir transmis au Conseil d'État, en application de l'article R. 351-2 du code de justice administrative, le pourvoi enregistré le 23 mai 2016 au greffe de cette cour, présenté par la société Zara France en ce qui concerne les années 2011 et 2012, prononcé la restitution de la taxe sur les surfaces commerciales due au titre de l'année 2010 à concurrence d'une somme correspondant à une surface de 468 m² et à un chiffre d'affaires proportionnel à cette surface.<br/>
<br/>
              Par un pourvoi et un mémoire en réplique, enregistrés le 15 novembre 2017 et le 17 avril 2018 au secrétariat du contentieux du Conseil d'Etat, le ministre de l'action et des comptes publics demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt en tant qu'il statue sur la taxe due au titre de l'année 2010 ; <br/>
<br/>
              2°) réglant l'affaire au fond, dans cette mesure, de rejeter l'appel formé par la société.  <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code général des impôts et le livre des procédures fiscales ; <br/>
              - la loi n° 72-657 du 13 juillet 1972 ;<br/>
              - le décret n° 95-85 du 26 janvier 1995 ;<br/>
              - le code de justice administrative ;		<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Alexandre Koutchouk, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Benoît Bohnert, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Marlange, de la Burgade, avocat de la société Zara France.<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que la société Zara France, qui exploite un commerce de détail situé aux n°s 71 et 73, rue de la République à Lyon, a demandé la restitution des cotisations de taxe sur les surfaces commerciales qu'elle a acquittées au titre des années 2010 à 2012. Le ministre de l'action et des comptes publics se pourvoit en cassation contre l'arrêt du 28 septembre 2017 par lequel la cour administrative d'appel de Lyon, après avoir renvoyé au Conseil d'Etat, le pourvoi formé par la société contre le jugement du tribunal administratif de Lyon du 26 janvier 2016 en tant qu'il statuait sur les impositions dues au titre des années 2011 et 2012, a partiellement fait droit à son appel dirigé contre ce même jugement, en tant qu'il avait rejeté sa demande en décharge des impositions dues au titre de 2010. <br/>
<br/>
                2. Aux termes de l'article 3 de la loi du 13 juillet 1972 instituant des mesures en faveur de certaines catégories de commerçants et artisans âgés, dans sa rédaction applicable aux années d'imposition en litige : " Il est institué une taxe sur les surfaces commerciales assise sur la surface de vente des magasins de commerce de détail, dès lors qu'elle dépasse quatre cents mètres carrés des établissements ouverts à partir du 1er janvier 1960 quelle que soit la forme juridique de l'entreprise qui les exploite. (...) La surface de vente des magasins de commerce de détail, prise en compte pour le calcul de la taxe (...) s'entendent des espaces affectés à la circulation de la clientèle pour effectuer ses achats, de ceux affectés à l'exposition des marchandises proposées à la vente, à leur paiement et de ceux affectés à la circulation du personnel pour présenter les marchandises à la vente (...) ". Il résulte de ces dispositions que la taxe sur les surfaces commerciales n'est pas due au titre d'un établissement lorsqu'une activité de commerce de détail y était exercée antérieurement au 1er janvier 1960 et a continué à y être exercé depuis de façon continue dans l'ensemble des surfaces de cet établissement. <br/>
<br/>
              3. Pour statuer sur la demande en décharge de la société Zara France au titre de l'année 2010, la cour administrative d'appel de Lyon, après avoir estimé que cette société établissait l'existence et le caractère continu d'une activité de commerce de détail dans la partie de son établissement correspondant aux locaux situés au n° 71 de la rue de la République à Lyon depuis une date antérieure au 1er janvier 1960 mais qu'elle n'apportait, en revanche, pas cette même preuve en ce qui concerne la partie de son établissement correspondant aux locaux situés au n° 73 de la même rue, en a déduit qu'il y avait lieu de prononcer une décharge partielle de la taxe en litige, à concurrence de la surface commerciale correspondant au local situé au n° 71 et d'un chiffre d'affaires proportionnel à cette surface. Il résulte toutefois de ce qui a été dit au point 2 que la cour, après avoir souverainement constaté que la société exerçait son activité au sein d'un unique établissement, ne pouvait procéder, sans commettre d'erreur de droit, alors même que cet établissement était situé sur deux parcelles correspondant à deux adresses différentes, à une appréciation adresse par adresse du respect de la condition tenant à l'existence d'une activité continue de commerce de détail depuis une date antérieure au 1er janvier 1960, laquelle doit être satisfaite sur l'ensemble des surfaces de l'établissement, pour en déduire un droit à décharge partielle de la taxe. Il s'ensuit que le ministre est fondé à demander l'annulation des articles 2 à 4 de l'arrêt qu'il attaque. <br/>
<br/>
              4. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que l'Etat, qui n'est pas la partie perdante dans la présente instance, verse à la société Zara France la somme qu'elle demande à ce titre.  <br/>
<br/>
<br/>
<br/>                             D E C I D E :<br/>
                                           --------------<br/>
<br/>
<br/>
Article 1er : Les articles 2 à 4 de l'arrêt de la cour administrative d'appel de Lyon du 28 septembre 2017 sont annulés. <br/>
<br/>
Article 2 : L'affaire est renvoyée, dans cette mesure, à la cour administrative d'appel de Lyon. <br/>
<br/>
Article 3 : Les conclusions de la société Zara France présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
<br/>
Article 4 : La présente décision sera notifiée au ministre de l'action et des comptes publics et à la société anonyme à responsabilité limitée Zara France.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
