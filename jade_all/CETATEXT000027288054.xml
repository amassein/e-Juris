<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027288054</ID>
<ANCIEN_ID>JG_L_2013_04_000000351735</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/28/80/CETATEXT000027288054.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème et 7ème sous-sections réunies, 08/04/2013, 351735</TITRE>
<DATE_DEC>2013-04-08</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>351735</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème et 7ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ODENT, POULET</AVOCATS>
<RAPPORTEUR>M. Yves Doutriaux</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Damien Botteghi</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:351735.20130408</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi, enregistré le 9 août 2011 au secrétariat du contentieux du Conseil d'Etat, présenté pour la Fédération française des sports de glace (FFSG), dont le siège est au 6 avenue du professeur André Lemierre à Paris (75980 cedex 20) ; la Fédération française des sports de glace demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler l'arrêt n° 10LY00451 du 9 juin 2011 par lequel la cour administrative d'appel de Lyon a rejeté sa requête tendant à l'annulation du jugement n° 0804801 du 22 janvier 2010 par lequel le tribunal administratif de Grenoble a annulé la décision du 29 septembre 2008 de son président refusant à M. B...l'autorisation de quitter l'équipe de France de bobsleigh pour pouvoir participer aux compétitions sportives internationales au sein de l'équipe présentée par une autre fédération nationale ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ; <br/>
<br/>
              3°) de mettre à la charge de M. B...la somme de 3 000 euros en application des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code du sport ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Yves Doutriaux, Conseiller d'Etat,<br/>
<br/>
              - les observations de la SCP Odent, Poulet, avocat de la Fédération française des sports de glace,<br/>
<br/>
              - les conclusions de M. Damien Botteghi, rapporteur public,<br/>
<br/>
              La parole ayant à nouveau été donnée à la SCP Odent, Poulet, avocat de la Fédération française des sports de glace ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1.	Considérant que les décisions prises par une personne privée chargée de l'exécution d'une mission de service public à caractère administratif présentent le caractère d'actes administratifs si elles procèdent de la mise en oeuvre de prérogatives de puissance publique conférées à cette personne pour l'accomplissement de la mission de service public qui lui a été confiée ; <br/>
<br/>
              2.	Considérant qu'aux termes du premier alinéa de l'article L. 131-14 du code du sport : " Dans chaque discipline sportive et pour une durée déterminée, une seule fédération agréée reçoit délégation du ministre chargé des sports " ; qu'aux termes de l'article L. 131-15 du même code : " Les fédérations délégataires : / 1° Organisent les compétitions sportives à l'issue desquelles sont délivrés les titres internationaux, nationaux, régionaux ou départementaux ; / 2° Procèdent aux sélections correspondantes (...) " ; qu'en confiant ainsi, à titre exclusif, aux fédérations sportives ayant reçu délégation la mission d'organiser des compétitions sur le territoire national et celle de sélectionner les équipes correspondantes, le législateur a chargé ces fédérations de l'exécution d'une mission de service public à caractère administratif ; que les décisions de ces fédérations relatives à la sélection des sportifs dans les équipes nationales, qu'elles aient pour effet de permettre cette sélection ou d'y faire obstacle, procèdent de la mise en oeuvre des prérogatives de puissance publique qui ont été conférées à ces fédérations pour l'accomplissement de leur mission de service public et présentent, par suite, le caractère d'actes administratifs ; <br/>
<br/>
              3.	Considérant qu'il ressort des énonciations de l'arrêt attaqué que, par décision du 29 septembre 2008, le président de la Fédération française des sports de glace a refusé de faire droit à la demande de M.B..., qui était membre de l'équipe de France de bobsleigh, tendant à ce qu'il soit mis fin à son appartenance à l'équipe de France afin de lui permettre de participer à des compétitions internationales comme membre d'une autre équipe nationale, conformément à ce que permettent les dispositions du règlement de la fédération internationale de bobsleigh et de toboganning ;<br/>
<br/>
              4.	Considérant que, pour juger que cette décision du 29 septembre 2008 présentait le caractère d'un acte administratif relevant de la compétence de la juridiction administrative, la cour administrative d'appel de Lyon, après avoir relevé que la demande de M. B... portait uniquement sur l'acceptation de son départ de l'équipe de France de bobsleigh et non sur son rattachement à une autre équipe nationale, pas davantage que sur l'appréciation de son droit éventuel à participer aux compétitions internationales au titre d'une autre équipe nationale, s'est fondée sur le motif que les décisions prises par la fédération française sur la sélection d'un sportif dans l'équipe nationale, dont font partie les décisions acceptant ou refusant la demande d'un sportif de sortir de cette équipe, sont prises dans le cadre des prérogatives de puissance publique dont cette fédération est investie par les articles L. 131-14 et L. 131-15 du code du sport ; qu'en statuant ainsi, la cour administrative d'appel, qui a suffisamment motivé sa décision, n'a pas commis d'erreur de droit ;<br/>
<br/>
              5.	Considérant qu'il résulte de ce qui précède que la Fédération française des sports de glace n'est pas fondée à demander l'annulation de l'arrêt qu'elle attaque ; <br/>
<br/>
              6.	Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'il soit fait droit aux conclusions présentées sur le fondement de ces dispositions par la fédération ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : Le pourvoi de la Fédération française des sports de glace est rejeté.<br/>
<br/>
Article 2 : La présente décision sera notifiée à la Fédération française des sports de glace et à M. A... B.... Copie en sera adressée, pour information, à la ministre des sports, de la jeunesse, de l'éducation populaire et de la vie associative.<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-01-05-01-01 ACTES LÉGISLATIFS ET ADMINISTRATIFS. DIFFÉRENTES CATÉGORIES D'ACTES. ACTES ADMINISTRATIFS - NOTION. ACTES À CARACTÈRE ADMINISTRATIF. ACTES PRÉSENTANT CE CARACTÈRE. - DÉCISIONS PRISES PAR LES FÉDÉRATIONS SPORTIVES SUR LA SÉLECTION D'UN SPORTIF DANS L'ÉQUIPE NATIONALE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">17-03-02-005-01 COMPÉTENCE. RÉPARTITION DES COMPÉTENCES ENTRE LES DEUX ORDRES DE JURIDICTION. COMPÉTENCE DÉTERMINÉE PAR UN CRITÈRE JURISPRUDENTIEL. ACTES. ACTES ADMINISTRATIFS. - DÉCISIONS PRISES PAR LES FÉDÉRATIONS SPORTIVES SUR LA SÉLECTION D'UN SPORTIF DANS L'ÉQUIPE NATIONALE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">63-05-01 SPORTS ET JEUX. SPORTS. FÉDÉRATIONS SPORTIVES. - DÉCISIONS PRISES SUR LA SÉLECTION D'UN SPORTIF DANS L'ÉQUIPE NATIONALE - CHAMP - DÉCISIONS STATUANT SUR LA DEMANDE D'UN SPORTIF DE SORTIR DE CETTE ÉQUIPE - INCLUSION - NATURE - ACTES ADMINISTRATIFS - EXISTENCE.
</SCT>
<ANA ID="9A"> 01-01-05-01-01 Les décisions prises par les fédérations sportives sur la sélection d'un sportif dans l'équipe nationale, dont font partie les décisions acceptant ou refusant la demande d'un sportif de sortir de cette équipe, sont prises dans le cadre des prérogatives de puissance publique dont cette fédération est investie par les articles L. 131-14 et L. 131-15 du code du sport et présentent le caractère d'actes administratifs relevant de la compétence de la juridiction administrative.</ANA>
<ANA ID="9B"> 17-03-02-005-01 Les décisions prises par les fédérations sportives sur la sélection d'un sportif dans l'équipe nationale, dont font partie les décisions acceptant ou refusant la demande d'un sportif de sortir de cette équipe, sont prises dans le cadre des prérogatives de puissance publique dont cette fédération est investie par les articles L. 131-14 et L. 131-15 du code du sport et présentent le caractère d'actes administratifs relevant de la compétence de la juridiction administrative.</ANA>
<ANA ID="9C"> 63-05-01 Les décisions prises par une fédération sportive sur la sélection d'un sportif dans l'équipe nationale, dont font partie les décisions acceptant ou refusant la demande d'un sportif de sortir de cette équipe, sont prises dans le cadre des prérogatives de puissance publique dont cette fédération est investie par les articles L. 131-14 et L. 131-15 du code du sport et présentent le caractère d'actes administratifs relevant de la compétence de la juridiction administrative.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
