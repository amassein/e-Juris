<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026504630</ID>
<ANCIEN_ID>JG_L_2012_10_000000349361</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/50/46/CETATEXT000026504630.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  3ème sous-section jugeant seule, 17/10/2012, 349361, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2012-10-17</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>349361</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 3ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Jean Courtial</PRESIDENT>
<AVOCATS>CARBONNIER</AVOCATS>
<RAPPORTEUR>Mme Christine Allais</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Vincent Daumas</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2012:349361.20121017</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 17 mai et 17 août 2011 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. Jean-Marc A, demeurant ... ; M. A demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 07PA03125 du 6 décembre 2010 par lequel la cour administrative d'appel de Paris a rejeté sa requête tendant, d'une part, à l'annulation du jugement n° 0517943/5, 0518840/5 du 2 mai 2007 du tribunal administratif de Paris ayant rejeté sa demande tendant à l'annulation de l'arrêté du 15 septembre 2005 l'affectant en qualité d'attaché d'administration scolaire et universitaire au collège Sainte-Blaise à compter du même jour, et de l'arrêté du 19 janvier 2006 par lequel le ministre de l'éducation nationale ne l'a pas autorisé à effectuer une nouvelle année de stage de conseiller d'administration scolaire et universitaire et l'a remis à la disposition de son administration d'origine à compter du 1er septembre 2005, d'autre part, à l'annulation pour excès de pouvoir de ces décisions, et à ce qu'il soit enjoint au ministre de l'éducation nationale de l'intégrer dans le corps des conseillers d'administration scolaire et universitaire à compter du 15 septembre 2005 et, enfin, à l'annulation des éléments de notation au titre de l'année 2003, en se substituant au rectorat de Créteil pour lui attribuer au titre de l'année 2002 la note de 17,5 et au rectorat de Paris en lui attribuant la note de 18,2 ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 600 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
              il soutient qu'en jugeant qu'était tardif son appel formé contre le jugement du 2 mai 2007 du tribunal administratif de Paris, la cour administrative d'appel de Paris a dénaturé les pièces du dossier, notamment la télécopie du 29 mars 2007 par laquelle il avait informé le tribunal de son changement d'adresse ; qu'en jugeant au surplus que sa requête sommaire était dépourvue de moyens d'appel dirigés contre le jugement attaqué et en relevant qu'elle n'aurait été complétée, à la suite d'une mise en demeure, qu'après l'expiration du délai imparti pour la présentation d'un mémoire complémentaire, la cour a dénaturé les faits et commis une erreur de droit ; <br/>
<br/>
              Vu l'arrêt attaqué ;<br/>
<br/>
              Vu le mémoire en défense, enregistré le 28 février 2012, présenté par le ministre de l'éducation nationale, de la jeunesse et de la vie associative, qui conclut au rejet du pourvoi ; il soutient que le pourvoi est tardif et par suite irrecevable ; que la cour administrative d'appel de Paris a pu, par une appréciation souveraine exempte de dénaturation, estimer que l'information relative à son changement d'adresse contenue dans la télécopie du 29 mars 2007, laquelle n'a d'ailleurs pas été régularisée par la production d'un original, n'était pas suffisante, et en déduire que l'appel formé par M. A était tardif ; que la requête introductive d'appel consistait en la seule reproduction littérale du mémoire de première instance et que le mémoire complémentaire contenant la motivation adéquate n'a été produit qu'après l'expiration du délai de recours, qui a commencé à courir au plus tard à la date de l'enregistrement de sa requête d'appel ; <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Christine Allais, Maître des requêtes en service extraordinaire,<br/>
<br/>
              - les observations de Me Carbonnier, avocat de M. A, <br/>
<br/>
              - les conclusions de M. Vincent Daumas, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à Me Carbonnier, avocat de M. A ;  <br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que M. A, attaché d'administration territoriale, a été reçu en 2003 au concours interne de conseiller d'administration scolaire et universitaire, puis au concours d'accès au cycle préparatoire au concours de l'Ecole nationale d'administration ; qu'ayant échoué à ce dernier concours, M. A a poursuivi son stage de conseiller d'administration scolaire et universitaire stagiaire à compter du 1er novembre 2004 ; que la commission administrative paritaire a émis, le 28 juin 2005, un avis défavorable au renouvellement de son année de stage ; que par arrêté du 8 juillet 2005, le ministre de l'éducation nationale a prononcé la réintégration de l'intéressé dans son corps d'origine à compter du 1er septembre 2005 ; que par un autre arrêté du 15 septembre 2005, le ministre a affecté M. A au collège parisien Sainte-Blaise à compter du 1er septembre 2005 ; que par un dernier arrêté en date du 19 janvier 2006, le ministre a retiré son arrêté du 8 juillet 2005, décidé que M. A n'était pas autorisé à effectuer une nouvelle année de stage et remis l'intéressé à la disposition de son administration d'origine avec effet rétroactif à compter du 1er septembre 2005 ; que par un jugement du 2 mai 2007, le tribunal administratif de Paris a rejeté la demande de M. A tendant à l'annulation des arrêtés du 15 septembre 2005 et du 18 janvier 2006 ; que la cour administrative d'appel de Paris a rejeté l'appel de M. A par un arrêt du 6 décembre 2010 contre lequel celui-ci se pourvoit régulièrement en cassation ;<br/>
<br/>
              2. Considérant que l'arrêt attaqué a été notifié au requérant le 3 janvier 2011 ; que celui-ci a formé une demande d'aide juridictionnelle enregistrée au bureau d'aide juridictionnelle du Conseil d'Etat le 5 janvier 2011 ; que la décision du bureau d'aide juridictionnelle a été notifiée à M. A par lettre du 16 mars 2011 ; que, par suite, le pourvoi enregistré au secrétariat du contentieux du Conseil d'Etat le 17 mai 2011 n'est pas tardif ; <br/>
<br/>
              3. Considérant que le mémoire adressé par télécopie au greffe du tribunal administratif de Paris le 29 mars 2007, avant l'audience fixée au 4 avril, par M. A, commençait par les termes : " je vous signale mon changement d'adresse (...) " et que l'adresse figurant en haut de ce mémoire était effectivement différente de celle que le requérant  avait indiquée dans sa requête introductive ; qu'en estimant dans ces conditions, pour en déduire que la notification du jugement du tribunal administratif de Paris effectuée à l'adresse mentionnée par l'intéressé dans sa requête introductive était régulière et avait fait courir le délai de recours contentieux à l'égard du requérant, que ce dernier n'avait pas informé de façon " très explicite " le greffe du tribunal administratif de son changement d'adresse, la cour administrative d'appel de Paris a dénaturé les pièces du dossier ; que, sans qu'il soit besoin d'examiner l'autre moyen de son pourvoi , M. A est, par suite, fondé à demander l'annulation de l'arrêt attaqué; <br/>
<br/>
              4. Considérant qu'il résulte des dispositions du deuxième alinéa de l'article R. 811-1 du code de justice administrative, combinées avec celles du 2° de l'article R. 222-13 du même code, que le tribunal administratif statue en premier et dernier ressort dans les litiges relatifs à la situation individuelle des fonctionnaires autres que ceux qui concernent l'entrée au service, la discipline ou la sortie du service ; que la contestation par un fonctionnaire de la décision mettant fin à son stage probatoire et refusant de le titulariser dans un nouveau corps, rendu accessible par la voie d'un concours interne, concerne le déroulement de la carrière de cet agent et non son entrée au service ; qu'elle est donc au nombre des litiges sur lesquels le tribunal administratif statue en premier et dernier ressort ; que, par suite, la requête de M. A, attaché d'administration territoriale, tendant à l'annulation du jugement du 2 mai 2007 du tribunal administratif de Paris ayant  rejeté ses conclusions énoncées ci-dessus, a le caractère d'un pourvoi en cassation relevant de la compétence du Conseil d'Etat ; qu'il y a lieu d'y statuer sans qu'il soit besoin de se prononcer sur la recevabilité des écritures produites par M. A devant la cour administrative d'appel de Paris ;<br/>
<br/>
              5. Considérant qu'aux termes de l'article 1er de la loi du 11 juillet 1979 : " Les personnes physiques ou morales ont le droit d'être informées sans délai des motifs des décisions administratives individuelles défavorables qui les concernent. / A cet effet, doivent être motivées les décisions qui : / (...) - infligent une sanction ; / (...) - retirent ou abrogent une décision créatrice de droits ; / (...) - refusent un avantage dont l'attribution constitue un droit pour les personnes qui remplissent les conditions légales pour l'obtenir (...) " ; que la décision refusant de titulariser M. A dans le corps des conseillers d'administration scolaire et universitaire n'a pas le caractère d'une sanction ; que si la nomination dans un corps en tant que fonctionnaire stagiaire confère à son bénéficiaire le droit d'effectuer un stage dans la limite de la durée maximale prévue par les règlements qui lui sont applicables, elle ne lui confère aucun droit à être titularisé ; qu'il en résulte que la décision refusant, au terme du stage, de le titulariser n'a pour effet, ni de refuser à l'intéressé un avantage qui constituerait, pour lui, un droit, ni, dès lors que le stage a été accompli dans la totalité de la durée prévue par la décision de nomination comme stagiaire, de retirer ou d'abroger une décision créatrice de droits ; que, par suite, en jugeant que la décision refusant de titulariser M. A n'était pas au nombre de celles qui doivent être motivées en application de l'article 1er de la loi du 11 juillet 1979, le tribunal administratif de Paris n'a pas commis d'erreur de droit ;<br/>
<br/>
              6. Considérant que l'exception tirée de l'illégalité de l'arrêté du 8 juillet 2005 prononçant la réintégration de M. A dans son corps d'origine à compter du 1er juillet 2005 est nouvelle en cassation et ne saurait par suite, en tout état de cause, être accueillie ; qu'au demeurant, cet arrêté a été retiré par l'arrêté du 19 janvier 2006, qui a pris la même mesure avec effet rétroactif ; qu'ainsi, l'arrêté d'affectation du 15 septembre 2005 ne peut être regardé comme pris pour l'application de l'arrêté du 8 juillet 2005 ;<br/>
<br/>
              7. Considérant qu'il ressort des pièces du dossier que, eu égard notamment aux appréciations portées sur l'aptitude au travail en équipe du requérant,  le tribunal administratif de Paris a pu, sans entacher son jugement de dénaturation, estimer que le refus de titulariser l'intéressé dans le corps des conseillers d'administration scolaire et universitaire n'était pas, eu égard à la nature des fonctions des membres de ce corps et aux conditions d'exercice de celles-ci, entaché d'erreur manifeste d'appréciation  et écarter le moyen tiré de ce que le refus de le titulariser constituerait un détournement de pouvoir ; <br/>
<br/>
              8. Considérant qu'il résulte de tout ce qui précède que M. A n'est pas fondé à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Paris a rejeté sa demande ; que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Paris du 6 décembre 2010 est annulé.<br/>
Article 2 : Les conclusions du pourvoi de M. A dirigées contre le jugement du 2 mai 2007 du tribunal administratif de Paris sont rejetées.<br/>
Article 3 : La présente décision sera notifiée à M. A et au ministre de l'éducation nationale.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
