<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032260325</ID>
<ANCIEN_ID>JG_L_2016_03_000000384786</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/26/03/CETATEXT000032260325.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère SSJS, 16/03/2016, 384786, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-03-16</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>384786</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère SSJS</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP CELICE, BLANCPAIN, SOLTNER, TEXIDOR ; SCP WAQUET, FARGE, HAZAN</AVOCATS>
<RAPPORTEUR>M. Yannick Faure</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Jean Lessi</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2016:384786.20160316</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
Procédure contentieuse antérieure<br/>
<br/>
              M. et Mme B...et Françoise A...ont demandé au tribunal administratif de Marseille d'annuler pour excès de pouvoir l'arrêté du 16 décembre 2010 par lequel le maire de Marseille a accordé à la société UNIMO un permis de construire tendant à autoriser la démolition d'un ensemble de constructions et la construction d'un immeuble de soixante-cinq logements au 70, boulevard Hilarion Boeuf. Par un jugement n° 1104096 du 22 novembre 2012, le tribunal administratif de Marseille a rejeté leur demande.<br/>
<br/>
              Par un arrêt n° 13MA00370 du 21 juillet 2014, la cour administrative d'appel de Marseille a, à la demande de M. et MmeA..., annulé le jugement du tribunal administratif de Marseille du 22 novembre 2012 ainsi que l'arrêté du maire de Marseille du 16 décembre 2010.<br/>
<br/>
Procédure devant le Conseil d'Etat<br/>
<br/>
              1° Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique enregistrés, sous le n° 384786, les 25 septembre 2014, 22 décembre 2014 et 30 novembre 2015 au secrétariat du contentieux du Conseil d'Etat, la société Crédit agricole immobilier entreprise, venant aux droits de la société Crédit agricole immobilier promotion qui elle-même venait aux droits de la société UNIMO, demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt de la cour administrative d'appel de Marseille du 21 juillet 2014 ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de M. et MmeA... ;<br/>
<br/>
              3°) de mettre à la charge de M. et Mme A...la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              2° Par un pourvoi sommaire et un mémoire complémentaire enregistrés, sous le n° 384790, les 25 septembre et 23 décembre 2014 au secrétariat du contentieux du Conseil d'Etat, la ville de Marseille demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler le même arrêt de la cour administrative d'appel du 21 juillet 2014 ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de M. et MmeA... ; <br/>
<br/>
              3°) de mettre à la charge de M. et Mme A...la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu :<br/>
              - le code de l'urbanisme ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Yannick Faure, auditeur,  <br/>
<br/>
              - les conclusions de M. Jean Lessi, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Célice, Blancpain, Soltner, Texidor, avocat de la société Crédit agricole immobilier entreprise, à la SCP Waquet, Farge, Hazan, avocat de M. et MmeA..., et à Me Haas, avocat de la ville de Marseille ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Les pourvois de la société Crédit agricole immobilier entreprise et de la ville de Marseille sont dirigés contre le même arrêt. Il y a lieu de les joindre pour qu'il y soit statué par une même décision.<br/>
<br/>
              2. Il ressort des pièces du dossier soumis aux juges du fond que le maire de Marseille a, par un arrêté du 16 décembre 2010, accordé à la société UNIMO, aux droits de laquelle a succédé la société Crédit agricole immobilier promotion puis la société Crédit agricole immobilier entreprise, un permis de construire en vue de l'édification d'un immeuble d'habitation comprenant 65 appartements sur un terrain situé 70 boulevard Hilarion Boeuf dans le 10ème arrondissement. Par un jugement du 22 novembre 2012, le tribunal administratif de Marseille a rejeté la demande de M. et MmeA..., propriétaires d'une maison voisine, tendant à l'annulation de ce permis de construire. Par un arrêt du 21 juillet 2014, contre lequel la ville de Marseille et la société Crédit agricole immobilier entreprise se pourvoient en cassation, la cour administrative d'appel de Marseille a annulé ce jugement ainsi que le permis de construire délivré le 16 décembre 2010, au motif qu'il méconnaissait l'article R - UA 7 du règlement du plan d'occupation des sols de Marseille.<br/>
<br/>
              3. Le règlement du plan d'occupation des sols de Marseille, approuvé par délibération du 22 décembre 2000 et modifié par délibération du 28 juin 2010, définit la zone UA comme correspondant aux " tissus centraux ", soit un " tissu constitué, continu et aligné le long de rues, à l'intérieur duquel des constructions en retrait ou en interruption de façade pourront être admises le long de certaines voies dans un souci d'aération et de modernisation du tissu ". Aux termes de l'article R - UA 7 de ce règlement, relatif à l'implantation des constructions par rapport aux limites séparatives : " Les constructions à édifier sont implantées : / 1. ... sur une profondeur, mesurée à compter de la limite de l'alignement (...) et égale à la plus grande profondeur de la parcelle, diminuée de 4 mètres, sans être supérieure à 17 mètres, / 1.1. par rapport aux limites latérales, (...) /  1.1.2. ... sans nécessaire continuité en UA (...) à condition que la distance mesurée horizontalement de tout point desdites constructions au point le plus proche de la limite concernée soit au moins égale à la moitié de la hauteur de la construction à édifier diminuée de 3 mètres, sans être inférieure à 4 mètres (...) / 1.2. par rapport aux limites arrière, sans prescription particulière (...) ".<br/>
<br/>
              4. Ces dispositions permettent, en zone UA, une implantation des constructions sur une profondeur qui peut atteindre, si la parcelle a au moins 21 mètres de profondeur, 17 mètres à compter de l'alignement avec la voie, soit en continuité par rapport aux limites séparatives latérales, soit dans le respect d'une  règle de prospect. Dans le cas d'un terrain situé à l'angle de deux voies, en l'absence de règle particulière dans le règlement du plan d'occupation des sols, peuvent être délimitées à partir de l'alignement de ces voies deux bandes d'une profondeur maximale de 17 mètres, se recoupant pour partie, à l'intérieur desquelles la construction doit être édifiée. Par suite, en excluant le cumul de zones de constructibilité mesurées à partir de chacun des alignements, la cour administrative d'appel a commis une erreur de droit.<br/>
<br/>
              5. Il résulte de ce qui précède que la ville de Marseille et la société Crédit agricole immobilier entreprise sont fondées à demander l'annulation de l'arrêt de la cour administrative d'appel de Marseille du 21 juillet 2014. Le moyen d'erreur de droit retenu suffisant à entraîner cette annulatuion, il n'est pas nécessaire d'examiner l'autre moyen des pourvois.<br/>
<br/>
              6. Il n'y a pas lieu, dans les circonstances de l'espèce, de mettre à la charge de M. et Mme A...les sommes que la société Crédit agricole immobilier entreprise et la ville de Marseille demandent au titre de l'article L. 761-1 du code de justice administrative. Les dispositions de cet article font obstacle à ce qu'une somme soit mise à ce titre à la charge de la société Crédit agricole immobilier entreprise et de la ville de Marseille, qui ne sont pas, dans la présente instance, les parties perdantes.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Marseille du 21 juillet 2014 est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Marseille.<br/>
Article 3 : Les conclusions des parties présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée à la ville de Marseille, à la société Crédit agricole immobilier entreprise et à M. et Mme B...et FrançoiseA....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
