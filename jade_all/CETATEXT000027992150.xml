<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027992150</ID>
<ANCIEN_ID>JG_L_2013_09_000000352616</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/99/21/CETATEXT000027992150.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème et 1ère sous-sections réunies, 25/09/2013, 352616, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-09-25</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>352616</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème et 1ère sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP NICOLAY, DE LANOUVELLE, HANNOTIN</AVOCATS>
<RAPPORTEUR>M. Didier Ribes</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Xavier de Lesquen</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:352616.20130925</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 12 septembre et 12 décembre 2011 au secrétariat du contentieux du Conseil d'État, présentés pour la commune d'Ornaisons, représentée par son maire ; la commune d'Ornaisons demande au Conseil d'État :<br/>
<br/>
              1°) d'annuler l'arrêt n° 09MA03130 du 11 juillet 2011 par lequel la cour administrative d'appel de Marseille, réformant le jugement n° 0704415 du 11 juin 2009 du tribunal administratif de Montpellier, a annulé, à la demande de M. et MmeB..., le plan local d'urbanisme approuvé par la délibération du 31 juillet 2007 en tant qu'il classe en zone N la parcelle n° 626 leur appartenant ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à ses conclusions d'appel ; <br/>
<br/>
              3°) de mettre à la charge de M. et Mme B...le versement de la somme de  4 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de l'urbanisme ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Didier Ribes, Maître des Requêtes,  <br/>
<br/>
              - les conclusions de M. Xavier de Lesquen, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Nicolaÿ, de Lanouvelle, Hannotin, avocat de la commune d'Ornaisons ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, par un jugement du 11 juin 2009, le tribunal administratif de Montpellier a rejeté la demande de M. et Mme B...tendant à l'annulation du plan local d'urbanisme de la commune d'Ornaisons en tant qu'il classe en zone naturelle une parcelle leur appartenant située à proximité immédiate d'une importante zone boisée dont la préservation constitue un des objectifs fixés par le projet d'aménagement et de développement durable de ce plan ; que, par un arrêt du 11 juillet 2011, contre lequel la commune d'Ornaisons se pourvoit en cassation, la cour administrative d'appel de Marseille, après avoir jugé que le classement litigieux était entaché d'une erreur manifeste d'appréciation, a annulé, dans cette mesure, le plan local d'urbanisme de la commune d'Ornaisons et réformé le jugement du tribunal administratif de Montpellier ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article R. 123-8 du code de l'urbanisme, dans sa rédaction alors applicable : " Les zones naturelles et forestières sont dites "zones N". Peuvent être classés en zone naturelle et forestière les secteurs de la commune, équipés ou non, à protéger en raison soit de la qualité des sites, des milieux naturels, des paysages et de leur intérêt, notamment du point de vue esthétique, historique ou écologique, soit de l'existence d'une exploitation forestière, soit de leur caractère d'espaces naturels. (...) " ;<br/>
<br/>
              3. Considérant qu'il appartient aux auteurs d'un plan local d'urbanisme de  déterminer le parti d'aménagement à retenir pour le territoire concerné par le plan, en tenant compte de la situation existante et des perspectives d'avenir, et de fixer en conséquence le zonage et les possibilités de construction ; qu'ils peuvent être amenés, à cet effet, à classer en zone naturelle, pour les motifs énoncés à l'article R. 123-8, un secteur qu'ils entendent soustraire, pour l'avenir, à l'urbanisation ; que leur appréciation sur ces différents points ne peut être censurée par  le juge administratif qu'au cas où elle serait entachée d'une erreur  manifeste ou fondée sur des faits matériellement inexacts ; <br/>
<br/>
              4. Considérant que la cour a relevé que la parcelle litigieuse, bien que située à l'extrémité d'une vaste zone boisée, n'était cependant pas couverte par la servitude d'espace boisé, qu'elle se trouvait à proximité de parcelles bâties qui constituent un secteur d'urbanisation de la commune et qu'elle pourrait être desservie par les réseaux présents à proximité ; qu'il résulte de ce qui a été dit au point 3 qu'en déduisant de ces seuls éléments que le classement de la parcelle était entaché d'une erreur manifeste d'appréciation, sans prendre en compte les caractéristiques de la parcelle et la vocation des zones naturelles, telle que précisée par les dispositions précitées de l'article R. 123-8 du code de l'urbanisme, ni le parti d'aménagement retenu par la commune dans le plan d'aménagement et de développement durable, la cour a commis une erreur de droit ; que, par suite, et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, la commune d'Ornaisons est fondée à demander l'annulation de l'arrêt attaqué ;<br/>
<br/>
              5. Considérant qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par la commune d'Ornaisons au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Marseille du 11 juillet 2011 est annulé.<br/>
<br/>
      Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Marseille.<br/>
<br/>
Article 3 : Les conclusions présentées par la commune d'Ornaisons au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 4 : La présente décision sera notifiée à la commune d'Ornaisons et à M. et Mme A...B....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
