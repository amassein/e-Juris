<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030259204</ID>
<ANCIEN_ID>JG_L_2015_02_000000367414</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/25/92/CETATEXT000030259204.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème - 1ère SSR, 11/02/2015, 367414, Publié au recueil Lebon</TITRE>
<DATE_DEC>2015-02-11</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>367414</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème - 1ère SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LYON-CAEN, THIRIEZ ; FOUSSARD</AVOCATS>
<RAPPORTEUR>Mme Sophie Roussel</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Suzanne von Coester</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:XX:2015:367414.20150211</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 4 avril et 5 juillet 2013 au secrétariat du contentieux du Conseil d'Etat, présentés pour Mme A...D..., demeurant la même adresse et M. C... B..., demeurant à la même adresse; Mme D...et M. B...demandent au Conseil d'Etat :  <br/>
<br/>
              1°) d'annuler l'arrêt n° 12PA02300 du 24 janvier 2013 par lequel la cour administrative d'appel de Paris a rejeté leur requête tendant, d'une part, à l'annulation du jugement du 22 mars 2012 du tribunal administratif de Melun rejetant leur demande tendant à l'annulation de l'arrêté du 2 avril 2009 par lequel le maire de Gretz-Armainvilliers (77220) a refusé de leur délivrer le permis de construire qu'ils avaient sollicité en vue de l'agrandissement d'une maison et de la modification de sa toiture et, d'autre part, à l'annulation de cet arrêté ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à leur appel ;  <br/>
<br/>
              3°) de mettre à la charge de la commune de Gretz-Armainvilliers la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative, ainsi que la somme de 35 euros correspondant à la contribution à l'aide juridique prévue à l'article R. 761-1 du même code ; <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de l'urbanisme ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Sophie Roussel, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Suzanne von Coester, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Lyon-Caen, Thiriez, avocat de Mme D...et de M. B...et à Me Foussard, avocat de la commune de Gretz-Armainvilliers ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, par un arrêté du 2 avril 2009, le maire de Gretz-Armainvilliers a refusé de délivrer à Mme D... et M. B...un permis de construire, sollicité en vu de l'agrandissement de leur maison et de la modification de sa toiture, au motif que le projet méconnaissait les prescriptions du règlement du plan local d'urbanisme relatives à l'implantation des constructions par rapport aux limites séparatives ; que, par un jugement du 22 mars 2011, le tribunal administratif de Melun a rejeté la demande des intéressés tendant à l'annulation de cet arrêté ; que par un arrêt du 24 janvier 2013, contre lequel Mme D...et M. B...se pourvoient en cassation, la cour administrative d'appel de Paris a rejeté leur appel dirigé contre ce jugement ; <br/>
<br/>
              2. Considérant qu'aux termes de l'article L. 421-6 du code de l'urbanisme : " Le permis de construire ou d'aménager ne peut être accordé que si les travaux projetés sont conformes aux dispositions législatives et réglementaires relatives à l'utilisation des sols, à l'implantation, la destination, la nature, l'architecture, les dimensions, l'assainissement des constructions et à l'aménagement de leurs abords et s'ils ne sont pas incompatibles avec une déclaration d'utilité publique. " ; qu'aux termes du premier alinéa de l'article L. 123-1-9 de ce code : " Les règles et servitudes définies par un plan local d'urbanisme ne peuvent faire l'objet d'aucune dérogation, à l'exception des adaptations mineures rendues nécessaires par la nature du sol, la configuration des parcelles ou le caractère des constructions avoisinantes. " ; <br/>
<br/>
              3. Considérant qu'il appartient à l'autorité administrative, saisie d'une demande de permis de construire, de déterminer si le projet qui lui est soumis ne méconnaît pas les dispositions du plan local d'urbanisme applicables, y compris telles qu'elles résultent le cas échéant d'adaptations mineures lorsque la nature particulière du sol, la configuration des parcelles d'assiette du projet ou le caractère des constructions avoisinantes l'exige ; que le pétitionnaire peut, à l'appui de sa contestation, devant le juge de l'excès de pouvoir, du refus opposé à sa demande se prévaloir de la conformité de son projet aux règles d'urbanisme applicables, le cas échéant assorties d'adaptations mineures dans les conditions précisées ci-dessus, alors même qu'il n'a pas fait état, dans sa demande à l'autorité administrative, de l'exigence de telles adaptations ; <br/>
<br/>
              4. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que Mme D...et M. B...soutenaient devant la cour administrative d'appel de Paris que leur projet était conforme aux règles relatives à l'implantation des constructions par rapport aux voies publiques et privées et par rapport aux limites séparatives, fixées par les articles UD 6 et UD 7 du règlement du plan local d'urbanisme, au bénéfice d'adaptations mineures de ces règles ; qu'il résulte de ce qui précède qu'en se fondant, pour écarter un tel moyen, sur le fait que Mme D... et M. B...n'avaient pas fait état, dans leur demande de permis de construire, d'adaptations mineures des règles en cause, la cour a commis une erreur de droit ;  <br/>
<br/>
              5. Considérant qu'il résulte de qui précède que Mme D...et M. B... sont fondés à demander l'annulation de l'arrêt qu'ils attaquent ; <br/>
<br/>
              6. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la commune de Gretz-Armainvilliers la somme de 3 000 euros, à verser à Mme D... et M.B..., au titre des dispositions de l'article L. 761-1 du code de justice administrative ; que ces dispositions font en revanche obstacle à ce que soit mise à la charge de Mme D...et de M.B..., qui ne sont pas, dans la présente instance, les parties perdantes, la somme que demande la commune ; qu'il y a lieu, par ailleurs, de mettre à la charge de cette dernière la contribution pour l'aide juridique au titre  des dispositions de l'article R. 761-1 du même code ; <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Paris est annulé. <br/>
<br/>
      Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Paris.  <br/>
<br/>
Article 3 : La commune de Gretz-Armainvilliers versera une somme de 3 000 euros à Mme D... et M. B...au titre des dispositions des articles L. 761-1 du code de justice administrative. <br/>
<br/>
Article 4 : Les conclusions présentées par la commune de Gretz-Armainvilliers au titre de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
<br/>
Article 5 : La contribution pour l'aide juridique est mise à la charge de la commune de Gretz-Armainvilliers au titre  des dispositions de l'article R. 761-1 du code de justice administrative. <br/>
<br/>
Article 6 : La présente décision sera notifiée à Mme A...D..., à M. B...et à la commune de Gretz-Armainvilliers. <br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">68-01-01-02-03-01 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. PLANS D'AMÉNAGEMENT ET D'URBANISME. PLANS D`OCCUPATION DES SOLS (POS) ET PLANS LOCAUX D'URBANISME (PLU). APPLICATION DES RÈGLES FIXÉES PAR LES POS OU LES PLU. DÉROGATIONS. ADAPTATIONS MINEURES. - DEMANDE PORTANT SUR UN PROJET EXIGEANT DES ADAPTATIONS MINEURES AUX DISPOSITIONS DU PLU (ART. L. 123-1-9 DU CODE DE L'URBANISME) - 1) OBLIGATIONS DE L'ADMINISTRATION - INCLUSION - PRISE EN COMPTE DE CETTE EXIGENCE LORS DE L'EXAMEN DE LA CONFORMITÉ DU PROJET - 2) RECOURS CONTRE UNE DÉCISION DE REFUS - POSSIBILITÉ POUR LE REQUÉRANT D'INVOQUER CETTE EXIGENCE ALORS MÊME QU'IL N'EN AURAIT PAS FAIT ÉTAT DANS SA DEMANDE À L'ADMINISTRATION - EXISTENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">68-03-02-02 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. PERMIS DE CONSTRUIRE. PROCÉDURE D'ATTRIBUTION. INSTRUCTION DE LA DEMANDE. - DEMANDE PORTANT SUR UN PROJET EXIGEANT DES ADAPTATIONS MINEURES AUX DISPOSITIONS DU PLU (ART. L. 123-1-9 DU CODE DE L'URBANISME) - 1) OBLIGATIONS DE L'ADMINISTRATION - INCLUSION - PRISE EN COMPTE DE CETTE EXIGENCE LORS DE L'EXAMEN DE LA CONFORMITÉ DU PROJET - 2) RECOURS CONTRE UNE DÉCISION DE REFUS - POSSIBILITÉ POUR LE REQUÉRANT D'INVOQUER CETTE EXIGENCE ALORS MÊME QU'IL N'EN AURAIT PAS FAIT ÉTAT DANS SA DEMANDE À L'ADMINISTRATION - EXISTENCE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">68-06-04 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. RÈGLES DE PROCÉDURE CONTENTIEUSE SPÉCIALES. POUVOIRS DU JUGE. - DEMANDE PORTANT SUR UN PROJET EXIGEANT DES ADAPTATIONS MINEURES AUX DISPOSITIONS DU PLU (ART. L. 123-1-9 DU CODE DE L'URBANISME) - 1) OBLIGATIONS DE L'ADMINISTRATION - INCLUSION - PRISE EN COMPTE DE CETTE EXIGENCE LORS DE L'EXAMEN DE LA CONFORMITÉ DU PROJET - 2) RECOURS CONTRE UNE DÉCISION DE REFUS - POSSIBILITÉ POUR LE REQUÉRANT D'INVOQUER CETTE EXIGENCE ALORS MÊME QU'IL N'EN AURAIT PAS FAIT ÉTAT DANS SA DEMANDE À L'ADMINISTRATION - EXISTENCE.
</SCT>
<ANA ID="9A"> 68-01-01-02-03-01 1) Il appartient à l'autorité administrative, saisie d'une demande de permis de construire, de déterminer si le projet qui lui est soumis ne méconnaît pas les dispositions du PLU applicables, y compris telles qu'elles résultent le cas échéant d'adaptations mineures, comme le prévoit l'article L. 123-1-9 du code de l'urbanisme, lorsque la nature particulière du sol, la configuration des parcelles d'assiette du projet ou le caractère des constructions avoisinantes l'exige.... ,,2) Le pétitionnaire peut, à l'appui de sa contestation, devant le juge de l'excès de pouvoir, du refus opposé à sa demande se prévaloir de la conformité de son projet aux règles d'urbanisme applicables, le cas échéant assorties d'adaptations mineures dans les conditions précisées ci-dessus, alors même qu'il n'a pas fait état, dans sa demande à l'autorité administrative, de l'exigence de telles adaptations.</ANA>
<ANA ID="9B"> 68-03-02-02 1) Il appartient à l'autorité administrative, saisie d'une demande de permis de construire, de déterminer si le projet qui lui est soumis ne méconnaît pas les dispositions du plan local d'urbanisme (PLU) applicables, y compris telles qu'elles résultent le cas échéant d'adaptations mineures, comme le prévoit l'article L. 123-1-9 du code de l'urbanisme, lorsque la nature particulière du sol, la configuration des parcelles d'assiette du projet ou le caractère des constructions avoisinantes l'exige.... ,,2) Le pétitionnaire peut, à l'appui de sa contestation, devant le juge de l'excès de pouvoir, du refus opposé à sa demande se prévaloir de la conformité de son projet aux règles d'urbanisme applicables, le cas échéant assorties d'adaptations mineures dans les conditions précisées ci-dessus, alors même qu'il n'a pas fait état, dans sa demande à l'autorité administrative, de l'exigence de telles adaptations.</ANA>
<ANA ID="9C"> 68-06-04 1) Il appartient à l'autorité administrative, saisie d'une demande de permis de construire, de déterminer si le projet qui lui est soumis ne méconnaît pas les dispositions du plan local d'urbanisme (PLU) applicables, y compris telles qu'elles résultent le cas échéant d'adaptations mineures, comme le prévoit l'article L. 123-1-9 du code de l'urbanisme, lorsque la nature particulière du sol, la configuration des parcelles d'assiette du projet ou le caractère des constructions avoisinantes l'exige.... ,,2) Le pétitionnaire peut, à l'appui de sa contestation, devant le juge de l'excès de pouvoir, du refus opposé à sa demande se prévaloir de la conformité de son projet aux règles d'urbanisme applicables, le cas échéant assorties d'adaptations mineures dans les conditions précisées ci-dessus, alors même qu'il n'a pas fait état, dans sa demande à l'autorité administrative, de l'exigence de telles adaptations.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
