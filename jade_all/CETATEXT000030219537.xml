<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030219537</ID>
<ANCIEN_ID>J2_L_2015_02_000001400558</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/21/95/CETATEXT000030219537.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>COUR ADMINISTRATIVE D'APPEL DE LYON, 3ème chambre - formation à 3, 03/02/2015, 14LY00558, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-02-03</DATE_DEC>
<JURIDICTION>COUR ADMINISTRATIVE D'APPEL DE LYON</JURIDICTION>
<NUMERO>14LY00558</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème chambre - formation à 3</FORMATION>
<TYPE_REC>excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. MARTIN</PRESIDENT>
<AVOCATS>CJA PUBLIC CHAVENT - MOUSEGHIAN</AVOCATS>
<RAPPORTEUR>Mme Pascale  DECHE</RAPPORTEUR>
<COMMISSAIRE_GVT>M. CLEMENT</COMMISSAIRE_GVT>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu, I, sous le n° 14LY00558, le recours enregistré le 3 mars 2014, présenté par le ministre du redressement productif ;  <br/>
<br/>
       Le ministre du redressement productif demande à la Cour :<br/>
<br/>
       1°) d'annuler le jugement n° 1105379-1200549 en date du 12 décembre 2013 du tribunal administratif de Lyon en tant qu'il a annulé la décision du préfet de l'Ardèche du 8 mars 2012 refusant d'abroger l'arrêté préfectoral n° 2011208-0004 du 27 juillet 2011 approuvant le plan de prévention des risques miniers (PPRM) sur le territoire des communes de Largentière, Chassiers, Montréal et Vinezac et qu'il a enjoint au préfet de l'Ardèche d'abroger cet arrêté dans le délai d'un mois suivant la notification du jugement ;  <br/>
<br/>
       2°) de rejeter la demande présentée par M. et Mme A...devant le Tribunal ; <br/>
<br/>
       il soutient que : <br/>
<br/>
       - c'est à tort que les premiers juges ont entièrement fait droit à la demande d'injonction alors que l'intérêt à agir de M. A...le limitait à pouvoir demander l'abrogation partielle du PPRM (sur le seul territoire de la commune de Largentière) ; en tout état de cause, le caractère divisible du PPRM aurait dû les conduire à prononcer tout au plus une injonction d'abrogation partielle de ce plan ;  <br/>
       - c'est à tort que les premiers juges ont critiqué le fait que les avis des communes n'avaient pas été consignés ou annexés au registre d'enquête publique et que l'avis de la chambre d'agriculture n'avait pas été recueilli ; l'absence des avis des communes dans le dossier soumis à enquête publique, à la supposer établie n'est pas de nature à entacher d'une irrégularité substantielle la procédure de concertation ; aucune prescription du règlement du PPRM ne porte sur des terrains agricoles ou forestiers et n'a pour finalité leur diminution effective : l'avis de la chambre d'agriculture n'était pas requis ;  <br/>
       - c'est à tort que les premiers juges ont considéré que l'absence d'audition des maires des communes intéressées par le commissaire-enquêteur avait été susceptible d'exercer une influence sur le contenir du PPRM ; les maires ont été effectivement étroitement associés à l'élaboration et à l'établissement du PPRM ; leurs avis ayant pu être connus par ailleurs, le fait de ne pas les avoir auditionnés n'a pas eu d'influence sur le sens de la décision ; en outre, l'avis du commissaire-enquêteur ne lie pas l'autorité compétente qui au demeurant, a pris connaissance des avis des maires ; enfin, le maire de Largentière ayant saisi le préfet de l'Ardèche d'une demande d'élaboration du PPRM, il a pu exposer son appréciation des risques de toute nature concernant le territoire de sa commune, qui a ainsi été nécessairement prise en compte dans le processus d'élaboration du document ; ainsi, la méconnaissance des dispositions de l'article R. 562-8 du code de l'environnement n'entache pas d'irrégularité l'arrêté litigieux ;  <br/>
<br/>
       Vu le jugement attaqué ;<br/>
<br/>
       Vu l'ordonnance, en date du 16 mai 2014, fixant la clôture d'instruction au 20 juin 2014, en application des articles R. 613-1 et R. 613-3 du code de justice administrative ;<br/>
<br/>
       Vu le mémoire, enregistré le 20 juin 2014, présenté pour M. et Mme C...A...qui concluent : <br/>
<br/>
       1°) à titre principal, au rejet du recours ; <br/>
<br/>
       2°) à titre subsidiaire, à ce qu'il soit ordonné une expertise ; <br/>
<br/>
       3°) à ce qu'une somme de 3 000 euros soit mise à la charge de l'Etat sur le fondement des dispositions de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
       ils soutiennent que : <br/>
<br/>
       - ils ont intérêt à demander l'abrogation du règlement illégal dans son intégralité et l'injonction prononcée par les premiers juges est d'autant plus justifiée que l'illégalité retenue affecte l'ensemble de la procédure d'élaboration du PPRM et qu'aucune régularisation partielle n'est envisageable ; <br/>
       - les maires des communes sur le territoire desquelles le plan doit s'appliquer n'ont pas été entendus, du moins s'agissant des maires de Chassiers, Vinezac et Montréal ; de même, les avis émis entre 2009 et 2010 par les conseils municipaux concernés n'ont pas été consignés, ni même annexés au registre d'enquête publique ; l'avis de la chambre d'agriculture n'a pas été requis alors que le zonage du plan intègre des terrains agricoles de Largentière ; ce vice de procédure constitue une irrégularité substantielle ;  <br/>
       - les dispositions de la loi n° 2003-699 du 30 juillet 2003 n'ont pas été visées dans le cadre du dossier d'enquête, pas plus que la façon dont l'enquête s'insère dans la procédure administrative en cause ;  <br/>
       - l'arrêté préfectoral du 13 janvier 2006 prescrivant l'élaboration du PPRM et fixant les modalités de la concertation est irrégulier en ce qu'il ne prévoit pas suffisamment de modalités d'association des collectivités territoriales concernées, seuls leurs avis étant sollicités ; les dispositions des articles L. 562-3 et R. 562-3 du code de l'environnement ont ainsi été méconnues ; cet arrêté est également insuffisant quant aux modalités de la concertation avec la population qui ne prévoit qu'une seule réunion publique d'information ; les différentes formes de concertation avec le public qui auraient été organisées n'ont, en tout état de cause, pas été prévues par cet arrêté et révèlent ainsi l'incompétence négative du préfet, seule autorité compétente pour définir les modalités de la concertation ; le préfet n'établit en rien la réalité des éléments de concertation avec le public dont il fait état ; <br/>
       - la création d'une zone rouge, à cheval sur les parcelles cadastrées B1592 et B153 est intervenue alors même que tant le bureau d'étude que les services de l'Etat sont dans l'incertitude totale quant au positionnement du puits du Mas de Bosc ; ils apportent la preuve qu'il n'y a pas de puits sur leur terrain ; ainsi, l'arrêté litigieux est entaché d'inexactitude matérielle et d'erreur manifeste d'appréciation ;  <br/>
       - la Cour devra diligenter une expertise dans le cas où elle estimerait que les éléments apportés ne permettent pas d'écarter tout risque minier ; <br/>
<br/>
       Vu l'ordonnance, en date du 30 juin 2014, rouvrant la clôture de l'instruction ;<br/>
<br/>
       Vu le mémoire enregistré le 9 janvier 2015 présenté par le ministre de l'économie, de l'industrie et du numérique qui conclut aux mêmes fins par les mêmes moyens ; <br/>
<br/>
       Vu, II, sous le n° 14LY00692, le recours enregistré le 6 mars 2014, présenté par le ministre du redressement productif ; <br/>
<br/>
       Le ministre du redressement productif demande à la Cour :<br/>
<br/>
       1°) de prononcer, en application de l'article R. 811-15 du code de justice administrative, le sursis à exécution du jugement n° 1105379-1200549 en date du 12 décembre 2013 du tribunal administratif de Lyon en tant qu'il a annulé la décision du préfet de l'Ardèche du 8 mars 2012 refusant d'abroger l'arrêté préfectoral n° 2011208-0004 du 27 juillet 2011 approuvant le PPRM sur le territoire des communes de Largentière, Chassiers, Montréal et Vinezac et qu'il a enjoint au préfet de l'Ardèche d'abroger cet arrêté dans le délai d'un mois suivant la notification du jugement ; <br/>
<br/>
       il soutient que c'est à tort que les premiers juges ont considéré que l'absence d'audition des maires des communes intéressées par le commissaire-enquêteur avait été susceptible d'exercer une influence sur le contenu du PPRM ; les maires ont été effectivement étroitement associés à l'élaboration et à l'établissement du PPRM ; leurs avis ayant pu être connus par ailleurs, le fait de ne pas les avoir auditionnés n'a pas eu d'influence sur le sens de la décision ; en outre, l'avis du commissaire-enquêteur ne lie pas l'autorité compétente qui, au demeurant, a pris connaissance des avis des maires ; enfin, le maire de Largentière ayant saisi le préfet de l'Ardèche d'une demande d'élaboration du PPRM, il a pu exposer son appréciation des risques de toute nature concernant le territoire de sa commune, qui a ainsi été nécessairement prise en compte dans le processus d'élaboration du document ; ainsi, la méconnaissance des dispositions de l'article R. 562-8 du code de l'environnement n'entache pas d'irrégularité l'arrêté litigieux ;  <br/>
<br/>
       Vu le jugement attaqué ;<br/>
<br/>
       Vu l'ordonnance, en date du 16 mai 2014, fixant la clôture d'instruction au 20 juin 2014, en application des articles R. 613-1 et R. 613-3 du code de justice administrative ; <br/>
<br/>
<br/>
       Vu le mémoire, enregistré le 20 juin 2014, présenté pour M. et Mme C...A...qui concluent : <br/>
<br/>
       1°) au rejet du recours ; <br/>
<br/>
       2°) à ce qu'une somme de 3 000 euros soit mise à la charge de l'Etat sur le fondement des dispositions de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
       ils soutiennent que : <br/>
<br/>
       - les maires des communes sur le territoire desquelles le plan doit s'appliquer n'ont pas été entendus, du moins s'agissant des maires de Chassiers, Vinezac et Montréal ; de même, les avis émis entre 2009 et 2010 par les conseils municipaux concernés n'ont pas été consignés, ni même annexés au registre d'enquête publique ; l'avis de la chambre d'agriculture n'a pas été requis alors que le zonage du plan intègre des terrains agricoles de Largentière ; ce vice de procédure constitue une irrégularité substantielle ; <br/>
       - les dispositions de la loi n° 2003-699 du 30 juillet 2003 n'ont pas été visées dans le cadre du dossier d'enquête, pas plus que la façon dont l'enquête s'insère dans la procédure administrative en cause ;  <br/>
       - l'arrêté préfectoral du 13 janvier 2006 prescrivant l'élaboration du PPRM et fixant les modalités de la concertation est irrégulier en ce qu'il ne prévoit pas suffisamment de modalités d'association des collectivités territoriales concernées, seuls leurs avis étant sollicités ; les dispositions des articles L. 562-3 et R. 562-3 du code de l'environnement ont ainsi été méconnues ; cet arrêté est également insuffisant quant aux modalités de la concertation avec la population qui ne prévoit qu'une seule réunion publique d'information ; les différentes formes de concertation avec le public qui auraient été organisées n'ont, en tout état de cause, pas été prévues par cet arrêté et révèlent ainsi l'incompétence négative du préfet, seule autorité compétente pour définir les modalités de la concertation ; le préfet n'établit en rien la réalité des éléments de concertation avec le public dont il fait état ; <br/>
       - la création d'une zone rouge, à cheval sur les parcelles cadastrées B1592 et B153 est intervenue alors même que tant le bureau d'étude que les services de l'Etat sont dans l'incertitude totale quant au positionnement du puits du Mas de Bosc ; ils apportent la preuve qu'il n'y a pas de puits sur leur terrain ; ainsi, l'arrêté litigieux est entaché d'inexactitude matérielle et d'erreur manifeste d'appréciation ;  <br/>
       - la Cour devra diligenter une expertise dans le cas où elle estimerait que les éléments apportés ne permettent pas d'écarter tout risque minier ; <br/>
<br/>
       Vu l'ordonnance, en date du 30 juin 2014, rouvrant la clôture de l'instruction ;<br/>
<br/>
       Vu les autres pièces du dossier ;<br/>
<br/>
       Vu le code minier ; <br/>
<br/>
       Vu le code de l'environnement ; <br/>
<br/>
       Vu le code de l'urbanisme ; <br/>
<br/>
       Vu le code de justice administrative ;<br/>
<br/>
       Les parties ayant été régulièrement averties du jour de l'audience ;<br/>
<br/>
       Après avoir entendu au cours de l'audience publique du 13 janvier 2015 :<br/>
<br/>
- le rapport de Mme Dèche, premier conseiller ;<br/>
- les conclusions de M. Clément, rapporteur public ;<br/>
       - et les observations de MeB..., représentant M. et MmeA... ;<br/>
<br/>
<br/>
       1. Considérant que, par deux requêtes distinctes, le ministre du redressement productif  demande à la Cour d'ordonner le sursis à exécution puis de prononcer l'annulation du jugement en date du 12 décembre 2013 du tribunal administratif de Lyon en tant qu'il a annulé la décision du préfet de l'Ardèche refusant d'abroger l'arrêté préfectoral n° 2011208-0004 du 27 juillet 2011 approuvant le PPRM sur le territoire des communes de Largentière, Chassiers, Montréal et Vinezac et qu'il a enjoint au préfet de l'Ardèche d'abroger cet arrêté dans le délai d'un mois suivant la notification du jugement ;  <br/>
<br/>
       2. Considérant qu'il y a lieu de joindre ces deux requêtes pour statuer par un seul arrêt ; <br/>
<br/>
       Sur la requête n° 14LY00558 :<br/>
<br/>
       En ce qui concerne l'annulation de la décision du préfet de l'Ardèche du 8 mars 2012 refusant d'abroger son arrêté du 27 juillet 2011 : <br/>
<br/>
       3. Considérant qu'aux termes de l'article L. 174-5 du nouveau code minier : " L'Etat élabore et met en  oeuvre des plans de prévention des risques miniers, dans les conditions prévues aux articles L. 562-1 à L. 562-7 du code de l'environnement, relatifs aux plans de prévention des risques naturels prévisibles. Ces plans emportent les mêmes effets que les plans de prévention des risques naturels prévisibles. Toutefois, les dispositions de l'article 13 de la loi n° 95-101 du 2 février 1995 relative au renforcement de la protection de l'environnement ne leur sont pas applicables. " ; qu'aux termes de l'article L. 562-3 du code de l'environnement : " Le préfet définit les modalités de la concertation relative à l'élaboration du projet de plan de prévention des risques naturels prévisibles. / Sont associés à l'élaboration de ce projet les collectivités territoriales et les établissements publics de coopération intercommunale concernés. / Après enquête publique réalisée conformément au chapitre III du titre II du livre Ier et après avis des conseils municipaux des communes sur le territoire desquelles il doit s'appliquer, le plan de prévention des risques naturels prévisibles est approuvé par arrêté préfectoral. Au cours de cette enquête, sont entendus, après avis de leur conseil municipal, les maires des communes sur le territoire desquelles le plan doit s'appliquer. " ; qu'aux termes de l'article R. 562-7 du même code : " Le projet de plan de prévention des risques naturels prévisibles est soumis à l'avis des conseils municipaux des communes et des organes délibérants des établissements publics de coopération intercommunale compétents pour l'élaboration des documents d'urbanisme dont le territoire est couvert en tout ou partie par le plan / (...) / Si le projet de plan concerne des terrains agricoles ou forestiers, les dispositions relatives à ces terrains sont soumises à l'avis de la chambre d'agriculture et du centre régional de la propriété forestière / Tout avis demandé en application des trois alinéas ci-dessus qui n'est pas rendu dans un délai de deux mois à compter de la réception de la demande est réputé favorable. " ; que l'article R. 562-8 dispose quant à lui : " Le projet de plan est soumis par le préfet à une enquête publique dans les formes prévues par les articles R. 123-6 à R. 123-23, sous réserve des dispositions des deux alinéas qui suivent. / Les avis recueillis en application des trois premiers alinéas de l'article R. 562-7 sont consignés ou annexés aux registres d'enquête dans les conditions prévues par l'article R. 123-17. / Les maires des communes sur le territoire desquelles le plan doit s'appliquer sont entendus par le commissaire enquêteur ou par la commission d'enquête une fois consigné ou annexé aux registres d'enquête l'avis des conseils municipaux. " ; qu'aux termes, enfin, du premier alinéa de l'article R. 123-17 dudit code, applicable à la date d'ouverture de l'enquête publique : " Pendant la durée de l'enquête, les appréciations, suggestions et contre-propositions du public peuvent être consignées sur le registre d'enquête tenu à leur disposition dans chaque lieu où est déposé un dossier ; ce registre, établi sur feuillets non mobiles, est coté et paraphé par le commissaire enquêteur, le président de la commission d'enquête ou un membre de celle-ci. " ; <br/>
<br/>
       4. Considérant que si le ministre du redressement productif fait valoir que l'arrêté préfectoral du 9 septembre 2010 de mise à l'enquête publique qui vise les avis des conseils municipaux des communes de Montréal, Chassiers, Largentière et Vinezac, avis émis respectivement les 11 mai 2009, 27 avril 2010 et 31 mai 2010, le conseil municipal de Montréal n'ayant pas émis d'avis, il ne ressort pas des pièces du dossier que ces avis auraient été consignés ou annexés au registre d'enquête publique ; que si le ministre fait également valoir que plusieurs réunions auraient été organisées avec les maires des communes concernées en 2003, 2005 et 2006, il ne ressort pas des pièces du dossier que les maires des communes de Montréal, Chassiers et Vinezac auraient été entendus par le commissaire enquêteur dans les conditions prévues par les dispositions susrappelées ; qu'enfin, il n'est pas plus établi en appel qu'en première instance que les terrains agricoles de Largentière inclus dans le zonage du PPRM ne seraient pas affectés par ce plan et que l'avis de la chambre d'agriculture n'avait pas à être recueilli pour être consigné ou annexé au registre d'enquête publique ; qu'en se bornant à faire valoir que le rapport d'enquête et l'avis motivé de la commission d'enquête font état des avis des conseils municipaux susmentionnés ainsi que d'observations émises par le maire de Largentière, le ministre n'établit pas que le public aurait bénéficié de garanties équivalentes à celles prévues par les dispositions précitées du code de l'environnement concernant notamment la production complète des avis des conseils municipaux ainsi que l'audition effective des maires des communes concernées ; qu'ainsi, ces irrégularités ont pu être de nature à nuire à l'information de l'ensemble des personnes intéressées par l'opération ; qu'il suit de là que le ministre du redressement productif n'est pas fondé à soutenir que c'est à tort que le tribunal administratif de Lyon a estimé que la décision du préfet du l'Ardèche en date du 8 mars 2012 portant refus d'abroger son arrêté du 27 juillet 2011 était entaché d'illégalité pour ces motifs ; <br/>
<br/>
       En ce qui concerne l'injonction prononcée par le Tribunal : <br/>
<br/>
       5. Considérant qu'aux termes de l'article L. 911-1 du code de justice administrative : " Lorsque sa décision implique nécessairement qu'une personne morale de droit public (...) prenne une mesure d'exécution dans un sens déterminé, la juridiction, saisie de conclusions en ce sens, prescrit, par la même décision, cette mesure assortie, le cas échéant, d'un délai d'exécution. " ;<br/>
<br/>
       6. Considérant que M. et Mme A...avaient demandé aux premiers juges d'annuler la décision de refus du préfet de l'Ardèche, d'abroger son arrêté du 27 juillet 2011 dans son intégralité, et non pas uniquement en tant qu'il concernait la commune de Largentière où se situaient leurs parcelles ; que contrairement à ce que soutient le ministre, les intéressés justifiaient d'un intérêt à agir à l'encontre de l'ensemble de cet arrêté alors même qu'il présente le caractère d'un acte divisible ; que les moyens d'annulation précédemment exposés, retenus par le Tribunal, impliquaient l'abrogation de l'intégralité du PPRM, document dont le contenu doit s'apprécier globalement ; que par suite, le ministre du redressement productif n'est pas fondé à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Lyon a enjoint au préfet de l'Ardèche d'abroger son arrêté du 27 juillet 2011 dans le délai qu'il a prescrit ; <br/>
<br/>
       7. Considérant qu'il résulte de ce qui précède que le ministre du redressement productif  n'est pas fondé à soutenir que c'est à tort que le tribunal administratif de Lyon, après avoir annulé la décision en date du 8 mars 2012, par laquelle le préfet de l'Ardèche a refusé d'abroger l'arrêté du 27 juillet 2011 approuvant le PPRM sur le territoire des communes de Largentière, Chassiers, Montréal et Vinezac, a enjoint audit préfet d'abroger cet arrêté dans le délai d'un mois suivant la notification du jugement ; <br/>
<br/>
       Sur la requête n° 14LY00692 :<br/>
<br/>
       8. Considérant que, dès lors qu'il est statué au fond sur les conclusions à fin d'annulation du jugement attaqué, les conclusions tendant à ce que la Cour prononce le sursis à exécution de ce jugement sont devenues sans objet ;<br/>
<br/>
       Sur les conclusions de M. et Mme A...tendant au bénéfice des dispositions de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
       9. Considérant qu'en application des dispositions de l'article L. 761-1 du code de justice administrative, il y a lieu de faire droit aux conclusions de M. et Mme A...en mettant à la charge de l'Etat une somme de 1 500 euros au titre des frais exposés par eux et non compris dans les dépens ;<br/>
<br/>
<br/>
<br/>
       DECIDE :<br/>
<br/>
<br/>
<br/>
       Article 1er : La requête n° 14LY00558 du ministre du redressement productif est rejetée.<br/>
<br/>
       Article 2 : Il n'y a pas lieu de statuer sur la requête n° 14LY00692.<br/>
<br/>
       Article 3 : L'Etat versera à M. et Mme A...une somme de 1 500 euros en application des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
       Article 4 : Le présent arrêt sera notifié au ministre de l'économie, de l'industrie et du numérique et à M. et Mme C...A.... <br/>
<br/>
<br/>
       Délibéré après l'audience du 13 janvier 2015 à laquelle siégeaient :<br/>
<br/>
       M. Martin, président de chambre,<br/>
       Mme Courret, président-assesseur,<br/>
       Mme Dèche, premier conseiller.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
       Lu en audience publique, le 3 février 2015.<br/>
<br/>
''<br/>
''<br/>
''<br/>
''<br/>
1<br/>
2<br/>
N° 14LY00558...	<br/>
lt<br/>
<br/>
<br/>
<br/>
</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">68-01 Urbanisme et aménagement du territoire. Plans d'aménagement et d'urbanisme.
</SCT>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
