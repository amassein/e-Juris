<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027069231</ID>
<ANCIEN_ID>JG_L_2013_02_000000348006</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/06/92/CETATEXT000027069231.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème et 8ème sous-sections réunies, 15/02/2013, 348006, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-02-15</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>348006</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème et 8ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MONOD, COLIN</AVOCATS>
<RAPPORTEUR>Mme Anne Egerszegi</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emmanuelle Cortot-Boucher</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:348006.20130215</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 31 mars et 30 juin 2011 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. et Mme A...B..., demeurant... ; M. et Mme B... demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 09VE01840 du 18 janvier 2011 par lequel la cour administrative d'appel de Versailles a rejeté l'appel qu'ils ont interjeté du jugement du 9 avril 2009 du tribunal administratif de Versailles rejetant leur demande tendant à la décharge des cotisations supplémentaires d'impôt sur le revenu et de contributions sociales auxquelles ils ont été assujettis au titre de l'année 2002 ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à leur appel ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 10 000 euros au titre de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code général des impôts et le livre des procédures fiscales ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Anne Egerszegi, Maître des Requêtes,<br/>
<br/>
              - les observations de la SCP Monod, Colin, avocat de M. et MmeB...,<br/>
<br/>
              - les conclusions de Mme Emmanuelle Cortot-Boucher, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Monod, Colin, avocat de M. et Mme B...;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond qu'à l'occasion de l'augmentation de capital de la société Financière La Galise, le 15 novembre 2000, Mme B...a apporté 41 650 bons de souscription autonomes (BSA) de la société Agate Investissements en rémunération desquels 7437 actions nouvelles et 74 370 BSA de la société La Galise lui ont été attribuées ; que le 9 juillet 2001, ces BSA Financière La Galise ont été transformés en BSA Agate Investissements à la suite de la fusion-absorption de la première société par la seconde ; que, le 2 décembre 2002, Mme B...a cédé les BSA Agate Investissements à la société Grande Paroi dont elle était la gérante-associée ; que, à l'occasion de la vérification de comptabilité de la société Grande Paroi, il a été constaté que Mme B...n'avait pas déclaré de plus-value à raison de cette opération de cession ; que l'administration fiscale a réintégré le montant total du prix de la vente des bons dans les revenus de la contribuable au titre de l'année 2002 et soumis ce montant au taux proportionnel de 16 % sur le fondement des dispositions de l'article 150-0 A du code général des impôts ; que M. et Mme B... se pourvoient en cassation contre l'arrêt du 18 janvier 2011 par lequel la cour administrative d'appel de Versailles a rejeté leur requête tendant à l'annulation du jugement du 9 avril 2009 du tribunal administratif de Versailles rejetant leur demande tendant à la décharge des cotisations supplémentaires d'impôt sur le revenu et de contributions sociales auxquelles ils ont été assujettis à raison de ce redressement ;<br/>
<br/>
              Sans qu'il soit besoin d'examiner l'autre moyen du pourvoi ; <br/>
<br/>
              2. Considérant qu'aux termes de l'article 150-0-A du code général des impôts dans sa rédaction applicable à l'année 2002 : " I. - 1. (...) les gains nets retirés des cessions à titre onéreux, effectuées directement ou par personne interposée, de valeurs mobilières (...), de droits portant sur ces valeurs (...) sont soumis à l'impôt sur le revenu lorsque le montant de ces cessions excède, par foyer fiscal, 7 650 euros par an " ; qu'aux termes du premier alinéa de l'article 150-0-B du même code dans sa rédaction applicable au litige : " Les dispositions de l'article 150-0 A ne sont pas applicables, au titre de l'année de l'échange des titres, aux plus-values réalisées dans le cadre d'une opération (...) de fusion (...), réalisée conformément à la réglementation en vigueur (...) " ; qu'aux termes de l'article 150-0 D de ce code : "  1. Les gains nets mentionnés au I de l'article 150-0 A sont constitués par la différence entre le prix effectif de cession des titres ou droits, net des frais et taxes acquittés par le cédant, et leur prix effectif d'acquisition par celui-ci ou, en cas d'acquisition à titre gratuit, leur valeur retenue pour la détermination des droits de mutation / (...) / 9. En cas de vente ultérieure de titres reçus à l'occasion d'une opération mentionnée à l'article 150-0 B, le gain net est calculé à partir du prix ou de la valeur d'acquisition des titres échangés, diminué de la soulte reçue ou majoré de la soulte versée lors de l'échange " ; qu'il résulte de ces dispositions que, sous réserve de l'application du régime de sursis d'imposition prévu à l'article 150-0 B du code général des impôts, dans le cas d'opérations d'échange de titres réalisées conformément à la réglementation en vigueur, le gain net issu de la cession à titre onéreux de valeurs mobilières et de droits afférents donne lieu à taxation immédiate selon les modalités prévues au 1 de l'article 150-0 D du même code ; <br/>
<br/>
              3. Considérant qu'en jugeant qu'à supposer que l'opération d'échange de titres réalisée en 2001 n'entrât pas dans le champ d'application de l'article 150-0-B du code général des impôts, les requérants ne pouvaient soutenir que le gain net réalisé à l'occasion de cet échange devait faire l'objet d'une taxation immédiate et que, par suite, lors de la vente, en 2002, des titres reçus à l'échange, la valeur d'acquisition à prendre en compte était celle de ces titres à la date de l'échange, la cour a commis une erreur de droit ; que dès lors, M. et Mme B...sont fondés à demander l'annulation de l'arrêt qu'ils attaquent ; <br/>
<br/>
              4. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 3 000 euros à verser à M. et MmeB..., au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 18 janvier 2011 de la cour administrative d'appel de Versailles est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Versailles.<br/>
Article 3 : L'Etat versera à M. et Mme B...la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
Article 4 : La présente décision est notifiée à M. et Mme B...et au ministre de l'économie et des finances. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
