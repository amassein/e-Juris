<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038244635</ID>
<ANCIEN_ID>JG_L_2019_03_000000414814</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/24/46/CETATEXT000038244635.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère et 4ème chambres réunies, 18/03/2019, 414814</TITRE>
<DATE_DEC>2019-03-18</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>414814</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère et 4ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROCHETEAU, UZAN-SARANO ; SCP BOULLEZ</AVOCATS>
<RAPPORTEUR>M. Frédéric Pacoud</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Charles Touboul</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:414814.20190318</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A...B...a demandé au tribunal administratif de Marseille de condamner Pôle emploi à lui verser la somme de 28 214,44 euros, sous réserve d'actualisation ultérieure, en réparation du préjudice qu'il estime avoir subi en raison du défaut d'information sur ses droits à l'allocation équivalent retraite et de versement de cette allocation. Par un jugement n° 1304281 du 29 septembre 2015, le tribunal administratif de Marseille a rejeté sa demande.<br/>
<br/>
              Par une décision n° 397162 du 31 mars 2017, le Conseil d'Etat a, sur le pourvoi de M.B..., annulé ce jugement et renvoyé l'affaire au tribunal administratif de Marseille.<br/>
<br/>
              Par un jugement n° 1702537 du 11 juillet 2017, statuant sur renvoi du Conseil d'Etat, le tribunal administratif de Marseille a rejeté la demande de M.B....<br/>
<br/>
              Par une ordonnance n° 17MA03635 du 19 septembre 2017, enregistrée le 3 octobre suivant au secrétariat du contentieux du Conseil d'Etat, le président de la cour administrative d'appel de Marseille a transmis au Conseil d'Etat, en application de l'article R. 351-2 du code de justice administrative, le pourvoi, enregistré le 17 août 2017 au greffe de cette cour, présenté par M.B.... Par ce pourvoi et par un mémoire complémentaire, enregistré le 12 janvier 2018 au secrétariat du contentieux du Conseil d'Etat, M. B...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler le jugement du tribunal administratif de Marseille du 11 juillet 2017 ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à ses conclusions de première instance ;<br/>
<br/>
              3°) de mettre à la charge de Pôle emploi la somme de 3 000 euros sur le fondement de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de la sécurité sociale ;<br/>
              - le code du travail ;<br/>
              - la loi n° 2007-1822 du 24 décembre 2007 ;<br/>
              - le décret n° 2009-608 du 29 mai 2009 ;<br/>
              - le décret n° 2010-458 du 6 mai 2010 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Frédéric Pacoud, maître des requêtes, <br/>
<br/>
              - les conclusions de M. Charles Touboul, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Rocheteau, Uzan-Sarano, avocat de M. B...et à la SCP Boullez, avocat de Pôle emploi.<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. En application des articles L. 5423-18 à L. 5423-23 du code du travail en vigueur jusqu'au 1er janvier 2009, les demandeurs d'emploi justifiant, avant l'âge de soixante ans, de la durée de cotisation à l'assurance vieillesse requise pour l'ouverture du droit à une pension de vieillesse à taux plein, validée dans les régimes de base obligatoires d'assurance vieillesse, ainsi que de celle des périodes reconnues équivalentes, pouvaient prétendre, sous condition de ressources, au versement de l'allocation équivalent retraite, qui se substituait alors à l'allocation de solidarité spécifique. L'article 132 de la loi du 24 décembre 2007 de finances pour 2008 a abrogé ces dispositions à compter du 1er janvier 2009, tout en permettant la poursuite du versement de l'allocation équivalent retraite, jusqu'à l'expiration de leurs droits, aux personnes en bénéficiant à cette date. Les décrets des 29 mai 2009 et 6 mai 2010 instituant à titre exceptionnel une allocation équivalent retraite pour certains demandeurs d'emploi ont temporairement rétabli, pour certains demandeurs d'emploi, la possibilité de solliciter une telle allocation, à condition qu'ils en fassent la demande au plus tard le 31 décembre 2010.<br/>
<br/>
              2. Il ressort des pièces du dossier soumis aux juges du fond que M. B... a perçu, en sa qualité de demandeur d'emploi, l'allocation d'aide au retour à l'emploi jusqu'au 25 février 2009 et que, par une lettre du 19 mars 2009, Pôle emploi lui a notifié son admission à l'allocation de solidarité spécifique à compter du 26 février 2009. Par une décision du 30 mars 2011, Pôle emploi a rejeté la demande de M. B...tendant à ce qu'il bénéficie de l'allocation équivalent retraite, au motif que cette demande aurait dû être déposée avant le 31 décembre 2010. M. B...a saisi Pôle emploi d'une demande tendant à la réparation du préjudice qu'il estime avoir subi en raison d'une défaillance dans son suivi personnel ayant conduit à le priver du bénéfice de l'allocation à laquelle il estimait pouvoir prétendre à compter du 26 février 2009. A la suite du rejet de cette demande, il a saisi le tribunal administratif de Marseille de conclusions indemnitaires, qui doivent être regardées comme dirigées contre l'Etat, au nom et pour le compte duquel Pôle emploi assurait le versement de l'allocation. Par un jugement du 11 juillet 2017, statuant de nouveau sur la demande indemnitaire de M. B...après annulation de son premier jugement et renvoi de l'affaire par le Conseil d'Etat, le tribunal l'a rejetée comme irrecevable au motif que l'intéressé avait disposé d'une voie de recours parallèle pour contester la décision de refus du 30 mars 2011.<br/>
<br/>
              3. Dès lors qu'une décision ayant un objet exclusivement pécuniaire est devenue définitive avec toutes les conséquences pécuniaires qui en sont inséparables, toute demande ultérieure présentée devant le tribunal administratif qui, fondée sur la seule illégalité de cette décision, tend à l'octroi d'une indemnité correspondant aux montants non versés ou illégalement réclamés est irrecevable. <br/>
<br/>
              4. Le tribunal administratif de Marseille a relevé, d'une part, que la décision du 30 mars 2011 par laquelle Pôle emploi avait refusé à M. B...le bénéfice de l'allocation équivalent retraite était devenue définitive, ainsi qu'il l'avait précédemment jugé par une ordonnance du 10 avril 2012 rejetant comme tardive sa demande tendant à l'annulation de cette décision, et, d'autre part, que la nouvelle demande dont il était saisi tendait à l'obtention d'une indemnité correspondant à la différence entre le montant de l'allocation équivalent retraite et le montant perçu au titre de l'allocation de solidarité spécifique. En déduisant de ces circonstances que la demande indemnitaire de M. B...était irrecevable en raison du recours parallèle dont il avait disposé pour contester la décision de Pôle emploi du 30 mars 2011, alors qu'il ressortait des pièces du dossier qui lui était soumis que sa demande n'était pas fondée sur l'illégalité de cette décision mais sur des agissements fautifs de Pôle emploi, qui ne lui auraient pas permis de demander le bénéfice de l'allocation équivalent retraite dans le délai requis par les décrets mentionnés ci-dessus des 29 mai 2009 et 6 mai 2010, le tribunal administratif a commis une erreur de droit. Par suite, le requérant est fondé à demander l'annulation du jugement qu'il attaque. Ce moyen suffisant à entraîner cette annulation, il n'est pas nécessaire d'examiner les autres moyens du pourvoi. <br/>
<br/>
              5. Aux termes du second alinéa de l'article L. 821-2 du code de justice administrative : " Lorsque l'affaire fait l'objet d'un second pourvoi en cassation, le Conseil d'Etat statue définitivement sur cette affaire ". Le Conseil d'Etat étant saisi, en l'espèce, d'un second pourvoi en cassation, il lui incombe de régler l'affaire au fond.<br/>
<br/>
              6. En vertu du 2° de l'article L. 5312-1 du code du travail, dans sa rédaction applicable au litige, Pôle emploi a notamment une mission d'information, d'orientation et d'accompagnement des personnes, qu'elles disposent ou non d'un emploi, à la recherche d'un emploi, d'une formation ou d'un conseil professionnel. Par ailleurs, aux termes du 4° du même article, il lui revient d'assurer " pour le compte de l'organisme gestionnaire du régime d'assurance chômage, le service de l'allocation d'assurance et, pour le compte de l'Etat ou du Fonds de solidarité prévu à l'article L. 5423-24, le service des allocations de solidarité prévues à la section 1 du chapitre III du titre II du livre IV de la présente partie, (...) ainsi que de toute autre allocation ou aide dont l'Etat lui confierait le versement par convention (...) ", comme l'allocation équivalent retraite. L'article R. 5411-4 du même code dispose que : " Lors de son inscription, le travailleur recherchant un emploi est informé de ses droits et obligations ". Il résulte de ces dispositions que, dans le cadre de sa mission de service du revenu de remplacement, outre qu'il est tenu de répondre aux demandes d'information dont il est saisi, Pôle emploi doit, d'une part, à tout moment et notamment en cas de création ou de modification substantielle des conditions d'octroi d'une allocation, diffuser une information générale à l'attention des personnes à la recherche d'un emploi sur les allocations dont il assure le service à ce titre et, d'autre part, lorsqu'une personne s'inscrit en qualité de demandeur d'emploi ou parvient à la fin de ses droits à l'allocation d'assurance,  l'informer personnellement de celles de ces allocations auxquelles elle est susceptible d'avoir droit. <br/>
<br/>
              7. Il résulte de l'instruction que M.B..., inscrit en qualité de demandeur d'emploi le 4 janvier 2009, a été admis au bénéfice de l'allocation de solidarité spécifique à l'expiration de ses droits à l'assurance chômage, le 26 février 2009, à une date où l'allocation équivalent retraite, supprimée à compter du 1er janvier 2009 par l'article 132 de la loi du 24 décembre 2007, n'avait pas encore été rétablie. Il n'a formulé aucune demande visant à percevoir cette indemnité avant le 31 décembre 2010 et ne se prévaut d'aucune demande de renseignement relative à ses droits à allocation. Il résulte de ce qui a été dit au point précédent qu'il n'est pas fondé à rechercher la responsabilité de l'administration au motif qu'elle aurait manqué à son devoir d'information en ne prenant pas l'initiative de lui faire connaître personnellement qu'il était susceptible d'avoir droit à l'allocation équivalent retraite au regard des conditions posées par le décret précité du 29 mai 2009 et de lui en accorder le bénéfice au moment où la possibilité de la demander a été de nouveau ouverte. <br/>
<br/>
              8. Dès lors, sans qu'il soit besoin de statuer sur les fins de non-recevoir opposées par Pôle emploi, les conclusions indemnitaires de M. B...doivent être rejetées.<br/>
<br/>
              9. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à la charge de Pôle emploi, qui n'est pas, dans la présente instance, la partie perdante. Il n'y a pas lieu, dans les circonstances de l'espèce, de mettre une somme à la charge de M. B...au titre de ces dispositions.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : Le jugement du tribunal administratif de Marseille du 11 juillet 2017 est annulé. <br/>
Article 2 : La demande présentée par M. B...devant le tribunal administratif de Marseille est rejetée. <br/>
Article 3 : Les conclusions présentées par M. B...et par Pôle emploi au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
Article 4 : La présente décision sera notifiée à M. A...B..., à la ministre du travail et à Pôle emploi.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">60-02-013 RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. RESPONSABILITÉ EN RAISON DES DIFFÉRENTES ACTIVITÉS DES SERVICES PUBLICS. SERVICE PUBLIC DE L'EMPLOI. - CARENCES DE PÔLE EMPLOI DANS L'EXERCICE DE CES MISSIONS - MISSION DE SERVICE DU REVENU DE REMPLACEMENT INCOMBANT À PÔLE EMPLOI - DEVOIR D'INFORMATION DES DEMANDEURS D'EMPLOI SUR LES ALLOCATIONS - PORTÉE [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">66-11-001-01 TRAVAIL ET EMPLOI. SERVICE PUBLIC DE L'EMPLOI. - MISSION DE SERVICE DU REVENU DE REMPLACEMENT INCOMBANT À PÔLE EMPLOI - DEVOIR D'INFORMATION DES DEMANDEURS D'EMPLOI SUR LES ALLOCATIONS - PORTÉE [RJ1].
</SCT>
<ANA ID="9A"> 60-02-013 Il résulte des articles L. 5312-1 et R. 5411-4 du code du travail que, dans le cadre de sa mission de service du revenu de remplacement, outre qu'il est tenu de répondre aux demandes d'information dont il est saisi, Pôle emploi doit, d'une part, à tout moment et notamment en cas de création ou de modification substantielle des conditions d'octroi d'une allocation, diffuser une information générale à l'attention des personnes à la recherche d'un emploi sur les allocations dont il assure le service à ce titre et, d'autre part, lorsqu'une personne s'inscrit en qualité de demandeur d'emploi ou parvient à la fin de ses droits à l'allocation d'assurance, l'informer personnellement de celles de ces allocations auxquelles elle est susceptible d'avoir droit.</ANA>
<ANA ID="9B"> 66-11-001-01 Il résulte des articles L. 5312-1 et R. 5411-4 du code du travail que, dans le cadre de sa mission de service du revenu de remplacement, outre qu'il est tenu de répondre aux demandes d'information dont il est saisi, Pôle emploi doit, d'une part, à tout moment et notamment en cas de création ou de modification substantielle des conditions d'octroi d'une allocation, diffuser une information générale à l'attention des personnes à la recherche d'un emploi sur les allocations dont il assure le service à ce titre et, d'autre part, lorsqu'une personne s'inscrit en qualité de demandeur d'emploi ou parvient à la fin de ses droits à l'allocation d'assurance, l'informer personnellement de celles de ces allocations auxquelles elle est susceptible d'avoir droit.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr. Cass. soc. 8 février 2012, n°10-30.892, Bull. 2012, V, n° 65 ; s'agissant des organismes de sécurité sociale, Cass. civ. 2ème, 28 novembre 2013, n° 12-24.210, Bull. 2013, II, n° 227 ; Cass. civ. 2ème, 5 nov. 2015, n° 14-25.053, Bull. 2015, II, n° 244.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
