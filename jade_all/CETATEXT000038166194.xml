<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038166194</ID>
<ANCIEN_ID>JG_L_2019_02_000000425477</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/16/61/CETATEXT000038166194.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème chambre, 22/02/2019, 425477, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-02-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>425477</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème chambre</FORMATION>
<TYPE_REC>Autres</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MATUCHANSKY, POUPOT, VALDELIEVRE ; SCP FABIANI, LUC-THALER, PINATEL</AVOCATS>
<RAPPORTEUR>Mme Sara-Lou Gerber</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Sophie-Justine Lieber</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2019:425477.20190222</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Le président de l'université de Poitiers a engagé plusieurs poursuites disciplinaires contre M. B... A...devant la section disciplinaire de cette université. <br/>
<br/>
              Par une première décision du 19 juillet 2016, la section disciplinaire a infligé à M. B... A...la sanction d'interdiction d'exercer toute fonction d'enseignement dans tout établissement public d'enseignement supérieur pendant cinq ans, assortie de la privation de la moitié de son traitement, et a précisé que la sanction serait exécutoire immédiatement.<br/>
<br/>
              Par une deuxième décision du 16 juin 2017, la section disciplinaire a infligé à M. A... la sanction d'interdiction d'exercer toute fonction d'enseignement dans tout établissement public d'enseignement supérieur pendant trois ans, assortie de la privation de la totalité de son traitement et a précisé que la sanction serait exécutoire immédiatement.<br/>
<br/>
              Par une unique décision du 10 juillet 2018, le Conseil national de l'enseignement supérieur et de la recherche (CNESER), statuant en formation disciplinaire, a rejeté les appels formés par M. A... contre ces deux décisions et, sur les appels incidents de l'université de Poitiers, a ordonné la révocation de M. A....<br/>
<br/>
              Par une requête, enregistrée le 19 novembre 2018 au secrétariat du contentieux du Conseil d'Etat, M. A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'ordonner, en application de l'article R. 821-5 du code de justice administrative, qu'il soit sursis à l'exécution de cette décision du CNESER ;<br/>
<br/>
              2°) de mettre à la charge de l'université de Poitiers la somme de 3 000 euros à verser à la SCP Matuchansky, Poupot et Valdelievre au titre de des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 relative à l'aide juridique.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de l'éducation ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 7 février 2019, présentée par l'université de Poitiers. <br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Sara-Lou Gerber, auditeur,  <br/>
<br/>
              - les conclusions de Mme Sophie-Justine Lieber, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Matuchansky, Poupot, Valdelièvre, avocat de M. A...et à la SCP Fabiani, Luc-Thaler, Pinatel, avocat de l'université de Poitiers ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article R. 821-5 du code de justice administrative : " La formation de jugement peut, à la demande de l'auteur du pourvoi, ordonner qu'il soit sursis à l'exécution d'une décision juridictionnelle rendue en dernier ressort si cette décision risque d'entraîner des conséquences difficilement réparables et si les moyens invoqués paraissent, en l'état de l'instruction, sérieux et de nature à justifier, outre l'annulation de la décision juridictionnelle rendue en dernier ressort, l'infirmation de la solution retenue par les juges du fond ".<br/>
<br/>
              2. En l'état de l'instruction, aucun des moyens invoqués par la requête de M. A... ne peut être regardé comme étant de nature à justifier, en sus de l'éventuelle annulation de la décision attaquée, l'infirmation de la sanction de révocation retenue par les juges du fond. Il suit de là que M. A... n'est pas fondé à demander qu'il soit sursis à l'exécution de la décision qu'il attaque.<br/>
<br/>
              3. Les dispositions de l'article L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'université de Poitiers, qui n'est pas la partie perdante dans la présente instance. Dans les circonstances de l'espèce, il n'y a pas lieu de faire droit aux conclusions présentées par l'université au titre des mêmes dispositions.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de M. A... est rejetée.<br/>
Article 2 : Le surplus des conclusions de l'université de Poitiers, présenté au titre de l'article L. 761-1 du code de justice administrative, est rejeté.<br/>
Article 3 : La présente décision sera notifiée à M. B... A...et à l'université de Poitiers.<br/>
Copie en sera adressée à la ministre de l'enseignement supérieur et de la recherche.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
