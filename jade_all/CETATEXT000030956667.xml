<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030956667</ID>
<ANCIEN_ID>JG_L_2015_07_000000383163</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/95/66/CETATEXT000030956667.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème SSJS, 27/07/2015, 383163, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-07-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>383163</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème SSJS</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Marie Gautier-Melleray</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Fabienne Lambolez</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2015:383163.20150727</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B...A...a demandé au tribunal administratif de Cergy-Pontoise d'annuler la décision référencée " 48 SI " du 21 décembre 2012 par laquelle le ministre de l'intérieur a constaté la perte de validité de son permis de conduire pour solde de points nul, d'annuler les décisions de retrait de points récapitulées dans cette décision et d'enjoindre au ministre de lui restituer son permis affecté d'un capital de douze points. Par un jugement n° 1301434 du 3 juin 2014, le tribunal administratif a partiellement fait droit à sa demande en annulant les décisions de retrait de points consécutives aux infractions des 21 avril 2010, 1er juin 2010 et 21 juillet 2010 et en enjoignant à l'administration de réexaminer sa situation après lui avoir reconnu le bénéfice des points illégalement retirés.<br/>
<br/>
              Par un pourvoi enregistré le 28 juillet 2014 au secrétariat du contentieux du Conseil d'Etat, le ministre de l'intérieur demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter la demande de M.A....<br/>
<br/>
<br/>
<br/>
              Vu :<br/>
<br/>
              - le code de la route ;<br/>
<br/>
              - le code de procédure pénale ;<br/>
<br/>
              - le code de justice administrative ; <br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Marie Gautier-Melleray, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Fabienne Lambolez, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, par une décision du 21 décembre 2012, le ministre de l'intérieur a constaté la perte de validité du permis de conduire de M. A...pour solde de points nul ; que M. A...a demandé au tribunal administratif de Cergy-Pontoise d'annuler cette décision ainsi que les décisions de retraits de points antérieures qu'elle récapitulait et d'enjoindre au ministre de l'intérieur de lui restituer son permis de conduire affecté d'un capital de douze points ; que le ministre de l'intérieur se pourvoit en cassation contre le jugement du 3 juin 2014 en tant que le tribunal administratif y a partiellement fait droit à la demande de M. A...en annulant les trois retraits d'un point consécutifs aux infractions des 21 avril 2010, 1er juin 2010 et 21 juillet 2010 et en enjoignant au ministre de lui reconnaître le bénéfice des points en cause avant de réexaminer sa situation ; <br/>
<br/>
              2. Considérant que la délivrance, préalablement au règlement de l'amende, de l'information prévue par les articles L. 223-3 et R. 223-3 du code de la route constitue une condition de la légalité des décisions de retrait de points ; que le paiement par le contrevenant de l'amende forfaitaire majorée prévue par le second alinéa de l'article 529-2 du code de procédure pénale implique nécessairement qu'il ait préalablement reçu l'avis d'amende forfaitaire majorée rendu exécutoire par le ministère public ; qu'avant même qu'elles ne soient rendues obligatoires par un arrêté du 13 mai 2011 introduisant dans le code de procédure pénale un article A. 37-28, le formulaire d'avis d'amende forfaitaire majorée utilisé par l'administration était revêtu de mentions qui permettaient au contrevenant de comprendre qu'en l'absence de contestation de l'amende, il serait procédé à un retrait de points et portaient à sa connaissance l'ensemble des informations requises par les articles L. 223-3 et R. 223-3 ; qu'ainsi, le paiement de l'amende forfaitaire majorée suffit à établir que l'administration s'est acquittée envers le titulaire du permis de son obligation préalable de lui délivrer l'information requise, à moins que l'intéressé, à qui il appartient à cette fin de produire l'avis qu'il a nécessairement reçu, ne démontre s'être vu remettre un avis inexact ou incomplet ;<br/>
<br/>
              3. Considérant qu'il résulte de ce qui précède qu'en jugeant que le ministre de l'intérieur n'apportait pas la preuve que l'administration s'était acquittée envers M. A...de son obligation de lui délivrer l'information prévue par les articles L. 223-3 et R. 223-3 du code de la route à l'occasion des infractions des 21 avril 2010, 1er juin 2010 et 21 juillet 2010, alors, d'une part, que le ministre avait produit devant lui trois attestations de la trésorerie du paiement automatisé établissant le paiement par M. A...de l'amende forfaitaire majorée relative à ces infractions et, d'autre part, que le conducteur ne démontrait ni même n'alléguait avoir été destinataire d'avis inexacts ou incomplets, le tribunal administratif de Cergy-Pontoise a commis une erreur de droit ; que cette erreur justifie l'annulation de son jugement en tant qu'il annule les retraits de points consécutifs à ces infractions et enjoint au ministre de l'intérieur de reconnaître à M. A...le bénéfice des points en cause avant de réexaminer sa situation  ; <br/>
<br/>
              4. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler dans cette mesure l'affaire au fond, en application des dispositions de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              5. Considérant qu'ainsi qu'il a été dit, le ministre de l'intérieur a produit trois attestations de la trésorerie du paiement automatisé établissant le paiement par M. A...de l'amende forfaitaire majorée relative aux infractions des 21 avril 2010, 1er juin 2010 et 21 juillet 2010 ; que l'administration doit, dès lors, être regardée comme s'étant acquittée envers M. A...de son obligation de lui délivrer, préalablement au paiement de l'amende, l'information prévue par les articles L. 223-3 et R. 223-3 du code de la route ; que M. A...n'est, par suite, pas fondé à demander l'annulation des décisions de retrait de points correspondantes ; que ses conclusions à fins d'injonction de restitution de points ne peuvent, par suite, qu'être rejetées ;<br/>
<br/>
<br/>
<br/>
<br/>			D E C I D E :<br/>
              			--------------<br/>
<br/>
Article 1er : Les articles 2 et 3 du jugement du tribunal administratif de Cergy-Pontoise du 3 juin 2014 sont annulés. <br/>
Article 2 : Les conclusions présentées par M. A...devant le tribunal administratif de Cergy-Pontoise tendant à l'annulation des retraits de point consécutifs aux infractions des 21 avril 2010, 1er juin 2010 et 21 juillet 2010 et à ce qu'il soit enjoint au ministre de rétablir les points correspondants sont rejetées. <br/>
Article 3 : La présente décision sera notifiée au ministre de l'intérieur et à M. B...A....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
