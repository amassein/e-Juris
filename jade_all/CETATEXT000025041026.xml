<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000025041026</ID>
<ANCIEN_ID>JG_L_2011_12_000000323612</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/25/04/10/CETATEXT000025041026.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème et 10ème sous-sections réunies, 22/12/2011, 323612</TITRE>
<DATE_DEC>2011-12-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>323612</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème et 10ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Philippe Martin</PRESIDENT>
<AVOCATS>RICARD ; SCP ROCHETEAU, UZAN-SARANO</AVOCATS>
<RAPPORTEUR>M. Philippe Josse</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Pierre Collin</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2011:323612.20111222</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête sommaire et le mémoire complémentaire, enregistrés les 24 décembre 2008 et 24 mars 2009 au secrétariat du contentieux du Conseil d'Etat, présentés pour l'UNION MUTUALISTE GENERALE DE PREVOYANCE, dont le siège est 28, rue Fortuny à Paris (75017), représentée par son président ; l'UNION MUTUALISTE GENERALE DE PREVOYANCE demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler la décision n° 2008/33 du 8 octobre 2008 par laquelle l'Autorité de contrôle des assurances et des mutuelles a prononcé un avertissement à son encontre ;<br/>
<br/>
              2°) de dire qu'il n'y a pas lieu de la sanctionner ;<br/>
<br/>
              3°) de mettre à la charge de l'Autorité de contrôle des assurances et des mutuelles le versement de la somme de 10 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 14 décembre 2011, présentée pour l'UNION MUTUALISTE GENERALE DE PREVOYANCE ;<br/>
<br/>
              Vu la Constitution, notamment son Préambule et ses articles 34, 61-1 et 66 ;<br/>
<br/>
              Vu la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
<br/>
              Vu l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
<br/>
              Vu le code des assurances ;<br/>
<br/>
              Vu le code de la mutualité ;<br/>
<br/>
              Vu le code de la sécurité sociale ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Philippe Josse, Conseiller d'Etat,  <br/>
<br/>
              - les observations de Me Ricard, avocat de l'UNION MUTUALISTE GENERALE DE PREVOYANCE et de la SCP Rocheteau, Uzan-Sarano, avocat de l'Autorité de contrôle des assurances et des mutuelles et de l'Autorité de contrôle prudentiel, <br/>
<br/>
              - les conclusions de M. Pierre Collin, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à Me Ricard, avocat de l'UNION MUTUALISTE GENERALE DE PREVOYANCE et à la SCP Rocheteau, Uzan-Sarano, avocat de l'Autorité de contrôle des assurances et des mutuelles et de l'Autorité de contrôle prudentiel ;<br/>
<br/>
<br/>
<br/>Sur la fin de non-recevoir opposée par l'Autorité de contrôle des assurances et des mutuelles :<br/>
<br/>
              Considérant que l'UNION MUTUALISTE GENERALE DE PREVOYANCE a produit la décision du 8 octobre 2008 prononçant un avertissement à son encontre dont elle demande l'annulation ; qu'ainsi, la fin de non-recevoir opposée par l'Autorité de contrôle des assurances et des mutuelles et tirée de ce que la requête ne serait pas accompagnée de la décision attaquée, en méconnaissance de l'article R. 412-1 du code de justice administrative, doit être écartée ;<br/>
<br/>
              Sur les questions prioritaires de constitutionnalité :<br/>
<br/>
              Considérant qu'aux termes du premier alinéa de l'article 23-5 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel : "Le moyen tiré de ce qu'une disposition législative porte atteinte aux droits et libertés garantis par la Constitution peut être soulevé (...) à l'occasion d'une instance devant le Conseil d'Etat (...)" ; qu'il résulte des dispositions de ce même article que le Conseil constitutionnel est saisi de la question prioritaire de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux ;<br/>
<br/>
              En ce qui concerne la conformité aux droits et libertés que la Constitution garantit de l'article L. 510-5 du code de la mutualité et des articles L. 951-4, L. 951-5 et L. 951-8 du code de la sécurité sociale :<br/>
<br/>
              Considérant que l'UNION MUTUALISTE GENERALE DE PREVOYANCE soutient que l'article L. 510-5 du code de la mutualité et les articles L. 951-4, L. 951-5 et L. 951-8 du code de la sécurité sociale, en vigueur à la date de la décision attaquée, sont contraires au principe d'inviolabilité du domicile, ainsi qu'au droit à un recours juridictionnel effectif qui découle de l'article 16 de la Déclaration des droits de l'homme et du citoyen du 26 août 1789, et que le législateur n'a pas exercé pleinement la compétence que lui confie la Constitution et, en particulier, son article 34 ; que toutefois, les dispositions critiquées, qui organisent le contrôle, par l'Autorité de contrôle des assurances et des mutuelles, des mutuelles et de leurs unions et fédérations, dans l'intérêt des adhérents de ces institutions, ne peuvent avoir pour effet de permettre aux contrôleurs relevant de l'Autorité de pénétrer dans les locaux des organismes contrôlés sans le consentement de représentants de ceux-ci, ni de permettre la saisie de pièces ou de documents ; que les contrôleurs, qui ne peuvent accéder qu'à des locaux à usage exclusivement professionnel, ne disposent d'aucune possibilité de contrainte matérielle ; que, dans l'hypothèse où les représentants de l'organisme contrôlé feraient obstacle à l'exercice des missions des contrôleurs, il appartient seulement aux autorités compétentes de prononcer les sanctions prévues par les textes ; que par suite, la question soulevée, qui n'est pas nouvelle, ne présente pas un caractère sérieux ; que, dès lors, il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité invoquée ;<br/>
<br/>
              En ce qui concerne la conformité aux droits et libertés que la Constitution garantit des articles L. 310-12, L. 310-12-1, L. 310-13, L. 310-14, L. 310-16 et L. 310-18 du code des assurances :<br/>
<br/>
              Considérant, en premier lieu, que les articles L. 310-13, L. 310-14, L. 310-16 et L. 310-18 du code des assurances, dans leur rédaction en vigueur à la date de la décision attaquée, régissent la procédure de contrôle et de sanction disciplinaire des entreprises d'assurance et autres organismes régis par le code des assurances ; qu'il n'en a pas été fait application à l'UNION MUTUALISTE GENERALE DE PREVOYANCE, qui a été contrôlée et sanctionnée sur le fondement des articles L. 510-5 et suivants du code de la mutualité, alors en vigueur, qui constituent des dispositions différentes, quoique pour partie similaires à celles qui sont contestées ; que les articles L. 310-13, L. 310-14, L. 310-16 et L. 310-18 du code des assurances ne sont par conséquent pas applicables au présent litige ;<br/>
<br/>
              Considérant, en second lieu, que l'UNION MUTUALISTE GENERALE DE PREVOYANCE soutient que les articles L. 310-12 et L. 310-12-1 du code des assurances, dans leur rédaction en vigueur à la date de la décision attaquée, sont contraires aux principes d'indépendance et d'impartialité des juridictions ; que toutefois, ni l'article L. 310-12 du code des assurances, qui énonce en termes généraux les compétences de l'Autorité de contrôle des assurances et des mutuelles vis-à-vis, notamment, des entreprises d'assurance et des mutuelles, ni l'article L. 310-12-1 du même code, reproduit par l'article L. 510-1-1 du code de la mutualité, qui détermine principalement la composition de l'Autorité, ses modes de votation, le rôle de son président et la nature des services qui lui sont attachés, ne sont susceptibles, par eux-mêmes, de porter atteinte aux principes d'indépendance et d'impartialité des juridictions ; que par suite, la question soulevée, qui n'est pas nouvelle, ne présente pas un caractère sérieux ; que, dès lors, il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité invoquée ;<br/>
<br/>
              Sur la légalité de la décision attaquée :<br/>
<br/>
              Sans qu'il soit besoin d'examiner les autres moyens de la requête ;<br/>
<br/>
              Considérant qu'aux termes du premier paragraphe de l'article 6 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales : "Toute personne a droit à ce que sa cause soit entendue équitablement, publiquement (...) par un tribunal indépendant et impartial établi par la loi, qui décidera, soit des contestations sur ses droits et obligations de caractère civil, soit du bien-fondé de toute accusation en matière pénale dirigée contre elle (...)" ; que l'Autorité de contrôle des assurances et des mutuelles, lorsqu'elle est saisie d'agissements pouvant donner lieu à l'exercice de son pouvoir de sanction, doit être regardée comme décidant du bien-fondé d'accusations en matière pénale au sens de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ; que, compte tenu du fait que sa décision peut faire l'objet d'un recours de pleine juridiction devant le Conseil d'Etat, la circonstance que la procédure suivie devant elle ne serait pas en tous points conforme aux prescriptions de l'article 6, § 1, de cette convention n'est pas de nature à entraîner dans tous les cas une méconnaissance du droit à un procès équitable ; que, cependant, le moyen tiré de ce qu'elle aurait statué dans des conditions qui ne respecteraient pas le principe d'impartialité rappelé par cet article peut, eu égard à la nature, à la composition et aux attributions de cet organisme, être utilement invoqué à l'appui d'un recours formé devant le Conseil d'Etat à l'encontre de sa décision ;<br/>
<br/>
              Considérant qu'aux termes du premier aliéna de l'article L. 510-1 du code de la mutualité, dans sa rédaction applicable au présent litige : "Le contrôle de l'Etat sur les mutuelles, unions et fédérations régies par le présent code est exercé, dans l'intérêt de leurs membres et de leurs ayants droit, par l'Autorité de contrôle des assurances et des mutuelles instituée à l'article L. 310-12 du code des assurances" ; qu'aux termes du premier alinéa de l'article L. 510-3 du même code, alors en vigueur : "L'Autorité de contrôle veille au respect, par les mutuelles, unions et fédérations (...) des dispositions législatives et réglementaires du présent code" ; que le contrôle des mutuelles, unions et fédérations, effectué sur pièces et sur place, est, en vertu de l'article L. 510-5 du même code et de l'article L. 951-4 du code de la sécurité sociale, alors en vigueur, organisé par l'Autorité de contrôle, qui en définit les modalités ; qu'en vertu du premier alinéa de l'article L. 510-11 du même code, alors en vigueur : "Lorsqu'une mutuelle, une union (...) ou une fédération a enfreint une disposition législative ou réglementaire dans le domaine relevant du contrôle de l'Autorité ou a des pratiques qui mettent en péril sa marge de solvabilité ou l'exécution des engagements qu'elle a contractés envers les membres participants, ayants droit ou bénéficiaires, l'Autorité peut prononcer à son encontre, ou celle de ses dirigeants, l'une ou plusieurs des sanctions disciplinaires" que cet article énumère ; que la décision d'ouvrir une procédure disciplinaire est prise, en vertu de l'article R. 510-11 du même code, alors en vigueur, par l'Autorité de contrôle, saisie par son secrétaire général sur le fondement d'un rapport de contrôle réalisé par ses services ; qu'en vertu de l'article R. 510-13 du même code, alors en vigueur, le secrétaire général et les autres membres des services de l'Autorité participant à l'audience disciplinaire peuvent présenter des observations ;<br/>
<br/>
              Considérant que la possibilité conférée à une autorité administrative indépendante investie d'un pouvoir de sanction de se saisir de son propre mouvement d'affaires qui entrent dans le domaine de compétence qui lui est attribué n'est pas, en soi, contraire à l'exigence d'équité dans le procès énoncée par les stipulations de l'article 6 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ; que, toutefois, ce pouvoir doit être suffisamment encadré pour ne pas donner à penser que les membres de la formation disciplinaire tiennent les faits visés par la décision d'ouverture de la procédure ou la notification ultérieure des griefs comme d'ores et déjà établis ou leur caractère répréhensible au regard des règles ou principes à appliquer comme d'ores et déjà reconnu, en méconnaissance du principe d'impartialité rappelé par l'article 6 précité ; que cet encadrement est insuffisant dans le cas de l'Autorité de contrôle des assurances et des mutuelles, au regard de l'étendue des pouvoirs dont elle dispose, couvrant à la fois le contrôle des organismes relevant du code des assurances ou du code de la mutualité, la décision d'ouvrir une procédure disciplinaire et de définir les griefs reprochés, l'instruction de la procédure et le prononcé des sanctions disciplinaires ; qu'eu égard à l'insuffisance des garanties dont la procédure est entourée, la circonstance que les mêmes personnes se prononcent sur la décision de poursuivre, d'une part, et sur la sanction, d'autre part, est de nature à faire naître un doute objectivement justifié sur l'impartialité de cette autorité ;<br/>
<br/>
              Considérant qu'à la suite d'un contrôle réalisé à compter du 4 mai 2006, l'Autorité de contrôle des assurances et des mutuelles a, lors de sa séance du 23 juillet 2008, décidé d'ouvrir une procédure disciplinaire à l'encontre de l'UNION MUTUALISTE GENERALE DE PREVOYANCE, pour entrave au contrôle ; que le président de l'Autorité a notifié cette décision par lettre du 31 juillet 2008 ; que cette procédure disciplinaire s'est achevée par la décision de l'Autorité de prononcer, lors de sa séance du 8 octobre 2008, une sanction d'avertissement à l'encontre de l'UNION MUTUALISTE GENERALE DE PREVOYANCE ; qu'il résulte de l'instruction que cinq des sept membres du collège de l'Autorité ayant siégé le 8 octobre 2008 avaient également participé à la délibération du 23 juillet 2008, qui avait décidé l'ouverture de la procédure disciplinaire ; qu'il suit de là que l'UNION MUTUALISTE GENERALE DE PREVOYANCE est fondée à soutenir que la procédure suivie devant l'Autorité de contrôle des assurances et des mutuelles a méconnu l'exigence d'impartialité rappelée par l'article 6 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales et que la décision du 8 octobre 2008 doit, dès lors, être annulée ;<br/>
<br/>
              Sur les conclusions tendant à l'application de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat, avec imputation sur le budget de l'Autorité de contrôle prudentiel, le versement d'une somme de 2 000 euros à l'UNION MUTUALISTE GENERALE DE PREVOYANCE, au titre des dispositions de l'article L. 761-1 du code de justice administrative ; que ces dispositions font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'UNION MUTUALISTE GENERALE DE PREVOYANCE, qui n'est pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : Il n'y a pas lieu de renvoyer au Conseil constitutionnel les questions prioritaires de constitutionnalité soulevées par l'UNION MUTUALISTE GENERALE DE PREVOYANCE.<br/>
Article 2 : La décision n° 2008/33 du 8 octobre 2008 de l'Autorité de contrôle des assurances et des mutuelles est annulée.<br/>
Article 3 : L'Etat (Autorité de contrôle prudentiel) versera à l'UNION MUTUALISTE GENERALE DE PREVOYANCE une somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
Article 4 : Les conclusions présentées par l'Autorité de contrôle des assurances et des mutuelles au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée à l'UNION MUTUALISTE GENERALE DE PREVOYANCE et à l'Autorité de contrôle prudentiel.<br/>
Copie en sera adressée au Premier ministre, au ministre du travail, de l'emploi et de la santé, à la ministre du budget, des comptes publics et de la réforme de l'Etat, porte-parole du Gouvernement et au Conseil constitutionnel.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">12-01 ASSURANCE ET PRÉVOYANCE. ORGANISATION DE LA PROFESSION ET INTERVENTION DE LA PUISSANCE PUBLIQUE. - 1) POUVOIR CONFÉRÉ À L'AUTORITÉ DE CONTRÔLE DES ASSURANCES ET DES MUTUELLES DE SE SAISIR D'OFFICE - ENCADREMENT INSUFFISANT - CONSÉQUENCE - IDENTITÉ DES PERSONNES SE PRONONÇANT SUR LA DÉCISION DE POURSUIVRE ET SUR LA SANCTION - CIRCONSTANCE DE NATURE À FAIRE NAÎTRE UN DOUTE OBJECTIVEMENT JUSTIFIÉ SUR L'IMPARTIALITÉ DE L'AUTORITÉ - EXISTENCE - 2) APPLICATION EN L'ESPÈCE - MÉCONNAISSANCE DE L'EXIGENCE D'IMPARTIALITÉ RAPPELÉE PAR L'ARTICLE 6 DE LA CONVENTION EDH - EXISTENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">26-055-01-06 DROITS CIVILS ET INDIVIDUELS. CONVENTION EUROPÉENNE DES DROITS DE L'HOMME. DROITS GARANTIS PAR LA CONVENTION. DROIT À UN PROCÈS ÉQUITABLE (ART. 6). - 1) POSSIBILITÉ D'AUTOSAISINE CONFÉRÉE À UNE AUTORITÉ ADMINISTRATIVE INDÉPENDANTE INVESTIE D'UN POUVOIR DE SANCTION - COMPATIBILITÉ AVEC L'EXIGENCE D'IMPARTIALITÉ RAPPELÉE PAR L'ARTICLE 6 DE LA CONVENTION EDH - EXISTENCE - CONDITION - ENCADREMENT SUFFISANT DE CE POUVOIR D'AUTOSAISINE [RJ1] - 2) CAS DE L'AUTORITÉ DE CONTRÔLE DES ASSURANCES ET DES MUTUELLES - INSUFFISANCE DE L'ENCADREMENT DE LA PROCÉDURE - CONSÉQUENCE - IDENTITÉ DES PERSONNES SE PRONONÇANT SUR LA DÉCISION DE POURSUIVRE ET SUR LA SANCTION - CIRCONSTANCE DE NATURE À FAIRE NAÎTRE UN DOUTE OBJECTIVEMENT JUSTIFIÉ SUR L'IMPARTIALITÉ DE L'AUTORITÉ - EXISTENCE - 3) APPLICATION EN L'ESPÈCE - MÉCONNAISSANCE DE L'EXIGENCE D'IMPARTIALITÉ RAPPELÉE PAR L'ARTICLE 6 DE LA CONVENTION EDH - EXISTENCE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">42-01-01-05 MUTUALITÉ ET COOPÉRATION. MUTUELLES. QUESTIONS GÉNÉRALES. CONTRÔLE DE L'ETAT. - 1) POUVOIR CONFÉRÉ À L'AUTORITÉ DE CONTRÔLE DES ASSURANCES ET DES MUTUELLES DE SE SAISIR D'OFFICE - ENCADREMENT INSUFFISANT - CONSÉQUENCE - IDENTITÉ DES PERSONNES SE PRONONÇANT SUR LA DÉCISION DE POURSUIVRE ET SUR LA SANCTION - CIRCONSTANCE DE NATURE À FAIRE NAÎTRE UN DOUTE OBJECTIVEMENT JUSTIFIÉ SUR L'IMPARTIALITÉ DE L'AUTORITÉ - EXISTENCE - 2) APPLICATION EN L'ESPÈCE - MÉCONNAISSANCE DE L''EXIGENCE D'IMPARTIALITÉ RAPPELÉE PAR L'ARTICLE 6 DE LA CONVENTION EDH - EXISTENCE.
</SCT>
<SCT ID="8D" TYPE="PRINCIPAL">52-045 POUVOIRS PUBLICS ET AUTORITÉS ADMINISTRATIVES INDÉPENDANTES. AUTORITÉS ADMINISTRATIVES INDÉPENDANTES. - 1) POSSIBILITÉ D'AUTOSAISINE CONFÉRÉE À UNE AUTORITÉ ADMINISTRATIVE INDÉPENDANTE INVESTIE D'UN POUVOIR DE SANCTION - COMPATIBILITÉ AVEC L'EXIGENCE D'IMPARTIALITÉ RAPPELÉE PAR L'ARTICLE 6 DE LA CONVENTION EDH - EXISTENCE - CONDITION - ENCADREMENT SUFFISANT DE CE POUVOIR D'AUTOSAISINE [RJ1] - 2) CAS DE L'AUTORITÉ DE CONTRÔLE DES ASSURANCES ET DES MUTUELLES - INSUFFISANCE DE L'ENCADREMENT DE LA PROCÉDURE - CONSÉQUENCE - IDENTITÉ DES PERSONNES SE PRONONÇANT SUR LA DÉCISION DE POURSUIVRE ET SUR LA SANCTION - CIRCONSTANCE DE NATURE À FAIRE NAÎTRE UN DOUTE OBJECTIVEMENT JUSTIFIÉ SUR L'IMPARTIALITÉ DE L'AUTORITÉ - EXISTENCE - 3) APPLICATION EN L'ESPÈCE - MÉCONNAISSANCE DE L'EXIGENCE D'IMPARTIALITÉ RAPPELÉE PAR L'ARTICLE 6 DE LA CONVENTION EDH - EXISTENCE.
</SCT>
<ANA ID="9A"> 12-01 1) L'encadrement du pouvoir de l'Autorité de contrôle des assurances et des mutuelles (ACAM) de se saisir de son propre mouvement d'affaires qui entrent dans le domaine de compétence qui lui est attribué n'est, au regard de l'étendue des pouvoirs dont cette Autorité dispose, couvrant à la fois le contrôle des organismes relevant du code des assurances ou du code de la mutualité, la décision d'ouvrir une procédure disciplinaire et de définir les griefs reprochés, l'instruction de la procédure et le prononcé des sanctions disciplinaires, pas suffisant. Eu égard à l'insuffisance des garanties dont la procédure est entourée, la circonstance que les mêmes personnes se prononcent sur la décision de poursuivre, d'une part, et sur la sanction, d'autre part, est de nature à faire naître un doute objectivement justifié sur l'impartialité de cette autorité.... ...2) En l'espèce, annulation pour méconnaissance de l'exigence d'impartialité rappelée par l'article 6 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales (convention EDH) de la sanction d'avertissement prise par l'ACAM à l'issue d'une procédure disciplinaire ouverte, à la suite d'un contrôle, par cette Autorité à l'encontre d'une mutuelle, à l'adoption de laquelle ont participé cinq des sept membres du collège qui avaient également participé à la délibération ayant décidé l'ouverture de la procédure.</ANA>
<ANA ID="9B"> 26-055-01-06 1) La possibilité conférée à une autorité administrative indépendante investie d'un pouvoir de sanction de se saisir de son propre mouvement d'affaires qui entrent dans le domaine de compétence qui lui est attribué n'est pas, en soi, contraire à l'exigence d'équité dans le procès énoncée par les stipulations de l'article 6 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales (convention EDH). Toutefois, ce pouvoir doit être suffisamment encadré pour ne pas donner à penser que les membres de la formation disciplinaire tiennent les faits visés par la décision d'ouverture de la procédure ou la notification ultérieure des griefs comme d'ores et déjà établis ou leur caractère répréhensible au regard des règles ou principes à appliquer comme d'ores et déjà reconnu, en méconnaissance du principe d'impartialité rappelé par l'article 6.... ...2) Cet encadrement est insuffisant dans le cas de l'Autorité de contrôle des assurances et des mutuelles (ACAM), au regard de l'étendue des pouvoirs dont elle dispose, couvrant à la fois le contrôle des organismes relevant du code des assurances ou du code de la mutualité, la décision d'ouvrir une procédure disciplinaire et de définir les griefs reprochés, l'instruction de la procédure et le prononcé des sanctions disciplinaires. Eu égard à l'insuffisance des garanties dont la procédure est entourée, la circonstance que les mêmes personnes se prononcent sur la décision de poursuivre, d'une part, et sur la sanction, d'autre part, est de nature à faire naître un doute objectivement justifié sur l'impartialité de cette autorité.... ...3) En l'espèce, annulation pour méconnaissance de l'exigence d'impartialité rappelée par l'article 6 de la convention EDH de la sanction d'avertissement prise par l'ACAM à l'issue d'une procédure disciplinaire ouverte, à la suite d'un contrôle, par cette Autorité à l'encontre d'une mutuelle, à l'adoption de laquelle ont participé cinq des sept membres du collège qui avaient également participé à la délibération ayant décidé l'ouverture de la procédure.</ANA>
<ANA ID="9C"> 42-01-01-05 1) L'encadrement du pouvoir de l'Autorité de contrôle des assurances et des mutuelles (ACAM) de se saisir de son propre mouvement d'affaires qui entrent dans le domaine de compétence qui lui est attribué n'est, au regard de l'étendue des pouvoirs dont cette Autorité dispose, couvrant à la fois le contrôle des organismes relevant du code des assurances ou du code de la mutualité, la décision d'ouvrir une procédure disciplinaire et de définir les griefs reprochés, l'instruction de la procédure et le prononcé des sanctions disciplinaires, pas suffisant. Eu égard à l'insuffisance des garanties dont la procédure est entourée, la circonstance que les mêmes personnes se prononcent sur la décision de poursuivre, d'une part, et sur la sanction, d'autre part, est de nature à faire naître un doute objectivement justifié sur l'impartialité de cette autorité.... ...2) En l'espèce, annulation pour méconnaissance de l'exigence d'impartialité rappelée par l'article 6 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales (convention EDH) de la sanction d'avertissement prise par l'ACAM à l'issue d'une procédure disciplinaire ouverte, à la suite d'un contrôle, par cette Autorité à l'encontre d'une mutuelle, à l'adoption de laquelle ont participé cinq des sept membres du collège qui avaient également participé à la délibération ayant décidé l'ouverture de la procédure.</ANA>
<ANA ID="9D"> 52-045 1) La possibilité conférée à une autorité administrative indépendante investie d'un pouvoir de sanction de se saisir de son propre mouvement d'affaires qui entrent dans le domaine de compétence qui lui est attribué n'est pas, en soi, contraire à l'exigence d'équité dans le procès énoncée par les stipulations de l'article 6 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales (convention EDH). Toutefois, ce pouvoir doit être suffisamment encadré pour ne pas donner à penser que les membres de la formation disciplinaire tiennent les faits visés par la décision d'ouverture de la procédure ou la notification ultérieure des griefs comme d'ores et déjà établis ou leur caractère répréhensible au regard des règles ou principes à appliquer comme d'ores et déjà reconnu, en méconnaissance du principe d'impartialité rappelé par l'article 6.... ...2) Cet encadrement est insuffisant dans le cas de l'Autorité de contrôle des assurances et des mutuelles (ACAM), au regard de l'étendue des pouvoirs dont elle dispose, couvrant à la fois le contrôle des organismes relevant du code des assurances ou du code de la mutualité, la décision d'ouvrir une procédure disciplinaire et de définir les griefs reprochés, l'instruction de la procédure et le prononcé des sanctions disciplinaires. Eu égard à l'insuffisance des garanties dont la procédure est entourée, la circonstance que les mêmes personnes se prononcent sur la décision de poursuivre, d'une part, et sur la sanction, d'autre part, est de nature à faire naître un doute objectivement justifié sur l'impartialité de cette autorité.... ...3) En l'espèce, annulation pour méconnaissance de l'exigence d'impartialité rappelée par l'article 6 de la convention EDH de la sanction d'avertissement prise par l'ACAM à l'issue d'une procédure disciplinaire ouverte, à la suite d'un contrôle, par cette Autorité à l'encontre d'une mutuelle, à l'adoption de laquelle ont participé cinq des sept membres du collège qui avaient également participé à la délibération ayant décidé l'ouverture de la procédure.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr. CEDH, 11 septembre 2009, Dubus S.A. contre France, n° 5242/04.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
