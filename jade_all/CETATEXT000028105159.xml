<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028105159</ID>
<ANCIEN_ID>JG_L_2013_10_000000371946</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/10/51/CETATEXT000028105159.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 15/10/2013, 371946, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-10-15</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>371946</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2013:371946.20131015</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 5 septembre 2013 au secrétariat du contentieux du Conseil d'Etat, présentée par le syndicat des négociants indépendants en métaux précieux (SNIMEP), dont le siège est 1, boulevard Malesherbes à Paris (75008) représenté par son président, le syndicat national des changeurs et auxiliaires financiers (SNCAF), dont le siège est 89, boulevard Magenta à Paris (75010), représenté par son président, la société Change et Collection, dont le siège est 1, rue Rouget de Lisle à Paris (75001), représentée par son directeur, la société AAEO Compagnie générale de bourse, dont le siège est 36, rue Vivienne à Paris (75002), représentée par son président, la société Galerie Les Chevaux Légers, dont le siège est 36, rue Vivienne à Paris (75002), représentée par son président, la société AOC Côte d'Azur, dont le siège est 3, rue de la République à Lyon (69001), représentée par son président, la société Gold by gold, dont le siège est 111, avenue Victor Hugo à Paris (75784 cedex 16), représentée par son président, la société Aucoffre.com, dont le siège est 9, allée de l'Arche à Courbevoie (92400), représentée par son président, la société Godot et fils net, dont le siège est 66, rue Vivienne à Paris (75002), représentée par son directeur général, la société Joubert groupe, dont le siège est 38 bis, rue Vivienne à Paris (75002), représentée par son président, la société Comptoir des tuileries, dont le siège est 53, rue Vivienne, à Paris (75002), représentée par sa gérante, et la société Sodima, dont le siège est 7, rue Marbeuf à Paris (75008), représentée par son président ; les requérants demandent au juge des référés du Conseil d'Etat :<br/>
<br/>
              1°) d'ordonner, sur le fondement de l'article L. 521-1 du code de justice administrative, la suspension de l'exécution de l'article 3 du décret n° 2013-417 du 21 mai 2013 portant modification du code des postes et des communications électroniques ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat le versement de la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              ils soutiennent que :<br/>
              - la condition d'urgence est remplie, car l'interdiction posée par le nouvel article D1 du code des postes et des communications électroniques obligera à faire appel, en lieu et place des envois en valeur déclarée par La Poste, à des entreprises de convoyeurs de fond, beaucoup plus coûteuses pour les requérants ; <br/>
              - le décret contesté n'entrait pas dans la compétence du Premier ministre ; <br/>
              - le décret contesté méconnaît le principe d'égalité devant la loi, puisqu'il interdit le transport postal des métaux précieux, mais autorise celui des bijoux, sans considération de leur valeur ; <br/>
              - le décret contesté viole le droit de la concurrence de l'Union européenne, en plaçant les entreprises françaises dans une situation défavorable par rapport à leurs concurrentes installées dans des pays où le transport de métaux précieux par voie postale est possible ; <br/>
              - le décret n'est pas compatible avec les dispositions de la directive 97/67/CE du Parlement européen et du Conseil du 15 décembre 1997 et avec les stipulations de la Convention postale universelle du 14 septembre 1994 ;<br/>
<br/>
<br/>
              Vu l'article 3 du décret n° 2013-417 du 21 mai 2013 dont la suspension de l'exécution est demandée ;<br/>
<br/>
              Vu la copie de la requête à fin d'annulation de ce décret en tant qu'il modifie, à son article 3, l'article D. 1 du code des postes et des communications électroniques ; <br/>
<br/>
              Vu le mémoire en défense, enregistré le 26 septembre 2013, présenté par le ministre du redressement productif ; il soutient que :<br/>
              - le décret contesté ne porte aucune atteinte grave et immédiate aux intérêts économiques des requérants ; <br/>
              - le pouvoir réglementaire pouvait compétemment prendre un tel texte sur le fondement de l'article L. 4 du code des postes et communications électroniques ; <br/>
              - la distinction introduite entre les métaux précieux et les bijoux s'explique par le caractère plus aisément négociable des premiers ; <br/>
              - le décret contesté ne méconnaît pas les textes de droit international qui régissent les activités postales, lesquels ne s'opposent pas à l'interdiction de certaines catégories d'objet dans les envois postaux ; <br/>
              - le décret contesté ne méconnaît pas le droit européen de la concurrence ;  <br/>
<br/>
              Vu les observations, enregistrées le 1er octobre 2013, présentées par le Premier ministre ;<br/>
<br/>
              Vu le mémoire en réplique, enregistré le 2 octobre 2013, présenté par le syndicat des négociants indépendants en métaux précieux (SNIMEP) et autres qui reprennent les conclusions de leur requête et les mêmes moyens ; ils soutiennent en outre que le décret litigieux est entaché d'une erreur manifeste d'appréciation dans la mesure où il va conduire les comptoirs à stocker leurs métaux précieux, les exposant ainsi à des attaques à main armée ;<br/>
<br/>
<br/>
              Vu le procès-verbal de l'audience publique du 3 octobre 2013 à 10 heures au cours de laquelle ont été entendus : <br/>
<br/>
              - les représentants du Syndicat des négociants indépendants en métaux précieux (SNIMEP) et autres ;<br/>
<br/>
              - les représentants du ministre du redressement productif ;<br/>
<br/>
              et à l'issue de laquelle l'instruction a été prolongée jusqu'au vendredi 11 octobre 2013 ;<br/>
<br/>
              Vu le nouveau mémoire, enregistré le 8 octobre 2013, présenté par le syndicat des négociants indépendants en métaux précieux (SNIMEP) et autres qui reprennent les conclusions de leur requête et les mêmes moyens ; ils soutiennent en outre que l'inintelligibilité du nouvel article D. 1 du code des postes et communications électroniques justifie l'urgence à en suspendre l'exécution ; que les interdictions posées par cet article ne sont pas dépourvues de sanctions pénales ; que l'impact financier de l'application du décret aux entreprises requérantes n'appelle pas une réponse évidente ; <br/>
<br/>
              Vu le nouveau mémoire, enregistré le 9 octobre 2013, présenté par le ministre du redressement productif ; il soutient que seuls les envois effectués par des services postaux, au sens de l'article L. 1, sont des envois postaux ; qu'une prochaine modification de l'article D. 1 viendra préciser que l'interdiction qu'il pose vise exclusivement les pièces métalliques ayant cours légal et pouvoir libératoire ; <br/>
<br/>
              Vu le nouveau mémoire, enregistré le 11 octobre 2013, présenté par le syndicat des négociants indépendants en métaux précieux (SNIMEP) et autres qui reprennent les conclusions de leur requête et les mêmes moyens ; ils acquiescent à l'interprétation du décret donnée par le ministre du redressement productif ;<br/>
<br/>
              Vu les autres pièces du dossier ; <br/>
<br/>
              Vu le code des postes et des communications électroniques, modifié notamment par le décret n°2013-417 du 21 mai 2013 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision " ; que l'urgence justifie la suspension de l'exécution d'une décision administrative lorsque celle-ci porte atteinte, de manière suffisamment grave et immédiate, à un intérêt public, à la situation du requérant ou aux intérêts qu'il entend défendre ;<br/>
<br/>
              2. Considérant que le syndicat des négociants indépendants en métaux précieux, le Syndicat national des changeurs et auxiliaires financiers, ainsi que dix entreprises qui ont pour activité le négoce de métaux précieux, demandent la suspension de l'exécution du nouvel article D. 1 du code des postes et communications électroniques, introduit par le décret du 21 mai 2013 portant modification du code des postes et des communications électroniques, qui dispose : " l'insertion de billets de banque, de pièces et de métaux précieux est interdite dans les envois postaux, y compris dans les envois à valeur déclarée, les envois recommandés et les envois faisant l'objet de formalités attestant leur dépôt et leur distribution. " ; <br/>
<br/>
              3. Considérant, d'une part, qu'aux termes de l'article L. 1 du code des postes et des communications électroniques : " Pour l'application du présent code, les services postaux sont la levée, le tri, l'acheminement et la distribution des envois postaux dans le cadre de tournées régulières./ Constitue un envoi postal tout objet destiné à être remis à l'adresse indiquée par l'expéditeur sur l'objet lui-même ou sur son conditionnement, y compris sous forme de coordonnées géographiques codées, et présenté dans la forme définitive dans laquelle il doit être acheminé. (...) " ; qu'il résulte de ces dispositions que l'interdiction, posée par les dispositions citées ci-dessus du nouvel article D. 1 du code des postes et des communications électroniques, ne s'applique pas aux objets transportés par les entreprises qui, en raison notamment de ce qu'elles n'effectuent pas de " tournées régulières ", n'ont pas la qualité de prestataires de services postaux ; <br/>
<br/>
              4. Considérant, d'autre part, qu'il résulte de l'instruction, et notamment des précisions apportées au cours de l'audience publique par le représentant du ministre chargé des postes, que la disposition contestée n'a pas pour objet d'interdire l'introduction dans les envois postaux des pièces n'ayant plus cours légal, et notamment des " Napoléons " en or ; que l'administration a notamment indiqué qu'un nouveau décret, modifiant le décret contesté, devait à courte échéance expressément préciser ce point et que, d'ici là, l'interdiction en litige serait appliquée conformément à cette interprétation ; <br/>
<br/>
              5. Considérant que, pour justifier l'urgence à suspendre les dispositions contestées, les requérants soutiennent que l'interdiction d'avoir désormais recours, pour le transport des pièces et métaux précieux, aux services peu onéreux de La Poste, entraîne un renchérissement du transport des pièces et métaux précieux achetés ou vendus par correspondance, ainsi que de l'acheminements des pièces et métaux précieux entre les comptoirs d'une même entreprise ; qu'ils soutiennent que ce surcoût affecte, de manière grave et immédiate, la marge bénéficiaire des négociants en métaux précieux et diminue, lorsqu'il est répercuté sur la clientèle, le volume d'achats et de ventes par correspondance ; <br/>
<br/>
              6. Considérant toutefois que les requérants n'ont apporté, au cours de l'instruction, aucun élément précis sur l'ampleur du surcoût de transport spécifiquement imputable à la disposition contestée, et notamment son importance par rapport au montant total des coûts de transport, au montant des transactions ou à la marge bénéficiaire ; qu'en particulier, il n'a été tenu compte, dans les estimations économiques qui ont été produites, ni du fait que les bijoux ne sont pas dans le champ de l'interdiction en litige, ni du fait - établi en cours d'instance et mentionné au paragraphe 3 ci-dessus - que les colis et correspondances transportés par les entreprises qui n'assurent pas un service postal ne sont pas affectés par cette interdiction, ni, enfin, du fait - également établi en cours d'instance et mentionné au paragraphe 4 ci-dessus - que les envois de " Napoléons " en or ne sont pas non plus frappés par cette interdiction ;<br/>
<br/>
              7. Considérant que, compte tenu de ce qui précède, les conséquences économiques des dispositions réglementaires contestées n'apparaissent pas de nature à créer, pour les entreprises requérantes et les syndicats qui en représentent les intérêts, un préjudice grave et immédiat constitutif d'une situation d'urgence au sens des dispositions de l'article L. 521-1 du code de justice administrative ; <br/>
<br/>
              8. Considérant qu'il résulte de tout ce qui précède que la condition d'urgence n'est pas remplie ; que la requête à fin de suspension présentée par le syndicat des négociants indépendants en métaux précieux et les autres requérants ne peut donc être accueillie ; que les conclusions qu'ils présentent sur le fondement de l'article L. 761-1 du code de justice administrative doivent, par voie de conséquence, être également rejetées ;<br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
------------------<br/>
 Article 1er : La requête du syndicat des négociants indépendants en métaux précieux et autres est rejetée.<br/>
<br/>
Article 2 : La présente ordonnance sera notifiée au syndicat des négociants indépendants en métaux précieux, au syndicat national des changeurs et auxiliaires financiers, à la société Change et Collection, à la société AAEO Compagnie générale de bourse, à la société Galerie Les Chevaux Légers, à la société AOC Côte d'Azur, à la société Gold by gold, à la société Aucoffre.com, à la société Godot et fils net, à la société Joubert groupe, à la société Comptoir des tuileries, à la société Sodima, au Premier ministre et au ministre du redressement productif.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
