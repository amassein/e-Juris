<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036631225</ID>
<ANCIEN_ID>JG_L_2018_02_000000410283</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/63/12/CETATEXT000036631225.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème chambre, 21/02/2018, 410283, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-02-21</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>410283</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BARADUC, DUHAMEL, RAMEIX</AVOCATS>
<RAPPORTEUR>M. Alain Seban</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Laurence Marion</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:410283.20180221</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Les consorts A...ont demandé au tribunal administratif de Paris de condamner l'Etat à leur verser la somme de 37 500 euros en réparation du préjudice subi du fait de l'absence de proposition d'un relogement. Par un jugement n° 1600716/6-1 du 26 janvier 2017, le tribunal administratif a rejeté leur demande.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 4 mai 2017 et 3 août 2017 au secrétariat du contentieux du Conseil d'Etat, M. B... A...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) réglant l'affaire au fond, d'accorder à M. A... une indemnité de 7 500 euros ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 2 000 euros au profit de la SCP Baraduc, Duhamel, Rameix, en vertu des dispositions de l'article 37 de la loi du 10 juillet 1991 et L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
                          Vu : <br/>
<br/>
              - le code de la construction et de l'habitation<br/>
<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Alain Seban, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Laurence Marion, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Baraduc, Duhamel, Rameix, avocat de M.A....<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que M. A...a été reconnu prioritaire et devant être relogé en urgence, sur le fondement de l'article L. 411-2-3 du code de la construction et de l'habitation, par une décision du 9 octobre 2009 de la commission de médiation de Paris, au motif qu'il résidait dans un logement sur-occupé avec au moins une personne mineure ou handicapée ; que, par un jugement du 15 septembre 2010, le tribunal administratif de Paris, saisi par M. A...sur le fondement du I de l'article L. 441-2-3-1 du même code, a enjoint au préfet de la région Ile-de-France, préfet de Paris, d'assurer son relogement ; que, constatant l'absence d'offre de relogement, le tribunal administratif, par un jugement du 10 décembre 2013, a condamné l'Etat à verser à l'intéressé la somme de 3 000 euros en réparation du préjudice subi entre avril 2010 et juillet 2013 du fait de son absence de relogement ; que M. et Mme A...ont formé une nouvelle demande indemnitaire, en leur noms propres et au nom de leurs deux enfants mineurs, tendant à ce que l'Etat soit condamné à réparer les préjudices subis au cours de la période ultérieure ; que M. A... se pourvoit en cassation contre le jugement du 26 janvier 2017 par lequel le tribunal administratif de Paris a rejeté cette demande, aux motifs, d'une part, que son épouse et leurs deux enfants mineurs ne justifiaient pas de droits propres et, d'autre part, que lui-même ne justifiait pas de nouveaux troubles dans ses conditions d'existence ; <br/>
<br/>
              2. Considérant que, lorsqu'une personne a été reconnue comme prioritaire et comme devant être logée ou relogée d'urgence par une commission de médiation, en application des dispositions de l'article L. 441-2-3 du code de la construction et de l'habitation, et que le juge administratif a ordonné son logement ou son relogement par l'Etat, en application de l'article L. 441-2-3-1 de ce code, la carence fautive de l'Etat à exécuter ces décisions dans le délai imparti engage sa responsabilité à l'égard du demandeur, au titre des troubles dans les conditions d'existence résultant du maintien de la situation qui a motivé la décision de la commission ; que ces troubles doivent être appréciés en fonction des conditions de logement qui ont perduré du fait de la carence de l'Etat, de la durée de cette carence et du nombre de personnes composant le foyer du demandeur pendant la période de responsabilité de l'Etat ;<br/>
<br/>
              3. Considérant que, bien qu'ayant constaté que le préfet n'avait pas proposé de relogement à M. A...dans le délai prévu par le code de la construction et de l'habitation à compter de la décision de la commission de médiation, le tribunal administratif a rejeté sa demande d'indemnisation faute pour le requérant de produire des pièces susceptibles d'établir qu'il continuait à subir une situation de sur-occupation de son logement ; qu'en se prononçant ainsi, alors que l'intéressé avait notamment produit un document de la caisse d'allocations familiales dont il résultait qu'en novembre 2014 ses trois enfants, dont deux nés en 1997 et un en 2003, étaient pris en compte pour la détermination de ses droits aux allocations familiales, ce qui impliquait qu'ils étaient à sa charge et logés sous son toit, le tribunal administratif, auquel il appartenait au besoin de procéder à une mesure d'instruction relative à la période ultérieure, a dénaturé les pièces du dossier ; qu'il suit de là, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, que son jugement doit être annulé en tant qu'il rejette les conclusions indemnitaires présentées par M. A...en son nom propre ; <br/>
<br/>
              4. Considérant que M. A...a obtenu le bénéfice de l'aide juridictionnelle ; que, par suite, la SCP Baraduc, Duhamel, Rameix, avocat du requérant, peut se prévaloir des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 ; qu'il y a lieu, dans les circonstances de l'espèce, et sous réserve que la SCP Baraduc, Duhamel, Rameix renonce à percevoir la somme correspondant à la part contributive de l'Etat, de mettre à la charge de l'Etat le somme de 2 000 euros à verser à la SCP Baraduc, Duhamel, Rameix.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du 26 janvier 2017 du tribunal administratif de Paris est annulé en tant qu'il se prononce sur la responsabilité de l'Etat à l'égard de M.A....<br/>
Article 2 : L'affaire est renvoyée au tribunal administratif de Paris dans la mesure de la cassation prononcée à l'article 1er.<br/>
Article 3 : L'Etat versera à la SCP Baraduc-Duhamel-Rameix, avocat du requérant, la somme de 2 000 euros en application de l'article L. 761-1 du code de justice administrative et du deuxième alinéa de l'article 37 de la loi du 10 juillet 1991, sous réserve que cette société renonce à percevoir la somme correspondant à la part contributive de l'Etat.<br/>
Article 4 : Le surplus des conclusions du pourvoi est rejeté.<br/>
Article 5 : La présente décision sera notifiée à M. B...A...et au ministre de la cohésion des territoires.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
