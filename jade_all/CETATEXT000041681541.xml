<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000041681541</ID>
<ANCIEN_ID>J5_L_2020_02_000001902544</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/41/68/15/CETATEXT000041681541.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>CAA de NANCY, 3ème chambre, 04/02/2020, 19NC02544, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-02-04</DATE_DEC>
<JURIDICTION>CAA de NANCY</JURIDICTION>
<NUMERO>19NC02544</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème chambre</FORMATION>
<TYPE_REC>excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. WURTZ</PRESIDENT>
<AVOCATS>SCP ROTH-PIGNON LEPAROUX ROSENSTIEHL ET ANDREINI</AVOCATS>
<RAPPORTEUR>M. Eric  MEISSE</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme SEIBT</COMMISSAIRE_GVT>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
       Procédure contentieuse antérieure :<br/>
<br/>
       M. C... A... a demandé au tribunal administratif de Strasbourg d'annuler l'arrêté du 24 octobre 2018 par lequel le préfet du Bas-Rhin a refusé de lui délivrer un titre de séjour, lui a fait obligation de quitter le territoire français dans un délai de trente jours et a fixé le pays de destination de son éventuelle reconduite d'office à la frontière. <br/>
<br/>
       Par un jugement n° 1900146 du 16 avril 2019, le tribunal administratif de Strasbourg a rejeté sa demande. <br/>
<br/>
       Procédure devant la cour :<br/>
<br/>
       Par une requête, enregistrée le 5 août 2019, M. C... A..., représenté par Me B..., demande à la cour :<br/>
<br/>
       1°) d'annuler le jugement n° 1900146 du tribunal administratif de Strasbourg du 16 avril 2019 ; <br/>
<br/>
       2°) d'annuler l'arrêté du préfet du Bas-Rhin du 24 octobre 2018 ; <br/>
       3°) d'enjoindre au préfet du Bas-Rhin, dans le délai d'un mois suivant la notification de l'arrêt à intervenir, de lui délivrer un titre de séjour, subsidiairement, de réexaminer sa situation et, dans l'intervalle, de lui délivrer une autorisation provisoire de séjour ; <br/>
<br/>
       4°) de mettre à la charge de l'Etat le versement à son conseil de la somme de 1 500 euros en application des dispositions combinées des articles L. 761-1 du code de justice administrative et 37 de la loi n° 91-647 du 10 juillet 1991, relative à l'aide juridique.<br/>
<br/>
       Il soutient que : <br/>
       - la décision portant refus de délivrance d'un titre de séjour est entachée d'un vice de procédure en raison des irrégularités affectant l'avis du collège de médecins de l'Office français de l'immigration et de l'intégration du 13 avril 2018 ; <br/>
       - la décision en litige méconnaît les dispositions du 11° de l'article L. 313-11 du code de l'entrée et du séjour des étrangers et du droit d'asile ; <br/>
       - elle méconnaît également les dispositions du 7 de l'article L. 313-11 du code de l'entrée et du séjour des étrangers et du droit d'asile, ainsi que les stipulations de l'article 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ; <br/>
       - elle est entachée d'une erreur manifeste d'appréciation ; <br/>
       - la décision portant obligation de quitter le territoire français est illégale en raison de l'illégalité de la décision portant refus de délivrance d'un titre de séjour ; <br/>
       - la décision portant fixation du pays de destination est illégale en raison de l'illégalité de la décision portant refus de délivrance d'un titre de séjour. <br/>
<br/>
       La requête a été régulièrement communiquée au préfet du Bas-Rhin qui n'a pas défendu dans la présente instance. <br/>
<br/>
       M. A... a été admis au bénéfice de l'aide juridictionnelle totale par une décision du 9 juillet 2019. <br/>
<br/>
       Vu les autres pièces du dossier.<br/>
<br/>
       Vu : <br/>
       - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ; <br/>
       - le code de l'entrée et du séjour des étrangers et du droit d'asile ; <br/>
       - la loi n° 91-647 du 10 juillet 1991;<br/>
       - le code de justice administrative.<br/>
<br/>
       Le président de la formation de jugement a dispensé le rapporteur public, sur sa proposition, de prononcer des conclusions à l'audience.<br/>
<br/>
       Les parties ont été régulièrement averties du jour de l'audience.<br/>
<br/>
       Le rapport de M. Meisse, premier conseiller, a été entendu au cours de l'audience publique. <br/>
<br/>
       Considérant ce qui suit ; <br/>
<br/>
       1. M. C... A... est un ressortissant mongol, né le 13 octobre 1973. Il a déclaré être entré irrégulièrement en France en avril 2014. Il a présenté une demande d'asile, qui a été successivement rejetée par l'Office français de protection des réfugiés et apatrides, le 15 mai 2015, puis par la Cour nationale du droit d'asile, le 12 avril 2016. En conséquence de ces refus, le requérant a fait l'objet, le 16 janvier 2017, d'une obligation de quitter le territoire français à laquelle il n'a pas déféré. Le 31 octobre 2017, l'intéressé a sollicité la délivrance d'un titre de séjour en qualité d'étranger malade sur le fondement du 11° de l'article L. 313-11 du code de l'entrée et du séjour des étrangers et du droit d'asile. Toutefois, à la suite de l'avis défavorable du collège de médecins de l'Office français de l'immigration et de l'intégration du 13 avril 2018, le préfet du Bas-Rhin, par un arrêté du 24 octobre 2018, a refusé de faire droit à cette demande, lui a fait obligation de quitter le territoire français dans un délai de trente jours et a fixé le pays de destination de son éventuelle reconduite d'office à la frontière. Par une requête, enregistrée le 9 janvier 2019, M. A... a saisi le tribunal administratif de Strasbourg d'une demande tendant l'annulation de l'arrêté du 24 octobre 2018. Il relève appel du jugement n° 1900146 du 16 avril 2019, qui rejette sa demande. <br/>
<br/>
       Sur le bien-fondé du jugement : <br/>
<br/>
       En ce qui concerne la légalité de décision portant refus de délivrance d'un titre de séjour : <br/>
<br/>
       2. En premier lieu, le moyen tiré de ce que la décision en litige serait entachée d'un vice de procédure en raison des irrégularités affectant l'avis du collège de médecins de l'Office français de l'immigration et de l'intégration du 13 avril 2018 n'est pas assorti de précisions suffisantes pour permettre à la cour d'en apprécier le bien-fondé. Par suite, ce moyen ne peut qu'être écarté. <br/>
<br/>
       3. En deuxième lieu, il ne ressort pas des pièces du dossier que M. A... ait sollicité la délivrance d'un titre de séjour en application du 7° de l'article L. 313-11 du code de l'entrée et du séjour des étrangers et du droit d'asile. Par suite, et alors qu'il ressort des motifs de la décision en litige que le préfet du Bas-Rhin a examiné uniquement le droit au séjour de l'intéressé au regard du 11° de l'article L. 313-11, le moyen tiré de la méconnaissance des dispositions du 7° de l'article L. 313-11 doit être écarté comme inopérant. <br/>
<br/>
       4. En troisième lieu, aux termes de l'article L. 313-11 du code de l'entrée et du séjour des étrangers et du droit d'asile : " Sauf si sa présence constitue une menace pour l'ordre public, la carte de séjour temporaire portant la mention " vie privée et familiale " est délivrée de plein droit : (...) 11° A l'étranger résidant habituellement en France, si son état de santé nécessite une prise en charge médicale dont le défaut pourrait avoir pour lui des conséquences d'une exceptionnelle gravité et si, eu égard à l'offre de soins et aux caractéristiques du système de santé dans le pays dont il est originaire, il ne pourrait pas y bénéficier effectivement d'un traitement approprié. La condition prévue à l'article L. 313-2 n'est pas exigée. La décision de délivrer la carte de séjour est prise par l'autorité administrative après avis d'un collège de médecins du service médical de l'Office français de l'immigration et de l'intégration, dans des conditions définies par décret en Conseil d'Etat. Sous réserve de l'accord de l'étranger et dans le respect des règles de déontologie médicale, les médecins de l'office peuvent demander aux professionnels de santé qui en disposent les informations médicales nécessaires à l'accomplissement de cette mission. Les médecins de l'office accomplissent cette mission dans le respect des orientations générales fixées par le ministre chargé de la santé. Si le collège de médecins estime dans son avis que les conditions précitées sont réunies, l'autorité administrative ne peut refuser la délivrance du titre de séjour que par une décision spécialement motivée. (...) ". <br/>
       5. Il ressort des pièces du dossier que, pour refuser d'admettre M. A... au séjour en qualité d'étranger malade, le préfet du Bas-Rhin s'est notamment fondé sur l'avis du collège de médecins de l'Office français de l'immigration et de l'intégration du 13 avril 2018. Or, selon cet avis, si l'état de santé de l'intéressé nécessite une prise en charge médicale, dont le défaut peut entraîner des conséquences d'une exceptionnelle gravité, il lui permet néanmoins de voyager sans risque vers son pays d'origine, où, eu égard à l'offre de soins et aux caractéristiques du système de santé, il peut y bénéficier effectivement d'un traitement approprié à sa pathologie. Le requérant fait valoir qu'il souffre d'une hépatite chronique virale B et D et verse aux débats plusieurs certificats médicaux de praticiens hospitaliers et médecins spécialistes, qui insistent essentiellement sur la nécessité du traitement et sur la régularité du suivi, ainsi qu'un rapport de l'Organisation mondiale de la santé sur les carences du système de soins en Mongolie au cours de la période 2010-2015. Toutefois, ces différents documents, eu égard aux termes dans lesquels ils sont rédigés, et notamment à leur caractère peu circonstancié ou général, ne sont pas de nature à remettre en cause l'appréciation à laquelle s'est livrée le préfet du Bas-Rhin sur la disponibilité effective dans le pays d'origine et la capacité de l'étranger à voyager sans risque. Par suite, le moyen tiré de la méconnaissance du 11° de l'article L. 313-11 du code de l'entrée et du séjour des étrangers et du droit d'asile ne peut qu'être écarté. <br/>
<br/>
       6. En quatrième lieu, aux termes de l'article 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales : " 1. Toute personne a droit au respect de sa vie privée et familiale, de son domicile et de sa correspondance. / 2. Il ne peut y avoir ingérence d'une autorité publique dans l'exercice de ce droit que pour autant que cette ingérence est prévue par la loi et qu'elle constitue une mesure qui, dans une société démocratique, est nécessaire à la sécurité nationale, à la sûreté publique, au bien-être économique du pays, à la défense de l'ordre et à la prévention des infractions pénales, à la protection de la santé ou de la morale, ou à la protection des droits et libertés d'autrui. ".<br/>
<br/>
       7. Il ressort des pièces du dossier que, si M. A... a déclaré être présent sur le territoire français depuis avril 2014, il a fait l'objet, le 16 janvier 2017, d'une mesure d'éloignement à laquelle il n'a pas déféré. Il ne justifie d'aucune attache familiale ou personnelle en France et ne produit aucun élément permettant d'apprécier son insertion dans la société française. Il n'est pas isolé dans son pays d'origine, où vivent notamment son épouse et son fils mineur. Par suite, le moyen tiré de la méconnaissance des stipulations de l'article 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales doit être écarté. <br/>
<br/>
       8. En cinquième et dernier lieu, pour les raisons qui viennent d'être exposées, le moyen tiré de l'erreur manifeste d'appréciation de la situation de l'intéressé au regard du pouvoir de régularisation du préfet ne peut être accueilli. <br/>
<br/>
       En ce qui concerne la légalité des décisions portant obligation de quitter le territoire français et fixation du pays de destination : <br/>
<br/>
       9. Compte tenu de ce qui précède, il y a lieu d'écarter les moyens tirés de ce que les décisions en litige seraient illégales en raison de l'illégalité de la décision portant refus de délivrance d'un titre de séjour. <br/>
<br/>
       10. Il résulte de tout ce qui précède que M. A... n'est pas fondé à demander l'annulation de l'arrêté du préfet du Bas-Rhin du 24 octobre 2018. Par suite, il n'est pas davantage fondé à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Strasbourg a rejeté sa demande. Par voie de conséquence, ses conclusions à fin d'injonction et à fin d'application des dispositions combinées des articles L. 761-1 du code de justice administrative et 37 de la loi n° 91-647 du 10 juillet 1991, relative à l'aide juridique, doivent elles aussi être rejetées. <br/>
<br/>
D E C I D E :<br/>
<br/>
       Article 1er : La requête de M. A... est rejetée. <br/>
       Article 2 : Le présent arrêt sera notifié à M. C... A... et au ministre de l'intérieur.<br/>
<br/>
       Copie en sera adressée au préfet du Bas-Rhin. <br/>
<br/>
N° 19NC02544	2<br/>
<br/>
<br/>
<br/>
</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">335-01-03 Étrangers. Séjour des étrangers. Refus de séjour.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">335-03 Étrangers. Obligation de quitter le territoire français (OQTF) et reconduite à la frontière.
</SCT>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
