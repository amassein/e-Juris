<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034993728</ID>
<ANCIEN_ID>JG_L_2017_06_000000411060</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/99/37/CETATEXT000034993728.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 20/06/2017, 411060, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-06-20</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>411060</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BORE, SALVE DE BRUNETON ; SCP LYON-CAEN, THIRIEZ</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2017:411060.20170620</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés les 31 mai et 16 juin 2017 au secrétariat du contentieux du Conseil d'Etat, Mme C...A...épouse B... demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative :<br/>
<br/>
              1°) d'ordonner la suspension de l'exécution de la décision du conseil régional de l'ordre des chirurgiens-dentistes d'Alsace du 6 avril 2017 prononçant le refus de son inscription sur le tableau de l'ordre des chirurgiens-dentistes du Bas-Rhin ; <br/>
<br/>
              2°) d'enjoindre, à titre principal, au conseil départemental de l'ordre des chirurgiens-dentistes du Bas-Rhin de procéder à son inscription provisoire sur le tableau de l'ordre jusqu'à ce qu'il soit statué définitivement sur ses recours administratifs, sinon au conseil régional d'Alsace de cet organisme d'ordonner au conseil départemental du Bas-Rhin une telle mesure et d'assortir cette injonction d'une astreinte de 100 euros par jour de retard à compter de la notification de l'ordonnance ; <br/>
<br/>
              3°) d'enjoindre, à titre subsidiaire, au conseil départemental de l'ordre des chirurgiens-dentistes du Bas-Rhin de procéder au réexamen de sa demande d'inscription, dans un délai de dix jours à compter de la notification de l'ordonnance et ce sous astreinte de 100 euros par jour de retard ;<br/>
<br/>
              4°) de mettre à la charge de l'ordre des chirurgiens-dentistes la somme de 3 000 euros en application de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Elle soutient que :<br/>
              - sa requête est recevable dès lors que l'existence d'une procédure de recours préalable obligatoire, qui a été introduite le 5 mai 2017, ne fait pas obstacle à l'introduction d'une demande de suspension présentée sur le fondement de l'article L. 521-1 du code de justice administrative ; <br/>
              - la condition d'urgence, qui doit s'apprécier à compter de la notification de la décision de refus d'inscription prise par le conseil départemental de l'ordre des chirurgiens-dentistes du Bas-Rhin le 8 février 2017, est remplie dès lors que, d'une part, cette décision l'empêche d'exercer sa profession et d'effectuer des remplacements prévus de longue date alors même que des cotisations continuent de lui être réclamées et, d'autre part, les délais de jugement devant le conseil national de l'ordre sont incompatibles avec sa situation de précarité actuelle bien qu'il ait fixé au 21 juin 2017 la date de l'audience pour l'examen de l'appel qu'elle a formé ;<br/>
              - il existe un doute sérieux quant à la légalité de la décision contestée ;<br/>
              - la décision contestée est entachée d'une erreur de droit en ce qu'elle se fonde sur une exigence édictée par une circulaire du conseil national de l'ordre des chirurgiens-dentistes en date du 19 juin 2014 alors que cette exigence n'est pas mentionnée à l'article R. 4112-1 du code de la santé publique qui fixe la liste limitative des pièces requises pour s'inscrire au tableau de l'ordre de sorte que le conseil régional de l'ordre a ajouté une condition supplémentaire au texte ; <br/>
              - la circulaire du secrétaire général du conseil national de l'ordre des chirurgiens-dentistes du 19 juin 2014, qui revêt un caractère impératif en édictant une règle nouvelle, méconnaît l'article R. 4112-1 du code de la santé publique et est entachée d'une double incompétence ; <br/>
              - la restriction apportée par la circulaire et par la décision attaquée porte atteinte à la liberté d'expression religieuse qui constitue une liberté fondamentale.<br/>
<br/>
              Par un mémoire en défense, enregistré le 15 juin 2017, le conseil régional de l'ordre des chirurgiens-dentistes d'Alsace conclut au rejet de la requête et demande au juge des référés du Conseil d'Etat de mettre à la charge de Mme B...la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. Il fait valoir que le Conseil d'Etat n'est pas compétent pour se prononcer sur une décision rendue par un conseil régional, que la requête est irrecevable, que la condition d'urgence n'est pas remplie et, enfin, qu'aucun des moyens soulevés par la requérante n'est de nature à faire naître un doute sérieux quant à la légalité de la décision contestée. <br/>
<br/>
              La requête a été communiquée à la ministre des solidarités et de la santé qui n'a pas produit d'observations. <br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, Mme A..., d'autre part, le conseil régional de l'ordre des chirurgiens-dentistes d'Alsace et la ministre des solidarités et de la santé ; <br/>
<br/>
              Vu le procès-verbal de l'audience publique du 19 juin 2017 à 10 heures au cours de laquelle ont été entendus : <br/>
              - Me Megret, avocat au Conseil d'Etat et à la Cour de cassation, avocat de Mme B...;<br/>
<br/>
              - la représentante de MmeB... ;<br/>
<br/>
              - Me Thiriez, avocat au Conseil d'Etat et à la Cour de cassation, avocat du conseil régional de l'ordre des chirurgiens-dentistes d'Alsace ;<br/>
              et à l'issue de laquelle le juge des référés a différé la clôture de l'instruction jusqu'au lundi 19 juin 2017 à 15 heures puis a repoussé cette clôture à 17 heures ;<br/>
<br/>
<br/>
              Vu le mémoire complémentaire, enregistré le 19 juin 2017, présenté par le conseil régional de l'ordre des chirurgiens-dentistes d'Alsace qui persiste dans ses précédentes conclusions ; <br/>
              Vu le mémoire en réponse, enregistré le 19 juin 2017, présenté par Mme B... qui maintient ses précédentes conclusions ;<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de la santé publique ;<br/>
              - le décret n° 55-1397 du 22 octobre 1955 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. Aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision ". <br/>
<br/>
              2. Il résulte des dispositions de l'article L. 4112-1 du code de la santé publique qu'un chirurgien-dentiste qui exerce dans un département doit être inscrit sur un tableau établi et tenu à jour par le conseil départemental de l'ordre des chirurgiens-dentistes de ce département qui doit s'assurer qu'il remplit les conditions requises pour exercer, notamment les conditions nécessaires de moralité, d'indépendance et de compétence. En vertu de l'article L. 4112-3 du même code, le conseil départemental de l'ordre statue sur la demande d'inscription au tableau dans un délai maximum de trois mois à compter de la réception de la demande, accompagnée d'un dossier complet dont le contenu est précisé à article R. 4112-1 de ce code. L'article L. 4112-4 de ce code prévoit que les décisions du conseil départemental refusant l'inscription demandées peuvent être frappées d'appel devant le conseil régional et les décisions prises par celui-ci sur cet appel peuvent être elles-mêmes frappées d'appel devant le conseil national de l'ordre, dans un délai de trente jours, les articles R. 4112-5 et R. 4112-5-1 précisant que ces appels n'ont pas d'effet suspensif. La décision du conseil national peut faire l'objet d'un recours pour excès de pouvoir devant le Conseil d'Etat.<br/>
<br/>
              3. A la suite de l'obtention de son diplôme en chirurgie dentaire le 3 septembre 2015, Mme B...a été inscrite au tableau de l'ordre des chirurgiens-dentistes auprès du conseil départemental du Territoire de Belfort le 13 octobre 2015. Le 22 septembre 2016, elle a sollicité son inscription au tableau de l'ordre du département du Bas-Rhin. Par une décision du 8 février 2017, le conseil départemental du Bas-Rhin a refusé de faire droit à sa demande d'inscription au motif que la photographie d'identité qu'elle avait jointe au formulaire " curriculum vitae " en vue de l'établissement de sa carte professionnelle ne répondait pas aux normes de la carte d'identité française. Le 23 février 2017, Mme B...a interjeté appel de cette décision devant le conseil régional de l'ordre des chirurgiens-dentistes d'Alsace. Par une décision du 6 avril 2017, le conseil régional d'Alsace a rejeté son appel. L'intéressée a contesté cette décision devant le conseil national de l'ordre qui examinera ce recours le 21 juin 2017. Par la présente requête, MmeB..., après avoir vainement saisi le juge des référés du tribunal administratif de Strasbourg d'une demande identique, demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative, de suspendre la décision du conseil régional de l'ordre des chirurgiens-dentistes d'Alsace du 6 avril 2017 et d'enjoindre au conseil départemental du Bas-Rhin de l'ordre des chirurgiens-dentistes, à titre principal, notamment de procéder à son inscription provisoire sur le tableau de l'ordre et, à titre subsidiaire, de procéder au réexamen de sa demande.<br/>
<br/>
              4. L'objet même du référé organisé par les dispositions législatives mentionnées au point 1 est de permettre, dans tous les cas où l'urgence le justifie, la suspension dans les meilleurs délais d'une décision administrative contestée par le demandeur. Une telle possibilité est ouverte y compris dans le cas où un texte législatif ou réglementaire impose l'exercice d'un recours administratif préalable avant de saisir le juge de l'excès de pouvoir, sans donner un caractère suspensif à ce recours obligatoire. Dans une telle hypothèse, la suspension peut être demandée au juge des référés sans attendre que l'administration ait statué sur le recours préalable, dès lors que l'intéressé a justifié, en produisant une copie de ce recours, qu'il a engagé les démarches nécessaires auprès de l'administration pour obtenir l'annulation ou la réformation de la décision contestée. Saisi d'une telle demande de suspension, le juge des référés peut y faire droit si l'urgence justifie la suspension avant même que l'administration ait statué sur le recours préalable et s'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision. Sauf s'il en décide autrement, la mesure qu'il ordonne en ce sens vaut, au plus tard, jusqu'à l'intervention de la décision administrative prise sur le recours présenté par l'intéressé. Il en résulte que le conseil régional de l'ordre des chirurgiens-dentistes d'Alsace n'est fondé à soutenir ni que le juge des référés du Conseil d'Etat serait incompétent pour connaître de la requête de Mme B...ni que la requête de Mme B...serait irrecevable. <br/>
<br/>
              5. D'une part, la condition d'urgence à laquelle est subordonné le prononcé d'une mesure de suspension doit être regardée comme remplie lorsque la décision administrative contestée préjudicie de manière suffisamment grave et immédiate à un intérêt public, à la situation du requérant et aux intérêts qu'il entend défendre. Il ressort des pièces du dossier et des échanges lors de l'audience publique que la décision du conseil régional d'Alsace du 6 avril 2017 refusant l'inscription de Mme B...au tableau de l'ordre des chirurgiens-dentistes et confirmant le refus opposé par le conseil départemental du Bas-Rhin l'empêche d'exercer sa profession et la prive de toute ressource tirée de son activité, et ce depuis le 8 février 2017. Par suite, et alors même que le conseil national de l'ordre examinera le 21 juin l'appel dont Mme B... l'a saisi, la condition d'urgence prévue à l'article L. 521-1 du code de justice administrative doit être regardée comme remplie.<br/>
<br/>
              6. D'autre part, il résulte de l'instruction que le conseil régional de l'ordre a rejeté la demande d'inscription de Mme B...au tableau de l'ordre au seul motif que la photographie qu'elle a jointe à son dossier n'est pas conforme aux exigences posées par une circulaire du secrétaire général du conseil national de l'ordre des chirurgiens-dentistes du 19 juin 2014, qui prévoit l'obligation de faire figurer sur le " curriculum vitae " du demandeur une photographie conforme aux normes édictées pour la carte nationale d'identité par le décret du 22 octobre 1955 instituant la carte nationale d'identité, dès lors qu'elle n'y figurait pas tête nue et qu'elle a refusé de joindre une autre photographie conforme à ces exigences. Or, l'article R. 4112-1 du code de la santé publique, qui énumère de façon limitative les documents à joindre à une demande d'inscription au nombre desquels figurent une photocopie d'une pièce d'identité en cours de validité et un curriculum vitae, ne prévoit pas la présence d'une photographie sur le curriculum vitae. De plus, le législateur n'a pas confié au conseil national de l'ordre une compétence pour compléter cette liste. En outre, si le conseil régional de l'ordre fait valoir à l'audience que cette photographie doit figurer sur la carte professionnelle mise en place par l'ordre, le refus de transmettre à l'ordre une photographie conforme aux normes édictées pour la carte nationale d'identité pour figurer sur cette carte, laquelle ne vaut pas document d'identité contrairement à ce qu'il soutient, ne saurait en tout état de cause justifier un refus d'inscription au tableau de l'ordre. Par suite, les moyens tirés de l'illégalité de la circulaire du secrétaire général du conseil national de l'ordre des chirurgiens-dentistes du 19 juin 2014 en ce qu'elle ajoute une condition non prévue à l'article R. 4112-1 du code de la santé publique et porte à la liberté de conscience une atteinte qui n'est pas justifiée ainsi que de l'incompétence du conseil national pour instaurer cette l'obligation paraissent, en l'état de l'instruction, de nature à faire naître un doute sérieux sur la légalité de la décision du conseil régional de l'ordre des chirurgiens-dentistes d'Alsace du 6 avril 2017 refusant l'inscription de Mme B...au tableau de l'ordre dans le département du Bas-Rhin.<br/>
<br/>
              7. Il résulte de tout ce qui précède que Mme B...est fondée à demander la suspension de l'exécution de la décision du conseil régional de l'ordre des chirurgiens-dentistes d'Alsace du 6 avril 2017 refusant son inscription au tableau. Compte tenu de l'intervention imminente de la décision que le conseil national de l'ordre des chirurgiens-dentistes doit prendre sur l'appel formé par Mme B...contre cette décision, il n'y a pas lieu de prononcer d'injonction.<br/>
<br/>
              8. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge du conseil régional de l'ordre des chirurgiens-dentistes d'Alsace la somme de 2 000 euros à verser à Mme B...au titre des dispositions de l'article L. 761-1 du code de justice administrative. Ces dispositions font en revanche obstacle à ce qu'il soit fait droit aux conclusions présentées au même titre par le conseil régional de l'ordre des chirurgiens-dentistes d'Alsace.<br/>
<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : L'exécution de la décision du conseil régional de l'ordre des chirurgiens-dentistes d'Alsace du 6 avril 2017 est suspendue.<br/>
Article 2 : Le conseil régional de l'ordre des chirurgiens-dentistes d'Alsace versera à Mme B... la somme de 2 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : Le surplus des conclusions de la requête de Mme B...et les conclusions présentées par le conseil régional de l'ordre des chirurgiens-dentistes d'Alsace au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
Article 4 : La présente ordonnance sera notifiée à Mme C...A...épouse B...et au conseil régional de l'ordre des chirurgiens-dentistes d'Alsace.<br/>
Copie en sera adressée au conseil national de l'ordre des chirurgiens-dentistes, au conseil départemental de l'ordre des chirurgiens-dentistes du Bas-rhin et à la ministre des solidarités et de la santé.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
