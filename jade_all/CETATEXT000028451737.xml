<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028451737</ID>
<ANCIEN_ID>JG_L_2013_12_000000360802</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/45/17/CETATEXT000028451737.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  3ème sous-section jugeant seule, 30/12/2013, 360802, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-12-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>360802</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 3ème sous-section jugeant seule</FORMATION>
<TYPE_REC/>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GASCHIGNARD ; SCP LEVIS</AVOCATS>
<RAPPORTEUR>M. Alain Méar</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Vincent Daumas</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2013:360802.20131230</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 6 juillet et 8 octobre 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour la communauté urbaine de Strasbourg, représentée par le président du conseil de la communauté ; la communauté urbaine de Strasbourg demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'ordonnance n° 12NC00762 du 4 mai 2012 par laquelle le président de la première chambre de la cour administrative d'appel de Nancy a rejeté comme irrecevable sa requête tendant, d'une part, à l'annulation du jugement du 29 février 2012 par lequel le tribunal administratif de Strasbourg a rejeté sa demande tendant à l'annulation de la délibération n° 25 du 17 décembre 2009 du conseil d'administration du service départemental d'incendie et de secours (SDIS) du Bas-Rhin relative aux contributions des communes et établissements publics de coopération intercommunale au titre de l'année 2010, d'autre part, à l'annulation des délibérations n° 25, 6 et 32 du 17 décembre 2009, de l'arrêté n° 3688-2009 du SDIS du 17 décembre 2009, des titres exécutoires n° 2, 4, 6, 8, 10, 12, 14, 16, 18, 20, 22 et 24 du 4 janvier 2010, du titre exécutoire n° 2238 du 31 décembre 2009, des délibérations n° 21 et 28 du 16 décembre 2010, de l'arrêté n° 2010-3754 du SDIS du 16 décembre 2010 et des titres exécutoires n° 001651, 001653, 001655, 001657, 001659, 001661, 001663, 001665, 001667, 001669, 001671 et 001673 du 13 octobre 2011 relatifs à la contribution des communes et des établissements publics de coopération intercommunale au SDIS pour l'année 2011 ;<br/>
<br/>
              2°) de mettre à la charge du SDIS du Bas-Rhin une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la Constitution, notamment son Préambule ;<br/>
<br/>
              Vu la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
<br/>
              Vu le code général des impôts ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Alain Méar, Conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Vincent Daumas, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Gaschignard, avocat de la Communauté urbaine de Strasbourg et à la SCP Lévis, avocat du service départemental d'incendie (SDIS) du Bas-Rhin ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant, d'une part, qu'aux termes de l'article <br/>
R. 411-2 du code de justice administrative : " Lorsque la contribution pour l'aide juridique prévue à l'article 1635 bis Q du code général des impôts est due et n'a pas été acquittée, la requête est irrecevable. / Cette irrecevabilité est susceptible d'être couverte après l'expiration du délai de recours (...) / Par exception au premier alinéa de l'article R. 612-1, la juridiction peut rejeter d'office une requête entachée d'une telle irrecevabilité sans demande de régularisation préalable, lorsque l'obligation d'acquitter la contribution ou, à défaut, de justifier du dépôt d'une demande d'aide juridictionnelle est mentionnée dans la notification de la décision attaquée ou lorsque la requête est introduite par un avocat " ; que, d'autre part, l'article R. 222-1 du même code dispose que : " Les présidents de tribunal administratif et de cour administrative d'appel, le vice-président du tribunal administratif de Paris et les présidents de formation de jugement des tribunaux et des cours peuvent, par ordonnance : / (...) 4° Rejeter les requêtes manifestement irrecevables, lorsque la juridiction n'est pas tenue d'inviter leur auteur à les régulariser ou qu'elles n'ont pas été régularisées à l'expiration du délai imparti par une demande en ce sens (...) " ;<br/>
<br/>
              2. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que par une ordonnance du 4 mai 2012, le président de la première chambre de la cour administrative d'appel de Nancy a rejeté comme irrecevable, sur le fondement du 4° de l'article R. 222-1 du code de justice administrative, l'appel interjeté par la communauté urbaine de Strasbourg (CUS) du jugement du 29 avril 2012 du tribunal administratif de Strasbourg, au motif que la requérante n'avait pas acquitté la contribution pour l'aide juridique alors que sa requête, introduite par un avocat, n'en était pas dispensée et que la notification du jugement mentionnait cette obligation ; que la communauté urbaine de Strasbourg se pourvoit en cassation contre cette ordonnance ;<br/>
<br/>
              3. Considérant, en premier lieu, qu'en adoptant les dispositions créant la contribution pour l'aide juridique, qui ont été codifiées à l'article1635 bis Q du code général des impôts , le législateur a entendu établir une solidarité financière entre les justiciables dans le but d'intérêt général d'assurer le financement de la réforme de la garde à vue résultant de la loi du 14 avril 2011 et, en particulier, le coût résultant, au titre de l'aide juridique, de l'intervention de l'avocat au cours de la garde à vue ; qu'eu égard à l'objet de la contribution et à la généralité de l'obligation posée par la loi fiscale, aucune irrecevabilité résultant du défaut de versement de la contribution pour l'aide juridique ne peut être relevée d'office par les juridictions sans que le requérant ait été mis en mesure, directement ou par l'intermédiaire de son avocat, professionnel averti, de respecter la formalité exigée ; que, en effet, il résulte des dispositions citées ci-dessus de l'article R. 411-2 du code de justice que le rejet d'une requête sans que le requérant ait été préalablement invité à présenter des observations ou à régulariser sa requête ne peut intervenir que si l'obligation d'acquitter la contribution est mentionnée dans la notification de la décision attaquée ou si la requête est introduite par un avocat auquel il incombe, conformément à l'article 1635 bis Q du code général des impôts, d'acquitter directement la contribution pour le compte de la partie qu'il représente ; que, dès lors, les dispositions de l'article R. 411-2 du code de justice administrative ne méconnaissent ni le principe d'égalité devant la justice, ni le droit à un recours effectif garanti par l'article 16 de la Déclaration des droits de l'homme et du citoyen de 1789, ni les stipulations de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ; qu'elles ne méconnaissent pas plus les dispositions de l'article 1635 bis Q du code général des impôts, ni les objectifs poursuivis par le législateur en adoptant ces dispositions ; que, par suite, en jugeant, après avoir constaté que la notification du jugement attaqué comportait l'obligation d'acquitter la contribution pour l'aide juridique, que la requête de la communauté urbaine de Strasbourg devait être regardée comme manifestement irrecevable, le président de la première chambre de la cour administrative d'appel de Nancy n'a pas commis d'erreur de droit ;  <br/>
<br/>
              4. Considérant, en deuxième lieu, qu'il résulte des dispositions de l'article R. 411-2 du code de justice administrative qu'une requête pour laquelle la contribution pour l'aide juridique est due et n'a pas été acquittée est irrecevable et que la juridiction peut la rejeter d'office sans demande de régularisation préalable, lorsqu'elle est introduite par un avocat ; que la circonstance que cette irrecevabilité est susceptible d'être couverte en cours d'instance ne fait pas obstacle à ce qu'une requête, introduite par un avocat et pour laquelle la contribution n'a pas été acquittée, soit regardée comme entachée d'une irrecevabilité manifeste ; que, par suite, le président de la première chambre de la cour administrative d'appel de Nancy était compétent pour rejeter la requête par ordonnance, sur le fondement des dispositions du 4° de l'article R. 222-1 du code de justice administrative ;<br/>
<br/>
              5. Considérant, en troisième lieu, que la circonstance qu'un requérant demande dans sa requête le remboursement de la contribution pour l'aide juridique n'a pas pour effet de régulariser la requête ni d'imposer au juge de procéder à une demande de régularisation ;  que le président de la première chambre de la cour administrative d'appel de Nancy n'a pas commis d'erreur de droit en se fondant sur le défaut d'acquittement de la contribution pour l'aide juridique pour juger la requête irrecevable ;<br/>
<br/>
              6. Considérant qu'il résulte de tout ce qui précède que la communauté urbaine de Strasbourg n'est pas fondée à demander l'annulation de l'ordonnance attaquée ;<br/>
<br/>
              7. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge du SDIS du Bas-Rhin qui n'est pas, dans la présente instance, la partie perdante ; qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la communauté urbaine de Strasbourg la somme de 3 000 euros à verser au SDIS du Bas-Rhin au titre des mêmes dispositions ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de la communauté urbaine de Strasbourg est rejeté.<br/>
Article 2 : La communauté urbaine de Strasbourg versera au service départemental d'incendie et de secours du Bas-Rhin une somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée à la communauté urbaine de Strasbourg et au service départemental d'incendie et de secours du Bas-Rhin ;<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
