<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026504653</ID>
<ANCIEN_ID>JG_L_2012_10_000000359321</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/50/46/CETATEXT000026504653.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  7ème sous-section jugeant seule, 17/10/2012, 359321, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2012-10-17</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>359321</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 7ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Rémy Schwartz</PRESIDENT>
<AVOCATS/>
<RAPPORTEUR>Mme Cécile Chaduteau-Monplaisir</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Bertrand Dacosta</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2012:359321.20121017</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 11 mai 2012 au secrétariat du contentieux du Conseil d'Etat, présentée pour la Commission nationale des comptes de campagne et des financements politiques, dont le siège est 34-36 rue du Louvre à Paris Cedex 01 (75042), représentée par son président ; la Commission nationale des comptes de campagne et des financements politiques demande au Conseil d'Etat d'annuler le jugement n° 1105544 du 17 avril 2012 par lequel le tribunal administratif de Strasbourg a rejeté sa saisine fondée, en application de l'article L. 52-15 du code électoral, sur sa décision du 24 octobre 2011 constatant le dépôt tardif du compte de campagne de M. Jérôme A, candidat à l'élection cantonale qui s'est déroulée les 20 et 27 mars 2011 dans le canton de Cattenom (Moselle) ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code électoral ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Cécile Chaduteau-Monplaisir, Maître des Requêtes,  <br/>
<br/>
              - les conclusions de M. Bertrand Dacosta, rapporteur public ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'il résulte de l'instruction que, par décision du 24 octobre 2011, la Commission nationale des comptes de campagne et des financements politiques a constaté que M. A, candidat aux élections qui se sont déroulées les 20 et 27 mars 2011 en vue de la désignation du conseiller général du canton de Cattenom (Moselle), n'avait pas déposé son compte de campagne dans le délai prescrit par l'article L. 52-12 du code électoral ; que saisi en application de l'article L. 52-15 du même code par une requête enregistrée le 8 novembre 2011, le tribunal administratif de Strasbourg a, par jugement du 17 avril 2012, rejeté la saisine de la Commission nationale des comptes de campagne et des financements politiques ; que celle-ci fait appel de ce jugement ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article R. 114 du code électoral : " Le tribunal administratif prononce sa décision dans le délai de deux mois à compter de l'enregistrement de la réclamation au greffe (bureau central ou greffe annexe) ; (...) / En cas de renouvellement d'une série sortante, ce délai est porté à trois mois. (...) " ; qu'aux termes de l'article R. 117 du même code : " Faute d'avoir statué dans les délais fixés par les articles R. 114 et R. 115, le tribunal administratif est dessaisi. Le secrétaire greffier en informe le préfet et les parties intéressées en leur faisant connaître qu'ils ont un délai d'un mois pour se pourvoir devant le Conseil d'Etat. " ;<br/>
<br/>
              3. Considérant que les délais impartis au tribunal administratif par l'article R. 114 du code électoral s'appliquent tant au jugement des protestations électorales qu'à celui des saisines de la Commission nationale des comptes de campagne et des financements politiques que le candidat concerné ait été élu ou non ; qu'il résulte de l'instruction que le jugement attaqué est intervenu plus de cinq mois après l'enregistrement de la saisine soit postérieurement à l'expiration du délai prévu à l'article R. 114 du code électoral ; qu'ainsi, le tribunal administratif de Strasbourg n'était plus compétent pour statuer sur la saisine de la Commission nationale des comptes de campagne et des financements politiques ; que, par suite et sans qu'il soit besoin d'examiner les moyens de la requête, son jugement doit être annulé ; <br/>
<br/>
              4. Considérant qu'il y a lieu pour le Conseil d'Etat d'évoquer et de statuer immédiatement sur la demande présentée par la Commission nationale des comptes de campagne et des financements politiques devant le tribunal administratif de Strasbourg ;<br/>
<br/>
              5. Considérant, d'une part, qu'aux termes de l'article L. 52-12 du code électoral dans sa rédaction issue de l'article 10 de la loi du 14 avril 2011 portant simplification de dispositions du code électoral et relative à la transparence financière de la vie politique : " Chaque candidat ou candidat tête de liste soumis au plafonnement prévu à l'article L. 52-11 et qui a obtenu au moins 1 % des suffrages exprimés est tenu d'établir un compte de campagne (...) Au plus tard avant 18 heures le dixième vendredi suivant le premier tour de scrutin, chaque candidat ou candidat tête de liste présent au premier tour dépose à la Commission nationale des comptes de campagne et des financements politiques son compte de campagne et ses annexes accompagné des justificatifs de ses recettes ainsi que des factures, devis et autres documents de nature à établir le montant des dépenses payées ou engagées par le candidat ou pour son compte. (...) " ; qu'aux termes de l'alinéa 3 de l'article L. 52-15 du même code : " Lorsque la commission a constaté que le compte de campagne n'a pas été déposé dans le délai prescrit (...), la commission saisit le juge de l'élection. " ;<br/>
<br/>
<br/>
              6. Considérant, d'autre part, qu'aux termes de l'article L. 118-3 du code électoral dans sa rédaction issue également de la loi du 14 avril 2011 : " Saisi par la commission instituée par l'article L. 52-14, le juge de l'élection peut déclarer inéligible le candidat dont le compte de campagne, le cas échéant après réformation, fait apparaître un dépassement du plafond des dépenses électorales. / Saisi dans les mêmes conditions, le juge de l'élection peut déclarer inéligible le candidat qui n'a pas déposé son compte de campagne dans les conditions et le délai prescrits à l'article L. 52-12. / Il prononce également l'inéligibilité du candidat dont le compte de campagne a été rejeté à bon droit en cas de volonté de fraude ou de manquement d'une particulière gravité aux règles relatives au financement des campagnes électorales. (...) " ; que l'inéligibilité prévue par les dispositions précitées de l'article L. 118-3 du code électoral constituant une sanction ayant le caractère d'une punition il incombe, dès lors, au juge de l'élection, lorsqu'il est saisi de conclusions tendant à ce qu'un candidat dont le compte de campagne est rejeté soit déclaré inéligible et à ce que son élection soit annulée, de faire application, le cas échéant, d'une loi nouvelle plus douce entrée en vigueur entre la date des faits litigieux et celle à laquelle il statue ; qu'ainsi ces dispositions sont applicables au présent litige ;<br/>
<br/>
              7. Considérant qu'il résulte de l'instruction que M. A a déposé son compte de campagne le 8 juillet 2011 soit postérieurement au délai prescrit par l'article L. 52-12 du code électoral ; que la Commission nationale des comptes de campagne et des financements politiques était ainsi fondée à saisir le juge de l'élection en application de l'article L. 52-15 du même code sans que puisse lui être opposée la circonstance que ce retard serait imputable aux délais d'encaissement des chèques émis dans le cadre de la campagne électorale ; que les conclusions de M. A tendant à ce qu'il soit enjoint à la Commission nationale des comptes de campagne et des financements politiques de lui rembourser ses dépenses de campagne ne peuvent, en tout état de cause, qu'être rejetées ;<br/>
<br/>
              8. Considérant, comme il l'a été rappelé précédemment, qu'en vertu du deuxième alinéa de l'article L. 118-3 dans sa rédaction issue de la loi du 14 avril 2011, le juge de l'élection, saisi par la Commission nationale des comptes de campagne et des financements politiques, peut déclarer inéligible le candidat qui n'a pas déposé son compte de campagne dans les conditions et le délai prescrits à l'article L. 52-12 du même code ; que pour apprécier s'il y a lieu, pour lui, de faire usage de la faculté de déclarer un candidat inéligible, il appartient au juge de l'élection de tenir compte de la nature de la règle méconnue, du caractère délibéré ou non du manquement, de l'existence éventuelle d'autres motifs d'irrégularité du compte et du montant des sommes en cause ; <br/>
<br/>
              9. Considérant qu'il résulte de l'instruction, d'une part, que le compte de campagne déposé par M. A ne présentait aucune autre irrégularité que celle tenant à un dépôt tardif de ce compte et, d'autre part, que la méconnaissance du délai prescrit à l'article L. 52-12 du code électoral ne résultait pas d'un manquement délibéré du candidat mais du retard pris dans l'encaissement des chèques émis pour le paiement de prestations de service liées à sa campagne électorale ; qu'eu égard aux circonstances de l'espèce, il n'y a, dès lors, pas lieu de déclarer M. A inéligible, ni de se prononcer sur ses conclusions relevant d'un litige distinct ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du 17 avril 2012 du tribunal administratif de Strasbourg est annulé.<br/>
Article 2 : Il n'y a pas lieu de déclarer M. A inéligible en application de l'article L. 118-3 du code électoral.<br/>
Article 3 : Les conclusions de M. A sont rejetées.<br/>
Article 4 : La présente décision sera notifiée à la Commission nationale des comptes de campagne et des financements politiques et à M. Jérôme A.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
