<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044487268</ID>
<ANCIEN_ID>JG_L_2021_12_000000458970</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/48/72/CETATEXT000044487268.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 09/12/2021, 458970, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-12-09</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>458970</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FOUSSARD, FROGER</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2021:458970.20211209</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés le 29 novembre et le 7 décembre 2021 au secrétariat du contentieux du Conseil d'Etat, la société Solinest et la coopérative Arla Foods demandent au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative :<br/>
<br/>
              1°) à titre principal, de suspendre l'exécution de l'arrêté du 24 septembre 2021 relatif à la teneur en plastique maximale autorisée dans les gobelets en plastique à usage unique en tant qu'il s'applique à des gobelets vendus préremplis de boisson et dans lesquels l'utilisation du plastique à plus de 15 pour cent est nécessaire pour contenir et conserver les boissons jusqu'à leur consommation ;<br/>
<br/>
              2°) à titre subsidiaire, de suspendre l'exécution de l'arrêté du 24 septembre 2021 relatif à la teneur en plastique maximale autorisée dans les gobelets en plastique à usage unique en tant qu'il prévoit une interdiction, au 1er janvier 2022, de la mise à disposition de gobelets vendus préremplis de boissons et dans lesquels l'utilisation du plastique à plus de 15 pour cent est nécessaire pour contenir et conserver les boissons jusqu'à leur consommation ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 15 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Elles soutiennent que :<br/>
              - la condition d'urgence est satisfaite dès lors que, en premier lieu, l'arrêté porte une atteinte grave et immédiate aux droits conférés par l'ordre juridique européen, en deuxième lieu, la mesure contestée comporte des effets anti-concurrentiels qui les privent de toute possibilité de développement et conduisent à conférer à leurs concurrents un avantage irréversible et, en dernier lieu, leurs intérêts financiers ainsi que l'existence même de leurs entreprises sont en péril ; <br/>
              - il existe un doute sérieux quant à la légalité de la décision contestée ; <br/>
              - les dispositions contestées sont entachées d'incompétence dès lors que, d'une part, le décret n° 2020-1828 du 31 décembre 2020 se borne à définir vaguement ce que constituent des " gobelets et des verres " au sens du code de l'environnement et renvoie à un arrêté le soin de déterminer la teneur maximale de plastique autorisée dans les gobelets en plastique à usage unique, les conditions dans lesquelles la teneur maximale autorisée est progressivement réduite et le délai d'écoulement des stocks et, d'autre part, les ministres de la transition écologique et de l'économie, des finances et de la relance n'ont pas été légalement habilités à prendre cet arrêté ; <br/>
              - les dispositions contestées méconnaissent le principe d'égalité en ce que, d'une part, elles traitent de manière différente des situations comparables, et, d'autre part, elles traitent de manière identique des situations différentes relatives à la mise à disposition de gobelets en plastique préremplis de boisson ; <br/>
              - l'interdiction des gobelets et verres à usage unique avec une teneur en plastique supérieure à 15 pour cent est entachée d'inconventionnalité dès lors qu'elle méconnaît, d'une part, les objectifs de la directive 2019/904/UE du Parlement européen et du Conseil du 5 juin 2019 et, d'autre part, le traité sur le fonctionnement de l'Union européenne et notamment son article 34 sur la liberté de circulation des marchandises ;<br/>
              - l'interdiction de la totalité des gobelets en plastique est disproportionnée en ce qu'elle ne permet pas d'atteindre l'objectif poursuivi et revient en pratique à interdire le produit fini lui-même, à savoir le gobelet prérempli de boisson ;<br/>
              - l'arrêté contesté méconnaît le principe de sécurité juridique dès lors que, d'une part, son entrée en vigueur au 1er janvier 2022 ne leur permet pas de prendre à temps les mesures prévues par l'arrêté alors qu'il n'existe à ce jour aucune solution alternative au gobelet composé de plus de 15 pour cent de plastique pour des gobelets devant assurer la conservation des boissons qu'ils contiennent afin de garantir l'hygiène, la sécurité et la protection des consommateurs et, d'autre part, il conduit les entreprises à se tourner vers l'usage de bouteilles en plastiques qui ont un impact environnemental négatif.<br/>
<br/>
              Par un mémoire en défense, enregistré le 6 décembre 2021, la ministre de la transition écologique conclut au rejet de la requête. Elle soutient que la requête est irrecevable, que la condition d'urgence n'est pas satisfaite, et que les moyens soulevés ne sont pas fondés. <br/>
<br/>
              La requête a été communiquée au ministre de l'économie, des finances et de la relance qui n'a pas produit d'observations. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la directive 2019/904/UE du Parlement européen et du Conseil du 5 juin 2019 ;<br/>
              - le code de l'environnement ;<br/>
              - la loi n° 2020-105 du 10 février 2020 ;<br/>
              - le décret n° 2020-1828 du 31 décembre 2020 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, la société Solinest et la coopérative Arla Foods, et d'autre part, le ministre de l'économie, des finances et de la relance et la ministre de la transition écologique ;<br/>
<br/>
              Ont été entendus lors de l'audience publique du 7 décembre 2021, à 11 heures 30: <br/>
<br/>
              - Me Froger, avocat au Conseil d'Etat et à la Cour de cassation, avocat de la société Solinest et de la coopérative Arla Foods;<br/>
<br/>
              - les représentants de la société Solinest et de la coopérative Arla Foods ;<br/>
<br/>
              - les représentants de la ministre de la transition écologique ; <br/>
<br/>
              à l'issue de laquelle le juge des référés a clos l'instruction ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              Aux termes de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision ". <br/>
<br/>
               Pour mettre en œuvre la directive 2019 /904 du parlement européen et du conseil du 5 juin 2019 relative à la réduction de l'incidence de certains produits en plastique sur l'environnement, les pouvoirs publics ont, sur le fondement de dispositions législatives et réglementaires fixées par un arrêté du 24 septembre 2021 relatif à la teneur en plastique maximale autorisée dans les gobelets en plastique à usage unique, publié au Journal officiel le 15 octobre 2021, fixé à 15% cette teneur. La société Solinest, commercialise des produits prêts à boire conditionnés dans des gobelets en plastique fermés, que la coopérative de droit danois Arla Foods fabrique en remplissant des gobelets achetés en Allemagne. La société et la coopérative ont demandé l'annulation pour excès de pouvoir de cet arrêté, et, sur le fondement de l'article L. 521-1 du code de justice administrative, sa suspension, ou, à tout le moins, qu'il soit suspendu en tant qu'il s'applique à compter du 1er janvier 2022<br/>
<br/>
              Sans qu'il soit besoin d'examiner la fin de non-recevoir opposée en défense ;<br/>
<br/>
              Pour établir l'urgence qui s'attache à la suspension demandée, les requérants soutiennent que l'interdiction de fabriquer les produits qu'elles commercialisent, à compter du 1er janvier 2022, et de commercialiser les stocks plus de six mois après cette date, rendue publique le 15 octobre 2021 par la publication de l'arrêté critiqué, équivaut à l'arrêt complet de leur activité, dès lors qu'il leur est impossible, dans de si brefs délais, de mettre en œuvre, à supposer qu'elles existent, une production et une commercialisation alternatives.<br/>
<br/>
              Toutefois, l'interdiction de la commercialisation des gobelets en plastique, dont le principe est issu de la directive 5 juin 2019, a été opérée par le décret du 24 décembre 2019. Conformément à la directive, des mesures législatives puis réglementaires figurant aux articles L. 541-15-10 et D. 541-330 et suivants du code de l'environnement ont organisé le principe de l'interdiction progressive des gobelets comprenant du plastique, en renvoyant à un arrêté la définition du seuil de la teneur en plastique. Ainsi, dès le début de l'année 2020, les requérants ne pouvaient ignorer que les gobelets entièrement en plastique étaient interdits, et que la teneur en plastique allait être abaissée en vue de sa disparition. L'arrêté critiqué a été soumis à concertation, accessible aux entreprises, en mai 2021, et sa notification auprès de la commission européenne, rendue publique sur son site, a été opérée le 26 avril 2021. A cette date, alors même que le taux de plastique toléré envisagé, identique à celui arrêté définitivement, n'était pas certain, les requérants ne pouvaient ignorer, dans un contexte global dont l'horizon est la disparition complète du plastique comme composant de gobelets, que la teneur tolérée allait connaître rapidement une diminution importante. Si les requérants estiment que six à huit mois au moins étaient nécessaires pour assurer la reconversion, industrielle et commerciale, de leurs productions, ce délai doit être apprécié à compter d'avril 2021 et non de la publication de l'arrêté, et leur aurait permis de s'y conformer, au regard des périodes de commercialisation des stocks qu'il autorise. Il est vrai que les requérants soutiennent que les solutions alternatives au conditionnement en gobelet n'existent pas. Ils ont toutefois reconnu, au cours de l'audience, que le recours à des emballages multicouches ou à des bouteilles, d'ailleurs envisagé pour les secondes dans des courriers de l'entreprise Solinest à l'administration au printemps 2021, était techniquement possible, tout en affirmant que ces conditionnements ne satisferaient pas les mêmes besoins chez les consommateurs. Cependant, la préférence pour les gobelets résultant d'une étude portant sur plusieurs pays ne suffit pas à regarder comme établi que les autres conditionnements, lorsque l'arrêté sera en application, empêchant la commercialisation en gobelet, ne bénéficieront pas du report d'achat des consommateurs ne disposant plus du choix d'un autre contenant.<br/>
<br/>
              Il résulte de l'ensemble de ces considérations que, en l'état des informations soumises au juge des référés, la situation des requérants ne peut être regardée comme constitutive d'une urgence au sens de l'article L. 521-1 du code de justice administrative. Dès lors, faute que cette condition soit présente, et sans qu'il soit besoin d'examiner si les moyens invoqués seraient de nature à faire naître un doute sérieux sur la légalité de l'arrêté attaqué, les conclusions des requérants ne peuvent qu'être rejetées, y compris en tant qu'elles tendaient au versement d'une somme d'argent sur le fondement de l'article L. 761-1, dont les dispositions y font obstacle dès lors que l'Etat n'est pas la partie perdante.<br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
              ------------------<br/>
<br/>
Article 1er : La requête de la société Solinest et de la coopérative Arla Foods est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à la société Solinest, première requérante dénommée, et à la ministre de la transition écologique.<br/>
Copie en sera adressée au ministre de l'économie, des finances et de la relance.<br/>
Fait à Paris, le 9 décembre 2021<br/>
Signé : Thierry Tuot<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
