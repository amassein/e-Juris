<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038077338</ID>
<ANCIEN_ID>JG_L_2019_01_000000416418</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/07/73/CETATEXT000038077338.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème - 9ème chambres réunies, 30/01/2019, 416418, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-01-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>416418</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème - 9ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DIDIER, PINET ; SCP ODENT, POULET</AVOCATS>
<RAPPORTEUR>Mme Christelle Thomas</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Aurélie Bretonneau</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:416418.20190130</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme B...D...a demandé au tribunal administratif de Nantes d'annuler pour excès de pouvoir l'arrêté du 29 juillet 2015 par lequel le maire de La Baule-Escoublac a délivré à M. et Mme C...un permis de construire une maison d'habitation sur un terrain sis 22 allée de la chapelle sur le territoire de ladite commune. Par un jugement n° 1508982 du 9 octobre 2017, le tribunal administratif de Nantes a rejeté sa demande.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 11 décembre 2017 et 8 mars 2018 au secrétariat du contentieux du Conseil d'Etat, Mme D... demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler ce jugement ; <br/>
<br/>
              2°) de mettre à la charge de la commune de La Baule-Escoublac et des époux C...la somme de 4 200 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'urbanisme ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Christelle Thomas, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Aurélie Bretonneau, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Didier, Pinet, avocat de Mme B...D...et à la SCP Odent, Poulet, avocat de la commune de La Baule-Escoublac ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que, par un arrêté du 29 juillet 2015, le maire de La Baule-Escoublac a délivré à M. et Mme C...un permis de construire une maison d'habitation sur un terrain situé 22 allée de la chapelle. Par un jugement du 9 octobre 2017, le tribunal administratif de Nantes a rejeté la demande d'annulation de cet arrêté dont l'avait saisi MmeD..., propriétaire d'une maison située immédiatement en face du terrain d'assiette du projet. Celle-ci se pourvoit en cassation contre ce jugement.<br/>
<br/>
Sur la régularité de la procédure devant le tribunal administratif de Nantes :<br/>
<br/>
              2. Aux termes de l'article R. 611-1 du code de justice administrative, " la requête et les mémoires, ainsi que les pièces produites par les parties, sont déposés ou adressés au greffe. / La requête, le mémoire complémentaire annoncé dans la première requête et le premier mémoire de chaque défendeur sont communiqués aux parties avec les pièces jointes dans les conditions prévues aux articles R. 611-3, R. 611-5 et R. 611-6. / Les répliques, autres mémoires et pièces sont communiqués s'ils contiennent des éléments nouveaux ". Ces dispositions font obligation de communiquer à toutes les parties l'ensemble des mémoires et pièces produits. Ne sont toutefois pas soumis à une telle exigence les répliques et autres mémoires, observations ou pièces par lesquels les parties se bornent à réitérer des éléments de fait ou de droit qu'elles ont antérieurement fait valoir au cours de la procédure.<br/>
<br/>
              3. Il ressort des pièces de la procédure devant le tribunal administratif de Nantes que le second mémoire en défense produit par la commune la veille de la clôture de l'instruction se bornait à réitérer les arguments développés dans son premier mémoire en défense et relatifs aux conditions d'appréciation du retrait d'une construction en attique par rapport à la façade des niveaux inférieurs. Il s'ensuit qu'en ne communiquant pas ce second mémoire à la requérante, le tribunal administratif n'a pas entaché la procédure d'irrégularité. <br/>
<br/>
Sur le bien-fondé du jugement attaqué :<br/>
<br/>
              4. En premier lieu, aux termes de l'article UD 1 du règlement du plan local d'urbanisme de la commune de La Baule-Escoublac : " Occupations et utilisations du sol interdites / (...) Les nouvelles constructions et créations de logements supplémentaires sur les terrains situés à moins de 3, 20 mA... 69 (zone rouge au document graphique) et dans une zone de 100 mètres derrière un ouvrage ou un cordon dunaire jouant un rôle de protection contre les submersions (...) ". En jugeant que les conditions posées par cet article devaient être cumulativement remplies pour que soient interdites les nouvelles constructions, le tribunal administratif n'a entaché son jugement d'aucune erreur de droit. En recherchant si le terrain d'assiette de la construction litigieuse remplissait ou non ces deux conditions en se référant au document graphique correspondant à une annexe au règlement du plan local d'urbanisme, versé à la procédure contradictoire par la commune, le tribunal administratif n'a entaché son jugement d'aucune dénaturation ni d'aucune erreur de droit.<br/>
<br/>
              5. En deuxième lieu, aux termes de l'article UD 3 du règlement du plan local d'urbanisme, relatif aux conditions de desserte des terrains par les voies publiques ou privées et d'accès aux voies ouvertes au public, dans sa rédaction applicable au permis de construire litigieux : " (...) En cas de création de parkings en rives le long des voies publiques, l'accès direct des places sur la voie publique est interdit. Le parking doit être conçu de manière à ce que l'ensemble des places soit desservi avec un seul accès ou un nombre d'accès limité. / Lorsque le terrain est desservi par plusieurs voies, l'accès doit être réalisé sur celle sur laquelle il présentera la moindre gêne. (...) ". Après avoir relevé que le projet litigieux prévoit la création de quatre places de stationnement dont les deux premières sont situées l'une derrière l'autre du côté de l'allée de la chapelle au nord du terrain et les deux autres côte à côte le long de l'allée des marronniers au sud du terrain, le tribunal administratif a jugé que la desserte de ces places, qui se fait par un unique accès depuis chacune de ces deux voies publiques, respecte les conditions posées par l'article UD 3 du règlement du plan local d'urbanisme précité. En interprétant ainsi ces dispositions comme ne faisant pas obstacle à la création d'un accès réalisé sur chacune des deux voies publiques desservant le terrain d'assiette de la construction litigieuse, le tribunal administratif n'a pas commis d'erreur de droit.<br/>
<br/>
              6. En troisième et dernier lieu, aux termes de l'article II.1.1.1 du règlement de l'aire de mise en valeur de l'architecture et du patrimoine (AVAP) relatif à la hauteur maximale des constructions, en cas d'aménagement d'une toiture-terrasse, le retrait exigé de l'étage en attique est " d'au moins deux mètres par rapport à la façade des niveaux inférieurs ". En jugeant que ces dispositions, eu égard à la finalité qu'elles poursuivent, d'une part, et aux caractéristiques d'un étage en attique, d'autre part, sont applicables aux seules façades donnant sur la voie publique, le tribunal administratif n'a pas commis d'erreur de droit.<br/>
<br/>
              7. Il résulte de ce qui précède que Mme D...n'est pas fondée à demander l'annulation du jugement qu'elle attaque.<br/>
<br/>
              8. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de la commune de La Baule-Escoublac qui n'est pas, dans la présente instance, la partie perdante. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de Mme D...une somme de 2 500 euros à verser à la commune de La Baule-Escoublac au titre de ces mêmes dispositions. <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : Le pourvoi de Mme D...est rejeté. <br/>
Article 2 : Mme D...versera une somme de 2 500 euros à la commune de La Baule-Escoublac au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée à Madame B... D...et à la commune de La Baule-Escoublac.<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
