<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032750865</ID>
<ANCIEN_ID>JG_L_2016_06_000000393310</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/75/08/CETATEXT000032750865.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème chambre, 22/06/2016, 393310, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-06-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>393310</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Marie Gautier-Melleray</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Laurence Marion</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2016:393310.20160622</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              M. A...B...a demandé au tribunal administratif de Clermont-Ferrand, d'une part, d'annuler les décisions du ministre de l'intérieur retirant treize points de son permis de conduire à la suite des infractions commises les 29 juin 2010, 14 décembre 2011, 18 octobre 2012, 2 juillet 2012 et 28 septembre 2013, d'autre part, d'annuler la décision du ministre de l'intérieur du 7 mars 2014 récapitulant les retraits de points réalisés sur son permis de conduire, l'informant de la perte de validité de ce titre pour solde de points nul et lui enjoignant de le restituer, et, enfin, d'enjoindre au ministre de l'intérieur de réaffecter douze points sur ce permis à compter de la notification du jugement. Par un jugement n°1500044 du 10 juillet 2015, le tribunal administratif a partiellement fait droit à sa demande en annulant les décisions de retrait de points faisant suite aux infractions commises les 2 juillet 2012 et 18 octobre 2012, ainsi que la décision du 7 mars 2014 l'informant de la perte de validité de son permis de conduire, et en enjoignant au ministre de l'intérieur de lui restituer six points.<br/>
<br/>
              Par un pourvoi, enregistré le 9 septembre 2015 au secrétariat du contentieux du Conseil d'Etat, le ministre de l'intérieur demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter la demande de M.B....<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
<br/>
              - le code de la route ;<br/>
<br/>
              - le code de procédure pénale ;<br/>
<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Marie Gautier-Melleray, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Laurence Marion, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant que le président du tribunal administratif de Clermont-Ferrand s'est fondé sur l'absence de preuve de la délivrance de l'information prévue par les articles L. 223-3 et R. 223-3 du code de la route pour annuler les décisions de retrait de points consécutives aux infractions constatées les 2 juillet et 18 octobre 2012 ainsi que, par voie de conséquence, la décision constatant la perte de validité du permis de conduire de M. B...et pour enjoindre au ministre de restituer les six points correspondant sur le permis de conduire de M.B... ; <br/>
<br/>
              2. Considérant que la délivrance, préalablement au règlement de l'amende, de l'information prévue par les articles L. 223-3 et R. 223-3 du code de la route constitue une condition de la légalité des décisions de retrait de points ; <br/>
<br/>
              3. Considérant, d'une part, que le paiement par le contrevenant de l'amende forfaitaire majorée prévue par le second alinéa de l'article 529-2 du code de procédure pénale implique nécessairement qu'il a préalablement reçu l'avis d'amende forfaitaire majorée ; que tant avant qu'elles ne soient rendues obligatoires par un arrêté du 13 mai 2011 introduisant dans le code de procédure pénale un article A. 37-28 que depuis l'entrée en vigueur de cet arrêté, le formulaire d'avis d'amende forfaitaire majorée utilisé par l'administration est revêtu de mentions qui permettent au contrevenant de comprendre qu'en l'absence de contestation de l'amende, il sera procédé au retrait de points et qui portent à sa connaissance l'ensemble des informations requises par les articles L. 223-3 et R. 223-3 du code de la route ; qu'ainsi, le paiement de l'amende forfaitaire majorée suffit à établir que l'administration s'est acquittée envers le titulaire du permis de son obligation d'information, à moins que l'intéressé, à qui il appartient à cette fin de produire l'avis qu'il a nécessairement reçu, ne démontre que cet avis était inexact ou incomplet ; <br/>
<br/>
              4. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que le ministre avait produit le relevé intégral d'information de M. B...ainsi qu'un bordereau de situation des amendes et condamnations pécuniaires de l'intéressé établi au 22 février 2014 par la trésorerie de Clermont-Ferrand, lesquels faisaient apparaître que M. B...avait payé le 23 novembre 2012 l'amende forfaitaire majorée correspondant à l'infraction commise le 2 juillet 2012 ; que, dans ces conditions et alors que l'intéressé n'alléguait pas avoir reçu un avis d'amende forfaitaire majorée incomplet ou inexact, le président du tribunal administratif a dénaturé les pièces du dossier qui lui était soumis en relevant " que l'administration ne produit aucun document concernant cette infraction, notamment quant à un éventuel paiement de l'amende forfaitaire majorée " pour accueillir le moyen tiré de ce que M. B...n'aurait pas reçu l'information prévue par les articles L. 223-3 et R. 223-3 du code de la route et relative à cette infraction ;  que, par suite, le ministre de l'intérieur est fondé à demander l'annulation du jugement en tant qu'il a annulé le retrait de points correspondant ;<br/>
<br/>
              5. Considérant, d'autre part, que lorsqu'une contravention soumise à la procédure de l'amende forfaitaire est relevée avec interception du véhicule et donne lieu au paiement immédiat de l'amende entre les mains de l'agent verbalisateur, il incombe à l'administration d'apporter la preuve, par la production de la souche de la quittance prévue à l'article R. 49-2 du code de procédure pénale dépourvue de réserve sur la délivrance de l'information requise, que celle-ci est bien intervenue préalablement au paiement ;<br/>
<br/>
              6. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que le ministre de l'intérieur avait produit le duplicata de la quittance de paiement de l'infraction commise le 18 octobre 2012 et que celle-ci, signée par M. B...et dépourvue de toute réserve sur la délivrance de l'information requise alors que la case " retrait de points " avait été cochée, permettait d'établir que M. B... s'était vu délivrer l'information requise ; que, dès lors, en estimant que l'administration n'avait pas apporté la preuve de la délivrance de l'information prévue aux articles L.  223-3 et R.  222-3 du code de la route à M.B..., le tribunal administratif a dénaturé les pièces du dossier qui lui était soumis ; que, par suite, le ministre de l'intérieur est fondé à demander l'annulation du jugement en tant qu'il a annulé les deux retraits de points correspondant pour un total de six points ;<br/>
<br/>
              7. Considérant qu'il résulte de l'annulation du jugement en tant qu'il annule ces deux décisions de retrait de points consécutives aux infractions commises par M. B...les 2 juillet et 18 octobre 2012 que le ministre de l'intérieur est également fondé à demander l'annulation du jugement en tant qu'il en tire les conséquences sur la validité du permis de conduire de l'intéressé et qu'il lui enjoint de restituer les six points correspondant ;<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Les articles 1er et 2 du jugement du 10 juillet 2015 du tribunal administratif de Clermont-Ferrand sont annulés.<br/>
<br/>
Article 2 : L'affaire est renvoyée au tribunal administratif de Clermont-Ferrand dans la limite de la cassation ainsi prononcée.<br/>
<br/>
Article 3 : La présente décision sera notifiée au ministre de l'intérieur et à M. A...B....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
