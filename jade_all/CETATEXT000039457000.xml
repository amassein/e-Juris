<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039457000</ID>
<ANCIEN_ID>JG_L_2019_12_000000423326</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/45/70/CETATEXT000039457000.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 06/12/2019, 423326, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-12-06</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>423326</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP CAPRON ; SCP SPINOSI, SUREAU</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT>M. Alexandre Lallet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CEORD:2019:423326.20191206</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête sommaire, un mémoire complémentaire et un nouveau mémoire, enregistrés les 17 août et 16 novembre 2018 et le 31 octobre 2019 au secrétariat du contentieux du Conseil d'Etat, Mme A... demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision par laquelle la Commission nationale de l'informatique et des libertés (CNIL) a clôturé sa plainte aux fins de déréférencement d'un lien obtenu sur la base d'une recherche effectuée à partir de son nom sur le moteur de recherche Google, qui lui a été notifiée par un courrier du 26 mars 2018 ; <br/>
<br/>
              2°) de mettre à la charge de la CNIL la somme de 3 000 euros à verser à la SCP Capron, son avocat, au titre de l'article L. 761-1 du code de justice administrative et de l'article 37 de la loi du 10 juillet 1991.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ; <br/>
<br/>
              Vu : <br/>
              - la Charte des droits fondamentaux de l'Union européenne ; <br/>
              - le règlement (UE) n° 2016/679 du Parlement européen et du Conseil du 27 avril 2016 ; <br/>
              - la loi n° 78-17 du 6 janvier 1978 ;<br/>
              - l'arrêt de la Cour de justice de l'Union européenne du 13 mai 2014, Google Spain SL, Google Inc. contre Agencia Espanola de Proteccion de Datos, Mario Costeja Gonzalez (C-131/12) ;<br/>
              - l'arrêt de la Cour de justice de l'Union européenne du 24 septembre 2019, GC, AF, BH et ED contre CNIL (C-136/17) ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Myriam Benlolo Carabot, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Alexandre Lallet, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Capron, avocat de Mme A... et à la SCP Spinosi, Sureau, avocat de la société Google LLC ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier que Mme A... a demandé à la société Google de procéder au déréférencement, dans les résultats affichés par le moteur de recherche qu'elle exploite à la suite d'une recherche portant sur son nom, d'un lien hypertexte renvoyant vers les résultats des élections législatives de 2012, auxquelles elle était candidate. A la suite du refus opposé par la société Google, elle a saisi la Commission nationale de l'informatique et des libertés (CNIL) d'une plainte tendant à ce qu'il soit enjoint à cette société de procéder au déréférencement du lien en cause. Par un courrier du 26 mars 2018, la présidente de la CNIL l'a informée de la clôture de sa plainte. Mme A... demande l'annulation pour excès de pouvoir du refus de la CNIL de mettre en demeure la société Google de procéder au déréférencement demandé. <br/>
<br/>
              2. L'effet utile de l'annulation pour excès de pouvoir du refus de la CNIL de mettre en demeure l'exploitant d'un moteur de recherche de procéder au déréférencement de liens vers des pages web réside dans l'obligation, que le juge peut prescrire d'office en vertu des dispositions de l'article L. 911-1 du code de justice administrative, pour la CNIL de procéder à une telle mise en demeure afin que disparaissent de la liste de résultats affichée à la suite d'une recherche les liens en cause.<br/>
<br/>
              3. En premier lieu, il résulte de ce qui a été dit au point précédent que lorsqu'il est saisi de conclusions aux fins d'annulation du refus de la CNIL de mettre en demeure l'exploitant d'un moteur de recherche de procéder au déréférencement de liens, le juge de l'excès de pouvoir est conduit à apprécier la légalité d'un tel refus au regard des règles applicables et des circonstances prévalant à la date de sa décision.<br/>
<br/>
              4. En second lieu, dans l'hypothèse où il apparaît que les liens litigieux ont été déréférencés à la date à laquelle il statue, soit à la seule initiative de l'exploitant du moteur de recherche, soit pour la mise en oeuvre d'une mise en demeure, le juge de l'excès de pouvoir doit constater que le litige porté devant lui a perdu son objet. <br/>
<br/>
              5. Il ressort des pièces du dossier qu'ainsi qu'elle le fait valoir dans ses écritures devant le Conseil d'Etat, la société Google a, postérieurement à l'introduction de la présente requête, procédé au déréférencement du lien en litige. Il découle des motifs énoncés au point précédent que les conclusions dirigées contre le refus de la CNIL d'ordonner à la société Google de procéder au déréférencement de ce lien ont perdu leur objet et qu'il n'y a, dès lors, plus lieu d'y statuer.<br/>
<br/>
              6. Il n'y a pas lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme que demande Mme A... au titre de l'article L. 761-1 du code de justice administrative et de l'article 37 de la loi du 10 juillet 1991. <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
                               --------------<br/>
<br/>
Article 1er : Il n'y a pas lieu de statuer sur les conclusions de la requête de Mme X. <br/>
Article 2 : Les conclusions présentées par Mme A... au titre de l'article L. 761-1 du code de justice administrative et de l'article 37 de la loi du 10 juillet 1991 sont rejetées. <br/>
Article 3 : La présente décision sera notifiée à Mme A..., à la Commission nationale de l'informatique et des libertés et à la société Google LLC. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
