<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000045122258</ID>
<ANCIEN_ID>JG_L_2022_02_000000451960</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/45/12/22/CETATEXT000045122258.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème chambre, 03/02/2022, 451960, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2022-02-03</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>451960</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SARL MATUCHANSKY, POUPOT, VALDELIEVRE ; CABINET ROUSSEAU ET TAPIE</AVOCATS>
<RAPPORTEUR>M. Clément Tonon</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Sophie Roussel</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2022:451960.20220203</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu les procédures suivantes :<br/>
<br/>
              1/ Sous le n° 451960, par une requête sommaire et un mémoire complémentaire, enregistrés les 23 avril et 23 juillet 2021 au secrétariat du contentieux du Conseil d'Etat, la société anonyme sportive professionnelle Amiens Sporting Club Football (Amiens SC) demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler la décision du 24 février 2021 par laquelle le conseil d'administration de la Ligue de football professionnel a adopté les guides de répartition des droits audiovisuels Ligue 1 et Ligue 2 pour l'ensemble de la saison 2020/2021, qui annulent et remplacent rétroactivement les guides précédemment adoptés le 24 septembre 2020, ensemble son annexe intitulée " Guide de répartition droits audiovisuels Ligue 1 saison 2020/2021 " ;<br/>
<br/>
              2°) de mettre à la charge de la Ligue de football professionnel la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              2/ Sous le n° 451965, par une requête sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 23 avril, 19 juillet et 13 décembre 2021 au secrétariat du contentieux du Conseil d'Etat, la société anonyme sportive professionnelle Toulouse Football Club (TFC) demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler la décision du 24 février 2021 par laquelle le conseil d'administration de la Ligue de football professionnel a adopté les guides de répartition des droits audiovisuels Ligue 1 et Ligue 2 pour l'ensemble de la saison 2020/2021, qui annulent et remplacent rétroactivement les guides précédemment adoptés le 24 septembre 2020, ensemble son annexe intitulée " Guide de répartition droits audiovisuels Ligue 1 saison 2020/2021 " ;<br/>
<br/>
              2°) de mettre à la charge de la Ligue de football professionnel la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
			....................................................................................<br/>
<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu le code du sport ;<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Clément Tonon, auditeur, <br/>
<br/>
              - les conclusions de Mme Sophie Roussel, rapporteure publique,<br/>
<br/>
              La parole ayant été donnée, après les conclusions, au cabinet Rousseau et Tapie, avocat de la société anonyme sportive professionnelle Amiens Sporting Club Football (Amiens SC), à la SCP Duhamel, Rameix, Gury, Maître, avocat de la société Toulouse Football club, et à la SARL Matuchansky, Poupot, Valdelièvre, avocat de la Ligue de football professionnel ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1.	Les requêtes de la société anonyme sportive professionnelle Amiens Sporting Club Football et de la société anonyme sportive professionnelle Toulouse Football Club étant dirigées contre les mêmes décisions, il y a lieu de les joindre pour y statuer par une même décision.<br/>
<br/>
              2.	Aux termes de l'article L. 333-1 du code du sport : " Les fédérations sportives, ainsi que les organisateurs de manifestations sportives mentionnés à l'article L. 331-5, sont propriétaires du droit d'exploitation des manifestations ou compétitions sportives qu'ils organisent./ Toute fédération sportive peut céder aux sociétés sportives, à titre gratuit, la propriété de tout ou partie des droits d'exploitation audiovisuelle des compétitions ou manifestations sportives organisées chaque saison sportive par la ligue professionnelle qu'elle a créée, dès lors que ces sociétés participent à ces compétitions ou manifestations sportives. La cession bénéficie alors à chacune de ces sociétés ". Aux termes de l'article L. 333-2 du même code : " Les droits d'exploitation audiovisuelle cédés aux sociétés sportives sont commercialisés par la ligue professionnelle dans des conditions et limites précisées par décret en Conseil d'Etat (...) ". Aux termes de l'article L. 333-3 du même code : " Afin de garantir l'intérêt général et les principes d'unité et de solidarité entre les activités à caractère professionnel et les activités à caractère amateur, les produits de la commercialisation par la ligue des droits d'exploitation des sociétés sont répartis entre la fédération, la ligue et les sociétés./ La part de ces produits destinée à la fédération et celle destinée à la ligue sont fixées par la convention passée entre la fédération et la ligue professionnelle correspondante./ Les produits revenant aux sociétés leur sont redistribués selon un principe de mutualisation, en tenant compte de critères arrêtés par la ligue et fondés notamment sur la solidarité existant entre les sociétés, ainsi que sur leurs performances sportives et leur notoriété ".<br/>
<br/>
              3.	Les requérantes demandent au Conseil d'Etat d'annuler la décision du 24 février 2021 par laquelle le conseil d'administration de la Ligue de football professionnel a adopté le guide de répartition des droits audiovisuels 2020/2021, ensemble son annexe intitulée " Guide de répartition droits audiovisuels Ligue 1 saison 2020/2021 ", qui annule et remplace sa précédente décision du 24 septembre 2020.<br/>
<br/>
              4.	Si la Ligue de football professionnel, personne morale de droit privé, s'est vu confier, par convention conclue avec la Fédération française, la gestion du football professionnel, notamment l'organisation et la règlementation des championnats de Ligue 1 et de Ligue 2, et est à ce titre chargée d'une mission de service public administratif, les actes et décisions pris par elle ne ressortissent à la compétence de la juridiction administrative que pour autant qu'ils constituent l'exercice d'une prérogative de puissance publique pour l'accomplissement de cette mission. Il n'en va pas ainsi de la décision contestée, qui se rattache à l'activité de la Ligue de commercialisation des droits d'exploitation audiovisuelle et qui concerne la redistribution des produits en résultant entre les sociétés sportives propriétaires de ces droits. Par suite, les conclusions aux fins d'annulation des requêtes doivent être rejetées comme portées devant un ordre de juridiction incompétent pour en connaître. <br/>
<br/>
              5.	Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mise à la charge de la Ligue de football professionnel, qui n'est pas, dans la présente instance, la partie perdante, une somme à ce titre. Il n'y a pas lieu, dans les circonstances de l'espèce, de mettre à la charge des sociétés requérantes le versement d'une somme au même titre. <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
                                          --------------<br/>
<br/>
Article 1er : Les requêtes de la société anonyme sportive professionnelle Amiens Sporting Club Football (Amiens SC) et de la société anonyme sportive professionnelle Toulouse Football Club (TFC) sont rejetées comme portées devant une juridiction incompétente pour en connaître.<br/>
Article 2 : Les conclusions présentées par la Ligue de football professionnel au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La présente décision sera notifiée à la société anonyme sportive professionnelle Amiens Sporting Club Football (Amiens SC), à la société anonyme sportive professionnelle Toulouse Football Club (TFC) et à la Ligue de football professionnel.<br/>
Copie en sera adressée au ministre de l'éducation nationale, de la jeunesse et des sports.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
