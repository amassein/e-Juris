<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027353513</ID>
<ANCIEN_ID>JG_L_2013_04_000000348237</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/35/35/CETATEXT000027353513.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème et 10ème sous-sections réunies, 24/04/2013, 348237, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-04-24</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>348237</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème et 10ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BOULLEZ</AVOCATS>
<RAPPORTEUR>M. Olivier Gariazzo</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Aladjidi</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:348237.20130424</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 7 avril et 7 juillet 2011 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. et Mme B...A..., demeurant... ; M. et Mme A...demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 09PA01714 du 3 février 2011 par lequel la cour administrative d'appel de Paris a rejeté leur requête tendant à l'annulation du jugement n° 0317436/2 du 22 janvier 2009 par lequel le tribunal administratif de Paris a rejeté leur demande tendant à la décharge des cotisations supplémentaires d'impôt sur le revenu et de contributions sociales auxquelles ils ont été assujettis au titre des années 1997 et 1998, ainsi que des pénalités correspondantes ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à leur appel ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat le versement d'une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code général des impôts et le livre des procédures fiscales ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Olivier Gariazzo, Maître des Requêtes en service extraordinaire,  <br/>
<br/>
              - les observations de la SCP Boullez, avocat de M. et Mme A...,<br/>
<br/>
              - les conclusions de M. Frédéric Aladjidi, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Boullez, avocat de M. et Mme A... ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que M. et Mme A... ont, le 25 mars 1998, acquis un appartement, financé par la mobilisation des fonds disponibles sur leur compte bancaire et, pour le solde, par un emprunt bancaire ; qu'ils ont fait l'objet d'un examen contradictoire de leur situation fiscale personnelle portant sur les années 1997 et 1998, à l'issue duquel l'administration a réintégré dans leur revenu global, d'une part la somme de 600 643 francs, créditée sur leur compte par deux virements des 9 et 12 mars 1998, d'autre part la somme de 5 000 francs, créditée sur ce même compte le 12 juin 1998 ; qu'ils ont demandé la décharge des cotisations supplémentaires d'impôt sur le revenu et de contributions sociales résultant de ce redressement au tribunal administratif de Paris qui, par un jugement du 22 janvier 2009, a rejeté leur demande ; que la cour administrative d'appel de Paris a, par un arrêt du 3 février 2011, rejeté leur requête tendant à l'annulation de ce jugement ; qu'ils se pourvoient en cassation contre cet arrêt en tant qu'il statue sur le bien-fondé de la réintégration de la somme précitée de 600 643 francs dans leur revenu imposable ;<br/>
<br/>
              2. Considérant qu'il appartient à l'administration fiscale, lorsqu'elle entend remettre en cause, même par voie d'imposition d'office, le caractère non imposable de sommes perçues par un contribuable, dont il est établi qu'elles lui ont été versées par l'un de ses parents et alors qu'elle ne se prévaut pas de l'existence entre eux d'une relation d'affaires, de justifier que les sommes en cause ne revêtent pas le caractère d'un prêt familial ;<br/>
<br/>
              3. Considérant que, pour rejeter la requête de M. et Mme A..., la cour, après avoir admis que la somme de 600 643 francs en litige avait été créditée sur leur compte bancaire par deux virements des 9 et 12 mars 1998 émanant de la mère de M. A..., s'est bornée à relever que l'administration fiscale française se trouvait dans l'impossibilité de recueillir en République populaire de Chine, où résidait la mère de M. A..., des renseignements sur les revenus et le patrimoine de celle-ci de nature à lui permettre de s'assurer de la vraisemblance d'un prêt consenti par elle et en a déduit que les virements en cause ne pouvaient être présumés correspondre à des prêts familiaux ; qu'en statuant ainsi, elle a entaché son arrêt d'une erreur de droit ; qu'il y a lieu, par suite, et sans qu'il soit besoin d'examiner l'autre moyen du pourvoi, d'annuler l'arrêt attaqué en tant qu'il statue sur la réintégration dans l'assiette du revenu imposable de M. et Mme A... de la somme de 600 643 francs;<br/>
<br/>
              4. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond, dans la limite de la cassation ainsi prononcée, par application de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              5. Considérant qu'il résulte de l'instruction que M. et Mme A... ont reçu une somme de 199 843 francs et une somme de 400 800 francs, portées au crédit de leur compte bancaire les 9 et 12 mars 1998 ; qu'ils ont établi que la mère de M. A... était le donneur d'ordre de ces versements; que l'administration fiscale ne démontre pas l'existence d'une relation d'affaires entre les époux A...et la mère de M. A... et se borne à relever que les premiers n'auraient procédé à aucun remboursement au titre de ce prêt et à mettre en doute, sans étayer son affirmation, que la seconde puisse avoir disposé des sommes nécessaires pour financer un prêt du montant en cause, dans la mesure où ses revenus proviennent seulement d'un commerce de pâtisserie qu'elle tient en République populaire de Chine ; qu'ainsi, l'administration n'apporte pas d'élément de nature à établir que les sommes versées ne correspondraient pas à un prêt à caractère familial ; que M. et Mme A... sont, dès lors, fondés à soutenir que c'est à tort que le tribunal administratif de Paris a rejeté leur demande de décharge des cotisations supplémentaires d'impôt sur le revenu et de contributions sociales, ainsi que des pénalités correspondantes, auxquelles ils ont été assujettis au titre de l'année 1998, du fait de la réintégration dans l'assiette de leur revenu imposable de la somme de 600 643 francs;<br/>
<br/>
              6. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : L'arrêt du 3 février 2011 de la cour administrative d'appel de Paris est annulé en tant qu'il statue sur la réintégration dans l'assiette du revenu imposable de M. et Mme A... de la somme de 600 643 francs correspondant aux sommes virées sur leur compte bancaire sur ordre de la mère de M. A....<br/>
Article 2 : M. et Mme A... sont déchargés des cotisations supplémentaires d'impôt sur le revenu et de contributions sociales, ainsi que des pénalités correspondantes, mises à leur charge en conséquence du redressement mentionné à l'article 1er. <br/>
Article 3 : Le jugement du 22 janvier 2009 du tribunal administratif de Paris est réformé en ce qu'il a de contraire à la présente décision.<br/>
Article 4 : L'Etat versera à M. et Mme A... une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 5 : La présente décision sera notifiée à M. et Mme B... A...et au ministre de l'économie et des finances.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
