<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026768143</ID>
<ANCIEN_ID>JG_L_2012_12_000000357865</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/76/81/CETATEXT000026768143.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème et 7ème sous-sections réunies, 12/12/2012, 357865</TITRE>
<DATE_DEC>2012-12-12</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>357865</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème et 7ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LE BRET-DESACHE</AVOCATS>
<RAPPORTEUR>M. Camille Pascal</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Damien Botteghi</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2012:357865.20121212</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi, enregistré le 23 mars 2012 au secrétariat du contentieux du Conseil d'Etat, présenté par le garde des sceaux, ministre de la justice ; le ministre demande au Conseil d'Etat d'annuler l'arrêt n° 11PA02570 du 26 janvier 2012 par lequel la cour administrative d'appel de Paris a rejeté son recours contre le jugement n° 1005044/7-1 du 31 mars 2011 du tribunal administratif de Paris ayant annulé sa décision du 9 mars 2009 par laquelle il a refusé le changement de nom sollicité par Mlle C...D...et M.  B...pour leur fille mineure A...E..., ainsi que la décision du 12 janvier 2010 rejetant le recours gracieux formé par Mlle D...;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code civil ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Camille Pascal, Conseiller d'Etat,<br/>
<br/>
              - les observations de la SCP Le Bret-Desache, avocat de Mlle C...D...,<br/>
<br/>
              - les conclusions de M. Damien Botteghi, Rapporteur public,<br/>
<br/>
              La parole ayant à nouveau été donnée à la SCP Le Bret-Desache, avocat de Mlle C... D...;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1.	Considérant qu'aux termes du premier alinéa de l'article 61 du code civil : " Toute personne qui justifie d'un intérêt légitime peut demander à changer de nom. " ; <br/>
<br/>
              2.	Considérant qu'il ressort des énonciations de l'arrêt attaqué que Mlle D... a donné naissance à la jeune A...le 20 août 2008 ; que cette naissance a été déclarée le lendemain par M.B..., le père de l'enfant, auprès des services de l'état-civil de Vichy ; que, alors que le voeu des deux parents était de donner à leur fille leurs deux noms accolés, M. B...a omis, lors de la déclaration de naissance, de déposer une déclaration conjointe mentionnant ce choix de nom pour l'enfant ; qu'en conséquence de cette omission, la jeune A...s'est vu conférer le seul nom de son père par application des dispositions du premier alinéa de l'article 311-21 du code civil, selon lesquelles : " Lorsque la filiation d'un enfant est établie à l'égard de ses deux parents au plus tard le jour de la déclaration de sa naissance ou par la suite mais simultanément, ces derniers choisissent le nom de famille qui lui est dévolu : soit le nom du père, soit le nom de la mère, soit leurs deux noms accolés dans l'ordre choisi par eux dans la limite d'un nom de famille pour chacun d'eux. En l'absence de déclaration conjointe à l'officier de l'état civil mentionnant le choix du nom de l'enfant, celui-ci prend le nom de celui de ses parents à l'égard duquel sa filiation est établie en premier lieu et le nom de son père si sa filiation est établie simultanément à l'égard de l'un et de l'autre " ; que les parents ont entrepris sans délai diverses démarches pour tenter de faire rectifier les conséquences de cette omission ; que ces démarches étant demeurées infructueuses, ils ont saisi ensemble le garde des sceaux, ministre de la justice, d'une demande tendant à ce que le nom de l'enfant soit changé en "B...D..." ; <br/>
<br/>
              3.	Considérant que cette demande de changement de nom a été rejetée par décision du 9 mars 2009, confirmée sur recours gracieux le 12 janvier 2010 ; que le tribunal administratif de Paris a annulé ces refus par un jugement du 31 mars 2011 ; que la cour administrative d'appel de Paris, par l'arrêt attaqué, a rejeté le recours du ministre contre ce jugement ;<br/>
<br/>
              4.	Considérant que, pour rejeter le recours dont elle était saisie, la cour administrative d'appel a relevé qu'il était constant que les deux parents avaient décidé de donner leurs deux noms accolés à leur fille et estimé que l'omission de M. B...de procéder à la déclaration conjointe du choix du nom concomitamment à la déclaration de naissance avait été rendue possible par des circonstances de fait particulières, liées aux suites d'un accouchement difficile et à l'absence d'information donnée sur les conséquences irréversibles de l'indication du nom lors de la déclaration de naissance pour le premier enfant d'une fratrie puis pour ses éventuels frères et soeurs ; qu'après avoir en outre relevé que les deux parents avaient entrepris sans délai diverses démarches pour tenter de faire rectifier l'erreur commise par le père de l'enfant lors de la déclaration de naissance, la cour a estimé que les parents avaient fait preuve de détermination et de diligence pour tenter de pallier les conséquences, qu'elle a jugées disproportionnées, de l'omission du dépôt de la déclaration conjointe du nom de l'enfant choisi par les deux parents ; qu'au vu de ces circonstances, qualifiées d'exceptionnelles, la cour administrative d'appel s'est fondée, pour rejeter le recours du ministre, sur ce que le motif affectif tiré de ce que Mlle D...avait la volonté de transmettre son nom, qui est celui de ses parents adoptifs et lui avait été conféré en conséquence de son adoption prononcée par jugement du tribunal de grande instance de Clermont-Ferrand du 27 avril 1981, devait être regardé, dans les circonstances particulières de l'espèce, comme caractérisant un intérêt légitime pour demander le changement du nom de l'enfant ; <br/>
<br/>
              5.	Considérant qu'en admettant ainsi, après avoir souverainement caractérisé l'existence de circonstances exceptionnelles en rapport avec l'objet de la demande, que le motif d'ordre affectif allégué caractérisait en l'espèce un intérêt légitime à changer de nom au sens de l'article 61 du code civil, la cour administrative d'appel, qui a suffisamment motivé sa décision, n'a pas commis d'erreur de droit ; que, dès lors, le garde des sceaux, ministre de la justice n'est pas fondé à demander l'annulation de l'arrêt attaqué ; <br/>
<br/>
              6.	Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat le versement à Mlle D...de la somme de 2 500 euros qu'elle demande sur le fondement de l'article L. 761-1 du code de justice administrative au titre des frais exposés par elle et non compris dans les dépens ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi du garde des sceaux, ministre de la justice est rejeté.<br/>
<br/>
Article 2 : L'Etat versera à Mlle D...la somme de 2 500 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
Article 3 : La présente décision sera notifiée à la garde des sceaux, ministre de la justice et à Mlle C...D....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">26-01-03 DROITS CIVILS ET INDIVIDUELS. ÉTAT DES PERSONNES. CHANGEMENT DE NOM PATRONYMIQUE. - EXISTENCE D'UN INTÉRÊT LÉGITIME - EXISTENCE EN L'ESPÈCE.
</SCT>
<ANA ID="9A"> 26-01-03 Cas dans lequel, bien que les deux parents avaient décidé de donner leurs deux noms accolés à leur fille, le père a omis de procéder à la déclaration conjointe du choix du nom concomitamment à la déclaration de naissance en raison de circonstances de fait particulières, liées aux suites d'un accouchement difficile et à l'absence d'information donnée sur les conséquences irréversibles de l'indication du nom lors de la déclaration de naissance pour le premier enfant d'une fratrie puis pour ses éventuels frères et soeurs, et dans lequel les deux parents avaient entrepris sans délai diverses démarches pour tenter de faire rectifier l'erreur commise.... ...En admettant ainsi, après avoir souverainement caractérisé l'existence de circonstances exceptionnelles en rapport avec l'objet de la demande, que le motif d'ordre affectif allégué tenant à ce que la mère avait la volonté de transmettre son nom, qui est celui de ses parents adoptifs et lui avait été conféré en conséquence de son adoption, caractérisait en l'espèce un intérêt légitime à changer de nom au sens de l'article 61 du code civil, une cour ne commet pas d'erreur de droit.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
