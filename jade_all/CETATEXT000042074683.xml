<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042074683</ID>
<ANCIEN_ID>JG_L_2020_06_000000441163</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/07/46/CETATEXT000042074683.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 26/06/2020, 441163, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-06-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>441163</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROUSSEAU, TAPIE ; SCP MATUCHANSKY, POUPOT, VALDELIEVRE ; SCP GATINEAU, FATTACCINI, REBEYROL</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2020:441163.20200626</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
              Par une requête et un mémoire en réplique, enregistrés les 12 et 22 juin au secrétariat du contentieux du Conseil d'Etat, la société Espérance Sportive Troyes Aube Champagne (ESTAC) et la société anonyme sportive professionnelle (SASP) Clermont Foot 63 demandent au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative :<br/>
              1°) de suspendre l'exécution de la décision du 30 avril 2020 par laquelle le conseil d'administration de la Ligue de football professionnel a supprimé les matchs de " play-offs " devant opposer les clubs ayant terminé 3ème, 4ème et 5ème de Ligue 2 ainsi que le match de barrage devant opposer le vainqueur de ces play-offs au 18ème de Ligue 1 et en tirer toutes les conséquences ;<br/>
<br/>
              2°) d'enjoindre à la Ligue de football professionnel de prendre ou de faire prendre par toute instance compétente toutes dispositions permettant l'organisation des matchs de " play-offs " devant opposer les clubs ayant terminé 3ème, 4ème et 5ème de Ligue 2 ainsi que du match de barrage devant opposer le vainqueur de ces play-offs au 18ème de Ligue 1 ;<br/>
<br/>
              3°) de mettre à la charge de la Ligue de football professionnel la somme de 10 000 euros à verser aux sociétés requérantes au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Elles soutiennent que :<br/>
              - la condition d'urgence est remplie, la décision attaquée préjudiciant de manière suffisamment grave et immédiate à leur situation et aux intérêts qu'elles entendent défendre en ce que, en premier lieu, elles se trouvent dans l'incapacité de concourir, pour la saison 2020-2021, en Ligue 1, alors même que leurs résultats sportifs rendaient possible l'accession à un championnat de catégorie supérieure, en deuxième lieu, l'imminence de la prochaine saison sportive, débutant au plus tard le 21 août 2020, implique la nécessité de connaître le championnat dans lequel elles évolueront et, en dernier lieu, les catégories de compétition disputées ont un impact immédiat sur les ressources des clubs et sur les décisions à venir relatives aux investissements, notamment humains ;<br/>
              - il existe un doute sérieux quant à la légalité de la décision attaquée ;<br/>
              - la décision attaquée est entachée d'incompétence, dès lors que seule l'assemblée générale de la Ligue de football professionnel pouvait procéder au changement de format des compétitions et que le choix de recourir ou non à des " play-offs " et barrages concerne le format des compétitions ; à titre subsidiaire, à supposer même qu'un tel choix ne puisse être regardé comme concernant le format des compétitions, l'organisation des compétitions appartient à la commission des compétitions et seul le bureau peut décider de déprogrammer des rencontres ;<br/>
              - elle porte atteinte aux intérêts généraux du football compte tenu, d'une part, des incertitudes qui continuaient de peser sur le sort du club de Ligue 2 susceptible d'être relégué en National 1 à l'issue des barrages et, d'autre part, de la distorsion qu'elle autorisait par principe dans les règles applicables pour l'accession des clubs de Ligue 2 à la Ligue 1 et de relégation des clubs de Ligue 2 vers le championnat National, avec les conséquences qui en résultent pour les clubs de Ligue 2 ;<br/>
              - elle méconnaît de manière disproportionnée et non justifiée par les circonstances le principe d'équité sportive et d'égalité de traitement entre les équipes participant à un même championnat, dès lors qu'elle traite différemment les équipes arrivées en 1ère et 2ème position et celles classées 3ème, 4ème et 5ème, qui ont été privées de toute possibilité de monter dans la division supérieure, alors que les contraintes du calendrier ne s'opposaient pas à l'organisation de " play-offs " et de barrages ;<br/>
              - elle est entachée d'erreur manifeste d'appréciation, dès lors que l'organisation des matchs de play-offs et de barrage était, d'une part, envisageable au début du mois d'août, voire au mois de juillet, compte tenu de l'évolution positive de la situation sanitaire et, d'autre part, nécessaire eu égard aux enjeux sportifs et financiers pour les clubs de Ligue 2 concernés.<br/>
              Par un mémoire en défense, enregistré le 17 juin 2020, la Ligue de football professionnel conclut au rejet de la requête et à ce que soit mise à la charge des clubs requérants la somme de 6 000 euros au titre de l'article L. 761-1 du code de justice administrative. Elle soutient que la condition d'urgence n'est pas remplie, qu'aucun des moyens soulevés n'est opérant et, à titre subsidiaire, qu'aucun de ces moyens n'est de nature à créer un doute sérieux quant à la légalité de la décision attaquée.<br/>
<br/>
              La Fédération française de football a présenté des observations, enregistrées le 22 juin 2020.<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
<br/>
              Vu :<br/>
              - le code de la santé publique ;<br/>
              - le code du sport ;<br/>
              - la loi n° 2020-290 du 23 mars 2020 ;<br/>
              - le décret n° 2020-293 du 23 mars 2020 ;<br/>
              - le règlement administratif de la Ligue de football professionnel 2019/2020 ;<br/>
              - le code de justice administrative et l'ordonnance n° 2020-305 du 25 mars 2020 modifiée ;<br/>
              Les parties ont été informées, sur le fondement de l'article 9 de l'ordonnance du 25 mars 2020 portant adaptation des règles applicables devant les juridictions de l'ordre administratif, de ce qu'aucune audience ne se tiendrait et de ce que la clôture de l'instruction serait fixée le 22 juin 2020 à 19 heures.<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. Aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : "Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision ". <br/>
<br/>
              Sur les circonstances dans lesquelles est intervenue la décision contestée :<br/>
<br/>
              2. En raison de l'émergence d'un nouveau coronavirus (covid-19), de caractère pathogène et particulièrement contagieux, et de sa propagation sur le territoire français, le ministre des solidarités et de la santé, par plusieurs arrêtés successifs pris à compter du 4 mars 2020, a interdit, de façon de plus en plus stricte, les rassemblements, réunions ou activités mettant en présence de manière simultanée un certain nombre de personnes, et a décidé la fermeture d'un nombre croissant de catégories d'établissements recevant du public. Par un décret du 16 mars 2020, le Premier ministre a interdit le déplacement de toute personne hors de son domicile à l'exception des déplacements pour des motifs limitativement énumérés. La loi du 23 mars 2020 d'urgence pour faire face à l'épidémie de covid-19 a déclaré l'état d'urgence sanitaire pour une durée de deux mois à compter du 24 mars 2020, terme ultérieurement reporté au 10 juillet 2020 par l'article 1er de la loi du 11 mai 2020 prorogeant l'état d'urgence sanitaire. Le Premier ministre, par un décret du 23 mars 2020 prescrivant les mesures générales nécessaires pour faire face à l'épidémie de covid-19 dans le cadre de l'état d'urgence sanitaire, a réitéré le principe de l'interdiction des déplacements, la prohibition de tout rassemblement, réunion ou activité mettant en présence de manière simultanée plus de 100 personnes en milieu clos ou ouvert et la fermeture de la plupart des établissements accueillant du public, notamment les établissements sportifs couverts et les établissements de plein air, ainsi que les établissements dans lesquels sont pratiquées des activités physiques ou sportives. Ce régime juridique est resté applicable, avec quelques ajustements, jusqu'au 11 mai 2020, soit postérieurement à l'édiction de la décision contestée. <br/>
<br/>
              3. A partir de l'intervention du décret du 11 mai 2020 prescrivant les mesures générales nécessaires pour faire face à l'épidémie de covid-19 dans le cadre de l'état d'urgence sanitaire, les déplacements ont été autorisés dans un rayon de cent kilomètres ; a été maintenue l'interdiction de tout rassemblement, réunion ou activité à un titre autre que professionnel sur la voie publique ou dans un lieu public, mettant en présence de manière simultanée plus de dix personnes ; les établissements sportifs couverts sont demeurés fermés ; les établissements sportifs de plein air ont pu organiser la pratique de certaines activités physiques et sportives, mais pas celle des sports collectifs. Le décret du 31 mai 2020 prescrivant les mesures générales nécessaires pour faire face à l'épidémie de covid-19 dans le cadre de l'état d'urgence sanitaire a maintenu ce régime juridique, sauf pour l'interdiction des déplacements, dans les départements classés en zone orange ; dans ceux classés en zone verte, la pratique des sports collectifs a été rendue possible pour les sportifs de haut niveau et les sportifs professionnels, à l'exception de toute pratique compétitive, cette dernière réserve n'ayant été levée que par un décret du 21 juin 2020. Par ailleurs, aucun événement réunissant plus de 5 000 personnes ne peut se dérouler sur le territoire de la République jusqu'au 31 août 2020.<br/>
<br/>
              4. Lors de sa réunion téléphonique du 30 avril 2020, le conseil d'administration de la Ligue de football professionnel a décidé :<br/>
              - de prononcer l'arrêt définitif des championnats de Ligue 1 et de Ligue 2 pour la saison 2019/2020 ;<br/>
              - pour la Ligue 1, de tirer les conséquences du fait que toutes les rencontres de la 28ème journée n'avaient pas pu avoir lieu en arrêtant un classement définitif sur la base d'un indice de performance défini comme le quotient issu du rapport entre le nombre de points marqués et le nombre de matchs disputés ;<br/>
              - pour la Ligue 2, d'arrêter un classement définitif sur la base de celui existant à l'issue de la 28ème journée ;<br/>
              - d'enregistrer en conséquences les classements de la Ligue 1 et de la Ligue 2 ;<br/>
              - d'attribuer le titre de champion de France de Ligue 1 au Paris-Saint-Germain et celui de champion de France de Ligue 2 au FC Lorient ;<br/>
              - de ne pas organiser, contrairement aux règles normalement applicables, de matchs de " play-offs " entre les clubs ayant terminé 3ème, 4ème et 5ème de Ligue 2 non plus que le match de barrage aller-retour devant normalement opposer le vainqueur de ces " play-offs " au 18ème de Ligue 1 et, par suite, de prononcer l'accession en Ligue 1 des clubs classés en première et deuxième position de Ligue 2 (FC Lorient et RC Lens) et de prononcer la relégation en Ligue 2 des clubs classés en dix-neuvième et vingtième position de Ligue 1 (Amiens SC et Toulouse FC).<br/>
<br/>
              5. La société Espérance Sportive Troyes Aube Champagne (ESTAC) et la société anonyme sportive professionnelle (SASP) Clermont Foot 63 demandent au juge des référés du Conseil d'Etat, sur le fondement de l'article L. 521-1 du code de justice administrative, de suspendre l'exécution de la décision ne pas organiser les matchs de " play-offs " entre les clubs ayant terminé 3ème, 4ème et 5ème de Ligue 2 non plus que le match de barrage devant opposer le vainqueur de ces play-offs au 18ème de Ligue 1.<br/>
              Sur le cadre juridique :<br/>
<br/>
              6. Aux termes de l'article L. 131-14 du code du sport : " Dans chaque discipline sportive et pour une durée déterminée, une seule fédération agréée reçoit délégation du ministre chargé des sports. " Aux termes de l'article L. 131-15 du même code : " Les fédérations délégataires : 1° Organisent les compétitions sportives à l'issue desquelles sont délivrés les titres internationaux, nationaux, régionaux ou départementaux (...). " Aux termes de l'article L. 131-16 du même code : " Les fédérations délégataires édictent : 1° Les règles techniques propres à leur discipline ainsi que les règles ayant pour objet de contrôler leur application et de sanctionner leur non-respect par les acteurs des compétitions sportives (...). " Aux termes de l'article R. 131-32 du même code : " Les règles techniques édictées par les fédérations sportives délégataires comprennent :1° Les règles du jeu applicables à la discipline sportive concernée ; / 2° Les règles d'établissement d'un classement national, régional, départemental ou autre, des sportifs, individuellement ou par équipe ; / 3° Les règles d'organisation et de déroulement des compétitions ou épreuves aboutissant à un tel classement ; / 4° Les règles d'accès et de participation des sportifs, individuellement ou par équipe, à ces compétitions et épreuves. " Par ailleurs, aux termes de l'article L. 132-1 du même code : " Les fédérations sportives délégataires peuvent créer une ligue professionnelle, pour la représentation, la gestion et la coordination des activités sportives à caractère professionnel des associations qui leur sont affiliées et des sociétés sportives (...). " Aux termes de l'article R. 132-1 du même code, une fédération sportive délégataire peut créer une ligue professionnelle dotée de la personnalité morale soit pour organiser les compétitions sportives qu'elle définit, soit pour fixer, pour les compétitions sportives qu'elle définit, leurs conditions d'organisation et celles de la participation des sportifs. En application de l'article R. 132-12 du même code, la réglementation et la gestion des compétitions mentionnées à l'article R. 132-1 relèvent de la compétence de la ligue professionnelle, sous réserve des dispositions des articles R. 132-10 et R. 132-11.<br/>
<br/>
              7. En confiant, à titre exclusif, aux fédérations sportives ayant reçu délégation la mission d'organiser des compétitions sur le territoire national, le législateur a chargé ces fédérations de l'exécution d'une mission de service public à caractère administratif. Les décisions procédant de l'usage par ces fédérations des prérogatives de puissance publique qui leur ont été conférées pour l'accomplissement de cette mission de service public présentent le caractère d'actes administratifs. Le pouvoir d'organiser les compétitions sportives à l'issue desquelles sont délivrés les titres internationaux, nationaux, régionaux ou départementaux, conféré aux fédérations délégataires par l'article L. 131-16, peut être exercé par des ligues professionnelles pour la participation aux compétitions qu'elles organisent. Par convention conclue entre la Fédération française de football et la Ligue de football professionnel en application de l'article R. 132-9 du code du sport, la gestion du football professionnel a été déléguée à la Ligue de football professionnel, notamment chargée d'organiser, de gérer et de réglementer le championnat de Ligue 1 et le championnat de Ligue 2. Il appartient en conséquence à la Ligue de football professionnel de réglementer ces compétitions.<br/>
<br/>
              Sur la demande en référé :<br/>
<br/>
              8. En premier lieu, aux termes de l'article 12 des statuts de la Ligue de football professionnel, l'assemblée générale de la Ligue est compétente pour procéder au changement de format des compétitions organisées par la Ligue dans la limite des dispositions de la convention liant la Ligue à la Fédération. Aux termes de l'article 24 de ces statuts, le conseil d'administration a compétence pour " établir le règlement administratif de la Ligue et le règlement des compétitions qu'elle organise ". La décision de ne pas procéder, à l'issue de la saison 2019-2020, à des matchs de " play-offs " et à un match de barrage, par dérogation au règlement des compétitions, ne remet pas en cause, par elle-même, le format des compétitions, cette notion devant être entendue, en l'absence de toute définition textuelle, comme relative au nombre de clubs admis à participer aux championnats de Ligue 1 et de Ligue 2. Par ailleurs, les dispositions du règlement des compétitions de la Ligue de football professionnel relatives aux compétences de la commission des compétitions et du bureau de révision des règlements ne les dotent pas d'un pouvoir normatif. Par suite, le moyen tiré de ce que le conseil d'administration n'était pas compétent pour prendre la décision contestée n'est pas, en l'état de l'instruction, de nature à créer un doute sérieux sur sa légalité.<br/>
<br/>
              9. En deuxième lieu, la réglementation des compétitions organisées par la Ligue de football professionnel ne comporte pas de dispositions prévoyant les règles à suivre lorsque des circonstances imprévues conduisent à interrompre ces compétitions de façon définitive avant leur terme. Le Premier ministre et la ministre des sports ayant annoncé, à la fin du mois d'avril 2020, que la saison 2019-2020, s'agissant des compétitions de sports collectifs professionnels, et en particulier du football, ne pourrait reprendre, en raison du contexte sanitaire lié à l'épidémie de covid-19, le conseil d'administration de la Ligue a estimé, compte tenu de ces annonces et des contraintes de calendrier, et au regard de la nécessité de préserver la santé de tous les acteurs des rencontres de football, ainsi que de l'intérêt s'attachant à ce que les clubs disposent de la visibilité nécessaire pour gérer l'intersaison et organiser la saison 2020-2021, qu'il convenait de prendre dès à présent la décision d'arrêter de façon définitive les championnats de Ligue 1 et de Ligue 2. Dans de telles circonstances, il appartenait au conseil d'administration, soit, s'il estimait que l'équité sportive ne devait pas conduire à procéder à des relégations et des accessions, de retenir le principe d'une " saison blanche ", soit, dans le cas contraire, de décider selon quelles modalités ces relégations et accessions auraient lieu. Le choix de ne pas faire de la saison 2019-2020 une " saison blanche " n'est, en tout état de cause, pas contesté par les requérantes. S'il est vrai qu'à la date du 30 avril 2020, il ne pouvait être totalement exclu que l'évolution du contexte sanitaire et un allègement des contraintes juridiques permettent l'organisation de " plays-offs " et d'un match de barrage, il appartenait au conseil d'administration de la Ligue de procéder, comme elle l'a fait, à la pesée des avantages et des inconvénients d'une décision immédiate, alors qu'une très grande incertitude affectait l'hypothèse d'une possible tenue de ces rencontres  en temps utile. Alors même que, selon les requérantes, il apparaîtrait désormais que l'organisation des matchs de " play-offs " et de barrage serait envisageable au début du mois d'août, voire au mois de juillet, compte tenu de l'évolution positive de la situation sanitaire, le moyen tiré de ce que la décision contestée serait entachée d'erreur manifeste d'appréciation n'est pas, en l'état de l'instruction, propre à créer un doute sérieux sur sa légalité.<br/>
<br/>
              10. En troisième lieu, compte tenu de ce qui a été indiqué au point 9, ne sont pas davantage propres à créer, en l'état de l'instruction, un doute sérieux sur la légalité de la décision contestée, qui se borne à priver les clubs concernés d'une chance d'accéder à la division supérieure, les moyens tirés de ce que cette décision porterait atteinte aux intérêts généraux du football et méconnaîtrait, de manière disproportionnée et non justifiée par les circonstances, le principe d'équité sportive et d'égalité de traitement entre les équipes participant à un même championnat.<br/>
<br/>
              11. Il résulte de tout ce qui précède que la requête de la société Espérance Sportive Troyes Aube Champagne et de la SASP Clermont Foot 63 doit être rejetée, y compris, par voie de conséquence, leurs conclusions présentées au titre de l'article L. 761-1 du code de justice administrative. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge des deux requérantes le versement à la Ligue de football professionnel d'une somme de 1 500 euros chacune.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de la société Espérance Sportive Troyes Aube Champagne et de la SASP Clermont Foot 63 est rejetée.<br/>
Article 2 : La société Espérance Sportive Troyes Aube Champagne et la SASP Clermont Foot 63 verseront chacune à la Ligue de football professionnelle une somme de 1 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente ordonnance sera notifiée à la société Espérance Sportive Troyes Aube Champagne (ESTAC), premier requérant dénommé, et à la Ligue de football professionnel.<br/>
Copie en sera adressée à la Fédération française de football.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
