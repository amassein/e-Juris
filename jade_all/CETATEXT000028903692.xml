<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028903692</ID>
<ANCIEN_ID>JG_L_2014_05_000000362281</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/90/36/CETATEXT000028903692.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème / 4ème SSR, 05/05/2014, 362281</TITRE>
<DATE_DEC>2014-05-05</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>362281</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème / 4ème SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROUSSEAU, TAPIE ; SCP ROGER, SEVAUX, MATHONNET</AVOCATS>
<RAPPORTEUR>M. Charles Touboul</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:362281.20140505</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 28 août et 27 novembre 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. et Mme A...B..., demeurant... ; M. et Mme B... demandent au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler l'arrêt n°11LY02493 du 28 juin 2012 par lequel la cour administrative d'appel de Lyon a, d'une part, annulé le jugement n° 0903261 du 1er août 2011 du tribunal administratif de Lyon en tant qu'il a mis à la charge de l'Office national d'indemnisation des accidents médicaux, des affectations iatrogènes et des infections nosocomiales (ONIAM) la réparation des préjudices propres des parents et du frère de Ceylanie B...à la suite de l'accident médical dont il a été victime au centre hospitalier universitaire de Saint-Etienne et, d'autre part, ramené à la somme de 841 733,47 euros sous déduction des provisions de 500 000 et de 200 000 euros, l'indemnité fixée par l'article 2 de ce jugement ; <br/>
<br/>
              2°) de mettre à la charge de l'ONIAM la somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de la santé publique ;<br/>
<br/>
              Vu le code de la sécurité sociale ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Charles Touboul, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Rousseau, Tapie, avocat de M. et Mme B...et à la SCP Roger, Sevaux, Mathonnet, avocat de l'Office national d'indemnisation des accidents médicaux, des affectations iatrogènes et des infections nosocomiales ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que l'enfant Ceylanie B...a été victime, alors qu'il était âgé de quatorze mois, d'un arrêt cardiorespiratoire lors d'une tentative d'extraction d'un corps étranger par bronchoscopie réalisée, le 16 mars 2005, au centre hospitalier universitaire de Saint-Etienne ; que, par un jugement du 1er août 2011, le tribunal administratif de Lyon a mis à la charge de l'ONIAM la réparation, au titre de la solidarité nationale, des conséquences dommageables de l'accident médical survenu lors de cette intervention ; que, saisie d'un appel de l'ONIAM et d'un appel incident de M. et Mme B..., la cour administrative d'appel de Lyon a, par un arrêt du 28 juin 2012, annulé le jugement en tant qu'il  a mis à la charge de l'ONIAM la réparation des préjudices subis par les parents et le frère de Ceylanie B...du fait de l'accident et ramené l'indemnité fixée par l'article 2 de ce jugement à la somme de 841 733,47 euros, sous déduction des provisions de 500 000 et 200 000 euros ; que M. et Mme B...se pourvoient en cassation contre cet arrêt ;<br/>
<br/>
              Sur les dépenses de santé :<br/>
<br/>
              2. Considérant qu'après avoir visé la demande d'allocation d'une somme de 6 478,46 euros au titre de dépenses de santé actuelles, montant qui incluait les frais exposés par M. et Mme B...pour l'acquisition de gants destinés à l'hygiène corporelle de leur enfant, la cour a fait droit à cette demande à hauteur d'une somme de 2 757 euros au titre de la période comprise entre janvier 2007 et janvier 2012, après déduction de l'aide versée par le département de la Loire au titre des protections pour incontinence ; que, par suite, le moyen tiré de ce que la cour aurait insuffisamment motivé son arrêt faute de répondre à la demande des époux B...au titre des dépenses de gants doit être écarté ; <br/>
<br/>
              Sur les frais liés au handicap :<br/>
<br/>
              En ce qui concerne les frais d'aménagement du logement :<br/>
<br/>
              3. Considérant, en premier lieu, que la cour a retenu que le handicap de l'enfant nécessitait l'aménagement d'un accès spécifique au logement, dont le coût, justifié par un devis en date du 15 mars 2010, s'élevait à 23 465,52 euros ; que les écritures de M. et Mme B...devant la cour ne tendaient pas explicitement à la prise en charge de frais d'équipement de la salle de bain et de surélévation de la charpente de la maison ; que, par suite, le moyen tiré de ce que la cour aurait omis de répondre à la requête sur ce point doit être écarté ;<br/>
<br/>
              4. Considérant, en second lieu, que la cour a pu, sans erreur de droit ni erreur de qualification juridique, écarter la demande des époux B...tendant à la prise en charge des frais d'acquisition d'une nouvelle maison, de travaux divers de menuiserie, de maçonnerie et d'installation électrique, à hauteur de 89 873 euros, au motif qu'il n'était pas justifié devant elle qu'ils aient été en lien direct avec le handicap de l'enfant ;<br/>
<br/>
              En ce qui concerne les frais d'adaptation du véhicule :<br/>
<br/>
              5. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que si, dans leur mémoire d'appel, les époux B...indiquaient qu'ils n'auraient pas fait appel du jugement du tribunal administratif en l'absence d'appel de l'ONIAM et demandaient que le jugement du tribunal administratif soit confirmé en ce qu'il leur avait alloué une somme de 25 000 euros au titre des frais d'adaptation du véhicule et une rente annuelle de 4 167 euros, ce mémoire reprenait expressément leurs conclusions de première instance qui tendaient, s'agissant de ce chef de préjudice, à l'allocation d'une somme de 28 152 euros pour l'achat d'un véhicule adapté et de 158 732 euros au titre des frais futurs de ce même poste ; qu'ainsi, en relevant que l'indemnité allouée par le tribunal au titre des frais d'adaptation du véhicule, d'un montant de 25 000 euros, et la rente annuelle de 4 167 euros au titre des frais futurs du renouvellement n'étaient pas contestés par M. et MmeB..., la cour n'a pas donné aux écritures de ces derniers leur exacte portée ; que son arrêt doit par suite être annulé en tant qu'il statue sur les frais d'adaptation du véhicule ;<br/>
<br/>
              En ce qui concerne les frais d'assistance d'une tierce personne :<br/>
<br/>
              6. Considérant, en premier lieu, que pour fixer l'indemnité due par l'ONIAM au titre des frais de maintien à domicile de l'enfant avant le 1er janvier 2012, la cour a évalué le coût d'une assistance au domicile familial, par référence au salaire minimum moyen augmenté des charges sociales, à la somme de 200 euros par tranche de vingt-quatre heures pour la période comprise entre le 20 janvier 2006 et le 18 février 2008 et à la somme de 220 euros par vingt-quatre heures au titre de la période comprise entre le 19 février 2008 et le 1er janvier 2012 ; qu'elle a ensuite énoncé que les frais de maintien à domicile de Ceylanie devaient être évalués, selon ce mode de calcul, à 253 775 euros ;  que, toutefois, l'application des coûts journaliers retenus par la cour pour les deux périodes en cause ne permet pas de parvenir à ce montant ; que, par suite, la cour a entaché son arrêt d'une insuffisance de motivation qui justifie qu'il soit annulé en tant qu'il statue sur les frais d'assistance par une tierce personne au titre de la période antérieure au 1er janvier 2012 ; <br/>
<br/>
              7. Considérant, en second lieu, que si le juge n'est pas en mesure de déterminer, lorsqu'il se prononce, si l'enfant handicapé sera placé dans une institution spécialisée ou hébergé au domicile de sa famille, il lui appartient de lui accorder une rente trimestrielle couvrant les frais de son maintien au domicile familial, en précisant le mode de calcul de cette rente, dont le montant doit dépendre du temps passé au domicile familial au cours du trimestre ; que l'arrêt attaqué prévoit qu'à compter du 1er juillet 2012 les frais de maintien à domicile de l'enfant seront indemnisés par le versement d'une rente trimestrielle calculée sur la base d'un taux quotidien de 220 euros " au prorata des heures nocturnes qu'il aura passées au domicile familial " au cours du trimestre ; que s'il était loisible à la cour, pour tenir compte du fait que jusqu'à la date de sa décision l'enfant avait été pris en charge dans un établissement pendant la journée, sauf pendant les week-ends et les périodes de vacances scolaires, de prévoir que le montant de la rente varierait en fonction du nombre d'heures pendant lesquelles il aurait été hébergé au domicile familial, elle a commis une erreur de droit en décidant que seules les " heures nocturnes " seraient prises en compte pour la détermination de ce montant ;<br/>
<br/>
              Sur les préjudices personnels de la victime :<br/>
<br/>
              8. Considérant que la cour a alloué à Ceylanie B...la somme globale de 560 000 euros au titre des troubles personnels de toute nature qu'il subissait dans ses conditions d'existence en raison de son handicap ; que le déficit fonctionnel temporaire relève des troubles dans les conditions d'existence ainsi indemnisés ; que, par suite, le moyen tiré de ce que la cour aurait omis de répondre à la demande de prise en charge de ce chef de préjudice doit être écarté ;<br/>
<br/>
              9. Considérant qu'il résulte de tout ce qui précède que l'arrêt du 28 juin 2012 de la cour administrative d'appel de Lyon doit être annulé en tant qu'il statue sur les frais d'adaptation du véhicule et les frais liés à l'assistance d'une tierce personne ;   <br/>
<br/>
              Sur les conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative : <br/>
<br/>
              10. Considérant, qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'ONIAM une somme de 3 000 euros à verser à M. et Mme B...au titre des dispositions de l'article L. 761-1 du code de justice administrative ; que ces mêmes dispositions font obstacle à ce qu'une somme soit mise à ce titre à la charge de M. et Mme B...qui ne sont pas, dans la présente instance, la partie perdante ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Lyon du 28 juin 2012 est annulé en tant qu'il statue sur les frais d'adaptation du véhicule et les frais liés à l'assistance d'une tierce personne.<br/>
<br/>
Article 2 : Le surplus des conclusions du pourvoi de M. et Mme B...est rejeté.<br/>
<br/>
Article 3 : L'affaire est renvoyée, dans la limite de la cassation prononcée, à la cour administrative d'appel de Lyon. <br/>
<br/>
Article 4 : L'ONIAM versera à M. et Mme B...une somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 5 : Les conclusions présentées par l'ONIAM au titre de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
<br/>
Article 6 : La présente décision sera notifiée à M. et Mme A...B...et à l'Office national d'indemnisation des accidents médicaux, des affectations iatrogènes et des infections nosocomiale.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">60-04-04 RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. RÉPARATION. MODALITÉS DE LA RÉPARATION. - FRAIS LIÉS AU HANDICAP D'UN ENFANT - RENTE TRIMESTRIELLE COUVRANT LES FRAIS DU MAINTIEN DE LA VICTIME À DOMICILE - CALCUL - MONTANT FONCTION DU TEMPS PASSÉ AU DOMICILE FAMILIAL AU COURS DU TRIMESTRE - CAS D'UN ENFANT FAISANT L'OBJET D'UNE PRISE EN CHARGE DE JOUR EN PÉRIODE SCOLAIRE - PRISE EN COMPTE DES SEULES HEURES NOCTURNES POUR LE CALCUL DE LA RENTE - ABSENCE [RJ1].
</SCT>
<ANA ID="9A"> 60-04-04 Si le juge n'est pas en mesure de déterminer, lorsqu'il se prononce, si l'enfant handicapé sera placé dans une institution spécialisée ou hébergé au domicile de sa famille, il lui appartient de lui accorder une rente trimestrielle couvrant les frais de son maintien au domicile familial, en précisant le mode de calcul de cette rente dont le montant doit dépendre du temps passé au domicile familial au cours du trimestre. Dans le cas d'un enfant pris en charge dans un établissement pendant la journée, sauf pendant les week-ends et les périodes de vacances scolaires, il est loisible au juge d'en tenir compte en faisant varier le montant de la rente en fonction du nombre d'heures pendant lesquelles il est hébergé au domicile familial, mais pas en décidant que seules les heures nocturnes seraient prises en compte pour la détermination de ce montant.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Comp. CE, 26 juin 2008, Caisse primaire d'assurance maladie de Dunkerque, n° 235887, p. 232.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
