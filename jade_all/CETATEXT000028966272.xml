<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028966272</ID>
<ANCIEN_ID>JG_L_2014_05_000000368888</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/96/62/CETATEXT000028966272.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  2ème sous-section jugeant seule, 21/05/2014, 368888, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-05-21</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>368888</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 2ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Contentieux des pensions</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>OCCHIPINTI</AVOCATS>
<RAPPORTEUR>Mme Airelle Niepce</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Béatrice Bourgeois-Machureau</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2014:368888.20140521</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi, enregistré le 28 mai 2013 au secrétariat du contentieux du Conseil d'Etat, présenté par le ministre de la défense, qui demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° R 10/00054 du 25 mars 2013 par lequel la cour régionale des pensions de Nîmes a rejeté son recours contre le jugement du 7 juillet 2010 par lequel le tribunal départemental des pensions de Vaucluse a fait droit à la demande de revalorisation de la pension militaire d'invalidité au taux de 55 % de M. B...sur la base de l'indice de grade de la marine nationale équivalent à celui d'adjudant chef, à compter du 3 avril 2006, avec perception des arrérages revalorisés échus et non versés antérieurement, à compter du 1er janvier 2003 ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code des pensions militaires d'invalidité et des victimes de la guerre ; <br/>
<br/>
              Vu la loi n° 91-647 du 10 juillet 1991 ;<br/>
<br/>
              Vu le décret n° 56-913 du 5 septembre 1956 ;<br/>
<br/>
              Vu le décret n° 59-327 du 20 février 1959 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Airelle Niepce, maître des requêtes en service extraordinaire, <br/>
<br/>
              - les conclusions de Mme Béatrice Bourgeois-Machureau, Rapporteur public,<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Occhipinti, avocat de M. B...; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant, d'une part, qu'aux termes du premier alinéa de l'article L. 24 du code des pensions militaires d'invalidité et des victimes de la guerre, alors en vigueur : " Les pensions militaires prévues par le présent code sont liquidées et concédées (...) par le ministre des anciens combattants et des victimes de guerre ou par les fonctionnaires qu'il délègue à cet effet. Les décisions de rejet des demandes de pension sont prises dans la même forme " ; qu'en vertu de l'article L. 25 du même code, la notification des décisions prises en vertu de l'article L. 24 du même code doit mentionner que le délai de recours contentieux court à partir de cette notification ; qu'en vertu de l'article 5 du décret du 20 février 1959 relatif aux juridictions des pensions, l'intéressé dispose d'un délai de six mois pour contester, devant le tribunal départemental des pensions, la décision prise sur ce fondement ; <br/>
<br/>
              Considérant, d'autre part, qu'aux termes de l'article L. 78 du même code : " Les pensions définitives ou temporaires attribuées au titre du présent code peuvent être révisées dans les cas suivants : / 1° Lorsqu'une erreur matérielle de liquidation a été commise. / 2° Lorsque les énonciations des actes ou des pièces sur le vu desquels l'arrêté de concession a été rendu sont reconnues inexactes soit en ce qui concerne le grade, le décès ou le genre du mort, soit en ce qui concerne l'état des services, soit en ce qui concerne l'état civil ou la situation de famille, soit en ce qui concerne le droit au bénéfice d'un statut légal générateur de droits. / Dans tous les cas, la révision a lieu sans condition de délai (...) " ; <br/>
<br/>
              Considérant que le décalage défavorable entre l'indice de la pension servie à un ancien sous-officier de l'armée de terre, de l'armée de l'air ou de la gendarmerie et l'indice afférent au grade équivalent au sien des personnels de la marine nationale, lequel ne résulte ni d'une erreur matérielle dans la liquidation de sa pension, ni d'une inexactitude entachant les informations relatives à sa personne, ne figure pas au nombre des cas permettant la révision, sans condition de délai, d'une pension militaire d'invalidité ; qu'ainsi, la demande présentée par le titulaire d'une pension militaire d'invalidité, concédée à titre temporaire ou définitif sur la base du grade que l'intéressé détenait dans l'armée de terre, l'armée de l'air ou la gendarmerie, tendant à la revalorisation de cette pension en fonction de l'indice afférent au grade équivalent applicable aux personnels de la marine nationale, doit être formée dans le délai de six mois fixé par l'article 5 du décret du 20 février 1959 ; que passé ce délai de six mois ouvert au pensionné pour contester l'arrêté lui concédant sa pension, l'intéressé ne peut demander sa révision que pour l'un des motifs limitativement énumérés aux 1° et 2° de cet article L. 78 ;<br/>
<br/>
              Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que M.B..., ancien adjudant-chef de l'armée de terre, est titulaire d'une pension militaire d'invalidité, concédée au taux de 55 %, qui lui a été accordée par arrêté du 23 août 1973 ; que, par lettre du 3 avril 2006, il a demandé au ministre de la défense la revalorisation de sa pension, en fonction de l'indice, plus favorable, afférent au grade équivalent au sien dans la marine nationale ; que, le 5 mai 2006, le ministre lui a indiqué qu'il recherchait les moyens de donner suite à sa demande ; qu'en l'absence d'autre réponse, M. B...a saisi le 17 janvier 2008 le tribunal départemental des pensions de Vaucluse d'un recours contre le rejet qui a été implicitement opposé par le ministre à sa demande de revalorisation ; que, par un jugement du 7 juillet 2010, le tribunal a fait droit à la demande de M. B...en lui accordant la revalorisation de la pension dont il est titulaire à compter de la date de sa demande, avec les arrérages revalorisés échus et non versés pour les trois années antérieures ; que, par un arrêt du 25 mars 2013, la cour régionale des pensions de Nîmes a rejeté le recours formé par le ministre de la défense contre le jugement de première instance ; que le ministre se pourvoit en cassation contre cet arrêt du 25 mars 2013 ;<br/>
<br/>
              Considérant que, pour rejeter le recours du ministre de la défense dirigé contre le jugement du tribunal départemental des pensions, la cour régionale des pensions de Nîmes s'est fondée sur ce que l'arrêté du 23 août 1973 portant concession d'une pension militaire d'invalidité à M. B...ne lui aurait pas été régulièrement notifié ; que, toutefois, il ressort des pièces du dossier soumis aux juges du fond que l'arrêté du 23 août 1973 a été régulièrement notifié à son destinataire par la remise, le 17 mai 1974, du brevet d'inscription de sa pension d'invalidité, comportant les mentions alors requises pour déclencher le cours du délai de recours contentieux, ainsi que le ministre l'avait fait valoir devant la cour régionale des pensions ; que, par suite, en jugeant que l'arrêté n'aurait pas été régulièrement notifié à l'intéressé, la cour régionale des pensions a dénaturé les pièces du dossier qui lui était soumis ; que, dès lors, son arrêt du 25 mars 2013 doit être annulé ; <br/>
<br/>
              Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              Considérant qu'il résulte de l'instruction que, ainsi qu'il vient d'être dit, l'arrêté du 23 août 1973 portant concession à M. B...d'une pension militaire d'invalidité lui a été notifié par la remise, le 17 mai 1974, du brevet d'inscription de sa pension d'invalidité comportant les mentions alors requises pour déclencher le cours du délai de recours contentieux, ainsi qu'en atteste le procès-verbal de remise du brevet établi par le percepteur d'Evry le jour de cette remise ; que la lettre que M. B...a adressée à l'administration le 3 avril 2006 en vue d'obtenir la revalorisation de sa pension ne pouvait être regardée que comme un recours gracieux contre l'arrêté du 23 août 1973 lui concédant sa pension ; que, ce recours ayant été formé après l'expiration du délai de six mois fixé par l'article 5 du décret du 20 février 1959, la demande présentée par M. B...le 17 janvier 2008 au tribunal départemental des pensions de Vaucluse, en vue de contester le refus qui lui a été opposé, était tardive ; <br/>
<br/>
              Considérant qu'il résulte de ce qui précède que le ministre de la défense est fondé à soutenir que c'est à tort que le tribunal départemental des pensions a fait droit à la demande de M. B...; <br/>
<br/>
              Considérant que les dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 font obstacle à ce que l'Etat, qui n'est pas la partie perdante dans la présente instance, verse une somme au titre de ces dispositions à Me Occhipinti, avocat de M. B...;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : L'arrêt de la cour régionale des pensions de Nîmes du 25 mars 2013 et le jugement du tribunal départemental des pensions de Vaucluse du 7 juillet 2010 sont annulés. <br/>
<br/>
Article 2 : La demande présentée par M. B...devant le tribunal départemental des pensions de Vaucluse ainsi que les conclusions présentées au titre des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 sont rejetées.<br/>
<br/>
Article 3 : La présente décision sera notifiée au ministre de la défense et à M. A...B....<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
