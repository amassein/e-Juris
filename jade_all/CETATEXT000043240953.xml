<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043240953</ID>
<ANCIEN_ID>JG_L_2021_03_000000434643</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/24/09/CETATEXT000043240953.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème chambre, 08/03/2021, 434643, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-03-08</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>434643</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BOUTET-HOURDEAUX</AVOCATS>
<RAPPORTEUR>M. Lionel Ferreira</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Céline  Guibé</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:434643.20210308</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société civile immobilière (SCI) Reims Champigny a demandé au tribunal administratif de Châlons-en-Champagne de prononcer la réduction de la taxe d'aménagement et de la redevance d'archéologie auxquelles elle a été assujettie à raison de l'opération immobilière qu'elle a réalisée à Champigny. Par un jugement n° 1801713 du 23 mai 2019, le tribunal administratif de Châlons-en-Champagne a rejeté sa demande. <br/>
<br/>
              Par une ordonnance n° 19NC02327 du 16 septembre 2019, enregistrée le 17 septembre 2019 au secrétariat du contentieux du Conseil d'Etat, le président de la cour administrative d'appel de Nancy a transmis au Conseil d'Etat, en application de l'article R. 351-2 du code de justice administrative, le pourvoi, enregistré au greffe de cette cour le 22 juillet 2019, présenté par la SCI Reims Champigny contre ce jugement.<br/>
<br/>
              Par ce pourvoi et un nouveau mémoire enregistré le 29 novembre 2019 au secrétariat du contentieux du Conseil d'Etat, la SCI Reims Champigny demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler ce jugement ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 1 500 euros en application de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Lionel Ferreira, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme B... A..., rapporteure publique ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Boutet-Hourdeaux, avocat de la société Reims Champigny ;<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              Sur la redevance d'archéologie préventive :<br/>
<br/>
              1. Le produit de la redevance d'archéologie préventive était, en vertu de l'article L. 524-11 du code du patrimoine dans sa rédaction applicable à l'imposition en litige, reversé à l'Institut national de recherches archéologiques préventives, établissement public national à caractère administratif ou, après prélèvement d'un pourcentage au profit du Fonds national pour l'archéologie préventive, à la collectivité territoriale ou au groupement de collectivités territoriales concernés dans le cas où ils ont confié à leur propre service archéologique l'ensemble des opérations d'aménagement ou de travaux réalisés sur leur territoire. Compte tenu de ces règles d'affectation, le litige concernant cette redevance ne saurait être regardé comme étant relatif à un impôt local au sens de l'article R. 222 13 du code de justice administrative, auquel renvoie l'article R. 811-1 définissant les matières dans lesquelles les tribunaux administratifs statuent en dernier ressort. Dès lors, les conclusions du pourvoi de la SCI Reims Champigny dirigées contre le jugement attaqué en tant qu'il se prononce sur la redevance d'archéologie préventive doivent être regardées comme un appel relevant de la compétence de la cour administrative d'appel de Nancy.<br/>
<br/>
              Sur la taxe d'aménagement :<br/>
<br/>
              2. Aux termes de l'article L. 822-1 du code de justice administrative : " Le pourvoi en cassation devant le Conseil d'Etat fait l'objet d'une procédure préalable d'admission. L'admission est refusée par décision juridictionnelle si le pourvoi est irrecevable ou n'est fondé sur aucun moyen sérieux. "<br/>
<br/>
              3. Pour demander l'annulation du jugement qu'elle attaque, la SCI Reims Champigny soutient que le tribunal administratif de Châlons-en-Champagne l'a entaché:<br/>
              - d'insuffisance de motivation en jugeant que la délivrance du certificat d'urbanisme était sans incidence sur le taux de la taxe d'aménagement ;<br/>
              - d'insuffisance de motivation en estimant que l'importance des modifications opérées par le permis modificatif était insuffisante pour qu'il se substitue au projet initial ;<br/>
              - d'une méconnaissance de l'article R. 611-1 du code de justice administrative faute de lui avoir communiqué le mémoire en défense produit le 2 mai 2019 par le Préfet de la Marne ;<br/>
              - à titre principal, d'une erreur de droit en jugeant que la délivrance du certificat d'urbanisme était sans incidence sur le taux de la taxe d'aménagement ;<br/>
              - à titre subsidiaire, d'une erreur de droit en jugeant que le fait générateur de la taxe d'aménagement était le permis de construire initial.<br/>
<br/>
              4. Aucun de ces moyens n'est de nature à permettre l'admission du surplus des conclusions du pourvoi.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : Le jugement des conclusions présentées par la SCI Reims Champigny tendant à l'annulation du jugement du tribunal administratif de Châlons-en-Champagne du 23 mai 2019 en tant qu'il se prononce sur la redevance d'archéologie préventive est attribué à la cour administrative d'appel de Nancy.<br/>
Article 2 : Le surplus des conclusions du pourvoi de la SCI Reims Champigny n'est pas admis.<br/>
Article 3 : La présente décision sera notifiée à la SCI Reims Champigny. <br/>
Copie en sera adressée au ministre de la cohésion des territoires et des relations avec les collectivités. <br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
