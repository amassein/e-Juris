<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038366685</ID>
<ANCIEN_ID>JG_L_2019_04_000000428670</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/36/66/CETATEXT000038366685.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 08/04/2019, 428670, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-04-08</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>428670</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2019:428670.20190408</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B... A...a demandé au juge des référés du tribunal administratif d'Amiens, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, en premier lieu, d'annuler la décision par laquelle le directeur général de l'Office français de l'immigration et de l'intégration (OFII) a suspendu les conditions matérielles d'accueil des demandeurs d'asile desquelles il bénéficiait, en deuxième lieu, d'enjoindre au directeur général de l'OFII de rétablir les conditions matérielles d'accueil et de verser sans délai l'allocation de demandeur d'asile à titre rétroactif à compter du 1er septembre 2018, pour un montant de 2 570 euros, sous astreinte, en troisième lieu, d'enjoindre au directeur de l'OFII de reprendre sans délai le versement de l'allocation de demandeur d'asile avec montant journalier additionnel, sous astreinte et, en quatrième lieu, d'assortir l'ordonnance à intervenir de l'exécution provisoire à compter de son prononcé en application de l'article R. 522-3 du code de justice administrative. Par une ordonnance n° 1900551 du 25 février 2019, le juge des référés du tribunal administratif d'Amiens a rejeté sa requête.<br/>
<br/>
              Par une requête, enregistrée le 7 mars 2019 au secrétariat de la section du contentieux du Conseil d'Etat, M. A... demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) d'annuler la décision du directeur général de l'OFII suspendant les conditions matérielles d'accueil des demandeurs d'asile ;<br/>
<br/>
              3°) d'enjoindre au directeur général de l'OFII de rétablir les conditions matérielles d'accueil et de ne verser l'allocation de demandeur d'asile à titre rétroactif à compter de la date de sa suspension, le 1er septembre 2018, soit le montant de 3 010,02 euros sans délai et sous astreinte de 200 euros par jour de retard à compter de la notification de l'ordonnance à intervenir ;<br/>
<br/>
              4°) d'enjoindre au directeur général de l'OFII de reprendre sans délai le versement de l'allocation de demandeur d'asile avec montant journalier additionnel sous astreinte de 200 euros par jour de retard à compter de la notification de l'ordonnance à intervenir ;<br/>
<br/>
              5°) d'assortir l'ordonnance à intervenir de l'exécution provisoire à compter de son prononcé en application de l'article R. 522-3 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Il soutient que :<br/>
              - l'OFII a cessé de lui verser l'allocation pour demandeur d'asile à compter de septembre 2018, sans que la décision ne soit matérialisée ;<br/>
              - si son épouse et son fils sont pris en charge financièrement par sa famille au Maroc, il n'est pas moins tenu de subvenir à leurs besoins ;<br/>
              - l'allocation pour demandeur d'asile n'est pas réservée aux requérants ayant une famille à charge ;<br/>
              - il est traité pour des troubles anxieux et épisodes dépressifs majeurs résultant de l'infliction de violences psychologiques par l'OFPRA puis l'OFII ;<br/>
              - il ne dispose d'aucun revenu permettant de se loger, de se nourrir et de se vêtir ;<br/>
              - faute de bénéficier des conditions matérielles d'accueil, il n'est pas en mesure d'instruire son recours devant la Cour nationale du droit d'asile ;<br/>
              - il s'est endetté pour assurer sa subsistance ;<br/>
              - il vit de la charité, ce qui porte atteinte à sa dignité.<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
	Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Aux termes du premier alinéa de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. Le juge des référés se prononce dans un délai de quarante-huit heures. ". En vertu de l'article L. 522-3 du même code, le juge des référés peut, par une ordonnance motivée, rejeter une requête sans instruction ni audience lorsque la condition d'urgence n'est pas remplie ou lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable ou qu'elle est mal fondée. Il appartient au juge des référés saisi en appel de porter son appréciation sur ce point au regard de l'ensemble des pièces du dossier, et notamment des éléments recueillis par le juge de première instance dans le cadre de la procédure écrite et orale qu'il a diligentée.<br/>
<br/>
              2. M. A... est privé depuis le mois de septembre 2018 du bénéfice de l'allocation pour demandeurs d'asile. Par une requête, enregistrée le 20 février 2019, M. A... a saisi le juge des référés du tribunal administratif d'Amiens, sur le fondement de l'article L. 521-2 du code de justice administrative, d'une demande de rétablissement des conditions matérielles d'accueil. Par l'ordonnance attaquée en date du 25 février 2009, le juge des référés du tribunal administratif d'Amiens a rejeté ses demandes.<br/>
<br/>
              3. Le juge des référés du tribunal administratif d'Amiens a rejeté la demande pour défaut d'urgence, d'une part, à raison de l'engagement de l'OFII de servir de nouveau l'allocation pour demandeurs d'asile à M. A... à compter du mois d'avril 2019, et de verser les sommes qui auraient dû l'être depuis septembre 2018, d'autre part, à raison de l'absence de circonstances personnelles ou familiales particulières, hormis des troubles d'anxiété pour lesquels M. A... est traité.<br/>
<br/>
              4. Le requérant se borne en appel à contester la solution donnée en première instance. Il n'apporte aucun élément susceptible d'infirmer la solution retenue par le juge des référés du tribunal administratif. Il y a donc lieu de rejeter sa requête selon la procédure prévue à l'article L. 522-3 du code de justice administrative.<br/>
<br/>
              5. Il résulte de ce qui précède que l'appel de M. A... ne peut être accueilli.<br/>
<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de M. A... est rejetée.<br/>
<br/>
Article 2 : La présente ordonnance sera notifiée à M. B...A....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
