<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030200585</ID>
<ANCIEN_ID>JG_L_2015_01_000000382136</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/20/05/CETATEXT000030200585.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème SSJS, 16/01/2015, 382136, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-01-16</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>382136</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème SSJS</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Alain Méar</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emmanuelle Cortot-Boucher</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2015:382136.20150116</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
Procédure contentieuse antérieure<br/>
<br/>
              Mme P...W...a demandé au tribunal administratif de Lyon d'annuler les opérations électorales qui se sont déroulées le 23 mars 2014 en vue de la désignation des conseillers municipaux et des conseillers communautaires de la commune de Marcy-L'Etoile (Rhône) ; <br/>
<br/>
              Par un jugement n° 1402132 du 3 juin 2014, le tribunal administratif de Lyon a rejeté la protestation de MmeW....<br/>
<br/>
Procédure devant le Conseil d'Etat<br/>
<br/>
              Par une requête enregistrée le 2 juillet 2014 au secrétariat du contentieux du Conseil d'Etat, Mme W...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler le jugement n° 1402132 du 3 juin 2014 du tribunal administratif de Lyon ;<br/>
<br/>
              2°) d'annuler les opérations électorales.<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code général des collectivités territoriales ;<br/>
              - le code électoral ;<br/>
              - le code de justice administrative.<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Alain Méar, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Emmanuelle Cortot-Boucher, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              Sur l'intervention de M.O..., MmeS..., M.B..., Mme. K...et M.M... :<br/>
<br/>
              1. Cette intervention n'est pas motivée. Elle n'est, dès lors, pas recevable. <br/>
<br/>
              Sur les conclusions de la requête :<br/>
<br/>
              S'agissant des avantages prohibés :<br/>
<br/>
              2. Aux termes du deuxième alinéa de l'article L. 52-8 du code électoral : " Les personnes morales, à l'exception des partis ou groupements politiques, ne peuvent participer au financement de la campagne électorale d'un candidat, ni en lui consentant des dons sous quelque forme que ce soit, ni en lui fournissant des biens, services ou autres avantages directs ou indirects à des prix inférieurs à ceux qui sont habituellement pratiqués ".<br/>
<br/>
              3. En premier lieu, la circonstance que la commune de Marcy-L'Etoile aurait mis gratuitement à la disposition de la liste conduite par M. N...des salles de réunion ne méconnaît pas les dispositions de l'article L. 52-8 du code électoral, dès lors qu'il résulte de l'instruction que la liste conduite par Mme W...a pu bénéficier de facilités analogues.<br/>
<br/>
              4. En deuxième lieu, si Mme W...fait valoir que la liste conduite par M. N...a bénéficié du soutien de l'association " Dégaine Escalade " à travers l'envoi d'un courriel à ses adhérents résidant à Marcy-L'Etoile, la prise de position de cette association, dont il n'est pas allégué qu'elle n'aurait pas été indépendante des listes candidates et qui était libre d'inciter à voter en faveur de l'une d'elles, ne peut être regardée comme constituant une aide illégale au sens des dispositions précitées.<br/>
<br/>
              5. En troisième lieu, la seule circonstance qu'un des candidats de la liste conduite par M. N...apparaisse sur la photographie illustrant le mois d'octobre 2014 du calendrier édité par l'association " Amicale des sapeurs-pompiers de Marcy-Charbonnières " ne saurait être regardée comme constituant une aide illégale au sens des mêmes dispositions.<br/>
<br/>
              S'agissant du bulletin d'information et de la lettre signée par l'adjoint au maire :<br/>
<br/>
              6. Aux termes du deuxième alinéa de l'article L. 52-1 du code électoral : " A compter du premier jour du sixième mois précédant le mois au cours duquel il doit être procédé à des élections générales, aucune campagne de promotion publicitaire des réalisations ou de la gestion d'une collectivité ne peut être organisée sur le territoire des collectivités intéressées par le scrutin. (...) ".<br/>
<br/>
              7. En premier lieu, il résulte de l'instruction que le numéro de décembre 2013-janvier 2014 du bulletin d'information générale diffusé par la commune de Marcy-L'Etoile comprenait un éditorial de M.N..., maire de la commune, dont la longueur était supérieure à la longueur habituelle des éditoriaux qu'il signait dans ce bulletin. Toutefois, cet éditorial, eu égard au ton employé et à son contenu, qui se bornait à la présentation d'informations de caractère général et ne contenait aucun élément de polémique électorale, ne saurait être regardé comme constituant une campagne de promotion publicitaire au sens de l'article L. 52-1 du code électoral. Il ne saurait davantage être regardé comme un document de propagande en faveur de la liste conduite par M.N..., et ne peut donc être assimilé à un don d'une personne morale prohibé par l'article L. 52-8 de ce code.<br/>
<br/>
              8. En deuxième lieu, il résulte de l'instruction que le même numéro du bulletin d'information générale diffusé par la commune contenait une page d'informations de la commission d'urbanisme, qui présentait notamment l'état d'avancement de la révision du plan local d'urbanisme intercommunal et de la politique de protection des espaces naturels et agricoles périurbains. Si ces sujets ont fait l'objet de débats au cours de la campagne électorale, la publication litigieuse se bornait toutefois à présenter des informations objectives, à une date normale au vu de sa périodicité. Eu égard à son contenu et au ton employé, elle ne saurait dès lors être regardée comme une campagne publicitaire au sens de l'article L. 52-1 du code électoral ou comme un document de propagande assimilable à un don prohibé par l'article L. 52-8 de ce code.<br/>
<br/>
              9. En troisième lieu, il résulte de l'instruction que, le 24 janvier 2014, des habitants de la résidence " Les Hauts de Marcy " ont saisi le maire de Marcy-L'Etoile de la question de la dangerosité de l'avenue Marcel Mérieux aux abords de la résidence, en raison du passage de voitures à une vitesse excessive. Le maire leur a répondu par un courrier du 7 février 2014 leur indiquant les travaux de sécurisation projetés et leur proposant une rencontre. Le maire ayant été saisi à nouveau par le gérant de la résidence le 18 février 2014 de cette question et d'une demande de pose d'un miroir à la sortie du garage de la résidence, l'un de ses adjoints a répondu, par une lettre du 5 mars 2014, en indiquant les travaux projetés d'une part, en refusant la pose du miroir demandé d'autre part ; il a prévu qu'une copie de ce courrier soit distribuée aux habitants de la résidence. Contrairement à ce que soutient MmeW..., la diffusion en nombre de ce courrier à une date proche du scrutin, pour regrettable qu'elle soit, ne saurait être regardée, eu égard à son contenu purement informatif et au ton employé, comme la réalisation d'une campagne de promotion publicitaire prohibée par l'article L. 52-1 du code électoral ou la distribution d'un document de propagande assimilable à un don prohibé par l'article L. 52-8 de ce code.<br/>
<br/>
              S'agissant des autres griefs :<br/>
<br/>
              10. En premier lieu, il résulte de l'instruction que le centre communal d'action sociale de Marcy-L'Etoile a consenti, le 17 mars 2014, la location à MmeE..., candidate de la liste conduite par M.N..., d'un local à mi-temps afin qu'elle y exerce son activité professionnelle. Contrairement à ce que soutient MmeW..., il ne résulte de l'instruction ni que cette attribution aurait été faite dans le but " de faire valoir l'ancrage local " de la liste conduite par M.N..., ni qu'elle ait pu avoir une quelconque influence sur la sincérité du scrutin.<br/>
<br/>
              11. En deuxième lieu, il est constant que le compte-rendu du conseil municipal du 27 février 2014 n'a été affiché que le 24 mars suivant, en méconnaissance de l'article L. 2121-25 du code général des collectivités territoriales qui prévoit son affichage sous huitaine. Toutefois, il ne résulte pas de l'instruction que cette circonstance, qui n'a pas privé la liste conduite par Mme W...de la possibilité de porter le débat, au cours de la campagne électorale, sur les sujets évoqués lors de ce conseil municipal, ait constitué une manoeuvre de nature à altérer la sincérité du scrutin.  <br/>
<br/>
              12. En troisième lieu, il résulte de l'instruction que, alors que 1 477 personnes ont signé les listes d'émargement, seuls 1 476 enveloppes ou bulletins sans enveloppe ont été trouvés dans les urnes. Toutefois, il n'est pas allégué que cette différence résulterait d'une manoeuvre. Dès lors, il y avait lieu, pour le calcul de la majorité et la détermination des candidats élus, de se fonder, ainsi qu'il a été fait, sur le nombre de bulletins trouvés dans les urnes. <br/>
<br/>
              13. Il résulte de tout ce qui précède que Mme W...n'est pas fondée à se plaindre de ce que, par le jugement attaqué, le tribunal administratif de Lyon a rejeté sa protestation.<br/>
<br/>
              Sur les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              17. Il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par les défendeurs au titre de ces dispositions.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'intervention de M.O..., MmeS..., M.B..., Mme. K...et M.M... n'est pas admise.<br/>
Article 2 : La requête de Mme W...est rejetée.<br/>
Article 3 : Les conclusions présentées par M.N..., MmeAC..., M. Y..., MmeE..., M.D..., MmeX..., M.R..., MmeJ..., M.AA..., M.T..., M.V..., MmeA..., M.AB..., M.Z..., M.U..., Mme L..., MmeQ..., MmeG..., MmeI..., Mme F...et M. H...au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée à Mme P...W..., M. C...N...et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
