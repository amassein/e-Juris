<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029800157</ID>
<ANCIEN_ID>JG_L_2014_11_000000382712</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/80/01/CETATEXT000029800157.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème SSJS, 13/11/2014, 382712, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-11-13</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>382712</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème SSJS</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Pauline Pannier</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Aurélie Bretonneau</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2014:382712.20141113</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 15 juillet 2014 au secrétariat du contentieux du Conseil d'Etat, présentée par la commune de Girancourt, représentée par son maire ; la commune de Girancourt demande au Conseil d'Etat d'annuler pour excès de pouvoir le décret n° 2014-268 du 27 février 2014, modifié par décret n° 2014-351 du 19 mars 2014, portant délimitation des cantons dans le département des Vosges ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code électoral ;<br/>
<br/>
              Vu le code général des collectivités territoriales ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Pauline Pannier, auditeur,  <br/>
<br/>
              - les conclusions de Mme Aurélie Bretonneau, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'aux termes de l'article L. 191-1 du code électoral, résultant de la loi du 17 mai 2013 relative à l'élection des conseillers départementaux, des conseillers municipaux et des conseillers communautaires et modifiant le calendrier électoral : " Le nombre de cantons dans lesquels sont élus les conseillers départementaux est égal, pour chaque département, à la moitié du nombre de cantons existant au 1er janvier 2013, arrondi à l'unité impaire supérieure si ce nombre n'est pas entier impair. / Le nombre de cantons dans chaque département comptant plus de 500 000 habitants ne peut être inférieur à dix-sept. Il ne peut être inférieur à treize dans chaque département comptant entre 150 000 et 500 000 habitants " ; qu'aux termes de l'article L. 3113-2 du code général des collectivités territoriales, dans sa rédaction issue de la même loi, applicable à la date du décret attaqué : " I. - Les modifications des limites territoriales des cantons, les créations et suppressions de cantons et le transfert du siège de leur chef-lieu sont décidés par décret en Conseil d'Etat après consultation du conseil général qui se prononce dans un délai de six semaines à compter de sa saisine. A l'expiration de ce délai, son avis est réputé rendu. (...) / III. - La modification des limites territoriales des cantons effectuée en application du I est conforme aux règles suivantes : / a) Le territoire de chaque canton est défini sur des bases essentiellement démographiques ; / b) Le territoire de chaque canton est continu ; / c) Est entièrement comprise dans le même canton toute commune de moins de 3 500 habitants ; / IV. - Il n'est apporté aux règles énoncées au III que des exceptions de portée limitée, spécialement justifiées, au cas par cas, par des considérations géographiques (...) ou par d'autres impératifs d'intérêt général " ;<br/>
<br/>
              2. Considérant que le décret attaqué a, sur le fondement de l'article L. 3113-2 du code général des collectivités territoriales, procédé à une nouvelle délimitation des cantons du département des Vosges, dont le nombre passe de trente-et-un à dix-sept, compte tenu de l'exigence de réduction du nombre des cantons de ce département résultant de l'article L. 191-1 du code électoral ;<br/>
<br/>
              Sur la légalité externe du décret attaqué :<br/>
<br/>
              3. Considérant qu'il résulte des termes mêmes des dispositions législatives précitées qu'il appartenait au Gouvernement de procéder, par décret en Conseil d'Etat, à une nouvelle délimitation territoriale de l'ensemble des cantons ; <br/>
<br/>
              4. Considérant que les dispositions de l'article L. 3113-2 du code général des collectivités territoriales se bornent à prévoir la consultation du conseil général du département concerné à l'occasion de l'opération de création et suppression de cantons ; que ni le principe constitutionnel de libre administration des collectivités territoriales, ni aucune autre disposition législative ou réglementaire n'imposent une consultation des communes du département et des établissements publics de coopération intercommunale faisant l'objet d'un remodelage des limites cantonales ; <br/>
<br/>
              5. Considérant qu'il n'est pas contesté qu'un projet de décret accompagné d'un exposé des motifs décrivant les critères retenus pour procéder à la nouvelle délimitation des cantons des Vosges et d'une carte, a été soumis au conseil général de ce département ; que sur cette base, et alors même que la requérante soutient que ce projet aurait eu une portée générale, il n'est pas contesté que l'assemblée départementale a été mise à même d'émettre un avis sur les modalités de mise en oeuvre de la nouvelle délimitation des cantons prévue par le législateur et de faire des propositions spécifiques pour le département des Vosges, notamment en proposant un nouveau projet de découpage cantonal ; qu'il ne ressort pas des pièces du dossier que le délai laissé au conseil général des Vosges pour se prononcer ait été insuffisant ; que, par suite, la requérante n'est pas fondée à soutenir que la consultation du conseil général aurait été irrégulière ;<br/>
<br/>
              Sur la légalité interne du décret attaqué :<br/>
<br/>
              6. Considérant que la circonstance que le décret attaqué se borne à identifier, pour chaque canton, un " bureau centralisateur " sans mentionner les chefs-lieux de canton est sans incidence sur la légalité de ce décret, qui porte sur la délimitation des circonscriptions électorales dans le département des Vosges ;<br/>
<br/>
              7. Considérant qu'il résulte des dispositions de l'article L. 3113-2 du code général des collectivités territoriales que le territoire de chaque canton doit être établi sur des bases essentiellement démographiques, qu'il doit être continu et que toute commune de moins de 3 500 habitants doit être entièrement comprise dans un même canton, seules des exceptions de portée limitée et spécialement justifiées pouvant être apportées à ces règles ; que si la commune de Girancourt soutient que le redécoupage cantonal dans le département des Vosges a eu pour effet de créer des cantons dont la population est nettement inférieure à la moyenne départementale, elle n'assortit pas ce moyen des précisions permettant d'en apprécier le bien fondé ;<br/>
<br/>
              8. Considérant que ni les dispositions précitées, ni aucun autre texte non plus qu'aucun principe n'imposent au Premier ministre de prévoir que les limites des cantons, qui sont des circonscriptions électorales, coïncident avec les limites des circonscriptions législatives, les périmètres des établissements publics de coopération intercommunale figurant dans le schéma départemental de coopération intercommunale ou les limites des " bassins de vie " définis par l'Institut national de la statistique et des études économiques ; que, de même, si l'article L. 192 du code électoral, dans sa rédaction antérieure à l'intervention de la loi précitée du 17 mai 2013, relatif aux modalités de renouvellement des conseils généraux, faisait référence aux arrondissements, aucun texte en vigueur à la date du décret contesté ne mentionne ces arrondissements, circonscriptions administratives de l'Etat, pour la détermination des limites cantonales ; que, par suite, la requérante ne saurait utilement se prévaloir de ce que la délimitation de plusieurs cantons du département des Vosges ne correspondrait pas à celle d'autres circonscriptions électorales ou de subdivisions administratives ; <br/>
<br/>
              9. Considérant que, contrairement à ce que soutient la requérante, le nouveau découpage des cantons n'a ni pour objet ni pour effet de modifier le champ territorial de compétence de services publics jusque-là organisés sur une base cantonale, tel que le ressort des tribunaux de grande instance ; <br/>
<br/>
              10. Considérant qu'il résulte de ce qui précède que la requête de la commune de Girancourt doit être rejetée ;<br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er : La requête de la commune de Girancourt est rejetée. <br/>
Article 2 : La présente décision sera notifiée la commune de Girancourt et au ministre de l'intérieur.<br/>
Copie en sera adressée pour information au Premier ministre.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
