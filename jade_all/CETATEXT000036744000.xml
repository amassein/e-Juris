<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036744000</ID>
<ANCIEN_ID>JG_L_2018_03_000000411437</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/74/40/CETATEXT000036744000.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème chambre, 26/03/2018, 411437, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-03-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>411437</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP THOUVENIN, COUDRAY, GREVY</AVOCATS>
<RAPPORTEUR>M. Marc Firoud</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Gilles Pellissier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:411437.20180326</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              M. A...B...a demandé au tribunal administratif de Rennes, d'une part, d'annuler la décision du 17 juin 2011 par laquelle le ministre de la défense a rejeté sa demande d'indemnisation présentée au titre de la loi n° 2010-2 du 5 janvier 2010 relative à la reconnaissance et à l'indemnisation des victimes des essais nucléaires français et, d'autre part, de condamner l'Etat à l'indemniser intégralement des préjudices qu'il a subis du fait de ces essais. Par un jugement n° 1103017 du 31 décembre 2015, le tribunal administratif de Rennes a annulé la décision attaquée et a enjoint au comité d'indemnisation des victimes des essais nucléaires (CIVEN) de présenter à M. B...une proposition d'indemnisation des préjudices subis, dans un délai de trois mois.<br/>
<br/>
              Par un arrêt n° 16NT01028 du 12 avril 2017, sur un recours formé par le ministre de la défense, la cour administrative d'appel de Nantes a annulé ce jugement et rejeté la demande présentée par M. B...devant le tribunal administratif de Rennes. <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 12 juin et 12 septembre 2017 au secrétariat du contentieux du Conseil d'Etat, M. B...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel du ministre de la défense ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la loi n° 2010-2 du 5 janvier 2010 ;<br/>
              - la loi n° 2017-256 du 28 février 2017 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Marc Firoud, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Gilles Pellissier, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Thouvenin, Coudray, Grevy, avocat de M.B....<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des énonciations de l'arrêt attaqué que M. B..., militaire de carrière, né le 9 septembre 1934, a été affecté au centre d'expérimentation du Pacifique et a servi en qualité d'encadrant de l'équipe de sécurité du bord sur le bâtiment-base " Maurienne " du 11 avril 1971 au 16 juin 1972 ; qu'au cours de cette période, cinq essais nucléaires atmosphériques ont été réalisés ; qu'il a déclaré un cancer cutané et un cancer de l'estomac, diagnostiqués en 2009 ; qu'il a présenté le 6 août 2010 une demande d'indemnisation des préjudices subis du fait de ces essais au comité d'indemnisation des victimes des essais nucléaires (CIVEN), en se prévalant des dispositions de la loi du 5 janvier 2010 relative à la reconnaissance et à l'indemnisation des victimes des essais nucléaires français ; que le ministre de la défense a, par une décision du 17 juin 2011, rejeté sa demande au motif que le risque imputable aux essais nucléaires dans la survenance de sa maladie pouvait être qualifié de négligeable ; qu'à la demande de M.B..., par un jugement du 31 décembre 2015, le tribunal administratif de Rennes a annulé cette décision et a enjoint au CIVEN de présenter une proposition d'indemnisation des préjudices subis par M.B..., imputables à ses maladies radio-induites, dans un délai de trois mois ; que, par un arrêt du 12 avril 2017 contre lequel M. B...se pourvoit en cassation, la cour administrative d'appel de Nantes a annulé ce jugement et rejeté la demande présentée par M. B...devant le tribunal administratif de Rennes ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article 1er de la loi du 5 janvier 2010 relative à la reconnaissance et à l'indemnisation des victimes des essais nucléaires français : " Toute personne souffrant d'une maladie radio-induite résultant d'une exposition à des rayonnements ionisants dus aux essais nucléaires français et inscrite sur une liste fixée par décret en Conseil d'Etat conformément aux travaux reconnus par la communauté scientifique internationale peut obtenir réparation intégrale de son préjudice dans les conditions prévues par la présente loi. / Si la personne est décédée, la demande de réparation peut être présentée par ses ayants droit " ; qu'aux termes de l'article 2 de cette même loi : " La personne souffrant d'une pathologie radio-induite doit avoir résidé ou séjourné : / 1° Soit entre le 13 février 1960 et le 31 décembre 1967 au Centre saharien des expérimentations militaires, ou entre le 7 novembre 1961 et le 31 décembre 1967 au Centre d'expérimentations militaires des oasis ou dans les zones périphériques à ces centres ; / 2° Soit entre le 2 juillet 1966 et le 31 décembre 1998 en Polynésie française. / (...) " ; qu'aux termes de l'article 4 de cette même loi, dans sa rédaction antérieure à la loi du 28 février 2017 de programmation relative à l'égalité réelle outre-mer et portant autres dispositions en matière sociale et économique, disposait : " I. - Les demandes individuelles d'indemnisation sont soumises au comité d'indemnisation des victimes des essais nucléaires (...) / V. - Ce comité examine si les conditions de l'indemnisation sont réunies. Lorsqu'elles le sont, l'intéressé bénéficie d'une présomption de causalité à moins qu'au regard de la nature de la maladie et des conditions de son exposition le risque attribuable aux essais nucléaires puisse être considéré comme négligeable. Le comité le justifie auprès de l'intéressé (...) " ; qu'enfin, aux termes de l'article 113 de la loi du 28 février 2017 : " I.- Au premier alinéa du V de l'article 4 de la loi n° 2010-2 du 5 janvier 2010 relative à la reconnaissance et à l'indemnisation des victimes des essais nucléaires français, les mots et la phrase : " à moins qu'au regard de la nature de la maladie et des conditions de son exposition le risque attribuable aux essais nucléaires puisse être considéré comme négligeable. Le comité le justifie auprès de l'intéressé. " sont supprimés. / II.- Lorsqu'une demande d'indemnisation fondée sur les dispositions du I de l'article 4 de la loi n° 2010-2 du 5 janvier 2010 relative à la reconnaissance et à l'indemnisation des victimes des essais nucléaires français a fait l'objet d'une décision de rejet par le ministre de la défense ou par le comité d'indemnisation des victimes des essais nucléaires avant l'entrée en vigueur de la présente loi, le comité d'indemnisation des victimes des essais nucléaires réexamine la demande s'il estime que l'entrée en vigueur de la présente loi est susceptible de justifier l'abrogation de la précédente décision. Il en informe l'intéressé ou ses ayants droit s'il est décédé qui confirment leur réclamation et, le cas échéant, l'actualisent. Dans les mêmes conditions, le demandeur ou ses ayants droit s'il est décédé peuvent également présenter une nouvelle demande d'indemnisation, dans un délai de douze mois à compter de l'entrée en vigueur de la présente loi. / III.- Une commission composée pour moitié de parlementaires et pour moitié de personnalités qualifiées propose, dans un délai de douze mois à compter de la promulgation de la présente loi, les mesures destinées à réserver l'indemnisation aux personnes dont la maladie est causée par les essais nucléaires. Elle formule des recommandations à l'attention du Gouvernement " ; <br/>
<br/>
              3. Considérant que l'entrée en vigueur des dispositions précitées du I de l'article 113 de la loi du 28 février 2017 n'est pas manifestement impossible en l'absence de mesures d'application ; qu'elle est dès lors intervenue le lendemain de la publication de cette loi au Journal officiel de la République française ; que ces dispositions sont applicables aux instances en cours à cette date ; qu'il suit de là qu'en jugeant que ces dispositions n'étaient pas applicables à la demande d'indemnisation présentée par M.B..., la cour a commis une erreur de droit ; que son arrêt doit donc être annulé, sans qu'il soit besoin d'examiner les autres moyens du pourvoi ;<br/>
<br/>
              4. Considérant que, dans les circonstances de l'espèce, il y a lieu de régler l'affaire au fond, en application des dispositions de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              5. Considérant qu'il résulte des dispositions citées au point 2 ci-dessus que la décision du 17 juin 2011 par laquelle le ministre de la défense a rejeté la demande d'indemnisation présentée par M. B...au titre de la loi du 5 janvier 2010 relative à la reconnaissance et à l'indemnisation des victimes des essais nucléaires français au motif que le risque imputable aux essais nucléaires dans la survenue de sa maladie pouvait être considéré comme négligeable est illégale ; que, par suite, le ministre de la défense n'est pas fondé à soutenir que c'est à tort que par le jugement attaqué le tribunal administratif de Rennes a annulé cette décision et a enjoint au comité d'indemnisation des victimes des essais nucléaires de procéder au réexamen de la demande de M. B...;<br/>
<br/>
              6. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat le versement à M. B...d'une somme de 3 500 euros au titre des frais exposés par lui et non compris dans les dépens ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Nantes du 12 avril 2017 est annulé.<br/>
Article 2 : Le recours du ministre de la défense est rejeté.<br/>
Article 3 : L'Etat versera à M. B...la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à M. A...B..., au comité d'indemnisation des victimes des essais nucléaires et à la ministre des armées.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
