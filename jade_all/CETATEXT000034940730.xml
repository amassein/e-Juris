<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034940730</ID>
<ANCIEN_ID>JG_L_2017_06_000000398974</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/94/07/CETATEXT000034940730.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème chambre, 14/06/2017, 398974, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-06-14</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>398974</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP CELICE, SOLTNER, TEXIDOR, PERIER ; SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR>M. Cyrille Beaufils</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Suzanne von Coester</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2017:398974.20170614</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              La société Free Mobile a demandé au tribunal administratif de Versailles d'annuler pour excès de pouvoir l'arrêté du 23 octobre 2013 par lequel le maire de la commune de Ris-Orangis (Essonne) s'est opposé à sa déclaration préalable pour la construction d'un relais téléphonique sur un terrain situé 8 rue du Front populaire. Par un jugement n° 1307809 du 3 octobre 2014 le tribunal administratif de Versailles a rejeté cette demande.<br/>
<br/>
              Par un arrêt n° 14VE03152 du 18 février 2016, la cour administrative d'appel de Versailles a, sur la demande de la société Free Mobile, annulé ce jugement et l'arrêté du 23 octobre 2013 du maire de Ris-Orangis.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés le 22 avril et le 22 juillet 2016 au secrétariat du contentieux du Conseil d'Etat, la commune de Ris-Orangis demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de la société Free Mobile ;<br/>
<br/>
              3°) de mettre à la charge de la société Free Mobile la somme de 3 200 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
              Vu :<br/>
               - le code de l'urbanisme ;<br/>
               - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Cyrille Beaufils, auditeur,  <br/>
<br/>
              - les conclusions de Mme Suzanne von Coester, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Célice, Soltner, Texidor, Perier, avocat de la commune de Ris-orangis et à la SCP Piwnica, Molinié, avocat de la société Free mobile ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 22 mai 2017, présentée par la société Free Mobile ; <br/>
<br/>
<br/>
<br/>
<br/>1. Considérant que la commune de Ris-Orangis se pourvoit en cassation contre l'arrêt du 18 février 2016 par lequel la cour administrative d'appel de Versailles, faisant droit à l'appel formé par la société Free Mobile, a annulé l'arrêté du 23 octobre 2013 par lequel le maire de la commune de Ris-Orangis (Essonne) s'est opposé à une déclaration préalable pour la construction d'un relais téléphonique, composé notamment d'un pylône de 25 mètres, sur un terrain situé 8 rue du Front populaire, ainsi que le jugement du tribunal administratif qui avait rejeté sa demande tendant à l'annulation de cet arrêté ;<br/>
<br/>
              2. Considérant qu'en vertu de l'article Ul 10 du règlement du plan local d'urbanisme de la commune de Ris-Orangis, la hauteur maximale des constructions, dans le secteur concerné par le projet de relais téléphonique en litige, est fixée à 20 mètres ; qu'il est précisé que : " la hauteur des constructions est mesurée à partir du sol naturel jusqu'au point le plus élevé du bâtiment (faîtage, acrotère), les ouvrages techniques, cheminées et autres superstructures exclus, excepté les enseignes et les éléments de décoration. " ; que le lexique annexé à ce règlement définit la hauteur des constructions comme la hauteur " mesurée à partir du sol naturel jusqu'au point le plus élevé du bâtiment (faîtage, acrotère) ; les ouvrages techniques, cheminées et autres superstructures exclus " ; que ce lexique définit les ouvrages techniques comme les " ouvrages nécessaires à l'occupation d'un immeuble en superstructure ou en infrastructure de cet immeuble : cheminées, antennes, machineries d'ascenseur, ainsi que les relais téléphoniques ou radiographiques. " ; qu'il résulte de la combinaison de ces dispositions que les relais téléphoniques ou radiographiques sont soumis à la règle de hauteur maximale de construction de 20 mètres qu'elles édictent, à la seule exception de ceux qui, constituant des ouvrages techniques nécessaires à l'occupation d'un immeuble en superstructure ou en infrastructure, entrent dans le champ d'application de la dérogation à cette règle ;<br/>
<br/>
              3. Considérant qu'en jugeant que la dérogation à la règle maximale de hauteur de 20 mètres fixée par les dispositions citées au point précédent s'applique tant aux relais implantés sur les toits des bâtiments qu'à ceux qui, comme celui en cause dans la présente instance, sont implantés au sol, la cour administrative d'appel de Versailles a méconnu la portée de l'article Ul 10 du règlement du plan local d'urbanisme de la commune de Ris-Orangis et a ainsi commis une erreur de droit ; que, par suite, la commune de Ris-Orangis est fondée, sans qu'il soit besoin de statuer sur l'autre moyen du pourvoi, à demander l'annulation de l'arrêt de la cour administrative d'appel de Versailles ; <br/>
<br/>
              4. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la société Free Mobile une somme de 3 000 euros à verser à la commune de Ris-Orangis en application des dispositions de l'article L. 761-1 du code de justice administrative ; qu'en revanche, ces dispositions font obstacle à ce qu'une somme soit mise à la charge de la commune de Ris-Orangis, qui n'est pas, dans la présente instance, la partie perdante ; <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 18 février 2016 de la cour administrative d'appel de Versailles est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Versailles.<br/>
Article 3 : La société Free Mobile versera à la commune de Ris-Orangis une somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Les conclusions présentées par la société Free mobile au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée à la commune de Ris-Orangis et à la société Free mobile. <br/>
Copie en sera adressée au ministre de la cohésion des territoires.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
