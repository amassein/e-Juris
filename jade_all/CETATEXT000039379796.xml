<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039379796</ID>
<ANCIEN_ID>JG_L_2019_10_000000435435</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/37/97/CETATEXT000039379796.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 31/10/2019, 435435, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-10-31</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>435435</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP CELICE, SOLTNER, TEXIDOR, PERIER</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2019:435435.20191031</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              La société Diderot Education Campus a demandé au juge des référés du tribunal administratif de Rouen, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, d'une part, d'enjoindre à la rectrice de l'académie de Rouen de prendre toutes mesures pour que cessent les atteintes à la liberté d'enseignement, au droit à l'éducation, au principe d'égal accès à l'instruction, à la liberté d'entreprendre ainsi qu'à la liberté du commerce et de l'industrie et, d'autre part, de suspendre l'exécution de la décision du 26 septembre 2019 d'opposition à l'ouverture de l'établissement Château Le Vaillant. Par une ordonnance n° 1903428 du 4 octobre 2019, le juge des référés du tribunal administratif de Rouen a suspendu l'exécution de cette décision.<br/>
<br/>
              Par une requête, enregistrée le 18 octobre 2019 au secrétariat du contentieux du Conseil d'Etat, le ministre de l'éducation nationale et de la jeunesse demande au juge des référés du Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) statuant en référé, de rejeter la demande de la société Diderot Education Campus.<br/>
<br/>
<br/>
<br/>
<br/>
              Il soutient que :<br/>
              - le juge des référés du tribunal administratif de Rouen a entaché son ordonnance d'une erreur de droit dès lors qu'il a omis de se prononcer sur la condition d'urgence ; <br/>
              - la condition d'urgence n'est pas remplie, dès lors que la société Diderot Education Campus s'est placée dans une situation qui lui est imputable en ce qu'elle a procédé à des inscriptions avant l'expiration du délai prévu par l'article L. 441-1 du code de l'éducation ;  <br/>
              - c'est à tort que le juge des référés du tribunal administratif de Rouen a considéré que la décision a été notifiée au requérant après l'expiration du délai de trois mois prévu à l'article L. 441-1 du code de l'éducation ; <br/>
              - c'est à tort qu'il a considéré que la décision reposait sur des faits qui n'étaient matériellement pas établis dès lors qu'il n'est pas sérieusement contesté que l'établissement était ouvert avant le délai légal de trois mois et qu'il a accueilli des élèves les 25 et 26 septembre 2019 ; <br/>
              - c'est à tort qu'il a considéré qu'aucune ouverture effective de l'établissement au sens de l'article L. 441-1 du code de l'éducation n'était caractérisée dès lors que des enseignements étaient dispensés dès le mois de septembre. <br/>
<br/>
              Par un mémoire en défense, enregistré le 23 octobre 2019, la société Diderot Education Campus conclut au rejet de la requête et à ce que la somme de 4 000 euros soit mise à la charge de l'Etat au titre de l'article L. 761-1 du code de justice administrative. Elle soutient qu'il n'y a plus lieu de statuer sur l'appel du ministre et que les moyens soulevés par ce dernier ne sont pas fondés. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la Constitution ; <br/>
              - le code de l'éducation ;<br/>
              - le code des relations entre le public et l'administration ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, le ministre de l'éducation nationale et de la jeunesse et, d'autre part, la société Diderot Education Campus ;<br/>
<br/>
              Vu le procès-verbal de l'audience publique du 24 octobre 2019 à 15 heures au cours de laquelle ont été entendus : <br/>
<br/>
              - les représentantes du ministre de l'éducation nationale et de la jeunesse ;<br/>
<br/>
              - Me Périer, avocat au Conseil d'Etat et à la Cour de cassation, avocat de la société Diderot Education Campus ;<br/>
<br/>
              - les représentants de la société Diderot Education Campus ;<br/>
<br/>
              et à l'issue de laquelle le juge des référés a différé la clôture de l'instruction au 30 octobre 2019 à 12 heures puis le même jour à 18 heures ;<br/>
<br/>
              Vu les nouveaux mémoires, enregistrés le 30 octobre 2019, présentés par le ministre de l'éducation nationale et de la recherche ; <br/>
<br/>
              Vu les nouveaux mémoires, enregistrés le 30 octobre 2019, présentés par la société Diderot Campus Education ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale (...) ". <br/>
<br/>
              2. Aux termes de l'article L. 441-1 du code de l'éducation : " I.- Toute personne respectant les conditions de capacité et de nationalité fixées aux 1° et 2° du I de l'article L. 914-3 peut ouvrir un établissement d'enseignement scolaire privé à condition d'en déclarer son intention à l'autorité compétente de l'Etat en matière d'éducation, qui transmet la déclaration au maire de la commune dans laquelle l'établissement est situé, au représentant de l'Etat dans le département et au procureur de la République. II.- L'autorité compétente de l'Etat en matière d'éducation, le maire, le représentant de l'Etat dans le département et le procureur de la République peuvent former opposition à l'ouverture de l'établissement : 1° Dans l'intérêt de l'ordre public ou de la protection de l'enfance et de la jeunesse ; 2° Si la personne qui ouvre l'établissement ne remplit pas les conditions prévues au I du présent article ; 3° Si la personne qui dirigera l'établissement ne remplit pas les conditions prévues à l'article L. 914-3 ; 4° S'il ressort du projet de l'établissement que celui-ci n'a pas le caractère d'un établissement scolaire ou, le cas échéant, technique. A défaut d'opposition, l'établissement est ouvert à l'expiration d'un délai de trois mois. " En vertu de l'article L. 441-4 du même code, le fait d'ouvrir un établissement d'enseignement privé en dépit d'une opposition formulée par les autorités compétentes ou sans remplir les conditions prescrites par les dispositions du code relatives à l'ouverture des établissements d'enseignement scolaire privés est puni de 15 000 euros d'amende et de la fermeture de l'établissement et peut, en outre, conduire au prononcé d'une peine complémentaire d'interdiction d'ouvrir et de diriger un établissement scolaire ainsi que d'y enseigner, à titre définitif ou pour une durée de cinq ans au plus.<br/>
<br/>
              3. Il résulte de l'instruction que M. A..., représentant de la société Diderot Education Campus, après de premiers contacts avec les services du rectorat de Rouen en mars 2018, a fait part au rectorat, par une lettre du 29 avril 2019, de sa volonté d'ouvrir un établissement d'enseignement privé au Château Le Vaillant à Guerville (Seine-Maritime). Le dossier de déclaration d'ouverture de l'établissement, prévu par l'article L. 441-2 du code de l'éducation, a été adressé au rectorat le 31 mai 2019 et reçu le 3 juin. Après avoir réclamé des documents complémentaires, la rectrice de l'académie de Rouen a indiqué, par une lettre du 27 juin 2019, que le dossier de demande était complet et que le délai de trois mois prévu par l'article L. 441-1 expirerait le 27 septembre 2019. Par une décision du 26 septembre 2019, la rectrice s'est opposée à l'ouverture de l'établissement en se fondant sur le 1° du II de l'article L. 441-1 du code de l'éducation. La société Diderot Education Campus a saisi le juge des référés du tribunal administratif de Rouen sur le fondement de l'article L. 521-2 du code de justice administrative, lequel, par une ordonnance du 4 octobre 2019, a suspendu l'exécution de la décision d'opposition du 26 septembre 2019. Le ministre de l'éducation nationale et de la jeunesse relève appel de cette ordonnance devant le juge des référés du Conseil d'Etat. Contrairement à ce qui est soutenu en défense, la circonstance que des mesures provisoires aient été prises par les services du rectorat en exécution de l'ordonnance rendue en première instance n'est pas de nature à priver d'objet l'appel du ministre.<br/>
<br/>
              4. La liberté de l'enseignement, qui constitue l'un des principes fondamentaux reconnus par les lois de la République, réaffirmés par le Préambule de la Constitution du 27 octobre 1946 auquel se réfère le Préambule de la Constitution du 4 octobre 1958, présente le caractère d'une liberté fondamentale au sens de l'article L. 521-2 du code de justice administrative. La liberté d'entreprendre et la liberté du commerce de l'industrie, qui en est une composante, constituent, de même, des libertés fondamentales au sens de l'article L. 521-2 du code de justice administrative.<br/>
<br/>
              5. Pour s'opposer, par sa décision du 26 septembre 2019, à l'ouverture de l'établissement privé en cause, la rectrice de l'académie de Rouen s'est essentiellement fondée sur l'intérêt de l'ordre public qui aurait été mis en cause par l'ouverture prématurée de l'établissement avant l'expiration du délai de trois mois prévu par l'article L. 441-1 du code de l'éducation. Si la décision mentionne, en outre, le délit prévu à l'article L. 471-5 du même code, qui sanctionne la méconnaissance des règles de publicité et de démarchage applicables aux établissements d'enseignement, et si elle fait aussi état, de façon hypothétique, de ce que les dispositions du code du travail n'auraient pas été respectées par la société Diderot Education Campus " dans le cadre d'activités comparables dans une autre région ", aucun élément versé dans le cadre de l'instruction contradictoire menée devant le juge administratif des référés n'est de nature à étayer ces deux affirmations non circonstanciées.<br/>
<br/>
              6. S'agissant du motif essentiel de la décision contestée, ni cette décision, ni aucun élément produit dans le cadre de l'instruction en référé ne font état de motifs d'ordre public autres que l'ouverture de l'établissement avant l'expiration du délai d'opposition de trois mois. Sur ce point, à supposer que le point de départ du délai d'opposition ait bien été le 27 juin 2019, date à laquelle le dossier de déclaration a été regardé par les services du rectorat de Rouen comme complet, et non à une date antérieure, il ne résulte pas des éléments versés dans le cadre de l'instruction contradictoire menée en référé devant le juge administratif que l'établissement aurait été effectivement ouvert aux élèves pour leur dispenser des enseignements avant l'expiration du délai d'opposition. Si le ministre de l'éducation nationale et de la jeunesse se prévaut d'articles de presse et du constat de la présence d'élèves, effectué par la gendarmerie le 25 septembre 2019, il ne produit ni le procès-verbal qui aurait été établi par la gendarmerie, dont le procureur de la République refuse la communication, ni aucun autre document circonstancié de nature à établir l'ouverture effective de l'établissement. La société Diderot Education Campus produit, pour sa part, divers documents afin de montrer que si des élèves, et certains membres de leurs familles, étaient présents dans l'établissement les 25 et 26 septembre, leur accueil était organisé dans le cadre de journées " portes ouvertes " et que la rentrée était prévue pour le lundi 30 septembre 2019. Dans ces conditions, le motif essentiel de la décision contestée ne peut, en l'état de l'instruction devant le juge des référés du Conseil d'Etat, être tenu pour établi. Il s'ensuit que, sans qu'il y ait lieu de trancher en référé la question de savoir si, indépendamment des sanctions pénales prévues à l'article L. 441-4 du code de l'éducation, l'ouverture d'un établissement privé quelques jours avant l'expiration du délai de trois mois est au nombre des motifs susceptibles de justifier qu'il soit fait opposition à la déclaration " dans l'intérêt de l'ordre public ou de la protection de l'enfance et de la jeunesse " au sens de l'article L. 441-1 du code de l'éducation, la société Diderot Education Campus est fondée, en l'état de l'instruction, à soutenir que la décision du 26 septembre 2019 a porté une atteinte grave et manifestement illégale à la liberté de l'enseignement et à la liberté d'entreprendre.<br/>
<br/>
              7. Par ailleurs, l'exécution de la décision contestée du 26 septembre 2019 caractérise, eu égard à la situation des élèves inscrits dans l'établissement et aux intérêts de la société Diderot Education Campus, sans que ne soit allégué aucun intérêt public tenant aux conditions de l'accueil des élèves ou de l'enseignement dispensé, une situation d'urgence au sens de l'article L. 521-2 du code de justice administrative.<br/>
<br/>
              8. Il résulte de ce qui précède que le ministre de l'éducation nationale et de la jeunesse n'est pas fondé à se plaindre de ce que, par l'ordonnance attaquée, le juge des référés du tribunal administratif de Rouen a suspendu l'exécution de la décision du 26 septembre 2019.<br/>
<br/>
              9. Il y a lieu, dans les circonstances de l'espèce, de mettre une somme de 3 000 euros à la charge de l'Etat à verser à la société Diderot Education Campus au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : Le recours du ministre de l'éducation nationale et de la jeunesse est rejeté.<br/>
Article 2 : L'Etat versera une somme de 3 000 euros à la société Diderot Education Campus au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente ordonnance sera notifiée au ministre de l'éducation nationale et de la jeunesse et à la société Diderot Education Campus.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
