<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030286082</ID>
<ANCIEN_ID>JG_L_2015_02_000000382896</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/28/60/CETATEXT000030286082.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème et 5ème sous-sections réunies, 25/02/2015, 382896, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-02-25</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>382896</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème et 5ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BARTHELEMY, MATUCHANSKY, VEXLIARD, POUPOT</AVOCATS>
<RAPPORTEUR>M. David Moreau</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Gaëlle Dumortier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:382896.20150225</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
Procédure contentieuse antérieure<br/>
<br/>
              Par un jugement n° 1401378 du 19 juin 2014, le tribunal administratif de Nîmes a rejeté la protestation soulevée par MM. C...B..., E...D...et A...F...contre les opérations électorales qui se sont déroulées le 12 B...2014 en vue de l'élection du président de la communauté de communes du Pays de Rhône et Ouvèze.<br/>
<br/>
Procédure devant le Conseil d'Etat<br/>
<br/>
              Par une requête sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 21 juillet 2014, 21 août 2014 et 27 janvier 2015 au secrétariat du contentieux du Conseil d'Etat, M. D...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement n° 1401378 du 19 juin 2014 du tribunal administratif de Nîmes ;<br/>
<br/>
              2°) d'annuler les opérations électorales qui se sont déroulées le 12 B...2014 en vue de l'élection du président de la communauté de communes du Pays de Rhône et Ouvèze ;<br/>
<br/>
              3°) de mettre à la charge de M. Milon le versement de la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code électoral ;<br/>
              - le code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. David Moreau, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Gaëlle Dumortier, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Barthélemy, Matuchansky, Vexliard, Poupot, avocat de M. D...;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant que M. D...relève appel du jugement du 19 juin 2014 par lequel le tribunal administratif de Nîmes a rejeté sa protestation tendant à l'annulation des opérations électorales du 12 B...2014, qui ont conduit à l'élection de M. Milon en qualité de président de la communauté de communes du Pays de Rhône et Ouvèze ;<br/>
<br/>
              2. Considérant que, contrairement à ce que soutient M. Milon, la protestation de M.D..., qui a été reçue au greffe du tribunal administratif de Nîmes par télécopie le 17 B... 2014 avant 18h00, n'était pas tardive au regard du délai fixé par l'article R. 119 du code électoral ;<br/>
<br/>
              3. Considérant qu'aux termes de l'article L. 5211-2 du code général des collectivités territoriales : " A l'exception de celles des deuxième à quatrième alinéas de l'article L. 2122-4, les dispositions du chapitre II du titre II du livre Ier de la deuxième partie relatives au maire et aux adjoints sont applicables au président et aux membres du bureau des établissements publics de coopération intercommunale, en tant qu'elles ne sont pas contraires aux dispositions du présent titre " ; qu'aux termes du premier alinéa de l'article L. 2122-7 du même code : " Le maire est élu au scrutin secret et à la majorité absolue. / Si, après deux tours de scrutin, aucun candidat n'a obtenu la majorité absolue, il est procédé à un troisième tour de scrutin et l'élection a lieu à la majorité relative. / En cas d'égalité de suffrages, le plus âgé est déclaré élu " ; qu'aucun texte ne fixant les autres modalités de cette élection, il appartient au juge de l'élection de vérifier si les opérations relatives à l'élection du président du conseil d'un établissement public de coopération intercommunale se sont déroulées dans des conditions qui n'ont pas porté atteinte à la sincérité du scrutin ;<br/>
<br/>
              4. Considérant qu'il résulte de l'instruction que M. Milon, président sortant de la communauté de communes du Pays de Rhône et Ouvèze, a envoyé avant le scrutin au domicile de tous les conseillers communautaires, grâce à un fichier d'adresses dont ne pouvait disposer son adversaire, M.F..., un tract de six pages dans lequel il se présente notamment comme le défenseur d'une vision " collégiale " de l'intercommunalité, à laquelle s'opposerait la vision " hiérarchique " de son adversaire, dénonce l'attitude centralisatrice de la commune d'Orange, vante son bilan financier et exprime sa position sur de nombreux sujets relatifs au fonctionnement et aux compétences de la communauté de communes ; qu'en raison de sa réception tardive, la veille du scrutin, et de la nouveauté de son contenu, M.F..., qui, en outre, n'avait pas accès au fichier des élus, n'a pas été en mesure de répondre utilement à ce tract avant l'élection; que, compte tenu du très faible écart de voix séparant les deux candidats, M. Milon ayant remporté l'élection avec 26 voix contre 24 pour M.F..., la distribution de ce tract a été de nature à altérer la sincérité du scrutin ; que, par suite, et sans qu'il soit besoin d'examiner les autres moyens de la requête, M. D...est fondé à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Nîmes a rejeté sa protestation contre l'élection de M. Milon en qualité de président de la communauté de communes du Pays de Rhône et Ouvèze ;<br/>
<br/>
              5. Considérant que, dans les circonstances de l'espèce, il n'y a pas lieu de mettre à la charge de M. Milon la somme que demande M. D...au titre des dispositions de l'article L. 761-1 du code de justice administrative ; que ces mêmes dispositions font obstacle à ce qu'une somme soit à la charge de M.D..., qui n'est pas la partie perdante dans la présente instance, au titre des frais exposés par M. Milon et non compris dans les dépens ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er : Le jugement du tribunal administratif de Nîmes du 19 juin 2014 est annulé.<br/>
<br/>
Article 2 : Les opérations électorales du 12 B...2014 qui ont conduit à l'élection de M. Milon en qualité de président du conseil communautaire de la communauté de communes du Pays de Rhône et Ouvèze sont annulées.<br/>
<br/>
Article 3 : Les conclusions présentées par les parties au titre de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
<br/>
Article 4 : La présente décision sera notifiée à M. E...D..., à M. A...Milon et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
