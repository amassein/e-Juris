<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027120711</ID>
<ANCIEN_ID>J0_L_2013_02_000001004169</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/12/07/CETATEXT000027120711.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Cour Administrative d'Appel de Versailles, 3ème Chambre, 26/02/2013, 10VE04169, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-02-26</DATE_DEC>
<JURIDICTION>Cour Administrative d'Appel de Versailles</JURIDICTION>
<NUMERO>10VE04169</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème Chambre</FORMATION>
<TYPE_REC>plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>Mme SIGNERIN-ICRE</PRESIDENT>
<AVOCATS>SOCIETE D'AVOCATS TAJ</AVOCATS>
<RAPPORTEUR>M. Patrick  BRESSE</RAPPORTEUR>
<COMMISSAIRE_GVT>M. LOCATELLI</COMMISSAIRE_GVT>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 29 décembre 2010 au greffe de la Cour administrative d'appel de Versailles, présentée pour la SOCIETE AGAPES, ayant son siège immeuble Péricentre, rue Van Gogh à Villeneuve-d'Ascq (59658), par la société d'avocat TAJ, avocat ; la société AGAPES SA demande à la Cour :<br/>
<br/>
      1°) d'annuler le jugement n° 0809608 et 0902754 en date du 14 octobre 2010 par lequel le Tribunal administratif de Montreuil a rejeté ses demandes tendant à la réduction des cotisations d'impôt sur les sociétés, de contributions additionnelles à cet impôt et de contributions sociales auxquelles elle a été assujettie au titre des années 2005, 2006 et 2007 à hauteur, respectivement, de 365 512 euros, 105 158 euros et 464 688 euros ;<br/>
<br/>
      2°) de prononcer la restitution des impositions en litige ;<br/>
<br/>
	3°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
	4°) de mettre les dépens à la charge de l'Etat ;<br/>
      Elle soutient que :<br/>
<br/>
      - le jugement est irrégulier dès lors que le mémoire produit le 27 septembre 2010 par l'administration fiscale ne lui a pas été communiqué en méconnaissance du principe du contradictoire et des dispositions du 4ème alinéa de l'article R. 611-1 du code de justice administrative et qu'à supposer que ce mémoire n'ait comporté aucun élément nouveau, le tribunal administratif a omis d'en viser les conclusions en méconnaissance du 1er alinéa de l'article R. 742-2 du code de justice administrative ; <br/>
      - le jugement est entaché d'un manque de motivation dès lors que, contrairement à ce qu'ont estimé les premiers juges, elle ne demandait pas que ses filiales Flunch Italie et Agapes Polska soient, sur le fondement des articles 223 A et suivants du code général des impôts, fiscalement intégrées au groupe fiscal qu'elle a constitué en France, mais que les pertes définitives de ces deux sociétés soient imputées sur le résultat d'ensemble du groupe dont elle est la société mère, sur le fondement des articles 49 et suivants du traité sur le fonctionnement de l'Union Européenne qui prévoient le principe de la liberté d'établissement, tels qu'ils ont été interprétés par la Cour de justice des communautés européennes dans son arrêt du 13 décembre 2005 affaire C-446/03 Marks et Spencer ; que, faute d'avoir saisi les conclusions présentées, le jugement doit être annulé pour insuffisance de motivation ;<br/>
<br/>
      - le jugement est également entaché d'un défaut de réponse à certaines de ses conclusions dès lors que le tribunal a fait application de la jurisprudence issue de l'arrêt de la Cour de justice de l'Union européenne du 25 février 2010 affaire 337/08 X Holding BV, d'ailleurs sans la citer, sans répondre à ses conclusions tendant à démontrer son absence de pertinence en l'espèce et établissant, au contraire, l'applicabilité de la solution résultant de l'arrêt de cette Cour du 13 décembre 2005 affaire 446/03 Marks et Spencer ; <br/>
<br/>
      - le régime d'intégration fiscale en vigueur aux Pays-Bas, qui est en cause dans l'arrêt X holding BV, offre la possibilité de procéder à une consolidation complète des profits et pertes au niveau de la société mère et permet de comprendre dans le périmètre de l'intégration à la fois des sociétés néerlandaises et des sociétés non résidentes des Pays-Bas à condition, toutefois, qu'elles y disposent d'un établissement stable et qu'ainsi, seules les sociétés sujettes à l'impôt aux Pays-Bas peuvent être fiscalement intégrées ; que ce régime, qui constitue une restriction à la liberté d'établissement, n'a été validé par la Cour de justice de l'Union Européenne qu'au regard de la justification tirée de la répartition équilibrée du pouvoir d'imposer et d'un risque de trafic des pertes ; que, cependant, le risque de transfert de pertes n'existait pas dans l'espèce soumise à la Cour et est très largement surestimé par celle-ci compte tenu du fonctionnement de la fiscalité internationale et alors, au surplus, que les Pays-Bas limitent à 5 ans la prise en compte des pertes ; que, par ailleurs, même si cet argument a été rejeté par la Cour, le régime d'intégration néerlandais comporte une mesure moins contraignante que la complète exclusion du régime d'intégration fiscale des filiales étrangères, à savoir le régime de déduction temporaire des pertes avec reprise qui est applicable aux établissements stables ; <br/>
<br/>
      - la solution résultant de l'arrêt Marks et Spencer, qui n'est pas remise en cause par l'arrêt X Holding BV, est la seule pertinente dans le cas, comme en l'espèce, de pertes devenues définitives ; qu'en effet, l'arrêt Marks et Spencer pose le principe qu'il est contraire aux articles 43 et 48 traité instituant la Communauté européenne d'exclure la possibilité d'imputer les pertes réalisées par des filiales non résidentes dès lors que la filiale non résidente a épuisé les possibilités de prise en compte des pertes et qu'il n'existe pas de possibilité que ces pertes puissent être prises en compte dans son Etat de résidence au titre des exercices futurs, ceci dans le cadre du régime anglais du " group relief ", régime de consolidation assez limité ; que cette position a été confirmée par l'arrêt de la Cour du 15 mai 2008 affaire 414/06 Lidl Belgium Gmbh, qui impose à nouveau la prise en compte des pertes devenues définitives ; <br/>
<br/>
      - il a été justifié, par la production des renseignements nécessaires sur les législations fiscales italienne et polonaise, de l'impossibilité définitive d'imputer les pertes subies et de la réalité des pertes en cause ; <br/>
<br/>
      - la solution adoptée par le tribunal administratif qui a refusé d'appliquer la solution résultant de l'arrêt Marks et Spencer est donc contraire à la liberté d'établissement telle qu'interprétée par le juge communautaire, interprétation qui s'impose en droit interne ;<br/>
<br/>
      ......................................................................................................<br/>
<br/>
      Vu les autres pièces du dossier ;<br/>
      Vu le traité instituant la Communauté européenne ;<br/>
<br/>
      Vu le code général des impôts et le livre des procédures fiscales ;<br/>
<br/>
      Vu l'arrêt de la Cour de justice des Communautés européennes du 13 décembre 2005, Marks et Spencer (C-446/03) et l'arrêt de la Cour de justice de l'Union européenne du 25 février 2010, X Holding BV (C-337/08) ; <br/>
<br/>
      Vu le code de justice administrative ;<br/>
<br/>
      Les parties ayant été régulièrement averties du jour de l'audience ;<br/>
<br/>
Après avoir entendu au cours de l'audience publique du 15 janvier 2013 :<br/>
- le rapport de M. Bresse, président assesseur ;<br/>
- et les conclusions de M. Locatelli, rapporteur public ;<br/>
       1. Considérant que la SOCIETE AGAPES, société mère d'un groupe fiscal intégré au sens de l'article 223 A du code général des impôts, a demandé à l'administration fiscale de prendre en compte dans le résultat d'ensemble du groupe au titre des années 2005 à 2007 les pertes subies par sa filiale polonaise Agapes Polska et par sa sous-filiale italienne Flunch Italie et de lui restituer les cotisations d'impôts correspondantes, au motif qu'en application des législations polonaise et italienne, ces sociétés ne pouvaient plus reporter lesdites pertes sur leur résultat propre ; que l'administration ayant rejeté ses réclamations, par application de l'article 223A du code général des impôts qui réserve le régime d'intégration fiscale aux seules sociétés françaises, la SOCIETE AGAPES a saisi le Tribunal administratif de Montreuil de demandes tendant aux mêmes fins ; qu'elle fait appel du jugement du 14 octobre 2010 par lequel le tribunal a rejeté lesdites demandes ;<br/>
       Sur la régularité du jugement attaqué :<br/>
       2. Considérant, en premier lieu, qu'aux termes de l'article R. 611-1 du code de justice administrative : " (...) La requête, le mémoire complémentaire annoncé dans la requête et le premier mémoire de chaque défendeur sont communiqués aux parties avec les pièces jointes dans les conditions prévues aux articles R. 611-3, R. 611-5 et R. 611-6. / Les répliques, autres mémoires et pièces sont communiqués s'ils contiennent des éléments nouveaux " ; qu'aux termes de l'article R. 613-2 du même code : " Si le président de la formation de jugement n'a pas pris une ordonnance de clôture, l'instruction est close trois jours francs avant la date de l'audience (...) " ; qu'aux termes de l'article R. 613-3 de ce code : " Les mémoires produits après la clôture de l'instruction ne donnent pas lieu à communication et ne sont pas examinés par la juridiction (...) " ;<br/>
       3. Considérant qu'il résulte de ces dispositions, d'une part, que l'instruction est normalement close, à défaut d'ordonnance de clôture, trois jours francs avant la date de l'audience ; que ce délai doit être dans tous les cas décompté sans qu'il y ait lieu de distinguer selon qu'il comporte ou non un samedi, un dimanche ou un jour férié ou selon qu'il est ou non précédé d'un tel jour ; qu'ainsi, un mémoire enregistré le lundi pour une audience prévue le jeudi parvient après la clôture de l'instruction et n'a normalement pas à être examiné par la juridiction ni communiqué aux parties ; d'autre part, que, lorsque, postérieurement à la clôture de l'instruction, le juge est saisi d'un mémoire émanant de l'une des parties à l'instance, il lui appartient, dans tous les cas, de prendre connaissance de ce mémoire avant de rendre sa décision, ainsi que de le viser sans l'analyser ; que s'il a toujours la faculté, dans l'intérêt d'une bonne justice, d'en tenir compte - après l'avoir visé et, cette fois, analysé -, il n'est tenu de le faire, à peine d'irrégularité de sa décision, que si ce mémoire contient soit l'exposé d'une circonstance de fait dont la partie qui l'invoque n'était pas en mesure de faire état avant la clôture de l'instruction écrite et que le juge ne pourrait ignorer sans fonder sa décision sur des faits matériellement inexacts, soit d'une circonstance de droit nouvelle ou que le juge devrait relever d'office ;<br/>
      4. Considérant qu'en l'espèce, il ressort des pièces du dossier de première instance qu'un nouveau mémoire a été produit par l'administration et enregistré le lundi 27 septembre 2010 au greffe du tribunal administratif, alors que l'audience était prévue et s'est tenue le jeudi 30 septembre 2010 ; qu'en l'absence d'ordonnance de clôture de l'instruction, celle-ci était close, en application des dispositions de l'article R. 613-2 du code de justice administrative, trois jours francs avant l'audience, soit le dimanche 26 septembre à minuit ; que ce mémoire de l'administration a donc été produit postérieurement à la clôture de l'instruction ; <br/>
      5. Considérant, par ailleurs, qu'il ressort également des pièces du dossier de première instance que ce mémoire, dans lequel l'administration fiscale se bornait à reprendre son argumentation antérieure y compris s'agissant de l'interprétation de l'arrêt rendu le 25 février 2010 par la Cour de justice de l'Union européenne dans l'affaire 337/08 X Holding BV, ne contenait pas d'éléments nouveaux que le tribunal administratif aurait pris en compte pour fonder sa décision ; que, dès lors, le tribunal, qui a visé ce mémoire, a fait une exacte application des règles prévues par l'article R. 613-3 du code de justice administrative en se bornant à le viser sans l'analyser et n'a pas méconnu le principe du contradictoire en statuant sans rouvrir l'instruction et sans communiquer ce mémoire à la SOCIETE AGAPES ;<br/>
       6. Considérant, en second lieu, que le moyen tiré de ce que le jugement serait entaché d'insuffisance de motivation au motif que le tribunal administratif se serait mépris sur la portée des moyens invoqués par la requérante s'agissant de la méconnaissance de liberté d'établissement garantie par les articles 43 et suivants du traité instituant la Communauté européenne manque en fait ; que, par ailleurs, le tribunal n'a omis de répondre à aucune des conclusions de la SOCIETE AGAPES ; que, dès lors, celle-ci n'est pas fondée à soutenir que le jugement attaqué serait entaché d'irrégularité de ces chefs ;<br/>
       Sur le bien fondé des impositions :<br/>
       7. Considérant qu'aux termes de l'article 43 du traité instituant la Communauté européenne, devenu l'article 49 du traité sur le fonctionnement de l'Union européenne : " Dans le cadre des dispositions visées ci-après, les restrictions à la liberté d'établissement des ressortissants d'un État membre dans le territoire d'un autre État membre sont interdites. Cette interdiction s'étend également aux restrictions à la création d'agences, de succursales ou de filiales, par les ressortissants d'un État membre établis sur le territoire d'un État membre. La liberté d'établissement comporte l'accès aux activités non salariées et leur exercice, ainsi que la constitution et la gestion d'entreprises, et notamment de sociétés au sens de l'article 48, deuxième alinéa, dans les conditions définies par la législation du pays d'établissement pour ses propres ressortissants, sous réserve des dispositions du chapitre relatif aux capitaux " ; qu'aux termes de l'article 48 du même traité, devenu l'article 54 du traité sur le fonctionnement de l'Union européenne : " Les sociétés constituées en conformité de la législation d'un État membre et ayant leur siège statutaire, leur administration centrale ou leur principal établissement à l'intérieur de la Communauté sont assimilées, pour l'application des dispositions du présent chapitre, aux personnes physiques ressortissantes des États membres " ;<br/>
       8. Considérant que, pour demander la restitution des impositions en litige, la SOCIETE AGAPES soutient que le refus de prise en compte des pertes définitivement subies par les sociétés Agapes Polska et Flunch Italie constitue une restriction disproportionnée à la liberté d'établissement et, par suite, contraire aux articles 43 et 48 précités du traité instituant la Communauté européenne ;<br/>
       9. Considérant, toutefois, que les articles 43 et 48 du traité instituant la Communauté européenne, tels qu'ils ont été interprétés par la Cour de justice de l'Union européenne, ne s'opposent pas, en l'état actuel du droit communautaire, à la législation d'un Etat membre qui exclut de manière générale la possibilité pour une société mère résidente de déduire de son bénéfice imposable des pertes subies dans un autre Etat membre par une filiale établie sur le territoire de celui-ci et dont les bénéfices ne sont ainsi pas soumis à la loi fiscale de l'Etat de la société mère, alors qu'elle accorde une telle possibilité pour des pertes subies par une filiale résidente, y compris dans le cas où la filiale non résidente a épuisé les possibilités de prise en compte des pertes qui existent dans son Etat de résidence ; qu'en effet, la restriction ainsi apportée à la liberté d'établissement est justifiée par la nécessité de préserver la répartition du pouvoir d'imposition entre les Etats membres et ne va pas au-delà de ce qui est nécessaire pour atteindre cet objectif dès lors qu'il n'incombe pas à l'Etat de résidence de la société mère d'assurer la neutralisation de la charge fiscale que la société filiale supporte ou supportera du fait de la décision de l'Etat membre où elle réside d'exercer sa compétence fiscale en limitant le droit d'imputer les pertes subies ; qu'il ne pourrait en aller différemment que lorsque l'impossibilité d'imputer les pertes ne résulte pas de l'application de la législation fiscale de l'Etat membre de résidence de la filiale et, notamment, en cas de liquidation de la filiale ; <br/>
<br/>
      10. Considérant qu'en l'espèce, il résulte de l'instruction que les déficits de la société polonaise Agapes Polska ne pouvaient plus être imputés sur ses résultats en application de la législation polonaise qui exclut le report en arrière des déficits et n'autorise le report en avant que sur les cinq exercices qui suivent celui de leur réalisation, dans la limite de 50 % de leur montant ; que, de même, les déficits de la société italienne Flunch Italie, constatés en 2000 et 2002, ne pouvaient plus être imputés sur ses résultats en application de la législation italienne qui limite le report en avant aux cinq exercices qui suivent celui de leur réalisation ; que, dans ces conditions, et alors qu'il est constant que les sociétés Flunch Italie et Agapes Polska poursuivaient, au cours des exercices en litige, leur activité et restaient soumises à l'impôt sur les bénéfices dans leur pays d'établissement, l'administration fiscale a pu, sans méconnaître les stipulations des articles 43 et 48 précités du traité instituant la Communauté européenne, refuser l'imputation des pertes en cause sur le résultat d'ensemble du groupe fiscalement intégré dont la SOCIETE AGAPES est la mère ;<br/>
      11. Considérant qu'il résulte de tout ce qui précède que la SOCIETE AGAPES n'est pas fondée à soutenir que c'est à tort, que, par le jugement attaqué, le Tribunal administratif de Montreuil a rejeté ses demandes ;<br/>
<br/>
      Sur les conclusions tendant à l'application de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
      12. Considérant que ces dispositions font obstacle à ce que l'Etat qui n'est pas, dans la présente instance, la partie perdante, verse à la SOCIETE AGAPES une somme au titre des frais exposés par elle et non compris dans les dépens ;<br/>
<br/>
      Sur les conclusions tendant à l'application des dispositions de l'article R. 761-1 du code de justice administrative :<br/>
      13. Considérant qu'aux termes de l'article R. 761-1 du code de justice administrative : " Les dépens comprennent les frais d'expertise, d'enquête et de toute autre mesure d'instruction dont les frais ne sont pas à la charge de l 'Etat. / Sous réserve de dispositions particulières, ils sont mis à la charge de toute partie perdante sauf si les circonstances particulières de l'affaire justifient qu'ils soient mis à la charge d'une autre partie ou partagés entre les parties (...) " ;<br/>
<br/>
      14. Considérant qu'aucun dépens n'a été exposé au cours de l'instance d'appel ; que les conclusions présentées par la SOCIETE AGAPES à ce titre ne peuvent donc qu'être rejetées ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
DECIDE :<br/>
       Article 1er : La requête de la SOCIETE AGAPES est rejetée.<br/>
<br/>
''<br/>
''<br/>
''<br/>
''<br/>
10VE04169		2<br/>
<br/>
<br/>
<br/>
<br/>
</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">15-02-01 Communautés européennes et Union européenne. Portée des règles de droit communautaire et de l'Union européenne. Traité de Rome.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">19-04-02-01 Contributions et taxes. Impôts sur les revenus et bénéfices. Revenus et bénéfices imposables - règles particulières. Bénéfices industriels et commerciaux.
</SCT>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
