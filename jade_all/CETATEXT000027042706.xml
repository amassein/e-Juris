<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027042706</ID>
<ANCIEN_ID>JG_L_2013_02_000000336555</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/04/27/CETATEXT000027042706.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère sous-section jugeant seule, 06/02/2013, 336555, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-02-06</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>336555</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère sous-section jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP THOUIN-PALAT, BOUCARD</AVOCATS>
<RAPPORTEUR>M. Rémi Decout-Paolini</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Alexandre Lallet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:336555.20130206</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 11 février 2010 au secrétariat du contentieux du Conseil d'Etat, présentée pour la société d'exploitation des adhésifs, dont le siège est 14, avenue de l'Ile-de-France à Vernon (27200) ; la société d'exploitation des adhésifs demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision du 11 décembre 2009 par laquelle le ministre du travail, des relations sociales, de la famille, de la solidarité et de la ville, le ministre du budget, des comptes publics, de la fonction publique et de la réforme de l'Etat et le ministre de la santé et des sports ont rejeté sa demande du 12 octobre 2009 tendant à l'abrogation du décret n° 85-630 du 19 juin 1985, et la décision implicite par laquelle le Premier ministre a rejeté cette même demande .<br/>
<br/>
              2°) d'enjoindre au Premier ministre d'abroger ce décret ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat le versement de la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
              Vu la note en délibéré, enregistrée le 30 janvier 2013, présentée pour la société d'exploitation des adhésifs ;<br/>
<br/>
              Vu la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
<br/>
              Vu le code de la sécurité sociale ;<br/>
<br/>
              Vu la loi n° 2000-321 du 12 avril 2000 ;<br/>
<br/>
              Vu le décret n° 46-2959 du 31 décembre 1946 ;<br/>
<br/>
              Vu le décret n° 85-630 du 19 juin 1985 ;<br/>
<br/>
              Vu le décret n° 85-1353 du 17 décembre 1985 ;<br/>
<br/>
              Vu le décret n° 96-445 du 22 mai 1996 ;<br/>
	Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Rémi Decout-Paolini, Maître des Requêtes,  <br/>
<br/>
              - les observations de la SCP Thouin-Palat, Boucard, avocat de la société d'exploitation des adhésifs,<br/>
<br/>
              - les conclusions de M. Alexandre Lallet, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Thouin-Palat, Boucard, avocat de la société d'exploitation des adhésifs ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant que la société d'exploitation des adhésifs doit être regardée comme demandant l'annulation pour excès de pouvoir, d'une part, de la décision expresse du 11 décembre 2009 par laquelle le ministre du travail, des relations sociales, de la famille, de la solidarité et de la ville, le ministre du budget, des comptes publics, de la fonction publique et de la réforme de l'Etat, ainsi que le ministre de la santé et des sports, ont rejeté sa demande d'abrogation du décret du 19 juin 1985 en tant qu'il modifie le tableau n° 30 des maladies professionnelles annexé au décret du 31 décembre 1946 portant réglementation d'administration publique pour l'application de la loi du 30 octobre 1946 sur la prévention et la réparation des accidents du travail et des maladies professionnelles et, d'autre part, de la décision implicite par laquelle le Premier ministre a également refusé de faire droit à cette même demande d'abrogation ;<br/>
<br/>
              2. Considérant que, par son article 6, le décret du 17 décembre 1985 relatif au code de la sécurité sociale a abrogé " (...) toutes dispositions antérieurement prises par décret en Conseil d'Etat et qui sont reprises dans le code annexé au présent décret (...) et notamment les dispositions réglementaires suivantes, ainsi que les dispositions qui les ont modifiées : (...) / - le décret n° 46-2959 du 31 décembre 1946, à l'exception des troisième et cinquième alinéas de l'article 126 B et des articles 136 à 139 (...) " ; que le décret du 19 juin 1985 dont la société d'exploitation des adhésifs demande l'abrogation est au nombre des dispositions modificatives du décret du 31 décembre 1946 ; qu'il a dès lors été expressément abrogé par le décret du 17 décembre 1985 ; <br/>
<br/>
              3. Considérant qu'à supposer même que la demande d'abrogation de la société d'exploitation des adhésifs puisse être regardée comme dirigée contre les dispositions du tableau n° 30 des maladies professionnelles issues du décret du 19 juin 1985 et codifiés par le décret du 17 décembre 1985 en tant qu'annexe au livre IV du code de la sécurité sociale, ce tableau a lui-même été remplacé par un nouveau tableau lors de l'entrée en vigueur du décret du 22 mai 1996 modifiant et complétant les tableaux de maladies professionnelles annexés au livre IV du code de la sécurité sociale ;<br/>
<br/>
              4. Considérant qu'il résulte de ce qui précède qu'à la date d'introduction de la requête de la société d'exploitation des adhésifs dirigée contre le refus d'abrogation opposé par le Premier ministre et les ministres compétents, les dispositions litigieuses du décret du 19 juin 1985 avaient déjà fait l'objet d'une abrogation expresse ; que cette requête était dès lors dépourvue d'objet, sans que la requérante puisse utilement se prévaloir des dispositions de l'article 16-1 de la loi du 12 avril 2000 en vertu desquelles l'autorité administrative est tenue " d'abroger expressément tout règlement illégal ou sans objet " ; que la requête de la société d'exploitation des adhésifs est, par suite, irrecevable ; qu'elle doit, sans qu'y fassent obstacle les stipulations de l'article 6-1 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, être rejetée ; <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de la société d'exploitation des adhésifs est rejetée.<br/>
Article 2 : La présente décision sera notifiée à la société d'exploitation des adhésifs, au Premier et au ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
