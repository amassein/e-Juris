<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030983324</ID>
<ANCIEN_ID>JG_L_2015_07_000000368979</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/98/33/CETATEXT000030983324.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème SSJS, 31/07/2015, 368979, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-07-31</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>368979</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème SSJS</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BORE, SALVE DE BRUNETON</AVOCATS>
<RAPPORTEUR>M. Romain Godet</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Edouard Crépey</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2015:368979.20150731</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>	Vu la procédure suivante:<br/>
Procédure contentieuse antérieure :<br/>
	La SAS Autogrill Aéroports a demandé au tribunal administratif de Marseille, d'une part, d'annuler le refus opposé le 14 janvier 2010 par l'administration fiscale à sa demande du 24 juin 2009 tendant à la restitution des sommes correspondant aux montants de la taxe sur la valeur ajoutée qu'elle a dû acquitter au cours des années 2005 et 2006 à raison des dépenses liées aux repas servis gratuitement à son personnel durant cette période sans pouvoir les déduire et, d'autre part, que lui soit accordée la restitution de ces sommes.<br/>
	Par un jugement n° 1001866 du 15 mars 2011, le tribunal administratif de Marseille lui a accordé la restitution de ces sommes.<br/>
	Par un arrêt n° 11MA02920 du 5 avril 2013, la cour administrative d'appel de Marseille a rejeté l'appel formé par le ministre des finances et des comptes publics contre ce jugement en tant que, par son article premier, il a accordé cette restitution à la SAS Autogrill Aéroports.<br/>
Procédure devant le Conseil d'Etat :<br/>
	Par un pourvoi et un mémoire en réplique, enregistrés les 31 mai 2013 et 21 février 2014, le ministre des finances et des comptes publics demande au Conseil d'Etat :<br/>
	1°) d'annuler cet arrêt n° 11MA02920 du 5 avril 2013 de la cour administrative d'appel de Marseille ;<br/>
	2°) réglant l'affaire au fond, de procéder, en application de l'article L.203 du livre des procédures fiscales, à la compensation entre le montant de la taxe sur la valeur ajoutée ayant grevé les denrées utilisées pour les repas fournis gratuitement au personnel et la taxe sur la prestation de service à soi-même que la société était tenue de collecter en application du b du 2 du 8° de l'article 257 du code général des impôts.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la sixième directive 77/388/CEE du 17 mai 1977 ;<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - l'arrêt de la Cour de justice des Communautés européennes du 11 décembre 2008, Danfoss A/S et AstraZeneca A/S , C-371/07 ;<br/>
              - le code de justice administrative.<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Romain Godet, maître des requêtes en service extraordinaire,<br/>
<br/>
              - les conclusions de M. Edouard Crépey, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Boré, Salve de Bruneton, avocat de la société Autogrill aéroports ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que la SAS Autogrill Aéroports, qui exploite des établissements de restauration situés dans l'aéroport de Marseille-Provence, a présenté à l'administration fiscale le 24 juin 2009 une réclamation en vue d'obtenir la restitution des sommes, respectivement, de 2 856 euros et 3 205 euros, correspondant aux montants de la taxe sur la valeur ajoutée qu'elle a dû acquitter au cours des années 2005 et 2006 sans pouvoir les déduire à raison des dépenses liées aux repas servis gratuitement à son  personnel durant cette période. La société s'est prévalue, à l'appui de sa réclamation, des dispositions combinées du c de l'article R. 196-1 et de l'article L. 190 du livre des procédures fiscales dans leur rédaction alors en vigueur, estimant qu'un arrêt de la Cour de justice des Communautés européennes se prononçant sur une question préjudicielle avait révélé la non-conformité des éléments doctrinaux contenus dans le paragraphe 10 de la documentation administrative de base 3A 1221, à jour au 20 octobre 1999, à la sixième directive 77/388/CEE du 17 mai 1977 ;<br/>
<br/>
              2. Après le rejet de sa réclamation, le tribunal administratif de Marseille, saisi par la société, lui a accordé, par l'article premier de son jugement, la restitution des sommes correspondant aux montants de la taxe sur la valeur ajoutée ayant grevé les achats de denrées et de boissons correspondant aux repas fournis à son personnel au titre de ces années. Le ministre des finances et des comptes publics se pourvoit en cassation contre l'arrêt par lequel la cour administrative d'appel de Marseille a rejeté l'appel qu'il avait formé contre l'article premier de ce jugement ;<br/>
<br/>
              3. Il résulte de l'article L. 190 du livre des procédures fiscales dans sa rédaction alors en vigueur, que : " Les réclamations relatives aux impôts, contributions, droits, taxes, redevances, soultes et pénalités de toute nature, établis ou recouvrés par les agents de l'administration, relèvent de la juridiction contentieuse lorsqu'elles tendent à obtenir soit la réparation d'erreurs commises dans l'assiette ou le calcul des impositions, soit le bénéfice d'un droit résultant d'une disposition législative ou réglementaire. / Sont instruites et jugées selon les règles du présent chapitre toutes actions tendant ( ...) à l'exercice de droits à déduction, fondées sur la non-conformité de la règle de droit dont il a été fait application à une règle de droit supérieure (...) ". D'autre part, aux termes de l'article R. 196-1 du même livre dans sa rédaction alors applicable : " Pour être recevables, les réclamations relatives aux impôts autres que les impôts directs locaux et les taxes annexes à ces impôts, doivent être présentées à l'administration au plus tard le 31 décembre de la deuxième année suivant celle, selon le cas : a) De la mise en recouvrement du rôle ou de la notification d'un avis de mise en recouvrement ; b) Du versement de l'impôt contesté lorsque cet impôt n'a pas donné lieu à l'établissement d'un rôle ou à la notification d'un avis de mise en recouvrement ; c) De la réalisation de l'événement qui motive la réclamation. (...) " ;<br/>
<br/>
              4. Une décision de la Cour de justice des Communautés européennes se prononçant sur une question préjudicielle alors même qu'elle révèlerait, par l'interprétation du droit de l'Union qu'elle retient, l'illégalité d'une instruction fiscale ne révèle pas la non-conformité d'une règle de droit dont il a été fait application à une règle de droit supérieure, au sens et pour l'application des dispositions du livre des procédures fiscales citées au point 3, dès lors que l'imposition ne saurait être fondée sur l'interprétation de la loi fiscale que l'administration exprime dans ses instructions ;<br/>
<br/>
              5. En jugeant que la décision  de la Cour de justice des Communautés européennes du 11 décembre 2008, Danfoss A/S et AstraZeneca A/S, C- 371/07, était de nature à constituer la réalisation d'un événement ouvrant, au sens et pour l'application du c de l'article R. 196-1 du livre des procédures fiscales, un délai dans lequel pouvait être présentée une demande tendant au bénéfice d'un droit de restitution de cotisations de taxe sur la valeur ajoutée, la cour a ainsi commis une erreur de droit ;<br/>
<br/>
              6. Il résulte de ce qui précède, et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, que le ministre des finances et des comptes publics est fondé à demander l'annulation de l'arrêt qu'il attaque ;<br/>
<br/>
              7. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mises à ce titre à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Marseille du 5 avril 2013 est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Marseille.<br/>
Article 3 : Les conclusions de la SAS Autogrill Aéroports présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée au ministre des finances et des comptes publics et à la SAS Autogrill Aéroports.<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
