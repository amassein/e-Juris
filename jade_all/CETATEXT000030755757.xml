<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030755757</ID>
<ANCIEN_ID>JG_L_2015_06_000000386593</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/75/57/CETATEXT000030755757.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème SSJS, 19/06/2015, 386593, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-06-19</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>386593</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème SSJS</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Julien Anfruns</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emilie Bokdam-Tognetti</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2015:386593.20150619</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. E... C...a saisi le tribunal administratif de Mayotte d'une protestation contre les opérations électorales qui se sont déroulées les 23 et 30 mars 2014 en vue de la désignation des conseillers municipaux de la commune de Ouangani. Par un jugement n° 1400244 du 30 octobre 2014, le tribunal a rejeté sa protestation.<br/>
<br/>
              Par une requête, enregistrée le 19 décembre 2014 au secrétariat du contentieux du Conseil d'Etat, M. C... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) de faire droit à sa protestation et d'annuler les opérations électorales qui se sont déroulées les 23 et 30 mars 2014 dans la commune de Ouangani ;<br/>
<br/>
              3°) de mettre à la charge de M. D...A...B...la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code électoral ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Julien Anfruns, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Emilie Bokdam-Tognetti, rapporteur public ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Au second tour des élections municipales qui s'est déroulé le 30 mars 2014 dans la commune de Ouangani (Mayotte), la liste " Union pour un mouvement populaire ", conduite par M. D...A...B..., a recueilli 1171 voix, soit 51,81 % des suffrages exprimés, et la liste " Mouvement pour le développement de Mayotte - Union des démocrates et indépendants ", menée par M. E...C..., 1089 voix, soit 48,18 % des suffrages exprimés. M. C... demande l'annulation du jugement du 30 octobre 2014 par lequel le tribunal administratif de Mayotte a rejeté sa protestation dirigée contre cette élection. <br/>
<br/>
              2. Il résulte des dispositions combinées de l'article R. 773-1 du code de justice administrative et des articles R. 119 et R. 120 du code électoral que, par dérogation aux dispositions de l'article R. 611-1 du code de justice administrative, les tribunaux administratifs ne sont pas tenus de communiquer aux auteurs des protestations les mémoires en défense des conseillers municipaux dont l'élection est contestée, non plus que les autres mémoires ultérieurement enregistrés, et qu'il appartient seulement aux parties, si elles le jugent utile, d'en prendre connaissance au greffe du tribunal. Par suite, la circonstance que le mémoire présenté le 19 août 2014 par M. A...B..., qui ne comportait au demeurant pas d'élément nouveau, n'ait pas été communiqué à M. C...par le tribunal administratif de Mayotte n'est pas de nature à entacher d'irrégularité le jugement attaqué. <br/>
<br/>
              3. Si le juge de l'élection n'est pas compétent pour statuer sur la régularité des inscriptions sur les listes électorales, il lui appartient, en revanche, d'apprécier les faits révélant des manoeuvres ou des irrégularités susceptibles d'avoir altéré la sincérité du scrutin. M. C...soutient que plus de 300 électeurs ayant pris part au vote ont été inscrits sur la liste électorale de la commune de Ouangani alors qu'ils sont domiciliés dans une autre commune. Toutefois, les documents produits par le requérant n'établissent pas que figureraient sur la liste électorale de la commune des électeurs qui n'y sont pas domiciliés ni, en tout état de cause, l'existence de manoeuvres à l'occasion de l'établissement de la liste électorale. <br/>
<br/>
              4. M. C...soutient que des électeurs se seraient présentés au bureau de note n° 121 de Kahani munis de plusieurs procurations et auraient ainsi pu voter pour plusieurs mandants en méconnaissance de l'article L. 73 du code électoral. Il n'a cependant produit, au soutien de son moyen, qu'une copie des observations, consignées en ce sens et en des termes généraux par un assesseur titulaire au procès-verbal de ce bureau de vote, qui ne mentionnent ni les noms ni les numéros d'inscription des électeurs dont il s'agit. En l'absence de telles précisions, le moyen ne peut être regardé comme fondé et doit être écarté.<br/>
<br/>
              5. M. C...soutient que l'agent municipal chargé d'inscrire les procurations sur les listes d'émargement après leur réception en mairie a refusé de le faire, en méconnaissance des dispositions de l'article R. 76 du code électoral, s'agissant de procurations reçues le 23 mars 2014. Toutefois, l'attestation de cet agent qu'il a produite au soutien de ce moyen n'est assortie d'aucune précision s'agissant du nombre de procurations concernées, des bureaux de vote en cause et de l'identité des électeurs ayant donné procuration et qui n'auraient pu voter de ce fait. Dès lors, ce moyen ne peut qu'être écarté.<br/>
<br/>
              6. Il résulte de ce qui précède que M. C...n'est pas fondé à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Mayotte a rejeté sa protestation formée contre les opérations électorales qui se sont déroulées les 23 et 30 mars 2014 dans la commune de Ouangani en vue de l'élection des conseillers municipaux.<br/>
<br/>
              7. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mise à ce titre à la charge de M. B... qui n'est pas, dans la présente instance, la partie perdante, la somme que demande M. C...au titre des frais exposés par lui et non compris dans les dépens. Il n'y a pas lieu, dans les circonstances de l'espèce, de mettre à la charge de M. C...la somme que M. B...demande au titre des mêmes dispositions.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : La requête de M. C...est rejetée.<br/>
Article 2 : Les conclusions présentées par M. B...au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La présente décision sera notifiée à M. E... C..., à M. D... A...B....<br/>
Copie en sera adressée au ministre de l'intérieur et à la ministre des outre-mer.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
