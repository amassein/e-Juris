<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026555858</ID>
<ANCIEN_ID>JG_L_2012_10_000000345772</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/55/58/CETATEXT000026555858.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  9ème sous-section jugeant seule, 29/10/2012, 345772, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2012-10-29</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>345772</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 9ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Jean-Pierre Jouguelet</PRESIDENT>
<AVOCATS>SCP CELICE, BLANCPAIN, SOLTNER</AVOCATS>
<RAPPORTEUR>Mme Cécile Isidoro</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Aladjidi</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2012:345772.20121029</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi et le mémoire complémentaire, enregistrés les 14 janvier et 13 avril 2011 au secrétariat du contentieux du Conseil d'Etat, présentés pour la Société nationale d'exploitation industrielle des tabacs et allumettes, dont le siège est 143 boulevard Romain Rolland à Paris (75685) ; la Société nationale d'exploitation industrielle des tabacs et allumettes demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 08NT01189 du 25 novembre 2010 par lequel la cour administrative d'appel de Nantes a rejeté sa requête tendant à l'annulation du jugement n°s 04962 - 063240 du 14 mars 2008 par lequel le tribunal administratif de Rennes a rejeté le surplus de ses demandes tendant à la réduction des cotisations de taxe professionnelle auxquelles elle a été assujettie au titre des années 2001, 2002 et 2003 dans les rôles de la commune de Morlaix ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à ses conclusions d'appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat le versement de la somme de 3 500 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la loi n° 80-495 du 2 juillet 1980 ;<br/>
<br/>
              Vu la loi n  84-603 du 13 juillet 1984 ;<br/>
<br/>
              Vu le code général des impôts et le livre des procédures fiscales ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Cécile Isidoro, Maître des Requêtes,  <br/>
<br/>
              - les observations de la SCP Célice, Blancpain, Soltner, avocat de la société nationale d'exploitation industrielle des tabacs et allumettes,<br/>
<br/>
              - les conclusions de M. Frédéric Aladjidi, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Célice, Blancpain, Soltner, avocat de la société nationale d'exploitation industrielle des tabacs et allumettes ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, par des réclamations contentieuses des 26 décembre 2001, 16 décembre 2002 et 29 décembre 2003, la Société nationale d'exploitation industrielle des tabacs et allumettes a sollicité la réduction des cotisations de taxe professionnelle qu'elle a acquittées au titre des années 2000, 2001 et 2002 à raison de son établissement situé à Morlaix ; qu'elle contestait notamment les modalités de détermination de la valeur locative des immobilisations corporelles entrant dans l'assiette de la taxe ; qu'elle a saisi du litige le tribunal administratif de Rennes qui, par jugement du 14 mars 2008, a rejeté ses demandes ; qu'elle se pourvoit en cassation contre l'arrêt du 25 novembre 2010 par lequel la cour administrative d'appel de Nantes a rejeté sa requête dirigée contre ce jugement ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article 1467 du code général des impôts, dans sa rédaction applicable aux années d'imposition en litige : " La taxe professionnelle a pour base : (...) la valeur locative, telle qu'elle est définie aux articles 1469, 1518 A et 1518 B, des immobilisations corporelles dont le redevable a disposé pour les besoins de son activité professionnelle (...) " ; que selon l'article 1518 B du même code : " A compter du 1er janvier 1980, la valeur locative des immobilisations corporelles acquises à la suite d'apports, de scissions, de fusions de sociétés ou de cessions d'établissements réalisés à partir du 1er janvier 1976 ne peut être inférieure aux deux tiers de la valeur locative retenue l'année précédant l'apport, la scission, la fusion ou la cession (...) " ;<br/>
<br/>
              3. Considérant que la loi du 2 juillet 1980 portant modification du statut du Service d'exploitation industrielle des tabacs et allumettes a créé une société dénommée " Société nationale d'exploitation industrielle des tabacs et allumettes ", régie par la législation sur les sociétés anonymes, destinée à exercer les missions précédemment exercées par l'établissement public à caractère industriel et commercial dénommé " Service d'exploitation industrielle des tabacs et allumettes " (SEITA) ; que l'article 2 de cette loi précise que : " Le patrimoine de l'établissement à caractère industriel et commercial dénommé " Service d'exploitation industrielle des tabacs et allumettes " est apporté à la société créée par la présente loi, selon les modalités fixées par l'autorité compétente " ; que les modalités de cet apport ont été fixées par un arrêté du ministre du budget du 5 septembre 1980 dont l'article 1er prévoit que : " L'apport du patrimoine du Service d'exploitation industrielle des tabacs et allumettes à la société dénommée Société nationale d'exploitation industrielle des tabacs et allumettes est effectué par la reprise, dans les comptes de la société des valeurs actives et passives de l'établissement public, telles que ces valeurs sont enregistrées dans les écritures comptables de celui-ci dans leur intégralité et pour les montants à la date de l'arrêté définitif des derniers comptes " ; qu'il résulte de l'ensemble de ces dispositions que les immobilisations corporelles dont a initialement disposé la Société nationale d'exploitation industrielle des tabacs et allumettes ont été acquises, par cette dernière, par voie d'apport ;<br/>
<br/>
              4. Considérant, en revanche, que si les biens de la Société nationale d'exploitation industrielle des tabacs et allumettes ont été transférés, en vertu de l'article 1er de la loi du 13 juillet 1984, à une nouvelle société du même nom, il résulte de l'ensemble des dispositions de cette loi, éclairées par les travaux parlementaires, qu'elles ont eu pour seul objet d'opérer une substitution entre l'ancienne et la nouvelle société sans entraîner d'autre modification ; qu'ainsi, les immobilisations corporelles transmises à la seconde société ne sauraient être regardées comme ayant été acquises à la suite d'un apport de la première ;<br/>
<br/>
              5. Considérant qu'il résulte de ce qui précède que les dispositions précitées de l'article 1518 B du code général des impôts ont vocation à s'appliquer en ce qui concerne la détermination de la valeur locative des seules immobilisations apportées, en application de la loi du 2 juillet 1980, par l'établissement public à caractère industriel et commercial dénommé " Service d'exploitation industrielle des tabacs et allumettes " à la Société nationale d'exploitation industrielle des tabacs et allumettes ; que la cour administrative d'appel de Nantes a ainsi commis une erreur de droit en jugeant que les dispositions de cet article ne s'appliquaient pas à l'évaluation de la valeur locative de ces immobilisations ; que, par suite, et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, il y a lieu d'annuler son arrêt ;<br/>
<br/>
              6. Considérant qu'il y a lieu, dans l'intérêt d'une bonne administration de la justice, de renvoyer l'affaire à la cour administrative d'appel de Nancy à laquelle a été transmise la requête enregistrée sous le n°334208 par décision du Conseil d'Etat du 27 juillet 2012 qui porte sur un litige de même nature ;<br/>
<br/>
              7. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre une somme de 2 000 euros à la charge de l'Etat en application de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Nantes du 25 novembre 2010 est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Nancy. <br/>
Article 3 : L'Etat versera une somme de 2 000 euros à la Société nationale d'exploitation industrielle des tabacs et allumettes en application de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à la Société nationale d'exploitation industrielle des tabacs et allumettes et au ministre de l'économie et des finances.              <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
