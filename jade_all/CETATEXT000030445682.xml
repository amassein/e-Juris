<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030445682</ID>
<ANCIEN_ID>JG_L_2015_03_000000376653</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/44/56/CETATEXT000030445682.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  8ème sous-section jugeant seule, 27/03/2015, 376653, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-03-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>376653</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 8ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Mathieu Herondart</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Benoît Bohnert</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2015:376653.20150327</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Le syndicat national CGT Finances publiques a demandé au tribunal administratif de Grenoble d'annuler pour excès de pouvoir l'arrêté du 12 décembre 2013 du ministre de l'économie et des finances modifiant l'arrêté du 7 novembre 2012 portant création des services des impôts des particuliers dans les services déconcentrés de la direction générale des finances publiques. <br/>
<br/>
              Par une ordonnance n° 1401193 du 17 mars 2014, la présidente du tribunal administratif de Grenoble a transmis cette requête au Conseil d'Etat, en application de l'article R. 351-2 du code de justice administrative.<br/>
<br/>
              Par cette requête, enregistrée le 19 février 2014 au greffe du tribunal administratif de Grenoble, un mémoire récapitulatif et un mémoire en réplique, enregistrés les 22 avril et 26 août 2014 au secrétariat du contentieux du Conseil d'Etat, le syndicat national CGT Finances publiques demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler pour excès de pouvoir l'arrêté du 12 décembre 2013 modifiant l'arrêté du 7 novembre 2012 portant création de services des impôts des particuliers dans les services déconcentrés de la direction générale des finances publiques ;<br/>
<br/>
              2°) d'enjoindre au ministre des finances et des comptes publics ainsi qu'au directeur général des finances publiques de supprimer le service des impôts des particuliers de Grenoble Grésivaudan, sous astreinte de 300 euros par jour de retard à compter du premier mois suivant la notification de la décision ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 2 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la loi n° 84-16 du 11 janvier 1984 ; <br/>
              - le décret  n° 82-453 du 28 mai 1982 ; <br/>
              - le décret n° 2000-1234 du 18 décembre 2000 ; <br/>
              - le décret n° 2005-850 du 27 juillet 2005 ;<br/>
              - le décret n° 2009-707 du 16 juin 2009 ; <br/>
              - le code de justice administrative ;	<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Mathieu Herondart, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Benoît Bohnert, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant, en premier lieu, qu'aux termes de l'article 1er du décret du 27 juillet 2005 relatif aux délégations de signature des membres du Gouvernement : " A compter du jour suivant la publication au Journal officiel de la République française de l'acte les nommant dans leurs fonctions ou à compter du jour où cet acte prend effet, si ce jour est postérieur, peuvent signer, au nom du ministre ou du secrétaire d'Etat et par délégation, l'ensemble des actes, à l'exception des décrets, relatifs aux affaires des services placés sous leur autorité : 1°) Les (...) directeurs d'administration centrale (...) " ; qu'en sa qualité de directeur général des finances publiques, M. A...avait, en vertu de ces dispositions, qualité pour signer l'arrêté attaqué au nom du ministre de l'économie et des finances ;<br/>
<br/>
              2. Considérant, en deuxième lieu, qu'aux termes de l'article 55 du décret du 28 mai 1982 relatif à l'hygiène et à la sécurité du travail ainsi qu'à la prévention médicale dans la fonction publique : " Le comité d'hygiène, de sécurité et des conditions de travail peut demander au président de faire appel à un expert agréé conformément aux articles R. 4614-6 et suivants du code du travail : (...) / 2° En cas de projet important modifiant les conditions de santé et de sécurité ou les conditions de travail, prévu à l'article 57. (...) / La décision de l'administration refusant de faire appel à un expert doit être substantiellement motivée. Cette décision est communiquée au comité d'hygiène, de sécurité et des conditions de travail ministériel. / En cas de désaccord sérieux et persistant entre le comité et l'autorité administrative sur le recours à l'expert agréé, la procédure prévue à l'article 5-5 peut être mise en oeuvre " ; que l'article 57 du même décret prévoit que : " Le comité est consulté : / 1° Sur les projets d'aménagement importants modifiant les conditions de santé et de sécurité ou les conditions de travail et, notamment, avant toute transformation importante des postes de travail découlant de la modification de l'outillage, d'un changement de produit ou de l'organisation du travail, avant toute modification des cadences et des normes de productivité liées ou non à la rémunération du travail (...) " ; qu'enfin son article 5-5 dispose que : " (...) en cas de désaccord sérieux et persistant entre l'administration et le comité d'hygiène, de sécurité et des conditions de travail, le chef de service compétent ainsi que le comité d'hygiène et de sécurité compétent peuvent solliciter l'intervention de l'inspection du travail. Les inspecteurs santé et sécurité au travail, peuvent également solliciter cette intervention. / Dans le cas d'un désaccord sérieux et persistant, l'inspection du travail n'est saisie que si le recours aux inspecteurs santé et sécurité au travail n'a pas permis de lever le désaccord (...) " ; qu'il résulte de ces dispositions qu'un " projet important " s'entend de tout projet qui affecte de manière déterminante les conditions de santé, de sécurité ou de travail d'un nombre significatif d'agents, le critère du nombre de salariés ne déterminant toutefois pas, à lui seul, l'importance du projet ;<br/>
<br/>
              3. Considérant qu'il résulte de ces dispositions que si l'administration n'est tenue de consulter le comité d'hygiène, de sécurité et des conditions de travail que sur les projets prévus à l'article 57 du décret du 28 mai 1982, elle a toujours la faculté de consulter le comité d'hygiène, de sécurité et des conditions de travail ; que le syndicat requérant n'est, par suite, pas fondé à soutenir que l'administration ne pouvait, sans se contredire, refuser de soumettre le projet de création du service des impôts des particuliers (SIP) de Grenoble Grésivaudan à un expert agréé au motif qu'il ne revêtait pas un caractère d'importance au sens de l'article 57 du décret du 28 mai 1982, alors que la consultation du comité d'hygiène, de sécurité et des conditions de travail sur ce projet suffisait à démontrer son importance ; qu'il ressort des pièces du dossier que l'administration a communiqué au comité d'hygiène, de sécurité et des conditions de travail ministériel sa décision de refuser de recourir à un expert assortie d'un rapport motivé, conformément aux dispositions citées au point 2 ; que si le syndicat soutient que l'administration a méconnu la procédure prévue par les articles 55 et 5-5 du décret du 28 mai 1982 cités au point 2 en s'abstenant de saisir l'inspecteur du travail du différend relatif à la désignation d'un expert agréé, il n'établit ni que le comité d'hygiène, de sécurité et des conditions de travail, ni que les inspecteurs chargés de la santé et de la sécurité au travail ont sollicité l'intervention de l'inspection du travail ; qu'il ressort enfin des pièces du dossier que la création du SIP ne concerne que trois agents qui ne sont appelés, ni à changer de métier, ni à connaître d'évolution substantielle dans leurs pratiques professionnelles mais seulement à se déplacer de 3,8 kilomètres sur la base du volontariat ; qu'ainsi, l'administration n'a pas commis d'erreur d'appréciation en refusant de regarder ce projet comme revêtant un caractère d'importance au sens des dispositions des articles 55 et 57 du décret du 28 mai 1982 ; que, par suite, le moyen tiré de la méconnaissance de la procédure de consultation du comité d'hygiène, de sécurité et des conditions de travail ne peut qu'être écarté ; <br/>
<br/>
              4. Considérant, en troisième lieu, que le syndicat soutient que l'arrêté méconnaît plusieurs dispositions d'une circulaire de 2009 du directeur général des finances publiques, qui fixe les modalités de mise en place et d'organisation des SIP en prévoyant notamment l'intervention d'un ergonome, la consultation des organisations syndicales nationales pour les modalités de création des SIP créés dans des communes dans lesquelles il n'existe pas de trésorerie et de centre des impôts, une implantation à proximité des usagers ainsi que des aménagements en vue d'éviter l'exposition au risque des agents et des mesures indispensables pour garantir la sécurité des agents comme des usagers ; que cette circulaire étant dépourvue de caractère réglementaire, les moyens tirés de la méconnaissance de ses dispositions sont, en tout état de cause, inopérants ; <br/>
<br/>
              5. Considérant, en quatrième lieu, qu'il ne ressort pas des pièces du dossier que l'arrêté litigieux méconnaîtrait les dispositions des articles 1er, 3 et 4 du décret du 18 décembre 2000 déterminant les aménagements des locaux desservis par les personnes physiques ou morales exerçant l'activité de transport de fonds ; <br/>
<br/>
              6. Considérant, enfin, que le moyen tiré de ce que l'administration aurait en créant le SIP de Grenoble Grésivaudan commis une erreur manifeste d'appréciation dès lors que l'accès des contribuables au service public s'en trouverait dégradé n'est, en tout état de cause, pas assorti des précisions permettant d'en apprécier le bien-fondé ; <br/>
<br/>
              7. Considérant qu'il résulte de ce qui précède que le syndicat national CGT Finances publiques n'est pas fondé à demander l'annulation de l'arrêté attaqué ; que ses conclusions à fin d'injonction, ainsi que celles présentées sur le fondement de l'article L. 761-1 du code de justice administrative doivent, par suite, être rejetées ; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              	           D E C I D E :<br/>
                                --------------<br/>
<br/>
Article 1er : La requête du syndicat national CGT Finances publiques est rejetée.<br/>
<br/>
Article 2 : La présente décision sera notifiée au syndicat national CGT Finances publiques et au ministre des finances et des comptes publics.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
