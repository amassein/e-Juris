<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036015067</ID>
<ANCIEN_ID>JG_L_2017_10_000000403764</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/01/50/CETATEXT000036015067.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère chambre, 26/10/2017, 403764, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-10-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>403764</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Dorothée Pradines</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Charles Touboul</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2017:403764.20171026</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés les 24 septembre 2016 et 21 juin 2017 au secrétariat du contentieux du Conseil d'Etat, la Fédération de l'hospitalisation privée - Soins de suite et de réadaptation (FHP-SSR) demande au Conseil d'État :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le paragraphe 4.1 du guide méthodologique de production des informations relatives à l'activité médicale et à sa facturation en soins de suite et de réadaptation, constituant l'annexe II de l'arrêté du 30 juin 2011 modifié relatif au recueil et au traitement des données d'activité médicale des établissements de santé publics ou privés ayant une activité en soins de suite et de réadaptation et à la transmission d'informations issues de ce traitement, publié le 25 juillet 2016 au Bulletin officiel n° 2016/2 bis fascicule spécial du ministère des affaires sociales et de la santé, en tant qu'il distingue pour la facturation des " prestations interactivités " celle des " actes et consultations externes " de celle " des prestations donnant lieu à une admission en hospitalisation " ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de la santé publique ;<br/>
              - le code de la sécurité sociale ;<br/>
              - le décret n° 2014-405 du 16 avril 2014 ;<br/>
              - l'arrêté du 25 février 2016 relatif à la classification et à la prise en charge des prestations d'hospitalisation, des médicaments et des produits et prestations pour les activités de soins de suite ou de réadaptation et les activités de psychiatrie exercées par les établissements mentionnés aux d et e de l'article L. 162-22-6 du code de la sécurité sociale et pris pour l'application de l'article L. 162-22-1 du même code ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Dorothée Pradines, auditeur,  <br/>
<br/>
              - les conclusions de M. Charles Touboul, rapporteur public.<br/>
<br/>
              Vu la note en délibéré, enregistrée le 2 octobre 2017, présentée par la FHP-SSR ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              Sur le cadre juridique et l'objet du litige :<br/>
<br/>
              1. D'une part, il résulte des dispositions combinées du 2° de l'article L. 162-22 et des articles L. 162-22-1 et R. 162-29-1 du code de la sécurité sociale, dans leur rédaction alors applicable, que les frais des activités de soins de suite et de réadaptation, ainsi que de toutes les activités qu'elles recouvrent, sont, dans les établissements de santé privés mentionnés aux d et e de l'article L. 162-22-6 de ce code, c'est-à-dire autres que ceux à but non lucratifs ayant été admis à participer à l'exécution du service public hospitalier ou ayant opté pour la dotation globale de financement, pris en charge par les régimes obligatoires de sécurité sociale sur la base de tarifs journaliers, fixés pour chaque établissement par le directeur général de l'agence régionale de santé. L'article L. 162-22-1 renvoie, pour ces activités, à un décret en Conseil d'Etat la détermination, notamment, en son 1°, des " catégories de prestations d'hospitalisation, sur la base desquelles les ministres chargés de la santé et de la sécurité sociale arrêtent la classification des prestations donnant lieu à une prise en charge par les régimes obligatoires de sécurité sociale ". A ce titre, l'article R. 162-31 du même code, dans sa rédaction alors applicable, prévoit, parmi les catégories de prestations d'hospitalisation donnant lieu à une prise en charge par les régimes obligatoires de la sécurité sociale, " le séjour et les soins avec ou sans hébergement, représentatifs de la mise à disposition des moyens humains, techniques et matériels nécessaires à l'hospitalisation du patient ", dont la prise en charge est dans ces établissements assurée par des forfaits, l'article R. 162-31-1 précisant, dans sa rédaction alors applicable, qu'hormis dans les établissements relevant du régime du prix de journée, " sont exclus de tous les forfaits mentionnés à l'article R. 162-31 (...) et font l'objet d'une rémunération distincte ", notamment, " a) Les honoraires des praticiens et, le cas échéant, les rémunérations des personnels qu'ils prennent en charge directement, y compris les examens de biologie médicale ; / b) Les honoraires des auxiliaires médicaux à l'exception des soins infirmiers ". L'article 2 de l'arrêté du 25 février 2016 relatif à la classification et à la prise en charge des prestations d'hospitalisation, des médicaments et des produits et prestations pour les activités de soins de suite ou de réadaptation et les activités de psychiatrie exercées par les établissements mentionnés aux d et e de l'article L. 162-22-6 du code de la sécurité sociale et pris, ainsi que le prévoit l'article L. 162-22-1 du même code, sur la base des catégories de prestations d'hospitalisation déterminées par l'article R. 162-31 de ce code, précise, s'agissant des modalités de facturation aux régimes obligatoires d'assurance maladie des prestations dites " inter-activités ", que : " (...) 3° Lorsqu'au cours d'un séjour le patient est transféré pour une durée inférieure à deux jours dans un autre établissement, ou au sein du même établissement dans une unité médicale telle que définie aux annexes des arrêtés du 29 juin 2006 et du 30 juin 2011 susvisés, ne relevant pas du même champ d'activité au sens des articles R. 162-29 à R. 162-29-3 du code de la sécurité sociale, les prestations réalisées au cours du transfert sont facturées aux régimes obligatoires d'assurance maladie par l'établissement d'accueil indépendamment de la facturation des prestations réalisées au cours du reste du séjour par l'établissement d'origine, qui interrompt toute facturation liée au séjour durant la période du transfert. / Lorsque ce transfert comporte une nuitée dans l'établissement dans lequel le patient a été transféré, l'établissement d'origine ne facture, le jour du transfert, ni les forfaits prévus à l'article 1er, ni le forfait journalier. Le décompte de journée s'effectue à chaque présence du patient à minuit ".<br/>
<br/>
              2. D'autre part, l'article L. 6113-7 du code de la santé publique dispose que : " Les établissements de santé, publics ou privés, procèdent à l'analyse de leur activité. / Dans le respect du secret médical et des droits des malades, ils mettent en oeuvre des systèmes d'information qui tiennent compte notamment des pathologies et des modes de prise en charge en vue d'améliorer la connaissance et l'évaluation de l'activité et des coûts et de favoriser l'optimisation de l'offre de soins. (...) ". L'article L. 6113-8 du même code précise que : " Les établissements de santé transmettent aux agences régionales de santé, à l'Etat ou à la personne publique qu'il désigne et aux organismes d'assurance maladie les informations relatives à leurs moyens de fonctionnement, à leur activité, à leurs données sanitaires, démographiques et sociales qui sont nécessaires (...) à la détermination de leurs ressources, (...) ainsi qu'au contrôle de leur activité de soins et de leur facturation. (...) ". Aux termes de l'article R. 6113-1 du même code : " Pour l'analyse de leur activité médicale, les établissements de santé, publics et privés, procèdent, dans les conditions fixées par la présente section, à la synthèse et au traitement informatique de données figurant dans le dossier médical mentionné à l'article L. 1112-1 qui sont recueillies, pour chaque patient (...) ". Enfin, en vertu de l'article R. 6113-2 du même code, des arrêtés des ministres chargés de la santé et de la sécurité sociale " déterminent, en fonction de la catégorie de l'établissement dans lequel les soins sont dispensés et de la nature de ces soins : / 1° Les données dont le recueil et le traitement ont un caractère obligatoire ; / 2° Les nomenclatures et classifications à adopter ; (...) ".<br/>
<br/>
              3. Sur le fondement des dispositions mentionnées au point 2, le ministre du travail, de l'emploi et de la santé a, par un arrêté du 30 juin 2011, modifié en dernier lieu, dans sa rédaction alors applicable, par un arrêté du 22 décembre 2015 du ministre des affaires sociales, de la santé et des droits des femmes, défini les conditions de recueil et de traitement des données d'activité médicale des établissements de santé publics et privés ayant une activité en soins de suite et de réadaptation " afin de procéder à l'analyse médico-économique de l'activité de soins réalisée en leur sein ". Le V de son article 1er prévoit que les établissements de santé mentionnés aux d et e de l'article L. 162-22-6 du code de la sécurité sociale doivent transmettre à l'agence régionale de santé, pour chaque séjour d'un patient dans l'établissement, " un ou des résumés de facturation rendus anonymes, (...) dont le contenu est décrit dans l'annexe II du présent arrêté " et précise que " les informations contenues dans les résumés standardisés de facturation anonymes (RSFA) correspondent aux bordereaux de facturation transmis aux organismes d'assurance maladie ". Aux termes de son article 3, dans sa rédaction applicable au litige : " I. (...) / Le guide méthodologique de production des informations relatives à l'activité médicale et à sa facturation en soins de suite et de réadaptation produit en annexe II du présent arrêté précise les modalités de production et de codage des RHS [résumés hebdomadaires standardisés] ". Enfin, le paragraphe 4.1 de ce guide méthodologique constituant l'annexe II de cet arrêté, dans sa rédaction publiée le 25 juillet 2016 au Bulletin officiel du ministère des affaires sociales et de la santé, et dont la fédération demande l'annulation pour excès de pouvoir, dispose que : " On désigne par " prestation inter-activités " une situation dans laquelle une unité d'hospitalisation a recours au plateau technique ou aux équipements d'une autre unité d'hospitalisation relevant d'un champ d'activité différent, pour assurer au patient des soins ou des examens qu'elle ne peut pas effectuer elle-même. / Il convient de distinguer les prestations réalisées à titre externe des prestations donnant lieu à une admission en hospitalisation : / - Les prestations de type " actes et consultations externes " ne doivent pas faire l'objet d'une facturation à l'assurance maladie par le prestataire ; / - Les prestations donnant lieu à une admission en hospitalisation font l'objet d'une facturation à l'assurance maladie par le prestataire. (...) ".<br/>
<br/>
              Sur la légalité des dispositions attaquées du guide méthodologique de production des informations relatives à l'activité médicale et à sa facturation en soins de suite et de réadaptation : <br/>
<br/>
              4. En premier lieu, d'une part, aux termes de l'article L. 162-26 du code de la sécurité sociale, dans sa rédaction alors applicable : " Les consultations et actes externes (...) sont pris en charge par les régimes obligatoires d'assurance maladie dans les conditions prévues aux articles L. 162-1-7 et L. 162-14-1 et dans la limite des tarifs fixés en application de ces articles. (...) / Pour les activités de soins de suite ou de réadaptation et de psychiatrie, la part prise en charge par l'assurance maladie des consultations et actes mentionnés à l'alinéa précédent est incluse dans la dotation annuelle mentionnée à l'article L. 174-1. (...) ". Il résulte de ces dispositions que les consultations et actes externes réalisés au profit d'assurés sociaux hospitalisés en soins de suite et de réadaptation dans des établissements mentionnés aux d et e de l'article L. 162-22-6 du code de la sécurité sociale ne sont pas pris en charge par les régimes obligatoires de sécurité sociale sur la base des tarifs journaliers destinés à couvrir les prestations d'hospitalisation mais sur celle des tarifs définis par les conventions organisant les rapports entre les organismes d'assurance maladie et les professionnels de santé.    <br/>
<br/>
              5. D'autre part, il résulte des dispositions citées au point 1 que l'arrêté du 25 février 2016, pris pour l'application de l'article L. 162-22-1 du code de la sécurité sociale, détermine la classification des seules prestations d'hospitalisation, notamment les séjours et les soins avec ou sans hébergement, sur la base des catégories précisées par l'article R. 162-31, et ne s'applique pas aux consultations et actes externes. Par suite, le 3° de l'article 2 de l'arrêté du 25 février 2016, qui dispose que " les prestations réalisées au cours du transfert sont facturées aux régimes obligatoires d'assurance maladie par l'établissement d'accueil ", porte sur les seules prestations d'hospitalisation et ne régit pas les actes et consultations externes, lesquels sont, ainsi que l'admet en défense le ministre des affaires sociales et de la santé, facturables distinctement aux régimes obligatoires d'assurance maladie, dans les conditions prévues au point 4, par les établissements de soins de suite ou de réadaptation qui ne sont pas financés par dotation annuelle, alors même que ces actes et consultations auraient été effectués dans un autre établissement. <br/>
<br/>
              6. Dès lors, en prévoyant, à son paragraphe 4.1 relatif aux " prestations inter-activités ", que celles de ces prestations qui relèvent des " actes et consultations externes " ne doivent pas faire l'objet d'une facturation à l'assurance maladie par l'établissement prestataire, le guide méthodologique attaqué n'a pas méconnu les dispositions citées au point 1, notamment celles du 3° de l'article 2 de l'arrêté du 25 février 2016. La fédération requérante n'est ainsi pas fondée à soutenir que les dispositions du paragraphe 4.1 de ce guide auraient fixé des règles de traitement des informations non conformes à celles fixées en matière de financement.<br/>
<br/>
              7. En deuxième lieu, les règles de facturation à l'assurance maladie obligatoire des actes et consultations externes n'ont été modifiées, ainsi qu'il a été dit aux points 5 et 6, ni par l'arrêté du 25 février 2016, ni par le guide méthodologique. Les dispositions contestées du point 4.1 de ce guide se bornent ainsi à rappeler, comme il résultait des dispositions déjà en vigueur à la date du 4 janvier 2016 retenue pour l'application du guide, qu'en cas de recours au plateau technique ou aux équipements d'une autre unité d'hospitalisation relevant d'un champ d'activité différent, les actes et consultations externes ne font pas l'objet d'une facturation à l'assurance maladie par le prestataire. Elles ne sont dès lors entachées, contrairement à ce que soutient la fédération requérante, d'aucune rétroactivité.  <br/>
<br/>
              8. En dernier lieu, il résulte de ce qui a été dit ci-dessus que, par les dispositions contestées du guide méthodologique, le ministre des affaires sociales et de la santé s'est borné, pour permettre l'analyse médico-économique de l'activité de soins de suite et de réadaptation des établissements de santé ainsi que son contrôle et celui de sa facturation, à rappeler, sans en méconnaître le sens ou la portée, les règles relatives à la prise en charge de cette activité par les régimes obligatoires de sécurité sociale. Par suite, la fédération requérante n'est pas fondée à soutenir que ces dispositions seraient entachées d'incompétence. <br/>
<br/>
              9. Par suite, et sans qu'il soit besoin de se prononcer sur son intérêt pour agir, contesté par le ministre des affaires sociales et de la santé, la fédération requérante n'est pas fondée à demander l'annulation des dispositions qu'elle attaque du guide méthodologique de production des informations relatives à l'activité médicale et à sa facturation en soins de suite et de réadaptation. <br/>
<br/>
              Sur les frais exposés à l'occasion du litige :<br/>
<br/>
              10. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat, qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de la Fédération hospitalière privée - Soins de suite et de réadaptation est rejetée.<br/>
Article 2 : La présente décision sera notifiée à la Fédération hospitalière privée - Soins de suite et de réadaptation et à la ministre des solidarités et de la santé.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
