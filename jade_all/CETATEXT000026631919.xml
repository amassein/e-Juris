<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026631919</ID>
<ANCIEN_ID>JG_L_2012_11_000000342389</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/63/19/CETATEXT000026631919.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème et 1ère sous-sections réunies, 14/11/2012, 342389</TITRE>
<DATE_DEC>2012-11-14</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>342389</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème et 1ère sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>LE PRADO ; SCP NICOLAY, DE LANOUVELLE, HANNOTIN</AVOCATS>
<RAPPORTEUR>Mme Sophie Roussel</RAPPORTEUR>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CESSR:2012:342389.20121114</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<p>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 11 août et 12 novembre 2010 au secrétariat du contentieux du Conseil d'Etat, présentés pour la commune de Lunel (34400), représentée par son maire ; la commune de Lunel demande au Conseil d'Etat :<br clear="none"/>
<br clear="none"/>
1°) d'annuler le jugement n° 0903227 du 10 juin 2010 par lequel le tribunal administratif de Montpellier a annulé, à la demande de la société APS France Pare-Brise, l'arrêté du 13 juillet 2009 par lequel le maire de Lunel a retiré sa décision tacite de non-opposition à la déclaration préalable de travaux présentée par cette société ;<br clear="none"/>
<br clear="none"/>
2°) réglant l'affaire au fond, de rejeter la demande de la société APS France Pare-Brise ;<br clear="none"/>
<br clear="none"/>
3°) de mettre à la charge de la société APS France Pare-Brise la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br clear="none"/>
<br clear="none"/>
<br clear="none"/>
Vu les autres pièces du dossier ;<br clear="none"/>
<br clear="none"/>
Vu le code de l'urbanisme ;<br clear="none"/>
<br clear="none"/>
Vu le décret n° 2007-18 du 5 janvier 2007 ;<br clear="none"/>
<br clear="none"/>
Vu le code de justice administrative ;<br clear="none"/>
<br clear="none"/>
<br clear="none"/>
Après avoir entendu en séance publique :<br clear="none"/>
<br clear="none"/>
- le rapport de Mme Sophie Roussel, Auditeur,<br clear="none"/>
<br clear="none"/>
- les observations de la SCP Nicolaÿ, de Lanouvelle, Hannotin, avocat de la commune de Lunel et de Me Le Prado, avocat de la société APS France Pare-Brise,<br clear="none"/>
<br clear="none"/>
- les conclusions de M. Xavier de Lesquen, rapporteur public ;<br clear="none"/>
<br clear="none"/>
La parole ayant été à nouveau donnée à la SCP Nicolaÿ, de Lanouvelle, Hannotin, avocat de la commune de Lunel et à Me Le Prado, avocat de la société APS France Pare-Brise ;<br clear="none"/>
<br clear="none"/>
<br clear="none"/>
1. Considérant qu'il ressort des pièces du dossier soumis aux premiers juges que le 20 juillet 2007, la société APS France Pare-Brise a déposé à la mairie de Lunel une déclaration préalable de travaux pour la pose, sur la façade d'un commerce qu'elle exploite, d'un bardage métallique et d'une enseigne publicitaire ; que, par un arrêté du 23 août 2007, le maire de Lunel s'est opposé à cette déclaration de travaux puis, par un arrêté du 19 décembre 2007, a rejeté le recours gracieux formé par la société contre cet arrêté ; que, par un jugement du 11 juin 2009, le tribunal administratif de Montpellier a jugé que, faute pour le maire de s'être opposé aux travaux dans le délai d'un mois qui lui était imparti pour ce faire par l'article L. 422-2 du code de l'urbanisme alors en vigueur, une décision tacite de non-opposition à déclaration préalable de travaux était née le 20 août 2007 et a annulé, d'une part, l'arrêté d'opposition du 23 août 2007, requalifié en décision de retrait de la décision tacite, d'autre part, la décision de rejet du recours gracieux ; que, le 13 juillet 2009, le maire de Lunel a repris un arrêté de retrait de la décision tacite de non-opposition née le 20 août 2007 ; que, par un jugement du 10 juin 2010, contre lequel la commune de Lunel se pourvoit en cassation, le tribunal administratif de Montpellier a annulé l'arrêté du maire de Lunel du 13 juillet 2009 ;<br clear="none"/>
<br clear="none"/>
2. Considérant, d'une part, qu'aux termes du premier alinéa de l'article R. 600-1 du code de l'urbanisme, dans sa rédaction issue du décret du 5 janvier 2007 entrée en vigueur le 1er octobre 2007 : " En cas de déféré du préfet ou de recours contentieux à l'encontre d'un certificat d'urbanisme, d'une décision de non-opposition à une déclaration préalable ou d'un permis de construire, d'aménager ou de démolir, le préfet ou l'auteur du recours est tenu, à peine d'irrecevabilité, de notifier son recours à l'auteur de la décision et au titulaire de l'autorisation. Cette notification doit également être effectuée dans les mêmes conditions en cas de demande tendant à l'annulation ou à la réformation d'une décision juridictionnelle concernant un certificat d'urbanisme, une décision de non-opposition à une déclaration préalable ou un permis de construire, d'aménager ou de démolir. (...) " ;<br clear="none"/>
<br clear="none"/>
3. Considérant, d'autre part, qu'aux termes des deux premiers alinéas de l'article R. 424-15 du même code, issus eux aussi du décret du 5 janvier 2007 : " Mention du permis explicite ou tacite ou de la déclaration préalable doit être affichée sur le terrain, de manière visible de l'extérieur, par les soins de son bénéficiaire, dès la notification de l'arrêté ou dès la date à laquelle le permis tacite ou la décision de non-opposition à la déclaration préalable est acquis et pendant toute la durée du chantier. Cet affichage n'est pas obligatoire pour les déclarations préalables portant sur une coupe ou un abattage d'arbres situés en dehors des secteurs urbanisés. / Cet affichage mentionne également l'obligation, prévue à peine d'irrecevabilité par l'article R. 600-1, de notifier tout recours administratif ou tout recours contentieux à l'auteur de la décision et au bénéficiaire du permis ou de la décision prise sur la déclaration préalable. (...) " ;<br clear="none"/>
<br clear="none"/>
4. Considérant, en premier lieu, qu'à l'issue du jugement du 10 juin 2010 par lequel le tribunal administratif de Montpellier a annulé le retrait par le maire de Lunel de sa décision tacite de non opposition à déclaration préalable de travaux, la société APS France Pare-Brise s'est trouvée rétablie dans le droit à construire qui résultait de la décision originelle ; que les dispositions de la deuxième phrase du premier alinéa de l'article R. 600-1 du code de l'urbanisme citées ci-dessus doivent être regardées comme s'appliquant également au recours exercé contre une décision juridictionnelle dont résulte le rétablissement d'un droit à construire ; qu'il appartenait, dès lors, à la commune de Lunel, dont le pourvoi tend à l'annulation de ce jugement du tribunal administratif de Montpellier, de notifier son recours au bénéficiaire de cette décision, la société APS France Pare-Brise ; que, toutefois, il ne ressort pas des pièces de la procédure que le pourvoi ait été notifié par la commune de Lunel à la société APS France Pare-Brise ;<br clear="none"/>
<br clear="none"/>
5. Considérant, en second lieu, que la commune de Lunel soutient que l'absence d'accomplissement des formalités de notification requises par l'article R. 600-1 du code de l'urbanisme ne peut lui être opposée dès lors qu'il n'a pas été fait mention de cette obligation par un affichage sur le terrain postérieurement au jugement du 11 juin 2009, ainsi que le prescrit, depuis le 1er octobre 2007, le deuxième alinéa de l'article R. 424-15 du même code ; que, toutefois, les obligations d'affichage prévues par l'article R. 424-15 du code de l'urbanisme sont destinées à informer les tiers et non l'auteur de la décision ou le bénéficiaire de la décision prise sur la déclaration préalable ; que, par suite, la commune de Lunel, qui est l'auteur de la décision de non-opposition dont le retrait a été par la suite annulé, ne peut se prévaloir de la méconnaissance des obligations d'affichage qui résultent des dispositions de l'article R. 424-15 du code de l'urbanisme ;<br clear="none"/>
<br clear="none"/>
6. Considérant qu'il résulte de ce qui précède que le pourvoi de la commune de Lunel n'est pas recevable ; que les conclusions de la commune présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative ne peuvent, par suite, qu'être rejetées ; qu'en revanche, il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la commune de Lunel la somme de 3 000 euros à verser à la société APS France Pare-Brise, au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br clear="none"/>
<br clear="none"/>
<br clear="none"/>
<br clear="none"/>
<br clear="none"/>
D E C I D E :<br clear="none"/>
--------------<br clear="none"/>
Article 1er: Le pourvoi de la commune de Lunel est rejeté.<br clear="none"/>
Article 2 : La commune de Lunel versera à la société APS France Pare-Brise la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br clear="none"/>
Article 3 : La présente décision sera notifiée à la commune de Lunel et à la société APS France Pare-Brise.</p>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">68-06-01-04 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. RÈGLES DE PROCÉDURE CONTENTIEUSE SPÉCIALES. INTRODUCTION DE L'INSTANCE. OBLIGATION DE NOTIFICATION DU RECOURS. - 1) CHAMP D'APPLICATION - RECOURS CONTRE UNE DÉCISION JURIDICTIONNELLE AYANT ANNULÉ LE RETRAIT D'UNE DÉCISION DE NON-OPPOSITION À DÉCLARATION PRÉALABLE DE TRAVAUX - INCLUSION - 2) ABSENCE D'ACCOMPLISSEMENT DES FORMALITÉS DE PUBLICITÉ PRÉVUES À L'ARTICLE R. 424-15 DU CODE DE L'URBANISME - POSSIBILITÉ, POUR L'AUTEUR DE LA DÉCISION D'URBANISME, DE S'EN PRÉVALOIR - ABSENCE.
</SCT>
<ANA ID="9A"> 68-06-01-04 1) L'obligation de notification d'une demande tendant à l'annulation ou à la réformation d'une décision juridictionnelle concernant une décision de non-opposition à une déclaration préalable (article R. 600-1 du code de l'urbanisme dans sa rédaction issue du décret n° 2007-18 du 5 janvier 2007) s'applique au cas d'un recours dirigé contre une décision juridictionnelle ayant annulé le retrait d'une décision de non-opposition à déclaration préalable de travaux. 2) Lorsque l'auteur d'une décision d'urbanisme forme un recours soumis à l'obligation de notification posée par l'article R. 600-1 du code de l'urbanisme, il ne peut se prévaloir, pour justifier de l'absence d'accomplissement de cette formalité, du défaut d'accomplissement des obligations d'affichage prescrites par le deuxième alinéa de l'article R. 424-15 du code de l'urbanisme dans sa rédaction issue du même décret, ces formalités étant destinées à informer les tiers et non l'auteur de la décision d'urbanisme.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>



</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
