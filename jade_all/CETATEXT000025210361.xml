<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000025210361</ID>
<ANCIEN_ID>JG_L_2012_01_000000346689</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/25/21/03/CETATEXT000025210361.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème et 2ème sous-sections réunies, 23/01/2012, 346689</TITRE>
<DATE_DEC>2012-01-23</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>346689</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème et 2ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Jacques Arrighi de Casanova</PRESIDENT>
<AVOCATS>SCP GASCHIGNARD</AVOCATS>
<RAPPORTEUR>M. Fabrice Aubert</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Boulouis</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2012:346689.20120123</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 14 février et 16 mai 2011 au secrétariat du contentieux du Conseil d'Etat, présentés pour Mlle Joëlle A, demeurant au ... ; Mlle A demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 09BX02674 du 22 juin 2010 par lequel la cour administrative d'appel de Bordeaux a rejeté sa requête tendant à l'annulation du jugement n° 0404328 du 28 juillet 2009 par lequel le tribunal administratif de Toulouse a rejeté sa demande tendant à la condamnation de l'Etat à l'indemniser du préjudice causé par diverses décisions du recteur de l'académie de Toulouse l'ayant contrainte à renoncer au bénéfice de sa réussite au concours externe de secrétaire d'administration scolaire et universitaire ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 4 janvier 2012, présentée pour Mlle A ;<br/>
<br/>
              Vu la loi n° 84-16 du 11 janvier 1984 ;<br/>
<br/>
              Vu le décret n° 94-1017 du 18 novembre 1994 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Fabrice Aubert, Auditeur,  <br/>
<br/>
              - les observations de la SCP Gaschignard, avocat de Mlle A, <br/>
<br/>
              - les conclusions de M. Nicolas Boulouis, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Gaschignard, avocat de Mlle A ; <br/>
<br/>
<br/>
<br/>Considérant que la personne qui a demandé au tribunal administratif la réparation des conséquences dommageables d'un fait qu'elle impute à une administration est recevable à détailler ces conséquences devant le juge d'appel, en invoquant le cas échéant des chefs de préjudice dont elle n'avait pas fait état devant les premiers juges, dès lors qu'ils se rattachent au même fait générateur et que ses prétentions demeurent dans la limite du montant total de l'indemnité chiffrée en première instance, augmentée le cas échéant des éléments nouveaux apparus postérieurement au jugement, sous réserve des règles qui gouvernent la recevabilité des demandes fondées sur une cause juridique nouvelle ; qu'il en va ainsi même lorsque le requérant n'a spécifié aucun chef de préjudice précis devant les premiers juges ;<br/>
<br/>
              Considérant qu'il ressort des pièces du dossier soumis au juge du fond que Mlle A, adjointe administrative du ministère de l'éducation nationale, a réussi le concours externe de secrétaire d'administration scolaire et universitaire, organisé conjointement par le ministère de l'éducation nationale et le ministère de l'agriculture ; qu'à la suite de son admission, Mlle A a été nommée secrétaire d'administration scolaire et universitaire stagiaire dans un lycée agricole, alors qu'elle avait émis le souhait d'être affectée dans un établissement dépendant de son ministère d'origine ; qu'afin de rester affectée au ministère de l'éducation nationale, Mlle A a présenté sa démission et demandé sa réintégration en qualité d'adjointe administrative, renonçant ainsi au bénéfice de sa réussite au concours ; <br/>
<br/>
              Considérant que, pour rejeter, par l'arrêt attaqué, l'appel de la requérante contre le jugement du tribunal administratif de Toulouse rejetant sa demande tendant à l'indemnisation du préjudice subi du fait de cette renonciation, qu'elle impute à la mauvaise gestion de son dossier par le rectorat, la cour administrative d'appel de Bordeaux a regardé comme nouvelles en appel les conclusions de Mlle A tendant à l'indemnisation de son préjudice moral et de son préjudice de carrière, au motif que la requérante n'avait identifié aucun élément de préjudice spécifique devant les premiers juges ; qu'il résulte de ce qui a été dit <br/>
ci-dessus que la cour a, ce faisant, commis une erreur de droit ; que, sans qu'il soit besoin d'examiner l'autre moyen du pourvoi, son arrêt du 22 juin 2010 doit dès lors être annulé ; <br/>
<br/>
              Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative ; <br/>
<br/>
              Considérant qu'il résulte des dispositions de l'article 4 du décret du 18 novembre 1994 fixant les dispositions statutaires communes applicables aux corps des secrétaires administratifs des administrations de l'Etat et de certains corps analogues, que les concours externes et internes d'accès à ces corps peuvent être organisés conjointement par plusieurs ministères ; qu'en 2000, le ministère de l'éducation nationale et le ministère de l'agriculture ont ainsi organisé un concours commun pour le recrutement de secrétaires d'administration scolaire et universitaire ; que les candidats ont été invités à indiquer, lors des épreuves d'admission, s'ils entendaient concourir pour les postes ouverts par un seul de ces ministères ou pour les deux ; <br/>
<br/>
              Considérant qu'il résulte de l'instruction que Mlle A, qui s'était portée candidate à ce concours, a fait le choix de se présenter aux épreuves pour l'ensemble des postes ouverts, mais qu'aucun des six voeux de postes formulés par l'intéressée à la suite de sa réussite au concours ne pouvait plus être satisfait, à l'issue du mouvement des titulaires, dans le cadre de la procédure d'affectation des candidats admis ; que, par suite, l'administration pouvait la nommer indifféremment dans un établissement dépendant soit du ministère de l'éducation nationale soit du ministère de l'agriculture, sans que l'intéressée puisse utilement se prévaloir d'un droit à choisir elle-même son affectation ; que Mlle A ne peut dès lors se prévaloir d'aucune faute de l'administration susceptible d'engager la responsabilité de l'Etat à son égard ; qu'elle n'est ainsi pas fondée à se plaindre de ce que le tribunal administratif de Toulouse a rejeté sa demande indemnitaire ; que ses conclusions subsidiaires tendant à ce qu'il soit enjoint au ministre de l'éducation nationale de réaliser des simulations de traitement afin d'obtenir une évaluation du préjudice qu'elle estime avoir subi ne peuvent qu'être également rejetées par voie de conséquence ;<br/>
<br/>
              Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mis à la charge de l'Etat, qui n'est pas, dans la présente instance, la partie perdante, le versement à Mlle A de la somme qu'elle demande au titre des frais exposés par elle devant le Conseil d'Etat et devant la cour administrative d'appel de Bordeaux et non compris dans les dépens ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 22 juin 2010 de la cour administrative d'appel de Bordeaux est annulé.<br/>
Article 2 : La requête présentée par Mlle A devant la cour administrative d'appel de Bordeaux et le surplus des conclusions de son pourvoi sont rejetés.<br/>
Article 3 : La présente décision sera notifiée à Mlle Joëlle A et au ministre de l'éducation nationale, de la jeunesse et de la vie associative.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-07-01-03 PROCÉDURE. POUVOIRS ET DEVOIRS DU JUGE. QUESTIONS GÉNÉRALES. CONCLUSIONS. - ACTION INDEMNITAIRE - CHIFFRAGE DU PRÉJUDICE - INVOCATION POUR LA PREMIÈRE FOIS DE NOUVEAUX CHEFS DE PRÉJUDICE DEVANT LE JUGE D'APPEL - FACULTÉ - CONDITIONS [RJ1] - CAS DANS LEQUEL LE REQUÉRANT N'A SPÉCIFIÉ AUCUN CHEF DE PRÉJUDICE PRÉCIS DEVANT LE PREMIER JUGE - INCLUSION.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">60-04-03 RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. RÉPARATION. ÉVALUATION DU PRÉJUDICE. - INVOCATION POUR LA PREMIÈRE FOIS DE NOUVEAUX CHEFS DE PRÉJUDICE DEVANT LE JUGE D'APPEL - FACULTÉ - CONDITIONS [RJ1] - CAS DANS LEQUEL LE REQUÉRANT N'A SPÉCIFIÉ AUCUN CHEF DE PRÉJUDICE PRÉCIS DEVANT LE PREMIER JUGE - INCLUSION.
</SCT>
<ANA ID="9A"> 54-07-01-03 La personne qui a demandé au tribunal administratif la réparation des conséquences dommageables d'un fait qu'elle impute à une administration est recevable à détailler ces conséquences devant le juge d'appel, en invoquant le cas échéant des chefs de préjudice dont elle n'avait pas fait état devant les premiers juges, dès lors qu'ils se rattachent au même fait générateur et que ses prétentions demeurent dans la limite du montant total de l'indemnité chiffrée en première instance, augmentée le cas échéant des éléments nouveaux apparus postérieurement au jugement, sous réserve des règles qui gouvernent la recevabilité des demandes fondées sur une cause juridique nouvelle.,,Il en va ainsi même lorsque le requérant n'a spécifié aucun chef de préjudice précis devant les premiers juges.</ANA>
<ANA ID="9B"> 60-04-03 La personne qui a demandé au tribunal administratif la réparation des conséquences dommageables d'un fait qu'elle impute à une administration est recevable à détailler ces conséquences devant le juge d'appel, en invoquant le cas échéant des chefs de préjudice dont elle n'avait pas fait état devant les premiers juges, dès lors qu'ils se rattachent au même fait générateur et que ses prétentions demeurent dans la limite du montant total de l'indemnité chiffrée en première instance, augmentée le cas échéant des éléments nouveaux apparus postérieurement au jugement, sous réserve des règles qui gouvernent la recevabilité des demandes fondées sur une cause juridique nouvelle.,,Il en va ainsi même lorsque le requérant n'a spécifié aucun chef de préjudice précis devant les premiers juges.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 31 mai 2007, Herbeth, n° 278905, p. 225.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
