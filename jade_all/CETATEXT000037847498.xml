<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037847498</ID>
<ANCIEN_ID>JG_L_2018_12_000000416201</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/84/74/CETATEXT000037847498.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème chambre, 21/12/2018, 416201, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-12-21</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>416201</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROCHETEAU, UZAN-SARANO ; SCP MATUCHANSKY, POUPOT, VALDELIEVRE</AVOCATS>
<RAPPORTEUR>M. Laurent Domingo</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Romain Victor</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:416201.20181221</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société anonyme (SA) L'Immobilière patrimoine et la société civile immobilière (SCI) Les Chalets du Soleil ont demandé au tribunal administratif de Marseille de condamner la commune de Saint-Etienne-en-Dévoluy à leur restituer une somme perçue à titre de participation financière aux coûts d'équipements publics réalisés sur le territoire de la commune. Par un jugement n° 1104041 du 15 juin 2015, ce tribunal a fait droit à cette demande.<br/>
<br/>
              Par un arrêt n° 15MA03538 du 28 septembre 2017, la cour administrative d'appel de Marseille a, sur appel de la commune du Dévoluy, issue de la fusion de Saint-Etienne-en-Dévoluy et de trois autres communes, annulé ce jugement et rejeté la demande des deux sociétés. <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés le 4 décembre 2017 et le 1er mars 2018 au secrétariat du contentieux du Conseil d'Etat, la SA L'Immobilière patrimoine et la SCI Les Chalets du Soleil demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de la commune du Dévoluy ;<br/>
<br/>
              3°) de mettre à la charge de la commune du Dévoluy la somme de 4 500 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'urbanisme ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Laurent Domingo, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Romain Victor, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Rocheteau, Uzan-Sarano, avocat de la société L'immobilière Patrimoine et de la société Les Chalets du Soleil et à la SCP Matuchansky, Poupot, Valdelièvre, avocat de la commune du Dévoluy.<br/>
<br/>
              Vu la note en délibéré, enregistrée le 11 décembre 2018, présentée par la commune du Dévoluy.<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1.	Il ressort des pièces du dossier soumis aux juges du fond que la SCI Les chalets du soleil, ayant pour gérante la SA L'Immobilière patrimoine, s'est acquittée, entre les mois de novembre 2006 et de juillet 2008, de sommes mises à sa charge par des titres exécutoires émis pour le compte de la commune de Saint-Etienne-en-Dévoluy, correspondant à la participation pour voirie et réseaux alors prévue aux articles L. 332-11-1 et suivants du code de l'urbanisme, à raison de l'aménagement d'une unité touristique nouvelle dans le cadre duquel, après que la SA L'Immobilière patrimoine eut conclu une convention avec la commune le 13 février 2006, la SCI Les chalets du soleil a obtenu un permis de construire délivré par le maire de la commune le 19 avril 2006. Les deux sociétés ont ultérieurement demandé à la commune, le 8 juin 2011, la répétition des sommes versées à hauteur de 575 473,44 euros et saisi le lendemain d'une requête en ce sens le tribunal administratif de Marseille, qui y a fait droit par un jugement du 15 juin 2015. Les sociétés requérantes se pourvoient en cassation contre l'arrêt du 28 septembre 2017 par lequel la cour administrative d'appel de Marseille, sur l'appel de la commune du Dévoluy, a annulé ce jugement et rejeté leur demande de première instance.<br/>
<br/>
              2.	Aux termes de l'article L. 332-6 du code de l'urbanisme, dans sa rédaction applicable aux participations en litige : " Les bénéficiaires d'autorisations de construire ne peuvent être tenus que des obligations suivantes : / (...) 2° Le versement des contributions aux dépenses d'équipements publics mentionnées à l'article L. 332-6-1. (...) ". Aux termes de ce dernier article, dans sa version alors en vigueur : " Les contributions aux dépenses d'équipements publics prévues au 2° de l'article L. 332-6 sont les suivantes : / (...) 2° (...) / d) La participation pour voirie et réseaux prévue à l'article L. 332-11-1 ". Aux termes de ce dernier article, dans sa version alors en vigueur : " Le conseil municipal peut instituer une participation pour voirie et réseaux en vue de financer en tout ou en partie la construction des voies nouvelles ou l'aménagement des voies existantes ainsi que l'établissement ou l'adaptation des réseaux qui leur sont associés, lorsque ces travaux sont réalisés pour permettre l'implantation de nouvelles constructions. / Pour chaque voie, le conseil municipal précise les études, les acquisitions foncières et les travaux à prendre en compte pour le calcul de la participation, compte tenu de l'équipement de la voie prévu à terme ". Aux termes de l'article L. 332-28 du même code, dans sa rédaction alors en vigueur : " Les contributions mentionnées ou prévues au 2° de l'article L. 332-6-1 (...) sont prescrites, selon le cas, par l'autorisation de construire, l'autorisation de lotir, l'autorisation d'aménager un terrain destiné à l'accueil d'habitations légères de loisir ou l'acte approuvant un plan de remembrement. Cette autorisation ou cet acte en constitue le fait générateur. Il en fixe le montant, la superficie s'il s'agit d'un apport de terrains ou les caractéristiques générales (...) ". Enfin, aux termes de l'article L. 332-30 du même code, dans sa version alors en vigueur : " Les taxes et contributions de toute nature qui sont obtenues ou imposées en violation des dispositions des articles L. 311-4 et L. 332-6 sont réputées sans cause ; les sommes versées ou celles qui correspondent au coût de prestations fournies sont sujettes à répétition. L'action en répétition se prescrit par cinq ans à compter du dernier versement ou de l'obtention des prestations indûment exigées ".<br/>
<br/>
              3.	Il résulte de ces dispositions, notamment celles précitées de l'article L. 332-28 du code de l'urbanisme, que la participation qu'un conseil municipal peut instituer pour le financement de voies nouvelles ou des réseaux réalisés pour permettre l'implantation d'une nouvelle construction ne peut légalement être mise à la charge du pétitionnaire que par l'acte autorisant l'opération de construction, de lotissement ou d'aménagement ou approuvant le plan de remembrement. Dès lors, les sociétés requérantes sont fondées à soutenir qu'en jugeant qu'elles étaient redevables de la participation pour voirie et réseaux à hauteur de la somme en litige, alors qu'il ressortait des pièces du dossier qui lui était soumis que l'autorisation de construire du 19 avril 2006 ne mentionnait pas cette participation, la cour administrative d'appel de Marseille a commis une erreur de droit. Il suit de là que les sociétés requérantes sont fondées, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, à demander l'annulation de l'arrêt qu'elles attaquent.<br/>
<br/>
              4.	Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la commune du Dévoluy la somme de 3 000 euros, à verser solidairement aux sociétés requérantes, au titre de l'article L. 761-1 du code de justice administrative. Cet article fait en revanche obstacle à ce qu'une somme soit mise à ce titre à la charge des sociétés requérantes qui ne sont pas, dans la présente espèce, la partie perdante. <br/>
<br/>
<br/>
<br/>                           D E C I D E :<br/>
                                         --------------<br/>
<br/>
Article 1er : L'arrêt du 28 septembre 2017 de la cour administrative d'appel de Marseille est annulé.<br/>
<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Marseille.<br/>
<br/>
Article 3 : La commune du Dévoluy versera solidairement à la SA L'Immobilière Patrimoine et à la SCI Les Chalets du Soleil la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : Les conclusions présentées par la commune du Dévoluy sur le fondement de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 5 : La présente décision sera notifiée à la SA L'Immobilière Patrimoine, à la SCI Les Chalets du Soleil et à la commune du Dévoluy.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
