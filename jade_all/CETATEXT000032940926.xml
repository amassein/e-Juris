<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032940926</ID>
<ANCIEN_ID>JG_L_2016_07_000000387472</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/94/09/CETATEXT000032940926.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère chambre, 27/07/2016, 387472, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-07-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>387472</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GASCHIGNARD</AVOCATS>
<RAPPORTEUR>M. Frédéric Pacoud</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Rémi Decout-Paolini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2016:387472.20160727</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête sommaire et un mémoire complémentaire, enregistrés les 27 janvier et 28 avril 2015 au secrétariat du contentieux du Conseil d'Etat, la Fondation pour l'école demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la circulaire du délégué général à l'emploi et à la formation professionnelle du 14 novembre 2014 relative à l'élaboration des listes des formations technologiques et professionnelles initiales et organismes et services éligibles à la fraction " hors quota " de la taxe d'apprentissage ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la Constitution ;<br/>
              - le code du travail ;<br/>
              - la décision du 22 juillet 2015 par laquelle le Conseil d'Etat, statuant au contentieux, a renvoyé au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée par la Fondation pour l'école ;<br/>
              - la décision n° 2015-496 QPC du 21 octobre 2015 par laquelle le Conseil constitutionnel a statué sur cette question prioritaire de constitutionnalité ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Frédéric Pacoud, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Rémi Decout-Paolini, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Gaschignard, avocat de la Fondation pour l'école ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. En vertu de l'article L. 6241-8 du code du travail, les employeurs peuvent bénéficier d'une exonération totale ou partielle de la taxe d'apprentissage à raison des dépenses réellement exposées afin de favoriser des formations technologiques et professionnelles qui, dispensées dans le cadre de la formation initiale hors du cadre de l'apprentissage, conduisent à des diplômes ou titres enregistrés au répertoire national des certifications professionnelles et classés dans la nomenclature interministérielle des niveaux de formation. La loi du 5 mars 2014 relative à la formation professionnelle, à l'emploi et à la démocratie sociale a notamment restreint les catégories d'établissements habilités à percevoir la part de la taxe d'apprentissage, dite " hors quota ", correspondant à de telles dépenses. A ce titre, l'article L. 6241-9 du code du travail énumère : " 1° Les établissements publics d'enseignement du second degré ; / 2° Les établissements privés d'enseignement du second degré sous contrat d'association avec l'Etat, mentionnés à l'article L. 442-5 du code de l'éducation et à l'article L. 813-1 du code rural et de la pêche maritime ; / 3° Les établissements publics d'enseignement supérieur ; / 4° Les établissements gérés par une chambre consulaire ; / 5° Les établissements privés relevant de l'enseignement supérieur gérés par des organismes à but non lucratif ; / 6° Les établissements publics ou privés dispensant des formations conduisant aux diplômes professionnels délivrés par les ministères chargés de la santé, des affaires sociales, de la jeunesse et des sports ". L'article L. 6241-10 du même code prévoit que, par dérogation à l'article L. 6241-9, certains établissements, organismes et services peuvent également bénéficier de cette part de la taxe d'apprentissage, dans la limite d'un plafond fixé par voie réglementaire. Enfin, le même article précise que, chaque année, un arrêté du préfet de région fixe la liste de ces organismes et services ainsi que celle des formations dispensées par les établissements mentionnés à l'article L. 6241-9, implantés dans la région, susceptibles de bénéficier des dépenses libératoires.<br/>
<br/>
              2. Par une circulaire du 14 novembre 2014, dont la Fondation pour l'école demande l'annulation pour excès de pouvoir, le délégué général à l'emploi et à la formation professionnelle a fait connaître aux préfets de région, par des dispositions impératives à caractère général, l'interprétation qu'il convenait de retenir de ces dispositions, à la suite de leur modification par la loi du 5 mars 2014, ainsi que les modalités selon lesquelles les listes régionales mentionnées à l'article L. 6241-10 devaient être élaborées.<br/>
<br/>
              Sur les conclusions dirigées contre les dispositions de la circulaire relatives aux seuls établissements de l'enseignement supérieur :<br/>
<br/>
              3. Il ressort de ses statuts que l'action de la Fondation pour l'école vise à favoriser le développement d'établissements d'enseignement primaire et secondaire n'ayant pas passé un contrat avec l'Etat. En cette qualité, elle ne justifie pas d'un intérêt lui donnant qualité pour agir contre celles des dispositions de la circulaire attaquée qui concernent uniquement les établissements relevant de l'enseignement supérieur et, notamment, qui précisent la façon dont le caractère non lucratif d'un organisme gestionnaire d'un établissement privé de l'enseignement supérieur doit être apprécié. Par suite, le ministre chargé du travail est fondé à soutenir que sa requête n'est pas recevable en tant qu'elle demande l'annulation de la circulaire dans cette mesure.<br/>
<br/>
              Sur les conclusions dirigées contre les autres dispositions de la circulaire attaquée :<br/>
<br/>
              4. Par sa décision n° 2015-496 QPC du 21 octobre 2015, le Conseil constitutionnel a déclaré conforme à la Constitution l'article L. 6241-9 du code du travail, déterminant les catégories d'établissements habilités à percevoir la part de la taxe d'apprentissage dite " hors quota ". Par suite, la fondation requérante n'est pas fondée à soutenir que la circulaire attaquée du 14 novembre 2014 réitèrerait des dispositions portant atteinte aux droits et libertés garantis par la Constitution. <br/>
<br/>
              5. Il résulte de ce qui précède que la requête de la fondation requérante doit être rejetée, y compris ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de la Fondation pour l'école est rejetée.<br/>
Article 2 : La présente décision sera notifiée à la Fondation pour l'école et à la ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
