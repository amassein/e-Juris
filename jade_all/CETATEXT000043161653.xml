<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043161653</ID>
<ANCIEN_ID>JG_L_2021_02_000000449491</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/16/16/CETATEXT000043161653.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 17/02/2021, 449491, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-02-17</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>449491</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2021:449491.20210217</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et un nouveau mémoire, enregistrés les 8 et 16 février 2021 au secrétariat du contentieux du Conseil d'Etat, M. B... A... demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative : <br/>
<br/>
              1°) d'annuler l'alinéa 1 du I de l'article 57-2 du décret n° 2020-1262 du 16 octobre 2020 tel que modifié par le décret n° 2021-99 du 30 janvier 2021 ; <br/>
<br/>
              2°) de prendre les mesures urgentes et nécessaires pour faire lever les limitations à la liberté de circuler mises en place par les dispositions précitées et autoriser les citoyens français, leurs conjoints et leurs enfants à entrer en France et à en sortir librement, en respectant les protocoles sanitaires et les mesures de quarantaine en vigueur.  <br/>
<br/>
<br/>
              Il soutient que :<br/>
              - la condition d'urgence est satisfaite ;<br/>
              - les dispositions litigieuses portent une atteinte grave et manifestement illégale à la liberté de circulation ;<br/>
              - le Gouvernement ne disposait d'aucun texte, et notamment pas de l'article L. 3131-15 du code de la santé publique, de la compétence pour prendre ces mesures ;<br/>
              - elles ne respectent pas les principes de stricte nécessité, d'adaptation et de proportionnalité dès lors que les mesures de contrôles, de tests et de quarantaine déjà mises en place sont suffisantes pour s'assurer du ralentissement de la transmission du virus et que ni le conseil scientifique, ni la Commission nationale consultative des droits de l'homme, ni le Conseil d'Etat dans son avis sur le projet de loi de prorogation de l'état d'urgence sanitaire ne les recommandaient. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la Constitution ; <br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - le code de la santé publique ;<br/>
              - le décret n° 2021-99 du 30 janvier 2021 ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. (...) ". En vertu de l'article L. 522-3 du même code, le juge des référés peut, par une ordonnance motivée, rejeter une requête sans instruction ni audience lorsque la condition d'urgence n'est pas remplie ou lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable ou qu'elle est mal fondée.<br/>
<br/>
              2. La circonstance qu'une atteinte à une liberté fondamentale, portée par une mesure administrative, serait avérée, n'est pas à elle seule de nature à caractériser l'existence d'une situation d'urgence justifiant l'intervention du juge des référés dans le très bref délai prévu par les dispositions de l'article L. 521-2 du code de justice administrative. Il appartient au juge des référés d'apprécier, au vu des éléments que lui soumet le requérant comme de l'ensemble des circonstances de l'espèce, si la condition d'urgence particulière requise par l'article L. 521-2 est satisfaite, en prenant en compte la situation du requérant et les intérêts qu'il entend défendre mais aussi l'intérêt public qui s'attache à l'exécution des mesures prises par l'administration.<br/>
<br/>
              Sur le cadre juridique du litige : <br/>
<br/>
              3. Aux termes de l'article L. 3131-12 du code de la santé publique, issu de la loi du 23 mars 2020 d'urgence pour faire face à l'épidémie de Covid-19 : " L'état d'urgence sanitaire peut être déclaré sur tout ou partie du territoire métropolitain ainsi que du territoire des collectivités régies par les articles 73 et 74 de la Constitution et de la Nouvelle-Calédonie en cas de catastrophe sanitaire mettant en péril, par sa nature et sa gravité, la santé de la population ". L'article L. 3131-13 du même code précise que " L'état d'urgence sanitaire est déclaré par décret en conseil des ministres pris sur le rapport du ministre chargé de la santé. Ce décret motivé détermine la ou les circonscriptions territoriales à l'intérieur desquelles il entre en vigueur et reçoit application. Les données scientifiques disponibles sur la situation sanitaire qui ont motivé la décision sont rendues publiques. / (...) ". Aux termes de l'article L. 3131-15 du même code : " Dans les circonscriptions territoriales où l'état d'urgence sanitaire est déclaré, le Premier ministre peut, par décret réglementaire pris sur le rapport du ministre chargé de la santé, aux seules fins de garantir la santé publique : / (...) 1° Réglementer ou interdire la circulation des personnes (...). " Ces mesures " sont strictement proportionnées aux risques sanitaires encourus et appropriées aux circonstances de temps et de lieu. Il y est mis fin sans délai lorsqu'elles ne sont plus nécessaires. "<br/>
<br/>
              4. Par un décret du 14 octobre dernier, pris sur le fondement des articles L. 3131-12 et L. 3131-13 du code de la santé publique, le Président de la République a déclaré l'état d'urgence sanitaire à compter du 17 octobre sur l'ensemble du territoire national. Les 16 et 29 octobre 2020, le Premier ministre a pris, sur le fondement de l'article L. 3131-15 du code de la santé publique, deux décrets prescrivant les mesures générales nécessaires pour faire face à l'épidémie de Covid-19 dans le cadre de l'état d'urgence sanitaire. Le décret du 30 janvier 2021 modifiant les décrets des 16 et 29 octobre 2020, soumet l'entrée sur le territoire métropolitain des ressortissants français présents dans " un pays étranger autre que ceux de l'Union européenne, Andorre, l'Islande, le Liechtenstein, Monaco, la Norvège, Saint-Marin, le Saint-Siège ou la Suisse " à la justification d'un " motif impérieux d'ordre personnel ou familial, un motif de santé relevant de l'urgence ou un motif professionnel ne pouvant être différé ".<br/>
<br/>
              Sur les demandes présentées par M. A... :<br/>
<br/>
              5. M. A... demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, d'une part, d'annuler l'alinéa 1 du I de l'article 57-2 du décret n° 2020-1262 du 16 octobre 2020 tel que modifié par le décret n° 2021-99 du 30 janvier 2021 et, d'autre part, de prendre sur le fondement de l'article L. 521-2 du code de justice administrative les mesures urgentes et nécessaires pour faire lever les limitations au droit des ressortissants français d'entrer sur le territoire national.<br/>
<br/>
              6. En premier lieu, pour le cas où l'ensemble des conditions posées par l'article L. 521-2 du code de justice administrative sont remplies, le juge des référés peut prescrire toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale. Toutefois, de telles mesures doivent, ainsi que l'impose l'article L. 511-1 du même code, présenter un caractère provisoire. Il suit de là que le juge des référés ne peut, sans excéder sa compétence, prononcer l'annulation d'une décision administrative. Par suite, les conclusions à fin d'annulation présentées dans le cadre de l'instance en référé sont manifestement irrecevables.<br/>
<br/>
              7. En second lieu, pour justifier de l'urgence à ce qu'il soit fait droit aux conclusions de sa demande en référé, M. A..., citoyen français résidant au Liban, soutient que les dispositions litigieuses portent un préjudice grave et une entrave à la liberté fondamentale de circuler, sans compter les dommages, affectifs, moraux et psychiques. Si le droit d'entrer sur le territoire français constitue, pour un ressortissant français, une liberté fondamentale au sens de l'article L. 521-2 du code de justice administrative, M. A... ne fait en rien état de l'impact de cette mesure sur sa situation personnelle, ni ne soutient même avoir de projet de déplacement en France à brève échéance. Dans ces conditions, il n'établit pas l'urgence caractérisée justifiant seule qu'il soit ordonné à très bref délai, sur le fondement de l'article L. 521-2 du code de justice administrative, une mesure de sauvegarde d'une liberté fondamentale. Sa demande ne peut, par suite, qu'être rejetée. <br/>
<br/>
              8. Il résulte de tout ce qui précède que la requête de M. A... doit être rejetée, selon la procédure prévue à l'article L. 522-3 du code de justice administrative. <br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
              ------------------<br/>
<br/>
Article 1er : La requête de M. A... est rejetée. <br/>
Article 2 : La présente ordonnance sera notifiée à M. B... A....<br/>
Copie en sera adressée au ministre des solidarités et de la santé.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
