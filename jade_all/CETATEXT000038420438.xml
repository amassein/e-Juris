<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038420438</ID>
<ANCIEN_ID>JG_L_2019_04_000000408992</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/42/04/CETATEXT000038420438.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème - 9ème chambres réunies, 24/04/2019, 408992</TITRE>
<DATE_DEC>2019-04-24</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>408992</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème - 9ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BARADUC, DUHAMEL, RAMEIX</AVOCATS>
<RAPPORTEUR>M. Raphaël Chambon</RAPPORTEUR>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CECHR:2019:408992.20190424</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A...B...a demandé à la Cour nationale du droit d'asile d'annuler la décision du 31 octobre 2014 par laquelle l'Office français de protection des réfugiés et apatrides a rejeté sa demande d'admission au bénéfice de l'asile, ou, à défaut, de la protection subsidiaire.<br/>
<br/>
              Par une décision n° 15005532 du 25 octobre 2016, la Cour nationale du droit d'asile a rejeté son recours.<br/>
<br/>
              Par un pourvoi, enregistré le 17 mars 2017 au secrétariat du contentieux du Conseil d'Etat, M. B... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette décision ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative et de l'article 37 de la loi du 10 juillet 1991.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ; <br/>
<br/>
              Vu :  <br/>
              - la convention de Genève du 28 juillet 1951 relative aux réfugiés et le protocole signé à New-York le 31 janvier 1967 relatif au statut des réfugiés ;<br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - le code de justice administrative ; <br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Raphaël Chambon, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Anne Iljic, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Baraduc, Duhamel, Rameix, avocat de M. A...B...;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis à la Cour nationale du droit d'asile que M.B..., qui est d'origine palestinienne et qui réside en Cisjordanie, a sollicité la reconnaissance de la qualité de réfugié, qui lui a été refusée par une décision de l'Office de protection des réfugiés et apatrides du 31 octobre 2014 au motif que les craintes personnelles dont il faisait état en cas de retour en Cisjordanie n'étaient pas fondées. Il se pourvoit en cassation contre la décision du 25 octobre 2016 par laquelle la Cour nationale du droit d'asile a rejeté son recours contre cette décision. <br/>
<br/>
              2. Aux termes du 2° du A de l'article 1er de la convention de Genève, la qualité de réfugié est reconnue à " toute personne qui, craignant avec raison d'être persécutée du fait de sa race, de sa religion, de sa nationalité ou de son appartenance à un certain groupe social ou de ses opinions politiques, se trouve hors du pays dont elle a la nationalité et qui ne peut ou, du fait de cette crainte, ne veut se réclamer de la protection de ce pays ". Aux termes de l'article L. 712-3 du code de l'entrée et du séjour des étrangers et du droit d'asile : " Sous réserve des dispositions de l'article L. 712-2, le bénéfice de la protection subsidiaire est accordé à toute personne qui ne remplit pas les conditions pour se voir reconnaître la qualité de réfugié mentionnées à l'article L. 711-1 et qui établit qu'elle est exposée dans son pays à l'une des menaces graves suivantes : a) La peine de mort ; b) la torture ou des peines ou traitements inhumains ou dégradants ; c) S'agissant d'un civil, une menace grave, directe ou individuelle contre sa vie ou sa personne en raison d'une violence généralisée résultant d'une situation de conflit armé interne ou international (...) ". Selon l'article L. 713-2 du même code: " Les persécutions prises en compte dans l'octroi de la qualité de réfugié et les menaces graves pouvant donner lieu au bénéfice de la protection subsidiaire peuvent être le fait des autorités de l'Etat, de partis ou d'organisations qui contrôlent l'Etat ou une partie substantielle du territoire de l'Etat, ou d'acteurs non étatiques dans les cas où les autorités définies à l'alinéa suivant refusent ou ne sont pas en mesure d'offrir une protection. Les autorités susceptibles d'offrir une protection peuvent être les autorités de l'Etat et des organisations internationales et régionales ". Pour l'application de ces dispositions, une demande d'admission au statut de réfugié présentée par une personne qui réside sur un territoire délimité par des frontières à l'intérieur desquelles une ou plusieurs autorités exercent effectivement les prérogatives liées au pouvoir, même sans inclure la possibilité de conférer la nationalité, doit être examinée au regard des persécutions dont il est allégué que cette autorité ou l'une de ces autorités serait l'auteur.<br/>
<br/>
              3. Il ressort des pièces du dossier soumis à la Cour nationale du droit d'asile que la demande de M. B...se fondait sur ses craintes de persécution de la part de l'armée israélienne en raison de ses activités politiques et que l'intéressé faisait notamment état de photographies qu'il avait prises de soldats interpellant des enfants palestiniens et d'actions de collage d'affiches à caractère politique. <br/>
<br/>
              4. Il ressort des énonciations non contestées de la décision attaquée que, pour déterminer l'autorité à l'égard de laquelle les craintes du requérant devaient être examinées, la Cour s'est fondée sur l'accord intérimaire israélo-palestinien " Oslo II " signé en septembre 1995. Aux termes de cet accord, la ville de Qalqilya, où M. B...résidait habituellement, fait partie de la zone A de la Cisjordanie. Si la Cour nationale du droit d'asile a relevé que, dans cette zone, " tous les pouvoirs et responsabilités du domaine civil ont été transférés à l'Autorité palestinienne ", qui est notamment " responsable de la sécurité interne et de l'ordre public ", elle a omis de relever que le même accord prévoit également que, dans la zone A de la Cisjordanie, " Israël restera responsable de la défense extérieure, (...), tout comme de la sécurité globale des Israéliens et des colonies, aux fins de protéger la sécurité intérieure et l'ordre public, et [qu'] à cette fin, elle aura tout pouvoir pour prendre les mesures qu'elle jugera nécessaire ". Il s'ensuit qu'en jugeant que les craintes de M. B...devaient être examinées en prenant en compte, comme autorité exerçant effectivement les prérogatives liées au pouvoir, la seule Autorité palestinienne, alors que l'accord intérimaire Oslo II confie aussi, dans la zone A de la Cisjordanie, des prérogatives liées au pouvoir à Israël, la Cour nationale du droit d'asile a entaché sa décision d'erreur de droit. <br/>
<br/>
              5. Il résulte de ce qui précède que, sans qu'il soit besoin de se prononcer sur les autres moyens du pourvoi, M. B...est fondé à demander l'annulation de la décision qu'il attaque.  <br/>
<br/>
              6. M. B...a obtenu le bénéfice de l'aide juridictionnelle. Il s'ensuit que son avocat peut se prévaloir des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991. Il y a lieu, dans les circonstances de l'espèce, et sous réserve que la SCP Baraduc, Duhamel, Rameix renonce à percevoir la somme correspondant à la part contributive de l'Etat, de mettre à la charge de l'Etat le versement à cette SCP de la somme de 3 000 euros.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : La décision du 25 octobre 2016 de la Cour nationale du droit d'asile est annulée.<br/>
Article 2 : L'affaire est renvoyée à la Cour nationale du droit d'asile.<br/>
Article 3 : L'Etat versera à la SCP Baraduc, Duhamel, Rameix, avocat de M.B..., une somme de 3 000 euros au titre du deuxième alinéa de l'article 37 de la loi du 10 juillet 1991 et de l'article L. 761-1 du code de justice administrative, sous réserve qu'elle renonce à percevoir la somme correspondant à la part contributive de l'Etat.<br/>
Article 4 : La présente décision sera notifiée à M. A...B...et à l'Office français de protection des réfugiés et apatrides.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">095-03-02-01 - TERRITOIRE DÉLIMITÉ PAR DES FRONTIÈRES À L'INTÉRIEUR DESQUELLES UNE OU PLUSIEURS AUTORITÉS EXERCENT EFFECTIVEMENT LES PRÉROGATIVES LIÉES AU POUVOIR - APPRÉCIATION DES PERSÉCUTIONS AU REGARD DE CETTE OU DE L'UNE DE CES AUTORITÉS [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">095-03-02-02-02 - TERRITOIRE DÉLIMITÉ PAR DES FRONTIÈRES À L'INTÉRIEUR DESQUELLES UNE OU PLUSIEURS AUTORITÉS EXERCENT EFFECTIVEMENT LES PRÉROGATIVES LIÉES AU POUVOIR - APPRÉCIATION DES PERSÉCUTIONS AU REGARD DE CETTE OU DE L'UNE DE CES AUTORITÉS [RJ1].
</SCT>
<ANA ID="9A"> 095-03-02-01 Une demande d'admission au statut de réfugié présentée par une personne qui réside sur un territoire délimité par des frontières à l'intérieur desquelles une ou plusieurs autorités exercent effectivement les prérogatives liées au pouvoir, même sans inclure la possibilité de conférer la nationalité, doit être examinée au regard des persécutions dont il est allégué que cette autorité ou l'une de ces autorités serait l'auteur.</ANA>
<ANA ID="9B"> 095-03-02-02-02 Une demande d'admission au statut de réfugié présentée par une personne qui réside sur un territoire délimité par des frontières à l'intérieur desquelles une ou plusieurs autorités exercent effectivement les prérogatives liées au pouvoir, même sans inclure la possibilité de conférer la nationalité, doit être examinée au regard des persécutions dont il est allégué que cette autorité ou l'une de ces autorités serait l'auteur.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 5 novembre 2014,,, n° 363181, T. p. 524.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
