<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032629942</ID>
<ANCIEN_ID>JG_L_2014_12_000000385792</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/62/99/CETATEXT000032629942.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 05/12/2014, 385792, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-12-05</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>385792</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2014:385792.20141205</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 18 novembre 2014 au secrétariat du contentieux du Conseil d'Etat, présentée par la commune de Baisieux, représentée par son maire ; la commune requérante demande au juge des référés du Conseil d'Etat :<br/>
<br/>
              1°) d'ordonner, sur le fondement de l'article L. 123-16 du code de l'environnement, la suspension du décret du 25 juillet 2014 portant classement parmi les sites du département du Nord de l'ensemble formé par le champ de bataille de Bouvines et ses abords, en ce qu'il intègre dans le périmètre des parcelles de la zone ZH et ZK stiuées au nord de l'autoroute A27 ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat le versement de la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              elle soutient que : <br/>
              - la requête est recevable ; <br/>
              - la date de départ du délai de recours contentieux qui doit être prise en compte est celle de la notification du décret adressée par le préfet à la commune et reçue en mairie le 30 octobre 2014 ; <br/>
              - le commissaire enquêteur a émis une réserve suffisamment importante pour valoir " avis défavorable " au sens de l'article L. 123-16 du code de l'environnement ;<br/>
              - il existe un doute sérieux quant à la légalité du décret contesté ;<br/>
              - le décret contesté est entaché de plusieurs erreurs manifestes d'appréciation ;<br/>
<br/>
<br/>
              Vu le décret dont la suspension de l'exécution est demandée ;<br/>
              Vu la copie de la requête à fin d'annulation de ce décret ; <br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de l'environnement ;<br/>
<br/>
              Vu le code de l'urbanisme ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'aux termes de l'article L. 554-12 du code de justice administrative : " La décision de suspension d'une décision d'aménagement soumise à une enquête publique préalable obéit aux règles définie par l'article L. 123-16 du code de l'environnement " ; qu'aux termes de l'article L. 123-16 du code de l'environnement : " Le juge administratif des référés, saisi d'une demande de suspension d'une décision prise après des conclusions défavorables du commissaire-enquêteur ou de la commission d'enquête, fait droit à cette demande si elle comporte un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de celle-ci " ; qu'en vertu de l'article L. 522-1 du code de justice administrative, le juge des référés statue, en audience publique, au terme d'une procédure contradictoire écrite ou orale ; qu'enfin, aux termes de l'article L. 522-3 : " Lorsque la demande ne présente pas un caractère d'urgence ou lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable ou qu'elle est mal fondée, le juge des référés peut la rejeter par une ordonnance motivée sans qu'il y ait lieu d'appliquer les deux premiers alinéas de l'article L. 522-1. " ; qu'il résulte de la combinaison de ces dispositions que, si les demandes de suspension de l'exécution d'une décision d'aménagement soumise à une enquête publique préalable présentées sur le fondement de l'article L. 554-12 du code de justice administrative doivent, sans qu'il y ait lieu de rechercher si la condition tenant à l'urgence est ou non remplie, être accueillies par le juge des référés, lorsque la décision a été prise après conclusions défavorables du commissaire enquêteur et qu'est soulevé un moyen de nature à faire naître un doute sérieux quant à sa légalité, elles obéissent pour le surplus, et notamment pour leur instruction, leur jugement et les voies de recours, au régime de droit commun des demandes de suspension de l'exécution des décisions administratives ; qu'en particulier, le juge des référés a la faculté de rejeter la demande selon la procédure fixée par l'article L. 522-3 du code de justice administrative ; <br/>
<br/>
              2. Considérant qu'aux termes de l'article R. 341-6 du code de l'environnement : " La décision de classement fait l'objet d'une publication au Journal officiel " ; qu'aux termes de l'article R. 341-7 du même code : " Lorsque la décision de classement comporte des prescriptions particulières tendant à modifier l'état ou l'utilisation des lieux, elle doit être notifiée au propriétaire./ Cette notification s'accompagne de la mise en demeure d'avoir à mettre les lieux en conformité avec ces prescriptions particulières prévues par les dispositions du troisième alinéa de l'article L. 341-6. " ; <br/>
              3. Considérant qu'en vertu de ces dispositions, les décisions portant classement d'un monument naturel ou d'un site sont publiées au Journal officiel ; que si, d'après l'article R. 341-7 du code de l'environnement, ces décisions sont notifiées aux propriétaires intéressés lorsqu'elles comportent des prescriptions particulières tendant à modifier l'état ou l'utilisation des lieux et si, dans ce cas, le délai de recours contentieux ne court qu'à partir de la notification du décret ou de l'arrêté de classement, cette dernière disposition n'est applicable que s'il y a lieu de mettre le propriétaire en demeure de modifier l'état ou l'utilisation des lieux ; qu'en revanche, dans les autres cas, le délai de recours contentieux court dès la publication de la décision au Journal officiel même si, postérieurement à cette publication la décision a été notifiée au propriétaire ; <br/>
              4. Considérant qu'il ressort des pièces du dossier soumis au juge des référés, que le décret contesté, qui prononce le classement au titre des sites du département du Nord, de l'ensemble formé par le champ de bataille de Bouvines et de ses abords, a été publié au Journal officiel le 26 juillet 2014 ; que ce décret ne comportait aucune mise en demeure aux propriétaires de modifier l'état ou l'utilisation des lieux ; que la circonstance que le préfet du Nord ait notifié le décret de classement au maire de la commune de Baisieux en lui demandant de reporter, en application de l'article R. 341-8  du code de l'urbanisme, la décision de classement sur le document d'urbanisme applicable dans la commune ne constitue pas une mise en demeure adressée à un propriétaire de modifier l'état ou l'utilisation des lieux ; qu'il suit de là que, conformément aux dispositions réglementaires rappelées ci-dessus, le délai de recours contentieux contre ledit décret a couru, y compris à l'égard de la commune de Baisieux, à compter de sa publication au Journal officiel ; <br/>
              5. Considérant qu'il ressort des pièces et des écritures soumis au juge des référés que la commune de Baisieux n'a déposé une requête en annulation qu'au mois de novembre 2014 ; que, par suite, sa demande tendant à la suspension du décret de classement ne peut qu'être rejetée, y compris ses conclusions tendant à l'application de l'article L. 761-1 du code de justice administrative, selon la procédure prévue à l'article L. 522-3 du même code ;<br/>
<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de la commune de Baisieux est rejetée.<br/>
<br/>
Article 2 : La présente ordonnance sera notifiée à la commune de Baisieux. <br/>
Copie en sera adressée au Premier ministre et à la ministre de l'écologie, du développement durable et de l'énergie.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
