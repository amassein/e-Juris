<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030755660</ID>
<ANCIEN_ID>JG_L_2015_06_000000369558</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/75/56/CETATEXT000030755660.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Section, 19/06/2015, 369558, Publié au recueil Lebon</TITRE>
<DATE_DEC>2015-06-19</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>369558</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Section</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP NICOLAY, DE LANOUVELLE, HANNOTIN ; SCP COUTARD, MUNIER-APAIRE</AVOCATS>
<RAPPORTEUR>Mme Esther de Moustier</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Nathalie Escaut</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESEC:2015:369558.20150619</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              La société immobilière du port de Boulogne (SIPB) a demandé au tribunal administratif de Lille de condamner la chambre de commerce et d'industrie de Boulogne-sur-Mer Côte d'Opale à lui verser la somme de 1 587 768,12 euros en réparation du préjudice subi du fait de la résiliation du sous-traité de concession d'outillage public n° 712 du 3 février 1995. Par un jugement n° 0907813 du 5 juillet 2012, le tribunal administratif a rejeté cette demande.<br/>
<br/>
              Par un arrêt n° 12DA01360 du 2 mai 2013, la cour administrative d'appel de Douai a rejeté l'appel formé par la SIPB contre ce jugement.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire, un mémoire en réplique et deux nouveaux mémoires, enregistrés les 21 juin et 4 septembre 2013, le 30 juin 2014 et les 12 mars et 3 juin 2015 au secrétariat du contentieux du Conseil d'Etat, la SIPB demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ; <br/>
<br/>
              3°) de mettre à la charge de la chambre de commerce et d'industrie de Boulogne-sur-Mer Côte d'Opale le versement d'une somme de 10 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Esther de Moustier, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Nathalie Escaut, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Nicolaÿ, de Lanouvelle, Hannotin, avocat de la SIPB SAS et à la SCP Coutard, Munier-Apaire, avocat de la chambre de commerce et d'industrie de  Boulogne-sur-Mer Côte d'Opale ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que la société immobilière du port de Boulogne (SIPB) donnait en location un entrepôt dit " hangar D10 " qu'elle avait édifié sur le terre-plein central du môle Ouest de la darse Sarraz-Bournet dans le port de Boulogne-sur-Mer ; qu'en 2008, elle a informé la chambre de commerce et d'industrie de Boulogne-sur-Mer Côte d'Opale, gestionnaire du port, de son intention de reprendre directement la gestion de cet entrepôt ; que celle-ci lui a répondu qu'elle occupait irrégulièrement le domaine public portuaire et a contesté sa qualité de propriétaire de l'entrepôt, puis a conclu une convention d'occupation de ce hangar avec une autre société ; que le tribunal administratif de Lille, par un jugement du 5 juillet 2012, a refusé de faire droit aux conclusions indemnitaires présentées sur le terrain de la responsabilité contractuelle par la SIPB qui soutenait que la chambre de commerce et d'industrie devait être regardée comme ayant illégalement prononcé la résiliation unilatérale d'une convention d'occupation du domaine public portuaire qui les aurait liées jusqu'en 2016 ; que la cour administrative d'appel de Douai a, par un arrêt du 2 mai 2013 contre lequel la SIPB se pourvoit en cassation, confirmé ce jugement et rejeté comme irrecevables ses conclusions tendant à obtenir la réparation du préjudice qu'elle estime avoir subi sur les terrains quasi-contractuel et quasi-délictuel ; <br/>
<br/>
              Sur l'arrêt en tant qu'il porte sur la responsabilité contractuelle : <br/>
<br/>
              2. Considérant que nul ne peut, sans disposer d'un titre l'y habilitant, occuper une dépendance du domaine public ; qu'eu égard aux exigences qui découlent tant de l'affectation normale du domaine public que des impératifs de protection et de bonne gestion de ce domaine, l'existence de relations contractuelles en autorisant l'occupation privative ne peut se déduire de sa seule occupation effective, même si celle-ci a été tolérée par l'autorité gestionnaire et a donné lieu au versement de redevances domaniales ; qu'en conséquence, une convention d'occupation du domaine public ne peut être tacite et doit revêtir un caractère écrit ; <br/>
<br/>
              3. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond qu'un projet de convention d'occupation du domaine public portuaire  a été élaboré par la SIPB et la chambre de commerce et d'industrie mais n'a pas été signé par les parties en raison d'un désaccord portant sur le montant de la redevance due à raison de cette occupation ; que la chambre de commerce et d'industrie a toutefois toléré la présence de la société sur son domaine public pendant plus de dix ans tout en percevant les redevances correspondantes, dont elle avait arrêté le montant pour les années 1994 à 1999 dans une lettre du 3 février 1995 ; que la cour administrative d'appel de Douai n'a pas commis d'erreur de droit en jugeant que ces éléments, sur lesquels elle a porté une appréciation souveraine exempte de dénaturation, n'étaient pas de nature à caractériser l'existence d'une convention habilitant la SIPB à occuper le domaine public portuaire et qu'en conséquence, la société requérante ne pouvait invoquer la faute contractuelle qu'aurait commise l'établissement public en prononçant la résiliation de la convention d'occupation dont elle se prévalait ; <br/>
<br/>
              Sur l'arrêt en tant qu'il porte sur la responsabilité extra contractuelle : <br/>
<br/>
              4. Considérant, toutefois, que lorsque le juge, saisi d'un litige engagé sur le terrain de la responsabilité contractuelle, est conduit à constater, le cas échéant d'office, l'absence ou la nullité du contrat, les parties qui s'estimaient liées par ce contrat peuvent poursuivre le litige qui les oppose en invoquant, y compris pour la première fois en appel, des moyens tirés de l'enrichissement sans cause que l'application du contrat par lequel elles s'estimaient liées a apporté à l'une d'elles ou de la faute consistant, pour l'une d'elles, à avoir induit l'autre partie en erreur sur l'existence de relations contractuelles ou à avoir passé un contrat nul, bien que ces moyens, qui ne sont pas d'ordre public, reposent sur des causes juridiques nouvelles ;<br/>
<br/>
              5. Considérant qu'il résulte de ce qui précède qu'en relevant que la SIPB, dont la demande devant le tribunal administratif tendait exclusivement à mettre en jeu la responsabilité contractuelle de la chambre de commerce et d'industrie, n'était pas recevable à invoquer, dans le délai de recours contentieux, pour la première fois devant elle à l'appui de sa requête, d'une part, l'enrichissement sans cause de l'établissement public, d'autre part, la faute qu'il aurait commise, en lui laissant croire, sans signer la convention, qu'elle pouvait occuper le domaine public, la cour administrative d'appel a commis une erreur de droit ; que la SIPB est, par suite, fondée à demander l'annulation de l'arrêt attaqué ;<br/>
<br/>
              Sur les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              6. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la chambre de commerce et d'industrie de Boulogne-sur-Mer Côte d'Opale la somme de 3 000 euros à verser à la SIPB au titre de ces dispositions ; que ces dispositions font en revanche obstacle à ce qu'une somme soit mise à la charge de la SIPB qui n'est pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : L'arrêt du 2 mai 2013 de la cour administrative d'appel de Douai est annulé.<br/>
<br/>
      Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Douai.<br/>
<br/>
Article 3 : La chambre de commerce et d'industrie de Boulogne-sur-Mer Côte d'Opale versera à la SIPB une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : Les conclusions de la chambre de commerce et d'industrie de Boulogne-sur-Mer Côte d'Opale présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 5 : La présente décision sera notifiée à la société immobilière du port de Boulogne et à la chambre de commerce et d'industrie de Boulogne-sur-Mer Côte d'Opale.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">24-01-02-01-01-02 DOMAINE. DOMAINE PUBLIC. RÉGIME. OCCUPATION. UTILISATIONS PRIVATIVES DU DOMAINE. CONTRATS ET CONCESSIONS. - CONVENTION NE POUVANT ÊTRE TACITE ET REVÊTANT OBLIGATOIREMENT UN CARACTÈRE ÉCRIT.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">39-01-01 MARCHÉS ET CONTRATS ADMINISTRATIFS. NOTION DE CONTRAT ADMINISTRATIF. EXISTENCE D'UN CONTRAT. - 1) CONVENTION D'OCCUPATION DU DOMAINE PUBLIC - CONVENTION NE POUVANT ÊTRE TACITE ET REVÊTANT OBLIGATOIREMENT UN CARACTÈRE ÉCRIT - 2) CONSTAT DE L'ABSENCE OU DE LA NULLITÉ DU CONTRAT ALORS QUE LE LITIGE A ÉTÉ ENGAGÉ SUR LE TERRAIN DE LA RESPONSABILITÉ CONTRACTUELLE - MOYENS TIRÉS DE L'ENRICHISSEMENT SANS CAUSE OU DE LA FAUTE À AVOIR INDUIT L'AUTRE PARTIE EN ERREUR SUR L'EXISTENCE DE RELATIONS CONTRACTUELLES OU À AVOIR PASSÉ UN CONTRAT NUL - POSSIBILITÉ D'INVOQUER CES MOYENS, Y COMPRIS POUR LA PREMIÈRE FOIS EN APPEL - EXISTENCE [RJ1].
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">39-02 MARCHÉS ET CONTRATS ADMINISTRATIFS. FORMATION DES CONTRATS ET MARCHÉS. - CONVENTION D'OCCUPATION DU DOMAINE PUBLIC - CONVENTION NE POUVANT ÊTRE TACITE ET REVÊTANT OBLIGATOIREMENT UN CARACTÈRE ÉCRIT.
</SCT>
<SCT ID="8D" TYPE="PRINCIPAL">39-04-01 MARCHÉS ET CONTRATS ADMINISTRATIFS. FIN DES CONTRATS. NULLITÉ. - CONSTAT DE L'ABSENCE OU DE LA NULLITÉ DU CONTRAT ALORS QUE LE LITIGE A ÉTÉ ENGAGÉ SUR LE TERRAIN DE LA RESPONSABILITÉ CONTRACTUELLE - MOYENS TIRÉS DE L'ENRICHISSEMENT SANS CAUSE OU DE LA FAUTE À AVOIR INDUIT L'AUTRE PARTIE EN ERREUR SUR L'EXISTENCE DE RELATIONS CONTRACTUELLES OU À AVOIR PASSÉ UN CONTRAT NUL - POSSIBILITÉ D'INVOQUER CES MOYENS, Y COMPRIS POUR LA PREMIÈRE FOIS EN APPEL - EXISTENCE [RJ1].
</SCT>
<SCT ID="8E" TYPE="PRINCIPAL">39-08-04-01-01 MARCHÉS ET CONTRATS ADMINISTRATIFS. RÈGLES DE PROCÉDURE CONTENTIEUSE SPÉCIALES. VOIES DE RECOURS. APPEL. MOYENS RECEVABLES EN APPEL. - EXISTENCE - MOYENS TIRÉS DE L'ENRICHISSEMENT SANS CAUSE OU DE LA RESPONSABILITÉ POUR FAUTE DANS UN LITIGE ENGAGÉ SUR LE TERRAIN DE LA RESPONSABILITÉ CONTRACTUELLE EN CAS D'ABSENCE OU DE NULLITÉ DU CONTRAT [RJ1].
</SCT>
<SCT ID="8F" TYPE="PRINCIPAL">54-08-01-03-02 PROCÉDURE. VOIES DE RECOURS. APPEL. MOYENS RECEVABLES EN APPEL. PRÉSENTENT CE CARACTÈRE. - EXISTENCE - MOYENS TIRÉS DE L'ENRICHISSEMENT SANS CAUSE OU DE LA RESPONSABILITÉ POUR FAUTE DANS UN LITIGE ENGAGÉ SUR LE TERRAIN DE LA RESPONSABILITÉ CONTRACTUELLE EN CAS D'ABSENCE OU DE NULLITÉ DU CONTRAT [RJ1].
</SCT>
<SCT ID="8G" TYPE="PRINCIPAL">60-01-02-01-04 RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. FAITS SUSCEPTIBLES OU NON D'OUVRIR UNE ACTION EN RESPONSABILITÉ. FONDEMENT DE LA RESPONSABILITÉ. RESPONSABILITÉ SANS FAUTE. ENRICHISSEMENT SANS CAUSE. - MOYENS TIRÉS DE L'ENRICHISSEMENT SANS CAUSE OU DE LA RESPONSABILITÉ POUR FAUTE DANS UN LITIGE ENGAGÉ SUR LE TERRAIN DE LA RESPONSABILITÉ CONTRACTUELLE EN CAS D'ABSENCE OU DE NULLITÉ DU CONTRAT [RJ1].
</SCT>
<SCT ID="8H" TYPE="PRINCIPAL">60-01-02-02 RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. FAITS SUSCEPTIBLES OU NON D'OUVRIR UNE ACTION EN RESPONSABILITÉ. FONDEMENT DE LA RESPONSABILITÉ. RESPONSABILITÉ POUR FAUTE. - LITIGE ENGAGÉ SUR LE TERRAIN DE LA RESPONSABILITÉ CONTRACTUELLE - CONSTAT DE L'ABSENCE OU DE LA NULLITÉ DU CONTRAT - MOYEN TIRÉ DE LA FAUTE À AVOIR INDUIT L'AUTRE PARTIE EN ERREUR SUR L'EXISTENCE DE RELATIONS CONTRACTUELLES OU À AVOIR PASSÉ UN CONTRAT NUL  - MOYEN REPOSANT SUR UNE CAUSE JURIDIQUE NOUVELLE - EXISTENCE - MOYEN D'ORDRE PUBLIC - ABSENCE - POSSIBILITÉ D'INVOQUER CE MOYEN, Y COMPRIS POUR LA PREMIÈRE FOIS EN APPEL - EXISTENCE [RJ1].
</SCT>
<ANA ID="9A"> 24-01-02-01-01-02 Nul ne peut, sans disposer d'un titre l'y habilitant, occuper une dépendance du domaine public.,,,Eu égard aux exigences qui découlent tant de l'affectation normale du domaine public que des impératifs de protection et de bonne gestion de ce domaine, l'existence de relations contractuelles en autorisant l'occupation privative ne peut se déduire de sa seule occupation effective, même si celle-ci a été tolérée par l'autorité gestionnaire et a donné lieu au versement de redevances domaniales.,,,Une convention d'occupation du domaine public ne peut être tacite et doit revêtir un caractère écrit.</ANA>
<ANA ID="9B"> 39-01-01 1) Nul ne peut, sans disposer d'un titre l'y habilitant, occuper une dépendance du domaine public.,,,Eu égard aux exigences qui découlent tant de l'affectation normale du domaine public que des impératifs de protection et de bonne gestion de ce domaine, l'existence de relations contractuelles en autorisant l'occupation privative ne peut se déduire de sa seule occupation effective, même si celle-ci a été tolérée par l'autorité gestionnaire et a donné lieu au versement de redevances domaniales.,,,Une convention d'occupation du domaine public ne peut être tacite et doit revêtir un caractère écrit.,,,2) Lorsque le juge, saisi d'un litige engagé sur le terrain de la responsabilité contractuelle, est conduit à constater, le cas échéant d'office, l'absence ou la nullité du contrat, les parties qui s'estimaient liées par ce contrat peuvent poursuivre le litige qui les oppose en invoquant, y compris pour la première fois en appel, des moyens tirés de l'enrichissement sans cause que l'application du contrat par lequel elles s'estimaient liées a apporté à l'une d'elles ou de la faute consistant, pour l'une d'elles, à avoir induit l'autre partie en erreur sur l'existence de relations contractuelles ou à avoir passé un contrat nul, bien que ces moyens, qui ne sont pas d'ordre public, reposent sur des causes juridiques nouvelles.</ANA>
<ANA ID="9C"> 39-02 Nul ne peut, sans disposer d'un titre l'y habilitant, occuper une dépendance du domaine public.,,,Eu égard aux exigences qui découlent tant de l'affectation normale du domaine public que des impératifs de protection et de bonne gestion de ce domaine, l'existence de relations contractuelles en autorisant l'occupation privative ne peut se déduire de sa seule occupation effective, même si celle-ci a été tolérée par l'autorité gestionnaire et a donné lieu au versement de redevances domaniales.,,,Une convention d'occupation du domaine public ne peut être tacite et doit revêtir un caractère écrit.</ANA>
<ANA ID="9D"> 39-04-01 Lorsque le juge, saisi d'un litige engagé sur le terrain de la responsabilité contractuelle, est conduit à constater, le cas échéant d'office, l'absence ou la nullité du contrat, les parties qui s'estimaient liées par ce contrat peuvent poursuivre le litige qui les oppose en invoquant, y compris pour la première fois en appel, des moyens tirés de l'enrichissement sans cause que l'application du contrat par lequel elles s'estimaient liées a apporté à l'une d'elles ou de la faute consistant, pour l'une d'elles, à avoir induit l'autre partie en erreur sur l'existence de relations contractuelles ou à avoir passé un contrat nul, bien que ces moyens, qui ne sont pas d'ordre public, reposent sur des causes juridiques nouvelles.</ANA>
<ANA ID="9E"> 39-08-04-01-01 Lorsque le juge, saisi d'un litige engagé sur le terrain de la responsabilité contractuelle, est conduit à constater, le cas échéant d'office, l'absence ou la nullité du contrat, les parties qui s'estimaient liées par ce contrat peuvent poursuivre le litige qui les oppose en invoquant, y compris pour la première fois en appel, des moyens tirés de l'enrichissement sans cause que l'application du contrat par lequel elles s'estimaient liées a apporté à l'une d'elles ou de la faute consistant, pour l'une d'elles, à avoir induit l'autre partie en erreur sur l'existence de relations contractuelles ou à avoir passé un contrat nul, bien que ces moyens, qui ne sont pas d'ordre public, reposent sur des causes juridiques nouvelles.</ANA>
<ANA ID="9F"> 54-08-01-03-02 Lorsque le juge, saisi d'un litige engagé sur le terrain de la responsabilité contractuelle, est conduit à constater, le cas échéant d'office, l'absence ou la nullité du contrat, les parties qui s'estimaient liées par ce contrat peuvent poursuivre le litige qui les oppose en invoquant, y compris pour la première fois en appel, des moyens tirés de l'enrichissement sans cause que l'application du contrat par lequel elles s'estimaient liées a apporté à l'une d'elles ou de la faute consistant, pour l'une d'elles, à avoir induit l'autre partie en erreur sur l'existence de relations contractuelles ou à avoir passé un contrat nul, bien que ces moyens, qui ne sont pas d'ordre public, reposent sur des causes juridiques nouvelles.</ANA>
<ANA ID="9G"> 60-01-02-01-04 Lorsque le juge, saisi d'un litige engagé sur le terrain de la responsabilité contractuelle, est conduit à constater, le cas échéant d'office, l'absence ou la nullité du contrat, les parties qui s'estimaient liées par ce contrat peuvent poursuivre le litige qui les oppose en invoquant, y compris pour la première fois en appel, des moyens tirés de l'enrichissement sans cause que l'application du contrat par lequel elles s'estimaient liées a apporté à l'une d'elles ou de la faute consistant, pour l'une d'elles, à avoir induit l'autre partie en erreur sur l'existence de relations contractuelles ou à avoir passé un contrat nul, bien que ces moyens, qui ne sont pas d'ordre public, reposent sur des causes juridiques nouvelles.</ANA>
<ANA ID="9H"> 60-01-02-02 Lorsque le juge, saisi d'un litige engagé sur le terrain de la responsabilité contractuelle, est conduit à constater, le cas échéant d'office, l'absence ou la nullité du contrat, les parties qui s'estimaient liées par ce contrat peuvent poursuivre le litige qui les oppose en invoquant, y compris pour la première fois en appel, des moyens tirés de l'enrichissement sans cause que l'application du contrat par lequel elles s'estimaient liées a apporté à l'une d'elles ou de la faute consistant, pour l'une d'elles, à avoir induit l'autre partie en erreur sur l'existence de relations contractuelles ou à avoir passé un contrat nul, bien que ces moyens, qui ne sont pas d'ordre public, reposent sur des causes juridiques nouvelles.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr. CE, Section, 20 octobre 2000, Société Citécâble Est, n° 196553, p. 457 ; CE, 15 mai 2013, Commune de Villeneuve-les-Avignon, n° 354593, T. pp. 803-826-871.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
