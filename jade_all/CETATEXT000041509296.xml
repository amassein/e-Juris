<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000041509296</ID>
<ANCIEN_ID>JG_L_2020_01_000000437276</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/41/50/92/CETATEXT000041509296.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 24/01/2020, 437276, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-01-24</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>437276</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LYON-CAEN, THIRIEZ</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2020:437276.20200124</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés les 31 décembre 2019 et 20 janvier 2020 au secrétariat du contentieux du Conseil d'Etat, l'association Canopée et l'association Les Amis de la Terre France demandent au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative :<br/>
<br/>
              1°) de suspendre l'exécution de la note d'information du 19 décembre 2019 de la direction générale des douanes et droits indirects du ministère de l'action et des comptes publics relative à la taxe incitative relative à l'incorporation de bio-carburants (TIRIB) en tant qu'elle prévoit que les biocarburants produits à partir de distillats d'acides gras de palme (PFAD) ne seront pas exclus du mécanisme de la TIRIB à compter du 1er janvier 2020 ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Les associations requérantes soutiennent que :<br/>
              - leur requête est recevable dès lors que, d'une part, elles ont un intérêt à agir, d'autre part, leur requête n'est pas tardive et, enfin, la décision contestée revêt le caractère d'un acte faisant grief ;<br/>
              - la condition d'urgence est remplie dès lors que la note litigieuse, en ce qu'elle maintient à compter du 1er janvier 2020 un avantage fiscal pour les biocarburants issus de distillats d'acides gras de palme, susceptible d'avoir pour effet d'augmenter la demande française d'huile de palme et, partant, sa production à l'étranger, porte atteinte de manière grave et immédiate à l'intérêt public qui s'attache à la lutte contre la déforestation et le changement climatique ;<br/>
              - il existe un doute sérieux quant à la légalité de la note contestée ; <br/>
              - les dispositions contestées de la note litigieuse, relatives au maintien d'un avantage fiscal pour les biocarburants issus de distillats d'acides gras de palme, ont été prises par une autorité incompétente pour en connaître dès lors qu'elles instituent une règle nouvelle relevant du domaine de la loi ;<br/>
              - la note litigieuse a été prise en méconnaissance de l'article 266 quindecies du code des douanes et à l'objectif national et européen de lutte contre la déforestation et le changement climatique.<br/>
<br/>
              Par un mémoire en défense, enregistré le 16 janvier 2020, le ministre de l'action et des comptes publics conclut au rejet de la requête. Il soutient que la requête est tardive, que la condition d'urgence n'est pas remplie et que les moyens soulevés par les requérantes ne sont pas fondés.<br/>
<br/>
              Par un mémoire en intervention, enregistré le 16 janvier 2020, la société Total Raffinage France conclut au rejet des conclusions de la requête présentée par l'association Canopée et l'association Les Amis de la Terre France. Elle soutient que leur requête n'est pas recevable dès lors que les associations sont dépourvues d'intérêt à agir.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la directive 2018/2001 du Parlement et du Conseil du 11 décembre 2018 relative à la promotion de l'utilisation de l'énergie produite à partir de sources renouvelables ;<br/>
              - la loi de finance pour 2019 n° 2018-1317 du 28 décembre 2018 ;<br/>
              - le code des douanes ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, l'association Canopée, l'association Les Amis de la Terre France et, d'autre part, le ministre de l'action et des comptes publics et la société Total Raffinage France ; <br/>
<br/>
              Vu l'audience publique du mercredi 22 janvier 2020 à 12 heures au cours de laquelle ont été entendus :<br/>
<br/>
              - les représentants de l'association Canopée et de l'association Les Amis de la Terre France ;<br/>
<br/>
              - le représentant du ministre de l'action et des comptes publics ;<br/>
              - Me Lyon-Caen, avocat au Conseil d'Etat et à la Cour de cassation, avocat de la société Total Raffinage France ;<br/>
<br/>
              - les représentants de la société Total Raffinage France ;<br/>
<br/>
              et à l'issue de laquelle le juge des référés a clos l'instruction ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 22 janvier 2020 présentée pour la société Total Raffinage France ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
              1. Aux termes de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision ". <br/>
<br/>
              2. Par une note d'information relative à la taxe incitative relative à l'incorporation de bio-carburants (TIRIB) du 19 décembre 2019, applicable au 1er janvier 2020, la Direction générale des douanes et droits indirects du ministère de l'action et des comptes publics a maintenu les biocarburants produits à partir de distillats d'acides gras de palme (PFAD) au sein du mécanisme de la TIRIB. <br/>
<br/>
              3. La société Total Raffinage France a intérêt au maintien de la note du 19 décembre 2019. Par suite son intervention en défense est recevable. <br/>
<br/>
              4. Pour établir l'urgence qui s'attache à la suspension qu'elles demandent, les associations requérantes se bornent à alléguer que la note attaquée a pour effet de contribuer à la déforestation résultant de l'extension des plantations de palmiers dans les pays producteurs, dans des terres riches en carbone, cette extension s'opérant également par des changements indirects dans l'affectation des sols. Leur requête n'est toutefois soutenue que d'appréciations générales sur le développement des palmeraies et leur caractère nocif, et n'est assortie d'aucun élément quantitatif, d'études ou de précisions sur les mécanismes par lesquels la note des douanes contribuerait directement et immédiatement au développement de phénomènes dégradant l'environnement. Il n'est pas manifeste que la réduction de la taxe incitative relative à l'incorporation des biocarburants sur les distillats d'acides gras du palmier, même si elle n'est à l'évidence pas dépourvue d'incidence sur la rentabilité de l'exploitation des palmeraies et donc les décisions de les développer, aurait par elle-même et directement pour effet d'inciter au développement de la plantation de palmiers à des échéances telles qu'elle justifierait l'intervention du juge des référés pour en prévenir les conséquences sur l'environnement. Dès lors que la question de déterminer si la sous-production fatale d'acides gras, susceptibles d'être distillés, à l'occasion de la production d'huile de palme, en vue de fabriquer des biocarburants, aboutit, ou non, à l'inclusion de ceux-ci parmi les biocarburants à base d'huile de palme pour la mise en oeuvre de la taxe, est susceptible d'être examinée par la 9e chambre de la section du contentieux du Conseil d'Etat lors d'une audience qui se tiendrait dans les prochains mois et en tout état de cause avant l'été 2020, de manière à ce que, en cas d'annulation, les effets induits sur l'environnement allégués soient effectivement paralysés en temps utile, la condition d'urgence au sens de l'article L. 521-1 du code de justice administrative ne peut être regardée comme satisfaite.<br/>
<br/>
              5. Il y a donc lieu, sans qu'il soit besoin d'examiner les irrecevabilités soulevées en défense ni de se prononcer sur le sérieux des moyens invoqués, de rejeter la demande de suspension, ainsi que les conclusions des associations requérantes tendant au versement d'une somme d'argent par l'Etat sur le fondement de l'article L. 761-1 du code de justice administrative qui y font obstacle dès lors que l'Etat n'est pas la partie perdante.<br/>
<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : L'intervention de la société Total France Raffinage est admise.<br/>
Article 2 : La requête de l'association Canopée et de l'association Les Amis de la Terre France est rejetée.<br/>
Article 3 : La présente ordonnance sera notifiée à l'association Canopée, à l'association Les Amis de la Terre France, au ministre de l'action et des comptes publics et à la société Total Raffinage France.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
