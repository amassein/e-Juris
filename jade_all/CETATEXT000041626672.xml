<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000041626672</ID>
<ANCIEN_ID>JG_L_2020_02_000000421291</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/41/62/66/CETATEXT000041626672.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème - 8ème chambres réunies, 24/02/2020, 421291</TITRE>
<DATE_DEC>2020-02-24</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>421291</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème - 8ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP RICARD, BENDEL-VASSEUR, GHNASSIA ; LE PRADO</AVOCATS>
<RAPPORTEUR>Mme Cécile Isidoro</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Marie-Gabrielle Merloz</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:421291.20200224</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              M. A... B... a demandé au tribunal administratif de Bordeaux, en premier lieu, d'annuler l'arrêté du 24 juin 2015 par lequel le maire de Marmande a mis fin à son stage en qualité d'adjoint technique territorial de deuxième classe et prononcé sa radiation des effectifs de la commune, en deuxième lieu, d'enjoindre à la commune de Marmande de le titulariser, enfin de condamner cette commune à lui verser une somme de 5 000 euros en réparation du préjudice moral qu'il estime avoir subi. <br/>
<br/>
              Par un jugement n° 1503789 du 5 juillet 2016, le tribunal administratif de Bordeaux a rejeté sa demande. <br/>
<br/>
              Par un arrêt n° 16BX03098 du 10 avril 2018, la cour administrative d'appel de Bordeaux a, sur appel de M. B..., annulé ce jugement. <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 7 juin et 6 septembre 2018 au secrétariat du contentieux du Conseil d'Etat, la commune de Marmande demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de M. B... ;<br/>
<br/>
              3°) de mettre à la charge de M. B... la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Cécile Isidoro, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Marie-Gabrielle Merloz, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Ricard, Bendel-Vasseur, Ghnassia, avocat de la commune de Marmande et à Me Le Prado, avocat de M. A... B... ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que, après avoir été adjoint technique territorial dans les services de la commune de Marmande, avec laquelle il avait conclu des contrats à durée déterminée d'une durée totale de vingt-neuf mois, M. B... a été nommé à compter du 1er janvier 2014 dans le cadre d'emplois des adjoints techniques territoriaux en qualité d'adjoint technique de deuxième classe stagiaire à temps complet. Après avoir prolongé le stage de M. B..., à partir du 1er janvier 2015, du fait de congés maladie, " jusqu'à accomplissement de la durée effective de stage d'un an ", le maire de Marmande a, par un arrêté du 24 juin 2015, rayé M. B... des effectifs de la collectivité à l'issue de cette prolongation. M. B... a saisi le tribunal administratif de Bordeaux d'une demande tendant à l'annulation de cet arrêté, à ce qu'il soit enjoint à la commune de Marmande de le réintégrer et de le titulariser et à la condamnation de la commune à lui verser une somme de 5 000 euros en réparation de son préjudice moral. Le tribunal administratif de Bordeaux a rejeté ses demandes par un jugement du 5 juillet 2016. Par un arrêt du 10 avril 2018, la cour administrative d'appel de Bordeaux a annulé ce jugement et l'arrêté du 24 juin 2015 et condamné la commune de Marmande à verser à M. B... la somme de 2 000 euros en réparation du préjudice subi. La commune de Marmande se pourvoit en cassation contre cet arrêt. <br/>
<br/>
              2. Aux termes de l'article 5 du décret du 4 novembre 1992 fixant les dispositions communes applicables aux fonctionnaires stagiaires : " Le fonctionnaire territorial stagiaire peut être licencié pour insuffisance professionnelle lorsqu'il est en stage depuis un temps au moins égal à la moitié de la durée normale du stage. Le licenciement est prononcé après avis de la commission administrative paritaire compétente pour le cadre d'emplois dans lequel l'intéressé a vocation à être titularisé (...) ". Aux termes de l'article 8 du décret du 22 décembre 2006 portant statut particulier du cadre d'emplois des agents techniques territoriaux, dans sa rédaction applicable au présent litige : " Les candidats recrutés en qualité d'adjoint technique territorial de 2e classe sur un emploi d'une collectivité territoriale (...) sont nommés stagiaires par l'autorité territoriale investie du pouvoir de nomination pour une durée d'un an. (...) ". L'article 10 du même décret dispose que : " A l'issue du stage, les stagiaires dont les services ont donné satisfaction sont titularisés par décision de l'autorité territoriale investie du pouvoir de nomination. (...) Les autres stagiaires peuvent, sur décision de l'autorité territoriale, être autorisés à effectuer un stage complémentaire d'une durée maximale d'un an. Si le stage complémentaire a été jugé satisfaisant, les intéressés sont titularisés. / Les adjoints techniques territoriaux de 2e classe stagiaires (...) qui n'ont pas été autorisés à effectuer un stage complémentaire, ou dont le stage complémentaire n'a pas été jugé satisfaisant, sont soit licenciés s'ils n'avaient pas auparavant la qualité de fonctionnaire, soit réintégrés dans leur grade d'origine ". <br/>
<br/>
              3. Un agent public ayant, à la suite de son recrutement ou dans le cadre de la formation qui lui est dispensée, la qualité de stagiaire se trouve dans une situation probatoire et provisoire. La décision de ne pas le titulariser en fin de stage est fondée sur l'appréciation portée par l'autorité compétente sur son aptitude à exercer les fonctions auxquelles il peut être appelé et, de manière générale, sur sa manière de servir, et se trouve ainsi prise en considération de sa personne. <br/>
<br/>
              4. L'autorité compétente ne peut donc prendre légalement une décision de refus de titularisation, qui n'est soumise qu'aux formes et procédures expressément prévues par les lois et règlements, que si les faits qu'elle retient caractérisent des insuffisances dans l'exercice des fonctions et la manière de servir de l'intéressé. Cependant, la circonstance que tout ou partie de tels faits seraient également susceptibles de caractériser des fautes disciplinaires ne fait pas obstacle à ce que l'autorité compétente prenne légalement une décision de refus de titularisation, pourvu que l'intéressé ait alors été mis à même de faire valoir ses observations. <br/>
<br/>
              5. Il résulte de ce qui précède que, pour apprécier la légalité d'une décision de refus de titularisation, il incombe au juge de vérifier qu'elle ne repose pas sur des faits matériellement inexacts, qu'elle n'est entachée ni d'erreur de droit, ni d'erreur manifeste dans l'appréciation de l'insuffisance professionnelle de l'intéressé, qu'elle ne revêt pas le caractère d'une sanction  disciplinaire et n'est entachée d'aucun détournement de pouvoir et que, si elle est fondée sur des motifs qui caractérisent une insuffisance professionnelle mais aussi des fautes disciplinaires, l'intéressé a été mis à même de faire valoir ses observations.<br/>
<br/>
              6. La cour administrative d'appel a relevé que l'autorité compétente de la commune de Marmande reprochait à M. B..., pour refuser de le titulariser, des absences injustifiées et le fait que, comme le montraient des attestations concordantes des divers responsables de l'intéressé au cours de son stage, il n'accomplissait les tâches demandées que dans la mesure où elles l'intéressaient. Il résulte de ce qui a été dit aux points 3 à 5 ci-dessus qu'en jugeant que les faits ainsi reprochés à M. B... ne pouvaient caractériser une insuffisance professionnelle justifiant légalement un refus de titularisation au motif qu'ils étaient également susceptibles de caractériser des fautes disciplinaires, la cour a commis une erreur de droit. <br/>
<br/>
              7. Dès lors, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, la commune de Marmande est fondée à demander l'annulation de l'arrêt qu'elle attaque. <br/>
<br/>
              8. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de M. B... la somme de 3 000 euros que demande la commune de Marmande au titre des dispositions de l'article L. 761-1 du code de justice administrative. Ces mêmes dispositions font en revanche obstacle à ce que soit mise à la charge de la commune de Marmande, qui n'est pas la partie perdante, la somme que demande M. B... à ce titre.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Bordeaux du 10 avril 2018 est annulé.<br/>
<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Bordeaux.<br/>
<br/>
Article 3 : M. B... versera à la commune de Marmande la somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative. <br/>
Article 4 : Les conclusions présentées par M. B... au titre de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
<br/>
Article 5 : La présente décision sera notifiée à la commune de Marmande et à M. A... B....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">36-03-04-01 FONCTIONNAIRES ET AGENTS PUBLICS. ENTRÉE EN SERVICE. STAGE. FIN DE STAGE. - REFUS DE TITULARISATION EN FIN DE STAGE - 1) DÉCISION PRISE EN CONSIDÉRATION DE LA PERSONNE - EXISTENCE [RJ1] - 2) CONDITION - INSUFFISANCES DANS L'EXERCICE DES FONCTIONS ET LA MANIÈRE DE SERVIR - 3) POSSIBILITÉ DE SE FONDER SUR DES FAITS ÉTANT ÉGALEMENT SUSCEPTIBLES DE JUSTIFIER UNE SANCTION DISCIPLINAIRE - EXISTENCE, SOUS RÉSERVE QUE LE STAGIAIRE AIT ÉTÉ MIS À MÊME DE FAIRE VALOIR SES OBSERVATIONS [RJ2] - 4) CONTRÔLE DU JUGE.
</SCT>
<ANA ID="9A"> 36-03-04-01 1) Un agent public ayant, à la suite de son recrutement ou dans le cadre de la formation qui lui est dispensée, la qualité de stagiaire se trouve dans une situation probatoire et provisoire. La décision de ne pas le titulariser en fin de stage est fondée sur l'appréciation portée par l'autorité compétente sur son aptitude à exercer les fonctions auxquelles il peut être appelé et, de manière générale, sur sa manière de servir, et se trouve ainsi prise en considération de sa personne.... ,,2) L'autorité compétente ne peut donc prendre légalement une décision de refus de titularisation, qui n'est soumise qu'aux formes et procédures expressément prévues par les lois et règlements, que si les faits qu'elle retient caractérisent des insuffisances dans l'exercice des fonctions et la manière de servir de l'intéressé.... ,,3) Cependant, la circonstance que tout ou partie de tels faits seraient également susceptibles de caractériser des fautes disciplinaires ne fait pas obstacle à ce que l'autorité compétente prenne légalement une décision de refus de titularisation, pourvu que l'intéressé ait été alors mis à même de faire valoir ses observations....   ,,4) Il résulte de ce qui précède que, pour apprécier la légalité d'une décision de refus de titularisation, il incombe au juge de vérifier qu'elle ne repose pas sur des faits matériellement inexacts, qu'elle n'est entachée ni d'erreur de droit, ni d'erreur manifeste dans l'appréciation de l'insuffisance professionnelle de l'intéressé, qu'elle ne revêt pas le caractère d'une sanction disciplinaire et n'est entachée d'aucun détournement de pouvoir et que, si elle est fondée sur des motifs qui caractérisent une insuffisance professionnelle mais aussi des fautes disciplinaires, l'intéressé a été mis à même de faire valoir ses observations.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, Section, 3 décembre 2003,,, n° 236485, p. 469.,,[RJ2] Rappr., pour le refus de renouvellement d'un contrat, CE, 19 décembre 2019, Commune du Vésinet, n° 423685, à mentionner aux Tables.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
