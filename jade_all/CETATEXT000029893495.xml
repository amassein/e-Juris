<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029893495</ID>
<ANCIEN_ID>JG_L_2014_12_000000364775</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/89/34/CETATEXT000029893495.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème / 4ème SSR, 12/12/2014, 364775</TITRE>
<DATE_DEC>2014-12-12</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>364775</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème / 4ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP PIWNICA, MOLINIE ; SCP BARTHELEMY, MATUCHANSKY, VEXLIARD, POUPOT ; SCP SPINOSI, SUREAU ; SCP LYON-CAEN, THIRIEZ</AVOCATS>
<RAPPORTEUR>M. Jean-Dominique Langlais</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Fabienne Lambolez</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:364775.20141212</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la requête sommaire et le mémoire complémentaire, enregistrés le 24 décembre 2012 et le 25 mars 2013 au secrétariat du contentieux du Conseil d'Etat, présentés pour la société RML, dont le siège est 167, rue du Chevaleret à Paris (75013), représentée par son président-directeur général ; la société RML demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler les décisions du 2 octobre 2012 par lesquelles le Conseil supérieur de l'audiovisuel (CSA) a, d'une part, rejeté sa candidature en vue d'obtenir l'autorisation d'exploiter le service de radiodiffusion sonore par voie hertzienne terrestre de catégorie D dénommé Latina FM dans la zone de Reims et, d'autre part, autorisé l'exploitation dans cette zone des services Fun Radio, Virgin Radio, Radio Nova et Chérie FM ; <br/>
<br/>
              2°) d'enjoindre au CSA de réexaminer sa candidature dans un délai de deux mois à compter de la décision à intervenir ; <br/>
<br/>
              3°) de mettre à la charge du CSA une somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 27 novembre 2014, présentée pour la société Europe 2 Entreprises ;<br/>
<br/>
              Vu la loi n° 86-1067 du 30 septembre 1986 ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Dominique Langlais, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Fabienne Lambolez, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Spinosi, Sureau, avocat de la société RML, à la SCP Lyon-Caen, Thiriez, avocat de la SA SERC, à la SCP Barthélemy, Matuchansky, Vexliard, Poupot, avocat de la SAS Europe 2 Entreprises et à la SCP Piwnica, Molinié, avocat de la SAS Chérie FM ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant que, par une décision du 3 novembre 2011, le Conseil supérieur de l'audiovisuel (CSA) a lancé un appel à candidatures pour l'exploitation de services de radio par voie hertzienne terrestre dans la zone de Reims ; que son assemblée plénière, réunie le 2 octobre 2012 a, d'une part, rejeté la candidature présentée par la société RML pour la diffusion de Latina FM, service de catégorie D, au motif que sa programmation musicale, " exclusivement composée de musiques latines, [était] susceptible d'intéresser un moins large public que les candidats retenus dans cette catégorie, dont l'offre est plus diversifiée ", et, d'autre part, retenu dans cette catégorie les candidatures des services Radio Nova, édité par la SARL Radio Nova, Fun Radio, édité par la SA SERC, Virgin Radio, édité par la SAS Europe 2 Entreprises et Chérie FM, édité par la SAS Chérie FM ; que la société RML demande l'annulation pour excès de pouvoir de l'ensemble de ces décisions ; <br/>
<br/>
              2. Considérant, d'une part, qu'aux termes de l'article 4 de la loi du 30 septembre 1986 : " Le Conseil supérieur de l'audiovisuel comprend neuf membres nommés par décret du Président de la République (...) Le Conseil supérieur de l'audiovisuel ne peut délibérer que si six au moins de ses membres sont présents (...) " ; qu'il ressort de l'extrait du procès-verbal de la réunion plénière du 2 octobre 2012 que les neuf membres du Conseil supérieur de l'audiovisuel ont siégé lors de cette réunion ; qu'ainsi, le moyen tiré de ce que la règle de quorum applicable à cette instance n'aurait pas été respectée manque en fait ;<br/>
<br/>
              3. Considérant, d'autre part, qu'aux termes de l'article 29 de la loi du 30 septembre 1986, le CSA " accorde les autorisations en appréciant l'intérêt de chaque projet pour le public, au regard des impératifs prioritaires que sont la sauvegarde du pluralisme des courants d'expression socioculturels, la diversification des opérateurs, et la nécessité d'éviter les abus de position dominante ainsi que les pratiques entravant le libre exercice de la concurrence. / Il tient également compte : (...) 2° Du financement et des perspectives d'exploitation du service notamment en fonction des possibilités de partage des ressources publicitaires entre les entreprises de presse écrite et les services de communication audiovisuelle ; (...) / Le conseil veille également au juste équilibre entre les réseaux nationaux de radiodiffusion, d'une part, et les services locaux, régionaux et thématiques indépendants, d'autre part " ;<br/>
<br/>
              4. Considérant que, par ses communiqués n° 34 du 29 août 1989 et n° 281 du 10 novembre 1994, le CSA, faisant usage des pouvoirs qu'il tient de ces dispositions, a déterminé cinq catégories de services en vue de l'appel à candidatures pour l'exploitation de services de radiodiffusion sonore par voie hertzienne terrestre ; que ces cinq catégories sont ainsi définies : services associatifs éligibles au fonds de soutien, mentionnés à l'article 80 (catégorie A), services locaux ou régionaux indépendants ne diffusant pas de programme national identifié (catégorie B), services locaux ou régionaux diffusant le programme d'un réseau thématique à vocation nationale (catégorie C), services thématiques à vocation nationale (catégorie D) et services généralistes à vocation nationale (catégorie E) ;<br/>
<br/>
              Sur la décision relative au service Radio Nova :<br/>
<br/>
              5. Considérant que, dans la zone de Reims, où quinze services étaient autorisés avant appel à candidatures, dont sept services de catégorie D, le CSA a autorisé sept nouveaux services, dont cinq services de catégorie D, parmi lesquels le service Radio Nova ; que, compte tenu des caractéristiques respectives des deux services, il n'a pas commis d'erreur d'appréciation en retenant que le service Latina FM était susceptible d'intéresser, dans cette zone, un moins large public que le service Radio Nova, radio musicale proposant des genres très variés ; qu'en se fondant ainsi sur l'intérêt de chaque projet pour le public, il n'a pas méconnu les dispositions précitées de la loi du 30 septembre 1986 ; <br/>
<br/>
              Sur les décisions relatives aux services Fun Radio, Virgin Radio, Chérie FM et Latina FM : <br/>
<br/>
              6. Considérant que le CSA a également autorisé en catégorie D les services Skyrock, Fun Radio et Virgin Radio, dont la programmation et le public sont proches, et le service Chérie FM, dont la programmation et le public sont proches de ceux du service Nostalgie, déjà autorisé dans la zone ; qu'en accordant ainsi une part importante des fréquences disponibles à des services présentant de fortes analogies, alors que le nombre de ces fréquences lui permettait d'accueillir la candidature de services plus originaux, il a méconnu l'impératif prioritaire de sauvegarde du pluralisme des courants d'expression socioculturels ; qu'il y a lieu, par suite, d'annuler ses décisions autorisant les services Fun Radio, Virgin Radio et Chérie FM, ainsi que sa décision rejetant la candidature du service Latina FM, qui repose sur la comparaison de ce service avec les services précédents ; <br/>
<br/>
              7. Considérant qu'il résulte de tout ce qui précède que la société requérante est seulement fondée à demander l'annulation des décisions par lesquelles le CSA a autorisé les services Fun Radio, Virgin Radio et Chérie FM, ainsi que de la décision par laquelle il a rejeté la candidature de son service ; <br/>
<br/>
              Sur la date d'effet des annulations prononcées par la présente décision :<br/>
<br/>
              8. Considérant que le fait d'interrompre immédiatement la diffusion des services dont les autorisations sont annulées par la présente décision et de laisser les fréquences correspondantes inutilisées dans l'attente de la délivrance de nouvelles autorisations entraînerait une atteinte excessive à l'intérêt du public ainsi qu'à la situation des titulaires des autorisations annulées ; qu'il y a lieu, dès lors, par dérogation à la règle de l'effet rétroactif des annulations contentieuses, de prévoir que ces annulations ne prendront effet qu'au terme du délai nécessaire à la sélection des services auxquels les fréquences seront attribuées et à la délivrance des nouvelles autorisations ; <br/>
<br/>
              9. Considérant qu'il incombe au CSA, à la suite de l'annulation par le juge de l'excès de pouvoir d'une décision autorisant l'exploitation d'un service de radiodiffusion sonore par voie hertzienne, de statuer à nouveau au vu des candidatures présentées dans la zone concernée, dans le cadre de la procédure ayant conduit à cette autorisation ; qu'il lui appartient d'informer les candidats de la reprise de cette procédure en les invitant à confirmer et, le cas échéant, à compléter leur dossier de candidature ; qu'il n'en va autrement que si le vice censuré par l'annulation prononcée par le juge a entaché d'irrégularité l'ensemble de la procédure d'attribution, si l'évolution des circonstances de droit depuis la date de la décision initiale l'exige ou si une évolution des circonstances de fait rend manifestement impossible l'attribution de la fréquence sans nouvel appel à candidatures ; <br/>
<br/>
              10. Considérant qu'en l'espèce aucune circonstance ne justifie que le CSA procède à un nouvel appel à candidatures pour attribuer les fréquences en cause ; que, par ailleurs, il ne résulte pas de l'instruction que le CSA soit dans l'obligation de procéder, préalablement à la nouvelle attribution de ces fréquences, à la consultation publique et à l'étude d'impact prévues par l'article 31 de la loi du 30 septembre 1986 dans sa rédaction issue de l'article 31 de la loi du 15 novembre 2013 relative à l'indépendance de l'audiovisuel public, en cas de modification importante du marché des services de communication audiovisuelle concerné ; <br/>
<br/>
              11. Considérant que, dans ces conditions, il y a lieu de ne prononcer l'annulation des autorisations des services Fun Radio, Virgin Radio et Chérie FM dans la zone de Reims qu'à l'expiration d'un délai de six mois à compter de la date de la présente décision ;<br/>
<br/>
              Sur les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative : <br/>
<br/>
              12. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge du CSA la somme de 4 000 euros à verser à la société RML ; que ces dispositions font en revanche obstacle à ce que soient mises à la charge de la société RML, qui n'est pas, dans la présente instance, la partie perdante, les sommes demandées par la SAS Chérie FM, par la société Europe 2 Entreprises et par la SA SERC ;   <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Les décisions du 2 octobre 2012 par lesquelles le CSA a autorisé la diffusion des services Fun Radio, Virgin Radio et Chérie FM en catégorie D dans la zone de Reims et la décision du 2 octobre 2012 par laquelle il a rejeté la candidature du service Latina FM dans cette zone sont annulées. Cette annulation prendra effet à l'expiration d'un délai de six mois à compter de la date de la présente décision. <br/>
<br/>
Article 2 : Le CSA versera une somme de 4 000 euros à la société RML au titre de l'article L. 761-1 du code de justice administrative.<br/>
		Article 3 : Le surplus des conclusions de la requête est rejeté.<br/>
Article 4 : Les conclusions présentées par la SAS Chérie FM, par la société Europe 2 Entreprises et par la SA SERC au titre de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
Article 5 : La présente décision sera notifiée à la société RML, au Conseil supérieur de l'audiovisuel, à la SA SERC, à la SAS Europe 2 Entreprises, à la SARL Radio Nova et à la SAS Chérie FM.<br/>
Copie pour information en sera adressée à la ministre de la culture et de la communication. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">56-01 RADIO ET TÉLÉVISION. CONSEIL SUPÉRIEUR DE L'AUDIOVISUEL. - AUTORISATION D'UN SERVICE DE RADIO DIFFUSÉ PAR VOIE HERTZIENNE TERRESTRE - IMPÉRATIF PRIORITAIRE DE SAUVEGARDE DU PLURALISME DES COURANTS D'EXPRESSION SOCIOCULTURELS - MÉCONNAISSANCE EN L'ESPÈCE - ATTRIBUTION D'UNE PART IMPORTANTE DES FRÉQUENCES DISPONIBLES À DES SERVICES PRÉSENTANT DE FORTES ANALOGIES - CONSÉQUENCES DE CETTE ILLÉGALITÉ - ANNULATION DES DÉCISIONS AUTORISANT CES SERVICES ET DE LA DÉCISION REJETANT UNE CANDIDATURE SUR LA BASE D'UNE COMPARAISON ENTRE CETTE DERNIÈRE ET LES SERVICES PRÉCÉDENTS.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">56-04-01-01 RADIO ET TÉLÉVISION. SERVICES PRIVÉS DE RADIO ET DE TÉLÉVISION. SERVICES DE RADIO. OCTROI DES AUTORISATIONS. - IMPÉRATIF PRIORITAIRE DE SAUVEGARDE DU PLURALISME DES COURANTS D'EXPRESSION SOCIOCULTURELS - MÉCONNAISSANCE EN L'ESPÈCE - ATTRIBUTION D'UNE PART IMPORTANTE DES FRÉQUENCES DISPONIBLES À DES SERVICES PRÉSENTANT DE FORTES ANALOGIES - CONSÉQUENCES DE CETTE ILLÉGALITÉ - ANNULATION DES DÉCISIONS AUTORISANT CES SERVICES ET DE LA DÉCISION REJETANT UNE CANDIDATURE SUR LA BASE D'UNE COMPARAISON ENTRE CETTE DERNIÈRE ET LES SERVICES PRÉCÉDENTS.
</SCT>
<ANA ID="9A"> 56-01 Zone dans laquelle le Conseil supérieur de l'audiovisuel (CSA) a autorisé, en plus des quinze services déjà autorisés dont sept de catégorie D, sept nouveaux services dont cinq de catégorie D. Le CSA a notamment autorisé à ce dernier titre les services Skyrock, Fun Radio et Virgin Radio, dont la programmation et le public sont proches, et le service Chérie FM, dont la programmation et le public sont proches de ceux du service Nostalgie, déjà autorisé dans la zone. En accordant ainsi une part importante des fréquences disponibles à des services présentant de fortes analogies, alors que le nombre de ces fréquences lui permettait d'accueillir la candidature de services plus originaux, il a méconnu l'impératif prioritaire de sauvegarde du pluralisme des courants d'expression socioculturels.... ,,Par suite, annulation des décisions du CSA autorisant les services Fun Radio, Virgin Radio et Chérie FM, ainsi que sa décision rejetant la candidature du service Latina FM, qui repose sur la comparaison de ce service avec les services précédents.</ANA>
<ANA ID="9B"> 56-04-01-01 Zone dans laquelle le Conseil supérieur de l'audiovisuel (CSA) a autorisé, en plus des quinze services déjà autorisés dont sept de catégorie D, sept nouveaux services dont cinq de catégorie D. Le CSA a notamment autorisé à ce dernier titre les services Skyrock, Fun Radio et Virgin Radio, dont la programmation et le public sont proches, et le service Chérie FM, dont la programmation et le public sont proches de ceux du service Nostalgie, déjà autorisé dans la zone. En accordant ainsi une part importante des fréquences disponibles à des services présentant de fortes analogies, alors que le nombre de ces fréquences lui permettait d'accueillir la candidature de services plus originaux, il a méconnu l'impératif prioritaire de sauvegarde du pluralisme des courants d'expression socioculturels.... ,,Par suite, annulation des décisions du CSA autorisant les services Fun Radio, Virgin Radio et Chérie FM, ainsi que sa décision rejetant la candidature du service Latina FM, qui repose sur la comparaison de ce service avec les services précédents.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
