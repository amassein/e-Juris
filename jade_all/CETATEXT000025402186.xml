<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000025402186</ID>
<ANCIEN_ID>JG_L_2012_02_000000356207</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/25/40/21/CETATEXT000025402186.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 22/02/2012, 356207, Publié au recueil Lebon</TITRE>
<DATE_DEC>2012-02-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>356207</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DE CHAISEMARTIN, COURJON ; SCP TIFFREAU, CORLAY, MARLANGE</AVOCATS>
<RAPPORTEUR>Mme Isabelle de Silva</RAPPORTEUR>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2012:356207.20120222</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 27 janvier 2012 au secrétariat du contentieux du Conseil d'Etat, présentée par la société FRANCE TELECOM, dont le siège est situé 6 place d'Alleray à Paris (75015), représentée par son représentant légal ; la société FRANCE TELECOM demande au juge des référés du Conseil d'Etat :<br/>
<br/>
              1°) d'ordonner, sur le fondement de l'article L. 521-1 du code de justice administrative, la suspension de la délibération du conseil territorial de Saint-Barthélemy en date du 28 octobre 2011 portant création d'une taxe sur les installations électriques et de télécommunications aériennes ;<br/>
<br/>
              2°) de mettre à la charge de la collectivité de Saint-Barthélemy le versement de la somme de 15 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              elle soutient que sa requête en référé suspension est recevable dès lors qu'il résulte des articles L.O. 6243-1 et L.O. 6243-3 du code général des collectivités territoriales que les actes du conseil territorial de Saint-Barthélemy relevant du domaine de la loi peuvent être contestés par la voie d'un recours porté devant le Conseil d'Etat et soumis à la procédure du recours pour excès de pouvoir ; que la condition d'urgence est remplie, dès lors que l'impact financier de la taxe, éligible avant le 28 février 2012, porte un préjudice suffisamment grave et immédiat à sa situation ; que la délibération contestée porte une atteinte immédiate à l'intérêt public qui s'attache à la couverture du territoire national par le réseau de téléphonie ; qu'il existe un doute sérieux quant à la légalité de la décision contestée ; que le conseil territorial de Saint-Barthélemy, n'ayant pas reçu compétence en matière de télécommunications a, en instaurant la taxe contestée, excédé les compétences qui lui étaient dévolues par l'article L.O. 6214-3 du code général des collectivités territoriales ; que la taxe contestée, dès lors qu'elle vise à punir la non-utilisation des fourreaux souterrains réalisés par la collectivité, a le caractère d'une sanction, et a méconnu les principes de nécessité et de non-rétroactivité applicables aux dispositions fiscales à caractère répressif ; que la taxe contestée méconnaît le principe d'égalité devant l'impôt, dès lors notamment qu'elle prévoit une exonération qui n'est pas justifiée par des critères objectifs et rationnels, qu'elle ne prend pas en compte les activités spécifiques des opérateurs qui y sont soumis, ni la nature des emprises du réseau ; que la délibération litigieuse place la requérante en situation de double imposition au titre de son réseau et a omis de fixer le montant de la taxe en fonction des facultés contributives des usagers et consommateurs de services de télécommunications ; qu'à supposer que la taxe soit requalifiée en redevance à caractère domanial, elle serait contraire aux principes généraux du droit applicables aux redevances ; que la délibération a été prise selon une procédure irrégulière, faute d'avoir été précédée de la consultation du conseil économique, social et culturel de la collectivité de Saint-Barthélemy prévue par le II de l'article L.O. 6223-3 du code général des collectivités territoriales ;<br/>
<br/>
<br/>
              Vu la délibération dont la suspension de l'exécution est demandée ;<br/>
<br/>
              Vu la requête à fin d'annulation de la délibération contestée ; <br/>
<br/>
              Vu le mémoire en défense, enregistré le 10 février 2012, présenté pour la collectivité de Saint-Barthélemy, qui conclut au rejet de la requête et à ce que soit mis à la charge de la société FRANCE TELECOM le versement de la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative ; elle soutient que la requête est irrecevable ; qu'en effet, eu égard aux dispositions du code général des collectivités territoriales issues de la loi organique n° 2007-223 du 21 février 2007, seul le représentant de l'Etat peut introduire un recours tendant à la suspension d'un acte du conseil territorial intervenant dans le domaine de la loi, les autres parties n'ayant pas la possibilité de former un recours suspensif dans l'attente de la décision du Conseil d'Etat rendue au fond, qui intervient à très bref délai ; à titre subsidiaire, que la condition d'urgence n'est pas remplie, la société requérante ne démontrant aucune atteinte grave à sa situation financière ; à titre très subsidiaire, qu'aucun des moyens invoqués n'est propre à créer un doute sérieux quant à la légalité de la délibération contestée ; qu'en vertu de l'article L.O. 6214-3 du code général des collectivités territoriales, la collectivité est compétente pour fixer les règles applicables en matière d'impôts, droits et taxes ; que le décret n° 2005-1676 du 27 décembre 2005 relatif aux redevances d'occupation du domaine public non routier, aux droits de passage sur le domaine public routier et aux servitudes sur les propriétés privées n'est pas applicable pour les matières relevant de la compétence de la collectivité ; que le caractère prétendument punitif de la taxe instituée étant inexistant, le moyen tiré d'une violation des principes de nécessité et de non-rétroactivité doit être écarté ; que la délibération contestée ne méconnaît pas le principe d'égalité devant l'impôt ; qu'il ne résulte pas des dispositions du II de l'article L.O. 6223-3 du code général des collectivités territoriales que le conseil économique, social et culturel de la collectivité de Saint-Barthélemy devrait être consulté sur l'institution d'une nouvelle taxe et en matière fiscale ;<br/>
<br/>
              Vu les observations, enregistrées le 9 février 2012, présentées par l'Autorité de régulation des communications électroniques et des postes (ARCEP) ;<br/>
<br/>
              Vu le nouveau mémoire en défense, enregistré le 14 février 2012, présenté pour la collectivité de Saint-Barthélemy qui conclut au rejet de la requête ; elle soutient que la société France TELECOM  ayant cessé tout investissement sur l'île dans les années 2000, la collectivité a décidé de poser elle-même ses propres fourreaux sur tout le réseau routier rénové ; qu'elle a, contrairement à ce que soutient la requérante, donné suite aux propositions de FRANCE TELECOM  relatives à la mise en souterrain des réseaux aériens de communications électroniques ; que si le décret n° 2005-1676 du 27 décembre 2005 ouvre la possibilité à la collectivité de faire payer une redevance pour occupation du domaine public, FRANCE TELECOM n'a jusqu'à présent payé aucune redevance à la collectivité pour l'occupation du domaine public tant aérien que souterrain  ;  <br/>
<br/>
              Vu le mémoire en réplique, enregistré le 16 février 2012, présenté pour la société FRANCE TELECOM, qui reprend les conclusions de sa requête et les mêmes moyens ; elle soutient en outre que les dispositions du code général des collectivités territoriales ne font pas obstacle à ce que le recours en annulation ouvert par le législateur organique contre les actes de la collectivité territoriale relevant du domaine de la loi puisse être assorti d'un référé suspension en application de l'article L. 521-1 du code de justice administrative ; que la condition d'urgence est remplie dès lors que l'enjeu financier ne se limite pas aux éventuelles pénalités de retard mais porte principalement sur le montant de la taxe à payer au plus tard le 28 février, soit 194 000 euros ; que le très bref préavis imparti à la requérante pour réaliser des travaux d'enfouissement, sous peine de sanction financière, caractérise une situation d'urgence ; que l'équilibre financier des activités de la requérante sur le territoire de la collectivité, tant en ce qui concerne ses services commerciaux que le service universel, est mis en cause par la délibération litigieuse ; que la taxe instituée par la délibération en litige bouleverse substantiellement l'économie générale du service universel dont est chargée la requérante ; <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ; <br/>
              Vu le code général des collectivités territoriales, notamment ses articles L.O. 6243-1 et suivants et l'article L.O. 6251-2 ;<br/>
<br/>
              Vu le code des postes et communications électroniques ;<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, la société FRANCE TELECOM et, d'autre part, la collectivité de Saint-Barthélemy ainsi que l'Autorité de régulation des communications électroniques et des postes ;<br/>
<br/>
              Vu le procès-verbal de l'audience publique du 17 février 2012 à 10 heures, au cours de laquelle ont été entendus :<br/>
<br/>
              - Me Tiffreau, avocat au Conseil d'Etat et à la Cour de cassation, avocat de la société FRANCE TELECOM ; <br/>
<br/>
              - le représentant de la société FRANCE TELECOM ;<br/>
<br/>
              - Me de Chaisemartin, avocat au Conseil d'Etat et à la Cour de cassation, avocat de la collectivité de Saint-Barthélemy ;<br/>
<br/>
              et à l'issue de laquelle le juge des référés a prolongé l'instruction jusqu'au 21 février 2012 à 12 heures ;<br/>
<br/>
              Vu le nouveau mémoire en défense, enregistré le 21 février 2012, présenté pour la collectivité de Saint-Barthélemy, qui conclut au rejet de la requête ;<br/>
<br/>
              Vu le nouveau mémoire, enregistré le 21 février 2012, présenté pour la société FRANCE TELECOM, qui conclut aux mêmes fins que sa requête par les mêmes moyens ;<br/>
<br/>
<br/>
<br/>
              Considérant qu'aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision. " ; <br/>
<br/>
              Considérant qu'en application du 1° de l'article L.O. 6214-3 du code général des collectivités territoriales, la collectivité de Saint-Barthélemy fixe les règles applicables en matière d'impôts, droits et taxes ; que par une délibération du 28 octobre 2011, le conseil territorial de Saint-Barthélemy a créé une taxe sur les installations électriques et de télécommunications aériennes, afin d'inciter les opérateurs à procéder à l'enfouissement des lignes de transport électriques et de télécommunication ; que la société FRANCE TELECOM demande au juge des référés, sur le fondement de l'article L. 521-1 du code de justice administrative, la suspension de l'exécution de cette délibération ;<br/>
<br/>
              Considérant qu'en application des articles L.O. 6243-1 et L.O. 6251-2 du code général des collectivités territoriales, les délibérations par lesquelles le conseil territorial fixe les règles applicables à Saint-Barthélemy dans les matières énumérées à l'article L.O. 6214-3 de ce code relevant du domaine de la loi peuvent être contestées par la voie d'un recours motivé porté devant le Conseil d'Etat dans les deux mois qui suivent leur publication au Journal Officiel de Saint-Barthélemy ; que l'article L.O. 6243-4 de ce même code dispose que : " Le Conseil d'Etat statue sur la conformité des actes prévus à l'article L.O. 6251-2 au regard de la Constitution, des lois organiques, des engagements internationaux de la France et des principes généraux du droit. / Le Conseil d'Etat se prononce dans les trois mois de sa saisine. (...) " ; que si le législateur organique a, comme l'article 74 de la Constitution l'y autorisait, déterminé les conditions dans lesquelles le Conseil d'Etat exercerait un contrôle juridictionnel spécifique sur certaines catégories d'actes de l'assemblée délibérante de la collectivité d'outre-mer de Saint-Barthélemy au titre des compétences qu'elle exerce dans le domaine de la loi, les modalités particulières du contrôle juridictionnel défini par les articles L.O. 6243-1 à L.O. 6243-5 du code général des collectivités territoriales ne font pas obstacle à l'application de l'article L. 521-1 du code de justice administrative ; que la collectivité de Saint-Barthélemy n'est donc pas fondée à opposer une fin de non-recevoir à la demande de suspension présentée par la société FRANCE TELECOM ;<br/>
<br/>
              Considérant que la possibilité pour le juge des référés d'ordonner la suspension de l'exécution d'une décision administrative est subordonnée notamment à la condition qu'il y ait urgence ; qu'il lui appartient d'apprécier concrètement, compte tenu des justifications fournies par le requérant, si les effets de l'acte litigieux sont de nature à porter à un intérêt public, à la situation du requérant ou aux intérêts qu'il entend défendre une atteinte suffisamment grave et immédiate pour caractériser une urgence justifiant que, sans attendre le jugement de la requête au fond, l'exécution de la décision soit suspendue ; que s'agissant de la contestation d'une délibération créant une taxe, le juge des référés doit, pour vérifier si la condition d'urgence est satisfaite, apprécier la gravité des conséquences que pourraient entraîner, à brève échéance, l'obligation de payer sans délai l'imposition ou les mesures mises en oeuvre ou susceptibles de l'être pour son recouvrement, eu égard aux capacités du contribuable à acquitter les sommes qui lui sont demandées ; que, lorsqu'il est saisi d'une demande de suspension des actes mentionnés à l'article L.O. 6251-2 du code général des collectivités territoriales relevant du domaine de la loi, il appartient au juge des référés du Conseil d'Etat de tenir compte, dans son appréciation de la condition d'urgence, de l'existence d'un recours au fond sur lequel, en application de l'article L.O. 6243-4 du code général des collectivités territoriales, le Conseil d'Etat se prononce dans les trois mois  de sa saisine ;<br/>
<br/>
              Considérant que, pour demander la suspension de la délibération du conseil territorial de Saint-Barthélemy en date du 28 octobre 2011, la société FRANCE TELECOM fait valoir que l'exécution de la délibération contestée lui cause un préjudice financier important, résultant de l'obligation de payer une somme de 194 000 euros, au titre de son réseau aérien de télécommunications d'une longueur de 97 km, avant le 28 février 2012 ; qu'elle n'a pas été en mesure d'anticiper l'instauration d'une telle taxe, rendue exigible à brève échéance ; que l'exécution de la délibération contestée porterait également atteinte à l'intérêt public qui s'attache à la couverture du territoire national par le réseau de téléphonie ; <br/>
<br/>
              Mais considérant, d'une part, qu'il ne ressort pas des pièces versées au dossier soumis au juge des référés que l'obligation de payer sans délai la taxe litigieuse entraînerait à brève échéance, pour la société FRANCE TELECOM, des conséquences graves, eu égard aux capacités financières de la société requérante ; que l'exécution de la délibération n'est ainsi susceptible d'avoir qu'un impact limité sur la situation et l'activité de la société requérante ; que par suite, l'exécution de la délibération attaquée n'est pas de nature, à la date de la présente ordonnance, à porter une atteinte suffisamment grave et immédiate aux intérêts de la société requérante pour justifier que cette exécution soit suspendue en référé dans l'attente du jugement de la requête au fond ; <br/>
<br/>
              Considérant, d'autre part, qu'il ne ressort pas des pièces versées au dossier que l'exécution de la délibération serait par elle-même susceptible de mettre en cause la couverture du territoire de la collectivité de Saint-Barthélemy par le réseau de téléphonie ; que l'exécution de la délibération contestée ne peut ainsi être regardée comme portant une atteinte grave et immédiate à l'intérêt public qui s'attache à la couverture du territoire national par le réseau de téléphonie justifiant que, sans attendre le jugement de la requête au fond, l'exécution de la délibération soit immédiatement suspendue ; <br/>
<br/>
              Considérant qu'il résulte de ce qui précède que la condition d'urgence, requise par l'article L. 521-1 du code de justice administrative, n'est pas remplie ; qu'ainsi, sans qu'il soit besoin d'examiner si la société FRANCE TELECOM fait état de moyens propres à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision, les conclusions de la requête tendant à la suspension de la délibération du 28 octobre 2011 du conseil territorial de Saint-Barthélemy portant création d'une taxe sur les installations électriques et de télécommunications aériennes doivent être rejetées et, par voie de conséquence, les conclusions présentées par la société requérante sur le fondement des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
              Considérant qu'il y a lieu, dans les circonstances de l'espèce, de faire application des dispositions de l'article L. 761-1 du code de justice administrative et de mettre à la charge de la société FRANCE TELECOM le versement à la collectivité de Saint-Barthélemy de la somme de 2 000 euros ;<br/>
<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de la société FRANCE TELECOM est rejetée.<br/>
Article 2 : La société FRANCE TELECOM versera à la collectivité de Saint-Barthélemy une somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente ordonnance sera notifiée à la société FRANCE TELECOM, à la collectivité de Saint-Barthélemy et à l'Autorité de régulation des communications électroniques et des postes.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">46-01-08 OUTRE-MER. DROIT APPLICABLE DANS LES COLLECTIVITÉS D'OUTRE-MER ET EN NOUVELLE-CALÉDONIE. ORGANISATION JUDICIAIRE ET PARTICULARITÉS CONTENTIEUSES. - ACTES DE LA COLLECTIVITÉ D'OUTRE-MER DE SAINT-BARTHÉLEMY RELEVANT DU DOMAINE DE LA LOI ET SOUMIS À UN CONTRÔLE JURIDICTIONNEL PARTICULIER (ART. L.O. 6243-1 À L.O. 6243-5 DU CGCT) - 1) RECEVABILITÉ D'UNE REQUÊTE EN RÉFÉRÉ-SUSPENSION À LEUR ÉGARD - EXISTENCE - 2) PRISE EN COMPTE DU DÉLAI DANS LEQUEL LE CONSEIL D'ETAT DOIT STATUER - EXISTENCE, AU STADE DE L'APPRÉCIATION DE LA CONDITION D'URGENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-035-02 PROCÉDURE. PROCÉDURES INSTITUÉES PAR LA LOI DU 30 JUIN 2000. RÉFÉRÉ SUSPENSION (ART. L. 521-1 DU CODE DE JUSTICE ADMINISTRATIVE). - ACTES DE LA COLLECTIVITÉ D'OUTRE-MER DE SAINT-BARTHÉLEMY RELEVANT DU DOMAINE DE LA LOI ET SOUMIS À UN CONTRÔLE JURIDICTIONNEL PARTICULIER (ART. L.O. 6243-1 À L.O. 6243-5 DU CGCT) - 1) RECEVABILITÉ D'UNE REQUÊTE EN RÉFÉRÉ-SUSPENSION À LEUR ÉGARD - EXISTENCE - 2) PRISE EN COMPTE DU DÉLAI DANS LEQUEL LE CONSEIL D'ETAT DOIT STATUER - EXISTENCE, AU STADE DE L'APPRÉCIATION DE LA CONDITION D'URGENCE.
</SCT>
<ANA ID="9A"> 46-01-08 1) Si le législateur organique a, comme l'article 74 de la Constitution l'y autorisait, déterminé les conditions dans lesquelles le Conseil d'Etat exercerait un contrôle juridictionnel spécifique sur certaines catégories d'actes de l'assemblée délibérante de la collectivité d'outre-mer de Saint-Barthélemy au titre des compétences qu'elle exerce dans le domaine de la loi, les modalités particulières du contrôle juridictionnel défini par les articles L.O. 6243-1 à L.O. 6243-5 du code général des collectivités territoriales ne font pas obstacle à l'application de l'article L. 521-1 du code de justice administrative.,,2) Lorsqu'il est saisi d'une demande de suspension des actes mentionnés à l'article L.O. 6251-2 du code général des collectivités territoriales relevant du domaine de la loi, il appartient au juge des référés du Conseil d'Etat de tenir compte, dans son appréciation de la condition d'urgence, de l'existence d'un recours au fond sur lequel, en application de l'article L.O. 6243-4 du code général des collectivités territoriales, le Conseil d'Etat se prononce dans les trois mois  de sa saisine.</ANA>
<ANA ID="9B"> 54-035-02 1) Si le législateur organique a, comme l'article 74 de la Constitution l'y autorisait, déterminé les conditions dans lesquelles le Conseil d'Etat exercerait un contrôle juridictionnel spécifique sur certaines catégories d'actes de l'assemblée délibérante de la collectivité d'outre-mer de Saint-Barthélemy au titre des compétences qu'elle exerce dans le domaine de la loi, les modalités particulières du contrôle juridictionnel défini par les articles L.O. 6243-1 à L.O. 6243-5 du code général des collectivités territoriales ne font pas obstacle à l'application de l'article L. 521-1 du code de justice administrative.,,2) Lorsqu'il est saisi d'une demande de suspension des actes mentionnés à l'article L.O. 6251-2 du code général des collectivités territoriales relevant du domaine de la loi, il appartient au juge des référés du Conseil d'Etat de tenir compte, dans son appréciation de la condition d'urgence, de l'existence d'un recours au fond sur lequel, en application de l'article L.O. 6243-4 du code général des collectivités territoriales, le Conseil d'Etat se prononce dans les trois mois  de sa saisine.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
