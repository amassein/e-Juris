<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026423494</ID>
<ANCIEN_ID>JG_L_2012_09_000000361787</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/42/34/CETATEXT000026423494.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 26/09/2012, 361787, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2012-09-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>361787</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GADIOU, CHEVALLIER</AVOCATS>
<RAPPORTEUR>Mme Pascale Fombeur</RAPPORTEUR>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2012:361787.20120926</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 9 août 2012 au secrétariat du contentieux du Conseil d'Etat, présentée pour la S.A.S. Laboratoire Addmedica, dont le siège social est 101, rue Saint-Lazare à Paris (75009), représentée par son président ; la S.A.S. Laboratoire Addmedica demande au juge des référés du Conseil d'Etat :<br/>
<br/>
              1°) d'ordonner, sur le fondement de l'article L. 521-1 du code de justice administrative, la suspension de l'exécution de la décision du Comité économique des produits de santé du 21 juin 2012 réitérant le refus du prix proposé pour la spécialité Siklos 100 mg et de la décision implicite des ministres chargés de la santé et de la sécurité sociale refusant d'inscrire cette spécialité sur les listes mentionnées à l'article L. 162-17 du code de la sécurité sociale et à l'article L. 5123-2 du code de la santé publique ;<br/>
<br/>
              2°) d'enjoindre, dans l'attente de la décision sur le recours au fond, à titre principal, d'une part, au Comité économique des produits de santé d'accepter la dernière proposition de prix pour Siklos 100 mg à hauteur de 110 euros la boîte, assorti des ristournes de 50 % au-delà de 1 100 patients et de 95 % au-delà de 2 000 patients, et de transmettre à la société requérante le projet de conventionnement correspondant et, d'autre part, aux ministres chargés de la sécurité sociale et de la santé de procéder, dès la fixation de ce prix, à l'inscription de la spécialité sur les listes mentionnées aux articles L. 162-17 du code de la sécurité sociale et L. 5123-2 du code de la santé publique, à titre subsidiaire, aux mêmes ministres de procéder à l'inscription de la spécialité sur la liste mentionnée à l'article L. 5123-2 du code de la santé publique ou, à défaut, au comité et aux mêmes ministres de réexaminer, dans les quinze jours à compter du prononcé de l'ordonnance, la demande d'inscription du Siklos 100 mg sur les deux listes précitées et la demande de fixation de son prix de vente au public, sur la base de la dernière proposition de prix formulée par la société ;<br/>
<br/>
              3°) d'assortir l'injonction d'une astreinte d'un montant de 1 500 euros par jour de retard à compter du prononcé de l'ordonnance ; <br/>
<br/>
              4°) de mettre à la charge de l'Etat le versement de la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              elle soutient que :<br/>
              - la condition d'urgence est remplie du fait, d'une part, des conséquences graves en termes de santé publique et de la perte de chance des patients de bénéficier d'un traitement approprié et, d'autre part, du préjudice financier résultant pour elle de l'impossibilité de commercialiser le Siklos 100 mg comme médicament remboursable ;<br/>
              - la décision du Comité économique des produits de santé est irrégulière, dès lors qu'elle reprend l'appréciation de la Commission de la transparence comparant Siklos à Hydrea et dégradant ainsi le niveau d'amélioration du service médical rendu ;<br/>
              - elle a été irrégulièrement privée de la garantie de prix européen prévue par l'accord-cadre conclu entre le Comité économique des produits de santé et le syndicat Les entreprises du médicament ;<br/>
              - le prix de Droxia aux Etats-Unis n'est pas un comparateur pertinent ;<br/>
              - le prix de vente de Siklos 100 mg qu'elle propose est économiquement justifié au regard de toutes ses composantes ;<br/>
              - ce prix ne porte pas atteinte à l'objectif d'équilibre financier des régimes de sécurité sociale ;<br/>
              - la régularisation de l'usage d'Hydrea hors AMM porte atteinte à l'exclusivité commerciale accordée à Siklos par la réglementation européenne ;<br/>
              - le refus d'inscription de Siklos 100 mg sur la liste de l'article L. 162-17 du code de la sécurité sociale est illégal, dès lors que le refus du Comité économique des produits de santé est illégal, que le ministre prend en compte de façon irrégulière les critères de fixation de prix de l'article L. 162-14-4 du même code et que la spécialité remplit les conditions d'inscription au remboursement ; <br/>
              - le refus d'inscription sur la liste des produits agréés à l'usage des collectivités publiques est illégal, les deux listes étant distinctes et le ministre ne pouvant valablement se fonder sur le risque de contournement du refus de prix en ville ;<br/>
<br/>
              Vu les décisions dont la suspension de l'exécution est demandée ;<br/>
<br/>
              Vu la copie de la requête à fin d'annulation de ces décisions ;<br/>
<br/>
              Vu le mémoire en défense, enregistré le 10 septembre 2012, présenté par le Comité économique des produits de santé, qui conclut au rejet de la requête ; il soutient que :<br/>
              - la condition d'urgence n'est pas remplie dès lors que les malades drépanocytaires sont actuellement pris en charge de manière satisfaisante en France et que le laboratoire n'établit pas la réalité des difficultés économiques qui résulteraient de l'acceptation du prix proposé par le Comité ; <br/>
              - son courrier du 26 juin 2012 ne constitue pas une décision mais une invitation à la poursuite des négociations en cours ;<br/>
              - les principes législatifs de fixation des prix des médicaments doivent se conjuguer avec l'objectif d'équilibre financier des régimes de sécurité sociale ; <br/>
              - les critères de fixation du prix qu'il a antérieurement proposés n'ont pas méconnu les dispositions des articles L. 162-16-4 du code de la sécurité sociale ;<br/>
<br/>
              Vu le mémoire en défense, enregistré le 10 septembre 2012, présenté par le ministre des affaires sociales et de la santé, qui conclut au rejet de la requête ; il soutient que :<br/>
              - le courrier du Comité économique des produits de santé du 26 juin 2012 n'a pas le caractère d'une décision et le délai imparti à l'administration pour se prononcer sur la demande d'inscription sur les listes des médicaments remboursables et des produits agréés à l'usage des collectivités publiques n'est pas expiré ;<br/>
              - la condition d'urgence n'est pas remplie, le recours du requérant étant prématuré, le préjudice économique invoqué n'étant pas établi et les patients ayant accès à une prise en charge adaptée ;<br/>
              - les refus d'inscription et la décision du CEPS reposent sur des fondements distincts ;<br/>
              - l'administration était fondée à regarder le prix proposé par la société requérante comme injustifié au regard des critères de l'article L. 162-16-4 du code de la sécurité sociale ;<br/>
               - aucune nouvelle autorisation de mise sur le marché n'ayant été accordée, il ne peut être allégué d'atteinte à l'exclusivité commerciale dont bénéficie Siklos 100 mg ;<br/>
              - l'inscription de Siklos 100 mg sur la liste des médicaments agréés à l'usage des collectivités pouvait être refusée tant que cette spécialité n'était pas inscrite sur la liste des médicaments remboursables, de manière à éviter les risques de rupture de prise en charge du traitement pour les patients et les risques de contournement du refus de prix opposé par le CEPS ; <br/>
<br/>
              Vu l'intervention, enregistrée le 11 septembre 2012, présentée par la Fédération des malades drépanocytaires et thalassémiques, dont le siège est situé Hôpital Henri Mondor, 51 avenue Maréchal de Lattre de Tassigny à Créteil (94000), représentée par sa présidente ; la Fédération des malades drépanocytaires et thalassémiques demande au Conseil d'Etat de faire droit aux conclusions de la requête de la S.A.S. Laboratoire Addmedica ; elle se réfère aux moyens exposés dans la requête de la société et soutient en outre que : <br/>
              - elle est recevable à intervenir dès lors qu'elle a pour objet la défense des intérêts et droits des patients drépanocytaires en France ;<br/>
              - il y a urgence pour les malades, dès lors que le traitement hors autorisation de mise sur le marché, qui ne peut désormais être ni prescrit ni remboursé, présente des inconvénients majeurs ;<br/>
              Vu le mémoire en réplique, enregistré le 12 septembre 2012, présenté pour la S.A.S. Laboratoire Addmedica, qui reprend les conclusions de sa requête et les mêmes moyens ; elle soutient en outre que la décision du CEPS, adoptée dans sa séance du 21 juin 2012, confirmée par un entretien téléphonique du 16 juillet 2012, constitue bien une décision de refus du prix proposé et non une invitation à la poursuite des négociations en cours ; <br/>
<br/>
              Vu les autres pièces du dossier ; <br/>
	Vu le code de la santé publique ; <br/>
              Vu le code de la sécurité sociale ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, la S.A.S Laboratoire Addmedica, d'autre part, le Comité économique des produits de santé et le ministre des affaires sociales et de la santé ainsi que la Fédération des malades drépanocytaires et thalassémiques ;<br/>
<br/>
              Vu le procès-verbal de l'audience publique du 14 septembre 2012 à 10 heures, au cours de laquelle ont été entendus :<br/>
<br/>
              - Me Chevallier, avocat au Conseil d'Etat et à la Cour de cassation, avocat de la S.A.S. Laboratoire Addmedica ;<br/>
<br/>
              - les représentants de la S.A.S. Laboratoire Addmedica ;<br/>
<br/>
- les représentants du ministre des affaires sociales et de la santé ;<br/>
<br/>
              - le représentant du Comité économique des produits de santé ;<br/>
<br/>
              - la représentante de la Fédération des malades drépanocytaires et thalassémiques ;<br/>
<br/>
              et à l'issue de laquelle le juge des référés a décidé de prolonger l'instruction jusqu'au 21 septembre 2012, puis jusqu'au 25 septembre 2012 ;<br/>
<br/>
              Vu le nouveau mémoire, enregistré le 18 septembre 2012, présenté pour la S.A.S. Laboratoire Addmedica, qui reprend les conclusions de sa requête et les mêmes moyens ; elle soutient en outre que la somme des coûts fixes et des coûts variables s'élève, eu égard aux hypothèses de vente retenues au cours de l'audience, à 90,35 euros par boîte de Siklos 100 mg et à 409,46 euros par boîte de Siklos 1000 mg ;<br/>
<br/>
              Vu le nouveau mémoire, enregistré le 21 septembre 2012, présenté par le Comité économique des produits de santé, qui produit sa décision du 20 septembre 2012 fixant les prix du Siklos 100 mg et 1000 mg et le courrier de notification de cette décision à la S.A.S. Laboratoire Addmedica ;<br/>
<br/>
              Vu les nouveaux mémoires, enregistrés les 21 et 25 septembre 2012, présentés par le ministre des affaires sociales et de la santé, qui conclut à ce qu'il n'y ait lieu de statuer sur la requête de la S.A.S. Laboratoire Addmedica ; il soutient que la requête a perdu son objet dès lors que le Comité économique des produits de santé a pris le 20 septembre 2012 une décision fixant les prix de Siklos dans ses présentations 100 mg et 1000 mg et qu'il a pour sa part, le 21 septembre 2012, inscrit la spécialité, sous ses deux dosages, sur la liste des médicaments remboursables aux assurés sociaux et sur la liste des médicaments agréés à l'usage des collectivités publiques ;<br/>
<br/>
              Vu le nouveau mémoire, enregistré le 25 septembre 2012, présenté pour la S.A.S. Laboratoire Addmedica, qui reprend les conclusions de sa requête et les mêmes moyens ; elle soutient en outre que la requête conserve son objet, faute de légalité et donc d'opposabilité de la décision du Comité économique des produits de santé du 20 septembre 2012 ;<br/>
<br/>
<br/>
<br/>Sur l'intervention de la Fédération des malades drépanocytaires et thalassémiques :<br/>
<br/>
              1. Considérant que la Fédération des malades drépanocytaires et thalassémiques justifie d'un intérêt à demander la suspension des décisions contestées ; qu'ainsi, son intervention au soutien de la requête est recevable ;<br/>
<br/>
              Sur les conclusions de la requête de la S.A.S. Laboratoire Addmedica :<br/>
<br/>
              2. Considérant qu'aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision " ;<br/>
<br/>
              3. Considérant que, d'une part, aux termes du premier alinéa de l'article L. 162-17 du code de la sécurité sociale : " Les médicaments spécialisés (...) ne peuvent être pris en charge ou donner lieu à remboursement par les caisses d'assurance maladie, lorsqu'ils sont dispensés en officine, que s'ils figurent sur une liste établie dans les conditions fixées par décret en Conseil d'Etat (...) " ; qu'aux termes du premier alinéa de l'article L. 162-16-4 du même code : " Le prix de vente au public de chacun des médicaments mentionnés au premier alinéa de l'article L. 162-17 est fixé par convention entre l'entreprise exploitant le médicament et le Comité économique des produits de santé conformément à l'article L. 162-17-4 ou, à défaut, par décision du comité, sauf opposition conjointe des ministres concernés qui arrêtent dans ce cas le prix dans un délai de quinze jours après la décision du comité (...) " ; que, d'autre part, aux termes du premier alinéa de l'article L. 5123-2 du code de la santé publique : " L'achat, la fourniture, la prise en charge et l'utilisation par les collectivités publiques des médicaments définis aux articles L. 5121-8, L. 5121-9-1, L. 5121-12, L. 5121-13 et L. 5121-14-1 (...) sont limités, dans les conditions propres à ces médicaments fixées par le décret mentionné à l'article L. 162-17 du code de la sécurité sociale, aux produits agréés dont la liste est établie par arrêté des ministres chargés de la santé et de la sécurité sociale " ; <br/>
<br/>
              4. Considérant que la S.A.S. Laboratoire Addmedica a obtenu, par décision de la Commission européenne du 29 juin 2007, une autorisation de mise sur le marché pour sa spécialité Siklos 1000 mg, destinée à la prévention des crises vaso-occlusives douloureuses récurrentes chez le patient souffrant de drépanocytose symptomatique, étendue par décision du 28 février 2011 au Siklos 100 mg, dont le dosage facilite l'administration à l'enfant ; que, par courrier reçu par le ministère de la santé et des sports le 12 avril 2011, le laboratoire a sollicité l'inscription de la spécialité Siklos 100 mg sur la liste des spécialités remboursables par l'assurance maladie et sur celle des spécialités agréées à l'usage des collectivités publiques prévues respectivement par les articles L. 162-17 du code de la sécurité sociale et L. 5123-2 du code de la santé publique précités ; que des décisions implicites de rejet de ces demandes sont nées à l'issue du délai de cent quatre-vingts jours prévu par l'article R. 163-9 du code de la sécurité sociale ; que, par courrier du 13 décembre 2011, le président du Comité économique des produits de santé a fait savoir au laboratoire que ce comité refusait la fixation d'un prix fabricant hors taxe supérieur à 13,40 euros par boîte ; qu'à la suite de l'ordonnance du juge des référés du 13 mars 2012 et des nouveaux documents adressés au Comité et examinés par ce dernier dans sa séance du 21 juin 2012, le président du Comité a fait savoir au laboratoire, par courrier du 26 juin 2012, que les éléments fournis confortaient cette instance dans la juste appréciation du prix qu'elle avait proposé au laboratoire ; que la S.A.S. Laboratoire Addmedica demande la suspension de l'exécution de cet acte ainsi que des décisions implicites refusant l'inscription de la spécialité Siklos 100 mg sur les listes prévues aux articles L. 162-17 du code de la sécurité sociale et L. 5123-2 du code de la santé publique ;<br/>
<br/>
              5. Considérant que, postérieurement à l'audience du 14 septembre 2012, le Comité économique des produits de santé a fixé les prix des spécialités Siklos 100 mg et 1000 mg  par une décision du 20 septembre 2012, publiée au Journal officiel du 25 septembre suivant ; que si sa légalité est contestée par la S.A.S. Laboratoire Addmedica, cette décision n'en a pas moins un caractère exécutoire ; que, par décisions du 21 septembre 2012, également publiées au Journal officiel du 25 septembre suivant, la ministre des affaires sociales et de la santé et le ministre délégué auprès du ministre de l'économie et des finances, chargé du budget, ont inscrit les spécialités Siklos 100 mg et Siklos 1000 mg sur la liste des médicaments remboursables aux assurés sociaux et sur la liste des médicaments agréés à l'usage des collectivités publiques ; que, dans ces conditions, les conclusions de la S.A.S. Laboratoire Addmedica à fin de suspension et d'injonction sont devenues sans objet ; que, par suite, il n'y a pas lieu d'y statuer ;<br/>
<br/>
              6. Considérant qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par la S.A.S. Laboratoire Addmedica au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
------------------<br/>
Article 1er : L'intervention de la Fédération des malades drépanocytaires et thalassémiques est admise. <br/>
Article  2 : Il n'y a pas lieu de statuer sur les conclusions à fin de suspension et d'injonction de la S.A.S. Laboratoire Addmedica.<br/>
Article 3 : Le surplus des conclusions de la requête de la S.A.S. Laboratoire Addmedica est rejeté.<br/>
Article 4 : La présente ordonnance sera notifiée à la S.A.S. Laboratoire Addmedica, à la ministre des affaires sociales et de la santé, au Comité économique des produits de santé et à la Fédération des malades drépanocytaires et thalassémiques.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
