<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033970241</ID>
<ANCIEN_ID>JG_L_2017_02_000000396810</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/97/02/CETATEXT000033970241.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème chambre, 01/02/2017, 396810, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-02-01</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>396810</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MASSE-DESSEN, THOUVENIN, COUDRAY ; SCP LYON-CAEN, THIRIEZ</AVOCATS>
<RAPPORTEUR>M. Jean Sirinelli</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Olivier Henrard</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2017:396810.20170201</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme A...B...a demandé au tribunal administratif de Clermont-Ferrand, d'une part, l'annulation de l'arrêté du 8 juin 2012 par lequel le maire de Cournon-d'Auvergne a refusé de reconnaître 1'imputabilité au service de l'accident dont elle a été victime le 10 mai 2011, ensemble la décision du 27 décembre 2012 ayant rejeté son recours gracieux contre cet arrêté, et, d'autre part, l'annulation de l'arrêté du 29 juin 2012 par lequel le même maire a réduit à 13/35ème sa durée hebdomadaire de travail à compter du 1er juillet 2012, ensemble la décision du 27 décembre 2012 par laquelle il a rejeté son recours gracieux dirigé contre cet arrêté. Par un jugement n°s 1300314, 1300315 du 21 janvier 2014, le tribunal administratif de Clermont-Ferrand a joint ces demandes et les a rejetées.<br/>
<br/>
              Par un arrêt n° 14LY00866 du 8 décembre 2015, la cour administrative d'appel de Lyon a, sur appel de MmeB..., annulé ce jugement et les décisions attaquées et a enjoint au maire de reconnaître l'imputabilité au service de la maladie de Mme B...pour la période du 10 mai au 30 novembre 2011, dans le délai de deux mois à compter de la notification de son arrêt.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 8 février et 9 mai 2016 au secrétariat du contentieux du Conseil d'Etat, la commune de Cournon-d'Auvergne demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de Mme B...;<br/>
<br/>
              3°) de mettre à la charge de Mme B...la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la loi n° 84-53 du 26 janvier 1984 ;<br/>
              - le décret n° 2000-815 du 25 août 2000 ;<br/>
              - le décret n° 2001-623 du 12 juillet 2001 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean Sirinelli, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Olivier Henrard, rapporteur public. <br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Lyon-Caen, Thiriez, avocat de la commune de Cournon-d'Auvergne et à la SCP Masse-Dessen, Thouvenin, Coudray, avocat de Mme B...;<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que MmeB..., fonctionnaire de la commune de Cournon-d'Auvergne, a été placée en congé de maladie le 10 mai 2011 ; que, par un arrêté du 8 juin 2012, le maire de la commune a refusé de reconnaître l'imputabilité au service des troubles dont souffrait l'intéressée, décision confirmée le 27 décembre 2012 à la suite du recours gracieux formé contre cet arrêté ; que, par ailleurs, le maire de Cournon-d'Auvergne, par un arrêté du 29 juin 2012, a fixé à treize heures la durée hebdomadaire de travail de Mme B...à compter du 1er juillet 2012, décision également confirmée le 27 décembre 2012 ; que, par un arrêt du 8 décembre 2015 contre lequel la commune de Cournon-d'Auvergne se pourvoit en cassation, la cour administrative d'appel de Lyon a annulé le jugement du tribunal administratif de Clermont-Ferrand rejetant le recours formé par Mme B...contre ces décisions ainsi que l'ensemble de ces dernières et a enjoint au maire de reconnaître l'imputabilité au service de la maladie de Mme B...; <br/>
<br/>
              Sur l'arrêt attaqué en tant qu'il se prononce sur le refus d'imputabilité au service de la maladie de Mme B...: <br/>
<br/>
              2. Considérant qu'aux termes de l'article 57 de la loi du 26 janvier 1984 portant dispositions statutaires relatives à la fonction publique territoriale, dans sa rédaction applicable à la date du litige : " Le fonctionnaire en activité a droit : (...) 2° A des congés de maladie dont la durée totale peut atteindre un an pendant une période de douze mois consécutifs en cas de maladie dûment constatée mettant l'intéressé dans l'impossibilité d'exercer ses fonctions. (...) / Toutefois, si la maladie provient de l'une des causes exceptionnelles prévues à l'article L. 27 du code des pensions civiles et militaires de retraite ou d'un accident survenu dans l'exercice ou à l'occasion de l'exercice de ses fonctions, le fonctionnaire conserve l'intégralité de son traitement jusqu'à ce qu'il soit en état de reprendre son service ou jusqu'à sa mise à la retraite. (...) Dans le cas visé à l'alinéa précédent, l'imputation au service de l'accident ou de la maladie est appréciée par la commission de réforme instituée par le régime des pensions des agents des collectivités locales (...) " ; <br/>
<br/>
              3. Considérant qu'il ressort des énonciations de l'arrêt attaqué que, pour juger que la maladie de Mme B...était imputable au service, la cour administrative d'appel de Lyon a relevé qu'un refus de paiement d'heures supplémentaires avait été opposé à Mme B...lors de deux entretiens avec sa hiérarchie les 14 octobre 2010 et 21 avril 2011, que des attestations établies par un responsable syndical et deux médecins missionnés par la commune faisaient un lien entre son état de santé et son travail et qu'enfin, la commission départementale de réforme avait admis l'imputabilité de la maladie au service lors de sa séance du 2 mai 2012 ; que toutefois, si Mme B...faisait valoir que sa pathologie était ainsi la conséquence du refus de paiement d'heures supplémentaires qui lui avait été opposé ainsi, plus généralement, que de ses conditions de travail, aucune des pièces du dossier, et notamment pas les avis des médecins et de la commission de réforme qui ne lient pas l'administration, ne permettait d'identifier un incident ou un dysfonctionnement du service susceptible d'être regardé comme pouvant constituer la cause de la maladie ; qu'ainsi, en jugeant que celle-ci était imputable au service, la cour administrative d'appel de Lyon a entaché son arrêt d'erreur de qualification juridique ; que la commune de Cournon-d'Auvergne est fondée à en demander, pour ce motif et dans cette mesure, l'annulation ; <br/>
<br/>
              Sur l'arrêt attaqué en tant qu'il se prononce sur la modification du temps de travail de Mme B...:<br/>
<br/>
              4. Considérant, en premier lieu, qu'aux termes de l'article 107 de la loi du 26 janvier 1984 : " Le fonctionnaire nommé dans un emploi à temps non complet doit être affilié à la Caisse nationale de retraite des agents des collectivités locales, s'il consacre à son service un nombre minimal d'heures de travail fixé par délibération de cette caisse " ; que par une délibération du 3 octobre 2001, le conseil d'administration de la Caisse nationale de retraite des agents des collectivités locales (CNRACL) a fixé ce seuil d'affiliation des fonctionnaires territoriaux à temps non complet aux 4/5e de la durée légale hebdomadaire de travail des fonctionnaires à temps complet au 1er janvier 2002 ; qu'aux termes de l'article 1er du décret du 25 août 2000 : " La durée du travail effectif est fixée à trente-cinq heures par semaine dans les services et établissements publics administratifs de l'Etat ainsi que dans les établissements publics locaux d'enseignement. / Le décompte du temps de travail est réalisé sur la base d'une durée annuelle de travail effectif de 1 607 heures maximum, sans préjudice des heures supplémentaires susceptibles d'être effectuées (...) " ; qu'aux termes de l'article 1er du décret du 12 juillet 2001 : " Les règles relatives à la définition, à la durée et à l'aménagement du temps de travail applicables aux agents des collectivités territoriales et des établissements publics en relevant sont déterminées dans les conditions prévues par le décret du 25 août 2000 (...) " ;<br/>
<br/>
              5. Considérant que les mesures prises à l'égard d'agents publics qui, compte tenu de leurs effets, ne peuvent être regardées comme leur faisant grief, constituent de simples mesures d'ordre intérieur insusceptibles de recours ; qu'il en va ainsi des mesures qui, tout en modifiant leur affectation ou les tâches qu'ils ont à accomplir, ne portent pas atteinte aux droits et prérogatives qu'ils tiennent de leur statut ou à l'exercice de leurs droits et libertés fondamentaux, ni n'emportent perte de responsabilités ou de rémunération ; que le recours contre de telles mesures, à moins qu'elles ne traduisent une discrimination, est irrecevable ;<br/>
<br/>
              6. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, par un arrêté du 29 décembre 2006, le maire de Cournon-d'Auvergne a titularisé Mme B... dans un emploi d'agent des services techniques dont la quotité de temps de travail était de 20/35ème et dont l'objet était de surveiller les entrées et les sorties d'école ; que par deux arrêtés du 26 février 2008, le maire lui a confié, en outre, la responsabilité des régies de recettes des sanitaires automatiques et des droits de place, qu'elle a assumée à compter du 1er juillet 2008 ; que, pour tenir compte de ces missions supplémentaires, Mme B...a été nommée, par un arrêté du 8 juillet 2008, sur un emploi d'agent technique dont la quotité était de 30/35ème ; que si Mme B...a fait savoir au maire, le 27 mars 2012, qu'elle souhaitait voir ramener son temps de travail à sa mission de surveillance des entrées et sorties d'école, elle ne lui a pas demandé qu'il soit réduit au 13/35ème de la durée légale annuelle du temps de travail ; que l'arrêté du 29 juin 2012 fixant à 13h la durée hebdomadaire de travail de Mme B...à compter du 1er juillet 2012 emportait donc pour l'intéressée une perte de rémunération à laquelle elle n'avait pas consenti ; que l'arrêté du 29 juin 2012 et la décision du 27 décembre 2012 rejetant son recours gracieux contre cet arrêté lui faisaient donc grief ; que, par suite, le moyen tiré de ce que la cour administrative d'appel de Lyon aurait commis une erreur de droit en jugeant recevables les conclusions de Mme B...dirigées contre ces décisions, doit être écarté ;<br/>
<br/>
              7. Considérant, en deuxième lieu, qu'aux termes du I de l'article 97 de la loi du 26 janvier 1984 : " Un emploi ne peut être supprimé qu'après avis du comité technique sur la base d'un rapport présenté par la collectivité territoriale ou l'établissement public. (...) La modification du nombre d'heures de service hebdomadaire afférent à un emploi permanent à temps non complet n'est pas assimilée à la suppression d'un emploi comportant un temps de service égal, lorsque la modification n'excède pas 10 % du nombre d'heures de service afférent à l'emploi en question et lorsqu'elle n'a pas pour effet de faire perdre le bénéfice de l'affiliation à la Caisse nationale de retraites des agents des collectivités locales (...). " ;<br/>
<br/>
              8. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que l'avis du comité technique a été recueilli le 6 juin 2012 sur la création d'un poste à temps non complet de 13 heures hebdomadaires ; que la modification du temps de travail de Mme B...de 30 à 13h par l'arrêté du 29 juin 2012 a toutefois excédé 10% du nombre d'heures de service afférent à l'emploi occupé par l'intéressée ; qu'en outre, la quotité de 13/35ème de la durée légale annuelle du temps de travail est inférieure au seuil d'affiliation à la CNRACL fixé par la délibération de son conseil d'administration du 3 octobre 2001 mentionnée au point 4 ; que l'arrêté du 29 juin 2012 doit donc être assimilé à une suppression d'emploi au sens des dispositions citées au point 7 ; qu'il s'ensuit qu'en jugeant que l'avis du comité technique aurait aussi dû être recueilli sur cette suppression d'emploi, la cour administrative d'appel de Lyon n'a pas commis d'erreur de droit ; <br/>
<br/>
              9. Considérant, en troisième lieu, que si les actes administratifs doivent être pris selon les formes et conformément aux procédures prévues par les lois et règlements, un vice affectant le déroulement d'une procédure administrative préalable, suivie à titre obligatoire ou facultatif, n'est de nature à entacher d'illégalité la décision prise que s'il ressort des pièces du dossier qu'il a été susceptible d'exercer, en l'espèce, une influence sur le sens de la décision prise ou qu'il a privé les intéressés d'une garantie ;<br/>
<br/>
              10. Considérant qu'en estimant que l'absence de consultation du comité technique sur une suppression de poste avait été, en l'espèce, susceptible de priver Mme B...d'une garantie, la cour administrative d'appel n'a pas dénaturé les pièces du dossier ; que la commune de Cournon-d'Auvergne n'est donc pas fondée à soutenir que les juges d'appel ont commis une erreur de droit en jugeant que l'arrêté du 29 juin 2012 et la décision du 27 décembre 2012 rejetant son recours gracieux contre cet arrêté étaient entachés d'illégalité en ce que l'avis du comité technique n'avait pas été recueilli sur une suppression d'emploi ;<br/>
<br/>
              11. Considérant qu'il résulte de ce qui précède que les conclusions de la commune tendant à l'annulation de l'arrêt attaqué en tant qu'il se prononce sur la modification du temps de travail de Mme B...doivent être rejetées ; <br/>
<br/>
              Sur les conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative : <br/>
<br/>
              12. Considérant qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions des parties tendant à l'application des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Lyon du 8 décembre 2015 est annulé en tant qu'il se prononce sur le refus d'imputabilité au service de la maladie de MmeB....<br/>
Article 2 : L'affaire est renvoyée dans cette mesure à la cour administrative d'appel de Lyon.<br/>
Article 3 : Le surplus des conclusions du pourvoi de la commune de Cournon-d'Auvergne ainsi que la demande de Mme B...présentée au titre de l'article L. 761-1 du code de justice administrative sont rejetés.<br/>
Article 4 : La présente décision sera notifiée à la commune de Cournon-d'Auvergne et à Mme A...B....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
