<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037076511</ID>
<ANCIEN_ID>JG_L_2018_06_000000421333</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/07/65/CETATEXT000037076511.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 13/06/2018, 421333, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-06-13</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>421333</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GADIOU, CHEVALLIER ; SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2018:421333.20180613</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme B...A...a demandé au juge des référés du tribunal administratif de Paris, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, à titre principal, d'enjoindre à l'Assistance publique - Hôpitaux de Paris de prendre toutes mesures utiles afin de permettre le transfert des gamètes de M. C...E...vers un établissement de santé situé dans l'Union européenne, autorisé à pratiquer les procréations médicalement assistées et qui lui sera indiqué par Mme A...et, à titre subsidiaire, de saisir la Cour de justice de l'Union européenne d'une question préjudicielle relative à l'interprétation des articles 7 et 9 de la charte des droits fondamentaux de l'Union européenne. Par une ordonnance n° 1808020 du 25 mai 2018, le juge des référés du tribunal administratif de Paris a rejeté sa demande.<br/>
<br/>
              Par une requête, enregistrée le 8 juin 2018 au secrétariat du contentieux du Conseil d'Etat, Mme A...demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) de faire droit à sa demande de première instance.<br/>
<br/>
<br/>
<br/>
              Elle soutient que :<br/>
              - la condition d'urgence est remplie, les gamètes de Matthieu E...étant susceptibles d'être détruites à tout moment depuis son décès le 29 septembre 2017 et les possibilités de procéder à une insémination artificielle dans d'autres Etats membres étant enfermées dans un délai limité à compter de cette date ;<br/>
              - la décision implicite de rejet de l'Assistance Publique - Hôpitaux de Paris de sa demande de restitution des paillettes de M. C...E..., en la privant de toute possibilité d'avoir un enfant de son défunt compagnon et donc de mettre en oeuvre leur projet parental, lui porte un préjudice grave et immédiat ;<br/>
              - il est porté une atteinte grave et manifestement illégale à ses libertés fondamentales ;<br/>
              - la décision contestée porte une atteinte grave et manifestement disproportionnée à son droit au respect de sa vie privée et familiale, plus particulièrement au droit de devenir parent garanti par l'article 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - elle est illégale dans la mesure où la situation particulière dans laquelle le projet parental du couple n'a pu aboutir justifie l'autorisation de transfert des paillettes de Matthieu E...vers un pays européen autorisant l'insémination post mortem et que soit par suite écartée l'application de la loi française dont la mise en oeuvre de cette législation est excessive dans les circonstances particulières de l'espèce ;<br/>
              - à défaut et subsidiairement, cette situation justifie la transmission d'une question préjudicielle à la Cour de justice de l'Union européenne sur la compatibilité de la législation française, qui interdit de façon générale le transfert de gamètes vers un autre Etat membre en vue d'une insémination post mortem, avec les articles 7 et 9 de la Charte des droits fondamentaux de l'Union européenne .<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ; <br/>
<br/>
              Vu :<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - la charte des droits fondamentaux de l'Union européenne ;<br/>
              - le code de la santé publique ;<br/>
      - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. Le juge des référés se prononce dans un délai de quarante-huit heures ". En vertu de l'article L. 522-3 du même code, le juge des référés peut rejeter une requête par une ordonnance motivée, sans instruction contradictoire ni audience publique, lorsque la condition d'urgence n'est pas remplie ou lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable ou qu'elle est mal fondée. A cet égard, il appartient au juge d'appel de prendre en considération les éléments recueillis par le juge du premier degré dans le cadre de la procédure écrite et orale qu'il a diligentée.<br/>
<br/>
              2. Eu égard à son office, qui consiste à assurer la sauvegarde des libertés fondamentales, il appartient au juge des référés, saisi sur le fondement de l'article L. 521-2 du code de justice administrative, de prendre, en cas d'urgence, toutes les mesures qui sont de nature à remédier aux effets résultant d'une atteinte grave et manifestement illégale portée, par une autorité administrative, à une liberté fondamentale, y compris lorsque cette atteinte résulte de l'application de dispositions législatives qui sont manifestement incompatibles avec les engagements européens ou internationaux de la France, ou dont la mise en oeuvre entraînerait des conséquences manifestement contraires aux exigences nées de ces engagements.<br/>
<br/>
              3. Aux termes de l'article L. 2141-2 du code de la santé publique : " L'assistance médicale à la procréation a pour objet de remédier à l'infertilité d'un couple ou d'éviter la transmission à l'enfant ou à un membre du couple d'une maladie d'une particulière gravité. Le caractère pathologique de l'infertilité doit être médicalement diagnostiqué./ L'homme et la femme formant le couple doivent être vivants, en âge de procréer et consentir préalablement au transfert des embryons ou à l'insémination. Font obstacle à l'insémination ou au transfert des embryons le décès d'un des membres du couple, le dépôt d'une requête en divorce ou en séparation de corps ou la cessation de la communauté de vie, ainsi que la révocation par écrit du consentement par l'homme ou la femme auprès du médecin chargé de mettre en oeuvre l'assistance médicale à la procréation ". L'article L. 2141-11 de ce même code dispose que : " Toute personne dont la prise en charge médicale est susceptible d'altérer la fertilité, ou dont la fertilité risque d'être prématurément altérée, peut bénéficier du recueil et de la conservation de ses gamètes ou de ses tissus germinaux, en vue de la réalisation ultérieure, à son bénéfice, d'une assistance médicale à la procréation, ou en vue de la préservation et de la restauration de sa fertilité. Ce recueil et cette conservation sont subordonnés au consentement de l'intéressé et, le cas échéant, de celui de l'un des titulaires de l'autorité parentale, ou du tuteur, lorsque l'intéressé, mineur ou majeur, fait l'objet d'une mesure de tutelle./ Les procédés biologiques utilisés pour la conservation des gamètes et des tissus germinaux sont inclus dans la liste prévue à l'article L. 2141-1, selon les conditions déterminées par cet article ". Le III de l'article R. 2141-18 du même code dispose également que : " Il est mis fin à la conservation des gamètes ou des tissus germinaux en cas de décès de la personne. Il en est de même si, n'ayant pas répondu à la consultation selon les modalités fixées par l'arrêté prévu aux articles R. 2142-24 et R. 2142-27, elle n'est plus en âge de procréer ". Il résulte de ces dispositions que, d'une part, le dépôt et la conservation des gamètes ne peuvent être autorisés, en France, qu'en vue de la réalisation d'une assistance médicale à la procréation entrant dans les prévisions légales du code de la santé publique et, d'autre part, la conservation des gamètes ne peut être poursuivie après le décès du donneur.<br/>
<br/>
              4. En outre, en vertu des dispositions de l'article L. 2141-11-1 de ce même code : " L'importation et l'exportation de gamètes ou de tissus germinaux issus du corps humain sont soumises à une autorisation délivrée par l'Agence de la biomédecine./ Seul un établissement, un organisme ou un laboratoire titulaire de l'autorisation prévue à l'article L. 2142-1 pour exercer une activité biologique d'assistance médicale à la procréation peut obtenir l'autorisation prévue au présent article./ Seuls les gamètes et les tissus germinaux recueillis et destinés à être utilisés conformément aux normes de qualité et de sécurité en vigueur, ainsi qu'aux principes mentionnés aux articles L. 1244-3, L. 1244-4, L. 2141-2, L. 2141-3, L. 2141-7 et L. 2141-11 du présent code et aux articles 16 à 16-8 du code civil, peuvent faire l'objet d'une autorisation d'importation ou d'exportation./ Toute violation des prescriptions fixées par l'autorisation d'importation ou d'exportation de gamètes ou de tissus germinaux entraîne la suspension ou le retrait de cette autorisation par l'Agence de la biomédecine. ".<br/>
<br/>
              5. Il résulte de l'instruction que Mme A...et son compagnon, M. E..., avaient formé ensemble un projet parental. En raison du diagnostic d'un cancer dont le traitement risquait d'altérer sa fertilité, son compagnon a procédé, à titre préventif, à un dépôt de gamètes au centre d'étude et de conservation des oeufs et du sperme (CECOS) de l'hôpital Cochin, à Paris, le 13 décembre 2016, l'avant-veille du début de son traitement par chimiothérapie. Le 25 septembre 2017, ce compagnon avec lequel elle avait entre temps conclu un pacte civil de solidarité, a été admis à la suite d'une dégradation rapide de son état de santé à l'hôpital Cochin, à Paris, où il est décédé le 29 septembre 2017. Le 26 décembre 2017, Mme A... a demandé à cet hôpital, qui dépend de l'Assistance publique - Hôpitaux de Paris, le transfert des paillettes de son compagnon vers un établissement de santé étranger à déterminer, en vue d'une insémination post-mortem. Du silence gardé sur cette demande est née une décision implicite de rejet. L'intéressée a demandé au juge des référés du tribunal administratif de Paris d'enjoindre à l'Assistance publique - Hôpitaux de Paris, sur le fondement de l'article L. 521-2 du code de justice administrative, de prendre toutes mesures utiles afin de permettre ce transfert des paillettes de son compagnon vers un établissement de santé situé dans l'Union européenne et autorisé à pratiquer les procréations médicalement assistées post mortem, qui lui sera indiqué. Par une ordonnance n° 1808020 du 25 mai 2018, le juge des référés du tribunal administratif de Paris a rejeté sa demande. Mme A...relève appel de cette ordonnance.<br/>
<br/>
              En ce qui concerne la violation grave et manifeste d'une liberté fondamentale garantie par la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales :<br/>
<br/>
              6. Les dispositions mentionnées aux points 3 et 4 ne sont pas incompatibles avec les stipulations de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales et, en particulier, de son article 8.<br/>
<br/>
              7. Pour estimer que le refus opposé à Mme A...n'entraîne pas, en l'espèce, de conséquences manifestement contraires aux exigences nées de l'article 8 de cette convention, le juge des référés du tribunal administratif de Paris s'est fondé sur la double circonstance que, d'une part, alors qu'il a été indiqué à M. E...que la conservation des spermatozoïdes était strictement personnelle et qu'en cas de décès, il serait mis fin à cette conservation, celui-ci n'a jamais exprimé la volonté que ses paillettes soient utilisées en vue d'une éventuelle insémination artificielle post mortem et que, d'autre part, MmeA..., qui est de nationalité française, réside en France et n'a pas de lien particulier avec l'Espagne, pays où se trouve un établissement avec lequel elle a pris contact en vue d'une assistance médicale à la procréation après le décès de M.E..., ne démontre pas l'existence d'une circonstance particulière constituant une ingérence disproportionnée dans ses droits garantis par la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales. La requérante n'apporte en appel aucun élément nouveau susceptible d'infirmer l'appréciation ainsi portée par le juge des référés de première instance tant en ce qui concerne la volonté manifestée par M. D...qu'en ce qui concerne l'existence de circonstances tenant à des liens particuliers entretenus avec un autre Etat membre de l'Union européenne dans lequel une insémination artificielle post mortem peut être légalement pratiquée.<br/>
<br/>
              En ce qui concerne la violation grave et manifeste d'une liberté fondamentale garantie par la charte des droits fondamentaux de l'Union européenne :<br/>
<br/>
              8. Aux termes de l'article 51 de la charte des droits fondamentaux de l'Union européenne : " Les dispositions de la présente Charte s'adressent aux institutions, organes et organismes de l'Union dans le respect du principe de subsidiarité, ainsi qu'aux États membres uniquement lorsqu'ils mettent en oeuvre le droit de l'Union. (...) ". Or, ainsi que l'a constaté le juge des référés du tribunal administratif de Paris, le refus de transfert des paillettes de M. D... qui a été opposé à MmeA..., qui, contrairement à ce qu'elle soutient, ne porte nullement atteinte à son droit à la libre circulation dans l'Union européenne garanti par l'article 20 du traité sur le fonctionnement de l'Union européenne, ne met pas en oeuvre le droit de l'Union européenne. Par suite, la requérante ne peut utilement invoquer un moyen tiré de la méconnaissance des articles 7 et 9 de cette charte à l'appui de sa demande d'injonction.<br/>
<br/>
              9. Il résulte de tout ce qui précède que, sans qu'il soit besoin de saisir la Cour de justice de l'Union européenne d'une question préjudicielle ni de se prononcer sur l'existence d'une situation d'urgence, il est manifeste que l'appel de Mme A...ne peut être accueilli. Par suite, il y a lieu de rejeter sa requête selon la procédure prévue à l'article L. 522-3 du code de justice administrative, y compris les conclusions présentées sur le fondement des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de Mme A...est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à Mme B...A....<br/>
Copie en sera adressée au président-directeur général de l'Assistance publique - Hôpitaux de Paris, à la directrice générale de l'Agence de la biomédecine et à la ministre des solidarités et de la santé.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
