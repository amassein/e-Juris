<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036976383</ID>
<ANCIEN_ID>JG_L_2018_04_000000419621</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/97/63/CETATEXT000036976383.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 16/04/2018, 419621, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-04-16</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>419621</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2018:419621.20180416</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B...A...a demandé au juge des référés du tribunal administratif de Paris, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, d'ordonner la suspension de l'exécution de la décision du 8 février 2018 par laquelle le ministre de l'Europe et des affaires étrangères l'a placé en situation d'appel spécial. Par une ordonnance n° 1804261/9 du 28 mars 2018, le juge des référés du tribunal administratif de Paris a rejeté sa demande.<br/>
<br/>
              Par une requête enregistrée le 8 avril 2018 au secrétariat du contentieux du Conseil d'Etat, M. A...demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative :<br/>
<br/>
              1°) d'annuler l'ordonnance attaquée ; <br/>
<br/>
              2°) de suspendre l'exécution de la décision du 8 février 2018 ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.  <br/>
<br/>
<br/>
<br/>
              Il soutient que :<br/>
              - la condition d'urgence est remplie eu égard, d'une part, au caractère soudain de son départ contraint d'Abidjan en Côte d'Ivoire et, d'autre part, à la gravité de l'atteinte portée à son droit au respect de la vie privée et familiale et à l'intérêt supérieur de ses enfants ; qu'il n'a formé son recours que plus d'un mois après l'intervention de la décision litigieuse en raison des contraintes ayant résulté de l'exécution de cette décision et des effets qui se sont révélés dans les semaines qui l'ont suivie ;<br/>
              - il est porté une atteinte grave et manifestement illégale à son droit au respect de la vie privée et familiale ainsi qu'à l'intérêt supérieur de ses enfants ;<br/>
              - en effet, la décision litigieuse est entachée d'un défaut de motivation en méconnaissance de l'article L. 211-2 du code des relations entre le public et l'administration dès lors qu'elle constitue une décision administrative individuelle défavorable qui lui impose des sujétions et revêt le caractère d'une sanction déguisée ;<br/>
              - elle est entachée d'une erreur de droit et d'une erreur de qualification juridique des faits dès lors que les circonstances invoquées par le ministre de l'Europe et des affaires étrangères ne caractérisent pas des circonstances locales au sens de l'article 22-1 du décret du 28 mars 1967 fixant les modalités de calcul des émoluments des personnes de l'Etat et des établissements publics de l'Etat à caractère administratif en service à l'étranger ;<br/>
               - elle le contraint à s'éloigner de sa femme et de ses enfants, ce qui entraîne pour eux une grande détresse psychologique ;<br/>
              - elle n'est justifiée par aucun intérêt s'attachant au bon fonctionnement du service où il était affecté ;<br/>
              - le juge des référés du tribunal administratif de Paris a commis une erreur d'appréciation des faits et une erreur de droit en ce qu'il a considéré que l'absence de motivation propre de la décision litigieuse n'avait privé le requérant d'aucune garantie.<br/>
<br/>
              Par un mémoire en défense enregistré le 12 avril 2018, le ministre de l'Europe et des affaires étrangères conclut au rejet de la requête. Il soutient que la condition d'urgence n'est pas remplie et que les moyens soulevés par M. A...ne sont pas fondés.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la convention internationale relative aux droits de l'enfant ; <br/>
              - le code des relations entre le public et l'administration ; <br/>
              - le décret n° 67-290 du 28 mars 1967 modifié fixant les modalités de calcul des émoluments des personnels de l'Etat et des établissements publics de l'Etat à caractère administratif en service à l'étranger ;<br/>
              - le décret n° 79-433 du 1er juin 1979 relatif aux pouvoirs des ambassadeurs et à l'organisation des services de l'Etat à l'étranger ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, M.A..., d'autre part, le ministre de l'Europe et des affaires étrangères ; <br/>
<br/>
              Vu le procès-verbal de l'audience publique du 13 avril 2018 à 15 heures au cours de laquelle ont été entendus : <br/>
<br/>
              - le représentant de M.A... ;<br/>
<br/>
              - les représentants du ministre de l'Europe et des affaires étrangères ;<br/>
              et à l'issue de laquelle le juge des référés a clos l'instruction ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. Le juge des référés se prononce dans un délai de quarante-huit heures ". <br/>
<br/>
              2. Aux termes du premier alinéa de l'article 22-1 du décret n° 67-290 du 28 mars 1967 fixant les modalités de calcul des émoluments des personnels de l'Etat et des établissements publics de l'Etat à caractère administratif en service à l'étranger : "  L'appel spécial est la situation de l'agent qui, en raison de la situation politique ou des circonstances locales appréciées par le ministre ou par le directeur de l'établissement public dont relève l'intéressé, reçoit instruction soit de quitter le pays où il est affecté et de regagner la France métropolitaine, soit, s'il est en congé, de rentrer en France métropolitaine ou d'y demeurer ". <br/>
<br/>
              3. Il résulte de l'instruction que M.A..., exerçant les fonctions de chef de chancellerie au consulat de France à Abidjan (Côte d'Ivoire), a été informé le 12 janvier 2018, lors d'un entretien qui s'est déroulé à Paris avec la direction des ressources humaines du ministère de l'Europe et des affaires étrangères, que son placement en appel spécial était envisagé dans l'attente d'une éventuelle mutation dans l'intérêt du service, en raison de l'ouverture en France d'une procédure judiciaire visant des infractions à caractère sexuel qu'aurait commises son fils aîné, âgé de seize ans, à l'encontre d'enfants mineurs d'autres agents diplomatiques français en poste à Abidjan, afin d'apaiser les tensions avec l'un de ces agents et de prévenir toute dégradation des relations au sein du service. Le principe de cette mesure a été confirmé à M. A...par un courrier du 2 février 2018 qui lui a été notifié le 5 février 2018. Par une décision du 8 février 2018, le ministre de l'Europe et des affaires étrangères a pris cette mesure, qui est entrée en vigueur à compter du 1er mars 2018. M. A...a demandé au juge des référés du tribunal administratif de Paris, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, de suspendre l'exécution de la décision du 8 février 2018. Par une ordonnance n° 1804261/9 du 28 mars 2018, le juge des référés du tribunal administratif de Paris a rejeté sa demande. Par la présente requête, M. A... relève appel de cette ordonnance.<br/>
<br/>
              4. En premier lieu, aux termes de l'article L. 211-2 du code des relations entre le public et l'administration : " Les personnes physiques ou morales ont le droit d'être informées sans délai des motifs des décisions administratives individuelles défavorables qui les concernent. A cet effet, doivent être motivées les décisions qui: (...)/2° Infligent une sanction; 3o Subordonnent l'octroi d'une autorisation à des conditions restrictives ou imposent des sujétions ". Aux termes de l'article L. 211-5 du même code : " La motivation exigée par le présent chapitre doit être écrite et comporter l'énoncé des considérations de droit et de fait qui constituent le fondement de la décision. ".<br/>
<br/>
<br/>
              5. Il résulte de l'instruction que la décision litigieuse, prise au vu des conséquences que les faits à l'origine de la procédure pénale en cours concernant le fils de M. A... pouvait avoir sur le fonctionnement du service, est sans rapport avec la façon de servir de ce dernier et ne constitue pas une mesure à caractère disciplinaire prise à son encontre. A supposer qu'elle puisse être regardée comme une mesure défavorable lui imposant des sujétions, il résulte de l'instruction que la directrice des ressources humaines a fait connaître à M.A..., par un courrier du 2 février 2018 qui lui était adressé personnellement, la décision de le placer dans la situation de l'appel spécial prévu par l'article 22-1 du décret du 28 mars 1967 précité. Cette lettre, notifiée à M. A...en main propre le 5 février 2018, comporte la motivation de la décision. Le moyen tiré de ce que l'acte attaqué du 8 février 2018, qui peut être regardé comme formalisant cette décision, ne serait pas motivé peut, par suite et en tout état de cause, être écarté.<br/>
<br/>
              6. En deuxième lieu, il n'apparaît pas que le ministre de l'Europe et des affaires étrangères ait manifestement méconnu la portée du 1er alinéa de l'article 22-1 du décret du 28 mars 1967 précité en considérant que des circonstances ayant trait au fonctionnement interne de la mission diplomatique ainsi qu'à son image pouvaient constituer des " circonstances locales " pour l'application de cet article. Il résulte par ailleurs de l'instruction que, par un courriel formel en date du 26 janvier 2018, l'ambassadeur de France en Côte d'Ivoire a relevé que la situation résultant des faits ayant donné lieu à procédure judiciaire concernant le fils de M. A... présentait un risque de conflit potentiel entre agents ainsi qu'un " risque réputationnel " pour l'ambassade. Il a indiqué que cette situation ne pouvait " rester en l'état " et, écartant des mesures de caractère disciplinaire, a préconisé diverses solutions conduisant à éloigner M. A...des services diplomatiques à Abidjan. Même si ce dernier fait valoir qu'il estimait pouvoir continuer à exercer ses fonctions au consulat de France à Abidjan sans perturbation pour le fonctionnement du service, le ministre a pu légalement prendre la décision litigieuse au vu des circonstances particulières de l'espèce.<br/>
<br/>
              7. En troisième lieu, eu égard, d'une part, à la teneur du courrier du 2 février 2018, dont il ressort que la mesure litigieuse a un caractère conservatoire dans l'attente d'une mutation de l'intéressé et, d'autre part, aux motifs ayant justifié cette mesure, le dossier ne fait pas apparaître, bien que la mesure contestée entraîne un éloignement du requérant de sa famille demeurée provisoirement en Côte d'Ivoire,  d'atteinte grave et manifestement illégale au droit au respect de sa vie privée et familiale ou à l'intérêt supérieur de ses enfants.<br/>
<br/>
              8. Il résulte de tout ce qui précède que M. A...n'est pas fondé à soutenir que c'est à tort que, par l'ordonnance contestée, le juge des référés du tribunal administratif de Paris a rejeté la demande qu'il lui avait présentée sur le fondement de l'article L. 521-2 du code de justice administrative. Par suite, il y a lieu, sans qu'il soit besoin de statuer sur l'urgence, de rejeter sa requête, y compris les conclusions tendant à l'application des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de M. A...est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à M. B...A...et au ministre de l'Europe et des affaires étrangères.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
