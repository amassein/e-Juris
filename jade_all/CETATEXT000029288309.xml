<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029288309</ID>
<ANCIEN_ID>JG_L_2014_07_000000375375</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/28/83/CETATEXT000029288309.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère sous-section jugeant seule, 23/07/2014, 375375, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-07-23</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>375375</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère sous-section jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP NICOLAY, DE LANOUVELLE, HANNOTIN</AVOCATS>
<RAPPORTEUR>M. Yannick Faure</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Alexandre Lallet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:375375.20140723</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
Procédure contentieuse antérieure<br/>
<br/>
              M. C...A...B...a demandé au tribunal administratif de Rouen d'annuler l'arrêté du recteur de l'académie de Rouen du 21 octobre 2010 en tant qu'il fixe la date de mise en paiement de sa pension de retraite à l'âge de référence fixé par le code de la sécurité sociale. Par un jugement n° 1100909 du 6 décembre 2012, le tribunal administratif de Rouen a rejeté sa demande.<br/>
<br/>
Procédure devant le Conseil d'Etat<br/>
<br/>
              Par un arrêt n° 13DA00156 du 12 décembre 2013, enregistré le 11 février 2014 au secrétariat du contentieux du Conseil d'Etat, la cour administrative d'appel de Douai a transmis au Conseil d'Etat, en application de l'article R. 351-2 du code de justice administrative, le pourvoi, enregistré le 6 février 2013 au greffe de cette cour, présenté par M. A...B.... Par ce pourvoi, par un mémoire en réplique et par un mémoire complémentaire enregistré le 22 avril 2014 au secrétariat du contentieux du Conseil d'Etat, M. A...B...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler le jugement n° 1100909 du tribunal administratif de Rouen du 6 décembre 2012 ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 6 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
                          Vu :<br/>
              - les autres pièces du dossier ;<br/>
              - le code des pensions civiles et militaires de retraite ;<br/>
              - la loi n° 2010-1330 du 9 novembre 2010 ;<br/>
              - le code de justice administrative.<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Yannick Faure, auditeur,  <br/>
<br/>
              - les conclusions de M. Alexandre Lallet, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Nicolaÿ, de Lanouvelle, Hannotin, avocat de M. A...B....<br/>
<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis au juge du fond que M. A...B..., professeur certifié de mathématiques, admis au bénéfice d'une cessation progressive d'activité à compter du 1er septembre 2008, a demandé à être radié des cadres le 3 janvier 2011 et admis à la retraite à son soixantième anniversaire, le 26 septembre 2011. Par un arrêté du recteur de l'académie de Rouen du 21 octobre 2010, il a été radié des cadres à compter du 3 janvier 2011. Toutefois, l'article 2 du même arrêté, dont M. A...B...a demandé l'annulation au tribunal administratif de Rouen, indiquait que la mise en paiement de sa pension de retraite ne pourrait intervenir avant l'âge de référence fixé par le code de la sécurité sociale. <br/>
<br/>
              2. En premier lieu, il résulte de l'avant-dernier alinéa de l'article L. 25 du code des pensions civiles et militaires de retraites, issu de la loi du 21 août 2003 portant réforme des retraites, que, pour l'application des règles relatives à l'âge d'ouverture des droits à pension, les règles de liquidation de la pension sont " celles en vigueur au moment de sa mise en paiement ". Sauf disposition législative contraire, les règles applicables au calcul de la pension d'un fonctionnaire sont celles en vigueur à la date à laquelle, dès lors que l'ensemble des conditions d'ouverture des droits est réuni, la pension peut être mise en paiement. Par suite, le tribunal n'a pas commis d'erreur de droit en jugeant que les règles applicables à la liquidation de la pension de M. A...B...n'étaient pas celles en vigueur à la date de sa radiation des cadres, dès lors qu'elles avaient été modifiées entre cette date et celle à laquelle il aurait atteint l'âge légal de la retraite.<br/>
<br/>
              3. En deuxième lieu, il ressort des énonciations du jugement attaqué que la pension de M. A...B...ne pouvait être liquidée, et donc prendre effet, avant le 1er juillet 2011, date d'entrée en vigueur des dispositions de la loi du 9 novembre 2010 portant réforme des retraites, dès lors que M. A...B...n'avait pas encore atteint, à cette date, l'âge de soixante ans fixé pour l'ouverture des droits par l'article L. 25 du code des pensions civiles et militaires de retraite dans sa rédaction antérieure à cette loi. Par suite, le tribunal n'a pas commis d'erreur de droit en jugeant que la loi du 9 novembre 2010 était applicable à la fixation de la date de liquidation de la pension de M. A...B...et ce dernier n'est pas fondé à soutenir qu'il en aurait fait une application rétroactive.<br/>
<br/>
              4. En troisième lieu, le tribunal n'a pas commis d'erreur de droit en jugeant que le défaut de mise à jour des formulaires de demande d'admission à la retraite et l'absence d'information sur les évolutions législatives à intervenir ainsi que sur la possibilité de différer la date d'admission à la retraite de M. A...B...étaient sans incidence sur la légalité de la décision attaquée.<br/>
<br/>
              5. En dernier lieu, le moyen tiré de ce que le recteur aurait dû retirer son arrêté du 21 octobre 2010 sur le recours de M. A...B..., dès lors qu'il estimait ne pouvoir faire droit à la demande de ce dernier de mise en paiement de sa pension à compter de son soixantième anniversaire, n'a pas été soulevé devant le tribunal, qui n'y a ainsi pas répondu, et n'avait pas à être relevé d'office par lui. Par suite, il est sans incidence sur le bien-fondé du jugement attaqué.<br/>
<br/>
              6. Il résulte de tout ce qui précède que M. A...B...n'est pas fondé à demander l'annulation du jugement qu'il attaque. Ses conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative ne peuvent, par suite, qu'être également rejetées.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de M. A...B...est rejeté.<br/>
Article 2 : La présente décision sera notifiée à M. C...A...B...et au ministre de l'éducation nationale, de l'enseignement supérieur et de la recherche.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
