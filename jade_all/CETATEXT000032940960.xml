<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032940960</ID>
<ANCIEN_ID>JG_L_2016_07_000000392282</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/94/09/CETATEXT000032940960.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème - 4ème chambres réunies, 27/07/2016, 392282, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-07-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>392282</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème - 4ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FOUSSARD, FROGER ; SCP HEMERY, THOMAS-RAQUIN</AVOCATS>
<RAPPORTEUR>M. Jean-Dominique Langlais</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2016:392282.20160727</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Statuant sur la plainte du conseil départemental de l'ordre des masseurs-kinésithérapeutes du Val-de-Marne, la chambre disciplinaire de première instance du conseil interrégional de l'ordre des masseurs-kinésithérapeutes d'Île-de-France et de La Réunion a, par une décision n° 14/002 du 24 octobre 2014, prononcé contre M. A...C...la sanction de l'interdiction d'exercer la profession de masseur-kinésithérapeute pour une durée de six mois dont trois mois avec sursis. <br/>
<br/>
              Par une décision n° 060-2014 du 2 juillet 2015, la chambre disciplinaire nationale de l'ordre des masseurs-kinésithérapeutes a rejeté l'appel formé par M. C...contre cette décision et fixé au 1er octobre 2015 le début de la période d'interdiction d'exercer.<br/>
<br/>
              Par un pourvoi enregistré le 3 août 2015 au secrétariat du contentieux du Conseil d'Etat, M. C... demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cette décision ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ; <br/>
<br/>
              3°) de mettre à la charge du Conseil national de l'ordre des masseurs-kinésithérapeutes et du conseil départemental de l'ordre des masseurs-kinésithérapeutes du Val-de-Marne, solidairement, la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code civil ; <br/>
              - le code de la santé publique ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Dominique Langlais, maître des Requêtes,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Foussard, Froger, avocat de M. C...et à la SCP Hémery, Thomas-Raquin, avocat du conseil départemental de l'ordre des masseurs-kinésithérapeutes du Val-de-Marne ;<br/>
<br/>
<br/>
<br/>1. Considérant, d'une part, qu'aux termes des dispositions de l'article R. 4321-79 du code de la santé publique : " Le masseur-kinésithérapeute s'abstient, même en dehors de l'exercice de sa profession, de tout acte de nature à déconsidérer celle-ci " ; <br/>
<br/>
              2. Considérant, d'autre part, qu'aux termes de l'article R. 4321-97 du même code : " Le masseur-kinésithérapeute qui a participé au traitement d'une personne pendant la maladie dont elle est décédée ne peut profiter des dispositions entre vifs et testamentaires faites en sa faveur par celle-ci pendant le cours de cette maladie que dans les cas et conditions prévus par l'article 909 du code civil " ; qu'aux termes de l'article 909 du code civil : " Les membres des professions médicales et de la pharmacie, ainsi que les auxiliaires médicaux qui ont prodigué des soins à une personne pendant la maladie dont elle meurt ne peuvent profiter des dispositions entre vifs ou testamentaires qu'elle aurait faites en leur faveur pendant le cours de celle-ci " ; que ces dispositions font interdiction aux masseurs-kinésithérapeutes de profiter des dispositions testamentaires prises en leur faveur par un de leurs patients, si ces dispositions testamentaires ont été prises pendant une maladie qui a entraîné le décès de ce patient et s'ils ont prodigué des soins à ce patient au cours de cette maladie ; <br/>
<br/>
              3. Considérant qu'il ressort de la décision attaquée qu'après avoir cité les dispositions des articles R. 4321-79 et R. 4321-97 du code de la santé publique, la chambre disciplinaire nationale de l'ordre des masseurs-kinésithérapeutes a relevé que M.C..., masseur-kinésithérapeute, avait prodigué des soins à Mme B...plusieurs fois par semaine de juillet 2008 jusqu'au décès de la patiente, en 2013, à l'âge de 87 ans, l'avait accompagnée à plusieurs reprises dans sa maison de la Creuse, avait noué avec elle des relations qui l'avaient mis en mesure d'influencer Mme B...dans la gestion de son patrimoine et avait en outre accepté qu'elle lui lègue sa maison de la Creuse et consente des legs à ses enfants ; que la chambre disciplinaire s'est fondée sur l'ensemble de ces circonstances pour juger que le comportement de l'intéressé contrevenait aux dispositions précitées et pour lui infliger une sanction d'interdiction temporaire d'exercer ; que, toutefois, en jugeant que le comportement de M. C...contrevenait aux dispositions de l'article R. 4321-97 du code de la santé publique sans rechercher si Mme B...était décédée d'une maladie, si M. C...lui avait prodigué des soins durant cette maladie ni si les dispositions testamentaires en causes avaient été prises pendant cette même maladie, la chambre disciplinaire nationale n'a pas légalement fondé sa décision ; que si elle a également fondé la sanction sur une méconnaissance des dispositions de l'article R. 4321-79 du code de la santé publique par l'intéressé, l'erreur de droit qu'elle a commise dans l'application des dispositions de l'article R. 4321-97, qui entache un motif de sa décision ne présentant pas un caractère surabondant, doit en entraîner l'annulation, sans qu'il soit besoin d'examiner les autres moyens du pourvoi ; <br/>
<br/>
              4. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à la charge de M. C...qui n'est pas, dans la présente instance, la partie perdante ; qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge du conseil départemental de l'ordre des masseurs-kinésithérapeutes du Val-de-Marne la somme de 4 000 euros, à verser à M. C...au titre de ces dispositions ; que le Conseil national de l'ordre des masseurs-kinésithérapeutes n'ayant pas la qualité de partie à l'instance, les conclusions qu'il présente au même titre, de même que celles qui sont dirigées contre lui, doivent être rejetées ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : La décision du 2 juillet 2015 de la chambre disciplinaire nationale de l'ordre des masseurs-kinésithérapeutes est annulée.<br/>
Article 2 : L'affaire est renvoyée à la chambre disciplinaire nationale de l'ordre des masseurs-kinésithérapeutes.<br/>
Article 3 : Le conseil départemental de l'ordre des masseurs-kinésithérapeutes du Val-de-Marne versera la somme de 4 000 euros à M. C...au titre des dispositions de l'article L. 761-1 du code de justice administrative. <br/>
Article 4 : Le surplus des conclusions du pourvoi est rejeté.<br/>
Article 5 : Les conclusions présentées par le conseil départemental de l'ordre des masseurs-kinésithérapeutes du Val-de-Marne et par le Conseil national de l'ordre des masseurs-kinésithérapeutes au titre de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
Article 6 : La présente décision sera notifiée à M. A...C...et au conseil départemental de l'ordre des masseurs-kinésithérapeutes du Val-de-Marne. <br/>
Copie en sera adressée au Conseil national de l'ordre des masseurs-kinésithérapeutes.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
