<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033442749</ID>
<ANCIEN_ID>JG_L_2016_11_000000382484</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/44/27/CETATEXT000033442749.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème - 4ème chambres réunies, 21/11/2016, 382484, Publié au recueil Lebon</TITRE>
<DATE_DEC>2016-11-21</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>382484</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème - 4ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>OCCHIPINTI</AVOCATS>
<RAPPORTEUR>M. Lionel Collet</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Laurence Marion</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2016:382484.20161121</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A...B...a demandé au tribunal administratif d'Amiens d'annuler pour excès de pouvoir la décision du 1er août 2013 par laquelle le préfet de la Somme a refusé de procéder à l'échange de son permis de conduire géorgien contre un permis de conduire français et d'enjoindre au préfet de la Somme de lui délivrer un permis de conduire français. Par un jugement n° 1302538 du 17 avril 2014, le tribunal administratif a annulé la décision du préfet de la Somme et enjoint à celui-ci de procéder au réexamen de la demande de M. B....<br/>
<br/>
              Par une ordonnance n°14DA00773 du 27 juin 2014, enregistrée au secrétariat du contentieux du Conseil d'Etat le 11 juillet 2014, le président de la cour administrative d'appel de Douai a transmis au Conseil d'Etat, en application de l'article R. 351-2 du code de justice administrative, le pourvoi dirigé contre ce jugement présenté devant cette cour par le ministre de l'intérieur.<br/>
<br/>
              Par ce pourvoi, enregistré le 9 mai 2014 au greffe de la cour administrative d'appel, et un nouveau mémoire, enregistré le 22 juin 2015 au secrétariat du contentieux du Conseil d'Etat, le ministre de l'intérieur demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter la demande de M. B....<br/>
<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
<br/>
              - le code de la route;<br/>
<br/>
              - la loi n° 91-647 du 10 juillet 1991;<br/>
<br/>
              - le décret n° 2008-1281 du 8 décembre 2008 ;<br/>
<br/>
              - l'arrêté du ministre de l'équipement, des transports et du logement en date du 8 février 1999 fixant les conditions de reconnaissance et d'échange des permis de conduire délivrés par les Etats n'appartenant ni à l'Union européenne, ni à l'Espace économique européen ;<br/>
<br/>
              - l'arrêté de la ministre de l'écologie, du développement durable, des transports et du logement et du ministre de l'intérieur, de l'outre-mer, des collectivités territoriales et de l'immigration en date du 12 janvier 2012 fixant les conditions de reconnaissance et d'échange des permis de conduire délivrés par les Etats n'appartenant ni à l'Union européenne, ni à l'Espace économique européen ;<br/>
<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Lionel Collet, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Laurence Marion, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Occhipinti, avocat de M.B....<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que M.B..., ressortissant géorgien, a obtenu en 1990 un permis de conduire délivré par les autorités de l'Union des républiques socialistes soviétiques (URSS) puis, après l'accession de la Géorgie à l'indépendance, trois permis de conduire successifs délivrés par les autorités de ce pays en 1993, 1997 et 2012 ; que M. B...a demandé le 29 janvier 2013 au préfet de la Somme d'échanger le permis délivré en 2012 contre un permis de conduire français ; que le préfet a rejeté sa demande par une décision du 1er août 2013 ; que le ministre de l'intérieur se pourvoit en cassation contre le jugement du 9 octobre 2013 par lequel le tribunal administratif d'Amiens a annulé cette décision à la demande de l'intéressé ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article R. 222-3 du code de la route : " Tout permis de conduire national, en cours de validité, délivré par un Etat ni membre de la Communauté européenne, ni partie à l'accord sur l'Espace économique européen, peut être reconnu en France jusqu'à l'expiration d'un délai d'un an après l'acquisition de la résidence normale de son titulaire. Pendant ce délai, il peut être échangé contre le permis français, sans que son titulaire soit tenu de subir les examens prévus au premier alinéa de l'article  R. 221-3. Les conditions de cette reconnaissance et de cet échange sont définies par arrêté du ministre chargé des transports, après avis du ministre de la justice, du ministre de l'intérieur et du ministre chargé des affaires étrangères. Au terme de ce délai, ce permis n'est plus reconnu et son titulaire perd tout droit de conduire un véhicule pour la conduite duquel le permis de conduire est exigé " ; qu'aux termes de l'article 5 de l'arrêté du 12 janvier 2012 visé ci-dessus, pris pour l'application de ces dispositions : " I. - Pour être échangé contre un titre français, tout permis de conduire délivré par un Etat n'appartenant ni à l'Union européenne, ni à l'Espace économique européen doit répondre aux conditions suivantes :/ A.  Avoir été délivré au nom de l'Etat dans le ressort duquel le conducteur avait alors sa résidence normale, sous réserve qu'il existe un accord de réciprocité entre la France et cet Etat conformément à l'article R. 222-1 du code de la route./ (...)  " ;  qu'aux termes de l'article 14 du même arrêté : " Une liste des Etats dont les permis de conduire nationaux sont échangés en France contre un permis français est établie conformément aux articles R. 222-1 et R. 222-3 du code de la route. Cette liste précise pour chaque Etat la ou les catégories de permis de conduire concernée (s) par l'échange contre un permis français. Elle ne peut inclure que des Etats qui procèdent à l'échange des permis de conduire français de catégorie équivalente et dans lesquels les conditions effectives de délivrance des permis de conduire nationaux présentent un niveau d'exigence conforme aux normes françaises dans ce domaine. / Les demandes d'échange de permis introduites avant la date de publication au JORF de la liste prévue au premier alinéa du présent article sont traitées sur la base de la liste prévue à l'article 14 de l'arrêté du 8 février 1999 modifié fixant les conditions de reconnaissance et d'échange des permis de conduire délivrés par les Etats n'appartenant ni à l'Union européenne, ni à l'Espace économique européen " ; que l'article 14 de l'arrêté du  8 février 1999 dispose que le ministre chargé des transports établit, après consultation du ministre des affaires étrangères, la liste des Etats qui procèdent à l'échange des permis de conduire français ; <br/>
<br/>
              3. Considérant qu'il résulte des termes du premier alinéa de l'article 14 de l'arrêté du 12 janvier 2012 cité ci-dessus que la liste d'Etats qu'il prévoit doit être établie conformément aux articles R. 222-1 et R. 222-3 du code de la route, à savoir " par arrêté du ministre chargé des transports, après avis du ministre de l'intérieur et du ministre chargé des affaires étrangères " ; qu'aucune liste n'a été établie par le ministre des transports en application de ces dispositions ; que le second alinéa du même article prévoit qu'en pareil cas, les demandes d'échange sont traitées sur la base de la liste prévue à l'article 14 de l'arrêté du 8 février 1999 ; que si une circulaire du 22 septembre 2006 du ministre des transports avait fixé une liste d'Etats sur le fondement de cet article, l'annexe de cette circulaire fixant la liste n'a pas été mise en ligne sur le site internet relevant du Premier ministre prévu au premier alinéa de l'article 1er du décret du 8 décembre 2008 relatif aux conditions de publication des instructions et circulaires, repris à l'article R. 312-8 du code des relations entre le public et l'administration ; que, par suite, en application de l'article 2 du même décret, aux termes duquel les instructions et circulaire déjà signées " sont regardées comme abrogées si elles ne sont pas reprises sur le site mentionné à l'article 1er ", la liste doit être regardée comme abrogée ; que, dans ces conditions, pour déterminer si un permis de conduire délivré par un Etat n'appartenant ni à l'Union européenne, ni à l'Espace économique européen est susceptible d'être échangé contre un permis français, il y a seulement lieu de vérifier si, conformément aux dispositions précitées du I de l'article 5 de l'arrêté du 12 janvier 2012, cet Etat est lié à la France par un accord de réciprocité en matière d'échange de permis de conduire ; <br/>
<br/>
              4. Considérant que, pour annuler le refus opposé à M. B...par le préfet de la Somme, le tribunal administratif d'Amiens, après avoir relevé qu'aucun accord de réciprocité portant sur les échanges de permis de conduire n'avait été conclu entre la France et la Géorgie, s'est fondé sur la circonstance " qu'une circulaire du 3 août 2012 prévoit, dans la liste  prise en application de l'article 14 de l'arrêté du 12 janvier 2012, la possibilité, s'agissant de la Géorgie, d'un échange, dans le cas où le permis de conduire a été délivré avant le 1er janvier 1992 au nom de l'URSS " ;  que, toutefois, la circulaire du 3 août 2012 relative à la mise en oeuvre de l'arrêté du 12 janvier 2012, émanant du ministre de l'intérieur et non du ministre chargé des transports, n'a pu légalement avoir pour objet ni pour effet de fixer la liste prévue par l'article 14 de cet arrêté ; qu'en se fondant sur cette circulaire pour annuler la décision attaquée, le tribunal administratif a commis une erreur de droit ; que le ministre de l'intérieur est, par suite, et sans qu'il soit besoin d'examiner l'autre moyen du pourvoi, fondé à demander l'annulation du jugement attaqué ;<br/>
<br/>
              5. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              6. Considérant que le permis de conduire présenté par M. B... ayant été délivré par les autorités géorgiennes, le préfet devait apprécier sa demande au regard des règles gouvernant l'échange de permis de conduire délivrés par ce pays ; que la circonstance, à la supposer établie, que ce permis lui avait été délivré en échange du permis de conduire qu'il avait antérieurement obtenu des autorités de l'URSS ne pouvait conduire à lui appliquer les règles d'échange applicables aux permis délivrés par cet Etat avant sa disparition ;  qu'il est constant qu'aucun accord de réciprocité n'existe entre la France et la Géorgie en matière d'échange de permis de conduire ; que, dès lors, le préfet était tenu, en application des dispositions précitées de l'article 5 de l'arrêté du 12 janvier 2012 cité ci-dessus, de refuser l'échange demandé ; <br/>
<br/>
              7. Considérant qu'il résulte de ce qui précède que M. B...n'est pas fondé à demander l'annulation de la décision de refus du préfet de la Somme ; que ses conclusions à fins d'injonction doivent, par voie de conséquence, être rejetées ainsi que ses conclusions présentées au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 ;<br/>
<br/>
<br/>
<br/>
<br/>			D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : Le jugement du tribunal administratif d'Amiens du 9 octobre 2013 est annulé.<br/>
Article 2 : La demande présentée par M. B...devant le tribunal administratif d'Amiens est rejetée.<br/>
Article 3 : Les conclusions présentées par M. B...au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 sont rejetées.<br/>
Article 4 : La présente décision sera notifiée au ministre de l'intérieur et à M. A... B....<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">49-04-01-04 POLICE. POLICE GÉNÉRALE. CIRCULATION ET STATIONNEMENT. PERMIS DE CONDUIRE. - ECHANGE D'UN PERMIS DE CONDUIRE ÉTRANGER CONTRE UN PERMIS DE CONDUIRE FRANÇAIS (ART. R. 222-3 DU CODE DE LA ROUTE) - APPLICATION DE L'ARRÊTÉ DU 12 JANVIER 2012 - ABSENCE DE LA LISTE PRÉVUE À L'ARTICLE 14 DE L'ARRÊTÉ - CONSÉQUENCES.
</SCT>
<ANA ID="9A"> 49-04-01-04 Arrêté du 12 janvier 2012 fixant les conditions de reconnaissance et d'échange des permis de conduire délivrés par les Etats n'appartenant ni à l'Union européenne, ni à l'Espace économique européen et prévoyant qu'il doit exister un accord de réciprocité entre la France et l'Etat au nom duquel le permis a été délivré et qu'une liste des Etats dont les permis de conduire nationaux sont échangés en France est établie par arrêté.... ,,Il résulte du premier alinéa de l'article 14 de cet arrêté du 12 janvier 2012 que la liste d'Etats qu'il prévoit doit être établie conformément aux articles R. 222-1 et R. 222-3 du code de la route, à savoir par arrêté du ministre chargé des transports, après avis du ministre de l'intérieur et du ministre chargé des affaires étrangères. Aucune liste n'a été établie par le ministre des transports en application de ces dispositions. Le second alinéa du même article prévoit qu'en pareil cas, les demandes d'échange sont traitées sur la base de la liste prévue à l'article 14 de l'arrêté du 8 février 1999. Si une circulaire du 22 septembre 2006 du ministre des transports avait fixé une liste d'Etats sur le fondement de cet article, l'annexe de cette circulaire fixant la liste n'a pas été mise en ligne sur le site internet relevant du Premier ministre prévu au premier alinéa de l'article 1er du décret du 8 décembre 2008 relatif aux conditions de publication des instructions et circulaires, repris à l'article R. 312-8 du code des relations entre le public et l'administration. Par suite, en application de l'article 2 du même décret, aux termes duquel les instructions et circulaires déjà signées sont regardées comme abrogées si elles ne sont pas reprises sur le site mentionné à l'article 1er, la liste doit être regardée comme abrogée.... ,,Dans ces conditions, pour déterminer si un permis de conduire délivré par un Etat n'appartenant ni à l'Union européenne, ni à l'Espace économique européen est susceptible d'être échangé contre un permis français, il y a seulement lieu de vérifier si, conformément aux dispositions du I de l'article 5 de l'arrêté du 12 janvier 2012, cet Etat est lié à la France par un accord de réciprocité en matière d'échange de permis de conduire.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
