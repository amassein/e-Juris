<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027164312</ID>
<ANCIEN_ID>JG_L_2013_03_000000334188</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/16/43/CETATEXT000027164312.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème et 9ème sous-sections réunies, 11/03/2013, 334188, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-03-11</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>334188</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème et 9ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MASSE-DESSEN, THOUVENIN, COUDRAY</AVOCATS>
<RAPPORTEUR>M. Nicolas Labrune</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Delphine Hedary</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:334188.20130311</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête sommaire et le mémoire complémentaire, enregistrés les 30 novembre 2009 et 23 février 2010 au secrétariat du contentieux du Conseil d'Etat, présentés pour le Syndicat de la magistrature, dont le siège est 12-14, rue Charles Fourier à Paris (75013), le Syndicat des avocats de France, dont le siège est 34, rue Saint-Lazare à Paris (75009), l'Union syndicale Solidaires, dont le siège est 144, boulevard de la Villette à Paris (75019), la Ligue de l'enseignement, dont le siège est 3, rue Récamier à Paris (75341 Cedex 07), la Ligue des droits de l'Homme, dont le siège est 138, rue Marcadet à Paris (75018), le Groupe d'information et de soutien des immigrés (GISTI), dont le siège est 3, villa Marcès à Paris (75011), la Fédération syndicale unitaire, dont le siège est 104, rue Romain Rolland aux Lilas (93260), la Confédération générale du travail (CGT), dont le siège est 263, rue de Paris à Montreuil (93516 Cedex), la Confédération française démocratique du travail (CFDT), dont le siège est 4, boulevard de la Villette à Paris (75955 Cedex 19), l'association Imaginons un réseau internet solidaire (IRIS), dont le siège est 40, rue de la Justice à Paris (75020), l'association INTER-LGBT, dont le siège est 5, rue Perrée à Paris (75003) et l'association AIDES, dont le siège est 14, rue Scandicci à Pantin (93508 Cedex) ; les organisations requérantes demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret n° 2009-1250 du 16 octobre 2009 portant création d'un traitement de données à caractère personnel relatif aux enquêtes administratives liées à la sécurité publique ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la Constitution, notamment son article 22 ;<br/>
<br/>
              Vu la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, notamment son article 8 ;<br/>
<br/>
              Vu la loi n° 78-17 du 6 janvier 1978 ;<br/>
<br/>
              Vu la loi n° 83-634 du 13 juillet 1983, notamment ses articles 6 et 18 ;<br/>
<br/>
              Vu la loi n° 95-73 du 21 janvier 1995, notamment son article 17-1 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Nicolas Labrune, Auditeur,  <br/>
<br/>
              - les observations de la SCP Masse-Dessen, Thouvenin, Coudray, avocat du Syndicat de la magistrature, du Syndicat des avocats de France, de l'Union syndicale Solidaires, de la Ligue de l'enseignement, de la Ligue des droits de l'Homme, du Groupe d'information et de soutien des immigres (GISTI), de la Fédération syndicale unitaire, de la Confédération générale du travail, de la Confédération française démocratique du travail, de l'association Imaginons un réseau internet solidaire (IRIS), de l'association INTER-LGBT et de l'association AIDES,<br/>
<br/>
              - les conclusions de Mme Delphine Hedary, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Masse-Dessen, Thouvenin, Coudray, avocat du Syndicat de la magistrature, du Syndicat des avocats de France, de l'Union syndicale Solidaires, de la Ligue de l'enseignement, de la Ligue des droits de l'Homme, du Groupe d'information et de soutien des immigres (GISTI), de la Fédération syndicale unitaire, de la Confédération générale du travail, de la Confédération française démocratique du travail, de l'association Imaginons un réseau internet solidaire (IRIS), de l'association INTER-LGBT et de l'association AIDES ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant que l'article 17-1 de la loi du 21 janvier 1995 d'orientation et de programmation relative à la sécurité, dans sa rédaction applicable à la date du décret attaqué, dispose que : " Les décisions administratives de recrutement, d'affectation, d'autorisation, d'agrément ou d'habilitation, prévues par des dispositions législatives ou réglementaires, concernant soit les emplois publics participant à l'exercice des missions de souveraineté de l'Etat, soit les emplois publics ou privés relevant du domaine de la sécurité ou de la défense, soit les emplois privés ou activités privées réglementées relevant des domaines des jeux, paris et courses, soit l'accès à des zones protégées en raison de l'activité qui s'y exerce, soit l'utilisation de matériels ou produits présentant un caractère dangereux, peuvent être précédées d'enquêtes administratives destinées à vérifier que le comportement des personnes physiques ou morales intéressées n'est pas incompatible avec l'exercice des fonctions ou des missions envisagées " ; que le décret attaqué du 16 octobre 2009 a créé un traitement automatisé de données à caractère personnel relatif aux enquêtes administratives liées à la sécurité publique ; que le Syndicat de la magistrature et les autres organisations requérantes demandent l'annulation de ce décret ;<br/>
<br/>
              Sur l'intervention du Mouvement contre le racisme et pour l'amitié entre les peuples (MRAP) :<br/>
<br/>
              2. Considérant que le Mouvement contre le racisme et pour l'amitié entre les peuples (MRAP) a intérêt à l'annulation du décret attaqué ; qu'ainsi, son intervention est recevable ;<br/>
<br/>
              Sur la légalité externe : <br/>
<br/>
              En ce qui concerne la compétence du pouvoir réglementaire :<br/>
<br/>
              3. Considérant qu'en vertu des dispositions combinées du I et du IV de l'article 8 et du II de l'article 26 de la loi du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés, les traitements de données à caractère personnel mis en oeuvre pour le compte de l'Etat, qui intéressent la sûreté de l'Etat, la défense ou la sécurité publique et qui portent sur des données à caractère personnel qui font apparaître, directement ou indirectement, les origines raciales ou ethniques, les opinions politiques, philosophiques ou religieuses ou l'appartenance syndicale des personnes, ou qui sont relatives à la santé ou à la vie sexuelle de celles-ci, sont autorisés par décret en Conseil d'Etat pris après avis motivé et publié de la Commission nationale de l'informatique et des libertés ; que la constitutionnalité de ces dispositions législatives ne peut être contestée que par la voie d'une question prioritaire de constitutionnalité ; que la question prioritaire de constitutionnalité soulevée par les organisations requérantes à l'encontre de ces dispositions n'a pas été renvoyée au Conseil constitutionnel ; que, par suite et en application de ces dispositions, le pouvoir réglementaire était compétent pour créer, par le décret attaqué, pris en Conseil d'Etat après avis de la Commission nationale de l'informatique et des libertés, le traitement automatisé relatif aux enquêtes administratives liées à la sécurité publique ; <br/>
<br/>
              En ce qui concerne la régularité de la procédure suivie :<br/>
<br/>
              4. Considérant, en premier lieu, qu'il ressort de la copie de la minute de la section de l'intérieur du Conseil d'Etat, telle qu'elle a été produite au dossier par le ministre, que le texte publié ne contient pas de disposition qui diffèrerait à la fois du projet initial du Gouvernement, et du texte adopté par la section de l'intérieur ; qu'ainsi, aucune méconnaissance des règles qui gouvernent l'examen par le Conseil d'Etat des projets de décret ne saurait être retenue ;<br/>
<br/>
              5. Considérant, en deuxième lieu, que l'organisme dont une disposition législative ou réglementaire prévoit la consultation avant l'intervention d'un texte doit être mis à même d'exprimer son avis sur l'ensemble des questions soulevées par ce texte ; que, dans le cas où, après avoir recueilli son avis, l'autorité compétente pour prendre ce texte envisage d'apporter à son projet des modifications qui posent des questions nouvelles, elle doit le consulter à nouveau ; que, toutefois, dans les circonstances de l'espèce, il ressort des pièces du dossier que la Commission nationale de l'informatique et des libertés a été consultée sur l'ensemble des questions traitées par le texte adopté ; que, par suite, le moyen tiré de l'irrégularité de la consultation de cette commission ne peut qu'être écarté ;<br/>
<br/>
              En ce qui concerne le moyen tiré du défaut de contreseings :<br/>
<br/>
              6. Considérant qu'aux termes de l'article 22 de la Constitution : " Les actes du Premier ministre sont contresignés, le cas échéant, par les ministres chargés de leur exécution " ; que, s'agissant d'actes de nature réglementaire, les ministres chargés de leur exécution sont ceux qui ont compétence pour signer ou contresigner les mesures réglementaires ou individuelles que comporte nécessairement l'exécution de ces actes ; que, contrairement à ce que soutiennent les organisations requérantes, l'exécution du décret attaqué ne comporte pas nécessairement de mesures réglementaires ou individuelles qu'un ministre autre que celui de l'intérieur aurait à signer ou à contresigner ; que, par suite, dès lors que le décret attaqué comporte le contreseing du ministre de l'intérieur, le moyen tiré de l'absence de contreseing d'autres ministres doit être écarté ;<br/>
<br/>
              Sur la légalité interne :<br/>
<br/>
              7. Considérant qu'aux termes de l'article 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales : " 1. Toute personne a droit au respect de sa vie privée et familiale, de son domicile et de sa correspondance. / 2. Il ne peut y avoir ingérence d'une autorité publique dans l'exercice de ce droit que pour autant que cette ingérence est prévue par la loi et qu'elle constitue une mesure qui, dans une société démocratique, est nécessaire à la sécurité nationale, à la sûreté publique, au bien-être économique du pays, à la défense de l'ordre et à la prévention des infractions pénales, à la protection de la santé ou de la morale, ou à la protection des droits et libertés d'autrui " ; qu'aux termes de l'article 6 de la loi du 6 janvier 1978 : " Un traitement ne peut porter que sur des données à caractère personnel qui satisfont aux conditions suivantes : / 1° Les données sont collectées et traitées de manière loyale et licite ; / 2° Elles sont collectées pour des finalités déterminées, explicites et légitimes et ne sont pas traitées ultérieurement de manière incompatible avec ces finalités. (...) ; / 3° Elles sont adéquates, pertinentes et non excessives au regard des finalités pour lesquelles elles sont collectées et de leurs traitements ultérieurs ; / 4° Elles sont exactes, complètes et, si nécessaire, mises à jour ; les mesures appropriées doivent être prises pour que les données inexactes ou incomplètes au regard des finalités pour lesquelles elles sont collectées ou traitées soient effacées ou rectifiées ; / 5° Elles sont conservées sous une forme permettant l'identification des personnes concernées pendant une durée qui n'excède pas la durée nécessaire aux finalités pour lesquelles elles sont collectées et traitées " ;<br/>
<br/>
              8. Considérant qu'il résulte de l'ensemble de ces stipulations et dispositions que l'ingérence dans l'exercice du droit de toute personne au respect de sa vie privée que constituent la collecte, la conservation et le traitement, par une autorité publique, d'informations personnelles nominatives, ne peut être légalement autorisée que si elle répond à des finalités légitimes et que le choix, la collecte et le traitement des données sont effectués de manière adaptée, nécessaire et proportionnée au regard de ces finalités ;<br/>
<br/>
              En ce qui concerne les finalités du traitement :<br/>
<br/>
              9. Considérant qu'aux termes de l'article 1er du décret attaqué, le traitement de données à caractère personnel, intitulé " Enquêtes administratives liées à la sécurité publique ", a pour finalité " de faciliter la réalisation d'enquêtes administratives en application des dispositions du premier alinéa de l'article 17-1 de la loi du 21 janvier 1995 (...) par la conservation des données issues de précédentes enquêtes relatives à la même personne " ; que les finalités légitimes de protection de la sécurité publique ainsi assignées au traitement s'inscrivent dans le cadre des enquêtes administratives prévues par l'article 17-1 de la loi du 21 janvier 1995, dans sa rédaction applicable à la date du décret attaqué, et sont énoncées de façon suffisamment précise ;<br/>
<br/>
              En ce qui concerne les données enregistrées : <br/>
<br/>
              10. Considérant, en premier lieu, que l'article 2 du décret attaqué autorise l'enregistrement des catégories de données à caractère personnel suivantes, recueillies dans le cadre d'enquêtes administratives : " 1° Motif de l'enquête ; / 2° Informations ayant trait à l'état civil, à la nationalité et à la profession, adresses physiques, numéros de téléphone et adresses électroniques (...) " ; que ces dispositions prévoient l'enregistrement du motif de l'enquête pour chaque enquête administrative ; que la catégorie de données " Informations ayant trait à l'état civil, à la nationalité et à la profession " est désignée de façon suffisamment précise au regard des finalités du traitement dès lors, d'une part, que celles-ci consistent à faciliter la réalisation d'enquêtes administratives par la conservation des données issues de précédentes enquêtes relatives à la même personne et, d'autre part, que les données enregistrées à ce titre ne peuvent l'être que dans la stricte mesure où elles sont nécessaires à la poursuite des finalités du traitement et doivent, en conséquence, être en rapport direct avec le motif de l'enquête ; qu'ainsi, le choix, la collecte et le traitement des données enregistrées dans le traitement sont effectués de manière adaptée, nécessaire et proportionnée au regard des finalités poursuivies ;<br/>
<br/>
              11. Considérant, en deuxième lieu, qu'aux termes de l'article 10 de la loi du 6 janvier 1978 : " Aucune autre décision produisant des effets juridiques à l'égard d'une personne ne peut être prise sur le seul fondement d'un traitement automatisé de données destiné à définir le profil de l'intéressé ou à évaluer certains aspects de sa personnalité " ; que lorsqu'une décision administrative est précédée d'une enquête administrative, destinée à vérifier que le comportement des personnes physiques ou morales intéressées n'est pas incompatible avec l'exercice des fonctions ou des missions envisagées, le rapport de cette enquête administrative, conservé dans le traitement automatisé créé par le décret attaqué, ne constitue qu'un des éléments qu'apprécie l'autorité administrative pour prendre sa décision et ne saurait en constituer le seul fondement ; que, par suite, le moyen tiré de ce que le traitement automatisé " Enquêtes administratives liées à la sécurité publique " constituerait, en méconnaissance de l'article 10 de la loi du 6 janvier 1978, le seul fondement de décisions administratives doit, en tout état de cause, être écarté ;<br/>
<br/>
              12. Considérant, en troisième lieu, que l'article 3 du décret attaqué dispose que : " L'interdiction prévue au I de l'article 8 de la loi du 6 janvier 1978 susvisée s'applique au présent traitement. / Toutefois, l'enregistrement de données, contenues dans un rapport d'enquête, relatives à un comportement incompatible avec l'exercice des fonctions ou des missions envisagées est autorisé alors même que ce comportement aurait une motivation politique, religieuse, philosophique ou syndicale " ; que les organisations requérantes soutiennent que ces dispositions méconnaissent le I de l'article 8 de la loi du 6 janvier 1978, le principe d'égal accès aux fonctions publiques, le principe de la liberté d'opinion, garantie aux fonctionnaires par l'article 6 de la loi du 13 juillet 1983, et l'article 18 de cette même loi aux termes duquel : " (...) Il ne peut être fait état dans le dossier d'un fonctionnaire, de même que dans tout document administratif, des opinions ou des activités politiques, syndicales, religieuses ou philosophiques de l'intéressé. (...) " ;<br/>
<br/>
              13. Considérant que le IV de l'article 8 de la loi du 6 janvier 1978 dispose que : " De même, ne sont pas soumis à l'interdiction prévue au I les traitements, automatisés ou non, justifiés par l'intérêt public et autorisés dans les conditions prévues au I de l'article 25 ou au II de l'article 26 " ; que tel est bien le cas du décret attaqué, qui peut donc légalement déroger aux dispositions du I de l'article 8 de la loi du 6 janvier 1978 ;<br/>
<br/>
              14. Considérant que si les dispositions de l'article 3 du décret attaqué permettent d'enregistrer des données relatives à des comportements incompatibles avec l'exercice des fonctions ou des missions envisagées, quand bien même ces comportements auraient une motivation politique, religieuse, philosophique ou syndicale, elles n'ont pas pour objet et ne sauraient avoir légalement pour effet d'autoriser l'enregistrement de données relatives aux opinions ou activités politiques, syndicales, religieuses ou philosophiques des personnes en cause ; qu'il suit de là que, lorsqu'une décision administrative est précédée d'une enquête administrative en application des dispositions du premier alinéa de l'article 17-1 de la loi du 21 janvier 1995, l'autorité administrative n'apprécie pas, pour prendre sa décision, les opinions politiques, religieuses, philosophiques ou syndicales de la personne intéressée, mais seulement si celle-ci a eu un comportement incompatible avec l'exercice des fonctions ou des missions envisagées, quelle que soit la motivation de ce comportement ; que, par suite, les moyens tirés de la méconnaissance du principe d'égal accès aux fonctions publiques et des dispositions des articles 6 et 18 de la loi du 13 juillet 1983 doivent être écartés ;<br/>
<br/>
              En ce qui concerne la durée de conservation des données :<br/>
<br/>
              15. Considérant que l'article 4 du décret attaqué dispose que : " Les données peuvent être conservées pendant une durée maximale de cinq ans à compter de leur enregistrement " ; que l'article 5 dispose que : " Les données mentionnées aux articles 2 et 3 ne peuvent concerner des mineurs que s'ils sont âgés de seize ans au moins et ont fait l'objet d'une enquête administrative mentionnée à l'article 1er " ; qu'en application de l'article 10 du décret attaqué, " le directeur général de la police nationale présente chaque année à la Commission nationale de l'informatique et des libertés un rapport sur ses activités de vérification, de mise à jour et d'effacement des données enregistrées dans le traitement " et que ce rapport " indique également les procédures suivies par les services gestionnaires pour que les données enregistrées soient en permanence exactes, pertinentes et non excessives au regard des finalités pour lesquelles elles sont collectées " ; que la durée de conservation de cinq ans, applicable à toutes les données enregistrées dans le traitement à compter de leur enregistrement, n'excède pas la durée nécessaire pour répondre aux finalités du traitement, quand bien même l'enquête administrative n'aurait mis en évidence aucun comportement incompatible avec l'exercice des fonctions ou des missions envisagées ; qu'il en va ainsi quel que soit l'âge des personnes, dès lors que les seuls mineurs susceptibles d'être concernés par le traitement " Enquêtes administratives liées à la sécurité publique " sont ceux, âgés d'au moins seize ans, concernés par les décisions administratives énumérées à l'article 17-1 de la loi du 21 janvier 1995 et ayant été, à ce titre, l'objet d'une enquête administrative ;<br/>
<br/>
              En ce qui concerne l'accès au traitement automatisé :<br/>
<br/>
              16. Considérant qu'aux termes de l'article 6 du décret attaqué : " Dans la limite du besoin d'en connaître, en vue de la réalisation d'enquêtes administratives, sont autorisés à accéder aux données mentionnées aux articles 2 et 3 : / 1° Les fonctionnaires relevant de la sous-direction de l'information générale de la direction centrale de la sécurité publique, individuellement désignés et spécialement habilités par le directeur central de la sécurité publique ; / 2° Les fonctionnaires affectés dans les services d'information générale des directions départementales de la sécurité publique, individuellement désignés et spécialement habilités par le directeur départemental ; / 3° Les fonctionnaires affectés dans les services de la préfecture de police chargés du renseignement, individuellement désignés et spécialement habilités par le préfet de police. / En outre, peut être destinataire des données mentionnées aux articles 2 et 3, dans la limite du besoin d'en connaître, tout agent d'un service de la police nationale ou de la gendarmerie nationale chargé d'une enquête administrative, sur demande expresse précisant l'identité du demandeur, l'objet et les motifs de la consultation. Les demandes sont agréées par les responsables des services mentionnés aux 1° à 3° " ;<br/>
<br/>
              17. Considérant que les personnes ayant habituellement accès au traitement sont limitativement énumérées et doivent avoir été individuellement désignées et spécialement habilitées ; que si tout autre agent d'un service de la police nationale ou de la gendarmerie nationale peut être destinataire des données enregistrées dans le traitement, ce n'est qu'à la suite d'une demande expresse précisant son identité, l'objet et les motifs de sa demande, celle-ci devant être agréée par les responsables des services ayant habituellement accès au traitement ; que la possibilité d'accéder au traitement " dans la limite du besoin d'en connaître " est suffisamment encadrée dès lors qu'elle renvoie ainsi aux finalités du traitement ; que l'enregistrement des informations relatives à la consultation du traitement et leur conservation pendant une durée de cinq ans permettront de contrôler que le traitement ne fait pas l'objet de consultations abusives ; que le moyen tiré de ce que le décret attaqué prévoirait un accès trop large aux informations traitées doit, par suite, être écarté ;<br/>
<br/>
              18. Considérant qu'il résulte de tout ce qui précède que les organisations requérantes ne sont pas fondées à demander l'annulation du décret attaqué ; que, par suite, leurs conclusions tendant à l'application de l'article L. 761-1 du code de justice administrative ne peuvent qu'être rejetées ;<br/>
<br/>
<br/>
<br/>				D E C I D E  :<br/>
              				--------------<br/>
<br/>
Article 1er : L'intervention du Mouvement contre le racisme et pour l'amitié entre les peuples (MRAP) est admise.<br/>
<br/>
Article 2 : La requête du Syndicat de la magistrature, du Syndicat des avocats de France, de l'Union syndicale Solidaires, de la Ligue de l'enseignement, de la Ligue des droits de l'Homme, du Groupe d'information et de soutien des immigrés, de la Fédération syndicale unitaire, de la Confédération générale du travail, de la Confédération française démocratique du travail, de l'association Imaginons un réseau internet solidaire, de l'association INTER-LGBT et de l'association AIDES est rejetée.<br/>
<br/>
Article 3 : La présente décision sera notifiée au Syndicat de la magistrature, premier requérant dénommé, au Premier ministre et au ministre de l'intérieur.<br/>
Les autres requérants seront informés de la présente décision par la SCP H. Masse-Dessen, G. Thouvenin, avocat au Conseil d'Etat et à la Cour de cassation, qui les représente devant le Conseil d'Etat.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
