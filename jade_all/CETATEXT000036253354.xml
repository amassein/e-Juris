<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036253354</ID>
<ANCIEN_ID>JG_L_2017_12_000000396339</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/25/33/CETATEXT000036253354.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  3ème chambre jugeant seule, 22/12/2017, 396339, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-12-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>396339</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 3ème chambre jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Cécile Isidoro</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emmanuelle Cortot-Boucher</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2017:396339.20171222</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés le 25 janvier 2016 et le 13 février 2017 au secrétariat du contentieux du Conseil d'Etat, la Confédération paysanne demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêté du 18 novembre 2015 du ministre de l'agriculture, de l'agroalimentaire et de la forêt et du ministre de l'économie, de l'industrie et du numérique, portant extension de l'accord interprofessionnel relatif au financement des actions de recherche-développement et d'expérimentation, des actions de promotion, communication et études économiques et autres actions d'intérêt général pour la filière française des céréales pour les campagnes 2016-2017, 2017-2018 et 2018-2019 ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales et son premier protocole additionnel ;<br/>
              - le règlement (UE) n° 1308/2013 du Parlement européen et du Conseil, du 17 décembre 2013 ;<br/>
              - le code rural et de la pêche maritime ;<br/>
              - l'arrêté du ministre de l'agriculture, de l'agroalimentaire et de la forêt, du ministre de l'économie, de l'industrie et du numérique, du ministre de l'outre-mer et du secrétaire d'Etat chargé du budget du 26 février 2015 relatif aux demandes d'extension des accords conclus dans le cadre d'une organisation interprofessionnelle reconnue ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Cécile Isidoro, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Emmanuelle Cortot-Boucher, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. La Confédération paysanne demande l'annulation pour excès de pouvoir de l'arrêté du ministre de l'agriculture, de l'agroalimentaire et de la forêt et du ministre de l'économie, de l'industrie et du numérique du 18 novembre 2015 portant extension de l'accord interprofessionnel relatif au financement des actions de recherche-développement et d'expérimentation, des actions de promotion, communication et études économiques et autres actions d'intérêt général pour la filière française des céréales pour les campagnes 2016-2017, 2017-2018 et 2018-2019.<br/>
<br/>
              Sur l'intervention de la Coordination rurale-Union nationale :<br/>
<br/>
              2. La Coordination rurale-Union nationale justifie d'un intérêt suffisant à l'annulation de l'arrêté attaqué. Ainsi, son intervention au soutien de la requête de la Confédération paysanne est recevable.<br/>
<br/>
              Sur le moyen tiré de l'irrégularité de la procédure de consultation : <br/>
<br/>
              2. La Confédération paysanne soutient que la procédure d'adoption de l'arrêté attaqué a été irrégulière faute de publication du texte de l'accord faisant l'objet de l'extension, concurremment avec l'avis de consultation ou antérieurement à celui-ci, et en raison de l'insuffisance des informations figurant dans le tableau annexé à l'avis de consultation. Aux termes de l'article 165 du règlement (UE) n° 1308/2013 : " Dans le cas où les règles (...) d'une organisation interprofessionnelle sont étendues au titre de l'article 164 et lorsque les activités couvertes par ces règles présentent un intérêt économique général pour les opérateurs économiques dont les activités sont liées aux produits concernés, l'Etat membre qui a accordé la reconnaissance peut décider, après consultation des acteurs concernés, que les opérateurs économiques individuels ou les groupes d'opérateurs non membres de l'organisation qui bénéficient de ces activités sont redevables à l'organisation de tout ou partie des contributions versées par les membres, dans la mesure où ces dernières sont destinées à couvrir les coûts directement liés à la conduite des actions concernées ". Aux termes de l'article 3 de l'arrêté du 26 février 2015 relatif aux demandes d'extension des accords conclus dans le cadre d'une organisation interprofessionnelle reconnue : " La consultation des acteurs concernés lors d'une demande d'extension d'un accord en application de l'article 165 du règlement (UE) n° 1308/2013 du 17 décembre 2013 susvisé est réalisée par une publication pendant trois semaines au Bulletin officiel du ministère chargé de l'agriculture (BO-agri) d'un avis, auquel est annexé le document mentionné au point 8 de l'article 1 ". Le document mentionné au point 8 de l'article 1er de cet arrêté correspond au tableau qui est prévu par l'annexe 1 de ce dernier et qui précise, en premier lieu, la période couverte par l'accord, en deuxième lieu, pour les différentes actions prévisionnelles qui sont financées par les cotisations interprofessionnelles, leur financement prévisionnel par les contributions des acteurs et, en troisième lieu, les modalités de financement par ces contributions.<br/>
<br/>
              3. Il ressort des pièces du dossier qu'avant la publication de l'arrêté attaqué a été publié l'avis imposé par les dispositions précitées de l'article 3 de l'arrêté du 26 février 2015, qui ne prévoient pas que l'accord dont l'extension est envisagée soit lui-même soumis à consultation. Le tableau joint à cet avis a été complété. Dès lors, le moyen tiré de l'irrégularité de procédure, tenant au caractère insuffisant de la consultation qui a été menée au regard de l'arrêté du 18 novembre 2015, doit être écarté.<br/>
<br/>
              Sur le moyen tiré du défaut de représentativité de l'association Intercéréales :<br/>
<br/>
              4. Aux termes de l'article 164 du règlement (UE) n° 1308/2013 du 17 décembre 2013 : " 1. Dans le cas où une organisation de producteurs reconnue, une association d'organisations de producteurs reconnue ou une organisation interprofessionnelle reconnue opérant dans une ou plusieurs circonscriptions économiques déterminées d'un État membre est considérée comme représentative de la production ou du commerce ou de la transformation d'un produit donné, l'État membre concerné peut, à la demande de cette organisation, rendre obligatoires, pour une durée limitée, certains accords, certaines décisions ou certaines pratiques concertées arrêtés dans le cadre de cette organisation pour d'autres opérateurs, individuels ou non, opérant dans la ou les circonscriptions économiques en question et non membres de cette organisation ou association. / (...) / 3. Une organisation ou association est considérée comme représentative lorsque, dans la ou les circonscriptions économiques concernées d'un État membre, elle représente : a) en proportion du volume de la production ou du commerce ou de la transformation du produit ou des produits concernés : i) pour les organisations de producteurs dans le secteur des fruits et légumes, au moins 60 % ; ou ii) dans les autres cas, au moins deux tiers ; et b) dans le cas des organisations de producteurs, plus de 50 % des producteurs concernés. Toutefois, lorsque, dans le cas des organisations interprofessionnelles, la détermination de la proportion du volume de la production ou du commerce ou de la transformation du produit ou des produits concernés pose des problèmes pratiques, un État membre peut fixer des règles nationales afin de déterminer le niveau précis de représentativité visé au premier alinéa, point a) ii) ".<br/>
<br/>
              5. Les règles nationales mentionnées au point 3 figurent à l'article L. 632-4 du code rural et de la pêche maritime, aux termes duquel : " Pour l'application de l'article 164 du règlement (UE) n° 1308/2013 du Parlement européen et du Conseil du 17 décembre 2013 portant organisation commune des marchés des produits agricoles et abrogeant les règlements (CEE) n° 922/72, (CEE) n° 234/79, (CE) n° 1037/2001 et (CE) n° 1234/2007 du Conseil, la représentativité des organisations interprofessionnelles est appréciée en tenant compte de la structuration économique de chaque filière. Les volumes pris en compte sont ceux produits, transformés ou commercialisés par les opérateurs professionnels auxquels sont susceptibles de s'appliquer les obligations prévues par les accords. En outre, lorsque la détermination de la proportion du volume de la production ou de la commercialisation ou de la transformation du produit ou des produits concernés pose des problèmes pratiques, l'organisation interprofessionnelle est regardée comme représentative si elle représente deux tiers de ces opérateurs ou de leur chiffre d'affaires./ Pour la production, ces conditions sont présumées respectées lorsque des organisations syndicales d'exploitants agricoles représentant au total au moins 70 % des voix aux élections des chambres d'agriculture participent à l'organisation interprofessionnelle, directement ou par l'intermédiaire d'associations spécialisées adhérentes à ces organisations. / Pour tout secteur d'activité, ces conditions sont présumées respectées lorsque l'organisation interprofessionnelle démontre que l'accord dont l'extension est demandée n'a pas fait l'objet, dans le mois suivant sa publication par cette organisation, de l'opposition d'organisations professionnelles réunissant des opérateurs économiques de ce secteur d'activité représentant au total plus du tiers des volumes du secteur d'activité concerné ". <br/>
<br/>
              6. La Confédération paysanne soutient que l'association Intercéréales n'est pas représentative, au sens des dispositions citées aux points 4 et 5, faute de justifier de la part du volume de la production, de la commercialisation ou de la transformation des céréales qu'elle représente, et qu'au demeurant les seuils de représentativité ne sont pas définis par la loi.<br/>
<br/>
              7. Tant l'article 164 du règlement (UE) n° 1308/2013 du 17 décembre 2013 que l'article L. 632-4 du code rural et de la pêche maritime disposent que, pour qu'elle soit considérée comme représentative, une organisation ou une association doit assurer au moins les deux tiers de la production, du commerce ou de la transformation du ou des produits concernés. Dès lors le moyen tiré de ce que les textes applicables en la matière ne fixeraient aucun seuil de représentativité manque en fait.<br/>
<br/>
              8. Il ressort des pièces du dossier qu'en matière de production, les volumes mis sur le marché dans la filière des céréales, lesquels doivent s'entendre des volumes collectés en France selon les modalités prévues par l'article L. 666-1 du code rural et de la pêche maritime, par les sociétés coopératives agricoles membres de l'association Intercéréales par le biais de " Coop de France métiers du grain " représentent plus de 68 % du total des volumes collectés. S'agissant de la transformation, les opérateurs membres de l'association Intercéréales transforment la totalité des volumes de blé dur et de malt, 80 % du volume de farine et 85 % du volume des aliments pour animaux. Enfin, les opérateurs membres d'Intercéréales commercialisent plus de 90 % des volumes commercialisés en France. Si ces chiffres portent sur les campagnes 2010-2011, 2011-2012 et 2012-2013 et si la Confédération paysanne soutient que ces campagnes seraient trop anciennes pour pouvoir être prises en compte, il ne ressort pas des pièces du dossier que les données ont substantiellement changé au cours des campagnes plus récentes qui ont précédé l'adoption de l'arrêté litigieux. Par ailleurs, il n'y a pas lieu de tenir compte des volumes de production auto-consommés puisqu'ils ne sont pas soumis aux cotisations volontaires obligatoires prévues par l'accord. Il suit de là que l'association Intercéréales doit être regardée comme satisfaisant à l'exigence de représentativité résultant de l'article 164 du règlement (UE) n° 1308/2013 du 17 décembre 2013 et de l'article L. 632-4 du code rural et de la pêche maritime.<br/>
<br/>
              9. La Confédération paysanne fait valoir, enfin, qu'elle-même et une autre organisation professionnelle se sont opposées à l'accord faisant objet de l'extension et que les organisations professionnelles membres de l'association Intercéréales n'auraient obtenu que 55,57 % des voix aux dernières élections aux chambres d'agriculture. En vertu, toutefois, des dispositions précitées de l'article L. 632-4 du code rural et de la pêche maritime, les critères de représentativité évalués à partir des résultats obtenus aux élections professionnelles ne revêtent qu'un caractère subsidiaire et n'ont vocation à s'appliquer que " lorsque la détermination de la proportion du volume de la production ou de la commercialisation ou de la transformation du produit ou des produits concernés pose des problèmes pratiques ". Or, il résulte de ce qui a été dit au point 8 qu'en l'espèce l'appréciation des volumes de céréales produits, commercialisés ou transformés par les membres de l'association Intercéréales ne soulève pas de difficultés pratiques. Dès lors, le moyen tiré de ce que l'association Intercéréales ne satisferait pas à ces critères subsidiaires de représentativité ne peut qu'être écarté. <br/>
<br/>
              Sur le moyen tiré de ce que l'accord faisant l'objet de l'extension n'a pas été adopté à l'issue d'un vote à la majorité qualifiée au sein de chacun des collèges professionnels de l'association Intercéréales, en méconnaissance des statuts de cette dernière :<br/>
<br/>
              10. Si l'article 13 des statuts de l'association Intercéréales prévoit que " les décisions dont l'extension pourrait être demandée dans le cadre de l'article L. 632-3 du livre VI du code rural et de l'article 164 du règlement (UE) n° 1308/2013 (...) doivent être prises à l'unanimité des collèges professionnels, le vote de chaque collège étant décidé à la majorité des 8/10 de ses délégués présents ou représentés au cours d'une réunion préparatoire propre à chaque collège ", il ressort du procès-verbal de l'assemblée générale ordinaire de l'association Intercéréales du 13 octobre 2015 que cette majorité a été acquise dans les trois collèges de cette association. La circonstance que l'association Intercéréales n'aurait obtenu, par l'intermédiaire de certains syndicats d'agriculteurs, que 55,7 % des voix aux dernières élections professionnelles aux chambres d'agriculture est dépourvue de toute pertinence à cet égard. Dès lors, le moyen tiré de ce que l'accord faisant l'objet de l'extension n'a pas été adopté à l'issue d'un vote à la majorité des 8/10 au sein de chacun des collèges professionnels de l'association Intercéréales, en méconnaissance de ses statuts, doit être écarté.<br/>
<br/>
              Sur le moyen tiré de la méconnaissance du principe de liberté d'association :<br/>
<br/>
              11. Si la Confédération paysanne soutient, à cet égard, que les montants prélevés au titre des cotisations volontaires obligatoires prévues par l'accord faisant l'objet de l'extension seraient sans rapport avec les missions d'intérêt général dévolues à l'association Intercéréales et que les missions financées par ces cotisations profiteraient surtout aux membres de celle-ci, elle n'apporte aucun élément susceptible de l'établir. Par ailleurs, il est constant que les cotisations volontaires obligatoires n'ont ni pour objet, ni pour effet d'imposer, directement ou indirectement, à quiconque l'adhésion ou le maintien de l'adhésion à une organisation professionnelle. Dès lors, le moyen tiré de la méconnaissance du principe de liberté d'association, garanti par l'article 11 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, doit être écarté.<br/>
<br/>
              Sur le moyen tiré de la méconnaissance de l'article 1er du premier protocole additionnel à la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales :<br/>
<br/>
              12. Si la Confédération paysanne soutient qu'il n'est pas démontré que les cotisations volontaires obligatoires versées à l'association Intercéréales en vertu de l'arrêté litigieux poursuivraient un but d'intérêt général, elle n'apporte aucun commencement de preuve à l'appui de ses allégations. Le moyen doit, par suite, être écarté. <br/>
<br/>
              Sur le moyen tiré de la méconnaissance du principe de liberté du commerce et de l'industrie : <br/>
<br/>
              13. Si la Confédération paysanne soutient que les montants prélevés au titre des cotisations volontaires obligatoires prévues par l'accord faisant l'objet de l'extension sont excessifs, son moyen n'est pas assorti des précisions suffisantes pour en apprécier le bien fondé. Dès lors, le moyen tiré de la méconnaissance du principe de liberté du commerce et de l'industrie doit être rejeté. <br/>
<br/>
              14. Il résulte de tout ce qui précède, sans qu'il soit besoin de statuer sur la fin de non-recevoir opposée par l'association Intercéréales, que la Confédération paysanne n'est pas fondée à demander l'annulation de l'arrêté attaqué. Sa requête doit dès lors être rejetée, y compris ses conclusions présentées, conjointement avec la Coordination rurale Union nationale, au titre de l'article L. 761-1 du code de justice administrative. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la Confédération paysanne le versement à l'association Intercéréales de la somme de 3 000 euros, au titre de ces mêmes dispositions. <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'intervention de la Coordination rurale Union nationale est admise.<br/>
Article 2 : La requête de la Confédération paysanne est rejetée.<br/>
Article 3 : La Confédération paysanne versera à l'association Intercéréales la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à la Confédération paysanne, au ministre de l'agriculture, de l'agroalimentaire et de la forêt, au ministre de l'économie, de l'industrie et du numérique, à l'association Intercéréales et à la Coordination rurale Union nationale. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
