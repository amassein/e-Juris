<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036610505</ID>
<ANCIEN_ID>JG_L_2018_02_000000405306</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/61/05/CETATEXT000036610505.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème - 7ème chambres réunies, 16/02/2018, 405306, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-02-16</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>405306</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème - 7ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FOUSSARD, FROGER ; SCP GARREAU, BAUER-VIOLAS, FESCHOTTE-DESBOIS</AVOCATS>
<RAPPORTEUR>Mme Sophie-Caroline de Margerie</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Guillaume Odinet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:405306.20180216</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A...B...a demandé au tribunal administratif de Cergy-Pontoise de condamner la commune de Saint-Brice-sous-Forêt (Val d'Oise) à lui verser la somme de 24 452 euros en réparation du préjudice moral qu'il estime avoir subi du fait du harcèlement moral qu'elle a exercé à son encontre. Par un jugement n° 1107193 en date du 23 juin 2014, le tribunal administratif de Cergy-Pontoise a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 14VE02598 du 22 septembre 2016, la cour administrative d'appel de Versailles a, sur appel de M.B..., condamné la commune de Saint-Brice-sous-Forêt à lui verser la somme de 5 000 euros en réparation du préjudice moral qu'il estime avoir subi du fait du harcèlement moral exercé à son encontre, réformé en ce sens le jugement du tribunal administratif et rejeté le surplus des conclusions de la requête de M. B...et les conclusions de la commune de Saint-Brice-sous-Forêt.  <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 22 novembre 2016 et 22 février 2017 au secrétariat du contentieux du Conseil d'Etat, la commune de Saint-Brice-sous-Forêt  demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler  cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de M. B...et de faire droit à l'ensemble de ses conclusions d'appel et de première instance ;<br/>
<br/>
              3°) de mettre à la charge de M. B...la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la loi n° 83-634 du 13 juillet 1983 ;<br/>
              - la loi n° 84-53 du 26 janvier 1984 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Sophie-Caroline de Margerie, conseiller d'Etat,<br/>
<br/>
              - les conclusions de M. Guillaume Odinet, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Foussard, Froger, avocat de la commune de Saint-Brice-sous-Forêt, et à la SCP Garreau, Bauer-Violas, Feschotte-Desbois, avocat de M. B...; <br/>
<br/>
              Vu la note en délibéré, enregistrée le 31 janvier 2018, présentée par M. B...;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1.	Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que M. A...B..., animateur territorial au sein des services de la commune de Saint-Brice-sous-Forêt, a demandé à cette dernière, par un courrier du 29 avril 2011, reçu le 2 mai 2011, la réparation du préjudice, à hauteur de 24 452 euros, qu'il estimait avoir subi à raison des faits constitutifs de harcèlement moral dont il aurait été victime dans l'exercice de ses fonctions ; que, par un jugement du 23 juin 2014, le tribunal administratif de Cergy-Pontoise a rejeté sa demande de condamnation de la commune ; que celle-ci se pourvoit en cassation contre l'arrêt du 22 septembre 2016 par lequel la cour administrative d'appel de Versailles a réformé le jugement du tribunal administratif de Cergy-Pontoise et l'a condamnée à verser à M. B...la somme de 5 000 euros au titre des troubles dans les conditions d'existence du fait de harcèlement moral ;<br/>
<br/>
              2.	Considérant qu'aux termes du premier alinéa de l'article 6 quinquies de la loi du 13 juillet 1983 : " Aucun fonctionnaire ne doit subir les agissements répétés de harcèlement moral qui ont pour objet ou pour effet une dégradation des conditions de travail susceptible de porter atteinte à ses droits et à sa dignité, d'altérer sa santé physique ou mentale ou de compromettre son avenir professionnel " ; qu'il appartient à un agent public qui soutient avoir été victime d'agissements constitutifs de harcèlement moral, de soumettre au juge des éléments de fait susceptibles de faire présumer l'existence d'un tel harcèlement ; qu'il incombe à l'administration de produire, en sens contraire, une argumentation de nature à démontrer que les agissements en cause sont justifiés par des considérations étrangères à tout harcèlement ; que la conviction du juge, à qui il revient d'apprécier si les agissements de harcèlement sont ou non établis, se détermine au vu de ces échanges contradictoires, qu'il peut compléter, en cas de doute, en ordonnant toute mesure d'instruction utile ; que, pour apprécier si des agissements dont il est allégué qu'ils sont constitutifs d'un harcèlement moral revêtent un tel caractère, le juge administratif doit tenir compte des comportements respectifs de l'administration auquel il est reproché d'avoir exercé de tels agissements et de l'agent qui estime avoir été victime d'un harcèlement moral ; que, pour être qualifiés de harcèlement moral, ces agissements doivent être répétés et excéder les limites de l'exercice normal du pouvoir hiérarchique ; que, dès lors qu'elle n'excède pas ces limites, une simple diminution des attributions justifiée par l'intérêt du service, en raison d'une manière de servir inadéquate ou de difficultés relationnelles, n'est pas constitutive de harcèlement moral ;<br/>
<br/>
              3.	Considérant que, pour juger que la commune de Saint-Brice-sous-Forêt s'était rendue coupable envers M. B...de faits constitutifs de harcèlement moral, la cour a retenu que celui-ci, qui avait toujours été bien noté et dont la manière de servir donnait satisfaction, avait été affecté en août 2010, à la suite d'un conflit avec son supérieur hiérarchique, auprès du pôle " médiation et démocratie participative ", en vue de mettre en place et d'animer un " point information jeunesse " et que cette affectation s'était traduite par son isolement dans un local préfabriqué éloigné géographiquement des autres services de la commune et dépourvu de visiteurs, l'avait empêché d'avoir des relations de travail normales et apaisées avec sa hiérarchie et lui avait causé des troubles anxio-dépressifs nécessitant un arrêt de travail en février 2011 ; <br/>
<br/>
              4.	Considérant, toutefois, qu'il ressort des pièces du dossier soumis aux juges du fond que la décision de changement d'affectation de l'intéressé est intervenue alors que le comportement agressif de M. B...et ses difficultés relationnelles avec les agents et les interlocuteurs extérieurs du service du scolaire, de la petite enfance et de l'enfance avaient déjà été relevés dans un rapport établi le 17 juin 2010 par son supérieur hiérarchique de l'époque et jugés incompatibles avec le bon fonctionnement du service ; que sa nouvelle affectation correspondait à son grade et à ses compétences ; que l'installation du futur " point information jeunesse " s'est faite dans un nouveau local, situé à quelques centaines de mètres des locaux des services de la commune qui a été progressivement équipé en vue de son ouverture au public, prévue et effectivement intervenue en janvier 2011 ; que l'absence de visiteurs de septembre à décembre 2010 s'explique par le fait que le service n'était pas encore en activité durant cette période, au cours de laquelle il appartenait à M. B...d'accomplir l'ensemble des tâches requises pour permettre l'accueil des visiteurs en début d'année suivante ; que l'ouverture du service a été annoncée dans un article du journal municipal où figuraient le nom, la qualité et la photographie de M. B...; que de nouvelles difficultés relationnelles ont opposé l'intéressé à sa supérieure hiérarchique dès sa prise de fonctions, contraignant le maire à lui désigner une autre responsable, puis à le nommer dans un autre poste en février 2011 ; qu'en déduisant ainsi des éléments qu'elle avait relevés que l'affectation de M. B...en août 2010 l'avait conduit à subir, pendant quelques mois, une situation de relégation et d'isolement constitutive de harcèlement moral, la cour administrative d'appel de Versailles  a inexactement qualifié les faits qui lui étaient soumis ;<br/>
<br/>
              5.	Considérant, par suite et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, que la commune de Saint-Brice-sous-Forêt est fondée à demander l'annulation de l'arrêt qu'elle attaque ;<br/>
<br/>
              6.	Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              7.	Considérant qu'il résulte de l'instruction qu'à la suite d'un rapport de son supérieur hiérarchique en date du 17 juin 2010, il a été décidé de changer M. B...d'affectation au sein des services de la commune de Saint-Brice-sous-Forêt ; qu'ayant été chargé de la préfiguration d'un futur " point information jeunesse ", l'intéressé a été installé seul, à partir du mois de septembre 2010, dans les locaux préfabriqués éloignés des autres services communaux, qui n'étaient pas encore aménagés et ne pouvaient donc recevoir le public ; qu'à la suite d'un rapport de sa nouvelle supérieure hiérarchique, il a été sanctionné d'un avertissement ; qu'ayant fait l'objet d'un nouveau changement d'affectation en février 2011, il a été placé en congé de maladie en raison d'un état anxio-dépressif réactionnel et d'un stress au travail ; que ces éléments de fait sont susceptibles de faire présumer l'existence d'agissements constitutifs d'un harcèlement moral ;<br/>
<br/>
              8.	Considérant, toutefois, que, eu égard à ce qui a été dit au point 2 ci-dessus, ces faits ne peuvent être appréciés sans tenir compte du comportement de l'intéressé et de l'intérêt du service ; qu'il résulte de l'instruction que, comme le fait valoir l'administration, M. B... a fait preuve, avant son affectation au " point  information jeunesse ", d'un comportement excessivement autoritaire et a tenu des propos déplacés ; qu'ainsi, contrairement à ce qu'il fait valoir, il ne saurait être soutenu que son comportement n'avait fait l'objet d'aucun reproche avant l'affectation qu'il critique ; que l'installation du " point information jeunesse " en dehors des locaux des services de la mairie se justifiait par l'espace nécessaire au fonctionnement de cette structure ; que la mission de préfiguration confiée à M.B..., avant l'ouverture au public du local, n'a revêtu aucun caractère fictif ; que l'aménagement du local a eu lieu de manière progressive, dans les délais initialement prévus ; que, par ailleurs, si M. B... fait état d'une diminution de sa rémunération liée à cette affectation, il ressort des pièces du dossier que les indemnités versées dans ses précédentes fonctions de coordonnateur compensaient ses astreintes et qu'en l'absence de telles sujétions dans ses nouvelles fonctions, leur suppression était justifiée ; que, s'il se plaint de ne pas avoir obtenu le remboursement des frais d'utilisation du véhicule qui était mis à sa disposition pour les besoins du service, il ne conteste pas s'être abstenu de fournir régulièrement ses états de frais et avoir bénéficié à titre gracieux d'une carte d'essence de la commune ; que le rejet, en février 2011, de sa demande d'avancement de grade ne révèle par lui-même aucune marque d'hostilité à son égard alors qu'il n'était titulaire dans le corps d'animateur territorial que depuis le 1er mars 2010 et qu'ainsi qu'il a été dit, son comportement faisait l'objet de vives critiques ; que l'avertissement dont il a fait l'objet, annulé pour vice de procédure, était justifié par ce comportement, lequel avait donné lieu à deux rapports circonstanciés ; que, dans ces conditions, et compte tenu de l'ensemble des échanges contradictoires entre la commune et M.B..., les faits allégués par celui-ci ne peuvent être regardés comme caractérisant un harcèlement moral ; que la circonstance qu'il ait été placé en congé de maladie en raison d'un état anxio-dépressif réactionnel et d'un stress au travail n'est pas de nature à infirmer cette analyse ;<br/>
<br/>
              9.	Considérant qu'il résulte de tout ce qui précède que  M. B...n'est pas fondé à se plaindre de ce que, par le jugement attaqué du 23 juin 2014, qui est suffisamment motivé et dont la minute comporte les signatures requises par les dispositions de l'article R. 741-7 du code de justice administrative, le tribunal administratif de Cergy-Pontoise a rejeté ses conclusions à fin d'indemnisation ;<br/>
<br/>
              10.	Considérant qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées au titre de l'article L. 761-1 du code de justice administrative par la commune de Saint-Brice-sous-Forêt ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : Les articles 1 à 3 de l'arrêt de la cour administrative d'appel de Versailles du 22 septembre 2016 sont annulés.<br/>
<br/>
Article 2 : La requête de M. B...devant la cour administrative d'appel de Versailles est rejetée.<br/>
<br/>
Article 3 : Les conclusions présentées par la commune de Saint-Brice-sous-Forêt au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 4 : La présente décision sera notifiée à la commune de Saint-Brice-sous-Forêt, à M. A... B...et au ministre de l'action et des comptes publics.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
