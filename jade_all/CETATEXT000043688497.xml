<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043688497</ID>
<ANCIEN_ID>JG_L_2021_06_000000418694</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/68/84/CETATEXT000043688497.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème - 5ème chambres réunies, 21/06/2021, 418694, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-06-21</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>418694</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème - 5ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP THOUVENIN, COUDRAY, GREVY</AVOCATS>
<RAPPORTEUR>Mme Fanélie Ducloz</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Olivier  Fuchs</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:418694.20210621</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une décision n° 418694 du 9 septembre 2020, le Conseil d'Etat, statuant au contentieux a sursis à statuer sur la requête du Syndicat des avocats de France tendant à l'annulation du point 5.1.3.2.6 de l'arrêté du 18 août 2016 du garde des sceaux, ministre de la justice, portant approbation de la politique ministérielle de défense et de sécurité au sein du ministère de la justice et de la décision implicite du garde des sceaux, ministre de la justice, refusant de l'abroger, jusqu'à ce que le Tribunal des conflits ait tranché la question de savoir quel est l'ordre de juridiction compétent pour connaître de cette requête.<br/>
<br/>
              Par une décision du 8 février 2021, le Tribunal des conflits a déclaré la juridiction administrative compétente pour connaître du litige.<br/>
<br/>
              Par une intervention, enregistrée le 22 mars 2021, le Conseil national des barreaux demande que le Conseil d'Etat fasse droit aux conclusions de la requête du Syndicat des avocats de France. Il se réfère aux moyens exposés dans la requête.<br/>
<br/>
              En application des dispositions de l'article R. 611-7 du code de justice administrative, les parties ont été informées que la décision du Conseil d'Etat était susceptible d'être notamment fondée sur le moyen, relevé d'office, tiré de l'irrecevabilité, comme tardives, des conclusions de la requête dirigées contre l'arrêté du garde des sceaux, ministre de la justice, du 18 août 2016.<br/>
<br/>
              Vu les autres pièces du dossier, y compris celles visées par la décision du Conseil d'Etat du 9 septembre 2020 ;<br/>
<br/>
              Vu :<br/>
              - la Constitution ;<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - la directive (UE) 2016/343 du Parlement européen et du Conseil du 9 mars 2016 ;<br/>
              - le code de la défense ;<br/>
              - le code de procédure pénale ;<br/>
              - l'arrêté du 2 juin 2006 fixant la liste des secteurs d'activité d'importance vitale et désignant les ministres coordonnateurs desdits secteurs ;<br/>
              - l'arrêté du 8 juin 2010 modifiant l'arrêté du 30 octobre 1981 portant création d'un Bulletin officiel du ministère de la justice ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Fanélie Ducloz, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Olivier Fuchs, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Thouvenin, Coudray, Grevy, avocat du Syndicat des avocats de France ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. L'arrêté du 18 août 2016 du garde des sceaux, ministre de la justice, portant approbation de la politique ministérielle de défense et de sécurité au sein du ministère de la justice, définit différentes zones au sein des locaux judiciaires afin d'organiser leur défense et leur sécurité. Il prévoit notamment, au sein des palais de justice, des zones réservées aux détenus avant leur comparution et un box sécurisé dans les salles d'audience. Le point 5.1.3.2.6 de l'arrêté prévoit que " Les boxes sécurisés en salles d'audiences sont des espaces fermés destinés à accueillir les prévenus retenus sous escorte. Deux types de sécurisation du box détenus sont recommandés : le premier à vitrage complet du box, le second à barreaudage en façade avec un vitrage sur les faces latérales côté public et coté magistrats ". Par une lettre du 26 octobre 2017, le Syndicat des avocats de France a saisi le garde des sceaux, ministre de la justice, d'une demande tendant à l'abrogation des dispositions de ce point 5.1.3.2.6. Il demande l'annulation pour excès de pouvoir, dans cette mesure, de cet arrêté ainsi que de la décision implicite refusant de l'abroger.<br/>
<br/>
              Sur la recevabilité des conclusions tendant à l'annulation de l'arrêté du 18 août 2016 :<br/>
<br/>
              2. La publication d'un acte réglementaire dans un recueil autre que le Journal officiel fait courir le délai de recours contentieux à son encontre si l'obligation de publier cet acte dans ce recueil résulte d'un texte législatif ou réglementaire lui-même publié au Journal officiel de la République française.<br/>
<br/>
              3. L'arrêté du 18 août 2016 a été publié au Bulletin officiel du ministère de la justice du 31 août 2016. Cette publication a fait courir le délai du recours contentieux, en conséquence de l'article 2 de l'arrêté du 8 avril 2010 modifiant l'arrêté du 30 octobre 1981 portant création d'un Bulletin officiel de la justice, lequel prévoit qu'y sont notamment publiés les directives, instructions, circulaires, notes qui comportent une interprétation du droit positif ou une description des procédures administratives et qui n'ont pas fait l'objet d'une publication au Journal officiel, ainsi que les arrêtés et décisions non publiés au Journal officiel de la République française, pris en application de mesures de portée générale ou individuelle.<br/>
<br/>
              4. Il résulte de ce qui précède que les conclusions présentées le 1er mars 2018 par le Syndicat des avocats de France et tendant à l'annulation pour excès de pouvoir de cet arrêté sont tardives et, par suite, irrecevables.<br/>
<br/>
              Sur l'intervention du Conseil national des barreaux :<br/>
<br/>
              5. Le Conseil national des barreaux justifie, eu égard à la nature et l'objet du litige, d'un intérêt suffisant pour intervenir au soutien des conclusions de la requête du Syndicat des avocats de France tendant à l'annulation de la décision refusant d'abroger le point 5.1.3.2.6 de l'arrêté du 18 août 2016. Son intervention est, par suite, recevable.<br/>
<br/>
              Sur les conclusions dirigées contre la décision refusant d'abroger l'arrêté du 18 août 2016 en ce qu'il concerne l'installation de boxes sécurisés à barreaudage :<br/>
<br/>
              6. L'effet utile de l'annulation pour excès de pouvoir du refus d'abroger un acte réglementaire illégal réside dans l'obligation, que le juge peut prescrire d'office en vertu des dispositions de l'article L. 911-1 du code de justice administrative, pour l'autorité compétente, de procéder à l'abrogation de cet acte afin que cessent les atteintes illégales que son maintien en vigueur porte à l'ordre juridique. Il s'ensuit que lorsque l'acte réglementaire dont l'abrogation est demandée cesse de recevoir application avant que le juge, saisi d'un recours pour excès de pouvoir contre le refus de l'abroger, ait statué, ce recours perd son objet.<br/>
<br/>
              7. Il ressort des pièces du dossier que, par une instruction du 22 décembre 2017, le garde des sceaux, ministre de la justice a interrompu tout nouveau déploiement de boxes sécurisés à barreaudage dans les salles d'audience des juridictions judiciaires et demandé le démontage de ceux de ces boxes déjà installés, lesquels, au demeurant, n'ont été utilisés que jusqu'au 22 décembre 2017, et ont tous été déposés en 2018 et 2019. L'instruction du 22 décembre 2017 doit ainsi être regardée comme ayant abrogé l'arrêté du 18 août 2016 en tant qu'il prévoyait la possibilité d'installer, dans les salles d'audience des juridictions judiciaires, des boxes sécurisés à barreaudage.<br/>
<br/>
              8. Il résulte de ce qui précède qu'il n'y a plus lieu de statuer sur les conclusions de la requête du Syndicat des avocats de France en tant qu'elles sont dirigées contre la décision du garde des sceaux, ministre de la justice, refusant d'abroger l'arrêté du 18 août 2016 en tant qu'il recommande l'installation de boxes sécurisés à barreaudage dans les salles d'audience des juridictions judiciaires.<br/>
<br/>
              Sur les conclusions dirigées contre la décision refusant d'abroger l'arrêté du 18 août 2016 en ce qu'il concerne l'installation de boxes sécurisés vitrés :<br/>
<br/>
              9. En raison de la permanence de l'acte réglementaire, la légalité des règles qu'il fixe, la compétence de son auteur et l'existence d'un détournement de pouvoir doivent pouvoir être mises en cause à tout moment, de telle sorte que puissent toujours être sanctionnées les atteintes illégales que cet acte est susceptible de porter à l'ordre juridique. Cette contestation peut prendre la forme d'un recours pour excès de pouvoir dirigé contre la décision refusant d'abroger l'acte réglementaire, comme l'exprime l'article L. 243-2 du code des relations entre le public et l'administration aux termes duquel : " L'administration est tenue d'abroger expressément un acte réglementaire illégal ou dépourvu d'objet, que cette situation existe depuis son édiction ou qu'elle résulte de circonstances de droit ou de faits postérieures, sauf à ce que l'illégalité ait cessé (...) ".<br/>
<br/>
              10. En premier lieu, l'arrêté attaqué a été pris sur le fondement des articles L. 1332-1 et R. 1332-1 et suivants du code de la défense. Ces dispositions imposent, pour chaque secteur d'importance vitale, la réalisation, par le ministre compétent, d'une analyse des risques et l'adoption d'une ou plusieurs directives nationales de sécurité définissant des mesures planifiées et graduées de vigilance, de prévention, de protection et de réaction contre toute menace. A ce titre, l'arrêté du 2 juin 2006 fixant la liste des secteurs d'activité d'importance vitale et désignant les ministres coordonnateurs desdits secteurs, a désigné le ministre de la justice comme ministre coordonnateur pour le secteur vital des activités judiciaires. Le ministre de la justice est ainsi compétent pour, dans ce secteur, adapter la directive nationale de sécurité et élaborer la politique ministérielle de sécurité et de défense et, à ce titre, arrêter des recommandations relatives à la sécurisation des salles d'audience des tribunaux, telles que celles en cause en l'espèce. Par suite, le moyen tiré de ce que le ministre de la justice n'aurait pas eu compétence pour prendre l'arrêté du 18 août 2016 doit être écarté.<br/>
<br/>
              11. En deuxième lieu, l'article préliminaire du code de procédure pénale dispose, au 4ème alinéa de son III, que " les mesures de contraintes dont la personne suspectée ou poursuivie peut faire l'objet sont prises sur décision ou sous le contrôle effectif de l'autorité judiciaire. Elles doivent être strictement limitées aux nécessités de la procédure, proportionnées à la gravité de l'infraction reprochée et ne pas porter atteinte à la dignité de la personne ". L'article 309 du même code prévoit que : " Le président a la police de l'audience et la direction des débats. / Il rejette tout ce qui tendrait à compromettre leur dignité ou à les prolonger sans donner lieu d'espérer plus de certitude dans les résultats ". Aux termes de l'article 318 du même code : " L'accusé comparaît libre et seulement accompagné de gardes pour l'empêcher de s'évader ". <br/>
<br/>
              12. Il résulte de la combinaison de ces dispositions que l'article 318 du code de procédure pénale ne fait pas obstacle à ce que soient prises à l'égard de la personne prévenue ou accusée, dans le respect des droits de la défense, les mesures de contrainte justifiées par la sécurité des personnes présentes à l'audience ou la nécessité de l'empêcher de fuir ou de communiquer avec des tiers. Ainsi, cet article n'interdit pas que l'accusé comparaisse dans un box sécurisé vitré si les circonstances le justifient. Il en résulte que l'installation de boxes sécurisés vitrés dans les salles d'audience des juridictions judiciaires n'est pas, par elle-même, contraire à ces dispositions. Le moyen tiré de la méconnaissance, par l'arrêté du 18 août 2016, de l'article 318 du code de procédure pénale ne peut, dès lors, qu'être écarté.<br/>
<br/>
              13. En troisième lieu, d'une part, aux termes de l'article 9 de la Déclaration des droits de l'homme et du citoyen : " Tout homme étant présumé innocent jusqu'à ce qu'il ait été déclaré coupable, s'il est jugé indispensable de l'arrêter, toute rigueur qui ne serait pas nécessaire pour s'assurer de sa personne doit être sévèrement réprimée par la loi ". Le paragraphe 2 de l'article 6 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales stipule que : " Toute personne accusée d'une infraction est présumée innocente jusqu'à ce que sa culpabilité ait été légalement établie ".<br/>
<br/>
              14. D'autre part, selon l'article 5 de la directive du 9 mars 2016 portant renforcement de certains aspects de la présomption d'innocence et du droit d'assister à son procès dans le cadre de procédures pénales : " 1. Les États membres prennent les mesures appropriées pour veiller à ce que les suspects et les personnes poursuivies ne soient pas présentés, à l'audience ou en public, comme étant coupables par le recours à des mesures de contrainte physique. / 2. Le paragraphe 1 n'empêche pas les États membres d'appliquer les mesures de contrainte physique qui s'avèrent nécessaires pour des raisons liées au cas d'espèce relatives à la sécurité ou à la nécessité d'empêcher les suspects ou les personnes poursuivies de prendre la fuite ou d'entrer en contact avec des tiers ".<br/>
<br/>
              15. Les dispositions contestées de l'arrêté du 16 août 2018 se bornent à définir les modalités de sécurisation des boxes des salles d'audience des juridictions judiciaires destinés à accueillir des prévenus ou des accusés retenus sous escorte. Elles n'ont ni pour objet ni pour effet d'instaurer une présomption de culpabilité à leur égard. Le placement de ces personnes dans un box sécurisé vitré est décidé, dans le respect des dispositions citées au point 11, par la juridiction compétente, sous le contrôle de la Cour de cassation. Par suite, les moyens tirés de ce que l'arrêté du 18 août 2016, en tant qu'il prévoit l'installation de boxes sécurisés vitrés dans les salles d'audience, méconnaîtrait le principe du respect de la présomption d'innocence garanti par l'article 9 de la Déclaration des droits de l'homme et du citoyen de 1789 et l'article 6 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ainsi que les objectifs de l'article 5 de la directive du 9 mars 2016 doivent être écartés.<br/>
<br/>
              16. En quatrième lieu, l'article 3 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales stipule que : " Nul ne peut être soumis à la torture ni à des peines ou traitements inhumains ou dégradants ". <br/>
<br/>
              17. D'une part, le placement d'un prévenu ou d'un accusé dans un box vitré n'est pas par lui-même de nature, contrairement à ce qui est soutenu, à exposer le prévenu ou l'accusé à un traitement inhumain ou dégradant prohibé par les stipulations de l'article 3 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales. D'autre part, l'article 318 du code de procédure pénale confie au président de la juridiction, sous le contrôle de la Cour de cassation, le soin de déterminer, dans chaque cas, les modalités de comparution du prévenu ou de l'accusé adaptées à sa personne et aux circonstances, le garde des sceaux, ministre de la justice n'étant, contrairement à ce qui est soutenu, pas habilité à déterminer les cas dans lesquels il pourrait être fait usage de ces boxes sécurisés. Par suite, le moyen tiré de la méconnaissance des stipulations de l'article 3 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales doit être écarté.<br/>
<br/>
              18. En cinquième lieu, l'installation de boxes sécurisés vitrés dans les salles d'audience des juridictions judiciaires ne méconnaît pas, par elle-même, le droit à un procès équitable et les droits de la défense garantis par l'article 6 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, dès lors que le prévenu ou l'accusé qui comparaît dans un tel box est en mesure de participer de manière effective aux débats et de communiquer librement et secrètement avec son avocat, ce dont le président de la juridiction, sous le contrôle de la Cour de cassation, doit s'assurer en application des dispositions l'article 309 du code de procédure pénale. Par suite, le moyen tiré de la méconnaissance de l'article 6 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales n'est pas fondé.<br/>
<br/>
              19. En dernier lieu, en conséquence des motifs énoncés aux points précédents, le moyen tiré de ce que l'arrêté critiqué serait entaché d'une erreur manifeste d'appréciation en tant qu'il recommande l'installation de boxes sécurisés vitrés dans les salles d'audience des juridictions judiciaires, ne peut qu'être écarté.<br/>
<br/>
              20. Il résulte de tout ce qui précède, sans qu'il soit besoin de statuer sur la fin de non-recevoir opposée par le garde des sceaux, ministre de la justice, que les conclusions de la requête du Syndicat des avocats de France dirigées contre la décision du garde des sceaux, ministre de la justice refusant d'abroger l'arrêté du 18 août 2016 en tant qu'il recommande l'installation de boxes sécurisés vitrés dans les salles d'audience des juridictions judiciaires doivent être rejetées. <br/>
<br/>
              Sur les conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative : <br/>
<br/>
              21. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise, à ce titre, à la charge de l'État, qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'intervention du Conseil national des barreaux est admise.<br/>
Article 2 : Il n'y a pas lieu de statuer sur les conclusions de la requête du Syndicat des avocats de France dirigées contre la décision du garde des sceaux, ministre de la justice, refusant d'abroger l'arrêté du 18 août 2016 en tant qu'il recommande l'installation de boxes sécurisés à barreaudage dans les salles d'audience des juridictions judiciaires.<br/>
Article 3 : Le surplus des conclusions de la requête du Syndicat des avocats de France est rejeté.<br/>
Article 4 : La présente décision sera notifiée au Syndicat des avocats de France, au Conseil national des barreaux et au garde des sceaux, ministre de la justice.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
