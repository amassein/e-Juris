<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036550304</ID>
<ANCIEN_ID>JG_L_2018_01_000000407220</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/55/03/CETATEXT000036550304.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème - 9ème chambres réunies, 26/01/2018, 407220</TITRE>
<DATE_DEC>2018-01-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>407220</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème - 9ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Stéphane Hoynck</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Aurélie Bretonneau</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:407220.20180126</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              Par une requête enregistrée le 25 janvier 2017 au secrétariat du contentieux du Conseil d'Etat, l'association dissoute " Fraternité musulmane Sanâbil (Les Epis) " demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret du 24 novembre 2016 par lequel le Président de la République a prononcé sa dissolution ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 600 euros au titre de l'article L. 761-1 du code de justice administrative.  <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier.<br/>
<br/>
              Vu :<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - le code de la sécurité intérieure ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Stéphane Hoynck, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Aurélie Bretonneau, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier que, par décret du 24 novembre 2016, le Président de la République a prononcé la dissolution de l'association " Fraternité musulmane Sanâbil  (Les Epis) " dont l'objet déclaré est le soutien aux personnes détenues en situation de précarité et à leurs familles. Le décret est fondé  sur les motifs tirés de ce que l'association, dont le siège est à Noisiel (Seine et Marne), d'une part, a le caractère d'un groupement provoquant à la discrimination, à la haine ou à la violence envers un groupe de personnes en raison de leur non-appartenance à une religion au sens du 6° de l'article L. 212-1 du code de la sécurité intérieure et, d'autre part, pouvait être regardée comme se livrant sur le territoire français à des agissements en vue de provoquer des actes de terrorisme en France ou à l'étranger au sens du 7° de cet article . L'association " Fraternité musulmane Sanâbil  (Les Epis) " demande l'annulation pour excès de pouvoir de ce décret.<br/>
<br/>
              2. Aux termes de l'article L. 212-1 du code de la sécurité intérieure : " Sont dissous, par décret en conseil des ministres, toutes les associations ou groupements de fait : / (...) 6° (...) qui, soit provoquent à la discrimination, à la haine ou à la violence envers une personne ou un groupe de personnes à raison de leur origine ou de leur appartenance ou de leur non-appartenance à une ethnie, une nation, une race ou une religion déterminée, soit propagent des idées ou théories tendant à justifier ou encourager cette discrimination, cette haine ou cette violence ; / 7° Ou qui se livrent, sur le territoire français ou à partir de ce territoire, à des agissements en vue de provoquer des actes de terrorisme en France ou à l'étranger (...) ". <br/>
<br/>
              3. Il ressort des pièces du dossier, notamment des six " notes blanches " précises et circonstanciées, établies entre septembre 2015 et décembre 2016, qui ont été versées au débat contradictoire, en premier lieu, que le président de l'association requérante, M. C..., qui a fait l'objet de mesures d'assignation à résidence, entretient de nombreuses relations avec différentes personnes qui relèvent de la mouvance radicale de l'islam ou qui ont rejoint les rangs de l'organisation dénommée Etat islamique. Le trésorier actuel de l'association, M. A...B..., a pour sa part fait l'objet d'une assignation à résidence et d'une interdiction de sortie du territoire. Il apparaît également que d'anciens dirigeants ou membres de l'association requérante sont impliqués dans la mouvance radicale de l'islam et ont fait l'objet, pour ce motif, d'assignations à résidence et que d'autres ont quitté la France pour rejoindre la zone irako-syrienne. Il en résulte, sans incidence sur ce point étant les inexactitudes figurant dans les notes des services de renseignement relatives aux dates des fonctions exercées par certains des anciens dirigeants et aux dates d'appartenance à l'association de certains des membres qui l'ont ensuite quittée, que l'association " Fraternité musulmane Sanâbil  (Les Epis) " est en lien avec de nombreuses personnes fortement et activement engagées dans la pratique d'un islam radical. <br/>
<br/>
<br/>
              4. Il ressort des mêmes pièces du dossier, en second lieu, que, sous couvert d'une assistance morale, logistique ou de bienfaisance aux personnes détenues de confession musulmane et à leur famille, l'association requérante a développé, au travers de ses activités, en particulier sur des sites internet et par l'organisation de rencontres, notamment de pique-niques, un important réseau relationnel dans le cadre duquel elle manifeste de la sympathie et apporte son soutien à des individus en lien avec la mouvance terroriste se revendiquant de l'islamisme radical. De nombreux détenus qui bénéficient de son assistance sont ainsi poursuivis pour des activités en lien avec le terrorisme. Une perquisition judiciaire menée le 7 décembre 2016 dans le cadre d'une procédure diligentée en flagrant délit du chef d'apologie du terrorisme a en outre conduit à la découverte de documents portant l'en-tête de l'association qui confirment l'existence des liens qu'elle entretient avec les réseaux terroristes.<br/>
<br/>
              5. Il résulte de ce qui précède, tout d'abord, que le décret, qui relève que le président de l'association et certains de ses dirigeants ou anciens dirigeants sont impliqués dans la mouvance islamiste radicale, que plusieurs d'entre eux font l'objet de mesures d'interdiction de sortie du territoire ou d'assignation à résidence dans le cadre de la loi sur l'état d'urgence ou de poursuites pénales pour des faits en lien avec le terrorisme tandis que d'autres ont quitté la France pour la zone irako-syrienne et que l'activité réelle de l'association consiste à soutenir des détenus impliqués dans des activités terroristes ainsi qu'à orienter d'autres détenus vers la cause djihadiste, n'est pas entaché d'erreurs de fait. Il en résulte également, étant sans incidence sur ce point la circonstance que l'association requérante ni aucun de ses membre n'a fait l'objet de poursuites ou de condamnations pénales, que le Président de la République a fait une exacte application des dispositions du 7° de l'article L. 212-1 du code de la sécurité intérieure en prononçant la dissolution contestée au motif que l'association requérante a des agissements en vue de provoquer des actes de terrorisme en France ou à l'étranger <br/>
<br/>
              6. Dès lors qu'il résulte de l'instruction que le Président de la République aurait pris la même décision s'il n'avait retenu que ce premier motif, le moyen tiré de la méconnaissance des dispositions précitées du 7° de l'article L. 212-1 du code de la sécurité intérieure doit être écarté, sans qu'il soit besoin de se prononcer sur la légalité du second motif de la dissolution fondé sur le 6° de cet article.<br/>
<br/>
              7. Par ailleurs, le décret attaqué, qui constitue une restriction à l'exercice de la liberté d'association justifiée par la gravité des dangers pour l'ordre public et la sécurité publique résultant des activités de l'association en cause et proportionnée au but poursuivi, ne méconnaît pas les stipulations de l'article 11 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales.<br/>
<br/>
              8. Il résulte de tout ce qui précède et sans qu'il soit besoin de statuer sur les fins de non-recevoir soulevées par le ministre de l'intérieur, que l'association " Fraternité musulmane Sanâbil (Les Epis" n'est pas fondée à demander l'annulation du décret du 24 novembre 2016. Les dispositions de l'article L761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat, qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>DECIDE :<br/>
              -----------<br/>
<br/>
Article 1er : La requête de l'association " Fraternité musulmane Sanâbil (Les Epis) " est rejetée.<br/>
<br/>
Article 2 : La présente décision sera notifiée à l'association " Fraternité musulmane Sanâbil (Les Epis) ", au Premier ministre et au ministre d'Etat, ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">10-01-04-01 ASSOCIATIONS ET FONDATIONS. QUESTIONS COMMUNES. DISSOLUTION. ASSOCIATIONS ET GROUPEMENTS DE FAIT - LOI DU 10 JANVIER 1936. - DISSOLUTION D'UNE ASSOCIATION SE LIVRANT À DES AGISSEMENTS EN VUE DE PROVOQUER DES ACTES DE TERRORISME EN FRANCE OU À L'ÉTRANGER (7° DE L'ART. L. 212-1 DU CSI) [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">49-05-13 POLICE. POLICES SPÉCIALES. POLICE DES ASSOCIATIONS ET GROUPEMENTS DE FAIT (LOI DU 10 JANVIER 1936) (VOIR : ASSOCIATIONS ET FONDATIONS). - DISSOLUTION D'UNE ASSOCIATION SE LIVRANT À DES AGISSEMENTS EN VUE DE PROVOQUER DES ACTES DE TERRORISME EN FRANCE OU À L'ÉTRANGER (7° DE L'ART. L. 212-1 DU CSI) [RJ1].
</SCT>
<ANA ID="9A"> 10-01-04-01 Recours pour excès de pouvoir dirigé contre un décret de dissolution d'une association en lien avec de nombreuses personnes fortement et activement engagées dans la pratique d'un islam radical qui, sous couvert d'une assistance morale, logistique ou de bienfaisance aux personnes détenues de confession musulmane et à leur famille, a développé, au travers de ses activités, un important réseau relationnel dans le cadre duquel elle manifeste de la sympathie et apporte son soutien à des individus en lien avec la mouvance terroriste se revendiquant de l'islamisme radical.... ,,Le Président de la République a fait une exacte application des dispositions du 7° de l'article L. 212-1 du code de la sécurité intérieure en prononçant la dissolution de cette association en raison de l'action menée par cette dernière en vue de provoquer des actes de terrorisme en France ou à l'étranger, étant sans incidence sur ce point la circonstance que ni celle-ci ni aucun de ses membre n'a fait l'objet de poursuites ou de condamnations pénales.</ANA>
<ANA ID="9B"> 49-05-13 Recours pour excès de pouvoir dirigé contre un décret de dissolution d'une association en lien avec de nombreuses personnes fortement et activement engagées dans la pratique d'un islam radical qui, sous couvert d'une assistance morale, logistique ou de bienfaisance aux personnes détenues de confession musulmane et à leur famille, a développé, au travers de ses activités, un important réseau relationnel dans le cadre duquel elle manifeste de la sympathie et apporte son soutien à des individus en lien avec la mouvance terroriste se revendiquant de l'islamisme radical.... ,,Le Président de la République a fait une exacte application des dispositions du 7° de l'article L. 212-1 du code de la sécurité intérieure en prononçant la dissolution de cette association en raison de l'action menée par cette dernière en vue de provoquer des actes de terrorisme en France ou à l'étranger, étant sans incidence sur ce point la circonstance que ni celle-ci ni aucun de ses membre n'a fait l'objet de poursuites ou de condamnations pénales.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., s'agissant d'une dissolution prononcée sur le fondement du 6° de l'article L. 212-1 du CSI, décision du même jour, Association Rhama de Torcy, n° 412312, à mentionner aux Tables.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
