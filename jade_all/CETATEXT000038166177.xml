<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038166177</ID>
<ANCIEN_ID>JG_L_2019_02_000000416610</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/16/61/CETATEXT000038166177.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème - 5ème chambres réunies, 25/02/2019, 416610</TITRE>
<DATE_DEC>2019-02-25</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>416610</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème - 5ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP NICOLAY, DE LANOUVELLE, HANNOTIN ; BALAT</AVOCATS>
<RAPPORTEUR>Mme Laure Durand-Viel</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Julie Burguburu</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:416610.20190225</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B...et Mme C...E...ont demandé au tribunal administratif d'Amiens d'annuler pour excès de pouvoir l'arrêté du 23 mars 2015 par lequel le maire de Saint Crépin-aux-Bois (Oise) a délivré à M. D...un permis de construire pour la construction d'une maison individuelle. Par une ordonnance n° 1602045 du 8 août 2016, le président de la 4ème chambre du tribunal administratif d'Amiens a rejeté leur demande. <br/>
<br/>
              Par un arrêt n° 16DA01744 du 12 octobre 2017, la cour administrative d'appel de Douai a rejeté l'appel formé par M. et Mme E...contre ce jugement.  <br/>
<br/>
              Par un pourvoi, enregistré le 18 décembre 2017 au secrétariat du contentieux du Conseil d'Etat, M. B...et Mme C...E...demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à leur demande ;<br/>
<br/>
              3°) de mettre à la charge de la commune de Saint-Crépin-aux-Bois la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'urbanisme ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Laure Durand-Viel, auditeur,  <br/>
<br/>
              - les conclusions de Mme Julie Burguburu, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Nicolaÿ, de Lanouvelle, Hannotin, avocat de M. et Mme E...et à Me Balat, avocat de la commune de Saint-Crépin-aux-Bois.<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article R. 600-2 du code de l'urbanisme : " Le délai de recours contentieux à l'encontre (...) d'un permis de construire (...) court à l'égard des tiers à compter du premier jour d'une période continue de deux mois d'affichage sur le terrain des pièces mentionnées à l'article R. 424-15. " Aux termes de l'article R. 424-15 du même code : " Mention du permis explicite ou tacite (...) doit être affichée sur le terrain, de manière visible de l'extérieur, par les soins de son bénéficiaire, dès la notification de l'arrêté ou dès la date à laquelle le permis tacite (...) est acquis et pendant toute la durée du chantier. / (...) " Aux termes de l'article A. 424-16 de ce même code : " Le panneau prévu à l'article A. 424-1 indique le nom, la raison sociale ou la dénomination sociale du bénéficiaire, la date et le numéro du permis, la nature du projet et la superficie du terrain ainsi que l'adresse de la mairie où le dossier peut être consulté. / Il indique également, en fonction de la nature du projet : a) Si le projet prévoit des constructions, la surface de plancher autorisée ainsi que la hauteur de la ou des constructions, exprimée en mètres par rapport au sol naturel ; / (...) ".<br/>
<br/>
              2. En imposant que figurent sur le panneau d'affichage du permis de construire diverses informations sur les caractéristiques de la construction projetée, dont la hauteur du bâtiment par rapport au sol naturel, les dispositions rappelées au point précédent ont eu pour objet de permettre aux tiers, à la seule lecture de ce panneau, d'apprécier l'importance et la consistance du projet, le délai de recours ne commençant à courir qu'à la date d'un affichage complet et régulier. L'affichage ne peut être regardé comme complet et régulier si la mention de la hauteur fait défaut ou si elle est affectée d'une erreur substantielle, alors qu'aucune autre indication ne permet aux tiers d'estimer cette hauteur. Pour apprécier si la mention de la hauteur de la construction figurant sur le panneau d'affichage est affectée d'une erreur substantielle, il convient de se référer à la hauteur maximale de la construction par rapport au sol naturel telle qu'elle ressort de la demande de permis de construire.<br/>
<br/>
              3. Il ressort de l'arrêt attaqué que, pour apprécier si la mention de la hauteur de la construction en cause figurant sur le panneau d'affichage était affectée d'une erreur substantielle, la cour a décidé de ne pas tenir compte de la hauteur maximale de la construction par rapport au sol naturel telle qu'elle ressortait des plans du projet au motif que, eu égard à la déclivité du terrain, la prise en compte de cette hauteur supposait de qualifier la partie basse de la construction au regard des règles de hauteur fixées par le règlement du plan local d'urbanisme et donc de se prononcer sur la légalité de la construction projetée. En statuant ainsi, elle a commis une erreur de droit. <br/>
<br/>
              4. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à la charge de M. et MmeE..., qui ne sont pas, dans la présente instance, la partie perdante. Il y a lieu, en revanche, dans les circonstances de l'espèce, de mettre à la charge de la commune de Saint-Crépin-aux-Bois une somme de 3 000 euros à verser à M. et Mme E...au titre de ces mêmes dispositions.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Douai du 12 octobre 2017 est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Douai.<br/>
Article 3 : La commune de Saint-Crépin-aux-Bois versera à M. et Mme E...la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Les conclusions présentées par la commune de Saint Crépin-aux-Bois au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée à M. B...et Mme C...E...et à la commune de Saint-Crépin-aux-Bois.<br/>
      Copie en sera adressée à M. A...D....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">68-06-01-03-01 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. RÈGLES DE PROCÉDURE CONTENTIEUSE SPÉCIALES. INTRODUCTION DE L'INSTANCE. DÉLAIS DE RECOURS. POINT DE DÉPART DU DÉLAI. - PERMIS DE CONSTRUIRE - AFFICHAGE SUR LE TERRAIN - MENTIONS CONTENUES DANS L'AFFICHAGE - MENTIONS SUBSTANTIELLES - HAUTEUR DU BÂTIMENT [RJ1] - NOTION DE HAUTEUR DU BÂTIMENT - HAUTEUR MAXIMALE DE LA CONSTRUCTION PAR RAPPORT AU SOL RESSORTANT DU DOSSIER DE PERMIS DE CONSTRUIRE.
</SCT>
<ANA ID="9A"> 68-06-01-03-01 En imposant que figurent sur le panneau d'affichage du permis de construire diverses informations sur les caractéristiques de la construction projetée, dont la hauteur du bâtiment par rapport au sol naturel, les articles R. 600-2, R. 424-15 et A. 424-16 du code de l'urbanisme ont eu pour objet de permettre aux tiers, à la seule lecture de ce panneau, d'apprécier l'importance et la consistance du projet, le délai de recours ne commençant à courir qu'à la date d'un affichage complet et régulier. L'affichage ne peut être regardé comme complet et régulier si la mention de la hauteur fait défaut ou si elle est affectée d'une erreur substantielle, alors qu'aucune autre indication ne permet aux tiers d'estimer cette hauteur.... ...Pour apprécier si la mention de la hauteur de la construction figurant sur le panneau d'affichage est affectée d'une erreur substantielle, il convient de se référer à la hauteur maximale de la construction par rapport au sol naturel telle qu'elle ressort de la demande de permis de construire.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 16 février 1994, Société Northern Telecom Immobilier, n° 138207, p. 73 ; CE, 6 juillet 2012,,, n° 339883, T. 1028.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
