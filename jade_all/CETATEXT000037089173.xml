<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037089173</ID>
<ANCIEN_ID>JG_L_2018_06_000000408507</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/08/91/CETATEXT000037089173.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  7ème chambre jugeant seule, 20/06/2018, 408507, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-06-20</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>408507</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 7ème chambre jugeant seule</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GATINEAU, FATTACCINI ; SCP GARREAU, BAUER-VIOLAS, FESCHOTTE-DESBOIS</AVOCATS>
<RAPPORTEUR>M. Thomas Pez-Lavergne</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Olivier Henrard</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:408507.20180620</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Sous le n° 408507, par une décision du 6 décembre 2017, le Conseil d'Etat, statuant au contentieux, a, d'une part, prononcé l'admission des conclusions du pourvoi de la société La Communication Hospitalière (LCH) dirigées contre l'arrêt n° 14DA00211 du 30 décembre 2016 de la cour administrative d'appel de Douai, en tant seulement que cet arrêt a statué sur la demande d'indemnisation de cette société au titre du retour anticipé des biens visés par l'article 2.2 de l'avenant n° 1 signé le 28 octobre 2005 avec le centre hospitalier de Valenciennes et, d'autre part, prononcé la non-admission du surplus des conclusions du pourvoi.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code des marchés publics ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Thomas Pez-Lavergne, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Olivier Henrard, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Gatineau, Fattaccini, avocat de la société La Communication Hospitalière et à la SCP Garreau, Bauer-Violas, Feschotte-Desbois, avocat du centre hospitalier de Valenciennes.<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des énonciations de l'arrêt attaqué que le centre hospitalier de Valenciennes (Nord) a conclu, le 28 octobre 2004, un contrat avec la société La Communication Hospitalière (LCH), spécialisée dans la mise à disposition des usagers du service public hospitalier de moyens audiovisuels et de télécommunication ; que, toutefois, des dysfonctionnements sont apparus au cours de l'exécution du contrat ; qu'estimant que la société LCH avait manqué à ses obligations, le directeur du centre hospitalier de Valenciennes a décidé, le 12 avril 2010, de le résilier ; que, par un jugement du 31 décembre 2013, le tribunal administratif de Lille a rejeté la demande de la société LCH tendant à la condamnation du centre hospitalier de Valenciennes à lui verser une somme de 1 746 817 euros hors taxes, assortie des intérêts au taux légal à compter du 25 mai 2010, à titre de réparation des préjudices qu'elle estimait avoir subis en conséquence de la résiliation de son contrat ; que, par un arrêt du 30 décembre 2016, la cour administrative d'appel de Douai a, d'une part, annulé le jugement du 31 décembre 2013 du tribunal administratif de Lille en tant qu'il rejette les conclusions de la société LCH tendant à être indemnisée de la valeur des biens de retour non amortis et, d'autre part, condamné le centre hospitalier de Valenciennes à lui verser à ce titre une somme de 154 206,10 euros, portant intérêts au taux légal à compter du 25 mai 2010 ; que la société LCH se pourvoit en cassation contre cet arrêt en tant qu'il n'a fait droit que partiellement à sa requête d'appel ; <br/>
<br/>
              2. Considérant que, dans le cadre d'une délégation de service public ou d'une concession de travaux mettant à la charge du cocontractant les investissements correspondant à la création ou à l'acquisition des biens nécessaires au fonctionnement du service public, l'ensemble de ces biens, meubles ou immeubles appartient, dans le silence de la convention, dès leur réalisation ou leur acquisition, à la personne publique ; qu'à l'expiration de la convention, les biens qui sont entrés dans la propriété de la personne publique et ont été amortis au cours de l'exécution du contrat font nécessairement retour à celle-ci gratuitement, sous réserve des clauses contractuelles permettant à la personne publique, dans les conditions qu'elles déterminent, de faire reprendre par son cocontractant les biens qui ne seraient plus nécessaires au fonctionnement du service public ; que lorsque la personne publique résilie la convention avant son terme normal, le délégataire est fondé à demander l'indemnisation du préjudice qu'il subit à raison du retour anticipé des biens à titre gratuit dans le patrimoine de la collectivité publique, dès lors qu'ils n'ont pu être totalement amortis ; que lorsque l'amortissement de ces biens a été calculé sur la base d'une durée d'utilisation inférieure à la durée du contrat, cette indemnité est égale à leur valeur nette comptable inscrite au bilan ; que, dans le cas où leur durée d'utilisation était supérieure à la durée du contrat, l'indemnité est égale à la valeur nette comptable qui résulterait de l'amortissement de ces biens sur la durée du contrat ; que si, en présence d'une convention conclue entre une personne publique et une personne privée, il est loisible aux parties de déroger à ces principes, l'indemnité mise à la charge de la personne publique au titre de ces biens ne saurait en toute hypothèse excéder le montant calculé selon les modalités précisées ci-dessus ; <br/>
<br/>
              3. Considérant qu'il ressort des énonciations de l'arrêt attaqué que, pour écarter la demande d'indemnisation présentée par la société LCH au titre du retour anticipé des meubles multimédia dans le patrimoine de la collectivité publique, la cour administrative d'appel de Douai a interprété les stipulations de l'article 2.2 de l'avenant n° 1 conclu le 28 octobre 2005 entre la société LCH et le centre hospitalier de Valenciennes, aux termes desquelles " Le mobilier multimédias installé sera maintenu par le délégataire pendant toute la durée de la convention de délégation. A son issue, il sera intégré au patrimoine de la collectivité sans contrepartie financière, dans le cadre des biens de retour ", comme impliquant que ces meubles ne donneraient pas lieu à indemnité au titre des biens de retour, y compris dans l'hypothèse d'une résiliation anticipée ; qu'il est toutefois constant que les stipulations de l'article 2.2 de l'avenant n° 1 ne visent à prévoir une intégration, sans contrepartie financière, de ces biens dans le patrimoine de la collectivité publique qu'à l'issue du contrat, au terme de la durée initialement prévue, et non dans le cas d'une résiliation anticipée de celui-ci ; qu'ainsi, en estimant que ces stipulations faisaient obstacle à ce qu'une indemnité soit due à ce titre à la société LCH, y compris dans l'hypothèse où, du fait de la résiliation anticipée du contrat, les biens n'auraient pas été entièrement amortis, la cour en a dénaturé les termes ; que, par suite, son arrêt doit être annulé en tant qu'il statue sur la demande d'indemnisation de la société LCH au titre du retour anticipé des biens visés par l'article 2.2 de l'avenant n° 1 conclu avec le centre hospitalier de Valenciennes ;<br/>
<br/>
              4. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de la société LCH qui n'est pas, dans la présente instance, la partie perdante ; qu'il y a lieu, en revanche, dans les circonstances de l'espèce, de mettre à la charge du centre hospitalier de Valenciennes la somme de 3 500 euros à verser à la société LCH, au titre de ces dispositions ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 30 décembre 2016 de la cour administrative d'appel de Douai est annulé en tant qu'il statue sur la demande d'indemnisation de la société LCH au titre du retour anticipé des biens visés par l'article 2.2 de l'avenant n° 1 signé le 28 octobre 2005 avec le centre hospitalier de Valenciennes.<br/>
Article 2 : L'affaire est renvoyée, dans cette mesure, à la cour administrative d'appel de Douai.<br/>
Article 3 : Le centre hospitalier de Valenciennes versera à la société LCH une somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Les conclusions du centre hospitalier de Valenciennes présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée à la société La Communication Hospitalière (LCH) et au centre hospitalier de Valenciennes.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
