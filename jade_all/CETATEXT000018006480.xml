<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000018006480</ID>
<ANCIEN_ID/>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/18/00/64/CETATEXT000018006480.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème et 8ème sous-sections réunies, 04/06/2007, 284380</TITRE>
<DATE_DEC>2007-06-04</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>284380</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème et 8ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Martin Laprade</PRESIDENT>
<AVOCATS>SCP LYON-CAEN, FABIANI, THIRIEZ ; SCP PEIGNOT, GARREAU</AVOCATS>
<RAPPORTEUR>M. François  Delion</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Glaser</COMMISSAIRE_GVT>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête sommaire et le mémoire complémentaire, enregistrés les 23 août et 22 décembre 2005 au secrétariat du contentieux du Conseil d'Etat, présentés pour la COMMUNE DE CARRIERES-SUR-SEINE (Yvelines), représentée par son maire ; la COMMUNE DE CARRIERES-SUR-SEINE demande au Conseil d'Etat :
              
              1°) d'annuler le jugement du 20 juin 2005 du tribunal administratif de Versailles en tant qu'il a annulé la décision du maire de la commune supprimant le versement à Mme Sandrine A de la nouvelle bonification indiciaire au titre de la période du 1er janvier au 31 août 2004 ;
              
              2°) de rejeter la demande de Mme A tendant à l'annulation de cette décision ;
              
              3°) de mettre à la charge de Mme A le versement de la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative ;
              
              
     
	
              Vu les autres pièces du dossier ;
              
              Vu la loi n° 84-53 du 26 janvier 1984 ; 
              
              Vu la loi n° 91-73 du 18 janvier 1991 ;
              
              Vu le décret n° 91-711 du 24 juillet 1991 modifié ; 
              
              Vu le code de justice administrative ;
              
              
     
              Après avoir entendu en séance publique :
              
              - le rapport de M. François Delion, Maître des Requêtes,
              
              - les observations de la SCP Lyon-Caen, Fabiani, Thiriez, avocat de la COMMUNE DE CARRIERES-SUR-SEINE et de la SCP Peignot, Garreau, avocat de Mme A,
              
              - les conclusions de M. Emmanuel Glaser, Commissaire du gouvernement ;
     
     <br/>
              Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, par décision du 1er juillet 1999, le maire de CARRIERES-SUR-SEINE a attribué à Mme A, adjoint administratif, une bonification indiciaire à raison de ses fonctions d'accueil du public au service de l'administration générale de la commune ; que, à la suite de la mutation de l'intéressée au service scolaire enfance jeunesse, la commune a cessé de lui verser cette bonification du 1er janvier au 31 août 2004 ; que la COMMUNE DE CARRIERES-SUR-SEINE se pourvoit en cassation contre le jugement du 20 juin 2005 du tribunal administratif de Versailles en tant qu'il a annulé cette décision ;
              
              Considérant qu'aux termes du I de l'article 27 de la loi du 18 janvier 1991 : La nouvelle bonification indiciaire des fonctionnaires et des militaires instituée à compter du 1er août 1990 est attribuée pour certains emplois comportant une responsabilité ou une technicité particulières dans des conditions fixées par décret ; qu'en vertu de l'article 1er du décret du 24 juillet 1991 pris pour l'application de cette loi, dans sa rédaction issue du décret du 24 juillet 1997, la nouvelle bonification indiciaire est versée mensuellement aux (&#133;) 18° Adjoints administratifs et agents administratifs exerçant à titre principal des fonctions d'accueil du public dans les communes de plus de 5 000 habitants ou les établissements publics communaux et intercommunaux en relevant : 10 points ;
              
              Considérant qu'il résulte de ces dispositions que le bénéfice de la nouvelle bonification indiciaire est lié non au corps ou cadre d'emplois d'appartenance ou au grade des fonctionnaires, ou encore à leur lieu d'affectation, mais aux seules caractéristiques des emplois occupés, au regard des responsabilités qu'ils impliquent ou de la technicité qu'ils requièrent ; qu'ainsi, les dispositions précitées du décret du 24 juillet 1991 qui ouvrent droit au bénéfice de la nouvelle bonification indiciaire à raison de l'exercice à titre principal de fonctions d'accueil du public doivent être interprétées comme réservant ce droit aux agents dont l'emploi implique qu'ils consacrent plus de la moitié de leur temps de travail total à des fonctions d'accueil du public ; que, pour l'application de cette règle, il convient de prendre en compte les heures d'ouverture au public du service, si l'agent y est affecté dans des fonctions d'accueil du public, ainsi que, le cas échéant, le temps passé par l'agent au contact du public en dehors de ces périodes, notamment à l'occasion de rendez-vous avec les administrés ;
              
              Considérant que, pour juger que Mme A exerçait à titre principal des fonctions d'accueil du public, le tribunal administratif de Versailles s'est fondé sur ce qu'il ressortait des pièces du dossier qui lui était soumis que, si le service scolaire n'était ouvert au public que treize heures par semaine, l'intéressée, qui bénéficiait par ailleurs d'une délégation aux fins d'établir des procurations, recevait également du public, sur rendez-vous, en dehors des heures normales d'ouverture du service ; qu'en statuant ainsi sans rechercher si, comme l'y invitait expressément la commune, le temps effectivement passé par Mme A au contact du public en dehors des heures d'ouverture du service était suffisant pour faire regarder l'intéressée comme exerçant des fonctions d'accueil du public durant la majeure partie de son temps de travail, le tribunal administratif a commis une erreur de droit ;
              
              Considérant qu'il résulte de ce qui précède que la COMMUNE DE CARRIERES-SUR-SEINE est fondée à demander, pour ce motif, l'annulation du jugement attaqué en tant qu'il a statué sur les conclusions de Mme A tendant à l'annulation de la décision du maire de CARRIERES-SUR-SEINE supprimant le versement à Mme A de la nouvelle bonification indiciaire au titre de la période du 1er janvier au 31 août 2004 ;
              
              Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mise à la charge de la COMMUNE DE CARRIERES-SUR-SEINE, qui n'est pas dans la présente instance la partie perdante, la somme que demande Mme A au titre des frais exposés par elle et non compris dans les dépens ; que, dans les circonstances de l'espèce, il n'y a pas lieu de mettre à la charge de Mme A la somme que la commune demande au même titre ;
              
     
     <br/>
D E C I D E :
--------------
Article 1er : Le jugement du 20 juin 2005 du tribunal administratif de Versailles est annulé en tant qu'il a statué sur les conclusions de Mme A tendant à l'annulation de la décision du maire de CARRIERES-SUR-SEINE supprimant le versement à Mme A de la nouvelle bonification indiciaire au titre de la période du 1er janvier au 31 août 2004.
Article 2 : L'affaire est renvoyée au tribunal administratif de Versailles.
Article 3 : Le surplus des conclusions de la requête de la COMMUNE DE CARRIERES-SUR-SEINE et les conclusions présentées par Mme A au titre de l'article L. 761-1 du code de justice administrative sont rejetés.
Article 4 : La présente décision sera notifiée à la COMMUNE DE CARRIERES-SUR-SEINE, à Mme Sandrine A et au ministre de l'intérieur, de l'outre-mer et des collectivités territoriales.

<br/>
</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
