<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030556730</ID>
<ANCIEN_ID>JG_L_2015_05_000000382485</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/55/67/CETATEXT000030556730.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème sous-section jugeant seule, 06/05/2015, 382485, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-05-06</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>382485</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP WAQUET, FARGE, HAZAN</AVOCATS>
<RAPPORTEUR>M. Thomas Campeaux</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Aurélie Bretonneau</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2015:382485.20150506</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>	Vu la procédure suivante :<br/>
<br/>
Procédure contentieuse antérieure<br/>
	Mme G...D...a saisi le tribunal administratif de Besançon d'une protestation tendant à l'annulation des opérations électorales qui se sont déroulées les 23 et 30 mars 2014 en vue de la désignation des conseillers municipaux de la commune de Saint-Loup-sur-Semouse (Haute-Saône). Par un jugement n° 1400490 du 10 juin 2014, le tribunal administratif de Besançon a rejeté sa protestation.<br/>
Procédure devant le Conseil d'Etat<br/>
	Par une requête et un mémoire en réplique, enregistrés les 10 juillet et 22 octobre 2014 au secrétariat du contentieux du Conseil d'Etat, Mme G...D...demande au Conseil d'Etat :<br/>
	1°) d'annuler ce jugement du 10 juin 2014 du tribunal administratif de Besançon ;<br/>
	2°) d'annuler les opérations électorales contestées ; <br/>
	3°) de mettre à la charge de M. H...F...la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Vu :<br/>
<br/>
              - les autres pièces du dossier ;<br/>
              - le code électoral ;<br/>
- le code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Thomas Campeaux, maître des requêtes, <br/>
<br/>
              - les conclusions de Mme Aurélie Bretonneau, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Waquet, Farge, Hazan, avocat de Mme D...;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. A l'issue du second tour des élections municipales, qui se sont déroulées les 23 et 30 mars 2014 dans la commune de Saint-Loup-sur-Semouse (Haute-Saône), la liste " Pour Saint-Loup, continuons ensemble " conduite par M. H...F..., maire sortant, et la liste " Un autre Saint-Loup " conduite par Mme G...D...ont obtenu le même nombre de voix. En vertu des dispositions de l'article L. 262 du code électoral, la liste de M. F...a remporté l'élection au bénéfice de la moyenne d'âge la plus élevée. <br/>
<br/>
              Sur la recevabilité de la protestation portant sur le premier tour des élections : <br/>
<br/>
              2. Aux termes de l'article R. 119 du code électoral : " Les réclamations contre les opérations électorales doivent être consignées au procès-verbal, sinon être déposées, à peine d'irrecevabilité, au plus tard à dix-huit heures le cinquième jour qui suit l'élection, à la sous-préfecture ou à la préfecture. (...) ". En conséquence, le délai ouvert pour contester les opérations du premier tour de scrutin expirait le 28 mars 2014. La protestation de Mme D...dirigée contre les opérations électorales de ce premier tour qui se sont déroulées le 23 mars 2014, formulée dans un mémoire en réplique enregistré au greffe du tribunal administratif de Besançon le 30 avril 2014, a donc été présentée après l'expiration de ce délai. Il suit de là que la requérante n'est pas fondée à soutenir que c'est à tort que le tribunal administratif de Besançon a rejeté cette protestation comme tardive.<br/>
<br/>
              Sur le grief tiré de l'inéligibilité de deux candidats de la liste " Pour Saint-Loup, continuons ensemble " :<br/>
<br/>
              3. Aux termes du deuxième alinéa de l'article L. 228 du code électoral : " Sont éligibles au conseil municipal tous les électeurs de la commune et les citoyens inscrits au rôle des contributions directes ou justifiant qu'ils devaient y être inscrits au 1er janvier de l'année de l'élection ". En l'espèce, il résulte de l'instruction que Mme C...F...et M. A...B...sont inscrits sur les listes électorales de la commune de Saint-Loup-sur-Semouse. Il n'appartient pas au juge de l'élection de statuer sur la régularité ou le bien fondé d'une inscription ou d'une radiation sur les listes électorales au regard de la condition de domicile exigée par l'article L. 11 du code électoral, mais uniquement d'apprécier tous les faits résultant de manoeuvre ou d'irrégularités susceptibles d'avoir affecté la sincérité du scrutin. Ainsi, en l'absence de manoeuvre alléguée, le grief tiré de ce que Mme F...et M. B...ne remplissaient pas les conditions requises pour être inscrits sur la liste électorale ne peut qu'être écarté.<br/>
<br/>
              Sur les griefs relatifs au déroulement de la campagne :<br/>
<br/>
              4. La lettre par laquelle le maire remercie ses administrés de leur soutien à la suite des dégradations commises sur son véhicule personnel ne contient pas, contrairement à ce qui est soutenu, d'accusation publique contre Mme D...et ses colistiers, qui avaient la possibilité d'y répondre. Son contenu n'excède en rien les limites de la polémique électorale. <br/>
<br/>
              5. Aux termes du second alinéa de l'article L. 52-1 du code électoral : " A compter du premier jour du sixième mois précédant le mois au cours duquel il doit être procédé à des élections générales, aucune campagne de promotion publicitaire des réalisations ou de la gestion d'une collectivité ne peut être organisée sur le territoire des collectivités intéressées par le scrutin. Sans préjudice des dispositions du présent chapitre, cette interdiction ne s'applique pas à la présentation, par un candidat ou pour son compte, dans le cadre de l'organisation de sa campagne, du bilan de la gestion des mandats qu'il détient ou qu'il a détenus. (...) ". En l'espèce, dans un document intitulé " Infos pratiques " diffusé au cours du mois de mars 2014, M. F...a annoncé la création de plusieurs services, notamment un service gratuit de navettes, à compter du 1er juin 2014. Eu égard à son contenu, qui se limite à une énumération des nouveaux services mis en place par la municipalité et qui est dépourvu de toute polémique électorale, ce document ne peut être regardé comme constitutif d'une campagne de promotion publicitaire au sens des dispositions précitées. <br/>
<br/>
              6. Il résulte de ce qui précède que les griefs tirés de ce que la distribution de ces documents aurait altéré la sincérité du scrutin doivent être écartés.<br/>
<br/>
              Sur la régularité des opérations électorales : <br/>
<br/>
              7. En premier lieu, il résulte de l'instruction, notamment des deux procès-verbaux du recensement des votes établis au bureau centralisateur, que le grief tiré de l'augmentation du nombre d'électeurs inscrits entre les deux tours manque en fait. <br/>
<br/>
              8. En deuxième lieu, le procès-verbal relatif au second tour des opérations électorales ne comporte aucune observation sur le déroulement du scrutin ou des opérations de dépouillement. En particulier, aucune mention relative au décompte des voix dans le bureau de vote n° 2 n'y a été consignée. En outre, les deux feuilles de dépouillement en cause ont été signées par les scrutateurs parmi lesquels figurait M.E..., colistier de MmeD..., et comportaient le même nombre de voix pour chaque liste. Il suit de là que, même s'il est constant qu'une des feuilles de dépouillement comporte une rature sur la dernière voix attribuée à Mme D..., cette rature est sans conséquence sur le décompte des voix. <br/>
<br/>
              9. En troisième lieu, si, pour le bureau de vote n° 2, le nombre des émargements était supérieur d'une unité au nombre des bulletins trouvés dans l'urne, il n'est pas allégué que cette différence résulterait d'une manoeuvre. En conséquence, il n'y a pas lieu de modifier le décompte des voix.<br/>
<br/>
              10. En quatrième lieu, la circonstance qu'un tiers aurait signé à la place de l'une des électrices sur la liste d'émargement n'a pas privé cette dernière de la possibilité de voter. Cette erreur est donc dépourvue d'incidence sur les résultats du scrutin. <br/>
<br/>
              11. Il résulte de tout ce qui précède, sans qu'il soit besoin de statuer sur la recevabilité de certains griefs relatifs à la régularité des opérations électorales, que Mme D...n'est pas fondée à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Besançon a rejeté sa protestation. <br/>
<br/>
              Sur les conclusions relatives à l'application de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              12. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de M.F..., qui n'est pas, dans la présente instance, la partie perdante. En outre, il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par M. F...au titre de ces mêmes dispositions. <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de Mme D...est rejetée. <br/>
Article 2 : Les conclusions présentées par M. F...au titre de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
Article 3 : La présente décision sera notifiée à Mme G...D..., à M. H...F...et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
