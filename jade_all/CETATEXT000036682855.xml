<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036682855</ID>
<ANCIEN_ID>JG_L_2018_03_000000411639</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/68/28/CETATEXT000036682855.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère chambre, 07/03/2018, 411639, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-03-07</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>411639</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP NICOLAY, DE LANOUVELLE, HANNOTIN ; SCP DELAMARRE, JEHANNIN</AVOCATS>
<RAPPORTEUR>M. Frédéric Pacoud</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Charles Touboul</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:411639.20180307</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme B...A...a demandé au tribunal administratif de Lyon, à titre principal, d'annuler la décision implicite de rejet née du silence gardé par le président du conseil général de l'Ain sur son recours gracieux formé contre la décision du 16 décembre 2013 de récupération d'un indu de revenu de solidarité active d'un montant de 10 955,31 euros et, à titre subsidiaire, d'annuler la décision implicite par laquelle le président du conseil départemental a rejeté sa demande de remise gracieuse de sa dette. Par un jugement n° 1406987 du 20 décembre 2016, le tribunal administratif de Lyon a rejeté sa demande.<br/>
<br/>
              Par un pourvoi, enregistré le 20 juin 2017 au secrétariat du contentieux du Conseil d'Etat, Mme A...demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande ;<br/>
<br/>
              3°) de mettre à la charge du département de l'Ain et de la caisse d'allocations familiales de ce département le versement à Me Delamarre, son avocat, de la somme de 3 000 euros au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'action sociale et des familles ;<br/>
              - la loi n° 79-587 du 11 juillet 1979 ;<br/>
              - la loi  n° 91-647 du 10 juillet 1991 ;<br/>
              - le décret n° 91-1266 du 19 décembre 1991 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Frédéric Pacoud, maître des requêtes,<br/>
<br/>
              - les conclusions de M. Charles Touboul, rapporteur public .<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Delamarre, Jéhannin, avocat de MmeA..., et à la SCP Nicolaÿ, de Lanouvelle, Hannotin, avocat du département de l'Ain ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis au juge du fond que la caisse d'allocations familiales de l'Ain, estimant à la suite d'un contrôle que Mme B...A..., contrairement à ses déclarations, n'était pas séparée de son conjoint, a pris, le 16 décembre 2013, la décision de récupérer un indu de revenu de solidarité active d'un montant de 10 955,31 euros pour la période du 1er décembre 2011 au 30 octobre 2013. Mme A...a saisi le président du conseil général de l'Ain d'un recours préalable obligatoire contre cette décision. Elle se pourvoit en cassation contre le jugement du 20 décembre 2016 par lequel le tribunal administratif de Lyon a rejeté sa demande tendant à l'annulation de la décision implicite de rejet de ce recours préalable.<br/>
<br/>
              2. D'une part, en vertu de la première phrase de l'article L. 262-47 du code de l'action sociale et des familles, dans sa rédaction applicable au litige, toute réclamation dirigée contre une décision relative au revenu de solidarité active fait l'objet, préalablement à l'exercice d'un recours contentieux, d'un recours administratif auprès du président du conseil général. Aux termes de l'article 1er de la loi du 11 juillet 1979 relative à la motivation des actes administratifs et à l'amélioration des relations entre l'administration et le public alors en vigueur, aujourd'hui codifié à l'article L. 211-2 du code des relations entre le public et l'administration : " (...) doivent être motivées les décisions qui : / (...) - rejettent un recours administratif dont la présentation est obligatoire préalablement à tout recours contentieux en application d'une disposition législative ou réglementaire ". L'article 5 de la même loi, aujourd'hui codifié à l'article L. 232-4 du code des relations entre le public et l'administration, dispose que : " Une décision implicite intervenue dans les cas où la décision explicite aurait dû être motivée n'est pas illégale du seul fait qu'elle n'est pas assortie de cette motivation. Toutefois, à la demande de l'intéressé, formulée dans les délais du recours contentieux, les motifs de toute décision implicite de rejet devront lui être communiqués dans le mois suivant cette demande. Dans ce cas, le délai du recours contentieux contre ladite décision est prorogé jusqu'à l'expiration de deux mois suivant le jour où les motifs lui auront été communiqués ". <br/>
<br/>
              3. D'autre part, aux termes de l'article 38 du décret du 19 décembre 1991 portant application de la loi du 10 juillet 1991 relative à l'aide juridique, dans sa rédaction applicable au litige : " Lorsqu'une action en justice doit être intentée avant l'expiration d'un délai devant la juridiction du premier degré, (...) l'action est réputée avoir été intentée dans le délai si la demande d'aide juridictionnelle s'y rapportant est adressée au bureau d'aide juridictionnelle avant l'expiration dudit délai et si la demande en justice est introduite dans un nouveau délai de même durée à compter : / (...) c) De la date à laquelle la décision d'admission ou de rejet de la demande est devenue définitive (...) ". <br/>
<br/>
              4. Il résulte des dispositions de l'article 5 de la loi du 11 juillet 1979 que lorsqu'un recours préalable obligatoire fait l'objet d'une décision implicite de rejet, cette décision se trouve entachée d'illégalité si son auteur n'en communique pas les motifs à l'intéressé dans le délai d'un mois qui suit la demande formée par ce dernier à cette fin dans le délai de recours contentieux. Lorsque l'intéressé présente dans le même délai de recours une demande d'aide juridictionnelle au titre du recours contentieux qu'il a formé ou envisage de former devant une juridiction administrative pour contester la décision implicite de rejet de son recours préalable obligatoire, le délai pendant lequel il peut demander la communication des motifs de cette décision est interrompu. Il recommence à courir selon les modalités prévues à l'article 38 du décret du 19 décembre 1991.<br/>
<br/>
              5. Le tribunal administratif de Lyon a relevé que Mme A...avait demandé le 22 juillet 2014 la communication des motifs de la décision implicite de rejet née le 31 mars 2014 du silence gardé par le président du conseil général sur son recours administratif préalable obligatoire, ce dont il a déduit que cette demande avait été formée au-delà du " délai d'un mois " prévu par l'article 5 de la loi du 11 juillet 1979 et que la requérante ne pouvait, dès lors, se prévaloir du défaut de motivation de la décision attaquée. En statuant ainsi, alors que l'article 5 de cette loi prévoit qu'une telle demande peut être formée dans le délai du recours contentieux, lequel, au surplus, avait été en l'espèce interrompu par la demande d'aide juridictionnelle présentée par Mme A...le 28 mai 2014 au bureau d'aide juridictionnelle près le tribunal de grande instance de Lyon et rejetée par une décision de ce bureau le 23 juin 2014, le tribunal a commis une erreur de droit. Il en résulte que Mme A...est fondée à demander pour ce motif l'annulation du jugement du tribunal administratif de Lyon du 20 décembre 2016.<br/>
<br/>
              6. Mme A...ayant obtenu le bénéfice de l'aide juridictionnelle, son avocat peut se prévaloir des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991. Il y a lieu, dans les circonstances de l'espèce, et sous réserve que Me Delamarre, avocat de MmeA..., renonce à percevoir la somme correspondant à la part contributive de l'Etat, de mettre à la charge du département de l'Ain une somme de 2 000 euros à verser à Me Delamarre.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du tribunal administratif de Lyon du 20 décembre 2016 est annulé.<br/>
Article 2 : L'affaire est renvoyée au tribunal administratif de Lyon.<br/>
Article 3 : Le département de l'Ain versera à Me Delamarre, avocat de MmeA..., une somme de 2 000 euros au titre des dispositions du deuxième alinéa de l'article 37 de la loi du 10 juillet 1991, sous réserve que Me Delamarre renonce à percevoir la somme correspondant à la part contributive de l'Etat.<br/>
Article 4 : La présente décision sera notifiée à Mme B...A...et au département de l'Ain. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
