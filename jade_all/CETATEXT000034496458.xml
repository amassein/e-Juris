<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034496458</ID>
<ANCIEN_ID>JG_L_2017_04_000000403717</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/49/64/CETATEXT000034496458.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère chambre, 26/04/2017, 403717, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-04-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>403717</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROCHETEAU, UZAN-SARANO</AVOCATS>
<RAPPORTEUR>Mme Sabine Monchambert</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Jean Lessi</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2017:403717.20170426</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, enregistrée le 22 septembre 2016 au secrétariat du contentieux du Conseil d'Etat, la Fédération de la formation professionnelle demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir les décisions, révélées par les relevés de décisions des 26 mai 2015, 7 juillet 2015 et 29 mars 2016, par lesquelles le Comité paritaire interprofessionnel national pour l'emploi et la formation (COPANEF) a procédé à l'habilitation d'organismes formateurs et évaluateurs, au titre du socle de connaissances et de compétences défini par l'article D. 6113-1 du code du travail, ainsi que la décision du 20 juillet 2016 par laquelle ce comité a rejeté son recours gracieux ; <br/>
<br/>
              2°) de mettre à la charge du Comité paritaire interprofessionnel national pour l'emploi et la formation la somme de 10 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code du travail ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Sabine Monchambert, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Jean Lessi, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Rocheteau, Uzan-Sarano, avocat de la Fédération de la formation professionnelle.<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 6123-5 du code du travail : " Le Comité paritaire interprofessionnel national pour l'emploi et la formation est constitué des organisations syndicales de salariés et des organisations professionnelles d'employeurs représentatives au niveau national et interprofessionnel (...) ". Aux termes du I de l'article L. 6323-6 du même code, dans sa rédaction applicable au litige : " Les formations éligibles au compte personnel de formation sont les formations permettant d'acquérir le socle de connaissances et de compétences défini par décret ". L'article D. 6113-1 du même code dispose que : " Le socle de connaissances et de compétences (...) est constitué de l'ensemble des connaissances et des compétences qu'il est utile pour un individu de maîtriser afin de favoriser son accès à la formation professionnelle et son insertion professionnelle (...) ". Enfin, aux termes de l'article D. 6113-3 du même code : " Le socle de connaissances et de compétences professionnelles (...) fait l'objet, sur proposition du Comité paritaire interprofessionnel national pour l'emploi et la formation, d'une certification. / Cette certification s'appuie sur un référentiel qui précise les connaissances et les compétences (...) et sur un référentiel de certification qui détermine les conditions d'évaluation des acquis. / (...) / Le Comité paritaire interprofessionnel national pour l'emploi et la formation définit les modalités de délivrance de la certification. Dans ce cadre, il s'assure notamment que la délivrance de la certification s'effectue dans le respect : / 1° De la transparence de l'information donnée au public ; / 2° De la qualité du processus de certification. (...) ".<br/>
<br/>
              2. Sur le fondement de ces dispositions, le Comité paritaire interprofessionnel national pour l'emploi et la formation a prévu que le socle de connaissances et de compétences professionnelles ferait l'objet d'une certification délivrée par un jury composé paritairement et que les organismes chargés de l'évaluation et de la formation des personnes souhaitant obtenir cette certification feraient l'objet d'une habilitation. La Fédération de la formation professionnelle demande l'annulation pour excès de pouvoir des décisions d'habilitation de divers organismes adoptées par ce comité lors de ses séances des 26 mai 2015, 7 juillet 2015 et 29 mars 2016 ou révélées par les relevés de décisions de ces séances, ainsi que la décision du 20 juillet 2016 par laquelle le comité a rejeté son recours gracieux.  <br/>
<br/>
              3. L'acte, dépourvu de caractère général et impersonnel, par lequel le Comité paritaire interprofessionnel national pour l'emploi et la formation habilite ou refuse d'habiliter un tel organisme n'a pas, par lui-même, pour objet l'organisation d'un service public et ne revêt donc pas un caractère réglementaire. Par suite, les décisions attaquées n'entrent pas dans le champ du 2° de l'article R. 311-1 du code de justice administrative qui donne compétence au Conseil d'Etat pour connaître en premier et dernier ressort des recours dirigés contre les actes réglementaires des ministres et des autres autorités à compétence nationale. Aucune autre disposition du code de justice administrative ne donne compétence au Conseil d'Etat pour connaître en premier et dernier ressort des conclusions de la Fédération de la formation professionnelle tendant à l'annulation de ces décisions. Il y a lieu, en application de l'article R. 351-1 du code de justice administrative, et eu égard à la connexité existant entre les conclusions de la demande, d'en attribuer le jugement au tribunal administratif de Paris.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : Le jugement de la requête de la Fédération de la formation professionnelle est attribué au tribunal administratif de Paris. <br/>
Article 2 : La présente décision sera notifiée à la Fédération de la formation professionnelle, au Comité paritaire national interprofessionnel pour l'emploi et la formation et à la présidente du tribunal administratif de Paris. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
