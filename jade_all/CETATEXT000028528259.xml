<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028528259</ID>
<ANCIEN_ID>JG_L_2014_01_000000356727</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/52/82/CETATEXT000028528259.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère sous-section jugeant seule, 27/01/2014, 356727, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-01-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>356727</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère sous-section jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BORE, SALVE DE BRUNETON ; SCP FABIANI, LUC-THALER</AVOCATS>
<RAPPORTEUR>M. Rémi Decout-Paolini</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Maud Vialettes</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:356727.20140127</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>VU LA PROCEDURE SUIVANTE :<br/>
<br/>
 Procédure contentieuse antérieure<br/>
<br/>
              M. B...E...et M. D...A...ont demandé au tribunal administratif de Caen d'annuler pour excès de pouvoir les arrêtés du maire de Villerville (Calvados) des 17 avril et 31 août 2009 délivrant à M. D...C...un permis de construire une maison d'habitation ainsi qu'un permis de construire modificatif. Par un jugement nos 0901389, 0902406 du 4 février 2010, le tribunal administratif de Caen a annulé le permis de construire du 17 avril 2009 modifié le 31 août 2009.<br/>
<br/>
              Par un arrêt n° 10NT00581 du 30 septembre 2011, la cour administrative d'appel de Nantes, sur la demande de M.C..., a annulé le jugement du tribunal administratif de Caen du 4 février 2010 et rejeté les demandes présentées par M. E...et M. A...devant ce tribunal.<br/>
<br/>
 Procédure devant le Conseil d'Etat <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 14 février et 10 mai 2012 au secrétariat du contentieux du Conseil d'Etat, M. E...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt n° 10NT00581 de la cour administrative d'appel de Nantes du 30 septembre 2011 ;<br/>
<br/>
              2°) de mettre solidairement à la charge de M. C...et de la commune de Villerville le versement de la somme de 2 500 euros à son avocat, la SCP Fabiani, Luc-Thaler, au titre des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991.<br/>
<br/>
              Par un mémoire en défense, enregistré le 2 août 2012, M. C... conclut au rejet du pourvoi et à ce que la somme de 3 000 euros soit mise à la charge de M. E...au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Le pourvoi a été communiqué à la commune de Villerville, qui n'a pas produit de mémoire.<br/>
<br/>
              Vu :<br/>
              - les autres pièces du dossier ;<br/>
              - le code de justice administrative.<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Rémi Decout-Paolini, Maître des Requêtes,  <br/>
<br/>
              - les conclusions de Mme Maud Vialettes, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Fabiani, Luc-Thaler, avocat de M.E..., et à la SCP Boré, Salve de Bruneton, avocat de M.C... ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>CONSIDERANT CE QUI SUIT :<br/>
<br/>
              Sur les moyens relatifs à l'enclavement du terrain d'assiette de la construction :<br/>
<br/>
              1. Aux termes de l'article 2UB3 " Accès et voirie " du règlement du plan d'occupation des sols de la commune de Villerville, alors applicable : " 1. Accès / Tout terrain enclavé est inconstructible à moins que son propriétaire ne produise une servitude de passage suffisante, instituée par acte authentique ou par voie judiciaire, en application de l'article 682 du code civil. En aucun cas l'accès ne pourra avoir une largeur inférieure à 4 mètres (...). / 2. Voirie / Les constructions et installations doivent être desservies par des voies publiques ou privées dont les caractéristiques correspondent à leur destination ; ces voies doivent permettre l'accès du matériel de lutte contre l'incendie. / La création ou l'aménagement de voies publiques ou privées communes, ouvertes à la circulation automobile, est soumise aux conditions suivantes (...) : / - largeur minimale de chaussée : 5 m / - largeur minimale de plate-forme : 8 m (...) ".<br/>
<br/>
              2. En premier lieu, en estimant que le terrain d'assiette de la construction projetée bénéficiait, sur le chemin d'accès reliant, à travers la parcelle cadastrée A n° 11 appartenant à M.A..., la parcelle de M. C...à la rue du Général-Leclerc, d'une servitude de passage, au sens des dispositions du 1. de l'article 2UB3 précité, consentie par l'acte de partage du 15 mars 1857 et attestée par les actes de vente notariés des 10 août 1973 et 6 mars 2006, la cour administrative d'appel de Nantes n'a entaché son arrêt ni d'insuffisance de motivation, ni de dénaturation, ni d'erreur de droit.<br/>
<br/>
              3. En second lieu, il résulte des dispositions précitées que l'accès au sens du 1. de l'article 2UB3 du règlement du plan d'occupation des sols de la commune de Villerville doit s'entendre du seul débouché sur une voie publique ou privée, ouverte à la circulation, de la servitude de passage permettant de relier cette voie à un terrain enclavé. En jugeant, par suite, qu'alors même que sa largeur serait limitée par endroits à 2,50 mètres, la servitude de passage traversant la parcelle A n° 11 satisfaisait aux prescriptions du plan d'occupation des sols, dès lors que la largeur de son débouché sur la rue du Général-Leclerc est de 4,04 mètres, la cour, qui a suffisamment motivé son arrêt, n'a pas commis d'erreur de droit et n'a pas dénaturé les pièces du dossier. <br/>
<br/>
              Sur le moyen relatif à la nécessité d'un projet architectural :<br/>
<br/>
              4. Aux termes de l'article L. 431-1 du code de l'urbanisme : " Conformément aux dispositions de l'article 3 de la loi n° 77-2 du 3 janvier 1977 sur l'architecture, la demande de permis de construire ne peut être instruite que si la personne qui désire entreprendre des travaux soumis à une autorisation a fait appel à un architecte pour établir le projet architectural faisant l'objet de la demande de permis de construire ". Aux termes de l'article R. 431-2 du même code, dans sa rédaction applicable au litige : " Conformément à l'article 1er du décret n° 77-190 du 3 mars 1977, ne sont toutefois pas tenues de recourir à un architecte les personnes physiques ou les exploitations agricoles à responsabilité limitée à associé unique qui déclarent vouloir édifier ou modifier pour elles-mêmes : / a) Une construction à usage autre qu'agricole dont la surface de plancher hors oeuvre nette n'excède pas cent soixante-dix mètres carrés (...) ". Aux termes de l'article R. 112-2 du même code, dans sa rédaction également applicable au litige : " (...) La surface de plancher hors oeuvre nette d'une construction est égale à la surface hors oeuvre brute de cette construction après déduction : / a) Des surfaces de plancher hors oeuvre des combles et des sous-sols non aménageables pour l'habitation ou pour des activités à caractère professionnel, artisanal, industriel ou commercial (...) ". En estimant que la superficie supplémentaire de 28,88 mètres carrés de surface de plancher hors oeuvre nette dont M. E...demandait la prise en compte, en sus des 149,99 mètres carrés déclarés par M. C... dans sa demande de permis de construire, comprenait en réalité la surface du sous-sol, dont le caractère non aménageable n'était pas contesté, ainsi que celle du grenier, dont la surface utile se limitait à 15,20 mètres carrés, ce dont elle a déduit que le dossier de demande de permis de construire n'avait pas à comporter un projet architectural établi par un architecte, la cour s'est livrée à une appréciation souveraine des faits exempte de dénaturation.<br/>
<br/>
              Sur le moyen relatif à la méconnaissance de l'article R. 111-5 du code de l'urbanisme :<br/>
<br/>
              5. Aux termes du troisième alinéa de l'article R. 111-1 du code de l'urbanisme, dans sa rédaction alors en vigueur : " Les dispositions des articles R. 111-3, R. 111-5 à 111-14, R. 111-16 à R. 111-20 et R. 111-22 à R. 111-24-2 ne sont pas applicables dans les territoires dotés d'un plan local d'urbanisme ou d'un document d'urbanisme en tenant lieu ". En vertu de ces dispositions, l'article R. 111-5 du même code relatif à la desserte des terrains d'assiette par des voies publiques ou privées n'était pas applicable dans la commune de Villerville, dotée d'un plan d'occupation des sols. Par suite, si M. E...soutient que la cour a dénaturé les pièces du dossier en estimant que le projet de construction satisfaisait aux prescriptions de l'article R. 111-5, il ne pouvait, en tout état de cause, utilement invoquer ces dispositions à l'encontre des arrêtés litigieux. Le moyen soulevé devant la cour étant ainsi inopérant, il convient de l'écarter pour ce motif qui doit être substitué au motif retenu par la cour administrative d'appel de Nantes.<br/>
<br/>
              6. Il résulte de tout ce qui précède que M. E...n'est pas fondé à demander l'annulation de l'arrêt qu'il attaque.<br/>
<br/>
              7. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à la charge de M. C...et de la commune de Villerville, qui ne sont pas, dans la présente instance, les parties perdantes. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de M.E..., sur le fondement des mêmes dispositions, le versement à M. C...d'une somme de 1 000 euros.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de M. E...est rejeté.<br/>
Article 2 : M. E...versera à M. C...une somme de 1 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée à M. B...E..., à M. D...C...et à la commune de Villerville.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
