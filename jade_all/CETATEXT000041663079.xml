<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000041663079</ID>
<ANCIEN_ID>JG_L_2020_02_000000436454</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/41/66/30/CETATEXT000041663079.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème - 10ème chambres réunies, 28/02/2020, 436454, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-02-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>436454</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème - 10ème chambres réunies</FORMATION>
<TYPE_REC>Autres</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Nicolas Agnoux</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emilie Bokdam-Tognetti</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:436454.20200228</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par un mémoire, enregistré le 7 octobre 2019, M. A... B... a demandé au tribunal administratif de Rennes, en application de l'article 23-1 de l'ordonnance n° 58-1067 du 7 novembre 1958 et à l'appui de sa demande tendant à la décharge des cotisations supplémentaires d'impôt sur le revenu auxquelles il a été assujetti au titre des années 2015 et 2016, de renvoyer au Conseil constitutionnel la question de la conformité aux droits et libertés garantis par la Constitution des dispositions du 2° du II de l'article 156 du code général des impôts en tant qu'elles s'appliquent à la contribution aux charges du mariage. <br/>
<br/>
              Par une ordonnance n° 1904640 du 3 décembre 2019, enregistrée le 4 décembre 2019 au secrétariat du contentieux du Conseil d'Etat, le président de la 2ème chambre du tribunal administratif de Rennes a décidé, par application de l'article 23-2 de l'ordonnance n° 58-1067 du 7 novembre 1958, de transmettre cette question au Conseil d'Etat. <br/>
<br/>
              Par la question prioritaire de constitutionnalité transmise, M. B... soutient que les dispositions du 2° du II de l'article 156 du code général des impôts, applicables au litige, en tant qu'elles réservent la déductibilité fiscale des seules contributions aux charges du mariage dont le versement résulte d'une décision de justice, méconnaissent le principe d'égalité devant la loi énoncé à l'article 6 de la Déclaration des droits de l'homme et du citoyen et le principe d'égalité devant les charges publiques énoncé à l'article 13 de la même Déclaration. <br/>
<br/>
              La question prioritaire de constitutionnalité soulevée par M. B... a été communiquée au Premier ministre et au ministre de l'action et des comptes publics qui n'ont pas présenté d'observations. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la Constitution, notamment son Préambule et son article 61-1 ; <br/>
              - l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
              - le code général des impôts ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Nicolas Agnoux, maître des requêtes,<br/>
<br/>
              - les conclusions de Mme Emilie Bokdam-Tognetti, rapporteur public ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il résulte des dispositions de l'article 23-4 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel que, lorsqu'une juridiction relevant du Conseil d'Etat a transmis à ce dernier, en application de l'article 23-2 de cette même ordonnance, la question de la conformité à la Constitution d'une disposition législative, le Conseil constitutionnel est saisi de cette question de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux.<br/>
<br/>
              2. Aux termes de l'article 156 du code général des impôts, dans sa rédaction applicable au litige : " L'impôt sur le revenu est établi d'après le montant total du revenu net annuel dont dispose chaque foyer fiscal. Ce revenu net est déterminé (...) sous déduction : / II. Des charges ci-après lorsqu'elles n'entrent pas en compte pour l'évaluation des revenus des différentes catégories / (...) 2° (...) contribution aux charges du mariage définie à l'article 214 du code civil, lorsque son versement résulte d'une décision de justice et à condition que les époux fassent l'objet d'une imposition séparée (...) ". <br/>
<br/>
              3. Ces dispositions, qui n'ont pas été déclarées conformes à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sont applicables au litige. Le moyen tiré de ce qu'elles méconnaissent le principe d'égalité devant la loi et devant les charges publiques garantis par les articles 6 et 13 de la Déclaration des droits de l'homme et du citoyen du 26 août 1789, en tant qu'elles subordonnent, pour le calcul du revenu net annuel soumis à l'impôt, la déduction de la contribution aux charges du mariage à la condition que son versement résulte d'une décision de justice, soulève une question présentant un caractère sérieux. Il y a lieu, dès lors, de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité invoquée.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La question de la conformité à la Constitution des mots : " lorsque son versement résulte d'une décision de justice et " figurant au 2° du II de l'article 156 du code général des impôts est renvoyée au Conseil Constitutionnel.<br/>
<br/>
Article 2 : La présente décision sera notifiée à M. A... B... et au ministre de l'action et des comptes publics.<br/>
Copie en sera adressée au Premier ministre et au tribunal administratif de Rennes.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
