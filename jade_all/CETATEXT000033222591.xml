<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033222591</ID>
<ANCIEN_ID>JG_L_2016_10_000000395311</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/22/25/CETATEXT000033222591.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème - 2ème chambres réunies, 07/10/2016, 395311, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-10-07</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>395311</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème - 2ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FOUSSARD, FROGER</AVOCATS>
<RAPPORTEUR>M. Thomas Odinot</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Olivier Henrard</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2016:395311.20161007</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu les procédures suivantes :<br/>
<br/>
              1. Sous le n° 395311, par une requête, enregistrée le 15 décembre 2015 au secrétariat du contentieux du Conseil d'Etat, la chambre de commerce et d'industrie d'Alençon et le département de l'Orne demandent au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret n° 2015-1641 du 11 décembre 2015 portant création de la chambre de commerce et d'industrie territoriale Portes de Normandie ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              2. Sous le n° 395732, par une requête et un mémoire en réplique, enregistrés le 31 décembre 2015 et le 24 août 2016 au secrétariat du contentieux du Conseil d'Etat, MM. A... D...et B...C...demandent au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler pour excès de pouvoir le même décret ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu :<br/>
              - le code de commerce ; <br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Thomas Odinot, auditeur,  <br/>
<br/>
              - les conclusions de M. Olivier Henrard, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Foussard, Froger, avocat de la chambre de commerce et d'industrie d'Alençon, du département de l'Orne et de MM. D...et C...;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 26 septembre 2016, présentée par le département de l'Orne ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 26 septembre 2016, présentée par MM. D... et C...;<br/>
<br/>
<br/>
<br/>1. Considérant que les requêtes visées ci-dessus sont dirigées contre le même décret ; qu'il y a lieu de les joindre pour statuer par une seule décision ; <br/>
<br/>
              2. Considérant que le désistement de la chambre de commerce et d'industrie territoriale Portes de Normandie, venant aux droits de la chambre de commerce et d'industrie d'Alençon, est pur et simple ; que rien ne s'oppose à ce qu'il en soit donné acte ;<br/>
<br/>
              3. Considérant qu'aux termes de l'article L. 711-1 du code de commerce, dans sa rédaction en vigueur à la date du décret attaqué : " Les chambres de commerce et d'industrie territoriales sont créées par décret sur la base du schéma directeur mentionné au 2° de l'article L. 711-8. L'acte de création fixe la circonscription de la chambre et son siège ainsi que la chambre de commerce et d'industrie de région à laquelle elle est rattachée. Toute modification est opérée dans les mêmes formes. / (...) / Les chambres de commerce et d'industrie territoriales qui le souhaitent peuvent s'unir en une seule chambre dans le cadre des schémas directeurs mentionnés au 2° de l'article L. 711-8 ; elles peuvent disparaître au sein de la nouvelle chambre territoriale ou devenir des délégations de la chambre territoriale nouvellement formée et ne disposent alors plus du statut d'établissement public. Dans ce cas, elles déterminent conjointement la façon dont elles souhaitent mutualiser et exercer les fonctions normalement dévolues aux chambres territoriales / (...) " ; qu'en vertu du 2° de l'article L. 711-8 du même code, dans sa rédaction alors en vigueur, les chambres de commerce et d'industrie de région " établissent, dans des conditions fixées par décret en Conseil d'Etat, un schéma directeur qui définit le nombre et la circonscription des chambres territoriales (...) dans leur circonscription en tenant compte de l'organisation des collectivités territoriales en matière de développement et d'aménagement économique, ainsi que de la viabilité économique et de l'utilité pour leurs ressortissants des chambres territoriales (...) " ;<br/>
<br/>
              4. Considérant, en premier lieu, qu'il ressort des pièces du dossier que, par une délibération du 2 avril 2012, l'assemblée générale de la chambre de commerce et d'industrie d'Alençon a décidé d'engager un processus de regroupement avec la chambre de commerce et d'industrie de l'Eure ; que, par une délibération du 7 juin 2013, l'assemblée générale de la chambre de commerce et d'industrie de la région Basse-Normandie a approuvé un schéma directeur prévoyant la réunion de ces deux chambres en une seule ; que ce schéma a été approuvé par le ministre chargé de la tutelle des chambres de commerce et d'industrie par un arrêté du 30 janvier 2014 ; que, par une délibération du 16 juin 2014, l'assemblée générale de la chambre de commerce et d'industrie d'Alençon a décidé d'engager le processus de mise en oeuvre du schéma directeur et a approuvé dans son principe le projet de règlement intérieur de la chambre de commerce et d'industrie territoriale à créer ; que, par une délibération du 24 juin 2014, la chambre de l'Eure a également exprimé son souhait de se regrouper avec celle d'Alençon ; que postérieurement à l'approbation par le ministre du schéma directeur régional, modifié à la demande des chambres de commerce et d'industrie d'Alençon et de l'Eure afin de prévoir leur fusion, l'assemblée générale de la chambre de commerce et d'industrie d'Alençon a, par une délibération du 28 septembre 2015, décidé " de ne pas donner suite au processus de rapprochement avec la chambre de commerce et d'industrie de l'Eure " ; que dès lors que le schéma directeur destiné à permettre la fusion avait été approuvé par la chambre de commerce et d'industrie de la région Basse-Normandie et par le ministre chargé de la tutelle des chambres de commerce et d'industrie, la chambre d'Alençon ne pouvait plus s'opposer à cette fusion ; qu'il suit de là que les moyens tirés de la méconnaissance des dispositions de l'article L. 711-1 du code de commerce, du défaut de visa de la délibération du 28 septembre 2015 de l'assemblée générale de la chambre de commerce et d'industrie d'Alençon et du détournement de pouvoir et de procédure doivent être écartés ; <br/>
<br/>
              6. Considérant, en second lieu, que si le deuxième alinéa de l'article R. 711-1 du code de commerce, dans sa rédaction en vigueur à la date du décret attaqué, dispose qu'il y a au moins une chambre territoriale dans chaque département, le dernier alinéa du même article autorise le schéma directeur mentionné à l'article L. 711-8 du même code à prévoir que la circonscription d'une chambre de commerce et d'industrie territoriale puisse s'étendre sur plusieurs départements ; que la définition de la circonscription des chambres territoriales doit, dans tous les cas, respecter les critères énoncés au 2° de l'article L. 711-8 cité au point 3 et prendre ainsi en compte, en vertu des textes en vigueur à la date du décret attaqué, l'organisation des collectivités territoriales en matière de développement et d'aménagement économique ainsi que la viabilité économique et l'utilité pour leurs ressortissants des chambres territoriales ; que la circonstance que le département de l'Orne ne bénéficie pas d'une chambre de commerce et d'industrie dont la circonscription correspondrait aux limites administratives du département, ce qui priverait, selon les requérants, le tissu économique et professionnel local des interlocuteurs locaux adaptés à leur besoin, ne suffit pas à établir que la fusion organisée par le décret attaqué serait entachée d'erreur manifeste d'appréciation ; qu'au demeurant, ainsi qu'en disposent l'avant-dernier alinéa de l'article L. 711-1 du code de commerce et l'article R. 711-18 du même code, les chambres fusionnées ont la faculté de devenir des délégations de la chambre territoriale nouvellement formée, dans l'hypothèse où l'existence de bassins d'activités économiques le rendrait nécessaire ;<br/>
<br/>
              7. Considérant qu'il résulte de tout ce qui précède, sans qu'il soit besoin de statuer sur les fins de non recevoir opposées par le ministre, que les requêtes doivent être rejetées, y compris les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Il est donné acte du désistement de la chambre de commerce et d'industrie territoriale Portes de Normandie, venant aux droits de la chambre de commerce et d'industrie d'Alençon, sous le n° 395311.<br/>
Article 2 : Les requêtes du département de l'Orne et de MM. D...et C...sont rejetées. <br/>
Article 3 : La présente décision sera notifiée à la chambre de commerce et d'industrie territoriale Portes de Normandie, au département de l'Orne, à M. A...D..., à M. B...C...et au ministre de l'économie et des finances.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
