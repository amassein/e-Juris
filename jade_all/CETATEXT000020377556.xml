<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000020377556</ID>
<ANCIEN_ID>JG_L_2009_02_000000324410</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/20/37/75/CETATEXT000020377556.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 03/02/2009, 324410, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2009-02-03</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>324410</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Bélaval</PRESIDENT>
<AVOCATS>SCP PEIGNOT, GARREAU ; SCP WAQUET, FARGE, HAZAN</AVOCATS>
<RAPPORTEUR>M. Philippe  Bélaval</RAPPORTEUR>
<COMMISSAIRE_GVT/>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 23 janvier 2009 au secrétariat du contentieux du Conseil d'Etat, présentée par M. Tayeb A, ... ; M. A demande au juge des référés du Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'ordonnance du 22 janvier 2009 par laquelle le juge des référés du tribunal administratif de Lyon a rejeté sa demande tendant, sur le fondement de l'article L. 521-2 du code de justice administrative, à la suspension des mesures prises par le préfet du Rhône pour exécuter l'arrêté d'expulsion pris à son encontre le 30 septembre 1988 ;<br/>
<br/>
              2°) de suspendre l'exécution de la décision du 6 décembre 2008 par laquelle le préfet du Rhône a refusé d'abroger l'arrêté d'expulsion en date du 30 septembre 1988 et de la décision du 20 janvier 2009 par laquelle le même préfet l'a placé en rétention dans des locaux ne relevant pas de l'administration pénitentiaire ;<br/>
<br/>
              3°) d'enjoindre au préfet du Rhône de le remettre en liberté sans délai ;<br/>
<br/>
              4°) que la somme de 1 500 euros soit mise à la charge de l'Etat sur le fondement de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              il soutient que l'urgence est caractérisée par l'imminence de son éloignement ; que l'ordonnance rendue est entachée d'une erreur de droit en ce qu'elle a méconnu les dispositions de l'article 86, II, de la loi du 26 novembre 2003 et celles de l'article 78, II, de la loi du 24 juillet 2006, applicables à M. A ; qu'en effet, il justifie d'une résidence habituelle en France depuis qu'il a atteint au plus l'âge de treize ans ; que cette erreur de droit porte une atteinte grave et manifestement illégale à sa liberté d'aller et venir ainsi qu'à son droit au respect de sa vie privée et familiale tel que reconnu par les dispositions de l'article 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
<br/>
<br/>
              Vu l'ordonnance attaquée ;<br/>
              Vu le mémoire en défense, enregistré le 28 janvier 2009, présenté pour le ministre de l'intérieur, de l'outre-mer et des collectivités territoriales ; le ministre conclut au rejet de la requête de M. A ; il fait valoir que c'est à bon droit que le juge des référés du tribunal administratif de Lyon a jugé que le préfet du Rhône avait fait une exacte application des dispositions de l'article 86 de la loi du 26 novembre 2003 et de celles de l'article 78 de la loi du 24 juillet 2006 dès lors que compte tenu de son retour illégal en France à la suite d'une première expulsion, M. A a perdu son droit au séjour et ne remplissait plus la condition de résidence habituelle en France à la date de l'arrêté d'expulsion sur le fondement duquel les décisions litigieuses ont été prises ; que M. A ne justifie pas d'une résidence habituelle en France depuis qu'il a atteint au plus l'âge de treize ans ; qu'en tout état de cause la demande d'abrogation de l'arrêté d'expulsion est tardive en ce qu'elle date du 4 janvier 2007 alors que cette possibilité n'était ouverte que jusqu'au 31 décembre 2004 ;<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
<br/>
              Vu la loi n° 2003-1119 du 26 novembre 2003 relative à la maîtrise de l'immigration, au séjour des étrangers en France et à la nationalité ;<br/>
<br/>
              Vu la loi n° 2006-911 du 24 juillet 2006 relative à l'immigration et à l'intégration ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, M. A, et d'autre part, le ministre de l'intérieur, de l'outre-mer et des collectivités territoriales ;<br/>
<br/>
              Vu le procès-verbal de l'audience publique du jeudi 29 janvier 2009 à 11h au cours de laquelle ont été entendus :<br/>
<br/>
              - Me Waquet, avocat au Conseil d'Etat et à la Cour de cassation, avocat de M. A ;<br/>
              - Me Garreau, avocat au Conseil d'Etat et à la Cour de cassation, avocat du ministre de l'intérieur, de l'outre-mer et des collectivités territoriales ;<br/>
              - le représentant du ministre de l'intérieur, de l'outre-mer et des collectivités territoriales ;<br/>
<br/>
<br/>
<br/>
              Considérant qu'aux termes de l'article L. 521-2 du code de justice administrative : « Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public (...) aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale (...) » ;<br/>
<br/>
              Considérant que M. A, ressortissant algérien qui est né le 3 mai 1958 en France et qui a fait l'objet d'un arrêté d'expulsion le 30 septembre 1988, soutient que la décision du 6 décembre 2008 par laquelle le préfet du Rhône a refusé d'abroger cet arrêté et celle du 20 janvier 2009 par laquelle le préfet a ordonné, après son interpellation, son placement dans des locaux ne relevant pas de l'administration pénitentiaire portent une atteinte grave et manifestement illégale à sa liberté d'aller et de venir, ainsi qu'à son droit de mener une vie privée et familiale normale ;<br/>
<br/>
              Considérant qu'aux termes des dispositions du II de l'article 86 de la loi n° 2003-1119 du 26 novembre 2003 susvisée : « (...) s'il en fait la demande avant le 31 décembre 2004, tout étranger justifiant qu'il résidait habituellement en France avant le 30 avril 2003 et ayant fait l'objet d'un arrêté d'expulsion peut obtenir l'abrogation de cette décision s'il entre dans l'une des catégories visées aux 1° à 4° du I. (...) » ; que ces catégories, qui sont celles applicables à un étranger qui demande à être relevé de plein droit d'une peine complémentaire d'interdiction du territoire français, sont définies de la manière suivante : « 1° Il résidait habituellement en France depuis au plus l'âge de treize ans à la date du prononcé de la peine ; 2° Il résidait régulièrement en France depuis plus de vingt ans à la date du prononcé de la peine ; 3° Il résidait régulièrement en France depuis plus de dix ans à la date du prononcé de la peine et, ne vivant pas en état de polygamie, est marié depuis au moins trois ans avec un ressortissant français ayant conservé la nationalité française ou avec un ressortissant étranger qui réside habituellement en France depuis au plus l'âge de 13 ans, à condition que la communauté de vie n'ait pas cessé ; 4° Il résidait régulièrement en France depuis plus de dix ans à la date du prononcé de la peine et, ne vivant pas, en état de polygamie, est père ou mère d'un enfant français mineur résidant en France, à condition qu'il établisse contribuer effectivement à l'entretien et à l'éducation de l'enfant dans les conditions prévues par l'article 371-2 du code civil, cette condition devant être remplie depuis la naissance de ce dernier ou depuis un an. » ; qu'enfin les dispositions du II de l'article 78 de la loi n° 2006-911 du 24 juillet 2006 susvisée précisent : « Pour l'application des dispositions du II du même article 86, lorsqu'un étranger a présenté, avant le 31 décembre 2004, une demande tendant à l'abrogation d'un arrêté d'expulsion dont il a fait l'objet et établit qu'il n'a pas quitté le territoire français pendant une période de plus de trois ans durant les dix années précédant le 30 avril 2003, la condition de résidence habituelle en France mentionnée au premier alinéa du même II est réputée satisfaite. Dans un délai de six mois suivant la publication de la présente loi, les étrangers qui, ayant présenté une demande en ce sens avant le 31 décembre 2004, ont vu leur demande d'abrogation rejetée, sont recevables à présenter une nouvelle demande auprès de l'autorité administrative compétente. » ;<br/>
<br/>
              Considérant qu'il résulte de la combinaison de ces dispositions que, pour justifier de l'illégalité de la décision par laquelle le préfet du Rhône a rejeté sa demande d'abrogation de l'arrêté d'expulsion dont il a fait l'objet, M. A devait établir de manière distincte, en premier lieu, avoir présenté une demande en ce sens avant le 31 décembre 2004, en deuxième lieu, n'avoir pas quitté le territoire français pendant plus de trois ans entre 1993 et 2003, et, en troisième lieu, dès lors qu'il ne contestait pas qu'il n'entrait dans aucune des autres catégories définies par le I précité de l'article 86 de la loi du 26 novembre 2003, avoir eu sa résidence habituelle en France depuis au plus l'âge de treize ans à la date de l'arrêté d'expulsion ; <br/>
<br/>
              Considérant que pour l'appréciation de cette dernière condition, il ne pouvait être fait usage, contrairement à ce que soutient M. A, de la présomption instituée par des dispositions du II de l'article 78 de la loi du 24 juillet 2006, dès lors que cette présomption ne vaut, selon ses termes mêmes, que pour l'application du II de l'article 86, de la loi du 26 novembre 2003 ; qu'il est constant que M. A a fait l'objet le 30 juin 1977 d'un premier arrêté d'expulsion, qui a été exécuté le 3 août 1977 ; que s'il soutient être rentré en France dès 1980, la présence en France de M. A n'est, en l'état de l'instruction, pas attestée avant le 6 novembre 1981, date à laquelle il a commis une infraction pénale ; que compte tenu de cette interruption, il ne pouvait être regardé comme ayant eu sa résidence habituelle en France entre au plus tard le 3 mai 1971, date de son treizième anniversaire, et le 30 septembre 1988, date de l'arrêté d'expulsion litigieux ; que pour ce seul motif et sans qu'il y ait lieu de rechercher si les autres conditions étaient remplies, la décision par laquelle le préfet du Rhône a rejeté la demande d'abrogation présentée par M. A ne saurait être regardée comme entachée d'une illégalité manifeste ; que par voie de conséquence il en va de même, en l'absence de moyen spécifique articulé à l'encontre de cette décision, de celle par laquelle le préfet l'a placé dans un local de rétention en vue de l'exécution de l'expulsion ; que par suite M. A n'est pas fondé à soutenir que c'est à tort que, par l'ordonnance attaquée, le juge des référés du tribunal administratif de Lyon a rejeté la demande dont il l'a saisi sur le fondement de l'article L. 521-2 précité du code de justice administrative ; que dès lors sa requête ne peut qu'être rejetée, y compris les conclusions présentées sur le fondement de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de M. Tayeb A est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à M. Tayeb A et au ministre de l'intérieur, de l'outre-mer et des collectivités territoriales.<br/>
<br/>
</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
