<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032772240</ID>
<ANCIEN_ID>JG_L_2016_06_000000400769</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/77/22/CETATEXT000032772240.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 23/06/2016, 400769, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-06-23</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>400769</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2016:400769.20160623</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, enregistrée le 20 juin 2016 au secrétariat du contentieux du Conseil d'Etat, M. B...A...demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative :<br/>
<br/>
              1°) d'ordonner la suspension de l'exécution du décret n° 2016-503 du 23 avril 2016 relatif à la consultation des électeurs des communes de la Loire-Atlantique sur le projet de transfert de l'aéroport de Nantes-Atlantique sur la commune de Notre-Dame-des-Landes ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 150 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Il soutient que : <br/>
              - il a intérêt à agir dès lors que le projet Notre-Dame-des-Landes est financé par l'Etat et la région Bretagne et qu'il est contribuable français et réside en Bretagne ;<br/>
              - la condition d'urgence est remplie, dès lors que, d'une part, la consultation locale ouverte par le décret en cause se déroule le 26 juin 2016 et que, d'autre part, l'étendue géographique de la consultation prévue par le décret contesté le prive de son droit de vote ;<br/>
              - il existe un doute sérieux sur la légalité du décret litigieux ;<br/>
              - le décret est dépourvu de base légale dès lors que le référendum ne pouvait porter sur un projet national ;<br/>
              - il est entaché d'illégalité dès lors qu'il ne lui permet pas d'exercer son droit de vote.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la Constitution ;<br/>
              - le code de l'environnement ; <br/>
              - la loi n° 2015-990 du 6 aout 2015 ;<br/>
              - l'ordonnance n° 2016-488 du 21 avril 2016 ;<br/>
              - le décret n° 2016-491 du 21 avril 2016 ;<br/>
              - le décret n° 2016-503 du 23 avril 2016 ;<br/>
              - le code de justice administrative ;<br/>
              - la décision du Conseil d'Etat, statuant au contentieux n°s 400364, 400365 du 20 juin 2016 ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision " ; qu'en vertu de l'article L. 522-3 du même code, le juge des référés peut rejeter une requête par une ordonnance motivée, sans instruction contradictoire ni audience publique, lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable ou qu'elle est mal fondée ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article L. 123-20 du code de l'environnement, issu de l'ordonnance du 21 avril 2016 relative à la consultation locale sur les projets susceptibles d'avoir une incidence sur l'environnement : " L'Etat peut consulter les électeurs d'une aire territoriale déterminée afin de recueillir leur avis sur un projet d'infrastructure ou d'équipement susceptible d'avoir une incidence sur l'environnement dont la réalisation est subordonnée à la délivrance d'une autorisation relevant de sa compétence, y compris après une déclaration d'utilité publique. " ; qu'aux termes de l'article L. 123-23 du même code : " La consultation est décidée par un décret qui en indique l'objet, la date ainsi que le périmètre, qui définit la question posée et qui convoque les électeurs. Il est publié au plus tard deux mois avant la date de la consultation (...) " ; qu'aux termes de l'article L. 123-21 du code de l'environnement : " L'aire de la consultation correspond à celle du territoire couvert par l'enquête publique dont ce projet a fait l'objet ou, lorsque plusieurs enquêtes publiques ont été réalisées au titre de législations distinctes, à celle de l'ensemble du territoire couvert par ces enquêtes. Le territoire couvert par l'enquête est celui des communes désignées comme lieux d'enquête par l'arrêté d'ouverture de celle-ci ainsi que, lorsque le chef-lieu d'une circonscription administrative de l'Etat a également été désigné comme lieu d'enquête, le territoire des communes comprises dans cette circonscription (...) " ;<br/>
<br/>
              3. Considérant que concomitamment à l'enregistrement de la requête, le Conseil d'Etat a, par une décision nos 400364 et 400365, rejeté au fond la requête dirigée contre le décret n° 2016-503 du 23 avril 2016 notamment en précisant que lorsqu'une préfecture de département est désignée en cette qualité comme lieu d'enquête, ainsi que le prévoyait en l'espèce l'avis d'ouverture des enquêtes publiques pris en application de l'arrêté de déclaration d'utilité publique pris par le préfet de la Loire-Atlantique du 27 septembre 2006 qui prévoyait que les enquêtes publiques étaient ouvertes, notamment, à la préfecture de la Loire-Atlantique, il résulte des dispositions précitées qu'il y a lieu de consulter les électeurs des communes du département, sans qu'ait une incidence sur ce point la circonstance que la commune où se situe cette préfecture ait également la qualité de chef-lieu de région ; que par ailleurs aucune des dispositions législatives précitées n'a prévu que la consultation devait se dérouler dans toutes les collectivités territoriales ayant  participé au financement du projet ; <br/>
<br/>
              4. Considérant qu'il résulte de tout ce qui précède que M. A...n'est pas fondé à demander la suspension de l'exécution du décret contesté et à qu'il soit fait droit à sa demande ; que la requête, y compris les conclusions tendant à l'application de l'article L. 761-1 du code de justice administrative, ne peut, par suite, qu'être rejetée selon la procédure prévue par l'article L. 522-3 de ce code ;<br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de M. A...est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à M. B...A....<br/>
Copie en sera adressée au Premier ministre, à la ministre de l'environnement, de l'énergie et de la mer, chargée des relations internationales sur le climat, et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
