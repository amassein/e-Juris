<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037070267</ID>
<ANCIEN_ID>JG_L_2018_06_000000409227</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/07/02/CETATEXT000037070267.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème et 5ème chambres réunies, 14/06/2018, 409227</TITRE>
<DATE_DEC>2018-06-14</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>409227</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème et 5ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROCHETEAU, UZAN-SARANO ; SCP MARLANGE, DE LA BURGADE ; SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR>M. Cyrille Beaufils</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Louis Dutheillet de Lamothe</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:409227.20180614</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête sommaire, un mémoire complémentaire et un nouveau mémoire, enregistrés les 24 mars et 22 juin 2017 et le 14 mai 2018 au secrétariat du contentieux du Conseil d'Etat, l'association Fédération environnement durable et l'association Vent de colère ! Fédération nationale demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret n° 2017-81 du 26 janvier 2017 relatif à l'autorisation environnementale, à titre principal en tant seulement qu'il prévoit que, pour les éoliennes, l'autorisation environnementale dispense de permis de construire et, à titre subsidiaire, dans son intégralité ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la Constitution, notamment son Préambule ;<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - le code de l'environnement ;<br/>
              - le code de l'urbanisme ;<br/>
              - l'ordonnance n° 2017-80 du 6 janvier 2017 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Cyrille Beaufils, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Louis Dutheillet de Lamothe, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Marlange, de la Burgade, avocat de l'association Fédération environnement durable et autres, à la SCP Rocheteau, Uzan-Sarano, avocat de l'association France énergie éolienne et à la SCP Piwnica, Molinié, avocat de la société Electricité de France.<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1.	Aux termes de l'article L. 181-2 du code de l'environnement, dans sa rédaction issue de l'ordonnance du 26 janvier 2017 relative à l'autorisation environnementale : " I. L'autorisation environnementale tient lieu, y compris pour l'application des autres législations, des autorisations, enregistrements, déclarations, absences d'opposition, approbations et agréments suivants, lorsque le projet d'activités, installations, ouvrages et travaux relevant de l'article L. 181-1 y est soumis ou les nécessite : / (...) / 7° Récépissé de déclaration ou enregistrement d'installations mentionnées aux articles L. 512-7 ou L. 512-8, à l'exception des déclarations que le pétitionnaire indique vouloir effectuer de façon distincte de la procédure d'autorisation environnementale, ou arrêté de prescriptions applicable aux installations objet de la déclaration ou de l'enregistrement (...) / 12° Autorisations prévues par les articles L. 5111-6, L. 5112-2 et L. 5114-2 du code de la défense, autorisations requises dans les zones de servitudes instituées en application de l'article L. 5113-1 de ce code et de l'article L. 54 du code des postes et des communications électroniques, autorisations prévues par les articles L. 621-32 et L. 632-1 du code du patrimoine et par l'article L. 6352-1 du code des transports, lorsqu'elles sont nécessaires à l'établissement d'installations de production d'électricité utilisant l'énergie mécanique du vent. (...) ". Le décret du 26 janvier 2017 relatif à l'autorisation environnementale a créé l'article R. 425-29-2 du code de l'urbanisme aux termes duquel : " Lorsqu'un projet d'installation d'éoliennes terrestres est soumis à autorisation environnementale en application du chapitre unique du titre VIII du livre Ier du code de l'environnement, cette autorisation dispense du permis de construire ". L'association Fédération environnement durable et autre demandent l'annulation de ce décret dans cette mesure. <br/>
<br/>
              2.	L'association France énergie éolienne, qui réunit des professionnels de ce secteur, a intérêt au maintien du décret attaqué. Son intervention est donc recevable.<br/>
<br/>
              3.	En premier lieu, en vertu des articles 13 et 19 de la Constitution, les décrets délibérés en conseil des ministres sont signés par le Président de la République et contresignés par le Premier ministre ainsi que, le cas échéant, par les ministres responsables. Ces derniers sont ceux auxquels incombent, à titre principal, la préparation et l'exécution des décrets dont s'agit. La circonstance que les dispositions contestées modifient la procédure applicable en matière d'urbanisme pour l'autorisation de projets d'éoliennes terrestres n'entraîne pas que le ministre de l'aménagement du territoire, de la ruralité et des collectivités territoriales soit chargé, à titre principal, de la préparation et de l'exécution du décret attaqué. Ce ministre ne saurait, dès lors, être regardé comme un ministre responsable au sens de l'article 19 de la Constitution. Par suite, le moyen tiré du défaut de contreseing du ministre de l'aménagement du territoire, de la ruralité et des collectivités territoriales ne peut qu'être écarté.<br/>
<br/>
              4.	En deuxième lieu, aux termes de l'article L. 110-1 du code de l'environnement : " I. - Les espaces, ressources et milieux naturels terrestres et marins, les sites, les paysages diurnes et nocturnes, la qualité de l'air, les êtres vivants et la biodiversité font partie du patrimoine commun de la nation. (...) / II. - Leur connaissance, leur protection, leur mise en valeur, leur restauration, leur remise en état, leur gestion, la préservation de leur capacité à évoluer et la sauvegarde des services qu'ils fournissent sont d'intérêt général et concourent à l'objectif de développement durable qui vise à satisfaire les besoins de développement et la santé des générations présentes sans compromettre la capacité des générations futures à répondre aux leurs. Elles s'inspirent, dans le cadre des lois qui en définissent la portée, des principes suivants : / (...) 9° Le principe de non-régression, selon lequel la protection de l'environnement, assurée par les dispositions législatives et réglementaires relatives à l'environnement, ne peut faire l'objet que d'une amélioration constante, compte tenu des connaissances scientifiques et techniques du moment. (...) ". <br/>
<br/>
              5.	D'une part, aux termes de l'article L. 421-5 du code de l'urbanisme : " Un décret en Conseil d'Etat arrête la liste des constructions, aménagements, installations et travaux qui, par dérogation aux dispositions des articles L. 421-1 à L. 421-4, sont dispensés de toute formalité au titre du présent code en raison : / (...) / d) Du fait que leur contrôle est exclusivement assuré par une autre autorisation ou une autre législation ; (...) ". Aux termes de l'article L. 421-8 du même code : " A l'exception des constructions mentionnées aux b et e de l'article L. 421-5, les constructions, aménagements, installations et travaux dispensés de toute formalité au titre du présent code doivent être conformes aux dispositions mentionnées à l'article L. 421-6. ". Le premier alinéa de l'article L. 421-6 du même code prévoit que : " Le permis de construire ou d'aménager ne peut être accordé que si les travaux projetés sont conformes aux dispositions législatives et réglementaires relatives à l'utilisation des sols, à l'implantation, la destination, la nature, l'architecture, les dimensions, l'assainissement des constructions et à l'aménagement de leurs abords et s'ils ne sont pas incompatibles avec une déclaration d'utilité publique. ".<br/>
<br/>
              6.	D'autre part, le 12° du I de l'article D. 181-15-2 du code de l'environnement, dans sa rédaction résultant du décret attaqué, prévoit que le dossier de demande d'autorisation environnementale, pour les installations terrestres de production d'électricité à partir de l'énergie mécanique du vent, est complété d'un document établissant que le projet est conforme aux documents d'urbanisme. <br/>
<br/>
              7.	Il résulte des dispositions combinées citées aux points précédents que, si l'article R. 425-29-2 introduit dans le code de l'urbanisme par le décret attaqué dispense les projets d'installation d'éoliennes terrestres soumis à autorisation environnementale de l'obtention d'un permis de construire, il n'a, en revanche, ni pour objet ni pour effet de dispenser de tels projets du respect des règles d'urbanisme qui leurs sont applicables. Les dispositions citées aux points 5 et 6 mettent à la charge de l'autorité administrative, à l'occasion de l'instruction de la demande d'autorisation environnementale, l'examen de la conformité des projets d'installations d'éoliennes aux documents d'urbanisme applicables. Le moyen tiré de ce que le décret attaqué méconnaîtrait le principe de non-régression posé par l'article L. 110-1 du code de l'environnement au motif qu'il dispenserait ces projets du respect des règles d'urbanisme qui leurs sont applicables ne peut donc qu'être écarté. <br/>
<br/>
              8.	En dernier lieu, d'une part, il résulte de ce qui vient d'être dit que le moyen tiré de ce que les dispositions contestées méconnaîtraient le droit au recours effectif découlant de l'article 16 de la Déclaration des droits de l'homme et du citoyen et des stipulations de l'article 13 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, en ce qu'elles restreindraient la possibilité d'invoquer l'incompatibilité du projet avec les règles d'urbanisme applicables à l'occasion d'un recours contre l'autorisation environnementale, doit, en tout état de cause, être écarté. <br/>
<br/>
              9.	D'autre part, en vertu de l'article R. 181-50 du code de l'environnement, dans sa rédaction issue du décret attaqué, les autorisations environnementales " peuvent être déférées à la juridiction administrative : / (...) / 2° Par les tiers intéressés en raison des inconvénients ou des dangers pour les intérêts mentionnés à l'article L. 181-3 (...) ". L'article L. 181-3 du même code renvoie à son article L. 511-1 pour les installations " qui peuvent présenter des dangers ou des inconvénients soit pour la commodité du voisinage, soit pour la santé, la sécurité, la salubrité publiques, soit pour l'agriculture, soit pour la protection de la nature, de l'environnement et des paysages, soit pour l'utilisation rationnelle de l'énergie, soit pour la conservation des sites et des monuments ainsi que des éléments du patrimoine archéologique ". Ces dispositions, qui, contrairement à ce qui est soutenu, ne sauraient être interprétées comme exigeant qu'un tiers soit en mesure de se prévaloir d'une atteinte à l'ensemble des intérêts mentionnés à l'article L. 181-3 pour être recevable à déférer une autorisation environnementale à la juridiction administrative, assurent l'accès au juge des personnes susceptibles d'être affectées par la délivrance d'une telle autorisation, y compris pour contester la méconnaissance des règles d'urbanisme applicables. Par suite, le moyen tiré de ce que les dispositions contestées de l'article R. 425-29-2 du code de l'urbanisme porteraient atteinte au droit au recours effectif de ces personnes doit être écarté.<br/>
<br/>
              10.	Il résulte de tout ce qui précède que l'association Fédération environnement durable et autre ne sont pas fondées à demander l'annulation du décret qu'elles attaquent. <br/>
<br/>
              11.	Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante. Par ailleurs, l'association France énergie éolienne n'aurait pas eu qualité pour former tierce opposition à la présente décision si celle-ci avait prononcé l'annulation du décret 26 janvier 2017 relatif à l'autorisation environnementale et si elle n'avait pas été présente à l'instance. Elle ne peut donc être regardée comme une partie pour l'application de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
      Article 1er : L'intervention de l'association France énergie éolienne est admise. <br/>
<br/>
Article 2 : La requête de l'association Fédération environnement durable et autre est rejetée.<br/>
<br/>
Article 3 : Les conclusions présentées par l'association France énergie éolienne au titre de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
<br/>
Article 4 : La présente décision sera notifiée à l'association Fédération environnement durable, première dénommée, pour l'ensemble des requérants, à l'association France énergie éolienne, au Premier ministre et au ministre d'Etat, ministre de la transition écologique et solidaire.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">29-035 ENERGIE. - RÉGLEMENTATION DISPENSANT LES PROJETS D'INSTALLATION D'ÉOLIENNES TERRESTRES SOUMIS À AUTORISATION ENVIRONNEMENTALE DE L'OBTENTION D'UN PERMIS DE CONSTRUIRE - PRINCIPE DE NON-RÉGRESSION (II DE L'ARTICLE L. 110-1 DU CODE DE L'ENVIRONNEMENT) - MÉCONNAISSANCE - ABSENCE, DÈS LORS QUE CES PROJETS NE SONT PAS DISPENSÉS DU RESPECT DES RÈGLES D'URBANISME [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">44 NATURE ET ENVIRONNEMENT. - PRINCIPE DE NON-RÉGRESSION (II DE L'ARTICLE L. 110-1 DU CODE DE L'ENVIRONNEMENT) - RÉGLEMENTATION DISPENSANT LES PROJETS D'INSTALLATION D'ÉOLIENNES TERRESTRES SOUMIS À AUTORISATION ENVIRONNEMENTALE DE L'OBTENTION D'UN PERMIS DE CONSTRUIRE - MÉCONNAISSANCE - ABSENCE, DÈS LORS QUE CES PROJETS NE SONT PAS DISPENSÉS DU RESPECT DES RÈGLES D'URBANISME [RJ1].
</SCT>
<ANA ID="9A"> 29-035 Si l'article R. 425-29-2 du code de l'urbanisme dispense les projets d'installation d'éoliennes terrestres soumis à autorisation environnementale de l'obtention d'un permis de construire, il n'a, en revanche, ni pour objet ni pour effet de dispenser de tels projets du respect des règles d'urbanisme qui leurs sont applicables. Les articles L. 421-5, L. 421-6 et L. 421-8 du code de l'urbanisme et le 12° du I de l'article D. 181-15-2 du code de l'environnement mettent à la charge de l'autorité administrative, à l'occasion de l'instruction de la demande d'autorisation environnementale, l'examen de la conformité des projets d'installation d'éoliennes aux documents d'urbanisme applicables. Par suite, l'article R. 425-29-2 ne méconnaît pas le principe de non-régression posé par l'article L. 110-1 du code de l'environnement au motif qu'il dispenserait ces projets du respect des règles d'urbanisme qui leurs sont applicables.</ANA>
<ANA ID="9B"> 44 Si l'article R. 425-29-2 du code de l'urbanisme dispense les projets d'installation d'éoliennes terrestres soumis à autorisation environnementale de l'obtention d'un permis de construire, il n'a, en revanche, ni pour objet ni pour effet de dispenser de tels projets du respect des règles d'urbanisme qui leurs sont applicables. Les articles L. 421-5, L. 421-6 et L. 421-8 du code de l'urbanisme et le 12° du I de l'article D. 181-15-2 du code de l'environnement mettent à la charge de l'autorité administrative, à l'occasion de l'instruction de la demande d'autorisation environnementale, l'examen de la conformité des projets d'installation d'éoliennes aux documents d'urbanisme applicables. Par suite, l'article R. 425-29-2 ne méconnaît pas le principe de non-régression posé par l'article L. 110-1 du code de l'environnement au motif qu'il dispenserait ces projets du respect des règles d'urbanisme qui leurs sont applicables.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., sur l'application du principe de non-régression, CE, 8 décembre 2017, Fédération Allier Nature, n° 404391, T. p. 690.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
