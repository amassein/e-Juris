<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039409928</ID>
<ANCIEN_ID>JG_L_2019_11_000000426696</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/40/99/CETATEXT000039409928.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème chambre, 21/11/2019, 426696, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-11-21</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>426696</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MARLANGE, DE LA BURGADE</AVOCATS>
<RAPPORTEUR>M. Vincent Daumas</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Laurent Cytermann</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2019:426696.20191121</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              La société Alliance La Foncière a demandé au tribunal administratif de Montpellier de prononcer la décharge de la cotisation de taxe d'aménagement à laquelle elle a été assujettie au titre de l'année 2015 à raison du permis de construire que lui a délivré le maire de Carcassonne pour la construction d'un bâtiment destiné à Pôle Emploi. Par un jugement n° 1604145 du 9 novembre 2018, le tribunal administratif de Montpellier a rejeté sa demande.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 28 décembre 2018 et 19 mars 2019 au secrétariat du contentieux du Conseil d'Etat, la société Alliance La Foncière demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code général des impôts ; <br/>
              - le code de l'urbanisme ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Vincent Daumas, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Laurent Cytermann, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Marlange, de la Burgade, avocat de la société Alliance La Foncière ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que la société Alliance La Foncière a été assujettie à la taxe d'aménagement à raison du permis de construire qu'elle a obtenu, le 13 septembre 2013, en vue de la construction, dans le cadre d'un contrat de location en l'état futur d'achèvement (LEFA) conclu le 29 juillet 2013 avec Pôle emploi, d'un immeuble destiné à abriter une de ses agences. Elle a demandé au tribunal administratif de Montpellier de prononcer la décharge de la cotisation mise à sa charge par un titre de perception émis le 30 janvier 2015 par le préfet de l'Aude. Elle se pourvoit en cassation contre le jugement du 9 novembre 2018 par lequel le tribunal administratif de Montpellier a rejeté sa demande.<br/>
<br/>
              2. Aux termes de l'article L. 331-7 du code de l'urbanisme : " Sont exonérées de la part communale ou intercommunale de la taxe : / 1° Les constructions et aménagements destinés à être affectés à un service public ou d'utilité publique, dont la liste est fixée par un décret en Conseil d'Etat (...). ". Aux termes de l'article R. 331-4 du même code : " Pour l'application du 1° de l'article L. 331-7, sont exonérées de la part communale ou intercommunale de la taxe d'aménagement les constructions définies ci-après : (...) / 2° Les constructions édifiées pour le compte de l'Etat, des collectivités territoriales ou de leurs groupements, en vertu d'un contrat de partenariat, (...) qui sont incorporées au domaine de la personne publique conformément aux clauses du contrat, au plus tard à l'expiration de ce contrat, et exemptées de la taxe foncière sur les propriétés bâties en application du 1° de l'article 1382 du code général des impôts ; (...) / 3° Les constructions (...) édifiées par, ou, dans le cadre d'un des contrats mentionnés au 2°, pour le compte : / a) Des établissements publics n'ayant pas un caractère industriel ou commercial (...) ". <br/>
<br/>
              3. Il ressort des pièces du dossier soumis aux juges du fond, ainsi que l'administration l'avait relevé devant le tribunal administratif sans être contredite, que le contrat de LEFA conclu entre la société requérante et Pôle emploi ne comportait pas de clause prévoyant l'incorporation de l'immeuble en cause au domaine de cet établissement public, mais seulement la possibilité d'un tel transfert de propriété au bénéfice de ce dernier, sous la forme d'un droit de préférence reconnu au preneur en cas de cession de l'immeuble. Dès lors, la société requérante ne pouvait, en tout état de cause, prétendre au bénéfice de l'exonération prévue par les dispositions du 3° de l'article R. 331-4 du code de l'urbanisme. Il y a lieu de substituer ce motif, qui n'appelle l'appréciation d'aucune circonstance de fait et qui justifie légalement le dispositif du jugement attaqué, à celui retenu par le tribunal administratif pour écarter le moyen selon lequel le contrat conclu entre la société requérante et Pôle emploi devait être regardé comme un contrat de partenariat. Les moyens du pourvoi, dirigés contre ce dernier motif, ne peuvent, par suite, qu'être écartés.<br/>
<br/>
              4. Il résulte de ce qui précède que le pourvoi de la société Alliance La Foncière doit être rejeté. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de la société Alliance La Foncière est rejeté. <br/>
Article 2 : La présente décision sera notifiée à la société Alliance La Foncière et au ministre de la cohésion des territoires et des relations avec les collectivités. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
