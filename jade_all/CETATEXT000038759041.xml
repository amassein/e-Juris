<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038759041</ID>
<ANCIEN_ID>JG_L_2019_07_000000417985</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/75/90/CETATEXT000038759041.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème chambre, 10/07/2019, 417985, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-07-10</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>417985</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BARADUC, DUHAMEL, RAMEIX ; SCP WAQUET, FARGE, HAZAN</AVOCATS>
<RAPPORTEUR>M. Laurent-Xavier Simonel</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emmanuelle Cortot-Boucher</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2019:417985.20190710</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme B...A...a demandé au tribunal administratif de Dijon d'annuler les arrêtés des 21 juillet et 30 septembre 2014 par lesquels le président de la communauté de communes du Florentinois lui a supprimé la part variable de la prime liée aux résultats et la part fonctionnelle de la prime de fonction et de résultats, d'annuler l'arrêté du 5 décembre 2014 par lequel le président de la communauté de communes l'a licenciée pour insuffisance professionnelle, d'ordonner la communication de divers documents, marchés et enregistrements et d'ordonner sa réintégration et la reconstitution de sa carrière ainsi que le versement d'une indemnité égale aux primes qu'elle aurait dû recevoir, dont la prime de fonction et de résultats, jusqu'à la date du jugement.  <br/>
<br/>
              Par un jugement n° 1403830 du 10 juillet 2017, le tribunal administratif de Dijon a annulé l'arrêté du 21 juillet 2014 par lequel le président de la communauté de communes du Florentinois lui a supprimé la part variable de la prime liée aux résultats à compter du 1er juillet 2014, a enjoint à la communauté de communes du Florentinois de payer le montant de cette prime jusqu'à la date de son licenciement et a rejeté le surplus de ses conclusions. <br/>
<br/>
              Par une ordonnance n° 17LY03380 du 4 décembre 2017, le président de la troisième chambre de la cour administrative d'appel de Lyon a rejeté l'appel formé par Mme A... contre ce jugement. <br/>
<br/>
              Par un pourvoi et un mémoire en réplique, enregistrés les 8 février et 28 juin 2018 au secrétariat du contentieux du Conseil d'Etat, Mme A...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de la communauté de communes Serein et Armance, venant aux droits de la communauté de communes du Florentinois, la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ; <br/>
              - le code civil ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Laurent-Xavier Simonel, conseiller d'Etat en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Emmanuelle Cortot-Boucher, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Baraduc, Duhamel, Rameix, avocat de Mme A...et à la SCP Waquet, Farge, Hazan, avocat de la communauté de communes Serein et Armance ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que le président de la communauté de communes du Florentinois, devenue la communauté de communes Serein et Armance, a, d'une part, par deux arrêtés du 21 juillet 2014 et du 30 septembre 2014, supprimé respectivement la part variable de la prime liée aux résultats et la part fonctionnelle de la prime de fonction et de résultats de MmeA..., alors directrice générale des services de cette communauté de communes et a, d'autre part, prononcé le 2 décembre 2014 son licenciement pour insuffisance professionnelle. Par un jugement du 10 juillet 2017, le tribunal administratif de Dijon a annulé l'arrêté du 21 juillet 2014 et rejeté le surplus des conclusions de MmeA.... Mme A... a fait appel de ce jugement devant la cour administrative d'appel de Lyon. Par une ordonnance du 4 décembre 2017, prise sur le fondement du 4° de l'article R. 222-1 du code de justice administrative, le président de la troisième chambre de la cour a rejeté sa requête. Mme A...se pourvoit en cassation contre cette ordonnance. <br/>
<br/>
              2. Aux termes de l'article R. 222-1 du code de justice administrative : " Les présidents de tribunal administratif et de cour administrative d'appel, les premiers vice-présidents des tribunaux et des cours, le vice-président du tribunal administratif de Paris, les présidents de formation de jugement des tribunaux et des cours et les magistrats ayant une ancienneté minimale de deux ans et ayant atteint au moins le grade de premier conseiller désignés à cet effet par le président de leur juridiction peuvent, par ordonnance : (...) 4° Rejeter les requêtes manifestement irrecevables, lorsque la juridiction n'est pas tenue d'inviter leur auteur à les régulariser ou qu'elles n'ont pas été régularisées à l'expiration du délai imparti par une demande en ce sens (...) ".<br/>
<br/>
              3. En premier lieu, aux termes des dispositions de l'article R. 431-2 du même code : " Les requêtes et les mémoires doivent, à peine d'irrecevabilité, être présentés soit par un avocat, soit par un avocat au Conseil d'Etat et à la Cour de cassation, lorsque les conclusions de la demande tendent au paiement d'une somme d'argent, à la décharge ou à la réduction de sommes dont le paiement est réclamé au requérant ou à la solution d'un litige né de l'exécution d'un contrat (...) ". Aux termes de l'article R. 811-7 du même code : " Sous réserve des dispositions de l'article L. 774-8, les appels ainsi que les mémoires déposés devant la cour administrative d'appel doivent être présentés, à peine d'irrecevabilité, par l'un des mandataires mentionnés à l'article R. 431-2 ". Aux termes de l'article 1984 du code civil : " Le mandat ou procuration est un acte par lequel une personne donne à une autre le pouvoir de faire quelque chose pour le mandant et en son nom (...) ". <br/>
<br/>
              4. Ces dispositions relatives au mandat impliquent, en principe, que l'avocat soit une personne distincte du requérant, dont les intérêts personnels ne sont pas en cause dans l'affaire, faisant ainsi obstacle à ce qu'un requérant exerçant la profession d'avocat puisse assurer sa propre représentation au titre de l'article R. 431-2 du code de justice administrative dans une instance à laquelle il est personnellement partie.<br/>
<br/>
              5. Il ressort des énonciations de l'ordonnance attaquée que la requête de Mme A..., contre le jugement du tribunal administratif qui a rejeté sa demande tendant, notamment, à l'annulation de la décision du 5 décembre 2014 prononçant son licenciement pour insuffisance professionnelle, a été présentée par elle-même devant la cour, en sa qualité d'avocate inscrite au barreau de Chambéry à compter du 14 mars 2016 et ayant choisi d'exercer sous la forme d'une société d'exercice libéral à responsabilité limitée unipersonnelle, et non par l'un des mandataires mentionnés à l'article R. 431-2 du code de justice administrative. C'est sans erreur de droit, et par une ordonnance suffisamment motivée, que le président de la troisième chambre de la cour en a déduit, sur le fondement des dispositions précitées et eu égard à l'objet du litige, que la requête d'appel était manifestement irrecevable et l'a rejetée sur le fondement du 4° de l'article R. 222-1 du code de justice administrative.  <br/>
<br/>
              6. En second lieu, aux termes de l'article 6, paragraphe 3, de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales : " Tout accusé a droit notamment à (...) c) se défendre lui-même ou avoir l'assistance d'un défenseur de son choix et, s'il n'a pas les moyens de rémunérer un défenseur, pouvoir être assisté gratuitement par un avocat d'office, lorsque les intérêts de la justice l'exigent ". Le moyen tiré de ce que l'ordonnance attaquée aurait méconnu ces dispositions ne peut qu'être écarté, dès lors qu'elles ne s'appliquent qu'en matière pénale et que le litige sur lequel il a été statué ne relevait pas de cette matière.<br/>
<br/>
              7. Il résulte de ce qui précède que Mme A...n'est pas fondée à demander l'annulation de l'ordonnance qu'elle attaque. Par suite, ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative doivent être rejetées. <br/>
<br/>
              8. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de Mme A... la somme de 1 000 euros à verser à la communauté de communes Serein et Armance, au titre des dispositions de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de Mme A...est rejeté. <br/>
Article 2 : Mme A...versera la somme de 1 000 euros à la communauté de communes Serein et Armance au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : Les conclusions présentée par Mme A...au titre de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
Article 4 : La présente décision sera notifiée à Mme B...A...et à la communauté de communes Serein et Armance. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
