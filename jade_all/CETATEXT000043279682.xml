<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043279682</ID>
<ANCIEN_ID>JG_L_2021_03_000000437180</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/27/96/CETATEXT000043279682.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème chambre, 22/03/2021, 437180, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-03-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>437180</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP NICOLAY, DE LANOUVELLE, HANNOTIN</AVOCATS>
<RAPPORTEUR>M. Hervé Cassagnabère</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Romain Victor</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:437180.20210322</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société par actions simplifiée (SAS) Carrefour Hypermarchés a demandé au tribunal administratif de Bordeaux de prononcer la décharge des cotisations de taxe foncière sur les propriétés bâties auxquelles elle a été assujettie au titre des années 2013 et 2014 à raison d'un établissement situé à Bègles (Gironde). Par un jugement n° 1600448 du 21 septembre 2017, ce tribunal a rejeté cette demande.  <br/>
<br/>
              Par une décision n° 415893 du 17 octobre 2018, le Conseil d'Etat, statuant au contentieux a annulé ce jugement et renvoyé l'affaire au tribunal administratif de Bordeaux. <br/>
<br/>
              Par un jugement n° 1900322 du 31 octobre 2019, ce tribunal a rejeté la demande présentée par la société Carrefour Hypermarchés.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 27 décembre 2019 et 27 mars 2020 au secrétariat du contentieux du Conseil d'Etat, la société Carrefour Hypermarchés demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
- le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Hervé Cassagnabère, conseiller d'Etat,<br/>
<br/>
              - les conclusions de M. Romain Victor, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Nicolaÿ, de Lanouvelle, Hannotin, avocat de la société Carrefour Hypermarchés ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. La société Carrefour Hypermarchés est propriétaire de locaux au sein du centre commercial " Les Rives d'Arcins " à Bègles (Gironde), à raison desquels elle a été assujettie à la taxe foncière sur les propriétés bâties au titre des années 2013 et 2014. Ces locaux constituent le local-type n°46 du procès-verbal d'évaluation foncière complémentaire n°4 de la commune de Bègles, créé le 5 mars 1996 pour représenter la catégorie des hypermarchés. La valeur locative de cet immeuble a elle-même été déterminée par référence au local-type n°43 figurant sur le procès-verbal complémentaire n° 2 du 18 décembre 1973 de la commune de Bègles, qui correspondait à un supermarché " Mammouth " construit en 1968. La société Carrefour Hypermarchés se pourvoit en cassation contre le jugement du 31 octobre 2019 par lequel le tribunal administratif de Bordeaux, sur renvoi du Conseil d'Etat, statuant au contentieux à la suite d'une décision du 17 octobre 2018 ayant annulé son premier jugement du 2 septembre 2017, a rejeté sa demande tendant à la décharge des impositions auxquelles elle a été assujettie au titre de ces années.<br/>
<br/>
              2. Pour écarter le moyen tiré de l'irrégularité de l'évaluation de l'immeuble litigieux par application de la méthode par comparaison du b du 2° de l'article 1498 du code général des impôts, le tribunal administratif s'est fondé sur ce que le local-type n° 43 était donné en location au 1er janvier 1970. En retenant l'existence d'un bail au 1er janvier 1970 au seul motif qu'il résultait de l'instruction que l'occupant de ce local était une personne autre que son propriétaire et alors que le procès-verbal complémentaire de 1973 portait, s'agissant de cet immeuble, la mention " code-bien 32 ", qui correspond, ainsi que le précise le " bulletin de renseignements " produit par l'administration, à l'application à la surface pondérée d'une valeur unitaire du " type de référence ", c'est-à-dire à une évaluation par voie de comparaison, le tribunal administratif a dénaturé les faits et les pièces du dossier. <br/>
<br/>
              3. Il résulte de ce qui précède, sans qu'il soit besoin de se prononcer sur les autres moyens du pourvoi, que la société Carrefour Hypermarchés est fondée à demander l'annulation du jugement qu'elle attaque.<br/>
<br/>
              4. Aux termes de l'article L. 821-2 du code de justice administrative : " Lorsque l'affaire fait l'objet d'un second pourvoi en cassation, le Conseil d'Etat statue définitivement sur cette affaire ". Le Conseil d'Etat étant saisi d'un second pourvoi en cassation, il lui incombe de régler l'affaire au fond.<br/>
<br/>
              5. Aux termes de l'article 1498 du code général des impôts : " La valeur locative de tous les biens autres que les locaux d'habitation ou à usage professionnel visés au I de l'article 1496 et que les établissements industriels visés à l'article 1499 est déterminée au moyen de l'une des méthodes indiquées ci-après : / (...) 2° a. Pour les biens loués à des conditions de prix anormales ou occupés par leur propriétaire, occupés par un tiers à un autre titre que la location, vacants ou concédés à titre gratuit, la valeur locative est déterminée par comparaison. / Les termes de comparaison sont choisis dans la commune. Ils peuvent être choisis hors de la commune pour procéder à l'évaluation des immeubles d'un caractère particulier ou exceptionnel ; / b. La valeur locative des termes de comparaison est arrêtée : / Soit en partant du bail en cours à la date de référence de la révision lorsque l'immeuble type était loué normalement à cette date, / Soit, dans le cas contraire, par comparaison avec des immeubles similaires situés dans la commune ou dans une localité présentant, du point de vue économique, une situation analogue à celle de la commune en cause et qui faisaient l'objet à cette date de locations consenties à des conditions de prix normales ; (...) ". <br/>
<br/>
              6. En premier lieu, il résulte de ce qui a été dit au point 2 que le local-type n°43 figurant sur le procès-verbal complémentaire n° 2 du 18 décembre 1973, qui constitue le terme de comparaison retenu pour déterminer la valeur locative de l'immeuble en litige, n'a pas été évalué à partir d'un bail en vigueur au 1er janvier 1970, mais par voie de comparaison. Toutefois, les éléments du dossier ne permettent pas de déterminer les termes de comparaisons utilisés et, par suite, de s'assurer de la régularité de cette évaluation. Ce local-type ne peut, par suite et contrairement à ce que soutient le ministre, être régulièrement retenu pour évaluer l'immeuble en litige.<br/>
<br/>
              7. En deuxième lieu, il résulte de l'instruction que, contrairement à ce que soutient la société requérante, les communes de Bègles et d'Auch ne présentent pas, eu égard à la taille de leur population, à leur tissu économique respectif et au fait que Bègles est située dans la périphérie immédiate de Bordeaux, une situation analogue d'un point de vue économique. Par suite, le local-type n° 62 de la commune d'Auch, proposé par la société requérante, ne saurait être retenu comme terme de comparaison.<br/>
<br/>
              8. En troisième lieu, l'administration fiscale soutient, sans être contredite, que local-type n° 3 du procès-verbal des évaluations foncières de la commune de Bègles, également proposé par la société requérante, qui ne correspond pas à un hypermarché, ne présente pas des caractéristiques similaires à celles de l'établissement à évaluer, qui comporte une galerie marchande de 150 boutiques. Il ne peut davantage être retenu comme terme de comparaison pertinent pour déterminer la valeur locative des locaux litigieux.<br/>
<br/>
              9. Enfin, il ne résulte pas de l'instruction que le local-type n° 54 du procès-verbal de la commune de Bègles, également proposé par la société requérante, qui correspond à un local-type plus petit et plus ancien que celui à évaluer, constituerait un terme de comparaison adapté.<br/>
<br/>
              10. Il résulte de ce qui précède que ni l'administration ni la société requérante n'ayant été en mesure de proposer, tant au sein de la commune de Bègles que dans d'autres communes présentant une situation économique analogue, de terme de comparaison satisfaisant aux conditions prévues par les dispositions du 2° de l'article 1498 du code général des impôts, il y a lieu d'ordonner à l'administration de produire devant le Conseil d'Etat, contradictoirement avec la société requérante, un terme de comparaison adéquat ou, à défaut, les éléments permettant de procéder à l'évaluation directe de l'immeuble en litige en application des dispositions du 3° de l'article 1498 du code général des impôts et dont les modalités d'application sont précisées aux articles 324 AB et 324 AC de l'annexe III à ce code. <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement n° 1900322 du 31 octobre 2019 du tribunal administratif de Bordeaux est annulé. <br/>
Article 2 : Avant de statuer sur la demande de la société Carrefour Hypermarchés relative aux années 2013 et 2014, il sera procédé, contradictoirement, à la mesure d'instruction définie au point 10 de la présente décision. <br/>
Article 3 : La présente décision sera notifiée à la société par actions simplifiée Carrefour Hypermarchés et au ministre de l'économie, des finances et de la relance. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
