<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000025893490</ID>
<ANCIEN_ID>JG_L_2012_05_000000325876</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/25/89/34/CETATEXT000025893490.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème et 9ème sous-sections réunies, 16/05/2012, 325876, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2012-05-16</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>325876</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème et 9ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Philippe Martin</PRESIDENT>
<AVOCATS>SCP COUTARD, MUNIER-APAIRE</AVOCATS>
<RAPPORTEUR>M. Jean-Claude Hassan</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Delphine Hedary</COMMISSAIRE_GVT>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 9 mars et 5 juin 2009 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. Noël A, demeurant ... ; M. A demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 07VE01950 du 11 décembre 2008 par lequel la cour administrative d'appel de Versailles a rejeté sa requête tendant à l'annulation du jugement du 21 juin 2007 du tribunal administratif de Versailles rejetant sa demande tendant à la décharge des cotisations supplémentaires d'impôt sur le revenu et de contributions sociales auxquelles il a été assujetti au titre des années 2000 à 2004 et des pénalités dont elles ont été assorties ;<br/>
<br/>
              2°) réglant l'affaire au fond, de prononcer la décharge de ces impositions ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code civil ;<br/>
<br/>
              Vu le code général des impôts ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Claude Hassan, Conseiller d'Etat,<br/>
<br/>
              - les observations de la SCP Coutard, Munier-Apaire, avocat de M. A,<br/>
<br/>
              - les conclusions de Mme Delphine Hedary, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Coutard, Munier-Apaire, avocat de M. A ;<br/>
<br/>
<br/>
<br/>
<br/>Sans qu'il soit besoin d'examiner les autres moyens du pourvoi ;<br/>
<br/>
              Considérant que les articles 205 et 207 du code civil disposent respectivement que " Les enfants doivent des aliments à leurs père et mère ou autres ascendants qui sont dans le besoin " et que " Les obligations résultant de ces dispositions sont réciproques " ; qu'aux termes de l'article 156 du code général des impôts, dans sa rédaction applicable aux années d'imposition en litige, le " revenu net est déterminé (...) sous déduction : / (...) II. Des charges ci-après lorsqu'elles n'entrent pas en compte pour l'évaluation des revenus des différentes catégories : (...) / 2° (...) pensions alimentaires répondant aux conditions fixées par les articles 205 à 211 (...) du code civil (...), pensions alimentaires versées en vertu d'une décision de justice (...) / La déduction est limitée, par enfant majeur, au montant fixé pour l'abattement prévu par l'article 196 B (...) " ;<br/>
<br/>
              Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que le jugement de divorce prononcé le 30 septembre 1997 par le tribunal de grande instance d'Evry a notamment pris en compte la circonstance non contestée que Mme B assumait seule la charge du fils majeur handicapé né de son union avec M. A ; que les époux ont conservé, après le divorce, la propriété indivise et à parts égales du bien immobilier qu'ils avaient acquis ensemble pendant leur union ; que, par une convention d'indivision signée en 1998 pour cinq ans, qui a ensuite été renouvelée, ils sont convenus que Mme B percevrait l'intégralité des loyers de ce bien immobilier dont elle supporterait l'intégralité des charges, en contrepartie de ce qu'elle assumait seule l'entretien de leur fils handicapé ; que, dans les circonstances de l'espèce, M. A pouvait, en abandonnant ainsi à son ancienne épouse la part de loyer lui revenant, être regardé comme s'acquittant de l'obligation d'aliments née des articles 205 et 207 du code civil, dont le handicap du bénéficiaire pouvait justifier qu'elle donne lieu au transit des sommes correspondantes par le revenu de Mme B qui constituait avec son fils un même foyer fiscal pour l'impôt sur le revenu ;<br/>
<br/>
              Considérant qu'il résulte de ce qui précède que la cour administrative d'appel de Versailles a inexactement qualifié les faits de l'espèce en jugeant que les sommes complémentaires perçues directement par Mme B au-delà de ses propres droits dans l'indivision constituée avec M. A ne pouvaient être regardées comme ayant le caractère d'une pension alimentaire destinée à son fils handicapé rattaché à son foyer fiscal et répondant aux conditions fixées par les articles 205 à 211 du code civil ; que, par suite, M. A est fondé, pour ce motif, à demander l'annulation de l'arrêt attaqué ; <br/>
<br/>
              Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat le versement à M. A de la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>				D E C I D E  :<br/>
              				--------------<br/>
<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Versailles du 11 décembre 2008 est annulé.<br/>
<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Versailles.<br/>
Article 3 : L'Etat versera à M. A la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à M. Noël A et à la ministre du budget, des comptes publics et de la réforme de l'Etat, porte-parole du Gouvernement.<br/>
<br/>
</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
