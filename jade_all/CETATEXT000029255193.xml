<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029255193</ID>
<ANCIEN_ID>JG_L_2014_07_000000362230</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/25/51/CETATEXT000029255193.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème / 8ème SSR, 16/07/2014, 362230</TITRE>
<DATE_DEC>2014-07-16</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>362230</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème / 8ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP WAQUET, FARGE, HAZAN ; SCP GARREAU, BAUER-VIOLAS, FESCHOTTE-DESBOIS</AVOCATS>
<RAPPORTEUR>M. Christian Fournier</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Vincent Daumas</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:362230.20140716</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu l'ordonnance n° 12DA00909 du 22 août 2012, enregistrée le 28 août 2012 au secrétariat du contentieux du Conseil d'Etat, par laquelle le président de la cour administrative d'appel de Douai a transmis au Conseil d'Etat, en application de l'article R. 351-2 du code de justice administrative, le pourvoi présenté à cette cour par Mme B...A...;<br/>
<br/>
              Vu le pourvoi, enregistré le 21 juin 2012 au greffe de la cour administrative d'appel de Douai, et les mémoires complémentaires, enregistrés les 30 novembre 2012 et 4 novembre 2013 au secrétariat du contentieux du Conseil d'Etat, présentés pour Mme A..., demeurant au... ; Mme A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'ordonnance n° 1101698 du 20 avril 2012 par laquelle le président du tribunal administratif d'Amiens a jugé qu'il n'y avait pas lieu de liquider l'astreinte de 200 euros par jour de retard ordonnée par le jugement du 10 février 2012 enjoignant à la commune de Villers-Cotterêts de la réintégrer dans ses fonctions de responsable du service de restauration administrative et scolaire dans un délai d'un mois à compter de la notification du jugement ;<br/>
<br/>
              2°) réglant l'affaire au fond, de condamner la commune de Villers-Cotterêts, d'une part, à lui verser la somme de 20 000 euros au titre de la liquidation de l'astreinte prononcée le 10 février 2012, en y ajoutant la somme de 200 euros par jour de retard jusqu'à ce que la commune justifie pleinement de sa réintégration dans ses fonctions et, d'autre part, à la réintégrer dans ses fonctions sous astreinte de 1 000 euros par jour de retard au delà du délai d'un mois à compter de la notification de la décision ;<br/>
<br/>
              3°) de mettre à la charge de la commune de Villers-Cotterêts la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Christian Fournier, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Vincent Daumas, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Waquet, Farge, Hazan, avocat de Mme B...A...et à la SCP Garreau, Bauer-Violas, Feschotte-Desbois, avocat de la commune de Villers-Cotterêts ;<br/>
<br/>
<br/>
<br/>
              1. Considérant que MmeA..., adjointe technique territoriale de 2ème classe en fonction auprès de la commune de Villers-Cotterêts, se pourvoit en cassation contre l'ordonnance du 20 avril 2012, prise sur le fondement de l'article R. 222-1 du code de justice administrative, par laquelle le président du tribunal administratif d'Amiens, après avoir relevé que l'intéressée avait été réintégrée dans ses fonctions et en avoir conclu que le jugement du 10 février 2012 de ce tribunal enjoignant à la commune de Villers-Cotterêts sous astreinte de 200 euros par jour de retard à compter de la notification du jugement  de la réintégrer, a décidé qu'il n'y avait pas lieu de liquider l'astreinte ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article R. 222-1 du même code : " Les présidents de tribunal administratif et de cour administrative d'appel, le vice-président du tribunal administratif de Paris et les présidents de formation de jugement des tribunaux et des cours peuvent, par ordonnance : (...) / 3° Constater qu'il n'y a pas lieu de statuer sur une requête. " ; <br/>
<br/>
              3. Considérant que la décision par laquelle le juge de l'exécution se prononce sur la liquidation d'une astreinte s'inscrit dans la même instance contentieuse que celle dans laquelle a été prononcée l'injonction dont elle est un accessoire ; que dès lors que le juge de l'exécution a constaté l'exécution de la mesure prescrite, il lui appartient, même d'office, selon le cas, de se prononcer sur la liquidation de l'astreinte en constatant, le cas échéant, qu'il n'y a pas lieu d'y procéder ; que le président du tribunal administratif d'Amiens, statuant comme juge de l'exécution, n'a pas commis d'erreur de droit en constatant par ordonnance prise sur le fondement des dispositions citées au point 2 que la mesure prescrite avait été entièrement exécutée et qu'il n'y avait pas lieu de procéder à la liquidation de l'astreinte ; <br/>
<br/>
              4. Considérant, en deuxième lieu, qu'aux termes de l'article R. 742-6 du code de justice administrative : " Les ordonnances ne sont pas prononcées en audience publique " ; qu'il résulte de ces dispositions que le président du tribunal administratif d'Amiens, statuant en application des dispositions du 3° de l'article R. 222-1 du code de justice administrative, n'a pas commis d'irrégularité en prenant l'ordonnance attaquée sans avoir tenu d'audience publique ; <br/>
<br/>
              5. Considérant, en troisième lieu, que pour décider qu'il n'y avait pas lieu de liquider l'astreinte ordonnée par le jugement du 10 février 2012, le président du tribunal administratif d'Amiens a relevé qu'il résultait d'une fiche de poste, validée le 27 mars 2012 tant par le maire de la commune de Villers-Cotterêts que par l'intéressée, que Mme A...avait été réintégrée dans ses fonctions de responsable du service de la restauration administrative et scolaire de cette commune ; qu'il a ainsi suffisamment motivé son ordonnance ;<br/>
<br/>
              6. Considérant, en quatrième lieu, que le président du tribunal administratif d'Amiens, en se reférant au contenu de la fiche de poste du 27 mars 2012 pour juger que Mme A... avait été réintégrée dans ses fonctions de responsable du service de la restauration administrative et scolaire de la commune de Villers-Cotterêts, n'a commis aucune dénaturation des faits ; que le président du tribunal administratif d'Amiens, qui n'a pas constaté un défaut manifeste d'équivalence entre l'emploi occupé par Mme A...avant son éviction et celui dans lequel elle a été réintégrée, n'a commis aucune erreur de droit en jugeant qu'il n'y avait pas lieu à liquider l'astreinte ordonnée par le jugement du 10 février 2012 ;<br/>
<br/>
              7. Considérant, en dernier lieu, qu'en jugeant qu'il résultait de la fiche de poste du 27 mars 2012 que Mme A...a été réintégrée dans ses fonctions de responsable du service de la restauration administrative et scolaire de cette commune et que le jugement du 10 février 2012 avait ainsi été entièrement exécuté, le président du tribunal administratif d'Amiens n'a pas commis d'erreur de droit et s'est livré, sans les dénaturer, à une appréciation souveraine des circonstances de l'espèce, qui ne peut être discutée devant le juge de cassation ;<br/>
<br/>
              8. Considérant qu'il résulte de tout ce qui précède que Mme A... n'est pas fondée à demander l'annulation de l'ordonnance qu'elle attaque ;<br/>
<br/>
              9. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de Mme A...la somme de 3 000 euros à verser à la commune de Villers-Cotterêts, au titre des dispositions de l'article L. 761-1 du code de justice administrative ; qu'en revanche, ces dispositions font obstacle à ce qu'une somme soit mise à la charge de la commune de Villers-Cotterêts qui n'est pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de Mme A...est rejeté.<br/>
Article 2 : Mme A...versera à la commune de Villers-Cotterêts la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée à Mme B... A...et à la commune de Villers-Cotterêts.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-06-03 PROCÉDURE. JUGEMENTS. COMPOSITION DE LA JURIDICTION. - JUGE DE L'EXÉCUTION - PRÉSIDENT DU TA STATUANT PAR ORDONNANCE (ART. R. 222-1 DU CJA) - FACULTÉ DE CONSTATER QU'IL N'Y A PAS LIEU DE LIQUIDER UNE ASTREINTE - EXISTENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-06-07-01-04 PROCÉDURE. JUGEMENTS. EXÉCUTION DES JUGEMENTS. ASTREINTE. LIQUIDATION DE L'ASTREINTE. - FACULTÉ DU PRÉSIDENT DU TA DE CONSTATER PAR ORDONNANCE QUE LA MESURE A ÉTÉ EXÉCUTÉE ET QU'IL N'Y A PAS LIEU DE LIQUIDER L'ASTREINTE - EXISTENCE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">54-07-01 PROCÉDURE. POUVOIRS ET DEVOIRS DU JUGE. QUESTIONS GÉNÉRALES. - PRÉSIDENT DU TA - POUVOIR DE STATUER PAR ORDONNANCE - CONSTATATION QU'IL N'Y A PAS LIEU DE STATUER SUR UNE REQUÊTE (ART. R. 222-1 DU CJA) - INCLUSION - JUGE DE L'EXÉCUTION - CONSTATATION QU'IL N'Y A PAS LIEU DE LIQUIDER UNE ASTREINTE.
</SCT>
<ANA ID="9A"> 54-06-03 Le président du tribunal administratif (TA), statuant comme juge de l'exécution, peut constater par ordonnance prise sur le fondement du 3° de l'article R. 222-1 du code de justice administrative (CJA) que la mesure prescrite a été entièrement exécutée et qu'il n'y a pas lieu de procéder à la liquidation de l'astreinte.</ANA>
<ANA ID="9B"> 54-06-07-01-04 Le président du tribunal administratif (TA), statuant comme juge de l'exécution, peut constater par ordonnance prise sur le fondement du 3° de l'article R. 222-1 du code de justice administrative que la mesure prescrite a été entièrement exécutée et qu'il n'y a pas lieu de procéder à la liquidation de l'astreinte.</ANA>
<ANA ID="9C"> 54-07-01 Le président du tribunal administratif (TA), statuant comme juge de l'exécution, peut constater par ordonnance prise sur le fondement du 3° de l'article R. 222-1 du code de justice administrative (CJA) que la mesure prescrite a été entièrement exécutée et qu'il n'y a pas lieu de procéder à la liquidation de l'astreinte.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
