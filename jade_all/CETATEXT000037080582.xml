<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037080582</ID>
<ANCIEN_ID>JG_L_2018_06_000000412074</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/08/05/CETATEXT000037080582.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème et 6ème chambres réunies, 18/06/2018, 412074</TITRE>
<DATE_DEC>2018-06-18</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>412074</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème et 6ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BARADUC, DUHAMEL, RAMEIX ; SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR>M. Alain Seban</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Laurence Marion</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:412074.20180618</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Par une requête et deux mémoires en réplique, enregistrés au secrétariat du contentieux du Conseil d'Etat le 3 juillet 2017 et les 21 février et 18 mai 2018, la société C8 demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler la décision n° 2017-298 du 7 juin 2017 par laquelle le Conseil supérieur de l'audiovisuel lui a infligé la sanction de la suspension de la diffusion des séquences publicitaires au sein de l'émission " Touche pas à mon poste " et de celles diffusées pendant les quinze minutes qui précèdent et les quinze minutes qui suivent la diffusion de cette émission pendant une durée d'une semaine ;<br/>
<br/>
              2°) de mettre à la charge du Conseil supérieur de l'audiovisuel une somme de 10 000 euros au titre de l'article L.761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
<br/>
              - la Constitution, notamment son Préambule ;<br/>
<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
<br/>
              - la loi n° 86-1067 du 30 septembre 1986<br/>
<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Alain Seban, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Laurence Marion, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Piwnica, Molinié, avocat de la société C8  et à la SCP Baraduc, Duhamel, Rameix, avocat du Conseil supérieur de l'audiovisuel.<br/>
<br/>
              Vu la note en délibéré, enregistrée le 30 mai 2018 présentée par le Conseil supérieur de l'audiovisuel ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant, d'une part, qu'aux termes du premier alinéa  de l'article 42 de la loi du 30 septembre 1986 relative à la liberté de communication : " Les éditeurs et distributeurs de services de communication audiovisuelle et les opérateurs de réseaux satellitaires peuvent être mis en demeure de respecter les obligations qui leur sont imposées par les textes législatifs et réglementaires et par les principes définis aux articles 1er et 3-1 " ; qu'aux termes de l'article 42-1 de la même loi : " Si la personne faisant l'objet de la mise en demeure ne se conforme pas à celle-ci, le Conseil supérieur de l'audiovisuel peut prononcer à son encontre, compte tenu de la gravité du manquement, et à la condition que celui-ci repose sur des faits distincts ou couvre une période distincte de ceux ayant déjà fait l'objet d'une mise en demeure, une des sanctions suivantes : / 1° La suspension de l'édition, de la diffusion ou de la distribution du ou des services d'une catégorie de programme, d'une partie du programme, ou d'une ou plusieurs séquences publicitaires pour un mois au plus ; (...) " ; <br/>
<br/>
              2. Considérant, d'autre part, qu'aux termes de l'article 4-2-2 de la convention relative au service de télévision " Direct 8 ", devenu C8, conclue le 10 juin 2003 entre le Conseil supérieur de l'audiovisuel (CSA) et la société Bolloré Médias, aux droits de laquelle est venue la société C8, sur le fondement de l'article 28 de la loi du 30 septembre 1986, le conseil supérieur peut, si l'éditeur ne se conforme pas aux mises en demeure de respecter les obligations prévues par cette convention, " compte tenu de la gravité du manquement, prononcer l'une des sanctions suivantes : (...) 2° la suspension pour un mois au plus de l'édition, de la diffusion ou de la distribution du service, d'une catégorie de programme, d'une partie du programme ou d'une ou plusieurs séquences publicitaires (...) " ; que l'article 4-2-4 de la convention prévoit que les sanctions mentionnées à ses articles 4-2-2 et 4-2-3 sont prononcées dans le respect des garanties fixées par les articles 42 et suivants de la loi du 30 septembre 1986 ;<br/>
<br/>
              3. Considérant que, par une décision adoptée lors de sa séance du 7 juin 2017, le CSA a estimé que des séquences diffusées le 3 novembre 2016 par le service de télévision C8 était constitutives d'un manquement aux stipulations de l'article 2-3-4 de la convention du 10 juin 2003 aux termes desquelles : " (...) L'éditeur s'engage à ce qu'aucune émission qu'il diffuse ne porte atteinte à la dignité de la personne humaine telle qu'elle est définie par la loi et la jurisprudence. (...) L'éditeur veille en particulier : à ce qu'il soit fait preuve de retenue dans la diffusion d'images ou de témoignages susceptibles d'humilier les personnes (...) " ; que, constatant que des faits de même nature avait donné lieu à une mise en demeure en date du 30 mars 2010, le conseil supérieur a décidé d'infliger à la société C8, en sa qualité d'éditeur du service, la sanction de l'interdiction de diffuser des séquences publicitaires, pendant une durée d'une semaine, au sein de l'émission en cause et pendant les quinze minutes précédant et suivant la diffusion de cette émission ; que la société C8 demande l'annulation de cette décision ; <br/>
<br/>
              4. Considérant qu'il résulte de l'instruction que, le 3 novembre 2016, au cours de l'émission " Touche pas à mon poste ", ont été diffusées plusieurs séquences tournées selon le principe courant de la " caméra cachée " mettant en scène l'animateur et des chroniqueurs de l'émission censés avoir été filmés à leur insu ; qu'en particulier, lors de la séquence ayant donné lieu à la sanction attaquée, l'animateur et le chroniqueur ont été montrés se rendant chez un tiers, en réalité un acteur se faisant passer pour un producteur américain avec lequel l'animateur venait négocier le passage dans l'émission d'un acteur vedette ; que, la discussion s'étant tendue, l'animateur et son comparse ont eu une altercation au cours de laquelle le second est tombé, apparemment inanimé ; que l'animateur et son garde du corps ont ensuite tenté de dissuader le chroniqueur d'appeler la police et de le contraindre à endosser la responsabilité de l'incident qui ne lui était cependant en rien imputable ; que le chroniqueur, qui a été présenté comme n'ayant été avisé que le lendemain qu'il s'agissait d'une mise en scène, est apparu, tout au moins initialement, déstabilisé par le comportement de l'animateur, mais faisant preuve de sang-froid, appelant la police, alors qu'il lui était demandé avec insistance de n'en rien faire, et se préoccupant à plusieurs reprises de l'état de la prétendue victime avec qui il a partagé un repas après qu'elle a repris ses esprits  ; qu'eu égard à son comportement tout au long de la séquence, il n'a pas été montré sous un jour dégradant, humiliant ou attentatoire à sa dignité ; que, dès lors, la diffusion de cette séquence, à laquelle le chroniqueur a consenti et qu'il a lui-même accepté de commenter, ne révèle, contrairement à ce qu'a estimé le CSA dans la décision attaquée et eu égard au caractère humoristique de l'émission et à la protection qui s'attache à la liberté d'expression en vertu des articles 11 de la déclaration des droits de l'homme et du citoyen du 26 août 1789 et 10 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, aucune méconnaissance des stipulations de l'article 2-3-4 de la convention du service C8 citées au point 3 ; que, dès lors, sans qu'il soit besoin d'examiner les autres moyens de la requête, la décision attaquée doit être annulée ;<br/>
<br/>
              5. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise, à ce titre, à la charge de la société C8 qui n'est pas, dans la présente instance, la partie perdante ; qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge du CSA la somme de 3 000 euros à verser à la société C8 au titre de ces dispositions.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : La décision du Conseil supérieur de l'audiovisuel n° 2017-298 du 7 juin 2017 est annulée.<br/>
Article 2 : Le Conseil supérieur de l'audiovisuel versera à la société C8 une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : Le surplus des conclusions de la requête de la société C8 est rejeté.<br/>
Article 4 : La présente décision sera notifiée à la société C8 et au Conseil supérieur de l'audiovisuel.<br/>
Copie en sera adressée à la ministre de la culture.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">56-01 RADIO ET TÉLÉVISION. CONSEIL SUPÉRIEUR DE L'AUDIOVISUEL. - SANCTION PRONONCÉE CONTRE LA SOCIÉTÉ C8 EN RAISON D'UNE SÉQUENCE DE L'ÉMISSION TOUCHE PAS À MON POSTE DU 3 NOVEMBRE 2016 - MÉCONNAISSANCE DE L'ARTICLE 2-3-4 DE LA CONVENTION DE SERVICE - ABSENCE - CONSÉQUENCE - ILLÉGALITÉ DE LA SANCTION [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">59-02-02-03 RÉPRESSION. DOMAINE DE LA RÉPRESSION ADMINISTRATIVE RÉGIME DE LA SANCTION ADMINISTRATIVE. BIEN-FONDÉ. - SANCTION PRONONCÉE PAR LE CSA À L'ENCONTRE LA SOCIÉTÉ C8 EN RAISON D'UNE SÉQUENCE DE L'ÉMISSION TOUCHE PAS À MON POSTE DU 3 NOVEMBRE 2016 - MÉCONNAISSANCE DE L'ARTICLE 2-3-4 DE LA CONVENTION DE SERVICE - ABSENCE - CONSÉQUENCE - ILLÉGALITÉ DE LA SANCTION [RJ1].
</SCT>
<ANA ID="9A"> 56-01 Le 3 novembre 2016, au cours de l'émission Touche pas à mon poste, ont été diffusées plusieurs séquences tournées selon le principe courant de la caméra cachée mettant en scène l'animateur et des chroniqueurs de l'émission censés avoir été filmés à leur insu. En particulier, lors de la séquence ayant donné lieu à la sanction attaquée, l'animateur et le chroniqueur ont été montrés se rendant chez un tiers, en réalité un acteur se faisant passer pour un producteur américain avec lequel l'animateur venait négocier le passage dans l'émission d'un acteur vedette. La discussion s'étant tendue, le producteur et son comparse ont eu une altercation au cours de laquelle le second est tombé, apparemment inanimé. L'animateur et son garde du corps ont ensuite tenté de dissuader le chroniqueur d'appeler la police et de le contraindre à endosser la responsabilité de l'incident qui ne lui était cependant en rien imputable. Le chroniqueur, qui a été présenté comme n'ayant été avisé que le lendemain qu'il s'agissait d'une mise en scène, est apparu, tout au moins initialement, déstabilisé par le comportement de l'animateur, mais faisant preuve de sang-froid, appelant la police, alors qu'il lui était demandé avec insistance de n'en rien faire, et se préoccupant à plusieurs reprises de l'état de la prétendue victime avec qui il a partagé un repas après qu'elle a repris ses esprits.... ,,Eu égard à son comportement tout au long de la séquence, il n'a pas été montré sous un jour dégradant, humiliant ou attentatoire à sa dignité. Dès lors, la diffusion de cette séquence, à laquelle le chroniqueur a consenti et qu'il a lui-même accepté de commenter, ne révèle, contrairement à ce qu'a estimé le Conseil supérieur de l'audiovisuel (CSA) et eu égard au caractère humoristique de l'émission et à la protection qui s'attache à la liberté d'expression en vertu des articles 11 de la Déclaration des droits de l'homme et du citoyen (DDHC) du 26 août 1789 et 10 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales (Conv. EDH), aucune méconnaissance des stipulations de l'article 2-3-4 de la convention du service C8. Par suite, annulation de la décision, prononcée par le CSA à l'encontre de la société C8, en sa qualité d'éditeur du service, d'interdiction de diffuser des séquences publicitaires, pendant une durée d'une semaine, au sein de l'émission en cause et pendant les quinze minutes précédant et suivant la diffusion de cette émission.</ANA>
<ANA ID="9B"> 59-02-02-03 Le 3 novembre 2016, au cours de l'émission Touche pas à mon poste, ont été diffusées plusieurs séquences tournées selon le principe courant de la caméra cachée mettant en scène l'animateur et des chroniqueurs de l'émission censés avoir été filmés à leur insu. En particulier, lors de la séquence ayant donné lieu à la sanction attaquée, l'animateur et le chroniqueur ont été montrés se rendant chez un tiers, en réalité un acteur se faisant passer pour un producteur américain avec lequel l'animateur venait négocier le passage dans l'émission d'un acteur vedette. La discussion s'étant tendue, le producteur et son comparse ont eu une altercation au cours de laquelle le second est tombé, apparemment inanimé. L'animateur et son garde du corps ont ensuite tenté de dissuader le chroniqueur d'appeler la police et de le contraindre à endosser la responsabilité de l'incident qui ne lui était cependant en rien imputable. Le chroniqueur, qui a été présenté comme n'ayant été avisé que le lendemain qu'il s'agissait d'une mise en scène, est apparu, tout au moins initialement, déstabilisé par le comportement de l'animateur, mais faisant preuve de sang-froid, appelant la police, alors qu'il lui était demandé avec insistance de n'en rien faire, et se préoccupant à plusieurs reprises de l'état de la prétendue victime avec qui il a partagé un repas après qu'elle a repris ses esprits.... ,,Eu égard à son comportement tout au long de la séquence, il n'a pas été montré sous un jour dégradant, humiliant ou attentatoire à sa dignité. Dès lors, la diffusion de cette séquence, à laquelle le chroniqueur a consenti et qu'il a lui-même accepté de commenter, ne révèle, contrairement à ce qu'a estimé le Conseil supérieur de l'audiovisuel (CSA) et eu égard au caractère humoristique de l'émission et à la protection qui s'attache à la liberté d'expression en vertu des articles 11 de la Déclaration des droits de l'homme et du citoyen (DDHC) du 26 août 1789 et 10 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales (Conv. EDH), aucune méconnaissance des stipulations de l'article 2-3-4 de la convention du service C8. Par suite, annulation de la décision, prononcée par le CSA à l'encontre de la société C8, en sa qualité d'éditeur du service, d'interdiction de diffuser des séquences publicitaires, pendant une durée d'une semaine, au sein de l'émission en cause et pendant les quinze minutes précédant et suivant la diffusion de cette émission.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. sol. contr. CE, décisions du même jour, Société C8, n°s 412071 et 414532, à mentionner aux Tables.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
