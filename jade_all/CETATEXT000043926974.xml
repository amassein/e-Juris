<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043926974</ID>
<ANCIEN_ID>JG_L_2021_08_000000432947</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/92/69/CETATEXT000043926974.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème chambre, 06/08/2021, 432947, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-08-06</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>432947</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>CABINET BRIARD</AVOCATS>
<RAPPORTEUR>Mme Coralie Albumazard</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Olivier  Fuchs</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:432947.20210806</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société Parc éolien de l'Aire a demandé au tribunal administratif de Nancy d'annuler pour excès de pouvoir l'arrêté du 28 novembre 2014 par lequel le préfet de la Meuse a refusé de lui délivrer des permis de construire portant sur la réalisation, dans les communes de Ville-sur-Cousances, Julvécourt, Ippécourt et Autrécourt-sur-Aire, d'un ensemble de dix-sept éoliennes et de sept postes de livraison. Par un jugement n° 1500323 du 7 juin 2016, le tribunal administratif a rejeté sa demande. <br/>
<br/>
              Par un arrêt n° 16NC01755 du 8 juin 2017, la cour administrative d'appel de Nancy a, sur appel de la société Parc éolien de l'Aire, annulé le jugement du tribunal administratif de Nancy du 7 juin 2016 et l'arrêté du préfet de la Meuse du 28 novembre 2014. <br/>
<br/>
              Par une décision n° 413136 du 25 juin 2018, le Conseil d'Etat statuant au contentieux, a, sur le pourvoi du ministre de la cohésion des territoires, annulé l'arrêt de la cour administrative d'appel de Nancy du 8 juin 2017 et renvoyé l'affaire devant cette même cour.<br/>
<br/>
              Par un arrêt n° 18NC01852 du 28 mai 2019, la cour administrative d'appel de Nancy a rejeté l'appel formé par la société Parc éolien de l'Aire contre le jugement du tribunal administratif.  <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 24 juillet et 23 octobre 2019 au secrétariat du contentieux du Conseil d'Etat, la société Parc éolien de l'Aire demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'urbanisme ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Coralie Albumazard, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Olivier Fuchs, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, au Cabinet Briard, avocat de la société Parc éolien de l'Aire ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que la société Parc éolien de l'Aire a sollicité, le 6 janvier 2014, la délivrance de quatre permis de construire pour l'implantation de dix-sept éoliennes et sept postes de livraison sur le territoire des communes de Ville-sur-Cousances, Julvécourt, Autrecourt-sur-Aire et Ippécourt, dans la Meuse. Le préfet de la Meuse a, par un arrêté unique du 28 novembre 2014, rejeté ces demandes. Par un jugement du 7 juin 2016, le tribunal administratif de Nancy a rejeté la demande de la société Parc éolien de l'Aire tendant à l'annulation de cet arrêté. Par un arrêt du 8 juin 2017, la cour administrative d'appel de Nancy a annulé ce jugement et l'arrêté du préfet. Saisi d'un pourvoi en cassation présenté par le ministre de la cohésion des territoires, le Conseil d'Etat, statuant au contentieux, a, par une décision du 25 juin 2018, annulé cet arrêt et renvoyé l'affaire à la cour administrative d'appel de Nancy. Celle-ci, statuant sur ce renvoi, a refusé de donner acte à la société Parc éolien de l'Aire de son désistement partiel de ses demandes tendant à l'annulation des permis de construire s'agissant de douze éoliennes et a rejeté son appel tendant à l'annulation partielle du jugement attaqué, en tant, seulement, qu'il rejette les conclusions d'annulation du refus de permis de construire portant sur les cinq éoliennes situées le plus au nord du site du projet. La société Parc éolien de l'Aire se pourvoit en cassation contre cet arrêt. <br/>
<br/>
              2. Le juge administratif, lorsqu'il est saisi de conclusions tendant à l'annulation partielle d'un acte dont les dispositions sont indivisibles, est tenu de rejeter ces conclusions comme irrecevables, quels que soient les moyens invoqués contre la décision attaquée.<br/>
<br/>
              3. Aux termes de l'article L. 421-6 du code de l'urbanisme, dans sa rédaction applicable : " Le permis de construire ne peut être accordé que si les travaux projetés sont conformes aux normes de fond résultant des dispositions législatives et réglementaires relatives à l'utilisation des sols, à l'implantation, la destination, la nature, l'architecture, les dimensions, l'assainissement des constructions et à l'aménagement de leurs abords (...) ". Il résulte de ces dispositions qu'une construction constituée de plusieurs éléments formant, en raison des liens physiques ou fonctionnels entre eux, un ensemble immobilier unique, doit en principe faire l'objet d'un seul permis de construire. <br/>
<br/>
              4. Le permis de construire a pour seul objet de s'assurer de la conformité des travaux qu'il autorise avec la législation et la réglementation d'urbanisme. Il suit de là que, lorsque deux constructions sont distinctes, la seule circonstance que l'une ne pourrait fonctionner ou être exploitée sans l'autre, au regard de considérations d'ordre technique ou économique et non au regard des règles d'urbanisme, ne suffit pas à caractériser un ensemble immobilier unique. <br/>
<br/>
              5. Pour estimer que la décision du préfet présentait un caractère indivisible, la cour administrative d'appel s'est fondée sur la circonstance que l'ensemble du projet a fait l'objet d'une instruction globale. Il résulte de ce qui précède qu'en se fondant sur ce seul élément pour en déduire que la décision de refus présentait un caractère indivisible, la cour a commis une erreur de droit. Dès lors, sans qu'il soit besoin de se prononcer sur l'autre moyen soulevé, la société Parc éolien de l'Aire est fondée à demander l'annulation de l'arrêt qu'elle attaque. <br/>
<br/>
              6. L'affaire faisant l'objet d'un second recours en cassation, il y a lieu de la régler au fond par application des dispositions du deuxième alinéa de l'article L. 821-2 du code de justice administrative aux termes desquelles : " Lorsque l'affaire fait l'objet d'un second pourvoi en cassation, le Conseil d'Etat statue définitivement sur cette affaire ". <br/>
<br/>
              7. Il résulte de ce qui a été dit au point 5 que le refus de permis de construire opposé à l'ensemble du projet constitue une décision à caractère divisible. Par suite,  d'une part, il doit être donné acte du désistement partiel des conclusions de la société Parc éolien de l'Aire en tant qu'elles tendent à l'annulation du refus de permis de construire portant sur douze éoliennes et, d'autre part, la société Parc éolien de l'Aire est recevable à demander, dans le dernier état de ses écritures d'appel, l'annulation partielle de l'arrêté du 28 novembre 2014 en tant, seulement, qu'il refuse un permis de construire portant sur les cinq éoliennes situées le plus au nord du site. <br/>
<br/>
              8. Aux termes de l'article R. 425-9 du code de l'urbanisme, " Lorsque le projet porte sur une construction susceptible, en raison de son emplacement et de sa hauteur, de constituer un obstacle à la navigation aérienne, le permis de construire ou le permis d'aménager tient lieu de l'autorisation prévue par l'article R. 244-1 du code de l'aviation civile dès lors que la décision a fait l'objet d'un accord du ministre chargé de l'aviation civile et du ministre de la défense. "<br/>
<br/>
              9. Il ressort des pièces du dossier que le ministre de la défense, qui relève dans son avis du 27 mars 2014 que " compte tenu de l'existence de parcs éoliens importants dans ce secteur constituant des obstacles massifs et compte tenu que ce tronçon, lorsqu'il est activé, est à contournement obligatoire pour tout trafic situé à l'extérieur, le projet serait de nature à compromettre ou empêcher le transit sous le RTBA en toute sécurité aux aéronefs volant à vue selon les règles des circulations aériennes civiles ou militaires (CAG ou CAM) ", a entendu s'opposer à l'implantation de toute éolienne supplémentaire dans le secteur concerné par le projet. Il résulte des dispositions précitées de l'article R. 425-9 du code de l'urbanisme que le préfet était dès lors, pour ce motif, tenu de rejeter l'ensemble des demandes de la requérante. Par suite, et sans qu'il soit besoin d'examiner les moyens présentés par la société Parc éolien de l'Aire, tirés de l'illégalité du refus préfectoral en tant qu'il concerne cinq des dix-sept éoliennes initialement projetées, les conclusions d'appel dirigées contre le jugement du tribunal administratif doivent être rejetées. <br/>
<br/>
              10. Il résulte de ce qui précède que la société Parc éolien de l'Aire n'est pas fondée à demander l'annulation partielle du jugement et de l'arrêté litigieux. <br/>
<br/>
              11. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 28 mai 2019 de la cour administrative d'appel de Nancy est annulé.<br/>
Article 2 : Il est donné acte du désistement partiel de la société Parc éolien de l'Aire.<br/>
Article 3 : Le surplus des conclusions présentées par la société Parc éolien de l'Aire devant la cour administrative d'appel de Nancy est rejeté.<br/>
Article 4 : La présente décision sera notifiée à la société Parc éolien de l'Aire et à la ministre de la cohésion des territoires et des relations avec les collectivités territoriales.<br/>
Copie en sera adressée à la ministre des armées.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
