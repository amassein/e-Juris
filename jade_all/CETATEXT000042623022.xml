<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042623022</ID>
<ANCIEN_ID>JG_L_2020_12_000000445394</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/62/30/CETATEXT000042623022.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 01/12/2020, 445394, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-12-01</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>445394</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2020:445394.20201201</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B... A... a demandé au juge des référés du tribunal administratif de Cergy-Pontoise, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, d'ordonner la suspension de l'exécution de l'arrêté préfectoral n° 2020-680 du 10 septembre 2020 par lequel le préfet des Hauts-de-Seine a rendu obligatoire le port du masque dans le département des Hauts-de-Seine pour les personnes de plus de onze ans sur l'ensemble de la voie publique et dans tous les lieux ouverts au public, sous réserve des dispositions prévues aux articles 2 et 3 de cet arrêté. Par une ordonnance n° 2009034 du 18 septembre 2020, le juge des référés du tribunal administratif de Cergy-Pontoise a rejeté sa demande. <br/>
<br/>
              Par une requête et un nouveau mémoire, enregistrés les 15 et 17 octobre 2020 au secrétariat du contentieux du Conseil d'Etat, M. A... demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative : <br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) de faire droit à ses conclusions de première instance ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 500 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
              Il soutient que :<br/>
              - l'ordonnance contestée doit être annulée dès lors que le juge des référés de première instance ne pouvait écarter l'atteinte grave et manifestement illégale aux libertés invoquées eu égard au caractère général et absolu de la mesure contestée ;<br/>
              - la condition d'urgence est remplie eu égard au caractère grave et immédiat de l'atteinte portée aux libertés fondamentales ;<br/>
              - il est porté une atteinte grave et manifestement illégale à la liberté d'aller et venir et à la liberté personnelle ;<br/>
              - l'arrêté contesté méconnaît les dispositions de la loi n° 2010-1192 du 11 octobre 2010 interdisant la dissimulation du visage dans l'espace public ; <br/>
              - il méconnait les dispositions de l'article 1er du décret n° 2020-860 du 10 juillet 2020 et de son annexe 1 dès lors que dans certains lieux du département les circonstances locales garantissent le respect des règles de distanciation physique ;<br/>
              - l'obligation du port du masque sur la voie publique n'est ni nécessaire, ni adaptée, ni proportionnée dès lors, en premier lieu, que plusieurs études démontrent que la quasi-totalité des contaminations à la covid-19 a lieu en milieu clos, en deuxième lieu, que cette mesure induit un relâchement de l'attention et du comportement de la population dans les lieux non soumis à l'obligation, en troisième lieu, qu'elle ne pouvait être imposée que dans les communes à très forte densité de population et, en dernier lieu, que d'autres mesures moins attentatoires aux libertés sont possibles ;  <br/>
              - l'article 1er du décret n° 2020-860 du 10 juillet 2020, sur lequel se fonde l'arrêté contesté, est illégal dès lors qu'en vertu des dispositions de l'article L. 3131-1 du code de la santé publique, seul le ministre chargé de la santé est compétent pour prescrire, en cas de menace sanitaire grave, toute mesure afin de prévenir et limiter les conséquences des menaces possibles sur la santé de la population.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la Constitution, et notamment son Préambule ;<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - le code de la santé publique ;<br/>
              - le décret n° 2020-1257 du 14 octobre 2020 ;<br/>
              - le décret n° 2020-1310 du 29 octobre 2020 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. Le juge des référés se prononce dans un délai de quarante-huit heures ". <br/>
<br/>
              2. Par un arrêté du 10 septembre 2020, le préfet des Hauts-de-Seine a imposé le port du masque aux personnes de onze ans et plus sur l'ensemble de la voie publique et dans tous les lieux ouverts au public dans le département des Hauts-de-Seine, à l'exception de certains lieux énumérés, notamment les forêts de ce département hormis les samedis, dimanches et jours fériés. Cet arrêté prévoyait en outre qu'il ne s'applique pas aux personnes circulant à vélo, à celles circulant dans un véhicule, à celles pratiquant une activité physique et sportive et aux personnes en situation de handicap munies d'un certificat médical justifiant de cette dérogation. M. A... relève appel de l'ordonnance du 18 septembre 2020 par laquelle le juge des référés du tribunal administratif de Cergy-Pontoise a rejeté sa demande, présentée sur le fondement de l'article L. 521-2 du code de justice administrative, tendant à ce que soit suspendue l'exécution de cet arrêté.<br/>
<br/>
              3. Il résulte de l'instruction que par un décret n° 2020-1257 du 14 octobre 2020, le Président de la République a déclaré l'état d'urgence sanitaire sur l'ensemble du territoire de la République à compter du 17 octobre 2020. Dans ce contexte renouvelé, le préfet des Hauts-de-Seine a, par un arrêté n° 2020-869 du 17 octobre 2020, prescrit les mesures applicables au département des Hauts-de-Seine, abrogeant ainsi l'arrêté du 10 septembre 2020. Ce nouvel arrêté du 17 octobre 2020 a lui-même été abrogé par un arrêté n° 2020-882 du 30 octobre 2020 pris sur le fondement du décret n° 2020-1310 du 29 octobre 2020 par lequel le Premier ministre a prescrit de nouvelles mesures générales nécessaires pour faire face à l'épidémie de covid-19. Par suite, les conclusions de M. A... tendant, d'une part, à ce que soit annulée l'ordonnance contestée et, d'autre part, à ce que soit ordonnée la suspension de l'exécution de l'arrêté du 10 septembre 2020 ont perdu leur objet. Il n'y a dès lors plus lieu d'y statuer.<br/>
<br/>
              4. Il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par M. A... au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>			O R D O N N E :<br/>
              			------------------<br/>
<br/>
Article 1er : Il n'y a pas lieu de statuer sur les conclusions présentées sur le fondement de l'article L. 521-2 du code de justice administrative. <br/>
Article 2 : Le surplus des conclusions de la requête est rejeté.<br/>
Article 3 : La présente ordonnance sera notifiée à M. B... A.... <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
