<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036411858</ID>
<ANCIEN_ID>JG_L_2017_12_000000403106</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/41/18/CETATEXT000036411858.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème chambre, 28/12/2017, 403106, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-12-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>403106</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR>Mme Mireille Le Corre</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Louis Dutheillet de Lamothe</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2017:403106.20171228</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              Par une requête sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 2 septembre 2016, 2 décembre 2016 et 24 mai 2017 au secrétariat du contentieux du Conseil d'Etat, le conseil national des administrateurs judiciaires et des mandataires judiciaires demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret n° 2016-902 du 1er juillet 2016 relatif à l'exercice sous forme de société ou d'autre entité dotée de la personnalité morale de la profession d'administrateur judiciaire ou de mandataire judiciaire ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la Constitution ;<br/>
              - le code de commerce ;<br/>
              - la loi n° 90-1258 du 31 décembre 1990 ;<br/>
              - la loi n° 2015-990 du 6 août 2015 ;<br/>
              - l'ordonnance n° 2016-394 du 31 mars 2016 ;<br/>
              - le décret n° 2017-796 du 5 mai 2017 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Mireille Le Corre, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Louis Dutheillet de Lamothe, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Piwnica, Molinié, avocat du conseil national des administrateurs judiciaires et des mandataires judiciaires ;<br/>
<br/>
<br/>
<br/>1. Considérant que le a) du 8° de l'article 1er du décret du 1er juillet 2016 relatif à l'exercice sous forme de société ou d'autre entité dotée de la personnalité morale de la profession d'administrateur judiciaire ou de mandataire judiciaire remplace le premier alinéa de l'article R. 814-64 du code de commerce par les dispositions suivantes : " Toute modification de la répartition ou du nombre des actions ou parts sociales détenues par les associés exerçant la profession ou des droits de vote y afférents, ainsi que toute modification des statuts, fait l'objet, dans les trente jours, d'une déclaration à la Commission nationale d'inscription et de discipline, par lettre recommandée avec demande d'avis de réception, à la diligence de la société ou de l'un au moins des associés concernés. La déclaration est accompagnée de la copie des statuts et de tout document permettant d'établir l'accord de la société ou des autres associés lorsque celui-ci est requis. " ;  <br/>
<br/>
              2. Considérant que le décret attaqué a été pris pour l'application de l'article 63 de la loi du 6 août 2015 pour la croissance, l'activité et l'égalité des chances économiques, et non pour l'application de l'ordonnance du 31 mars 2016 relative aux sociétés constituées pour l'exercice en commun de plusieurs professions libérales soumises à un statut législatif ou réglementaire ou dont le titre est protégé, ou en raison de cette ordonnance, qui lui est d'ailleurs postérieure ; que, dès lors, le moyen tiré de ce que le décret attaqué doit être annulé par voie de conséquence de l'annulation de l'ordonnance du 31 mars 2016 doit être, en tout état de cause, écarté ;<br/>
<br/>
              3. Considérant que l'article R. 814-64 du code de commerce, dans sa rédaction issue du 8° de l'article 1er du décret attaqué, prévoit que la modification des statuts des sociétés d'administrateurs judiciaires et de mandataires judiciaires, qui était auparavant soumise à autorisation de la commission nationale d'inscription et de discipline, après avis du conseil national des administrateurs judiciaires et mandataires judiciaires, est désormais soumise à simple déclaration auprès de cette commission sans avis préalable du conseil national ; qu'il appartient à l'autorité investie du pouvoir réglementaire de fixer, en vertu des pouvoirs qu'elle tient de l'article 37 de la Constitution, des prescriptions applicables aux membres d'une profession réglementée, complémentaires de celles qui résultent de la loi ; qu'au demeurant, la loi du 6 août 2015 précitée a inséré à chacun des articles L. 811-7 et L. 812-5 du code de commerce, qui sont relatifs à l'exercice en commun sous forme de société de la profession, respectivement, d'administrateur judiciaire et de mandataire judiciaire, un cinquième alinéa rédigé comme suit : " Dans le respect des règles de déontologie applicables à chaque profession, un décret en Conseil d'Etat détermine les conditions d'application du présent article. Il présente notamment les conditions d'inscription et d'omission de ces sociétés auprès de l'autorité professionnelle compétente." ; que, par suite, le moyen tiré de ce que les dispositions réglementaires attaquées seraient entachées d'incompétence, faute d'une habilitation expresse du législateur, doit être également et en tout état de cause écarté ;  <br/>
<br/>
              4. Considérant qu'aux termes de l'article L. 814-2 du code de commerce, " Il incombe (...) au conseil national de veiller au respect de leurs obligations par les mandataires de justice, d'organiser leur formation professionnelle, de s'assurer qu'ils se conforment à leur obligation d'entretien et de perfectionnement des connaissances et de contrôler leurs études. " ; que, contrairement à ce qui est soutenu, ces dispositions n'imposent pas que le conseil national des administrateurs judiciaires et des mandataires judiciaires soit consulté avant toute modification des statuts des sociétés constituées pour l'exercice des professions placées sous son contrôle ; que, par ailleurs, en vertu des articles L. 811-11 et L. 812-9 du code de commerce, les administrateurs judiciaires et les mandataires judiciaires sont tenus, sans pouvoir opposer le secret professionnel, de déférer aux demandes des personnes chargées du contrôle tendant à la communication de tous renseignements ou documents utiles ; qu'il ne ressort pas des pièces du dossier que l'absence d'une information systématique sur les modifications statutaires de ces sociétés empêcherait le conseil national d'exercer la mission de contrôle qui lui est confiée ; que, par suite, le moyen tiré de ce que le décret attaqué, en supprimant sa consultation préalable, aurait méconnu les dispositions précitées et serait entaché d'une erreur manifeste d'appréciation ne peut être accueilli ;<br/>
<br/>
              5. Considérant que le 6° de l'article R. 814-60 du code du commerce, dans sa rédaction issue du c) du 5° du décret attaqué, prévoit que le dossier à soumettre à la commission nationale d'inscription et de discipline comprend : " Dans le cas d'une demande d'inscription d'une société prévue au deuxième alinéa de l'article L. 811-7 ou de l'article L. 812-5, autre que celle mentionnée au 5°, la liste des associés qui n'exercent pas la profession d'administrateur judiciaire ou de mandataire judiciaire précisant pour chacun d'eux leurs nom, prénoms, domicile et profession, la part de capital qu'ils détiennent et, si l'associé est une personne morale, la raison ou la dénomination sociale, le siège social et la part du capital social que cette personne morale détient ainsi que tout élément permettant d'établir que les exigences de détention de capital prévues par la loi du 31 décembre 1990 précitée sont satisfaites " ; que le requérant ne peut utilement soutenir que le pouvoir réglementaire se serait illégalement abstenu de prévoir les mesures nécessaires à la prévention des conflits d'intérêts entre associés d'une société pluri-professionnelle d'exercice, dès lors qu'à la date de son intervention le décret attaqué n'était pas applicable à cette catégorie de sociétés ; que les moyens tirés de ce que le pouvoir réglementaire se serait illégalement abstenu d'instituer des modalités particulières de surveillance et d'inspection des sociétés pluri-professionnelles d'exercice et de prévoir les conditions d'application à ces sociétés des règles de discipline doivent être écartés pour le même motif ;<br/>
<br/>
              6. Considérant qu'il résulte de tout ce qui précède que le conseil national des administrateurs judiciaires et des mandataires judiciaires n'est pas fondé à demander l'annulation du décret attaqué ; que ses conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative ne peuvent, par suite, qu'être rejetées ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
          D E C I D E :<br/>
          --------------<br/>
<br/>
Article 1er : La requête du conseil national des administrateurs judiciaires et des mandataires judiciaires est rejetée.<br/>
Article 2 : La présente décision sera notifiée au conseil national des administrateurs judiciaires et des mandataires judiciaires, au Premier ministre et au ministre de l'économie et des finances.<br/>
Copie en sera adressée à la garde des sceaux, ministre de la justice.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
