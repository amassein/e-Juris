<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000024911123</ID>
<ANCIEN_ID>JG_L_2011_11_000000348164</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/24/91/11/CETATEXT000024911123.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  9ème sous-section jugeant seule, 28/11/2011, 348164, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2011-11-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>348164</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 9ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Contentieux des pensions</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Jean-Pierre Jouguelet</PRESIDENT>
<AVOCATS>SCP POTIER DE LA VARDE, BUK LAMENT</AVOCATS>
<RAPPORTEUR>Mme Cécile Isidoro</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Aladjidi</COMMISSAIRE_GVT>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 7 avril et 5 juillet 2011 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. Gilbert A, demeurant ... ; M. A demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'article 3 de l'ordonnance n° 1014776/5-2 du 3 février 2011 par laquelle le vice-président de la cinquième section du tribunal administratif de Paris, après avoir annulé l'arrêté du ministre de l'économie, des finances et du budget du 16 mars 1992 concédant à M. A sa pension de retraite en tant qu'il ne comporte pas le bénéfice de la bonification pour enfant, a enjoint au ministre du budget, des comptes publics, de la fonction publique et de la réforme de l'Etat, porte-parole du Gouvernement, de modifier les conditions dans lesquelles la pension du requérant lui a été concédée et de revaloriser rétroactivement cette pension à compter du 1er janvier 2006 ;<br/>
<br/>
              2°) réglant l'affaire au fond, d'enjoindre à la ministre du budget, des comptes publics, de la fonction publique et de la réforme de l'Etat, porte-parole du Gouvernement, de revaloriser sa pension rétroactivement à compter du 1er janvier 1998 ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat le versement de la somme de 1 500 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code civil ;<br/>
<br/>
              Vu le code des pensions civiles et militaires de retraite ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Cécile Isidoro, Maître des Requêtes,  <br/>
<br/>
              - les observations de la SCP Potier de la Varde, Buk Lament, avocat de M. A, <br/>
<br/>
              - les conclusions de M. Frédéric Aladjidi, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Potier de la Varde, Buk Lament, avocat de M. A ;<br/>
<br/>
<br/>
<br/>Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, par arrêté du 16 mars 1992, M. A a été admis au bénéfice d'une pension de retraite ; qu'il a présenté, le 11 novembre 2002, une demande de révision de sa pension pour tenir compte de la bonification pour enfants prévue au b) de l'article L. 12 du code des pensions civiles et militaires de retraite, que l'administration n'a pas accueillie ; qu'il a directement saisi la juridiction administrative, le 6 juillet 2010, d'une demande d'annulation de l'arrêté du 16 mars 1992, en tant qu'il ne prenait pas en compte la bonification prévue au b) de l'article L. 12 ; que, par une ordonnance du 3 février 2011, le tribunal administratif de Paris a fait droit à la demande d'annulation de cet arrêté, mais n'a enjoint à l'administration de revaloriser la pension du requérant pour tenir compte de cette bonification qu'à compter du 1er janvier 2006 ; que M. A se pourvoit en cassation contre l'article 3 de cette ordonnance qui fixe la date de cette revalorisation ;<br/>
<br/>
              Considérant qu'aux termes de l'article L. 53 du code des pensions civiles et militaires de retraite en vigueur à la date de la demande : Lorsque, par suite du fait personnel du pensionné, la demande de liquidation ou de révision de la pension est déposée postérieurement à l'expiration de la quatrième année qui suit celle de l'entrée en jouissance normale de la pension, le titulaire ne peut prétendre qu'aux arrérages afférents à l'année au cours de laquelle la demande a été déposée et aux quatre années antérieures ; que le tribunal administratif de Paris a jugé que la requête transmise au greffe du tribunal par une ordonnance du président de la section du contentieux du Conseil d'Etat le 7 août 2010 était la première demande de M. A visant à la révision de sa pension et que la date de revalorisation de sa pension devait, par suite, être fixée au 1er janvier 2006 ; que, toutefois, le courrier daté du 11 novembre 2002 qui avait pour objet le versement de la bonification pour enfants par l'invocation du caractère discriminatoire de la législation réservant cet avantage aux femmes fonctionnaires et qui précisait ainsi le fondement de sa demande constituait une demande de liquidation ou de révision de la pension telle que définie par l'article L. 53 du code des pensions civiles et militaires de retraite ; que, dès lors, le tribunal administratif de Paris a commis une erreur de droit en ne retenant pas ce courrier comme de nature à interrompre la prescription de l'article L. 53 ; que, par suite, l'article 3 du jugement attaqué doit, pour ce motif et sans qu'il soit besoin d'examiner l'autre moyen du pourvoi, être annulé ;<br/>
<br/>
              Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler dans cette mesure l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              Considérant qu'il résulte de ce qui a été dit ci-dessus que l'intéressé avait adressé dès le 11 novembre 2002 au service des pensions une demande tendant à la prise en compte de la bonification pour enfants prévue au b) de l'article L. 12 du code des pensions civiles et militaires de retraite ; que, dans ces conditions, et alors même qu'il n'a ensuite saisi le tribunal administratif de Paris que le 7 août 2010, la date à retenir pour le délai de prescription des arrérages est celle de la réception du courrier daté du 11 novembre 2002 ; que M. A peut en conséquence prétendre à la revalorisation de sa pension à compter du 1er janvier 1998 ; qu'il y a lieu, dès lors, de prescrire à la ministre de modifier les conditions dans lesquelles la pension de M. A lui a été concédée et de revaloriser rétroactivement cette pension à compter du 1er janvier 1998 ;<br/>
<br/>
              Considérant, enfin, qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat le versement à M. A de la somme de 1 500 euros en application des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : L'article 3 de l'ordonnance du tribunal administratif de Paris du 3 février 2011 est annulé.<br/>
Article 2 : La ministre du budget, des comptes publics et de la réforme de l'Etat, porte-parole du Gouvernement, revalorisera rétroactivement la pension de M. A à compter du 1er janvier 1998.<br/>
Article 3 : L'Etat versera à M. A la somme de 1 500 euros en application de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à M. Gilbert A et à la ministre du budget, des comptes publics et de la réforme de l'Etat, porte-parole du Gouvernement.<br/>
<br/>
</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
