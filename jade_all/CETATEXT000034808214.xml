<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034808214</ID>
<ANCIEN_ID>JG_L_2017_05_000000392678</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/80/82/CETATEXT000034808214.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère - 6ème chambres réunies, 24/05/2017, 392678, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-05-24</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>392678</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère - 6ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MONOD, COLIN, STOCLET ; SCP ROCHETEAU, UZAN-SARANO</AVOCATS>
<RAPPORTEUR>Mme Marie Sirinelli</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Jean Lessi</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:392678.20170524</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La Fédération des employés et cadres de la Confédération générale du travail - Force Ouvrière a demandé au tribunal administratif de Melun d'annuler pour excès de pouvoir l'arrêté du 2 août 2012 par lequel le préfet du Val-de-Marne a autorisé la société Folies Douces à déroger pour une durée de cinq ans à la règle du repos dominical pour les salariés du magasin " Réserve naturelle " qu'elle exploite à Thiais. Par un jugement n° 1209900 du 29 novembre 2013, le tribunal administratif de Melun a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 14PA00438 du 22 juin 2015, la cour administrative d'appel de Paris a rejeté l'appel de la Fédération des employés et cadres de la Confédération générale du travail - Force Ouvrière contre ce jugement.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés le 14 août 2015, le 10 novembre 2015 et le 19 mai 2016 au secrétariat du contentieux du Conseil d'Etat, la Fédération des employés et cadres de la Confédération générale du travail - Force Ouvrière demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 4 500 euros au titre de l'article L.761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code du travail ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Marie Sirinelli, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Jean Lessi, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Rocheteau, Uzan-Sarano, avocat de la Fédération des employés et cadres de la CGT - Force Ouvrière et de la Confédération générale du travail - Force ouvrière et à la SCP Monod, Colin, Stoclet, avocat de la société Folies Douces.<br/>
<br/>
<br/>
<br/>
<br/>Sur l'intervention de la Confédération générale du travail - Force ouvrière : <br/>
<br/>
              1. Considérant que la Confédération générale du travail - Force ouvrière justifie d'un intérêt suffisant à l'annulation de l'arrêt attaqué ; qu'ainsi son intervention est recevable ; <br/>
<br/>
              Sur les conclusions aux  fins d'annulation de l'arrêt attaqué : <br/>
<br/>
              2. Considérant, d'une part, qu'aux termes de l'article L. 3132-25-1 du code du travail, dans sa rédaction alors en vigueur : " Sans préjudice des dispositions de l'article L. 3132-20, dans les unités urbaines de plus de 1 000 000 d'habitants, le repos hebdomadaire peut être donné, après autorisation administrative, par roulement, pour tout ou partie du personnel, dans les établissements de vente au détail qui mettent à disposition des biens et des services dans un périmètre d'usage de consommation exceptionnel caractérisé par des habitudes de consommation dominicale, l'importance de la clientèle concernée et l'éloignement de celle-ci de ce périmètre " ;<br/>
<br/>
              3. Considérant, d'autre part, qu'aux termes de l'article L. 2131-1 du code du travail : " Les syndicats professionnels ont exclusivement pour objet l'étude et la défense des droits ainsi que des intérêts matériels et moraux, tant collectifs qu'individuels, des personnes mentionnées dans leurs statuts " ; qu'aux termes de l'article L. 2132-3 du même code : " Les syndicats professionnels ont le droit d'agir en justice./ Ils peuvent, devant toutes les juridictions, exercer tous les droits réservés à la partie civile concernant les faits portant un préjudice direct ou indirect à l'intérêt collectif de la profession qu'ils représentent " ; qu'aux termes de l'article L. 2133-3 de ce code : " Les unions de syndicats jouissent de tous les droits conférés aux syndicats professionnels par le présent titre " ;<br/>
<br/>
              4. Considérant qu'il résulte des dispositions précitées des articles L. 2131-1 et L. 2132-3 du code du travail que tout syndicat professionnel peut utilement, en vue de justifier d'un intérêt lui donnant qualité pour demander l'annulation d'une décision administrative, se prévaloir de l'intérêt collectif que la loi lui donne pour objet de défendre, dans l'ensemble du champ professionnel et géographique qu'il se donne pour objet statutaire de représenter, et sans que cet intérêt collectif ne soit limité à celui de ses adhérents ; qu'en application de l'article L. 2133-3 précité du même code, il en va de même d'une union de syndicats, sauf stipulations contraires de ses statuts ; que, dans ce cadre, l'intérêt pour agir d'un syndicat ou d'une union de syndicats en vertu de cet intérêt collectif s'apprécie au regard de la portée de la décision contestée;<br/>
<br/>
              5. Considérant que, pour juger que la Fédération des employés et cadres Confédération générale du travail - Force Ouvrière ne justifiait pas d'un intérêt lui donnant qualité pour demander l'annulation de la décision du 2 août 2012 par laquelle le préfet du Val-de-Marne a, sur le fondement des dispositions précitées de l'article L. 3132-25-1 du code du travail, accordé à la société Folies Douces une dérogation au repos dominical de cinq ans pour son établissement de Thiais, la cour s'est fondée sur ce que ses statuts ne mentionnaient pas qu'elle avait pour mission d'assurer la défense des membres des syndicats qui la composent ; qu'il résulte de ce qui précède que la fédération requérante est fondée à soutenir qu'elle a ainsi commis une erreur de droit ;<br/>
<br/>
              6. Considérant, toutefois, que la décision du préfet du Val-de-Marne en cause avait pour objet d'accorder dans un périmètre d'usage de consommation exceptionnel, sur le fondement de l'article L. 3132-25-1 du code du travail précité, une dérogation au repos dominical à un unique établissement de la société Folies Douces ; qu'eu égard à la portée de la décision en cause, une union nationale de syndicats défendant les intérêts collectifs notamment des salariés du commerce non alimentaire comme la fédération requérante ne justifiait pas d'un intérêt lui donnant qualité pour demander l'annulation de la décision qu'elle attaquait, alors même que cette décision accordait une dérogation à la règle du repos dominical que la fédération requérante s'est donné pour objectif de préserver lors de plusieurs congrès fédéraux ; que ce motif, qui répond à un moyen soulevé devant les juges du fond et dont l'examen n'implique l'appréciation d'aucune circonstance de fait, doit être substitué au motif erroné en droit retenu par l'arrêt attaqué, dont il justifie le dispositif ;<br/>
<br/>
              7. Considérant qu'il résulte de ce qui précède que la Fédération des employés et cadres de la Confédération générale du travail - Force Ouvrière n'est pas fondée à demander l'annulation de l'arrêt du 22 juin 2015 de la cour administrative d'appel de Paris ; que son pourvoi doit, dès lors, être rejeté, y compris en ce qu'il comporte des conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative ; qu'en revanche, il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la Fédération des employés et cadres de la Confédération générale du travail - Force Ouvrière la somme de 1 500 euros à verser à la société Folies Douces, au titre des mêmes dispositions ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'intervention de la Confédération générale du travail - Force ouvrière est admise. <br/>
Article 2 : Le pourvoi de la Fédération des employés et cadres de la Confédération générale du travail - Force Ouvrière est rejeté.<br/>
Article 3 : La Fédération des employés et cadres de la Confédération générale du travail - Force Ouvrière versera à la société Folies Douces une somme de 1 500 euros au titre de l'article L. 761-1 du code de justice administrative <br/>
Article 4 : La présente décision sera notifiée à la Fédération des employés et cadres de la Confédération générale du travail - Force Ouvrière, à la société Folies Douces, à la Confédération générale du travail - Force ouvrière et à la ministre du travail.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
