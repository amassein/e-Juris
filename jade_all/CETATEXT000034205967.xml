<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034205967</ID>
<ANCIEN_ID>JG_L_2017_03_000000393610</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/20/59/CETATEXT000034205967.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème - 3ème chambres réunies, 15/03/2017, 393610, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-03-15</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>393610</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème - 3ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Emmanuelle Petitdemange</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Romain Victor</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:393610.20170315</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Par une ordonnance n° 1513167 du 18 septembre 2015, enregistrée le même jour au secrétariat du contentieux du Conseil d'Etat, le président du tribunal administratif de Paris a transmis au Conseil d'Etat, en application de l'article R. 351-2 du code de justice administrative, la requête présentée à ce tribunal par les associations Société de protection des paysages et de l'esthétique de la France et Association Yvelines Environnement.<br/>
<br/>
              Par cette requête, enregistrée au greffe du tribunal administratif de Paris le 30 juillet 2015, et un mémoire en réplique, enregistré au secrétariat du contentieux du Conseil d'État le 9 novembre 2016, ces associations demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret n° 2015-163 du 12 février 2015 modifiant le décret n° 2013-252 du 25 mars 2013 fixant la liste des biens pouvant être aliénés en application de l'article L. 3211-5-1 du code général de la propriété des personnes publiques en tant qu'il autorise l'aliénation du pavillon du Butard et de ses dépendances situés à la Celle-Saint-Cloud (Hauts-de-Seine), ensemble la décision implicite du Premier ministre de refuser de retirer dans cette mesure ce décret, née du silence gardé sur leur demande du 13 avril 2015 ;<br/>
<br/>
              2°) d'enjoindre au ministre de saisir le juge judiciaire pour résilier la vente dans l'hypothèse où celle-ci interviendrait en cours d'instance ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code forestier ;<br/>
              - le code du patrimoine ;<br/>
              - le code général de la propriété des personnes publiques ;<br/>
              - le décret n° 2015-163 du 12 février 2015 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Emmanuelle Petitdemange, auditeur,  <br/>
<br/>
              - les conclusions de M. Romain Victor, rapporteur public.<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier que le décret du 12 février 2015 pris en application de l'article L. 3211-5-1 du code général de la propriété des personnes publiques place le Pavillon du Butard, la maison forestière du Butard n° 1 et la maison forestière du Butard n° 2, qui sont situées dans la forêt de Fausses Reposes à La Celle-Saint-Cloud, sur la liste des immeubles pouvant être aliénés sur le fondement de ces dispositions. Les associations Société de protection des paysages et de l'esthétique de la France et Association Yvelines Environnement en demandent, dans cette mesure, l'annulation pour excès de pouvoir. <br/>
<br/>
              2. Aux termes de l'article L. 3211-5-1 du code général de la propriété des personnes publiques : " I. - L'aliénation d'un immeuble relevant du patrimoine immobilier bâti de l'Etat situé sur un terrain mentionné au 1° de l'article L. 211-1 du code forestier, ainsi que de son terrain d'assiette, n'est possible que si cet immeuble satisfait aux conditions suivantes : / 1° Il ne présente pas d'utilité pour atteindre les objectifs de gestion durable des bois et forêts conformément au chapitre Ier du titre II du livre Ier du code forestier ; / 2° Il est desservi par l'une des voies mentionnées aux articles L. 111-1 et L 161-1 du code de la voirie routière ou par un chemin forestier ouvert à la circulation publique. / Le terrain d'assiette pouvant être ainsi aliéné est limité à la superficie permettant un usage normal de l'immeuble bâti, comprenant notamment la cour, le jardin ou, le cas échéant, le parc qui lui est attaché. Un décret peut étendre cette superficie lorsque l'aliénation a pour objet de garantir la cohérence de la gestion forestière. / II. - La vente intervient dans la forme ordinaire des ventes des biens de l'Etat. La liste des immeubles pouvant être vendus dans les conditions mentionnées au présent article est fixée par décret pris sur le rapport conjoint des ministres chargés des forêts et du Domaine ".<br/>
<br/>
              3. En premier lieu, il ressort des termes du décret attaqué que ce dernier a été pris sur le rapport du ministre des finances et des comptes publics et du ministre de l'agriculture, de l'agroalimentaire et de la forêt. Par suite, le moyen tiré de que ce décret n'a pas été pris conformément aux dispositions du II de L. 3211-5-1 du code général de la propriété des personnes publiques manque en fait. <br/>
<br/>
              4. En deuxième lieu, aux termes de l'article L. 621-22 du code du patrimoine dans sa rédaction applicable à la date de la signature du décret attaqué : " L'immeuble classé au titre des monuments historiques qui appartient à l'État, à une collectivité territoriale ou à un établissement public, ne peut être aliéné qu'après que l'autorité administrative compétente a été appelée à présenter ses observations. Elle devra les présenter dans le délai de deux mois après la notification. L'autorité administrative compétente pourra, dans le délai de cinq ans, faire prononcer la nullité de l'aliénation consentie sans l'accomplissement de cette formalité ". Aux termes de l'article R. 621-52 du même code : " En cas de projet d'aliénation d'un immeuble classé appartenant à l'État, à une collectivité territoriale ou à un établissement public, l'autorité compétente pour présenter ses observations dans le délai de deux mois suivant la notification, en application de l'article L. 621-22, est le ministre chargé de la culture quand l'immeuble appartient à l'État ou à l'un de ses établissements publics et le préfet de région quand l'immeuble appartient à une collectivité territoriale ou à l'un de ses établissements publics ". <br/>
<br/>
              5. Il résulte des dispositions précitées de l'article L. 3211-5-1 du code général de la propriété des personnes publiques que le décret attaqué n'a pour objet que de constater que les conditions fixées au I de cet article sont remplies s'agissant des biens qu'il énumère et ne prévoit, à leur endroit, qu'une faculté d'aliénation. Les associations requérantes soutiennent qu'en application des dispositions des articles L. 621-22 et R. 621-52 du code du patrimoine, le ministre de la culture aurait dû être consulté préalablement à l'inscription du Pavillon du Butard sur la liste établie par le décret attaqué au motif qu'il a été classé monument historique par arrêté du 29 août 1927. Toutefois, les dispositions précitées du code du patrimoine n'imposent la consultation du ministre de la culture que préalablement à l'aliénation d'un monument historique de l'Etat et n'ont pas pour effet d'imposer une telle consultation préalablement à l'inscription sur la liste du décret attaqué. Ce moyen ne saurait, par suite, être utilement invoqué.<br/>
<br/>
              6. En troisième lieu, des biens relevant du domaine public de l'Etat ne sauraient être aliénés sans avoir été préalablement déclassés après, le cas échéant, désaffectation. Cependant, si les associations requérantes soutiennent que le décret attaqué méconnaît l'obligation de déclassement préalable du Pavillon du Butard au motif que ce dernier relèverait, avec les deux maisons forestières attenantes, du domaine public de l'Etat, une telle obligation ne s'imposerait en tout état de cause à l'Etat que préalablement à l'aliénation elle-même et non préalablement à l'inscription sur la liste établie par le décret attaqué, pour les motifs exposés au point 5. Ce moyen ne saurait, par suite, être utilement invoqué.<br/>
<br/>
              7. En quatrième lieu, aux termes de l'article L. 3211-1 du code général de la propriété des personnes publiques : " Lorsqu'ils ne sont plus utilisés par un service civil ou militaire de l'Etat ou un établissement public de l'Etat, les immeubles du domaine privé de l'Etat peuvent être vendus dans les conditions fixées par décret en Conseil d'Etat ".<br/>
<br/>
              8. Les associations soutiennent qu'à supposer que le Pavillon du Butard et ses dépendances relèvent du domaine privé de l'Etat, le décret méconnaît le " principe d'affectation préférentielle " qui serait issu des dispositions précitées de l'article L. 3211-1 du code général de la propriété des personnes publiques. Toutefois, le décret attaqué est pris pour l'application des dispositions de l'article L. 3211-5-1 de ce code, qui ne créent en tout état de cause aucune obligation à la charge de l'Etat tendant à rechercher, préalablement à l'aliénation, si le bien pour lequel celle-ci est envisagée est susceptible d'être affecté à un autre service civil ou militaire de l'Etat ou un établissement public de l'Etat ou utilisé par lui. Ce moyen doit, dès lors, être écarté.<br/>
<br/>
              9. En cinquième lieu, l'article L. 121-1 du code forestier dispose que : " (...) La politique forestière a pour objet d'assurer la gestion durable des bois et forêts. Elle prend en compte leurs fonctions économique, écologique et sociale. Elle concourt au développement de la qualification des emplois en vue de leur pérennisation. Elle vise à favoriser le regroupement technique et économique des propriétaires et l'organisation interprofessionnelle de la filière forestière pour en renforcer la compétitivité. Elle tend à satisfaire les demandes sociales relatives à la forêt ". Aux termes de l'article L. 121-2 du même code : " (...) L'Etat assure la cohérence de la politique forestière avec les autres politiques publiques relatives notamment au développement rural, à l'aménagement du territoire, à la protection des sols et des eaux et à la prévention des risques naturels. (...) ". Aux termes de l'article L. 121-3 de ce code : " Les bois et forêts relevant du régime forestier satisfont de manière spécifique à des besoins d'intérêt général soit par l'accomplissement des obligations particulières prévues par ce régime, soit par une promotion d'activités telles que l'accueil du public, la conservation des milieux, la prise en compte de la biodiversité et la recherche scientifique ".<br/>
<br/>
              10. Les associations requérantes soutiennent que le Pavillon du Butard et les deux maisons forestières présentent une utilité pour atteindre certains des objectifs de gestion durable des bois et forêts fixés au chapitre Ier du titre II du livre Ier du code forestier et ne satisferaient donc pas à la première condition posée par l'article L. 3211-5-1 précité du code général de la propriété des personnes publiques. Cependant, l'objectif de cohérence de la politique forestière avec les autres politiques publiques prévu par l'article L. 121-2 du code forestier et notamment, comme le soutiennent les associations, avec la politique du patrimoine, ne saurait être regardé comme un objectif de gestion durable des bois et forêts au sens de l'article L. 3211-5-1 du code général de la propriété des personnes publiques. Ensuite, les associations n'établissent pas que l'aliénation du Pavillon et de ses dépendances serait susceptible de nuire aux objectifs de conservation des milieux et de prise en compte de la biodiversité prévus à l'article L. 121-3 du code forestier. Enfin, le statut de forêt de protection conféré, par décret du 23 août 2007, à la forêt des Fausses Reposes est indifférent pour apprécier l'utilité de ces immeubles au regard de l'objectif d'accueil du public dans les bois et forêts énoncé par l'article L. 121-3 du code forestier, tout comme l'est la tenue de visites organisées du Pavillon. Si les associations soutiennent que le Pavillon et ses dépendances participent, en raison de leur position sur un belvédère, à un meilleur accueil du public dans la forêt, il ressort des pièces du dossier que ces bâtiments sont entourés de grilles et que leur accès n'est possible que de manière limitée, lors de visites organisées. Ce moyen doit donc être écarté.<br/>
<br/>
              11. En dernier lieu, la circonstance que le décret poursuivrait des finalités financières n'est pas de nature à caractériser l'existence d'un détournement de pouvoir.<br/>
<br/>
              12. Il résulte de ce qui précède que les associations requérantes ne sont pas fondées à demander l'annulation du décret qu'elles attaquent en tant qu'il concerne le Pavillon du Butard et les deux maisons forestières attenantes, non plus que de la décision implicite par laquelle le Premier ministre a refusé de le retirer dans cette mesure. Leurs conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative ne peuvent, par suite, qu'être rejetées.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de la Société de protection des paysages et de l'esthétique de la France et de l'Association Yvelines Environnement est rejetée. <br/>
Article 2 : La présente décision sera notifiée à la Société de protection des paysages et de l'esthétique de la France, à l'Association Yvelines Environnement, au Premier ministre, au ministre de l'économie et des finances et au ministre de l'agriculture, de l'agroalimentaire et de la forêt.<br/>
Copie en sera adressée au ministre de la culture et de la communication.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
