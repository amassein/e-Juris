<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037824116</ID>
<ANCIEN_ID>JG_L_2018_12_000000411920</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/82/41/CETATEXT000037824116.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème et 6ème chambres réunies, 17/12/2018, 411920, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-12-17</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>411920</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème et 6ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROUSSEAU, TAPIE ; SCP BOULLOCHE ; BALAT</AVOCATS>
<RAPPORTEUR>M. Alain Seban</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Cécile Barrois de Sarigny</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:411920.20181217</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              M. et Mme D...F...ont demandé au tribunal administratif de Bordeaux d'annuler pour excès de pouvoir le permis de construire tacite du 9 octobre 2010 par lequel le maire de Vaunac (Dordogne) a autorisé M. A...B...à construire une maison d'habitation. Par un jugement n° 1303053 du 3 mars 2015, le tribunal administratif a rejeté cette demande.<br/>
<br/>
              Par un arrêt n° 15BX01473 du 27 avril 2017 la cour administrative d'appel de Bordeaux, sur appel de M. et MmeF..., a annulé ce jugement et ce permis de construire.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et deux mémoires en réplique, enregistrés au secrétariat du contentieux du Conseil d'Etat les 28 juin et 28 septembre 2017 et les 6 juillet et 23 novembre 2018, M. et Mme E...demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de M. et Mme F...; <br/>
<br/>
              3°) de mettre à la charge de l'Etat une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
<br/>
              - le code de l'urbanisme ;<br/>
<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Alain Seban, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Cécile Barrois de Sarigny, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Boulloche, avocat de M. et MmeE..., à Me Balat, avocat de la commune de Vaunac et à la SCP Rousseau, Tapie, avocat de M. et MmeF....<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que M. et Mme E... ont acquis, le 23 janvier 2013, un terrain situé sur le territoire de la commune de Vaunac (Dordogne) sur lequel le précédent propriétaire avait obtenu, le 9 octobre 2010, un permis de construire tacite, délivré par le maire au nom de l'Etat, autorisant la construction d'une maison individuelle. Le 4 février 2013, le maire a transféré le permis de construire à M. E.... Le 26 juin 2013, il lui a délivré, au nom de l'Etat, un permis de construire modificatif. Par deux jugements du 3 mars 2015, le tribunal administratif de Bordeaux a rejeté les demandes de M. et Mme F...tendant à l'annulation pour excès de pouvoir du permis de construire initial et du permis modificatif. Sur appel de M. et MmeF..., la cour administrative d'appel de Bordeaux, par deux arrêts du 27 avril 2017, a annulé ces jugements ainsi que les permis de construire attaqués. M. et MmeE..., qui se sont par ailleurs pourvus, sous le n° 411921, contre l'arrêt relatif au permis modificatif, demandent sous le présent numéro l'annulation de l'arrêt relatif au permis initial. <br/>
<br/>
              Sur le pourvoi :<br/>
<br/>
              2. Aux termes de l'article R. 600-2 du code de l'urbanisme : " Le délai de recours contentieux à l'encontre d'une décision de non-opposition à une déclaration préalable ou d'un permis de construire, d'aménager ou de démolir court à l'égard des tiers à compter du premier jour d'une période continue de deux mois d'affichage sur le terrain des pièces mentionnées à l'article R. 424-15 ". Aux termes de l'article R. 424-15 du même code : " Mention du permis explicite ou tacite ou de la déclaration préalable doit être affichée sur le terrain, de manière visible de l'extérieur, par les soins de son bénéficiaire, dès la notification de l'arrêté ou dès la date à laquelle le permis tacite ou la décision de non-opposition à la déclaration préalable est acquis et pendant toute la durée du chantier. / (...) / Un arrêté du ministre chargé de l'urbanisme règle le contenu et les formes de l'affichage  ". Aux termes de l'article A. 424-17 du même code : " Le panneau d'affichage comprend la mention suivante : / " Droit de recours : / " Le délai de recours contentieux est de deux mois à compter du premier jour d'une période continue de deux mois d'affichage sur le terrain du présent panneau (art. R. 600-2 du code de l'urbanisme). / (...) ".<br/>
<br/>
              3. M. et Mme E...ont soutenu devant les juges du fond que le recours contre le permis de construire tacite du 9 octobre 2010, enregistré le 17 août 2013 au greffe du tribunal administratif, était tardif, en relevant que M. et Mme F...avaient eux-mêmes versé au dossier la photographie du panneau relatif à ce permis, apposé sur le terrain lors de sa délivrance. Pour écarter cette fin de non recevoir, la cour administrative d'appel a relevé que M. et Mme E...n'établissaient pas que l'affichage avait été maintenu pendant une période continue de deux mois. Toutefois, s'il incombe au bénéficiaire d'un permis de construire de justifier qu'il a accompli les formalités d'affichage prescrites par les dispositions précitées, le juge doit apprécier la continuité de l'affichage en examinant l'ensemble des pièces qui figurent au dossier qui lui est soumis. Il suit de là qu'en mettant à la charge de M. et Mme E...la preuve de la continuité de l'affichage sur le terrain, alors que M. et Mme F...se bornaient à déclarer que rien n'établissait que l'affichage avait été régulier, sans apporter le moindre élément de nature à mettre en doute qu'il ait été maintenu pendant une période continue de deux mois, la cour administrative d'appel a commis une erreur de droit qui justifie la cassation de l'arrêt attaqué, sans qu'il soit besoin d'examiner les autres moyens du pourvoi.<br/>
<br/>
              4. Il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond par application des dispositions de l'article L. 821-1 du code de justice administrative. <br/>
<br/>
              5. Le principe de sécurité juridique, qui implique que ne puissent être remises en cause sans condition de délai des situations consolidées par l'effet du temps, fait obstacle à ce que puisse être contesté indéfiniment par les tiers un permis de construire, une décision de non-opposition à une déclaration préalable, un permis d'aménager ou un permis de démolir. Dans le cas où l'affichage du permis ou de la déclaration, par ailleurs conforme aux prescriptions de l'article R. 424-15 du code de l'urbanisme, n'a pas fait courir le délai de recours de deux mois prévu à l'article R. 600-2, faute de mentionner ce délai comme l'exigeait l'article A. 424-17, un recours contentieux doit néanmoins, pour être recevable, être présenté dans un délai raisonnable à compter du premier jour de la période continue de deux mois d'affichage sur le terrain. En règle générale et sauf circonstance particulière dont se prévaudrait le requérant, un délai excédant un an ne peut être regardé comme raisonnable. Il résulte en outre de l'article R. 600-3 du code de l'urbanisme qu'un recours présenté postérieurement à l'expiration du délai qu'il prévoit, qui court à compter de la date d'achèvement des travaux, n'est pas recevable, alors même que le délai raisonnable mentionné ci-dessus n'aurait pas encore expiré.<br/>
<br/>
              6. M. et Mme F...ont versé au dossier la copie d'une plainte qu'ils ont transmise en juin 2013 au procureur de la République et des documents qu'ils y avaient joint, notamment la photographie d'un panneau d'affichage relatif au permis de construire délivré en octobre 2010 à M.B..., dont ils indiquaient qu'il avait été apposé sur le terrain en 2010. Ce panneau ne comportant pas la mention du délai de recours des tiers exigé par l'article A. 424-17 du code de l'urbanisme, l'affichage n'a pas pu faire courir le délai de recours de deux mois prévu à l'article R. 600-2. En revanche, dès lors que rien ne conduit à douter que l'affichage, par ailleurs conforme aux prescriptions de l'article R. 424-15, ait duré deux mois, il a fait courir le délai raisonnable imparti aux tiers pour attaquer le permis. Ce délai était expiré lorsque M. et Mme F...ont, le 17 août 2013, saisi le tribunal administratif de Bordeaux. Leur demande de première instance était, par suite, irrecevable. <br/>
<br/>
              7. Il résulte de ce qui précède que M. et Mme F...ne sont pas fondés à se plaindre de ce que le tribunal administratif de Bordeaux, par son jugement n° 1303053 du 3 mars 2015, a rejeté leur recours pour excès de pouvoir dirigé contre le permis de construire tacite né le 9 octobre 2010. <br/>
<br/>
              Sur les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              8. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de M. et Mme F...le versement à M. et Mme E...d'une somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative, pour l'ensemble de la procédure. Les dispositions de cet article font, en revanche, obstacle à ce que les sommes respectivement demandées par M. et MmeF..., qui sont la partie perdante dans la présente instance, et par la commune de Vaunac, qui, appelée en la cause devant le Conseil d'Etat pour produire des observations, n'a pas la qualité de partie à l'instance, soient mises à la charge de M. et Mme E....<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt n° 15BX01473 du 27 avril 2017 de la cour administrative d'appel de Bordeaux est annulé.<br/>
Article 2 : La requête présentée par M. et Mme F...devant la cour administrative d'appel de Bordeaux contre le jugement n° 1303053 du 3 mars 2015 du tribunal administratif de Bordeaux est rejetée.<br/>
Article 3 : M. et Mme F...verseront à M. et Mme E...une somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Les conclusions présentées par M. et Mme F...et par la commune de Vaunac au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée à M. et Mme C...E..., à M. et Mme D...F...et à la commune de Vaunac.<br/>
Copie en sera adressée à la ministre de la cohésion des territoires et des relations avec les collectivités territoriales.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
