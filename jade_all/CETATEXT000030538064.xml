<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030538064</ID>
<ANCIEN_ID>JG_L_2015_04_000000369000</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/53/80/CETATEXT000030538064.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème - 4ème SSR, 29/04/2015, 369000, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-04-29</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>369000</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème - 4ème SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BARTHELEMY, MATUCHANSKY, VEXLIARD, POUPOT ; SCP ROGER, SEVAUX, MATHONNET ; LE PRADO</AVOCATS>
<RAPPORTEUR>M. Charles Touboul</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:XX:2015:369000.20150429</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Par une décision du 24 juin 2014, le Conseil d'Etat, statuant au contentieux, a prononcé l'admission des conclusions du pourvoi de M. A...B...dirigées contre l'arrêt n° 11MA01106 et 11MA01124 du 2 avril 2013 de la cour administrative d'appel de Marseille en tant que, par son article 4, il subroge le centre hospitalier intercommunal de Toulon-La Seyne-sur-Mer, à concurrence de 26 600 euros, dans ses droits résultant ou pouvant résulter des condamnations prononcées par les tribunaux judiciaires à l'encontre du conducteur responsable de l'accident de la circulation survenu le 4 mars 2006 et de son assureur. <br/>
<br/>
              A l'appui de ces conclusions, M. B...soutient dans son pourvoi que la cour : <br/>
              - a méconnu le principe du contradictoire en prononçant d'office la subrogation du centre hospitalier dans les droits qui résultent ou pourraient résulter pour lui de la condamnation du responsable de l'accident de la circulation par le juge judiciaire, sans l'avoir mis à même de discuter de cette subrogation ;<br/>
              - a commis une erreur de droit en subrogeant le centre hospitalier, à concurrence des sommes versées au titre du préjudice personnel, dans les droits qui résultent ou pourraient résulter pour l'intéressé des condamnations prononcées à son profit par le juge judiciaire sans rechercher si les préjudices susceptibles d'être réparés par ce dernier pouvaient être identiques ni réserver à cette hypothèse la subrogation.<br/>
<br/>
              Par un mémoire en défense enregistré le 16 octobre 2014, le centre hospitalier de Toulon-La Seyne-sur-Mer conclut au rejet du pourvoi. Il soutient qu'aucun de ces moyens n'est fondé. <br/>
<br/>
              Par un mémoire en réplique enregistré le 24 novembre 2014, M. B... reprend les conclusions de son pourvoi et les mêmes moyens.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code civil ;<br/>
              -  le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Charles Touboul, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Barthélemy, Matuchansky, Vexliard, Poupot, avocat de M.B..., à Me Le Prado, avocat du centre hospitalier intercommunal de Toulon et à la SCP Roger, Sevaux, Mathonnet, avocat de l'office national d'indemnisation des accidents médicaux, des affections iatrogènes et des infections nosocomiales ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond qu'à la suite d'un accident de la circulation dont il a été victime le 4 mars 2006, M. B...a été pris en charge par le centre hospitalier de Toulon-La-Seyne-sur-Mer ; qu'en juin 2006, dans les jours suivant une intervention réalisée dans cet établissement,  une infection s'est déclarée ; que, par un jugement du 19 janvier 2011, le tribunal administratif de Toulon, retenant le caractère nosocomial de cette infection, a condamné le centre hospitalier à verser à M. B... une indemnité de 26 600 euros mais rejeté les conclusions de l'Etablissement national des invalides de la marine tendant au remboursement de ses débours ; que, par un arrêt du 2 avril 2013, la cour administrative d'appel de Marseille a fait droit à un appel de l'Etablissement national des invalides de la marine, rejeté l'appel du centre hospitalier et l'appel incident de M. B... et prononcé la subrogation du centre hospitalier, à concurrence de la somme de 26 600 euros allouée à ce dernier, dans les droits qui résultent ou pourraient résulter pour lui des condamnations prononcées à son profit par les tribunaux judiciaires à l'encontre du conducteur responsable de l'accident de la circulation et de son assureur ; que, par une décision du 24 juin 2014, le Conseil d'Etat, statuant au contentieux, a prononcé l'admission des conclusions du pourvoi de M. B...dirigées contre l'arrêt du 2 avril 2013 en tant que, par son article 4, il prononce cette subrogation ;<br/>
<br/>
              2. Considérant qu'il appartient au juge administratif, lorsqu'il détermine le montant et la forme des indemnités allouées par lui, de prendre, au besoin d'office, les mesures nécessaires pour que sa décision n'ait pas pour effet de procurer à la victime d'un dommage, par les indemnités qu'elle a pu ou pourrait obtenir en raison des mêmes faits, une réparation supérieure au préjudice subi ; <br/>
<br/>
              3. Considérant que la subrogation ainsi prononcée ne saurait porter que sur les droits dont la victime dispose à l'égard d'un tiers au titre des préjudices dont le juge administratif met la réparation à la charge de la collectivité publique dont la responsabilité est recherchée devant lui ; que, saisie par M. B...d'une action civile dirigée contre l'auteur de l'accident de la circulation et son assureur, la juridiction judiciaire est susceptible de mettre à la charge des défendeurs des indemnités réparant l'ensemble des conséquences dommageables de cet accident, y compris les préjudices nés de l'infection nosocomiale dont il a été victime au centre hospitalier de Toulon-La Seyne-sur-Mer ; que, par suite, la cour administrative d'appel de Marseille a commis une erreur de droit en s'abstenant de limiter la subrogation de ce centre hospitalier dans les droits susceptibles de résulter pour M. B...des condamnations prononcées à son profit par les tribunaux judiciaires à l'encontre de M. C...et de son assureur aux indemnités destinées à réparer les conséquences de l'infection nosocomiale ; que, par suite, sans qu'il soit besoin d'examiner l'autre moyen du pourvoi, l'article 4 de son arrêt doit être annulé ; <br/>
<br/>
              4. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond, dans la limite de la cassation prononcée en application des dispositions de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              5. Considérant qu'il résulte de ce qui a été dit ci-dessus que le centre hospitalier de Toulon-La Seyne-sur-Mer doit être subrogé, à concurrence de la somme de 26 600 euros et des intérêts capitalisés qui s'y rapportent, qu'il a été condamné à payer à M. B... par l'article 2 du jugement du 19 janvier 2011 du tribunal administratif de Toulon, dans les droits qui résultent ou pourraient résulter pour M. B...des condamnations prononcées à son profit par les tribunaux judiciaires à l'encontre de M. C... et de son assureur, au titre des préjudices causés par l'infection nosocomiale dont il a été victime au centre hospitalier de Toulon-La Seyne-sur-Mer ;<br/>
<br/>
              6. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge du centre hospitalier de Toulon-La-Seyne-sur-Mer, une somme de 1 500 euros au titre de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'article 4 de l'arrêt de la cour administrative d'appel de Marseille du 2 avril 2013 est annulé.<br/>
<br/>
Article 2 : Le centre hospitalier de Toulon-La Seyne-sur-Mer est subrogé, à concurrence de la somme de 26 600 euros et des intérêts capitalisés qui s'y rapportent qu'il a été condamné à payer à M. B...par l'article 2 du jugement du 19 janvier 2011 du tribunal administratif de Toulon, dans les droits qui résultent ou pourraient résulter pour M. B... des condamnations prononcées à son profit par les tribunaux judiciaires à l'encontre de M. C... et de son assureur, au titre des préjudices causés par l'infection nosocomiale dont il a été victime au centre hospitalier de Toulon- La Seyne-sur-Mer.<br/>
<br/>
Article 3 : Le centre hospitalier de Toulon-La Seyne-sur-Mer versera à M. B... une somme de 1 500 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
Article 4 : La présente décision sera notifiée à M. A... B...et au centre hospitalier de Toulon-La Seyne-sur-Mer.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
