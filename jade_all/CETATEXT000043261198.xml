<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043261198</ID>
<ANCIEN_ID>JG_L_2021_03_000000449743</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/26/11/CETATEXT000043261198.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 12/03/2021, 449743, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-03-12</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>449743</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DELAMARRE, JEHANNIN</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2021:449743.20210312</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu les procédures suivantes :<br/>
<br/>
              I. Sous le n° 449743, par une requête et un mémoire en réplique, enregistrés les 16 février et 2 mars 2021 au secrétariat du contentieux du Conseil d'Etat, M. A... B... demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative, d'ordonner la suspension de l'exécution du décret n° 2021-99 du 30 janvier 2021 modifiant les décrets n° 2020-1262 du 16 octobre 2020 et n° 2020-1310 du 29 octobre 2020 prescrivant les mesures générales nécessaires pour faire face à l'épidémie de la Covid-19 dans le cadre de l'état d'urgence sanitaire, en tant qu'il soumet l'entrée sur le territoire métropolitain des ressortissants français présents dans un pays étranger autre que ceux de l'Union européenne, Andorre, l'Islande, le Liechtenstein, Monaco, la Norvège, Saint-Marin, le Saint-Siège ou la Suisse à la justification d'un motif impérieux d'ordre personnel ou familial, un motif de santé relevant de l'urgence ou un motif professionnel ne pouvant être différé. <br/>
<br/>
<br/>
              Il soutient que :<br/>
              - le Conseil d'Etat est compétent en premier et dernier ressort pour connaître de ce litige ;<br/>
              - il justifie d'un intérêt à agir ; <br/>
              - la condition d'urgence est satisfaite dès lors que, en premier lieu, les ressortissants français présents dans un pays étranger autre que ceux de l'Union européenne, Andorre, l'Islande, le Liechtenstein, Monaco, la Norvège, Saint-Marin, le Saint-Siège ou la Suisse ne peuvent désormais pénétrer sur le territoire national que s'ils justifient d'un motif impérieux dont la liste a été établie discrétionnairement par les pouvoirs publics, en deuxième lieu, aucune disposition n'a été prise pour permettre aux ressortissants français d'exercer un recours effectif contre la décision de non-admission sur le territoire national qui pourrait leur être opposée et, en dernier lieu, il souhaite entrer sur le territoire national le 30 mars 2021 par un vol en provenance d'Istanbul ;<br/>
              - il existe un doute sérieux quant à la légalité des dispositions dont la suspension est demandée ;<br/>
              - ces dispositions méconnaissent le droit pour un citoyen d'entrer sur le territoire du pays dont il a la nationalité, garanti par les articles 2 et 4 de la Déclaration des droits de l'homme et du citoyen, l'article 13 de la Déclaration universelle des droits de l'homme, l'article 12 du Pacte international relatif aux droits civils et politiques, le paragraphe 2 de l'article 3 du Protocole n° 4 à la CEDH et le règlement (UE) 2016/399 du Parlement européen et du Conseil du 9 mars 2016 concernant un code de l'Union relatif au régime de franchissement des frontières par les personnes (code frontières Schengen), droit qui présente un caractère absolu et général ;<br/>
              - elles portent atteinte au droit au recours effectif, dès lors qu'elles ne prévoient aucune garantie au profit des ressortissants français qui feraient l'objet d'une mesure de refus d'entrée sur le territoire national ;<br/>
              - elles ne sont pas proportionnées à l'objectif de santé publique poursuivi dès lors que, en premier lieu, les pays ayant obtenu les meilleurs résultats dans la lutte contre la Covid-19 n'ont pas restreint le droit de leurs ressortissants à revenir sur le territoire national, en deuxième lieu, elles portent une atteinte injustifiée à l'égalité entre les citoyens français car le libre accès au territoire est maintenu pour les ressortissants français se déplaçant depuis les pays de l'espace économique européen, et ce alors même que certains de ces pays comptent parmi les plus touchés par la Covid-19, en troisième lieu, la liste des motifs impérieux envisagée par le ministre de l'intérieur est suffisamment large pour permettre à tout citoyen français ou presque de pénétrer sur le territoire, en quatrième lieu, la simple interdiction faite aux ressortissants français de quitter le territoire national suffit à réduire considérablement les déplacements internationaux de personnes et, en dernier lieu, des mesures moins restrictives sont possibles, comme une mise en quarantaine systématique ou l'exigence d'une preuve de vaccination préalable.<br/>
<br/>
              Par un mémoire en défense, enregistré le 1er mars 2021, le ministre des solidarités et de la santé conclut au rejet de la requête. Il soutient qu'aucun moyen n'est propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité des dispositions dont la suspension est demandée.<br/>
<br/>
              La requête a été communiquée au Premier ministre, au ministre de l'intérieur et au ministre de l'Europe et des affaires étrangères, qui n'ont pas produit de mémoire.<br/>
<br/>
<br/>
              II. Sous le n° 449830, par une requête, deux mémoires complémentaires, et deux mémoires en réplique, enregistrés les 17, 18 et 23 février et le 2 mars 2021 au secrétariat du contentieux du Conseil d'Etat, l'Union des Français de l'étranger demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative :<br/>
<br/>
              1°) d'ordonner la suspension de l'exécution des dispositions du 7° de l'article 1er  et du 16° de l'article 2 du décret n° 2021-31 du 15 janvier 2021 modifiant les décrets n° 2020-1262 du 16 octobre 2020 et n° 2020-1310 du 29 octobre 2020 prescrivant les mesures générales nécessaires pour faire face à l'épidémie de Covid-19 dans le cadre de l'état d'urgence sanitaire, en tant qu'elles imposent à tous les Français voulant rejoindre le territoire métropolitain depuis un pays autre qu'un pays membre de l'Union européenne, Andorre, l'Islande, le Liechtenstein, Monaco, la Norvège, Saint-Marin, le Saint-Siège ou la Suisse, de présenter le résultat d'un examen biologique de dépistage virologique réalisé moins de 72 heures avant le transport et ne concluant pas à une contamination par la Covid-19 ;<br/>
<br/>
              2°) d'ordonner la suspension de l'exécution des dispositions du 4° de l'article 1er et du 3° de l'article 2 du décret n° 2021-99 du 30 janvier 2021 modifiant les décrets n° 2020-1262 du 16 octobre 2020 et n° 2020-1310 du 29 octobre 2020 prescrivant les mesures générales nécessaires pour faire face à l'épidémie de Covid-19 dans le cadre de l'état d'urgence sanitaire, en tant qu'elles interdisent, sauf pour des motifs limitativement énumérés, l'entrée sur le territoire métropolitain des Français en provenance d'un pays autre qu'un pays membre de l'Union européenne, Andorre, l'Islande, le Liechtenstein, Monaco, la Norvège, Saint-Marin, le Saint-Siège ou la Suisse ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Elle soutient que :<br/>
              - sa requête est recevable ; <br/>
              - la condition d'urgence est satisfaite, dès lors que les dispositions contestées, qui font obstacle au retour des ressortissants français sur le territoire national s'ils ne peuvent présenter le résultat d'un examen biologique de dépistage virologique réalisé moins de 72 heures avant le vol ne concluant pas à une contamination par la Covid-19 et justifier en outre d'un motif impérieux, portent une atteinte suffisamment grave et immédiate aux intérêts matériels et moraux de ses membres ;<br/>
              - il existe un doute sérieux quant à la légalité des décrets dont la suspension est demandée ; <br/>
              - les dispositions contestées du décret du 15 janvier 2021 portent une atteinte disproportionnée à la liberté d'aller et venir ainsi qu'au droit de mener une vie familiale normale dès lors que, en premier lieu, elles imposent à tout ressortissant français établi hors de France et provenant d'un Etat étranger de réaliser un examen biologique de dépistage avant son départ, sans réserver l'hypothèse d'une dispense pour indisponibilité des tests, force majeure ou motif impérieux, en second lieu, elles ne sont pas justifiées par le risque sanitaire puisqu'elles ne sont pas applicables aux déplacements en provenance de l'Union européenne, où la situation sanitaire est très préoccupante, notamment en raison des variants ;<br/>
              - elles méconnaissent le principe d'égalité dès lors qu'elles créent une distinction entre les ressortissants français établis à l'étranger et ceux établis dans l'Union européenne, en Norvège, en Islande, en Andorre, au Saint-Siège, à Monaco, au Liechtenstein, à Saint Marin ou encore en Suisse, qui ne peut être justifiée par la lutte contre la propagation des variants, déjà présents sur le territoire national ; <br/>
              - les dispositions contestées du décret du 30 janvier 2021 méconnaissent le champ d'application des dispositions de l'article L. 3131-15 du code de la santé publique et sont entachées d'incompétence dès lors que le législateur n'a pas entendu confier au Premier ministre le pouvoir d'interdire l'entrée sur le territoire français à ses ressortissants, ni même de subordonner cette entrée à la justification d'un motif impérieux ;<br/>
              - elles méconnaissent les stipulations de l'article 3 du protocole 4 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales et de l'article 12 du Pacte international relatif aux droits civils et politiques, en ce qu'elles restreignent le droit conféré à toute personne d'entrer sur le territoire de l'Etat dont elle est ressortissante, qui revêt un caractère général et absolu ;<br/>
              - elles portent une atteinte disproportionnée à la liberté d'aller et venir ainsi qu'au droit de mener une vie familiale normale et méconnaissent les dispositions de l'article L. 3131-15 du code de la santé publique en ce qu'elles ne sont pas justifiées par le développement de l'épidémie ni par l'apparition des variants, déjà présents sur le territoire français, et que le ministre n'établit en rien le risque d'un afflux élevé, sur le territoire national, de Français établis hors de France ;<br/>
              - elles méconnaissent le principe d'égalité, dès lors qu'il n'existe pas de différences de situation, au regard de l'objectif de sauvegarde de la santé publique, entre les résidents de l'Union européenne et les ressortissants français résident à l'étranger justifiant une différence de traitement ;<br/>
              - elles méconnaissent le droit au respect de la vie privée et familiale ainsi que le droit au recours effectif dès lors que, d'une part, le refus d'embarquer opposé par une entreprise de transport, personne privée, ne peut faire l'objet d'aucun recours effectif et, d'autre part, l'entreprise de transport a accès à des informations personnelles touchant au motif du voyage, et notamment des informations couvertes par le secret médical ;<br/>
              - elles méconnaissent l'objectif à valeur constitutionnelle d'intelligibilité et d'accessibilité du droit ainsi que le principe de sécurité juridique dès lors que les motifs impérieux qu'elles prévoient sont imprécis et qu'aucun critère d'appréciation objectif n'est énoncé ;<br/>
              - elles méconnaissent l'exigence constitutionnelle de protection de la santé dès lors qu'elles subordonnent le droit de rentrer pour un motif de santé à la constatation d'une situation d'urgence ;<br/>
              - en confiant une activité de police administrative, à savoir le contrôle du motif de déplacement et le pouvoir de refuser l'embarquement, à des entreprises privées de transport, elles violent l'article 12 de la Déclaration des droits de l'homme et du citoyen et, à titre subsidiaire, celles de l'article 34 de la Constitution.<br/>
<br/>
              Par un mémoire en défense, enregistré le 1er mars 2021, le ministre des solidarités et de la santé conclut au rejet de la requête. Il soutient qu'aucun moyen n'est propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité des dispositions dont la suspension est demandée.<br/>
<br/>
              La requête a été communiquée au Premier ministre, au ministre de l'intérieur et au ministre de l'Europe et des affaires étrangères, qui n'ont pas produit de mémoire.<br/>
<br/>
              Vu le nouveau mémoire, enregistré le 4 mars 2020, présenté par le ministre des solidarités et de la santé, qui maintient ses conclusions et ses moyens ;<br/>
<br/>
              Vu le nouveau mémoire, enregistré le 5 mars 2021, présenté par M. B..., qui maintient ses conclusions et ses moyens et soutient que les mesures contestées pèsent sur 2,1 millions de Français, dont les déplacements vers la France ne représentent toutefois que 1 à 2 % des entrées quotidiennes sur le territoire national ;<br/>
<br/>
              Vu le nouveau mémoire, enregistré le 5 mars 2021, présenté par l'Union des Français de l'étranger, qui maintient ses conclusions et ses moyens ;<br/>
<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu : <br/>
              - la Constitution, et notamment son Préambule ; <br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ; <br/>
              - le code de la santé publique ;<br/>
              - la loi n° 2021-1379 du 14 novembre 2020 ; <br/>
              - la loi n° 2021-160 du 15 février 2021 ;<br/>
              - le décret n° 2020-1262 du 16 octobre 2020 ;<br/>
              - le décret n° 2020-1310 du 29 octobre 2020 ;<br/>
              - le décret n° 2021-31 du 15 janvier 2021 ;<br/>
              - le décret n° 2021-57 du 23 janvier 2021 ;<br/>
              - le décret n° 2021-99 du 30 janvier 2021 ;<br/>
              - le décret n° 2021-272 du 11 mars 2021 ;<br/>
              - le code de justice administrative, l'ordonnance n° 2020-1402 du 18 novembre 2020 et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, M. B... et l'Union des Français de l'étrangers et, d'autre part, le ministre de l'intérieur, le ministre de l'Europe et des affaires étrangères et le ministre des solidarités et de la santé ;<br/>
<br/>
              Ont été entendus lors de l'audience publique du 3 mars 2021, à 11 heures : <br/>
<br/>
              - Me Delamarre, avocat au Conseil d'Etat et à la Cour de cassation, avocat de M. B... et de l'Union des Français de l'étranger ;<br/>
<br/>
              - les représentants de l'Union des Français de l'étranger ; <br/>
<br/>
              - les représentants du ministre des solidarités et de la santé ;  <br/>
<br/>
              à l'issue de laquelle le juge des référés a reporté la clôture de l'instruction au 6 mars 2021 à 20 heures ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 12 mars 2021, présentée par le ministre des solidarités et de la santé ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Les requêtes de M. B... et de l'Union des Français de l'étranger présentent à juger les mêmes questions. Il y a lieu de les joindre pour statuer par une même décision. <br/>
<br/>
              2. Aux termes de l'article L. 521-1 du même code : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision ". <br/>
<br/>
              Sur le cadre juridique du litige : <br/>
<br/>
              3. Aux termes de l'article L. 3131-12 du code de la santé publique, issu de la loi du 23 mars 2020 d'urgence pour faire face à l'épidémie de Covid-19 : " L'état d'urgence sanitaire peut être déclaré sur tout ou partie du territoire métropolitain ainsi que du territoire des collectivités régies par les articles 73 et 74 de la Constitution et de la Nouvelle-Calédonie en cas de catastrophe sanitaire mettant en péril, par sa nature et sa gravité, la santé de la population ". L'article L. 3131-13 du même code précise que " L'état d'urgence sanitaire est déclaré par décret en conseil des ministres pris sur le rapport du ministre chargé de la santé. Ce décret motivé détermine la ou les circonscriptions territoriales à l'intérieur desquelles il entre en vigueur et reçoit application. Les données scientifiques disponibles sur la situation sanitaire qui ont motivé la décision sont rendues publiques. / (...) ". Aux termes de l'article L. 3131-15 du même code : " Dans les circonscriptions territoriales où l'état d'urgence sanitaire est déclaré, le Premier ministre peut, par décret réglementaire pris sur le rapport du ministre chargé de la santé, aux seules fins de garantir la santé publique : / (...) 1° Réglementer ou interdire la circulation des personnes (...) ". Ces mesures " sont strictement proportionnées aux risques sanitaires encourus et appropriées aux circonstances de temps et de lieu. Il y est mis fin sans délai lorsqu'elles ne sont plus nécessaires. "<br/>
<br/>
              4. Par un décret du 14 octobre 2020, pris sur le fondement des articles L. 3131-12 et L. 3131-13 du code de la santé publique, le Président de la République a déclaré l'état d'urgence sanitaire à compter du 17 octobre sur l'ensemble du territoire national. Les 16 et 29 octobre 2020, le Premier ministre a pris, sur le fondement de l'article L. 3131-15 du code de la santé publique, deux décrets prescrivant les mesures générales nécessaires pour faire face à l'épidémie de Covid-19 dans le cadre de l'état d'urgence sanitaire. La loi du 15 février 2021 prorogeant l'état d'urgence sanitaire a prorogé cet état d'urgence jusqu'au 1er juin 2021.<br/>
<br/>
              Sur le droit des citoyens français d'entrer en France :<br/>
<br/>
              5. Il ne peut être porté atteinte au droit fondamental qu'a tout Français de rejoindre le territoire national qu'en cas de nécessité impérieuse pour la sauvegarde de l'ordre public, notamment pour prévenir, de façon temporaire, un péril grave et imminent. La seule circonstance que l'état d'urgence sanitaire ait été déclenché pour protéger d'une pandémie mondiale la population résidant sur le territoire français ne peut, par elle-même, justifier une telle atteinte. Les restrictions de toute nature mises à l'embarquement de Français depuis l'étranger dans un moyen de transport à destination de la France, en vue de préserver la situation sanitaire sur le territoire national, ne peuvent être légalement prises que si le bénéfice, pour la protection de la santé publique excède manifestement l'atteinte ainsi portée au droit fondamental en cause. Elles ne sauraient, en tout état de cause, avoir pour effet de faire durablement obstacle au retour d'un Français sur le territoire national, sans préjudice de la possibilité, pour l'autorité administrative compétente, une fois la personne entrée sur le territoire national, de prendre les mesures que la situation sanitaire justifie, comme, le cas échéant, des mesures de quarantaine.<br/>
<br/>
              Sur l'obligation de présenter à l'embarquement le résultat d'un test réalisé moins de 72 heures avant et ne concluant pas à une contamination par la Covid-19 :<br/>
<br/>
              6. Le décret du 15 janvier 2021 modifiant les décrets du 16 octobre 2020 et du 29 octobre 2020 prescrivant les mesures générales nécessaires pour faire face à l'épidémie de covid-19 dans le cadre de l'état d'urgence sanitaire soumet les Français en provenance d'" un pays étranger autre que ceux de l'Union européenne, Andorre, l'Islande, le Liechtenstein, Monaco, la Norvège, Saint-Marin, le Saint-Siège ou la Suisse " souhaitant rejoindre le territoire métropolitain de la France par transport maritime ou transport public aérien, à l'obligation de présenter à l'embarquement, en premier lieu, le résultat d'un test PCR réalisé moins de 72 heures avant le transport ne concluant pas à une contamination par la Covid-19, en second lieu, " une déclaration sur l'honneur attestant : / 1° Qu'il ne présente pas de symptôme d'infection au covid-19 ; / 2° Qu'il n'a pas connaissance d'avoir été en contact avec un cas confirmé de covid-19 dans les quatorze jours précédant le vol ; / 3° (...) qu'il accepte qu'un test ou un examen biologique de dépistage virologique de détection du SARS-CoV-2 puisse être réalisé à son arrivée sur le territoire national (...). / 4° (...) qu'il s'engage à respecter un isolement prophylactique de sept jours après son arrivée en France métropolitaine " et " à réaliser, au terme de cette période, un examen biologique de dépistage virologique permettant la détection du SARS-CoV-2 ". Enfin, " à défaut de présentation de ces documents, l'embarquement est refusé (...). "<br/>
<br/>
              En ce qui concerne le caractère proportionné de la mesure :<br/>
<br/>
              7. L'Union des Français de l'étranger soutient que ces dispositions portent une atteinte disproportionnée à la liberté d'aller et venir ainsi qu'au droit de mener une vie familiale normale, en tant qu'elles ne permettent pas de remplacer l'obligation d'un test préalable à l'embarquement par un test à l'arrivée, en cas d'indisponibilité des tests, de force majeure ou de motif impérieux. Dans son avis cité au point 12 du 14 janvier 2021 du Haut conseil de la santé publique relatif aux mesures de contrôle et de prévention de la diffusion des nouveaux variants du SARS-CoV-2, le HCSP recommande : " Mesures visant à réduire l'introduction et la diffusion des variants émergents en provenance de l'étranger sur le territoire national : / (...) &#149; Exiger un test de RT-PCR négatif réalisée dans les 72 heures dans le pays de départ avant l'embarquement pour tout voyage à destination de la France. Isolement d'une durée de 7 jours à l'arrivée sur le territoire français. Contrôle de la RT-PCR à l'issue de la période d'isolement pour autoriser la levée de cet isolement en cas de résultat négatif. " Il résulte de l'instruction que cette obligation a pour objet de prévenir l'arrivée sur le sol français d'une personne atteinte du virus et que l'alternative consistant en la réalisation d'un test antigénique à l'arrivée n'a pas le même effet en terme de risque de diffusion du virus pendant le transport comme ensuite, sur le sol français. Les dispositions du décret dont la suspension est demandée ne sauraient toutefois être lues comme faisant obstacle à l'embarquement lorsque la réalisation d'un test préalable s'avère impossible, que ce soit en raison de l'indisponibilité du test dans le pays de départ ou d'une urgence impérieuse à rejoindre le territoire national, tenant à la santé ou la sécurité de la personne, comme le rappelle d'ailleurs la circulaire du 22 février du Premier ministre relative aux " Mesures frontalières mises en oeuvre dans le cadre de l'état d'urgence sanitaire ". Il s'en déduit que le moyen tiré de ce que le décret devait expressément réserver l'hypothèse de la formalité impossible en prévoyant une dispense pour indisponibilité des tests, force majeure ou motif impérieux, ne peut par suite être considéré, en l'état de l'instruction, comme sérieux.<br/>
<br/>
              En ce qui concerne le principe d'égalité :<br/>
<br/>
              8. Il résulte de l'instruction que les déplacements intra-européens, notamment ceux réalisés par les travailleurs frontaliers, sont, par les nécessités auxquelles ils répondent et par leur nombre, sans commune mesure avec ceux effectués depuis la France avec des Etats tiers. Le moyen tiré de ce que le choix de conserver un espace intégré de libre circulation, en n'y exigeant pas, pour l'entrée en France, un test PCR réalisé moins de 72 heures avant l'embarquement ne concluant pas à une contamination par la Covid-19, violerait le principe d'égalité, et alors au demeurant que la situation sanitaire du territoire métropolitain de la France est comparable à celle de ses voisins, ne peut être considéré, en l'état de l'instruction, comme sérieux. Cette obligation a d'ailleurs été étendue aux déplacements vers la France en provenance de tous les pays du monde par les décrets des 23 et 30 janvier 2021 modifiant les décrets du 16 octobre 2020 et du 29 octobre 2020 prescrivant les mesures générales nécessaires pour faire face à l'épidémie de covid-19 dans le cadre de l'état d'urgence sanitaire.<br/>
<br/>
              9. Aucun des autres moyens soulevés n'est davantage, en l'état de l'instruction, propre à faire naître un doute sérieux quant à la légalité des dispositions contestées. Il en résulte que, sans qu'il soit besoin de se prononcer sur l'urgence, l'Union des Français de l'étranger n'est pas fondée à demander la suspension de l'exécution du décret du 15 janvier 2021 en tant qu'il impose à tous les Français voulant rejoindre le territoire métropolitain depuis un pays autre qu'un pays membre de l'Union européenne, Andorre, l'Islande, le Liechtenstein, Monaco, la Norvège, Saint-Marin, le Saint-Siège ou la Suisse, de présenter le résultat d'un examen biologique de dépistage virologique réalisé moins de 72 heures avant le transport et ne concluant pas à une contamination par la Covid-19.<br/>
<br/>
              Sur l'obligation de justifier pour entrer sur le territoire français d'un motif impérieux d'ordre personnel ou familial, un motif de santé relevant de l'urgence ou un motif professionnel ne pouvant être différé :<br/>
<br/>
              10. Aux termes des articles 57-2 du décret du 16 octobre 2020 et 56-5 du décret du 29 octobre 2020, tous deux créés par le décret du 30 janvier 2021 modifiant les décrets du 16 octobre 2020 et du 29 octobre 2020 prescrivant les mesures générales nécessaires pour faire face à l'épidémie de covid-19 dans le cadre de l'état d'urgence sanitaire : " I.- Sont interdits, sauf s'ils sont fondés sur un motif impérieux d'ordre personnel ou familial, un motif de santé relevant de l'urgence ou un motif professionnel ne pouvant être différé, les déplacements de personnes : / 1° Entre le territoire métropolitain et un pays étranger autre que ceux de l'Union européenne, Andorre, l'Islande, le Liechtenstein, Monaco, la Norvège, Saint-Marin, le Saint-Siège ou la Suisse ; / (...). / " II.- Les personnes souhaitant bénéficier de l'une des exceptions mentionnées au premier alinéa du I doivent se munir d'un document permettant de justifier du motif de leur déplacement. Lorsque le déplacement est opéré par une entreprise de transport, la personne présente, avant l'embarquement, une déclaration sur l'honneur du motif de son déplacement, accompagnée de ce document. A défaut, l'embarquement est refusé et la personne est reconduite à l'extérieur des espaces concernés. (...) " Il résulte de ces dispositions, qui sont intelligibles, que tout Français souhaitant revenir en France par transport maritime ou transport public aérien se verra refuser l'embarquement s'il ne produit pas un document justifiant d'un motif impérieux d'ordre personnel ou familial, un motif de santé relevant de l'urgence ou un motif professionnel ne pouvant être différé. Il en résulte également que l'exigence de justifier d'un des motifs énumérés par le décret ne vise pas à interdire tout retour des ressortissants français en France, mais à différer ou éviter les voyages internationaux, dans un contexte de pandémie mondiale, en vue de minimiser les risques sanitaires pour la population vivant sur le territoire national.<br/>
<br/>
              11. Les requérants soutiennent que ces dispositions portent atteinte à la liberté d'aller et venir, au droit absolu de revenir sur le territoire national résultant notamment de l'article 3 du protocole n° 4 à la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, au droit au respect de la vie privée et au droit de mener une vie familiale normale, tant par la limitation des déplacements qu'elles imposent que par le fait, en cas de recours à une entreprise de transport, de devoir justifier du motif de son retour en France auprès d'une personne privée établie dans un pays étranger.<br/>
<br/>
              12. Les requérants font en outre état, d'une part, du nombre minime de déplacements que les Français de l'étranger sont susceptibles de réaliser vers la France pour des motifs autres que ceux prévus par le décret, en regard du nombre d'entrées quotidiennes sur le territoire métropolitain en provenance des pays bénéficiant de la libre circulation. Ils soulignent, d'autre part, que nombre des pays où résident les Français désormais soumis à l'obligation de justifier du motif de leur déplacement sont dans une situation sanitaire meilleure que les pays bénéficiant de la libre circulation. Ils en déduisent que la mesure n'est en tout état de cause pas de nature à produire d'impact discernable sur la situation sanitaire en France et n'est, par suite, pas nécessaire.<br/>
              En ce qui concerne les déplacements en provenance de certains pays :<br/>
<br/>
              13. Le décret du 11 mars 2021 modifiant les décrets du 16 octobre 2020 et du 29 octobre 2020 prescrivant les mesures générales nécessaires pour faire face à l'épidémie de covid-19 dans le cadre de l'état d'urgence sanitaire a ajouté à la liste, mentionnée au point 10, des pays pour lesquels aucun motif de déplacement n'est exigé, l'Australie, la Corée du Sud, Israël, le Japon, le Liechtenstein, la Nouvelle-Zélande, le Royaume-Uni, et Singapour. Il n'y a plus lieu, pour le juge des référés saisi sur le fondement de l'article L. 521-1 du code de justice administrative, de statuer sur la demande des requérants en tant qu'elle porte sur les déplacements entre le territoire métropolitain et ces pays.<br/>
<br/>
              En ce qui concerne la nécessité et la proportionnalité de la mesure : <br/>
<br/>
              14. Il résulte de l'instruction qu'en l'état actuel des connaissances scientifiques, le virus de la Covid-19 peut se transmettre par gouttelettes respiratoires, par contacts et par voie aéroportée et que les personnes peuvent être contagieuses sans le savoir, notamment pendant la phase pré-symptomatique. Dans une note du 12 décembre 2020 dernier, le comité de scientifiques a relevé que les porteurs asymptomatiques du virus étaient responsables d'environ 40 à 50% des nouvelles contaminations. La transmission des virus respiratoires tels que le Sars-CoV-2, par gouttelettes et aérosols, est favorisée par le brassage de population, la densité de population, le temps de contact avec des personnes potentiellement contaminées et la ventilation des locaux. Les déplacements internationaux sont des lieux propices à la transmission du virus responsable de la Covid-19. Il résulte en outre de l'instruction qu'au jour où ce décret a été signé, le taux d'incidence du virus de la Covid-19 était en hausse et que les taux d'hospitalisation et d'admission en réanimation de patients atteints de cette maladie demeuraient élevés. Le plateau épidémique élevé observé alors résultait en outre de deux dynamiques opposées : une circulation décroissante de la souche historique du virus, opposée à l'augmentation exponentielle du variant dit britannique, à la contagiosité beaucoup plus importante ayant justifié un nouveau confinement de la population au Royaume-Uni, ce variant étant appelé à devenir majoritaire à brève échéance sur le territoire national. En outre, si les variants dit sud-africain et brésilien demeuraient minoritaires, leur contagiosité est extrêmement forte. Enfin, et dans l'attente du moment où la vaccination de la population commencerait à produire des effets mesurables, le Gouvernement a entendu prendre les mesures nécessaires pour éviter un nouveau confinement.<br/>
<br/>
              15. Il résulte toutefois de l'instruction que, s'il est difficile de mesurer avec exactitude le nombre de déplacements depuis l'étranger vers le territoire national, le nombre de déplacements de Français qui se voient ainsi interdits par le décret n'est pas de nature à faire diminuer de manière significative le nombre total d'entrées sur le territoire métropolitain en provenance de l'étranger. <br/>
<br/>
              16. Il résulte de tout ce qui précède qu'en l'état de l'instruction, le moyen tiré de ce que l'atteinte ainsi portée au droit fondamental qu'a tout Français d'accéder au territoire national ne serait ni nécessaire, ni proportionnée, est de nature à faire naître un doute sérieux quant à la légalité de la disposition contestée. Il y a lieu, par suite, vu l'urgence, de suspendre l'exécution des articles 57-2 du décret du 16 octobre 2020 et 56-5 du décret du 29 octobre 2020 en tant qu'ils interdisent, sauf pour des motifs limitativement énumérés, l'entrée sur le territoire métropolitain d'un Français en provenance d'un pays étranger autre que ceux de l'Union européenne, Andorre, l'Australie, la Corée du Sud, l'Islande, Israël, le Japon, le Liechtenstein, Monaco, la Norvège, la Nouvelle-Zélande, le Royaume-Uni, Saint-Marin, le Saint-Siège, Singapour ou la Suisse.<br/>
<br/>
              17. Il y a lieu, dans les circonstances de l'espèce, d'ordonner le versement par l'Etat à l'Union des Français de l'étranger d'une somme de 3 000 euros, au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
              ------------------<br/>
<br/>
Article 1er : Il n'y a pas lieu de statuer sur les demandes de M. B... et de l'Union des Français de l'étranger tendant à la suspension de l'exigence imposée aux Français de justifier d'un motif pour entrer sur le territoire métropolitain depuis l'Australie, la Corée du Sud, Israël, le Japon, le Liechtenstein, la Nouvelle-Zélande, le Royaume-Uni, et Singapour.<br/>
Article 2 : L'exécution des articles 57-2 du décret du 16 octobre 2020 et 56-5 du décret du 29 octobre 2020, issus du décret du 30 janvier 2021, est suspendue en tant qu'ils interdisent, sauf pour des motifs limitativement énumérés, l'entrée sur le territoire métropolitain d'un Français en provenance d'un pays étranger autre que ceux de l'Union européenne, Andorre, l'Australie, la Corée du Sud, l'Islande, Israël, le Japon, le Liechtenstein, Monaco, la Norvège, la Nouvelle-Zélande, le Royaume-Uni, Saint-Marin, le Saint-Siège, Singapour ou la Suisse.<br/>
Article 3 : Les conclusions de la requête de l'Union des Français de l'étranger tendant à la suspension de l'exécution des dispositions du décret du 15 janvier 2021 modifiant les décrets du 16 octobre 2020 et du 29 octobre 2020 prescrivant les mesures générales nécessaires pour faire face à l'épidémie de covid-19 dans le cadre de l'état d'urgence sanitaire sont rejetées.<br/>
Article 4 : L'Etat versera la somme de 3 000 euros à l'Union des Français de l'étranger, au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 5 : La présente ordonnance sera notifiée à M. A... B..., à l'Union des Français de l'étranger et au ministre des solidarités et de la santé. <br/>
Copie en sera adressée au Premier ministre, au ministre de l'intérieur et au ministre de l'Europe et des affaires étrangères.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
