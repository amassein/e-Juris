<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026529778</ID>
<ANCIEN_ID>JG_L_2012_10_000000329636</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/52/97/CETATEXT000026529778.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème et 5ème sous-sections réunies, 19/10/2012, 329636</TITRE>
<DATE_DEC>2012-10-19</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>329636</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème et 5ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROGER, SEVAUX ; SCP MASSE-DESSEN, THOUVENIN</AVOCATS>
<RAPPORTEUR>M. Hervé Guichon</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Rémi Keller</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2012:329636.20121019</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 10 juillet et 12 octobre 2009 au secrétariat du contentieux du Conseil d'Etat, présentés pour la commune de Molsheim, représentée par son maire ; la commune de Molsheim demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 08NC00536 du 7 mai 2009 par lequel la cour administrative d'appel de Nancy a rejeté sa requête tendant à l'annulation du jugement n° 0501651 du 12 février 2008 du tribunal administratif de Strasbourg en tant qu'il a annulé la décision du 30 mars 2005 par laquelle le maire de la commune a opéré une retenue sur le traitement de Mme B...A... ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de Mme A...la somme de 1 500 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la loi n° 61-825 du 29 juillet 1961, notamment son article 4, modifié par la loi n° 77-826 du 22 juillet 1977 ;<br/>
<br/>
              Vu la loi n° 83-634 du 13 juillet 1983, notamment son article 20 ;<br/>
<br/>
              Vu la loi n° 84-53 du 26 janvier 1984, notamment son article 87 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Hervé Guichon, Maître des Requêtes en service extraordinaire,  <br/>
<br/>
              - les observations de la SCP Masse-Dessen, Thouvenin avocat de Mme A...et de la SCP Roger, Sevaux avocat de la commune de Molsheim ;<br/>
<br/>
              - les conclusions de M. Rémi Keller, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Masse-Dessen, Thouvenin avocat de Mme A...et à la SCP Roger, Sevaux avocat de la commune de Molsheim ;<br/>
<br/>
<br/>
<br/>
<br/>
		1. Considérant qu'il résulte des dispositions de l'article R. 811-1 du code de justice administrative, combinées avec celles de l'article R. 222-13 du même code, que le tribunal administratif statue en premier et dernier ressort dans les litiges relatifs à la situation individuelle des agents publics à l'exception de ceux concernant l'entrée au service, la discipline ou la sortie du service, sauf pour les recours comportant des conclusions tendant au versement ou à la décharge de sommes d'un montant supérieur au montant déterminé par les articles R. 222-14 et R. 222-15 de ce code ;<br/>
<br/>
		2. Considérant que le litige porté devant le tribunal administratif de Strasbourg est relatif à une retenue sur traitement et ne concerne ni l'entrée ou la sortie du service, ni une procédure disciplinaire ; que la somme de 6 812,48 euros demandée par Mme A... au tribunal administratif de Strasbourg était inférieure au seuil déterminé par l'article R. 222-14 du code de justice administrative ; qu'il en résulte que le jugement du 12 février 2008 par lequel le tribunal administratif de Strasbourg a annulé la décision du 30 mars 2005 par laquelle le maire de la commune a opéré une retenue sur le traitement de Mme A... a été rendu en premier et dernier ressort et n'était susceptible que d'un pourvoi en cassation devant le Conseil d'Etat ; que la cour administrative d'appel de Nancy était par suite incompétente pour statuer par la voie de l'appel sur ce jugement ; que dès lors, sans qu'il soit besoin d'examiner les moyens du pourvoi, son arrêt doit être annulé ;<br/>
		3. Considérant qu'il y a lieu de regarder les conclusions présentées par la commune de Molsheim devant la cour administrative d'appel de Nancy comme des conclusions de cassation dirigées contre le jugement du 12 février 2008 du tribunal administratif de Strasbourg ;<br/>
<br/>
		4. Considérant qu'aux termes de l'article 20 de la loi du 13 janvier 1983 portant droits et obligations des fonctionnaires, auquel renvoie l'article 87 de la loi du 26 janvier 1984 portant dispositions statutaires relatives à la fonction publique territoriale : " Les fonctionnaires ont droit, après service fait, à une rémunération (...) " ;<br/>
<br/>
		5. Considérant que si, en vertu du 2° inséré à l'article 4 de la loi du 29 juillet 1961 par la loi du 22 juillet 1977,  il n'y a pas de service fait " Lorsque l'agent, bien qu'effectuant ses heures de service, n'exécute pas tout ou partie des obligations de service qui s'attachent à sa fonction telles qu'elles sont définies dans leur nature et leurs modalités par l'autorité compétente dans le cadre des lois et règlements. ", il résulte du premier alinéa du même article, qui se réfère aux traitements exigibles en application de l'article 22 de l'ordonnance du 4 février 1959, qu'il est applicable aux seuls fonctionnaires de l'Etat et de ses établissements publics ; que cette définition de l'absence de service fait pouvant donner lieu à retenue sur traitement ne saurait, par suite, être appliquée aux fonctionnaires des collectivités territoriales ; <br/>
<br/>
		6. Considérant qu'il résulte de ce qui précède que, si l'absence de service fait par un fonctionnaire d'une collectivité territoriale peut donner lieu à une retenue sur rémunération proportionnelle à cette absence, cette retenue ne peut être opérée que dans l'hypothèse où le fonctionnaire s'est abstenu d'effectuer tout ou partie de ses heures de service ; <br/>
<br/>
		7. Considérant que, pour procéder à une retenue sur le traitement de Mme A..., le maire de Molsheim s'est fondé sur la circonstance que celle-ci avait passé, depuis son poste de travail, des appels téléphoniques personnels ; qu'il résulte de ce qui précède qu'en jugeant, après avoir relevé qu'il n'était pas allégué que Mme A...n'avait pas accompli pendant la période en cause la totalité de ses heures de service, que celle-ci ne pouvait être privée du droit de percevoir l'intégralité de ses rémunérations, le tribunal administratif de Strasbourg n'a pas commis d'erreur de droit ; que, par suite, la commune de Molsheim n'est pas fondée à demander l'annulation du jugement attaqué en tant qu'il a annulé la décision du 30 mars 2005 par laquelle le maire de la commune a opéré une retenue sur le traitement de MmeA... ;<br/>
<br/>
		8. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mis à la charge de Mme A..., qui n'est pas, dans la présente instance, la partie perdante, le versement de la somme que demande la commune requérante ; qu'il y a lieu, en revanche, dans les circonstances de l'espèce, de mettre à la charge de cette commune, en application de ces mêmes dispositions, le versement de la somme de 3 500 euros à Mme A... au titre des frais engagés par elle et non compris dans les dépens ;   <br/>
<br/>
<br/>
<br/>
<br/>
              			D E C I D E :<br/>
              			--------------<br/>
<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Nancy du 7 mai 2009 est annulé.  <br/>
<br/>
Article 2 : Le surplus des conclusions du pourvoi de la commune de Molsheim est rejeté.<br/>
<br/>
Article 3 : La commune de Molsheim versera à Mme A...une somme de 3 500 euros sur le fondement des dispositions de l'article L.761-1 du code de justice administrative.<br/>
<br/>
Article 4 : La présente décision sera notifiée à la commune de Molsheim et à Mme B...A....<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">36-08-02-01-01 FONCTIONNAIRES ET AGENTS PUBLICS. RÉMUNÉRATION. TRAITEMENT. RETENUES SUR TRAITEMENT. RETENUES SUR TRAITEMENT POUR ABSENCE DU SERVICE FAIT. - 1) 2° DE L'ARTICLE 4 DE LA LOI DU 29 JUILLET 1961 - CHAMP D'APPLICATION - FONCTION PUBLIQUE DE L'ETAT - INCLUSION - AUTRES FONCTIONS PUBLIQUES - EXCLUSION [RJ1] - 2) FONCTIONNAIRE TERRITORIAL - NOTION D'ABSENCE DE SERVICE FAIT - ABSTENTION D'EFFECTUER TOUT OU PARTIE DES HEURES DE SERVICE [RJ2] - SANCTION - RETENUE PROPORTIONNELLE À CETTE ABSENCE.
</SCT>
<ANA ID="9A"> 36-08-02-01-01 1) Le 2° de l'article 4 de la loi n° 61-825 du 29 juillet 1961, qui prévoit qu'il y a absence de service fait pouvant donner lieu à retenue sur traitement, bien que les heures de service soient effectuées, lorsque ne sont pas exécutées tout ou partie des obligations de service, est applicable aux seuls fonctionnaires de l'Etat et de ses établissements publics. 2) En conséquence, un fonctionnaire d'une collectivité territoriale ne peut faire l'objet d'une retenue sur rémunération pour absence de service fait, proportionnelle à cette absence, que dans l'hypothèse où il s'est abstenu d'effectuer tout ou partie de ses heures de service.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 27 avril 1994, Service départemental d'incendie et de secours de la Haute-Garonne, n° 146119, p. 197.,  ,[RJ2] Cf., s'agissant de ce principe, applicable à la fonction publique de l'Etat avant l'édiction du 2° de l'article 4 de la loi du 29 juillet 1961, CE, Assemblée, 20 mai 1977, Ministre de l'Education c/ Quinteau et autres, n° 01827, p. 230.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
