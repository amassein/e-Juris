<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042391401</ID>
<ANCIEN_ID>JG_L_2020_10_000000433654</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/39/14/CETATEXT000042391401.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème chambre, 01/10/2020, 433654, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-10-01</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>433654</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Airelle Niepce</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Olivier  Fuchs</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2020:433654.20201001</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, un mémoire en réplique et trois nouveaux mémoires, enregistrés les 18 août 2019, 4 janvier, 14 juin, 13 juillet et 9 août 2020 au secrétariat du contentieux du Conseil d'État, M. A... D... demande au Conseil d'État d'annuler pour excès de pouvoir l'ordonnance n° 2019-740 du 17 juillet 2019 relative aux sanctions civiles applicables en cas de défaut ou d'erreur du taux effectif global.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la Constitution, notamment son Préambule et son article 61-1 ;<br/>
              - le traité sur le fonctionnement de l'Union européenne ;<br/>
              - la directive 2008/48/CE du Parlement européen et du Conseil du 23 avril 2008 ;<br/>
              - la directive 2014/17/UE du Parlement européen et du Conseil du 4 février 2014 ;<br/>
              - l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
              - le code de la consommation ;<br/>
              - le code monétaire et financier ;<br/>
              - la loi n° 2018-727 du 10 août 2018 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme C... B..., maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Olivier Fuchs, rapporteur public ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 6 septembre 2020, présentée par M. D... ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes du I de l'article 55 de la loi du 10 août 2018 pour un Etat au service d'une société de confiance : " Dans les conditions prévues à l'article 38 de la Constitution, le Gouvernement est habilité à prendre par ordonnances, dans un délai de douze mois à compter de la promulgation de la présente loi, toute mesure relevant du domaine de la loi visant à modifier les dispositions du code de la consommation et du code monétaire et financier relatives au taux effectif global et à prévoir les mesures de coordination et d'adaptation découlant de ces modifications en vue : / 1° D'une part, excepté dans le cas des contrats de crédit à taux fixe, de supprimer la mention obligatoire du taux effectif global dans les contrats de crédit aux entreprises lorsque cette mention est inappropriée à ces contrats ; / 2° D'autre part, de clarifier et d'harmoniser le régime des sanctions civiles applicables en cas d'erreur ou de défaut de ce taux, en veillant en particulier, conformément aux exigences énoncées par la directive 2008/48/CE du Parlement européen et du Conseil du 23 avril 2008 concernant les contrats de crédit aux consommateurs et abrogeant la directive 87/102/ CEE du Conseil et par la directive 2014/17/UE du Parlement européen et du Conseil du 4 février 2014 sur les contrats de crédit aux consommateurs relatifs aux biens immobiliers à usage résidentiel et modifiant les directives 2008/48/CE et 2013/36/UE et le règlement (UE) n° 1093/2010, au caractère proportionné de ces sanctions civiles au regard des préjudices effectivement subis par les emprunteurs. / (...) ". L'ordonnance du 17 juillet 2019 relative aux sanctions civiles applicables en cas de défaut ou d'erreur du taux effectif global, dont M. A... D... demande l'annulation pour excès de pouvoir, a été prise sur le fondement de cette disposition. Au soutien de sa requête, M. D... présente, par un mémoire distinct, une question prioritaire de constitutionnalité.<br/>
<br/>
              2. M. D... fait état, pour justifier de son intérêt à demander l'annulation de l'ordonnance du 17 juillet 2019, d'un litige pendant devant une juridiction judiciaire relatif à un contrat de prêt qu'il a conclu avec une banque antérieurement à l'entrée en vigueur de l'ordonnance et à l'occasion duquel la juridiction serait susceptible de faire application des dispositions de l'ordonnance relative au régime des sanctions civiles applicables en cas d'erreur ou de défaut de mention du taux effectif global. Toutefois, l'ordonnance du 17 juillet 2019 ne s'applique pas aux contrats conclus avant son entrée en vigueur, qui demeurent régis par la loi en vigueur au jour de leur conclusion. Par suite, M. D... ne justifie pas d'un intérêt lui donnant qualité pour demander l'annulation de l'ordonnance du 17 juillet 2019. Dès lors, sans qu'il y ait lieu pour le Conseil d'État de se prononcer sur le renvoi de la question prioritaire de constitutionnalité soulevée par M. D..., sa requête est irrecevable et ne peut qu'être rejetée.<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de M. D... est rejetée.<br/>
Article 2 : La présente décision sera notifiée à M. A... D... et au ministre de l'économie, des finances et de la relance.<br/>
Copie en sera adressée au Premier ministre, au ministre des outre-mer, au garde des sceaux, ministre de la justice, et au Conseil constitutionnel.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
