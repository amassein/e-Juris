<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044588599</ID>
<ANCIEN_ID>JG_L_2021_12_000000441820</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/58/85/CETATEXT000044588599.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème chambre, 27/12/2021, 441820, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-12-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>441820</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP WAQUET, FARGE, HAZAN</AVOCATS>
<RAPPORTEUR>M. Charles-Emmanuel Airy</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Karin Ciavaldini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:441820.20211227</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une décision du 22 novembre 2021, le Conseil d'Etat, statuant au contentieux, avant de statuer sur la demande de M. A... B... tendant à ce qu'il soit constaté que la créance déclarée le 6 mars 2015 par le comptable du pôle de recouvrement spécialisé parisien n° 1 (Paris Nord-Est) à la procédure de redressement judiciaire ouverte à son encontre portant le numéro 13 dans la liste établie par le mandataire judiciaire et d'un montant de 7 695,53 euros était atteinte par la prescription de l'action en recouvrement à la date de sa déclaration, a ordonné qu'il soit procédé à un supplément d'instruction visant à la production, par la partie la plus diligente, des avis à tiers détenteurs des 24 novembre 2010 et 18 janvier 2011 dont il était fait état par les parties devant le tribunal administratif.  <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier, y compris celles visées par la décision du Conseil d'État du 22 novembre 2021 ;<br/>
<br/>
              Vu : <br/>
              - le code civil ;<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Charles-Emmanuel Airy, auditeur,  <br/>
<br/>
              - les conclusions de Mme Karin Ciavaldini, rapporteure publique ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Waquet, Farge, Hazan, avocat de M. B... ;<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 274 du livre des procédures fiscales : " Les comptables publics des administrations fiscales qui n'ont fait aucune poursuite contre un redevable pendant quatre années consécutives à compter du jour de la mise en recouvrement du rôle ou de l'envoi de l'avis de mise en recouvrement sont déchus de tous droits et de toute action contre ce redevable ". L'article 2244 du code civil précise notamment que le délai de prescription est interrompu par un acte d'exécution forcée.<br/>
<br/>
              2. Il résulte de l'instruction que la créance déclarée le 6 mars 2015 sous la désignation de " créance n° 13 " par le comptable du pôle de recouvrement spécialisé parisien n° 1 (Paris Nord-Est) à la procédure de redressement judiciaire ouverte à l'encontre de M. B..., qui porte sur des rappels de taxe sur la valeur ajoutée d'un montant total initial de 7 695,53 euros au titre des mois de mai et juin 2006, a été mise en recouvrement le 15 novembre 2006 par un avis de mise en recouvrement n° 061100013 régulièrement notifié.<br/>
<br/>
              3. Il résulte également de l'instruction, et notamment des pièces produites par l'administration le 29 novembre 2021 en réponse au supplément d'instruction, que l'administration a régulièrement notifié à M. B..., le 8 décembre 2006, une mise en demeure de payer datée du 6 décembre 2006, portant le numéro 061105012, qui visait cette créance, identifiée par le numéro de l'avis de mise en recouvrement mentionné ci-dessus. Le 29 novembre 2010, M. B... a reçu notification d'un avis à tiers détenteur délivré le 24 novembre 2010 à l'établissement bancaire teneur de son compte en vue du recouvrement, notamment, de cette même créance identifiée par le numéro de la mise en demeure du 6 décembre 2006. Enfin, M. B... a reçu notification le 18 décembre 2013 d'un avis à tiers détenteur délivré le 13 décembre 2013 à plusieurs établissements bancaires teneurs de ses comptes en vue du recouvrement de cette même créance, également identifiée par le numéro de la mise en demeure du 6 décembre 2006. <br/>
<br/>
              4. Il résulte de ce qui précède que le cours de la prescription de l'action en recouvrement de la créance n° 13 en litige a été interrompu les 8 décembre 2006, 29 novembre 2010 et 18 décembre 2013 par la notification régulière d'actes d'exécution forcée. Cette créance était par suite, contrairement à ce que soutient M. B..., toujours exigible le 6 mars 2015, date de sa déclaration à la procédure collective dont il a fait l'objet. La demande de M. B... ne peut ainsi qu'être rejetée.<br/>
<br/>
              5. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Les conclusions de la demande de M. B... tendant à ce qu'il soit constaté que la créance n° 13 était atteinte par la prescription de l'action en recouvrement à la date de sa déclaration à la procédure collective dont il a fait l'objet ainsi que ses conclusions tendant à la mise en œuvre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 2 : La présente décision sera notifiée à M. A... B... et au ministre de l'économie, des finances et de la relance. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
