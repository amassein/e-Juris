<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036621277</ID>
<ANCIEN_ID>JG_L_2018_02_000000393267</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/62/12/CETATEXT000036621277.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème chambre, 16/02/2018, 393267, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-02-16</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>393267</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MATUCHANSKY, POUPOT, VALDELIEVRE</AVOCATS>
<RAPPORTEUR>M. Vincent Uher</RAPPORTEUR>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CECHS:2018:393267.20180216</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société par actions simplifiée Energies Var 3 a demandé au tribunal administratif de Nice l'annulation de l'arrêté du 6 septembre 2011 par lequel le préfet des Alpes-Maritimes a retiré son arrêté du 21 juillet 1983 l'autorisant à disposer de l'énergie du fleuve Var au niveau du seuil n° 9, sur le territoire de la commune de Castagniers (Alpes-Maritimes), et à établir les ouvrages nécessaires à l'exploitation d'une micro-centrale hydroélectrique. Par un jugement n° 1104530 du 17 décembre 2013, ce tribunal a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 14MA00758 du 7 juillet 2015, la cour administrative d'appel de Marseille a rejeté l'appel formé  par la société Energies Var 3 contre ce jugement.  <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 7 septembre et 7 décembre 2015 au secrétariat du contentieux du Conseil d'Etat, la société Energies Var 3 demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'environnement ;<br/>
              - le code de justice administrative ;		<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Vincent Uher, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Romain Victor, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Matuchansky, Poupot, Valdelièvre, avocat de la société Energies Var 3.<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que, par un arrêté du 21 juillet 1983, le préfet des Alpes-Maritimes a autorisé la société Sithe et Cie, aux droits de laquelle est venue la société Energies Var 3, à disposer de l'énergie du fleuve Var et à exploiter une micro-centrale hydroélectrique située au niveau du seuil n° 9, sur le territoire de la commune de Castagniers (Alpes-Maritimes), pour une période de quarante-cinq ans. Par un arrêté du 6 septembre 2011, le préfet des Alpes-Maritimes a retiré cette autorisation. Par un jugement du 17 décembre 2013, le tribunal administratif de Nice a rejeté la demande de la société Energies Var 3 tendant à l'annulation de ce dernier arrêté. La société Energies Var 3 se pourvoit en cassation contre l'arrêt du 7 juillet 2015 par lequel la cour administratif d'appel de Marseille a rejeté l'appel qu'elle avait formé contre ce jugement.<br/>
<br/>
              2. Aux termes du II de l'article L. 214-4 du code de l'environnement, dans sa rédaction alors applicable : " L'autorisation peut être retirée ou modifiée, sans indemnité de la part de l'Etat exerçant ses pouvoirs de police, dans les cas suivants : 1° Dans l'intérêt de la salubrité publique, et notamment lorsque ce retrait ou cette modification est nécessaire à l'alimentation en eau potable des populations ; / 2° Pour prévenir ou faire cesser les inondations ou en cas de menace pour la sécurité publique ; / 3° En cas de menace majeure pour le milieu aquatique, et notamment lorsque les milieux aquatiques sont soumis à des conditions hydrauliques critiques non compatibles avec leur préservation ; / 4° Lorsque les ouvrages ou installations sont abandonnés ou ne font plus l'objet d'un entretien régulier ". <br/>
<br/>
              3. En jugeant, après avoir relevé que les installations de la société Energies Var 3 faisaient obstacle à l'écoulement des eaux du fleuve Var, que ce motif justifiait le retrait de l'autorisation sur le fondement du 2° du II de l'article L. 214-4 du code de l'environnement, sans rechercher si cet obstacle à l'écoulement des eaux avait une incidence, dans les circonstances de l'espèce, sur le risque d'inondation et si, par suite, sa suppression était nécessaire pour prévenir ou faire cesser les inondations, la cour a insuffisamment motivé son arrêt et méconnu ces dispositions. Par suite, et sans qu'il soit besoin de se prononcer sur les autres moyens, son arrêt doit être annulé.<br/>
<br/>
              4. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat une somme de 3 000 euros à verser à la société Energies Var 3 au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>                      D E C I D E :<br/>
                                   --------------<br/>
<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Marseille du 7 juillet 2015 est annulé.<br/>
<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Marseille.<br/>
<br/>
Article 3 : L'Etat versera à la société Energies Var 3 une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : La présente décision sera notifiée à la société Energies Var 3 et au ministre d'Etat, ministre de la transition écologique et solidaire. <br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
