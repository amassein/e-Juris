<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036411931</ID>
<ANCIEN_ID>JG_L_2017_12_000000415281</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/41/19/CETATEXT000036411931.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème chambre, 28/12/2017, 415281, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-12-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>415281</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème chambre</FORMATION>
<TYPE_REC>Autres</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BRIARD</AVOCATS>
<RAPPORTEUR>Mme Karin Ciavaldini</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Benoît Bohnert</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2017:415281.20171228</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société à responsabilité limitée Lupa Immobilière France, à l'appui de sa défense contre l'appel formé par le ministre des finances et des comptes publics contre le jugement n° 1105857 du 18 juillet 2012 par lequel le tribunal administratif de Paris a prononcé la décharge de la cotisation supplémentaire d'impôt sur les sociétés mise à sa charge au titre de l'exercice clos en 2006 ainsi que des pénalités correspondantes, a produit un mémoire, enregistré le 15 août 2017 au greffe de la cour administrative d'appel de Paris, en application de l'article 23-1 de l'ordonnance n° 58-1067 du 7 novembre 1958, par lequel elle soulève une question prioritaire de constitutionnalité.<br/>
<br/>
              Par une ordonnance n° 16PA02400-3 du 24 octobre 2017, enregistrée le 26 octobre 2017 au secrétariat du contentieux du Conseil d'Etat, le président de la 5ème chambre de cette cour, avant qu'il soit statué sur l'appel du ministre, a décidé, par application des dispositions de l'article 23-2 de l'ordonnance du 7 novembre 1958, de transmettre au Conseil d'Etat la question de la conformité aux droits et libertés garantis par la Constitution des dispositions de l'article L. 57 du livre des procédures fiscales.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la Constitution, notamment son préambule et son article 61-1 ;<br/>
              - l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Karin Ciavaldini, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Benoît Bohnert, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Briard, avocat de la société Lupa Immobilière France.<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il résulte des dispositions de l'article 23-4 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel que, lorsqu'une juridiction relevant du Conseil d'Etat a transmis à ce dernier, en application de l'article 23-2 de cette même ordonnance, la question de la conformité à la Constitution d'une disposition législative, le Conseil constitutionnel est saisi de cette question de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement de circonstances, et que la question soit nouvelle ou présente un caractère sérieux.<br/>
<br/>
              2. Aux termes de l'article L. 57 du livre des procédures fiscales : " L'administration adresse au contribuable une proposition de rectification qui doit être motivée de manière à lui permettre de formuler ses observations ou de faire connaître son acceptation. / (...) Lorsque l'administration rejette les observations du contribuable sa réponse doit également être motivée ".<br/>
<br/>
              3. La société Lupa Immobilière France soutient que les dispositions de l'article L. 57 du livre des procédures fiscales, dans la portée qui leur est conférée par la jurisprudence du Conseil d'Etat, méconnaissent le principe d'égalité devant la loi énoncé à l'article 6 de la Déclaration des droits de l'homme et du citoyen de 1789, en tant qu'elles ne font pas obstacle à ce que l'administration, au cours de la procédure contentieuse introduite par le contribuable, modifie ou sollicite du juge qu'il modifie par une substitution de base légale le fondement des droits mis à la charge de ce contribuable, tel que l'administration l'avait mentionné dans la proposition de rectification qu'elle est tenue de lui notifier au cours de la procédure de redressement.<br/>
<br/>
              4. Il résulte d'une jurisprudence constante du Conseil d'Etat que l'administration est en droit, à tout moment de la procédure contentieuse suivie devant le juge de l'impôt, de justifier l'imposition en substituant une base légale à une autre, sous réserve que le contribuable ne soit pas privé des garanties de procédure qui lui sont données par la loi compte tenu de la base légale substituée. Eu égard aux garanties attachées à la procédure juridictionnelle, la jurisprudence ne subordonne pas la possibilité, pour l'administration, de solliciter devant le juge une telle substitution de base légale à la condition que la nouvelle base légale ait été mentionnée dans la proposition de rectification adressée au contribuable en application des dispositions de l'article L. 57 du livre des procédures fiscales.<br/>
<br/>
              5. Dès lors que, indépendamment de la base légale initialement retenue par l'administration pendant la procédure administrative d'établissement de l'impôt, les contribuables concernés par une demande de substitution de base légale disposent de la possibilité de débattre contradictoirement, devant le juge de l'impôt, de la régularité et du bien fondé de l'imposition mise à leur charge dans des conditions qui leur assurent des garanties équivalentes à celles que prévoient les dispositions de l'article L. 57 du livre des procédures fiscales, ces contribuables ne sont pas traités, au regard du respect de ces garanties, de manière moins favorable que les contribuables auxquels la base légale jugée pertinente par l'administration a été appliquée dès cette procédure administrative. Par suite, alors en outre que tous les contribuables sont également susceptibles d'être concernés par une demande de substitution de base légale devant le juge de l'impôt dans les mêmes conditions, l'article L. 57 du livre des procédures fiscales, en ce qu'il ne fait pas obstacle à la faculté dont dispose l'administration de solliciter une substitution de base légale au cours de la procédure contentieuse qui l'oppose à un contribuable, ne méconnaît pas le principe d'égalité devant la loi énoncé par l'article 6 de la Déclaration des droits de l'homme et du citoyen de 1789. <br/>
<br/>
              6. Ainsi, la question soulevée, qui n'est pas nouvelle, est dépourvue de caractère sérieux. Par suite, il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité invoquée.<br/>
<br/>
<br/>
<br/>             D E C I D E :<br/>
                          --------------<br/>
<br/>
Article 1er : Il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité transmise par le président de la 5ème chambre de la cour administrative d'appel de Paris.<br/>
<br/>
Article 2 : La présente décision sera notifiée à la société à responsabilité limitée Lupa Immobilière France et au ministre de l'action et des comptes publics.<br/>
Copie en sera adressée au Conseil constitutionnel, au Premier ministre et à la cour administrative d'appel de Paris.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
