<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026454665</ID>
<ANCIEN_ID>JG_L_2012_10_000000360840</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/45/46/CETATEXT000026454665.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème et 2ème sous-sections réunies, 03/10/2012, 360840, Publié au recueil Lebon</TITRE>
<DATE_DEC>2012-10-03</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>360840</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème et 2ème sous-sections réunies</FORMATION>
<TYPE_REC>Autres</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Nicolas Polge</RAPPORTEUR>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CESSR:2012:360840.20121003</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu l'arrêt n° 11DA01605 du 27 juin 2012, enregistré le 9 juillet 2012 au secrétariat du contentieux du Conseil d'Etat, par lequel la cour administrative d'appel de Douai, avant de statuer sur l'appel de la société Colas Nord Picardie contre l'ordonnance n° 1102087 du 5 octobre 2011 par laquelle le juge des référés du tribunal administratif d'Amiens a rejeté sa demande tendant à la condamnation de l'Etat à lui verser une provision correspondant aux intérêts moratoires dus sur le solde du montant d'un marché signé le 29 septembre 2005 a décidé, par application des dispositions de l'article L. 113-1 du code de justice administrative, de transmettre le dossier de cette requête au Conseil d'Etat, en soumettant à son examen les questions suivantes :<br/>
<br/>
              1°) Dans le cas de marchés publics de travaux qui ont donné lieu de la part d'un titulaire à une réclamation relative au décompte général, des intérêts moratoires peuvent-ils courir, en l'absence de toute acceptation expresse du décompte général et définitif, en particulier sur un montant correspondant à la différence entre les sommes en litige et celles admises et réglées par le maître d'ouvrage en application du V de l'article 5 du décret n° 2002-232 du 21 février 2002 ' Dans l'affirmative, comment déterminer le point de départ du délai global de paiement d'une durée de quarante-cinq jours au-delà duquel des intérêts moratoires seraient dus ' Y a-t-il lieu de prendre comme point de départ la date à laquelle est intervenue la réception du projet de décompte final présentée par le titulaire du marché, celle à laquelle ce dernier a reçu notification du décompte général, ou, en l'absence d'établissement du décompte, celle à laquelle celui-ci aurait dû être notifié, ou bien celle à laquelle est intervenue la réception de la réclamation présentée par le titulaire du marché relative au décompte général, ou encore une autre date '<br/>
<br/>
              2°) Les intérêts moratoires complémentaires prévus par le III de l'article 5 du même décret du 21 février 2002 font-ils obstacle à une demande, présentée sur le fondement des dispositions de l'article 1154 du code civil, tendant à la capitalisation des intérêts moratoires contractuels ' Dans l'affirmative, appartient-il au juge de requalifier une demande de capitalisation en demande d'intérêts moratoires complémentaires '<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
- le rapport de M. Nicolas Polge, Maître des Requêtes, <br/>
- les conclusions de M. Bertrand Dacosta, rapporteur public ;<br/>
<br/>
<br/>
<br/>                   REND L'AVIS SUIVANT :<br/>
<br/>
              1. D'une part, il résulte des termes mêmes de l'article L. 113-1 du code de justice administrative que le tribunal administratif ou la cour administrative d'appel peut, dans le cadre de la procédure instituée par ces dispositions, bénéficier d'un avis du Conseil d'Etat en vue de se prononcer sur une question de droit que soulève la requête dont la juridiction est saisie, à la condition, notamment, que cette question présente une difficulté sérieuse.<br/>
<br/>
              2. D'autre part, aux termes de l'article R. 541-1 du même code : " Le juge des référés peut, même en l'absence d'une demande au fond, accorder une provision au créancier qui l'a saisi lorsque l'existence de l'obligation n'est pas sérieusement contestable. ". Or, une obligation dont l'existence soulève une question de droit présentant une difficulté sérieuse ne peut être regardée comme une obligation dont l'existence n'est pas sérieusement contestable. Par suite, le juge du référé ne saurait, sans méconnaître les dispositions de cet article, se prononcer sur la difficulté ainsi soulevée pour accorder la provision demandée.<br/>
<br/>
              3. Il résulte de ce qui précède que, lorsqu'il est saisi d'une demande de provision sur le fondement de l'article R. 541-1 du code de justice administrative, il n'appartient pas au juge des référés de première instance ou d'appel de soumettre pour avis au Conseil d'Etat une question de droit que soulève l'existence de l'obligation invoquée devant lui, sur laquelle il ne pourrait lui-même se prononcer, pour accorder la provision, que si cette question, ne présentant pas de difficulté sérieuse, n'entrait par là-même pas dans les prévisions de l'article L. 113-1 du code de justice administrative.<br/>
<br/>
              4. Dès lors, les questions posées par la cour administrative d'appel de Douai, qui se rapportent toutes à l'existence de l'obligation invoquée par la société Colas Nord Picardie devant le juge du référé-provision, ne peuvent être accueillies.<br/>
<br/>
              Le présent avis sera notifié à la cour administrative d'appel de Douai, à la société Colas Nord Picardie et à la ministre de l'écologie, du développement durable et de l'énergie.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-03-015-03 PROCÉDURE. PROCÉDURES DE RÉFÉRÉ AUTRES QUE CELLES INSTITUÉES PAR LA LOI DU 30 JUIN 2000. RÉFÉRÉ-PROVISION. POUVOIRS ET DEVOIRS DU JUGE. - POSSIBILITÉ DE SOUMETTRE POUR AVIS AU CONSEIL D'ETAT UNE QUESTION DE DROIT QUE SOULÈVE L'EXISTENCE DE L'OBLIGATION INVOQUÉE DEVANT LUI (ART. L. 113-1 DU CJA) - ABSENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-07-01-085 PROCÉDURE. POUVOIRS ET DEVOIRS DU JUGE. QUESTIONS GÉNÉRALES. RENVOI AU CONSEIL D'ETAT D'UNE QUESTION DE DROIT NOUVELLE. - RÉFÉRÉ-PROVISION - QUESTION DE DROIT PORTANT SUR L'EXISTENCE DE L'OBLIGATION EN LITIGE - RECEVABILITÉ DE LA DEMANDE D'AVIS - ABSENCE.
</SCT>
<ANA ID="9A"> 54-03-015-03 Lorsqu'il est saisi d'une demande de provision sur le fondement de l'article R. 541-1 du code de justice administrative (CJA), il n'appartient pas au juge des référés de première instance ou d'appel de soumettre pour avis au Conseil d'Etat une question de droit que soulève l'existence de l'obligation invoquée devant lui.</ANA>
<ANA ID="9B"> 54-07-01-085 Lorsqu'il est saisi d'une demande de provision sur le fondement de l'article R. 541-1 du code de justice administrative, il n'appartient pas au juge des référés de première instance ou d'appel de soumettre pour avis au Conseil d'Etat une question de droit que soulève l'existence de l'obligation invoquée devant lui.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
