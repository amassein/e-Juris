<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032582776</ID>
<ANCIEN_ID>JG_L_2016_05_000000392515</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/58/27/CETATEXT000032582776.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème - 9ème chambres réunies, 25/05/2016, 392515, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-05-25</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>392515</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème - 9ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP WAQUET, FARGE, HAZAN</AVOCATS>
<RAPPORTEUR>M. Jacques Reiller</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Edouard Crépey</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2016:392515.20160525</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B...E...a demandé au tribunal administratif de Nîmes d'annuler la délibération du 28 mai 2015 du conseil municipal de la commune de Vedène portant désignation des conseillers communautaires de ladite commune au sein du conseil communautaire de la communauté d'agglomération du Grand Avignon. Par un jugement n° 1501794 du 9 juillet 2015, le tribunal administratif de Nîmes a rejeté sa protestation.<br/>
<br/>
              Par une requête et un mémoire, enregistrés le 10 août 2015 et le 4 mai 2016 au secrétariat du contentieux du Conseil d'Etat, M. E...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement et de faire droit à sa protestation ;<br/>
<br/>
              2°) de mettre à la charge de la commune de Vedène la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code électoral ;<br/>
              - le code général des collectivités territoriales ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jacques Reiller, conseiller d'Etat, <br/>
<br/>
              - les conclusions de M. Edouard Crépey, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Waquet, Farge, Hazan, avocat de la commune de Vedène ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant que, par un arrêté interpréfectoral du 25 février 2015, les préfets du Gard et de Vaucluse ont fixé une nouvelle composition du conseil communautaire de la communauté d'agglomération du Grand Avignon, qui ramène notamment de cinq à trois le nombre de sièges de conseillers communautaires dont bénéficie la commune de Vedène ; que, par une délibération du 28 mai 2015, le conseil municipal de Vedène a procédé, en application de l'article L. 5211-6-2 du code général des collectivités territoriales, à l'élection de trois conseillers communautaires ; que les trois candidats de la liste " Ensemble pour Vedène ", M. A..., Mme C...et MmeD..., ont été réélus dans cet ordre ; que les deux conseillers non réélus ont été, d'une part M.F..., qui se présentait sous la liste " Vedène Bleu Marine ", d'autre part, M.E..., qui ne figurait plus, comme lors de l'élection des conseillers communautaires de la commune qui avait eu lieu en mars 2014 en même temps que l'élection des conseillers municipaux, sur la liste " Ensemble pour Vedène " et qui ne s'est pas non plus présenté sur une autre liste ; que M. E...relève appel du jugement du 9 juillet 2015 par lequel le tribunal administratif de Nîmes a rejeté sa protestation contre l'élection qui a donné lieu à la délibération du 28 mai 2015 ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article L. 5211-6-2 du code général des collectivités territoriales : " Par dérogation aux articles L. 5211-6 et L. 5211-6-1, entre deux renouvellements généraux des conseils  municipaux : / 1 ° En cas de création d'un établissement public de coopération intercommunale à fiscalité propre, de fusion entre plusieurs établissements publics de coopération intercommunale dont au moins l'un d'entre eux est à fiscalité propre, d'extension du périmètre d'un tel établissement par l'intégration d'une ou de plusieurs communes ou la modification des limites territoriales d'une commune membre ou d'annulation par la juridiction administrative de la répartition des sièges de conseiller communautaire, il est procédé à la détermination du nombre et à la répartition des sièges de conseiller communautaire dans les conditions prévues à l'article L. 5211-6-1. 1(...) / Dans les communes dont le conseil municipal est élu selon les modalités prévues au chapitre III du titre IV dudit livre 1er : / (...) c) Si le nombre de sièges attribués à la commune est inférieur au nombre de conseillers communautaires élus à l'occasion du précédent renouvellement général du conseil municipal, les membres du nouvel organe délibérant sont élus par le conseil municipal parmi les conseillers communautaires sortants au scrutin de liste à un tour sans adjonction ni suppression de noms et sans modification de l'ordre de présentation. La répartition des sièges entre les listes est opérée à la représentation proportionnelle à la plus forte moyenne. Si le nombre de candidats figurant sur une liste est inférieur au nombre de sièges qui lui reviennent, le ou les sièges non pourvus sont attribués à  la ou aux plus fortes moyennes suivantes " ; <br/>
<br/>
              3. Considérant que M. E...soutient que, dès lors qu'il avait été candidat en troisième position pour l'élection en qualité de conseiller communautaire sur la liste " Ensemble pour Vedène " lors de l'élection qui avait eu lieu en mars 2014 en même temps que l'élection des conseillers municipaux, sa place sur cette liste ne pouvait être modifiée pour l'élection par le conseil municipal à laquelle il a été procédé en application des dispositions précitées après que le nombre des conseillers communautaires dont bénéficie la commune de Vedène a été modifié par arrêté interpréfectoral et que, faute qu'il ait été retenu comme candidat sur cette liste, l'élection à laquelle il a été procédé par la délibération du 28 mai 2015 est entachée d'irrégularité ; <br/>
<br/>
              4. Considérant, toutefois, que les dispositions des articles L. 273-6 à L. 273-10 du code électoral dont M. E...se prévaut à l'appui de son argumentation sont en tout état de cause inapplicables à l'élection de conseillers communautaires à laquelle il est procédé en application des dispositions précitées de l'article L. 5211-6-2 du code général des collectivités territoriales ; que, par ailleurs, contrairement à ce que soutient M.E..., si les dispositions du c) du 1° de ce dernier article interdisent toute " modification de l'ordre de présentation ", elles ne visent que l'expression du suffrage et non la composition des listes ; <br/>
<br/>
              5. Considérant qu'il résulte de ce qui précède que M. E...n'est pas fondé à soutenir que c'est à tort que, par le jugement qu'il attaque, le tribunal administratif de Nîmes a rejeté sa protestation ; <br/>
<br/>
              6. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de la commune de Vedène et à ce qu'il soit fait droit à la demande que celle-ci a présentée au même titre dans ses observations, dès lors que la commune n'est pas partie à la présente instance ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de M. E...est rejetée.<br/>
Article 2 : La demande présentée par la commune de Vedène au titre des dispositions de l'article L. 761-1 du code de justice administrative est rejetée.<br/>
Article 3 : La présente décision sera notifiée à M. B...E..., à la commune de Vedène, à M. A..., à MmeC..., à Mme D...et au ministre de l'intérieur. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
