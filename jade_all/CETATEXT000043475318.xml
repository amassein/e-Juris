<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043475318</ID>
<ANCIEN_ID>JG_L_2021_05_000000442212</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/47/53/CETATEXT000043475318.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème chambre, 03/05/2021, 442212, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-05-03</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>442212</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Fabio Gennari</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Sophie Roussel</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:442212.20210503</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et deux mémoires en réplique, enregistrés les 27 juillet, <br/>
21 septembre et 19 octobre 2020 au secrétariat du contentieux du Conseil d'État, Mme E... D... demande au Conseil d'État :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret du 21 avril 2020 par lequel le Premier ministre a rapporté le décret du 28 juin 2012 en ce qu'il lui a accordé la nationalité française ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - le traité sur le fonctionnement de l'Union Européenne ;<br/>
              - le code d'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              - le décret n° 93-1362 du 30 décembre 1993 ;<br/>
              - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Fabio Gennari, auditeur,<br/>
<br/>
              - les conclusions de Mme Sophie Roussel, rapporteure publique,<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article 27-2 du code civil : " Les décrets portant acquisition, naturalisation ou réintégration peuvent être rapportés sur avis conforme du Conseil d'Etat dans le délai de deux ans à compter de leur publication au Journal officiel si le requérant ne satisfait pas aux conditions légales ; si la décision a été obtenue par mensonge ou fraude, ces décrets peuvent être rapportés dans le délai de deux ans à partir de la découverte de la fraude ".<br/>
<br/>
              2. Il ressort des pièces du dossier que Mme D..., ressortissante camerounaise, a souscrit le 21 mai 2011 une déclaration d'acquisition de la nationalité française dans laquelle elle indiquait être la mère d'un enfant né le 23 février 2003, C..., de nationalité française en raison de la reconnaissance de paternité de M. B..., de nationalité française. Au vu de ses déclarations, l'intéressée a été naturalisée par le décret du 28 juin 2012. Toutefois, par courriel du 7 mai 2018, le ministre de la justice a informé le ministre de l'intérieur, chargé des naturalisations, de ce que, par un jugement du tribunal de grande instance de Nanterre du 9 juin 2015, la reconnaissance de paternité de M. B... envers l'enfant C... avait été annulée. Par décret du 21 avril 2020, le Premier ministre a rapporté le décret du 28 juin 2012 de naturalisation de Mme D... au motif qu'il n'avait été pris qu'à raison des manoeuvres frauduleuses de l'intéressée. Mme D... demande l'annulation pour excès de pouvoir de ce décret.<br/>
<br/>
              3. En premier lieu, le décret attaqué énonce les considérations de droit et de fait sur lesquelles il se fonde et est, dès lors, suffisamment motivé.<br/>
<br/>
              4. En deuxième lieu, il ressort des pièces du dossier qu'entrée irrégulièrement en France en mars 2001, Mme D... a obtenu un titre de séjour en novembre 2003 à la suite de la reconnaissance anticipée par M. B... en septembre 2002 de l'enfant C..., la demande de titre de séjour ayant été déposée dans le mois suivant la naissance de l'enfant. En juin 2013, moins d'un an après sa naturalisation, Mme D... a introduit devant le tribunal de grande instance de Nanterre une action tendant à obtenir l'annulation de la reconnaissance de paternité de M. B..., au bénéfice de l'enfant, C.... Par un jugement du 9 juin 2015, le tribunal de grande instance de Nanterre a fait droit à cette requête, à la suite d'une expertise établissant la paternité de M.A... F..., également demandeur. Au regard de ces circonstances, de l'absence d'éléments suffisamment probants permettant d'établir l'existence d'une vie commune de la requérante avec M. B... à l'époque de la reconnaissance anticipée de paternité de ce dernier et alors que Mme D... se borne à soutenir que, à supposer la reconnaissance de paternité frauduleuse, cette fraude lui serait étrangère, l'administration doit être regardée comme établissant que la reconnaissance de paternité souscrite par M. B... à l'égard de l'enfant C... avait un caractère frauduleux. Par suite, Mme D... ayant volontairement bénéficié de cette reconnaissance de paternité par l'obtention d'un titre de séjour, le Premier ministre, à qui il appartenait de faire échec à cette fraude et à ses conséquences, était légalement fondé à rapporter le décret de naturalisation de Mme D... dans le délai de deux ans à compter de la découverte de la fraude. Le moyen tiré de la méconnaissance de l'article 27-2 du code civil doit, par suite, être écarté. <br/>
<br/>
              5. En troisième lieu, la définition des conditions et de la perte de nationalité relève de la compétence de chaque Etat membre de l'Union européenne. Toutefois, dans la mesure où la perte de nationalité d'un Etat membre a pour conséquence la perte du statut de citoyen de l'Union, la perte de la nationalité d'un Etat membre doit, pour être conforme au droit de l'Union, répondre à des motifs d'intérêt général et être proportionnée à la gravité des faits qui la fondent, au délai écoulé depuis l'acquisition de la nationalité et à la possibilité pour l'intéressé de recouvrir une autre nationalité. Il résulte des dispositions de l'article 27-2 du code civil qu'un décret ayant conféré la nationalité française peut être rapporté dans un délai de deux ans à compter de la découverte de la fraude au motif que l'intéressé a obtenu la nationalité française par mensonge ou fraude. Ces dispositions, qui ne sont pas incompatibles avec le droit de l'Union européenne, permettaient en l'espèce, eu égard à la date à laquelle il est intervenu et aux motifs qui le fondent, de rapporter légalement le décret accordant à Mme D... la nationalité française. Si Mme D... soutient, en invoquant la loi camerounaise, qu'elle aurait perdu la nationalité camerounaise, elle n'apporte aucun élément de nature à établir qu'elle aurait effectivement perdu sa nationalité d'origine ou ne pourrait la recouvrer. Par suite, le Premier ministre a pu légalement prendre le décret attaqué en faisant application en l'espèce des dispositions de l'article 27-2 du code civil.<br/>
<br/>
              6. En dernier lieu, un décret qui rapporte un décret ayant conféré la nationalité française est, par lui-même, dépourvu d'effet sur la présence sur le territoire français de celui qu'il vise, comme sur ses liens avec les membres de sa famille, et n'affecte pas, dès lors, le droit au respect de sa vie familiale. Il affecte néanmoins un élément constitutif de l'identité de la personne concernée et est ainsi susceptible de porter atteinte au droit au respect de sa vie privée. En l'espèce, toutefois, eu égard à la date à laquelle il est intervenu, aux motifs qui le fondent et à la situation de Mme D... et de ses enfants, le décret attaqué ne peut être regardé comme ayant porté une atteinte disproportionnée au droit au respect de leur vie privée.<br/>
<br/>
              7. Il résulte de ce qui précède que Mme D... n'est pas fondée à demander l'annulation pour excès de pouvoir du décret du 21 avril 2020 par lequel le Premier ministre a rapporté le décret du 28 juin 2012. Ses conclusions présentées au titre de l'article <br/>
L. 761-1 du code de justice administrative ne peuvent, en conséquence, qu'être rejetées.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de Mme D... est rejetée.<br/>
Article 2 : La présente décision sera notifiée à Mme E... D... et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
