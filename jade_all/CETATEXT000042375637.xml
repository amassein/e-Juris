<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042375637</ID>
<ANCIEN_ID>JG_L_2020_09_000000430088</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/37/56/CETATEXT000042375637.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème chambre, 28/09/2020, 430088, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-09-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>430088</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP SEVAUX, MATHONNET ; SCP L. POULET-ODENT</AVOCATS>
<RAPPORTEUR>Mme Pearl Nguyên Duy</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Cécile Barrois de Sarigny</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2020:430088.20200928</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme F... B..., M. E... A..., M. C... A... et M. D... A... ont demandé au tribunal administratif de Paris de condamner l'Office national d'indemnisation des accidents médicaux, des affections iatrogènes et des infections nosocomiales (ONIAM) à verser à Mme B... la somme de 1 384 651,17 euros en réparation des préjudices qu'elle estime avoir subis à la suite d'une intervention chirurgicale en 2005. Par un jugement n° 1508210 du 4 avril 2017, le tribunal administratif a condamné l'ONIAM à verser à Mme B... la somme de 477 679,41 euros, ainsi qu'une rente couvrant les frais d'assistance par tierce personne d'un montant annuel de 15 330 euros.<br/>
<br/>
              Par un arrêt n° 17PA01888 du 19 février 2019, la cour administrative d'appel de Paris a, sur appels de Mme B... et de l'ONIAM, porté à 487 679,41 euros la somme que l'ONIAM est condamné à verser à Mme B....<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 23 avril et 23 juillet 2019 au secrétariat du contentieux du Conseil d'Etat, l'ONIAM demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt ; <br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de Mme B... et de faire droit à son appel ;  <br/>
<br/>
              3°) de mettre à la charge de MM. A... la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de la santé publique ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Pearl Nguyên Duy, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Cécile Barrois de Sarigny, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Sevaux, Mathonnet, avocat de l'Office national d'indemnisation des accidents médicaux, des affections iatrogènes et des infections nosocomiales et à la SCP L. Poulet, Odent, avocat de M. E... A... et autres.<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que Mme B... a subi une intervention chirurgicale en 2005 à l'hôpital de la Pitié-Salpêtrière (Paris), qui a été suivie d'une aggravation des troubles affectant la motricité de ses deux jambes. Par un jugement du 4 avril 2017, le tribunal administratif de Paris a condamné l'Office national d'indemnisation des accidents médicaux, des affections iatrogènes et des infections nosocomiales (ONIAM) à réparer les préjudices subis par Mme B... au titre de la solidarité nationale. Par un arrêt du 19 février 2019, la cour administrative d'appel de Paris a, sur appel de Mme B..., porté à 487 679,41 euros la somme que l'ONIAM a été condamné à lui verser.<br/>
<br/>
              2. Dans l'intérêt d'une bonne administration de la justice, le juge administratif a toujours la faculté de rouvrir l'instruction, qu'il dirige, lorsqu'il est saisi d'une production postérieure à la clôture de celle-ci. Il lui appartient, dans tous les cas, de prendre connaissance de cette production avant de rendre sa décision et de la viser. S'il décide d'en tenir compte, il rouvre l'instruction et soumet au débat contradictoire les éléments contenus dans cette production qu'il doit, en outre, analyser. Dans le cas particulier où cette production contient l'exposé d'une circonstance de fait ou d'un élément de droit dont la partie qui l'invoque n'était pas en mesure de faire état avant la clôture de l'instruction et qui est susceptible d'exercer une influence sur le jugement de l'affaire, le juge doit alors en tenir compte, à peine d'irrégularité de sa décision.<br/>
<br/>
              3. Il ressort des pièces du dossier soumis aux juges du fond que, informée la veille de l'audience du 5 février 2019 par les ayants droit de Mme B... de son décès survenu le 20 janvier précédent, la cour, tout en visant les écritures ainsi produites, n'a pas rouvert l'instruction, qui avait été close le 1er février 2019 à minuit, conformément aux dispositions de l'article R. 613-2 du code de justice administrative. Le décès de Mme B... devait toutefois, compte tenu du bref délai qui s'était écoulé depuis qu'il était survenu, être regardé comme n'ayant pu être invoqué par ses ayants droits avant la clôture de l'instruction. Par ailleurs, il s'agissait d'une circonstance susceptible d'exercer une influence sur le jugement de l'affaire, puisque l'évaluation des préjudices subis par la victime ne pouvait plus être calculée sur la base de son espérance de vie. Par suite, en s'abstenant de rouvrir l'instruction, la cour a entaché son arrêt d'irrégularité.<br/>
<br/>
              4. Il résulte de ce qui précède que, sans qu'il soit besoin de se prononcer sur les autres moyens du pourvoi, l'ONIAM est fondé à demander l'annulation de l'arrêt qu'il attaque.<br/>
<br/>
              5. Il n'y a pas lieu, dans les circonstances de l'espèce, de mettre à la charge de M. A... et autres la somme que demande l'ONIAM au titre des dispositions de l'article L. 761-1 du code de justice administrative. Ces mêmes dispositions font obstacle à ce qu'une somme soit mise, au même titre, à la charge de l'ONIAM qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Paris du 19 février 2019 est annulé.<br/>
<br/>
		Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Paris.<br/>
<br/>
Article 3 : Les conclusions présentées par l'Office national d'indemnisation des accidents médicaux, des affections iatrogènes et des infections nosocomiales et par MM. A... au titre de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
<br/>
Article 4 : La présente décision sera notifiée à l'Office national d'indemnisation des accidents médicaux, des affections iatrogènes et des infections nosocomiales, à M. E... A..., premier défendeur dénommé, à la ville de Tours, à la Mutuelle nationale territoriale et à la caisse primaire d'assurance maladie du Loir-et-Cher.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
