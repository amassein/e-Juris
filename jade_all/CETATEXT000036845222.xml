<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036845222</ID>
<ANCIEN_ID>JG_L_2018_04_000000407536</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/84/52/CETATEXT000036845222.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère chambre, 26/04/2018, 407536, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-04-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>407536</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MATUCHANSKY, POUPOT, VALDELIEVRE</AVOCATS>
<RAPPORTEUR>M. Jean-Luc Nevache</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Rémi Decout-Paolini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:407536.20180426</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 3 février 2017, 3 mai 2017 et 22 janvier 2018 au secrétariat du contentieux du Conseil d'Etat, le syndicat national des médecins biologistes, le syndicat des biologistes et le syndicat des laboratoires de biologie clinique demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir l'arrêté du ministre des affaires sociales et de la santé du 1er août 2016 déterminant la liste des tests, recueils et traitements de signaux biologiques qui ne constituent pas un examen de biologie médicale, les catégories de personnes pouvant les réaliser et les conditions de réalisation de certains de ces tests, recueils et traitements de signaux biologiques, ainsi que la décision implicite par laquelle le ministre des affaires sociales et de la santé  a rejeté leur recours gracieux contre cet arrêté ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la convention pour la protection des droits de l'homme et de la dignité de l'être humain à l'égard des applications de la biologie et de la médecine, signée à Oviedo le 4 avril 1997 ; <br/>
              - la loi n° 2011-814 du 7 juillet 2011 ;<br/>
              - le code de la santé publique ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Luc Nevache, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Rémi Decout-Paolini, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Matuchansky, Poupot, Valdelièvre, avocat du syndicat national des médecins biologistes, du syndicat des biologistes et du syndicat des laboratoires de biologie clinique.<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. L'article L. 6211-1 du code de la santé publique dispose que : " Un examen de biologie médicale est un acte médical qui concourt à la prévention, au dépistage, au diagnostic ou à l'évaluation du risque de survenue d'états pathologiques, à la décision et à la prise en charge thérapeutiques, à la détermination ou au suivi de l'état physiologique ou physiopathologique de l'être humain, hormis les actes d'anatomie et de cytologie pathologiques, exécutés par des médecins spécialistes dans ce domaine ". L'article L. 6211-7 du même code prévoit qu'" un examen de biologie médicale est réalisé par un biologiste médical ou, pour certaines phases, sous sa responsabilité ". Aux termes de l'article L. 6211-3 de ce code, dans sa rédaction applicable à la date de l'arrêté attaqué : " Ne constituent pas un examen de biologie médicale un test, un recueil et un traitement de signaux biologiques, à visée de dépistage, d'orientation diagnostique ou d'adaptation thérapeutique immédiate. / Un arrêté du ministre chargé de la santé établit la liste de ces tests, recueils et traitements de signaux biologiques, après avis de la commission mentionnée à l'article L. 6213-12 et du directeur général de l'Agence nationale de sécurité du médicament et des produits de santé. Cet arrêté détermine les catégories de personnes pouvant réaliser ces tests, recueils et traitements de signaux biologiques, ainsi que, le cas échéant, leurs conditions de réalisation. / Cet arrêté définit notamment les conditions dans lesquelles des tests rapides d'orientation diagnostique, effectués par un professionnel de santé ou par du personnel ayant reçu une formation adaptée et relevant de structures de prévention et associatives, contribuent au dépistage de maladies infectieuses transmissibles. / Cet arrêté précise également les conditions particulières de réalisation de ces tests ainsi que les modalités dans lesquelles la personne est informée de ces conditions et des conséquences du test ". Les syndicats requérants demandent au Conseil d'Etat d'annuler pour excès de pouvoir l'arrêté du 1er août 2016 par lequel le ministre des affaires sociales et de la santé a, pour l'application de ces dispositions, déterminé la liste des tests, recueils et traitements de signaux biologiques qui ne constituent pas un examen de biologie médicale, les catégories de personnes pouvant les réaliser et les conditions de réalisation de certains de ces tests, recueils et traitements de signaux biologiques, ainsi que la décision implicite du 5 décembre 2016 rejetant le recours gracieux qu'ils ont formé contre cet arrêté.<br/>
<br/>
              2. En premier lieu, il résulte des dispositions citées ci-dessus qu'en prévoyant que les tests, recueils et traitements de signaux biologiques qu'il mentionne ne sont pas des examens de biologie médicale, l'article L. 6211-3 du code de la santé publique a seulement entendu permettre que la réalisation de ces actes puisse être confiée à d'autres catégories de personnes que les biologistes médicaux et soumise à des règles moins contraignantes que celles en principe applicables aux examens de biologie médicale. Par suite, en déterminant les catégories de personnes pouvant réaliser ceux des tests, recueils et traitements de signaux biologiques, à visée de dépistage, d'orientation diagnostique ou d'adaptation thérapeutique immédiate, qu'il a précisés, l'arrêté attaqué n'a eu ni pour objet ni pour effet de faire obstacle à ce qu'un biologiste médical, seul habilité par l'article L. 6211-7 du même code à réaliser un examen de biologie médicale, réalise un tel test ou recueil et traitement de signaux biologiques. Les syndicats requérants ne sont, dès lors, fondés à soutenir ni qu'il serait illégal faute d'avoir mentionné cette faculté, ni qu'il priverait les patients d'une chance d'être correctement soignés au motif qu'il interdirait qu'un tel test ou recueil et traitement de signaux biologiques puisse être réalisé par un biologiste médical.<br/>
<br/>
              3. En deuxième lieu, d'une part, aux termes de l'article 5 de la convention pour la protection des droits de l'homme et de la dignité de l'être humain à l'égard des applications de la biologie et de la médecine, signée à Oviedo le 4 avril 1997, dont la ratification a été autorisée par l'article 1er de la loi du 7 juillet 2011 relative à la bioéthique : " Une intervention dans le domaine de la santé ne peut être effectuée qu'après que la personne concernée y a donné son consentement libre et éclairé. / Cette personne reçoit préalablement une information adéquate quant au but et à la nature de l'intervention ainsi que quant à ses conséquences et ses risques. / La personne concernée peut, à tout moment, librement retirer son consentement ". D'autre part, le dernier alinéa de l'article L. 6211-3 du code de la santé publique prévoit que l'arrêté établissant la liste des tests, recueils et traitements de signaux biologiques qui ne constituent pas des examens de biologie médicale précise les modalités selon lesquelles la personne est informée de ces conditions et des conséquences du test. L'article 2 de l'arrêté attaqué prévoit que les tests ou recueils et traitements de signaux biologiques dont il détermine la liste " constituent des éléments d'orientation diagnostique sans se substituer au diagnostic réalisé au moyen d'un examen de biologie médicale. Le patient est explicitement informé par le professionnel de santé qui le réalise. Le patient est également informé des moyens de confirmation par un examen de biologie médicale si la démarche diagnostique ou thérapeutique le justifie. Le professionnel de santé, qui réalise le test, en adresse, avec le consentement du patient, le résultat à son médecin traitant ou au médecin désigné par le patient. Le médecin traitant ou le médecin que le patient désigne propose au patient la confirmation du résultat de ce test par un examen de biologie médicale si la démarche diagnostique ou thérapeutique le justifie ". L'annexe II de l'arrêté, relative à la procédure d'assurance qualité applicable aux professionnels de santé réalisant les tests ou recueils et traitements de signaux biologiques, indique que celle-ci doit prévoir, d'une part, " les modalités pour la communication appropriée du résultat du test rapide au patient " et, d'autre part, " les modalités de la prise en charge du patient en cas de positivité d'un test rapide d'orientation diagnostique ". L'annexe III de l'arrêté fixe les éléments devant figurer dans la fiche de procédure d'assurance qualité pour la réalisation des tests d'orientation diagnostique, parmi lesquels la communication des résultats, pour laquelle " le professionnel de santé s'engage à transmettre à la personne à qui le test a été réalisé un document écrit " qui " mentionne les résultats du test et rappelle que ce test ne constitue qu'une orientation diagnostique " et les modalités de prise en charge du patient en cas de résultat positif d'un test d'orientation diagnostique. Contrairement à ce que soutiennent les syndicats requérants, ces dispositions précisent suffisamment, ainsi que l'exige le dernier alinéa de l'article L. 6211-3 du code de la santé publique, les modalités selon lesquelles la personne est informée des conditions du test rapide d'orientation diagnostique et des conséquences du test, afin qu'elle puisse donner son consentement éclairé, dans le respect des stipulations de l'article 5 de la convention pour la protection des droits de l'homme et de la dignité de l'être humain à l'égard des applications de la biologie et de la médecine.<br/>
<br/>
              4. En dernier lieu, le ministre des affaires sociales et de la santé a pu, sans erreur manifeste d'appréciation, eu égard notamment à l'avis émis le 7 janvier 2015 par le collège de la Haute Autorité de santé l'invitant à élargir le nombre de professionnels de santé pouvant avoir recours à ces tests, recueils et traitements pour, en particulier, diversifier les possibilités de toucher les populations à risque, ne pas retenir les préconisations émises par la commission nationale de biologie médicale lors de sa consultation préalable, d'une part, en ne réservant pas la réalisation de ces tests, en dépit des limites de leur fiabilité, aux contextes d'urgence ou d'absence d'environnement médico-technique permettant de réaliser les examens biologiques correspondant et en ne recommandant pas que, lorsque leur réalisation est motivée par l'urgence, le résultat en soit vérifié dans les deux heures par un examen biologique d'urgence et, d'autre part, en ne limitant pas la réalisation de tests par les pharmaciens d'officine au seul dépistage du diabète lorsque l'examen biologique correspondant ne peut être réalisé en raison de l'absence de proximité d'un laboratoire de biologie médicale. En outre, contrairement à ce qui est soutenu, l'arrêté comporte, en son article 2 et à ses annexes II et III, des dispositions propres à assurer le contrôle de la fiabilité des éventuels appareils de lecture des tests ou recueils et traitements de signaux biologiques.<br/>
<br/>
              5. Il résulte de tout ce qui précède que les syndicats requérants ne sont pas fondés à demander l'annulation de l'arrêté qu'ils attaquent. Les conclusions qu'ils présentent au titre des dispositions de l'article L.761-1 du code de justice administrative doivent également, en conséquence, être rejetées.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
                            --------------<br/>
<br/>
Article 1er : La requête du syndicat national des médecins biologistes, du syndicat des biologistes et du syndicat des laboratoires de biologie clinique est rejetée.<br/>
Article 2 : La présente décision sera notifiée, pour l'ensemble des requérants, au syndicat national des médecins biologistes, premier dénommé, et à la ministre des solidarités et de la santé.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
