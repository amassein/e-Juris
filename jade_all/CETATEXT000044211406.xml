<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044211406</ID>
<ANCIEN_ID>JG_L_2021_09_000000456520</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/21/14/CETATEXT000044211406.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 27/09/2021, 456520, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-09-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>456520</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BAUER-VIOLAS, FESCHOTTE-DESBOIS, SEBAGH</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2021:456520.20210927</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
               Mme A... C..., agissant en son nom et au nom de ses enfants mineurs, E... C... et F... B... C..., ont demandé au juge des référés du tribunal administratif de Paris, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, d'enjoindre au ministre de l'Europe et des affaires étrangères de les inscrire sur les listes d'évacuation d'Afghanistan tenues par les autorités françaises, de prendre contact avec Mme A... C... sur son téléphone portable et de prendre l'engagement de prendre toutes les mesures utiles pour les évacuer vers la France même après expiration des visas qui leur ont été délivrés. Par une ordonnance n° 2118112 du 28 août 2021, le juge des référés du tribunal administratif de Paris a enjoint au ministre de l'Europe et des affaires étrangères de prendre contact sans délai avec Mme A... C... au numéro de téléphone qu'elle a indiqué dans sa requête pour attester que sa demande de rapatriement a bien été prise en considération et l'informer des suites qu'il est en mesure d'y apporter.<br/>
<br/>
               Par une requête, enregistrée le 9 septembre 2021 au secrétariat du contentieux du Conseil d'Etat, le ministre de l'Europe et des affaires étrangères demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative :<br/>
<br/>
               1°) d'annuler cette ordonnance ; <br/>
<br/>
               2°) de rejeter la demande en référé présentée par Mmes C... .<br/>
<br/>
<br/>
               Elles soutiennent que :<br/>
               - la juridiction administrative n'est pas compétente pour connaître de la demande de Mmes C..., les mesures demandées se rattachant à l'action du gouvernement dans la conduite des relations internationales ;<br/>
               - il n'est pas porté d'atteinte grave et manifestement illégale à un droit fondamental dès lors que les autorités françaises ont organisé les opérations d'évacuation et de rapatriement en mobilisant tous les moyens nécessaires ; <br/>
               - les mesures ordonnées par le juge des référés de première instance ne sont pas de nature à sauvegarder les libertés fondamentales en cause.<br/>
<br/>
               Par un mémoire en défense, enregistré le 20 septembre 2021, Mmes C... sollicitent une prise de contact téléphonique de la part des services du ministère de l'Europe et des affaires étrangères et, d'autre part, un engagement sur le long terme de la part des services du ministère de l'Europe et des affaires étrangères de prendre toutes les mesures utiles, et notamment la garantie de la validité des visas au-delà de leurs limites de validité. Elles soutiennent que les moyens soulevés par le ministre de l'Europe et des affaires étrangères ne sont pas fondés.<br/>
<br/>
<br/>
               Vu les autres pièces du dossier ;<br/>
<br/>
               Vu : <br/>
               - la Constitution notamment son Préambule ;<br/>
               - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
               - la convention de Genève du 28 juillet 1951 ;<br/>
               - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
               - le code de justice administrative ;<br/>
<br/>
<br/>
               Après avoir convoqué à une audience publique, d'une part, le ministre de l'Europe et des affaires étrangères, et d'autre part, Mmes C... ;<br/>
<br/>
               Ont été entendus lors de l'audience publique du 21 septembre 2021, à 14 heures : <br/>
<br/>
               - Me Sebagh, avocat au Conseil d'Etat et à la Cour de cassation, avocat de Mmes C... ;<br/>
<br/>
               - la représentante de Mmes C... ;<br/>
<br/>
               - les représentants du ministre de l'Europe et des affaires étrangères ;<br/>
<br/>
               à l'issue de laquelle le juge des référés a clos l'instruction ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
               1. Aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. (...) ". <br/>
<br/>
               2. Il résulte de l'instruction que, par une décision du 15 février 2021, le tribunal administratif de Nantes a enjoint au ministre de l'intérieur de délivrer à Mme A... C... et à ses enfants, de nationalité afghane, des visas d'entrée en France pour rejoindre M. C..., bénéficiaire de la protection subsidiaire. Ces visas n'ont été délivrés que le 18 août 2021 par les autorités consulaires françaises à Islamabad. A la suite de la prise de contrôle de la ville de Kaboul par les Talibans, des évacuations en urgence aux fins d'acheminement vers la France ont été organisées à partir de l'aéroport international Hamid Karzaï. Mme C... et ses enfants ont demandé au juge des référés du tribunal administratif de Paris, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, d'enjoindre au ministre de l'Europe et des affaires étrangères de les inscrire sur les listes d'évacuation, de prendre contact avec elle et de prendre l'engagement de prendre toutes les mesures utiles pour les évacuer vers la France même après expiration des visas qui leur ont été délivrés. Par une ordonnance du 28 août 2021, le juge des référés du tribunal administratif de Paris a fait droit à ses conclusions et a enjoint au ministre de l'Europe et des affaires étrangères de prendre contact sans délai avec Mme C... au numéro de téléphone indiqué dans sa demande pour attester que sa demande de rapatriement avait bien été prise en considération et de l'informer des suites qu'il envisageait d'y apporter. Le ministre de l'Europe et des affaires étrangères relève appel de cette ordonnance.<br/>
<br/>
               3. Aux termes de l'article L. 561-2 du code de l'entrée et du séjour des étrangers et du droit d'aile : " Sauf si sa présence constitue une menace pour l'ordre public, le ressortissant étranger qui s'est vu reconnaître la qualité de réfugié ou qui a obtenu le bénéfice de la protection subsidiaire peut demander à bénéficier de son droit à être rejoint, au titre de la réunification familiale : / 1° Par son conjoint ou le partenaire avec lequel il est lié par une union civile, âgé d'au moins dix-huit ans, si le mariage ou l'union civile est antérieur à la date d'introduction de sa demande d'asile ; / 2° Par son concubin, âgé d'au moins dix-huit ans, avec lequel il avait, avant la date d'introduction de sa demande d'asile, une vie commune suffisamment stable et continue ; / 3° Par les enfants non mariés du couple, n'ayant pas dépassé leur dix-neuvième anniversaire. / (...) L'âge des enfants est apprécié à la date à laquelle la demande de réunification familiale a été introduite ". Aux termes de l'article L. 561-5 du même code : " Les membres de la famille d'un réfugié ou d'un bénéficiaire de la protection subsidiaire sollicitent, pour entrer en France, un visa d'entrée pour un séjour d'une durée supérieure à trois mois auprès des autorités diplomatiques et consulaires, qui statuent sur cette demande dans les meilleurs délais. Ils produisent pour cela les actes de l'état civil justifiant de leur identité et des liens familiaux avec le réfugié ou le bénéficiaire de la protection subsidiaire. / En l'absence d'acte de l'état civil ou en cas de doute sur leur authenticité, les éléments de possession d'état définis à l'article 311-1 du code civil et les documents établis ou authentifiés par l'Office français de protection des réfugiés et apatrides, sur le fondement de l'article L. 121-9 du présent code, peuvent permettre de justifier de la situation de famille et de l'identité des demandeurs. Les éléments de possession d'état font foi jusqu'à preuve du contraire. Les documents établis par l'office font foi jusqu'à inscription de faux ".<br/>
<br/>
               4. L'organisation d'opérations d'évacuation à partir d'un territoire étranger et de rapatriement vers la France n'est pas détachable de la conduite des relations internationales de la France. Par suite, la juridiction administrative n'est pas compétente pour connaître des demandes tendant à ce que des rapatriements soient ordonnés, alors même qu'est en cause l'évacuation de ressortissants afghans bénéficiant des dispositions citées au point 5. Dès lors, il y a lieu d'annuler l'ordonnance attaquée par laquelle le juge des référés s'est reconnu compétent pour connaître de la demande de Mme C... et ses enfants tendant à ce qu'il soit enjoint au ministre de l'Europe et des affaires étrangères de prendre des mesures pour organiser leur rapatriement et de prendre contact avec elle dans ce cadre. Il y a lieu de statuer sur la demande présentée par Mme C... et ses enfants au juge des référés du tribunal administratif de Paris par la voie de l'évocation.<br/>
<br/>
               5. Ainsi qu'il a été dit au point précédent, la juridiction administrative n'est pas compétente pour connaître des demandes tendant à ce que des rapatriements soient ordonnés. Par suite, les conclusions de Mme C... et ses enfants tendant à ce qu'ils soient inscrits sur les listes d'évacuation des autorités françaises et qu'un contact soit pris avec Mme C... dans le cadre des opérations d'évacuation ne peuvent qu'être rejetées.<br/>
<br/>
               6. En revanche, leurs conclusions tendant à ce qu'il soit ordonné au ministre de l'Europe et des affaires étrangères et au ministre de l'intérieur de prendre, en urgence, les mesures permettant aux ressortissants afghans pouvant bénéficier d'une réunification familiale de faire valoir leur droit par la délivrance d'un visa ou de toute autre mesure équivalente, ne peuvent être regardées comme échappant à la compétence que le juge des référés du Conseil d'Etat tient des dispositions de l'article L. 521-2 du code de justice administrative.<br/>
<br/>
               7. Il résulte des échanges tenus au cours de l'audience publique que l'administration s'est engagée à prolonger la durée de validité des visas délivrés à Mme C... et ses enfants au-delà de leur date d'expiration dans le cas où ils se trouveraient dans l'impossibilité de rejoindre le territoire français avant cette échéance Il n'apparaît donc pas, en l'état de l'instruction, que, compte tenu de la situation en Afghanistan, le ministre de l'Europe et des affaires étrangères porterait une atteinte grave et manifestement illégal aux libertés fondamentales de Mme C... et de ses enfants.<br/>
<br/>
               8. Il résulte de ce qui précède que la demande en référé présentée par Mme C... et de ses enfants doit être rejetée, y compris ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
              ------------------<br/>
<br/>
Article 1er : L'ordonnance du juge des référés du tribunal administratif de Paris du 28 août 2021 est annulée.<br/>
Article 2 :  Compte tenu de l'engagement pris par l'administration et rappelé au point 6, la demande de Mme C... et de ses enfants est rejetée.<br/>
Article 3 : La présente ordonnance sera notifiée au ministre de l'Europe et des affaires étrangères, à Mme A... C..., à Mme D... C... et Mme B... C....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
