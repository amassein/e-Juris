<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043799785</ID>
<ANCIEN_ID>JG_L_2021_07_000000448503</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/79/97/CETATEXT000043799785.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème chambre, 16/07/2021, 448503, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-07-16</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>448503</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Jean-Marc Vié</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Karin Ciavaldini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:448503.20210716</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société par actions simplifiée (SAS) But International a demandé au tribunal administratif de Strasbourg de prononcer la décharge des rappels de taxe sur les surfaces commerciales auxquels elle a été assujettie au titre de l'année 2011 à raison des établissements qu'elle exploite à Sarrebourg, Sarreguemines, Moulins-lès-Metz, Terville, Saint-Avold (Moselle), Vendenheim, Schweighouse-sur-Moder (Bas-Rhin) et Kingersheim (Haut-Rhin). Par un jugement n° 1903056 du 27 octobre 2020, ce tribunal a fait droit à sa demande.  <br/>
<br/>
              Par un pourvoi, enregistré le 8 janvier 2021 au secrétariat du contentieux du Conseil d'Etat, le ministre de l'économie, des finances et de la relance demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter la demande de la société But International.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - la loi n° 72-657 du 13 juillet 1972 ;<br/>
              - le décret n° 95-85 du 26 janvier 1995 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Marc Vié, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Karin Ciavaldini, rapporteure publique ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que la société But International a fait l'objet d'une vérification de comptabilité à l'issue de laquelle l'administration fiscale l'a assujettie à des cotisations supplémentaires de taxe sur les surfaces commerciales au titre de l'année 2011 à raison de ses établissements situés à Sarrebourg, Sarreguemines, Moulins-lès-Metz, Terville, Saint-Avold (Moselle), Vendenheim, Schweighouse-sur-Moder (Bas-Rhin) et Kingersheim (Haut-Rhin). Après le rejet de sa réclamation, la société But International a porté le litige devant le tribunal administratif de Strasbourg qui a prononcé la décharge de ces impositions par un jugement du 27 octobre 2020. Le ministre de l'économie, des finances et de la relance se pourvoit en cassation contre ce jugement.<br/>
<br/>
              2. D'une part, l'article 3 de la loi du 13 juillet 1972 instituant des mesures en faveur de certaines catégories de commerçants et artisans âgés établit une taxe sur les surfaces commerciales assise sur la surface de vente des magasins de commerce de détail, dès lors que cette surface dépasse 400 mètres carrés, quelle que soit la forme juridique de l'entreprise qui les exploite. Cet article précise que la taxe ne s'applique pas aux établissements dont le chiffre d'affaires annuel est inférieur à 460 000 euros. L'article 6 de la même loi dispose que la taxe est due par l'exploitant de l'établissement, et que le fait générateur de la taxe est constitué par l'existence de l'établissement au 1er janvier de l'année au titre de laquelle elle est due.<br/>
<br/>
              3. D'autre part, l'article 3 de la loi précitée prévoit que le taux de la taxe varie de manière croissante en fonction du montant du chiffre d'affaires annuel par mètre carré de l'établissement assujetti, le chiffre d'affaires étant calculé conformément aux dispositions de l'article L. 651-5 du code de la sécurité sociale.<br/>
<br/>
              4. Enfin, l'article 1er du décret du 26 janvier 1995, dans sa rédaction applicable aux impositions établies au titre de l'année 2011, dispose, pour l'application des dispositions citées aux points 1 et 2 ci-dessus, que " l'établissement s'entend de l'unité locale où s'exerce tout ou partie de l'activité d'une entreprise. (...) / Il n'y a pas ouverture d'établissement en cas de changement d'exploitant pour quelque cause juridique que ce soit, notamment par transmission à titre onéreux ou gratuit ou par apport, même après fermeture pour travaux d'amélioration ou de transformation, pourvu que l'activité professionnelle demeure une activité de vente au détail. ".<br/>
<br/>
              5. Il résulte de l'ensemble de ces dispositions, dans leur version applicable aux impositions établies au titre de l'année 2011, que la taxe sur les surfaces commerciales due au titre d'une année est assise sur la surface de vente de l'établissement existant au 1er janvier de cette année, dès lors que le chiffre d'affaires réalisé l'année précédente par cet établissement excède le seuil d'assujettissement de 460 000 euros, et que son taux est déterminé en  fonction du chiffre d'affaire par mètre carré réalisé par cet établissement au cours de l'année précédente. Si le redevable de la taxe est l'exploitant de l'établissement à la date du fait générateur, il ne résulte ni des dispositions de la loi du 13 juillet 1972, ni de celles de son décret d'application du 26 janvier 1995 que le chiffre d'affaires résultant de l'exploitation de cet établissement au cours de l'année précédente, pris en compte pour apprécier si l'établissement entre dans le champ de l'impôt et pour déterminer le taux applicable, serait limité au seul chiffre d'affaire réalisé par cet exploitant lui-même. Ainsi, dans la rédaction alors applicable de ces dispositions, lorsqu'un établissement fait l'objet d'un changement d'exploitant au cours de l'année précédant celle au titre de laquelle la taxe sur les surfaces commerciales est due, notamment du fait d'une opération de fusion-absorption entraînant la dissolution sans liquidation de l'ancien exploitant, le chiffre d'affaires à retenir pour apprécier si le seuil d'assujettissement est dépassé et pour déterminer le taux de la taxe est celui qui a été réalisé par cet établissement durant l'année précédente, sans distinguer selon qu'il est imputable à l'ancien ou au nouvel exploitant.<br/>
<br/>
              6. Par suite, en jugeant que le seul chiffre d'affaires à prendre en compte pour le calcul de la taxe sur les surfaces commerciales due au titre de l'année 2011 par la société But International à raison des établissements dont elle était devenue propriétaire à l'issue d'une opération de fusion-absorption intervenue le 31 décembre 2010 était celui réalisé par elle au cours de la seule période postérieure à cette opération, à l'exclusion de celui réalisé jusqu'au 31 décembre 2010, au titre des même établissements, par la société But France qu'elle a absorbée, le tribunal administratif de Strasbourg a commis une erreur de droit. Le ministre de l'économie, des finances et de la relance est par suite fondé à demander l'annulation du jugement qu'il attaque.<br/>
<br/>
              7. Il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative.<br/>
<br/>
              8. Ainsi qu'il résulte de ce qui a été dit au point 5, le chiffre d'affaires à retenir pour apprécier si le seuil d'assujettissement est dépassé et pour déterminer le taux de la taxe sur les surfaces commerciales de la société requérante au titre de l'année 2011 est celui qui a été réalisé durant l'année 2010, sans distinguer selon qu'il est imputable à l'ancien ou au nouvel exploitant. <br/>
<br/>
              9. Il en résulte que la société But International, qui ne conteste ni la surface retenue pour établir les cotisations en litige, ni le montant du chiffre d'affaire réalisé, tous exploitants confondus, par les établissements en litige au cours de l'année 2010, n'est pas fondée à demander la décharge des cotisations de la taxe sur les surfaces commerciale mises à sa charge. <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : Le jugement du 27 octobre 2020 du tribunal administratif de Strasbourg est annulé.<br/>
Article 2 : La demande de la société But International est rejetée.<br/>
Article 3 : La présente décision sera notifiée au ministre de l'économie, des finances et de la relance et à la société par actions simplifiée (SAS) But International.  <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
