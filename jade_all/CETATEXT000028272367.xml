<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028272367</ID>
<ANCIEN_ID>JG_L_2013_12_000000354386</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/27/23/CETATEXT000028272367.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème / 5ème SSR, 04/12/2013, 354386</TITRE>
<DATE_DEC>2013-12-04</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>354386</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème / 5ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MONOD, COLIN ; SCP GARREAU, BAUER-VIOLAS, FESCHOTTE-DESBOIS</AVOCATS>
<RAPPORTEUR>M. David Moreau</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Rémi Keller</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:354386.20131204</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et les mémoires complémentaires, enregistrés les 28 novembre 2011, 27 février et 10 juillet 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. A...B..., demeurant ... ; M. B... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler le jugement n° 0807279 du 27 septembre 2011 par lequel le tribunal administratif de Lyon a rejeté sa demande tendant à la réparation du préjudice qu'il a subi du fait des agissements du directeur du département arabe/turc/persan de l'université Jean Moulin Lyon III ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. David Moreau, Maître des Requêtes,  <br/>
<br/>
              - les conclusions de M. Rémi Keller, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Monod, Colin, avocat de M. B... et à la SCP Garreau, Bauer-Violas, Feschotte-Desbois, avocat de l'université Jean Moulin Lyon III  ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis au juge du fond que M. B..., maître de conférences au sein du département arabe/turc/persan de l'université Jean Moulin Lyon III, a saisi le tribunal administratif de Lyon le 15 octobre 2008 d'une demande tendant à obtenir la réparation du préjudice qu'il estimait avoir subi du fait des agissements du directeur du département arabe/turc/persan de l'université à son encontre dans le cadre de son service ; que, par un jugement du 27 septembre 2011, le tribunal administratif de Lyon a rejeté sa demande comme irrecevable en l'absence de liaison du contentieux ; que M. B... se pourvoit en cassation contre ce jugement ; <br/>
<br/>
              2. Considérant qu'aux termes de l'article R. 222-13 du code de justice administrative : " Le président du tribunal administratif ou le magistrat qu'il désigne à cette fin et ayant atteint au moins le grade de premier conseiller statue en audience publique et après audition du commissaire du gouvernement:/ (...) 2° Sur les litiges relatifs à la situation individuelle des fonctionnaires ou agents de l'Etat (...) à l'exception de ceux concernant l'entrée au service, la discipline et la sortie du service " ; que la compétence résultant du 2° de l'article R. 222-13 vaut pour tous les litiges relatifs à la situation individuelle des fonctionnaires et agents publics, dès lors qu'ils ne se rapportent ni à l'entrée au service, ni à la discipline, ni à la sortie du service et s'étend, en particulier, aux conclusions indemnitaires qui se rapportent à ces litiges, quel que soit le montant des indemnités demandées ; qu'ainsi, le président délégué du tribunal administratif de Lyon était compétent pour statuer, en vertu des dispositions précitées, sur les conclusions présentées par M. B... tendant au versement d'une somme de 15 000 euros en réparation du préjudice qu'il estime avoir subi dans le cadre de son service ;<br/>
<br/>
              3. Considérant qu'aucune fin de non-recevoir tirée du défaut de décision préalable ne peut être opposée à un requérant ayant introduit devant le juge administratif un contentieux indemnitaire à une date où il n'avait présenté aucune demande en ce sens devant l'administration lorsqu'il a formé, postérieurement à l'introduction de son recours juridictionnel, une demande auprès de l'administration sur laquelle le silence gardé par celle-ci a fait naître une décision implicite de rejet avant que le juge de première instance ne statue, et ce quelles que soient les conclusions du mémoire en défense de l'administration ; qu'en revanche, une telle fin de non-recevoir peut être opposée lorsque, à la date à laquelle le juge statue, le requérant s'est borné à l'informer qu'il avait saisi l'administration d'une demande mais qu'aucune décision de l'administration, ni explicite ni implicite, n'est encore née ; <br/>
<br/>
              4. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que M. B... a saisi le tribunal administratif de Lyon de conclusions indemnitaires sans avoir au préalable présenté de demande en ce sens devant l'administration ; que si le requérant fait valoir que, par un mémoire enregistré au greffe du tribunal administratif le 11 août 2011, il avait informé la juridiction de ce qu'il avait adressé, le 10 août 2011, une demande préalable au président de l'université tendant au versement d'une somme de 15 000 euros en réparation du préjudice qu'il estimait avoir subi, cette circonstance n'était pas de nature à faire obstacle à ce que ses conclusions soient rejetées comme irrecevables dès lors qu'aucune décision de l'administration n'était intervenue le 27 septembre 2011, date à laquelle le tribunal a statué sur sa requête ; que, par ailleurs, le contentieux ne s'était pas non plus trouvé lié par les conclusions en défense de l'université dans la mesure où cette dernière avait conclu, à titre principal, à l'irrecevabilité de la requête faute de décision préalable et, à titre subsidiaire seulement, au rejet au fond ; que, dès lors, le tribunal, qui n'a commis ni erreur de droit ni dénaturation des faits, a pu sans méconnaître son office, rejeter les conclusions présentées par M. B... comme irrecevables ;<br/>
<br/>
              5. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mise à la charge de l'Etat, qui n'est pas partie dans la présente instance, la somme que demande M. B... au titre des frais exposés par lui et non compris dans les dépens ; qu'il n'y a pas lieu, dans les circonstances de l'espèce, de mettre à la charge du requérant la somme de 1 500 euros demandée par l'université Jean Moulin Lyon III au titre des frais exposés par elle et non compris dans les dépens ; <br/>
<br/>
<br/>
<br/>
<br/>			D E C I D E :<br/>
              			--------------<br/>
<br/>
Article 1er : Le pourvoi de M. B... est rejeté.<br/>
Article 2 : Les conclusions de l'université Jean Moulin Lyon III tendant à l'application de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La présente décision sera notifiée à M. A...B... et à l'université Jean Moulin Lyon III.<br/>
Copie en sera adressée pour information à la ministre de l'enseignement supérieur et de la recherche.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-01-02-007 PROCÉDURE. INTRODUCTION DE L'INSTANCE. LIAISON DE L'INSTANCE. LIAISON DU CONTENTIEUX POSTÉRIEURE À L'INTRODUCTION DE L'INSTANCE. - ABSENCE - RECOURS INDEMNITAIRE FORMÉ DEVANT LE JUGE ADMINISTRATIF À UNE DATE OÙ AUCUNE DEMANDE N'A ÉTÉ FORMÉE DEVANT L'ADMINISTRATION - DEMANDE FORMÉE POSTÉRIEUREMENT À L'INTRODUCTION DE L'INSTANCE - CAS OÙ AUCUNE DÉCISION N'EST NÉE À LA DATE À LAQUELLE LE JUGE STATUE [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-07-01-03-02 PROCÉDURE. POUVOIRS ET DEVOIRS DU JUGE. QUESTIONS GÉNÉRALES. CONCLUSIONS. CONCLUSIONS IRRECEVABLES. - DÉFAUT DE LIAISON DU CONTENTIEUX - RECOURS INDEMNITAIRE FORMÉ DEVANT LE JUGE ADMINISTRATIF À UNE DATE OÙ AUCUNE DEMANDE N'A ÉTÉ FORMÉE DEVANT L'ADMINISTRATION - DEMANDE FORMÉE POSTÉRIEUREMENT À L'INTRODUCTION DE L'INSTANCE [RJ1] - POSSIBILITÉ POUR LE JUGE D'OPPOSER UNE FIN DE NON-RECEVOIR SANS ATTENDRE LA NAISSANCE D'UNE DÉCISION IMPLICITE OU EXPLICITE AVANT DE STATUER - EXISTENCE.
</SCT>
<ANA ID="9A"> 54-01-02-007 Aucune fin de non-recevoir tirée du défaut de décision préalable ne peut être opposée à un requérant ayant introduit devant le juge administratif un contentieux indemnitaire à une date où il n'avait présenté aucune demande en ce sens devant l'administration lorsqu'il a formé, postérieurement à l'introduction de son recours juridictionnel, une demande auprès de l'administration sur laquelle le silence gardé par celle-ci a fait naître une décision implicite de rejet avant que le juge de première instance ne statue, et ce quelles que soient les conclusions du mémoire en défense de l'administration. En revanche, une telle fin de non-recevoir peut être opposée lorsque, à la date à laquelle le juge statue, le requérant s'est borné à l'informer qu'il avait saisi l'administration d'une demande mais qu'aucune décision de l'administration, ni explicite ni implicite, n'était encore née.</ANA>
<ANA ID="9B"> 54-07-01-03-02 Aucune fin de non-recevoir tirée du défaut de décision préalable ne peut être opposée à un requérant ayant introduit devant le juge administratif un contentieux indemnitaire à une date où il n'avait présenté aucune demande en ce sens devant l'administration lorsqu'il a formé, postérieurement à l'introduction de son recours juridictionnel, une demande auprès de l'administration sur laquelle le silence gardé par celle-ci a fait naître une décision implicite de rejet avant que le juge de première instance ne statue, et ce quelles que soient les conclusions du mémoire en défense de l'administration. En revanche, une telle fin de non-recevoir peut être opposée lorsque, à la date à laquelle le juge statue, le requérant s'est borné à l'informer qu'il avait saisi l'administration d'une demande mais qu'aucune décision de l'administration, ni explicite ni implicite, n'était encore née.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 11 avril 2008, Etablissement français du sang, n° 281374, p. 168.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
