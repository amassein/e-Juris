<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038566428</ID>
<ANCIEN_ID>JG_L_2019_06_000000419856</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/56/64/CETATEXT000038566428.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème - 9ème chambres réunies, 07/06/2019, 419856, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-06-07</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>419856</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème - 9ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP COUTARD, MUNIER-APAIRE</AVOCATS>
<RAPPORTEUR>M. Paul-François Schira</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Anne Iljic</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:419856.20190607</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 16 avril et 13 juillet 2018 et le 16 mai 2019 au secrétariat du contentieux du Conseil d'Etat, Mme F...A..., Mme C...D...et M. B...D...demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret du 13 février 2018 portant classement au titre des monuments historiques d'objets mobiliers conservés au château de Craon à Haroué (Meurthe-et-Moselle) ; <br/>
<br/>
              2°) de condamner l'Etat à leur verser la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la Constitution, et notamment son Préambule ;<br/>
              - le traité sur le fonctionnement de l'Union européenne ;<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - le règlement n° 116/2009/CEE du Conseil du 18 décembre 2008 ;<br/>
              - le code du patrimoine ;<br/>
              - le code des relations entre le public et l'administration ;<br/>
              - la loi n° 2016-925 du 7 juillet 2016 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Paul-François Schira, auditeur,  <br/>
<br/>
              - les conclusions de Mme Anne Iljic, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Coutard, Munier-Apaire, avocat de Mme E...A..., de Mme C...D...et de M. B...D...;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. MmeA..., Mme D...et M. D...demandent l'annulation pour excès de pouvoir du décret du 13 février 2018 classant au titre des monuments historiques un ensemble d'objets mobiliers conservés au château de Craon à Haroué (Meurthe-et-Moselle) appartenant à MmeA.... <br/>
<br/>
Sur la légalité externe du décret du 13 février 2018 : <br/>
<br/>
              2. En premier lieu, l'article 113 de la loi du 7 juillet 2016 dispose que : " La Commission nationale des monuments historiques, la Commission nationale des secteurs sauvegardés et les commissions régionales du patrimoine et des sites sont maintenues jusqu'à la publication des décrets mentionnés aux articles L. 611-1 et L. 611-2 du code du patrimoine, dans leur rédaction résultant de la présente loi, et au plus tard, jusqu'au 1er juillet 2017. / Pendant ce délai : 1° La Commission nationale des monuments historiques exerce les missions dévolues à la Commission nationale du patrimoine et de l'architecture par les sections 1 à 4 et 6 du chapitre Ier et par le chapitre II du titre II du livre VI du code du patrimoine ; [...] Les avis émis par les commissions mentionnées au premier alinéa du présent article entre le 1er janvier 2006 et la date de publication de la présente loi tiennent lieu des avis de la Commission nationale du patrimoine et de l'architecture et des commissions régionales du patrimoine et de l'architecture prévus au livre VI du code du patrimoine, selon la même répartition qu'aux 1° à 3° du présent article ". Il résulte de ces dispositions que la consultation de la commission nationale du patrimoine et de l'architecture a remplacé la consultation de la commission nationale des monuments historiques, notamment pour les besoins de la procédure de classement d'office à compter du 9 juillet 2016, date d'entrée en vigueur de la loi du 7 juillet 2016. Il résulte toutefois des dispositions transitoires prévues à l'article 113 de cette loi que les avis émis, en application de l'article L. 622-4 du code du patrimoine, par la commission nationale des monuments historiques entre le 1er janvier 2006 et le 8 juillet 2016, date de publication de la loi, tiennent lieu des avis de la commission nationale du patrimoine et de l'architecture.<br/>
<br/>
              3. Il ressort des pièces du dossier que le décret attaqué procédant au classement d'office des biens appartenant à Mme A...sur le fondement de l'article L. 622-4 du code du patrimoine, pris le 13 février 2018, a été précédé d'un avis de la commission nationale des monuments historiques émis le 1er avril 2016. Il résulte de ce qui a été dit précédemment que cet avis, rendu avant le 8 juillet 2016, tient lieu de l'avis de la commission nationale du patrimoine et de l'architecture prévu à l'article L. 622-4 du code du patrimoine dans sa rédaction applicable au présent litige. Il s'ensuit que le moyen tiré de ce que le décret attaqué aurait dû être précédé d'un avis de la commission nationale du patrimoine et de l'architecture doit être écarté.<br/>
<br/>
              4. En deuxième lieu, le troisième alinéa de l'article R. 622-4 du code du patrimoine dispose que : " Lorsque le ministre chargé de la culture est saisi par le préfet d'une demande ou d'une proposition de classement, il statue après avoir recueilli l'avis de la Commission nationale des monuments historiques. Il consulte également la Commission nationale des monuments historiques lorsqu'il prend l'initiative d'un classement. Il informe la commission, avant qu'elle ne rende son avis, de l'avis du propriétaire ou de l'affectataire domanial sur la proposition de classement ". Aux termes de l'article L. 211-2 du code des relations entre le public et l'administration : " Les personnes physiques ou morales ont le droit d'être informées sans délai des motifs des décisions administratives individuelles défavorables qui les concernent ". Aux termes de l'article L. 121-1 du même code : " Les décisions mentionnées à l'article L. 211-2 n'interviennent qu'après que la personne intéressée a été mise à même de présenter des observations écrites et, le cas échéant, sur sa demande, des observations orales. Cette personne peut se faire assister par un conseil ou représenter par un mandataire de son choix ". Aux termes de l'article R. 133-10 du même code : " Le quorum est atteint lorsque la moitié au moins des membres composant la commission sont présents, y compris les membres prenant part aux débats au moyen d'une conférence téléphonique ou audiovisuelle, ou ont donné mandat ". Aux termes de l'arrêté du 7 septembre 2012 portant règlement intérieur de la commission nationale des monuments historiques : " la convocation aux réunions de chaque section et aux réunions du comité des sections est adressée, avec l'ordre du jour, aux membres titulaires par les mêmes moyens, 15 jours au moins avant la date de chaque séance ". <br/>
<br/>
              5. D'une part, il ressort des pièces du dossier et notamment du procès-verbal de la séance du 1er avril 2016 de la commission nationale des monuments historiques que ses membres ont été régulièrement convoqués le 17 mars 2016, que plus de la moitié de ces derniers étaient présents et que la commission était informée du sens de l'avis de Mme A...sur la procédure de classement dont le ministre chargé de la culture n'était pas tenu de préciser les motifs. D'autre part, il ressort de pièces du dossier que le ministre de la culture a adressé à Mme A..., le 20 septembre 2016, une lettre l'informant de l'avis émis le 1er avril 2016 et l'invitant à présenter ses observations sur le projet de classement, ce qu'elle a fait par courrier du 10 octobre 2016. Dans ces conditions, Mme A...a, en tout état de cause, été mise à même de faire valoir ses observations préalablement à l'édiction du décret attaqué. Il s'ensuit que le moyen tiré de l'irrégularité de la procédure préalable à l'édiction du décret attaqué doit être écarté. <br/>
<br/>
              6. En troisième lieu, les requérants ne sauraient utilement invoquer la méconnaissance des stipulations de l'article 6 de la convention européenne des droits de l'homme et des libertés fondamentales qui ne sont pas applicables à la procédure litigieuse.<br/>
<br/>
Sur la légalité interne du décret du 13 février 2018 :<br/>
<br/>
En ce qui concerne les moyens tirés de l'impossibilité de classer au titre des monuments historiques les biens bénéficiant de certificats d'exportation :<br/>
<br/>
              7. Aux termes de l'article L. 111-1 du code du patrimoine, dans sa version applicable au litige : " Les biens appartenant aux collections publiques et aux collections des musées de France, les biens classés en application des dispositions relatives aux monuments historiques et aux archives, ainsi que les autres biens qui présentent un intérêt majeur pour le patrimoine national au point de vue de l'histoire, de l'art ou de l'archéologie sont considérés comme trésors nationaux ". Aux termes de l'article L. 111-2 du code du patrimoine : " L'exportation temporaire ou définitive hors du territoire douanier des biens culturels, autres que les trésors nationaux, qui présentent un intérêt historique, artistique ou archéologique et entrent dans l'une des catégories définies par décret en Conseil d'Etat est subordonnée à l'obtention d'un certificat délivré par l'autorité administrative. Ce certificat atteste à titre permanent que le bien n'a pas le caractère de trésor national ". Aux termes de l'article L. 111-6 du même code : " En cas de refus du certificat, toute demande nouvelle pour le même bien est irrecevable pendant une durée de trente mois à compter de la date du refus. (...) Après ce délai, le refus de délivrance du certificat ne peut être renouvelé que dans le cas prévu pour la procédure d'offre d'achat au sixième alinéa de l'article L. 121-1, sans préjudice de la possibilité de classement du bien en application des dispositions relatives aux monuments historiques ou aux archives, ou de sa revendication par l'Etat en application des dispositions relatives aux fouilles archéologiques ou aux biens culturels maritimes. (...) Les demandes de certificat sont également irrecevables en cas d'offre d'achat du bien par l'Etat dans les conditions prévues à l'article L. 121-1, jusqu'à l'expiration des délais prévus aux cinquième, sixième et septième alinéas du même article ". Aux termes du dernier alinéa de l'article L. 622-4 du même code : " Le classement pourra donner lieu au paiement d'une indemnité représentative du préjudice résultant pour le propriétaire de l'application de la servitude de classement d'office. La demande d'indemnité devra être produite dans les six mois à dater de la notification du décret de classement. A défaut d'accord amiable, l'indemnité est fixée, selon le montant de la demande, par le tribunal d'instance ou de grande instance ". Aux termes de l'article L. 622-5 du même code : " Lorsque la conservation ou le maintien sur le territoire national d'un objet mobilier est menacée, l'autorité administrative peut notifier au propriétaire par décision sans formalité préalable une instance de classement au titre des monuments historiques. (...) A compter du jour où l'autorité administrative notifie au propriétaire une instance de classement au titre des monuments historiques, tous les effets du classement s'appliquent de plein droit à l'objet mobilier visé. Ils cessent de s'appliquer si la décision de classement n'intervient pas dans les douze mois de cette notification ".<br/>
<br/>
              8. Il résulte de ces dispositions combinées que la délivrance d'un certificat d'exportation de bien culturel ne fait obstacle ni au classement ultérieur de ce même bien au titre des dispositions relatives aux monuments historiques, ni à ce qu'il soit qualifié, à ce titre, de " trésor national ". Il suit de là que les requérants ne sont pas fondés à soutenir que l'existence de certificats d'exportation faisait obstacle au classement et à la qualification, à ce titre, de " trésor national " de l'ensemble d'objets mobiliers conservés au château de Craon à Haroué concernés par les décisions attaquées. Doivent être écartés, en conséquence, les moyens tirés de ce que l'administration aurait entaché le décret attaqué de détournement de procédure, et d'une atteinte au principe de sécurité juridique.<br/>
<br/>
En ce qui concerne le moyen tiré de l'inexacte qualification juridique dont serait entaché le décret du 13 février 2018 :<br/>
<br/>
              9. Aux termes du premier alinéa de l'article L. 622-1 du code du patrimoine : " Les objets mobiliers, soit meubles proprement dits, soit immeubles par destination, dont la conservation présente, au point de vue de l'histoire, de l'art, de la science ou de la technique, un intérêt public peuvent être classés au titre des monuments historiques par décision de l'autorité administrative ". <br/>
<br/>
              10. Il ressort des pièces du dossier que les biens mobiliers faisant l'objet du classement, dont la plupart sont l'oeuvre d'artisans renommés, tels que Pierre-Antoine Bellangé, Pierre-Philippe Thomire ou Jean-Jacques Feuchère, proviennent de la commande royale passée pour le compte de Louis XVIII pour l'ameublement du château de Saint-Ouen au bénéfice de la comtesse du Cayla. Formant un ensemble complet et cohérent exceptionnel, ils constituent, par leur réunion et leur unité de style, des éléments représentatifs du mobilier, devenu très rare, de l'époque de la Restauration. Il s'ensuit que le décret attaqué a pu légalement classer les objets qu'il vise en retenant qu'ils présentent un intérêt public pour l'histoire des arts, de l'architecture et des arts décoratifs au sens de l'article L. 622-1 du code du patrimoine.<br/>
<br/>
En ce qui concerne le moyen tiré de l'atteinte excessive au droit de propriété :<br/>
<br/>
              11. Aux termes de l'article 17 de la Déclaration des droits de l'homme et du citoyen : " La propriété étant un droit inviolable et sacré, nul ne peut en être privé, si ce n'est lorsque la nécessité publique, légalement constatée, l'exige évidemment, et sous la condition d'une juste et préalable indemnité ". Aux termes de l'article 1er du protocole additionnel à la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales : " Toute personne physique ou morale a droit au respect de ses biens. Nul ne peut être privé de sa propriété que pour cause d'utilité publique et dans les conditions prévues par la loi et les principes généraux du droit international ". <br/>
<br/>
              12. Si le décret attaqué affecte le droit de propriété de Mme A...en imposant une servitude d'utilité publique, les limitations qu'il apporte à l'exercice de ce droit sont justifiées par l'objectif d'intérêt général de conservation du patrimoine national. Ces limitations, qui ne méconnaissent pas les dispositions combinées des articles 34, 35 et 36 du traité sur le fonctionnement de l'Union européenne et du règlement 116/2009/CEE du Conseil du 18 décembre 2008 relatives aux restrictions à la libre circulation des biens culturels, et notamment des trésors nationaux, et qui peuvent donner lieu au paiement d'une indemnité représentative du préjudice résultant pour le propriétaire de l'application de la servitude du classement d'office, dans les conditions prévues au dernier alinéa de l'article L. 622-4 du code du patrimoine, sont proportionnées à cet objectif, eu égard à l'intérêt public qui s'attache à la conservation des objets mobiliers que le décret attaqué classe au titre des monuments historiques. Il s'ensuit que le moyen tiré d'une atteinte excessive au droit de propriété et à l'espérance légitime de Mme A...ne peut qu'être écarté.<br/>
<br/>
              13. Il résulte de tout ce qui précède que MmeA..., Mme D...et M. D...ne sont pas fondés à demander l'annulation du décret du 13 février 2018. Les dispositions de l'article L. 761-1 du code de justice administrative font alors obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat qui n'est pas, dans cette instance, la partie perdante.<br/>
<br/>
<br/>
<br/>
<br/>
              D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : La requête de MmeA..., de Mme D...et de M. D...est rejetée.<br/>
Article 2 : La présente décision sera notifiée à Mme F...A..., à Mme C...D..., à M. B...D...au Premier ministre et au ministre de la culture.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
