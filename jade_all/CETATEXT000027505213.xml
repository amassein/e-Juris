<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027505213</ID>
<ANCIEN_ID>JG_L_2013_06_000000356350</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/50/52/CETATEXT000027505213.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème et 3ème sous-sections réunies, 03/06/2013, 356350, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-06-03</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>356350</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème et 3ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DELAPORTE, BRIARD, TRICHET</AVOCATS>
<RAPPORTEUR>Mme Maryline Saleix</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Benoît Bohnert</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:356350.20130603</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 31 janvier et 30 avril 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour la société RREEF Investment GMBH, dont le siège est 3, avenue de Friedland à Paris (75008) ; elle demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler le jugement n° 1010571 du 30 novembre 2011 par lequel le tribunal administratif de Paris a rejeté sa demande tendant à la décharge des cotisations de taxe foncière sur les propriétés bâties auxquelles elle a été assujettie au titre des années 2008 et 2009, à raison d'un immeuble dont elle est propriétaire au 125, avenue des Champs-Elysées à Paris (75008) ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat le versement d'une somme de 8 500 euros au titre de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code général des impôts et le livre des procédures fiscales ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Maryline Saleix, Maître des Requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Benoît Bohnert, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Delaporte, Briard, Trichet, avocat de la société RREEF Investment GMBH ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'aux termes de l'article 1498 du code général des impôts : " La valeur locative de tous les biens autres que les locaux visés au I de l'article 1496 et que les établissements industriels visés à l'article 1499 est déterminée au moyen de l'une des méthodes indiquées ci-après : (...) 2° a. Pour les biens loués à des conditions de prix anormales ou occupés par leur propriétaire, occupés par un tiers à un autre titre que la location, vacants ou concédés à titre gratuit, la valeur locative est déterminée par comparaison. / Les termes de comparaison sont choisis dans la commune. Ils peuvent être choisis hors de la commune pour procéder à l'évaluation des immeubles d'un caractère particulier ou exceptionnel ; /  b. La valeur locative des termes de comparaison est arrêtée : soit en partant du bail en cours à la date de référence de la révision lorsque l'immeuble type était loué normalement à cette date, soit, dans le cas contraire, par comparaison avec des immeubles similaires situés dans la commune ou dans une localité présentant, du point de vue économique, une situation analogue à celle de la commune en cause et qui faisaient l'objet à cette date de locations consenties à des conditions de prix normales (...) " ; qu'aux termes de l'article 324 Z de l'annexe III du même code : " I. L'évaluation par comparaison consiste à attribuer à un immeuble ou à un local donné une valeur locative proportionnelle à celle qui a été adoptée pour d'autres biens de même nature pris comme types. II. Les types dont il s'agit doivent correspondre aux catégories dans lesquelles peuvent être rangés les biens de la commune visés aux articles 324 Y à 324 AC, au regard de l'affectation de la situation de la nature de la construction de son importance de son état d'entretien et de son aménagement (...) " ;<br/>
<br/>
              2. Considérant que, pour l'application de ces dispositions, lorsque l'administration fiscale renonce au terme de comparaison initialement choisi pour lui substituer un autre et que celui-ci est contesté au motif qu'il ne serait pas comparable à l'immeuble à évaluer, il appartient au juge de l'impôt de se prononcer sur les autres termes de comparaison proposés par le contribuable, afin de déterminer si l'un deux serait plus approprié que le local retenu par l'administration ;<br/>
<br/>
              3. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, par sa décision rejetant la réclamation de la société RREEF Investment GMBH, qui contestait la taxe foncière sur les propriétés bâties mise à sa charge au titre des années 2008 et 2009, à raison d'un immeuble à usage de bureaux dont elle est propriétaire sur l'avenue des Champs-Elysées à Paris, l'administration fiscale a substitué au local-type qu'elle avait initialement choisi comme terme de comparaison pour la détermination de la valeur locative de cet immeuble, le local de référence n° 246 du procès-verbal C " locaux commerciaux " de la commune de Paris/Champs-Elysées, dont la valeur locative était identique ; que, pour rejeter la demande de la société requérante, le tribunal, après avoir relevé que le local-type ainsi substitué par l'administration était pertinent au regard de sa superficie, de son architecture, de sa localisation et de son affectation, et l'avoir entériné comme terme de comparaison, a jugé qu'il n'y avait pas lieu, dès lors, d'examiner la pertinence des autres locaux-types que la société proposait comme termes de comparaison ; qu'en statuant ainsi, sans rechercher si ces autres locaux-types n'étaient pas plus appropriés que celui que l'administration avait en dernier lieu retenu, le tribunal a méconnu son office et commis une erreur de droit ; que, par suite, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, la société requérante est fondée à demander, pour ce motif, l'annulation du jugement attaqué ;<br/>
<br/>
              4. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 3 000 euros à verser à la société RREEF Investment GMBH en application de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>		D E C I D E :<br/>
              		--------------<br/>
<br/>
Article 1er : Le jugement du 30 novembre 2011 du tribunal administratif de Paris est annulé.<br/>
<br/>
Article 2 : L'affaire est renvoyée au tribunal administratif de Paris.<br/>
<br/>
Article 3 : L'Etat versera à la société RREEF Investment GMBH la somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : La présente décision sera notifiée à la société RREEF Investment GMBH et au ministre de l'économie et des finances.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
