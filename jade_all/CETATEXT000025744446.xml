<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000025744446</ID>
<ANCIEN_ID>JG_L_2012_04_000000353844</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/25/74/44/CETATEXT000025744446.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème et 3ème sous-sections réunies, 24/04/2012, 353844</TITRE>
<DATE_DEC>2012-04-24</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>353844</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème et 3ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Christian Vigouroux</PRESIDENT>
<AVOCATS/>
<RAPPORTEUR>Mme Eliane Chemla</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Nathalie Escaut</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2012:353844.20120424</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la requête enregistrée le 4 novembre 2011 au secrétariat du contentieux du Conseil d'Etat, présentée pour M. Jean-François C, demeurant ... ; M. C demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler le jugement n° 1101749 du 4 octobre 2011 par lequel le tribunal administratif de Versailles a, sur la protestation de M. Jean-Nicolas B, annulé les opérations électorales qui se sont déroulées les 20 et 27 mars 2011 pour l'élection aux fonctions de conseiller général dans le canton du Vésinet (Yvelines) ;<br/>
<br/>
              2°) de rejeter la protestation de M. B et de valider son élection en tant que conseiller général ;<br/>
<br/>
              3°) de mettre à la charge de M. B la somme de 2 500 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code électoral ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Eliane Chemla, Conseiller d'Etat, <br/>
<br/>
              - les conclusions de Mme Nathalie Escaut, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant qu'il résulte de l'instruction qu'à l'issue des opérations électorales qui se sont déroulées les 20 et 27 mars 2011 en vue de l'élection du conseiller général du canton du Vésinet (Yvelines), M. C a été élu au second tour de scrutin avec 64,47 % des suffrages exprimés ; que M. C fait appel du jugement du 4 octobre 2011 par lequel le tribunal administratif de Versailles, sur la protestation de M. B, candidat éliminé au premier tour avec 7,4 % des suffrages exprimés, l'a déclaré inéligible aux fonctions de conseiller général en raison de l'inéligibilité à ces fonctions de sa remplaçante, Mme A, et a, pour ce motif, annulé son élection ;<br/>
<br/>
              Sur l'éligibilité de Mme A :<br/>
<br/>
              Considérant qu'aux termes de l'article L. 210-1 du code électoral, dans sa rédaction applicable à l'élection contestée : " Tout candidat à l'élection au conseil général doit obligatoirement, avant chaque tour de scrutin, souscrire une déclaration de candidature dans les conditions prévues par un décret en Conseil d'Etat. Cette déclaration, revêtue de la signature du candidat, énonce les nom, prénoms, sexe, date et lieu de naissance, domicile et profession. Elle mentionne également la personne appelée à remplacer le candidat comme conseiller général dans le cas prévu à l'article L. 221. Les articles L. 155 et L. 163 sont applicables à la désignation du remplaçant. Le candidat et son remplaçant sont de sexe différent. / A cette déclaration sont jointes les pièces propres à prouver que le candidat et son remplaçant répondent aux conditions d'éligibilité prévues par l'article L. 194. / Si la déclaration de candidature n'est pas conforme aux dispositions du premier alinéa, qu'elle n'est pas accompagnée des pièces mentionnées au deuxième alinéa ou si ces pièces n'établissent pas que le candidat et son remplaçant répondent aux conditions d'éligibilité prévues par l'article L. 194, elle n'est pas enregistrée. " ; qu'aux termes de l'article L. 155 du même code : " Cette déclaration doit également indiquer les nom, prénoms, sexe, date et lieu de naissance, domicile et profession de la personne appelée à remplacer le candidat élu en cas de vacance du siège. Elle doit être accompagnée de l'acceptation écrite du remplaçant ; celui-ci doit remplir les conditions d'éligibilité exigées des candidats. Il joint les pièces de nature à prouver qu'il répond à ces conditions (...) " ; qu'aux termes de l'article L. 195 du code électoral : " Ne peuvent être élus membres du conseil général : / 11° les agents et comptables de tout ordre agissant en qualité de fonctionnaire, employés à l'assiette, à la perception et au recouvrement des contributions directes ou indirectes, et au paiement des dépenses publiques de toute nature, dans le département où ils exercent ou ont exercé leurs fonctions depuis moins de six mois " ;<br/>
<br/>
              Considérant, en premier lieu, qu'il résulte de ces dispositions que le remplaçant du candidat doit satisfaire aux mêmes conditions d'éligibilité, énoncées à l'article L. 195 du code électoral, que celles applicables au candidat ; que, par suite, le grief tiré de ce que les conditions d'éligibilité applicables au remplaçant du candidat seraient seulement celles énoncées à l'article L. 194 du code électoral doit être écarté ;<br/>
<br/>
              Considérant, en deuxième lieu, que Mme A, admise à la retraite avec effet au 1er juillet 2011, était inspectrice des impôts à la direction des services fiscaux des Yvelines ; qu'elle était chargée de fonctions de rédactrice à la division des affaires juridiques de cette direction et était appelée à traiter d'affaires de toute nature relevant de cette direction, en particulier du suivi des instances devant la cour administrative d'appel ; qu'à ce titre, elle prenait part au traitement de dossiers contentieux en matière d'assiette, de perception et de recouvrement de contributions ; qu'elle devait ainsi être regardée comme employée à l'assiette, à la perception et au recouvrement de contributions, au sens des dispositions du 11° de l'article L. 195 du code électoral alors même qu'elle exerçait ses fonctions dans le service juridique de sa direction et non dans les services directement en charge de l'établissement et du recouvrement des contributions et qu'elle n'était pas au nombre des agents habilités à se prononcer sur des demandes de rescrit en matière fiscale ; que, par suite, le tribunal administratif n'a pas commis d'erreur de droit en estimant qu'elle était inéligible aux fonctions de conseiller général, en application des dispositions du 11° de l'article L. 195 du code électoral ;<br/>
<br/>
              Considérant, en troisième lieu, que le candidat et son remplaçant doivent, ainsi qu'il a été dit, satisfaire aux mêmes conditions d'éligibilité, prévues par les articles L. 194 et L. 195 du code électoral ; qu'une déclaration de candidature de laquelle il ressort que le candidat ou son remplaçant ne répondent pas aux conditions d'éligibilité prévues par l'article L. 194 ne peut être enregistrée ; que par suite de l'inégibilité de Mme A, le préfet des Yvelines ne pouvait pas légalement procéder à l'enregistrement de la candidature de M. C et de sa remplaçante pour le scrutin des 20 et 27 mars 2011 pour les élections cantonales dans le canton du Vésinet ; qu'ainsi M. C ne pouvait légalement être candidat aux dites élections ;<br/>
<br/>
              Considérant qu'il résulte de ce qui précède que M. C n'est pas fondé à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Versailles a annulé son élection aux fonctions de conseiller général ;<br/>
<br/>
              Sur l'appel incident de M. B :<br/>
<br/>
              Considérant que si, à la suite de la communication qui lui a été donnée de la requête de M. C, M. B conclut à la réformation du jugement attaqué, en tant qu'il a rejeté sa demande tendant à ce que M. C soit déclaré inéligible sur le fondement de l'article L. 52-8 du code électoral, ces conclusions, qui n'ont pas été formulées dans le délai du recours contentieux, et alors que la voie du recours incident n'est pas ouverte en matière électorale, sont tardives et par suite irrecevables ;<br/>
<br/>
              Sur les conclusions tendant à l'application des dispositions de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              Considérant que ces dispositions font obstacle à ce qu'une somme soit mise à la charge de M. B, qui n'est pas, dans la présente instance, la partie perdante ; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>			D E C I D E :<br/>
              			--------------<br/>
<br/>
Article 1er : La requête de M. C et l'appel incident de M. B sont rejetés.<br/>
<br/>
Article 2 : La présente décision sera notifiée à M. Jean-François C, à M. Jean-Nicolas B et à Mme Marie-Aude A.<br/>
Copie en sera adressée, pour information, au ministre de l'intérieur, de l'outre-mer, des collectivités territoriales et de l'immigration.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">28-03-01-04 ÉLECTIONS ET RÉFÉRENDUM. ÉLECTIONS AU CONSEIL GÉNÉRAL. OPÉRATIONS PRÉLIMINAIRES À L'ÉLECTION. ENREGISTREMENT DES CANDIDATURES. - DÉCLARATION DE CANDIDATURE DONT IL RESSORT QUE LE CANDIDAT OU SON REMPLAÇANT NE RÉPONDENT PAS AUX CONDITIONS D'ÉLIGIBILITÉ PRÉVUES PAR L'ARTICLE L. 194 DU CODE ÉLECTORAL - CONSÉQUENCE - NON-ENREGISTREMENT.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">28-03-02 ÉLECTIONS ET RÉFÉRENDUM. ÉLECTIONS AU CONSEIL GÉNÉRAL. ÉLIGIBILITÉ. - INÉLIGIBILITÉ DES AGENTS EMPLOYÉS À L'ASSIETTE, À LA PERCEPTION ET AU RECOUVREMENT DE CONTRIBUTIONS (11° DE L'ART. L. 195 DU CODE ÉLECTORAL) - INSPECTEUR DES IMPÔTS CHARGÉ DE FONCTIONS DE RÉDACTEUR À LA DIVISION DES AFFAIRES JURIDIQUES DE LA DSF - INCLUSION.
</SCT>
<ANA ID="9A"> 28-03-01-04 Une déclaration de candidature de laquelle il ressort que le candidat ou son remplaçant ne répondent pas aux conditions d'éligibilité prévues par l'article L. 194 du code électoral ne peut être enregistrée.</ANA>
<ANA ID="9B"> 28-03-02 Un inspecteur des impôts chargé de fonctions de rédacteur à la division des affaires juridiques de la direction des services fiscaux (DSF), qui est appelé à traiter d'affaires de toute nature relevant de cette direction, en particulier du suivi des instances devant la cour administrative d'appel, prend part, à ce titre, au traitement de dossiers contentieux en matière d'assiette, de perception et de recouvrement de contributions et doit ainsi être regardé comme employé à l'assiette, à la perception et au recouvrement de contributions, au sens des dispositions du 11° de l'article L. 195 du code électoral. Il est donc inéligible aux fonctions de conseiller général.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
