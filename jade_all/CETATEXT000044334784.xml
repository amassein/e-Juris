<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044334784</ID>
<ANCIEN_ID>JG_L_2021_11_000000451512</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/33/47/CETATEXT000044334784.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème chambre, 16/11/2021, 451512, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-11-16</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>451512</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LE BRET-DESACHE</AVOCATS>
<RAPPORTEUR>Mme Catherine Moreau</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Olivier  Fuchs</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:451512.20211116</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La Commission nationale des comptes de campagne et des financements politiques a saisi, en application de l'article L. 52-15 du code électoral, le tribunal administratif de la Guadeloupe, sur le fondement de sa décision du 10 décembre 2020 par laquelle elle a rejeté le compte de campagne de Mme D... F..., candidate tête de liste aux élections municipales et communautaires qui se sont déroulées les 15 mars et 28 juin 2020 dans la commune de Saint-François (Guadeloupe). Par un jugement n° 2001166 du 18 mars 2021, le tribunal administratif de la Guadeloupe a rejeté cette saisine et fixé à 9 135 euros le montant du remboursement dû par l'Etat à Mme F....<br/>
<br/>
              Par une requête enregistrée le 8 avril 2021 au secrétariat du contentieux du Conseil d'Etat, la Commission nationale des comptes de campagne et des financements politiques demande au Conseil d'Etat d'annuler ce jugement.<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code électoral ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Catherine Moreau, conseillère d'Etat en service extraordinaire, <br/>
<br/>
              - les conclusions de M. Olivier Fuchs, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Le Bret-Desaché, avocat de Mme F... ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. Aux termes de l'article L. 52-15 du code électoral : " La commission nationale des comptes de campagne et des financements politiques approuve et, après procédure contradictoire, rejette ou réforme les comptes de campagne. Elle arrête le montant du remboursement forfaitaire prévu à l'article L. 52-11-1 (...) / Lorsque la commission a constaté que le compte de campagne n'a pas été déposé dans le délai prescrit, si le compte a été rejeté ou si, le cas échéant après réformation, il fait apparaître un dépassement du plafond des dépenses électorales, la commission saisit le juge de l'élection ". <br/>
<br/>
              2. Il résulte de l'instruction que, par une décision du 10 décembre 2020, la Commission nationale des comptes de campagne et des financements politiques a rejeté le compte de campagne de Mme D... F..., candidate tête de liste aux élections qui se sont déroulées les 15 mars et 28 juin 2020 en vue de la désignation des conseillers municipaux et communautaires de la commune de Saint-François (Guadeloupe). En application de l'article L. 52-15 du code électoral, la Commission nationale des comptes de campagne et des financements politiques a saisi le tribunal administratif de la Guadeloupe qui, par un jugement du 18 mars 2021, a rejeté cette saisine et fixé à 9 135 euros le montant du remboursement dû par l'Etat à Mme F... en application de l'article L. 52-11-1 du code électoral. La Commission nationale des comptes de campagne et des financements politiques fait appel de ce jugement. <br/>
<br/>
              3. Aux termes du premier alinéa de l'article L. 52-11-1 du code électoral : " Les dépenses électorales des candidats aux élections auxquelles l'article L. 52-4 est applicable font l'objet d'un remboursement forfaitaire de la part de l'Etat égal à 47,5 % de leur plafond de dépenses. Ce remboursement ne peut excéder le montant des dépenses réglées sur l'apport personnel des candidats et retracées dans leur compte de campagne ". Aux termes du premier alinéa de l'article L. 52-12 de ce code : " Chaque candidat ou candidat tête de liste soumis au plafonnement prévu à l'article L. 52-11 est tenu d'établir un compte de campagne retraçant, selon leur origine, l'ensemble des recettes perçues et, selon leur nature, l'ensemble des dépenses engagées ou effectuées en vue de l'élection, hors celles de la campagne officielle, par lui-même ou pour son compte, au cours de la période mentionnée à l'article L. 52-4. Sont réputées faites pour son compte les dépenses exposées directement au profit du candidat et avec l'accord de celui-ci, par les personnes physiques qui lui apportent leur soutien, ainsi que par les partis et groupements politiques qui ont été créés en vue de lui apporter leur soutien ou qui lui apportent leur soutien. Le candidat estime et inclut, en recettes et en dépenses, les avantages directs ou indirects, les prestations de services et dons en nature dont il a bénéficié. Le compte de campagne doit être en équilibre ou excédentaire et ne peut présenter un déficit (...) ". <br/>
<br/>
              4. Il résulte de ces dispositions, d'une part, que les dépenses pouvant faire l'objet d'un remboursement de la part de l'Etat sont celles, engagées par le candidat ou pour son compte pendant l'année précédant le premier mois du jour de l'élection, dont la finalité est l'obtention des suffrages des électeurs et, d'autre part, que les recettes inscrites au compte de campagne ne peuvent servir à payer que les dépenses électorales. Il appartient ainsi à la Commission nationale des comptes de campagne et des financements politiques de retrancher du montant des dépenses payées par le mandataire financier du candidat les sommes qui, bien qu'engagées pendant la campagne par le candidat tête de liste ou par ses colistiers, n'ont pas cette finalité. Ces dépenses ne pouvant être réputées avoir été réglées au moyen des recettes inscrites au compte de campagne du candidat, il y a également lieu de soustraire un montant équivalent de l'apport personnel à ce compte déclaré par l'intéressé. <br/>
<br/>
               5. Il ressort des énonciations de son jugement que le tribunal administratif a estimé, en se fondant sur une attestation du propriétaire du local loué par la candidate durant le temps nécessaire à sa campagne électorale, qu'une somme de 2 500 euros devait être retranchée du total de ses dépenses électorales, dès lors qu'une telle dépense correspondait au montant de travaux réalisés pour la rénovation de ce local dans l'intérêt du propriétaire, en vue d'une valorisation de son patrimoine. Il en a déduit que cette dépense devait être exclue du compte de campagne de Mme F.... Après réformation de ce compte, il a constaté que le montant des dépenses égal au plus à 11 935 euros étant inférieur au montant total des recettes, égal à 11 945 euros, le compte n'était plus déficitaire. Il en a déduit que la Commission nationale des comptes de campagne et des financements politiques avait rejeté à tort le compte de campagne de la candidate. Toutefois, il résulte des dispositions précitées de l'article L. 52-11-1 du code électoral que le tribunal administratif ne pouvait se borner à retrancher du total des dépenses électorales le montant de dépenses n'ayant pas ce caractère, sans en même temps retrancher ce même montant des recettes issues de l'apport personnel de la candidate. La Commission nationale des comptes de campagne et des financements politiques est dès lors fondée à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de la Guadeloupe s'est fondé sur ce motif pour rejeter sa saisine et fixer le montant du remboursement dû à Mme F.... <br/>
<br/>
              6. Il appartient au Conseil d'Etat, saisi de l'ensemble du litige par l'effet dévolutif de l'appel, d'examiner le bien-fondé de la saisine de la Commission nationale des comptes de campagne et des financements politiques.<br/>
<br/>
              7. Il résulte de ce qui a été dit ci-dessus qu'en application de l'article L. 52-11-1 du code électoral, une somme de 2 500 euros correspondant à des dépenses qui n'ont pas été engagées pour les besoins de la campagne électorale de Mme F... doit être retranchée à la fois du montant des dépenses déclarées, qui s'élevait à 14 435 euros, et de l'apport de la candidate, d'un montant déclaré de 11 500 euros. Il en résulte que la différence entre, d'une part, le total des dépenses établi à 11 935 euros et, d'autre part, le total des recettes établi à 9 445 euros se traduit par un solde déficitaire de 2 490 euros. Par suite, c'est à bon droit que la Commission nationale des comptes de campagne et des financements politiques a rejeté le compte de campagne de Mme F... et décidé, en conséquence, qu'elle n'avait pas droit au remboursement forfaitaire par l'Etat de ses dépenses de campagne. Toutefois, dans les circonstances particulières de l'espèce, il n'y a pas lieu de la déclarer inéligible en application de l'article L. 118-3 du code électoral.<br/>
<br/>
               8. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à la charge de la Commission nationale des comptes de campagne et des financements politiques, qui n'est pas la partie perdante dans la présente affaire. <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du 18 mars 2021 du tribunal administratif de la Guadeloupe est annulé. <br/>
Article 2 : Le compte de campagne de Mme F... a été rejeté à bon droit par la Commission nationale des comptes de campagne et des financements politiques. Mme F... n'a pas droit au remboursement forfaitaire de l'Etat en application de l'article L. 52-11-1 du code électoral.<br/>
Article 3 : Il n'y a pas lieu de déclarer Mme F... inéligible.<br/>
Article 4 : Les conclusions présentées par Mme F... au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée à la Commission nationale des comptes de campagne et des financements politiques et à Mme D... F....<br/>
Copie en sera adressée au ministre de l'intérieur et au ministre des outre-mer.<br/>
<br/>
<br/>
              Délibéré à l'issue de la séance du 14 octobre 2021 où siégeaient : Mme A... E..., assesseure, présidant ; M. Cyril Roger-Lacan, conseiller d'Etat et Mme Catherine Moreau, conseillère d'Etat en service extraordinaire-rapporteure. <br/>
<br/>
              Rendu le 16 novembre 2021.<br/>
<br/>
<br/>
                 La présidente : <br/>
                 Signé : Mme A... E...<br/>
 		La rapporteure : <br/>
      Signé : Mme Catherine Moreau<br/>
                 La secrétaire :<br/>
                 Signé : Mme B... C...<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
