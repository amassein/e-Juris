<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028750563</ID>
<ANCIEN_ID>JG_L_2014_03_000000359964</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/75/05/CETATEXT000028750563.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème et 4ème sous-sections réunies, 12/03/2014, 359964, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-03-12</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>359964</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème et 4ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SPINOSI ; HAAS</AVOCATS>
<RAPPORTEUR>M. Jean-Dominique Langlais</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Fabienne Lambolez</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:359964.20140312</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu le pourvoi sommaire et les mémoires complémentaires, enregistrés les 5 juin, 5 septembre et 21 septembre 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour l'EARL Les pépinières GeorgesA..., dont le siège est route de Malissard à Valence (26000), et M. B... A..., demeurant à... ; l'EARL Les pépinières Georges A...et M. A... demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 10LY02196 du 5 avril 2012 par lequel la cour administrative d'appel de Lyon, statuant sur l'appel de la commune de Valence, a rejeté leur appel incident tendant, d'une part, à l'annulation du jugement n° 0506199 du 15 juillet 2010 du tribunal administratif de Grenoble en tant qu'il statue sur l'indemnisation de leur préjudice et, d'autre part, à ce que la commune de Valence soit condamnée à verser à l'Earl Les Pépinières Georges A...les sommes de 6 981 647 euros au titre des préjudices subis de 1994 à 2005 et 6 120 000 euros au titre du préjudice d'exploitation de 2006 à 2012, et à M. A...la somme de 100 000 euros au titre de son préjudice moral ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à leur appel incident ; <br/>
<br/>
              3°) de mettre à la charge de la commune de Valence la somme de 6 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
	Vu le code général des collectivités territoriales ;<br/>
	Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Dominique Langlais, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Fabienne Lambolez, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Spinosi, avocat de l'Earl Les pépinières Georges A...et de M. A...et à Me Haas, avocat de la commune de Valence ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que l'EARL Les pépinières Georges A...et M. A...ont subi, à partir de juillet 1994, des dommages affectant des parcelles consacrées à la recherche et à l'expérimentation en matière d'arbres fruitiers, occasionnés par les gens du voyage qui stationnaient sur un terrain de sport jouxtant leur propriété et appartenant à la commune de Valence ; que, saisi d'un recours indemnitaire, le tribunal administratif de Grenoble, par un jugement du 15 juillet 2010, a estimé que la responsabilité de la commune de Valence était engagée au titre de la faute commise par son maire en faisant un usage tardif et insuffisant de ses pouvoirs de police pour mettre fin aux troubles et a mis à sa charge le versement aux requérants d'indemnités d'un montant total de 45 300 euros ; que, par un arrêt du 5 avril 2012, la cour administrative d'appel de Lyon a rejeté l'appel de la commune de Valence et l'appel incident de l'EARL Les pépinières Georges A...et de M.A... ; que ces derniers se pourvoient en cassation contre cet arrêt en tant qu'il rejette leurs conclusions ; <br/>
<br/>
              2. Considérant, en premier lieu, qu'en affirmant qu'il ne résultait pas de l'instruction " que la contamination du verger par le virus de la sharka à partir de l'année 2002 pût être (...) regardée comme étant en lien avec les intrusions indésirables ", la cour a répondu par une motivation suffisante et sans dénaturer les écritures dont elle était saisie au moyen tiré de ce que l'impossibilité d'assurer l'entretien normal du verger expérimental aurait entraîné sa contamination par le virus de la sharka à partir de 2002 ;<br/>
<br/>
              3. Considérant, en deuxième lieu, qu'il ressort des pièces du dossier soumis aux juges du fond que les requérants, dans leur demande de première instance, puis dans leur appel incident, avaient demandé à être indemnisés de leur préjudice économique, d'une part, au titre de la période d'exploitation antérieure à la présentation en octobre 2005 de leur recours indemnitaire devant le tribunal administratif de Grenoble et, d'autre part, au titre de la période ultérieure ; que la cour, en écartant l'existence de tout préjudice économique au motif que " la dégradation à partir de 1995 du chiffre d'affaires de l'exploitation, déjà en très forte baisse dans les années 1991-1994 avant les intrusions, provient des performances commerciales inférieures aux espérances de nouvelles variétés mises sur le marché ainsi que d'une demande en régression très marquée dans les espèces fruitières concernées ", n'a pas entaché son arrêt d'omission de réponse à conclusions ni d'insuffisance de motivation ;<br/>
<br/>
              4. Considérant, en troisième lieu, que, contrairement à ce que soutiennent les requérants, la cour administrative d'appel n'a pas omis de statuer sur leurs conclusions tendant à ce que le rapport de l'expert désigné par le tribunal administratif pour évaluer leurs préjudices soit écarté des débats ; qu'en retenant que les seules circonstances que cet expert ait été membre d'un organisme professionnel intervenant dans des procédures de labellisation des semences auxquelles l'EARL Les pépinières Georges A...s'était soumise et que, dans le cadre d'anciennes fonctions, il ait participé, au nom de la société pour laquelle il travaillait alors, à une transaction immobilière avec la commune de Valence ne suffisaient pas à faire naître un doute légitime sur son impartialité, la cour n'a pas donné aux éléments soumis à son examen une qualification inexacte et n'a pas méconnu les garanties d'un procès équitable ;<br/>
<br/>
              5. Considérant, en quatrième lieu, qu'en estimant que le tribunal administratif avait fait une juste appréciation des troubles dans les conditions d'existence et de l'atteinte à la réputation de l'entreprise en accordant à ce titre aux requérants une indemnité de 34 000 euros, la cour n'a pas dénaturé les faits qui lui étaient soumis ; qu'elle n'a pas commis d'erreur de droit en indemnisant au titre de ce chef de préjudice l'atteinte à l'image de marque et à la réputation de la société dirigée par M. A... ;<br/>
<br/>
              6. Considérant, en cinquième lieu, qu'en ne se prononçant pas explicitement sur le moyen, inopérant, tiré de la méconnaissance par le maire de la commune de Valence du principe de précaution, la cour n'a pas entaché son arrêt d'irrégularité ;<br/>
<br/>
              7. Considérant, en sixième lieu, que, statuant sur les conclusions des requérants tendant à la réparation d'un préjudice économique, la cour a estimé que " la dégradation à partir de 1995 du chiffre d'affaires de l'exploitation [provenait] des performances commerciales inférieures aux espérances de nouvelles variétés mises sur le marché ainsi que d'une demande en régression très marquée dans les espèces fruitières concernées " ; que si elle a pu, dans le cadre de son pouvoir souverain d'appréciation des faits de l'espèce, estimer, au vu du rapport de l'expert, que la forte dégradation des résultats de l'entreprise n'était pas intégralement la conséquence des graves nuisances ayant perturbé l'exploitation du verger expérimental, elle a commis une erreur de droit en omettant de rechercher si ces nuisances n'avaient pas, à tout le moins, privé l'EARL et M. A...d'une chance d'exploiter les nouvelles variétés mises en culture et de limiter ainsi cette dégradation ;<br/>
<br/>
              8. Considérant qu'il résulte de tout ce qui précède que l'EARL Les pépinières Georges A...et M. A...sont fondés à demander l'annulation de l'arrêt qu'ils attaquent en tant que, après avoir admis le principe de la responsabilité de la commune de Valence, il s'est prononcé sur les droits à indemnité de l'EARL et de M.A... au titre de la perte d'une chance de limiter la dégradation des résultats de l'entreprise ;<br/>
<br/>
              Sur l'application de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              9. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la commune de Valence une somme globale de 3 000 euros à verser à l'EARL Les pépinières Georges A...et à M. A...au titre des dispositions de l'article L. 761-1 du code de justice administrative ; que ces dispositions font obstacle à ce qu'une somme soit mise à la charge de l'EARL Les pépinières Georges A...et de M.A..., qui ne sont pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Lyon du 5 avril 2012 est annulé en tant que, après avoir admis le principe de la responsabilité de la commune de Valence, il se prononce sur les conclusions de l'EARL Les pépinières Georges A...et de M. A... tendant à la réparation de la perte d'une chance de limiter la dégradation des résultats de l'entreprise.<br/>
<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Lyon dans la limite de la cassation prononcée.<br/>
<br/>
Article 3 : La commune de Valence versera à l'EARL Les pépinières Georges A...et à M. A... une somme globale de 3 000 euros en application des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : Le surplus des conclusions du pourvoi et les conclusions de la commune de Valence tendant à l'application de l'article L. 761-1 du code de justice administrative sont rejetés.<br/>
<br/>
Article 5 : La présente décision sera notifiée à l'EARL Les pépinières GeorgesA..., à M. B... A...et à la commune de Valence.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
