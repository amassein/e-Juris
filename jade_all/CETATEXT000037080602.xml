<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037080602</ID>
<ANCIEN_ID>JG_L_2018_06_000000415335</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/08/06/CETATEXT000037080602.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème - 7ème chambres réunies, 18/06/2018, 415335, Publié au recueil Lebon</TITRE>
<DATE_DEC>2018-06-18</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>415335</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème - 7ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Louise Bréhier</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Guillaume Odinet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:415335.20180618</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              M. A...B...a demandé à la Cour nationale du droit d'asile d'annuler la lettre du 23 octobre 2015 par laquelle le directeur général de l'Office français de protection des réfugiés et apatrides n'a pas fait droit à sa demande de transfert de protection et l'a invité à s'adresser au préfet du lieu de son domicile afin de présenter une demande d'asile. Par une ordonnance n° 15035113 du 11 décembre 2015, le magistrat désigné par la présidente de la cour a rejeté cette demande. <br/>
<br/>
              M. B...a formé la même demande devant le tribunal administratif de Melun. Par un jugement n° 1510510 du 28 juillet 2017, le tribunal a transmis cette demande à la Cour nationale du droit d'asile en application de l'article R. 351-3 du code de justice administrative. <br/>
<br/>
              Par une ordonnance n° 15039207 du 23 octobre 2017, la présidente de la Cour nationale du droit d'asile a transmis la demande de M. B...au président de la section du contentieux du Conseil d'Etat en application de l'article R. 351-6 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la convention de Genève du 28 juillet 1951 relative au statut des réfugiés et le protocole signé à New York le 31 janvier 1967 ;<br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Louise Bréhier, auditrice, <br/>
<br/>
              - les conclusions de M. Guillaume Odinet, rapporteur public.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1.	Considérant que selon l'article L. 731-2 du code de l'entrée et du séjour des étrangers et du droit d'asile : " La Cour nationale du droit d'asile statue sur les recours formés contre les décisions de l'Office français de protection des réfugiés et apatrides prises en application des articles L. 711-1 à L. 711-4, L. 711-6, L. 712-1 à L. 712-3, L. 713-1 à L. 713-4, L. 723-1 à L. 723-8, L. 723-11, L. 723-15 et L. 723-16 (...) " ; <br/>
<br/>
              2.	Considérant qu'il ressort des pièces du dossier que M.B..., ressortissant algérien, s'est vu reconnaître la qualité de réfugié en Italie par décision du 24 septembre 1993 ; que, titulaire d'un titre de séjour en France délivré le 6 mai 2015, il a demandé, le 11 août 2015, à l'Office français de protection des réfugiés et apatrides le transfert d'Italie en France de la protection s'attachant au statut de réfugié ; que, par une lettre du 23 octobre 2015, le directeur général de l'Office n'a pas donné suite à cette demande, en faisant valoir que l'Office ne pouvait être saisi directement et en l'invitant à s'adresser à la préfecture du lieu de son domicile afin d'y présenter une demande d'asile ; que M. B...a d'abord demandé à la Cour nationale du droit d'asile l'annulation de cette lettre par un recours formé le 24 novembre 2015, qui a été rejeté par une ordonnance du 11 décembre 2015 ; que M. B...a ensuite saisi le tribunal administratif de Melun qui, par un jugement du 28 juillet 2017, a transmis ce recours à la Cour nationale du droit d'asile par application de l'article R. 351-3 du code de justice administrative ; que par une ordonnance du 23 octobre 2017, la présidente de la Cour nationale du droit d'asile a transmis le recours de M. B...au président de la section du contentieux du Conseil d'Etat sur le fondement de l'article R. 351-6 du code de justice administrative, afin que soit réglée la question de la compétence pour connaître de ce recours ; <br/>
<br/>
              3.	Considérant qu'aux termes du 2 du A de l'article 1er de la convention de Genève du 28 juillet 1951 et du protocole signé à New York le 31 janvier 1967, doit être considérée comme réfugiée toute personne  " qui, craignant avec raison d'être persécutée du fait de sa race, de sa religion, de sa nationalité, de son appartenance à un certain groupe social ou de ses opinions politiques, se trouve hors du pays dont elle a la nationalité et qui ne peut, ou, du fait de cette crainte, ne veut se réclamer de la protection de ce pays ; ou qui, si elle n'a pas de nationalité et se trouve hors du pays dans lequel elle avait sa résidence habituelle à la suite de tels événements, ne peut ou, en raison de ladite crainte, ne veut y retourner " ; qu'aux termes du 1 de l'article 31 de cette même convention : " Les Etats contractants n'appliqueront pas de sanctions pénales, du fait de leur entrée ou de leur séjour irréguliers, aux réfugiés qui, arrivant directement du territoire où leur vie ou leur liberté était menacée au sens prévu par l'article premier, entrent ou se trouvent sur leur territoire sans autorisation, sous la réserve qu'ils se présentent sans délai aux autorités et leur exposent des raisons reconnues valables de leur entrée ou présence irrégulières " ; qu'aux termes du 1 de l'article 33 de cette même convention : " Aucun des Etats contractants n'expulsera ou ne refoulera, de quelque manière que ce soit, un réfugié sur les frontières des territoires où sa vie ou sa liberté serait menacée en raison de sa race, de sa religion, de sa nationalité, de son appartenance à un certain groupe social ou de ses opinions politiques " ;<br/>
<br/>
              4.	Considérant que si, en vertu de ces stipulations, une personne qui s'est vu reconnaître le statut de réfugié dans un Etat partie à la convention de Genève à raison de persécutions subies dans l'Etat dont elle a la nationalité ne peut plus, aussi longtemps que le statut de réfugié lui est maintenu et effectivement garanti dans l'Etat qui lui a reconnu ce statut, revendiquer auprès d'un autre Etat, sans y avoir été préalablement admise au séjour, le bénéfice des droits qu'elle tient de la convention de Genève, il est toutefois loisible à cette personne, dans le cas où elle a été préalablement admise au séjour en France dans le cadre des procédures de droit commun applicables aux étrangers, de demander à ce que l'Office français de protection des réfugiés et apatrides exerce à son égard la protection qui s'attache au statut de réfugié ; qu'en l'absence de dispositions spéciales organisant un tel transfert, une telle demande doit être présentée dans les formes et selon les règles procédurales applicables aux demandes d'asile ; <br/>
<br/>
              5.	Considérant, à cet égard, que toute demande d'asile doit, en vertu des dispositions des articles L. 741-1 et R. 741-1 du code de l'entrée et du séjour des étrangers et du droit d'asile être présentée en personne à la préfecture afin d'y être enregistrée ; que, selon l'article L. 741-2 du même code, l'Office français de protection des réfugiés et apatrides " ne peut être saisi d'une demande d'asile que si celle-ci a été préalablement enregistrée par l'autorité administrative compétente et si l'attestation de demande d'asile a été remise à l'intéressé " ; qu'aux termes de l'article R. 741-2 du même code : " Lorsque l'étranger présente sa demande auprès de l'Office français de l'immigration et de l'intégration, des services de police ou de gendarmerie ou de l'administration pénitentiaire, la personne est orientée vers l'autorité compétente. Il en est de même lorsque l'étranger a introduit directement sa demande auprès de l'Office français de protection des réfugiés et apatrides, sans que sa demande ait été préalablement enregistrée par le préfet compétent. Ces autorités fournissent à l'étranger les informations utiles en vue de l'enregistrement de sa demande d'asile. (...) " ;<br/>
<br/>
              6.	Considérant, en l'espèce, que l'Office français de protection des réfugiés et apatrides a été directement saisi par M. B...d'une demande de " transfert de protection " sans qu'aucune demande n'ait été préalablement présentée et enregistrée en préfecture ; que la lettre par laquelle le directeur général de l'Office français de protection des réfugiés et apatrides n'a pas donné suite à sa demande et l'a, conformément à ce que prévoient les dispositions précédemment citées de l'article R. 741-2, invité à s'adresser au préfet du lieu de son domicile afin d'obtenir un formulaire de demande d'asile, n'est pas au nombre des décisions de l'Office français de protection des réfugiés et apatrides qui sont susceptibles d'être contestées devant la Cour nationale du droit d'asile en vertu de l'article L. 731-2 du code de l'entrée et du séjour des étrangers et du droit d'asile ; que le recours formé par M. B...relève ainsi de la compétence des juridictions administratives de droit commun ; qu'il y a lieu, par suite, d'en attribuer le jugement au tribunal administratif de Melun, compétent pour en connaître en vertu de l'article R. 312-1 du code de justice administrative ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : Le jugement de la demande de M. B...est attribué au tribunal administratif de Melun.<br/>
<br/>
Article 2 : La présente décision sera notifiée à M. A...B..., à l'Office français de protection des réfugiés et apatrides, à la présidente du tribunal administratif de Melun et à la présidente de la Cour nationale du droit d'asile. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">095-03 - CAS D'UNE PERSONNE S'ÉTANT DÉJÀ VU RECONNAÎTRE LE STATUT DE RÉFUGIÉ DANS UN ETAT MEMBRE DE L'UNION EUROPÉENNE - PRINCIPE - IMPOSSIBILITÉ DE SOLLICITER DE LA FRANCE LE BÉNÉFICE D'UNE PROTECTION CONVENTIONNELLE OU SUBSIDIAIRE - EXCEPTION - PERSONNE ADMISE AU SÉJOUR - POSSIBILITÉ DE DÉPOSER UNE DEMANDE TENDANT À CE QUE L'OFPRA EXERCE À SON ÉGARD LA PROTECTION QUI S'ATTACHE AU STATUT DE RÉFUGIÉ - EXISTENCE [RJ1] - RÈGLES DE FORME ET DE PROCÉDURE APPLICABLES - RÈGLES APPLICABLES AUX DEMANDES D'ASILE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">095-03-04 - CAS D'UNE PERSONNE S'ÉTANT DÉJÀ VU RECONNAÎTRE LE STATUT DE RÉFUGIÉ DANS UN ETAT MEMBRE DE L'UNION EUROPÉENNE - POSSIBILITÉ DE DÉPOSER UNE DEMANDE TENDANT À CE QUE L'OFPRA EXERCE À SON ÉGARD LA PROTECTION QUI S'ATTACHE AU STATUT DE RÉFUGIÉ - EXISTENCE - CONDITIONS [RJ1] - RÈGLES DE FORME ET DE PROCÉDURE APPLICABLES - RÈGLES APPLICABLES AUX DEMANDES D'ASILE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">095-07-01-02 - RECOURS DIRIGÉ CONTRE UN COURRIER DE L'OFPRA INVITANT UNE PERSONNE L'AYANT SAISI DIRECTEMENT D'UNE DEMANDE DE TRANSFERT DE PROTECTION [RJ1] À S'ADRESSER AU PRÉFET DU LIEU DE SON DOMICILE AFIN D'OBTENIR UN FORMULAIRE DE DEMANDE D'ASILE (ART. R. 741-2 DU CESEDA) - EXCLUSION [RJ2].
</SCT>
<SCT ID="8D" TYPE="PRINCIPAL">17-05-01-01 COMPÉTENCE. COMPÉTENCE À L'INTÉRIEUR DE LA JURIDICTION ADMINISTRATIVE. COMPÉTENCE EN PREMIER RESSORT DES TRIBUNAUX ADMINISTRATIFS. COMPÉTENCE MATÉRIELLE. - RECOURS DIRIGÉ CONTRE UN COURRIER DE L'OFPRA INVITANT UNE PERSONNE L'AYANT SAISI DIRECTEMENT D'UNE DEMANDE DE TRANSFERT DE PROTECTION [RJ1] À S'ADRESSER AU PRÉFET DU LIEU DE SON DOMICILE AFIN D'OBTENIR UN FORMULAIRE DE DEMANDE D'ASILE (ART. R. 741-2 DU CESEDA) - INCLUSION [RJ2].
</SCT>
<ANA ID="9A"> 095-03 Si, en vertu des stipulations du 2 du A de l'article 1er et du 1 des articles 31 et 33 de la convention de Genève du 28 juillet 1951 ainsi que du protocole signé à New York le 31 janvier 1967, une personne qui s'est vu reconnaître le statut de réfugié dans un Etat partie à la convention de Genève à raison de persécutions subies dans l'Etat dont elle a la nationalité ne peut plus, aussi longtemps que le statut de réfugié lui est maintenu et effectivement garanti dans l'Etat qui lui a reconnu ce statut, revendiquer auprès d'un autre Etat, sans y avoir été préalablement admise au séjour, le bénéfice des droits qu'elle tient de la convention de Genève, il est toutefois loisible à cette personne, dans le cas où elle a été préalablement admise au séjour en France dans le cadre des procédures de droit commun applicables aux étrangers, de demander à ce que l'Office français de protection des réfugiés et apatrides (OFPRA) exerce à son égard la protection qui s'attache au statut de réfugié. En l'absence de dispositions spéciales organisant un tel transfert, une telle demande doit être présentée dans les formes et selon les règles procédurales applicables aux demandes d'asile.</ANA>
<ANA ID="9B"> 095-03-04 Si, en vertu des stipulations du 2 du A de l'article 1er et du 1 des articles 31 et 33 de la convention de Genève du 28 juillet 1951 ainsi que du protocole signé à New York le 31 janvier 1967, une personne qui s'est vu reconnaître le statut de réfugié dans un Etat partie à la convention de Genève à raison de persécutions subies dans l'Etat dont elle a la nationalité ne peut plus, aussi longtemps que le statut de réfugié lui est maintenu et effectivement garanti dans l'Etat qui lui a reconnu ce statut, revendiquer auprès d'un autre Etat, sans y avoir été préalablement admise au séjour, le bénéfice des droits qu'elle tient de la convention de Genève, il est toutefois loisible à cette personne, dans le cas où elle a été préalablement admise au séjour en France dans le cadre des procédures de droit commun applicables aux étrangers, de demander à ce que l'Office français de protection des réfugiés et apatrides (OFPRA) exerce à son égard la protection qui s'attache au statut de réfugié. En l'absence de dispositions spéciales organisant un tel transfert, une telle demande doit être présentée dans les formes et selon les règles procédurales applicables aux demandes d'asile.</ANA>
<ANA ID="9C"> 095-07-01-02 Requérant ayant saisi l'OFPRA d'une demande de transfert de protection sans qu'aucune demande n'ait préalablement été présentée et enregistrée en préfecture.... ,,La lettre par laquelle le directeur général de l'OFPRA n'a pas donné suite à cette demande et a, conformément à ce que prévoient les dispositions de l'article R. 741-2 du code de l'entrée et du séjour des étrangers et du droit d'asile (CESEDA), invité l'intéressé à s'adresser au préfet du lieu de son domicile afin d'obtenir un formulaire de demande d'asile, n'est pas au nombre des décisions de l'OFPRA qui sont susceptibles d'être contestées devant la Cour nationale du droit d'asile (CNDA) en vertu de l'article L. 731-2 de ce code. Le recours formé par le requérant relève ainsi de la compétence des juridictions administratives de droit commun. Attribution du jugement de l'affaire au tribunal administratif territorialement compétent.</ANA>
<ANA ID="9D"> 17-05-01-01 Requérant ayant saisi l'OFPRA d'une demande de transfert de protection sans qu'aucune demande n'ait préalablement été présentée et enregistrée en préfecture.... ,,La lettre par laquelle le directeur général de l'OFPRA n'a pas donné suite à cette demande et a, conformément à ce que prévoient les dispositions de l'article R. 741-2 du code de l'entrée et du séjour des étrangers et du droit d'asile (CESEDA), invité l'intéressé à s'adresser au préfet du lieu de son domicile afin d'obtenir un formulaire de demande d'asile, n'est pas au nombre des décisions de l'OFPRA qui sont susceptibles d'être contestées devant la Cour nationale du droit d'asile (CNDA) en vertu de l'article L. 731-2 de ce code. Le recours formé par le requérant relève ainsi de la compétence des juridictions administratives de droit commun. Attribution du jugement de l'affaire au tribunal administratif territorialement compétent.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf., en précisant, CE, Assemblée, 13 novembre 2013, Cimade et,, n°s 349735 349736, p. 269.,,[RJ2] Comp. CE, 23 décembre 2016,,, n° 403971, T. p. 647. Rappr. CE, 17 janvier 2018,,, n° 410449, à mentionner aux Tables ; CE, 17 janvier 2018,,, n° 412292, à mentionner aux Tables.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
