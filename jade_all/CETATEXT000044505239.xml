<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044505239</ID>
<ANCIEN_ID>JG_L_2021_12_000000441711</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/50/52/CETATEXT000044505239.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère - 4ème chambres réunies, 15/12/2021, 441711, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-12-15</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>441711</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère - 4ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>CABINET COLIN - STOCLET ; SCP ROCHETEAU, UZAN-SARANO</AVOCATS>
<RAPPORTEUR>Mme Cécile Chaduteau-Monplaisir</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Marie Sirinelli</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:441711.20211215</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés le 9 juillet 2020 et le 11 mai 2021 au secrétariat du contentieux du Conseil d'Etat, la Fédération des syndicats des travailleurs du rail - SUD Rail demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler la décision implicite par laquelle le président de la société nationale SNCF a rejeté sa demande du 11 mars 2020, reçue le 13 mars 2020, tendant à l'abrogation du b) du § 2 des " dispositions diverses " de l'instruction RH00677 du 16 mars 2017 portant dispositions complémentaires à l'accord d'entreprise sur l'organisation du temps de travail du 14 juin 2016 ; <br/>
<br/>
              2°) d'enjoindre à la société nationale SNCF, sur le fondement des articles L. 911-1 et L. 911-3 du code de justice administrative, d'abroger ces dispositions dans un délai de deux semaines à compter de la notification de la décision à intervenir, sous astreinte de 1 000 euros par jour de retard ;<br/>
<br/>
              3°) de mettre à la charge de la société nationale SNCF la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code des transports ;<br/>
              - le code du travail ;<br/>
              - la loi du 24 mai 1872 relative au Tribunal des conflits ;<br/>
              - la loi n° 2014-872 du 4 août 2014 ;<br/>
              - la loi n° 2018-515 du 27 juin 2018 ;<br/>
              - l'ordonnance n° 2019-552 du 3 juin 2019 ;<br/>
              - le décret n° 2015-141 du 10 février 2015 ;<br/>
              - le décret n° 2016-755 du 8 juin 2016 ;<br/>
              - le décret n° 2015-233 du 27 février 2015 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Cécile Chaduteau-Monplaisir, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Marie Sirinelli, rapporteure publique ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Rocheteau, Uzan-Sarano, avocat de la Fédération des syndicats des travailleurs du rail - SUD Rail et au cabinet Colin - Stoclet, avocat de la société nationale SNCF ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. En premier lieu, l'article L. 2101-1 du code des transports, créé par la loi du 4 août 2014 portant réforme ferroviaire et applicable à la date de l'instruction en litige, dispose que : " La SNCF, SNCF Réseau et SNCF Mobilités constituent le groupe public ferroviaire au sein du système ferroviaire national " et que " le groupe remplit une mission, assurée conjointement par chacun des établissements publics dans le cadre des compétences qui leur sont reconnues par la loi, visant à exploiter le réseau ferré national et à fournir au public un service dans le domaine du transport par chemin de fer ". La loi du 27 juin 2018 pour un nouveau pacte ferroviaire a prévu la transformation, à compter du 1er janvier 2020, de ce " groupe public ferroviaire ", ainsi que des filiales des entités constituant celui-ci, en un " groupe public unifié ", constitué de la société nationale à capitaux publics SNCF, soumise aux dispositions législatives applicables aux sociétés anonymes et dont le capital est incessible et intégralement détenu par l'Etat, ainsi que de ses filiales, dont les sociétés SNCF Réseau et SNCF Mobilités, devenue SNCF Voyageurs, également soumises aux dispositions législatives applicables aux sociétés anonymes et dont le capital est incessible et intégralement détenu par la société nationale SNCF. L'article L. 2101-1 du code des transports dispose désormais que ce groupe public unifié " remplit des missions de service public dans le domaine du transport ferroviaire et de la mobilité et exerce des activités de logistique et de transport ferroviaire de marchandises, dans un objectif de développement durable, de lutte contre le réchauffement climatique, d'aménagement du territoire et d'efficacité économique et sociale ".<br/>
<br/>
              2. En deuxième lieu, l'article L. 1311-1 du code des transports dispose que : " Les dispositions du code du travail s'appliquent aux entreprises de transport ferroviaire (...) ainsi qu'à leurs salariés, sous réserve des dispositions particulières ou d'adaptation prévues par le présent code et sauf mention contraire dans le code du travail ou dans le présent code ". En vertu de l'article L. 2101-2 du même code, créé par la loi du 4 août 2014, la SNCF, SNCF Réseau et SNCF Mobilités et désormais, depuis le 1er janvier 2020, la société nationale SNCF et les sociétés relevant des activités exercées au 31 décembre 2019 par le groupe public ferroviaire, " emploient des salariés régis par un statut particulier élaboré dans des conditions fixées par décret en Conseil d'Etat " et " peuvent également employer " ou, désormais, " emploient " " des salariés sous le régime des conventions collectives ". L'article 3 de la loi du 27 juin 2018 a prévu que la SNCF, SNCF Réseau et SNCF Mobilités pouvaient procéder jusqu'au 31 décembre 2019 à des recrutements de personnels soumis à ce statut particulier. Les dispositions du statut particulier sont, en vertu de l'article 1er du décret du 10 février 2015 relatif à la commission du statut particulier mentionné à l'article L. 2101-2 du code des transports, adoptées par le conseil de surveillance de la SNCF devenu, depuis le 1er janvier 2020, le conseil d'administration de la société nationale SNCF et ne font plus l'objet, depuis cette date, d'une approbation par le ministre intéressé. L'article L. 2101-3 du code des transports prévoit en outre qu'une convention de branche ou un accord professionnel ou interprofessionnel étendu ou élargi peut, par dérogation aux dispositions des articles L. 2233-1 et L. 2233-3 du code du travail réservant l'application de conventions et accords collectifs de travail dans les entreprises publiques aux catégories de personnel qui ne sont pas soumises à un statut particulier, compléter les dispositions statutaires ou déterminer les conditions d'application du statut particulier, dans les limites fixées par ce statut. Enfin, l'article L. 2162-1 dispose qu'une convention collective de branche est applicable aux salariés des établissements publics constituant le groupe public ferroviaire, désormais société nationale SNCF, société SNCF Voyageurs, société SNCF Réseau et certaines de leurs filiales, ainsi qu'à certaines autres entreprises de transport ferroviaire.<br/>
<br/>
              3. En troisième lieu, en vertu du premier alinéa de l'article L. 1311-2 du code des transports : " La durée du travail des salariés et la durée de conduite des conducteurs sont fixées par décret en Conseil d'Etat ", notamment dans les entreprises de transport ferroviaire, et aux termes de l'article L. 2161-1 du même code : " Un décret en Conseil d'Etat fixe les règles relatives à la durée du travail communes [aux établissements publics constituant le groupe public ferroviaire et, désormais, à la société nationale SNCF, à la société SNCF Voyageurs, à la société SNCF Réseau et à certaines de leurs filiales] / Ces règles garantissent un haut niveau de sécurité des circulations et la continuité du service et assurent la protection de la santé et de la sécurité des travailleurs, en tenant compte des spécificités des métiers, notamment en matière de durée du travail et de repos ". Ce décret est intervenu le 8 juin 2016. Enfin, aux termes de l'article 34 de la loi du 4 août 2014 : " A titre transitoire, les salariés de la SNCF, de SNCF Réseau et de SNCF Mobilités conservent leur régime de durée du travail jusqu'à la publication de l'arrêté d'extension de la convention collective du transport ferroviaire ou de l'arrêté d'extension de l'accord relatif à l'organisation et à l'aménagement du temps de travail dans le transport ferroviaire, et au plus tard jusqu'au 1er juillet 2016. Pendant cette période, les organisations syndicales de salariés représentatives du groupe public ferroviaire peuvent négocier un accord collectif relatif à la durée du travail applicable aux salariés de la SNCF, de SNCF Réseau et de SNCF Mobilités. " <br/>
<br/>
              4. Il ressort des pièces du dossier qu'en application des dispositions de l'article 34 de la loi du 4 août 2014, la SNCF et deux organisations syndicales de salariés représentatives au sein du groupe public ferroviaire ont signé, le 14 juin 2016, un accord collectif relatif à l'organisation du temps de travail, applicable dans tous les établissements du groupe public ferroviaire. A cet accord ont été annexés trois textes, dont un groupe de travail paritaire devait préparer l'actualisation sans remise en cause des dispositions compatibles avec le principal de l'accord ou de celles plus favorables aux agents. Le 16 mars 2017, à l'issue des travaux de ce groupe de travail, le groupe public ferroviaire a adopté une nouvelle instruction RH00677 portant " dispositions complémentaires à l'accord d'entreprise sur l'organisation du temps de travail du 14 juin 2016 ". <br/>
<br/>
              5. La fédération requérante demande l'annulation de la décision implicite de rejet de sa demande du 13 mars 2020 tendant à l'abrogation des dispositions du b) du § 2 des " dispositions diverses " de cette instruction RH00677, relatives à la répercussion des absences sur l'octroi des repos hebdomadaires, périodiques et complémentaires ou supplémentaires. <br/>
<br/>
              6. Il ressort des pièces du dossier que les dispositions contestées de l'instruction RH00677 reprennent celles qui figuraient précédemment dans le règlement du personnel RH0677 " Réglementation du travail, instruction d'application du décret n° 99-1161 du 29 décembre 1999 " annexé à l'accord collectif de l'accord du 14 juin 2016 relatif à l'organisation du temps de travail dans les établissements du groupe public ferroviaire, le décret du 29 décembre 1999, pour sa part, ayant été abrogé par le décret du 8 juin 2016. Résultant d'un acte unilatéral de portée générale, elles peuvent être regardées comme affectant l'organisation du service public, dans la mesure où, fixant des règles de décompte des repos des agents absents, elles peuvent avoir une incidence sur l'accomplissement des missions de service public et sur la continuité du service. Elles sont, à ce titre, susceptibles de relever de la compétence du juge administratif.<br/>
<br/>
              7. Toutefois, il résulte des dispositions citées aux points 1 à 3 que l'instruction RH00677, annexée comme il a été dit à l'accord collectif du 14 juin 2016 dont elle se veut " complémentaire ", s'applique à l'ensemble des salariés relevant, à la date de son adoption, du groupe public ferroviaire et que le personnel des établissements du groupe public ferroviaire et, désormais, de la société nationale SNCF et des sociétés relevant des activités exercées antérieurement par ce groupe, est constitué à la fois de salariés sous le régime des conventions collectives et de salariés régis par un statut particulier élaboré dans des conditions fixées par décret en Conseil d'Etat. Ces derniers ne peuvent cependant plus être recrutés depuis le 1er janvier 2020 et leur statut, désormais adopté par le seul conseil d'administration de la société nationale SNCF sans être soumis à une approbation ministérielle, peut, sous certaines conditions, être complété par des conventions et accords collectifs de travail. Il en résulte également que le code du travail s'applique à l'ensemble de ces salariés à chaque fois qu'il n'en est pas disposé autrement, notamment par le code des transports. Les dispositions contestées peuvent être regardées comme portant pour l'essentiel sur l'organisation interne des entités du groupe public ferroviaire et comme ayant pour objet la détermination des conditions de travail et les garanties sociales de ses salariés. Elles sont, à ce titre, susceptibles de relever de la compétence du juge judiciaire.<br/>
<br/>
              8. Dans ces conditions, le litige présente à juger une question de compétence soulevant une difficulté sérieuse et de nature à justifier le recours à la procédure prévue par l'article 35 du décret du 27 février 2015. Par suite, il y a lieu de renvoyer au Tribunal des conflits la question de la compétence pour connaître de l'action introduite par la Fédération des syndicats des travailleurs du rail - SUD Rail et de surseoir à statuer jusqu'à la décision de ce tribunal.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : L'affaire est renvoyée au Tribunal des conflits.<br/>
Article 2 : Il est sursis à statuer sur la requête de la Fédération des syndicats des travailleurs du rail - SUD Rail jusqu'à ce que le Tribunal des conflits ait tranché la question de la compétence pour connaître de la demande de cette fédération tendant à l'annulation de la décision implicite de rejet de sa demande du 13 mars 2020 d'abroger les dispositions du b) du § 2 des " dispositions diverses " de l'instruction RH00677 du 16 mars 2017 portant dispositions complémentaires à l'accord d'entreprise sur l'organisation du temps de travail du 14 juin 2016, relatives à la répercussion des absences sur l'octroi des repos hebdomadaires, périodiques et complémentaires ou supplémentaires.<br/>
Article 3 : La présente décision sera notifiée à la Fédération des syndicats des travailleurs du rail - SUD Rail et à la société nationale SNCF.<br/>
Copie en sera adressée à la ministre de la transition écologique et à la ministre du travail, de l'emploi et de l'insertion. <br/>
              Délibéré à l'issue de la séance du 15 novembre 2021 où siégeaient : M. Jacques-Henri Stahl, président adjoint de la Section du Contentieux, présidant ; Mme A... M..., Mme D... L..., présidentes de chambre ; M. B... K..., Mme C... F..., Mme H... J..., M. I... G..., M. Damien Botteghi, conseillers d'Etat et Mme Cécile Chaduteau-Monplaisir, maître des requêtes-rapporteure. <br/>
<br/>
<br/>
Rendu le 15 décembre 2021.<br/>
<br/>
<br/>
<br/>
                 Le président : <br/>
                 Signé : M. Jacques-Henri Stahl<br/>
 		La rapporteure : <br/>
      Signé : Mme Cécile Chaduteau-Monplaisir<br/>
                 La secrétaire :<br/>
                 Signé : Mme N... E...<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
La République mande et ordonne à la ministre du travail, de l'emploi et de l'insertion en ce qui la concerne ou à tous huissiers de justice à ce requis en ce qui concerne les voies de droit commun contre les parties privées, de pourvoir à l'exécution de la présente décision.<br/>
			Pour expédition conforme,<br/>
			Pour le secrétaire du contentieux, par délégation :<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
