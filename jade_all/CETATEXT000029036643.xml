<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029036643</ID>
<ANCIEN_ID>JG_L_2014_06_000000373687</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/03/66/CETATEXT000029036643.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère SSJS, 02/06/2014, 373687, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-06-02</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>373687</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère SSJS</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BOULLOCHE ; DELAMARRE</AVOCATS>
<RAPPORTEUR>M. Rémi Decout-Paolini</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Alexandre Lallet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2014:373687.20140602</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>VU LA PROCEDURE SUIVANTE :<br/>
<br/>
              Procédure contentieuse antérieure<br/>
<br/>
              M. B...C...a demandé au juge des référés du tribunal administratif de Bordeaux, sur le fondement de l'article L. 521-1 du code de justice administrative, de suspendre l'exécution de l'arrêté du 1er juillet 2013 par lequel le maire de Bordeaux (Gironde) a décidé de préempter l'ensemble immobilier cadastré AD 20 situé 85, quai de Brazza.<br/>
<br/>
              Par une ordonnance n° 1303842 du 19 novembre 2013, le juge des référés du tribunal administratif de Bordeaux a rejeté sa demande.<br/>
<br/>
              Procédure devant le Conseil d'Etat<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 4 et 17 décembre 2013 au secrétariat du contentieux du Conseil d'Etat, M. C...demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cette ordonnance n° 1303842 du juge des référés du tribunal administratif de Bordeaux du 19 novembre 2013 ;<br/>
<br/>
              2°) statuant en référé, de faire droit à sa demande de suspension ;<br/>
<br/>
              3°) de mettre à la charge de la ville de Bordeaux la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Vu :<br/>
              - les autres pièces du dossier ;<br/>
              - le code de l'urbanisme ;<br/>
              - le code de justice administrative.<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Rémi Decout-Paolini, Maître des Requêtes,  <br/>
<br/>
              - les conclusions de M. Alexandre Lallet, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Delamarre, avocat de M. B...C...,  et à la SCP Boulloche, avocat de la commune de Bordeaux.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>CONSIDERANT CE QUI SUIT :<br/>
<br/>
              Sur le bien-fondé de l'ordonnance attaqué :<br/>
<br/>
              1. Aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision ".<br/>
<br/>
              2. Eu égard à l'objet d'une décision de préemption et à ses effets vis-à-vis de l'acquéreur évincé, la condition d'urgence doit en principe être constatée lorsque celui-ci demande la suspension d'une telle décision. Il peut toutefois en aller autrement au cas où le titulaire du droit de préemption justifie de circonstances particulières, tenant par exemple à l'intérêt s'attachant à la réalisation rapide du projet qui a donné lieu à l'exercice du droit de préemption. Il appartient au juge des référés de procéder à une appréciation globale de l'ensemble des circonstances de l'affaire qui lui est soumise.<br/>
<br/>
              3. Pour juger que la condition d'urgence, invoquée par M. C...à l'appui de sa demande de suspension de l'arrêté du 1er juillet 2013 par lequel le maire de Bordeaux a préempté l'ensemble immobilier appartenant à la société La Cornubia situé 85, quai de Brazza, n'était en l'espèce pas remplie, le juge des référés du tribunal administratif de Bordeaux a estimé que " l'aménagement de l'immeuble préempté, quand bien même il ne serait pas réalisé rapidement " et " la poursuite sans obstacle du projet Brazza dans le souhaitable respect de son équilibre " constituaient des circonstances particulières justifiant que l'arrêté litigieux ne soit pas suspendu dans l'attente que le tribunal ait statué sur la requête en annulation. En statuant ainsi, après s'être borné à mentionner la volonté de la ville de Bordeaux de mener à bien dans de brefs délais son projet d'aménagement et sans énoncer aucune circonstance particulière, tel l'intérêt s'attachant objectivement à la réalisation rapide du projet, susceptible de faire obstacle à ce que la condition d'urgence soit regardée comme remplie au profit de M.C..., acquéreur évincé, le juge des référés a commis une erreur de droit. <br/>
<br/>
              4. Par suite, et sans qu'il soit besoin d'examiner l'autre moyen de son pourvoi, M. C...est fondé à demander l'annulation de l'ordonnance qu'il attaque.<br/>
<br/>
              5. Il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au titre de la procédure de référé engagée, en application des dispositions de l'article L. 821-2 du code de justice administrative.<br/>
<br/>
              Sur l'urgence :<br/>
<br/>
              6. Il résulte de ce qui a été dit ci-dessus que M. C...bénéficie, en sa qualité d'acquéreur évincé, d'une présomption d'urgence. Si la ville de Bordeaux invoque l'importance de la parcelle préemptée pour le programme d'aménagement du quartier de Brazza et l'intérêt du projet urbain poursuivi, elle ne justifie pas ainsi de circonstances particulières qui justifieraient que, sans attendre le jugement de la requête en annulation par le tribunal administratif de Bordeaux, elle poursuive l'exécution de la décision de préemption et qui feraient ainsi obstacle à ce que la condition d'urgence soit regardée comme remplie à l'égard de M.C....<br/>
<br/>
              Sur l'existence d'un doute sérieux quant à la légalité de l'arrêté :<br/>
<br/>
              7. Le moyen invoqué par M. C...et tiré de l'incompétence du signataire de l'arrêté du 1er juillet 2013, faute de disposer d'une délégation de signature incluant le droit de préemption urbain, paraît, au vu des pièces versées au dossier en l'état de l'instruction, propre à créer un doute sérieux quant à la légalité de la décision en litige.  <br/>
<br/>
              8. En revanche, pour l'application de l'article L. 600-4-1 du code de l'urbanisme, les autres moyens invoqués par M. C...et tirés de l'illégalité et de l'absence de caractère exécutoire de la délégation par la communauté urbaine de Bordeaux de son droit de préemption à la ville, de l'illégalité et de l'absence de caractère exécutoire des délibérations de la communauté urbaine de Bordeaux des 21 juillet 2006 et 23 novembre 2012 et des délibérations du conseil municipal de Bordeaux des 21 mars 2008 et 22 octobre 2012, de l'irrégularité de la consultation du service des domaines, de la motivation insuffisante de la décision de préemption, de la méconnaissance du délai de préemption, de l'absence de justification d'un projet précis ainsi que de l'absence d'intérêt général de l'opération envisagée ne sont pas, en l'état de l'instruction, propres à créer un doute sérieux quant à la légalité de l'arrêté litigieux. <br/>
<br/>
              Sur la portée de la suspension prononcée et sur les conclusions aux fins d'injonction :<br/>
<br/>
              9. Il résulte de ce qui précède que M. C...est fondé à demander la suspension de l'arrêté du 1er juillet 2013 préemptant, aux prix et conditions indiqués par la déclaration d'intention d'aliéner, l'ensemble immobilier cadastré AD 20 situé 85, quai de Brazza, en tant qu'il permet à la ville de Bordeaux de disposer de l'ensemble acquis et peut la conduire à user de ce bien dans des conditions qui rendraient irréversible cette décision de préemption. Cette décision de suspension ne fait toutefois pas obstacle à ce que la ville prenne les mesures conservatoires qui s'avéreraient nécessaires.<br/>
<br/>
              10. La suspension prononcée n'appelle, dans l'attente du jugement de la requête aux fins d'annulation de l'arrêté litigieux, pas d'autres obligations pour la ville que celle de s'abstenir d'exercer les prérogatives s'attachant au droit de propriété, de sorte que la décision de préemption ne soit pas rendue plus difficilement réversible à la date à laquelle il sera statué sur le litige au fond. Par suite, il n'y a pas lieu de faire droit aux conclusions à fin d'injonction et d'astreinte présentées par M.C....<br/>
<br/>
              Sur les conclusions présentées au titre des dispositions des articles L. 761-1 et R. 761-1 du code de justice administrative :<br/>
<br/>
              11. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de M.C..., qui n'est pas, dans la présente instance, la partie perdante. Il y a lieu en revanche, dans les circonstances de l'espèce, de mettre à la charge de la ville de Bordeaux, sur le fondement des mêmes dispositions ainsi que de celles de l'article R. 761-1 du même code, le versement à M. C...d'une somme globale de 4 000 euros au titre des frais non compris dans les dépens qu'il a exposés tant en première instance qu'en cassation et de la contribution pour l'aide juridique acquittée en première instance.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'ordonnance du juge des référés du tribunal administratif de Bordeaux du 19 novembre 2013 est annulée.<br/>
Article 2 : L'exécution de la décision du 1er juillet 2013 par laquelle le maire de Bordeaux a fait usage du droit de préemption urbain sur un ensemble immobilier situé 85, quai de Brazza est suspendue en tant que cette décision permet à la ville de disposer de l'ensemble ainsi acquis et peut la conduire à en user dans des conditions qui rendraient irréversible cette décision.<br/>
Article 3 : La ville de Bordeaux versera une somme de 4 000 euros à M. C...au titre des articles L. 761-1 et R. 761-1 du code de justice administrative.<br/>
Article 4 : Le surplus des conclusions de M. C...est rejeté.<br/>
Article 5 : Les conclusions présentées par la ville de Bordeaux au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 6 : La présente décision sera notifiée à M. B...C...et à la ville de Bordeaux.<br/>
Copie en sera adressée pour information à Me D...A..., en qualité de liquidateur judiciaire de la société Cornubia.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
