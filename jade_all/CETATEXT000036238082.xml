<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036238082</ID>
<ANCIEN_ID>JG_L_2015_06_000000371490</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/23/80/CETATEXT000036238082.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère - 6ème SSR, 01/06/2015, 371490</TITRE>
<DATE_DEC>2015-06-01</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>371490</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère - 6ème SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP PIWNICA, MOLINIE ; SCP MASSE-DESSEN, THOUVENIN, COUDRAY</AVOCATS>
<RAPPORTEUR>Mme Marie Grosset</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Rémi Decout-Paolini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:XX:2015:371490.20150601</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
Procédure contentieuse antérieure<br/>
<br/>
              Mme A...C...a demandé à la commission départementale d'aide sociale des Pyrénées-Atlantiques, d'une part, d'annuler la décision du président du conseil général de ce département du 26 juillet 2011 lui réclamant la somme de 10 684,40 euros à titre d'indu de prestation de compensation du handicap et, d'autre part, d'enjoindre au département des Pyrénées-Atlantiques de lui reverser la somme de 2 000,26 euros qu'elle estimait avoir remboursée à tort. <br/>
<br/>
              Par décision du 26 janvier 2012, la commission départementale a annulé la décision du président du conseil général en tant qu'elle mettait à la charge de Mme C... un indu de 10 684,40 euros et a exonéré celle-ci de l'obligation de rembourser cette somme.<br/>
<br/>
              Par une décision n° 120455 du 24 mai 2013, la Commission centrale d'aide sociale, à la demande du département des Pyrénées-Atlantiques, a annulé la décision de la commission départementale d'aide sociale des Pyrénées-Atlantiques et rejeté la demande présentée à cette commission par MmeC....<br/>
<br/>
Procédure devant le Conseil d'Etat<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 21 août 2013, 21 novembre 2013 et 20 juin 2014 au secrétariat de la section du contentieux du Conseil d'Etat, Mme B...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette décision n° 120455 de la Commission centrale d'aide sociale du 15 mars 2013 ;<br/>
<br/>
              2°) de rejeter l'appel du département des Pyrénées-Atlantiques et de condamner celui-ci à lui verser une somme de 2 000,26 euros, assortie des intérêts légaux capitalisés ;<br/>
<br/>
              3°) de mettre à la charge du département des Pyrénées-Atlantiques la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de  l'action sociale et des familles ; <br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Marie Grosset, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Rémi Decout-Paolini, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Masse-Dessen, Thouvenin, Coudray, avocat de Mme B...et à la SCP Piwnica, Molinié, avocat du département des Pyrénées-Atlantiques ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il résulte des articles  L. 245-1 et L. 245-2 du code de l'action sociale et des familles que les personnes handicapées remplissant certaines conditions ont " droit à une prestation de compensation qui a le caractère d'une prestation en nature qui peut être versée, selon le choix du bénéficiaire, en nature ou en espèces " ; que le montant de cette prestation de compensation, servie par le département où le demandeur a son domicile de secours, est fixé après une évaluation des besoins de compensation du demandeur et l'établissement d'un plan personnalisé de compensation réalisés par une équipe pluridisciplinaire ; qu'aux termes des articles D. 245-57 et D. 245-58 du même code : " Le président du conseil général organise le contrôle de l'utilisation de la prestation à la compensation des charges pour lesquelles elle a été attribuée au bénéficiaire " et " peut à tout moment procéder ou faire procéder à un contrôle sur place ou sur pièces en vue de vérifier (...) si le bénéficiaire de cette prestation a consacré cette prestation à la compensation des charges pour lesquelles elle lui a été attribuée " ; <br/>
<br/>
              2. Considérant qu'aux termes de l'article L. 245-5 du même code : " Le service de la prestation de compensation peut être suspendu ou interrompu lorsqu'il est établi, au regard du plan personnalisé de compensation et dans des conditions fixées par décret, que son bénéficiaire n'a pas consacré cette prestation à la compensation des charges pour lesquelles elle lui a été attribuée. Il appartient, le cas échéant, au débiteur de la prestation d'intenter une action en recouvrement des sommes indûment utilisées " ; que l'article R. 245-72 du même code dispose que : " Tout paiement indu est récupéré en priorité par retenue sur les versements ultérieurs de la prestation de compensation. A défaut, le recouvrement de cet indu est poursuivi comme en matière de contributions directes (...) " ; <br/>
<br/>
              3. Considérant que la circonstance que le versement de la prestation de compensation fasse l'objet de retenues, en application de l'article R. 245-72 du code de l'action sociale et des familles, pour recouvrer un indu au titre d'une période antérieure, ne dispense pas le bénéficiaire de la prestation de son obligation de l'utiliser à la compensation des charges pour lesquelles elle lui a été attribuée ; que, par suite, la Commission centrale d'aide sociale n'a pas commis d'erreur de droit en jugeant qu'en l'absence d'une telle utilisation, le département était fondé à constater un paiement indu ;   <br/>
<br/>
              4. Mais considérant qu'il appartient au juge de l'aide sociale, eu égard tant à la finalité de son intervention qu'à sa qualité de juge de plein contentieux, non seulement d'apprécier la légalité de la décision ordonnant la récupération d'un indu mais de se prononcer lui-même sur la décision rejetant explicitement ou implicitement la demande du bénéficiaire de la prestation tendant à la remise ou à la modération, à titre gracieux, de la somme ainsi mise à sa charge, en recherchant si, au regard de l'ensemble des circonstances de fait dont il est justifié par l'une et l'autre partie à la date de sa propre décision, la situation de précarité de l'intéressé et sa bonne foi justifient une telle mesure ; <br/>
<br/>
              5. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que MmeC..., dont la demande de remise gracieuse avait été rejetée par le département, a non seulement contesté la légalité de la décision de récupération prise à son encontre mais a également demandé devant la commission départementale d'aide sociale à être dispensée de restituer le solde de l'indu et fait valoir tant sa bonne foi, compte tenu de l'insuffisante information dont elle avait bénéficié, du caractère itératif de l'indu réclamé et des remboursements déjà effectués, que son faible niveau de ressources et l'impossibilité pour sa famille de lui accorder une aide plus élevée que celle déjà consentie ; qu'ainsi, par des moyens dont la Commission centrale d'aide sociale était saisie par l'effet dévolutif de l'appel, Mme B... avait contesté le rejet par le département des Pyrénées-Atlantiques de sa demande de remise de l'indu réclamé ; qu'en jugeant que seule l'autorité administrative compétente aurait pu faire usage de ses pouvoirs de remise ou de modération de l'indu et qu'elle-même ne pouvait que rejeter la demande de MmeC..., alors qu'elle avait la faculté, en fonction des circonstances particulières de l'espèce, de lui accorder une remise de l'indu ou d'en réduire le montant, la Commission centrale d'aide sociale a méconnu l'étendue de ses pouvoirs ; <br/>
<br/>
              6. Considérant qu'il résulte de ce qui précède, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, que Mme B...est fondée à demander l'annulation de la décision de la Commission centrale d'aide  sociale du 24 mai 2013 ;<br/>
<br/>
              7. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge du département des Pyrénées-Atlantiques une somme de 3 000 euros au titre de l'article  L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La décision de la Commission centrale d'aide sociale du 24 mai 2013 est annulée.<br/>
Article 2 : L'affaire est renvoyée à la Commission centrale d'aide sociale.<br/>
Article 3 : Le département des Pyrénées-Atlantiques versera à Mme B...une somme de 3 000 euros au titre de l'article L.761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à Mme A...B...et au département des Pyrénées-Atlantiques.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">04-04 AIDE SOCIALE. CONTENTIEUX DE L'AIDE SOCIALE ET DE LA TARIFICATION. - INDU DE PRESTATIONS D'AIDE SOCIALE - COMPÉTENCE DU JUGE DE L'AIDE SOCIALE POUR SE PRONONCER SUR LA DÉCISION REJETANT EN TOUT OU PARTIE UNE DEMANDE DE REMISE EN MATIÈRE DE RÉPÉTITION DE L'INDU, QUEL QUE SOIT LE TYPE D'AIDE SOCIALE EN CAUSE - APPLICATION AU CAS DE LA PRESTATION DE COMPENSATION DU HANDICAP [RJ1].
</SCT>
<ANA ID="9A"> 04-04 Il appartient au juge de l'aide sociale, eu égard tant à la finalité de son intervention qu'à sa qualité de juge de plein contentieux, non seulement d'apprécier la légalité de la décision ordonnant la récupération d'un indu mais de se prononcer lui-même sur la décision rejetant explicitement ou implicitement la demande du bénéficiaire de la prestation tendant à la remise ou à la modération de la somme ainsi mise à sa charge, en recherchant si, au regard de l'ensemble des circonstances de fait dont il est justifié par l'une et l'autre partie à la date de sa propre décision, la situation de précarité de l'intéressé et sa bonne foi justifient une telle mesure. Ces principes valent pour la récupération, décidée par un département, de montants indument versés au titre de la prestation de compensation du handicap.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr. en matière de récupération sur donation ou succession CE, 25 novembre 1998, Département du Nord, n° 181242, p. 439 ; CE, Section, 25 avril 2001, Garofalo, n° 314252, p. 193. Cf., en matière de répétition du revenu minimum d'insertion, CE, 27 mars 2000, Mme Wéry, n° 200591, T. pp. 837-1155, et, pour le revenu de solidarité active, CE, avis, 23 mai 2011, Popin et El Moumny, n° 344970, p. 253.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
