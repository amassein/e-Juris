<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043209056</ID>
<ANCIEN_ID>JG_L_2021_03_000000441194</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/20/90/CETATEXT000043209056.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème chambre, 02/03/2021, 441194, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-03-02</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>441194</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LYON-CAEN, THIRIEZ ; SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR>Mme Sophie-Caroline de Margerie</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Guillaume Odinet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:441194.20210302</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu les procédures suivantes :<br/>
<br/>
              La SAS TDF a demandé au juge des référés du tribunal administratif de Limoges, sur le fondement de l'article L. 521-1 du code de justice administrative de suspendre l'exécution de l'arrêté en date du 9 janvier 2020 par lequel le maire de Saint-Priest-sous-Aixe s'est opposé à sa déclaration préalable en vue de l'installation d'une station de radiotéléphonie sur un terrain cadastré section AW n° 39, ainsi que de la décision du 7 mars 2020 rejetant son recours gracieux.<br/>
<br/>
              Par une ordonnance n° 2000630 du 28 mai 2020, le juge des référés du tribunal administratif de Limoges a fait droit à sa demande et a enjoint à la commune de Saint-Priest-sous-Aixe de prendre une décision de non-opposition dans un délai d'un mois.<br/>
<br/>
              1° Sous le n° 441194, par un pourvoi sommaire et un mémoire complémentaire enregistrés les 15 juin et 1er juillet 2020 au secrétariat du contentieux du Conseil d'Etat, la commune de Saint-Priest-sous-Aixe demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance ; <br/>
<br/>
              2°) statuant en référé, de rejeter la demande de suspension présentée par la SAS TDF ;<br/>
<br/>
              3°) de mettre à la charge de la SAS TDF une somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              2° Sous le n° 441807, par une requête, enregistrée le 13 juillet 2020 au secrétariat du contentieux du Conseil d'Etat, la commune de Saint-Priest-sous-Aixe demande au Conseil d'Etat, en application de l'article R. 821-5 du code de justice administrative, de prononcer le sursis à exécution de l'ordonnance du 28 mai 2020 du juge des référés du tribunal administratif de Limoges.<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la Constitution ;<br/>
              - le code de l'urbanisme ;<br/>
              - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020.<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme B... A..., conseillère d'Etat, <br/>
<br/>
              - les conclusions de M. Guillaume Odinet, rapporteur public,<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Lyon-Caen, Thiriez, avocat de la commune de Saint-Priest-sous-Aixe ; <br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Le pourvoi et la requête à fin de sursis à exécution présentés par la commune Saint-Priest-sous-Aixe sont dirigés contre la même ordonnance du juge des référés du tribunal administratif de Limoges. Il y a lieu de les joindre pour statuer par une seule décision.<br/>
<br/>
              Sur le pourvoi dirigé contre l'ordonnance attaquée :<br/>
<br/>
              2. Aux termes de l'article L. 822-1 du code de justice administrative : " Le pourvoi en cassation devant le Conseil d'Etat fait l'objet d'une procédure préalable d'admission. L'admission est refusée par décision juridictionnelle si le pourvoi est irrecevable ou n'est fondé sur aucun moyen sérieux ".<br/>
              3. Pour demander l'annulation de l'ordonnance qu'elle attaque, la commune de Saint-Priest-sous-Aixe soutient que le juge des référés du tribunal administratif de Limoges a :<br/>
              - commis une erreur de droit et dénaturé les pièces du dossier en ne prenant pas en compte, dans son appréciation globale de la condition d'urgence, le manque de diligence de la SAS TDF ;<br/>
              - entaché son ordonnance d'une erreur de droit et de dénaturation en se bornant à constater de manière abstraite la caractérisation d'une situation d'urgence sans rechercher quelle était la situation effective du réseau sur le territoire de la commune ni si des délais et contraintes pesaient effectivement sur la société TDF ;<br/>
              - dénaturé les pièces du dossier en estimant que le moyen tiré de l'erreur manifeste d'appréciation dans l'application des dispositions de l'article R. 111-27 du code de l'urbanisme était de nature à faire naître un doute sérieux quant à la légalité de la décision attaquée ;<br/>
              - commis une erreur de droit et dénaturé les pièces du dossier en regardant comme de nature à créer un doute sérieux le moyen tiré de l'application à la déclaration préalable présentée par la société TDF du principe de précaution au sens de l'article 5 de la Charte de l'environnement ;<br/>
              - commis une erreur de droit et dénaturé les pièces du dossier en ne relevant pas que l'implantation de l'antenne relais ne pouvait se faire que dans le cadre d'une demande de permis de construire, et non d'une simple déclaration préalable ;<br/>
              - commis une erreur de droit en lui enjoignant de prendre une décision de non-opposition alors, d'une part, que celle-ci ne peut, par nature, être provisoire en ce qu'elle emporte nécessairement des effets difficilement réversibles et que, d'autre part, des motifs y faisaient obstacle. <br/>
<br/>
              4. Aucun de ces moyens n'est de nature à permettre l'admission du pourvoi.<br/>
<br/>
              Sur la requête à fin de sursis à exécution :<br/>
<br/>
              5. Il résulte de ce qui précède que le pourvoi de la commune de <br/>
Saint-Priest-sous-Aixe contre l'ordonnance attaquée n'est pas admis. Par suite, ses conclusions à fin de sursis à l'exécution de cette ordonnance présentées sur le fondement de l'article R. 821-5 du code de justice administrative sont devenues sans objet.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de la commune de Saint-Priest-sous-Aixe n'est pas admis.<br/>
Article 2 : Il n'y a pas lieu de statuer sur les conclusions de la requête de la commune de <br/>
Saint-Priest-sous-Aixe à fin de sursis à exécution.<br/>
Article 3 : La présente décision sera notifiée à la commune de Saint-Priest-sous-Aixe.<br/>
Copie en sera adressée à la SAS TDF.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
