<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000035818905</ID>
<ANCIEN_ID>JG_L_2017_10_000000395268</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/35/81/89/CETATEXT000035818905.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème - 2ème chambres réunies, 06/10/2017, 395268, Publié au recueil Lebon</TITRE>
<DATE_DEC>2017-10-06</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>395268</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème - 2ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GARREAU, BAUER-VIOLAS, FESCHOTTE-DESBOIS ; SCP LYON-CAEN, THIRIEZ</AVOCATS>
<RAPPORTEUR>M. Marc Pichon de Vendeuil</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Olivier Henrard</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:395268.20171006</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société CEGELEC Sud-ouest a demandé au tribunal administratif de Montpellier de condamner le centre hospitalier de Narbonne à lui verser la somme de 150 397,21 euros HT assortie des intérêts légaux et de leur capitalisation, en réparation des préjudices résultant de l'annulation par le juge du référé contractuel du marché qui lui avait été attribué pour l'exécution du lot n° 8 " CVC - plomberie - paillasses humides " du marché de construction d'un centre de gérontologie. Par un jugement n° 1203291 du 10 décembre 2013, le tribunal administratif de Montpellier a partiellement fait droit à sa demande en condamnant le centre hospitalier de Narbonne à lui verser la somme de 132 616 euros assortie des intérêts légaux à compter du 26 juillet 2012, ces intérêts étant eux-mêmes capitalisés.<br/>
<br/>
              Par un arrêt n° 14MA00603 du 12 octobre 2015, la cour administrative d'appel de Marseille a, sur appel du centre hospitalier de Narbonne, ramené à 12 470 euros HT la somme que le centre hospitalier avait été condamné à verser à la société Cegelec Sud-ouest et réformé le jugement du 10 décembre 2013 en ce qu'il avait de contraire à son arrêt et, enfin, rejeté le surplus des conclusions des parties. <br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 14 décembre 2015, 15 mars 2016 et 27 février 2017 au secrétariat du contentieux du Conseil d'Etat, la société CEGELEC Perpignan, venant aux droits de la société CEGELEC Sud-ouest, demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à ses conclusions d'appel ;<br/>
<br/>
              3°) de mettre à la charge du centre hospitalier de Narbonne la somme de 6 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code des marchés publics ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Marc Pichon de Vendeuil, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Olivier Henrard, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Lyon-Caen, Thiriez, avocat de la société Cegelec Perpignan et à la SCP Garreau, Bauer-Violas, Feschotte-Desbois, avocat du centre hospitalier de Narbonne.  <br/>
<br/>
              Vu la note en délibéré, enregistrée le 21 septembre 2017, présentée par la société CEGELEC Perpignan.<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond qu'en 2011, le centre hospitalier de Narbonne a lancé une procédure d'appel d'offres ouvert en vue de la construction d'un centre de gérontologie ; qu'à l'issue de la consultation, le lot n° 8 de ce marché " CVC - plomberies - paillasses humides " a été attribué à la société CEGELEC Sud-ouest, aux droits de laquelle est depuis venue la société CEGELEC Perpignan ; que ce marché, d'un montant de 2 849 735,72 euros HT, a été notifié le 16 juin 2011 ; que, toutefois, à la demande de la société Spie Sud-ouest, concurrent évincé, le juge du référé contractuel du tribunal administratif de Montpellier, après avoir constaté que la signature du contrat litigieux avant l'expiration du délai exigé après l'envoi de la décision d'attribution aux opérateurs économiques ayant présenté une candidature ou une offre avait privé la société Spie Sud-ouest de son droit de former utilement un référé précontractuel et que plusieurs irrégularités affectant les critères de sélection des offres constituaient des manquements aux règles de publicité et de mise en concurrence ayant affecté les chances de cette société d'obtenir le marché litigieux, a prononcé l'annulation de ce marché, par une ordonnance du 7 juillet 2011 devenue définitive ; que le centre hospitalier de Narbonne a lancé un nouvel appel d'offres auquel la société CEGELEC Sud-ouest s'est de nouveau portée candidate, mais au terme duquel elle n'a pas été retenue ; que la société CEGELEC Sud-ouest a demandé au centre hospitalier de l'indemniser des préjudices qu'elle estimait avoir subis en raison de l'annulation par le juge du référé contractuel du marché dont elle était titulaire ; que, saisi par la société, le tribunal administratif de Montpellier a, par un jugement du 10 décembre 2013, condamné le centre hospitalier de Narbonne à lui verser la somme de 132 616 euros, assortie des intérêts légaux et de la capitalisation, au titre du manque à gagner auquel elle pouvait prétendre ; que, sur appel du centre hospitalier de Narbonne, la cour administrative d'appel de Marseille a, par un arrêt du 12 octobre 2015, contre lequel la société CEGELEC Perpignan se pourvoit en cassation, ramené à 12 470 euros la condamnation prononcée et réformé le jugement en ce qu'il avait de contraire ;<br/>
<br/>
              2. Considérant que l'entrepreneur dont le contrat est écarté peut prétendre, y compris en cas d'annulation du contrat par le juge du référé contractuel, sur un terrain quasi-contractuel, au remboursement de celles de ses dépenses qui ont été utiles à la collectivité envers laquelle il s'était engagé ; que les fautes éventuellement commises par l'intéressé antérieurement à la signature du contrat sont sans incidence sur son droit à indemnisation au titre de l'enrichissement sans cause de la collectivité, sauf si le contrat a été obtenu dans des conditions de nature à vicier le consentement de l'administration, ce qui fait obstacle à l'exercice d'une telle action ; que dans le cas où le contrat est écarté en raison d'une faute de l'administration, l'entrepreneur peut en outre, sous réserve du partage de responsabilités découlant le cas échéant de ses propres fautes, prétendre à la réparation du dommage imputable à la faute de l'administration ; qu'à ce titre il peut demander le paiement des sommes correspondant aux autres dépenses exposées par lui pour l'exécution du contrat et aux gains dont il a été effectivement privé du fait de sa non-application, notamment du bénéfice auquel il pouvait prétendre, si toutefois l'indemnité à laquelle il a droit sur un terrain quasi-contractuel ne lui assure pas déjà une rémunération supérieure à celle que l'exécution du contrat lui aurait procurée ; que, saisi d'une demande d'indemnité sur ce fondement, il appartient au juge d'apprécier si le préjudice allégué présente un caractère certain et s'il existe un lien de causalité direct entre la faute de l'administration et le préjudice ;<br/>
<br/>
              3. Considérant qu'il ressort des énonciations de l'arrêt attaqué que, après avoir relevé que le juge du référé contractuel avait prononcé l'annulation du contrat signé entre le centre hospitalier de Narbonne et la société CEGELEC Sud-ouest en raison de plusieurs manquements aux règles de publicité et de mise en concurrence relatifs aux critères de sélection des offres, qui avaient affecté les chances de la société Spie Sud-ouest d'obtenir le marché litigieux, la cour administrative d'appel de Marseille a, pour rejeter la demande d'indemnisation de la société CEGELEC Sud-ouest, retenu qu'elle ne pouvait " se prévaloir d'aucun droit à la conclusion du contrat " ; qu'elle a ainsi entendu juger que les manquements aux règles de passation commis par le pouvoir adjudicateur avaient eu une incidence déterminante sur l'attribution du marché à la société CEGELEC Sud-ouest et que, dès lors, eu égard aux motifs retenus en l'espèce par le juge du référé contractuel, le lien entre la faute de l'administration et le manque à gagner dont la société entendait obtenir la réparation ne pouvait être regardé comme direct ; que la cour n'a entaché son arrêt sur ce point d'aucune erreur de droit ni d'aucune erreur de qualification juridique ; que, par suite, la société CEGELEC Perpignan n'est pas fondée à demander l'annulation de l'arrêt qu'elle attaque ;<br/>
<br/>
              4. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la société CEGELEC Perpignan la somme de 3 000 euros à verser au centre hospitalier de Narbonne, au titre des dispositions de l'article L. 761-1 du code de justice administrative ; qu'en revanche, les mêmes dispositions font obstacle à ce qu'une somme soit mise à ce titre à la charge du centre hospitalier de Narbonne qui n'est pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de la société CEGELEC Perpignan est rejeté.<br/>
Article 2 : La société CEGELEC Perpignan versera au centre hospitalier de Narbonne une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée à la société CEGELEC Perpignan et au centre hospitalier de Narbonne.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">39-04-01 MARCHÉS ET CONTRATS ADMINISTRATIFS. FIN DES CONTRATS. NULLITÉ. - DROIT À INDEMNITÉ DU COCONTRACTANT EN CAS D'ANNULATION DU CONTRAT - 1) ACTION EN RESPONSABILITÉ QUASI-CONTRACTUELLE - PRINCIPES [RJ1] - 2) COMBINAISON DES ACTIONS EN RESPONSABILITÉ QUASI-CONTRACTUELLE ET QUASI-DÉLICTUELLE - A) PRINCIPES [RJ1] - B) OFFICE DU JUGE - APPRÉCIATION DU CARACTÈRE CERTAIN DU PRÉJUDICE ET DE L'EXISTENCE D'UN LIEN DE CAUSALITÉ DIRECT ENTRE LA FAUTE DE L'ADMINISTRATION ET LE PRÉJUDICE [RJ2] - C) ESPÈCE - MANQUEMENTS AUX RÈGLES DE PUBLICITÉ ET DE MISE EN CONCURRENCE AYANT EU UNE INCIDENCE DÉTERMINANTE SUR L'ATTRIBUTION DU MARCHÉ AU COCONTRACTANT - LIEN DIRECT - ABSENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">39-05-01-02-02 MARCHÉS ET CONTRATS ADMINISTRATIFS. EXÉCUTION FINANCIÈRE DU CONTRAT. RÉMUNÉRATION DU CO-CONTRACTANT. INDEMNITÉS. DROIT À INDEMNITÉ DES CONCESSIONNAIRES. - DROIT À INDEMNITÉ DU COCONTRACTANT EN CAS D'ANNULATION DU CONTRAT - 1) ACTION EN RESPONSABILITÉ QUASI-CONTRACTUELLE - PRINCIPES [RJ1] - 2) COMBINAISON DES ACTIONS EN RESPONSABILITÉ QUASI-CONTRACTUELLE ET QUASI-DÉLICTUELLE - A) PRINCIPES [RJ1] - B) OFFICE DU JUGE - APPRÉCIATION DU CARACTÈRE CERTAIN DU PRÉJUDICE ET DE L'EXISTENCE D'UN LIEN DE CAUSALITÉ DIRECT ENTRE LA FAUTE DE L'ADMINISTRATION ET LE PRÉJUDICE [RJ2] - C) ESPÈCE - MANQUEMENTS AUX RÈGLES DE PUBLICITÉ ET DE MISE EN CONCURRENCE AYANT EU UNE INCIDENCE DÉTERMINANTE SUR L'ATTRIBUTION DU MARCHÉ AU COCONTRACTANT - LIEN DIRECT - ABSENCE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">60-01-02-02 RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. FAITS SUSCEPTIBLES OU NON D'OUVRIR UNE ACTION EN RESPONSABILITÉ. FONDEMENT DE LA RESPONSABILITÉ. RESPONSABILITÉ POUR FAUTE. - DROIT À INDEMNITÉ DU COCONTRACTANT EN CAS D'ANNULATION DU CONTRAT - COMBINAISON DES ACTIONS EN RESPONSABILITÉ QUASI-CONTRACTUELLE ET QUASI-DÉLICTUELLE - 1) PRINCIPES [RJ1] - 2) OFFICE DU JUGE - APPRÉCIATION DU CARACTÈRE CERTAIN DU PRÉJUDICE ET DE L'EXISTENCE D'UN LIEN DE CAUSALITÉ DIRECT ENTRE LA FAUTE DE L'ADMINISTRATION ET LE PRÉJUDICE [RJ2] - 3) ESPÈCE - MANQUEMENTS AUX RÈGLES DE PUBLICITÉ ET DE MISE EN CONCURRENCE AYANT EU UNE INCIDENCE DÉTERMINANTE SUR L'ATTRIBUTION DU MARCHÉ AU COCONTRACTANT - LIEN DIRECT - ABSENCE.
</SCT>
<ANA ID="9A"> 39-04-01 1) L'entrepreneur dont le contrat est écarté peut prétendre, y compris en cas d'annulation du contrat par le juge du référé contractuel, sur un terrain quasi-contractuel, au remboursement de celles de ses dépenses qui ont été utiles à la collectivité envers laquelle il s'était engagé. Les fautes éventuellement commises par l'intéressé antérieurement à la signature du contrat sont sans incidence sur son droit à indemnisation au titre de l'enrichissement sans cause de la collectivité, sauf si le contrat a été obtenu dans des conditions de nature à vicier le consentement de l'administration, ce qui fait obstacle à l'exercice d'une telle action.,,2) a) Dans le cas où le contrat est écarté en raison d'une faute de l'administration, il peut en outre, sous réserve du partage de responsabilité découlant le cas échéant de ses propres fautes, prétendre à la réparation du dommage imputable à la faute de l'administration. A ce titre il peut demander le paiement des sommes correspondant aux autres dépenses exposées par lui pour l'exécution du contrat et aux gains dont il a été effectivement privé du fait de sa non-application, notamment du bénéfice auquel il pouvait prétendre, si toutefois l'indemnité à laquelle il a droit sur un terrain quasi-contractuel ne lui assure pas déjà une rémunération supérieure à celle que l'exécution du contrat lui aurait procurée.... ...b) Saisi d'une demande d'indemnité sur ce fondement, il appartient au juge d'apprécier si le préjudice allégué présente un caractère certain et s'il existe un lien de causalité direct entre la faute de l'administration et le préjudice.... ...c) En l'espèce, les manquements aux règles de publicité et de mise en concurrence commis par le pouvoir adjudicateur ayant eu une incidence déterminante sur l'attribution du marché au titulaire, le lien entre la faute de l'administration et le manque à gagner dont la société entend obtenir la réparation ne peut être regardé comme direct.</ANA>
<ANA ID="9B"> 39-05-01-02-02 1) L'entrepreneur dont le contrat est écarté peut prétendre, y compris en cas d'annulation du contrat par le juge du référé contractuel, sur un terrain quasi-contractuel, au remboursement de celles de ses dépenses qui ont été utiles à la collectivité envers laquelle il s'était engagé. Les fautes éventuellement commises par l'intéressé antérieurement à la signature du contrat sont sans incidence sur son droit à indemnisation au titre de l'enrichissement sans cause de la collectivité, sauf si le contrat a été obtenu dans des conditions de nature à vicier le consentement de l'administration, ce qui fait obstacle à l'exercice d'une telle action.,,2) a) Dans le cas où le contrat est écarté en raison d'une faute de l'administration, il peut en outre, sous réserve du partage de responsabilité découlant le cas échéant de ses propres fautes, prétendre à la réparation du dommage imputable à la faute de l'administration. A ce titre il peut demander le paiement des sommes correspondant aux autres dépenses exposées par lui pour l'exécution du contrat et aux gains dont il a été effectivement privé du fait de sa non-application, notamment du bénéfice auquel il pouvait prétendre, si toutefois l'indemnité à laquelle il a droit sur un terrain quasi-contractuel ne lui assure pas déjà une rémunération supérieure à celle que l'exécution du contrat lui aurait procurée.... ...b) Saisi d'une demande d'indemnité sur ce fondement, il appartient au juge d'apprécier si le préjudice allégué présente un caractère certain et s'il existe un lien de causalité direct entre la faute de l'administration et le préjudice.... ...c) En l'espèce, les manquements aux règles de publicité et de mise en concurrence commis par le pouvoir adjudicateur ayant eu une incidence déterminante sur l'attribution du marché au titulaire, le lien entre la faute de l'administration et le manque à gagner dont la société entend obtenir la réparation ne peut être regardé comme direct.</ANA>
<ANA ID="9C"> 60-01-02-02 1) Dans le cas où le contrat est écarté en raison d'une faute de l'administration, le cocontractant de l'administration peut, en sus d'une action sur un terrain quasi-contractuel, sous réserve du partage de responsabilité découlant le cas échéant de ses propres fautes, prétendre à la réparation du dommage imputable à la faute de l'administration. A ce titre il peut demander le paiement des sommes correspondant aux autres dépenses exposées par lui pour l'exécution du contrat et aux gains dont il a été effectivement privé du fait de sa non-application, notamment du bénéfice auquel il pouvait prétendre, si toutefois l'indemnité à laquelle il a droit sur un terrain quasi-contractuel ne lui assure pas déjà une rémunération supérieure à celle que l'exécution du contrat lui aurait procurée.... ,,2) Saisi d'une demande d'indemnité sur ce fondement, il appartient au juge d'apprécier si le préjudice allégué présente un caractère certain et s'il existe un lien de causalité direct entre la faute de l'administration et le préjudice.... ,,3) En l'espèce, les manquements aux règles de publicité et de mise en concurrence commis par le pouvoir adjudicateur ayant eu une incidence déterminante sur l'attribution du marché au titulaire, le lien entre la faute de l'administration et le manque à gagner dont la société entend obtenir la réparation ne peut être regardé comme direct.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, Section, 10 avril 2008, Decaux et département des Alpes-Maritimes, n°s 244950 284439 248607, p. 151.,,[RJ2] Cf., en précisant, CE, Section, 10 avril 2008, Decaux et département des Alpes-Maritimes, n°s 244950 284439 248607, p. 151.,,[RJ3] Rappr., s'agissant d'un recours au fond, CE, Section, 10 avril 2008, Decaux et département des Alpes-Maritimes, n°s 244950 284439 248607, p. 151.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
