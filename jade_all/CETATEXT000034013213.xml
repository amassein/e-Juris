<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034013213</ID>
<ANCIEN_ID>JG_L_2017_02_000000393015</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/01/32/CETATEXT000034013213.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème - 4ème chambres réunies, 07/02/2017, 393015, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-02-07</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>393015</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème - 4ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Alain Seban</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Laurence Marion</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:393015.20170207</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu les procédures suivantes :<br/>
<br/>
              1° Sous le n° 393015, par une requête sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés au secrétariat du contentieux du Conseil d'Etat les 27 août et 25 novembre 2015 et le 11 août 2016, la société Aftershokz LLC et la société SCPM demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le 3° de l'article 1er du décret n° 2015-743 du 24 juin 2015 relatif à la lutte contre l'insécurité routière ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat une somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              2° Sous le n° 393169, par une requête, enregistrée au secrétariat du contentieux du Conseil d'Etat le 26 août 2015, Mme A... B...demande au Conseil d'Etat d'annuler pour excès de pouvoir le 3° de l'article 1er du décret n° 2015-743 du 24 juin 2015 relatif à la lutte contre l'insécurité routière.<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu :<br/>
<br/>
              - le code de la route ;<br/>
<br/>
              - l'arrêté du 4 juillet 2008 portant répartition des affaires entre les sections administratives du Conseil d'Etat ;<br/>
<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Alain Seban, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Laurence Marion, rapporteur public.<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant que, par le 3° de son article 1er, le décret du 24 juin 2015 relatif à la lutte contre l'insécurité routière a inséré à l'article R. 412-6-1 du code de la route, après le premier alinéa, deux alinéas ainsi rédigés : "  Est également interdit le port à l'oreille, par le conducteur d'un véhicule en circulation, de tout dispositif susceptible d'émettre du son, à l'exception des appareils électroniques correcteurs de surdité./ Les dispositions du deuxième alinéa ne sont pas applicables aux conducteurs des véhicules d'intérêt général prioritaire prévus à l'article R. 311-1, ni dans le cadre de l'enseignement de la conduite des cyclomoteurs, motocyclettes, tricycles et quadricycles à moteur ou de l'examen du permis de conduire ces véhicules " ; que les requêtes des sociétés Aftershokz et SCPM et de Mme B...tendent l'une et l'autre à l'annulation pour excès de pouvoir de ces dispositions ; qu'il y a lieu de les joindre pour statuer par une seule décision ;<br/>
<br/>
              Sur la légalité externe : <br/>
<br/>
              2. Considérant qu'aux termes de l'article 3 de l'arrêté du 4 juillet 2008 portant répartition des affaires entre les sections administratives du Conseil d'Etat : " Sont examinées par la section des travaux publics les affaires relatives :/ (...) - aux transports (...) " ; qu'il suit de là que le décret attaqué a pu à bon droit être soumis à l'examen de la section des travaux publics ;<br/>
<br/>
              Sur la légalité interne :<br/>
<br/>
              3. Considérant, en premier lieu, que le principe de légalité des délits et l'objectif de valeur constitutionnelle de clarté et d'intelligibilité de la norme de droit impliquent que, pour les sanctions dont le régime relève de sa compétence, le pouvoir réglementaire définisse les infractions en des termes suffisamment clairs et précis pour exclure l'arbitraire ; qu'en interdisant, sous peine de l'amende prévue pour les contraventions de la quatrième classe, " le port à l'oreille, par le conducteur d'un véhicule en circulation, de tout dispositif susceptible d'émettre du son, à l'exception des appareils électroniques correcteurs de surdité ", le Premier ministre a satisfait à cette exigence ; qu'en particulier, ces dispositions impliquent clairement qu'un dispositif émettant du son porté à l'oreille est interdit, alors même qu'il n'obstrue pas le conduit auditif mais assure la transmission du son par conduction osseuse à travers la paroi crânienne ; <br/>
<br/>
              4. Considérant, en second lieu, qu'il ressort des pièces du dossier que les dispositifs émettant du son portés à l'oreille ont en commun, quelle que soit la technique utilisée, de distraire l'attention du conducteur tout en l'isolant, dans une certaine mesure, de son environnement sonore ; que, compte tenu de l'impact avéré de ces dispositifs sur la sécurité des usagers de la route, le décret attaqué a, en les prohibant, édicté une interdiction nécessaire et proportionnée aux buts poursuivis ; que la circonstance, à la supposer établie, que les systèmes qui ne sont pas portés à l'oreille et diffusent le son dans l'habitacle du véhicule, dont l'utilisation n'est pas interdite, présenteraient des risques comparables, est sans incidence sur la légalité de l'interdiction contestée ; que, par suite, le moyen pris de la violation du principe d'égalité devant la loi est inopérant ;<br/>
<br/>
              5. Considérant qu'il résulte de tout ce qui précède, sans qu'il soit besoin d'examiner la fin de non recevoir opposée par le ministre de l'intérieur à la requête de MmeB..., que les sociétés Aftershokz LLC et SCPM et Mme B...ne sont pas fondées à demander l'annulation du décret qu'elles attaquent ;<br/>
<br/>
              6. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat, qui n'est pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Les requêtes des sociétés Aftershokz LLC et SCPM et de Mme B... sont rejetées.<br/>
<br/>
Article 2 : La présente décision sera notifiée à la société Aftershokz LLC, à la société SCPM, à Mme A...B..., au Premier ministre et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
