<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026025640</ID>
<ANCIEN_ID>JG_L_2012_06_000000343530</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/02/56/CETATEXT000026025640.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème / 4ème SSR, 15/06/2012, 343530</TITRE>
<DATE_DEC>2012-06-15</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>343530</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème / 4ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Marie Gautier-Melleray</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Sophie-Justine Lieber</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2012:343530.20120615</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la requête sommaire et le mémoire complémentaire, enregistrés les 27 septembre 2010 et le 24 décembre 2010 au secrétariat du contentieux du Conseil d'Etat, présentés pour la SOCIETE SA VORTEX, dont le siège est 37 bis rue Greneta à Paris (75002) ; la société demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir les décisions du 23 mars 2010 par lesquelles le Conseil supérieur de l'audiovisuel (CSA) a rejeté sa candidature en vue de l'exploitation du service de radio Skyrock par voie hertzienne terrestre dans les zones de Lodève, Font-Romeu et Prades, dans le ressort du comité technique radiophonique de Toulouse ;<br/>
<br/>
              2°) d'enjoindre au CSA de lui délivrer les autorisations demandées, sous astreinte de 500 euros par jour de retard à compter de la notification de la décision à intervenir ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
<br/>
              Vu la loi n° 86-1067 du 30 septembre 2006 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Marie Gautier-Melleray, Maître des Requêtes en service extraordinaire,  <br/>
<br/>
<br/>
<br/>
              - les conclusions de Mme Sophie-Justine Lieber, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Sur le moyen relatif aux conditions d'examen des candidatures : <br/>
<br/>
              Considérant qu'afin d'être en mesure d'apprécier l'intérêt respectif des différents projets de service de radio par voie hertzienne terrestre qui lui sont présentés dans une zone, le Conseil supérieur de l'audiovisuel (CSA) est tenu de statuer sur l'ensemble des candidatures dont il est saisi pour cette zone et de décider de leur acceptation ou de leur rejet au cours d'une même séance ; que la circonstance qu'une décision de rejet soit notifiée au-delà du délai d'un mois après la publication au Journal officiel des autorisations accordées pour cette zone, prévu à l'article 32 de la loi du 30 septembre 1986, est sans incidence sur la légalité de cette décision ; que, dès lors, la circonstance que les décisions de rejet des candidatures de la SOCIETE SA VORTEX à l'exploitation d'un service de radio par voie hertzienne terrestre dénommé Skyrock et relevant de la catégorie D dans les zones de Lodève, Font-Romeu et Prades, prises par le CSA lors de sa séance du 23 mars 2010, lui ont été notifiées par une lettre du 26 juillet 2010 alors que les décisions d'autorisation prises lors de la même séance, et que cette société n'a pas contestées, avaient été publiées au Journal officiel du 23 avril 2010, est sans incidence sur leur légalité ;<br/>
<br/>
              Sur le moyen tiré d'une violation des dispositions de l'article 41 de la loi du 30 septembre 1986 :<br/>
<br/>
              Considérant qu'aux termes de cet article : " Une même personne physique ou morale ne peut, sur le fondement d'autorisations relatives à l'usage de fréquences dont elle est titulaire pour la diffusion d'un ou de plusieurs services de radio par voie hertzienne terrestre en mode analogique, ou par le moyen d'un programme qu'elle fournit à d'autres titulaires d'autorisation par voie hertzienne terrestre en mode analogique, disposer en droit ou en fait de plusieurs réseaux que dans la mesure où la somme des populations recensées dans les zones desservies par ces différents réseaux n'excède pas 150 millions d'habitants " ; qu'il résulte du 2° de l'article 41-3 que, pour l'application de cet article  " toute personne physique ou morale qui contrôle, au regard des critères figurant à l'article L. 233-3 du code de commerce, une société titulaire d'autorisation ou a placé celle-ci sous son autorité ou sa dépendance est regardée comme titulaire d'une autorisation (...) " ; <br/>
<br/>
              Considérant que la SA VORTEX soutient que le seuil de 150 millions d'habitants prévu par ces dispositions législatives étant dépassé tant pour le groupe NRJ que pour le groupe Lagardère Active Broadcast, le CSA ne pouvait légalement, dans le cadre de l'appel aux candidatures litigieux, délivrer des autorisations en vue de l'exploitation de services contrôlés par ces groupes ; qu'elle excipe ainsi de l'illégalité de ces autorisations à l'appui de son recours contre les refus qui lui ont été opposés ;<br/>
<br/>
              Considérant que, lorsqu'un refus d'autorisation d'exploiter un service audiovisuel est fondé sur une comparaison entre l'intérêt du projet écarté et celui des projets retenus, et non sur un motif étranger à toute comparaison, tel que l'irrecevabilité de la candidature, le candidat concerné peut, à l'appui de son recours contre ce refus, invoquer utilement l'illégalité d'une autorisation délivrée dans la même zone dans le cadre du même appel aux candidatures ; qu'une telle exception d'illégalité n'est toutefois recevable que si, à la date à laquelle elle est invoquée, l'autorisation concernée n'est pas devenue définitive ;<br/>
<br/>
              Considérant que le moyen invoqué par la SA VORTEX n'est pas opérant à l'encontre de la décision rejetant la candidature du service Skyrock pour la zone de Font-Romeu, dès lors qu'il ressort des pièces du dossier que, dans cette zone, aucune autorisation n'a été accordée, à l'issue de l'appel à candidatures, à un service contrôlé par les groupes NRJ et Lagardère Active Broadcast ;<br/>
<br/>
              Considérant que, si le même moyen est opérant à l'encontre des décisions prises pour les zones de Prades et Lodève dans lesquelles une autorisation a été accordée à un service du groupe Lagardère Active Broadcast pour la première et du groupe NRJ pour la seconde, les autorisations accordées pour ces zones ont été publiées au Journal officiel du 23 avril 2010 ; que, ces décisions étant déjà devenues définitives à la date du 24 décembre 2010 à laquelle la société requérante a invoqué le moyen tiré de ce que ces autorisations auraient été délivrées en méconnaissance des dispositions de l'article 41 de la loi du 30 septembre 1986, cette exception d'illégalité est tardive et, par suite, irrecevable ;<br/>
<br/>
              Sur le moyen tiré d'une violation des dispositions de l'article 29 de la loi du 30 septembre 1986 :<br/>
<br/>
              Considérant qu'aux termes de cet article, le conseil supérieur " accorde les autorisations en appréciant l'intérêt de chaque projet pour le public, au regard des impératifs prioritaires que sont la sauvegarde du pluralisme des courants d'expression socioculturels, la diversification des opérateurs, et la nécessité d'éviter les abus de position dominante ainsi que les pratiques entravant le libre exercice de la concurrence. / Il tient également compte : (...) 5° de la contribution à la production de programmes réalisés localement. / (...) Le Conseil supérieur de l'audiovisuel veille, sur l'ensemble du territoire, à ce qu'une part suffisante des ressources en fréquences soit attribuée aux services édités par une association et accomplissant une mission sociale de proximité (...) / Le Conseil veille également au juste équilibre entre les réseaux nationaux de radiodiffusion, d'une part, et les services locaux, régionaux et thématiques indépendants, d'autre part. / Il s'assure que le public bénéficie de services dont les programmes contribuent à l'information politique et générale " ;<br/>
<br/>
              Considérant que, par deux communiqués n° 34 du 29 août 1989 et n° 281 du 10 novembre 1994, le CSA, faisant usage des pouvoirs qu'il tient de ces dispositions, a déterminé cinq catégories de services en vue de l'appel à candidatures pour l'exploitation de services de radiodiffusion sonore par voie hertzienne terrestre ; que ces cinq catégories sont ainsi définies : services associatifs éligibles au fonds de soutien, mentionnés à l'article 80 (catégorie A), services locaux ou régionaux indépendants ne diffusant pas de programme national identifié (B), services locaux ou régionaux diffusant le programme d'un réseau thématique à vocation nationale (C), services thématiques à vocation nationale (D), et services généralistes à vocation nationale (E) ;<br/>
<br/>
              Considérant qu'il ne ressort pas des pièces du dossier qu'en statuant sur l'ensemble des candidatures au cours d'une même séance, le conseil supérieur ait omis d'examiner l'intérêt particulier de chaque projet ; <br/>
<br/>
              En ce qui concerne la zone de Font-Romeu :<br/>
<br/>
              Considérant que dans cette zone, où émettaient un service en catégorie B, un service en catégorie C, deux services en catégorie D, Fun Radio et NRJ et un service en catégorie E, le CSA a attribué la seule fréquence disponible à Radio Montaillou, radio sociale de proximité relevant de la catégorie A, au motif qu'il s'agissait d'un format inédit dans la zone et a écarté la candidature de Skyrock en catégorie D, au motif que le jeune public qu'il vise bénéficiait déjà des programmes de Fun Radio et NRJ ; que ce motif n'est entaché ni d'erreur de droit ni d'erreur d'appréciation au regard des critères énoncés à l'article 29 de la loi du 30 septembre 1986 ;<br/>
<br/>
              En ce qui concerne la zone de Lodève :<br/>
<br/>
              Considérant que dans cette zone, où émettaient Radio Lodève, Radio Pays d'Hérault et RDF Maguelone en catégorie A, RTS FM et Totem en catégorie B, Fun Radio Méditerranée et RFM Méditerranée en catégorie C, Nostalgie et Virgin Radio en catégorie D et RTL en catégorie E, deux fréquences étaient disponibles ; que le CSA a retenu Rire et Chansons en catégorie D, au motif qu'il s'agissait d'un format inédit dans la zone, et RMC en catégorie E, au motif que ses programmes contribuaient à l'information politique et générale ; qu'il a écarté la candidature de Skyrock en catégorie D, au motif qu'il s'agissait d'un format musical déjà représenté dans la zone puisque destiné au jeune public qui bénéficiait déjà des programmes de Fun Radio Méditerranée et RTS FM ; que ce motif n'est entaché ni d'erreur de droit ni d'erreur d'appréciation au regard des critères énoncés à l'article 29 de la loi du 30 septembre 1986 ; que la circonstance que l'un des services retenus appartiendrait à un groupe titulaire dans le ressort du comité technique radiophonique de Toulouse, après les résultats de l'appel à candidatures, d'un nombre de fréquences supérieur à celui du groupe auquel appartient le service Skyrock n'est pas, à elle seule, de nature à établir que le conseil aurait méconnu l'impératif de diversification des opérateurs ;<br/>
<br/>
              En ce qui concerne la zone de Prades :<br/>
<br/>
              Considérant que dans cette zone, où émettaient Radio Arrels en catégorie A, Radio Catalogne Nord et Sud Radio en catégorie B, Chérie FM Pyrénées en catégorie C, Fun Radio, Nostalgie et Virgin Radio en catégorie D et RMC en catégorie E, deux fréquences étaient disponibles ; que le CSA a retenu Radio Montaillou en catégorie A, au motif que ce service présente un programme visant à lutter contre l'isolement et la désertification des villages situés dans les hautes vallées du massif pyrénéen et RFM Perpignan en catégorie C ; qu'il a écarté Skyrock, au motif qu'il s'agit d'une radio musicale à destination d'un public jeune qui bénéficie déjà des programmes de Fun radio ; que ce motif n'est entaché ni d'erreur de droit ni d'erreur d'appréciation au regard des critères énoncés à l'article 29 de la loi du 30 septembre 1986 ; que la circonstance que le service RFM Perpignan appartiendrait à un groupe titulaire dans le ressort du comité technique radiophonique de Toulouse, après les résultats de l'appel à candidatures, d'un nombre de fréquences supérieur à celui du groupe auquel appartient le service Skyrock n'est pas, à elle seule, de nature à établir que le conseil aurait méconnu l'impératif de diversification des opérateurs ;<br/>
<br/>
              Sur le moyen tiré d'une violation des stipulations de l'article 10 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales :<br/>
<br/>
              Considérant que les décisions attaquées, qui résultent d'une exacte application des critères posés par la loi du 30 septembre 1986, ne méconnaissent pas les stipulations de l'article 10 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales qui garantit à toute personne le droit à la liberté d'expression et celui de recevoir et de communiquer des informations ou des idées ;<br/>
<br/>
              Considérant qu'il résulte de ce qui précède que la SOCIETE SA VORTEX n'est pas fondée à demander l'annulation des décisions du 23 mars 2010 par lesquelles le Conseil supérieur de l'audiovisuel lui a refusé l'autorisation d'exploiter le service Skyrock dans les zones de Font-Romeu, Lodève et Prades ; que doivent être rejetées, par voie de conséquence, ses conclusions à fin d'injonction ainsi que ses conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
		Article 1er : La requête de la SOCIETE SA VORTEX est rejetée.<br/>
<br/>
Article 2 : La présente décision sera notifiée à la SOCIETE SA VORTEX et au Conseil supérieur de l'audiovisuel. <br/>
Copie pour information sera adressée à la ministre de la culture et de la communication.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-07-01-04-04 PROCÉDURE. POUVOIRS ET DEVOIRS DU JUGE. QUESTIONS GÉNÉRALES. MOYENS. EXCEPTION D'ILLÉGALITÉ. - DÉLIVRANCE D'AUTORISATION PAR LE CSA APRÈS APPEL À CANDIDATURES - CONTESTATION, PAR LA VOIE DE L'EXCEPTION, DE LA LÉGALITÉ DE L'AUTORISATION ACCORDÉE [RJ1] - CONDITIONS - OPÉRANCE - REFUS FONDÉ SUR UNE COMPARAISON DE L'INTÉRÊT RESPECTIF DES PROJETS - RECEVABILITÉ - ABSENCE DE CARACTÈRE DÉFINITIF DE L'AUTORISATION CONCERNÉE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">56-02 RADIO ET TÉLÉVISION. RÈGLES GÉNÉRALES. - DÉLIVRANCE D'AUTORISATION PAR LE CSA APRÈS APPEL À CANDIDATURES - CONTESTATION, PAR LA VOIE DE L'EXCEPTION, DE LA LÉGALITÉ DE L'AUTORISATION ACCORDÉE [RJ1] - CONDITIONS - OPÉRANCE - REFUS FONDÉ SUR UNE COMPARAISON DE L'INTÉRÊT RESPECTIF DES PROJETS - RECEVABILITÉ - ABSENCE DE CARACTÈRE DÉFINITIF DE L'AUTORISATION CONCERNÉE.
</SCT>
<ANA ID="9A"> 54-07-01-04-04 Lorsqu'une décision du Conseil supérieur de l'audiovisuel (CSA) de refus d'autorisation d'exploiter un service audiovisuel est fondée sur une comparaison entre l'intérêt du projet écarté et celui des projets retenus, et non sur un motif étranger à toute comparaison, tel que l'irrecevabilité de la candidature, le candidat concerné peut, à l'appui de son recours contre ce refus, invoquer utilement l'illégalité d'une autorisation délivrée dans la même zone dans le cadre du même appel à candidatures. Une telle exception d'illégalité n'est toutefois recevable que si, à la date à laquelle elle est invoquée, l'autorisation concernée n'est pas devenue définitive.</ANA>
<ANA ID="9B"> 56-02 Lorsqu'une décision du Conseil supérieur de l'audiovisuel (CSA) de refus d'autorisation d'exploiter un service audiovisuel est fondée sur une comparaison entre l'intérêt du projet écarté et celui des projets retenus, et non sur un motif étranger à toute comparaison, tel que l'irrecevabilité de la candidature, le candidat concerné peut, à l'appui de son recours contre ce refus, invoquer utilement l'illégalité d'une autorisation délivrée dans la même zone dans le cadre du même appel à candidatures. Une telle exception d'illégalité n'est toutefois recevable que si, à la date à laquelle elle est invoquée, l'autorisation concernée n'est pas devenue définitive.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. décision du même jour, Association régionale d'animation (radio Totem), n° 351892, à mentionner aux Tables.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
