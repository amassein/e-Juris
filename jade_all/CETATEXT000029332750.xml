<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029332750</ID>
<ANCIEN_ID>JG_L_2014_07_000000381302</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/33/27/CETATEXT000029332750.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 24/07/2014, 381302, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-07-24</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>381302</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DELVOLVE ; SCP GARREAU, BAUER-VIOLAS, FESCHOTTE-DESBOIS</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2014:381302.20140724</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 16 juin 2014 au secrétariat du contentieux du Conseil d'Etat, présentée par la société S.A.S.P Stade Toulousain Rugby, dont le siège est 114, rue de Troenes à Toulouse (31200) représentée par son président ; la société requérante demande au juge des référés du Conseil d'Etat :<br/>
<br/>
              1°) d'ordonner, sur le fondement de l'article L. 521-1 du code de justice administrative, la suspension de l'exécution de l'article 1 et du b) de l'article 3.1.3. de l'annexe I de la convention conclue entre la Ligue nationale de rugby et la Fédération française de rugby, et la décision de la Fédération intégrant ces stipulations au règlement de la Fédération de rugby ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat, de la Ligue nationale de rugby et de la Fédération française de rugby la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              elle soutient que :<br/>
              - la condition d'urgence est remplie, dès lors, en premier lieu, que la convention contestée entrera en vigueur dès la première journée de la saison 2014/2015 du TOP 14, soit le week-end des 16 et 17 août 2014, en deuxième lieu, que les six joueurs concernés par les dispositions attaquées, qui représente près de 20 % de l'effectif total de l'équipe du club du Stade Toulousain, ne seront en mesure, dans l'hypothèse la plus favorable, de ne participer qu'à dix-neuf matches avec le club, en troisième lieu, que ces dispositions préjudicient de manière grave et immédiate aux intérêts du club du Stade Toulousain dès lors qu'elles auront pour effet de porter atteinte aux résultats sportifs du club, et préjudicieront ainsi de manière grave et immédiate aux intérêts économiques de la SASP Stade Toulousain, à court terme par le manque à gagner en terme de vente de billets, de " sponsoring " et de " marchandising " et, à long terme, par le déficit d'image dont aura à souffrir le club par ses moins bons résultats sportifs ;<br/>
              - la convention litigieuse méconnaît l'article R. 132-17 du code du sport et l'article 4 de la loi n° 2000-231 du 12 avril 2000 relative aux droits des citoyens dans leurs relations avec l'administration ; <br/>
              - l'article I de l'annexe I de la convention contestée méconnaît le principe de non rétroactivité des actes administratifs, le principe d'égalité, le principe de libre accès aux compétitions sportives, le principe d'équité des compétitions et le droit de la concurrence ; <br/>
              - il méconnaît les articles 45 et 56 du traité sur le fonctionnement de l'Union européenne ; <br/>
              - le b) de l'article 3.1.3 de l'annexe I de la convention attaquée méconnaît les dispositions du règlement de l'International Rugby Board relatives à la mise à disposition des joueurs ;<br/>
              - la Fédération était incompétente pour édicter de telles règles ;<br/>
<br/>
<br/>
<br/>
              	Vu la copie de la requête aux fins d'annulation de cette convention, présentée par la société  S.A.S.P Stade Toulousain Rugby ; <br/>
<br/>
              Vu le mémoire en défense, enregistré le 1er juillet 2014, présenté pour la Ligue nationale de Rugby, qui conclut au rejet de la requête et à ce que soit mise à la charge de la requérante la somme de 6 000 euros au titre de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
              elle soutient que : <br/>
              - les conclusions dirigées contre les dispositions du b) de l'article 3.1.3 de l'annexe 1 à la convention sont irrecevables dès lors qu'elles étaient dépourvues d'objet lors de l'introduction de la requête en référé le 16 juin 2014 ;<br/>
              - la condition d'urgence n'est pas remplie, dès lors, en premier lieu, que la requérante n'a formé sa demande de suspension que le 16 juin 2014 alors qu'elle avait introduit un recours en annulation le 21 février 2014, alors même qu'aucune circonstance nouvelle n'est intervenue entre temps, en deuxième lieu, que la convention contestée ne porte aucune atteinte grave à l'accès du Stade Toulousain aux compétitions, aux résultats sportifs de la requérante ou à ses résultats financiers ;<br/>
              - aucun des moyens soulevés n'est de nature à créer un doute sérieux sur la légalité de la décision contestée ; <br/>
              - la Fédération était bien compétente pour édicter de telles règles ;<br/>
<br/>
              Vu le mémoire en défense, enregistré le 2 juillet 2014, présenté pour la Fédération française de rugby, qui conclut au rejet de la requête et à ce que soit mise à la charge de la requérante la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
              elle soutient que : <br/>
              - la juridiction administrative est incompétente pour connaître du litige dès lors que la convention attaquée ne contient aucune disposition impérative à caractère général ; <br/>
              - la requête est irrecevable dès lors que la convention attaquée ne fait pas grief ; <br/>
              - les conclusions dirigées contre les dispositions du b) de l'article 3.1.3 de l'annexe 1 à la convention sont irrecevables dès lors qu'elles étaient dépourvues d'objet lors de l'introduction de la requête en référé le 16 juin 2014 ;<br/>
              - la condition d'urgence n'est pas remplie, dès lors, en premier lieu, que la requérante n'a formé sa demande de suspension que le 16 juin 2014 alors qu'elle avait introduit un recours en annulation le 21 février 2014, alors même qu'aucune circonstance nouvelle n'est intervenue entre temps, en deuxième lieu, que la convention contestée ne porte aucune atteinte grave à l'accès du Stade Toulousain aux compétitions, aux résultats sportifs de la requérante ou à ses résultats financiers ;<br/>
              - aucun des moyens soulevés n'est de nature à créer un doute sérieux sur la légalité de la décision contestée ; <br/>
<br/>
              Vu les observations, enregistrées le 3 juillet 2014, présentées par la ministre du droit des femmes, de la ville, de la jeunesse et des sports ;<br/>
<br/>
              Vu le mémoire en réplique, enregistré le 4 juillet 2014, présenté par la société S.A.S.P Stade Toulousain Rugby qui conclut aux mêmes fins par les mêmes moyens ; <br/>
<br/>
              elle soutient en outre que :<br/>
              - en premier lieu, la juridiction administrative est bien compétente, l'acte litigieux émanant d'une fédération sportive délégataire, conclu pour le compte de l'autorité délégante et pris dans le cadre d'une mission de service public constituant l'exercice de prérogative de puissance publique ; en deuxième lieu, que leur recours, introduit à l'encontre d'une décision qui est un acte réglementaire faisant grief et non un simple acte préparatoire, est bien recevable ; <br/>
              - la condition d'urgence est remplie dès lors, d'une part, que l'immédiateté du préjudice s'apprécie au regard de la date d'entrée en vigueur des dispositions querellées et, d'autre part, que la prétendue absence de diligence dans l'introduction de sa requête ne saurait lui être reprochée au regard des incertitudes existant sur les modalités d'entrée en vigueur de cette convention avant l'introduction de sa demande, l'empêchant d'appréhender les effets concrets des mesures qu'elle conteste ;<br/>
              - aucun intérêt supérieur tenant à la compétitivité de l'équipe de France ne s'oppose à ce que l'exécution de la convention soit suspendue dès lors que, en premier lieu, les mesures litigieuses apparaissent pour la première fois dans la réglementation sans qu'aucun élément scientifique, technique ou médical ne vienne les justifier, en deuxième lieu, qu'elle ne saurait être justifiées par l'objectif de protection de la santé des joueurs qui bénéficient d'un accompagnement médical rigoureux, en troisième lieu, qu'elles n'existent pas dans de telles proportions dans les autres pays, ni dans les règlements internationaux, en quatrième lieu, qu'elles porteront une atteinte excessive aux clubs disposant de joueurs français internationaux qui se trouveront fortement pénalisés dans leur participation aux compétitions nationales et internationales ;    <br/>
<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, la société S.A.S.P Stade Toulousain Rugby et, d'autre part, la Fédération française de rugby, la Ligue nationale de rugby et la ministre des droits des femmes, de la ville, de la jeunesse et des sports ;<br/>
<br/>
              Vu le procès-verbal de l'audience publique du 7 juillet 2014 à 9 heures 30 au cours de laquelle ont été entendus : <br/>
<br/>
              - les représentants de la société S.A.S.P Stade Toulousain ;<br/>
<br/>
              - Me Delvolvé, avocat au Conseil d'Etat et à la Cour de cassation, avocat de la Fédération Française de Rugby ;<br/>
<br/>
              - le représentant de la Fédération Française de Rugby ;<br/>
<br/>
              - Me Garreau, avocat au Conseil d'Etat et à la Cour de cassation, avocat de la Ligue nationale de Rugby ;<br/>
<br/>
              - les représentants de la Ligue nationale de Rugby ;<br/>
<br/>
              - les représentants de la ministre des droits des femmes, de la ville, de la jeunesse et des sports ;<br/>
<br/>
              et à l'issue de laquelle l'instruction a été prolongée jusqu'au jeudi 17 juillet 2014 à 16 heures ;<br/>
<br/>
              Vu le mémoire complémentaire, enregistré le 11 juillet 2014, présenté pour la Ligue nationale de Rugby, qui conclut aux mêmes fins, par les mêmes moyens ;<br/>
<br/>
              elle soutient, en outre, que :<br/>
              - le dispositif de limitation litigieux n'est applicable que sur deux des quatre saisons régies par la convention FFR/LNR ;<br/>
              - l'impact réel du dispositif pour 2013-2014 est limité, d'autant qu'il ne peut affecter que huit matches ;<br/>
              - la mise en place d'un Top 12 ne permettrait pas de garantir de la même manière la compétitivité de l'Equipe de France ;  <br/>
<br/>
              Vu le mémoire complémentaire, enregistré le 11 juillet 2014, et le mémoire rectificatif, présentés pour la Fédération Française de Rugby, qui conclut aux mêmes fins, par les mêmes moyens ; elle soutient, en outre, que la Rugby Football Union et la Premier Rugby Limited ont posé une règle similaire à celle qui est contestée ;<br/>
<br/>
              Vu le mémoire complémentaire, enregistré le 16 juillet 2014, présenté par la société S.A.S.P. Stade Toulousain, qui conclut aux mêmes fins, par les mêmes moyens ; elle soutient, en outre, que la mesure litigieuse, qui ne revêt pas un caractère provisoire, aura un impact important sur ses intérêts alors même que les autres nations du rugby n'ont pas mis en place de règles similaires et que des mesures alternatives sont possibles ;<br/>
<br/>
              Vu les autres pièces du dossier ; <br/>
<br/>
              Vu le code du sport ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'aux termes de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision (...) " ;<br/>
<br/>
              2. Considérant que la Ligue nationale de rugby et la Fédération française de rugby ont conclu, le 19 décembre 2013, une convention sur le fondement de l'article R. 132-9 du code du sport, qui a été approuvée par un arrêté du ministre chargé des sports du 24 janvier 2014 ; que la société S.A.S.P Stade Toulousain Rugby demande, sur le fondement de l'article L. 521-1 du code de justice administrative, la suspension de l'exécution de l'article 1er ainsi que du b) de l'article 3.1.3 de l'annexe 1 à cette convention, qui est relative à l'Equipe de France et à l'organisation du calendrier ; que, contrairement à ce qui est soutenu, la juridiction administrative est compétente pour connaître des conclusions dirigées à l'encontre de cette convention qui est susceptible de faire l'objet d'un recours ;<br/>
<br/>
              Sur les conclusions dirigées à l'encontre de l'article 1er de l'annexe 1 à la convention du 19 décembre 2013 :<br/>
<br/>
              3. Considérant que l'article 1er de l'annexe 1 à la convention du 19 décembre 2013 est relatif au " Groupe XV de France " ; qu'il prévoit que la Fédération française de rugby établit chaque saison une liste de trente joueurs susceptibles d'être sélectionnés en Equipe de France et définit des dispositions particulières dans la gestion de leur saison ; qu'à ce titre, il prévoit que chaque joueur du " Groupe XV de France " pour une saison donnée ne pourra disputer, lors de ladite saison, plus de trente matches ; <br/>
<br/>
              4. Considérant que le Stade Toulousain Rugby, dont 6 des joueurs ont été désignés pour être membres du " Groupe XV de France ", par une décision du 28 mai 2014, demande la suspension immédiate de l'exécution de la règle du plafonnement à trente matches par saison pour chaque joueur intégré dans le " Groupe France " ; <br/>
<br/>
              5. Considérant que l'urgence justifie la suspension de l'exécution d'un acte administratif lorsque celle-ci porte atteinte, de manière suffisamment grave et immédiate, à un intérêt public, à la situation du requérant ou aux intérêts qu'il entend défendre ; qu'il appartient au juge des référés d'apprécier concrètement, compte tenu des justifications fournies par le requérant, si les effets de l'acte contesté sont de nature à caractériser une urgence justifiant que, sans attendre le jugement de la requête au fond, l'exécution de la décision soit suspendue ; que l'urgence doit être appréciée objectivement et compte tenu de l'ensemble des circonstances de l'affaire ;<br/>
<br/>
              6. Considérant que le Stade Toulousain Rugby soutient que la règle du plafonnement, qui n'existait pas avant la conclusion de la convention litigieuse et ne connaît pas d'équivalent dans les autres nations du rugby, porte atteinte aux intérêts du club de manière grave et immédiate ; qu'elle s'applique en effet à compter de la saison 2014-2015 qui débute, s'agissant du TOP 14, les 16 et 17 août 2014 ; que le respect de cette règle privera le club, pour un nombre important de matches du TOP 14, de ses meilleurs joueurs et fera obstacle à la mise en place pérenne d'une équipe type ; que les résultats sportifs de la saison passée attestent d'une perte de compétitivité du club lorsque ses joueurs internationaux n'ont pas joué ; que les résultats sportifs du club se trouveront nécessairement affectés par le respect de cette règle, ainsi que par voie de conséquence, ses résultats économiques ; <br/>
<br/>
              7. Considérant toutefois qu'il ressort des pièces du dossier et en particulier des éléments versés aux débats lors de l'audience ainsi que postérieurement à celle-ci que les conditions de mise en oeuvre de la règle du plafonnement à trente matches sont de nature à en atténuer les effets sur le fonctionnement des clubs dont certains des joueurs sont membres du " groupe France "  ; en premier lieu, que ne sont comptabilisés que les matches dans lesquels le temps de jeu effectif d'un joueur est d'au moins 20 minutes ; en deuxième lieu, qu'il est prévu que ne sont pas pris en compte les matches de phases finales du TOP 14 et des Coupes d'Europe ainsi que les matches de barrages qualificatifs aux Coupes d'Europe de la saison suivante ; en troisième lieu, que si les joueurs membres du " Groupe France " sont susceptibles de jouer chaque saison onze matches en équipe Nationale qui s'imputent sur le total des trente, c'est en vertu du principe de mise à disposition des joueurs, posé par le règlement de l'International Rugby Board, qui n'est pas contesté dans le présent litige ; qu'en outre, l'impact théorique du respect de la règle contestée doit être apprécié compte tenu des indisponibilités dites " structurelles " qui font obstacle, indépendamment du respect du total de trente matches par joueur, à la disponibilité pour leur club des joueurs de l'Equipe de France et qui tiennent aux " doublons " découlant des différents calendriers de matches, aux périodes de stages ainsi qu'aux congés ; qu'enfin, la période de référence, retenue pour le calcul du total des matches joués, a été fixée du 1er juin de chaque année jusqu'au 31 mai de l'année suivante afin de permettre aux clubs concernés d'être en mesure, après les " tournées de l'Equipe de France ", d'organiser leur effectif au vu des matches restant à jouer pour leurs joueurs internationaux ; qu'en outre, il ressort des pièces du dossier que l'exécution immédiate de la règle du plafonnement qui poursuit l'objectif de favoriser la compétitivité de l'équipe de France en préservant les joueurs qui en sont membres d'un nombre trop élevé de matches durant la saison, présente un intérêt public ; <br/>
<br/>
              8. Considérant qu'il résulte de tout ce qui précède que la condition d'urgence, requise par l'article L. 521-1 du code de justice administrative pour justifier la suspension immédiate de l'article 1er de la convention litigieuse, n'est pas caractérisée ; que, sans qu'il soit besoin de statuer sur l'existence d'un doute sérieux quant à sa légalité, les conclusions tendant à la suspension de son exécution doivent donc être rejetées ;<br/>
              Sur les conclusions dirigées à l'encontre du b) de l'article 3.1.3 de l'annexe 1 à la convention du 19 décembre 2013 :<br/>
<br/>
              9. Considérant, en premier lieu, que les dispositions de l'article L. 521-1 du code de justice administrative ne permettent de demander la suspension d'une décision administrative qu'à la condition qu'une telle décision soit encore susceptible d'exécution ;<br/>
<br/>
              10. Considérant, en second lieu, que l'article 3.1.3 de l'annexe litigieuse est relatif au Tournoi des 6 Nations 2014 ; que son b) définit les conditions de remise à disposition de leur club des joueurs sélectionnés dans l'équipe nationale pour les 19ème et 21ème journée du TOP 14 qui se sont déroulées respectivement les 14 et 15 février 2014 et 28 février et 1er mars 2014 ; <br/>
<br/>
              11. Considérant qu'il résulte de ce qui précède que le b) de l'article 31.3 avait été entièrement exécuté, avant que la société requérante ne présente sa demande aux fins de suspension de son exécution ; que ses conclusions doivent donc être rejetées comme irrecevables ; <br/>
<br/>
              Sur les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              12. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à la charge de la Ligue nationale de rugby et de la Fédération française de rugby qui ne sont pas, dans la présente instance, les parties perdantes ; qu'il y a lieu, en revanche, de mettre à la charge de la société S.A.S.P Stade Toulousain Rugby, le versement à la Ligue nationale de rugby et à la Fédération française de rugby chacune d'une somme de 2 000 euros ;<br/>
<br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
------------------<br/>
 Article 1er : La requête de la société S.A.S.P Stade Toulousain Rugby est rejetée.<br/>
 Article 2 : La société S.A.S.P Stade Toulousain Rugby versera à la Ligue nationale de rugby et à la Fédération française de rugby chacune une somme de 2 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
 Article 3 : La présente ordonnance sera notifiée à la société S.A.S.P Stade Toulousain Rugby, à la Ligue nationale de rugby et à la Fédération française de rugby et à la ministre du droit des femmes, de la ville, de la jeunesse et des sports.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
