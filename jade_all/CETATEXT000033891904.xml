<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033891904</ID>
<ANCIEN_ID>JG_L_2017_01_000000387833</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/89/19/CETATEXT000033891904.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème - 10ème chambres réunies, 18/01/2017, 387833, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-01-18</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>387833</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème - 10ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LESOURD</AVOCATS>
<RAPPORTEUR>M. Bastien Lignereux</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emilie Bokdam-Tognetti</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:387833.20170118</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société Sucrerie de Toury a demandé au tribunal administratif d'Orléans de condamner l'Etat à lui verser une indemnité de 98 768 euros en réparation du préjudice qu'elle estime avoir subi du fait du retard mis par l'Etat à transposer la directive n° 2003/96/CE du Conseil du 27 octobre 2003 restructurant le cadre communautaire de taxation des produits énergétiques et de l'électricité. Par un jugement n° 1201723 du 31 janvier 2013, le tribunal administratif d'Orléans a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 13NT00966 du 18 décembre 2014, la cour administrative d'appel de Nantes a rejeté l'appel formé par la société Sucrerie de Toury contre ce jugement. <br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 10 février et 7 mai 2015 et 22 février 2016 au secrétariat du contentieux du Conseil d'Etat, la société Sucrerie de Toury demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le traité sur le fonctionnement de l'Union européenne ;<br/>
              - la directive n° 2003/96/CE du Conseil du 27 octobre 2003 ;<br/>
              - le code des douanes ;<br/>
              - la loi n° 2000-108 du 10 février 2000 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Bastien Lignereux, auditeur, <br/>
<br/>
              - les conclusions de Mme Emilie Bokdam-Tognetti, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Lesourd, avocat de la société Sucrerie de Toury ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que la société Sucrerie de Toury exploite une installation de cogénération de chaleur et d'électricité pour laquelle elle utilise du gaz naturel comme combustible. Le gaz qui lui a été livré entre le 1er janvier 2006 et le 25 décembre 2007 a été soumis, par son fournisseur qui en acquitté le montant, à la taxe intérieure de consommation de gaz naturel prévue à l'article 266 quinquies du code des douanes. Estimant que ces livraisons auraient dû être exonérées de cette taxe en application de l'article 14 de la directive du Conseil du 27 octobre 2003 restructurant le cadre communautaire de taxation des produits énergétiques et de l'électricité, elle a demandé au ministre chargé du budget l'indemnisation du préjudice qu'elle estime avoir subi du fait du retard de l'Etat à transposer cette directive. Elle se pourvoit en cassation contre l'arrêt du 18 décembre 2014 par lequel la cour administrative d'appel de Nantes a rejeté son appel contre le jugement du tribunal administratif d'Orléans du 31 janvier 2013 rejetant sa demande tendant à ce que l'Etat soit condamné à lui verser une indemnité en réparation de ce préjudice.<br/>
<br/>
              2. Il ressort des dispositions de l'article 266 quinquies du code des douanes, dans ses rédactions successives applicables à la période en litige, que les livraisons de gaz naturel destiné à être utilisé comme combustible pour la production d'électricité sont exonérées de la taxe intérieure de consommation de gaz naturel. Toutefois, les livraisons de gaz destiné à être utilisé dans des installations de cogénération, pour la production combinée de chaleur et d'électricité, ne bénéficient pas de cette exonération, sauf, à compter du 1er janvier 2007, si les producteurs de l'électricité issue de ces installations, d'une part, ne sont pas titulaires d'un contrat au titre de l'obligation d'achat prévue par la loi du 10 février 2000 relative à la modernisation et au développement du service public de l'électricité, d'autre part, renoncent au bénéfice de l'exonération spécifique prévue à l'article 266 quinquies A du même code. Ce dernier prévoit, pour les seules installations mises en service au plus tard le 31 décembre 2007, une exonération du gaz utilisé pendant une durée limitée à cinq ans à compter de leur mise en service. En application de ces dispositions combinées, le gaz utilisé par l'exposante dans son installation de cogénération n'a pas été exonéré de la taxe intérieure de consommation de gaz naturel.<br/>
<br/>
              3. L'article 14 de la directive du Conseil du 27 octobre 2003 restructurant le cadre communautaire de taxation des produits énergétiques et de l'électricité prévoit, en son paragraphe 1, que " (...) sans préjudice d'autres dispositions communautaires, les États membres exonèrent les produits suivants de la taxation, selon les conditions qu'ils fixent en vue d'assurer l'application correcte et claire de ces exonérations et d'empêcher la fraude, l'évasion ou les abus : / a) les produits énergétiques et l'électricité utilisés pour produire de l'électricité et l'électricité utilisée pour maintenir la capacité de produire de l'électricité. Toutefois, les États membres peuvent taxer ces produits pour des raisons ayant trait à la protection de l'environnement et sans avoir à respecter les niveaux minima de taxation prévus par la présente directive. (...) ". Aux termes de l'article 15 de cette directive : " 1. Sans préjudice d'autres dispositions communautaires, les États membres peuvent appliquer sous contrôle fiscal des exonérations totales ou partielles ou des réductions du niveau de taxation : / (...) c) aux produits énergétiques et à l'électricité utilisés pour la production combinée de chaleur et d'énergie ; (...) ". Le paragraphe 5 de son article 21 prévoit en outre que " Nonobstant les dispositions de l'article 14, paragraphe 1, point a), les États membres peuvent exonérer les petits producteurs d'électricité, pour autant qu'ils taxent les produits énergétiques utilisés pour produire cette électricité. ". Enfin, aux termes de l'article 28 de cette directive : " 1. Les États membres adoptent et publient les dispositions législatives, réglementaires et administratives nécessaires pour se conformer à la présente directive au plus tard le 31 décembre 2003. (...) / 2. Ils appliquent les présentes dispositions à partir du 1er janvier 2004 (...) ".<br/>
<br/>
              4. Devant la cour administrative d'appel, la société soutenait que l'application de la faculté d'exonération du gaz utilisé pour la production combinée de chaleur et d'énergie, ouverte aux Etats membres par l'article 15 de la directive, n'était pas exclusive de l'obligation d'exonérer le gaz utilisé pour produire de l'électricité posée par son article 14. Ainsi, selon l'interprétation de la requérante, d'ailleurs confirmée par un courrier que lui a adressé le 16 mai 2011 la direction générale fiscalité et union douanière de la Commission européenne et qu'elle a produit en appel, l'article 14 de la directive fait obligation aux Etats membres d'exonérer la part du gaz dont la consommation correspond à la production d'électricité, la faculté d'exonération ouverte par l'article 15 ne portant que sur la part correspondant à la génération de chaleur. Par l'arrêt attaqué, la cour a toutefois jugé que le gaz naturel utilisé par la société relevait exclusivement des dispositions de l'article 15 de la directive, et non de son article 14, et que son régime de taxation n'était pas dissociable selon qu'il était destiné à la production de chaleur ou à celle d'électricité.<br/>
<br/>
              5. En premier lieu, en statuant ainsi sans interroger la Cour de justice de l'Union européenne à titre préjudiciel sur l'interprétation à retenir de ces dispositions, la cour n'a pas méconnu son office, dès lors qu'en application de l'article 267 du traité sur le fonctionnement de l'Union européenne, un tel renvoi préjudiciel ne constitue, même en présence d'une difficulté sérieuse, qu'une faculté pour les juridictions dont les décisions sont susceptibles d'un recours juridictionnel de droit interne. En outre, contrairement à ce que soutient le pourvoi, l'absence de mention dans son arrêt du courrier de la Commission européenne mentionné au point 4 ne l'entache pas d'insuffisance de motivation. Enfin, le moyen tiré de ce que la cour aurait commis une erreur de droit en jugeant que les dispositions de l'article 266 quinquies du code des douanes étaient conformes à l'article 15 de la directive du 27 octobre 2003 alors que la faculté d'exonération ouverte par son article 15 ne pouvait, selon la requérante, être utilisée qu'avant l'expiration du délai de transposition de cette directive, n'a pas été soulevé devant les juges du fond et ne peut par suite qu'être écarté.<br/>
<br/>
              6. En second lieu, la société soutient que la cour a commis une erreur de droit en jugeant que le gaz naturel qu'elle utilise relevait exclusivement des dispositions de l'article 15 de la directive. Elle estime, en s'appuyant sur le courrier des services de la Commission européenne mentionné au point 4, que la faculté d'exonération ouverte par cet article s'applique uniquement à la part du gaz dont la consommation correspond à la production de chaleur, la part utilisée pour la production d'électricité devant en revanche être obligatoirement exonérée en application de l'article 14. Néanmoins, l'interprétation retenue par la cour, selon laquelle la règle spéciale prévue à l'article 15 déroge à la règle générale prévue par l'article 14, peut s'appuyer sur la lettre de l'article 15, qui ne réserve pas explicitement l'application de l'article 14 à la part du gaz utilisée pour la production d'électricité. Dans ces conditions, la question se pose de savoir si les produits énergétiques utilisés pour la production combinée de chaleur et d'électricité relèvent exclusivement de la faculté d'exonération ouverte par le c) du 1 de l'article 15 de la directive du Conseil du 27 octobre 2003 ou si, au contraire, cette faculté s'applique sans préjudice, s'agissant de la part du gaz dont la consommation correspond à la production d'électricité, de l'obligation d'exonération prévue par le a) du 1 de son article 14. Cette question, déterminante pour la décision que doit prendre le Conseil d'Etat, présente une difficulté sérieuse. Il y a lieu, par suite, d'en saisir la Cour de justice de l'Union européenne en application de l'article 267 du traité sur le fonctionnement de l'Union européenne et, jusqu'à ce que celle-ci se soit prononcée, de surseoir à statuer sur le pourvoi de la société Sucrerie de Toury.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Il est sursis à statuer sur le pourvoi de la société Sucrerie de Toury jusqu'à ce que la Cour de justice de l'Union européenne se soit prononcée sur la question suivante :<br/>
" Les produits énergétiques utilisés pour la production combinée de chaleur et d'électricité relèvent-ils exclusivement de la faculté d'exonération ouverte par le c) du 1 de l'article 15 de la directive n° 2003/96/CE du Conseil du 27 octobre 2003 ou entrent-ils également, s'agissant de la part de ces produits dont la consommation correspond à la production d'électricité, dans le champ de l'obligation d'exonération prévue par le a) du 1 de son article 14 ' ".<br/>
Article 2 : La présente décision sera notifiée à la société Sucrerie de Toury, au ministre de l'économie et des finances et au président de la Cour de justice de l'Union européenne.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
