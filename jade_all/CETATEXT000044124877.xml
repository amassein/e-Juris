<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044124877</ID>
<ANCIEN_ID>JG_L_2021_09_000000439696</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/12/48/CETATEXT000044124877.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème - 8ème chambres réunies, 28/09/2021, 439696, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-09-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>439696</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème - 8ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Cécile Isidoro</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Laurent Cytermann</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:439696.20210928</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Par une requête, enregistrée le 20 mars 2020 au secrétariat du contentieux du Conseil d'Etat, la Confédération paysanne demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler pour excès de pouvoir le refus du ministre de l'agriculture et de l'alimentation d'abroger le point 1.2 relatif aux activités à prendre en compte pour le calcul des revenus agricoles disponibles de la fiche 1 annexée à l'instruction technique DGPAAAT/SDEA/2015-330 du 9 avril 2015 ayant pour objet l'instruction des demandes d'aides à l'installation, relevant de la programmation 2014-2020 et déposées à partir du 1er janvier 2015 ;<br/>
<br/>
              2°) d'enjoindre au ministre de l'agriculture et de l'alimentation de procéder à la modification de cette instruction technique par la suppression du terme " première " de la phrase " les activités de première transformation de la production primaire de l'exploitation " figurant au point 1.2 de la fiche 1 annexée à l'instruction technique DGPAAAT/SDEA/2015-330 et d'ajouter un paragraphe précisant que les activités de panification, biscuiterie ou pâtisserie, situées dans le prolongement d'une activité de production de céréales, doivent être prises en compte au titre des revenus agricoles disponibles.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code rural et de la pêche maritime ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Cécile Isidoro, conseillère d'Etat, <br/>
<br/>
              - les conclusions de M. Laurent Cytermann, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. Aux termes de l'article D. 343-3 du code rural et de la pêche maritime : " I. En vue de faciliter leur première installation, il peut être accordé aux jeunes agriculteurs qui prévoient d'exercer une activité agricole au sens de l'article L. 311-1, à l'exclusion des activités aquacoles, et qui satisfont aux conditions fixées par la présente section les aides suivantes : 1° Une dotation jeunes agriculteurs en capital ; 2° Des prêts bonifiés à moyen terme spéciaux, dont une partie des intérêts peut être prise en charge (...) ". Aux termes de l'article L. 311-1 du même code : " Sont réputées agricoles toutes les activités correspondant à la maîtrise et à l'exploitation d'un cycle biologique de caractère végétal ou animal et constituant une ou plusieurs étapes nécessaires au déroulement de ce cycle ainsi que les activités exercées par un exploitant agricole qui sont dans le prolongement de l'acte de production ou qui ont pour support l'exploitation (...) ". Pour être éligible au bénéfice des aides mentionnées au I de l'article D. 343-3 du même code, le candidat à l'installation doit présenter un plan d'entreprise comportant un projet d'exploitation d'une durée de quatre ans et doit disposer, au cours de chacune des années de mise en œuvre de ce plan, d'un revenu disponible agricole au moins égal à 50 % du revenu professionnel global en cas d'installation à titre principal ou compris entre 30 et 50 % de son revenu professionnel en cas d'installation à titre secondaire. Il doit en outre s'engager à atteindre, au terme de la quatrième année de mise en œuvre du plan d'entreprise, un revenu disponible agricole supérieur ou égal à un montant défini par arrêté du ministre chargé de l'agriculture en cas d'installation à titre principal ou supérieur ou égal à la moitié de ce montant en cas d'installation à titre secondaire, ainsi qu'il résulte des articles D. 343-4, D. 343-5 et D. 343-7 du code rural et de la pêche maritime.<br/>
<br/>
              2. Le point 1.2 de la fiche n°1 annexée à l'instruction du 9 avril 2015 définit les activités à retenir pour l'établissement du revenu disponible agricole . Il prévoit que : " Sont considérées comme agricoles les activités suivantes : / - les activités liées à la production agricole : les activités de production primaire : production de produits du sol et de l'élevage ; - les activités de première transformation de la production primaire de l'exploitation ; - la vente des produits réalisés sur l'exploitation. / - les activités de diversification dans le prolongement de l'exploitation : tables d'hôtes, visites pédagogiques ou ayant pour support l'exploitation (chambres d'hôtes, camping à la ferme, ferme pédagogique notamment). Les marges brutes de ces activités ne doivent pas représenter plus de 50 % du total des marges brutes de l'exploitation. / Ne sont pas considérées comme agricoles, les activités suivantes : Les revenus d'une activité de diversification exercée dans une structure différente de celle de l'exploitation agricole (...) ".<br/>
<br/>
              3. Contrairement à ce que soutient la Confédération paysanne dans sa requête, le point 1.2 de la fiche n°1 annexée à l'instruction du 9 avril 2015 n'a pas pour objet de restreindre la définition des activités considérées comme agricoles aux seules activités de première transformation. Il vise aussi, explicitement, les activités de diversification situées dans le prolongement de l'exploitation, ce qui n'exclut pas, notamment, des activités de panification, de biscuiterie et de pâtisserie, afin de définir les conditions de détermination du revenu disponible agricole dont l'évolution prévisionnelle pendant les quatre premières années d'activité du demandeur constitue l'un des éléments pris en compte par l'administration pour apprécier la viabilité du projet d'installation présenté et statuer sur la demande d'aide à l'installation. Par suite, le point 1.2 critiqué ne saurait être regardé comme méconnaissant les dispositions des articles L. 311-1 et D. 343-3 du code rural et de la pêche maritime.<br/>
<br/>
              4. Toutefois, la disposition du point 1.2. de cette fiche n°1 annexée à l'instruction du 9 avril 2015, qui prévoit que, pour les activités de diversification situées dans le prolongement de l'exploitation, les marges brutes de ces activités ne doivent pas représenter plus de 50 % du total des marges brutes de l'exploitation, alors même que ces activités font partie des activités agricoles au sens de l'article L. 311-1 du code rural et de la pêche maritime, a été adoptée par une autorité incompétente, le ministre chargé de l'agriculture ne détenant d'aucun texte législatif ou réglementaire la compétence pour édicter une telle condition. Il y a donc lieu de l'annuler.<br/>
<br/>
              5. La présente décision n'appelant pas de mesure d'exécution, les conclusions de la Confédération paysanne à fin d'injonction ne peuvent qu'être rejetées.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Au point 1.2 de la fiche n° 1 annexée à l'instruction du 9 avril 2015 DGPAAT/SDEA/2015-330, la phrase " Les marges brutes de ces activités ne doit pas représenter plus de 50 % du total des marges brutes de l'exploitation " est annulée.<br/>
Article 2 : Le surplus des conclusions de la requête de la Confédération paysanne est rejeté. <br/>
Article 3 : La présente décision sera notifiée à la Confédération paysanne et au ministre de l'agriculture et de l'alimentation.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
