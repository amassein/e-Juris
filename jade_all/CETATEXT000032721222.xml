<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032721222</ID>
<ANCIEN_ID>JG_L_2016_06_000000387614</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/72/12/CETATEXT000032721222.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème - 4ème chambres réunies, 16/06/2016, 387614, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-06-16</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>387614</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème - 4ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP CELICE, BLANCPAIN, SOLTNER, TEXIDOR ; SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR>M. Guillaume Leforestier</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Laurence Marion</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2016:387614.20160616</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Par une requête, un mémoire complémentaire et un nouveau mémoire, enregistrés le 3 février et les 4 et 5 mai 2015 au secrétariat du contentieux du Conseil d'Etat, la communauté de communes du Bas-Chablais et les communes de Nernier, Messery, Chens-sur-Léman, Loisin, Massongy, Excenevex, Sciez et Yvoire demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir l'arrêté du 1er août 2014 du ministre des finances et des comptes publics, de la ministre du logement et de l'égalité des territoires et du secrétaire d'Etat chargé du budget, pris en application de l'article R. 304-1 du code de la construction et de l'habitation, en tant qu'il classe les communes de Nernier, Messery, Chens-sur-Léman, Loisin, Massongy, Excenevex, Sciez et Yvoire en zone B2, les huit décisions du 3 décembre 2014 par lesquelles la ministre du logement et de l'égalité des territoires a rejeté les recours gracieux formés par ces communes tendant à la révision de cet arrêté, ainsi que les décisions du 29 décembre 2014 par lesquelles la ministre du logement et de l'égalité des territoires a rejeté le recours gracieux formé par la communauté de communes du Bas-Chablais tendant au retrait de cet arrêté ;<br/>
<br/>
              2°) d'enjoindre à la ministre du logement et de l'égalité des territoires, au ministre des finances et des comptes publics et au secrétaire d'Etat chargé du budget de modifier cet arrêté dans un délai de deux mois pour classer les communes requérantes en zone A ou en zone B1 ou, subsidiairement, de l'abroger dans le même délai.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
<br/>
              - le code de la construction et de l'habitation ;<br/>
<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Guillaume Leforestier, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Laurence Marion, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Célice, Blancpain, Soltner, Texidor, avocat de communauté de communes du Bas-Chablais, de la commune de Nernier, de la commune de Chens-sur-Léman, de la commune de Massongy, de la commune de Messery, de la commune d'Yvoire, de la commune de Sciez, de la commune d'Excenevex et de la commune de Loisin et à la SCP Piwnica, Molinié, avocat de la ministre du logement et de l'habitat durable ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant, d'une part, qu'aux termes de l'article L. 311-1 du code de justice administrative : " Les tribunaux administratifs sont, en premier ressort, juges de droit commun du contentieux administratif, sous réserve des compétences que l'objet du litige ou l'intérêt d'une bonne administration de la justice conduisent à attribuer à une autre juridiction administrative " ; qu'aux termes de l'article R. 311-1 du même code : " Le Conseil d'Etat est compétent pour connaître en premier et dernier ressort : (...) 2° Des recours dirigés contre les actes réglementaires des ministres ainsi que contre les actes des ministres qui ne peuvent être pris qu'après avis du Conseil d'Etat " ; qu'aux termes du premier alinéa de l'article R. 312-1 du même code : " Lorsqu'il n'en est pas disposé autrement par les dispositions de la section 2 du présent chapitre ou par un texte spécial, le tribunal administratif territorialement compétent est celui dans le ressort duquel a légalement son siège l'autorité qui, soit en vertu de son pouvoir propre, soit par délégation, a pris la décision attaquée ou a signé le contrat litigieux (...) " ; que l'article R. 312-7 de ce code dispose que : " Les litiges relatifs aux déclarations d'utilité publique, au domaine public, aux affectations d'immeubles, au remembrement, à l'urbanisme et à l'habitation, au permis de construire, d'aménager ou de démolir, au classement des monuments et des sites et, de manière générale, aux décisions concernant des immeubles relèvent de la compétence du tribunal administratif dans le ressort duquel se trouvent les immeubles faisant l'objet du litige " ;<br/>
<br/>
              2. Considérant, d'autre part, qu'aux termes de l'article R. 304-1 du code de la construction et de l'habitation : " Pour l'application de certaines aides au logement, un arrêté des ministres chargés du logement et du budget, révisé au moins tous les trois ans, établit un classement des communes du territoire national en zones géographiques en fonction du déséquilibre entre l'offre et de la demande de logements. / Ces zones sont désignées, par ordre de déséquilibre décroissant, sous les lettres A bis, A, B1, B2 et C. La zone A bis est incluse dans la zone A, les zones B1 et B2 forment la zone B " ;<br/>
<br/>
              3. Considérant qu'un arrêté pris par les ministres chargés du logement et du budget en application de l'article R. 304-1 du code de la construction et de l'habitation, qui se borne à classer les communes dans différentes zones en fonction du déséquilibre entre l'offre et la demande de logements, ne revêt pas un caractère réglementaire ; qu'une demande d'annulation d'un tel arrêté ne relève d'aucune des catégories de recours dont il appartient au Conseil d'Etat de connaître en premier et dernier ressort en vertu des dispositions de l'article R. 311-1 du code de justice administrative ; <br/>
<br/>
              4. Considérant que la requête de la communauté de communes du Bas-Chablais et des communes de Nernier, Messery, Chens-sur-Léman, Loisin, Massongy, Excenevex, Sciez et Yvoire tend à l'annulation de l'arrêté du 1er août 2014 en tant qu'il classe ces communes en zone B2 ; que cette requête, qui soulève un litige qui est au nombre de ceux que mentionne l'article R. 312-7 du code de justice administrative, ressortit, en vertu des dispositions de cet article, à la compétence de premier ressort du tribunal administratif de Grenoble, dans le ressort duquel ces communes sont situées ; <br/>
<br/>
              5. Considérant qu'il résulte de ce qui précède qu'il y a lieu d'attribuer le jugement des conclusions de la requête au tribunal administratif de Grenoble ;<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement des conclusions de la requête de la communauté de communes du Bas-Chablais et des communes de Nernier, Messery, Chens-sur-Léman, Loisin, Massongy, Excenevex, Sciez et Yvoire est attribué au tribunal administratif de Grenoble.<br/>
<br/>
Article 2 : La présente décision sera notifiée à la communauté de communes du Bas-Chablais, aux communes de Nernier, Messery, Chens-sur-Léman, Loisin, Massongy, Excenevex, Sciez et Yvoire et à la ministre du logement et de l'habitat durable.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
