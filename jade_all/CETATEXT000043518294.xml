<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043518294</ID>
<ANCIEN_ID>JG_L_2021_05_000000430342</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/51/82/CETATEXT000043518294.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème - 1ère chambres réunies, 19/05/2021, 430342</TITRE>
<DATE_DEC>2021-05-19</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>430342</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème - 1ère chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Pierre Vaiss</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Raphaël Chambon</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:430342.20210519</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Par une requête sommaire, un mémoire complémentaire et quatre nouveaux mémoires, enregistrés les 29 avril, 23 mai, 25 octobre, 11 novembre 2019 et les 2 juin et 30 septembre 2020 au secrétariat du contentieux du Conseil d'Etat, le syndicat national des agents publics de l'éducation nationale (SNAPEN) demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le 3° de l'article 1er et l'article 2 de l'arrêté du 8 avril 2019 du ministre de l'éducation nationale et de la jeunesse et du ministre de l'action et des comptes publics modifiant l'arrêté du 10 mai 2017 fixant la liste des conditions d'exercice et des fonctions particulières des personnels des corps enseignants d'éducation et de psychologue au ministère de l'éducation nationale, de l'enseignement supérieur et de la recherche prises en compte pour un avancement à la classe exceptionnelle ; <br/>
<br/>
              2°) d'annuler par voie de conséquence l'ensemble des tableaux d'avancement des fonctionnaires promus sur la base de cet arrêté ainsi que les arrêtés individuels de promotion établis sur la base de ces tableaux d'avancement ;<br/>
<br/>
              3°) d'enjoindre au ministre de l'éducation nationale et de la jeunesse de reconstituer rétroactivement la carrière de l'ensemble des fonctionnaires concernés dans un délai de deux mois à compter de la présente décision. <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
<br/>
              - la loi n° 84-16 du 11 janvier 1984 ;<br/>
              - la loi n° 2010-751 du 5 juillet 2010 ;<br/>
              - le décret n° 72-580 du 4 juillet 1972 ;<br/>
              - le décret n° 2017-786 du 5 mai 2017 ; <br/>
              - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Pierre Vaiss, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Raphaël Chambon, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. En vertu de l'article 39 de la loi du 5 juillet 2010 relative à la rénovation du dialogue social, les statuts particuliers des corps mentionnés à l'article 10 de la loi du 11 janvier 1984 portant dispositions statutaires relatives à la fonction publique de l'Etat, au nombre desquels figurent les corps enseignants, peuvent " subordonner l'avancement de grade à l'exercice préalable d'autres fonctions impliquant notamment des conditions d'exercice difficiles ou comportant des missions particulières ". Aux termes de l'article 13 sexies du décret du 4 juillet 1972 relatif au statut particulier des professeurs agrégés de l'enseignement du second degré, dans sa rédaction issue de l'article 59 du décret du 5 mai 2017 : " I.-Peuvent être promus au grade de professeur agrégé de classe exceptionnelle, au choix, par voie d'inscription à un tableau annuel d'avancement, les professeurs agrégés qui, à la date d'établissement dudit tableau, ont atteint au moins le 2e échelon de la hors-classe, et justifient de huit années de fonctions accomplies dans des conditions d'exercice difficiles ou sur des fonctions particulières au sein d'un corps enseignant, d'éducation ou de psychologue relevant du ministère de l'éducation nationale. / La liste de ces fonctions est fixée par arrêté du ministre chargé de l'éducation nationale et du ministre chargé de la fonction publique. (...) ". <br/>
<br/>
              2. L'arrêté du 10 mai 2017 du ministre de l'éducation nationale et de la jeunesse et du ministre de l'action et des comptes publics fixant la liste des conditions d'exercice et des fonctions particulières des personnels des corps enseignants d'éducation et de psychologue au ministère de l'éducation nationale, de l'enseignement supérieur et de la recherche prises en compte pour un avancement à la classe exceptionnelle, prévoyait dans sa version initiale, au troisième alinéa de l'article 1er, que les " affectation[s] dans l'enseignement supérieur " étaient au nombre des fonctions particulières permettant l'accès à la classe exceptionnelle des professeurs agrégés du second degré au titre du I de l'article 13 sexies du décret du 4 juillet 1972. Le 3° de l'article 1er de l'arrêté du 8 avril 2019 du ministre de l'éducation nationale et de la jeunesse et du ministre de l'action et des comptes publics a modifié le troisième alinéa de l'article 1er de l'arrêté du 10 mai 2017, désormais sixième alinéa de l'article 1er de ce dernier arrêté, en substituant aux mots : " l'enseignement supérieur ; " les mots : " un établissement de l'enseignement supérieur ou exerçant l'intégralité de leur service dans une classe préparatoire aux grandes écoles ; ". L'article 2 de l'arrêté du 8 avril 2019 a en outre prévu que les agents reconnus éligibles à un avancement à la classe exceptionnelle au titre des années 2017 et 2018 le demeuraient.<br/>
<br/>
              3. Le syndicat national des agents publics de l'éducation nationale demande l'annulation pour excès de pouvoir, d'une part, des dispositions du 3° de l'article 1er et, d'autre part, des dispositions de l'article 2 de cet arrêté du 8 avril 2019. Eu égard aux moyens qu'il soulève, il doit être regardé comme ne demandant l'annulation de ces dispositions qu'en tant qu'elles concernent les professeurs agrégés. <br/>
<br/>
              4. Il résulte des dispositions du I de l'article 13 sexies du décret du 4 juillet 1972 relatif au statut particulier des professeurs agrégés de l'enseignement du second degré, dans sa rédaction issue de l'article 59 du décret du 5 mai 2017, citées au point 1, qu'au nombre des conditions que les professeurs agrégés hors classe doivent remplir pour être éligibles à une promotion au grade de professeur agrégé de classe exceptionnelle au titre du " 1er vivier " d'accès à ce grade à accès fonctionnel, figure la condition tirée de ce qu'ils " justifient de huit années de fonctions accomplies dans des conditions d'exercice difficiles ou sur des fonctions particulières ". Ces dernières dispositions se bornant à reprendre les termes de l'article 39 de la loi du 5 juillet 2010 relative à la rénovation du dialogue social, également cités au point 1, en vertu duquel les statuts particuliers des corps mentionnés à l'article 10 de la loi du 11 janvier 1984 peuvent " subordonner l'avancement de grade à l'exercice préalable d'autres fonctions impliquant notamment des conditions d'exercice difficiles ou comportant des missions particulières ", elles n'ont pu, sans méconnaître l'article 39 de la loi du 5 juillet 2010, renvoyer purement et simplement à un arrêté du ministre chargé de l'éducation nationale et du ministre chargé de la fonction publique le soin d'établir la liste de ces fonctions, sans définir au préalable, avec une précision suffisante, les modalités suivant lesquelles cette condition doit être appréciée. Par suite, les dispositions attaquées de l'arrêté du 10 mai 2017 modifié, auquel le I de l'article 13 sexies du décret du 4 juillet 1972, dans sa rédaction issue de l'article 59 du décret du 5 mai 2017, a illégalement subdélégué le soin de définir les fonctions accomplies dans des conditions d'exercice difficiles ou sur des fonctions particulières, sont elles-mêmes entachées d'incompétence. <br/>
<br/>
              5. Il résulte de tout ce qui précède que, sans qu'il soit besoin de se prononcer sur les autres moyens de sa requête, le syndicat national des agents publics de l'éducation nationale est fondé à demander l'annulation des dispositions du 3° de l'article 1er et de l'article 2 de l'arrêté du 8 avril 2019 attaqué, en tant que ces dispositions concernent les professeurs agrégés. En revanche, ses autres conclusions, faute d'être assorties des précisions suffisantes pour en apprécier le bien-fondé, ne peuvent qu'être rejetées. <br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Les dispositions du 3° de l'article 1er et celles de l'article 2 de l'arrêté du 8 avril 2019 modifiant l'arrêté du 10 mai 2017 fixant la liste des conditions d'exercice et des fonctions particulières des personnels des corps enseignants d'éducation et de psychologue au ministère de l'éducation nationale, de l'enseignement supérieur et de la recherche prises en compte pour un avancement à la classe exceptionnelle, sont annulées en tant que ces dispositions concernent les professeurs agrégés.<br/>
Article 2 : Le surplus des conclusions de la requête du syndicat national des agents publics de l'éducation nationale est rejeté.<br/>
Article 3 : La présente décision sera notifiée au syndicat national des agents publics de l'éducation nationale, au ministre de l'éducation nationale, de la jeunesse et des sports et à la ministre de la transformation et de la fonction publiques. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-02-02-02 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - COMPÉTENCE. RÉPARTITION DES COMPÉTENCES ENTRE AUTORITÉS DISPOSANT DU POUVOIR RÉGLEMENTAIRE. MESURES À PRENDRE PAR DÉCRET. - LOI PERMETTANT À CERTAINS STATUTS PARTICULIERS DE SUBORDONNER UN AVANCEMENT DE GRADE À L'EXERCICE PRÉALABLE D'AUTRES FONCTIONS IMPLIQUANT DES CONDITIONS D'EXERCICE DIFFICILES OU COMPORTANT DES MISSIONS PARTICULIÈRES - DÉCRET STATUTAIRE SE BORNANT À CONDITIONNER CET AVANCEMENT À LA JUSTIFICATION DE HUIT ANNÉES DE FONCTIONS ACCOMPLIES DANS DES CONDITIONS D'EXERCICE DIFFICILES OU SUR DES FONCTIONS PARTICULIÈRES, ET RENVOYANT POUR LE RESTE À UN ARRÊTÉ - SUBDÉLÉGATION ILLÉGALE, FAUTE POUR LE DÉCRET DE DÉFINIR AVEC UNE PRÉCISION SUFFISANTE LES MODALITÉS SUIVANT LESQUELLES CETTE CONDITION DOIT ÊTRE APPRÉCIÉE [RJ1] - CONSÉQUENCE - ANNULATION DE L'ARRÊTÉ.
</SCT>
<ANA ID="9A"> 01-02-02-02 I de l'article 13 sexies du décret n° 72-580 du 4 juillet 1972, dans sa rédaction issue de l'article 59 du décret n° 2017-786 du 5 mai 2017, prévoyant qu'au nombre des conditions que les professeurs agrégés hors classe doivent remplir pour être éligibles à une promotion au grade de professeur agrégé de classe exceptionnelle au titre du 1er vivier d'accès à ce grade à accès fonctionnel, figure la condition tirée de ce qu'ils justifient de huit années de fonctions accomplies dans des conditions d'exercice difficiles ou sur des fonctions particulières.,,,Ces dispositions se bornant à reprendre l'article 39 de la loi n° 2010-751 du 5 juillet 2010, en vertu duquel les statuts particuliers des corps mentionnés à l'article 10 de la loi n° 84-16 du 11 janvier 1984 peuvent subordonner l'avancement de grade à l'exercice préalable d'autres fonctions impliquant notamment des conditions d'exercice difficiles ou comportant des missions particulières, elles n'ont pu, sans méconnaître cet article 39 de la loi du 5 juillet 2010, renvoyer purement et simplement à un arrêté le soin d'établir la liste de ces fonctions, sans définir au préalable, avec une précision suffisante, les modalités suivant lesquelles cette condition doit être appréciée.... ,,Par suite, les dispositions attaquées de l'arrêté du 10 mai 2017 modifié, auquel le I de l'article 13 sexies du décret du 4 juillet 1972 a illégalement subdélégué le soin de définir les fonctions accomplies dans des conditions d'exercice difficiles ou sur des fonctions particulières, sont elles-mêmes entachées d'incompétence.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>1. Rappr., sur le caractère statutaire des règles de promotion au grade supérieur et sur la nécessité pour le décret statutaire de définir suffisamment les modalités d'appréciation des conditions de cette promotion, CE, 14 janvier 1987, Mme,, n° 59145, T. p. 539.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
