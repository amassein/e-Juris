<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033364645</ID>
<ANCIEN_ID>JG_L_2016_11_000000395122</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/36/46/CETATEXT000033364645.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Assemblée, 09/11/2016, 395122, Publié au recueil Lebon</TITRE>
<DATE_DEC>2016-11-09</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>395122</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Assemblée</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GARREAU, BAUER-VIOLAS, FESCHOTTE-DESBOIS ; SCP FOUSSARD, FROGER</AVOCATS>
<RAPPORTEUR>Mme Anne Iljic</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Aurélie Bretonneau</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CEASS:2016:395122.20161109</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La Fédération départementale des libres penseurs de Seine-et-Marne a demandé au tribunal administratif de Melun d'annuler pour excès de pouvoir la décision implicite par laquelle le maire de Melun a rejeté sa demande tendant à ce qu'il s'abstienne d'installer une crèche de Noël dans l'enceinte de l'hôtel de ville de cette commune durant le mois de décembre 2012. Par un jugement n°1300483 du 22 décembre 2014, le tribunal administratif de Melun a rejeté sa demande. <br/>
<br/>
              Par un arrêt n°15PA00814 du 8 octobre 2015, la cour administrative d'appel de Paris, faisant droit à l'appel formé par la Fédération départementale des libres penseurs de Seine-et-Marne, a annulé ce jugement et annulé pour excès de pouvoir la décision implicite de rejet du maire de la commune Melun. <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 8 décembre 2015 et 11 janvier 2016 au secrétariat du contentieux du Conseil d'Etat, la commune de Melun demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt n°15PA00814 du 8 octobre 2015 de la cour administrative d'appel de Paris ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à ses conclusions d'appel ;<br/>
<br/>
              3°) de mettre à la charge de la Fédération départementale des libre penseurs de Seine-et-Marne une somme de 6 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la Constitution ; <br/>
              - la loi du 9 décembre 1905 concernant la séparation des Eglises et de l'Etat ;<br/>
              - le code de justice administrative. <br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Anne Iljic, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Aurélie Bretonneau, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Garreau, Bauer-Violas, Feschotte-Desbois, avocat de la commune de Melun et à la SCP Foussard, Froger, avocat de la Fédération départementale des libres penseurs de Seine-et-Marne ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
<br/>
              1. L'intervention de l'association EGALE, qui tend au rejet du pourvoi, a été enregistrée le 26 octobre 2016, soit postérieurement à la clôture de l'instruction. Cette intervention, qui, au surplus, n'a pas été présentée par le ministère d'un avocat au Conseil d'Etat et à la Cour de cassation, n'est, par suite, pas recevable.<br/>
<br/>
              2. Il ressort des pièces du dossier soumis aux juges du fond que, par courrier du 18 octobre 2012, la Fédération départementale des libres penseurs de Seine-et-Marne a demandé au maire de Melun de s'abstenir d'installer une crèche de Noël dans l'enceinte de l'hôtel de ville de cette commune durant le mois de décembre 2012. Une crèche ayant néanmoins été installée dans la cour intérieure de l'hôtel de ville, la Fédération départementale des libres penseurs de Seine-et-Marne a demandé au tribunal administratif de Melun d'annuler pour excès de pouvoir la décision du maire de cette commune de procéder à cette installation. Par un jugement du 22 décembre 2014, le tribunal administratif de Melun a rejeté sa demande. Par un arrêt du 8 octobre 2015, la cour administrative d'appel de Paris a fait droit à l'appel formé par la Fédération des libres penseurs de Seine-et-Marne contre ce jugement. La commune de Melun se pourvoit en cassation contre cet arrêt. <br/>
<br/>
              3. Aux termes des trois premières phrases du premier alinéa de l'article 1er de la Constitution : " La France est une République indivisible, laïque, démocratique et sociale. Elle assure l'égalité devant la loi de tous les citoyens sans distinction d'origine, de race ou de religion. Elle respecte toutes les croyances. ". La loi du 9 décembre 1905 concernant la séparation des Eglises et de l'Etat crée, pour les personnes publiques, des obligations, en leur imposant notamment, d'une part, d'assurer la liberté de conscience et de garantir le libre exercice des cultes, d'autre part, de veiller à la neutralité des agents publics et des services publics à l'égard des cultes, en particulier en n'en reconnaissant ni n'en subventionnant aucun. Ainsi, aux termes de l'article 1er de cette loi : " La République assure la liberté de conscience. Elle garantit le libre exercice des cultes sous les seules restrictions édictées ci-après dans l'intérêt de l'ordre public " et, aux termes de son article 2 : " La République ne reconnaît, ne salarie ni ne subventionne aucun culte. ". Pour la mise en oeuvre de ces principes, l'article 28 de cette même loi précise que : " Il est interdit, à l'avenir, d'élever ou d'apposer aucun signe ou emblème religieux sur les monuments publics ou en quelque emplacement public que ce soit, à l'exception des édifices servant au culte, des terrains de sépulture dans les cimetières, des monuments funéraires ainsi que des musées ou expositions ". Ces dernières dispositions, qui ont pour objet d'assurer la neutralité des personnes publiques à l'égard des cultes, s'opposent à l'installation par celles-ci, dans un emplacement public, d'un signe ou emblème manifestant la reconnaissance d'un culte ou marquant une préférence religieuse. Elles ménagent néanmoins des exceptions à cette interdiction. Ainsi, est notamment réservée la possibilité pour les personnes publiques d'apposer de tels signes ou emblèmes dans un emplacement public à titre d'exposition. En outre, en prévoyant que l'interdiction qu'il a édictée ne s'appliquerait que pour l'avenir, le législateur a préservé les signes et emblèmes religieux existants à la date de l'entrée en vigueur de la loi.<br/>
<br/>
              4. Une crèche de Noël est une représentation susceptible de revêtir une pluralité de significations. Il s'agit en effet d'une scène qui fait partie de l'iconographie chrétienne et qui, par là, présente un caractère religieux. Mais il s'agit aussi d'un élément faisant partie des décorations et illustrations qui accompagnent traditionnellement, sans signification religieuse particulière, les fêtes de fin d'année.<br/>
<br/>
              5. Eu égard à cette pluralité de significations, l'installation d'une crèche de Noël, à titre temporaire, à l'initiative d'une personne publique, dans un emplacement public, n'est légalement possible que lorsqu'elle présente un caractère culturel, artistique ou festif, sans exprimer la reconnaissance d'un culte ou marquer une préférence religieuse. Pour porter cette dernière appréciation, il y a lieu de tenir compte non seulement du contexte, qui doit être dépourvu de tout élément de prosélytisme, des conditions particulières de cette installation, de l'existence ou de l'absence d'usages locaux, mais aussi du lieu de cette installation. A cet égard, la situation est différente, selon qu'il s'agit d'un bâtiment public, siège d'une collectivité publique ou d'un service public, ou d'un autre emplacement public.<br/>
<br/>
              6. Dans l'enceinte des bâtiments publics, sièges d'une collectivité publique ou d'un service public, le fait pour une personne publique de procéder à l'installation d'une crèche de Noël ne peut, en l'absence de circonstances particulières permettant de lui reconnaître un caractère culturel, artistique ou festif, être regardé comme conforme aux exigences qui découlent du principe de neutralité des personnes publiques. <br/>
<br/>
              7. A l'inverse, dans les autres emplacements publics, eu égard au caractère festif des installations liées aux fêtes de fin d'année notamment sur la voie publique, l'installation à cette occasion et durant cette période d'une crèche de Noël par une personne publique est possible, dès lors qu'elle ne constitue pas un acte de prosélytisme ou de revendication d'une opinion religieuse <br/>
<br/>
              8. Il ressort des énonciations de l'arrêt attaqué que, pour juger que la crèche installée dans l'enceinte de l'hôtel de ville de la commune de Melun revêtait le caractère d'un signe ou emblème religieux dont l'installation est interdite par l'article 28 de la loi du 9 décembre 1905, la cour administrative d'appel de Paris s'est bornée à relever que cette installation constituait la représentation figurative d'une scène fondatrice de la religion chrétienne. En se fondant sur ces seules constatations pour en déduire qu'elle méconnaissait l'article 28 de la loi du 9 décembre 1905, elle a entaché son arrêt d'erreur de droit. <br/>
<br/>
              9. Il résulte de ce qui précède que la commune de Melun est fondée à demander l'annulation de l'arrêt qu'elle attaque. <br/>
<br/>
              10. Il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application de l'article L. 821-2 du code de justice administrative.<br/>
<br/>
              11. Il ressort des pièces du dossier que, pendant la période des fêtes de la fin de l'année 2012, le maire de Melun a installé une crèche de Noël dans une alcôve située sous le porche reliant la cour d'honneur au jardin de l'hôtel de ville de Melun et permettant l'accès des usagers aux services publics municipaux. L'installation de cette crèche dans l'enceinte de ce bâtiment public, siège d'une collectivité publique, ne résultait d'aucun usage local et n'était accompagnée d'aucun autre élément marquant son inscription dans un environnement culturel, artistique ou festif. Il s'ensuit que le fait pour le maire de Melun d'avoir procédé à cette installation dans l'enceinte d'un bâtiment public, siège d'une collectivité publique, en l'absence de circonstances particulières permettant de lui reconnaître un caractère culturel, artistique ou festif, a méconnu l'article 28 de la loi du 9 décembre 1905 et les exigences attachées au principe de neutralité des personnes publiques.<br/>
<br/>
              12. Il résulte de qui précède que la Fédération des libres penseurs de Seine-et-Marne, qui a intérêt pour agir, contrairement à ce qui est soutenu dans la fin de non recevoir opposée par la commune de Melun, est fondée à soutenir que c'est à tort que, par le jugement du 22 décembre 2014, le tribunal administratif de Melun a rejeté sa demande et à demander l'annulation de la décision attaquée. <br/>
<br/>
              13. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la commune de Melun une somme de 3000 euros à verser à la Fédération des libres penseurs de Seine-et-Marne au titre de l'article L. 761-1 du code de justice administrative. Ces dispositions font en revanche obstacle à ce qu'une somme soit mise à ce titre à la charge la Fédération des libres penseurs de Seine-et-Marne qui n'est pas, dans la présente instance, la partie perdante. <br/>
<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : L'intervention de l'association EGALE n'est pas admise. <br/>
Article 2 : L'arrêt de la cour administrative d'appel de Paris du 8 octobre 2015 et le jugement du 22 décembre 2014 du tribunal administratif de Melun sont annulés.<br/>
Article 3 : La décision du maire de Melun d'installer une crèche de Noël dans l'enceinte de l'hôtel de ville de cette commune durant le mois de décembre 2012 est annulée.<br/>
Article 4 : La commune de Melun versera à la Fédération départementale des libres penseurs de Seine-et-Marne une somme de 3000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
Article 5 : Les conclusions présentées par la commune de Melun au titre de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
Article 6 : La présente décision sera notifiée à la commune de Melun, à la Fédération départementale des libres penseurs de Seine-et-Marne et au ministre de l'intérieur. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-04-03-07-02 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - VIOLATION DIRECTE DE LA RÈGLE DE DROIT. PRINCIPES GÉNÉRAUX DU DROIT. PRINCIPES INTÉRESSANT L'ACTION ADMINISTRATIVE. NEUTRALITÉ DU SERVICE PUBLIC. - 1) PRINCIPE DE LAÏCITÉ - LOI DU 9 DÉCEMBRE 1905 - PORTÉE [RJ1] - 2) INTERDICTION DES SIGNES OU EMBLÈMES RELIGIEUX SUR LES EMPLACEMENTS PUBLICS (ART. 28 DE LA LOI DE 1905) [RJ2] - A) PORTÉE - B) CAS DE L'INSTALLATION TEMPORAIRE D'UNE CRÈCHE DE NOËL - LÉGALITÉ - I) CONDITION - INSTALLATION PRÉSENTANT UN CARACTÈRE CULTUREL, ARTISTIQUE OU FESTIF ET N'EXPRIMANT NI RECONNAISSANCE D'UN CULTE, NI PRÉFÉRENCE RELIGIEUSE [RJ3] - II) ELÉMENTS D'APPRÉCIATION À PRENDRE EN COMPTE - III) PORTÉE DE L'ÉLÉMENT D'APPRÉCIATION DU LIEU - DISTINCTION ENTRE LES BÂTIMENTS PUBLICS ET LES AUTRES EMPLACEMENTS PUBLICS - IV) ESPÈCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">21 CULTES. - 1) PRINCIPE DE LAÏCITÉ - LOI DU 9 DÉCEMBRE 1905 - PORTÉE [RJ1] - 2) INTERDICTION DES SIGNES OU EMBLÈMES RELIGIEUX SUR LES EMPLACEMENTS PUBLICS (ART. 28 DE LA LOI DE 1905) [RJ2] - A) PORTÉE - B) CAS DE L'INSTALLATION TEMPORAIRE D'UNE CRÈCHE DE NOËL - LÉGALITÉ - I) CONDITION - INSTALLATION PRÉSENTANT UN CARACTÈRE CULTUREL, ARTISTIQUE OU FESTIF ET N'EXPRIMANT NI RECONNAISSANCE D'UN CULTE, NI PRÉFÉRENCE RELIGIEUSE [RJ3] - II) ELÉMENTS D'APPRÉCIATION À PRENDRE EN COMPTE - III) PORTÉE DE L'ÉLÉMENT D'APPRÉCIATION DU LIEU - DISTINCTION ENTRE LES BÂTIMENTS PUBLICS ET LES AUTRES EMPLACEMENTS PUBLICS - IV) ESPÈCE.
</SCT>
<ANA ID="9A"> 01-04-03-07-02 1) Le principe de laïcité et la loi du 9 décembre 1905 concernant la séparation des Eglises et de l'Etat créent, pour les personnes publiques, des obligations, en leur imposant notamment, d'une part, d'assurer la liberté de conscience et de garantir le libre exercice des cultes, d'autre part, de veiller à la neutralité des agents publics et des services publics à l'égard des cultes, en particulier en n'en reconnaissant ni n'en subventionnant aucun.... ,,2) a) L'article 28 de la loi du 9 décembre 1905, qui a pour objet d'assurer la neutralité des personnes publiques à l'égard des cultes, s'opposent à l'installation par celles-ci, dans un emplacement public, d'un signe ou emblème manifestant la reconnaissance d'un culte ou marquant une préférence religieuse.,,,Il ménage néanmoins des exceptions à cette interdiction. Ainsi, est notamment réservée la possibilité pour les personnes publiques d'apposer de tels signes ou emblèmes dans un emplacement public à titre d'exposition. En outre, en prévoyant que l'interdiction qu'il a édictée ne s'appliquerait que pour l'avenir, le législateur a préservé les signes et emblèmes religieux existant à la date de l'entrée en vigueur de la loi.,,,b) i) Une crèche de Noël est une représentation susceptible de revêtir une pluralité de significations. Il s'agit en effet d'une scène qui présente un caractère religieux, mais aussi d'un élément faisant partie des décorations et illustrations qui accompagnent traditionnellement les fêtes de fin d'année, sans signification religieuse particulière.,,,Eu égard à cette pluralité de significations, l'installation d'une crèche de Noël, à titre temporaire, à l'initiative d'une personne publique, dans un emplacement public, n'est légalement possible que lorsqu'elle présente un caractère culturel, artistique ou festif, sans exprimer la reconnaissance d'un culte ou marquer une préférence religieuse.... ,,ii) Pour apprécier si l'installation d'une crèche de Noël présente un caractère culturel, artistique ou festif, ou si elle exprime la reconnaissance d'un culte ou marque une préférence religieuse, il y a lieu de tenir compte non seulement du contexte, qui doit être dépourvu de tout élément de prosélytisme, des conditions particulières de cette installation, de l'existence ou de l'absence d'usages locaux, mais aussi du lieu de cette installation.,,,iii) Au regard du lieu de l'installation, la situation est différente, selon qu'il s'agit d'un bâtiment public, siège d'une collectivité publique ou d'un service public, ou d'un autre emplacement public.,,,Dans l'enceinte des bâtiments publics, sièges d'une collectivité publique ou d'un service public, le fait pour une personne publique de procéder à l'installation d'une crèche de Noël ne peut, en l'absence de circonstances particulières permettant de lui reconnaître un caractère culturel, artistique ou festif, être regardé comme conforme aux exigences qui découlent du principe de neutralité des personnes publiques.,,,A l'inverse, dans les autres emplacements publics, eu égard au caractère festif des installations liées aux fêtes de fin d'année notamment sur la voie publique, l'installation à cette occasion et durant cette période d'une crèche de Noël par une personne publique est possible dès lors qu'elle ne constitue pas un acte de prosélytisme ou de revendication d'une opinion religieuse.,,,iv) Crèche installée dans une alcôve d'un porche de l'hôtel de ville de Melun permettant l'accès des usagers aux services publics municipaux. L'installation de cette crèche dans l'enceinte de ce bâtiment public, siège d'une collectivité publique, ne résultait d'aucun usage local et n'était accompagnée d'aucun autre élément marquant son inscription dans un environnement culturel, artistique ou festif. En l'absence de circonstances particulières permettant de lui reconnaître un caractère culturel, artistique ou festif, l'installation de cette crèche méconnaît l'article 28 de la loi du 9 décembre 1905 et les exigences attachées au principe de neutralité des personnes publiques.</ANA>
<ANA ID="9B"> 21 1) Le principe de laïcité et la loi du 9 décembre 1905 concernant la séparation des Eglises et de l'Etat créent, pour les personnes publiques, des obligations, en leur imposant notamment, d'une part, d'assurer la liberté de conscience et de garantir le libre exercice des cultes, d'autre part, de veiller à la neutralité des agents publics et des services publics à l'égard des cultes, en particulier en n'en reconnaissant ni n'en subventionnant aucun.... ,,2) a) L'article 28 de la loi du 9 décembre 1905, qui a pour objet d'assurer la neutralité des personnes publiques à l'égard des cultes, s'opposent à l'installation par celles-ci, dans un emplacement public, d'un signe ou emblème manifestant la reconnaissance d'un culte ou marquant une préférence religieuse.,,,Il ménage néanmoins des exceptions à cette interdiction. Ainsi, est notamment réservée la possibilité pour les personnes publiques d'apposer de tels signes ou emblèmes dans un emplacement public à titre d'exposition. En outre, en prévoyant que l'interdiction qu'il a édictée ne s'appliquerait que pour l'avenir, le législateur a préservé les signes et emblèmes religieux existant à la date de l'entrée en vigueur de la loi.,,,b) i) Une crèche de Noël est une représentation susceptible de revêtir une pluralité de significations. Il s'agit en effet d'une scène qui présente un caractère religieux, mais aussi d'un élément faisant partie des décorations et illustrations qui accompagnent traditionnellement les fêtes de fin d'année, sans signification religieuse particulière.,,,Eu égard à cette pluralité de significations, l'installation d'une crèche de Noël, à titre temporaire, à l'initiative d'une personne publique, dans un emplacement public, n'est légalement possible que lorsqu'elle présente un caractère culturel, artistique ou festif, sans exprimer la reconnaissance d'un culte ou marquer une préférence religieuse.... ,,ii) Pour apprécier si l'installation d'une crèche de Noël présente un caractère culturel, artistique ou festif, ou si elle exprime la reconnaissance d'un culte ou marque une préférence religieuse, il y a lieu de tenir compte non seulement du contexte, qui doit être dépourvu de tout élément de prosélytisme, des conditions particulières de cette installation, de l'existence ou de l'absence d'usages locaux, mais aussi du lieu de cette installation.,,,iii) Au regard du lieu de l'installation, la situation est différente, selon qu'il s'agit d'un bâtiment public, siège d'une collectivité publique ou d'un service public, ou d'un autre emplacement public.,,,Dans l'enceinte des bâtiments publics, sièges d'une collectivité publique ou d'un service public, le fait pour une personne publique de procéder à l'installation d'une crèche de Noël ne peut, en l'absence de circonstances particulières permettant de lui reconnaître un caractère culturel, artistique ou festif, être regardé comme conforme aux exigences qui découlent du principe de neutralité des personnes publiques.,,,A l'inverse, dans les autres emplacements publics, eu égard au caractère festif des installations liées aux fêtes de fin d'année notamment sur la voie publique, l'installation à cette occasion et durant cette période d'une crèche de Noël par une personne publique est possible dès lors qu'elle ne constitue pas un acte de prosélytisme ou de revendication d'une opinion religieuse.,,,iv) Crèche installée dans une alcôve d'un porche de l'hôtel de ville de Melun permettant l'accès des usagers aux services publics municipaux. L'installation de cette crèche dans l'enceinte de ce bâtiment public, siège d'une collectivité publique, ne résultait d'aucun usage local et n'était accompagnée d'aucun autre élément marquant son inscription dans un environnement culturel, artistique ou festif. En l'absence de circonstances particulières permettant de lui reconnaître un caractère culturel, artistique ou festif, l'installation de cette crèche méconnaît l'article 28 de la loi du 9 décembre 1905 et les exigences attachées au principe de neutralité des personnes publiques.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1]Rappr. CE, 16 mars 2005, Ministre de l'outre-mer c/ Gouvernement de la Polynésie française, n° 265560, p. 108 ; Cons. constitutionnel, 21 février 2013, n° 2012-297 QPC.,,[RJ2] Cf. décision du même jour, Fédération de la libre pensée de Vendée, n° 395223, à publier au Recueil. Rappr., s'agissant de la neutralité politique, CE, 27 juillet 2005, Commune de Sainte-Anne, n° 259806, p. 347.,,[RJ3]Rappr., CE, Assemblée, 19 juillet 2011, Commune de Trélazé, n° 308544, p. 370 ; CE, Assemblée, 19 juillet 2011, Fédération de la libre pensée et de l'action sociale du Rhône et Picquier, n° 308817, p. 372 ; CE, 3 octobre 2011, Communauté d'agglomération Saint-Etienne métropole, n° 326460, T. pp. 795-810-920 ; CE, 4 février 2012, Fédération de la libre pensée et d'action sociale du Rhône, n° 336462, p. 185 ; CE, 26 novembre 2012, Agence de l'environnement et de la maîtrise de l'énergie, n° 344379, p. 390 ; CE, 15 février 2013, Association Grande confrérie de Saint Martial et autres, n° 347049, p. 10.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
