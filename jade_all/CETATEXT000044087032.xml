<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044087032</ID>
<ANCIEN_ID>JG_L_2021_08_000000455759</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/08/70/CETATEXT000044087032.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 30/08/2021, 455759, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-08-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>455759</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2021:455759.20210830</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              M. B... A... a demandé au juge des référés du tribunal administratif de Paris, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, en premier lieu, de l'admettre au bénéfice de l'aide juridictionnelle provisoire, en deuxième lieu, d'enjoindre au préfet de police de procéder à l'enregistrement de sa demande d'asile et de lui remettre une attestation de demande d'asile en procédure normale ainsi que le dossier de demande d'asile, dans un délai de vingt-quatre heures suivant la notification de l'ordonnance à intervenir, sous astreinte de 300 euros par jour de retard, en troisième lieu, d'enjoindre au directeur général de l'Office français de l'immigration et de l'intégration de le rétablir dans ses droits au bénéfice des conditions matérielles d'accueil dans un délai de vingt-quatre heures à compter de la notification de l'ordonnance à intervenir, sous astreinte de 300 euros par jour de retard. Par une ordonnance n° 2116850 du 10 août 2021, le juge des référés du tribunal administratif de Paris l'a admis au bénéfice de l'aide juridictionnelle à titre provisoire et a rejeté le surplus de ses conclusions.<br/>
<br/>
              Par une requête, enregistrée le 20 août 2021 au secrétariat du contentieux du Conseil d'Etat, M. A... demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative :<br/>
<br/>
              1°) de l'admettre à titre provisoire au bénéfice de l'aide juridictionnelle ;<br/>
<br/>
              2°) d'annuler cette ordonnance ;<br/>
<br/>
              3°) de faire droit à ses conclusions de première instance ; <br/>
<br/>
              4°) de mettre à la charge de l'Etat le versement à son avocat de la somme de 1 500 euros au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991.<br/>
<br/>
<br/>
<br/>
              Il soutient que :<br/>
              - la condition d'urgence est satisfaite dès lors que, en premier lieu, il est exposé à une mesure d'éloignement qui peut être mise en œuvre à tout moment, en deuxième lieu, ne bénéficiant plus des conditions matérielles d'accueil, il se trouve en situation d'extrême précarité et, en dernier lieu, il fait l'objet d'une décision de transfert susceptible d'être exécutée d'office à tout moment ;<br/>
              - la décision attaquée porte atteinte au droit d'asile et à son corollaire, le droit de solliciter le statut de réfugié, une atteinte grave et manifestement illégale dès lors qu'il ne saurait être regardé comme ayant pris la fuite au sens de l'article 29 du règlement (UE) 604/2013 du 26 juin 2013 permettant une prolongation du délai de transfert, puisqu'il a honoré l'ensemble de ses convocations à la préfecture de police et n'a jamais tenté de se soustraire au contrôle des autorités administratives de manière intentionnelle, que la préfecture n'a pas apporté la preuve qu'il avait été valablement informé des conséquences de son refus de se soumettre à la réalisation d'un test PCR, dans une langue qu'il est supposé comprendre et, enfin, qu'il n'a pas non plus été informé de la signification des convocations à l'Hôtel Dieu, ces dernières lui ayant été remises en français et en anglais, langues qu'il ne comprend pas.<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
              Vu : <br/>
              - le règlement (UE) 604/2013 du Parlement européen et du Conseil du 26 juin 2013 ; <br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ; <br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - le code de justice administrative ; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. (...) ". En vertu de l'article L. 522-3 du même code, le juge des référés peut, par une ordonnance motivée, rejeter une requête sans instruction ni audience lorsque la condition d'urgence n'est pas remplie ou lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable ou qu'elle est mal fondée.<br/>
              2. Le droit constitutionnel d'asile, qui a le caractère d'une liberté fondamentale, a pour corollaire le droit de solliciter le statut de réfugié. S'il implique que l'étranger qui sollicite la reconnaissance de la qualité de réfugié soit, en principe, autorisé à demeurer sur le territoire jusqu'à ce qu'il ait été statué sur sa demande, ce droit s'exerce dans les conditions définies par les articles L. 541-1 et suivants du code de l'entrée et du séjour des étrangers et du droit d'asile. L'article L. 572-1 de ce code prévoit que l'étranger dont l'examen de la demande d'asile relève de la responsabilité d'un autre Etat peut faire l'objet d'un transfert vers l'Etat qui est responsable de cet examen en application des dispositions du règlement du Parlement européen et du Conseil du 26 juin 2013. <br/>
<br/>
              3. Aux termes de l'article 29 du règlement (UE) n° 604/2013 du Parlement européen et du Conseil du 26 juin 2013 : " 1. Le transfert du demandeur (...) de l'État membre requérant vers l'État membre responsable s'effectue conformément au droit national de l'État membre requérant, après concertation entre les États membres concernés, dès qu'il est matériellement possible et, au plus tard, dans un délai de six mois à compter de l'acceptation par un autre État membre de la requête aux fins de prise en charge ou de reprise en charge de la personne concernée ou de la décision définitive sur le recours ou la révision lorsque l'effet suspensif est accordé conformément à l'article 27 (...) 2. Si le transfert n'est pas exécuté dans le délai de six mois, l'État membre responsable est libéré de son obligation de prendre en charge ou de reprendre en charge la personne concernée et la responsabilité est alors transférée à l'État membre requérant. Ce délai peut être porté à un an au maximum s'il n'a pas pu être procédé au transfert en raison d'un emprisonnement de la personne concernée ou à dix-huit mois au maximum si la personne concernée prend la fuite (...) ". Il résulte clairement de ces dispositions que le transfert vers l'Etat membre responsable peut avoir lieu pendant une période de six mois à compter de l'acceptation de la demande de prise en charge et est susceptible d'être portée à dix-huit mois si l'intéressé " prend la fuite ", cette notion devant s'entendre comme visant le cas où un ressortissant étranger se serait soustrait de façon intentionnelle et systématique au contrôle de l'autorité administrative en vue de faire obstacle à une mesure d'éloignement le concernant. Tel est le cas notamment s'il se soustrait intentionnellement à l'exécution d'un transfert organisé en refusant un test PCR obligatoire pour l'entrée effective sur le territoire de l'Etat membre responsable, dès lors qu'il avait connaissance des conséquences d'un refus de sa part et qu'il ne fait état d'aucune raison médicale particulière justifiant une absence de consentement à la réalisation du test. <br/>
<br/>
              4. En l'espèce, il ressort des pièces du dossier que l'intéressé a été informé qu'il devait se rendre le 24 avril 2021 à l'hôpital de l'Hôtel-Dieu pour effectuer un test PCR puis se rendre à la préfecture de police le 26 avril muni du résultat de son test. N'ayant pas effectué de test, le vol à destination de la Belgique prévu le 27 avril suivant a été annulé. Il a été de nouveau informé qu'il devait se rendre le 21 juin 2021 au même hôpital pour réaliser le test et se rendre à la préfecture de police le 23 juin muni du résultat. En l'absence de test effectué, le vol prévu le 24 juin a également été annulé. S'il soutient qu'il n'a pas été informé dans une langue qu'il comprend, à savoir le pachto, des conséquences du refus de réaliser un test PCR, le juge des référés du tribunal administratif de Paris a estimé que M. A... ne pouvait raisonnablement ignorer qu'une opposition au test de sa part ferait échec à son transfert vers la Belgique, dès lors, d'une part, qu'il avait signé les deux convocations rédigées en langue française et en anglais lesquelles mentionnaient expressément l'obligation de se présenter aux rendez-vous de la préfecture muni " impérativement " du résultat du test PCR et, d'autre part, que le 1er vol prévu le 27 avril 2021 avait été annulé en l'absence de présentation de test PCR, que l'intéressé avait de nouveau été convoqué à un autre rendez-vous et que la convocation mentionnait l'obligation d'être muni d'un test PCR. Il en a déduit que M. A... devait être regardé comme s'étant soustrait de manière intentionnelle à l'exécution du transfert organisé, se mettant ainsi en situation de fuite au sens de l'article 29 du règlement du 26 juin 2013. Le requérant, dans ses écritures d'appel, se borne à reprendre son argumentation de première instance, sans mettre en cause utilement cette appréciation. Il n'est donc pas fondé à soutenir que c'est à tort que le juge des référés du tribunal administratif a rejeté sa demande.<br/>
<br/>
              5. Il résulte de ce qui précède que la requête de M. A... doit être rejetée selon la procédure prévue par l'article L. 522-3 du code de justice administrative, y compris ses conclusions tendant à l'application des dispositions des articles L. 761-1 du même code et 37 de la loi du 10 juillet 1991, sans qu'il y ait lieu de l'admettre au bénéfice de l'aide juridictionnelle provisoire.<br/>
<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de M. A... est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à M. B... A.... <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
