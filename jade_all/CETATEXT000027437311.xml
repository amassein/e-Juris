<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027437311</ID>
<ANCIEN_ID>JG_L_2013_05_000000356276</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/43/73/CETATEXT000027437311.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème et 8ème sous-sections réunies, 22/05/2013, 356276</TITRE>
<DATE_DEC>2013-05-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>356276</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème et 8ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BORE, SALVE DE BRUNETON ; SCP TIFFREAU-CORLAY-MARLANGE</AVOCATS>
<RAPPORTEUR>M. Fabrice Benkimoun</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Vincent Daumas</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:356276.20130522</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 30 janvier et 30 avril 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour Mme C...A..., demeurant... ; Mme A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n°10DA01211 du 29 novembre 2011 par lequel la cour administrative d'appel de Douai, après avoir annulé le jugement n°0103732 du 7 juillet 2007 du tribunal administratif d'Amiens, a rejeté sa demande tendant à l'annulation du titre exécutoire, émis à son encontre le 20 février 2001, pour le recouvrement de la somme de 602 410,18 francs (91 836,84 euros), ainsi que du commandement de payer du 27 juillet 2001 ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat et de la commune de Gricourt le versement d'une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code civil ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Fabrice Benkimoun, Maître des Requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Vincent Daumas, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée avant et après les conclusions à la SCP Boré, Salve de Bruneton, avocat de Mme A...et à la SCP Tiffreau-Corlay-Marlange, avocat de la commune de Gricourt ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort de pièces du dossier soumis aux juges du fond que la commune de Gricourt a émis, le 20 février 2001, un titre exécutoire à l'encontre de Mme A... en vue du recouvrement de la somme de 602 410,18 francs (91 836,84 euros) correspondant au montant des rémunérations indûment versées à l'intéressée, en l'absence de service fait, en qualité de secrétaire de mairie durant la période du 1er avril 1971 au 31 décembre 1989 ; que Mme A... se pourvoit en cassation contre l'arrêt du 29 novembre 2011 par lequel la cour administrative d'appel de Douai, sur renvoi après décision du Conseil d'Etat du 12 mars 2010, a rejeté ses conclusions tendant à l'annulation de ces actes ;<br/>
<br/>
              2. Considérant, en premier lieu, que la cour administrative d'appel a relevé que si, par un arrêt du 14 septembre 2000, la cour d'appel d'Amiens s'est prononcée sur le préjudice financier subi par la commune de Gricourt de 1990 à 1995, la somme en litige devant elle correspondait au montant des traitements perçus par Mme A... sans avoir accompli aucun service au cours de la période précédente, du 1er avril 1971 au 31 décembre 1989 ; qu'en jugeant que la détermination du préjudice financier subi par la commune de Gricourt au cours des années 1990 à 1995 par la cour d'appel d'Amiens était sans incidence sur l'obligation de payer mise à la charge de Mme A...au titre des rémunérations indument perçues au cours de la période précédente, la cour administrative d'appel de Douai n'a ni commis d'erreur de droit ni méconnu l'autorité de la chose jugée par l'arrêt du 14 septembre 2000 ;<br/>
<br/>
              3. Considérant, par ailleurs, qu'il ressort des pièces du dossier soumis aux juges du fond que, pour soutenir ses affirmations, selon lesquelles la commune avait bénéficié d'une contrepartie aux rémunérations qui lui avaient été versées de 1971 à 1989 du fait de l'activité de son époux, M.B..., Mme A...s'est bornée à se référer à l'appréciation de la cour d'appel d'Amiens, qui portait sur une période différente de celle qui faisait l'objet du titre exécutoire litigieux, et que Mme A...avait en outre admis dans le procès verbal de son audition du 11 juin 1996 n'avoir effectué " aucun travail " pour la commune de Gricourt pendant la période allant de 1970 à 1996 ; que, dès lors, la requérante n'est pas fondée à soutenir que la cour administrative d'appel de Douai aurait dénaturé les pièces du dossier qui lui était soumis en estimant qu'elle n'apportait aucun élément sérieux susceptible de remettre en cause le bien fondé des sommes réclamées par la commune ;<br/>
<br/>
              4. Considérant, en second lieu, que le point de départ de la prescription quinquennale prévue à l'article 2277 du code civil, dans sa rédaction en vigueur à la date de la décision attaquée, qui est applicable aux actions en restitution de rémunérations versées par une personne publique à un agent public, est la date à laquelle la créance devient exigible ; que, sauf disposition législative contraire, en cas de fraude ayant pour effet de maintenir la personne publique ou l'agent public titulaire d'un droit à paiement ou à restitution dans l'ignorance de celui-ci et de le priver de la possibilité de l'exercer, le délai de prescription ne commence à courir qu'à compter de la date à laquelle l'ignorance de ce droit a cessé  ; <br/>
<br/>
              5. Considérant qu'en relevant que, dans les circonstances particulières de l'affaire qui lui était soumise, marquées par des détournements de fonds au détriment de la commune, auxquels avaient participé certains de ses représentants légaux, le caractère indu des traitements versés à MmeA..., en l'absence de service fait, à compter de 1970, n'avait été révélé à la commune qu'au cours de l'enquête préliminaire ordonnée par le procureur de la République et en estimant que la commune devait ainsi être regardée comme ayant eu connaissance suffisante des faits au plus tôt par le compte-rendu d'enquête établi le 18 juin 1996 par le commandant de police chargé de l'enquête,  puis en déduisant de ces constatations que le délai de la prescription quinquennale n'avait pu commencer à courir qu'à compter du 18 juin 1996, la cour administrative d'appel de Douai, qui a suffisamment motivé son arrêt, n'a pas commis d'erreur de droit et n'a pas donné aux faits une qualification juridique erronée ;<br/>
<br/>
              6. Considérant qu'il résulte de tout ce qui précède que Mme A...n'est pas fondée à demander l'annulation de l'arrêt de la cour d'appel de Douai qu'elle attaque ; que ses conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative ne peuvent, par suite, qu'être rejetées ; qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de Mme A...la somme de 3.000 euros à verser à la commune de Gricourt, au titre des mêmes dispositions ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er: Le pourvoi de Mme A...est rejeté.<br/>
Article 2 : Mme A...versera à la commune de Gricourt une somme de 3.000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée à Mme C...A..., à la commune de Gricourt. <br/>
Copie en sera adressée, pour information, au ministre de l'économie et des finances.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">18-03-02 COMPTABILITÉ PUBLIQUE ET BUDGET. CRÉANCES DES COLLECTIVITÉS PUBLIQUES. RECOUVREMENT. - RESTITUTION DE RÉMUNÉRATIONS D'AGENTS PUBLICS - APPLICATION DE LA PRESCRIPTION QUINQUENNALE [RJ1] - POINT DE DÉPART - PRINCIPE - DATE D'EXIGIBILITÉ DE LA CRÉANCE - EXCEPTION - FRAUDE AYANT POUR EFFET DE MAINTENIR LE TITULAIRE DU DROIT À RESTITUTION DANS L'IGNORANCE DE CELUI-CI - DATE À LAQUELLE L'IGNORANCE CESSE [RJ2].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">36-08-01 FONCTIONNAIRES ET AGENTS PUBLICS. RÉMUNÉRATION. QUESTIONS D'ORDRE GÉNÉRAL. - RESTITUTION DE RÉMUNÉRATIONS D'AGENTS PUBLICS - APPLICATION DE LA PRESCRIPTION QUINQUENNALE [RJ1] - POINT DE DÉPART - PRINCIPE - DATE D'EXIGIBILITÉ DE LA CRÉANCE - EXCEPTION - FRAUDE AYANT POUR EFFET DE MAINTENIR LE TITULAIRE DU DROIT À PAIEMENT OU À RESTITUTION DANS L'IGNORANCE DE CELUI-CI - DATE À LAQUELLE L'IGNORANCE CESSE [RJ2].
</SCT>
<ANA ID="9A"> 18-03-02 Le point de départ de la prescription quinquennale prévue à l'article 2277 du code civil, dans sa rédaction antérieure à la loi n° 2008-561 du 17 juin 2008, qui est applicable aux actions en restitution de rémunérations versées par une personne publique à un agent public, est la date à laquelle la créance devient exigible. Sauf disposition législative contraire, en cas de fraude ayant pour effet de maintenir la personne publique ou l'agent public titulaire d'un droit à paiement ou à restitution dans l'ignorance de celui-ci et de le priver de la possibilité de l'exercer, le délai de prescription ne commence à courir qu'à compter de la date à laquelle l'ignorance de ce droit a cessé.</ANA>
<ANA ID="9B"> 36-08-01 Le point de départ de la prescription quinquennale prévue à l'article 2277 du code civil, dans sa rédaction antérieure à la loi n° 2008-561 du 17 juin 2008, qui est applicable aux actions en restitution de rémunérations versées par une personne publique à un agent public, est la date à laquelle la créance devient exigible. Sauf disposition législative contraire, en cas de fraude ayant pour effet de maintenir la personne publique ou l'agent public titulaire d'un droit à paiement ou à restitution dans l'ignorance de celui-ci et de le priver de la possibilité de l'exercer, le délai de prescription ne commence à courir qu'à compter de la date à laquelle l'ignorance de ce droit a cessé.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 12 mars 2010, Mme,, n° 309118, T. p. 822.,,[RJ2] Rappr. Cass. civ. 1è, 28 octobre 1991, n° 88-14.410, Bull. 1991 I, n° 282, p. 185 ; Cass. civ. 1è, 3 novembre 1993, n° 92-10.563, Bull. 1993 I, n° 312.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
