<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032928927</ID>
<ANCIEN_ID>JG_L_2016_07_000000400270</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/92/89/CETATEXT000032928927.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème chambre, 22/07/2016, 400270, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-07-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>400270</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème chambre</FORMATION>
<TYPE_REC>Rectif. d'erreur matérielle</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Thomas Odinot</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Gilles Pellissier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2016:400270.20160722</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A...B...a demandé au Conseil d'Etat d'annuler pour excès de pouvoir la décision implicite du garde des sceaux, ministre de la justice, refusant d'abroger les dispositions de l'article 48 du décret n° 91-1266 du 19 décembre 1991, en tant qu'elles ne prévoient pas que les décisions d'aide juridictionnelle mentionnent l'identité, la profession, la fonction et l'adresse administrative des personnes qui ont traité une demande d'aide juridictionnelle ou qui ont participé à une décision qui s'y rapporte. Par une décision n° 376048 du 10 mars 2016, le Conseil d'Etat statuant au contentieux a rejeté sa demande.<br/>
<br/>
              Par une requête, enregistrée le 31 mai 2016 au secrétariat du contentieux du Conseil d'Etat, M. B...demande au Conseil d'Etat de rectifier pour erreur matérielle la décision n° 376048 du 10 mars 2016 du Conseil d'Etat. <br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la Constitution, notamment son article 61-1 ;<br/>
              - l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
              - la loi n° 2000-321 du 12 avril 2000 ;<br/>
              - le décret n° 91-1266 du 19 décembre 1991 ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Thomas Odinot, auditeur,  <br/>
<br/>
              - les conclusions de M. Gilles Pellissier, rapporteur public ;<br/>
<br/>
<br/>
<br/>Sur la question prioritaire de constitutionnalité :<br/>
<br/>
              1. Considérant, d'une part, qu'aux termes du premier alinéa de l'article 23-5 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel : " Le moyen tiré de ce qu'une disposition législative porte atteinte aux droits et libertés garantis par la Constitution peut être soulevé (...) à l'occasion d'une instance devant le Conseil d'Etat (...) " ; qu'il résulte des dispositions de ce même article que le Conseil constitutionnel est saisi de la question prioritaire de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux ;<br/>
<br/>
              2. Considérant, d'autre part, qu'aux termes de l'article R. 833-1 du code de justice administrative : " Lorsqu'une décision d'une cour administrative d'appel ou du Conseil d'Etat est entachée d'une erreur matérielle susceptible d'avoir exercé une influence sur le jugement de l'affaire, la partie intéressée peut introduire devant la juridiction qui a rendu la décision un recours en rectification " ; <br/>
<br/>
              3. Considérant qu'il résulte de ces dispositions que le recours en rectification d'erreur matérielle n'est ouvert qu'en vue de corriger des erreurs de caractère matériel de la juridiction qui ne sont pas imputables aux parties et qui ont pu avoir une influence sur le sens de la décision ; que l'objet de ce recours à l'encontre d'une décision du Conseil d'Etat statuant au contentieux n'est pas de remettre en question l'appréciation d'ordre juridique portée par ce dernier sur les mérites de la cause qui lui était soumise ; que, dès lors, ne peuvent être regardées comme applicables au présent litige les dispositions de l'article 16-1 de la loi du 12 avril 2000 relative aux droits des citoyens dans leurs relations avec les administrations, aux termes desquelles : " L'autorité compétente est tenue, d'office ou à la demande d'une personne intéressée, d'abroger expressément tout règlement illégal ou sans objet, que cette situation existe depuis la publication du règlement ou qu'elle résulte de circonstances de droit ou de fait postérieures à cette date " ;<br/>
<br/>
              4. Considérant par suite que, sans qu'il soit besoin de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité invoquée, le moyen tiré de ce que l'article 16-1 de la loi du 12 avril 2000 porterait atteinte aux droits et libertés garantis par la Constitution ne peut qu'être écarté ;<br/>
<br/>
<br/>
<br/>
<br/>
              Sur les autres moyens présentés à l'appui de la demande de rectification pour erreur matérielle : <br/>
<br/>
              5. Considérant, en premier lieu, que le moyen tiré de ce que M. B...n'aurait pas été préalablement avisé du moyen d'ordre public retenu d'office par le Conseil d'Etat statuant au contentieux dans sa décision n° 376048 du 10 mars 2016 pour rejeter sa demande d'abrogation des dispositions de l'article 48 du décret du 19 décembre 1991, ne met pas en cause une erreur présentant un caractère matériel ; <br/>
<br/>
              6. Considérant, en second lieu, qu'en estimant par sa décision du 10 mars 2016 que le maintien en vigueur de ces dispositions du décret du 19 décembre 1991 était dépourvu de tout effet sur la situation de M. B...et en en déduisant que celui-ci ne justifiait dès lors pas d'un intérêt direct et certain à demander l'annulation du refus d'abroger ces dispositions, le Conseil d'Etat s'est livré à une appréciation d'ordre juridique que M. B...n'est pas recevable à remettre en cause par la voie d'une requête en rectification d'erreur matérielle ;<br/>
<br/>
              7. Considérant qu'il résulte de ce qui précède que le recours en rectification d'erreur matérielle de M. B...doit être rejeté ;<br/>
<br/>
              8. Considérant qu'aux termes de l'article R. 741-12 du code de justice administrative : " Le juge peut infliger à l'auteur d'une requête qu'il estime abusive une amende dont le montant ne peut excéder 3 000 euros " ; qu'en l'espèce, la requête de M. B...présente un caractère abusif ; qu'il y a lieu de condamner M. B...à payer une amende de 100 euros ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée par M.B....<br/>
Article 2 : La requête de M. B...est rejetée. <br/>
Article 3 : M. B...est condamné à payer une amende de 100 euros.<br/>
Article 4 : La présente décision sera notifiée à M. A...B..., au Premier ministre, au garde des sceaux, ministre de la justice, et au directeur régional des finances publiques d'Ile-de-France. <br/>
Copie en sera adressée au Conseil constitutionnel.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
