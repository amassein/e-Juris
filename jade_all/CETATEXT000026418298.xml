<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026418298</ID>
<ANCIEN_ID>JG_L_2012_09_000000331081</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/41/82/CETATEXT000026418298.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème et 4ème sous-sections réunies, 24/09/2012, 331081</TITRE>
<DATE_DEC>2012-09-24</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>331081</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème et 4ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Jacques Arrighi de Casanova</PRESIDENT>
<AVOCATS>SCP GATINEAU, FATTACCINI ; SCP RICHARD</AVOCATS>
<RAPPORTEUR>M. Jean-Dominique Langlais</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Fabienne Lambolez</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2012:331081.20120924</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 24 août et 24 novembre 2009 au secrétariat du contentieux du Conseil d'Etat, présentés pour l'Etablissement public de santé mentale de l'agglomération lilloise, dont le siège est BP 4 à Saint-André-lez-Lille (59871) ; l'établissement demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler le jugement n° 0606517 du 17 juin 2009 par lequel le tribunal administratif de Lille a annulé les décisions des 1er et 26 avril 2004 par lesquelles son directeur a refusé de faire droit à la demande de Mme Maryvonne A tendant à la reconnaissance de l'imputabilité au service de son état de santé ; <br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter la demande de Mme A ; <br/>
<br/>
              3°) de mettre à la charge de Mme A la somme de 4 500 euros au titre des dispositions de l'article L. 761-1 du  code de justice administrative ; <br/>
<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de la sécurité sociale ; <br/>
<br/>
              Vu la loi n° 86-33 du 9 janvier 1986 ; <br/>
<br/>
              Vu le décret n° 60-58 du 11 janvier 1960 ;  <br/>
<br/>
              Vu le décret n° 88-386 du 19 avril 1988 ;<br/>
<br/>
              Vu le décret n° 2003-1306 du 23 décembre 2003 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Dominique Langlais, Maître des Requêtes,  <br/>
<br/>
              - les observations de la SCP Gatineau, Fattaccini avocat de l'Etablissement public de santé mentale de l'agglomération lilloise et de la SCP Richard avocat de Mme A ;<br/>
<br/>
              - les conclusions de Mme Fabienne Lambolez, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Gatineau, Fattaccini avocat de l'Etablissement public de santé mentale de l'agglomération lilloise et à la SCP Richard avocat de Mme A ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant, d'une part, qu'aux termes de l'article 41 de la loi du 9 janvier 1986 : " Le fonctionnaire en activité a droit : (...) 2° A des congés de maladie dont la durée totale peut atteindre un an pendant une période de douze mois consécutifs en cas de maladie dûment constatée mettant l'intéressé dans l'impossibilité d'exercer ses fonctions. Celui-ci conserve alors l'intégralité de son traitement pendant une durée de trois mois ; ce traitement est réduit de moitié pendant les neuf mois suivants. Le fonctionnaire conserve, en outre, ses droits à la totalité du supplément familial de traitement et de l'indemnité de résidence. / Toutefois, si la maladie provient de l'une des causes exceptionnelles prévues à l'article L. 27 du code des pensions civiles et militaires de retraite ou d'un accident survenu dans l'exercice ou à l'occasion de l'exercice de ses fonctions, le fonctionnaire conserve l'intégralité de son traitement jusqu'à ce qu'il soit en état de reprendre son service ou jusqu'à sa mise à la retraite. Il a droit, en outre, au remboursement des honoraires médicaux et des frais directement entraînés par la maladie ou l'accident. / Dans le cas visé à l'alinéa précédent, l'imputation au service de la maladie ou de l'accident est appréciée par la commission de réforme instituée par le régime des pensions des agents des collectivités locales (...) " ; <br/>
<br/>
              2. Considérant, d'autre part, qu'aux termes du deuxième alinéa de l'article L. 461-1 du code de la sécurité sociale : " Est présumée d'origine professionnelle toute maladie désignée dans un tableau de maladies professionnelles et contractée dans les conditions mentionnées à ce tableau " ; qu'aux termes de l'article D. 461-24 du même code : " Conformément aux dispositions du deuxième alinéa de l'article L. 431-1 et des articles L. 432-1 et L. 461-1, la charge des prestations, indemnités et rentes incombe à la caisse d'assurance maladie ou à l'organisation spéciale de sécurité sociale à laquelle la victime est affiliée à la date de la première constatation médicale définie à l'article D. 461-7. Dans le cas où, à cette date, la victime n'est plus affiliée à une caisse primaire ou à une organisation spéciale couvrant les risques mentionnés au présent livre, les prestations et indemnités sont à la charge de la caisse ou de l'organisation spéciale à laquelle la victime a été affiliée en dernier lieu, quel que soit l'emploi alors occupé par elle " ; qu'il résulte enfin de l'article 11 du décret du 11 janvier 1960 relatif au régime de sécurité sociale des agents permanents des départements, des communes et de leurs établissements publics n'ayant pas le caractère industriel ou commercial, que les prestations en espèces versées au titre de la maladie ou de l'invalidité temporaire " sont liquidées et payées par les collectivités ou établissements dont relèvent les agents intéressés " ;<br/>
<br/>
              3. Considérant qu'il résulte de ces dernières dispositions que la charge des prestations financières afférentes à une maladie professionnelle contractée dans le cadre d'une activité dans le secteur privé antérieure à l'entrée de l'agent dans la fonction publique hospitalière peut incomber à l'établissement hospitalier qui l'emploie, en sa qualité de gestionnaire du régime spécial de sécurité sociale des agents hospitaliers, et sous le contrôle des tribunaux des affaires de sécurité sociale, compétents en vertu de l'article L. 142-1 du code de la sécurité sociale dès lors que sont en cause des prestations de sécurité sociale ; que, par ailleurs, en cas de maladie contractée dans l'exercice des fonctions hospitalières, l'établissement doit accorder à l'intéressé, sous le contrôle du juge administratif, le bénéfice des avantages prévus par les dispositions précitées de l'article 41 de la loi du 9 janvier 1986 ; <br/>
<br/>
              4. Considérant qu'il ressort des pièces du dossier soumis au tribunal administratif de Lille que Mme A a saisi l'Etablissement public de santé mentale de l'agglomération lilloise afin que les affections des voies respiratoires dont elle souffre soient reconnues comme une " maladie professionnelle " ; que cette demande tendait au bénéfice des avantages prévus par les dispositions du 2° de l'article 41 de la loi du 9 janvier 1986 ; que, saisi d'une telle demande, l'établissement devait se borner à déterminer si la maladie avait été contractée dans l'exercice des fonctions hospitalières de l'agent ; qu'en jugeant qu'il lui appartenait, eu égard aux dispositions de l'article D. 461-24 du code de la sécurité sociale, de rechercher si les fonctions antérieurement exercées par Mme A étaient à l'origine de sa pathologie, le tribunal a commis une erreur de droit ; que par suite, et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, son jugement doit être annulé ; <br/>
<br/>
              5. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              6. Considérant qu'en adressant les 1er et 26 avril 2004 à Mme A des courriers l'informant des avis défavorables de la commission de réforme, le directeur de l'Etablissement public de santé mentale de l'agglomération lilloise ne s'est pas borné à transmettre ces avis mais a pris des décisions refusant de reconnaître l'imputabilité au service de l'affection dont l'intéressée est atteinte ; que, contrairement à ce que soutient l'établissement, ces décisions font grief et sont susceptibles de recours ; <br/>
<br/>
              7. Considérant qu'il résulte des termes de ces courriers que le directeur de l'établissement, auquel il appartenait en application de l'article 6 de la loi du 9 janvier 1986 de se prononcer sur l'imputabilité au service de la maladie de Mme A, s'est cru lié par les avis de la commission de réforme, lesquels n'ont qu'un caractère consultatif, et n'a pas exercé les compétences qu'il tient de la loi ; que ses décisions doivent, par suite, et sans qu'il soit besoin d'examiner les autres moyens de la demande, être annulées ; <br/>
<br/>
              8. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à la charge de Mme A, qui n'est pas, dans la présente instance, la partie perdante ; qu'il y a  lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etablissement public de santé mentale de l'agglomération lilloise une somme de 3 000 euros à verser à Mme A au titre des frais exposés par elle et non compris dans les dépens ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du 17 juin 2009 du tribunal administratif de Lille est annulé.<br/>
<br/>
Article 2 : Les décisions des 1er et 26 avril 2004 du directeur de l'Etablissement public de santé mentale de l'agglomération lilloise sont annulées.<br/>
<br/>
Article 3 : L'Etablissement public de santé mentale de l'agglomération lilloise versera à Mme A la somme de 3 000 euros en application de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
Article 4 : Les conclusions présentées par l'Etablissement public de santé mentale de l'agglomération lilloise au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 5 : La présente décision sera notifiée à l'Etablissement public de santé mentale de l'agglomération lilloise et à Mme Maryvonne A.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">36-11 FONCTIONNAIRES ET AGENTS PUBLICS. DISPOSITIONS PROPRES AUX PERSONNELS HOSPITALIERS. - CHARGE DES PRESTATIONS FINANCIÈRES AFFÉRENTES À UNE MALADIE PROFESSIONNELLE - CHARGES PESANT SUR L'ÉTABLISSEMENT PUBLIC HOSPITALIER - 1) MALADIE CONTRACTÉE DANS LE SECTEUR PRIVÉ AVANT L'ENTRÉE DANS LA FONCTION PUBLIQUE HOSPITALIÈRE - CHARGE POUVANT INCOMBER À L'ÉTABLISSEMENT PUBLIC HOSPITALIER EMPLOYEUR EN SA QUALITÉ DE GESTIONNAIRE DU RÉGIME SPÉCIAL DE SÉCURITÉ SOCIALE DES AGENTS HOSPITALIERS, SOUS LE CONTRÔLE DU TASS - 2) MALADIE CONTRACTÉE DANS L'EXERCICE DES FONCTIONS HOSPITALIÈRES - CHARGE DES AVANTAGES PRÉVUS À L'ARTICLE 41 DE LA LOI DU 9 JANVIER 1986, SOUS LE CONTRÔLE DU JUGE ADMINISTRATIF.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">62-04-05 SÉCURITÉ SOCIALE. PRESTATIONS. PRESTATIONS D'ASSURANCES ACCIDENTS DU TRAVAIL ET MALADIES PROFESSIONNELLES. - CHARGE DES PRESTATIONS FINANCIÈRES AFFÉRENTES À UNE MALADIE PROFESSIONNELLE - AGENTS DE LA FONCTION PUBLIQUE HOSPITALIÈRE - CHARGES PESANT SUR L'ÉTABLISSEMENT PUBLIC HOSPITALIER - 1) MALADIE CONTRACTÉE DANS LE SECTEUR PRIVÉ AVANT L'ENTRÉE DANS LA FONCTION PUBLIQUE HOSPITALIÈRE - CHARGE POUVANT INCOMBER À L'ÉTABLISSEMENT PUBLIC HOSPITALIER EMPLOYEUR EN SA QUALITÉ DE GESTIONNAIRE DU RÉGIME SPÉCIAL DE SÉCURITÉ SOCIALE DES AGENTS HOSPITALIERS, SOUS LE CONTRÔLE DU TASS - 2) MALADIE CONTRACTÉE DANS L'EXERCICE DES FONCTIONS HOSPITALIÈRES - CHARGE DES AVANTAGES PRÉVUS À L'ARTICLE 41 DE LA LOI DU 9 JANVIER 1986, SOUS LE CONTRÔLE DU JUGE ADMINISTRATIF.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">62-05-01-03 SÉCURITÉ SOCIALE. CONTENTIEUX ET RÈGLES DE PROCÉDURE CONTENTIEUSE SPÉCIALES. RÈGLES DE COMPÉTENCE. COMPÉTENCE DES JURIDICTIONS DE SÉCURITÉ SOCIALE. - CHARGE DES PRESTATIONS FINANCIÈRES AFFÉRENTES À UNE MALADIE PROFESSIONNELLE - MALADIE CONTRACTÉE DANS LE SECTEUR PRIVÉ AVANT L'ENTRÉE DANS LA FONCTION PUBLIQUE HOSPITALIÈRE - CHARGE POUVANT INCOMBER À L'ÉTABLISSEMENT PUBLIC HOSPITALIER EMPLOYEUR EN SA QUALITÉ DE GESTIONNAIRE DU RÉGIME SPÉCIAL DE SÉCURITÉ SOCIALE DES AGENTS HOSPITALIERS - COMPÉTENCE DES TASS.
</SCT>
<ANA ID="9A"> 36-11 1) La charge des prestations financières afférentes à une maladie professionnelle contractée dans le cadre d'une activité dans le secteur privé antérieure à l'entrée de l'agent dans la fonction publique hospitalière peut incomber à l'établissement public hospitalier qui l'emploie, en sa qualité de gestionnaire du régime spécial de sécurité sociale des agents hospitaliers, sous le contrôle des tribunaux des affaires de sécurité sociale (TASS), compétents en vertu de l'article L. 142-1 du code de la sécurité sociale dès lors que sont en cause des prestations de sécurité sociale. 2) En cas de maladie contractée dans l'exercice des fonctions hospitalières, l'établissement doit accorder à l'intéressé, sous le contrôle du juge administratif, le bénéfice des avantages prévus par l'article 41 de la loi n° 86-33 du 9 janvier 1986.</ANA>
<ANA ID="9B"> 62-04-05 1) La charge des prestations financières afférentes à une maladie professionnelle contractée dans le cadre d'une activité dans le secteur privé antérieure à l'entrée de l'agent dans la fonction publique hospitalière peut incomber à l'établissement public hospitalier qui l'emploie, en sa qualité de gestionnaire du régime spécial de sécurité sociale des agents hospitaliers, sous le contrôle des tribunaux des affaires de sécurité sociale (TASS), compétents en vertu de l'article L. 142-1 du code de la sécurité sociale dès lors que sont en cause des prestations de sécurité sociale. 2) En cas de maladie contractée dans l'exercice des fonctions hospitalières, l'établissement doit accorder à l'intéressé, sous le contrôle du juge administratif, le bénéfice des avantages prévus par l'article 41 de la loi n° 86-33 du 9 janvier 1986.</ANA>
<ANA ID="9C"> 62-05-01-03 La charge des prestations financières afférentes à une maladie professionnelle contractée dans le cadre d'une activité dans le secteur privé antérieure à l'entrée de l'agent dans la fonction publique hospitalière peut incomber à l'établissement public hospitalier qui l'emploie, en sa qualité de gestionnaire du régime spécial de sécurité sociale des agents hospitaliers, sous le contrôle des tribunaux des affaires de sécurité sociale (TASS), compétents en vertu de l'article L. 142-1 du code de la sécurité sociale dès lors que sont en cause des prestations de sécurité sociale.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
