<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031861082</ID>
<ANCIEN_ID>JG_L_2015_12_000000354603</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/86/10/CETATEXT000031861082.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère SSJS, 30/12/2015, 354603, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-12-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>354603</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère SSJS</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Yannick Faure</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Jean Lessi</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2015:354603.20151230</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              Par une décision du 26 février 2014, le Conseil d'Etat, statuant au contentieux sur la requête de la Fédération des entreprises du commerce et de la distribution (FCD) et de la Fédération des magasins de bricolage et de l'aménagement de la maison (FMB) tendant à l'annulation pour excès de pouvoir de la décision du 11 octobre 2011 du ministre de l'écologie, du développement durable, des transports et du logement rejetant leur recours gracieux du 5 août 2011 tendant au retrait de l'" Avis aux opérateurs économiques sur l'obligation de communiquer des informations sur les substances contenues dans les articles en application des articles 7.2 et 33 du règlement n° 1907/2006 (Reach) - Interprétation du seuil de 0.1 % (masse/masse) cité aux articles 7.2 et 33 ", publié au Journal officiel de la République française du 8 juin 2011, a sursis à statuer jusqu'à ce que la Cour de justice de l'Union européenne se soit prononcée sur la question de savoir si les obligations résultant du 2 de l'article 7 et de l'article 33 du règlement n° 1907/2006 (Reach) s'appliquent, lorsqu'un " article " au sens de ce règlement est composé de plusieurs éléments répondant eux-mêmes à la définition de l'" article " qu'il donne, seulement à l'égard de l'article assemblé ou à l'égard de chacun des éléments qui répondent à la définition de l'" article ".<br/>
<br/>
              Par un arrêt C-106/14 du 10 septembre 2015, la Cour de justice de l'Union européenne s'est prononcée sur cette question.<br/>
<br/>
              Vu les autres pièces du dossier, y compris celles visées par la décision du Conseil d'Etat statuant au contentieux du 26 février 2014 ;<br/>
<br/>
              Vu :<br/>
              - la Constitution, notamment son Préambule ;<br/>
              - le traité sur le fonctionnement de l'Union européenne ;<br/>
              - le règlement (CE) n° 1907/2006 du Parlement européen et du Conseil du 18 décembre 2006 ;<br/>
              - le règlement (UE) n° 366/2011 de la Commission du 14 avril 2011 ;<br/>
              - le code de l'environnement ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Yannick Faure, auditeur,  <br/>
<br/>
              - les conclusions de M. Jean Lessi, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes du 2 de l'article 7 du règlement n° 1907/2006 du Parlement européen et du Conseil du 18 décembre 2006 concernant l'enregistrement, l'évaluation et l'autorisation des substances chimiques, ainsi que les restrictions applicables à ces substances (REACH) : " Tout producteur ou importateur d'articles notifie à l'Agence conformément au paragraphe 4 du présent article, si une substance répond aux critères énoncés à l'article 57 et est identifiée conformément à l'article 59, paragraphe 1, si les deux conditions suivantes sont remplies : / a) la substance est présente dans ces articles dans des quantités supérieures au total à 1 tonne par producteur ou importateur par an ; / b) la substance est présente dans ces articles dans une concentration supérieure à 0,1 % masse/masse (w/w) ". Aux termes de l'article 33 du même règlement : " 1. Tout fournisseur d'un article contenant une substance répondant aux critères énoncés à l'article 57 et identifiée conformément à l'article 59, paragraphe 1, avec une concentration supérieure à 0,1 % masse/masse (w/w), fournit au destinataire de l'article des informations suffisantes dont il dispose pour permettre l'utilisation dudit article en toute sécurité et comprenant, au moins, le nom de la substance. / 2. Sur demande d'un consommateur, tout fournisseur d'un article contenant une substance répondant aux critères énoncés à l'article 57 et identifiée conformément à l'article 59, paragraphe 1, avec une concentration supérieure à 0,1 % masse/masse (w/w), fournit au consommateur des informations suffisantes dont il dispose pour permettre l'utilisation dudit article en toute sécurité et comprenant, au moins, le nom de la substance. / Les informations pertinentes sont fournies, gratuitement, dans les quarante-cinq jours qui suivent la réception de la demande ". L'article 3 point 3 du règlement définit l'article comme " un objet auquel sont donnés, au cours du processus de fabrication, une forme, une surface ou un dessin particuliers qui sont plus déterminants pour sa fonction que sa composition chimique ".<br/>
<br/>
              2. Dans l'arrêt du 10 septembre 2015 par lequel elle s'est prononcée sur la question dont le Conseil d'Etat, statuant au contentieux, l'avait saisie à titre préjudiciel, la Cour de justice de l'Union européenne a dit pour droit, d'une part, que l'article 7, paragraphe 2, du règlement n° 1907/2006 du Parlement européen et du Conseil du 18 décembre 2006, tel que modifié par le règlement n° 366/2011 de la Commission du 14 avril 2011, doit être interprété en ce sens que, aux fins de l'application de cette disposition, il appartient au producteur de déterminer si une substance extrêmement préoccupante identifiée conformément à l'article 59, paragraphe 1, de ce règlement, tel que modifié, est présente dans une concentration supérieure à 0,1 % masse/masse de tout article qu'il produit et, à l'importateur d'un produit composé de plusieurs articles, de déterminer pour chaque article si une telle substance est présente dans une concentration supérieure à 0,1 % masse/masse de cet article. Elle a dit pour droit, d'autre part, que l'article 33 du même règlement n° 1907/2006, tel que modifié, doit être interprété en ce sens que, aux fins de l'application de cette disposition, il appartient au fournisseur d'un produit, dont l'un ou plusieurs des articles qui le composent contiennent une substance extrêmement préoccupante identifiée conformément à l'article 59, paragraphe 1, de ce règlement dans une concentration supérieure à 0,1 % masse/masse par article, d'informer le destinataire et, sur demande, le consommateur sur la présence de cette substance en leur communiquant, à tout le moins, le nom de la substance en cause. En outre, elle a précisé au point 54 de son arrêt que " la qualification d'article reste applicable à tout objet répondant aux critères de l'article 3, point 3 du règlement REACH qui entre dans la composition d'un produit complexe, à moins que, à la suite du processus de fabrication, cet objet devienne un déchet ou perde la forme, la surface ou le dessin qui contribue plus à déterminer sa fonction que sa composition chimique ".<br/>
<br/>
              3. Aux termes de l'avis dont les fédérations requérantes demandent l'annulation pour excès de pouvoir : " (...) les Autorités françaises informent par le présent avis les opérateurs économiques de l'interprétation adoptée en France en vue de l'application des articles 7.2 et 33 du règlement Reach. Elles précisent que la notion d'article s'entend comme chaque objet répondant à la définition d'article au sens de Reach, c'est-à-dire " auquel sont donnés, au cours du processus de fabrication, une forme, une surface ou un dessin particuliers qui sont plus déterminants pour sa fonction que sa composition chimique " (article 3.3). Ainsi, un article peut être composé d'un ou plusieurs objets répondant à la définition d'article, et les dispositions prévues par les articles 7.2 et 33 s'appliquent alors à chacun d'eux (...) ".<br/>
<br/>
              4. En premier lieu, il résulte de ce qui précède qu'en précisant ainsi, pour l'application du règlement n° 1907/2006 du Parlement européen et du Conseil du 18 décembre 2006, qu'un produit peut être composé d'un ou plusieurs articles, au sens de l'article 3 de ce règlement, auxquels s'appliquent alors les dispositions des articles 7 et 33 de ce même règlement, le ministre de l'écologie, du développement durable, des transports et du logement n'a pas méconnu le sens ou la portée des dispositions du règlement qu'il entendait expliciter. La circonstance que le guide élaboré par l'Agence européenne des produits chimiques, dont l'interprétation est, ainsi que l'a jugé la Cour de justice de l'Union européenne, dépourvue de portée normative, retienne une interprétation différente des articles 7 et 33 de ce même règlement est à cet égard sans incidence. Par ailleurs, si le ministre n'a pas précisé à quelles conditions, d'une part, un produit complexe peut lui-même être qualifié d'article et, d'autre part, un article entrant dans la composition d'un produit complexe perd sa qualification d'article, il n'a pas pour autant méconnu les dispositions des articles 7 et 33 du règlement. <br/>
<br/>
              5. En deuxième lieu, s'il est vrai que l'avis attaqué ne distingue pas l'obligation de notification mise à la charge du producteur d'un produit composé de plusieurs articles de celle mise à la charge de l'importateur d'un tel produit, le ministre qui en est l'auteur n'a pas entendu se prononcer sur ce point mais seulement, ainsi que l'indique d'ailleurs le titre de l'avis, interpréter le " seuil de 0,1 % (masse/masse) cité aux articles 7.2 et 33 ". Il en est de même de la notion de fournisseur d'un article et des informations dont la communication lui incombe, pour l'application de l'obligation d'information du destinataire instituée par l'article 33 du règlement n° 1907/2006 du Parlement européen et du Conseil du 18 décembre 2006. Contrairement à ce que soutiennent les fédérations requérantes, il ne résulte pas de l'absence de précisions de l'avis sur ces points que le ministre aurait donné une interprétation erronée des dispositions des articles 7 et 33 du règlement n° 1907/2006.  <br/>
<br/>
              6. En troisième lieu, ainsi que le Conseil d'Etat, statuant au contentieux, l'a relevé dans sa décision du 26 février 2014, le ministre de l'écologie, du développement durable, des transports et du logement a entendu, par l'avis attaqué, préciser à l'attention des opérateurs économiques les conditions d'application du règlement n° 1907/2006. Dès lors qu'il résulte de ce qui vient d'être dit que ce ministre n'a fixé aucune règle nouvelle mais s'est borné, par cet avis, à donner aux opérateurs économiques l'interprétation qu'il convenait de faire de celles des dispositions du règlement qu'il entendait expliciter, les fédérations requérantes ne sont pas fondées à soutenir que l'avis serait entaché d'incompétence faute d'avoir été adopté conjointement par les autres ministres, notamment le ministre chargé de l'économie, ayant compétence pour assurer l'exécution du règlement n° 1907/2006.   <br/>
<br/>
              7. En dernier lieu, l'avis ne fixant, ainsi qu'il a été dit, aucune règle nouvelle et n'adoptant aucune mesure mais se bornant à interpréter certaines des dispositions du règlement n° 1907/2006 sans en méconnaître le sens ou la portée, les fédérations requérantes ne sont pas fondées à soutenir qu'il méconnaîtrait les stipulations de l'article 288 du traité sur le fonctionnement de l'Union européenne, en vertu desquelles les règlements sont directement applicables dans tout Etat membre, qu'il aurait été adopté en méconnaissance des procédures prévues, d'une part, par les articles 128 et 129 du règlement n° 1907/2006 et, d'autre part, par l'article 133 de ce règlement, qu'il introduirait une distorsion de concurrence entre les opérateurs d'un même marché ou qu'il reposerait sur une erreur manifeste d'appréciation. De même, l'avis attaqué n'ayant par lui-même aucune incidence sur l'environnement, les fédérations requérantes ne sauraient utilement se prévaloir des dispositions de l'article 7 de la Charte de l'environnement et de l'article L. 110-1 du code de l'environnement. Enfin, dès lors que l'avis ne fixe aucune règle nouvelle, le moyen tiré de la méconnaissance du principe de sécurité juridique, faute de mesures transitoires, ne peut qu'être écarté.<br/>
<br/>
              8. Il résulte de tout ce qui précède que la Fédération des entreprises du commerce et de la distribution et la Fédération des magasins de bricolage et de l'aménagement de la maison ne sont pas fondées à demander l'annulation de l'avis qu'elles attaquent.<br/>
<br/>
              9. Par suite, les conclusions qu'elles présentent au titre de l'article L. 761-1 du code de justice administrative ne peuvent qu'être également rejetées.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de la Fédération des entreprises du commerce et de la distribution et de la Fédération des magasins de bricolage et de l'aménagement de la maison est rejetée.<br/>
Article 2 : La présente décision sera notifiée à la Fédération des entreprises du commerce et de la distribution, à la Fédération des magasins de bricolage et de l'aménagement de la maison et à la ministre de l'écologie, du développement durable et de l'énergie.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
