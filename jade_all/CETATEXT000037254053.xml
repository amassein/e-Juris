<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037254053</ID>
<ANCIEN_ID>JG_L_2018_07_000000419058</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/25/40/CETATEXT000037254053.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème chambre, 26/07/2018, 419058, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-07-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>419058</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Dominique Chelle </RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Cécile Barrois de Sarigny</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:419058.20180726</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A...B...a demandé au tribunal administratif de Versailles d'annuler les décisions par lesquelles le ministre de l'intérieur a retiré des points de son permis de conduire à la suite d'infractions commises les 30 mars 2007, 8 septembre 2008, 13 mai 2009, 6 mai 2011, 4 août 2012 et 13 avril 2014. Par un jugement n° 1502471 du 16 janvier 2018, le tribunal administratif a fait droit à sa demande.<br/>
<br/>
              Par un pourvoi, enregistré le 16 mars 2018 au secrétariat du contentieux du Conseil d'Etat, le ministre d'Etat, ministre de l'intérieur demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler ce jugement ; <br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter la demande de M.B....<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de la route ; <br/>
              - la loi n° 79- 587 du 11 juillet 1979 ; <br/>
              - le code des relations entre le public et l'administration ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Dominique Chelle, conseiller d'Etat,  <br/>
<br/>
- les conclusions de Mme Cécile Barrois de Sarigny, rapporteur public.<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis au juge du fond que M. B...a demandé au tribunal administratif de Versailles d'annuler des décisions de retrait de points de son permis de conduire consécutives à des infractions commises les 30 mars 2007, 8 septembre 2008, 13 mai 2009, 6 mai 2011, 4 août 2012 et 13 avril 2014 ; que le ministre de l'intérieur se pourvoit en cassation contre le jugement du 16 janvier 2018 par lequel le tribunal administratif a fait droit à sa demande au motif que, l'administration n'ayant pas produit les décisions attaquées, dont le demandeur lui avait vainement demandé la communication, elle n'établissait pas avoir satisfait à l'obligation de motivation prévue par l'article 1er de la loi du 11 juillet 1979 relative à la motivation des actes administratifs et à l'amélioration des relations entre l'administration et le public, codifié à l'article L. 211-2 du code des relations entre le public et l'administration ; <br/>
<br/>
              2. Considérant qu'aux termes de l'article L. 225-1 du code de la route : " I.- Il est procédé, dans les services de l'Etat et sous l'autorité et le contrôle du ministre de l'intérieur, à l'enregistrement : / 1° De toutes informations relatives aux permis de conduire dont la délivrance est sollicitée ou qui sont délivrés en application du présent code, ainsi qu'aux permis de conduire délivrés par les autorités étrangères et reconnus valables sur le territoire national ; / 2° De toutes décisions administratives dûment notifiées portant restriction de validité, retrait, suspension, annulation et restriction de délivrance du permis de conduire, ainsi que des avertissements prévus par le présent code ; / 3° De toutes mesures de retrait du droit de faire usage du permis de conduire qui seraient communiquées par les autorités compétentes des territoires et collectivités territoriales d'outre-mer ; / 4° De toutes mesures de retrait du droit de faire usage du permis de conduire prises par une autorité étrangère et communiquées aux autorités françaises conformément aux accords internationaux en vigueur ; / 5° Des procès-verbaux des infractions entraînant retrait de points et ayant donné lieu au paiement d'une amende forfaitaire ou à l'émission d'un titre exécutoire de l'amende forfaitaire majorée ; / 6° De toutes décisions judiciaires à caractère définitif en tant qu'elles portent restriction de validité, suspension, annulation et interdiction de délivrance du permis de conduire, ou qu'elles emportent réduction du nombre de points du permis de conduire ainsi que de l'exécution d'une composition pénale ; / 7° De toute modification du nombre de points affectant un permis de conduire dans les conditions définies aux articles L. 223-1 à L. 223-8. / II.- Ces informations peuvent faire l'objet de traitements automatisés, soumis aux dispositions de la loi n° 78-17 du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés " ; <br/>
<br/>
              3. Considérant que la gestion du décompte des points retirés ou réattribués aux permis de conduire est assurée, sur le fondement des dispositions précitées de l'article L. 225-1 du code de la route, par un traitement automatisé d'informations à caractère nominatif dénommé " Système national des permis de conduire " (SNPC) ; que ce traitement transmet une fois par semaine, de manière groupée, les données relatives aux retraits de points qu'il enregistre à l'Imprimerie nationale, afin qu'elle procède de manière automatisée à la mise en forme, à l'impression et à l'expédition des décisions correspondantes, qui sont datées du jour de leur édition et revêtues du fac-similé de la signature du fonctionnaire habilité à cette date à les signer au nom du ministre de l'intérieur ; qu'au terme de ces opérations, l'Imprimerie nationale, qui ne figure pas parmi les autorités que l'article L. 225-4 du code de la route habilite à accéder aux informations énumérées à l'article L. 225-1 précité, efface les fichiers informatiques utilisés pour éditer les décisions ; qu'il en résulte que le ministre de l'intérieur n'est pas en mesure de fournir une copie conforme d'une décision de retrait de points et peut seulement communiquer à l'intéressé le relevé intégral d'information relatif à son permis de conduire, prévu à l'article L. 225-3 du code de la route, où figurent les informations relatives à ce retrait qui ont été transmises à l'Imprimerie nationale, notamment la date, le lieu et la qualification pénale de l'infraction ainsi que l'événement qui en a établi la réalité ; <br/>
<br/>
              4. Considérant qu'il résulte de ce qui a été dit ci-dessus que l'organisation actuelle du SNPC ne met pas l'administration en mesure d'éditer des copies des décisions de retrait de points telles qu'elles ont été établies et envoyées aux intéressés ; que, toutefois, en raison des garanties qui entourent l'enregistrement et l'édition de ces décisions, la mention d'un retrait sur le relevé intégral établit que l'Imprimerie nationale a édité une décision conforme à un modèle où figure un rappel des dispositions relatives au retrait de points du permis de conduire, en portant dans les emplacements prévus à cet effet des mentions, identiques à celles qui figurent sur le relevé intégral, relatives à la date, à l'heure et au lieu de l'infraction motivant le retrait ainsi qu'à l'événement qui a établi la réalité de cette infraction ; que la décision énonce ainsi les considérations de droit et de fait sur lesquelles elle repose et satisfait par suite à l'obligation de motivation prescrite par les articles 1er et 3 de la loi du 11 juillet 1979, codifiés aux articles L. 211-2 et L. 211-5 du code des relations entre le public et l'administration ; qu'il suit de là qu'en se fondant, pour juger que les décisions de retrait de points contestées par M. B...n'étaient pas motivées, sur la circonstance que le ministre de l'intérieur ne les avait pas produites, alors qu'il ressortait des pièces du dossier qui lui était soumis que ces décisions étaient mentionnées au relevé d'information intégral relatif au permis de conduire de l'intéressé, le tribunal administratif a commis une erreur de droit ; que, dès lors, son jugement doit être annulé ; <br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du 16 janvier 2018 du tribunal administratif de Versailles est annulé.<br/>
<br/>
		Article 2 : L'affaire est renvoyée au tribunal administratif de Versailles.<br/>
<br/>
Article 3 : La présente décision sera notifiée au ministre d'Etat, ministre de l'intérieur et à M. A... B.... <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
