<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031309634</ID>
<ANCIEN_ID>JG_L_2015_10_000000390479</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/30/96/CETATEXT000031309634.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème / 7ème SSR, 09/10/2015, 390479</TITRE>
<DATE_DEC>2015-10-09</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>390479</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème / 7ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Sophie-Caroline de Margerie</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Béatrice Bourgeois-Machureau</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:390479.20151009</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, enregistrée au secrétariat du contentieux du Conseil d'Etat le 28 mai 2015, M. A...B...demande au Conseil d'Etat d'annuler pour excès de pouvoir le décret du 17 mars 2015 par lequel le Premier ministre a accordé son extradition aux autorités russes.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - la convention européenne d'extradition du 13 décembre 1957 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Sophie-Caroline de Margerie, conseiller d'Etat,<br/>
<br/>
              - les conclusions de Mme Béatrice Bourgeois-Machureau, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1.	Considérant que, par le décret attaqué du 17 mars 2015, le Premier ministre a accordé aux autorités de la Fédération de Russie l'extradition de M. A...B..., ressortissant russe, aux fins de poursuites pour faits de vol par un groupe organisé et tentative de vol par un groupe organisé, en exécution de deux ordonnances décernées à Goriatchiy Klutch les 7 décembre 2011 et le 23 janvier 2012 ;<br/>
<br/>
              2.	Considérant qu'il n'appartient pas aux autorités françaises, sauf en cas d'erreur évidente, de statuer sur le bien-fondé des charges retenues contre la personne recherchée ; qu'en l'espèce, il ne ressort pas des pièces du dossier qu'une erreur évidente aurait été commise en ce qui concerne la participation de M. B... aux faits qui lui sont reprochés ; que la circonstance que l'ordonnance du 23 janvier 2012 mentionne que les informations données par le requérant permettront de déterminer le rôle d'autres personnes poursuivies dans le cadre de la même procédure, ne signifie pas qu'aucune charge ne serait retenue à son encontre ;<br/>
<br/>
              3.	Considérant que si M. B...soutient que la demande d'extradition a été présentée par les autorités russes à des fins politiques visant certains dirigeants locaux, il ne ressort des pièces du dossier ni que les faits qui lui sont reprochés constitueraient une infraction politique par nature, ni que les autorités russes auraient formé leur demande dans un but autre que la poursuite d'infractions de droit commun ;<br/>
<br/>
              4.	Considérant qu'aux termes du premier alinéa de l'article 1er des réserves émises par la France lors de la ratification de la convention européenne d'extradition du 13 décembre 1957: " L'extradition ne sera pas accordée lorsque la personne réclamée serait jugée dans l'Etat requérant par un tribunal n'assurant pas les garanties fondamentales de procédure et de protection des droits de la défense... " ; que si M. B...soutient que la procédure devant les juridictions russes n'assurerait pas ces garanties, il se borne à formuler des allégations de caractère très général, sans apporter aucun élément circonstancié à l'appui de ses dires ; qu'au demeurant, il ressort des pièces du dossier que les autorités russes se sont engagées, dans le cadre de la présente procédure d'extradition, à ce que M. B...bénéficie d'un procès équitable, incluant notamment l'assistance d'un avocat ; <br/>
<br/>
              5.	Considérant qu'aux termes du second alinéa du même article 1er des réserves émises par la France : " L'extradition pourra être refusée si la remise est susceptible d'avoir des conséquences d'une gravité exceptionnelle pour la personne réclamée " ; que si M. B... soutient qu'en cas d'exécution du décret attaqué, sa vie familiale en France sera affectée, une telle décision trouve, en principe, sa justification dans la nature même de la procédure d'extradition, qui est de permettre, dans l'intérêt de l'ordre public et sous les conditions fixées par les dispositions qui la régissent, le jugement de personnes se trouvant en France qui sont poursuivies à l'étranger pour des crimes ou des délits commis hors de France ; qu'ainsi, en autorisant l'extradition de l'intéressé vers la Fédération de Russie, le Premier ministre n'a pas commis d'erreur manifeste au regard des exigences résultant du second alinéa des réserves émises par la France à la ratification de la convention européenne d'extradition ;<br/>
<br/>
              6.	Considérant, enfin, que si M. B...soutient que les personnes d'origine arménienne font l'objet de mauvais traitements en Russie, il n'est ni établi ni même allégué qu'il courrait des risques personnels à cet égard ;<br/>
<br/>
              7.	Considérant qu'il résulte de ce qui précède que M. B...n'est pas fondé à demander l'annulation pour excès de pouvoir du décret du 17 mars 2015 accordant son extradition aux autorités de la Fédération de Russie ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : La requête de M. B...est rejetée.<br/>
<br/>
Article 2 : La présente décision sera notifiée à M. A...B...et à la garde des sceaux, ministre de la justice.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-05-04-02 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - MOTIFS. ERREUR MANIFESTE. ABSENCE. - POSSIBILITÉ DE REFUSER L'EXTRADITION EN CAS DE CONSÉQUENCES D'UNE GRAVITÉ EXCEPTIONNELLE POUR LA PERSONNE RÉCLAMÉE - CONSÉQUENCES SUR LA VIE FAMILIALE EN FRANCE DE L'INTÉRESSÉ.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">335-04-03-02 ÉTRANGERS. EXTRADITION. DÉCRET D'EXTRADITION. LÉGALITÉ INTERNE. - POSSIBILITÉ DE REFUSER L'EXTRADITION EN CAS DE CONSÉQUENCES D'UNE GRAVITÉ EXCEPTIONNELLE POUR LA PERSONNE RÉCLAMÉE - MOYEN TIRÉ DE CONSÉQUENCES D'UNE GRAVITÉ EXCEPTIONNELLE SUR LA VIE FAMILIALE EN FRANCE DE L'INTÉRESSÉ - 1) OPÉRANCE - 2) APPRÉCIATION [RJ1].
</SCT>
<ANA ID="9A"> 01-05-04-02 Le second alinéa de l'article 1er des réserves et déclarations émises par la France lors de la ratification de la convention européenne d'extradition du 13 décembre 1957 prévoit que l'extradition pourra être refusée si la remise est susceptible d'avoir des conséquences d'une gravité exceptionnelle pour la personne réclamée.,,,S'il est soutenu qu'en cas d'exécution du décret accordant l'extradition, la vie familiale en France de la personne réclamée sera affectée, une telle décision trouve, en principe, sa justification dans la nature même de la procédure d'extradition, qui est de permettre, dans l'intérêt de l'ordre public et sous les conditions fixées par les dispositions qui la régissent, le jugement de personnes se trouvant en France qui sont poursuivies à l'étranger pour des crimes ou des délits commis hors de France. En ne faisant pas usage de la faculté de refuser l'extradition, ainsi que le permettent les réserves et déclarations mentionnées ci-dessus, le Premier ministre n'a pas commis d'erreur manifeste d'appréciation.</ANA>
<ANA ID="9B"> 335-04-03-02 Le second alinéa de l'article 1er des réserves et déclarations émises par la France lors de la ratification de la convention européenne d'extradition du 13 décembre 1957 prévoit que l'extradition pourra être refusée si la remise est susceptible d'avoir des conséquences d'une gravité exceptionnelle pour la personne réclamée.,,,1) A l'appui d'un recours contre le décret accordant l'extradition, peut utilement être invoqué le moyen tiré de ce que la remise est susceptible d'avoir des conséquences exceptionnellement graves sur la vie familiale de la personne réclamée.,,,2) Toutefois, la décision accordant l'extradition trouve, en principe, sa justification dans la nature même de la procédure d'extradition, qui est de permettre, dans l'intérêt de l'ordre public et sous les conditions fixées par les dispositions qui la régissent, le jugement de personnes se trouvant en France qui sont poursuivies à l'étranger pour des crimes ou des délits commis hors de France. En ne faisant pas usage de la faculté de refuser l'extradition, ainsi que le permettent les réserves et déclarations mentionnées ci-dessus, le Premier ministre n'a pas commis d'erreur manifeste d'appréciation.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1]Cf., sur le degré de contrôle, CE, 11 février 2002, Mlle G., n° 226839, p. 31.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
