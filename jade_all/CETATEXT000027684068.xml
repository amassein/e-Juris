<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027684068</ID>
<ANCIEN_ID>JG_L_2013_07_000000368085</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/68/40/CETATEXT000027684068.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème et 7ème sous-sections réunies, 05/07/2013, 368085, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-07-05</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>368085</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème et 7ème sous-sections réunies</FORMATION>
<TYPE_REC>Autres</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP PEIGNOT, GARREAU, BAUER-VIOLAS ; SCP THOUIN-PALAT, BOUCARD</AVOCATS>
<RAPPORTEUR>M. Yves Doutriaux</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Damien Botteghi</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:368085.20130705</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu l'ordonnance n° 1213152/5-1 du 24 avril 2013, enregistrée le 25 avril 2013 au secrétariat du contentieux du Conseil d'Etat, par laquelle le président de section du tribunal administratif de Paris statuant sur le fondement de l'article R. 771-7 du code de justice administrative, avant de statuer sur la demande de Mme A...B..., tendant à l'annulation de l'arrêté du 16 mars 2012 par lequel le préfet de police l'a exclue temporairement de fonctions pour une durée de deux ans dont un an avec sursis et, du fait de la perte du bénéfice du sursis de dix-huit mois qui lui avait été accordé par un arrêté du 17 mai 2010 portant exclusion temporaire de fonctions, l'a exclue de ses fonctions pour une durée totale de trois ans et demi, dont un an avec sursis, a décidé, par application des dispositions de l'article 23-2 de l'ordonnance n° 58-1067 du 7 novembre 1958, de transmettre au Conseil d'Etat la question de la conformité aux droits et libertés garantis par la Constitution des dispositions de l'article 89 de la loi n° 84-53 du 26 janvier 1984 portant dispositions statutaires relatives à la fonction publique territoriale ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la Constitution, notamment son  Préambule et son article 61-1 ;<br/>
<br/>
              Vu l'ordonnance n° 58-1067 du 7 novembre 1958 ; <br/>
<br/>
              Vu l'article 89 de la loi n° 84-53 du 26 janvier 1984 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Yves Doutriaux, Conseiller d'Etat,<br/>
<br/>
              - les conclusions de M. Damien Botteghi, Rapporteur public,<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Thouin-Palat, Boucard, avocat de MmeB..., et à la SCP Peignot, Garreau, Bauer-Violas, avocat du ministre de l'intérieur ; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1.	Considérant qu'il résulte des dispositions de l'article 23-4 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel que, lorsqu'une juridiction relevant du Conseil d'Etat a transmis à ce dernier, en application de l'article 23-2 de cette même ordonnance, la question de la conformité à la Constitution d'une disposition législative, le Conseil constitutionnel est saisi de cette question de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux ;<br/>
<br/>
              2.	Considérant que, sur le fondement de ces dispositions, Mme B...demande que soit renvoyée au Conseil Constitutionnel la question de la conformité aux droits et libertés garantis par la Constitution des dispositions de l'article 89 de la loi du 26 janvier 1984 portant dispositions statutaires relatives à la fonction publique territoriale ; <br/>
<br/>
              3.	Considérant qu'aux termes de l'article 89 de la loi du 26 janvier 1984 : " (....) L'exclusion temporaire de fonctions, qui est privative de toute rémunération, peut être assortie d'un sursis total ou partiel. Celui-ci ne peut avoir pour effet, dans le cas de l'exclusion temporaire de fonctions du troisième groupe, de ramener la durée de cette exclusion à moins de un mois. L'intervention d'une sanction disciplinaire des deuxième et troisième groupes pendant une période de cinq ans après le prononcé de l'exclusion temporaire entraîne la révocation du sursis. En revanche, si aucune sanction disciplinaire, autre que celles prévues dans le cadre du premier groupe, n'a été prononcée durant cette même période à l'encontre de l'intéressé, ce dernier est dispensé définitivement de l'accomplissement de la partie de la sanction pour laquelle il a bénéficié du sursis./ (...) " ;<br/>
<br/>
              4.	Considérant que Mme B...soutient que l'article 89 de la loi du 26 janvier 1984, applicable au litige, est contraire aux principes de la nécessité et de l'individualisation des peines garantis par l'article 8 de la Déclaration des droits de l'homme et du citoyen de 1789 ; que, toutefois,  l'autorité disciplinaire tient compte des circonstances propres à chaque espèce et de l'adéquation de la sanction aux fautes commises lorsqu'elle choisit le quantum de la sanction disciplinaire à l'encontre d'un agent public et que, dans le cas du prononcé d'une exclusion temporaire de fonctions, elle l'assortit d'un sursis total ou partiel ; qu'il en va de même lorsqu'elle prononce, pendant une période de cinq ans après une décision d'exclusion temporaire, une nouvelle sanction disciplinaire du deuxième ou troisième groupe à l'encontre du même agent  entraînant la révocation du sursis ; qu'en outre, elle tient compte, à cette occasion,  de ce que la révocation du sursis conduirait à une sanction disproportionnée aux faits reprochés ; que, par suite, la question soulevée, qui n'est pas nouvelle, ne présente pas un caractère sérieux ;<br/>
<br/>
              5.	Considérant qu'il résulte de ce qui précède qu'il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité invoquée ; <br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : Il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité transmise par le tribunal administratif de Paris.<br/>
<br/>
Article 2 : La présente décision sera notifiée à Mme A...B...et au ministre de l'intérieur.<br/>
Copie en sera adressée au Conseil constitutionnel, au Premier ministre et au tribunal administratif de Paris.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
