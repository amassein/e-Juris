<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043875942</ID>
<ANCIEN_ID>JG_L_2021_07_000000451047</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/87/59/CETATEXT000043875942.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème chambre, 28/07/2021, 451047, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-07-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>451047</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Philippe Barbat</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Laurent Cytermann</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:451047.20210728</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              La Commission nationale des comptes de campagne et des financements politiques (CNCCFP), sur le fondement de l'article L. 52-15 du code électoral, a saisi le tribunal administratif de Bordeaux de sa décision du 23 novembre 2020 rejetant le compte de campagne de M. B... A..., candidat tête de liste aux élections municipales des 15 mars et 28 juin 2020 dans la commune de La Teste-de-Buch (Gironde). Par un jugement n° 2005729 du 8 mars 2021, le tribunal administratif de Bordeaux a dit que le compte de campagne de M. A... a été rejeté à bon droit par la CNCCFP et déclaré M. A... inéligible pour une durée d'un an à compter de la date à laquelle son jugement serait définitif.<br/>
<br/>
              Par une requête, enregistrée le 25 mars 2021 au secrétariat du contentieux du Conseil d'Etat, M. A... demande au Conseil d'Etat d'annuler ce jugement.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code électoral ;<br/>
              - la loi n° 2019-1269 du 2 décembre 2019<br/>
              - la loi n° 2020-290 du 23 mars 2020 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Rose-Marie Abel, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Laurent Cytermann, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. Monsieur B... A... était candidat au premier tour des élections municipales qui s'est déroulé le 15 mars 2020 dans la commune de la Teste-de-Buch (Gironde), à la tête d'une liste qui a obtenu 2,74 % des voix. Par décision du 23 novembre 2020, la commission nationale des comptes de campagne et des financements politiques (CNCCFP) a rejeté le compte de campagne de M. A... en se fondant sur le fait que ce compte, d'une part, faisait apparaître un déficit de 1 832 euros et, d'autre part, n'avait pas été présenté par un expert-comptable. M. A... fait appel du jugement du 8 mars 2021 par lequel le tribunal administratif de Bordeaux, saisi par la CNCCFP, d'une part, a décidé que cette commission avait rejeté à bon droit son compte de campagne, d'autre part, l'a déclaré inéligible pour une durée d'un an.<br/>
<br/>
              2. Aux termes de l'article L. 118-3 du code électoral, dans sa rédaction antérieure à la loi du 2 décembre 2019 visant à clarifier diverses dispositions du droit électoral : " Saisi par la commission instituée par l'article L. 52-14, le juge de l'élection peut prononcer l'inéligibilité du candidat dont le compte de campagne, le cas échéant après réformation, fait apparaître un dépassement du plafond des dépenses électorales. (...) / Saisi dans les mêmes conditions, le juge de l'élection peut prononcer l'inéligibilité du candidat ou des membres du binôme de candidats qui n'a pas déposé son compte de campagne dans les conditions et le délai prescrits à l'article L. 52-12. / Il prononce également l'inéligibilité du candidat ou des membres du binôme de candidats dont le compte de campagne a été rejeté à bon droit en cas de volonté de fraude ou de manquement d'une particulière gravité aux règles relatives au financement des campagnes électorales. ". Aux termes de ce même article dans sa rédaction issue de cette loi : " Lorsqu'il relève une volonté de fraude ou un manquement d'une particulière gravité aux règles de financement des campagnes électorales, le juge de l'élection, saisi par la Commission nationale des comptes de campagne et des financements politiques, peut déclarer inéligible : 1° Le candidat qui n'a pas déposé son compte de campagne dans les conditions et le délai prescrits à l'article L. 52-12 ; / 2° le candidat dont le compte de campagne, le cas échéant après réformation, fait apparaître un dépassement du plafond des dépenses électorales ; / 3° le candidat dont le compte de campagne a été rejeté à bon droit (...) ".<br/>
<br/>
              3. Aux termes du premier alinéa de l'article 15 de la loi du 2 décembre 2019 : " La présente loi, à l'exception de l'article 6, entre en vigueur le 30 juin 2020 ". Aux termes du XVI de l'article 19 de la loi du 23 mars 2020 d'urgence pour faire face à l'épidémie de covid-19 : " A l'exception de son article 6, les dispositions de la loi n° 2019-1269 du 2 décembre 2019 visant à clarifier diverses dispositions du droit électoral ne sont pas applicables au second tour de scrutin régi par la présente loi ". Il résulte de ces dispositions que les dispositions de la loi du 2 décembre 2019 modifiant celles du code électoral, à l'exception de son article 6, ne sont pas applicables aux opérations électorales en vue de l'élection des conseillers municipaux et communautaires organisées les 15 mars et 28 juin 2020, y compris en ce qui concerne les comptes de campagne. <br/>
<br/>
              4. Toutefois, l'inéligibilité prévue par les dispositions de l'article L. 118-3 du code électoral constitue une sanction ayant le caractère d'une punition. Il incombe dès lors au juge de l'élection, lorsqu'il est saisi de conclusions tendant à ce qu'un candidat dont le compte de campagne est rejeté soit déclaré inéligible et à ce que son élection soit annulée, de faire application, le cas échéant, d'une loi nouvelle plus douce entrée en vigueur entre la date des faits litigieux et celle à laquelle il statue. Le législateur n'ayant pas entendu, par les dispositions citées au point 3, faire obstacle à ce principe, le juge doit faire application aux opérations électorales mentionnées à ce même point des dispositions de cet article dans sa rédaction issue de la loi du 2 décembre 2019. En effet, cette loi nouvelle laisse désormais au juge, de façon générale, une simple faculté de déclarer inéligible un candidat en la limitant aux cas où il relève une volonté de fraude ou un manquement d'une particulière gravité aux règles de financement des campagnes électorales, alors que l'article L. 118-3 dans sa version antérieure, d'une part, prévoyait le prononcé de plein droit d'une inéligibilité lorsque le compte de campagne avait été rejeté à bon droit en cas de volonté de fraude ou de manquement d'une particulière gravité et, d'autre part, n'imposait pas cette dernière condition pour que puisse être prononcée une inéligibilité lorsque le candidat n'avait pas déposé son compte de campagne dans les conditions et le délai prescrit par l'article L 52-12 de ce même code.<br/>
<br/>
              5. Il résulte de ce qui précède que c'est à tort que le tribunal administratif de Bordeaux s'est fondé sur les dispositions de l'article L. 118-3 du code électoral dans sa rédaction antérieure à la loi du 2 décembre 2019 pour infliger à M. A... une sanction d'inéligibilité de six mois. <br/>
<br/>
              6. Il appartient au Conseil d'Etat, saisi de l'ensemble du litige par l'effet dévolutif de l'appel, d'examiner le bien-fondé de la décision de la CNCCFP et la question de savoir si les irrégularités relevées par celle-ci justifient que lui soit infligée une sanction sur le fondement de l'article L. 118-3 du code électoral dans sa rédaction issue de la loi du 2 décembre 2019.<br/>
<br/>
              7. Aux termes de l'article L. 52-12 du code électoral dans sa rédaction en vigueur à la date de l'élection municipale des 15 mars et 28 juin 2020 : " Chaque candidat ou candidat tête de liste soumis au plafonnement prévu à l'article L. 52-11 et qui a obtenu au moins 1 % des suffrages exprimés est tenu d'établir un compte de campagne retraçant, selon leur origine, l'ensemble des recettes perçues et, selon leur nature, l'ensemble des dépenses engagées ou effectuées en vue de l'élection, hors celles de la campagne officielle, par lui-même ou pour son compte, au cours de la période mentionnée à l'article L. 52-4. (...). Le compte de campagne doit être en équilibre ou excédentaire et ne peut présenter un déficit. / Au plus tard avant 18 heures le dixième vendredi suivant le premier tour de scrutin, chaque candidat ou candidat tête de liste présent au premier tour dépose à la Commission nationale des comptes de campagne et des financements politiques son compte de campagne et ses annexes accompagné des justificatifs de ses recettes (...). Le compte de campagne est présenté par un membre de l'ordre des experts-comptables et des comptables agréés ; celui-ci met le compte de campagne en état d'examen et s'assure de la présence des pièces justificatives requises. (...) ". Aux termes de l'article L. 52-15 du code électoral dans sa rédaction en vigueur à la date de cette même élection : " La Commission nationale des comptes de campagne et des financements politiques approuve et, après procédure contradictoire, rejette ou réforme les comptes de campagne. (...) / Lorsque la commission a constaté que le compte de campagne n'a pas été déposé dans le délai prescrit, si le compte a été rejeté (...) la commission saisit le juge de l'élection ".<br/>
<br/>
              8. L'obligation pour le candidat de remettre à la CNCCFP un compte de campagne en équilibre ou excédentaire et la présentation de ce compte par un expert-comptable, prévues par les dispositions de l'article L. 52-12 du code électoral citées ci-dessus, sont au nombre des règles substantielles relatives au financement des campagnes électorales dont la méconnaissance peut, si elle est délibérée, constituer un manquement d'une particulière gravité à ces règles et justifier ainsi une sanction d'inéligibilité prise sur le fondement des dispositions de l'article L. 118-3 du même code.<br/>
<br/>
              9. D'une part, le compte de campagne de M. A... n'est pas présenté par un expert-comptable. D'autre part, le compte de campagne qu'il a déposé le 10 juillet 2020 à la CNCCFP fait apparaître un solde déficitaire de 1 832 euros. Si M. A... indique avoir comblé ce déficit par un versement effectué en décembre 2020, cette circonstance est sans incidence sur la légalité de la décision du 23 novembre 2020 de la CNCCFP, dès lors que ce comblement est intervenu postérieurement au dépôt du compte à la commission.<br/>
<br/>
              10. Ainsi, en remettant à la CNCCFP un compte déficitaire qui n'était pas présenté par un expert-comptable, M. A... a méconnu, de manière délibérée, deux règles substantielles relatives au financement des campagnes électorales. Il a, par conséquent, commis un manquement d'une particulière gravité aux règles relatives au financement des campagnes électorales qui justifie, compte tenu de l'ensemble des circonstances de l'espèce, qu'il soit déclaré inéligible pour une durée d'un an.<br/>
<br/>
              11. Il résulte de tout ce qui précède que M. A... n'est pas fondé à se plaindre de ce que, par le jugement qu'il attaque, le tribunal administratif de Bordeaux a, d'une part, décidé que la CNCCFP avait rejeté à bon droit son compte de campagne et l'a, d'autre part, déclaré inéligible pour une durée d'un an.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de M. A... est rejetée.<br/>
Article 2  : La présente décision sera notifiée à M. B... A..., à la Commission nationale des comptes de campagne et des financements politiques et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
