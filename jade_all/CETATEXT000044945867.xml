<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044945867</ID>
<ANCIEN_ID>JG_L_2021_12_000000459740</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/94/58/CETATEXT000044945867.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 29/12/2021, 459740, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-12-29</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>459740</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2021:459740.20211229</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
              Par une requête, enregistrée le 22 décembre 2021 au secrétariat du contentieux du Conseil d'Etat, M. A... B... demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative :<br/>
<br/>
              1°) de suspendre l'exécution du décret n° 2021-1059 du 7 août 2021 en ce qu'il ne règlemente pas la situation des personnes ayant une sérologie positive ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 1 500 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Il soutient que :<br/>
              - la condition d'urgence est satisfaite en ce qu'il se trouvera, dès le 1er janvier 2022, dans l'impossibilité de pratiquer une activité sportive régulière pour se maintenir en bonne santé dès lors que son club sportif est soumis à l'obligation de présentation du passe sanitaire ;<br/>
              - le décret contesté, en ce qu'il contraint à présenter un passe sanitaire pour pouvoir effectuer toute une série d'activités, met en jeu plusieurs libertés fondamentales, à savoir le droit à l'intégrité physique, le droit au respect de la vie privée et la liberté d'aller et venir ;<br/>
              - ce décret porte une atteinte grave et manifestement illégale à son droit à l'intégrité physique dès lors que, en premier lieu, il a déjà contracté la Covid-19 et présente un taux d'anticorps, mesuré par un test sérologique, qui assure son immunité, en deuxième lieu, la vaccination à l'aide d'un produit disposant seulement d'une autorisation de mise sur le marché conditionnelle risque de l'exposer à d'éventuels effets secondaires indésirables et, en dernier lieu, la vaccination ne protège pas contre la transmission du virus ;<br/>
              - ce décret porte une atteinte grave et manifestement illégale à son droit au respect de sa vie privée et à sa liberté d'aller et venir dès lors que, d'une part, il doit se soumettre à plusieurs tests payants par semaine pour accéder à certains lieux de la vie quotidienne et pour emprunter les transports publics interrégionaux et, d'autre part, l'accès aux établissements de santé est restreint à la présentation du passe sanitaire. <br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
              Vu : <br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ; <br/>
              - le code de la santé publique ; <br/>
              - la loi n° 2021-689 du 31 mai 2021 ;<br/>
              - la loi n° 2021-1040 du 5 août 2021 ; <br/>
              - le décret n° 2021-699 du 1er juin 2021 ;<br/>
              - le de´cret n° 2021-724 du 7 juin 2021 ;<br/>
              - le décret n° 2021-1059 du 7 août 2021 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. (...) ". En vertu de l'article L. 522-3 du même code, le juge des référés peut, par une ordonnance motivée, rejeter une requête sans instruction ni audience lorsque la condition d'urgence n'est pas remplie ou lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable ou qu'elle est mal fondée.<br/>
<br/>
              2. M. Dolleans doit être regardé comme demandant, sur le fondement des dispositions citées au point 1, la suspension de l'exécution des dispositions du décret du 1er juin 2021 issues du décret du 7 août 2021, en ce qu'elles ne réglementent pas la situation des personnes ayant une sérologie positive.<br/>
<br/>
              3. A l'appui des moyens tirés de ce que l'exigence en application du décret du 1er juin 2021 du passe sanitaire pour accéder à certains lieux et effectuer certaines activités porterait une atteinte grave et manifestement illégale respectivement à son droit au respect de son intégrité physique, à son droit au respect de sa vie privée et à sa liberté d'aller et venir, le requérant se borne à faire valoir, pour contester la proportionnalité de ces obligations au regard de l'objectif de santé publique poursuivi, que les risques liés à la vaccination et les contraintes tenant à l'application du passe sanitaire l'emporteraient sur les bénéfices individuels qu'il pourrait en retirer, dès lors qu'il présenterait un taux d'anticorps, mesuré par un test sérologique, qui assurerait son immunité. Il est manifeste qu'il ne peut ce faisant être regardé comme justifiant d'une atteinte grave et manifestement illégale que porteraient à une liberté fondamentale les dispositions réglementaires contestées.<br/>
<br/>
              4. Il résulte de ce qui précède, sans qu'il soit besoin de se prononcer sur la condition d'urgence, que la requête doit être rejetée en application des dispositions de l'article L. 522-3 du code de justice administrative, y compris ses conclusions tendant à l'application des dispositions de l'article L. 761-1 du même code.<br/>
<br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de M. Dolleans est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à M. Pascal Dolleans.<br/>
Fait à Paris, le 29 décembre 2021<br/>
Signé : Christine Maugüé<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
