<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037437532</ID>
<ANCIEN_ID>JG_L_2018_09_000000410194</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/43/75/CETATEXT000037437532.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème chambre, 26/09/2018, 410194, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-09-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>410194</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GARREAU, BAUER-VIOLAS, FESCHOTTE-DESBOIS ; SCP SPINOSI, SUREAU</AVOCATS>
<RAPPORTEUR>Mme Laure Durand-Viel</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Julie Burguburu</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:410194.20180926</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La direction régionale des finances publiques de Midi-Pyrénées et du département de la Haute-Garonne, le conseil régional de l'ordre des experts-comptables de Toulouse Midi-Pyrénées, la société fiduciaire nationale d'expertise comptable ont saisi la chambre régionale de discipline du conseil régional de l'ordre des experts-comptables de Toulouse Midi-Pyrénées de plaintes contre M. B...A.... Par une décision du 26 octobre 2015, la chambre régionale de discipline a prononcé à l'encontre de M. B...A...la sanction de radiation du tableau comportant interdiction définitive d'exercer la profession d'expert-comptable. <br/>
<br/>
              Par une décision du 23 février 2017, la chambre nationale de discipline auprès du conseil supérieur de l'ordre des experts-comptables a, d'une part, déclaré irrecevables l'appel incident et l'appel de la société Fidexpertise, d'autre part, rejeté l'appel formé par M. B...A...contre la décision du 26 octobre 2015, confirmant la sanction qui lui avait été infligée par les premiers juges.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 28 avril et 28 juillet 2017 et le 4 mai 2018 au secrétariat du contentieux du Conseil d'Etat, M. B...A...demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler la décision précitée du 23 février 2017 en tant qu'elle a rejeté son appel contre la décision du 26 octobre 2015 par laquelle la chambre régionale de discipline a prononcé à son encontre la sanction de radiation du tableau comportant interdiction définitive d'exercer la profession d'expert-comptable ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat, du Conseil supérieur de l'ordre des experts-comptables et de la société fiduciaire nationale d'expertise-comptable la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
	Vu :<br/>
              - la Convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, notamment l'article 4 de son protocole n° 7 ;<br/>
              - le code des devoirs professionnels des experts-comptables ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Laure Durand-Viel, auditeur, <br/>
<br/>
              - les conclusions de Mme Julie Burguburu, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Spinosi, Sureau, avocat de M. A...et à la SCP Garreau, Bauer-Violas, Feschotte-Desbois, avocat du Conseil supérieur de l'ordre des experts-comptables ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que la chambre régionale de discipline du conseil régional de l'ordre des experts-comptables de Toulouse Midi-Pyrénées a, par une décision du 26 octobre 2015, prononcé à l'encontre de M. A...la sanction de radiation du tableau comportant interdiction définitive d'exercer la profession d'expert-comptable. Par une décision du 23 février 2017, contre laquelle M. A...se pourvoit en cassation, la chambre nationale de discipline du Conseil supérieur de l'ordre des experts-comptables a confirmé cette sanction.<br/>
<br/>
              2. Aux termes de l'article 184 du décret du 30 mars 2012 relatif à l'exercice de l'activité d'expertise comptable, applicable à la convocation devant la chambre régionale de discipline : " Trente jours au moins avant l'audience, le président convoque, par lettre recommandée avec avis de réception, l'intéressé et la personne qui a saisi l'instance disciplinaire. / La convocation comporte, à peine de nullité, l'indication des obligations législatives ou réglementaires auxquelles il est reproché à la personne poursuivie d'avoir contrevenu et des faits à l'origine des poursuites. (...) ". En vertu du dernier alinéa de l'article 192 du même décret, l'instruction des appels des décisions des chambres régionales de discipline et leur jugement sont assurés dans les conditions prévues aux articles 182 à 185. Il en résulte que la convocation à l'audience d'appel devant la chambre nationale de discipline doit, à peine de nullité, comporter l'indication des obligations législatives ou réglementaires auxquelles il est reproché à la personne poursuivie d'avoir contrevenu et des faits à l'origine des poursuites. <br/>
<br/>
              3. Il ressort des pièces de la procédure que la convocation de M. A...à l'audience au cours de laquelle la chambre nationale de discipline a pris la décision attaquée ne comportait pas l'indication des obligations législatives ou réglementaires auxquelles il était reproché, en appel, à M. A...d'avoir contrevenu. Par suite, et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, M. A...est fondé à demander l'annulation de la décision du 23 février 2017 qu'il attaque.<br/>
<br/>
              4. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge du Conseil supérieur de l'ordre des experts-comptables la somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La décision du 23 février 2017 de la chambre nationale de discipline des experts-comptables est annulée en tant qu'elle a rejeté son appel contre la décision du 26 octobre 2015 par laquelle la chambre régionale de discipline a prononcé à son encontre la sanction de radiation du tableau comportant interdiction définitive d'exercer la profession d'expert-comptable.<br/>
Article 2 : L'affaire est renvoyée dans cette mesure devant la chambre nationale de discipline des experts-comptables. <br/>
Article 3 : Le Conseil supérieur de l'ordre des experts-comptables versera à M. A...la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
Article 4 : La présente décision sera notifiée à M. B...A...et au Conseil supérieur de l'ordre des experts-comptables. Copie en sera adressée à la société fiduciaire nationale d'expertise comptable et au ministre de l'action et des comptes publics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
