<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028622881</ID>
<ANCIEN_ID>JG_L_2014_02_000000362331</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/62/28/CETATEXT000028622881.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  7ème sous-section jugeant seule, 14/02/2014, 362331, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-02-14</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>362331</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 7ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BOULLOCHE ; SCP BARTHELEMY, MATUCHANSKY, VEXLIARD ; SCP LYON-CAEN, THIRIEZ</AVOCATS>
<RAPPORTEUR>M. Arno Klarsfeld</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Gilles Pellissier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2014:362331.20140214</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 30 août et 28 novembre 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. A... B..., demeurant... ; M. B...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 11BX01797 du 5 juin 2012 par lequel la cour administrative d'appel de Bordeaux a rejeté sa requête tendant à l'annulation du jugement n° 0901557 du 19 mai 2011 par lequel le tribunal administratif de Fort-de-France a rejeté sa demande tendant à la condamnation de l'hôpital du François et de la Sarl Caribéenne d'études et de développement à lui verser la somme de 75 000 euros en paiement de l'indemnité prévue au règlement de la consultation dans le cadre du marché de conception-réalisation de la reconstruction de l'hôpital du François ; <br/>
<br/>
              2°) réglant l'affaire au fond, d'annuler le jugement attaqué et de condamner l'hôpital local du François à lui payer la somme de 75 000 euros HT, outre TVA et intérêts légaux à compter du 6 mai 2008, capitalisés ; <br/>
<br/>
              3°) de mettre à la charge des défendeurs le versement d'une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative, ainsi que la contribution à l'aide juridique de 35 euros au titre de l'article R. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code des marchés publics ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Arno Klarsfeld, Conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Gilles Pellissier, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Boulloche, avocat de M.B..., à la SCP Barthélemy, Matuchansky, Vexliard, avocat de l'hôpital local du François, et à la SCP Lyon-Caen, Thiriez, avocat de la société Caribéenne d'études et de développement ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article 37 du code des marchés publics : " Un marché de conception-réalisation est un marché de travaux qui permet au pouvoir adjudicateur de confier à un groupement d'opérateurs économiques ou, pour les seuls ouvrages d'infrastructure, à un seul opérateur économique, une mission portant à la fois sur l'établissement des études et l'exécution des travaux " ; que, selon le I de l'article 69 code des marchés publics, applicable au marché de conception-réalisation lancé par l'hôpital du François : " Les marchés de conception-réalisation définis à l'article 37 sont passés par les pouvoirs adjudicateurs soumis aux dispositions de la loi du 12 juillet 1985 susmentionnée selon la procédure d'appel d'offres restreint sous réserve des dispositions particulières qui suivent : / Un jury est composé dans les conditions fixées par le I de l'article 24. (...) / Le jury dresse un procès-verbal d'examen des candidatures et formule un avis motivé sur la liste des candidats à retenir. Le pouvoir adjudicateur arrête la liste des candidats admis à réaliser des prestations, auxquels sont remises gratuitement les pièces nécessaires à la consultation. Les candidats admis exécutent des prestations sur lesquelles se prononce le jury, après les avoir auditionnés. Ces prestations comportent au moins un avant-projet sommaire pour un ouvrage de bâtiment ou un avant-projet pour un ouvrage d'infrastructure, accompagné de la définition des performances techniques de l'ouvrage. (...) / Le marché est attribué au vu de l'avis du jury. / (...) Le règlement de la consultation prévoit le montant des primes et les modalités de réduction ou de suppression des primes des candidats dont le jury a estimé que les offres remises avant l'audition étaient incomplètes ou ne répondaient pas au règlement de la consultation. Le montant de la prime attribuée à chaque candidat est égal au prix estimé des études de conception à effectuer telles que définies par le règlement de la consultation, affecté d'un abattement au plus égal à 20 %. La rémunération de l'attributaire du marché tient compte de la prime qu'il a reçue " ; que l'article 2.11.4 du règlement de la consultation édicté pour la procédure de passation du marché de conception-réalisation en cause stipulait qu'une prime d'un montant de 75 000 euros hors taxe, avec taxe à la valeur ajoutée en sus au taux de la réglementation en vigueur, serait versée aux concurrents ayant remis une prestation conforme à ce règlement, le jury se réservant toutefois la possibilité d'en réduire le montant ou de la supprimer en fonction de critères tenant au contenu et à la qualité des documents transmis ; <br/>
<br/>
              2. Considérant, d'une part, que le pouvoir adjudicateur et les candidats sélectionnés par un jury pour exécuter les prestations visant à l'attribution d'un marché de conception-réalisation sont, indépendamment de l'attribution de ce marché, engagés dans un contrat ayant pour objet la remise de prestations conformes aux documents de la consultation et pour prix, conformément aux dispositions du code des marchés publics citées ci-dessus, une prime susceptible d'être réduite ou supprimée sur décision du jury ; <br/>
<br/>
              3. Considérant, d'autre part, que, lorsque les parties soumettent au juge un litige relatif à l'exécution du contrat qui les lie, il incombe en principe à celui-ci, eu égard à l'exigence de loyauté des relations contractuelles, de faire application du contrat ; que, toutefois, dans le cas seulement où il constate une irrégularité invoquée par une partie ou relevée d'office par lui, tenant au caractère illicite du contenu du contrat ou à un vice d'une particulière gravité relatif notamment aux conditions dans lesquelles les parties ont donné leur consentement, il doit écarter le contrat et ne peut régler le litige sur le terrain contractuel ; que, dans l'hypothèse où la procédure de passation d'un marché de conception-réalisation a été annulée, il appartient au juge saisi d'une demande de paiement de la prime par un candidat admis à concourir de déterminer si les vices ayant conduit à l'annulation de la procédure de passation du marché doivent ou non le conduire à écarter l'application du contrat passé par le pouvoir adjudicateur au titre de l'exécution des prestations exécutées dans le cours de cette procédure ; qu'ainsi en jugeant que la procédure de passation du marché relatif à la reconstruction de l'hôpital local du François a été annulée dans son ensemble par une ordonnance du juge des référés du tribunal administratif de Fort de France en date du 22 décembre 2007, en raison de discordances dans les avis d'appel public à la concurrence au regard du règlement de consultation du marché et que cette annulation s'étend ainsi nécessairement au règlement de la consultation et aux différents avis d'appel à concurrence, seuls à comporter l'indication de la prime à laquelle pouvait prétendre tout concurrent ayant présenté une offre conforme au dossier de consultation et que, par suite, le cabinet Lorenzo-Architecture ne pouvait utilement se prévaloir ni des mentions de l'appel public à concurrence et du règlement de la consultation prévoyant le versement de cette prime, ni des dispositions de l'article 69 du code des marchés publics qui fonde la disposition précitée du règlement de la consultation, sans rechercher si le vice avait une incidence sur la validité de l'engagement contractuel relatif au versement de la prime, la cour administrative d'appel de Bordeaux, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, a commis une erreur de droit ; <br/>
<br/>
              4. Considérant qu'il y a lieu dans les circonstances de l'espèce de régler l'affaire au fond en application des dispositions de l'article L. 821-1 du code de justice administrative ;<br/>
<br/>
              5. Considérant qu'il résulte de l'instruction que, pour prononcer l'annulation de la procédure de passation du marché de conception-réalisation, le juge des référés du tribunal administratif de Fort-de-France a relevé que la discordance entre le délai de validité des offres indiqué dans le règlement de la consultation et celui mentionné dans les avis de publicité, était constitutive d'un manquement aux obligations de publicité et de mise en concurrence ; que ce vice est toutefois sans incidence sur la validité de l'engagement contractuel relatif au versement de la prime ; que l'hôpital du François n'invoque aucune autre irrégularité et ne fait état d'aucun élément de nature à justifier une réduction du montant de la prime ; <br/>
<br/>
              6. Considérant qu'il résulte de ce qui précède que, conformément aux termes de l'acte d'engagement du groupement dont le mandataire était la société requérante et du règlement de consultation, figurant au dossier, la part de la prime qui revient à celle-ci s'élève à 75.000 euros avec application de la taxe à la valeur ajoutée ; que le cabinet Lorenzo Architecture a droit aux intérêts sur cette somme à compter du 6 mai 2008, date de la mise en demeure de payer adressée à l'hôpital du François ;<br/>
<br/>
              7. Considérant qu'il résulte de tout ce qui précède que le cabinet Lorenzo Architecture est fondé à demander l'annulation du jugement du tribunal administratif de Fort-de-France ayant rejeté sa demande tendant au versement de la prime et à ce que l'hôpital du François soit condamné à lui verser la somme de 75 000 euros HT avec application de la taxe à la valeur ajoutée, assortie des intérêts à compter du 6 mai 2008 ;<br/>
<br/>
              8. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge du cabinet Lorenzo Architecture qui n'est pas, dans la présente instance, la partie perdante ; qu'il y a lieu, en revanche, de mettre à la charge de l'hôpital du François la somme de 6 000 euros à verser à M B... en application des mêmes dispositions, au titre des frais exposés par lui et non compris dans les dépens, tant devant les juges du fond que devant le Conseil d'Etat ; que dans les circonstances de l'espèce, il y a lieu de mettre la contribution pour l'aide juridique à la charge de l'hôpital du François ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Bordeaux du 5 juin 2012 et le jugement du tribunal administratif de Fort-de-France du 19 mai 2011 sont annulés.<br/>
Article 2 : L'hôpital du François est condamné à verser au cabinet Lorenzo une somme de 75 000 euros à laquelle sera appliquée la taxe à la valeur ajoutée. Cette somme portera intérêts à compter du 6 mai 2008.<br/>
Article 3 : L'hôpital du François versera au cabinet Lorenzo une somme de 6 000 euros en application des dispositions de l'article L. 761-1 du code de justice administrative au titre des frais exposés par elle et non compris dans les dépens, tant devant les juges du fond que devant le Conseil d'Etat.<br/>
Article 4 : Les conclusions de l'hôpital local du François et de la société Caribéenne d'études et de développement présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée à M. A...B..., à l'hôpital local du François et à la société Caribéenne d'études et de développement.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
