<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042401311</ID>
<ANCIEN_ID>JG_L_2020_10_000000429484</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/40/13/CETATEXT000042401311.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème chambre, 02/10/2020, 429484, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-10-02</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>429484</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FOUSSARD, FROGER ; SCP WAQUET, FARGE, HAZAN ; SCP DIDIER, PINET</AVOCATS>
<RAPPORTEUR>Mme Sophie-Caroline de Margerie</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Guillaume Odinet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2020:429484.20201002</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. D... A... et Mme F... E... ont demandé au juge des référés du tribunal administratif de Versailles de suspendre l'exécution, sur le fondement de l'article L. 521-1 du code de justice administrative, de l'arrêté du 9 juin 2018 par lequel le maire de Neauphle-le-Château a délivré un permis de construire à Mme F... B... ainsi que la décision implicite du 23 juillet 2018 par laquelle le maire a rejeté leur recours gracieux contre cet arrêté. Par une ordonnance n° 1901467 du 22 mars 2019, le juge des référés du tribunal administratif de Versailles a rejeté leur demande.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 5 et 19 avril 2019 au secrétariat du contentieux du Conseil d'Etat, M. A... et Mme E... demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) statuant en référé, de faire droit à leur demande ;  <br/>
<br/>
              3°) de solidairement mettre à la charge de la commune de Neauphle-le-Château et de Mme B... une somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'urbanisme ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Sophie-Caroline de Margerie, conseiller d'Etat, <br/>
<br/>
              - les conclusions de M. Guillaume Odinet, rapporteur public,<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Foussard, Froger, avocat de M. A... et de Mme E..., à la SCP Waquet, Farge, Hazan, avocat de Mme B... et à la SCP Didier, Pinet, avocat de la commune de Neauphle-le-Château ; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              Sur le bien-fondé de l'ordonnance de référé :<br/>
<br/>
              1.	Par un arrêté en date du 9 juin 2018, le maire de Neauphle-le-Château a délivré à Mme B... un permis de construire. M. A... et Mme E... se pourvoient en cassation contre l'ordonnance par laquelle le juge des référés du tribunal administratif de Versailles a rejeté leur demande de suspension de cette décision.<br/>
<br/>
              2.	Aux termes de l'article R. 600-1 du code de l'urbanisme : " En cas (...) de recours contentieux à l'encontre d'un (...) permis de construire (...), (...) l'auteur du recours est tenu, à peine d'irrecevabilité, de notifier son recours à l'auteur de la décision et au titulaire de l'autorisation. (...). L'auteur d'un recours administratif est également tenu de le notifier à peine d'irrecevabilité du recours contentieux qu'il pourrait intenter ultérieurement en cas de rejet du recours administratif. La notification prévue au précédent alinéa doit intervenir par lettre recommandée avec accusé de réception, dans un délai de quinze jours francs à compter du dépôt (...) du recours ". Aux termes de l'article R. 424-15 du même code : " Mention du permis explicite ou tacite (...) doit être affichée sur le terrain, de manière visible de l'extérieur, par les soins de son bénéficiaire, dès la notification de l'arrêté (...) / Cet affichage mentionne également l'obligation, prévue à peine d'irrecevabilité par l'article R. 600-1, de notifier tout recours administratif ou contentieux à l'auteur de la décision et au bénéficiaire du permis (...) ". Aux termes de l'article A. 424-17 de ce code : " Le panneau d'affichage comprend la mention suivante : "Droit de recours : Le délai de recours contentieux est de deux mois à compter du premier jour d'une période continue de deux mois d'affichage sur le terrain du présent panneau (art. R. 600-2 du code de l'urbanisme)." Tout recours administratif ou tout recours contentieux doit, à peine d'irrecevabilité, être notifié à l'auteur de la décision et au bénéficiaire du permis ou de la décision prise sur la déclaration préalable. Cette notification doit être adressée par lettre recommandée avec accusé de réception dans un délai de quinze jours francs à compter du dépôt du recours (art. R. 600-1 du code de l'urbanisme). ".<br/>
<br/>
              3.	Il résulte de ces dispositions, d'une part, que l'exercice par un tiers d'un recours administratif ou contentieux contre un permis de construire montre qu'il a connaissance de cette décision et a, en conséquence, pour effet de faire courir à son égard le délai de recours contentieux, alors même que la publicité concernant ce permis n'aurait pas satisfait aux exigences prévues par l'article A. 424-17 du code de l'urbanisme, d'autre part, que si l'absence de mention dans l'affichage de l'obligation de notification du recours n'empêche pas le déclenchement du délai de recours contentieux mentionné à l'article R. 600-2 du code de l'urbanisme, elle a pour effet de rendre inopposable l'irrecevabilité prévue à l'article R. 600-1 du même code. <br/>
<br/>
              4.	Pour rejeter la demande de M. A... et Mme E... en raison de l'irrecevabilité de leur requête en annulation enregistrée le 21 novembre 2018, le juge des référés a estimé que le délai de quinze jours mentionné à l'article R. 600-1 du code de l'urbanisme était opposable à leur recours gracieux en date du 21 juillet 2018, notifié au maire le 23 juillet, dès lors que les exigences de notification au titulaire du permis avaient été rappelées sur le panneau d'affichage installé sur le terrain d'assiette du projet " au moins le 10 septembre ". En jugeant ainsi que la notification du recours gracieux dans le délai de quinze jours leur était opposable sans que soit établi, faute d'affichage, le rappel des exigences du code de l'urbanisme, le juge des référés a commis une erreur de droit. <br/>
<br/>
              5.	Par suite et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, l'ordonnance attaquée doit être annulée. <br/>
<br/>
              6.	Il y a lieu, dans les circonstances de l'espèce, de statuer sur la demande de suspension en application des dispositions de l'article L. 821-2 du code de justice administrative. <br/>
<br/>
              7.	Aux termes de l'article L. 521-1 du CJA : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision. ".<br/>
<br/>
              8.	Par un recours gracieux notifié au maire de Neauphle-le-Château le 23 juillet 2018, M. A... et Mme E... ont contesté le permis de construire délivré à Mme B... le 9 juin 2018. Le rejet implicite de ce recours le 23 septembre 2018 a fait courir à nouveau le délai de recours contentieux qui n'était pas expiré le 21 novembre, date d'enregistrement de la demande d'annulation du permis au tribunal administratif de Versailles, laquelle, contrairement à ce qui est soutenu, comporte la copie de la décision attaquée. Par suite alors que, ainsi qu'il a été dit ci-dessus, la notification du recours gracieux adressé au maire dans un délai de quinze jours ne leur était pas opposable, et contrairement à ce qui est soutenu, leurs conclusions tendant à la suspension de l'exécution de ce permis ne sont pas irrecevables. <br/>
<br/>
              Sur l'urgence :<br/>
<br/>
              9.	S'agissant de la demande de suspension d'un permis de construire, eu égard au caractère difficilement réversible de la construction d'un bâtiment, la condition d'urgence doit en principe être constatée lorsque les travaux vont commencer ou ont déjà commencé sans être pour autant achevés. Il ne ressort pas des pièces du dossier que les travaux seraient achevés.<br/>
<br/>
              Sur les moyens propres à créer un doute sérieux :<br/>
<br/>
              10.	En l'état de l'instruction, le moyen tiré de ce que le projet ne respecterait pas les dispositions de l'article U1/7 du plan local d'urbanisme paraît, de nature à faire naître un doute sérieux sur la légalité de l'arrêté en litige.<br/>
<br/>
              11.	En revanche, pour l'application de l'article L. 600-4-1 du code de l'urbanisme, en l'état du dossier soumis au Conseil d'Etat, les autres moyens, tirés du caractère incomplet du dossier, de ce que le pétitionnaire n'est pas propriétaire du terrain, qu'il a fait des déclarations frauduleuses sur la situation juridique du terrain, sa situation cadastrale et sa superficie, que le permis ne respecte pas les dispositions des articles U1/6, U1/8 et U1/13 du plan local d'urbanisme de la commune et que le maire a commis un détournement de pouvoir en accordant le permis contesté, ne sont pas susceptibles de fonder la suspension de l'exécution de l'arrêté attaqué.<br/>
<br/>
              12.	Il résulte de ce qui précède que M. A... et Mme E... sont fondés à demander la suspension de l'exécution de l'arrêté du maire de Neauphle-le-Château du 9 juin 2018.<br/>
<br/>
              Sur les frais liés au litige :<br/>
<br/>
              13.	Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la commune de Neauphle-le-Château et de Mme B..., la somme de 1 250 euros à verser chacun à M. A... et à Mme E..., au titre des dispositions de l'article L. 761-1 du code de justice administrative. Ces dispositions font obstacle à ce qu'une somme soit mise à ce titre à la charge de M. A... et à Mme E....<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
                                          --------------<br/>
Article 1er : L'ordonnance du tribunal administratif de Versailles du 22 mars 2019 est annulée. <br/>
<br/>
Article 2 : L'exécution du permis de construire n° PC n° 78442 18 Y0010 délivré à Mme B... par la commune de Neauphle-le Château, est suspendu jusqu'à ce que le tribunal administratif de Versailles ait statué sur la requête de M. A... et de Mme E.... <br/>
<br/>
Article 3 : La commune de Neauphle-le-Château et Mme B... verseront chacune une somme de 1 250 euros à M. A... et à Mme E..., au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : La présente décision sera notifiée à M. D... A..., à Mme F... E..., à Mme F... B... et à la commune de Neauphle-le-Château.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
