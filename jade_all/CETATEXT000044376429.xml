<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044376429</ID>
<ANCIEN_ID>JG_L_2021_11_000000450258</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/37/64/CETATEXT000044376429.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème - 2ème chambres réunies, 25/11/2021, 450258</TITRE>
<DATE_DEC>2021-11-25</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>450258</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème - 2ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FOUSSARD, FROGER</AVOCATS>
<RAPPORTEUR>M. Didier Ribes</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Mireille Le Corre</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:450258.20211125</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés les 1er mars et 18 juin 2021 au secrétariat du contentieux du Conseil d'État, M.  F... G... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision implicite du ministre de l'intérieur rejetant sa demande tendant à l'abrogation, d'une part, de la circulaire n° 57500 du 27 juillet 2015 relative au changement de subdivision d'arme des gradés et gendarmes de la gendarmerie mobile dans la gendarmerie départementale et, d'autre part, de la circulaire n° 17253 du 24 mars 2020 relative au changement de subdivision d'arme en 2021 des gendarmes mobiles et des gardes républicains dans la gendarmerie départementale ;<br/>
<br/>
              2°) d'enjoindre au ministre de l'intérieur d'abroger ces circulaires dans un délai d'un mois à compter de la notification de la décision à intervenir et, passé ce délai, sous astreinte de 1 000 euros par jour de retard ;<br/>
<br/>
              3°) de mettre à la charge de l'État la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la directive 2000/78/CE du Conseil du 27 novembre 2000 ;<br/>
              - le code de la défense ;<br/>
              - le code des relations entre le public et l'administration ;<br/>
              - la loi n° 83-634 du 13 juillet 1983 ;<br/>
              - le décret n° 2008-952 du 12 septembre 2008 ;<br/>
              - l'arrêté du ministre de l'intérieur du 5 avril 2012 relatif à la répartition des sous-officiers de gendarmerie par subdivision d'arme, par branche ou par spécialité et fixant les branches et spécialités au sein desquelles l'avancement intervient de façon distincte ;<br/>
              - l'arrêté du ministre de l'intérieur du 12 septembre 2016 fixant les conditions physiques et médicales d'aptitude exigées des personnels militaires de la gendarmerie nationale et des candidats à l'admission en gendarmerie ; <br/>
              - l'arrêté du ministre de l'intérieur du 8 juin 2021 fixant les conditions physiques et médicales d'aptitude exigées des personnels militaires de la gendarmerie nationale et des candidats à l'admission en gendarmerie ; <br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Didier Ribes, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Mireille Le Corre, rapporteure publique ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Foussard, Froger, avocat de M. G... ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article 3 du décret du 12 septembre 2008 portant statut particulier du corps des sous-officiers de gendarmerie : " Les sous-officiers de gendarmerie sont répartis par subdivision d'arme, par branche ou par spécialité définies par arrêté du ministre de l'intérieur ". Les subdivisions d'arme de la gendarmerie nationale, à savoir la gendarmerie départementale et la gendarmerie mobile, ont été définies par un arrêté du 5 avril 2012 du ministre de l'intérieur. <br/>
<br/>
              2. Par une circulaire du 27 juillet 2015, le ministre de l'intérieur a précisé les différentes modalités de mise en œuvre des changements de subdivision d'arme vers la gendarmerie départementale des gradés et gendarmes servant dans la gendarmerie mobile. Cette circulaire énonce en particulier que " le changement de subdivision d'arme (CSA) est une mesure de gestion permettant de répartir de manière équilibrée les effectifs et les compétences au sein des deux subdivisions d'arme de la gendarmerie. Il est destiné à préserver le dynamisme des unités de la gendarmerie mobile (GM) et de la garde républicaine (GR). Il permet également à la gendarmerie départementale (GD) de bénéficier de sous-officiers expérimentés et aguerris. Le CSA est prononcé par le ministre de l'intérieur qui possède un entier pouvoir d'appréciation en considération de l'intérêt et des nécessités du service ". Elle prévoit, en particulier, dans son point 2.1, que " pour préparer progressivement la sortie des (gendarmes mobiles) les plus anciens (avant la limite des 38 ans), le commandement initie annuellement une procédure CSA pour les militaires du grade de gendarme qui ont atteint 35 ans l'année qui précède celle du CSA " et que " tout gendarme appartenant à la gendarmerie mobile doit avoir fait l'objet d'une étude de sa situation dans le cadre du CSA au moins une fois avant d'atteindre l'âge de 38 ans ". Les conditions particulières de ce CSA, dit " tardif ", sont fixées annuellement par une circulaire. La circulaire du 24 mars 2020 a fixé ces conditions pour le changement de subdivision d'arme en 2021. Elle prévoit notamment que les gendarmes mobiles de carrière détenteurs d'un certificat d'aptitude médicale en cours de validité qui atteignent l'âge de 36, 37 ou 38 ans entre le 1er janvier et le 31 décembre 2021 sont tenus d'établir une fiche de vœux de mutation pour le " CSA tardif " au titre de l'année 2021 en vue d'une affectation en gendarmerie départementale, tout en précisant que les intéressés peuvent indiquer qu'ils ne souhaitent pas de changement de subdivision d'arme, l'opportunité de celui-ci étant appréciée par la hiérarchie.<br/>
<br/>
              3. Par lettre reçue le 21 décembre 2020, M. G..., gendarme de carrière affecté au sein d'un escadron de gendarmerie mobile, a demandé au ministre de l'intérieur l'abrogation des circulaires du 27 juillet 2015 et du 24 mars 2020. Eu égard à la teneur de ses écritures, il doit être regardé comme demandant l'annulation pour excès de pouvoir de la décision implicite de rejet de sa demande en tant qu'elle porte sur les dispositions des circulaires litigieuses relatives au CSA dit " tardif ".<br/>
<br/>
              4. L'autorité administrative compétente, saisie d'une demande tendant à l'abrogation d'un règlement illégal, est tenue d'y déférer, soit que ce règlement ait été illégal dès la date de sa signature, soit que l'illégalité résulte de circonstances de droit ou de fait postérieures à cette date.<br/>
<br/>
              5. En premier lieu, aux termes de l'article L. 312-2 du code des relations entre le public et l'administration : " Font l'objet d'une publication les instructions, les circulaires ainsi que les notes et réponses ministérielles qui comportent une interprétation du droit positif ou une description des procédures administratives. Les instructions et circulaires sont réputées abrogées si elles n'ont pas été publiées, dans des conditions et selon des modalités fixées par décret. / Un décret en Conseil d'Etat pris après avis de la commission mentionnée au titre IV précise les autres modalités d'application du présent article ". L'article R. 312-7 du même code dispose : " Les instructions ou circulaires qui n'ont pas été publiées sur l'un des supports prévus par les dispositions de la présente section ne sont pas applicables et leurs auteurs ne peuvent s'en prévaloir à l'égard des administrés. / A défaut de publication sur l'un de ces supports dans un délai de quatre mois à compter de leur signature, elles sont réputées abrogées ". Aux termes de l'article R. 312-8 du même code : " Par dérogation à l'article R. 312-3-1, les circulaires et instructions adressées par les ministres aux services et établissements de l'Etat sont publiées sur un site relevant du Premier ministre. Elles sont classées et répertoriées de manière à faciliter leur consultation ". Ces dispositions ne sont toutefois pas applicables aux circulaires comportant des dispositions à caractère réglementaire.<br/>
<br/>
              6. S'il résulte des dispositions citées au point 1 que les sous-officiers de gendarmerie sont répartis entre plusieurs subdivisions d'arme et qu'il appartient au ministre de l'intérieur de décider des changements de subdivision d'arme dans l'intérêt du service, elles ne définissent pas les modalités de gestion de ces mouvements de personnel au sein de la gendarmerie. C'est en sa qualité de chef de service que le ministre de l'intérieur a précisé ces modalités dans les circulaires contestées, qui revêtent ainsi un caractère réglementaire, en imposant notamment à certaines catégories de gendarmes d'établir une fiche de vœux pour un changement de subdivision d'arme. <br/>
<br/>
              7. Il résulte de ce qui précède que les circulaires contestées n'entrent pas dans le champ d'application des articles L. 312-1 et R. 312-8 du code des relations entre le public et l'administration. Au demeurant, il ressort des pièces du dossier que la circulaire du 27 juillet 2015 a été publiée, dans le délai prévu, sur le site internet relevant du Premier ministre mentionné à l'article R. 312-8. M. G... n'est, dès lors, pas fondé à soutenir que les circulaires contestées, faute d'avoir été publiées dans les conditions définies par le code des relations entre le public et l'administration, seraient réputées abrogées en application de ces dispositions et que, par suite et en tout état de cause, le refus de les abroger, en ce qu'il conduirait à les maintenir en vigueur, serait illégal.<br/>
<br/>
              8. En deuxième lieu, contrairement à ce que soutient le requérant, les circulaires contestées ne fixent pas une limite d'âge interdisant à un sous-officier de continuer à servir dans la gendarmerie mobile et imposant, avant qu'il n'atteigne cet âge, un changement de subdivision d'arme. Par suite, le moyen tiré de ce que le ministre de l'intérieur aurait incompétemment ajouté une condition non prévue par les dispositions statutaires applicables aux sous-officiers de la gendarmerie ne peut qu'être écarté.<br/>
<br/>
              9. En troisième lieu, aux termes l'article 1er de la directive 2000/78/CE du 27 novembre 2000 portant création d'un cadre général en faveur de l'égalité de traitement en matière d'emploi et de travail : " La présente directive a pour objet d'établir un cadre général pour lutter contre la discrimination fondée sur (...) l'âge (...), en ce qui concerne l'emploi et le travail, en vue de mettre en œuvre, dans les États membres, le principe de l'égalité de traitement ". Aux termes du 2 de l'article 2 de la même directive : " (...) a) une discrimination directe se produit lorsqu'une personne est traitée de manière moins favorable qu'une autre ne l'est, ne l'a été ou ne le serait dans une situation comparable, sur la base de l'un des motifs visés à l'article 1er (...) / b) une discrimination indirecte se produit lorsqu'une disposition, un critère ou une pratique apparemment neutre est susceptible d'entraîner un désavantage particulier pour des personnes (...) d'un âge (...) donnés, par rapport à d'autres personnes, à moins que (...) cette disposition, ce critère ou cette pratique ne soit objectivement justifié par un objectif légitime et que les moyens de réaliser cet objectif ne soient appropriés et nécessaires (...) ". <br/>
<br/>
              10. Ces dispositions ne font, en tout état de cause, pas obstacle à ce que, pour décider de changements de subdivision d'arme, notamment au regard de l'intérêt du service tendant à " la préservation du dynamisme des unités de gendarmerie mobile " et à l'apport de sous-officiers expérimentés à la gendarmerie départementale, l'âge soit susceptible d'être pris en compte. M. G... n'est ainsi pas fondé à soutenir que les circulaires contestées, qui se bornent, au demeurant, à prévoir un examen de l'affectation des gendarmes mobiles atteignant l'âge de 38 ans, créeraient une discrimination fondée sur l'âge.<br/>
<br/>
              11. En dernier lieu, l'article 3-1 du décret du 12 septembre 2008 renvoie à un arrêté du ministre de l'intérieur le soin de fixer les conditions médicales et physiques d'aptitude exigées pour servir en qualité de sous-officier de gendarmerie lors de l'admission dans le corps ou en cours de carrière. L'article 1er de l'arrêté du 12 septembre 2016, repris à l'article 1er de l'arrêté du 8 juin 2021, fixant ces conditions dispose que " les militaires de la gendarmerie nationale doivent présenter une aptitude médicale conforme aux exigences et aux contraintes inhérentes aux fonctions qu'ils exercent ". Contrairement à ce que soutient M. G..., ces dispositions ne font pas obstacle à ce qu'un changement de subdivision d'arme soit décidé à l'égard d'un gendarme mobile apte médicalement à continuer à exercer ses fonctions dans sa subdivision d'arme d'origine. Il s'ensuit que les circulaires contestées ne méconnaissent pas ces dispositions. <br/>
<br/>
              12. Il résulte de tout ce qui précède que M. G... n'est pas fondé à demander l'annulation de la décision qu'il attaque. Sa requête doit, par suite, être rejetée, y compris ses conclusions à fin d'injonction et celles présentées au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de M. G... est rejetée.<br/>
Article 2 : La présente décision sera notifiée à M. F... G... et au ministre de l'intérieur.<br/>
<br/>
              Délibéré à l'issue de la séance du 10 novembre 2021 où siégeaient : M. Rémy Schwartz, président adjoint de la section du contentieux, présidant ; M. I... K..., M. Olivier Japiot, présidents de chambre ; M. J... M..., Mme A... L..., M. C... H..., M. D... N..., M. Jean-Yves Ollier, conseillers d'Etat et M. Didier Ribes, maître des requêtes-rapporteur. <br/>
<br/>
              Rendu le 25 novembre 2021.<br/>
                 Le président : <br/>
                 Signé : M. Rémy Schwartz<br/>
 		Le rapporteur : <br/>
      Signé : M. Didier Ribes<br/>
                 La secrétaire :<br/>
                 Signé : Mme E... B...<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-01-05-03 ACTES LÉGISLATIFS ET ADMINISTRATIFS. - DIFFÉRENTES CATÉGORIES D'ACTES. - ACTES ADMINISTRATIFS - NOTION. - INSTRUCTIONS ET CIRCULAIRES. - ABROGATION DES INSTRUCTIONS ET CIRCULAIRES NON PUBLIÉES (ART. L. 312-2 DU CRPA) - CHAMP D'APPLICATION - CIRCULAIRES COMPORTANT DES DISPOSITIONS À CARACTÈRE RÉGLEMENTAIRE -EXCLUSION [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">01-07-02-035 ACTES LÉGISLATIFS ET ADMINISTRATIFS. - PROMULGATION - PUBLICATION - NOTIFICATION. - PUBLICATION. - EFFETS D'UN DÉFAUT DE PUBLICATION. - ABROGATION DES INSTRUCTIONS ET CIRCULAIRES NON PUBLIÉES (ART. L. 312-2 DU CRPA) - CHAMP D'APPLICATION - CIRCULAIRES COMPORTANT DES DISPOSITIONS À CARACTÈRE RÉGLEMENTAIRE - EXCLUSION [RJ1].
</SCT>
<ANA ID="9A"> 01-01-05-03 Les articles L. 312-2, R. 312-7 et R. 312-8 du code des relations entre le public et l'administration (CRPA), qui prévoient que les instructions et circulaires comportant une interprétation du droit positif ou une description des procédures administratives sont réputées abrogées si elles n'ont pas été publiées, ne sont pas applicables aux circulaires comportant des dispositions à caractère réglementaire.</ANA>
<ANA ID="9B"> 01-07-02-035 Les articles L. 312-2, R. 312-7 et R. 312-8 du code des relations entre le public et l'administration (CRPA), qui prévoient que les instructions et circulaires comportant une interprétation du droit positif ou une description des procédures administratives sont réputées abrogées si elles n'ont pas été publiées, ne sont pas applicables aux circulaires comportant des dispositions à caractère réglementaire.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf., s'agissant des instructions réglementaires prises par un ministre en sa qualité de chef de service à destination de ses agents, CE, 24 juillet 2019, Ligue des droits de l'Homme et Confédération générale du travail et autres, n°s 427638 428895 429621, T. p. 549.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
