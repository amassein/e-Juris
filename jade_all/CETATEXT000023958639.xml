<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000023958639</ID>
<ANCIEN_ID>JG_L_2011_05_000000336893</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/23/95/86/CETATEXT000023958639.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème et 10ème sous-sections réunies, 05/05/2011, 336893, Publié au recueil Lebon</TITRE>
<DATE_DEC>2011-05-05</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>336893</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème et 10ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Philippe Martin</PRESIDENT>
<AVOCATS>SCP BOUZIDI, BOUHANNA</AVOCATS>
<RAPPORTEUR>M. Frédéric Aladjidi</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Claire Legras</COMMISSAIRE_GVT>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi, enregistré le 22 février 2010 au secrétariat du contentieux du Conseil d'Etat, présenté par le MINISTRE D'ETAT, MINISTRE DE L'ECOLOGIE, DE L'ENERGIE, DU DEVELOPPEMENT DURABLE ET DE LA MER, EN CHARGE DES TECHNOLOGIES VERTES ET DES NEGOCIATIONS SUR LE CLIMAT ; le ministre demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'ordonnance n° 09MA03479 du 18 décembre 2009 par laquelle le juge des référés de la cour administrative d'appel de Marseille, sur appel de M. Jacques A, a annulé l'ordonnance n° 0905122 du 2 septembre 2009 du juge des référés du tribunal administratif de Marseille ayant suspendu, à la demande du préfet des Bouches-du-Rhône, l'exécution de l'arrêté du 22 décembre 2008 par lequel le maire de Rognonas a délivré à M. A un permis de construire une maison individuelle et a rejeté la demande de suspension présentée par le préfet des Bouches-du-Rhône ;<br/>
<br/>
              2°) statuant en référé, de rejeter l'appel de M. A ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code général des collectivités territoriales ; <br/>
<br/>
              Vu le code de l'urbanisme ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Frédéric Aladjidi, Maître des Requêtes,  <br/>
<br/>
              - les observations de la SCP Bouzidi, Bouhanna, avocat de M. A, <br/>
<br/>
              - les conclusions de Mme Claire Legras, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Bouzidi, Bouhanna, avocat de M. A ;<br/>
<br/>
<br/>
<br/>Considérant qu'aux termes du premier alinéa de l'article L. 2131-6 du code général des collectivités territoriales : "Le représentant de l'Etat dans le département défère au tribunal administratif les actes (...) qu'il estime contraires à la légalité dans les deux mois suivant leur transmission" ; qu'aux termes du second alinéa de l'article L. 424-5 du code de l'urbanisme, dans sa rédaction issue de l'article 6 de la loi n° 2006-872 du 13 juillet 2006 portant engagement national pour le logement : "Le permis de construire, d'aménager ou de démolir, tacite ou explicite, ne peut être retiré que s'il est illégal et dans le délai de trois mois suivant la date de cette décision. Passé ce délai, le permis ne peut être retiré que sur demande explicite de son bénéficiaire" ; qu'en outre, un acte administratif obtenu par fraude ne créant pas de droits, il peut être abrogé ou retiré par l'autorité compétente pour le prendre, alors même que le délai qui lui est normalement imparti à cette fin serait expiré ;<br/>
<br/>
              Considérant que sauf dans le cas où des dispositions législatives ou réglementaires ont organisé des procédures particulières, toute décision administrative peut faire l'objet, dans le délai imparti pour l'introduction d'un recours contentieux, d'un recours gracieux ou hiérarchique qui interrompt le cours de ce délai ; que les dispositions précitées du code de l'urbanisme, qui limitent le délai pendant lequel une autorisation de construire peut être retirée, spontanément ou à la demande d'un tiers, par l'autorité qui l'a délivrée, n'ont ni pour objet ni pour effet de faire obstacle, d'une part, à ce que le représentant de l'Etat puisse former un recours gracieux, jusqu'à l'expiration du délai dont il dispose pour déférer un tel acte au tribunal administratif, et d'autre part à ce que le cours de ce délai soit interrompu par ce recours gracieux ; que d'ailleurs, alors même que le délai de trois mois fixé par l'article L. 424-5 du code de l'urbanisme serait arrivé à son terme, un tel recours n'est pas dépourvu d'utilité, soit que l'auteur de l'acte litigieux justifie de la légalité de celui-ci, soit que son bénéficiaire sollicite son retrait au profit d'une nouvelle décision légalement prise ;<br/>
<br/>
              Considérant qu'en se fondant sur la circonstance qu'à la date du 6 avril 2009 à laquelle le sous-préfet d'Arles a saisi le maire de Rognonas d'un recours gracieux dirigé contre le permis de construire délivré le 22 décembre 2008 à M. A, cet acte ne pouvait plus, en l'absence de fraude invoquée, être retiré qu'à la demande de son bénéficiaire, dès lors qu'il avait été délivré depuis plus de trois mois, et en en déduisant que ce recours gracieux n'avait pas prorogé le délai dont disposait le préfet pour le déférer au tribunal administratif, le juge des référés de la cour administrative d'appel de Marseille a commis une erreur de droit ; que le MINISTRE D'ETAT, MINISTRE DE L'ECOLOGIE, DE L'ENERGIE, DU DEVELOPPEMENT DURABLE ET DE LA MER, EN CHARGE DES TECHNOLOGIES VERTES ET DES NEGOCIATIONS SUR LE CLIMAT est, dès lors, fondé à demander l'annulation de l'ordonnance rendue par ce juge le 18 décembre 2009 ;<br/>
<br/>
              Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au titre de la procédure de référé engagée, en application des dispositions de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              Considérant qu'en vertu du troisième alinéa de l'article L. 2131-6 du code général des collectivités territoriales, repris à l'article L. 554-1 du code de justice administrative, le représentant de l'Etat peut assortir son déféré d'une demande de suspension, à laquelle il est fait droit si l'un des moyens invoqués paraît, en l'état de l'instruction, propre à créer un doute sérieux quant à la légalité de l'acte attaqué ; qu'une telle demande de suspension doit, toutefois, être rejetée comme non fondée lorsque le déféré qu'elle assortit est irrecevable ; qu'aux termes des deux premiers alinéas de l'article R. 421-2 du code de justice administrative : "Sauf disposition législative ou réglementaire contraire, le silence gardé pendant plus de deux mois sur une réclamation par l'autorité compétente vaut décision de rejet. / Les intéressés disposent, pour se pourvoir contre cette décision implicite, d'un délai de deux mois à compter du jour de l'expiration de la période mentionnée au premier alinéa. Néanmoins, lorsqu'une décision explicite de rejet intervient dans ce délai de deux mois, elle fait à nouveau courir le délai du pourvoi." ;<br/>
<br/>
              Considérant, en premier lieu, qu'il résulte de l'instruction que le sous-préfet d'Arles a reçu le 13 juin 2009 une lettre du maire de Rognonas rejetant le recours gracieux que ce dernier avait reçu le 6 avril 2009, dans le délai de deux mois suivant la transmission du permis de construire litigieux au représentant de l'Etat ; que cette lettre, même si elle était confirmative du rejet tacite né le 6 juin 2009 du silence gardé pendant deux mois par le maire, a, conformément à l'article R. 421-2 du code de justice administrative, fait courir un nouveau délai de deux mois pendant lequel l'acte contesté pouvait faire l'objet d'un déféré ; qu'ainsi, le déféré introduit par le préfet des Bouches-du-Rhône à l'encontre du permis de construire litigieux, qui a été enregistré le 11 août 2009 au greffe du tribunal administratif de Marseille, n'était pas tardif ; que, par suite, M. A n'est pas fondé à soutenir qu'il était irrecevable ;<br/>
<br/>
              Considérant, en second lieu, que si le moyen invoqué par le préfet des Bouches-du-Rhône et tiré de la méconnaissance de l'article R. 431-9 du code de l'urbanisme n'est, en l'état de l'instruction, pas susceptible de fonder la suspension demandée, les moyens tirés de la méconnaissance de l'article R. 111-2 du code de l'urbanisme et des dispositions de l'article NC1 du règlement du plan d'occupation des sols de la commune de Rognonas paraissent, en l'état de l'instruction, propres à créer un doute sérieux quant à la légalité de l'acte attaqué ;<br/>
<br/>
              Considérant qu'il résulte de ce qui précède que M. A n'est pas fondé à soutenir que c'est à tort que, par l'ordonnance du 2 septembre 2009, le juge des référés du tribunal administratif de Marseille a suspendu l'exécution de l'arrêté du maire de Rognonas du 22 décembre 2008 lui délivrant un permis de construire ;<br/>
<br/>
              Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mis à la charge de l'Etat, qui n'est pas, dans la présente instance, la partie perdante, le versement de la somme que M. A demande au titre des frais exposés par lui et non compris dans les dépens ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : L'ordonnance du juge des référés de la cour administrative d'appel de Marseille du 18 décembre 2009 est annulée.<br/>
Article 2 : La requête présentée par M. A devant la cour administrative d'appel de Marseille est rejetée.<br/>
Article 3 : Les conclusions présentées par M. A au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente ordonnance sera notifiée à la MINISTRE DE L'ECOLOGIE, DU DEVELOPPEMENT DURABLE, DES TRANSPORTS ET DU LOGEMENT, à M. Jacques A et à la commune de Rognonas.<br/>
<br/>
</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-01-07-04 PROCÉDURE. INTRODUCTION DE L'INSTANCE. DÉLAIS. INTERRUPTION ET PROLONGATION DES DÉLAIS. - DÉLIVRANCE D'UNE AUTORISATION DE CONSTRUIRE - RECOURS GRACIEUX DU PRÉFET - CONSÉQUENCE - PROROGATION DU DÉLAI DE RECOURS CONTENTIEUX, ALORS MÊME QUE LE MAIRE NE PEUT PLUS RETIRER LE PERMIS DE CONSTRUIRE EN VERTU DE L'ARTICLE L. 424-5 DU CODE DE L'URBANISME.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">68-03-04-05 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. PERMIS DE CONSTRUIRE. RÉGIME D'UTILISATION DU PERMIS. RETRAIT DU PERMIS. - DÉLIVRANCE D'UNE AUTORISATION DE CONSTRUIRE - RECOURS GRACIEUX DU PRÉFET - CONSÉQUENCE - PROROGATION DU DÉLAI DE RECOURS CONTENTIEUX, ALORS MÊME QUE LE MAIRE NE PEUT PLUS RETIRER LE PERMIS DE CONSTRUIRE EN VERTU DE L'ARTICLE L. 424-5 DU CODE DE L'URBANISME.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">68-06-01-03 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. RÈGLES DE PROCÉDURE CONTENTIEUSE SPÉCIALES. INTRODUCTION DE L'INSTANCE. DÉLAIS DE RECOURS. - DÉLIVRANCE D'UNE AUTORISATION DE CONSTRUIRE - RECOURS GRACIEUX DU PRÉFET - CONSÉQUENCE - PROROGATION DU DÉLAI DE RECOURS CONTENTIEUX, ALORS MÊME QUE LE MAIRE NE PEUT PLUS RETIRER LE PERMIS DE CONSTRUIRE EN VERTU DE L'ARTICLE L. 424-5 DU CODE DE L'URBANISME.
</SCT>
<ANA ID="9A"> 54-01-07-04 Sauf dans le cas où des dispositions législatives ou réglementaires ont organisé des procédures particulières, toute décision administrative peut faire l'objet, dans le délai imparti pour l'introduction d'un recours contentieux, d'un recours gracieux ou hiérarchique qui interrompt le cours de ce délai. Les dispositions de l'article L. 424-5 du code de l'urbanisme, qui limitent le délai pendant lequel une autorisation de construire peut être retirée, spontanément ou à la demande d'un tiers, par l'autorité qui l'a délivrée, n'ont ni pour objet ni pour effet de faire obstacle, d'une part, à ce que le représentant de l'Etat puisse former un recours gracieux, jusqu'à l'expiration du délai dont il dispose pour déférer un tel acte au tribunal administratif, et, d'autre part, à ce que le cours de ce délai soit interrompu par ce recours gracieux.</ANA>
<ANA ID="9B"> 68-03-04-05 Sauf dans le cas où des dispositions législatives ou réglementaires ont organisé des procédures particulières, toute décision administrative peut faire l'objet, dans le délai imparti pour l'introduction d'un recours contentieux, d'un recours gracieux ou hiérarchique qui interrompt le cours de ce délai. Les dispositions de l'article L. 424-5 du code de l'urbanisme, qui limitent le délai pendant lequel une autorisation de construire peut être retirée, spontanément ou à la demande d'un tiers, par l'autorité qui l'a délivrée, n'ont ni pour objet ni pour effet de faire obstacle, d'une part, à ce que le représentant de l'Etat puisse former un recours gracieux, jusqu'à l'expiration du délai dont il dispose pour déférer un tel acte au tribunal administratif, et, d'autre part, à ce que le cours de ce délai soit interrompu par ce recours gracieux.</ANA>
<ANA ID="9C"> 68-06-01-03 Sauf dans le cas où des dispositions législatives ou réglementaires ont organisé des procédures particulières, toute décision administrative peut faire l'objet, dans le délai imparti pour l'introduction d'un recours contentieux, d'un recours gracieux ou hiérarchique qui interrompt le cours de ce délai. Les dispositions de l'article L. 424-5 du code de l'urbanisme, qui limitent le délai pendant lequel une autorisation de construire peut être retirée, spontanément ou à la demande d'un tiers, par l'autorité qui l'a délivrée, n'ont ni pour objet ni pour effet de faire obstacle, d'une part, à ce que le représentant de l'Etat puisse former un recours gracieux, jusqu'à l'expiration du délai dont il dispose pour déférer un tel acte au tribunal administratif, et, d'autre part, à ce que le cours de ce délai soit interrompu par ce recours gracieux.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
