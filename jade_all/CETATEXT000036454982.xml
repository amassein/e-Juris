<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036454982</ID>
<ANCIEN_ID>JG_L_2018_01_000000395542</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/45/49/CETATEXT000036454982.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème chambre, 10/01/2018, 395542, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-01-10</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>395542</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MATUCHANSKY, POUPOT, VALDELIEVRE ; SCP NICOLAY, DE LANOUVELLE, HANNOTIN</AVOCATS>
<RAPPORTEUR>M. Laurent Huet</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Sophie-Justine Lieber</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:395542.20180110</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés le 23 décembre 2015, le 23 mars 2016 et le 5 janvier 2017 au secrétariat du contentieux du Conseil d'Etat, Mme A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision n° 342 bis du 20 octobre 2015 du Conseil national de l'ordre des médecins, statuant en formation restreinte, rejetant sa demande de reprise d'exercice professionnel,  prononçant une nouvelle suspension de son droit d'exercer la médecine pendant une durée de trois ans et subordonnant la reprise de son droit d'exercer la médecine aux résultats d'une nouvelle expertise ;<br/>
<br/>
              2°) de mettre à la charge du Conseil national de l'ordre des médecins la somme de 4 200 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de la santé publique ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Laurent Huet, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Sophie-Justine Lieber, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Nicolaÿ, de Lanouvelle, Hannotin, avocat de Mme A...et à la SCP Matuchansky, Poupot, Valdelièvre, avocat du Conseil national de l'ordre des médecins ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'aux termes de l'article R. 4124-3 du code de la santé publique : " I. - Dans le cas d'infirmité ou d'état pathologique rendant dangereux l'exercice de la profession, la suspension temporaire du droit d'exercer est prononcée par le conseil régional ou interrégional pour une période déterminée, qui peut, s'il y a lieu, être renouvelée. (...) / II. - La suspension ne peut être ordonnée que sur un rapport motivé établi à la demande du conseil régional ou interrégional par trois médecins spécialistes désignés comme experts, désignés l'un par l'intéressé, le deuxième par le conseil régional ou interrégional et le troisième par les deux premiers experts.  (...) " ; qu'aux termes de l'article R. 4124-3-4 du même code : "Le praticien qui a fait l'objet d'une mesure de suspension du droit d'exercer ne peut reprendre son exercice sans que le conseil régional ou interrégional ait fait procéder, à la demande de l'intéressé, par des experts désignés selon les modalités définies aux II, III et IV de l'article R. 4124-3, à une nouvelle expertise, dans les deux mois qui précèdent l'expiration de la période de suspension.(...) / Si le rapport d'expertise est favorable à la reprise de l'exercice professionnel, le conseil régional ou interrégional peut décider que le praticien est apte à exercer sa profession et en informe les autorités qui avaient reçu notification de la suspension. S'il estime ne pas pouvoir suivre l'avis favorable des experts ou si l'expertise est défavorable à la reprise de l'exercice professionnel, le conseil régional ou interrégional prononce une nouvelle suspension temporaire. / La décision du conseil régional ou interrégional peut être contestée devant le Conseil national. " ;<br/>
<br/>
              2. Considérant qu'il ressort des pièces du dossier que MmeA..., médecin qualifié spécialiste en ophtalmologie, a fait l'objet, depuis le 2 octobre 2012, de mesures successives de suspension temporaire de son droit d'exercer la médecine prononcées par la formation restreinte du conseil régional du Limousin de l'ordre des médecins sur le fondement des dispositions citées au point 1  ; qu'au vu d'une nouvelle expertise réalisée le 21 janvier 2015, la formation restreinte du conseil régional du Limousin de l'ordre de médecins a, par une décision du 23 avril 2015, prononcé une mesure d'interdiction définitive du droit d'exercer une activité médicale ; que cette décision a été annulée, sur le recours de MmeA..., par une décision en date du 2 juin 2015 du Conseil national de l'ordre des médecins, qui a également ordonné une nouvelle expertise ; qu'au vu de cette expertise, réalisée le 17 juillet 2015, le Conseil national de l'ordre des médecins a, par une décision en date du 20 octobre 2015, rejeté la demande de reprise d'activité de MmeA..., prononcé à son encontre une nouvelle suspension du droit d'exercer pour une durée de trois ans, et subordonné la reprise de son activité de médecin aux résultats d'une nouvelle expertise ; que Mme A...demande l'annulation, par la voie de l'excès de pouvoir, de cette dernière décision ;<br/>
<br/>
              3. Considérant, en premier lieu, que la décision attaquée, qui indique l'ensemble des motifs qui sont de nature à établir que l'état de santé de Mme A...est incompatible avec l'exercice de la médecine et à justifier une mesure de suspension d'une durée de trois ans, est, contrairement à ce que soutient MmeA..., suffisamment motivée en fait  ; qu'en outre, s'agissant d'une décision administrative et non d'une décision juridictionnelle, la requérante ne saurait invoquer, au soutien de son moyen d'insuffisance de motivation, que le dispositif de la décision attaquée ne reprendrait pas certains éléments figurant dans ses motifs ; <br/>
<br/>
              4. Considérant, en deuxième lieu, qu'il ne ressort pas des pièces du dossier que les experts aient révélé, dans leur rapport, des éléments relatifs à Mme A...qui n'auraient pas été en rapport avec la mission qui leur était confiée ; que, par suite, le moyen tiré de ce que la procédure d'expertise aurait été irrégulière pour ce motif ne peut, en tout état de cause, qu'être écarté ; <br/>
<br/>
              5. Considérant, en troisième lieu, qu'il ressort des pièces du dossier que la personne ayant représenté le conseil départemental de l'ordre des médecins de la Haute-Vienne lors de la séance de la formation restreinte du Conseil national de l'ordre des médecins était membre de ce conseil départemental et avait, par suite, qualité pour le représenter ; qu'en outre, il a pu régulièrement y être assisté par un avocat, comme le prévoient les dispositions du dernier alinéa de l'article R. 4124-3-1 du code de la santé publique auxquelles renvoie l'article R. 4124-3-3 du même code ; que, par suite, le moyen tiré de ce que la procédure aurait été viciée par la présence irrégulière de ces deux personnes lors de la séance de la formation restreinte du Conseil national de l'ordre des médecins doit être écarté ; <br/>
<br/>
              6. Considérant, en dernier lieu, qu'en estimant, au vu de l'ensemble des pièces du dossier, notamment des conclusions des rapports d'expertise des 27 janvier et 17 juillet 2015, et alors que Mme A...n'apporte aucun élément permettant de retenir que son état de santé s'était, à la date de la décision attaquée, amélioré, que Mme A...présentait un état pathologique rendant dangereux l'exercice de sa profession et que son droit d'exercer la médecine devait, à nouveau, être suspendu, le Conseil national de l'ordre des médecins a fait une exacte application des dispositions de l'article R. 4124-3 du code de la santé publique ; qu'en prononçant cette suspension pour une durée de trois années, il n'a pas entaché sa décision d'une erreur manifeste appréciation ; <br/>
<br/>
              7. Considérant qu'il résulte de l'ensemble de ce qui précède que la requête de Mme A...doit être rejetée, y compris ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er : La requête de Mme A... est rejetée.<br/>
Article 2 : La présente décision sera notifiée à Mme B... A...et au Conseil national de l'ordre des médecins.<br/>
Copie en sera adressée au conseil départemental de la Haute-Vienne de l'ordre des médecins.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
