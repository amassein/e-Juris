<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026535724</ID>
<ANCIEN_ID>JG_L_2012_10_000000354489</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/53/57/CETATEXT000026535724.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  4ème sous-section jugeant seule, 24/10/2012, 354489, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2012-10-24</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>354489</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 4ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Marc Dandelot</PRESIDENT>
<AVOCATS/>
<RAPPORTEUR>M. Christophe Eoche-Duval</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Gaëlle Dumortier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2012:354489.20121024</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la requête sommaire et le mémoire complémentaire, enregistrés le 1er décembre 2011 et le 31 janvier 2012 au secrétariat du contentieux du Conseil d'Etat, présentés par la société Hoss, dont le siège social est avenue du Château d'Eau à Fonsorbes (31470), représentée par son président directeur général en exercice ; la société Hoss demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler la décision n° 1010 T du 12 octobre 2011 par laquelle la Commission nationale d'aménagement commercial a accordé à la société de promotion construction MG l'autorisation préalable requise en vue de créer un ensemble commercial " Les Portes du Gers" de 13 280 m² de surface totale de vente composé d'un hypermarché " Carrefour Market" de 4 000 m², d'une galerie marchande de 964 m², d'un magasin de bricolage de 3 616 m² et de 9 magasins spécialisés pour une surface de 4 700 m², à Fonsorbes (Haute-Garonne) ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat et de la société de promotion construction MG la somme de 2 000 euros chacun au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 21 septembre 2012 présentée par la société de promotion construction MG ; <br/>
<br/>
              Vu le code de commerce ;<br/>
<br/>
              Vu la loi n° 73-1193 du 27 décembre 1973 ;<br/>
<br/>
              Vu la loi n° 2008-776 du 4 août 2008 ;<br/>
<br/>
              Vu le décret n° 2008-1212 du 24 novembre 2008 ;<br/>
<br/>
              Vu le décret n° 2011-921 du 1er août 2011 ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Christophe Eoche-Duval, Conseiller d'Etat,<br/>
<br/>
              - les conclusions de Mme Gaëlle Dumortier, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
              Sur la procédure suivie devant la Commission nationale d'aménagement commercial :<br/>
<br/>
              1. Considérant qu'aux termes du quatrième alinéa de l'article R. 752-51 du code de commerce : " Le commissaire du gouvernement recueille les avis des ministres intéressés, qu'il présente à la commission nationale " ; qu'aux termes du deuxième alinéa de l'article R. 752-16 du même code : " Pour les projets d'aménagement commercial, l'instruction des demandes est effectuée conjointement par les services territorialement compétents chargés du commerce ainsi que ceux chargés de l'urbanisme et de l'environnement. " ; <br/>
<br/>
              2. Considérant qu'il résulte de la combinaison de ces dispositions que les ministres intéressés, au sens de l'article R. 752-51 du code de commerce, sont ceux qui ont autorité sur les services chargés d'instruire les demandes, soit les ministres en charge du commerce, de l'urbanisme et de l'environnement ; qu'en l'espèce, il ressort des pièces du dossier que les avis de ces ministres, qui sont signés par des personnes dûment habilitées à cet effet, ont bien été présentés à la commission ; que le moyen tiré de ce que l'avis d'autres ministres n'a pas été recueilli est inopérant ;<br/>
<br/>
              Sur le moyen tiré du défaut de titre habilitant le pétitionnaire à présenter une demande d'autorisation : <br/>
<br/>
              3. Considérant qu'aux termes de l'article R. 752-6 du code de commerce : " La demande d'autorisation prévue à l'article L. 752-1 (...) est présentée soit par le propriétaire de l'immeuble, soit par une personne justifiant d'un titre l'habilitant à construire sur le terrain ou à exploiter commercialement l'immeuble. " ; <br/>
<br/>
              4. Considérant que si la requérante soutient que la décision attaquée serait illégale en ce que la société de promotion construction MG ne justifierait pas d'un titre l'habilitant à présenter une demande d'autorisation, il ressort des pièces du dossier que la demande d'autorisation était accompagnée, d'une part, d'extraits des registres du cadastre relatifs aux terrains d'emprise du projet sur lesquels figurent le nom du pétitionnaire et celui de la commune de Fonsorbes et, d'autre part, d'une promesse de vente par laquelle cette dernière s'est engagée à céder à la société de promotion construction MG les parcelles lui appartenant concernées par le projet ; que, contrairement à ce que soutient la requérante, la commission nationale pouvait se fonder sur ces éléments pour retenir que le pétitionnaire justifiait bien d'un titre au sens de l'article R. 752-6 du code de commerce ;<br/>
<br/>
              Sur la délimitation de la zone de chalandise :<br/>
<br/>
              5. Considérant qu'aux termes de l'article R. 752-7 du code de commerce : " La demande est accompagnée : (...) / 2° Des renseignements suivants : / a) Délimitation de la zone de chalandise du projet, telle que définie à l'article R. 752-8, et mention de la population de chaque commune comprise dans cette zone ainsi que de son évolution entre les deux derniers recensements authentifiés par décret " ; <br/>
<br/>
              6. Considérant que si la requérante soutient que la décision attaquée serait illégale en raison de l'inexacte délimitation de la zone de chalandise définie dans le dossier de demande, il ne ressort pas des pièces du dossier que cette zone, qui a été délimitée pour tenir compte de plusieurs ensembles commerciaux à proximité du site et qui n'a pas été remise en cause par les services instructeurs, soit erronée ; <br/>
<br/>
              Sur la composition du dossier de demande :<br/>
<br/>
              7. Considérant qu'aux termes du II de l'article R. 752-7 du code de commerce : " La demande est également accompagnée d'une étude destinée à permettre à la commission d'apprécier les effets prévisibles du projet au regard des critères prévus par l'article L. 752-6. Celle-ci comporte les éléments permettant d'apprécier les effets du projet sur : (...) ; 2° Les flux de voitures particulières et de véhicules de livraison ainsi que sur les accès sécurisés à la voie publique ; (...) " ; que si la requérante soutient que le dossier de demande d'autorisation serait incomplet s'agissant de l'impact du projet sur les flux de véhicules, des consommations énergétiques du site et de son insertion dans l'environnement, il ressort des pièces du dossier que les éléments fournis par le pétitionnaire étaient suffisants pour permettre à la commission nationale d'apprécier la conformité du projet aux objectifs fixés par le législateur ; <br/>
<br/>
              Sur l'appréciation de la commission nationale :<br/>
<br/>
              8. Considérant qu'il appartient aux commissions d'aménagement commercial, lorsqu'elles se prononcent sur un projet d'exploitation commerciale soumis à autorisation en application de l'article L. 752-1 du code de commerce, d'apprécier la conformité de ce projet aux objectifs prévus à l'article 1er de la loi du 27 décembre 1973 et à l'article L. 750-1 du code de commerce, au vu des critères d'évaluation mentionnés à l'article L. 752-6 du même code ; que l'autorisation ne peut être refusée que si, eu égard à ses effets, le projet compromet la réalisation de ces objectifs ;<br/>
<br/>
              9. Considérant que la requérante soutient que la décision attaquée aurait méconnu l'objectif fixé par le législateur en matière d'aménagement du territoire en raison de la localisation du projet hors du centre-ville et de son impact excessif sur les flux de transport, il ressort des pièces du dossier que l'ensemble commercial autorisé, situé en continuité d'urbanisation et à proximité d'habitations et d'équipements publics, est de nature à contribuer à l'animation de la commune de Fonsorbes ; que des travaux de voirie sont en cours de réalisation en vue d'adapter la capacité des infrastructures routières desservant le site à l'accroissement des flux de circulation ;<br/>
<br/>
              10. Considérant que si le requérant soutient que la décision attaquée aurait méconnu l'objectif fixé par le législateur en matière de développement durable en raison de l'insuffisance de la qualité environnementale du projet et sa mauvaise insertion dans les réseaux de transports collectifs, il ressort des pièces du dossier que des dispositifs permettant la maîtrise des consommations énergétiques et le traitement des eaux et des déchets sont prévus par le pétitionnaire et que des espaces verts seront aménagés autour de l'ensemble commercial ; que le site du projet, qui est accessible par plusieurs lignes de bus, est suffisamment desservi par les transports collectifs, compte tenu notamment de sa localisation à proximité du centre-ville de Fonsorbes ; <br/>
<br/>
              Sur le moyen tiré de la méconnaissance de l'article L. 122-2 du code de l'urbanisme : <br/>
              11. Considérant qu'aux termes de l'article L. 122-2 du code de l'urbanisme, dans sa version alors en vigueur  : " Dans les communes qui sont situées à moins de quinze kilomètres de la périphérie d'une agglomération de plus de 50 000 habitants au sens du recensement général de la population, ou à moins de quinze kilomètres du rivage de la mer, et qui ne sont pas couvertes par un schéma de cohérence territoriale applicable, le plan local d'urbanisme ne peut être modifié ou révisé en vue d'ouvrir à l'urbanisation une zone à urbaniser délimitée après le 1er juillet 2002 ou une zone naturelle. / Dans les communes mentionnées au premier alinéa et à l'intérieur des zones à urbaniser ouvertes à l'urbanisation après l'entrée en vigueur de la loi n° 2003-590 du 2 juillet 2003 urbanisme et habitat, il ne peut être délivré d'autorisation d'exploitation commerciale en application de l'article L. 752-1 du code de commerce ou l'autorisation prévue aux articles L. 212-7 et L. 212-8 du code du cinéma et de l'image animée. / Il peut être dérogé aux dispositions des deux alinéas précédents soit avec l'accord du préfet [donné après avis de la commission départementale compétente en matière de nature, de paysages et de sites et de la chambre d'agriculture,] soit, lorsque le périmètre d'un schéma de cohérence territoriale incluant la commune a été arrêté, avec l'accord de l'établissement public prévu à l'article L. 122-4. La dérogation ne peut être refusée que si les inconvénients éventuels de l'urbanisation envisagée pour les communes voisines, pour l'environnement ou pour les activités agricoles sont excessifs au regard de l'intérêt que représente pour la commune la modification ou la révision du plan. " ; <br/>
<br/>
              12. Considérant qu'il résulte de ces dispositions que le porteur d'un projet d'urbanisme commercial ne peut solliciter d'autorisation d'urbanisme commercial d'un projet situé à l'intérieur des zones à urbaniser sur le territoire des communes non couvertes d'un schéma de cohérence territoriale et qui répondent aux autres critères mentionnés à l'article L. 122-2 du code de l'urbanisme que s'il bénéficie d'une dérogation délivrée par le préfet ou par l'établissement public auteur du schéma de cohérence territoriale ayant vocation à entrer en application ; <br/>
<br/>
              13. Considérant que par une délibération du 9 juillet 2010, le syndicat mixte d'études pour entreprendre et mettre en oeuvre le schéma de cohérence territoriale de l'agglomération toulousaine a accordé à la société promotion construction GM une dérogation lui permettant de solliciter une autorisation d'urbanisme commercial pour la création d'un ensemble commercial de 13 280 m² à Fonsorbes au motif que les caractéristiques générales du projet commercial " Les portes du Gers " sont compatibles avec les  enjeux et les orientations identifiés pour ce territoire par le futur schéma de cohérence territoriale ; qu'il ressort des pièces du dossier que les caractéristiques du projet qui a fait l'objet de la dérogation délivrée à la société promotion construction GM par le syndicat mixte d'études sont identiques à celles prévues par le projet de cette société soumis à la demande d'autorisation d'urbanisme commerciale ; qu'ainsi, la requérante n'est pas fondée à soutenir que la commission nationale aurait fait une inexacte application des dispositions précitées du code de l'urbanisme en autorisant cette société à procéder à l'ouverture de son projet ;<br/>
<br/>
              14. Considérant qu'il résulte de ce qui précède que la société Hoss n'est pas fondée à demander l'annulation de la décision attaquée ; <br/>
<br/>
              Sur les conclusions tendant à l'application des dispositions de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              15. Considérant que ces dispositions font obstacle à ce qu'une somme soit mise à la charge de la société de promotion construction MG, qui n'est pas, dans la présente instance, la partie perdante ; qu'en revanche, il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la société Hoss la somme de 3 000 euros à verser à la société de promotion construction MG, au titre de ces dispositions ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de la société Hoss est rejetée.<br/>
<br/>
Article 2 : Les conclusions de la société Hoss tendant à l'application des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La société Hoss versera à la société de promotion construction MG la somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative. <br/>
Article 4 : La présente décision sera notifiée à la société Hoss, à la société de promotion construction MG et à la Commission nationale d'aménagement commercial.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
