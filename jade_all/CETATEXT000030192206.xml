<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030192206</ID>
<ANCIEN_ID>JG_L_2015_02_000000366861</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/19/22/CETATEXT000030192206.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème / 9ème SSR, 04/02/2015, 366861</TITRE>
<DATE_DEC>2015-02-04</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>366861</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème / 9ème SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DE CHAISEMARTIN, COURJON ; SCP LEVIS</AVOCATS>
<RAPPORTEUR>M. Timothée Paris</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Aurélie Bretonneau</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:366861.20150204</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 15 mars 2013 et 17 juin 2013 au secrétariat du contentieux du Conseil d'Etat, présentés pour la commune de Sarzeau, représentée par son maire ; la commune de Sarzeau demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler le jugement n° 1002813 du 15 janvier 2013 par lequel le tribunal administratif de Rennes a annulé pour excès de pouvoir l'arrêté du 2 juin 2010 par lequel le maire de Sarzeau s'est opposé à la déclaration préalable de M. A...B...; <br/>
<br/>
              2°) de mettre à la charge de M. B...la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la Constitution, notamment son Préambule ;<br/>
<br/>
              Vu la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
<br/>
              Vu le code de l'urbanisme ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Timothée Paris, maître des requêtes,<br/>
<br/>
              - les conclusions de Mme Aurélie Bretonneau, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Lévis, avocat de la commune de Sarzeau et à la SCP de Chaisemartin, Courjon, avocat de M. B... ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que M.B..., propriétaire sur la commune de Sarzeau (Morbihan), a déposé une déclaration préalable en vue de régulariser l'édification d'une clôture en limite de sa propriété ; que, par un arrêté du 2 juin 2010, la commune s'est opposée à ce projet au motif qu'il méconnaissait la servitude de passage des piétons approuvé par l'arrêté préfectoral du 19 février 2001, pris sur le fondement des articles L. 160-6 à L. 160-8 du code de l'urbanisme ; que la commune de Sarzeau se pourvoit en cassation contre le jugement du 15 janvier 2013 par lequel le tribunal administratif de Rennes a fait droit à la  requête de  M. B...tendant à obtenir l'annulation pour excès de pouvoir de l'arrêté du 2 juin 2010 ; <br/>
<br/>
              2. Considérant qu'aux termes du premier alinéa de l'article L. 160-6 du code de l'urbanisme : " Les propriétés privées riveraines du domaine public maritime sont grevées sur une bande de trois mètres de largeur d'une servitude destinée à assurer exclusivement le passage des piétons " ; qu'aux termes du premier alinéa de l'article L. 160-6-1 du même code : " Une servitude de passage des piétons, transversale au rivage peut être instituée sur les voies et chemins privés d'usage collectif existants, à l'exception de ceux réservés à un usage professionnel selon la procédure prévue au deuxième alinéa de l'article L. 160-6 " ; que l'autorité administrative peut, par décision motivée prise après avis du ou des conseils municipaux intéressés et au vu du résultat d'une enquête publique, modifier le tracé de la servitude prévue à l'article L. 160-6 du code précité ou la suspendre et instituer ou modifier la servitude prévue à l'article L. 160-6-1 de ce code ; qu'en l'absence d'opposition des communes intéressées, cette décision revêt la forme d'un arrêté du préfet ; que l'article R. 160-22 du code de l'urbanisme prévoit que l'acte d'approbation du tracé et des caractéristiques d'une servitude : " (...) fait l'objet /: (...) b) d'une publication au recueil des actes administratifs de la ou des préfectures intéressées, s'il s'agit d'un arrêté préfectoral. / Une copie de cet acte est déposée à la mairie de chacune des communes concernées. Avis de ce dépôt est donné par affichage à la mairie pendant un mois. Mention de cet acte est insérée en caractères apparents dans deux journaux régionaux ou locaux diffusés dans le ou les départements concernés " ; que si ce même article précise, dans son dernier alinéa, que " cet acte fait en outre l'objet de la publicité prévue au 2° de l'article 36 du décret n° 55-22 du 4 janvier 1955 ", cette obligation faite à l'administration, dans l'intérêt de l'information des usagers, de publier au service chargé de la publicité foncière les décisions relatives à une servitude de passage n'est pas une condition de l'opposabilité de ces décisions, qui est subordonnée au seul respect des autres mesures de publicité qu'il prescrit ; que, dès lors qu'aucune autre disposition, ni aucun principe n'impose à l'autorité administrative de notifier au propriétaire concerné l'arrêté par lequel elle institue ou modifie une des servitudes prévues aux articles L. 160-6 et L. 160-6-1 du code de l'urbanisme, le défaut de notification individuelle d'un tel arrêté, s'il est de nature à faire obstacle au déclenchement du délai de recours contentieux à l'égard de ce propriétaire, est sans effet sur son opposabilité ; qu'il suit de là que le tribunal administratif de Rennes qui, contrairement à ce qui est soutenu, n'était pas lié, sur ce point, par le jugement rendu par le tribunal correctionnel de Vannes le 15 janvier 2013 sur les poursuites engagées contre M. B...pour n'avoir pas respecté les règle applicables en matière de servitudes, a entaché son jugement d'erreur de droit en estimant que l'arrêté préfectoral du 19 février 2001 ne pouvait servir de fondement à l'arrêté du 2 juin 2010 dès lors qu'il n'était pas opposable à M.B..., en l'absence de notification individuelle et alors même qu'il n'était pas contesté qu'il avait fait l'objet des mesures de publicité prévues par l'article R. 160-22 précité ;<br/>
<br/>
              3. Considérant qu'il résulte de ce qui précède que, sans qu'il soit besoin d'examiner les autres moyens de son pourvoi, la commune de Sarzeau est fondée à demander l'annulation du jugement du tribunal administratif de Rennes ; <br/>
<br/>
              4. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de M. B...le versement de la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative ; qu'en revanche, les dispositions de cet article font obstacle à ce que soit mise à la charge de la commune, qui n'est pas la partie perdante dans la présente instance, la somme demandée au même titre par M. B...;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du 15 janvier 2013 du tribunal administratif de Rennes est annulé.<br/>
Article 2 : L'affaire est renvoyée devant le tribunal administratif de Rennes.<br/>
Article 3 : M. B...versera à la commune de Sarzeau la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Les conclusions de M. B...au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée à la commune de Sarzeau et à M. A...B....<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-07-03-04 ACTES LÉGISLATIFS ET ADMINISTRATIFS. PROMULGATION - PUBLICATION - NOTIFICATION. NOTIFICATION. EFFETS D'UN DÉFAUT DE NOTIFICATION. - ARRÊTÉ INSTITUANT OU MODIFIANT UNE SERVITUDE TRANSVERSALE DE PASSAGE SUR LE LITTORAL (ART. L. 160-6 ET L. 160-6-1 DU CODE DE L'URBANISME) - DÉFAUT DE NOTIFICATION AU PROPRIÉTAIRE CONCERNÉ - OBSTACLE AU DÉCLENCHEMENT DU DÉLAI DE RECOURS - EXISTENCE - OBSTACLE À L'OPPOSABILITÉ - ABSENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">26-04-01-01-03 DROITS CIVILS ET INDIVIDUELS. DROIT DE PROPRIÉTÉ. SERVITUDES. INSTITUTION DES SERVITUDES. SERVITUDES DE PASSAGE SUR LE LITTORAL. - ARRÊTÉ INSTITUANT OU MODIFIANT UNE SERVITUDE TRANSVERSALE DE PASSAGE (ART. L. 160-6 ET L. 160-6-1 DU CODE DE L'URBANISME) - DÉFAUT DE NOTIFICATION AU PROPRIÉTAIRE CONCERNÉ - OBSTACLE AU DÉCLENCHEMENT DU DÉLAI DE RECOURS - EXISTENCE - OBSTACLE À L'OPPOSABILITÉ - ABSENCE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">54-01-07-02-01 PROCÉDURE. INTRODUCTION DE L'INSTANCE. DÉLAIS. POINT DE DÉPART DES DÉLAIS. NOTIFICATION. - ARRÊTÉ INSTITUANT OU MODIFIANT UNE SERVITUDE TRANSVERSALE DE PASSAGE SUR LE LITTORAL (ART. L. 160-6 ET L. 160-6-1 DU CODE DE L'URBANISME) - DÉFAUT DE NOTIFICATION AU PROPRIÉTAIRE CONCERNÉ - OBSTACLE AU DÉCLENCHEMENT DU DÉLAI DE RECOURS - EXISTENCE - OBSTACLE À L'OPPOSABILITÉ - ABSENCE.
</SCT>
<SCT ID="8D" TYPE="PRINCIPAL">68-06-01-03-01 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. RÈGLES DE PROCÉDURE CONTENTIEUSE SPÉCIALES. INTRODUCTION DE L'INSTANCE. DÉLAIS DE RECOURS. POINT DE DÉPART DU DÉLAI. - ARRÊTÉ INSTITUANT OU MODIFIANT UNE SERVITUDE TRANSVERSALE DE PASSAGE SUR LE LITTORAL (ART. L. 160-6 ET L. 160-6-1 DU CODE DE L'URBANISME) - DÉFAUT DE NOTIFICATION AU PROPRIÉTAIRE CONCERNÉ - OBSTACLE AU DÉCLENCHEMENT DU DÉLAI DE RECOURS - EXISTENCE - OBSTACLE À L'OPPOSABILITÉ - ABSENCE.
</SCT>
<ANA ID="9A"> 01-07-03-04 Les dispositions de l'article R. 160-22 du code de l'urbanisme prévoient les diverses modalités de publicité de l'arrêté par lequel est instituée ou modifiée une des servitudes transversales de passage prévues aux articles L. 160-6 et L. 160-6-1 du code de l'urbanisme. Dès lors qu'aucune disposition, ni aucun principe n'impose à l'autorité administrative de notifier au propriétaire concerné un tel arrêté, le défaut de notification individuelle d'un tel arrêté, s'il est de nature à faire obstacle au déclenchement du délai de recours contentieux à l'égard de ce propriétaire, est sans effet sur son opposabilité.</ANA>
<ANA ID="9B"> 26-04-01-01-03 Les dispositions de l'article R. 160-22 du code de l'urbanisme prévoient les diverses modalités de publicité de l'arrêté par lequel est instituée ou modifiée une des servitudes transversales de passage prévues aux articles L. 160-6 et L. 160-6-1 du code de l'urbanisme. Dès lors qu'aucune disposition, ni aucun principe n'impose à l'autorité administrative de notifier au propriétaire concerné un tel arrêté, le défaut de notification individuelle d'un tel arrêté, s'il est de nature à faire obstacle au déclenchement du délai de recours contentieux à l'égard de ce propriétaire, est sans effet sur son opposabilité.</ANA>
<ANA ID="9C"> 54-01-07-02-01 Les dispositions de l'article R. 160-22 du code de l'urbanisme prévoient les diverses modalités de publicité de l'arrêté par lequel est instituée ou modifiée une des servitudes transversales de passage prévues aux articles L. 160-6 et L. 160-6-1 du code de l'urbanisme. Dès lors qu'aucune disposition, ni aucun principe n'impose à l'autorité administrative de notifier au propriétaire concerné un tel arrêté, le défaut de notification individuelle d'un tel arrêté, s'il est de nature à faire obstacle au déclenchement du délai de recours contentieux à l'égard de ce propriétaire, est sans effet sur son opposabilité.</ANA>
<ANA ID="9D"> 68-06-01-03-01 Les dispositions de l'article R. 160-22 du code de l'urbanisme prévoient les diverses modalités de publicité de l'arrêté par lequel est instituée ou modifiée une des servitudes transversales de passage prévues aux articles L. 160-6 et L. 160-6-1 du code de l'urbanisme. Dès lors qu'aucune disposition, ni aucun principe n'impose à l'autorité administrative de notifier au propriétaire concerné un tel arrêté, le défaut de notification individuelle d'un tel arrêté, s'il est de nature à faire obstacle au déclenchement du délai de recours contentieux à l'égard de ce propriétaire, est sans effet sur son opposabilité.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
