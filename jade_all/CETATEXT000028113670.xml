<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028113670</ID>
<ANCIEN_ID>JG_L_2013_10_000000348116</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/11/36/CETATEXT000028113670.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  9ème sous-section jugeant seule, 24/10/2013, 348116, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-10-24</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>348116</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 9ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR>Mme Séverine Larere</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Aladjidi</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2013:348116.20131024</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 4 avril et 4 juillet 2011 au secrétariat du contentieux du Conseil d'État, présentés pour Mme A...B..., demeurant... ; Mme B...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 09PA04165 du 27 janvier 2011 de la cour administrative d'appel de Paris, réformant le jugement n° 0317109-0605657 et 0720414/2 du 11 mai 2009 du tribunal administratif de Paris, en tant qu'il n'a que partiellement fait droit à sa demande tendant à la décharge des cotisations supplémentaires d'impôt sur le revenu et de contributions sociales auxquelles elle a été assujettie au titre des années 1999 à 2000 ainsi que des pénalités correspondantes ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat le versement de la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code général des impôts et le livre des procédures fiscales ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Séverine Larere, Maître des Requêtes,  <br/>
<br/>
              - les conclusions de M. Frédéric Aladjidi, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Piwnica, Molinié, avocat de Mme B... ;<br/>
<br/>
<br/>
<br/>1. Considérant, en premier lieu, qu'aux termes de l'article L. 10 du livre des procédures fiscales, dans sa rédaction applicable aux années d'imposition en litige : " L'administration des impôts contrôle les déclarations ainsi que les actes utilisés pour l'établissement des impôts, droits, taxes et redevances. / (...) A cette fin, elle peut demander aux contribuables tous renseignements, justifications ou éclaircissements relatifs aux déclarations souscrites ou aux actes déposés. (...) " ; qu'aux termes de l'article L. 12 du même livre : " Dans les conditions prévues au présent livre, l'administration des impôts peut procéder à l'examen contradictoire de la situation fiscale des personnes physiques au regard de l'impôt sur le revenu, qu'elles aient ou non leur domicile fiscal en France, lorsqu'elles y ont des obligations au titre de cet impôt. / A l'occasion de cet examen, l'administration peut contrôler la cohérence entre, d'une part les revenus déclarés et, d'autre part, la situation patrimoniale, la situation de trésorerie et les éléments du train de vie des membres du foyer fiscal. (...) " ; que, pour écarter le moyen de Mme B...tiré de ce que, sous couvert d'un contrôle sur pièces des déclarations qu'elle avait déposées au titre des années 1999, 2000 et 2001, l'administration fiscale aurait procédé à un examen de sa situation fiscale personnelle, sans l'accompagner des garanties dont cette procédure est assortie, la cour administrative d'appel de Paris a relevé, d'une part, que l'administration ne s'était pas livrée à l'occasion de ces différents contrôles sur pièces à un contrôle de la cohérence entre les revenus déclarés par elle et sa situation patrimoniale, sa situation de trésorerie ou les éléments de son train de vie et, d'autre part, que la triple circonstance que le service lui a adressé des mises en demeure de déposer des déclarations de plus-values de cessions de valeurs mobilières ou de revenus de capitaux mobiliers, qu'elle se soit rendue à de nombreuses reprises dans les locaux des services fiscaux à son initiative et qu'elle aurait pu constater à cette occasion la présence de chemises portant la mention " ESFP " n'était pas de nature à établir que les opérations de contrôle auraient excédé l'ampleur d'un contrôle sur pièces pour revêtir la nature d'un examen contradictoire de situation fiscale personnelle ; que la cour a ainsi exactement qualifié les faits soumis à son examen, qu'elle a souverainement constatés sans les dénaturer, en jugeant que ces faits ne caractérisaient pas un examen contradictoire de la situation fiscale personnelle de Mme B..., lequel implique un contrôle de cohérence global entre l'ensemble des revenus déclarés par les contribuables et leur situation de trésorerie, leur situation patrimoniale ou leur train de vie ;<br/>
<br/>
              3. Considérant, en second lieu, que si Mme B...soutient que la cour a commis une erreur de droit et dénaturé les pièces du dossier qui lui était soumis et, notamment, le jugement du tribunal de grande instance de Paris du 25 février 1999, en jugeant que ces pièces ne permettaient pas d'établir le montant des frais d'expertise se rapportant à la fixation de la prestation compensatoire dont elle bénéficie, faute que les frais en question puissent être distingués d'autres frais occasionnés par la procédure de divorce engagée devant ce tribunal, il ressort de ce même dossier que Mme B...n'a fourni ni la copie du jugement du 25 février 1999 dans son intégralité, ni celle du rapport d'expertise, qui auraient permis au juge du fond de vérifier que l'expertise en question avait effectivement pour seul objet, comme elle le soutenait, la fixation du montant de sa prestation compensatoire ; qu'ainsi, Mme B...n'est pas fondée à soutenir que la cour, qui n'a pas commis d'erreur de droit, aurait entaché son arrêt de dénaturation ; <br/>
<br/>
              4. Considérant qu'il résulte de ce qui précède que le pourvoi de Mme B...doit être rejeté, y compris ses conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : Le pourvoi de Mme B...est rejeté.<br/>
Article 2 : La présente décision sera notifiée à Mme A...B...et au ministre de l'économie et des finances.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
