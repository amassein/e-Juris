<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030704423</ID>
<ANCIEN_ID>JG_L_2015_06_000000373464</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/70/44/CETATEXT000030704423.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème SSJS, 03/06/2015, 373464, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-06-03</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>373464</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème SSJS</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>HAAS ; SCP BARADUC, DUHAMEL, RAMEIX</AVOCATS>
<RAPPORTEUR>M. Bastien Lignereux</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Marie-Astrid Nicolazo de Barmon</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2015:373464.20150603</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La SCI Yerres Développement a demandé au tribunal administratif de Versailles de lui accorder le remboursement des droits de la taxe locale d'équipement acquittés au titre d'un permis de construire délivré par un arrêté du 11 février 1999, la décharge du reliquat de cette taxe, la compensation entre la somme de 52 000 euros correspondant au montant de taxe locale d'équipement déjà versé et les cotisations dues au titre des taxes foncières d'un montant de 44 000 euros, la décharge des cotisations correspondant à la totalité des majorations et pénalités de retard se rapportant aux taxes déjà versées et des cotisations de la redevance pour la construction de locaux à usage de bureaux, le sursis de paiement desdites taxes et de prononcer l'annulation de la décision du maire de la commune de Yerres du 26 janvier  2007 portant refus de retrait du permis de construire qui lui a été délivré le 11 février 1999.<br/>
<br/>
              Par un jugement n° 0807428 du 28 décembre 2012, le tribunal administratif de Versailles a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 13VE00866 du 31 octobre 2013, la cour administrative d'appel de Versailles a, d'une part, transmis au Conseil d'Etat, en application de l'article R. 351-2 du code de justice administrative, le dossier des conclusions de la requête de la SCI Yerres Développement relatives au remboursement des droits de la taxe locale d'équipement qu'elle a acquittés au titre du permis de construire délivré le 11 février 1999 par le maire de Yerres, à la décharge du reliquat de cette taxe, à la compensation entre la somme de 52 000 euros correspondant au montant de la taxe locale d'équipement qu'elle a déjà versé et les cotisations d'un montant de 44 000 euros dues au titre des taxes foncières et à la décharge des cotisations correspondant à la totalité des majorations et pénalités de retard se rapportant aux taxes déjà versées et des cotisations de la redevance pour la construction de locaux à usage de bureaux et, d'autre part, rejeté le surplus des conclusions de la requête de la SCI.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés le 21 février et 21 mai 2014 au secrétariat de la section du contentieux du Conseil d'Etat, la SCI Yerres Développement demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler le jugement n° 0807428 du tribunal administratif de Versailles du 28 décembre 2012 ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat et de la commune de Yerres la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Bastien Lignereux, auditeur,<br/>
<br/>
              - les conclusions de Mme Marie-Astrid Nicolazo de Barmon, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Baraduc, Duhamel, Rameix, avocat de la SCI Yerres Développement et à Me Haas, avocat de la commune de Yerres ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que, par un arrêté du 11 févier 1999, le maire de la commune de Yerres (Essonne) a délivré à la SA Migdal un permis de construire portant sur la construction d'un bâtiment à usage de bureaux et d'entrepôts. Ce permis de construire a été transféré à la SCI Yerres Développement par un arrêté du 26 mars 1999. En conséquence, les sommes dues au titre de la taxe locale d'équipement et des taxes annexes ont été mises à la charge de cette société. Par un arrêté du 6 mars 2003, le maire de Yerres a ordonné l'interruption des travaux au motif qu'ils n'étaient pas conformes au permis de construire délivré. Le bâtiment a fait l'objet d'un arrêté de péril pris par le maire le 27 juin 2006. Par un courrier du 4 janvier 2007, la société requérante a informé le maire de son intention d'abandonner le projet et lui a demandé de retirer le permis de construire du 11 février 1999 afin, notamment, de pouvoir obtenir le remboursement des sommes qu'elle avait versées au titre de la taxe locale d'équipement. Cette demande a été rejetée par un courrier du 26 janvier 2007. Par un courrier du 5 juin 2008, la société a demandé au maire de prendre acte de l'abandon du projet de construction en vue d'interrompre le recouvrement du solde de la taxe locale d'équipement. Cette demande ayant, également, été rejetée, elle a saisi le tribunal administratif de Versailles d'une demande tendant au remboursement des droits de taxe locale d'équipement acquittés au titre du permis de construire délivré par arrêté du 11 février 1999 (52 000 euros), à la décharge du reliquat de cette taxe (17 000 euros), à la compensation entre les sommes déjà versées au titre de ladite taxe et les cotisations dues au titre des taxes foncières, à la décharge des cotisations correspondant à la totalité des majorations et pénalités de retard se rapportant aux taxes déjà versées, à la décharge des cotisations de redevance pour la construction de locaux à usage de bureaux et à l'annulation de la décision du maire de Yerres du 26 janvier 2007 refusant de retirer le permis de construire du 11 février 1999. Par un jugement du 28 décembre 2012, le tribunal administratif de Versailles a rejeté cette demande. La société a contesté ce jugement devant la cour administrative d'appel de Versailles qui, par un arrêt du 31 octobre 2013 a, d'une part, renvoyé au Conseil d'Etat, en application de l'article R. 351-2 du code de justice administrative, le dossier des conclusions de la requête relatives au remboursement des droits de taxe locale d'équipement acquittés au titre du permis de construire, à la décharge du reliquat de cette taxe, à la compensation entre la somme de 52 000 euros et les cotisations dues au titre des taxes foncières et à la décharge des cotisations correspondant à la totalité des majorations et pénalités de retard se rapportant aux taxes déjà versées et des cotisations de la redevance pour la construction de locaux à usage de bureau et, d'autre part, rejeté le surplus des conclusions de la requête de la SCI.<br/>
<br/>
              2. Aux termes de l'article 1723 quinquies du code général des impôts, alors applicable : " Le redevable de la taxe peut en obtenir la décharge, la réduction ou la restitution totale ou partielle : / S'il justifie qu'il n'a pas été en mesure de donner suite à l'autorisation de construire ;  Si, en cas de modification apportée au permis de construire ou à l'autorisation tacite de construire, le constructeur devient redevable d'un montant de taxe inférieur à celui dont il était débiteur ou qu'il a déjà acquitté au titre des constructions précédemment autorisées ; / Si les constructions sont démolies en vertu d'une décision de justice. Toutefois, lorsque la démolition de tout ou partie de constructions faites sans autorisation ou en infraction aux obligations résultant de l'autorisation est ordonnée par décision de justice, la taxe et l'amende fiscale afférentes à ces constructions ne sont pas restituables ". Aux termes de l'article 1723 sexies du même code, alors en vigueur : " Les litiges relatifs à la taxe locale d'équipement sont de la compétence des tribunaux administratifs. / Les réclamations sont présentées, instruites et jugées selon les règles de procédure applicables en matière de contributions directes. L'administration compétente pour statuer sur les réclamations et produire ses observations sur les recours contentieux autres que ceux relatifs au recouvrement, est celle de l'équipement ".<br/>
<br/>
              3. Le tribunal administratif de Versailles a jugé, d'une part, que les conclusions de la SCI Yerres Développement tendant à la décharge des cotisations de taxe locale d'équipement et de redevance pour la construction de locaux à usage de bureaux étaient irrecevables faute d'avoir été précédées d'une réclamation préalable et, d'autre part, que ses conclusions tendant au remboursement de la taxe locale d'équipement devaient être rejetées comme non fondées.<br/>
<br/>
              4. En premier lieu, si la société requérante soutient que le remboursement d'une imposition n'est que la conséquence de sa décharge et que le tribunal a, dès lors, entaché son jugement d'erreur de droit et de contradiction de motifs en jugeant qu'elle était recevable à solliciter le remboursement des impositions en litige mais non leur décharge, il ressort tant de ses écritures devant le tribunal que des termes du jugement attaqué que ses conclusions aux fins de " remboursement " concernaient les droits de taxe locale d'équipement déjà acquittés et portaient sur la somme de 52 000 euros tandis que ses conclusions à fin de " décharge " concernaient le reliquat de la taxe locale d'équipement, soit la somme de 17 000 euros, ainsi que la redevance pour la construction de locaux à usage de bureaux. C'est, par suite, sans erreur de droit et sans contradiction de motifs que le tribunal a pu estimer que seules les secondes étaient irrecevables faute d'avoir été précédées d'une réclamation préalable et statuer au fond sur les premières.<br/>
<br/>
              5. En deuxième lieu, le tribunal n'a pas dénaturé les pièces du dossier qui lui étaient soumis en estimant que la preuve de l'existence d'une réclamation préalable concernant les cotisations de taxe locale d'équipement restant dues et de redevance pour la construction de locaux à usage de bureaux en Île-de-France dès lors qu'il ressort de ces pièces que la lettre du 7 septembre 2006 de la trésorerie générale de Corbeil-Villabé se bornait à faire savoir à la société requérante que la commune de Yerres avait informé l'administration fiscale que le permis de construire ne serait pas annulé et que la société était redevable d'une somme de 5 055,33 euros au titre de l'article 1857 du code civil sans faire état de ce que la SCI Yerres Développement aurait, par son courrier du 27 juin 2006, sollicité la décharge des cotisations de taxe locale d'équipement restant dues et de la redevance pour la construction de locaux à usage de bureaux, et que les courriers des 24 octobre 2006, 4 janvier 2007 et 22 juillet 2008, dont elle se prévaut, ne concernaient que le remboursement des droits de taxe déjà acquittés et non le reliquat de cette taxe et, enfin, que si le courrier adressé au maire de Yerres le 5 juin 2008 mentionne le " complément de la taxe locale d'équipement ", il sollicite seulement l'interruption du recouvrement de ce complément de taxe, au demeurant non chiffré, et non sa décharge.<br/>
<br/>
              6. En troisième lieu, pour l'application des dispositions précitées de l'article 1723 quinquies du code général des impôts, seuls les redevables n'ayant entrepris aucun travail de construction sont susceptibles d'être regardés comme n'ayant pas été en mesure de donner suite à l'autorisation de construire. En cas d'exécution partielle des travaux projetés, il leur incombe, pour pouvoir, le cas échéant, bénéficier d'une restitution, également partielle, de l'impôt acquitté, d'obtenir une modification de l'autorisation de construire initiale. Il ressort des pièces du dossier soumis au tribunal que la SCI Yerres Développement avait débuté les travaux de construction autorisés par le permis de construire du 11 février 1999 avant de procéder, sur sa propre initiative, à la démolition du bâtiment. Il en résulte que le tribunal n'a ni dénaturé les faits ni donné à ces faits une qualification juridique inexacte en jugeant que la SCI avait donné suite à l'autorisation de construire et en en déduisant, après avoir relevé que la société n'établissait pas que les éléments de construction réalisés auraient été démolis en vertu d'une décision de justice, qu'elle n'était pas fondée à demander le remboursement des cotisations de taxe locale d'équipement qu'elle avait acquittées.<br/>
<br/>
              7. En dernier lieu, le jugement attaqué, qui énonce clairement les motifs de droit et de fait qui ont conduit le tribunal à rejeter les conclusions de la SCI Yerres Développement tendant au remboursement des sommes acquittées au titre de la taxe locale d'équipement, est suffisamment motivé. Le tribunal n'avait pas, contrairement à ce que soutient le pourvoi, à rechercher si le maire avait refusé à tort de rapporter le permis de construire délivré le 11 février 1999 ni à s'interroger sur les effets des arrêtés municipaux de péril.<br/>
<br/>
              8. Il résulte de tout ce qui précède que la SCI Yerres Développement n'est pas fondée à demander l'annulation du jugement qu'elle attaque. Ses conclusions tendant à ce qu'une somme soit mise à la charge de l'Etat et de la commune de Yerres au titre de l'article L. 761-1 du code de justice administrative ne peuvent, par suite, qu'être rejetées. Il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par la commune de Yerres au titre de ces mêmes dispositions.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
Article 1er : Le pourvoi de la SCI Yerres Développement est rejeté.<br/>
Article 2 : Les conclusions présentées par la commune de Yerres au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La présente décision sera notifiée à la SCI Yerres Développement, à la commune de Yerres et à la ministre du logement, de l'égalité des territoires et de la ruralité.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
