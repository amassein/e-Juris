<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043852074</ID>
<ANCIEN_ID>JG_L_2021_07_000000434750</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/85/20/CETATEXT000043852074.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème - 5ème chambres réunies, 21/07/2021, 434750, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-07-21</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>434750</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème - 5ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SARL MEIER-BOURDEAU, LECUYER ET ASSOCIES</AVOCATS>
<RAPPORTEUR>Mme Fanélie Ducloz</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Olivier  Fuchs</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:434750.20210721</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 20 septembre 2019, 19 décembre 2019 et 23 avril 2021 au secrétariat du contentieux du Conseil d'Etat, l'association des jeunes avocats aux conseils demande au Conseil d'Etat d'annuler pour excès de pouvoir, d'une part, l'arrêté du 22 mars 2019 par lequel le garde des sceaux, ministre de la justice, a créé quatre offices d'avocat au Conseil d'Etat et à la Cour de cassation, et, d'autre part, la décision implicite du garde des sceaux, ministre de la justice, refusant de rapporter cet arrêté. <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de commerce ;<br/>
              - l'ordonnance du 10 septembre 1817 ;<br/>
              - le décret n° 2016-215 du 26 février 2016 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Fanélie Ducloz, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Olivier Fuchs, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SARL Meier-Bourdeau, Lecuyer et associés, avocat de l'association des jeunes avocats aux conseils.<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. L'association des jeunes avocats aux conseils demande l'annulation, pour excès de pouvoir, de l'arrêté du 22 mars 2019 du garde des sceaux, ministre de la justice, pris après avis de l'Autorité de la concurrence du 25 octobre 2018, portant création de quatre offices d'avocat au Conseil d'Etat et à la Cour de cassation.<br/>
<br/>
              Sur le cadre juridique du litige :<br/>
<br/>
              2. Aux termes de l'article 3 de l'ordonnance du 10 septembre 1817 relative à l'ordre des avocats au Conseil d'Etat et à la Cour de cassation : " I.- Au vu des besoins identifiés par l'Autorité de la concurrence dans les conditions prévues à l'article L. 462-4-2 du code de commerce, lorsque le demandeur remplit les conditions de nationalité, d'aptitude, d'honorabilité, d'expérience et d'assurance requises pour l'exercice de la profession d'avocat au Conseil d'Etat et à la Cour de cassation, le ministre de la justice le nomme titulaire de l'office d'avocat au Conseil d'Etat et à la Cour de cassation créé. Un décret précise les conditions d'application du présent alinéa. / Si, dans un délai de six mois à compter de la publication des recommandations de l'Autorité de la concurrence mentionnées au même article L. 462-4-2, le ministre de la justice constate un nombre insuffisant de demandes de créations d'office au regard des besoins identifiés, il procède, dans des conditions prévues par décret, à un appel à manifestation d'intérêt en vue d'une nomination dans un office. (...) ".  Aux termes de l'article L. 462-4-2 du code de commerce : " L'Autorité de la concurrence rend au ministre de la justice, qui en est le garant, un avis sur la liberté d'installation des avocats au Conseil d'Etat et à la Cour de cassation. / Elle fait toutes recommandations en vue d'améliorer l'accès aux offices d'avocat au Conseil d'Etat et à la Cour de cassation dans la perspective d'augmenter de façon progressive le nombre de ces offices. Elle établit, en outre, un bilan en matière d'accès des femmes et des hommes à ces offices. Ces recommandations sont rendues publiques au moins tous les deux ans. / A cet effet, elle identifie le nombre de créations d'offices d'avocat au Conseil d'Etat et à la Cour de cassation qui apparaissent nécessaires pour assurer une offre de services satisfaisante au regard de critères définis par décret et prenant notamment en compte les exigences de bonne administration de la justice ainsi que l'évolution du contentieux devant ces deux juridictions. / Les recommandations relatives au nombre de créations d'offices d'avocat au Conseil d'Etat et à la Cour de cassation permettent une augmentation progressive du nombre d'offices à créer, de manière à ne pas bouleverser les conditions d'activité des offices existants (...) ". Enfin, aux termes de l'article 2 du décret du 26 février 2016 portant définition des critères prévus pour l'application de l'article L. 462-4-2 du code de commerce : " L'Autorité de la concurrence identifie le nombre de créations d'offices d'avocat au Conseil d'Etat et à la Cour de cassation qui apparaissent nécessaires pour assurer une offre de services satisfaisante au regard des critères suivants : / 1° Critères permettant d'évaluer le niveau et les perspectives d'évolution de la demande : / - l'évolution de l'activité de la Cour de cassation et de la section du contentieux du Conseil d'Etat au cours des cinq dernières années telle que résultant des rapports d'activité publiés annuellement par ces deux juridictions sur le fondement des articles R. 431-9 du code de l'organisation judiciaire et R. 123-5 du code de justice administrative ; /  l'évolution du nombre de décisions prononcées par les juridictions du fond susceptibles de pourvoi en cassation au cours des cinq dernières années ; / 2° Critères permettant d'évaluer le niveau et les perspectives d'évolution de l'offre : / - la tendance de l'activité économique ; / l'évolution du nombre d'offices et du nombre d'avocats au Conseil d'Etat et à la Cour de cassation exerçant soit à titre individuel, soit dans le cadre d'une entité dotée de la personnalité morale, soit en qualité de salarié, au cours des cinq dernières années ; / - le nombre d'offices vacants ; / - le nombre de personnes titulaires du certificat d'aptitude à la profession d'avocat au Conseil d'Etat et à la Cour de cassation n'exerçant pas en qualité d'avocat au Conseil d'Etat et à la Cour de cassation ; / - le chiffre d'affaires global des offices d'avocat au Conseil d'Etat et à la Cour de cassation et celui réalisé par chacun d'entre eux au cours des cinq dernières années correspondant à leur activité devant la Cour de cassation et le Conseil d'Etat. ".<br/>
<br/>
              3. Il résulte de ces dispositions que le garde des sceaux, ministre de la justice, procède, sur avis de l'Autorité de la concurrence, à la création de nouveaux offices d'avocat au Conseil d'Etat et à la Cour de cassation afin d'assurer une offre de services satisfaisante compte tenu notamment de l'évolution du contentieux devant le Conseil d'Etat et la Cour de cassation, du nombre de personnes titulaires du certificat d'aptitude à la profession d'avocat au Conseil d'Etat et à la Cour de cassation n'exerçant pas en qualité d'avocat au Conseil d'Etat et à la Cour de cassation et des conditions d'activité des offices existants.<br/>
<br/>
              Sur la légalité externe de l'arrêté attaqué :<br/>
<br/>
              4. Il ne résulte pas du seul fait que le garde des sceaux, ministre de la justice, ait choisi de suivre l'avis de l'Autorité de la concurrence du 25 octobre 2018, émis en application de l'article L. 462-4-2 du code de commerce, recommandant la création de quatre offices d'avocat au Conseil d'Etat et à la Cour de cassation, qu'il se soit cru lié par cet avis, ni qu'il ait renoncé à exercer la compétence qu'il tient de l'article 3 de l'ordonnance du 10 septembre 1817. <br/>
<br/>
              Sur la légalité interne de l'arrêté attaqué :<br/>
<br/>
              5. En premier lieu, si les requérants font valoir que plusieurs avocats ont pu, au cours de la période précédant la décision attaquée, accéder à la profession d'avocat au Conseil d'Etat et à la Cour de cassation en intégrant ou en reprenant des offices existants, il n'en résulte pas que l'accès à la profession ne puisse également être amélioré par la création de nouveaux offices. <br/>
<br/>
              6. En deuxième lieu, si l'association des jeunes avocats aux conseils soutient que la création de quatre nouveaux offices d'avocat au Conseil d'Etat et à la Cour de cassation, laquelle correspond à une augmentation de 6 % du nombre des offices, ne serait pas justifiée par l'évolution de l'activité de la Cour de cassation qui, depuis 2018, connaitrait une baisse significative des pourvois notamment en matière civile où la représentation par un avocat au Conseil d'Etat et à la Cour de cassation est obligatoire, d'une part, en application des dispositions de l'article 2 du décret du 26 février 2016 portant définition des critères prévus pour l'application de l'article L. 462-4-2 du code de commerce, rappelées au point 2, doit être prise en compte, s'agissant du critère permettant d'évaluer le niveau et les perspectives d'évolution de la demande, l'évolution de l'activité de la Cour de cassation au cours des cinq années précédant la décision attaquée, soit en l'espèce les années 2014 à 2018. D'autre part, il ressort des pièces du dossier que l'activité de la Cour de cassation est restée globalement stable au cours des années 2014, 2015, 2016 et 2017, y compris s'agissant des affaires pour lesquelles le ministère d'un avocat au conseil d'Etat et à la Cour de cassation est obligatoire. Si, pour l'année 2018, le nombre des affaires nouvelles enregistrées à la Cour de cassation diminue effectivement, il ressort des pièces du dossier, notamment du rapport d'activité de la Cour de cassation publié pour l'année 2018, que cette diminution s'explique notamment par le fait que l'année 2017 avait connu l'enregistrement d'une très forte série de pourvois connexes devant la chambre sociale, qui ne s'est pas reproduit en 2018. Enfin, doit également être pris en considération le fait que, pendant la même période, le nombre de pourvois en cassation devant le Conseil d'Etat, pour lesquels la représentation par un avocat au Conseil d'Etat et à la Cour de cassation est obligatoire, s'est accru de 6,5%. <br/>
<br/>
              7. En troisième lieu, s'il ressort des pièces du dossier qu'en moyenne, cinq nouveaux avocats au Conseil d'Etat et à la Cour de cassation intègrent tous les ans un office existant et que le nombre de diplômés de l'Institut de formation et de recherche des avocats aux conseils (IFRAC) est d'environ cinq par an, il en ressort également, d'une part, que treize titulaires du certificat d'aptitude à la profession d'avocat aux conseils, dont huit l'ayant obtenu entre 2015 et 2018, n'exerçaient pas la profession d'avocat au Conseil d'Etat et à la Cour de cassation à la date de l'arrêté attaqué, d'autre part que le nombre d'inscriptions en première année de l'IFRAC a augmenté en 2018, passant de 4 pour l'année 2017 à 24, ce qui devrait permettre, à terme, une augmentation du nombre de diplômés. <br/>
<br/>
              8. En quatrième lieu, s'il existe une disparité entre les offices d'avocat au Conseil d'Etat et à la Cour de cassation s'agissant de leur chiffre d'affaires et de leur rentabilité, il ressort toutefois des pièces du dossier que la rémunération moyenne nette des avocats aux conseils, tenant compte des charges sociales, des charges professionnelles et des intérêts de l'emprunt relatif au droit de présentation, demeure à un niveau élevé et que le taux de marge de l'ensemble des offices, c'est-à-dire les bénéfices rapportés au chiffre d'affaires, reste également élevé. Il en ressort également que les nouveaux offices créés ont, jusqu'à présent, été rentables dès leur création, et que cette rentabilité s'est rapidement accrue. Et la création de quatre offices d'avocat au Conseil d'Etat et à la Cour de cassation, soit une augmentation de leur nombre de 6%, n'	apparaît pas de nature à bouleverser l'économie des offices existants, la charge constituée par le remboursement de l'emprunt destiné à financer le droit de présentation qu'ont dû supporter des avocats ayant été, à cet égard, prise en compte pour apprécier les effets de la création de ces quatre nouveaux offices. <br/>
<br/>
              	9. Il résulte de ce qui précède que le garde des sceaux, ministre de la justice, qui s'est légalement fondés sur les critères posés par l'article L. 462-4-2 du code de commerce et par l'article 2 du décret du 26 février 2016 précités, n'a pas fait une inexacte application de ces dispositions en créant, par l'arrêté attaqué, quatre nouveaux offices d'avocat au Conseil d'Etat et à la Cour de cassation.<br/>
<br/>
              10. Il résulte de tout ce qui précède qu'il y a lieu de rejeter la requête de l'association des jeunes avocats aux conseils.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de l'association des jeunes avocats aux conseils est rejetée.<br/>
Article 2 : La présente décision sera notifiée à l'association des jeunes avocats aux conseils et au garde des sceaux, ministre de la justice.<br/>
Copie en sera adressée à l'Ordre des avocats au Conseil d'Etat et à la Cour de cassation.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
