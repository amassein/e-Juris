<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038126201</ID>
<ANCIEN_ID>JG_L_2019_02_000000413323</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/12/62/CETATEXT000038126201.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème et 1ère chambres réunies, 13/02/2019, 413323, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-02-13</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>413323</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème et 1ère chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP RICHARD ; SCP MATUCHANSKY, POUPOT, VALDELIEVRE</AVOCATS>
<RAPPORTEUR>Mme Françoise Tomé</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Sophie-Justine Lieber</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:413323.20190213</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés au secrétariat du contentieux du Conseil d'Etat les 11 août et 13 novembre 2017 et le 18 avril 2018, M. A...B...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision du 13 juin 2017 du Conseil national de l'ordre des médecins, statuant en formation restreinte, le suspendant du droit d'exercer la médecine jusqu'à la constatation de son aptitude par une expertise effectuée dans les conditions prévues à l'article R. 4124-3-5 du code de la santé publique ;<br/>
<br/>
              2°) de mettre à la charge du Conseil national de l'ordre des médecins la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de la santé publique ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Françoise Tomé, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Sophie-Justine Lieber, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Richard, avocat de M. B...et à la SCP Matuchansky, Poupot, Valdelièvre, avocat du Conseil national de l'ordre des médecins ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article R. 4124-3-5 du code de la santé publique : " I. - En cas d'insuffisance professionnelle rendant dangereux l'exercice de la profession, la suspension temporaire, totale ou partielle, du droit d'exercer est prononcée par le conseil régional ou interrégional pour une période déterminée qui peut, s'il y a lieu, être renouvelée. / Le conseil régional ou interrégional est saisi à cet effet soit par le directeur général de l'agence régionale de santé, soit par une délibération du conseil départemental ou du conseil national. Ces saisines ne sont pas susceptibles de recours. / II. - La suspension ne peut être ordonnée que sur un rapport motivé établi à la demande du conseil régional ou interrégional dans les conditions suivantes : / 1° Pour les médecins, le rapport est établi par trois médecins qualifiés dans la même spécialité que celle du praticien concerné désignés comme experts, le premier par l'intéressé, le deuxième par le conseil régional ou interrégional et le troisième par les deux premiers experts. Ce dernier est choisi parmi les personnels enseignants et hospitaliers titulaires de la spécialité. Pour la médecine générale, le troisième expert est choisi parmi  les personnels enseignants titulaires ou les professeurs associés ou maîtres de conférences associés des universités (...) / IV. - Les experts procèdent ensemble, sauf impossibilité manifeste, à l'examen des connaissances théoriques et pratiques du praticien. Le rapport d'expertise est déposé au plus tard dans le délai de six semaines à compter de la saisine du conseil. Il indique les insuffisances relevées au cours de l'expertise, leur dangerosité et préconise les moyens de les pallier par une formation théorique et, si nécessaire, pratique (...) / Si l'intéressé ne se présente pas à la convocation fixée par les experts, une seconde convocation lui est adressée. En cas d'absence de l'intéressé aux deux convocations, les experts établissent un rapport de carence à l'intention du conseil régional ou interrégional, qui peut alors suspendre le praticien pour présomption d'insuffisance professionnelle rendant dangereux l'exercice de la profession (...) / VI. - Si le conseil régional ou interrégional n'a pas statué dans le délai de deux mois à compter de la réception de la demande dont il est saisi, l'affaire est portée devant le Conseil national de l'ordre (...) " ;<br/>
<br/>
              2. Il ressort des pièces du dossier que, sur le fondement des dispositions citées ci-dessus, le conseil départemental de la Ville de Paris de l'ordre des médecins a saisi le conseil régional d'Ile-de-France d'une demande de suspension du droit d'exercice de M.B..., médecin qualifié spécialiste en gynécologie-obstétrique. Le Conseil national de l'ordre des médecins, auquel le dossier avait été transmis par le conseil régional en application des dispositions du VI de l'article R. 4123-4-5 du code de la santé publique citées ci-dessus, a instruit cette demande en assurant la désignation de trois experts. M. B...n'ayant pas répondu aux deux convocations de ces experts, ceux-ci ont établi, le 17 juin 2016, un rapport de carence. La formation restreinte du conseil national a alors, par une décision du 21 juillet 2016, ordonné une nouvelle expertise par un nouveau groupe d'experts. Ayant, à nouveau, constaté la carence du praticien à répondre aux convocations des experts désignés au titre de la seconde expertise, le Conseil national de l'ordre des médecins, statuant en formation restreinte, a, par la décision du 13 juin 2017 dont M. B... demande l'annulation, suspendu ce dernier du droit d'exercer la médecine pour présomption d'insuffisance professionnelle rendant dangereux l'exercice de la profession, jusqu'à la constatation de son aptitude par une expertise effectuée dans les conditions prévues à l'article R. 4124-3-5 cité ci-dessus.<br/>
<br/>
              3. En premier lieu, la circonstance que M. B...n'aurait pas été mis à même d'être assisté par un avocat lors d'un entretien du 16 décembre 2014 avec les membres du conseil départemental de la Ville de Paris de l'ordre des médecins n'est pas, par elle-même, de nature à entacher d'irrégularité la délibération du 14 janvier 2015 par laquelle ce conseil départemental a saisi le conseil régional d'Ile-de-France d'une demande de suspension du droit d'exercice de l'intéressé.<br/>
<br/>
              4. En deuxième lieu, il ressort des termes mêmes des dispositions de l'article R. 4124-3-5 du code de la santé publique, citées au point 1, que la suspension ne peut être ordonnée que sur expertise. Dès lors, M. B...ne peut utilement soutenir que la décision attaquée est entachée d'irrégularité au motif que l'expertise aurait été inutile en l'absence d'élément propre à étayer l'allégation d'insuffisance professionnelle.<br/>
<br/>
              5. En troisième lieu, il ressort des pièces du dossier que, contrairement à ce qu'il soutient, M. B...a été convoqué à deux reprises par les experts commis dans le cadre de la première expertise décidée par le conseil national sans se rendre à leur convocation. Il a également refusé de répondre aux convocations des experts désignés pour réaliser la nouvelle expertise décidée par le conseil national le 21 juillet 2016. Il en résulte que le Conseil national de l'ordre des médecins, dont la décision est suffisamment motivée sur ce point, n'a pas fait une inexacte application des dispositions du IV de l'article R. 4124-3-4, citées ci-dessus, en prononçant, au vu des pièces du dossier dont il disposait, qui n'étaient pas de nature à infirmer la présomption d'insuffisance professionnelle mentionnée par ces mêmes dispositions à raison de la carence du praticien aux convocations des experts, la suspension du droit de M. B... d'exercer la médecine, pour présomption d'insuffisance professionnelle rendant dangereux l'exercice de la profession, jusqu'à la constatation de son aptitude par une expertise conduite dans les conditions posées par les dispositions citées au point 1.<br/>
<br/>
              6. Il résulte de tout ce qui précède que M.B...  n'est pas fondé à demander l'annulation de la décision qu'il attaque. Les dispositions de l'article L. 761-1 du code de justice administrative font, par suite, obstacle à ce qu'une somme soit mise, à ce titre, à la charge du Conseil national de l'ordre des médecins, qui n'est pas, dans la présente instance, la partie perdante. En revanche, il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de M. B... une somme de 1 500 euros à verser au Conseil national de l'ordre des médecins au titre des mêmes dispositions. <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de M. B...est rejetée.<br/>
Article 2 : M. B...versera au Conseil national de l'ordre des médecins la somme de 1 500 euros au titre de l'article L. 761-1 du code de la justice administrative.<br/>
Article 3 : La présente décision sera notifiée à M. A...B...et au Conseil national de l'ordre des médecins.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
