<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033172394</ID>
<ANCIEN_ID>JG_L_2016_09_000000400999</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/17/23/CETATEXT000033172394.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème chambre, 30/09/2016, 400999, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-09-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>400999</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GASCHIGNARD ; DELAMARRE</AVOCATS>
<RAPPORTEUR>M. Grégory Rzepski</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Gilles Pellissier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2016:400999.20160930</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B...A...a demandé au juge des référés du tribunal administratif de Strasbourg de condamner le Syndicat Mixte des Transports Urbains de Thionville-Fensch (SMITU) au versement d'une provision correspondant, d'une part, à la restitution des prélèvements indus sur ses traitements, au cours de la période du 1er mars 2011 au 28 février 2014, de cotisations à la contribution sociale généralisée (CSG) et à la contribution au remboursement de la dette sociale (CRDS), et, d'autre part, au paiement de son indemnité compensatrice de congés payés au titre de l'année 2014. Par une ordonnance n° 1505144 du 3 novembre 2015, le juge des référés du tribunal administratif de Strasbourg a prononcé un non-lieu à statuer sur les conclusions de la demande de M. A...tendant au versement d'une indemnité de congés payés au titre de l'année 2014 et a rejeté le surplus de ses conclusions. <br/>
<br/>
              Par une ordonnance n° 15NC02322 du 10 mai 2016, la présidente de la cour administrative d'appel de Nancy a annulé cette ordonnance en tant qu'elle s'est prononcée sur les conclusions tendant au versement d'une provision au titre du remboursement des cotisations à la CSG et à la CRDS et a rejeté le surplus des conclusions d'appel. <br/>
<br/>
              Par un pourvoi sommaire et un mémoire ampliatif, enregistrés les 27 juin et 13 juillet 2016 au secrétariat du contentieux du Conseil d'Etat, M. A...demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cette ordonnance ; <br/>
<br/>
              2°) de mettre à la charge du SMITU le versement de la somme de 1 800 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de justice administrative.<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Grégory Rzepski, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Gilles Pellissier, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Gaschignard, avocat de M. A...et à Me Delamarre, avocat du Syndicat Mixte des Transports Urbains de Thionville-Fensch ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux premiers juges que M.A..., ancien directeur des services techniques du Syndicat Mixte des Transports Urbains de Thioville-Fensch (SMITU), a saisi le juge des référés du tribunal administratif de Strasbourg d'une demande tendant à la condamnation du syndicat mixte au versement d'une provision correspondant, d'une part, à la restitution de prélèvements indus opérés par cet établissement sur son traitement au titre des cotisations à la CSG et à la CRDS, pour la période du 1er mars 2011 au 28 février 2014, et, d'autre part, au paiement de son indemnité compensatrice de congés payés au titre de l'année 2014 ; que le SMITU a versé cette indemnité à M. A...avant que le juge n'ait statué ; que, par une ordonnance du 3 novembre 2015, le juge des référés du tribunal administratif de Strasbourg a prononcé un non-lieu à statuer sur la demande de M. A...tendant au versement d'une indemnité de congés payés au titre de l'année 2014 et a rejeté le surplus de ses conclusions ; que par une ordonnance du 10 mai 2016, la présidente de la cour administrative d'appel de Nancy a, d'une part, annulé cette ordonnance en tant qu'elle s'est prononcée sur les conclusions tendant au versement d'une provision au titre du remboursement des cotisations à la CSG et à la CRDS et rejeté ces conclusions comme portées devant une juridiction incompétente pour en connaître et, d'autre part, rejeté le surplus des conclusions de M. A...; que M. A...se pourvoit contre cette ordonnance en tant qu'elle a confirmé le rejet de ses conclusions présentées en première instance sur le fondement de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
              2. Considérant, en premier lieu, qu'en vertu de l'article R. 541-1 du code de justice administrative, le juge des référés peut, même en l'absence d'une demande au fond, accorder une provision au créancier qui l'a saisi lorsque l'existence de l'obligation n'est pas sérieusement contestable ; qu'aux termes de l'article R. 541-5 du même code : " A l'occasion des litiges dont la cour administrative d'appel est saisie, le président de la cour ou le magistrat désigné par lui dispose des pouvoirs prévus à l'article R. 541-1 " ; <br/>
<br/>
              3. Considérant que le litige soumis par M. A...au juge des référés du tribunal administratif de Strasbourg tendait à l'octroi d'une provision ; qu'il résulte des dispositions mentionnées au point 2 que l'appel formé contre la décision du juge de première instance relevait de la compétence du président de la cour administrative d'appel ou d'un magistrat désigné par lui ; que, par suite, le moyen tiré de ce que le président de la cour aurait commis une erreur de droit en rejetant par ordonnance la requête d'appel de M. A...doit être écarté ; <br/>
<br/>
              4. Considérant, en deuxième lieu, qu'aux termes de l'article L. 761-1 du code de justice administrative : " Dans toutes les instances [...], le juge condamne la partie tenue aux dépens ou, à défaut, la partie perdante, à payer à l'autre partie la somme qu'il détermine, au titre des frais exposés et non compris dans les dépens. Le juge tient compte de l'équité ou de la situation économique de la partie condamnée. Il peut, même d'office, pour des raisons tirées des mêmes considérations, dire qu'il n'y a pas lieu à cette condamnation " ; <br/>
<br/>
              5. Considérant qu'il ressort des pièces du dossier que statuant sur les conclusions présentées en première instance par M. A...au titre des frais irrépétibles, la présidente de la cour a refusé d'y faire droit au motif que le SMITU ne pouvait être regardé comme la partie perdante, les conclusions de M. A...tendant au versement d'une provision correspondant à l'indemnité compensatrice de congés payés au titre de l'année 2014 ayant fait l'objet d'un non lieu à statuer et le surplus de ses conclusions ayant été rejeté ; que si la circonstance qu'un recours perde son objet en cours d'instance n'est pas de nature à faire obstacle à ce qu'il soit fait droit aux conclusions d'une partie au titre des frais irrépétibles, la présidente n'a pas donné aux faits dont elle était saisie une qualification juridique erronée en estimant que le SMITU ne devait pas être regardé comme la partie perdante en première instance au sens des dispositions citées ci-dessus de l'article L. 761-1 du code de justice administrative, alors que le non-lieu à statuer portait sur une partie minime des conclusions de M.A..., que ce non-lieu à statuer ne révélait au demeurant pas une attitude réticente de la part du SMITU, dont la décision de procéder au paiement de l'indemnité compensatrice de congés payés avait été prise antérieurement à l'introduction du référé, et que l'essentiel des conclusions de M. A...avaient été rejetées; que, par suite, M. A...n'est pas non plus fondé à demander l'annulation sur ce point de l'ordonnance attaquée ;<br/>
<br/>
              6. Considérant, enfin, que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mis à la charge du SMITU, qui n'est pas, dans la présente instance, la partie perdante, le versement de la somme que demande à ce titre M. A... ; qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par le SMITU au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de M. A...est rejeté.<br/>
Article 2 : Les conclusions du SMITU présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La présente décision sera notifiée à M. B...A...et au Syndicat Mixte des Transports Urbains de Thionville-Fensch.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
