<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031173255</ID>
<ANCIEN_ID>JG_L_2015_09_000000389127</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/17/32/CETATEXT000031173255.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème - 5ème SSR, 14/09/2015, 389127, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-09-14</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>389127</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème - 5ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LYON-CAEN, THIRIEZ</AVOCATS>
<RAPPORTEUR>M. Jean-François de Montgolfier</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Gaëlle Dumortier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:XX:2015:389127.20150914</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par un mémoire et un mémoire complémentaire enregistrés les 22 juin et 3 septembre 2015 au secrétariat du contentieux du Conseil d'Etat, la Confédération générale du travail demande au Conseil d'Etat, en application de l'article 23-5 de l'ordonnance n° 58-1067 du 7 novembre 1958 et à l'appui de sa requête tendant à l'annulation pour excès de pouvoir du décret n° 2015-87 du 28 janvier 2015 relatif au financement mutualisé des organisations syndicales de salariés et des organisations professionnelles d'employeurs, de renvoyer au Conseil constitutionnel la question de la conformité aux droits et libertés que la Constitution garantit des dispositions du 1° de l'article L. 2135-13 du code du travail, dans leur rédaction résultant de l'article 31 de la loi n° 2014-288 du 5 mars 2014 relative à la formation professionnelle, à l'emploi et à la démocratie sociale.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la Constitution, notamment son Préambule et son article 61-1 ;<br/>
              - l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
              - le code du travail ;<br/>
              - la loi n° 2014-288 du 5 mars 2014 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-François de Montgolfier, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Gaëlle Dumortier, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Lyon-Caen, Thiriez, avocat de la Confédération générale du travail ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes du premier alinéa de l'article 23-5 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel : " Le moyen tiré de ce qu'une disposition législative porte atteinte aux droits et libertés garantis par la Constitution peut être soulevé (...) à l'occasion d'une instance devant le Conseil d'Etat (...) " ; qu'il résulte des dispositions de cet article que le Conseil constitutionnel est saisi de la question prioritaire de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux ;<br/>
<br/>
              2. Considérant que l'article 31 de la loi du 5 mars 2014 relative à la formation professionnelle, à l'emploi et à la démocratie sociale a inséré dans le code du travail les articles L. 2135-9 à L. 2135-18 qui déterminent le régime du " financement mutualisé des organisations syndicales de salariés et des organisations professionnelles d'employeurs " ; que l'article L. 2135-9 institue un fonds paritaire, chargé d'une mission de service public, destiné à apporter une contribution au financement des organisations syndicales de salariés et des organisations professionnelles d'employeurs dans le cadre de leur participation aux missions définies à l'article L. 2135-11 ; que le 1° de cet article prévoit notamment que, parmi les missions d'intérêt général de ces organisations que le fonds paritaire doit contribuer à financer, figurent la conception, la gestion, l'animation et l'évaluation des politiques menées paritairement et dans le cadre des organismes que ces organisations gèrent majoritairement ; <br/>
<br/>
              3. Considérant que le 1° de l'article L. 2135-13 du code du travail, dont la constitutionnalité est contestée par le syndicat requérant, dispose que les crédits du fonds paritaire alloués au titre de la mission mentionnée au 1° de l'article L. 2135-11 du même code sont répartis à parité entre les organisations syndicales de salariés, d'une part, et les organisations professionnelles d'employeurs, d'autre part, et que " Les modalités de répartition des crédits entre organisations syndicales de salariés, d'une part, et entre organisations professionnelles d'employeurs, d'autre part, sont déterminées, par voie réglementaire, de façon uniforme pour les organisations syndicales de salariés et en fonction de l'audience ou du nombre des mandats paritaires exercés pour les organisations professionnelles d'employeurs " ;<br/>
<br/>
              4. Considérant que le moyen tiré de ce que la différence entre les modalités de répartition des crédits entre les organisations syndicales de salariés, d'une part, et entre les organisations professionnelles d'employeurs, d'autre part, méconnaît le principe d'égalité devant la loi garanti par l'article 6 de la Déclaration des droits de l'homme et du citoyen de 1789 soulève une question relative à la portée de cette disposition constitutionnelle qui, sans qu'il soit besoin pour le Conseil d'Etat d'examiner le caractère sérieux du moyen invoqué, sur ce fondement, à l'encontre des dispositions législatives en cause, doit être regardée comme nouvelle au sens de l'article 23-5 de l'ordonnance du 7 novembre 1958 ; qu'ainsi, il y a lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité invoquée ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La question de la conformité à la Constitution du 1° de l'article L. 2135-13 du code du travail est renvoyée au Conseil constitutionnel.<br/>
Article 2 : Il est sursis à statuer sur la requête de la Confédération générale du travail jusqu'à ce que le Conseil constitutionnel ait tranché la question de constitutionnalité ainsi soulevée.<br/>
Article 3 : La présente décision sera notifiée à la Confédération générale du travail et à la ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social.<br/>
Copie en sera adressée au Premier ministre.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
