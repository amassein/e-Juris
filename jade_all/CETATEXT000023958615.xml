<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000023958615</ID>
<ANCIEN_ID>JG_L_2011_05_000000318644</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/23/95/86/CETATEXT000023958615.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème et 9ème sous-sections réunies, 04/05/2011, 318644</TITRE>
<DATE_DEC>2011-05-04</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>318644</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème et 9ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BARTHELEMY, MATUCHANSKY, VEXLIARD ; SCP GADIOU, CHEVALLIER</AVOCATS>
<RAPPORTEUR>M. Aurélien Rousseau</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Delphine Hedary</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2011:318644.20110504</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 21 juillet et 17 octobre 2008 au secrétariat du contentieux du Conseil d'Etat, présentés pour Mme A...B..., demeurant..., ; Mme B... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 06DA01487 du 2 mai 2008 par lequel la cour administrative d'appel de Douai a rejeté sa requête tendant, d'une part, à l'annulation du jugement du 20 septembre 2006 du tribunal administratif de Lille rejetant sa demande tendant à ce que la commune de Fourmies soit condamnée à lui verser une indemnité de 28 355 euros au titre de rappels de rémunération, d'autre part, à constater sa qualification d'agent non titulaire depuis le 16 septembre 1980, son reclassement au 7ème échelon du corps des assistants d'enseignement artistique à compter de sa titularisation ainsi qu'à condamner la commune précitée à lui verser les sommes de 28 355 euros, pour la période du 1er octobre 2002 au 31 septembre 2006, et de 16 699,98 euros, pour la période du 1er octobre 2002 au 30 septembre 2006, en lui enjoignant de fournir les feuilles de paye régularisées ;<br/>
<br/>
              2°) de mettre à la charge de la commune de Fourmies le versement d'une somme de 3 000 euros en application de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la loi n° 84-53 du 26 janvier 1984 ;<br/>
<br/>
              Vu le décret n° 88-45 du 15 février 1988 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Aurélien Rousseau, Auditeur,<br/>
<br/>
              - les observations de la SCP Barthélemy, Matuchansky, Vexliard, avocat de Mme B...et de la SCP Gadiou, Chevallier, avocat de la commune de Fourmies,<br/>
<br/>
              - les conclusions de Mme Delphine Hedary, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Barthélemy, Matuchansky, Vexliard, avocat de Mme B...et à la SCP Gadiou, Chevallier, avocat de la commune de Fourmies ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant que la loi du 26 janvier 1984 portant dispositions statutaires relatives, à la fonction publique territoriale, dans sa rédaction en vigueur, antérieure à la loi n° 2009-972 du 3 août 2009, fixe à l'article 3 les cas dans lesquels les emplois permanents des collectivités territoriales peuvent par exception être pourvus par des agents non titulaires ; que l'article 136 de cette loi fixe les règles d'emploi de ces agents et précise qu'un décret en Conseil d'Etat déterminera les conditions d'application de cet article ; qu'il résulte des dispositions du dernier alinéa de l'article 1er du décret du 15 février 1988, pris pour l'application de l'article 136, que ces règles d'emploi s'appliquent aux agents contractuels sauf s'ils ont été " engagés pour un acte déterminé " ;<br/>
<br/>
              Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que Mme B...a assuré, à partir du 16 septembre 1980, des cours de violon à l'école municipale de musique de Fourmies ; qu'elle a été titularisée, après inscription sur liste d'aptitude, dans le corps des assistants d'enseignement artistique à compter du 1er mars 2000 ; qu'elle a demandé la requalification du contrat de vacataire qui la liait à son employeur en contrat d'agent non titulaire pour les années 1980 à 2000, au motif qu'elle a occupé de manière continue un emploi à caractère permanent ; qu'elle a, en conséquence, également demandé la modification de son reclassement dans le corps des assistants d'enseignement artistique lors de sa titularisation et la réparation du préjudice financier lié à la qualification erronée de son contrat ; que la commune de Fourmies a refusé de faire droit à ces demandes ; que la cour administrative d'appel de Douai a confirmé le jugement du tribunal administratif de Lille rejetant les demandes formulées par l'exposante, par un arrêt dont cette dernière demande l'annulation ;<br/>
<br/>
              Considérant qu'en se bornant à constater que le nombre de vacations effectuées par Mme B...qui variait d'un mois sur l'autre et l'absence de pièces relatives aux conditions d'emploi et aux modalités de rémunération de l'exposante ne permettaient pas de regarder ces vacations comme équivalentes à un emploi permanent, sans rechercher si, d'une part, les fonctions qu'occupait Mme B...correspondaient à un besoin permanent de la ville de Fourmies et, d'autre part, si celle-ci, en faisant appel de manière constante au même agent, n'avait pas en fait instauré avec Mme B...un lien contractuel qui présente les caractéristiques énoncées à l'article 3 de la loi du 26 janvier 1984, la cour a entaché son arrêt d'erreur de droit ; que Mme B...est fondée à en demander l'annulation ;<br/>
<br/>
              Considérant qu'il y a lieu, dans les circonstances de l'espèce, de faire application des dispositions de l'article L. 761-1 du code de justice administrative et de mettre à la charge de la commune de Fourmies le versement à Mme B...de la somme de 3 000 euros au titre des frais exposés par elle et non compris dans les dépens ; qu'en revanche ces même dispositions font obstacle à ce qu'il soit mis à la charge de Mme B...qui n'est pas la partie perdante dans la présente affaire, la somme que demande la commune de Fourmies au titre de ces frais ;<br/>
<br/>
<br/>
<br/>				D E C I D E  :<br/>
              				--------------<br/>
<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Douai du 2 mai 2008 est annulé.<br/>
<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Douai.<br/>
Article 3 : La commune de Fourmies versera à Mme B...une somme de 3 000 euros en application de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Les conclusions de la commune de Fourmies tendant à l'application  des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée à Mme A...B...et à la commune de Fourmies.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">36-02-01-03 FONCTIONNAIRES ET AGENTS PUBLICS. CADRES ET EMPLOIS. NOTION DE CADRE, DE CORPS, DE GRADE ET D'EMPLOI. NOTION D'EMPLOI. - CAS DANS LESQUELS LES EMPLOIS PERMANENTS DES COLLECTIVITÉS TERRITORIALES PEUVENT ÊTRE POURVUS PAR DES AGENTS NON TITULAIRES (ARTICLE 3 DE LA LOI DU 26 JANVIER 1984) - AGENT VACATAIRE QUI A OCCUPÉ DE MANIÈRE CONTINUE UN EMPLOI À CARACTÈRE PERMANENT CORRESPONDANT À UN BESOIN PERMANENT DE LA COLLECTIVITÉ [RJ1] - CONSÉQUENCE - REQUALIFICATION DE SON CONTRAT EN CONTRAT D'AGENT NON TITULAIRE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">36-07-01-03 FONCTIONNAIRES ET AGENTS PUBLICS. STATUTS, DROITS, OBLIGATIONS ET GARANTIES. STATUT GÉNÉRAL DES FONCTIONNAIRES DE L'ÉTAT ET DES COLLECTIVITÉS LOCALES. DISPOSITIONS STATUTAIRES RELATIVES À LA FONCTION PUBLIQUE TERRITORIALE (LOI DU 26 JANVIER 1984). - CAS DANS LESQUELS LES EMPLOIS PERMANENTS DES COLLECTIVITÉS TERRITORIALES PEUVENT ÊTRE POURVUS PAR DES AGENTS NON TITULAIRES (ARTICLE 3 DE LA LOI DU 26 JANVIER 1984) - AGENT VACATAIRE QUI A OCCUPÉ DE MANIÈRE CONTINUE UN EMPLOI À CARACTÈRE PERMANENT CORRESPONDANT À UN BESOIN PERMANENT DE LA COLLECTIVITÉ [RJ1] - CONSÉQUENCE - REQUALIFICATION DE SON CONTRAT EN CONTRAT D'AGENT NON TITULAIRE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">36-12-01 FONCTIONNAIRES ET AGENTS PUBLICS. AGENTS CONTRACTUELS ET TEMPORAIRES. NATURE DU CONTRAT. - CAS DANS LESQUELS LES EMPLOIS PERMANENTS DES COLLECTIVITÉS TERRITORIALES PEUVENT ÊTRE POURVUS PAR DES AGENTS NON TITULAIRES (ARTICLE 3 DE LA LOI DU 26 JANVIER 1984) - AGENT VACATAIRE QUI A OCCUPÉ DE MANIÈRE CONTINUE UN EMPLOI À CARACTÈRE PERMANENT CORRESPONDANT À UN BESOIN PERMANENT DE LA COLLECTIVITÉ [RJ1] - CONSÉQUENCE - REQUALIFICATION DE SON CONTRAT EN CONTRAT D'AGENT NON TITULAIRE.
</SCT>
<ANA ID="9A"> 36-02-01-03 La loi du 26 janvier 1984 portant dispositions statutaires relatives à la fonction publique territoriale, dans sa rédaction antérieure à la loi n° 2009-972 du 3 août 2009, fixe, à son article 3, les cas dans lesquels les emplois permanents des collectivités territoriales peuvent par exception être pourvus par des agents non titulaires. Un agent vacataire a droit à la requalification de son contrat en contrat d'agent non titulaire s'il a occupé de manière continue un emploi à caractère permanent correspondant à un besoin permanent de la collectivité.</ANA>
<ANA ID="9B"> 36-07-01-03 La loi du 26 janvier 1984 portant dispositions statutaires relatives à la fonction publique territoriale, dans sa rédaction antérieure à la loi n° 2009-972 du 3 août 2009, fixe, à son article 3, les cas dans lesquels les emplois permanents des collectivités territoriales peuvent par exception être pourvus par des agents non titulaires. Un agent vacataire a droit à la requalification de son contrat en contrat d'agent non titulaire s'il a occupé de manière continue un emploi à caractère permanent correspondant à un besoin permanent de la collectivité.</ANA>
<ANA ID="9C"> 36-12-01 La loi du 26 janvier 1984 portant dispositions statutaires relatives à la fonction publique territoriale, dans sa rédaction antérieure à la loi n° 2009-972 du 3 août 2009, fixe, à son article 3, les cas dans lesquels les emplois permanents des collectivités territoriales peuvent par exception être pourvus par des agents non titulaires. Un agent vacataire a droit à la requalification de son contrat en contrat d'agent non titulaire s'il a occupé de manière continue un emploi à caractère permanent correspondant à un besoin permanent de la collectivité.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] CE, 14 octobre 2009, M. Masson, n° 314722, T. p. 796.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
