<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038269975</ID>
<ANCIEN_ID>JG_L_2019_03_000000427184</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/26/99/CETATEXT000038269975.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème - 7ème chambres réunies, 25/03/2019, 427184</TITRE>
<DATE_DEC>2019-03-25</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>427184</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème - 7ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LE BRET-DESACHE</AVOCATS>
<RAPPORTEUR>M. Fabio Gennari</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Guillaume Odinet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:427184.20190325</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. C...E..., Mme G...E..., M. B...H...et Mme A...E..., épouseD..., ont demandé à la cour administrative d'appel de Nancy de renvoyer devant une autre juridiction, pour cause de suspicion légitime, leur demande présentée devant le tribunal administratif de Châlons-en-Champagne sous le n° 1800820 par laquelle ils ont demandé au juge des référés, sur le fondement de l'article L. 521-2 du code de justice administrative, de suspendre la décision du 9 avril 2018 du médecin responsable du centre hospitalier universitaire de Reims d'arrêter l'alimentation et l'hydratation de M. F... E.... Par un arrêt n° 18NC03279 du 16 janvier 2019, la cour administrative d'appel a rejeté cette demande.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 18 janvier et 4 février 2019 au secrétariat du contentieux du Conseil d'Etat, M. C...E...et autres, demandent au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à leur demande.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
<br/>
              Vu : <br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - la loi du 29 juillet 1881 sur la liberté de la presse ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Fabio Gennari, auditeur,<br/>
<br/>
              - les conclusions de M. Guillaume Odinet, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Le Bret-Desache, avocat de M. C...E...et autres ; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant  ce qui suit :<br/>
<br/>
              1.	Il ressort des pièces du dossier soumis au juge du fond que M. C...E...et d'autres requérants ont présenté, le 17 avril 2018, sur le fondement de l'article L. 521-2 du code de justice administrative, au juge des référés du tribunal administratif de Châlons-en-Champagne, une demande tendant à la suspension de la décision du 9 avril 2018 du médecin responsable du centre hospitalier universitaire de Reims d'arrêter l'alimentation et l'hydratation de M. F... E.... Le 6 décembre 2018 les requérants ont saisi la cour administrative d'appel de Nancy de conclusions aux fins de renvoi, pour cause de suspicion légitime, du jugement de cette demande devant une autre juridiction. Par un arrêt en date du 16 janvier 2019, la cour administrative d'appel a rejeté la demande de renvoi et ordonné la suppression d'un passage de la requête sur le fondement des dispositions de l'article 41 de la loi du 29 juillet 1881 reproduites à l'article L. 141-2 du code de justice administrative. M. C...E...et autres ont formé un pourvoi en cassation contre cet arrêt.  <br/>
<br/>
              2.	Aux termes de l'article L. 822-1 du code de justice administrative : " Le pourvoi en cassation devant le Conseil d'Etat fait l'objet d'une procédure préalable d'admission. L'admission est refusée par décision juridictionnelle si le pourvoi est irrecevable ou n'est fondé sur aucun moyen sérieux ".<br/>
<br/>
              Sur les conclusions dirigées contre l'arrêt en tant qu'il rejette la demande de renvoi pour cause de suspicion légitime :<br/>
<br/>
              3.	Il ressort des pièces du dossier que, par une ordonnance n° 1800820 du 31 janvier 2019, postérieure à l'introduction du pourvoi de M. C...E...et des autres requérants contre l'arrêt de la cour administrative d'appel de Nancy du 16 février 2019 rejetant leurs conclusions tendant au renvoi pour cause de suspicion légitime de leur demande présentée au juge des référés du  tribunal administratif de Châlons-en-Champagne, ce dernier a statué sur cette demande. Ainsi, les conclusions du pourvoi sont, sur ce point, sans objet. Dès lors, il n'y plus lieu d'y statuer sans que les requérants, auxquels il est loisible de former un recours contre cette ordonnance, puissent utilement invoquer une méconnaissance du droit à un recours effectif.<br/>
<br/>
              Sur les conclusions dirigées contre l'arrêt en tant qu'il prononce la suppression d'un passage de la requête d'appel : <br/>
<br/>
              4.	Pour demander l'annulation, sur ce point, de l'arrêt qu'ils attaquent, M. C... E...et autres soutiennent que la cour administrative d'appel de Nancy l'a entaché d'insuffisance de motivation et d'erreur de droit au regard des articles 29 et 41 de la loi du 29 juillet 1881 et de l'article 10 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales en supprimant un passage de la requête d'appel qu'elle a jugé diffamatoire.<br/>
<br/>
              5.	Aucun de ces moyens n'est de nature à permettre l'admission des conclusions du pourvoi sur ce point.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : Il n'y a pas lieu de statuer sur les conclusions du pourvoi de M. C...E...et autres dirigées contre l'arrêt du 16 janvier 2019 de la cour administrative d'appel de Nancy en tant qu'il a rejeté leurs conclusions tendant au renvoi à une autre juridiction, pour cause de suspicion légitime, de leur demande présentée devant le juge des référés du tribunal administratif de Châlons-en-Champagne sous le n° 1800820.<br/>
<br/>
Article 2 : Les conclusions du pourvoi de M. C...E...et autres dirigées contre l'arrêt du 16 janvier 2019 de la cour administrative d'appel de Nancy en tant qu'il a prononcé la suppression d'un passage de leur requête d'appel ne sont pas admises.<br/>
Article 3 : La présente décision sera notifiée à M. C...E..., premier dénommé, pour l'ensemble des requérants.<br/>
Copie sera transmise au centre hospitalier universitaire de Reims.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-05-025 PROCÉDURE. INCIDENTS. RENVOI POUR CAUSE DE SUSPICION LÉGITIME. - POURVOI DIRIGÉ CONTRE UN ARRÊT D'UNE COUR REJETANT UNE REQUÊTE EN SUSPICION LÉGITIME CONTRE UN JUGE DES RÉFÉRÉS - INTERVENTION, AVANT QUE LE JUGE DE CASSATION NE STATUE, DE L'ORDONNANCE DU JUGE DES RÉFÉRÉS - NON LIEU EN CASSATION [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-05-05-02-05 PROCÉDURE. INCIDENTS. NON-LIEU. EXISTENCE. INTERVENTION D'UNE DÉCISION JURIDICTIONNELLE. - NON-LIEU EN CASSATION - POURVOI DIRIGÉ CONTRE UN ARRÊT D'UNE COUR REJETANT UNE REQUÊTE EN SUSPICION LÉGITIME CONTRE UN JUGE DES RÉFÉRÉS - INTERVENTION, AVANT QUE LE JUGE DE CASSATION NE STATUE, DE L'ORDONNANCE DU JUGE DES RÉFÉRÉS [RJ1].
</SCT>
<ANA ID="9A"> 54-05-025 Par une ordonnance du 31 janvier 2019, postérieure à l'introduction d'un pourvoi contre l'arrêt d'une cour administrative d'appel du 16 février 2019 rejetant des conclusions tendant au renvoi pour cause de suspicion légitime d'une demande présentée au juge des référés d'un tribunal administratif, ce dernier a statué sur cette demande. Ainsi, les conclusions du pourvoi sont, sur ce point, sans objet. Dès lors, il n'y plus lieu d'y statuer sans que les requérants, auxquels il est loisible de former un recours contre cette ordonnance, puissent utilement invoquer une méconnaissance du droit à un recours effectif.</ANA>
<ANA ID="9B"> 54-05-05-02-05 Par une ordonnance du 31 janvier 2019, postérieure à l'introduction d'un pourvoi contre l'arrêt d'une cour administrative d'appel du 16 février 2019 rejetant des conclusions tendant au renvoi pour cause de suspicion légitime d'une demande présentée au juge des référés d'un tribunal administratif, ce dernier a statué sur cette demande. Ainsi, les conclusions du pourvoi sont, sur ce point, sans objet. Dès lors, il n'y plus lieu d'y statuer sans que les requérants, auxquels il est loisible de former un recours contre cette ordonnance, puissent utilement invoquer une méconnaissance du droit à un recours effectif.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 3 février 1993,,, n° 121617, T. pp. 696-963-984.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
