<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026945800</ID>
<ANCIEN_ID>JG_L_2013_01_000000354039</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/94/58/CETATEXT000026945800.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  4ème sous-section jugeant seule, 16/01/2013, 354039, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-01-16</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>354039</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 4ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Hervé Guichon</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Gaëlle Dumortier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2013:354039.20130116</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la requête, enregistrée le 14 novembre 2011 au secrétariat du contentieux du Conseil d'Etat, présentés par la SAS Divip, dont le siège est Place du 1er Mai à Damparis (39500), représentée par son président ; la SAS Divip demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision implicite née le 24 septembre 2011 du silence gardée par la Commission nationale d'aménagement commercial, par laquelle celle-ci a rejeté le recours dirigé contre la décision du 29 avril 2011 de la commission départementale d'aménagement commercial du Jura accordant à la société Supradis l'autorisation préalable requise en vue de procéder à l'extension de 1 190 m² d'un magasin à l'enseigne " Super U " et de 458 m² de sa galerie marchande, portant ainsi leur surface totale de vente à 2 990 m² et 682 m², à Tavaux (Jura) ;<br/>
<br/>
              2°) de mettre solidairement à la charge de l'Etat la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de commerce ;<br/>
<br/>
              Vu la loi n° 73-1193 du 27 décembre 1973 ;<br/>
<br/>
              Vu la loi n° 2008-776 du 4 août 2008 ;<br/>
<br/>
              Vu le décret n° 2008-1212 du 24 novembre 2008 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Hervé Guichon, Maître des Requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Gaëlle Dumortier, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant que, par un recours enregistré le 8 juin 2011, la SAS Divip a saisi la Commission nationale d'aménagement commercial pour obtenir l'annulation de la décision par laquelle la commission départementale d'aménagement commercial du Jura a autorisé la Société Supradis à procéder à l'extension de 1 190 m² d'un magasin à l'enseigne " Super U " et de 458 m² de sa galerie marchande, à Tavaux (Jura) ; que la commission nationale ne s'étant pas prononcée sur ce recours avant l'expiration du délai de quatre mois prévu par les dispositions de l'article L. 752-17 du code de commerce, une décision implicite de rejet du recours de la SAS Divip, valant autorisation implicite au profit de la Société Supradis, est intervenue le 8 octobre 2011 ; <br/>
<br/>
              Sur la légalité de la décision attaquée :<br/>
<br/>
              En ce qui concerne la maîtrise foncière du projet : <br/>
<br/>
              2. Considérant que si la société requérante soutient que la société demanderesse ne justifiait pas de la maîtrise foncière de la totalité du terrain d'assiette du projet, il ressort des pièces du dossier que le protocole conclu entre la commune de Tavaux, représentée par son maire, et la société demanderesse, par lequel la commune de Tavaux s'engageait à céder une partie du terrain d'assiette à ladite société, avait été autorisé par une délibération du conseil municipal en date du 17 décembre 2009 puis approuvé par une nouvelle délibération, intervenue le 20 décembre 2010, avant la date du 31 décembre 2010 fixée par le protocole ; que, par ailleurs, il ne ressort pas des pièces du dossier que la voie dénommée " rue de Rome ", qui avait été établie lors de la réalisation d'un lotissement privé, aurait fait partie du domaine public de la commune ; qu'il résulte de ce qui précède que le moyen tiré d'une absence de maîtrise foncière de l'ensemble du terrain d'assiette du projet par la société pétitionnaire doit être écarté ;<br/>
<br/>
              En ce qui concerne la délimitation de la zone de chalandise :<br/>
<br/>
              3. Considérant qu'il ressort des pièces du dossier que la zone de chalandise définie par la société pétitionnaire a été délimitée par des temps de trajet allant jusqu'à 25 minutes et tenant compte des différentes barrières naturelles et psychologiques propres à la zone en question ; qu'il ne ressort pas des pièces du dossier que cette délimitation, dans les circonstances de l'espèce, méconnaisse les dispositions de l'article R. 752-8 du code de commerce ;<br/>
<br/>
              En ce qui concerne l'appréciation de la Commission nationale d'aménagement commercial :<br/>
<br/>
              4. Considérant qu'il appartient aux commissions d'aménagement commercial, lorsqu'elles se prononcent sur un projet d'exploitation commerciale soumis à autorisation en application de l'article L. 752-1 du code de commerce, d'apprécier la conformité de ce projet aux objectifs prévus à l'article 1er de la loi du 27 décembre 1973 et à l'article L. 750-1 du code de commerce, au vu des critères d'évaluation mentionnés à l'article L. 752-6 du même code ; que l'autorisation ne peut être refusée que si, eu égard à ses effets, le projet compromet la réalisation de ces objectifs ;<br/>
<br/>
              5. Considérant, en premier lieu, que si la société requérante soutient que l'autorisation demandée aurait dû être rejetée en raison de la densité de l'équipement commercial existant dans la zone de chalandise, il résulte de ce qui a été dit ci-dessus que la commission nationale n'avait pas à prendre en compte un tel critère ; <br/>
<br/>
              6. Considérant, en deuxième lieu, que si la société requérante soutient que le projet porte atteinte à l'objectif d'aménagement du territoire, il ressort des pièces du dossier, d'une part, qu'il n'a pas d'effets négatifs sur l'animation de la vie urbaine, d'autre part, que deux lignes de transports en commun du réseau " Transport du Grand Dole " desservent son site d'implantation ; que, dès lors, le moyen doit être écarté ;<br/>
<br/>
              7. Considérant, en troisième lieu, que si la société requérante soutient que le projet ne répondrait pas suffisamment à l'objectif de développement durable, il ne ressort pas des pièces du dossier, eu égard notamment aux engagements pris par la société demanderesse en termes de matériaux utilisés pour la construction du projet ainsi que de réduction de la consommation d'énergie et de traitement des déchets, que le projet autorisé porterait atteinte à cet objectif ; que le moyen tiré d'un défaut de qualité environnementale du projet doit être écarté ;<br/>
<br/>
              8. Considérant en dernier lieu qu'il ne ressort pas des pièces du dossier que la commission nationale aurait fondé sa décision sur un schéma de cohérence territorial en cours d'élaboration et dépourvu d'effet juridique ;<br/>
<br/>
              9. Considérant qu'il résulte de ce qui précède que la société requérante n'est pas fondée à demander l'annulation de la décision attaquée ;<br/>
<br/>
              Sur les conclusions tendant à l'application des dispositions de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              10. Considérant que ces dispositions font obstacle à ce qu'il soit fait droit aux conclusions présentées par la SAS Divip ; qu'en revanche il y a lieu, dans les circonstances de l'espèce, de faire application de ces dispositions et mettre à la charge de la SAS Divip la somme de 5 000 euros à verser à la société Supradis ;<br/>
<br/>
<br/>
<br/>
<br/>
              			D E C I D E :<br/>
              			----------------<br/>
<br/>
            Article 1er : La requête de la SAS Divip est rejetée.<br/>
<br/>
Article 2 : La SAS Divip versera à la société Supradis la somme de 5 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 3 : La présente décision sera notifiée à la SAS Divip, à la société Supradis et à la Commission nationale d'aménagement commercial. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
