<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042322402</ID>
<ANCIEN_ID>JG_L_2020_09_000000425377</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/32/24/CETATEXT000042322402.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème chambre, 09/09/2020, 425377, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-09-09</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>425377</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP HEMERY, THOMAS-RAQUIN, LE GUERER ; SCP LESOURD</AVOCATS>
<RAPPORTEUR>Mme Françoise Tomé</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Dieu</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2020:425377.20200909</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu les procédures suivantes :<br/>
<br/>
              M. A... B... a demandé au tribunal administratif de Dijon d'annuler pour excès de pouvoir les décisions des 13 février et 10 mars 2015 par lesquelles le maire de Saint-Loup-d'Ordon a refusé de lui accorder une dérogation de secteur scolaire pour quatre de ses enfants et de condamner cette commune à lui verser la somme de 14 000 euros en réparation des préjudices nés de ces décisions. Par un jugement n° 1501673 du 16 juin 2016, le tribunal administratif a rejeté sa demande. <br/>
<br/>
              Par un arrêt du 12 juillet 2018, la cour administrative d'appel de Lyon a, sur appel de M. B..., annulé ce jugement en tant qu'il a statué sur les conclusions d'annulation, dit qu'il n'y a pas lieu de statuer sur ces conclusions et rejeté le surplus de la requête d'appel. <br/>
<br/>
              1° Sous le n° 425377, par un pourvoi sommaire et un mémoire complémentaire, enregistrés le 14 novembre 2018 et le 13 février 2019 au secrétariat du contentieux du Conseil d'Etat, M. B... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) de mettre à la charge de la commune de Saint-Loup-d'Ordon la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative et de l'article 37 de la loi du 10 juillet 1991. <br/>
<br/>
<br/>
<br/>
              2° Sous le n° 428000, par une requête enregistrée le 14 février 2019, M. B... demande de surseoir à l'exécution de cet arrêt et de mettre à la charge de la commune de Saint-Loup-d'Ordon la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative et de l'article 37 de la loi du 10 juillet 1991.<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu : <br/>
              - le code de l'éducation ; <br/>
              - le code général des collectivités territoriales ; <br/>
              - la loi n° 91-647 du 10 juillet 1991 ; <br/>
              - le code de justice administrative et l'ordonnance n° 2020-305 du 25 mars 2020 modifiée ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Françoise Tomé, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Frédéric Dieu, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Hémery, Thomas-Raquin, Le Guerer, avocat de M. B... et à la SCP Lesourd, avocat de la commune de Saint-Loup-d'Ordon ;<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces soumises aux juges du fond que, par une décision du 13 février 2015, le maire de Saint-Loup-d'Ordon (Yonne) a refusé de donner son accord à l'inscription dérogatoire dans les écoles de la commune de Courtenay (Loiret) de quatre des enfants de M. B..., domicilié dans la commune. Le recours gracieux formé par ce dernier contre cette décision a été rejeté le 10 mars 2015. Alors que M. B... avait déjà saisi le tribunal administratif de Dijon d'un recours, le maire de Saint-Loup-d'Ordon a, le 2 octobre 2015, retiré ses deux précédentes décisions et s'est déclaré incompétent pour statuer sur la demande de dérogation dont il estimait qu'elle relevait de la compétence du syndicat intercommunal d'intérêt scolaire associant sa commune ainsi que celles de Saint-Martin-d'Ordon et de Cudot. Par un jugement du 16 juin 2016, le tribunal administratif de Dijon a rejeté les demandes de M. B... tendant à l'annulation des décisions des 13 février et 10 mars 2015 ainsi qu'à l'indemnisation de préjudices qu'il estime qu'elles lui ont causés. M. B... se pourvoit en cassation contre l'arrêt du 12 juillet 2018 par lequel la cour administrative d'appel de Lyon a rejeté son appel contre ce jugement. <br/>
<br/>
              2. En premier lieu, un recours pour excès de pouvoir dirigé contre un acte administratif n'a d'autre objet que d'en faire prononcer l'annulation avec effet rétroactif. Si, avant que le juge n'ait statué, l'acte attaqué est rapporté par l'autorité compétente et si le retrait ainsi opéré acquiert un caractère définitif faute d'être critiqué dans le délai du recours contentieux, il emporte alors disparition rétroactive de l'ordonnancement juridique de l'acte contesté, ce qui conduit à ce qu'il n'y ait lieu, pour le juge de la légalité, de statuer sur le mérite du pourvoi dont il était saisi. Il en va ainsi, quand bien même l'acte rapporté aurait reçu exécution. <br/>
<br/>
              3. Par l'arrêt attaqué, la cour administrative d'appel de Lyon a relevé, par une appréciation souveraine exempte de dénaturation, que, par sa décision du 2 octobre 2015, le maire de Saint-Loup-d'Ordon avait rapporté ses deux décisions des 13 février et 10 mars 2015 et que ce retrait n'avait pas été contesté par M. B.... En en déduisant que ce retrait avait, dès lors, acquis un caractère définitif et qu'il n'y avait plus lieu de statuer sur les conclusions aux fins d'annulation pour excès de pouvoir présentées par M. B... à l'encontre des deux décisions que la décision du 2 octobre 2015 avait retirées, la cour n'a pas entaché son arrêt d'erreur de droit.  <br/>
<br/>
              4. En deuxième lieu, l'arrêt n'est pas insuffisamment motivé en ce qu'il juge que la réalité des préjudices invoqués par M. B... n'est pas établie.<br/>
<br/>
              5. En dernier lieu, aux termes de l'article L. 761-1 du code de justice administrative : " Dans toutes les instances, le juge condamne la partie tenue aux dépens ou, à défaut, la partie perdante, à payer à l'autre partie la somme qu'il détermine, au titre des frais exposés et non compris dans les dépens. Le juge tient compte de l'équité ou de la situation économique de la partie condamnée. Il peut, même d'office, pour des raisons tirées des mêmes considérations, dire qu'il n'y a pas lieu à cette condamnation ".  Ces dispositions ne font pas, par elles-mêmes, obstacle à ce que le paiement de frais exposés et non compris dans les dépens soit mis à la charge d'une partie qui bénéficie de l'aide juridictionnelle. <br/>
<br/>
              6. En estimant que M. B... devait être regardé comme la partie perdante en appel et en mettant à sa charge, alors même qu'il bénéficiait de l'aide juridictionnelle, le versement à la commune de Saint-Loup-d'Ordon de la somme de 800 euros au titre de l'article L. 761-1 du code de justice administrative, la cour administrative d'appel de Lyon a exactement qualifié les faits qui lui étaient soumis, n'a pas commis d'erreur de droit et s'est livrée une appréciation souveraine qui échappe au contrôle du Conseil d'Etat statuant comme juge de cassation. <br/>
<br/>
              7. Il résulte de tout ce qui précède que le pourvoi de M. B... doit être rejeté, y compris en ce qu'il présente des conclusions au titre de l'article L. 761-1 et de l'article 37 de la loi du 10 juillet 1991. <br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de M. B... est rejeté. <br/>
Article 2: La présente décision sera notifiée à M. A... B... et à la commune de Saint-Loup-d'Ordon. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
