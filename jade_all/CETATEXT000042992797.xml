<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042992797</ID>
<ANCIEN_ID>JG_L_2020_12_000000447227</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/99/27/CETATEXT000042992797.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 18/12/2020, 447227, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-12-18</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>447227</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2020:447227.20201218</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu les procédures suivantes :<br/>
<br/>
              M. A... C... et Mme B... C... ont demandé au juge des référés du tribunal administratif de Bordeaux, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, d'enjoindre au recteur de l'académie de Bordeaux d'autoriser leur fille Capucine à poursuivre sa scolarité au sein de l'école élémentaire Jules Verne de Villenave-d'Ornon en dérogeant à l'obligation de port d'un masque de protection sanitaire ou, subsidiairement, de prévoir des mesures de scolarisation alternatives à son accueil dans cet établissement. Par une ordonnance n° 2005506 du 4 décembre 2020, le juge des référés a rejeté leur demande.<br/>
<br/>
              Par une requête, enregistrée le 4 décembre 2020 au secrétariat du contentieux du Conseil d'Etat, M. A... C... et Mme B... C... demandent au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative : <br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) de faire droit à leurs conclusions de première instance ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 2 200 euros en application des dispositions de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Ils soutiennent que : <br/>
              - leur requête est recevable ; <br/>
              - la condition d'urgence est remplie eu égard, d'une part, à la situation de crise sanitaire et, d'autre part, à la déscolarisation de leur fille depuis le 26 novembre 2020 ; <br/>
              - il est porté une atteinte grave et manifestement illégale au droit à la protection de la santé et, en raison de l'entrave mise à la poursuite de sa scolarité, à l'intérêt supérieur de l'enfant, eu égard à sa pathologie asthmatique, établie par trois certificats médicaux, qui doit être regardée comme un handicap justifiant, en application du décret du 29 octobre 2020, une dérogation à l'obligation de port du masque.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la Constitution, notamment son Préambule ;<br/>
              - la convention internationale relative aux droits de l'enfant, signée à New York le 26 janvier 1990 ; <br/>
              - le code de l'éducation ;<br/>
              - le code de la santé publique ;<br/>
              - la loi n° 2020-1379 du 14 novembre 2020 ;<br/>
              - le décret n° 2020-1257 du 14 octobre 2020 ;<br/>
              - le décret n° 2020-1310 du 29 octobre 2020; <br/>
              - le code de justice administrative ; <br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. L'article L. 511-1 du code de justice administrative dispose que : " Le juge des référés statue par des mesures qui présentent un caractère provisoire. Il n'est pas saisi du principal et se prononce dans les meilleurs délais ". Aux termes de l'article L. 521-2 du même code : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. Le juge des référés se prononce dans un délai de quarante-huit heures ". Enfin, en vertu de l'article L. 522-3 du même code, le juge des référés peut, par une ordonnance motivée, rejeter une requête sans instruction ni audience lorsque la condition d'urgence n'est pas remplie ou lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable ou qu'elle est mal fondée.<br/>
<br/>
              Sur le cadre du litige :<br/>
<br/>
              2. L'émergence d'un nouveau coronavirus, responsable de la maladie à coronavirus 2019 ou covid-19 et particulièrement contagieux, a été qualifiée d'urgence de santé publique de portée internationale par l'Organisation mondiale de la santé le 30 janvier 2020, puis de pandémie le 11 mars 2020. La propagation du virus sur le territoire français a conduit le ministre des solidarités et de la santé puis le Premier ministre à prendre, à compter du 4 mars 2020, des mesures de plus en plus strictes destinées à réduire les risques de contagion. Pour faire face à l'aggravation de l'épidémie, la loi du 23 mars 2020 a créé un régime d'état d'urgence sanitaire aux articles L. 3131-12 à L. 3131-20 du code de la santé publique et déclaré l'état d'urgence sanitaire pour une durée de deux mois à compter du 24 mars 2020. La loi du 11 mai 2020 prorogeant l'état d'urgence sanitaire et complétant ces dispositions, a prorogé cet état d'urgence sanitaire jusqu'au 10 juillet 2020 inclus. L'évolution de la situation sanitaire a conduit à un assouplissement des mesures prises et la loi du 9 juillet 2020, a organisé un régime de sortie de cet état d'urgence.<br/>
<br/>
              3. Une nouvelle progression de l'épidémie au cours des mois de septembre et octobre 2020 a conduit le Président de la République à prendre, sur le fondement des articles L. 3131-12 et L. 3131-13 du code de la santé publique, le décret du 14 octobre 2020 déclarant l'état d'urgence à compter du 17 octobre à zéro heure sur l'ensemble du territoire national. Le 29 octobre 2020, le Premier ministre a pris, sur le fondement de l'article L. 3131-15 du code de la santé publique, un décret prescrivant les mesures générales nécessaires pour faire face à l'épidémie de covid-19 dans le cadre de l'état d'urgence sanitaire. En vertu de l'article 1er de la loi du 14 novembre 2020 autorisant la prorogation de l'état d'urgence sanitaire et portant diverses mesures de gestion de la crise sanitaire, l'état d'urgence sanitaire a été prorogé jusqu'au 16 février 2021 inclus. <br/>
<br/>
              Sur la demande en référé :<br/>
<br/>
              4. D'une part, le gouvernement, en prenant les mesures détaillées par le décret du 29 octobre 2020, a fait le choix d'une politique qui cherche à casser la dynamique de progression du virus. Aux termes de l'article 1er de ce décret: " I. - Afin de ralentir la propagation du virus, les mesures d'hygiène définies en annexe 1 au présent décret et de distanciation sociale, incluant la distanciation physique d'au moins un mètre entre deux personnes, dites barrières, définies au niveau national, doivent être observées en tout lieu et en toute circonstance ". L'article 36 du même décret dispose que: " II. - Portent un masque de protection : / (...) 3° Les élèves des écoles élémentaires ". Aux termes du II de l'annexe 1 à ce décret : " II. - L'obligation de porter un masque de protection mentionnée au présent décret s'applique aux personnes de onze ans ou plus, ainsi que dans les cas mentionnés aux 3° et 5° du II de l'article 36. " Enfin, le deuxième alinéa de l'article 2 de ce décret dispose que : " Les obligations de port du masque prévues au présent décret ne s'appliquent pas aux personnes en situation de handicap munies d'un certificat médical justifiant de cette dérogation et qui mettent en oeuvre les mesures sanitaires de nature à prévenir la propagation du virus ".<br/>
<br/>
              5. D'autre part, aux termes de l'article R. 421-10 du code de l'éducation : " En qualité de représentant de l'État au sein de l'établissement, le chef d'établissement : (...) / 2° Veille au bon déroulement des enseignements, de l'information, de l'orientation et du contrôle des connaissances des élèves ; / 3° Prend toutes dispositions, en liaison avec les autorités administratives compétentes, pour assurer la sécurité des personnes et des biens, l'hygiène et la salubrité de l'établissement ; / 4° Est responsable de l'ordre dans l'établissement. Il veille au respect des droits et des devoirs de tous les membres de la communauté scolaire et assure l'application du règlement intérieur ; (...) ". Aux termes de l'article R. 421-12 du même code : " En cas de difficultés graves dans le fonctionnement d'un établissement, le chef d'établissement peut prendre toutes dispositions nécessaires pour assurer le bon fonctionnement du service public. / S'il y a urgence, et notamment en cas de menace ou d'action contre l'ordre dans les enceintes et locaux scolaires de l'établissement, le chef d'établissement, sans préjudice des dispositions générales réglementant l'accès aux établissements, peut : / 1° Interdire l'accès de ces enceintes ou locaux à toute personne relevant ou non de l'établissement ; / (...) ".<br/>
<br/>
              6. Il résulte des termes de l'ordonnance attaquée que, pour rejeter la demande de M. et Mme C... tendant à ce qu'il soit enjoint à l'administration d'autoriser la poursuite de la scolarité de leur fille à l'école Jules Verne de Villenave-D'Ornon en dérogeant à l'obligation de port du masque, le juge des référés du tribunal administratif de Bordeaux s'est fondé, d'une part, sur la circonstance que la pathologie asthmatique dont elle est atteinte, n'est, eu égard, aux termes des certificats médicaux produits, pas assimilable à un handicap justifiant une telle dispense, d'autre part à l'utilité, relevée par le Haut conseil de la santé publique dans un avis du 29 octobre 2020, du port du masque pour la protection des enfants atteints d'une pathologie respiratoire sévère, et, enfin, sur la possibilité qui a été offerte à M. et Mme C... d'assurer une continuité  pédagogique en inscrivant leur fille au Centre national d'enseignement à  distance. Si M. et Mme C... relèvent appel de cette ordonnance en persistant à regarder dans la décision qui leur a été opposée une atteinte grave et manifestement illégale à la protection de la santé publique et au droit à l'éducation et une mesure contraire à l'intérêt supérieur de l'enfant, ils n'apportent, au soutien de cet appel, aucun élément de nature à infirmer l'appréciation portée par le juge des référés au regard des éléments qui viennent d'être énoncés. En particulier, la circonstance que l'article 2 du décret du 29 octobre 2020 ne définit pas la notion de handicap qu'il mentionne est sans cette incidence sur cette appréciation. Le juge des référés, qui n'a pas exclu qu'une situation de handicap justifie une dispense de port du masque, s'est prononcé sur ce point sur la base de l'ensemble des éléments qui lui étaient soumis, par une appréciation qui n'est pas utilement critiquée en appel.  <br/>
<br/>
              7. Il résulte de ce qui précède qu'il est manifeste que l'appel de M. et Mme C... ne peut être accueilli. Leur requête ne peut, dès lors, qu'être rejetée, selon la procédure prévue par l'article L. 522-3 du code de justice administrative, y compris les conclusions présentées en application de l'article L. 761-1 du même code.<br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
              ------------------<br/>
<br/>
Article 1er : La requête de M. et Mme C... est rejetée. <br/>
Article 2 : La présente ordonnance sera notifiée à M. A... C... et à Mme B... C.... <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
