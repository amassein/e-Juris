<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043605977</ID>
<ANCIEN_ID>JG_L_2021_05_000000452359</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/60/59/CETATEXT000043605977.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 17/05/2021, 452359, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-05-17</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>452359</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2021:452359.20210517</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B... A... a demandé au juge des référés du tribunal administratif de Nantes, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, à titre principal d'ordonner la suspension de l'exécution de l'arrêté du 2 mai 2021 par lequel le préfet de la Sarthe a imposé, jusqu'au 9 juin 2021 inclus, le port du masque à toute personne de onze ans et plus dans l'ensemble des lieux publics de la zone agglomérée des communes du département et, à titre subsidiaire, d'ordonner au préfet de la Sarthe de modifier cet arrêté en dispensant les communes rurales du département, en particulier celle de Solesmes, de l'obligation du port du masque. Par une ordonnance n° 2104885 du 7 mai 2021, le juge des référés du tribunal administratif de Nantes a rejeté sa demande. <br/>
<br/>
              Par une requête et un mémoire en complémentaire, enregistrés les 7 et 13 mai 2021 au secrétariat du contentieux du Conseil d'Etat, M. Bunel demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) de faire droit à ses demandes de première instance ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 1 500 euros au titre de l'article L. 761-1 du code de justice administrative et la somme de 3 000 euros à verser à son conseil, non encore désigné à ce jour par le bureau d'aide juridictionnelle, au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991.<br/>
<br/>
<br/>
              Il soutient que :<br/>
              - la condition d'urgence est satisfaite dès lors que les dispositions de l'arrêté contesté portent atteinte, de manière immédiate et durable, à sa liberté d'aller et venir ;  <br/>
              - il justifie d'un intérêt à agir ;<br/>
              - l'ordonnance attaquée est entachée d'un vice de forme grave, dès lors qu'aucune motivation n'est fournie à l'appui de la décision de ne pas organiser d'audience pour l'examen de sa requête ;<br/>
              - la mesure contestée porte une atteinte grave et manifestement illégale à la liberté d'aller et venir en ce qu'elle est disproportionnée à l'objectif de santé publique poursuivi par les pouvoirs publics dès lors que le risque de propagation du virus est résiduel sur la voie publique, exception faite des rassemblements, et à plus forte raison dans les zones rurales peu densément peuplées ;<br/>
              - elle est dépourvue de toute justification dès lors que le plan de de´confinement annoncé le 28 avril 2021 par le président de la République prévoit l'ouverture des terrasses des cafés et restaurant, où les masques ne seront pas obligatoires, à compter du 19 mai 2021.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la Constitution, notamment son Préambule ;<br/>
              - le code de la santé publique ;<br/>
              - la loi n° 2021-160 du 15 février 2021 ;<br/>
              - l'ordonnance n° 2020-1402 du 18 novembre 2020 ;<br/>
              - le décret n° 2020-1257 du 14 octobre 2020 ;<br/>
              - le décret n° 2020-1310 du 29 octobre 2020 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 521-2 du même code : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. (...) ". En vertu de l'article L. 522-3 du même code, le juge des référés peut, par une ordonnance motivée, rejeter une requête sans instruction ni audience lorsque la condition d'urgence n'est pas remplie ou lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable ou qu'elle est mal fondée. Il appartient au juge des référés saisi en appel de porter son appréciation sur ce point au regard de l'ensemble des pièces du dossier, et notamment des éléments recueillis par le juge de première instance dans le cadre de la procédure qu'il a diligentée.<br/>
<br/>
              2.  Une nouvelle progression de l'épidémie de covid-19 sur le territoire national a conduit le président de la République à prendre, le 14 octobre 2020, sur le fondement des articles L. 3131-12 et L. 3131-13 du code de la santé publique, un décret déclarant l'état d'urgence sanitaire à compter du 17 octobre 2020 sur l'ensemble du territoire de la République. L'article 1er de la loi du 14 novembre 2020 autorisant la prorogation de l'état d'urgence sanitaire et portant diverses mesures de gestion de la crise sanitaire, modifié par la loi du 15 février 2021, a prorogé l'état d'urgence sanitaire jusqu'au 1er juin 2021 inclus. Le Premier ministre a pris, sur le fondement de l'article L. 3131-15 du code de la santé publique, le décret du 29 octobre 2020 susvisé prescrivant les mesures générales nécessaires pour faire face à l'épidémie de covid-19 dans le cadre de l'urgence sanitaire. Aux termes du deuxième alinéa du II de l'article 1er de ce décret : " Dans les cas où le port du masque n'est pas prescrit par le présent décret, le préfet de département est habilité à le rendre obligatoire, sauf dans les locaux d'habitation, lorsque les circonstances locales l'exigent ".<br/>
<br/>
              3. Par un arrêté du 2 mai 2021 portant obligation du port du masque pour les personnes de onze ans et plus sur l'ensemble des communes du département de la Sarthe, pris sur le fondement de ce décret, le préfet de la Sarthe a imposé le port d'un masque de protection à tout piéton de onze ans et plus, jusqu'au 9 juin 2021 inclus, dans l'ensemble des lieux publics, y compris les voies publiques et les espaces publics de plein air, de la zone agglomérée des communes de la Sarthe. M. Bunel relève appel de l'ordonnance du 7 mai 2021 par laquelle le juge des référés du tribunal administratif de Nantes a rejeté ses demandes, présentées sur le fondement de l'article L. 521-2 du code de justice administrative, tendant à la suspension de l'exécution de cet arrêté en ce qu'il impose le port du masque à toute personne à pied de onze ans et plus dans l'ensemble des lieux publics de la zone agglomérée des communes du département.<br/>
<br/>
              4. En premier lieu, aux termes du premier alinéa de l'article 3 de l'ordonnance du 18 novembre 2020 portant adaptation des règles applicables aux juridiction susvisée : " Outre les cas prévus à l'article L. 522-3 du code de justice administrative, il peut être statué sans audience, par ordonnance motivée, sur les requêtes présentées en référé. Le juge des référés informe les parties de l'absence d'audience et fixe la date à partir de laquelle l'instruction sera close. ". Contrairement à ce que soutient M. Bunel, si ce texte prévoit que l'ordonnance rendue lorsqu'il est statué sans audience doit être motivée, il n'impose nullement que cette motivation porte sur la décision de ne pas tenir d'audience, dont les parties doivent seulement être informées par le juge des référés. En conséquence, le moyen tiré de ce que l'ordonnance attaquée serait entachée de vice de forme du fait de l'absence de motivation de cette décision ne peut qu'être écarté. <br/>
<br/>
              5. En second lieu, pour rejeter la demande de M. Bunel, le juge des référés du tribunal administratif de Nantes a relevé, en premier lieu, que la situation épidémiologique dans la région Pays de la Loire, et particulièrement dans la Sarthe, est préoccupante avec des indicateurs épidémiologiques et hospitaliers très dégradés. En deuxième lieu, il a constaté que la mesure, qui a pour objet d'imposer le port du masque dans les seules zones agglomérées des communes du département et ne s'applique notamment pas aux pratiques sportives et physiques sur la voie publique, n'est pas incompatible avec le calendrier du déconfinement annoncé par le président de la République et est justifiée par des nécessités de santé publique. En dernier lieu, il a estimé que, contrairement à ce que soutient M. Bunel, une telle mesure ne porte nullement atteinte à la liberté de circuler de ce dernier. Le juge des référés du tribunal administratif de Nantes en a déduit que l'arrêté contesté ne porte pas une atteinte grave et manifestement illégale à la liberté fondamentale d'aller et venir. M. Bunel n'apporte aucun élément nouveau en appel susceptible d'infirmer l'appréciation ainsi retenue par le juge des référés de première instance.<br/>
<br/>
              6. Il résulte de ce qui précède qu'il est manifeste que l'appel de M. Bunel ne peut être accueilli. Sa requête ne peut, dès lors, qu'être rejetée, selon la procédure prévue par l'article L. 522-3 du code de justice administrative, y compris, par voie de conséquence, les conclusions qu'elle présente au titre de l'article L. 761-1 du même code.<br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
              ------------------<br/>
<br/>
Article 1er : La requête de M. Bunel est rejetée. <br/>
Article 2 : La présente ordonnance sera notifiée à M. Jean-Dominique Bunel. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
