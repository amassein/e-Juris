<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000045122237</ID>
<ANCIEN_ID>JG_L_2022_02_000000438196</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/45/12/22/CETATEXT000045122237.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème - 8ème chambres réunies, 03/02/2022, 438196, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2022-02-03</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>438196</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème - 8ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BUK LAMENT - ROBILLOT ; SCP WAQUET, FARGE, HAZAN</AVOCATS>
<RAPPORTEUR>M. Laurent-Xavier Simonel</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Laurent Cytermann</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2022:438196.20220203</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              La société anonyme HLM Immobilière Atlantic Aménagement a demandé au tribunal administratif de Poitiers de condamner la commune de Saint-Georges-de-Didonne (Charente-Maritime) à lui verser une indemnité de 71 549, 41 euros hors taxes, assortie des intérêts au taux légal, en réparation des préjudices qu'elle estimait avoir subis du fait de l'abandon de la réalisation d'un projet immobilier causé par le refus de la commune de réitérer la vente du terrain d'assiette.<br/>
<br/>
              Par un jugement n° 1502624 du 18 octobre 2017, le tribunal administratif de Poitiers a, notamment, rejeté cette demande comme portée devant un ordre de juridiction incompétent pour en connaître.<br/>
<br/>
              Par un arrêt n° 17BX03994 du 2 décembre 2019, la cour administrative d'appel de Bordeaux, sur l'appel formé par la société anonyme HLM Immobilière Atlantic Aménagement, a annulé ce jugement en ce qu'il avait décliné la compétence de la juridiction administrative pour connaître de sa demande indemnitaire et rejeté le surplus de ses conclusions.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un nouveau mémoire, enregistrés les 3 février 2020, 12 mai 2020 et 21 septembre 2021 au secrétariat du contentieux du Conseil d'Etat, la société anonyme HLM Immobilière Atlantic Aménagement demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de la commune de Saint-Georges-de-Didonne la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code civil ;<br/>
              - le code général des collectivités territoriales ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Laurent-Xavier Simonel, conseiller d'Etat en service extraordinaire, <br/>
<br/>
              - les conclusions de M. Laurent Cytermann, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Waquet, Farge, Hazan, avocat de la société anonyme HLM Immobilière Atlantic Aménagement et à la SCP Buk Lament - Robillot, avocat de la commune de Saint-Georges de Didonne ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              Sur le pourvoi de la société anonyme HLM Immobilière Atlantic Aménagement :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond qu'en vue de l'acquisition d'une partie du terrain d'assiette nécessaire à la réalisation de son projet de construction d'une résidence de dix-sept logements sociaux sur le territoire de la commune de Saint-Georges-de-Didonne (Charente-Maritime), pour lequel elle a obtenu un permis de construire le 30 juin 2011, la société anonyme HLM Immobilière Atlantic Aménagement s'est portée acquéreuse des 2 332 mètres carrés des parcelles cadastrées section BC n° 1272 à 1275 et n° 1548 appartenant à cette commune. En retenant la valeur de 86,45 euros par mètre carré estimée par France Domaine le 18 juillet 2011 pour une période d'un an suivant la date de cette estimation, le conseil municipal de Saint-Georges-de-Didonne, par sa délibération du 21 septembre 2011, a approuvé la vente de ces parcelles à la société anonyme HLM Immobilière Atlantic Aménagement au prix de 201 600 euros, moyennant rétrocession au domaine public communal du parc de stationnement de la résidence et à condition de prévoir, dans l'acte authentique de vente, une clause de retour dans le patrimoine communal en cas de non-réalisation du projet. Cependant, alors que l'acte authentique de vente n'avait pas encore été conclu, le maire de Saint-Georges-de-Didonne a, le 23 août 2013, demandé à cette société le prix de 514 206 euros, conformément à la nouvelle valeur du mètre carré telle qu'estimée par France Domaine, le 10 juillet 2013, à 220,50 euros. Après avoir contesté ce dernier prix, la société anonyme HLM Immobilière Atlantic Aménagement a demandé au tribunal administratif de Poitiers de condamner la commune de Saint-Georges-de-Didonne à lui verser une indemnité de 71 549,41 euros hors taxes, assortie des intérêts au taux légal, en réparation des préjudices qu'elle estime avoir subis du fait de l'abandon de son projet immobilier. Cette demande a été rejetée par un jugement du 18 octobre 2017 au motif de l'incompétence de la juridiction administrative pour en connaître. La société anonyme HLM Immobilière Atlantic Aménagement se pourvoit en cassation contre l'arrêt du 2 décembre 2019 par lequel la cour administrative d'appel de Bordeaux, après avoir annulé ce jugement, a rejeté les conclusions de sa requête. Eu égard aux moyens qu'elle soulève, elle doit être regardée comme demandant l'annulation de cet arrêt en tant seulement qu'il a rejeté le surplus des conclusions de sa requête.<br/>
<br/>
              2. La délibération d'un conseil municipal d'une commune proposant le transfert de propriété de biens immobiliers relevant de son domaine privé moyennant des modalités déterminées, notamment de prix ou d'affectation future, crée des droits au profit de son bénéficiaire.<br/>
              3. Dès lors, en se fondant, pour juger que la délibération du conseil municipal de Saint-Georges-de-Didonne du 21 septembre 2011 n'était pas créatrice de droits au bénéfice de la société anonyme HLM Immobilière Atlantic Aménagement, sur la circonstance que cette délibération était assortie d'une condition résolutoire consistant dans une clause de retour des parcelles dans le patrimoine communal en cas de non-réalisation du projet, la cour administrative d'appel de Bordeaux a commis une erreur de droit. Par suite, sans qu'il soit besoin de se prononcer sur les autres moyens du pourvoi, la société anonyme HLM Immobilière Atlantic Aménagement est fondée à demander l'annulation de l'arrêt attaqué en tant qu'il a rejeté le surplus de ses conclusions. <br/>
<br/>
              4. Il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond dans cette mesure, en application des dispositions de l'article L. 821-2 du code de justice administrative.<br/>
<br/>
              Sur la responsabilité de la commune de Saint-Georges-de-Didonne :<br/>
<br/>
              5. En premier lieu, la société anonyme HLM Immobilière Atlantic Aménagement ne peut rechercher la responsabilité contractuelle de la commune de Saint-Georges-de-Didonne, en l'absence de tout contrat liant la société à la commune.<br/>
<br/>
              6. En second lieu, il résulte de ce qui a été dit ci-dessus que la délibération du 21 septembre 2011 du conseil municipal de Saint-Georges-de-Didonne, approuvant la vente des parcelles cadastrées BC n° 1272 à 1275 et n° 1548 au prix de 201 600 euros avec une clause particulière de retour dans le patrimoine communal en cas de non-réalisation du projet de construction de dix-sept logements à caractère social, a créé des droits au profit de la société acquéreuse. Toutefois, il ressort des pièces du dossier que la délibération du conseil municipal doit être regardée comme ayant créé des droits pendant une durée limitée à un an, correspondant à la période durant laquelle la valeur vénale des terrains, fixée par référence à l'estimation du service France Domaine, restait valable. Par suite, faute pour l'acte authentique d'avoir été signé avant le terme de ce délai, la société n'est pas fondée à se prévaloir des droits qu'elle détiendrait de la délibération du conseil municipal du 21 septembre 2011 pour soutenir que le maire de la commune aurait, en décidant le 23 août 2013 de ne pas procéder à la vente au prix initialement fixé, commis une illégalité fautive de nature à engager la responsabilité de la commune ou aurait méconnu l'espérance légitime que lui conférait la délibération du 21 septembre 2011 ou, encore, lui aurait causé un préjudice anormal et spécial dépassant le risque encouru dans le cadre de toute passation d'un contrat. <br/>
<br/>
              7. Il suit de là que la société anonyme HLM Immobilière Atlantic Aménagement n'est pas fondée à solliciter la condamnation de la commune de Saint-Georges-de-Didonne à lui verser une indemnité de 71 549,41 euros. <br/>
<br/>
              Sur les frais de l'instance :<br/>
<br/>
              8. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à la charge de la commune de Saint-Georges-de-Didonne, qui n'est pas la partie perdante. Il y a lieu, dans les circonstances de l'espèce, de mettre au même titre une somme de 3 000 euros à la charge de la société HLM Immobilière Atlantic Aménagement.<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : L'article 2 de l'arrêt du 2 décembre 2019 de la cour administrative d'appel de Bordeaux est annulé en tant qu'il a rejeté le surplus des conclusions de la requête de la société anonyme HLM Immobilière Atlantic Aménagement.<br/>
Article 2 : Le surplus des conclusions de la requête de la société anonyme HLM Immobilière Atlantic Aménagement est rejeté.<br/>
Article 3 : Les conclusions présentées par la société anonyme HLM Immobilière Atlantic Aménagement au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La société anonyme HLM Immobilière Atlantic Aménagement versera une somme de 3 000 euros à la commune de Saint-Georges-de-Didonne au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 5 : La présente décision sera notifiée à la société anonyme HLM Immobilière Atlantic Aménagement et à la commune de Saint-Georges-de-Didonne.<br/>
              Délibéré à l'issue de la séance du 7 janvier 2022 où siégeaient : Mme Christine Maugüé, présidente adjointe de la section du contentieux, présidant ; M. I... C..., M. Pierre Collin, présidents de chambre ; M. G... K..., M. H... E..., M. D... J..., M. B... L..., Mme Françoise Tomé, conseillers d'Etat ; M. Laurent-Xavier Simonel, conseiller d'Etat en service extraordinaire-rapporteur.<br/>
<br/>
              Rendu le 3 février 2022.<br/>
<br/>
<br/>
                 La Présidente : <br/>
                 Signé : Mme Christine Maugüé<br/>
 		Le rapporteur : <br/>
      Signé : M. Laurent-Xavier Simonel<br/>
                 La secrétaire :<br/>
                                  Signé : Mme A... F...<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
