<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037415497</ID>
<ANCIEN_ID>JG_L_2018_09_000000421688</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/41/54/CETATEXT000037415497.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème - 3ème chambres réunies, 19/09/2018, 421688, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-09-19</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>421688</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème - 3ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Laurent Domingo</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Romain Victor</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:421688.20180919</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par un mémoire distinct et un nouveau mémoire, enregistrés les 22 juin et 26 juillet 2018 au secrétariat du contentieux du Conseil d'Etat, la société Zimmer Biomet France Holdings SAS demande au Conseil d'Etat, en application de l'article 23-5 de l'ordonnance n° 58-1067 du 7 novembre 1958 et à l'appui de sa requête tendant à l'annulation pour excès de pouvoir des paragraphes n° 70 et 160 des commentaires administratifs publiés le 29 mars 2013 au Bulletin officiel des finances publiques sous la référence BOI-IS-BASE-35-30-10, de renvoyer au Conseil constitutionnel la question de la conformité aux droits et libertés garantis par la Constitution des dispositions du IX de l'article 209 du code général des impôts, dans sa rédaction résultant de la loi n° 2011-1978 du 28 décembre 2011. <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la Constitution, notamment son Préambule et son article 61-1 ;<br/>
              - l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
              - le code général des impôts ;<br/>
              - la loi n° 2011-1978 du 28 décembre 2011 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Laurent Domingo, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Romain Victor, rapporteur public.<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Aux termes du premier alinéa de l'article 23-5 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel : " Le moyen tiré de ce qu'une disposition législative porte atteinte aux droits et libertés garantis par la Constitution peut être soulevé (...) à l'occasion d'une instance devant le Conseil d'Etat (...) ". Il résulte des dispositions de ce même article que le Conseil constitutionnel est saisi de la question prioritaire de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux.<br/>
<br/>
              2. Aux termes du IX de l'article 209 du code général des impôts, dans sa rédaction  résultant de l'article 40 de la loi du 28 décembre 2011 de finances rectificative pour 2011 : " 1. Les charges financières afférentes à l'acquisition des titres de participation mentionnés au troisième alinéa du a quinquies du I de l'article 219 sont rapportées au bénéfice de l'exercice lorsque l'entreprise n'est pas en mesure de démontrer par tous moyens, au titre de l'exercice ou des exercices couvrant une période de douze mois à compter de la date d'acquisition des titres ou, pour les titres acquis au cours d'un exercice ouvert avant le 1er janvier 2012, du premier exercice ouvert après cette date, que les décisions relatives à ces titres sont effectivement prises par elle ou par une société établie en France la contrôlant au sens du I de l'article L. 233-3 du code de commerce ou par une société établie en France directement contrôlée par cette dernière au sens du même article L. 233-3 et, lorsque le contrôle ou une influence est exercé sur la société dont les titres sont détenus, que ce contrôle ou cette influence est effectivement exercé par la société détenant les titres ou par une société établie en France la contrôlant au sens du I dudit article L. 233-3 ou par une société établie en France directement contrôlée par cette dernière au sens de ce même article ".<br/>
<br/>
              3. Les dispositions du 1 du IX de l'article 209 du code général des impôts sont applicables au litige par lequel la société Zimmer Biomet France Holdings SAS demande l'annulation des paragraphes n° 70 et 160 des commentaires administratifs publiés le 29 mars 2013 au Bulletin officiel des finances publiques sous la référence BOI-IS-BASE-35-30-10 et n'ont pas déjà été déclarées conformes à la Constitution par le Conseil constitutionnel. Ces dispositions subordonnent la possibilité de ne pas être soumis à la réintégration qu'elles prévoient des charges financières afférentes à l'acquisition de titres de participation à la démonstration de ce que les décisions relatives à ces titres sont prises par une société établie en France qui est soit la société détentrice des titres, soit une société la contrôlant, soit une société contrôlée par cette dernière et de ce que le contrôle ou l'influence exercé sur la société dont les titres sont détenus l'est effectivement par les sociétés mentionnées ci-dessus, sans ouvrir une telle possibilité lorsqu'il est démontré qu'il en va de même pour une société établie en France autre que celles mentionnées ci-dessus. Le moyen tiré de ce que ces dispositions portent atteinte aux principes d'égalité devant la loi et d'égalité devant les charges publiques garantis par les articles 6 et 13 de la Déclaration des droits de l'homme et du citoyen soulève une question présentant un caractère sérieux. Ainsi, il y a lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité invoquée.<br/>
<br/>
<br/>
<br/>
<br/>                   D E C I D E :<br/>
                                --------------<br/>
<br/>
Article 1er : La question de la conformité à la Constitution du 1 du IX de l'article 209 du code général des impôts, dans sa rédaction résultant de l'article 40 de la loi du 28 décembre 2011 de finances rectificative pour 2011, est renvoyée au Conseil constitutionnel.<br/>
<br/>
Article 2 : Il est sursis à statuer sur la requête de la société Zimmer Biomet France Holdings SAS jusqu'à ce que le Conseil constitutionnel ait tranché la question de constitutionnalité ainsi soulevée.<br/>
<br/>
Article  3 : La présente décision sera notifiée la société Zimmer Biomet France Holdings SAS et au ministre de l'action et des comptes publics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
