<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042417953</ID>
<ANCIEN_ID>JG_L_2020_10_000000421312</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/41/79/CETATEXT000042417953.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème - 1ère chambres réunies, 09/10/2020, 421312</TITRE>
<DATE_DEC>2020-10-09</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>421312</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème - 1ère chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BUK LAMENT - ROBILLOT ; SCP CELICE, TEXIDOR, PERIER ; SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR>Mme Marie Grosset</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Raphaël Chambon</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:421312.20201009</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              La société Les Nemours, la société Victoria et la société JFR SAS ont demandé à la cour administrative d'appel de Lyon d'annuler pour excès de pouvoir la décision du 26 février 2016 par laquelle la commission nationale d'aménagement cinématographique (CNACi) a autorisé la société Agora à créer un établissement de spectacles cinématographiques de neuf salles et 1 380 places à l'enseigne " Megarama " à Seynod. Par un arrêt n° 16LY02184 du 12 avril 2018, la cour administrative d'appel de Lyon a rejeté cette demande.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique enregistrés les 8 juin et 10 septembre 2018 et le 6 février 2020 au secrétariat du contentieux du Conseil d'État, la société Les Nemours, la société Victoria et la société JFR SAS demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à leur requête ;<br/>
<br/>
              3°) mettre à la charge de la société Agora la somme de 5 000 euros au titre de l'article L.761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code du cinéma et de l'image animée ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Marie Grosset, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Raphaël Chambon, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Buk Lament, Robillot, avocat de la société Les Nemours, de la société Victoria et de la société JFR SAS, à la SCP Célice, Texidor, Perier, avocat de la société Agora et à la SCP Piwnica, Molinié, avocat du Centre national du cinéma et de l'image animée ;<br/>
<br/>
<br/>
              Vu la note en délibéré, enregistrée le 23 septembre 2020, présentée par les sociétés Les Nemours, Victoria et JFR SAS ; <br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que par une décision du 26 février 2016, la commission nationale d'aménagement cinématographique (CNACi) a délivré à la société Agora l'autorisation de créer un établissement de spectacles cinématographiques de neuf salles et 1 380 places à l'enseigne " Megarama " à Seynod (Haute-Savoie), commune fusionnée depuis avec la commune d'Annecy. La société Les Nemours, la société Victoria et la société JFR SAS se pourvoient en cassation contre l'arrêt de la cour administrative d'appel de Lyon du 12 avril 2018 ayant rejeté la requête par laquelle elles demandaient l'annulation pour excès de pouvoir de cette décision.<br/>
<br/>
              Sur la régularité de l'arrêt attaqué :<br/>
<br/>
              2. Aux termes du premier alinéa de l'article R. 613-2 du code de justice administrative : " Si le président de la formation de jugement n'a pas pris une ordonnance de clôture, l'instruction est close trois jours francs avant la date de l'audience indiquée dans l'avis d'audience prévu à l'article R. 711-2. Cet avis le mentionne ". Il ressort des pièces du dossier de la cour administrative d'appel que, contrairement à ce que soutiennent les sociétés requérantes, les courriers du 19 février 2016 informant les membres de la CNACi du report de la séance de la commission, initialement prévue le 18 février 2016, au 26 février 2016, ont été joints au premier mémoire en défense de la CNACi et enregistrés, comme ce mémoire, au greffe de la cour administrative d'appel de Lyon le 19 décembre 2016, soit avant la clôture de l'instruction, intervenue trois jours francs avant le 13 mars 2018, date de l'audience. Partant, les moyens tirés de ce que l'arrêt attaqué aurait été rendu au terme d'une procédure irrégulière, faute pour la cour d'avoir rouvert l'instruction de façon à soumettre les pièces en cause au débat contradictoire, ne peuvent qu'être écartés. <br/>
<br/>
              Sur le bien-fondé de l'arrêt attaqué : <br/>
<br/>
              En ce qui concerne la régularité de la convocation des participants à la réunion du 26 février 2016 :<br/>
<br/>
              3. Aux termes de l'article L. 212-6-5 du code du cinéma et de l'image animée : " La Commission nationale d'aménagement cinématographique comprend neuf membres nommés, pour une durée de six ans non renouvelable, par décret ". Aux termes du quatrième alinéa de l'article R. 212-6-9 du même code : " Pour chacun des membres hormis le président, un suppléant est nommé dans les mêmes conditions que celles de désignation du membre titulaire ". Aux termes de l'article R. 212-7-26 du même code : " La Commission nationale d'aménagement cinématographique se réunit sur convocation de son président. / Les membres de la commission reçoivent l'ordre du jour, accompagné des procès-verbaux des réunions des commissions départementales d'aménagement cinématographique, des décisions de ces commissions, des recours et des rapports des services instructeurs. / La commission ne peut valablement délibérer qu'en présence de cinq membres au moins. "<br/>
<br/>
              4. Il ressort des pièces du dossier soumis aux juges du fond que, par courrier du 12 février 2016, le secrétariat de la CNACi a convoqué les membres titulaires de cette commission à la réunion devant se tenir le 18 février 2016, séance de la commission au cours de laquelle le dossier de la société Agora devait être examiné. Ce courrier mentionne la transmission, en pièces jointes, de l'ordre du jour et du dossier d'instruction. Après qu'il a été décidé de reporter cette réunion, le secrétariat de la CNACi a adressé à cinq des membres titulaires précédemment convoqués ainsi qu'à deux membres suppléants, remplaçant deux membres titulaires précédemment convoqués et ayant fait connaître leur indisponibilité pour siéger à la réunion ainsi reportée, un courrier du 19 février 2016 les informant de ce que la réunion de la commission, initialement prévue le 18 février 2016, se tiendrait le 26 février 2016, bien que ce courrier ne comportât ni l'ordre du jour ni les mentions relatives au dossier d'instruction. C'est sans commettre d'erreur de droit ni méconnaître son office que, par une appréciation des faits exempte de dénaturation, la cour administrative d'appel a relevé que les membres suppléants de la CNACi, substituant les membres précédemment destinataires de la première convocation accompagnée des documents afférents, avaient été mis à même, en temps utile, de prendre connaissance des documents dont l'envoi aux membres est prévu par les dispositions citées au point 3.<br/>
<br/>
              5. Par ailleurs, le moyen tiré de ce que deux membres titulaires de la CNACi n'auraient pas été régulièrement convoqués est nouveau en cassation. Il s'ensuit que les sociétés requérantes ne peuvent utilement soutenir que la cour aurait, à cet égard, dénaturé les pièces du dossier qui lui était soumis.<br/>
<br/>
              En ce qui concerne la régularité des conditions de recueil de l'avis du ministre chargé de la culture :<br/>
<br/>
              6. Aux termes de l'article R. 212-7-29 du code du cinéma et de l'image animée : " Le commissaire du Gouvernement recueille l'avis du ministre chargé de la culture, qu'il présente à la Commission nationale d'aménagement cinématographique. Il donne son avis sur les demandes examinées par la commission au regard des auditions effectuées ". Si les sociétés requérantes font valoir qu'il ne ressort pas de la décision attaquée que le commissaire du gouvernement aurait recueilli l'avis du ministre chargé de la culture et l'aurait présenté à la CNACi, il ressort des pièces du dossier soumis aux juges du fond que l'avis du ministre sur le projet de la société Agora, émis le 25 février 2016, a été adressé au commissaire du gouvernement de la CNACi et que celui-ci a été entendu par la commission ainsi qu'il ressort des visas de sa décision du 26 février 2016. En écartant, en l'état des appréciations qu'elle a souverainement portées sur les pièces versées au dossier, les contestations portant sur les conditions dans lesquelles l'avis du ministre de la culture a été présenté à la commission, la cour administrative d'appel a suffisamment motivé sa décision et n'a pas commis d'erreur de droit. <br/>
<br/>
              En ce qui concerne l'appréciation de la commission nationale d'aménagement cinématographique :<br/>
<br/>
              7. Aux termes de l'article L. 212-6 du code du cinéma et de l'image animée : " Les créations, extensions et réouvertures au public d'établissements de spectacles cinématographiques doivent répondre aux exigences de diversité de l'offre cinématographique, d'aménagement culturel du territoire, de protection de l'environnement et de qualité de l'urbanisme, en tenant compte de la nature spécifique des oeuvres cinématographiques. Elles doivent contribuer à la modernisation des établissements de spectacles cinématographiques et à la satisfaction des intérêts du spectateur tant en ce qui concerne la programmation d'une offre diversifiée, le maintien et la protection du pluralisme dans le secteur de l'exploitation cinématographique que la qualité des services offerts ". Aux termes de l'article L. 212-9 du même code : " Dans le cadre des principes définis à l'article L. 212-6, la commission départementale d'aménagement cinématographique se prononce sur les deux critères suivants : / 1° L'effet potentiel sur la diversité cinématographique offerte aux spectateurs dans la zone d'influence cinématographique concernée, évalué au moyen des indicateurs suivants : / a) Le projet de programmation envisagé pour l'établissement de spectacles cinématographiques objet de la demande d'autorisation et, le cas échéant, le respect des engagements de programmation éventuellement souscrits en application des articles L. 212-19 et L. 212-20 ; / b) La nature et la diversité culturelle de l'offre cinématographique proposée dans la zone concernée, compte tenu de la fréquentation cinématographique ; / c) La situation de l'accès des oeuvres cinématographiques aux salles et des salles aux oeuvres cinématographiques pour les établissements de spectacles cinématographiques existants ; / 2° L'effet du projet sur l'aménagement culturel du territoire, la protection de l'environnement et la qualité de l'urbanisme, évalué au moyen des indicateurs suivants : / a) L'implantation géographique des établissements de spectacles cinématographiques dans la zone d'influence cinématographique et la qualité de leurs équipements ; / b) La préservation d'une animation culturelle et le respect de l'équilibre des agglomérations ; /c) La qualité environnementale appréciée en tenant compte des différents modes de transports publics, de la qualité de la desserte routière, des parcs de stationnement ; / d) L'insertion du projet dans son environnement ; /e) La localisation du projet, notamment au regard des schémas de cohérence territoriale et des plans locaux d'urbanisme./ Lorsqu'une autorisation s'appuie notamment sur le projet de programmation cinématographique, ce projet fait l'objet d'un engagement de programmation cinématographique souscrit en application du 3° de l'article L. 212-23. /(...) ". Il résulte de ces dispositions que l'autorisation d'aménagement cinématographique ne peut être refusée que si, eu égard à ses effets, le projet d'équipement cinématographique contesté compromet la réalisation des objectifs et principes énoncés par la loi. A ce titre, il appartient à la CNACi, lorsqu'elle se prononce sur les dossiers de demande d'autorisation, d'apprécier la conformité du projet à ces objectifs et principes, au vu des critères d'évaluation et indicateurs mentionnés à l'article L. 212-9 du code du cinéma et de l'image animée.<br/>
<br/>
              8. Pour rejeter le recours dirigé contre la décision de la CNACi autorisant le projet de la société Agora, la cour administrative d'appel a jugé que, compte tenu de la faiblesse d'équipements et de fréquentation cinématographiques sur le territoire de la commune de Seynod qui connaît une forte croissance démographique, de la répartition géographique des salles sur la zone d'influence cinématographique concernée, des engagements de programmation spécifique et conventions de partenariats souscrits par la société Agora, de l'amélioration prévisible des conditions d'exposition des films généralistes grâce à l'ouverture de ces salles, le projet ne pouvait être regardé comme compromettant la réalisation de l'objectif de diversité cinématographique offerte aux spectateurs de la zone, alors même que l'équipement et la fréquentation cinématographiques de cette zone sont supérieurs à la moyenne de référence et que des difficultés d'accès aux films rencontrées par les établissements mono-écran de la zone existent. Par suite, le moyen tiré de ce que la cour n'a pas répondu au moyen tiré de l'insuffisance du projet proposé à garantir la préservation de la diversité cinématographique poursuivie par la loi ne peut qu'être écarté. <br/>
<br/>
              9. Si les sociétés requérantes font grief à l'arrêt attaqué d'avoir affirmé que la CNACi a pris en compte le rapport d'instruction en ce qu'il mentionne l'autorisation accordée au cinéma de Rumilly d'ouvrir deux salles supplémentaires alors qu'il n'a été ni visé ni expressément mentionné dans sa décision, il ressort des pièces du dossier soumis aux juges du fond que c'est par une appréciation souveraine exempte de dénaturation que la cour a estimé que la CNACi, en relevant que le projet permettait d'améliorer les conditions d'exposition des films de la zone d'influence cinématographique et de favoriser un rééquilibrage géographique de la répartition de l'offre cinématographique au sein de la zone d'influence qui comprenait plusieurs établissement dont celui de Rumilly, a pris en compte cet élément.<br/>
<br/>
              10. Contrairement à ce que soutiennent les requérantes, la cour, qui a bien pris en compte la présence d'Annecy dans la zone primaire, ainsi qu'en témoigne la population de référence retenue, et le fait que la fréquentation et l'équipement de la zone d'influence cinématographique sont supérieurs à la moyenne de référence, a retenu, par une appréciation souveraine exempte de dénaturation, que la croissance démographique de Seynod et la faiblesse de l'offre cinématographique sur sa zone devaient être prises en compte. De même, au regard de ces éléments, la cour a souverainement jugé, sans dénaturer les pièces du dossier, que l'ouverture du " Mégarama " serait, dans un contexte où il existe une carence concernant la diffusion des films en sortie nationale, de nature à améliorer l'offre de films généralistes et la diversité cinématographique.<br/>
<br/>
              11. Pour apprécier la compatibilité du projet avec l'exigence de diversité cinématographique fixée par la loi, la cour a pris en compte les engagements souscrits par la société Agora, d'une part, dans le cadre d'un engagement de programmation spécifique de ne pas programmer de films d'art et d'essai pendant trente-six mois à compter de l'ouverture au public et, d'autre part, dans le cadre de la convention de partenariat signée avec l'auditorium de Seynod. Les dispositions mentionnées au a) du 1° de l'article L. 212-9 du code du cinéma et de l'image animé mentionnées au point 7 prévoient seulement que soit mesuré l'effet potentiel sur la diversité cinématographique des projets de programmation envisagés et le cas échéant des engagements homologués par le centre national du cinéma et de l'image animée (CNC). C'est par une appréciation souveraine exempte de dénaturation que la cour, qui a répondu au moyen soulevé devant elle, a estimé que ces engagements, dont certains seront limités dans le temps et néanmoins mis en oeuvre sous le contrôle du CNC et du médiateur du cinéma, sont suffisamment fermes pour qu'elle en tienne compte, et qu'elle en a déduit, sans commettre d'erreur de droit, que la durée de l'engagement de programmation spécifique, bien que limitée à trente-six mois ne pouvait être regardé comme compromettant la réalisation de l'objectif de diversité cinématographique offerte aux spectateurs dans cette zone .<br/>
<br/>
              12. La cour a également souverainement apprécié que, compte tenu du caractère généraliste de la programmation du nouveau cinéma, l'impact du projet pour les cinémas classés "art et essai", et notamment le cinéma "Les Nemours" situé à Annecy, serait limité. C'est ainsi sans entacher sa décision d'erreur de droit et par une appréciation souveraine des faits exempte de dénaturation que la cour, qui a procédé à une mise en balance des avantages et inconvénients du projet de la société Agora, a jugé que, malgré les difficultés que ce projet était susceptible de causer pour les établissements " mono-écran " de la zone d'influence cinématographique, il ne pouvait être regardé comme méconnaissant les critères mentionnés au c) du 1° de l'article L. 212-9 du code du cinéma et de l'image animée cité au point 7.<br/>
<br/>
              13. En ce qui concerne l'appréciation portée par la CNACi quant à l'effet potentiel du projet sur la protection de l'environnement et la qualité de l'urbanisme, d'une part, la cour a relevé que le projet de la société Agora, situé au sud-ouest d'Annecy, favorisera une répartition de l'offre cinématographique de la zone d'influence cinématographique en créant un pôle complémentaire à ceux d'Annecy et Aix-les-Bains et qu'il aura un impact limité pour les cinémas éloignés et pour les cinémas d'art et d'essai. Par suite, les sociétés requérantes ne sont pas fondées à soutenir que la cour, qui a ainsi recherché quel serait l'impact du projet sur l'animation culturelle de la zone et si celui-ci allait respecter l'équilibre des agglomérations mentionné au b) du 2° de l'article L. 212-9 du code du cinéma et de l'image animée, aurait entaché son arrêt d'insuffisance de motivation et d'erreur de droit. <br/>
<br/>
              14. Enfin, la cour qui a relevé que le projet, compatible avec le schéma de cohérence territoriale, s'insérera de manière satisfaisante dans le paysage environnant et comportera des dessertes suffisantes, a souverainement jugé, sans dénaturer les pièces du dossier, que le projet ne porterait pas atteinte à la qualité environnementale compte tenu des différents modes de transports publics et de la qualité de la desserte routière et des parcs de stationnement, au regard du c) du 2° de l'article L. 212-9 du code du cinéma et de l'image animée. <br/>
<br/>
              15. Il résulte de tout ce qui précède que le pourvoi des sociétés Les Nemours, Victoria et JFR SAS, y compris les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative, ne peut qu'être rejeté. <br/>
<br/>
              16. Il y a lieu, dans les circonstances de l'espèce, de mettre conjointement à la charge de la société Les Nemours, de la société Victoria et de la société JFR SAS, au titre de l'article L. 761-1 du code de justice administrative, la somme de 1 500 euros à verser à la société Agora et la somme de 1 500 euros à verser au Centre national du cinéma et de l'image animée, agissant en qualité d'établissement public support de la Commission nationale d'aménagement cinématographique et qui en assure le secrétariat en vertu de l'article R. 212-6-12 du code du cinéma et de l'image animée. <br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi des sociétés Les Nemours, Victoria et JFR SAS est rejeté.<br/>
Article 2 : Les sociétés Les Nemours, Victoria et JFR SAS verseront ensemble la somme de 1 500 euros à la société Agora et la somme de 1 500 euros au Centre national du cinéma et de l'image animée, agissant en qualité d'établissement public support de la Commission nationale d'aménagement cinématographique, au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée à la société Les Nemours, première requérante dénommée, à la société Agora, à la commission nationale d'aménagement cinématographique et au Centre national du cinéma et de l'image animée.<br/>
Copie en sera adressée à la ministre de la culture.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">09-05 ARTS ET LETTRES. CINÉMA. - AMÉNAGEMENT CINÉMATOGRAPHIQUE - CONTRÔLE DU JUGE DE CASSATION - APPRÉCIATION DES EFFETS D'UN PROJET AU REGARD DES OBJECTIFS POSÉS PAR LA RÈGLEMENTATION (ART. L. 212-9 DU CODE DU CINÉMA ET DE L'IMAGE ANIMÉE) - APPRÉCIATION SOUVERAINE SOUS RÉSERVE DE DÉNATURATION [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">14-02-01-07 COMMERCE, INDUSTRIE, INTERVENTION ÉCONOMIQUE DE LA PUISSANCE PUBLIQUE. RÉGLEMENTATION DES ACTIVITÉS ÉCONOMIQUES. ACTIVITÉS SOUMISES À RÉGLEMENTATION. DIVERSES ACTIVITÉS. - AMÉNAGEMENT CINÉMATOGRAPHIQUE - CONTRÔLE DU JUGE DE CASSATION - APPRÉCIATION DES EFFETS D'UN PROJET AU REGARD DES OBJECTIFS POSÉS PAR LA RÈGLEMENTATION (ART. L. 212-9 DU CODE DU CINÉMA ET DE L'IMAGE ANIMÉE) - APPRÉCIATION SOUVERAINE SOUS RÉSERVE DE DÉNATURATION [RJ1].
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">54-08-02-02-01-04 PROCÉDURE. VOIES DE RECOURS. CASSATION. CONTRÔLE DU JUGE DE CASSATION. BIEN-FONDÉ. DÉNATURATION. - APPRÉCIATION DES EFFETS D'UN PROJET AU REGARD DES OBJECTIFS POSÉS PAR LA RÈGLEMENTATION DE L'AMÉNAGEMENT CINÉMATOGRAPHIQUE (ART. L. 212-9 DU CODE DU CINÉMA ET DE L'IMAGE ANIMÉE) [RJ1].
</SCT>
<ANA ID="9A"> 09-05 Le juge de cassation exerce un contrôle limité à la dénaturation sur l'appréciation par les juges du fond des effets d'un projet au regard des objectifs et principes énoncés par la réglementation en matière d'aménagement cinématographique.</ANA>
<ANA ID="9B"> 14-02-01-07 Le juge de cassation exerce un contrôle limité à la dénaturation sur l'appréciation par les juges du fond des effets d'un projet au regard des objectifs et principes énoncés par la réglementation en matière d'aménagement cinématographique.</ANA>
<ANA ID="9C"> 54-08-02-02-01-04 Le juge de cassation exerce un contrôle limité à la dénaturation sur l'appréciation par les juges du fond des effets d'un projet au regard des objectifs et principes énoncés par la réglementation en matière d'aménagement cinématographique.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., s'agissant de l'appréciation des effets d'un projet au regard des objectifs de la règlementation en matière d'aménagement commercial, CE, 6 avril 2016, Société commerciale de Taiarapu Est, n° 367564, T. pp. 663-913.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
