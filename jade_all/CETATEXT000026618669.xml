<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026618669</ID>
<ANCIEN_ID>JG_L_2012_11_000000336118</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/61/86/CETATEXT000026618669.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère et 6ème sous-sections réunies, 12/11/2012, 336118, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2012-11-12</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>336118</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère et 6ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Bernard Stirn</PRESIDENT>
<AVOCATS>SCP POTIER DE LA VARDE, BUK LAMENT</AVOCATS>
<RAPPORTEUR>Mme Dominique Versini-Monod</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Alexandre Lallet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2012:336118.20121112</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête sommaire et le pourvoi complémentaire, enregistrés les 1er février et 3 mai 2010 au secrétariat du contentieux du Conseil d'Etat, présentés pour la société civile de moyens (SCM) Scanner de l'Ouest lyonnais, dont le siège est avenue David Ben Gourion à Lyon (69009), M. Yves N, demeurant ..., M. Pierre P, demeurant ..., M. Richard O, demeurant ..., M. François Q, demeurant ..., M. Pierre R, demeurant ..., M. Didier T, demeurant ..., M. Jean S, demeurant ..., M. Guy U, demeurant ..., M. Gaston V, demeurant ..., M. Bruno W, demeurant ..., M. Gérard Y, demeurant ..., M. Thierry X, demeurant ..., M. Jean Gérald Z, demeurant ... ; la SCM Scanner de l'Ouest lyonnais et autres demandent au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler l'arrêt n° 05LY00117 du 19 novembre 2009 par lequel la cour administrative d'appel de Lyon a rejeté leur requête dirigée contre le jugement du 23 novembre 2004 par lequel le tribunal administratif de Lyon a rejeté leur demande tendant à la condamnation de l'Etat au versement d'une somme de 811 337, 36 euros en réparation des conséquences dommageables résultant des fautes commises par l'administration, entre 1991 et 1996, dans l'exercice de son pouvoir réglementaire de fixation des tarifs de prise en charge, par les régimes obligatoires d'assurance maladie, des actes de scanographie ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à leur requête d'appel ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat le versement de la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
	Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Dominique Versini-Monod, Conseiller d'Etat,  <br/>
<br/>
              - les observations de la SCP Potier de la Varde, Buk Lament, avocat de la SCM Scanner de l'Ouest lyonnais et autres,<br/>
<br/>
              - les conclusions de M. Alexandre Lallet, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Potier de la Varde, Buk Lament, avocat de la SCM Scanner de l'Ouest lyonnais et autres ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond qu'à la suite de l'abrogation, par un arrêté du 11 juillet 1991, de la cotation des actes de scanographie en vigueur depuis 1978, le tarif de ces actes a été fixé par une lettre interministérielle du même jour pour les années 1991 et 1992, puis, pour les années suivantes, par des arrêtés des  1er février 1993, 14 février 1994, 22 février 1995 et 9 avril 1996 ; que l'arrêté et la lettre circulaire du 11 juillet 1991 ayant été annulés par le Conseil d'Etat, statuant au contentieux et les arrêtés ultérieurs ayant été déclarés illégaux par d'autres décisions du Conseil d'Etat, la SCM Scanner de l'Ouest lyonnais et treize autres cabinets de scanographie ont demandé la réparation du préjudice qu'ils estiment avoir subi du fait de l'application de cette réglementation illégale, qui leur était moins favorable que la cotation en vigueur entre 1978 et 1991 ; qu'ils se pourvoient en cassation contre l'arrêt du 19 novembre 2009 par lequel, en appel d'un jugement du tribunal administratif de Lyon, la cour administrative d'appel de Lyon a rejeté leur requête ; <br/>
<br/>
              2. Considérant que, contrairement à ce que soutient le ministre de la santé et des sports, le pourvoi en cassation de la SCM Scanner de l'Ouest lyonnais et des autres requérants, introduit moins de deux mois après les notifications de l'arrêt attaqué effectuées aux différents requérants entre les 2 et 4 décembre 2011, n'est pas tardif ; <br/>
<br/>
              3. Considérant, d'une part, que, pour écarter l'existence d'un lien direct de causalité entre l'illégalité de l'arrêté du 11 juillet 1991 et le préjudice invoqué par les requérants, la cour s'est bornée à relever que cette illégalité consistait en une " irrégularité de pure forme " ; qu'en statuant ainsi, alors que toute illégalité est constitutive d'une faute et, par suite, quelle que soit sa nature, susceptible d'engager la responsabilité de son auteur dès lors qu'elle est à l'origine du préjudice subi, la cour a entaché son arrêt d'une erreur de droit ; <br/>
<br/>
              4. Considérant, d'autre part, que, pour écarter l'existence d'un lien direct de causalité entre les illégalités entachant les décisions qui ont, entre 1991 et 1996, fixé les tarifs des actes de scanographie, et le préjudice invoqué par les requérants, la cour s'est fondée sur le fait que ces derniers n'avaient " pas de droit au maintien " de la tarification en vigueur entre 1978 et 1991 ; que toutefois, si nul n'a de droit acquis au maintien d'une réglementation, l'éventuelle illégalité dont est entachée une réglementation nouvelle est de nature à ouvrir droit à indemnité lorsque cette illégalité est directement à l'origine d'un préjudice ; que la cour a, par suite, entaché sur ce second point son arrêt d'une autre erreur de droit ; <br/>
<br/>
              5. Considérant qu'il résulte de ce qui précède que, sans qu'il soit besoin d'examiner les autres moyens de leur pourvoi, la SCM Scanner de l'Ouest lyonnais et les autres requérants sont fondés à demander l'annulation de l'arrêt du 19 novembre 2009 ;<br/>
<br/>
              6. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative ; <br/>
<br/>
              7. Considérant, en premier lieu, que l'abrogation, par l'arrêté du 11 juillet 1991, de la tarification des actes de scanographie qui était antérieurement applicable, n'a pas eu par elle-même pour effet de fixer les cotations litigieuses et n'en a en rien préjugé le montant, qui a été fixé par la seule lettre interministérielle du même jour ; que le préjudice dont les requérants recherchent réparation est, dès lors, dépourvu de lien de causalité avec l'illégalité dont cet arrêté était entaché ; <br/>
<br/>
              8. Considérant, en second lieu, que tant l'annulation de la lettre interministérielle du 11 juillet 1991 que la déclaration d'illégalité des arrêtés des 1er février 1993, 14 février 1994, 22 février 1995 et 9 avril 1996 résultant exclusivement de ce que les tarifs fixés par ces différentes décisions l'étaient à titre seulement provisoire, alors que les actes de scanographie concernés, qui étaient couramment pratiqués depuis plusieurs années, auraient dû légalement se voir fixer des cotations dépourvues de toute limite temporelle prédéfinie ; que le vice dont étaient entachés ces actes tarifaires n'a, dès lors, pas eu d'incidence directe sur les montants facturés, au cours de cette période, par les cabinets de scanographie ; que, dans ces conditions, la SCM Scanner de l'Ouest lyonnais et les autres requérants ne sont pas fondés à soutenir qu'il existe un lien direct de causalité entre l'illégalité qui affecte les décisions tarifaires et le préjudice qu'ils invoquent ; <br/>
<br/>
              9. Considérant qu'il résulte de tout ce qui précède que la SCM Scanner de l'Ouest lyonnais et les autres requérants ne sont pas fondés à se plaindre de ce que, par son jugement du 23 novembre 2004, le tribunal administratif de Lyon a rejeté leur demande tendant à ce que l'Etat soit condamné à réparer les conséquences dommageables des fautes imputables à l'administration dans l'exercice de son pouvoir réglementaire entre 1991 et 1996 pour la fixation des tarifs des actes de scanographie ; que, par voie de conséquence, doivent être également rejetées leurs conclusions tendant au bénéfice des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 19 novembre 2009 de la cour administrative d'appel de Lyon est annulé.<br/>
Article 2 : La requête introduite par la SCM Scanner de l'Ouest lyonnais et les autres requérants devant la cour administrative d'appel de Lyon est rejetée.<br/>
Article 3 : Le surplus des conclusions de cassation de la SCM Scanner de l'Ouest lyonnais et des autres requérants tendant à l'application de l'article L. 761-1 du code de justice administrative est rejeté.<br/>
Article 4 : La présente décision sera notifiée à la SCM Scanner de l'Ouest lyonnais, premier requérant dénommé et à la ministre des affaires sociales et de la santé.<br/>
Les autres requérants seront informés de la présente décision par la SCP Potier de la Varde, Buk Lament, avocat au Conseil d'Etat et à la Cour de cassation, qui les représente devant le Conseil d'Etat.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
