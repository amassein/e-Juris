<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039627816</ID>
<ANCIEN_ID>JG_L_2019_12_000000435802</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/62/78/CETATEXT000039627816.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 05/12/2019, 435802, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-12-05</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>435802</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2019:435802.20191205</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante : <br/>
              M. C... A... a demandé au juge des référés du tribunal administratif de Nantes, statuant sur le fondement de l'article L. 521-2 du code de justice administratif, d'une part, d'enjoindre au préfet de la Mayenne d'enregistrer sa demande d'asile en procédure normale dans les cinq jours de l'ordonnance à intervenir sous astreinte de 100 euros par jour de retard et, d'autre part, d'enjoindre au directeur de l'Office français de l'immigration et de l'intégration de le rétablir dans les conditions matérielles d'accueil et de lui verser de façon rétroactive les droits non perçus dans les cinq jours de l'ordonnance à intervenir sous astreinte de 100 euros par jour de retard. Par une ordonnance n° 1911203 du 18 octobre 2019, le juge des référés du tribunal administratif de Nantes a rejeté sa demande. <br/>
<br/>
              Par une requête, enregistrée le 6 novembre 2019 au secrétariat du contentieux du Conseil d'Etat, M. A... demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative :<br/>
<br/>
              1°) de l'admettre au bénéfice de l'aide juridictionnelle provisoire ;<br/>
<br/>
              2°) d'annuler cette ordonnance ; <br/>
<br/>
              3°) de suspendre les effets de la décision par laquelle le préfet de la Mayenne a prolongé le délai de transfert de M. A... auprès des autorités espagnoles ;<br/>
<br/>
              4°) d'enjoindre au préfet de la Mayenne d'enregistrer sa demande d'asile en procédure normale dans les cinq jours de l'ordonnance à intervenir sous astreinte de 100 euros par jour de retard ; <br/>
<br/>
              5°) de mettre à la charge de l'Etat le versement de la somme de 3 000 euros au bénéfice de Me B..., son conseil, sous réserve qu'elle renonce à percevoir la somme correspondant à la part contributive de l'Etat, au titre des dispositions de l'article 37 de la loi du 10 juillet 1991 et de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
              Il soutient que : <br/>
              - la condition d'urgence est remplie dès lors que le refus du préfet de la Mayenne de l'admettre au séjour au titre de l'asile porte atteinte à son droit à l'asile ;<br/>
              - les décisions attaquées portent une atteinte grave et manifestement illégale à son droit à l'asile ;<br/>
              - en particulier, le refus du préfet d'enregistrer sa demande d'asile en procédure normale et l'arrêté de prolongation de délai de transfert sont illégaux dès lors que la situation de fuite n'est pas caractérisée ;<br/>
              - l'exécution de son transfert vers l'Espagne compromettrait gravement sa santé ; <br/>
              - l'ordonnance attaquée est insuffisamment motivée. <br/>
<br/>
              Par un mémoire en défense, enregistré le 20 novembre 2019, le ministre de l'intérieur conclut au non-lieu à statuer dès lors que M. A... a été convoqué par la préfecture de la Loire-Atlantique à se présenter le 28 novembre en vue de requalifier sa demande d'asile en procédure normale et que l'Office français de l'immigration et de l'intégration sera en mesure de rétablir le bénéfice des conditions matérielles d'accueil au requérant sur la base de cette requalification.<br/>
<br/>
			Par un mémoire en défense, enregistré le 21 novembre 2019, l'Office français de l'immigration et de l'intégration conclut au non-lieu à statuer dès lors que le bénéfice des conditions matérielles d'accueil sera rétabli en faveur de M. A... dès que ce dernier sera en possession d'une attestation de demande d'asile. <br/>
<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, M. A... et, d'autre part, le ministre de l'intérieur et l'Office français de l'immigration et de l'intégration ;<br/>
              Vu le procès-verbal de l'audience publique du vendredi 22 novembre 2019 à 14 heures au cours de laquelle ont été entendus : <br/>
<br/>
              - Me Molinié, avocat au Conseil d'Etat et à la Cour de cassation, avocat de M. A... ;<br/>
              - la représentante du ministre de l'intérieur ;<br/>
<br/>
              - la représentante de l'Office français de l'immigration et de l'intégration ; <br/>
<br/>
              et à l'issue de laquelle le juge des référés a prolongé l'instruction jusqu'au 3 décembre à 18 heures ;<br/>
<br/>
              Vu le nouveau mémoire, enregistré le 29 novembre 2019, par lequel le ministre de l'intérieur maintient ses conclusions à fin de non-lieu à statuer ;<br/>
<br/>
              Vu le nouveau mémoire, enregistré le 2 décembre 2019, par lequel M. A... maintient ses conclusions et ses moyens ;<br/>
<br/>
              Vu le nouveau mémoire, enregistré le 3 décembre 2019, par lequel l'Office français de l'immigration et de l'intégration maintient ses conclusions à fin de non-lieu à statuer ; <br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - le règlement (UE) n° 604/2013 du Parlement européen et du Conseil du 26 juin 2013 ;<br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale ". <br/>
              2. M. A..., ressortissant guinéen, a demandé l'asile en France et a été placé en procédure " Dublin " le 14 novembre 2018 par le préfet de la Loire-Atlantique. Les autorités espagnoles, saisies par les autorités françaises le 30 novembre 2018 d'une demande de remise en application du règlement du 26 juin 2013, ont implicitement accepté le transfert de l'intéressé le 31 janvier 2019. Par deux arrêtés du 3 juin 2019, le préfet de la Mayenne a, d'une part, décidé du transfert de M. A... aux autorités espagnoles responsables de sa demande d'asile et, d'autre part, assigné l'intéressé à résidence pour une durée de 45 jours, renouvelable à la même date. Par une décision du 18 juillet 2017, l'Office français de l'immigration et de l'intégration a notifié au requérant son intention de lui suspendre les conditions matérielles d'accueil au motif qu'il n'avait pas respecté ses obligations de présentation au commissariat de police de la Mayenne du 4 au 16 juillet 2019. Par une décision du 7 octobre 2019, notifiée le 14 octobre, le préfet de la Mayenne a demandé à M. A... de se présenter au commissariat de police de Laval le 15 octobre 2019 en vue de l'exécution de son transfert aux autorités espagnoles. M. A... a demandé au juge des référés du tribunal administratif de Nantes, statuant sur le fondement de l'article L. 521-2 du code de justice administratif, d'une part, d'enjoindre au préfet de la Mayenne d'enregistrer sa demande d'asile en procédure normale et, d'autre part, d'enjoindre au directeur de l'Office français de l'immigration et de l'intégration de le rétablir dans les conditions matérielles d'accueil. Par une ordonnance du 18 octobre 2019, dont M. A... relève appel, le tribunal administratif de Nantes a rejeté sa demande.<br/>
              3. Il résulte de l'instruction que, le 28 novembre 2019, soit postérieurement à l'introduction de sa requête d'appel, M. A... s'est rendu à la préfecture de la Loire-Atlantique où une attestation de demande d'asile en procédure normale lui a été remise, ainsi qu'un formulaire de demande d'asile destiné à l'introduction de sa demande d'asile auprès de l'Office français de protection des réfugiés et des apatrides. A la suite de ce rendez-vous, l'Office français de l'immigration et de l'intégration a décidé de rétablir les conditions matérielles d'accueil au bénéfice de M. A..., avec un effet rétroactif à compter du 18 juillet 2019, date à partir de laquelle l'office avait mis fin à son bénéfice. <br/>
              4. Dans ces conditions et sans qu'il y ait lieu d'admettre M. A... au bénéfice de l'aide juridictionnelle provisoire, les conclusions de M. A... tendant à ce que le juge des référés fasse usage des pouvoirs qu'il tient de l'article L. 521-2 du code de justice administrative afin de suspendre les effets de la décision par laquelle le préfet de la Mayenne a prolongé le délai de transfert de M. A... auprès des autorités espagnoles et d'enjoindre au préfet de la Mayenne d'enregistrer sa demande d'asile en procédure normale sont devenues sans objet. Il n'y a donc plus lieu d'y statuer. <br/>
              5. Dans les circonstances de l'espèce, il y a lieu, de mettre à la charge de l'Etat le versement de la somme de 1 500 euros à M. A... en application de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : Il n'y a pas lieu de statuer sur les conclusions de la requête présentées sur le fondement de l'article L. 521-2 du code de justice administrative.<br/>
Article 2 : L'Etat versera à M. A... la somme de 1 500 euros en application de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente ordonnance sera notifiée à M. C... A..., au ministre de l'intérieur et à l'Office français de l'immigration et de l'intégration.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
