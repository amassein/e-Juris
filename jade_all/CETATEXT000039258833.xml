<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039258833</ID>
<ANCIEN_ID>JG_L_2019_10_000000419650</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/25/88/CETATEXT000039258833.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème chambre, 21/10/2019, 419650, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-10-21</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>419650</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP CELICE, SOLTNER, TEXIDOR, PERIER ; SCP BORE, SALVE DE BRUNETON, MEGRET</AVOCATS>
<RAPPORTEUR>Mme Fanélie Ducloz</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Louis Dutheillet de Lamothe</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2019:419650.20191021</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu les procédures suivantes :<br/>
<br/>
              M. et Mme B... A... ont demandé au tribunal administratif de Marseille d'annuler pour excès de pouvoir l'arrêté du 30 juillet 2015 par lequel le maire de Marseille a délivré à la société OGIC un permis de construire pour un immeuble dénommé " L'Eperon Blanc ", situé chemin du Roucas Blanc à Marseille. Par un jugement n° 1509676 du 8 février 2018, le tribunal administratif a annulé l'arrêté du maire de Marseille du 30 juillet 2015.<br/>
<br/>
              1° Sous le n° 419650, par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 9 avril et 9 juillet 2018 au secrétariat du contentieux du Conseil d'Etat, la société OGIC demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) de mettre à la charge de M. et Mme A... la somme de 2 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              2° Sous le n° 419699, par un pourvoi sommaire, un mémoire complémentaire et un autre mémoire, enregistrés les 9 avril et 9 juillet 2018 et le 10 septembre 2019 au secrétariat du Conseil d'Etat, la ville de Marseille demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) de mettre à la charge de M. C... la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu : <br/>
              - le code général des collectivités territoriales ;<br/>
              - le code de l'urbanisme ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Fanélie Ducloz, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Louis Dutheillet de Lamothe, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Célice, Soltner, Texidor, Perier, avocat de la société OGIC, à la SCP Boré, Salve de Bruneton, Mégret, avocat de M. et Mme A... et à la SCP Didier, Pinet, avocat de la ville de Marseille ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 19 septembre 2019, présentée par la société OGIC ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Les pourvois visés ci-dessus étant dirigés contre le même jugement, il y a lieu de les joindre pour statuer par une seule décision.<br/>
<br/>
              2. Il ressort des pièces du dossier soumis aux juges du fond que le maire de Marseille a, par un arrêté du 30 juillet 2015, délivré à la société OGIC un permis de construire pour un projet d'immeuble dénommé " L'Eperon blanc ", comprenant quinze logements et trente-trois places de stationnement, sur un terrain situé chemin du Roucas Blanc à Marseille. M. et Mme A... ont demandé au tribunal administratif de Marseille l'annulation pour excès de pouvoir de ce permis de construire. Par un jugement du 8 février 2018, contre lequel la société OGIC et la ville de Marseille se pourvoient en cassation, le tribunal administratif de Marseille a fait droit à cette demande.<br/>
<br/>
              3. En premier lieu, aux termes de l'article R. 431-9 du code de l'urbanisme : " Le projet architectural comprend également un plan de masse des constructions à édifier ou à modifier coté dans trois dimensions. (...) / Lorsque le terrain n'est pas directement desservi par une voie ouverte à la circulation publique, le plan de masse indique l'emplacement et les caractéristiques de la servitude de passage permettant d'y accéder. (...) ". Il ressort des pièces du dossier soumis aux juges du fond que la notice architecturale jointe au dossier de demande de permis de construire se bornait à indiquer que " le projet est desservi par une voie privée avec servitude de passage ", et que le plan de masse mentionnait l'existence d'une " route ", sans autre précision. Par suite, en jugeant que le service instructeur n'avait pu se prononcer en toute connaissance de cause pour apprécier le caractère suffisant de la desserte du projet, faute de pièces jointes à la demande de permis de construire indiquant l'emplacement de la servitude de passage et ses caractéristiques, le tribunal a porté sur les faits de l'espèce une appréciation souveraine exempte de dénaturation et n'a pas entaché son jugement d'erreur de droit et d'insuffisance de motivation. <br/>
<br/>
              4. En deuxième lieu, aux termes de l'article 3 de la zone UR du règlement du plan local d'urbanisme : " (...) Les constructions à réaliser sont desservies par au moins une voie présentant des caractéristiques suffisantes pour permettre l'accès des véhicules de lutte contre l'incendie et de secours (...) ".<br/>
<br/>
              5. Il résulte des dispositions des articles L. 1424-2 à L. 1424-4 du code général des collectivités territoriales que les services publics d'incendie et de secours sont, dans le cadre de leurs missions de protection et de secours, en droit d'intervenir sur tout le territoire de la commune, sans que puisse leur être opposé le caractère privé des voies qu'ils doivent emprunter. Dès lors, pour apprécier les possibilités d'accès de ces services au terrain d'assiette, il appartient seulement à l'autorité compétente et au juge de s'assurer que les caractéristiques physiques d'une voie d'accès permettent l'intervention de leurs engins, la circonstance que cette voie ne serait pas ouverte à la circulation publique ou grevée d''une servitude de passage étant sans incidence. Par suite, en retenant, pour dire que le permis de construire avait été délivré en méconnaissance des dispositions de l'article 3 de la zone UR du règlement du plan local d'urbanisme, qu'alors que la voie de desserte du terrain d'assiette est fermée à la circulation publique, le pétitionnaire ne justifie pas, dans le dossier de demande de permis de construire, ni avant la clôture de l'instruction, de l'existence d'un titre créant une servitude de passage permettant la desserte de son terrain par les engins d'incendie et de secours, le tribunal a entaché son jugement d'une erreur de droit.<br/>
<br/>
              6. En troisième et dernier lieu, il ressort des écritures de la société OGIC dans son mémoire en défense enregistré le 22 juillet 2016 au greffe du tribunal administratif de Marseille, qu'elle a demandé l'application des dispositions de l'article L. 600-5 du code de l'urbanisme permettant de prononcer l'annulation partielle du permis de construire et, le cas échéant, de fixer un délai dans lequel le titulaire du permis pourra en demander la régularisation. En omettant de se prononcer sur cette demande, le tribunal a entaché son jugement d'un défaut de réponse à conclusions.<br/>
<br/>
              7. Il ressort des énonciations du jugement attaqué que le tribunal a annulé le permis de construire litigieux après avoir jugé que le dossier de permis de construire était incomplet et que l'autorisation délivrée méconnaissait l'article 3 de la zone UR du règlement du plan local d'urbanisme. Toutefois, c'est par un motif entaché d'erreur de droit, ainsi qu'il a été dit au point 5, qu'il a retenu que le permis attaqué méconnaissait ces dispositions du règlement. En outre, il n'a pas recherché, ainsi que cela lui était demandé, si les vices qu'il retenait faisaient obstacle à la régularisation du permis litigieux par un permis modificatif. Dès lors, et sans qu'il soit besoin d'examiner l'autre moyen de régularité soulevé par le pourvoi n° 419699, il y a lieu de faire droit aux conclusions à fin d'annulation du jugement attaqué, présentées par la société OGIC et la ville de Marseille.<br/>
<br/>
              Sur les frais liés au litige : <br/>
<br/>
              8. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de la société OGIC et de la ville de Marseille, qui ne sont pas, dans la présente instance, la partie perdante. Il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions de la société OGIC et de la ville de Marseille présentée au titre des mêmes dispositions.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du tribunal administratif de Marseille du 8 février 2018 est annulé.<br/>
Article 2 : L'affaire est renvoyée au tribunal administratif de Marseille.<br/>
Article 3 : Les conclusions des parties présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée à la société OGIC, à la ville de Marseille, et à M. et Mme B... A....<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
