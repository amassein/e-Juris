<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037834562</ID>
<ANCIEN_ID>JG_L_2018_12_000000402028</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/83/45/CETATEXT000037834562.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème chambre, 19/12/2018, 402028, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-12-19</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>402028</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP THOUVENIN, COUDRAY, GREVY ; SCP CELICE, SOLTNER, TEXIDOR, PERIER</AVOCATS>
<RAPPORTEUR>Mme Tiphaine Pinault</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Dieu</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:402028.20181219</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société Alliance Healthcare Répartition, venant aux droits de la société Ouest Répartition Pharmaceutique, a demandé au tribunal administratif de Nancy d'annuler pour excès de pouvoir la décision du 26 avril 2012 par laquelle l'inspecteur du travail de la 4ème section de l'unité territoriale des Vosges de la direction régionale des entreprises, de la concurrence, de la consommation, du travail et de l'emploi de Lorraine a refusé à la société Ouest Répartition Pharmaceutique l'autorisation de licencier M. A...B...ainsi que la décision implicite par laquelle le ministre du travail, de l'emploi et de la santé a rejeté le recours de cette société contre cette décision. Par un jugement n° 1202658 du 16 décembre 2014, le  tribunal administratif a annulé ces deux décisions.<br/>
<br/>
              Par un arrêt n° 15NC00390 du 31 mai 2016, la cour administrative d'appel de Nancy  a rejeté l'appel formé par M. B...contre ce jugement.  <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 1er août et 2 novembre 2016 au secrétariat du contentieux du Conseil d'Etat, M. B...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de la société Alliance Healthcare Repartition la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
               Vu : <br/>
              - le code du travail ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Tiphaine Pinault, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Frédéric Dieu, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Thouvenin, Coudray, Grevy, avocat de M. B...et à la SCP Célice, Soltner, Texidor, Périer, avocat de la société Alliance Healthcare Répartition ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que la société Ouest Répartition Pharmaceutique a décidé, à la suite de difficultés économiques, de cesser son activité et de fermer, en conséquence trois établissements, dont celui de Rupt-sur-Moselle où était employée M.B..., salarié protégé, et d'en transférer un quatrième à la société Alliance Healthcare Répartition ; qu'après avoir proposé à M. B...quatre postes de reclassement, dont deux à l'étranger, qu'il a refusés, la société a demandé à l'inspecteur du travail l'autorisation de le licencier ; que, par une décision du 26 avril 2012, l'inspecteur du travail a rejeté cette demande ; que le ministre du travail, de l'emploi et de la santé a implicitement rejeté le recours hiérarchique formé par la société contre cette décision ; que M. B... demande l'annulation de l'arrêt du 31 mai 2016 par lequel la cour administrative d'appel de Nancy a rejeté son appel dirigé contre le jugement du tribunal administratif de Nancy du 16 décembre 2014 annulant pour excès de pouvoir, à la demande de la société Alliance Healthcare Répartition, venant aux droits de la société Ouest Répartition Pharmaceutique, ces deux décisions ;<br/>
<br/>
              2. Considérant qu'en vertu des dispositions du code du travail, le licenciement des salariés légalement investis de fonctions représentatives, qui bénéficient d'une protection exceptionnelle dans l'intérêt de l'ensemble des travailleurs qu'ils représentent, ne peut intervenir que sur autorisation de l'inspecteur du travail ; que, lorsque le licenciement d'un de ces salariés est envisagé, il ne doit pas être en rapport avec les fonctions représentatives normalement exercées ou l'appartenance syndicale de l'intéressé ; que, dans le cas où la demande de licenciement est fondée sur un motif de caractère économique, il appartient à l'inspecteur du travail et, le cas échéant, au ministre, de rechercher, sous le contrôle du juge de l'excès de pouvoir, si la situation de l'entreprise justifie le licenciement du salarié, en tenant compte notamment de la nécessité des réductions envisagées d'effectifs et de la possibilité d'assurer le reclassement du salarié ; <br/>
<br/>
              3. Considérant qu'aux termes de l'article L. 1233-4 du code du travail dans sa rédaction alors applicable : " Le licenciement pour motif économique d'un salarié ne peut intervenir que lorsque tous les efforts de formation et d'adaptation ont été réalisés et que le reclassement de l'intéressé ne peut être opéré dans l'entreprise ou dans les entreprises du groupe auquel l'entreprise appartient. / Le reclassement du salarié s'effectue sur un emploi relevant de la même catégorie que celui qu'il occupe ou sur un emploi équivalent assorti d'une rémunération équivalente. A défaut, et sous réserve de l'accord exprès du salarié, le reclassement s'effectue sur un emploi d'une catégorie inférieure. / Les offres de reclassement proposées au salarié sont écrites et précises " ;  que, pour apprécier si l'employeur a satisfait à cette obligation, l'autorité administrative doit s'assurer, sous le contrôle du juge de l'excès de pouvoir, qu'il a procédé à une recherche sérieuse des possibilités de reclassement du salarié, tant au sein de l'entreprise que dans les entreprises du groupe auquel elle appartient, ce dernier étant entendu comme les entreprises dont l'organisation, les activités ou le lieu d'exploitation permettent, en raison des relations qui existent avec elles, d'y effectuer la permutation de tout ou partie de son personnel ; <br/>
<br/>
              4. Considérant qu'il résulte de ce qui a été dit ci-dessus qu'il appartenait à la cour, pour juger du respect par la société Ouest Répartition Pharmaceutique de l'obligation de moyens dont elle était débitrice pour le reclassement de M.B..., de tenir compte de l'ensemble des circonstances de fait qui lui étaient soumises, notamment de ce que les recherches de reclassement conduites au sein de la société avaient débouché sur des propositions précises de reclassement, de la nature et du nombre de ces propositions, ainsi que des motifs de refus avancés par le salarié ; que dès lors, la cour a pu, sans entacher son arrêt de dénaturation ni commettre d'erreur de droit, juger que, même si ces propositions de reclassement n'étaient qu'au nombre de quatre, la société Ouest Répartition Pharmaceutique avait effectué une recherche sérieuse de reclassement en formulant à M.B..., qui les avait refusées, des propositions précises portant sur des emplois équivalents à celui qu'elle détenait ;<br/>
<br/>
              5. Considérant que la cour n'a pas davantage commis d'erreur de droit en déduisant de ce qui précède que, dans les circonstances de l'espèce, la circonstance que l'entreprise n'avait pas proposé à M. B...des postes figurant dans les actualisations du plan de sauvegarde de l'emploi était sans incidence sur le respect par la société Ouest Répartition Pharmaceutique de  l'obligation dont elle était débitrice ; <br/>
<br/>
              6. Considérant qu'il résulte de tout ce qui précède que M. B...n'est pas fondé à demander l'annulation de l'arrêt qu'il attaque ; que son pourvoi doit, par suite, être rejeté, y compris ses conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative ; qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées au même titre par la société Alliance Healthcare Répartition ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
     Article 1er : Le pourvoi de M. B...est rejeté. <br/>
Article 2 : Les conclusions présentées par la société Alliance Healthcare Répartition au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La présente décision sera notifiée à M. A...B..., à la société Alliance Healthcare Répartition et à la ministre du travail.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
