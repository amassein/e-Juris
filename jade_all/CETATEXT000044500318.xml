<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044500318</ID>
<ANCIEN_ID>JG_L_2021_12_000000434607</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/50/03/CETATEXT000044500318.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème chambre, 14/12/2021, 434607, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-12-14</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>434607</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP KRIVINE, VIAUD</AVOCATS>
<RAPPORTEUR>M. Florian Roussel</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Cécile Barrois de Sarigny</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:434607.20211214</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B... a demandé au tribunal administratif de Paris d'ordonner à l'Etat de lui attribuer un logement tenant compte de ses besoins et capacités. Par un jugement n° 1803195 du 24 avril 2018, le tribunal administratif a prononcé une astreinte à l'encontre de l'Etat. Par une ordonnance n° 1813155 du 27 mai 2019, le président de la quatrième section du tribunal administratif de Paris a jugé qu'il n'y avait pas lieu de procéder à la liquidation de cette astreinte.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 13 septembre et 12 décembre 2019 au secrétariat du contentieux du Conseil d'Etat, M. A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat le versement à la SCP Krivine et Viaud, son avocat, de la somme de 2 500 euros au titre des articles L. 761-1 du code de justice administrative et 37 de la loi n° 91-647 du 10 juillet 1991.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de la construction et de l'habitation ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Florian Roussel, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Cécile Barrois de Sarigny, rapporteure publique.<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Viaud, Krivine, avocat de M. A....<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que, par une décision du 20 septembre 2013, la commission de médiation de Paris a désigné M. A... comme prioritaire et devant être logé en urgence. Par un jugement en date du 24 avril 2018, le tribunal a, en application de l'article L. 441-2-3-1 du code de la construction et de l'habitation, prononcé une astreinte de 300 euros par mois à l'encontre de l'Etat si le préfet de la région Ile-de-France, préfet de Paris, ne justifiait pas avoir, passé la date du 1er juillet 2018, assuré le relogement de M. A.... Par une ordonnance du 27 mai 2019, contre laquelle M. A... se pourvoit en cassation, le président de la quatrième section du tribunal administratif de Paris a jugé qu'il n'y avait pas lieu de procéder à la liquidation de cette astreinte. <br/>
<br/>
              2. Aux termes de l'article L. 441-2-3-1 du code de la construction et de l'habitation : " (...) Le président du tribunal administratif ou le magistrat qu'il désigne, lorsqu'il constate que la demande a été reconnue comme prioritaire par la commission de médiation et doit être satisfaite d'urgence et que n'a pas été offert au demandeur un logement tenant compte de ses besoins et de ses capacités, ordonne le logement ou le relogement de celui-ci par l'Etat et peut assortir son injonction d'une astreinte ". Aux termes de l'article de l'article R. 778-8 du code de justice administrative : " Lorsque le président du tribunal administratif ou le magistrat désigné à cet effet constate, d'office ou sur la saisine du requérant, que l'injonction prononcée n'a pas été exécutée, il procède à la liquidation de cette astreinte en faveur du fonds prévu à l'article L. 300-2 du code de la construction et de l'habitation. (...). ".<br/>
<br/>
              3. Il ressort des termes de l'ordonnance attaquée que, pour juger qu'il n'y avait pas lieu de procéder à la liquidation de l'astreinte prononcée par le jugement du tribunal administratif du 24 avril 2018, elle se fonde sur la circonstance que M. A... n'a pas donné suite, en juillet 2015, à une proposition de logement qui lui avait été adressée et que l'Etat doit en conséquence être regardé comme ayant, à cette date, rempli ses obligations. En retenant ce motif, qui méconnaît les termes mêmes du dispositif du jugement du 24 avril 2018 qui reconnaît, à cette date, l'existence d'une obligation de relogement incombant à l'Etat, l'ordonnance attaquée est entachée d'erreur de droit. Par suite, sans qu'il soit besoin de se prononcer sur les autres moyens du pourvoi, M. A... est fondé à en demander l'annulation.<br/>
<br/>
              4. M. A... a obtenu le bénéfice de l'aide juridictionnelle. Par suite, son avocat peut se prévaloir des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991. Il y a lieu, dans les circonstances de l'espèce, de mettre à ce titre à la charge de l'Etat le versement à la SCP Krivine et Viaud, avocat de M. A..., la somme de 1 000 euros, sous réserve que cette société renonce à percevoir la somme correspondant à la part contributive de l'Etat.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : L'ordonnance du président de la quatrième section du tribunal administratif de Paris du 27 mai 2019 est annulée.<br/>
Article 2 : L'affaire est renvoyée au tribunal administratif de Paris.<br/>
<br/>
Article 3 : L'Etat versera, au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991, une somme de 1 000 euros à la SCP Krivine et Viaud, sous réserve que cette société renonce à percevoir la somme correspondant à la part contributive de l'Etat. <br/>
<br/>
Article 4 : La présente décision sera notifiée à M. B... et à la ministre de la transition écologique.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
