<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043296876</ID>
<ANCIEN_ID>JG_L_2021_03_000000445788</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/29/68/CETATEXT000043296876.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère chambre, 26/03/2021, 445788, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-03-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>445788</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP WAQUET, FARGE, HAZAN</AVOCATS>
<RAPPORTEUR>Mme Agnès Pic</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Vincent Villette</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:445788.20210326</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. C... B... a demandé au tribunal administratif de Versailles de réformer le résultat des opérations électorales qui se sont déroulées le 15 mars 2020 dans la commune de Ballainvilliers (Essonne) en vue de l'élection des conseillers municipaux ou, à titre subsidiaire, d'annuler ces opérations électorales. Par un jugement n° 2002117 du 29 septembre 2020, le tribunal administratif de Versailles a rejeté cette protestation. <br/>
              Par une requête sommaire et un mémoire complémentaire, enregistrés les 29 octobre et 30 novembre 2020 au secrétariat du contentieux du Conseil d'Etat, M B... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) de faire droit à sa protestation ;<br/>
<br/>
              3°) de mettre à la charge de Mme D... E... et des autres membres de la liste conduite par celle-ci la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
              Vu : <br/>
              - le code électoral ;<br/>
              - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme A..., Pic maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Vincent Villette, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Waquet, Farge, Hazan, avocat de M. B... ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. A l'issue des opérations électorales qui se sont déroulées le 15 mars 2020 dans la commune de Ballainvilliers, les vingt-sept sièges ont été pourvus. Vingt et un sièges ont été attribués à des candidats de la liste conduite par Mme E..., qui a obtenu 547 voix, soit 52,29 % des suffrages exprimés, et six sièges ont été attribués à des candidats de la liste conduite par M. B..., qui a obtenu 499 voix, soit 47,70 % des suffrages exprimés. M. B... relève appel du jugement du 29 septembre 2020 par lequel le tribunal administratif de Versailles a rejeté sa protestation.<br/>
<br/>
              Sur les bulletins de vote :<br/>
<br/>
              2. Aux termes de l'article L. 241 du code électoral : " Des commissions (...) sont chargées, pour les communes de 2 500 habitants et plus, d'assurer l'envoi et la distribution des documents de propagande électorale ". L'article R. 34 du même code prévoit que : " La commission de propagande (...) est chargée :  - d'adresser, au plus tard le mercredi précédent le premier tour de scrutin (...), à tous les électeurs de la circonscription, une circulaire et un bulletin de vote de chaque candidat, binôme de candidats ou liste (...). Si un candidat, un binôme de candidats ou une liste de candidats remet à la commission de propagande moins de circulaires ou de bulletins de vote que les quantités prévues ci-dessus, il peut proposer une répartition de ses circulaires et bulletins de vote entre les électeurs. A défaut de proposition ou lorsque la commission le décide, les circulaires demeurent à la disposition du candidat et les bulletins de vote sont distribués dans les bureaux de vote, à l'appréciation de la commission, en tenant compte du nombre d'électeurs inscrits (...) ". <br/>
<br/>
              3. S'il n'est pas contesté que certaines des enveloppes adressées par la commission de propagande aux électeurs de la commune ne contenaient pas le bulletin de vote de la liste " l'Alternative " menée par M. B..., il ne résulte toutefois de l'instruction, ni que M. B... aurait remis en temps utile à la commission de propagande des bulletins de vote en nombre suffisant, ni, au demeurant, que des électeurs soucieux, compte tenu de l'épidémie de covid-19, de ne pas manipuler d'autres documents que ceux reçus à leur domicile, auraient été dissuadés de se rendre dans les bureaux de vote en l'absence de bulletin de vote de cette liste dans l'enveloppe qu'ils avaient reçue. Enfin, il n'est pas contesté qu'un nombre suffisant de bulletins de vote de cette liste était disponible dans les bureaux de vote. Dans ces circonstances, compte tenu de l'écart de voix, et alors que seuls trois votes ont été déclarés nuls au motif que la circulaire électorale avait été déposée dans l'urne à la place d'un bulletin, M. B... n'est pas fondé à soutenir que l'absence d'envoi au domicile d'une partie des électeurs de bulletins de vote de la liste " L'Alternative " aurait été de nature à altérer la sincérité du scrutin.<br/>
<br/>
              Sur la régularité des opérations électorales :<br/>
<br/>
              4. Aux termes du premier alinéa de l'article L. 67 du code électoral : " Tout candidat ou son représentant dûment désigné a le droit de contrôler toutes les opérations de vote, de dépouillement des bulletins et de décompte des voix, dans tous les locaux où s'effectuent ces opérations, ainsi que d'exiger l'inscription au procès-verbal de toutes observations, protestations ou contestations sur lesdites opérations, soit avant la proclamation du scrutin, soit après ."  L'article R. 47 du même code précise que : " Chaque candidat, binôme de candidats ou liste de candidats à le droit d'exiger la présence en permanence dans chaque bureau de vote d'un délégué habilité à contrôler toutes les opérations électorales, dans les conditions fixées par le premier alinéa de l'article L. 67. (...) Les dispositions de l'article R. 46 concernant les assesseurs sont applicables aux délégués titulaires et suppléants visés au présent article ". L'article R. 46 de ce code prévoit que : " Les nom, prénoms, date et lieu de naissance et adresse des assesseurs et de leurs suppléants désignés par les candidats, binômes de candidats ou listes en présence, ainsi que l'indication du bureau de vote auquel ils sont affectés, sont notifiés au maire au plus tard à dix-huit heures le troisième jour précédant le scrutin. / Le maire délivre un récépissé de cette déclaration (...)". <br/>
<br/>
              5. D'une part, il ne résulte pas de l'instruction que la liste de M. B... aurait désigné des délégués dans les conditions prévues par les dispositions citées au point précédent. L'absence de signature de deux des trois procès-verbaux par un délégué de cette liste ne saurait ainsi témoigner de ce que les délégués désignés aurait été empêché de contrôler les opérations électorales.<br/>
              6. D'autre part, il ne résulte pas de l'instruction que les candidats et représentants de la liste " L'Alternative " qui ont assisté aux opérations de vote et au dépouillement auraient constaté des irrégularités dont l'inscription au procès-verbal leur aurait été refusée.  <br/>
              7. Il résulte de tout ce qui précède que M. B... n'est pas fondé à soutenir que c'est à tort que le tribunal administratif a rejeté sa protestation. <br/>
<br/>
              Sur les frais de l'instance :<br/>
<br/>
              8. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à la charge de Mme E... et autres, qui ne sont pas, dans la présente instance, la partie perdante. Il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par Mme E... et autres au titre des mêmes dispositions<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : La requête de M. B... est rejetée. <br/>
Article 2 : Les conclusions présentées par Mme E... et les autres défendeurs au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La présente décision sera notifiée à M. C... B..., ainsi qu'à Mme D... E..., pour l'ensemble des défendeurs, et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
