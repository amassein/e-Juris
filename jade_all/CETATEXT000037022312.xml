<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037022312</ID>
<ANCIEN_ID>JG_L_2018_06_000000412243</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/02/23/CETATEXT000037022312.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème et 9ème chambres réunies, 06/06/2018, 412243, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-06-06</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>412243</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème et 9ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DELAMARRE, JEHANNIN ; SCP OHL, VEXLIARD</AVOCATS>
<RAPPORTEUR>M. Richard Senghor</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Aurélie Bretonneau</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:412243.20180606</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              L'association pour le Musée des Iles de Saint-Pierre-et-Miquelon a demandé au tribunal administratif de Saint-Pierre et Miquelon : <br/>
              - d'annuler la convention en date du 31 décembre 1998 par laquelle elle a remis à la collectivité territoriale de Saint-Pierre-et-Miquelon la collection d'oeuvres et objets lui appartenant en vue de leur affectation au musée de l'Arche ;<br/>
              - d'annuler la désignation de M. A...en tant que président de l'association en 1995;<br/>
              - de désigner un expert afin d'examiner l'état de conservation de la collection cédée à la collectivité territoriale :<br/>
              - de vérifier l'état de conservation de la toile de M. B...D...exposée au musée et le degré d'hygrométrie des salles du musée de l'Arche ;<br/>
              - de procéder aux auditions de M. Bernard de Soavec, président du conseil général à l'époque des faits, et de Mme C...E..., directrice du musée de l'Arche ;<br/>
              - d'enjoindre au conseil territorial de fournir une copie du contrat " mentionné par Joseph Fontaine dans sa lettre du 15 mai 1995 " et de communiquer l'inventaire de la collection ;<br/>
              - d'enjoindre au conseil territorial de lui donner libre accès au local qui lui est affecté au musée de l'Arche ; <br/>
              - de déclarer que le local attribué est trop exigu, que la convention du 31 décembre 1998 n'a pas été respectée par la collectivité territoriale et que le musée de l'Arche ne "semble pas répondre aux missions dévolues aux musées de France " ;<br/>
              - d'enjoindre au conseil territorial de lui restituer les locaux actuellement occupés par le musée de l'Arche ;<br/>
              - de déclarer la directrice du musée de l'Arche et le conseil territorial responsables des dégâts et altérations éventuellement constatés sur la collection ;<br/>
              - de condamner le conseil territorial aux frais et dépens.<br/>
<br/>
              Par un jugement n° 1400039 du 15 juillet 2015, le tribunal administratif de Saint-Pierre et Miquelon  a décidé qu'il n'y avait pas lieu de statuer sur les conclusions à fin d'injonction en ce qui concerne l'inventaire de la collection cédée  à la collectivité territoriale et a rejeté le surplus de la demande.<br/>
<br/>
              Par un arrêt n°15BX03131 du 29 juin 2017, la cour administrative d'appel de Bordeaux a rejeté l'appel formé par l'association pour le Musée des Iles de Saint-Pierre-et-Miquelon contre ce jugement.  <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 7 juillet et 6 octobre 2017 au secrétariat du contentieux du Conseil d'Etat, l'association pour le Musée des îles de Saint-Pierre-et-Miquelon demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de la collectivité territoriale de Saint-Pierre et Miquelon la somme de 3000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code civil ; <br/>
              - la loi n° 2008-561 du 17 juin 2008 ;<br/>
              - le code de justice administrative ;		<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Richard Senghor, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Aurélie Bretonneau, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Delamarre, Jéhannin, avocat de l'association pour le Musée des Iles de Saint Pierre-et-Miquelon  et à la SCP Ohl, Vexliard, avocat de la collectivité territoriale de Saint-Pierre-et-Miquelon  ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que l'association pour le musée des Iles de Saint-Pierre-et-Miquelon a conclu, le 31 décembre 1998, une convention avec le conseil général de la collectivité territoriale de Saint-Pierre-et-Miquelon afin de céder à cette collectivité la propriété de l'ensemble des oeuvres d'art et objets constituant sa collection, en vue de son affectation au nouveau musée créé par la collectivité.  L'association requérante se pourvoit en cassation contre l'arrêt du 29 juin 2017 par lequel la cour administrative d'appel de Bordeaux a rejeté l'appel qu'elle avait formé contre le jugement du tribunal administratif de Saint-Pierre-et-Miquelon du 15 juillet 2015 rejetant sa demande d'annulation de la convention du 31 décembre 1998.<br/>
<br/>
              2. Aux termes de l'article 35 du décret du 27 février 2015 : " Lorsqu'une juridiction est saisie d'un litige qui présente à juger, soit sur l'action introduite, soit sur une exception, une question de compétence soulevant une difficulté sérieuse et mettant en jeu la séparation des ordres de juridiction, elle peut, par une décision motivée qui n'est susceptible d'aucun recours, renvoyer au Tribunal des conflits le soin de décider sur cette question de compétence. ".<br/>
<br/>
              3. Il ressort des pièces du dossier soumis aux juges du fond que la convention litigieuse du 31 décembre 1998 prévoit, en contrepartie de la remise de la collection de l'association  requérante au département, d'une part, que celui-ci devra respecter un certain nombre d'obligations  relatives à la garde, à la conservation et à l'utilisation de la collection, d'autre part, que l'association pourra participer à certaines activités du musée et devra donner son accord avant tout prêt ou toute exposition temporaire hors du musée de sa collection.<br/>
<br/>
              4. Le litige né de l'action de l'association pour le musée des Iles de Saint-Pierre-et-Miquelon tendant à ce que soit annulée une telle convention présente à juger une question de compétence soulevant une difficulté sérieuse et de nature à justifier le recours à la procédure prévue par l'article 35 du décret du 27 février 2015. Par suite, il y a lieu de renvoyer au Tribunal des conflits la question de savoir si l'action introduite par l'association requérante relève ou non de compétence de la juridiction administrative et de surseoir à toute procédure jusqu'à la décision de ce tribunal.<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
        --------------<br/>
Article 1er : L'affaire est renvoyée au Tribunal des conflits.<br/>
Article 2 : Il est sursis à statuer sur le pourvoi de l'association pour le musée des Iles de Saint-Pierre-et-Miquelon jusqu'à ce que le Tribunal des conflits ait tranché la question de savoir si le litige né de l'action de celle-ci tendant à l'annulation de la convention du 31 décembre 1998 conclue avec le conseil général de la collectivité territoriale de Saint-Pierre-et- Miquelon relève ou non de la compétence de la juridiction administrative.<br/>
Article 3 : La présente décision sera notifiée à l'association pour le Musée des Iles de Saint-Pierre-et-Miquelon et à la collectivité territoriale de Saint-Pierre-et-Miquelon.<br/>
Copie en sera adressée à la ministre de la culture et à la ministre des outre-mer.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
