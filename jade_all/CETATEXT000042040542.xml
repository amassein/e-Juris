<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042040542</ID>
<ANCIEN_ID>JG_L_2020_06_000000429102</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/04/05/CETATEXT000042040542.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème chambre, 12/06/2020, 429102, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-06-12</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>429102</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GATINEAU, FATTACCINI, REBEYROL</AVOCATS>
<RAPPORTEUR>Mme Myriam Benlolo Carabot</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Anne Iljic</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2020:429102.20200612</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A... B... a demandé au tribunal administratif de Poitiers de condamner l'Etat à l'indemniser d'un préjudice matériel correspondant à une perte de rémunération et aux effets de cette perte sur le montant de la prime pour l'emploi, pour un montant total de 2 865, 49 euros, d'un préjudice moral pour un montant de 1 000 euros et d'un préjudice correspondant aux troubles dans les conditions d'existence qu'il a subis lors de sa détention au centre pénitentiaire de Poitiers-Vivonne pour un montant de 1 000 euros. Par un jugement n° 1701014 du 13 juillet 2018, le tribunal administratif de Poitiers a partiellement fait droit à cette demande en condamnant l'Etat à verser à M. B... la somme de 1 749, 86 euros.<br/>
<br/>
              Par une ordonnance n° 18BX03443 du 18 mars 2019, le président de la cour administrative d'appel de Bordeaux a transmis au Conseil d'Etat, en application de l'article R. 351-2 du code de justice administrative, le pourvoi, enregistré le 13 septembre 2018 au greffe de cette cour, présenté par M. B.... <br/>
<br/>
              Par ce pourvoi et un nouveau mémoire, enregistré le 27 mai 2019 au secrétariat du contentieux du Conseil d'Etat, M. B... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement en tant qu'il n'a pas fait intégralement droit à ses demandes indemnitaires ; <br/>
<br/>
              2°) réglant l'affaire au fond, dans cette mesure, de faire intégralement droit à sa demande ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 4 500 euros à verser à son avocat, la SCP Gatineau, Fattaccini, Rebeyrol, au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991. <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code général des impôts ;<br/>
              - le code de procédure pénale ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - le code de justice administrative et l'ordonnance n° 2020-305 du 25 mars 2020 modifiée ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Myriam Benlolo Carabot, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Anne Iljic, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Gatineau, Fattaccini, Rebeyrol, avocat de M. A... B... ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis au juge du fond que M. B..., incarcéré au centre pénitentiaire de Poitiers Vivonne, a occupé les fonctions de gestionnaire de stocks au profit de l'administration pénitentiaire d'avril 2013 à juillet 2016. Par un jugement du 13 juillet 2018, le tribunal administratif de Poitiers, relevant que la rémunération perçue par le requérant au titre de son activité au sein de l'établissement avait été inférieure au minimum prévu par les dispositions combinées des articles 717-3 et D. 432-1 du code de procédure pénale, a condamné l'Etat à verser à M. B... la somme de 1 749, 86 euros assortie des intérêts légaux et de leur capitalisation, correspondant pour 1 449, 86 euros à l'indemnisation de la perte de rémunération et, pour un montant de 300 euros, à l'indemnisation des troubles dans les conditions d'existence. M. B... se pourvoit en cassation contre ce jugement en tant qu'il statue sur le préjudice matériel et le préjudice moral qu'il estime avoir subis. <br/>
<br/>
              Sur les conclusions dirigées contre le jugement en tant qu'il statue sur le préjudice matériel :<br/>
<br/>
              En ce qui concerne la perte de rémunération :<br/>
<br/>
              2. Il ressort des pièces du dossier soumis au juge du fond que M. B... soutenait avoir subi un préjudice matériel d'un montant de 2 737 euros du fait d'une rémunération de ses fonctions mentionnées au point 1 inférieure au minimum légal. En évaluant cette perte de rémunération à seulement 1 449, 86 euros sans préciser les modalités de calcul de cette somme, différente de l'évaluation qui lui était soumise, le tribunal administratif de Poitiers a entaché son jugement d'insuffisance de motivation. <br/>
<br/>
              En ce qui concerne la prime pour l'emploi :<br/>
<br/>
              3. En vertu de l'article R. 612-3 du code de justice administrative, lorsqu'une des parties appelées à produire un mémoire dans le cadre de l'instruction n'a pas respecté le délai qui lui a été imparti à cet effet, le président de la formation de jugement du tribunal administratif ou de la cour administrative d'appel peut lui adresser une mise en demeure. Aux termes de l'article R. 612-6 du code de justice administratif : " Si, malgré une mise en demeure, la partie défenderesse n'a produit aucun mémoire, elle est réputée avoir acquiescé aux faits exposés dans les mémoires du requérant ". Il résulte de ces dispositions que, sous réserve du cas où, postérieurement à la clôture de l'instruction, le défendeur soumettrait au juge une production contenant l'exposé d'une circonstance de fait dont il n'était pas en mesure de faire état avant cette date et qui serait susceptible d'exercer une influence sur le jugement de l'affaire, le défendeur à l'instance qui, en dépit d'une mise en demeure, n'a pas produit avant la clôture de l'instruction est réputé avoir acquiescé aux faits exposés par le requérant dans ses écritures. Il appartient alors seulement au juge de vérifier que la situation de fait invoquée par le demandeur n'est pas contredite par les pièces du dossier. <br/>
<br/>
              4. Il ressort des pièces de la procédure suivie devant le tribunal qu'en dépit de la mise en demeure qui lui avait été communiquée en application de l'article R. 612-3 du code de justice administrative, la garde des sceaux, ministre de la justice, n'a transmis un mémoire en défense qu'après la clôture de l'instruction, sans invoquer de circonstances dont elle n'aurait pas été en mesure de faire état avant la clôture de l'instruction. Il s'ensuit qu'elle était réputée avoir acquiescé aux faits exposés par M. B..., notamment sur la perception par celui-ci d'une prime pour l'emploi au titre des revenus perçus en 2014 et 2015. Or, pour écarter le chef de préjudice tiré de ce que le montant de cette prime aurait été plus élevé, pour ces deux années, si M. B... avait perçu l'intégralité de la rémunération qui lui était due, le tribunal administratif a relevé que les allégations du requérant selon lesquelles il aurait bénéficié de la prime pour l'emploi au titre de ces deux années n'étaient pas établies par les pièces du dossier. En statuant ainsi, alors qu'il lui appartenait seulement de vérifier que ces allégations se rapportant à des circonstances de fait n'étaient pas contredites par les pièces du dossier, le tribunal administratif a commis une erreur de droit.<br/>
<br/>
              Sur les conclusions dirigées contre le jugement en tant qu'il statue sur le préjudice moral : <br/>
<br/>
              5. En relevant, pour écarter toute indemnisation du préjudice moral allégué par M. B..., qu'il ne résultait pas de l'instruction que l'insuffisance de sa rémunération et le retard pris par voie de conséquence pour l'indemnisation des parties civiles auraient eu une incidence sur sa réinsertion et sur la préparation de son projet de sortie, le tribunal administratif n'a pas commis d'erreur de droit.<br/>
<br/>
              6. Il résulte de tout ce qui précède que M. B... est seulement fondé à demander l'annulation du jugement qu'il attaque en tant qu'il statue sur le préjudice matériel subi. <br/>
<br/>
              7. M. B... a obtenu le bénéfice de l'aide juridictionnelle. Il s'ensuit que son avocat peut se prévaloir des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991. Il y a lieu, dans les circonstances de l'espèce, sous réserve que la SCP Gatineau, Fattaccini, Rebeyrol renonce à percevoir la somme correspondant à la part contributive de l'Etat, de mettre à la charge de l'Etat la somme de 3 000 euros à verser à cette société.  <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
                            --------------<br/>
<br/>
Article 1er : Le jugement du 13 juillet 2018 du tribunal administratif de Poitiers est annulé en tant qu'il statue sur le préjudice matériel subi par M. B.... <br/>
Article 2 : L'affaire est renvoyée dans cette mesure au tribunal administratif de Poitiers. <br/>
Article 3 : L'Etat versera la somme de 3 000 euros à la SCP Gatineau, Fattaccini, Rebeyrol avocat de M. B..., en application des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991, sous réserve que cette société renonce à percevoir la somme correspondant à la part contributive de l'Etat. <br/>
Article 4 : Le surplus des conclusions du pourvoi est rejeté.<br/>
Article 5 : La présente décision sera notifiée à M. A... B... et à la garde des sceaux, ministre de la justice.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
