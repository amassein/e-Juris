<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036566162</ID>
<ANCIEN_ID>JG_L_2018_02_000000412155</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/56/61/CETATEXT000036566162.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème - 8ème chambres réunies, 01/02/2018, 412155, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-02-01</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>412155</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème - 8ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP NICOLAY, DE LANOUVELLE, HANNOTIN</AVOCATS>
<RAPPORTEUR>M. Géraud Sajust de Bergues</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Vincent Daumas</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:412155.20180201</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société par actions simplifiée (SAS) Mi Développement 2 a demandé au tribunal administratif de Rennes de prononcer la décharge des cotisations supplémentaires d'impôt sur les sociétés et de contribution additionnelle à cet impôt auxquelles elle a été assujettie au titre des exercices 2008 et 2009 ainsi que des pénalités correspondantes. <br/>
<br/>
              Par un jugement n° 1300571 du 13 avril 2015, le tribunal administratif a rejeté cette demande.<br/>
<br/>
              Par un arrêt n° 15NT01908 du 7 mai 2017, la cour administrative d'appel de Nantes a rejeté l'appel formé par la SAS Mi Développement 2 contre ce jugement.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 5 juillet et 5 octobre 2017 au secrétariat du contentieux du Conseil d'Etat, la SAS Mi Développement 2 demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat une somme de 10 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Par un mémoire distinct, enregistré le 6 novembre 2017, la SAS Mi Développement 2 a demandé, en application de l'article 23-1 de l'ordonnance n° 58-1067 du 7 novembre 1958, au Conseil d'Etat de transmettre au Conseil constitutionnel une question prioritaire de constitutionnalité relative à la conformité aux droits et libertés garantis par la Constitution du septième alinéa de l'article 223 B du code général des impôts. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
               - la Constitution, notamment son article 61-1 ; <br/>
              - l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
              - la loi n° 2007-2024 du 25 décembre 2007 ; <br/>
               - le code général des impôts et le livre des procédures fiscales ; <br/>
               - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Géraud Sajust de Bergues, conseiller d'Etat, <br/>
<br/>
              - les conclusions de M. Vincent Daumas, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Nicolaÿ, de Lanouvelle, Hannotin, avocat de la société Mi Developpement 2 ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il résulte des dispositions de l'article 23-5 de l'ordonnance n° 58-1067 du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel que, lorsque le moyen tiré de ce qu'une disposition législative porte atteinte aux droits et libertés garantis par la Constitution est soulevé à l'occasion d'une instance devant le Conseil d'Etat, le Conseil constitutionnel est saisi de cette question prioritaire de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et qu'elle soit nouvelle ou présente un caractère sérieux.<br/>
<br/>
              2. En vertu du septième alinéa de l'article 223 B du code général des impôts, dans sa rédaction applicable aux exercices 2008 et 2009, issue de l'article 59 de la loi de finances du 25 décembre 2007 rectificative pour 2007 : " Lorsqu'une société a acheté, après le 1er janvier 1988, les titres d'une société qui devient membre du même groupe aux personnes qui la contrôlent, directement ou indirectement, ou à des sociétés que ces personnes contrôlent, directement ou indirectement, au sens de l'article L. 233-3 du code de commerce, les charges financières déduites pour la détermination du résultat d'ensemble sont rapportées à ce résultat pour une fraction égale au rapport du prix d'acquisition de ces titres à la somme du montant moyen des dettes, de chaque exercice, des entreprises membres du groupe. Le prix d'acquisition à retenir est réduit du montant des fonds apportés à la société cessionnaire lors d'une augmentation du capital réalisée simultanément à l'acquisition des titres à condition que ces fonds soient apportés à la société cessionnaire par une personne autre qu'une société membre du groupe ou, s'ils sont apportés par une société du groupe, qu'ils ne proviennent pas de crédits consentis par une personne non membre de ce groupe. La réintégration s'applique pendant l'exercice d'acquisition des titres et les huit exercices suivants ". <br/>
<br/>
              3. Il résulte de ces dispositions que l'administration est fondée à réintégrer dans les résultats de la société mère d'un groupe fiscalement intégré une fraction des charges financières du groupe, lorsqu'une société est acquise en vue d'être intégrée par une société du groupe auprès d'une ou de plusieurs personnes qui contrôlent la société cessionnaire. Cette disposition est applicable, compte tenu de ce que l'existence d'un tel contrôle s'apprécie par référence aux critères définis par l'article L. 233-3 du code de commerce, non seulement dans l'hypothèse d'une identité entre le ou les actionnaires de la société cédée et le ou les actionnaires exerçant le contrôle de la société cessionnaire mais également dans le cas où l'actionnaire qui contrôlait la société cédée exerce, de concert avec d'autres actionnaires, le contrôle de la société cessionnaire. <br/>
<br/>
              4. La SAS Mi Développement 2 soutient, à l'appui de son pourvoi, que le septième alinéa de l'article 223 B du code général des impôts, qui a pour objet de lutter contre les montages abusifs dont le but est de réduire les résultats imposables d'un groupe faisant l'objet d'une intégration fiscale en utilisant cette intégration fiscale pour procéder à la " vente d'une société à soi-même ", en la finançant par l'emprunt, méconnaît le principe d'égalité devant la loi résultant de l'article 6 de la Déclaration des droits de l'homme et du citoyen et le principe d'égalité devant les charges publiques résultant de l'article 13 de cette dernière, en ne permettant pas au contribuable d'apporter la preuve que l'opération de restructuration effectuée dans ce cadre ne revêt pas un caractère artificiel. <br/>
<br/>
              5. L'article 223 B dans sa rédaction précitée est applicable au litige. Cette disposition n'a pas été déclarée conforme à la Constitution par le Conseil constitutionnel. Présente un caractère sérieux le moyen tiré de ce qu'elle porte atteinte au principe d'égalité devant les charges publiques en ne ménageant pas la possibilité pour le contribuable d'apporter la preuve, dans l'hypothèse où l'actionnaire qui contrôlait la société cédée exerce, de concert avec d'autres actionnaires, le contrôle de la société cessionnaire, que l'opération ne poursuit pas qu'un but fiscal. Il y a lieu, par suite, de la renvoyer au Conseil constitutionnel.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La question de la conformité à la Constitution du septième alinéa de l'article 223 B du code général des impôts, en tant qu'il porte atteinte au principe d'égalité devant les charges publiques, est renvoyée au Conseil constitutionnel.<br/>
Article 2 : Il est sursis à statuer sur le pourvoi de la SAS Mi Développement 2 jusqu'à ce que le Conseil constitutionnel ait tranché la question de constitutionnalité ainsi soulevée.<br/>
Article 3 : La présente décision sera notifiée à la SAS Mi Développement 2 et au ministre de l'action et des comptes publics. <br/>
Copie en sera adressée au Premier ministre. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
