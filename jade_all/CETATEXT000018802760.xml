<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000018802760</ID>
<ANCIEN_ID>JG_L_2008_05_000000280935</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/18/80/27/CETATEXT000018802760.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère et 6ème sous-sections réunies, 14/05/2008, 280935</TITRE>
<DATE_DEC>2008-05-14</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>280935</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère et 6ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Martin</PRESIDENT>
<AVOCATS>SCP VIER, BARTHELEMY, MATUCHANSKY ; SCP WAQUET, FARGE, HAZAN</AVOCATS>
<RAPPORTEUR>M. Eric  Berti</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Derepas Luc</COMMISSAIRE_GVT>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête sommaire et le mémoire complémentaire, enregistrés les 27 mai et 23 septembre 2005 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. Christophe B, demeurant ... et Mme Christine A ainsi que les autres héritiers de M. Michel A, venant aux droits de celui-ci, élisant domicile à cette même adresse ; M. B et autres demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt du 29 mars 2005 par lequel la cour administrative d'appel de Bordeaux a, d'une part, annulé le jugement du 9 novembre 2001 du tribunal administratif de Pau et, d'autre part, rejeté leurs conclusions tendant à la condamnation du centre hospitalier d'Oloron-Sainte-Marie et de l'Etat à leur verser une somme de 463 452 F (70 652,80 euros) assortie des intérêts au taux légal à compter de la demande préalable ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à leur appel incident ;<br/>
<br/>
              3°) de mettre à la charge du centre hospitalier d'Oloron-Sainte-Marie et de l'Etat le versement de la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de la santé publique ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Eric Berti, chargé des fonctions de Maître des requêtes,  <br/>
<br/>
              - les observations de la SCP Vier, Barthélemy, Matuchansky, avocat de M. B et autres et de la SCP Waquet, Farge, Hazan, avocat du centre hospitalier d'Oloron-Sainte-Marie, <br/>
<br/>
              - les conclusions de M. Luc Derepas, Commissaire du gouvernement ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, dans le cadre d'un GIE « Scanner d'Oloron » constitué en 1993, le centre hospitalier d'Oloron-Sainte-Marie a signé, le 16 décembre 1995, avec deux radiologues de cette ville, MM. A et B, ainsi qu'avec l'Etat représenté par le préfet des Pyrénées-Atlantiques, une convention prévoyant l'exploitation en commun d'un scanner devant être installé dans les locaux de ce centre hospitalier ; que cette convention prévoyait plusieurs engagements de la part du centre hospitalier d'Oloron-Sainte-Marie, tels que, dans son article 4, la diminution immédiate de 25 % de son activité externe de radiologie conventionnelle, par rapport au volume de cette activité en 1995, cette limitation étant assortie d'un mécanisme de contrôle de cette activité par le préfet et de sanctions en cas de non-respect, l'article 6 de la convention stipulant que tout dépassement supérieur à 1 % de l'activité ainsi plafonnée donnerait lieu à une augmentation de la participation du centre hospitalier aux frais de fonctionnement du GIE, d'un montant égal à la valeur des actes supplémentaires réalisés ; que l'activité de radiologie du centre hospitalier d'Oloron-Sainte-Marie s'étant révélée en 1996 et en 1997 très supérieure au volume d'activité de référence de 1995, MM. A et B ont engagé une action tendant à mettre en oeuvre la responsabilité contractuelle du centre hospitalier d'Oloron-Sainte-Marie et de l'Etat, en application de l'article 4 de la convention, en vue du versement d'une somme correspondant à la partie excédentaire de son activité externe au regard de l'engagement pris dans la convention, évaluée à 463 452 F pour la seule année 1997, à 215 224,63 euros avec intérêts pour les années 1998, 1999 et 2000 et 84 654,48 euros avec intérêts au titre de l'année 2001 ;<br/>
<br/>
              Considérant que, par un premier jugement du 9 novembre 2001, le tribunal administratif de Pau a fait partiellement droit à cette demande pour l'année 1997 en condamnant l'Etat et le centre hospitalier d'Oloron-Sainte-Marie à verser aux deux praticiens une somme de 150 000 F pour 1997, tandis que, par un second jugement du 14 octobre 2004, il a mis à la charge de l'Etat et du centre hospitalier les sommes de 54 100 et 27 300 euros pour les années 1998 à 2001 ; que par un arrêt du 29 mars 2005, contre lequel se pourvoient les requérants, la cour administrative d'appel de Bordeaux, saisie par le centre hospitalier d'Oloron-Sainte-Marie et, à titre incident, par MM. A et B, a annulé le premier jugement et rejeté la demande des intéressés ainsi que leur appel incident ;<br/>
<br/>
              Considérant, en premier lieu, que, lorsqu'une partie est une personne morale, il appartient à la juridiction administrative saisie, qui en a toujours la faculté, de s'assurer, le cas échéant, que le représentant de cette personne morale justifie de sa qualité pour agir au nom de cette partie ; que tel est le cas lorsque cette qualité est contestée sérieusement par l'autre partie ou qu'au premier examen, l'absence de qualité du représentant de la personne morale semble ressortir des pièces du dossier ; qu'il ressort des pièces du dossier soumis aux juges du fond que la cour administrative d'appel de Bordeaux a été saisie par une requête d'appel dirigée contre le jugement du 9 novembre 2001 du tribunal administratif de Pau indiquant que le centre hospitalier d'Oloron-Sainte-Marie était représenté par un avocat « dûment habilité par délibération du conseil d'administration » ; qu'au surplus, par une délibération du conseil d'administration du 29 avril 1998 qui figurait au dossier de première instance, le directeur du centre hospitalier avait été autorisé, dans le litige né des demandes indemnitaires présentées par les docteurs A et B, à défendre devant la juridiction compétente « les intérêts du centre hospitalier d'Oloron-Sainte-Marie et du service public hospitalier » ; qu'en s'abstenant, au vu de ces éléments et en l'absence de toute fin de non-recevoir soulevée par les intimés, de demander au centre hospitalier de régulariser sa requête par une nouvelle délibération de son conseil d'administration et en statuant sur cette requête, la cour administrative d'appel n'a pas méconnu son office ni commis d'erreur de droit ; <br/>
<br/>
              Considérant, en deuxième lieu, que les dispositions du titre Ier du livre VI du code de la santé publique, dans leur rédaction en vigueur à la date de signature de la convention conclue le 16 décembre 1995 entre les deux médecins, le centre hospitalier et le préfet des Pyrénées-Atlantiques, imposent l'élaboration et la révision d'une carte sanitaire et d'un schéma d'organisation sanitaire en vue de prévoir et de susciter les évolutions nécessaires de l'offre de soins afin de répondre de façon optimale aux besoins de santé de la population et de leur évolution ; qu'à ce titre, elles soumettent notamment à autorisation administrative, en principe délivrée par le préfet de région, l'installation et le maintien, au-delà de la durée de cette première autorisation, d'équipements lourds tels que les scanners et permettent, le cas échéant, de subordonner leur délivrance à des engagements relatifs au volume d'activité, compte tenu des besoins de la population et des objectifs définis par le schéma d'organisation ; qu'elles habilitent également les établissements publics de santé, dans le cadre des missions qui leur sont imparties, à signer des conventions avec des personnes de droit public et privé ; que cependant, aucune de ces dispositions, non plus qu'aucune autre disposition législative ou réglementaire ni aucun principe, n'autorise un centre hospitalier ou l'Etat à consentir à la réduction en volume d'activités de soins relevant de la mission de service public de l'établissement public de santé dans le seul but que l'installation dans ses locaux d'un équipement lourd de santé ne compromette pas l'activité d'établissements de santé privés ou de médecins libéraux, indépendamment de toute évaluation des besoins de santé de la population au regard de cette activité et de leur évolution, et ce alors même que ces établissements ou médecins participeraient au financement de cet équipement et l'utiliseraient ; qu'au contraire, les dispositions de l'article L. 711-4 du code de la santé publique, devenu l'article L. 6112-2 de ce code, imposent aux établissements qui concourent au service public hospitalier de garantir l'égal accès de tous aux soins qu'ils dispensent et d'être ouverts à toutes les personnes dont l'état requiert leurs services ;<br/>
<br/>
              Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que la convention conclue le 16 décembre 1995 impose à son article 4 une réduction, en volume et sans limitation de durée, de l'activité de consultation externe de radiologie conventionnelle du centre hospitalier, le non-respect de cet engagement faisant, aux termes de l'article 6, l'objet d'une sanction pécuniaire  et qu'elle prévoit en outre, à ses articles 3 et 5, que les activités de sénologie et de mammographie du centre hospitalier ne seront plus exercées dans ses locaux mais dans ceux du cabinet des docteurs B et A et avec leur matériel, selon certains horaires limités et moyennant une redevance, et que le centre hospitalier continuera à ne pas pratiquer d'activité dite de panoramique dentaire ; qu'il résulte de ce qui a été dit ci-dessus qu'en jugeant qu'une telle convention était nulle et ne pouvait pas, dès lors, créer de droits au profit des docteurs B et A, la cour administrative d'appel de Bordeaux, qui n'a procédé à aucune inversion de charge de la preuve et a suffisamment motivé son arrêt, n'a pas commis d'erreur de droit ni dénaturé les termes de la convention non plus que les autres pièces du dossier qui lui était soumis ;<br/>
<br/>
              Considérant, enfin, que la cour n'a pas commis d'erreur de droit ni entaché son appréciation de dénaturation en estimant, après avoir jugé nulle la convention du 16 décembre 1995 et rejeté les conclusions des requérants fondées sur les manquements de l'Etat et du centre hospitalier à leurs obligations contractuelles, que les docteurs B et A, dont l'activité de radiologie a augmenté depuis l'installation du scanner et qui ne soutenaient pas devant elle avoir exposé des dépenses en pure perte au bénéfice de l'administration, n'établissaient pas avoir subi un préjudice imputable à la faute commise par l'Etat et le centre hospitalier d'Oloron-Sainte-Marie en concluant avec eux une convention illégale ;<br/>
<br/>
              Considérant qu'il résulte de tout ce qui précède que M. B et les héritiers de M. A ne sont pas fondés à demander l'annulation de l'arrêt du 29 mars 2005 de la cour administrative d'appel de Bordeaux ; que, par voie de conséquence, leurs conclusions tendant à l'application de l'article L. 761-1 du code de justice administrative doivent également être rejetées ; que, dans les circonstances de l'espèce, il n'y a pas lieu de faire droit aux conclusions présentées par le centre hospitalier d'Oloron-Sainte-Marie sur le fondement des mêmes dispositions ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de M. B et des héritiers de M. A est rejetée.<br/>
Article 2 : Les conclusions présentées par le centre hospitalier d'Oloron-Sainte-Marie au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La présente décision sera notifiée à M. Christophe B, à Mme Christine A, aux autres héritiers de M. Michel A, au centre hospitalier d'Oloron Sainte-Marie et à la ministre de la santé, de la jeunesse, des sports et de la vie associative.<br/>
<br/>
<br/>
<br/>
</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">61-035 SANTÉ PUBLIQUE. PROFESSIONS MÉDICALES ET AUXILIAIRES MÉDICAUX. - CONVENTION ENTRE DES ÉTABLISSEMENTS PUBLICS DE SANTÉ ET DES PERSONNES DE DROIT PUBLIC OU PRIVÉ POUR L'INSTALLATION D'ÉQUIPEMENTS LOURDS - POSSIBILITÉ DE CONSENTIR UNE RÉDUCTION D'ACTIVITÉS POUR NE PAS COMPROMETTRE L'ACTIVITÉ DE MÉDECINS LIBÉRAUX - ABSENCE - CONSÉQUENCE - NULLITÉ DE LA CONVENTION.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">61-06-05 SANTÉ PUBLIQUE. ÉTABLISSEMENTS PUBLICS DE SANTÉ. EXERCICE D'UNE ACTIVITÉ LIBÉRALE. - CONVENTION ENTRE DES ÉTABLISSEMENTS PUBLICS DE SANTÉ ET DES PERSONNES DE DROIT PUBLIC OU PRIVÉ POUR L'INSTALLATION D'ÉQUIPEMENTS LOURDS - POSSIBILITÉ DE CONSENTIR UNE RÉDUCTION D'ACTIVITÉS POUR NE PAS COMPROMETTRE L'ACTIVITÉ DE MÉDECINS LIBÉRAUX - ABSENCE - CONSÉQUENCE - NULLITÉ DE LA CONVENTION.
</SCT>
<ANA ID="9A"> 61-035 Les dispositions du titre Ier du livre VI du code de la santé publique, dans leur rédaction alors applicable, soumettent notamment à autorisation administrative, en principe délivrée par le préfet de région, l'installation et le maintien, au-delà de la durée de cette première autorisation, d'équipements lourds tels que les scanners. Elles permettent également, le cas échéant, de subordonner leur délivrance à des engagements relatifs au volume d'activité, compte tenu des besoins de la population et des objectifs définis par le schéma d'organisation. Enfin, elles habilitent les établissements publics de santé, dans le cadre des missions qui leur sont imparties, à signer des conventions avec des personnes de droit public et privé. Toutefois, aucune de ces dispositions, non plus qu'aucune autre disposition législative ou réglementaire ni aucun principe, n'autorise un centre hospitalier ou l'Etat à consentir à la réduction en volume d'activités de soins relevant de la mission de service public de l'établissement public de santé dans le seul but que l'installation dans ses locaux d'un équipement lourd de santé ne compromette pas l'activité d'établissements de santé privés ou de médecins libéraux, indépendamment de toute évaluation des besoins de santé de la population au regard de cette activité et de leur évolution, et ce alors même que ces établissements ou médecins participeraient au financement de cet équipement et l'utiliseraient. Au contraire, les dispositions de l'article L. 711-4 du code de la santé publique, devenu l'article L. 6112-2 de ce code, imposent aux établissements qui concourent au service public hospitalier de garantir l'égal accès de tous aux soins qu'ils dispensent et d'être ouverts à toutes les personnes dont l'état requiert leurs services. Par suite, la convention conclue entre un centre hospitalier et deux radiologues, prévoyant l'exploitation en commun d'un scanner devant être installé au sein du centre hospitalier, est nulle dès lors qu'elle impose une réduction, en volume et sans limitation de durée, de l'activité de consultation externe de radiologie conventionnelle du centre hospitalier sous peine de sanction pécuniaire et qu'elle prévoit que certaines activités ne seront plus exercées dans les locaux du centre hospitalier mais par des radiologues parties à la convention, selon certains horaires limités et moyennant une redevance.</ANA>
<ANA ID="9B"> 61-06-05 Les dispositions du titre Ier du livre VI du code de la santé publique, dans leur rédaction alors applicable, soumettent notamment à autorisation administrative, en principe délivrée par le préfet de région, l'installation et le maintien, au-delà de la durée de cette première autorisation, d'équipements lourds tels que les scanners. Elles permettent également, le cas échéant, de subordonner leur délivrance à des engagements relatifs au volume d'activité, compte tenu des besoins de la population et des objectifs définis par le schéma d'organisation. Enfin, elles habilitent les établissements publics de santé, dans le cadre des missions qui leur sont imparties, à signer des conventions avec des personnes de droit public et privé. Toutefois, aucune de ces dispositions, non plus qu'aucune autre disposition législative ou réglementaire ni aucun principe, n'autorise un centre hospitalier ou l'Etat à consentir à la réduction en volume d'activités de soins relevant de la mission de service public de l'établissement public de santé dans le seul but que l'installation dans ses locaux d'un équipement lourd de santé ne compromette pas l'activité d'établissements de santé privés ou de médecins libéraux, indépendamment de toute évaluation des besoins de santé de la population au regard de cette activité et de leur évolution, et ce alors même que ces établissements ou médecins participeraient au financement de cet équipement et l'utiliseraient. Au contraire, les dispositions de l'article L. 711-4 du code de la santé publique, devenu l'article L. 6112-2 de ce code, imposent aux établissements qui concourent au service public hospitalier de garantir l'égal accès de tous aux soins qu'ils dispensent et d'être ouverts à toutes les personnes dont l'état requiert leurs services. Par suite, la convention conclue entre un centre hospitalier et deux radiologues, prévoyant l'exploitation en commun d'un scanner devant être installé au sein du centre hospitalier, est nulle dès lors qu'elle impose une réduction, en volume et sans limitation de durée, de l'activité de consultation externe de radiologie conventionnelle du centre hospitalier sous peine de sanction pécuniaire et qu'elle prévoit que certaines activités ne seront plus exercées dans les locaux du centre hospitalier mais par des radiologues parties à la convention, selon certains horaires limités et moyennant une redevance.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
