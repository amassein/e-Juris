<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026664205</ID>
<ANCIEN_ID>JG_L_2012_11_000000326375</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/66/42/CETATEXT000026664205.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  9ème sous-section jugeant seule, 21/11/2012, 326375, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2012-11-21</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>326375</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 9ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Jean-Pierre Jouguelet</PRESIDENT>
<AVOCATS>SCP PEIGNOT, GARREAU, BAUER-VIOLAS ; SCP POTIER DE LA VARDE, BUK LAMENT ; SCP DELAPORTE, BRIARD, TRICHET</AVOCATS>
<RAPPORTEUR>M. Matthieu Schlesinger</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Claire Legras</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2012:326375.20121121</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 23 mars et 23 juin 2009 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. Jean-Pierre B, demeurant ... ; M. B demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 08DA00807-08DA00808 du 22 janvier 2009 par lequel la cour administrative d'appel de Douai a rejeté ses requêtes tendant, d'une part, à l'annulation des jugements n°s 0600849 et 0600850 du 20 mars 2008 par lesquels le tribunal administratif de Rouen a rejeté les demandes de M. Pierre B, son père décédé en cours d'instance devant ce tribunal, tendant à l'annulation des décisions du 26 août 2001 et du 13 novembre 2005 du maire de la commune de Croixmare ayant délivré deux permis à l'EARL de Gainnemare en vue de la construction, puis de l'extension, d'un bâtiment à usage de stabulation, d'autre part, à ce que soient satisfaites les demandes de son père présentées en première instance ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à ses requêtes ;<br/>
<br/>
              3°) de mettre à la charge de l'EARL Gainnemare et de la commune de Croixmare le versement d'une somme de 2 500 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de l'urbanisme ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Matthieu Schlesinger, Auditeur,  <br/>
<br/>
              - les observations de la SCP Potier de la Varde, Buk Lament, avocat de M. B, de la SCP Delaporte, Briard, Trichet, avocat de l'EARL de Gainnemare et de la SCP Peignot, Garreau, Bauer-Violas, avocat de la commune de Croixmare,<br/>
<br/>
              - les conclusions de Mme Claire Legras, rapporteur public ;<br/>
<br/>
              - La parole ayant été a nouveau donné à la SCP Potier de la Varde, Buk Lament, avocat de M. B, à la SCP Delaporte, Briard, Trichet, avocat de l'EARL de Gainnemare et à la SCP Peignot, Garreau, Bauer-Violas, avocat de la commune de Croixmare ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que M. Pierre B, décédé le 24 octobre 2007, était propriétaire d'un terrain situé sur le territoire de la commune de Croixmare, donné à bail à l'EARL de Gainnemare ; que par un arrêté du 26 août 2001, le maire de Croixmare a accordé à cette EARL un permis de construire un bâtiment à usage de stabulation ; qu'un second permis a ensuite été accordé, aux fins d'extension de la stabulation, par arrêté du 13 novembre 2005 du maire de Croixmare ; que M. Pierre B a, le 14 mars 2006, demandé au tribunal administratif de Rouen d'annuler pour excès de pouvoir ces deux arrêtés ; que le tribunal a, par un premier jugement du 20 mars 2008, déclaré irrecevable la requête de M. Pierre B à l'encontre du permis délivré le 26 août 2001 et a, par un second jugement en date du même jour, rejeté comme non fondée sa requête à l'encontre du permis du 13 novembre 2005 ; que M. Jean-Pierre B, agissant en qualité d'héritier de M. Pierre B, a relevé appel de ces jugements que la cour administrative d'appel de Douai a confirmés par un arrêt en date du 22 janvier 2009 ; que M. Jean-Pierre B se pourvoit en cassation contre cet arrêt ;<br/>
<br/>
              Sur l'arrêt de la cour en tant qu'il statue sur le permis délivré le 26 août 2001 :<br/>
<br/>
              2. Considérant que le juge d'appel peut se prononcer sur un moyen examiné par le tribunal administratif par adoption des motifs des premiers juges sans méconnaître le principe de motivation des jugements, rappelé à l'article L. 9 du code de justice administrative, dès lors que la réponse du tribunal à ce moyen était elle-même suffisante et n'appelait pas de nouvelles précisions en appel ;<br/>
<br/>
              3. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que M. Jean-Pierre B soulevait dans sa requête d'appel, pour critiquer la fin de non-recevoir opposée par le tribunal administratif de Rouen à M. Pierre B, les mêmes moyens que ceux soulevés par ce dernier dans son mémoire en réplique devant les premiers juges, en reprenant la même argumentation, relative au défaut de caractère probant des attestations produites par l'EARL pour établir la réalité et le caractère suffisant de l'affichage et à l'absence des mentions requises par l'article A. 421-7 du code de l'urbanisme ; que le jugement du tribunal administratif avait, pour accueillir la fin de non-recevoir tirée de la tardiveté de la requête, écarté cette argumentation par des motifs qui y répondaient de façon complète ; que, par suite, la cour a pu écarter les moyens dont elle était saisie, tirés de ce que le délai de recours n'était pas expiré, par adoption des motifs retenus par le tribunal administratif de Rouen sans entacher sa décision d'insuffisance de motivation ;<br/>
<br/>
              Sur l'arrêt de la cour en tant qu'il statue sur le permis délivré le 13 novembre 2005 :<br/>
<br/>
              4. Considérant qu'aux termes de l'article R. 421-1-1 du code de l'urbanisme alors applicable : " La demande de permis de construire est présentée soit par le propriétaire du terrain ou son mandataire, soit par une personne justifiant d'un titre l'habilitant à construire sur le terrain, soit par une personne ayant qualité pour bénéficier de l'expropriation dudit terrain pour cause d'utilité publique. / La demande précise l'identité du demandeur, l'identité et la qualité de l'auteur du projet, la situation et la superficie du terrain, l'identité de son propriétaire au cas où celui-ci n'est pas l'auteur de la demande, la nature des travaux et la destination des constructions et la densité des constructions (...) " ; que, pour juger que l'EARL de Gainnemare disposait d'un titre l'habilitant à procéder à l'extension de la stabulation, autorisée par le permis de construire du 13 novembre 2005, la cour a retenu qu'une lettre manuscrite de M. Pierre B, en date du 25 novembre 2002, constituait un tel titre, sans répondre au moyen, qui n'était pas inopérant, tiré de ce que ce courrier aurait été obtenu de façon frauduleuse ; que, par suite, M. Jean-Pierre B est fondé à soutenir que la cour administrative d'appel de Douai a insuffisamment motivé son arrêt ; que, dès lors et sans qu'il soit besoin d'examiner l'autre moyen soulevé par le pourvoi à l'appui des mêmes conclusions, cet arrêt doit être annulé en tant qu'il statue sur l'appel formé par M. Jean-Pierre B contre le jugement du 20 mars 2008 par lequel le tribunal administratif de Rouen a rejeté la requête de M. Pierre B tendant à l'annulation, pour excès de pouvoir, du permis de construire délivré le 13 novembre 2005 ;<br/>
<br/>
              5. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond, dans la limite de la cassation ainsi prononcée ;<br/>
<br/>
              Sur les fins de non-recevoir soulevées par la commune de Croixmare et par l'EARL de Gainnemare :<br/>
<br/>
              6. Considérant, en premier lieu, qu'aux termes de l'article R. 490-7 du code de l'urbanisme alors applicable : " Le délai de recours contentieux à l'encontre d'un permis de construire court à l'égard des tiers à compter de la plus tardive des deux dates suivantes : / a) Le premier jour d'une période continue de deux mois d'affichage sur le terrain des pièces mentionnées, selon le cas, au premier ou au deuxième alinéa de l'article R. 421-39 ; / b) Le premier jour d'une période continue de deux mois d'affichage en mairie des pièces mentionnées au troisième alinéa de l'article R. 421-39 (...) " ; que, d'une part, si la commune de Croixmare et l'EARL de Gainnemare soutiennent que la requête de M. Pierre B, tendant à l'annulation du permis de construire du 13 novembre 2005 et enregistrée au greffe du tribunal administratif de Rouen le 14 mars 2006, a été présentée tardivement, elles n'apportent aucun élément de nature à établir que le permis de construire litigieux aurait été affiché ni sur le terrain ni en mairie ; que, d'autre part, si, par dérogation aux dispositions de l'article R. 490-7 précitées, l'exercice par un tiers d'un recours administratif ou contentieux contre un permis de construire a pour effet de faire courir le délai de recours contentieux à l'égard de ce tiers, en revanche, le courrier de protestation adressé au maire par M. Jean-Pierre B le 25 janvier 2006, faisant état d'un échange du 19 décembre 2005 et l'informant de son intention de former un recours contentieux, ne peut être regardé comme valant connaissance acquise du permis de construire litigieux susceptible de faire courir le délai de recours contentieux à compter du 25 janvier 2006 ni, a fortiori, du 19 décembre 2005 ; que, par suite, la commune de Croixmare et l'EARL de Gainnemare ne sont pas fondées à soutenir que la requête aurait été présentée tardivement ;<br/>
<br/>
              7. Considérant, en second lieu, que M. Jean-Pierre B, héritier de M. Pierre B décédé le 24 octobre 2007 alors que l'affaire était en l'état devant le tribunal administratif de Rouen, avait qualité pour faire appel du jugement rendu par ce tribunal le 20 mars 2008, alors même que l'instance n'avait pas été reprise devant les premiers juges et que cet appel n'avait pas été formé conjointement par les autres héritiers du requérant ;<br/>
<br/>
              Sur la légalité du permis de construire du 13 novembre 2005 :<br/>
<br/>
              9. Considérant qu'il ressort des pièces du dossier que, dans sa demande de permis de construire présentée le 9 septembre 2005, l'EARL de Gainnemare n'a pas renseigné la case relative au nom et à l'adresse du propriétaire du terrain, devant être remplie lorsque celui-ci n'est pas le demandeur, et s'est ainsi présentée comme étant elle-même propriétaire du terrain d'assiette de la construction litigieuse ; que si elle se prévaut de l'accord qui lui aurait été donné par le propriétaire, M. Pierre B, le 25 novembre 2002, ce courrier, qui avait été rédigé près de trois ans avant la demande de permis de construire et évoquait l'édification d'un bâtiment et non l'extension d'une stabulation existante, alors qu'aucun accord n'avait été donné avant la délivrance du permis de construire cette stabulation le 26 août 2001, ne peut être regardé comme habilitant l'EARL Gainnemare à édifier sur le terrain appartenant à M. B l'extension objet du permis de construire du 13 novembre 2005 ; que, par suite, ce permis a été délivré en méconnaissance des dispositions précitées de l'article R. 421-1-1 du code de l'urbanisme alors applicables ; qu'il suit de là que le jugement du tribunal administratif de Rouen du 20 mars 2008, ayant rejeté la requête de M. Pierre B tendant à l'annulation pour excès de pouvoir de l'arrêté du 13 novembre 2005 délivrant un permis de construire à l'EARL de Gainnemare doit être annulé, de même que ce permis de construire ;<br/>
<br/>
              10. Considérant que, pour l'application de l'article L. 600-4-1 du code de l'urbanisme, le moyen tiré de ce que la délivrance du permis en litige par le maire de la commune de Croixmare serait entachée de détournement de pouvoir n'est pas de nature à fonder l'annulation du permis qu'il attaque ;<br/>
<br/>
              Sur les conclusions tendant à l'application de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              11. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'EARL de Gainnemare une somme de 2 000 euros à verser à M. B, au titre des dispositions de l'article L. 761-1 du code de justice administrative ; qu'en revanche, ces dispositions font obstacle à ce que qu'une somme soit mise à ce titre à la charge de M. B, qui n'est pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Douai du 22 janvier 2009 est annulé en tant qu'il a rejeté la requête de M. Jean-Pierre B tendant à l'annulation du jugement du 20 mars 2008 du tribunal administratif de Rouen ayant rejeté la requête de M. Pierre B en annulation du permis de construire délivré le 13 novembre 2005 par le maire de Croixmare.<br/>
Article 2 : Le jugement n° 0600850 du tribunal administratif de Rouen du 20 mars 2008 est annulé.<br/>
Article 3 : Le permis de construire délivré le 13 novembre 2005 à l'EARL de Gainnemare par le maire de Croixmare est annulé.<br/>
Article 4 : L'EARL de Gainnemare versera une somme de 2 000 euros à M. Jean-Pierre B au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 5 : Le surplus des conclusions de M. Jean-Pierre B et les conclusions de l'EARL de Gainnemare et de la commune de Croixmare  présentées sur le fondement de l'article L. 761-1 du code de justice administrative sont rejetés.<br/>
Article 6 : La présente décision sera notifiée à M. Jean-Pierre B, à l'EARL de Gainnemare et à la commune de Croixmare.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
