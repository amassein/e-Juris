<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033358043</ID>
<ANCIEN_ID>JG_L_2016_10_000000403702</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/35/80/CETATEXT000033358043.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 25/10/2016, 403702, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-10-25</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>403702</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FOUSSARD, FROGER</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2016:403702.20161025</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés les 22 septembre et 14 octobre 2016 au secrétariat du contentieux du Conseil d'Etat, l'Instance de gestion du régime local d'assurance maladie d'Alsace et de Moselle demande au juge des référés du Conseil d'Etat d'ordonner, sur le fondement de l'article L. 521-1 du code de justice administrative, la suspension de l'exécution de la décision dénommée "Fiche d'information" émise par la Caisse nationale d'assurance maladie des travailleurs salariés (CNAMTS) relative aux séjours bénéficiaires du régime local Alsace Moselle en établissements médecine, chirurgie, obstétrique (MCO) publics en date du 13 juillet 2016.<br/>
<br/>
              Elle soutient que : <br/>
              - la requête est recevable dès lors que la décision contestée, dont elle a parallèlement demandé l'annulation pour excès de pouvoir, fait grief et n'a pas encore produit tous ses effets ; <br/>
              - la condition d'urgence est remplie dès lors que la décision litigieuse préjudicie de manière grave et immédiate à sa situation en raison de ses répercussions financières imminentes ; <br/>
              - il existe un doute sérieux sur la légalité de la décision contestée en ce qu'elle est entachée d'incompétence ; elle ne repose sur aucun fondement juridique ; elle instaure à la charge du régime local un nouveau financement de l'activité MCO au bénéfice des établissements publics de santé qui fait double emploi avec une partie des sommes qu'elle acquitte dans le cadre de la dotation globale annuelle ; elle repose sur une interprétation erronée des dispositions du code de la sécurité sociale ; enfin et à titre subsidiaire, elle est entachée d'illégalité en fixant une entrée en vigueur de la mesure rétroactive du 1er juillet 2016.  <br/>
<br/>
              Par un mémoire en défense, enregistré le 12 octobre 2016, la Caisse nationale d'assurance maladie des travailleurs salariés (CNAMTS) conclut, à titre principal, au rejet de la requête. A titre subsidiaire, elle demande au juge des référés du Conseil d'Etat d'ordonner la suspension de l'exécution de la fiche d'information attaquée uniquement en tant qu'elle a prévu une prise d'effet avant la date de sa modification, le 13 juillet 2016 ou, plus subsidiairement, le  22 juillet 2016, date de sa publication. Enfin, elle demande que soit mise à la charge de l'Instance régionale de gestion du régime local d'Alsace-Moselle une somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative. Elle soutient que la condition d'urgence n'est pas remplie et que les moyens soulevés ne sont pas de nature à faire naître un doute sérieux sur la légalité de la décision.<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, l'Instance de gestion du régime local d'assurance maladie d'Alsace et de Moselle, d'autre part, la CNAMTS ; <br/>
<br/>
              Vu le procès-verbal de l'audience publique du 17 octobre 2016 à 10  heures  au cours de laquelle ont été entendus : <br/>
<br/>
              - les représentants de l'Instance de gestion du régime local d'assurance maladie d'Alsace et de Moselle ;<br/>
<br/>
              - Me Froger, avocat au Conseil d'Etat et à la Cour de cassation, avocat de la CNAMTS ;<br/>
              - les représentants de la CNAMTS ;<br/>
<br/>
              et à l'issue de laquelle le juge des référés a différé la clôture de l'instruction au 19 octobre 2016 à 12 heures et communiqué la procédure à la ministre des affaires sociales et de la santé. <br/>
<br/>
              La ministre des affaires sociales et de la santé a présenté des observations, enregistrées le 18 octobre 2016, dans lesquelles elle conclut au rejet de la requête. <br/>
<br/>
              Par deux nouveaux mémoires enregistrés les 18 et 19 octobre 2016, l'Instance de gestion du régime local d'assurance maladie d'Alsace et de Moselle maintient les conclusions de sa demande tendant à ce que soit prononcée la suspension de la fiche d'information litigieuse. Elle fait valoir que la dotation globale versée mensuellement vise directement le " champ d'activité MCO " pour des avances de trésorerie T2A et participe donc bien doublement au financement du ticket modérateur des séjours MCO.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 24 octobre 2016, produite par la ministre des affaires sociales et de la santé ;<br/>
<br/>
              Vu : <br/>
              - le code de la sécurité sociale ;<br/>
     - le code de justice administrative ;<br/>
<br/>
<br/>1. Aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision ". Il résulte de ces dispositions que le prononcé de la suspension d'un acte administratif est subordonné notamment à une condition d'urgence. L'urgence justifie la suspension de l'exécution d'un acte administratif lorsque celui-ci porte atteinte de manière suffisamment grave et immédiate, à un intérêt public, à la situation du requérant ou aux intérêts qu'il entend défendre. Il appartient au juge des référés d'apprécier concrètement, compte tenu des justifications fournies par le requérant, si les effets de l'acte contesté sont de nature à caractériser une urgence justifiant que, sans attendre le jugement de la requête au fond, l'exécution de la décision soit suspendue. L'urgence doit être appréciée objectivement et compte tenu de l'ensemble des circonstances de l'affaire.<br/>
              2. L'Instance de gestion du régime local d'assurance maladie d'Alsace et de Moselle (ci-après le régime local) gère, en application de l'article L. 325-1 du code de la sécurité sociale, un régime d'assurance maladie complémentaire et obligatoire en Alsace-Moselle. A ce titre, il prend notamment en charge, au titre des frais d'hospitalisation, le forfait journalier hospitalier et, lorsque l'assuré social n'en est pas exonéré, le ticket modérateur. Une fiche d'information du 13 juillet 2016 du directeur de la Caisse nationale d'assurance maladie des travailleurs salariés (CNAMTS), intitulée " Séjours bénéficiaires du régime local Alsace Moselle " et adressée à toutes les caisses locales leur demande de modifier le calcul des frais à mettre à la charge de ce régime local au titre de la prise en charge en matière d'hospitalisation médecine, chirurgie, obstétrique (MCO) des patients affiliés au régime local Alsace-Moselle. Jusqu'à la date d'effet de cette fiche, l'ensemble des frais mis à la charge du régime local au titre des séjours en hospitalisation MCO des patients assurés à ce régime local étaient calculés sur la base de 100% des tarifs des " groupes homogènes de séjour " (GHS).  La fiche en litige prévoit que pour les séjours se terminant à compter du 1er juillet 2016, le calcul doit s'aligner sur le droit commun des assurés sociaux nationaux. Elle précise que ce calcul doit s'effectuer, pour la prise en charge de la part incombant à l'assurance maladie obligatoire, soit 80% de ces frais de séjour, sur la base des tarifs GHS et, pour la part incombant à l'assurance maladie complémentaire, soit 20% de ces frais, sur la base des tarifs journaliers de prestations (TJP). Cette mesure de tarification au flux concerne les établissements de santé publics et établissements de santé privés à but non lucratif visés aux a, b et c de l'article L. 162-22-6 du code de la sécurité sociale. <br/>
              3. Pour soutenir qu'il y a urgence à suspendre l'exécution de cette fiche d'information, la requérante fait valoir que, d'une part, la modification qu'elle apporte au calcul de la prise en charge qui incombe au régime local au titre du ticket modérateur préjudicie de manière grave et immédiate à sa situation en ce qu'elle menace son équilibre financier à court terme. Elle soutient, d'autre part, que la fiche d'information litigieuse instaurerait un double financement du ticket modérateur pour le même séjour MCO pour le même patient dès lors que la dotation globale annuelle due par le régime local au titre de l'article L. 174-1 du code de la sécurité sociale participerait d'ores et déjà au remboursement du ticket modérateur des séjours MCO pour les patients affiliés à son régime. <br/>
<br/>
              4. D'une part, la circonstance invoquée par la requérante selon laquelle la dotation globale annuelle prévue à l'article L. 174-1 du code de la sécurité sociale, que le régime local acquitte en sa qualité de régime obligatoire d'assurance maladie, ferait en partie double emploi avec la prise en charge du ticket modérateur dans les conditions financières désormais prévue par la fiche d'information en litige n'est pas de nature à caractériser l'existence d'une situation d'urgence. <br/>
<br/>
              5. D'autre part, il ressort des pièces du dossier ainsi que des éclaircissements recueillis à l'audience, que l'Instance de gestion du régime local évalue à 40,6 millions d'euros l'accroissement des dépenses à sa charge par l'effet de cette fiche pour la seule année 2016, soit moins de 10% des recettes prévues pour l'exercice 2016. Seule cette somme peut être prise en compte, à l'exclusion de l'impact d'actions contentieuses engagées par des établissements de santé contre le régime local, dès lors qu'il ne ressort pas des pièces du dossier que ces demandes contentieuses auraient été présentées sur le fondement de la fiche d'information en litige. Si la requérante fait valoir que cet accroissement de ses charges va se traduire par une dégradation du même montant de son résultat d'exploitation portant ainsi le déficit d'exploitation prévisible pour 2016 de 37,6 millions à un peu plus de 78 millions d'euros, il ressort également des pièces du dossier qu'elle dispose de réserves, dont la valorisation initiale s'élève à 198 millions d'euros et qui ont fait l'objet de placements assortis d'une garantie en capital sous réserve de respecter les dates de fin de période de chaque placement qui interviendront selon les placements en 2017, 2018, 2019, 2021 et 2024. Si la requérante fait valoir que la cession d'une partie de ces valeurs avant la date prévue de leur échéance risque de lui faire perdre cette garantie en capital, elle ne soutient pas que cette cession placerait le niveau restant de ces réserves en deçà d'un niveau légalement exigé. Dans ces conditions, l'accroissement invoqué de son déficit d'exploitation et le risque de ne pas bénéficier de la garantie en capital pour la partie des réserves placées en valeur qui devront être cédées avant leur terme afin de couvrir l'accroissement du déficit ne suffisent pas à établir l'existence, en l'espèce, d'une situation d'urgence justifiant la suspension des dispositions contestées. <br/>
<br/>
              6. Il résulte de tout ce qui précède que la condition d'urgence n'est pas remplie. Par suite, sans qu'il soit besoin de statuer sur les moyens de légalité soulevés par l'Instance de gestion du régime local d'assurance maladie d'Alsace et de Moselle, sa requête doit être rejetée.<br/>
<br/>
              7. Il n'y a pas lieu, dans les circonstances de l'espèce, de mettre à la charge du régime local le versement d'une somme à la CNAMTS au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de l'Instance de gestion du régime local d'assurance maladie d'Alsace et de Moselle est rejetée.<br/>
Article 2 : Les conclusions de la CNAMTS au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
Article 3 : La présente ordonnance sera notifiée à l'Instance de gestion du régime local d'assurance maladie d'Alsace et de Moselle et à la Caisse nationale d'assurance maladie des travailleurs salariés.<br/>
Copie en sera adressée à la ministre des affaires sociales et de la santé.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
