<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044806226</ID>
<ANCIEN_ID>JG_L_2021_12_000000449913</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/80/62/CETATEXT000044806226.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère - 4ème chambres réunies, 30/12/2021, 449913, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-12-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>449913</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère - 4ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Agnès Pic</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Marie Sirinelli</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:449913.20211230</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu les procédures suivantes :<br/>
<br/>
              1° Sous le n° 449913, par une requête et un mémoire en réplique, enregistrés les 18 février et 7 juin 2021 au secrétariat du contentieux du Conseil d'Etat, la Fédération nationale Sud santé sociaux demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler pour excès de pouvoir l'instruction DGOS/RH3/2021/5 du 6 janvier 2021 du ministre de la santé et des solidarités relative à la reconnaissance des pathologies liées à une infection au SARS-CoV2 dans la fonction publique hospitalière ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              2° Sous le n° 450447, par une requête, enregistrée le 8 mars 2021 au secrétariat du contentieux du Conseil d'Etat, la Fédération des personnels des services publics et des services de santé Force Ouvrière et M. E... C... demandent au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler pour excès de pouvoir l'instruction DGOS/RH3/2021/5 du 6 janvier 2021 du ministre de la santé et des solidarités relative à la reconnaissance des pathologies liées à une infection au SARS-CoV2 dans la fonction publique hospitalière ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
<br/>
              Vu les autres pièces des dossiers ; <br/>
<br/>
              Vu : <br/>
              - le code de la santé publique ;<br/>
              - le code de la sécurité sociale ; <br/>
              - la loi n° 83-634 du 13 juillet 1983 ; <br/>
              - le décret n° 86-442 du 14 mars 1986 ; <br/>
              - le décret n° 2005-850 du 27 juillet 2005 ;<br/>
              - le décret n° 2013-727 du 12 août 2013 ;<br/>
              - le décret n° 2020-1131 du 14 septembre 2020 ; <br/>
              - l'arrêté du 4 août 2004 relatif à la reconnaissance en maladies professionnelles des pathologies liées à une infection au SARS-CoV2 ; <br/>
              - le code de justice administrative ; <br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Agnès Pic, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Marie Sirinelli, rapporteure publique ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              Sur le cadre juridique :<br/>
<br/>
              1. D'une part, aux termes de l'article 21 bis de la loi n° 83-634 du 13 juillet 1983 portant droits et obligations des fonctionnaires : " (...) II.- Est présumé imputable au service tout accident survenu à un fonctionnaire, quelle qu'en soit la cause, dans le temps et le lieu du service, dans l'exercice ou à l'occasion de l'exercice par le fonctionnaire de ses fonctions ou d'une activité qui en constitue le prolongement normal, en l'absence de faute personnelle ou de toute autre circonstance particulière détachant l'accident du service. / IV.- Est présumée imputable au service toute maladie désignée par les tableaux de maladies professionnelles mentionnés aux articles L. 461-1 et suivants du code de la sécurité sociale et contractée dans l'exercice ou à l'occasion de l'exercice par le fonctionnaire de ses fonctions dans les conditions mentionnées à ce tableau. / Si une ou plusieurs conditions tenant au délai de prise en charge, à la durée d'exposition ou à la liste limitative des travaux ne sont pas remplies, la maladie telle qu'elle est désignée par un tableau peut être reconnue imputable au service lorsque le fonctionnaire ou ses ayants droit établissent qu'elle est directement causée par l'exercice des fonctions. / Peut également être reconnue imputable au service une maladie non désignée dans les tableaux de maladies professionnelles mentionnés aux articles L. 461-1 et suivants du code de la sécurité sociale lorsque le fonctionnaire ou ses ayants droit établissent qu'elle est essentiellement et directement causée par l'exercice des fonctions et qu'elle entraîne une incapacité permanente à un taux déterminé et évalué dans les conditions prévues par décret en Conseil d'Etat. (...) ". Le deuxième alinéa du II de l'article 47-3 du décret du 14 mars 1986 relatif à la désignation des médecins agréés, à l'organisation des comités médicaux et des commissions de réforme, aux conditions d'aptitude physique pour l'admission aux emplois publics et au régime de congés de maladie des fonctionnaires précise que : " Lorsque des modifications et adjonctions sont apportées aux tableaux de maladies professionnelles mentionnées aux articles L. 461-1 et suivants du code de la sécurité sociale après qu'il a été médicalement constaté qu'un fonctionnaire est atteint d'une maladie inscrite à ces tableaux, la déclaration est adressée par l'agent à l'administration dans le délai de deux ans à compter de la date d'entrée en vigueur de ces modifications ou adjonctions. (...) ". L'article 47-6 de ce décret prévoit que : " La commission de réforme est consultée : / (...) 3° Lorsque l'affection résulte d'une maladie contractée en service telle que définie au IV de l'article 21 bis de la loi du 13 juillet 1983 précitée dans les cas où les conditions prévues au premier alinéa du même IV ne sont pas remplies. "<br/>
<br/>
              2. D'autre part, aux termes de l'article L. 461-1 du code de la sécurité sociale : " (...) Est présumée d'origine professionnelle toute maladie désignée dans un tableau de maladies professionnelles et contractée dans les conditions mentionnées à ce tableau. / Si une ou plusieurs conditions tenant au délai de prise en charge, à la durée d'exposition ou à la liste limitative des travaux ne sont pas remplies, la maladie telle qu'elle est désignée dans un tableau de maladies professionnelles peut être reconnue d'origine professionnelle lorsqu'il est établi qu'elle est directement causée par le travail habituel de la victime. / Peut être également reconnue d'origine professionnelle une maladie caractérisée non désignée dans un tableau de maladies professionnelles lorsqu'il est établi qu'elle est essentiellement et directement causée par le travail habituel de la victime et qu'elle entraîne le décès de celle-ci ou une incapacité permanente d'un taux évalué dans les conditions mentionnées à l'article L. 434-2 et au moins égal à un pourcentage déterminé. / Dans les cas mentionnés aux deux alinéas précédents, la caisse primaire reconnaît l'origine professionnelle de la maladie après avis motivé d'un comité régional de reconnaissance des maladies professionnelles. La composition, le fonctionnement et le ressort territorial de ce comité ainsi que les éléments du dossier au vu duquel il rend son avis sont fixés par décret. L'avis du comité s'impose à la caisse dans les mêmes conditions que celles fixées à l'article L. 315-1 ". En vertu de l'article L. 461-2 du même code : " Des tableaux annexés aux décrets énumèrent les manifestations morbides d'intoxications aiguës ou chroniques présentées par les travailleurs exposés d'une façon habituelle à l'action des agents nocifs mentionnés par lesdits tableaux, qui donnent, à titre indicatif, la liste des principaux travaux comportant la manipulation ou l'emploi de ces agents. Ces manifestations morbides sont présumées d'origine professionnelle. / Des tableaux spéciaux énumèrent les infections microbiennes mentionnées qui sont présumées avoir une origine professionnelle lorsque les victimes ont été occupées d'une façon habituelle aux travaux limitativement énumérés par ces tableaux (...) Les tableaux mentionnés aux alinéas précédents peuvent être révisés et complétés par des décrets, après avis du Conseil d'orientation des conditions de travail ". Le 3° de l'article L. 221-3-1 du même code prévoit que le directeur général de la Caisse nationale de l'assurance maladie est chargé, pour ce qui concerne la gestion de la caisse nationale et du réseau des caisses régionales, locales et de leur groupements, de prendre les mesures nécessaires à l'organisation et au pilotage du réseau des caisses du régime général et qu'il peut notamment confier à certains organismes, à l'échelon national, interrégional, régional ou départemental, la charge d'assumer certaines missions.<br/>
<br/>
              3. Le décret du 14 septembre 2020 a notamment complété les tableaux des maladies professionnelles annexés au livre IV du code de la sécurité sociale, pour y insérer un tableau n° 100 intitulé " Affections respiratoires aiguës liées à une infection au SARS-COV2 " et a prévu, par dérogation à l'article D. 461-26, aux six premiers alinéas de l'article D. 461-27 et à l'article D. 461-28 du code de la sécurité sociale, que le directeur général de la Caisse nationale de l'assurance maladie peut, en application du 3° de l'article L. 221-3-1 du code de la sécurité sociale, confier à un comité régional de reconnaissance des maladies professionnelles l'instruction de l'ensemble des demandes de reconnaissance de maladie professionnelle liées à une contamination au SARS-CoV2. <br/>
<br/>
              4. L'instruction du 6 janvier 2021, prise par le ministre des solidarités et de la santé, précise les modalités d'examen des demandes des fonctionnaires de la fonction publique hospitalière pour la reconnaissance de maladies professionnelles liées à une infection au SARS-CoV2, en rappelant notamment que, pour les fonctionnaires, conformément aux dispositions de l'article 21 bis de la loi du 13 juillet 1983 et du titre VI bis du décret du 14 mars 1986, la reconnaissance en maladie professionnelle des pathologies liées à une infection au SARS-CoV2 doit se faire par référence au tableau n° 100 créé par le décret du 14 septembre 2020, en recommandant aux commissions de réforme, auxquelles sont soumises pour avis les pathologies ne satisfaisant pas à l'ensemble des conditions de ce tableau et les pathologie qui n'y sont pas inscrites, d'appliquer la doctrine du comité régional de reconnaissance des maladies professionnelles unique, dans le respect des dispositions réglementaires en vigueur, et en désignant la commission de réforme de l'Assistance publique - Hôpitaux de Paris (AP-HP) en tant que référente nationale, pouvant échanger au besoin avec le secrétariat du comité régional de reconnaissance des maladies professionnelles unique. La Fédération nationale Sud santé sociaux, d'une part, et la Fédération des personnels des services publics et des services de santé Force Ouvrière (FO SPS) et autre, d'autres part, demandent l'annulation pour excès de pouvoir de cette instruction, par des requêtes qu'il y a lieu de joindre.<br/>
<br/>
              Sur la légalité externe de l'instruction attaquée : <br/>
<br/>
              5. L'instruction en cause a été signée par M. H... I..., nommé secrétaire général des ministères chargés des affaires sociales par décret du 7 octobre 2020, publié au Journal officiel de la République française le 8 octobre 2020 et chargé, en vertu du I de l'article 2 du décret du 12 août 2013 portant création, organisation et attributions d'un secrétariat général des ministères chargés des affaires sociales, d'animer et coordonner l'action de l'ensemble des directions et services des ministères et de participer au pilotage des établissements qui en relèvent, ainsi que d'élaborer, en concertation avec les directions et services, les principes généraux de gestion des ressources humaines, ainsi que par Mme D... K..., nommée directrice générale de l'offre de soins par décret du 24 juillet 2019 publié au Journal officiel de la République française du 26 juillet 2019, compétente en vertu du 2° de l'article D. 1421-2 du code de la santé publique " pour toute question relative à la détermination et à l'emploi des ressources nécessaires à l'offre de soins, notamment en matière de ressources humaines ". L'un comme l'autre disposait, à compter du jour suivant la publication au Journal officiel de la République française de l'acte les nommant dans leurs fonctions, en application du 1° de l'article 1er décret du 27 juillet 2005 relatif aux délégations de signature des membres du gouvernement, de la compétence pour signer, au nom du ministre chargé de la santé, l'instruction attaquée, qui relevait des affaires des services placés sous leur autorité et non, comme les requérants le soutiennent, de la ministre de la transformation et de la fonction publiques, y compris en ce qu'elle permet aux commissions de réforme départementales qui le souhaitent de saisir la commission de réforme de l'AP-HP qu'elle institue comme référente au niveau national, pour obtenir des informations techniques destinées à leur permettre de rendre leur avis sur le caractère professionnel d'une maladie liée à une infection au SARS-CoV2. Par suite, les requérants ne sont pas fondés à soutenir qu'elle serait entachée d'incompétence.<br/>
<br/>
              Sur la légalité interne de l'instruction attaquée : <br/>
<br/>
              6. En premier lieu, par une décision n° 444500, 446453, 446455, 446459, 446462, du même jour, le Conseil d'Etat, statuant au contentieux, a rejeté les recours dirigées contre le décret du 14 septembre 2020. Par suite, le moyen tiré de ce que les dispositions de l'instruction seraient illégales par voie de conséquence de l'illégalité qui entacherait les dispositions du décret du 14 septembre 2020 qu'elle réitère pour une part, notamment tel qu'il était contesté sous le n° 446459, ne peut qu'être écarté. <br/>
<br/>
              7. En deuxième lieu, l'instruction attaquée, après avoir relevé que la contamination au SARS-CoV2 dans un contexte de circulation active du virus sur l'ensemble du territoire, ne peut être isolée avec certitude ni datée avec précision,  indique, en son point 3, que dans l'hypothèse où des agents auraient déposé des demandes de reconnaissance d'imputabilité au service de pathologies liées à ce virus au titre d'accidents de service, il convient de ne pas remettre en cause les décisions créatrices de droit lorsqu'il a déjà été statué sur la demande et que, pour les demandes sur lesquelles il n'a pas encore été statué, l'administration doit informer les agents que leur demande sera traitée au titre de la maladie professionnelle. Cette préconisation est destinée à orienter les demandes relatives à la reconnaissance des pathologies liées à une infection au SARS-CoV2 vers la procédure apparaissant la plus favorable aux demandeurs et la plus susceptible de permettre à l'imputabilité au service d'être reconnue, sans exclure la faculté pour ces derniers de maintenir, s'ils le souhaitent, une demande tendant à obtenir une déclaration d'accident de service. Par suite, les requérants ne sont pas fondés à soutenir que le ministre aurait méconnu la présomption d'imputabilité prévue par l'article 21 bis de la loi du 13 juillet 1983.<br/>
<br/>
              8. En troisième lieu, le point 2 de l'instruction indique qu'" au regard du nombre conséquent de commissions de réforme susceptibles de rendre des avis et afin de favoriser une appréciation homogène sur l'ensemble du territoire de la situation des fonctionnaires demandant la reconnaissance de l'imputabilité au service d'une pathologie liée à une infection au SARS-CoV2, il a été institué une commission référente au niveau national : la commission de réforme de l'AP-HP ". Il précise que les commissions de réforme départementales instituées par l'arrêté du 4 août 2004 relatif aux commissions de réforme des agents de la fonction publique territoriale et de la fonction publique hospitalière " restent compétentes pour tous les agents affectés ou mis à disposition dans un département " et que la commission de réforme de l'AP-HP peut être saisie par les commissions de réforme départementales " en cas de besoin de précisions pour les dossiers les plus complexes, sur le lien entre la maladie et l'infection covid-19 ou pour tout point d'éclairage complémentaire ". Ces dispositions n'ont ni pour objet ni pour effet de confier à la commission de réforme de l'AP-HP l'instruction des demandes de reconnaissance d'imputabilité au service des affections respiratoires liées à une infection au SARS-CoV2. Les requérants ne sont donc pas fondés à soutenir que l'instruction attaquée aurait, par ces dispositions, méconnu la compétence des commissions de réforme départementales et porté atteinte à leur indépendance. La Fédération nationale Sud santé sociaux n'est pas davantage fondée à soutenir que l'institution d'une commission référente résulterait d'une erreur manifeste d'appréciation, notamment en ce qu'elle serait de nature à compromettre l'examen des demandes dans des délais adéquats. <br/>
<br/>
              9. Il résulte de tout ce qui précède que les conclusions de la Fédération nationale Sud santé sociaux et de la Fédération des personnels des services publics et des services de santé Force Ouvrière et autre, y compris celles présentées au titre de l'article L. 761-1 du code de justice administrative, doivent être rejetées. <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Les requêtes de la Fédération nationale Sud santé sociaux et de la Fédération des personnels des services publics et des services de santé Force Ouvrière et autre sont rejetées. <br/>
Article 2 : La présente décision sera notifiée à la Fédération nationale Sud santé sociaux, à la Fédération des personnels des services publics et des services de santé Force Ouvrière, première dénommée, pour les deux requérants sous le n° 450447 et au ministre des solidarités et de la santé.<br/>
              Délibéré à l'issue de la séance du 15 décembre 2021 où siégeaient : M. Rémy Schwartz, président adjoint de la Section du Contentieux, présidant ; Mme A... O..., Mme F... N..., présidentes de chambre ; M. B... M..., Mme J... L..., M. Damien Botteghi, conseillers d'Etat et Mme Agnès Pic, maître des requêtes en service extraordinaire-rapporteure. <br/>
<br/>
              Rendu le 30 décembre 2021.<br/>
                 Le président : <br/>
                 Signé : M. Rémy Schwartz<br/>
 		La rapporteure: <br/>
      Signé : Mme Agnès Pic<br/>
                 La secrétaire :<br/>
                 Signé : Mme P... G...<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
