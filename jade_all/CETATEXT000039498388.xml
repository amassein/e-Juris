<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039498388</ID>
<ANCIEN_ID>JG_L_2019_12_000000422857</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/49/83/CETATEXT000039498388.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème - 7ème chambres réunies, 11/12/2019, 422857, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-12-11</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>422857</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème - 7ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP SPINOSI, SUREAU</AVOCATS>
<RAPPORTEUR>Mme Sophie-Caroline de Margerie</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Sophie Roussel</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:422857.20191211</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête sommaire, un mémoire complémentaire et deux nouveaux mémoires, enregistrés les 1er août et 2 novembre 2018 et 23 octobre et 24 novembre 2019 au secrétariat du contentieux du Conseil d'Etat, l'association La Cimade, l'association pour la reconnaissance des droits des personnes homosexuelles et transsexuelles à l'immigration et au séjour (ARDHIS), l'association Dom'Asile, la Fédération des acteurs de la Solidarité (FAS), la Fédération des associations de solidarité avec tou-te-s les immigré-e-s (FASTI), l'association Groupe Accueil et Solidarité (GAS), le Groupe d'information et de soutien des immigré.e.s (GISTI), le Service Jésuite des Réfugiés (JRS), le Réseau Accueil Immigrés à Lille (RAIL), et le Secours catholique-Caritas France demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret n° 2018-426 du 31 mai 2018 portant diverses dispositions relatives à l'allocation pour demandeur d'asile ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat le versement d'une somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la directive n° 2013/33/UE du Parlement européen et du Conseil du 26 juin 2013 ;<br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              - le code de l'action sociale et des familles ;<br/>
              - la loi n° 2015-925 du 29 juillet 2015 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Sophie-Caroline de Margerie, conseiller d'Etat, <br/>
<br/>
              - les conclusions de Mme Sophie Roussel, rapporteur public,<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Spinosi, Sureau, avocat de La Cimade et autres ; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 744-9 du code de l'entrée et du séjour des étrangers et du droit d'asile, créé par l'article 23 de la loi du 29 juillet 2015 relative à la réforme du droit d'asile : " Le demandeur d'asile qui a accepté les conditions matérielles d'accueil proposées en application de l'article L. 744-1 bénéficie d'une allocation pour demandeur d'asile s'il satisfait à des conditions d'âge et de ressources. L'Office français de l'immigration et de l'intégration ordonne son versement dans l'attente de la décision définitive lui accordant ou lui refusant une protection au titre de l'asile ou jusqu'à son transfert effectif vers un autre Etat responsable de l'examen de sa demande d'asile (...) / Un décret définit le barème de l'allocation pour demandeur d'asile, en prenant en compte les ressources de l'intéressé, son mode d'hébergement et, le cas échéant, les prestations offertes par son lieu d'hébergement. Le barème de l'allocation pour demandeur d'asile prend en compte le nombre d'adultes et d'enfants composant la famille du demandeur d'asile et accompagnant celui-ci. / Ce décret précise, en outre, les modalités de versement de l'allocation pour demandeur d'asile. / Ce décret peut prévoir une adaptation du montant de l'allocation pour demandeur d'asile et de ses modalités d'attribution, de calcul et de versement pour tenir compte de la situation particulière des départements et collectivités d'outre-mer. ". Dans sa rédaction issue du décret contesté, l'article D. 744-26 du même code prévoit que " l'allocation pour demandeur d'asile est composée d'un montant forfaitaire, dont le niveau varie en fonction du nombre de personnes composant le foyer, et, le cas échéant, d'un montant additionnel destiné à couvrir les frais d'hébergement ou de logement du demandeur. Le montant additionnel n'est pas versé au demandeur qui n'a pas manifesté de besoin d'hébergement ou qui a accès gratuitement à un hébergement ou un logement à quelque titre que ce soit. Lorsqu'il n'est pas hébergé dans un des lieux mentionnés à l'article L. 744-3, le demandeur d'asile informe l'Office français de l'immigration et de l'intégration de son lieu d'hébergement ou de logement ainsi que des modalités s'y rapportant. Le demandeur d'asile communique ces informations à l'Office français de l'immigration et de l'intégration deux mois après l'enregistrement de sa demande d'asile et ensuite tous les six mois. (...) / Le barème de l'allocation pour demandeur d'asile figure à l'annexe 7-1 du présent code. ". <br/>
<br/>
              2. Dans sa version issue du décret du 21 octobre 2015, l'annexe 7-1 du code de l'entrée et du séjour des étrangers et du droit d'asile prévoyait qu'" un montant journalier additionnel de 4,20 &#128; est versé à chaque demandeur d'asile adulte ayant accepté l'offre de prise en charge, auquel aucune place d'hébergement ne peut être proposée dans un des lieux mentionnés à l'article L. 744-3 et qui n'est pas hébergé en application des dispositions de l'article L. 345-2-2 du code de l'action sociale et des familles ". Le Conseil d'Etat, statuant au contentieux a, par une décision du 23 décembre 2016, annulé ces dispositions en tant qu'elles ne fixaient pas un montant journalier additionnel suffisant pour permettre aux demandeurs d'asile adultes ayant accepté une offre de prise en charge et auxquels aucune place d'hébergement ne peut être proposée de disposer d'un logement sur le marché privé de la location. A la suite de cette décision, par un décret du 29 mars 2017 portant diverses dispositions relatives à l'allocation pour demandeur d'asile, le Premier ministre a augmenté le montant additionnel de l'allocation qui est passé de 4,20 à 5,40 euros par jour et par personne adulte. Par une décision du 17 janvier 2018, le Conseil d'Etat, statuant au contentieux a annulé ces dispositions au même motif.<br/>
<br/>
              3. A la suite de cette deuxième annulation contentieuse, le Premier ministre a, par un décret du 31 mai 2018, fixé à 7,40 euros le montant additionnel de l'allocation et prévu qu'il ne serait pas versé au demandeur d'asile qui n'a pas manifesté de besoin d'hébergement ou qui a accès gratuitement à un hébergement ou un logement. L'association La Cimade, l'association pour la reconnaissance des droits des personnes homosexuelles et transsexuelles à l'immigration et au séjour (ARDHIS), l'association Dom'Asile, la Fédération des acteurs de la Solidarité (FAS), la Fédération des associations de solidarité avec tou-te-s les immigré-e-s (FASTI), l'association Groupe Accueil et Solidarité (GAS), le Groupe d'information et de soutien des immigré.e.s (GISTI), le Service Jésuite des Réfugiés (JRS), le Réseau Accueil Immigrés à Lille (RAIL), et le Secours catholique-Caritas France demandent l'annulation pour excès de pouvoir de ce décret.<br/>
<br/>
              Sur la recevabilité des conclusions :<br/>
<br/>
              4. L'article 3 du décret attaqué ne modifie pas le II de l'annexe 7-1 mentionnée à l'article D. 744-26 du code de l'entrée et du séjour des étrangers et du droit d'asile relatif aux montants journaliers de l'allocation pour demandeur d'asile versés en Guyane et à Saint-Martin, dans sa rédaction issue du 3° de l'article 6 du décret du 29 mars 2017, qui n'a pas été annulé par la décision du 17 janvier 2018 du Conseil d'Etat, statuant au contentieux mentionnée au point 2. Dès lors, le ministre de l'intérieur est fondé à soutenir que les conclusions dirigées contre ces dispositions confirmatives, qui sont divisibles des autres dispositions attaquées, sont tardives et, par suite, irrecevables. <br/>
<br/>
              Sur la légalité externe du décret attaqué : <br/>
<br/>
              5. Si les associations requérantes soutiennent que le décret publié  diffère  du projet soumis pour avis au conseil territorial de Saint-Martin, un tel moyen ne peut qu'être écarté, la consultation ayant précisément pour objet de recueillir les observations de l'organisme consulté et pouvant, dès lors, avoir pour effet de les prendre en compte dans le texte finalement retenu, à la condition que ce dernier ne soulève aucune question nouvelle par rapport à la version soumise pour avis à l'organisme consulté, ce qui a été le cas en l'espèce. Par suite, les associations requérantes ne sont pas fondées à soutenir que le décret contesté a été pris à l'issue d'une procédure irrégulière faute de consultation régulière du conseil territorial de Saint-Martin. <br/>
<br/>
              Sur la légalité interne du décret attaqué :<br/>
<br/>
              6. En premier lieu, l'article 2 g) de la directive de la directive 2013/33/UE du Parlement européen et du Conseil du 26 juin 2013 établissant des normes pour l'accueil des personnes demandant la protection internationale définit les " conditions matérielles d'accueil " comme " les conditions d'accueil comprenant le logement, la nourriture et l'habillement, fournis en nature ou sous forme d'allocation financière ou de bons, ou en combinant ces trois formules, ainsi qu'une allocation journalière ". Aux termes de l'article 17 de la même directive : " 1. Les Etats membres font en sorte que les demandeurs aient accès aux conditions matérielles d'accueil lorsqu'ils présentent leur demande de protection internationale. (...) / 3. Les Etats membres peuvent subordonner l'octroi de tout ou partie des conditions matérielles d'accueil et des soins de santé à la condition que les demandeurs ne disposent pas de moyens suffisants pour avoir un niveau de vie adapté à leur santé et pour pouvoir assurer leur subsistance. / 4. Les Etats membres peuvent exiger des demandeurs qu'ils couvrent le coût des conditions matérielles d'accueil et des soins de santé prévus dans la présente directive, ou qu'ils y contribuent, conformément au paragraphe 3, s'ils ont des ressources suffisantes, par exemple s'ils ont travaillé pendant une période raisonnable (...) ". Il résulte de ces dispositions qui permettent de tenir compte des ressources des demandeurs dans l'octroi des conditions matérielles d'accueil et de leur demander une participation en fonction de ces ressources, que, lorsqu'un Etat membre a choisi d'octroyer les conditions matérielles d'accueil relatives au logement sous la forme d'une allocation financière, il peut en subordonner l'octroi à l'existence de frais de logement effectivement supportés par ce demandeur. Lorsque le demandeur d'asile ne supporte pas de tels frais parce qu'il dispose gratuitement d'un hébergement ou d'un logement par l'entremise de réseaux familiaux, amicaux ou associatifs, l'allocation financière ne lui est pas due tant que dure la gratuité de l'hébergement ou du logement.<br/>
<br/>
              7. Par suite, le fait que le décret attaqué ait prévu que le montant additionnel de l'allocation ne sera pas versé au demandeur d'asile qui n'a pas manifesté un besoin d'hébergement ou qui a accès gratuitement à un hébergement ou un logement à quelque titre que ce soit ne méconnaît pas les objectifs  de cette directive et n'ajoute pas une condition non prévue par le législateur lequel, contrairement à ce qui est soutenu, n'a pas posé l'obligation que les demandeurs d'asile soient hébergés dans les lieux d'hébergement mentionnés à l'article L. 744-3 du code de l'entrée et du séjour des étrangers et du droit d'asile. Par ailleurs, l'obligation faite aux demandeurs d'asile qui ne sont pas hébergés dans un de ces lieux d'informer l'Office français de l'immigration et de l'intégration de leur lieu d'hébergement au plus tard dans les deux mois suivant l'enregistrement de leur demande d'asile puis tous les six mois, ne fait pas obstacle à ce qu'ils fournissent cette information plus tôt ou à tout moment à l'intérieur du délai de six mois s'ils le jugent utile, notamment en cas de changement de leur situation en matière d'hébergement.<br/>
<br/>
              8. En second lieu, aux termes de l'article 17 de la même directive : " (...) 2. Les Etats membres font en sorte que les mesures relatives aux conditions matérielles d'accueil assurent aux demandeurs un niveau de vie adéquat qui garantisse leur subsistance et protège leur santé physique et mentale. (...) / 5. Lorsque les Etats membres octroient les conditions matérielles d'accueil sous forme d'allocations financières ou de bons, le montant de ceux-ci est fixé en fonction du ou des niveaux établis dans l'Etat membre concerné, soit par le droit, soit par la pratique, pour garantir un niveau de vie adéquat à ses ressortissants. (...) ". Il résulte de ces dispositions que, lorsqu'un Etat membre n'est pas en mesure d'offrir à un demandeur d'asile une solution d'hébergement en nature, il doit lui verser une allocation financière d'un montant suffisant pour lui permettre de disposer d'un logement sur le marché privé de la location. Les associations requérantes font valoir que ce montant est manifestement insuffisant au regard des montants des loyers dans les résidences sociales situées dans les grandes métropoles et en particulier en Île-de-France, région qui connaît une concentration importante des demandeurs d'asile. Il ressort toutefois des pièces du dossier que le montant additionnel de 7,40 euros prévu par le décret attaqué n'est pas manifestement insuffisant pour permettre à un demandeur d'asile qui a manifesté un besoin d'hébergement et n'a pas accès gratuitement à un hébergement ou un logement, de disposer d'un logement sur le marché privé de la location dans l'ensemble du territoire métropolitain. <br/>
<br/>
              9. Il résulte de tout ce qui précède que les associations requérantes ne sont pas fondées à demander l'annulation du décret attaqué. Par suite, leurs conclusions au titre des dispositions de l'article L. 761-1 du code de justice administrative ne peuvent qu'être rejetées.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : La requête de l'association La Cimade et des autres requérants est rejetée. <br/>
<br/>
Article 2 : La présente décision sera notifiée à La Cimade, premier requérant dénommé, au ministre de l'intérieur et au Premier ministre.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
