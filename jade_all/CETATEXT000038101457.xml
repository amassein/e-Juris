<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038101457</ID>
<ANCIEN_ID>JG_L_2019_02_000000415648</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/10/14/CETATEXT000038101457.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème chambre, 01/02/2019, 415648, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-02-01</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>415648</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GARREAU, BAUER-VIOLAS, FESCHOTTE-DESBOIS ; SCP MEIER-BOURDEAU, LECUYER</AVOCATS>
<RAPPORTEUR>M. Olivier  Fuchs</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Sophie-Justine Lieber</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2019:415648.20190201</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme A...C...a demandé au tribunal administratif de Lille d'annuler pour excès de pouvoir la décision par laquelle le président du Centre national de la recherche scientifique (CNRS) a suspendu sa rémunération, pour absence de service fait, entre le 1er janvier et le 26 mai 2013, ainsi que la décision du 20 août 2013 par laquelle il a rejeté son recours gracieux, d'enjoindre au CNRS de lui verser sa rémunération majorée des intérêts de retard au taux légal et de le condamner à l'indemniser des préjudices nés, selon elle, de l'illégalité de ces décisions. Par un jugement n° 1306501 du 9 mai 2017, le tribunal administratif de Lille a rejeté sa demande. <br/>
<br/>
              Par une ordonnance n° 17DA01377 du 13 septembre 2017, le président de la 3ème chambre de la cour administrative d'appel de Douai a rejeté l'appel formé par Mme C...contre ce jugement.  <br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés le 13 novembre 2017 et les 13 février et 5 novembre 2018 au secrétariat du contentieux du Conseil d'Etat, Mme C...demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cette ordonnance ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande ; <br/>
<br/>
              3°) de mettre à la charge du CNRS la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
	Vu :<br/>
	- le code de la recherche ; <br/>
	- la loi de finances rectificative n° 61-825 du 29 juillet 1961 ; <br/>
	- le décret n° 83-1260 du 30 décembre 1983 ; <br/>
	- le décret n° 84-1185 du 27 décembre 1984<br/>
	- le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Olivier Fuchs, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Sophie-Justine Lieber, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Garreau, Bauer-Violas, Feschotte-Desbois, avocat de Mme C...et à la SCP Meier-Bourdeau, Lecuyer, avocat du centre national de la recherche scientifique ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article 20 de la loi du 13 juillet 1983 portant droits et obligations des fonctionnaires : " Les fonctionnaires ont droit, après service fait, à une rémunération comprenant le traitement, l'indemnité de résidence, le supplément familial de traitement ainsi que les indemnités instituées par un texte législatif ou réglementaire ". Le service fait, au sens de ces dispositions, est défini par l'article 4 de la loi de finances rectificative du 29 juillet 1961 qui dispose que : " Il n'y a pas de service fait : / 1° Lorsque l'agent s'abstient d'effectuer tout ou partie de ses heures de service ; / 2° Lorsque l'agent, bien qu'effectuant ses heures de service, n'exécute pas tout ou partie des obligations de service qui s'attachent à sa fonction telles qu'elles sont définies dans leur nature et leurs modalités par l'autorité compétente dans le cadre des lois et règlements ".<br/>
<br/>
              2. Premièrement, contrairement à ce que soutient MmeD..., il ressort des termes mêmes de l'ordonnance qu'elle attaque que le président de la 3ème chambre de la cour administrative d'appel de Douai ne s'est pas exclusivement fondé, pour juger qu'elle se trouvait en situation d'absence de service fait entre le 1er janvier et le 26 mai 2013, sur la circonstance qu'elle était physiquement absente du laboratoire dans lequel elle était affectée, mais a également retenu, par une appréciation souveraine exempte de dénaturation, qu'elle n'établissait pas non plus avoir, au cours de cette période, exercé, même hors de son laboratoire, les fonctions qui lui incombaient. En déduisant de l'ensemble de ces circonstances que Mme C...s'était abstenue d'effectuer son service, le président de la 3ème chambre de la cour administrative d'appel n'a pas entaché son ordonnance d'erreur de droit.<br/>
<br/>
              3. Deuxièmement, aux termes du neuvième alinéa de l'article R. 222-1 du code de justice administrative : " Les présidents des cours administratives d'appel, les premiers vice-présidents des cours et les présidents des formations de jugement des cours (...) peuvent, en outre, par ordonnance, rejeter (...) les requêtes d'appel manifestement dépourvues de fondement  ". Le président de la 3ème chambre de la cour administrative d'appel de Douai n'a pas commis d'erreur de droit ni abusé de la faculté que lui offrent ces dispositions en rejetant par ordonnance la requête d'appel de MmeB.... L'absence de motivation, par l'ordonnance, du recours à cette faculté ne l'entache pas d'irrégularité. <br/>
<br/>
              4. Il résulte de tout ce qui précède que le pourvoi de Mme C...doit être rejeté, y compris ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
        Article 1er : Le pourvoi de Mme C...est rejeté.<br/>
<br/>
Article 2 : La présente décision sera notifiée à Mme A...C...et au Centre national de la recherche scientifique.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
