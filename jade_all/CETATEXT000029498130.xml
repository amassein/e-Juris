<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029498130</ID>
<ANCIEN_ID>JG_L_2014_09_000000363194</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/49/81/CETATEXT000029498130.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème et 9ème sous-sections réunies, 24/09/2014, 363194, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-09-24</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>363194</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème et 9ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Isabelle Lemesle</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Delphine Hedary</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:363194.20140924</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              L'union syndicale solidaires Paris a demandé au tribunal administratif de Paris :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision du 18 février 2010 par laquelle le directeur général du travail a rejeté la demande qu'elle avait formulée auprès du ministre du travail, des relations sociales, de la famille et de la solidarité, le 5 janvier 2010, de supprimer du traitement automatisé de données à caractère personnel dénommé "  CAP SITERE " tout nom de salarié protégé ou accidenté ;<br/>
<br/>
              2°) d'enjoindre au ministre chargé du travail de retirer ces données nominatives.<br/>
<br/>
              Par une ordonnance du 24 septembre 2012, enregistrée le 2 octobre 2012 au secrétariat du contentieux du Conseil d'Etat, le président du tribunal administratif de Paris a transmis cette requête au Conseil d'Etat, en application de l'article R. 351-2 du code de justice administrative.<br/>
<br/>
<br/>
              Vu :<br/>
              - les autres pièces du dossier ;<br/>
              - la loi n° 78-17 du 6 janvier 1978 ;<br/>
              - le décret n° 2005-1309 du 20 octobre 2005 ;<br/>
              - le code de justice administrative.<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Isabelle Lemesle, maître des requêtes, 	<br/>
              - les conclusions de Mme Delphine Hedary, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>Sur la fin de non-recevoir opposée par le ministre :<br/>
<br/>
              1. Considérant en premier lieu, que par une lettre du 5 janvier 2010, l'union syndicale solidaires Paris (USSP) a demandé au ministre chargé du travail d'une part, un certain nombre d'informations sur le fonctionnement du traitement automatisé de données à caractère personnel intitulé " CAP SITERE ", destiné à la gestion des dossiers d'établissement par les agents des services du secteur travail de ce ministère, créé par un arrêté ministériel du 6 octobre 2006 et, d'autre part, de supprimer de ce traitement " tout nom de salariés protégés et tout nom de salariés accidentés " ; que, par une lettre du 18 février 2010, le directeur général du travail n'a répondu qu'à la demande d'information ; que, par suite, la requête de l'USSP, tendant à l'annulation de la décision du 18 février 2010 par laquelle le ministre chargé du travail aurait rejeté sa demande de suppression de données nominatives du traitement " CAP SITERE ", transmise au Conseil d'Etat par le président du tribunal administratif de Paris en application de l'article R. 351-2 du code de justice administrative, doit être regardée comme tendant à l'annulation de la décision implicite de rejet née du silence gardé par le ministre sur la demande de l'USSP tendant à l'abrogation des dispositions de l'article 2 de l'arrêté du 6 octobre 2006 qui mentionnent, parmi les catégories d'informations nominatives enregistrées par ce traitement, l'identité de salariés concernés par certaines procédures, à savoir : " victime d'accidents du travail ", " représentant du personnel élu ou désigné " et " salarié protégé de par l'exercice d'un mandat représentatif faisant l'objet d'une demande d'autorisation de licenciement " ; qu'ainsi, la fin de non-recevoir opposée tirée de ce que le courrier adressé le 18 février 2010 par le directeur général du travail à l'USSP constituerait une simple lettre d'information qui ne fait pas grief ne peut qu'être écartée ;<br/>
<br/>
              2. Considérant, en second lieu, qu'eu égard à la portée des dispositions dont l'abrogation est en cause, notamment celles qui permettent la collecte et le traitement des données relatives à l'appartenance syndicale des salariés des établissements contrôlés par l'inspection du travail, l'USSP, qui est une union de syndicats régie par les articles L. 2133-1 à L. 2133-3 du code du travail, à laquelle s'appliquent les dispositions de l'article L. 2132-3 du même code et dont l'objet est notamment de " soutenir les intérêts matériels et moraux de l'ensemble du monde du travail de son champ géographique ", justifie d'un intérêt lui donnant qualité pour demander l'annulation de la décision refusant de procéder à l'abrogation demandée ; <br/>
<br/>
              Sur la légalité des dispositions contestées : <br/>
<br/>
              3. Considérant qu'il résulte des dispositions de l'article 8 de la loi du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés qu'il est notamment interdit de collecter ou de traiter des données à caractère personnel qui font apparaître, directement ou indirectement, l'appartenance syndicale des personnes ou qui sont relatives à la santé ; que le IV de cet article prévoit toutefois que ne sont pas soumis à cette interdiction ceux de ces traitements qui sont justifiés par l'intérêt public et autorisés dans les conditions prévues au I de l'article 25 ou au II de l'article 26 ; que le III de l'article 25, relatif à la mise en oeuvre, après autorisation de la Commission nationale de l'informatique et des libertés (CNIL) des traitements qui ne relèvent pas de la procédure prévue aux articles 26 et 27 de la même loi, et notamment de ceux mentionnés au IV de l'article 8, dispose que lorsque la commission ne s'est pas prononcée dans un délai de deux mois à compter de la réception de la demande, celle-ci est réputée rejetée ;<br/>
<br/>
              4. Considérant que l'article 1er de l'arrêté litigieux précise que le traitement en cause est destiné à la gestion du dossier d'établissement, au partage d'information sur les établissements et structures complexes, à l'édition de courriers, à la gestion d'un agenda partagé ainsi que des procédures de signalement, au suivi des procédures judiciaires et au suivi et pilotage de l'activité du champ travail ; qu'il ressort par ailleurs de ses visas que cet arrêté a été pris après " accord tacite de la Commission nationale de l'informatique et des libertés intervenu le 28 juin 2006 sur la demande d'avis initiale qui lui a été transmise le 29 juin 2005 " ;<br/>
<br/>
              5. Considérant, d'une part, qu'eux égard à ces finalités, le traitement " CAP SITERE " n'entre pas dans le champ d'application du 1° du I de l'article 26 selon lequel certains traitements de données à caractère personnel mis en oeuvre pour le compte de l'Etat sont autorisés par arrêté ministériel ; que la seule circonstance qu'il soit notamment destiné à gérer les procédures de signalement et à suivre les procédures judiciaires ne permet pas de le regarder comme entrant dans le champ d'application du 2° du I du même article ; qu'il ne relève pas davantage des catégories de traitements énumérées au II de l'article 27 qui sont autorisés par arrêté et non par décret en Conseil d'Etat ; que, par suite, il n'est pas au nombre des traitements susceptibles d'être autorisés par arrêté ministériel ; <br/>
<br/>
              6. Considérant d'autre part, que dès lors qu'il ressort de l'article 2 de l'arrêté que ce traitement collecte, notamment, des données relatives à l'appartenance syndicale et à la santé de certains salariés et qu'il est justifié par l'intérêt public de gestion des établissements soumis au code du travail, il ne pouvait être créé, en application du IV de l'article 8 et du I de l'article 25 de la loi du 6 janvier 1978, qu'après autorisation de la CNIL ; que faute pour cette dernière de s'être prononcée dans un délai de deux mois à compter du 29 juin 2005, elle doit être regardée comme ayant rejeté la demande d'autorisation qui lui était soumise ; qu'ainsi, l'arrêté du 6 octobre 2006 qui autorise ce traitement a été pris par une autorité incompétente ; <br/>
<br/>
              7. Considérant qu'il résulte de ce qui précède que, sans qu'il soit besoin d'examiner les moyens de la requête, l'USPP est fondée à soutenir que c'est à tort que le ministre chargé du travail a refusé de prononcer l'abrogation des dispositions de l'article 2 de cet arrêté faisant l'objet de sa demande du 5 janvier 2010, c'est-à-dire celles correspondant à la rubrique " identité de salariés concernés par certaines procédures " ;<br/>
<br/>
              Sur les conclusions à fin d'injonction :<br/>
<br/>
              8. Considérant que si l'annulation de la décision refusant l'abrogation sollicitée implique normalement que le juge enjoigne au ministre d'y procéder, il ressort des pièces du dossier que le traitement autorisé par l'arrêté en cause est nécessaire à la gestion, par l'inspection du travail, des dossiers des établissements placés sous son contrôle ; qu'aucun des moyens invoqués par la requérante, sur le fond, n'aurait été de nature à justifier l'annulation prononcée par la présente décision ; qu'en particulier, les limitations d'accès au traitement sont suffisantes et que les conditions réelles de son fonctionnement sont sans incidence sur la légalité de l'acte autorisant le traitement ; qu'il appartient néanmoins au ministre, s'il entend maintenir dans ce traitement le recueil des données personnelles mentionnées ci-dessus, de suivre la procédure d'autorisation définie par les dispositions citées ci-dessus ;<br/>
<br/>
              9. Considérant que, dans ces conditions, il y a lieu de fixer à huit mois à compter de la notification de la présente décision le délai au terme duquel, s'il n'a pas obtenu de la CNIL l'autorisation nécessaire, le ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social devra prononcer l'abrogation des dispositions de l'article 2 de l'arrêté du 6 octobre 2006 mentionnées au point 7 et faire supprimer l'ensemble des données litigieuses recueillies dans le traitement ; <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : La décision implicite de rejet née du silence gardé par le ministre chargé du travail à la demande du 5 janvier 2010 de l'union syndicale solidaires Paris de voir abroger certaines des dispositions de l'arrêté du 6 octobre 2006 du ministre de l'emploi, de la cohésion sociale et du logement est annulée.<br/>
Article 2 : Il est enjoint au ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social de prendre les mesures définies au point 9.<br/>
Article 3 : La présente décision sera notifiée à l'union syndicale solidaires Paris et au ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
