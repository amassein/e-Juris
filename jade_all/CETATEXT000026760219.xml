<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026760219</ID>
<ANCIEN_ID>JG_L_2012_12_000000355220</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/76/02/CETATEXT000026760219.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  8ème sous-section jugeant seule, 10/12/2012, 355220, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2012-12-10</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>355220</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 8ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROCHETEAU, UZAN-SARANO ; SPINOSI</AVOCATS>
<RAPPORTEUR>Mme Maryline Saleix</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Benoît Bohnert</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2012:355220.20121210</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la décision du 7 juin 2012 par laquelle le Conseil d'Etat, statuant au contentieux, a prononcé l'admission des conclusions du pourvoi de M. Alain C dirigées contre l'ordonnance n° 1102277 du 8 décembre 2011 du juge des référés du tribunal administratif de Caen, statuant sur le fondement de l'article L. 521-1 du code de justice administrative, en tant que cette ordonnance a rejeté les conclusions de sa demande présentées au titre de l'article R. 761-1 du code de justice administrative ; M. C demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler, dans cette mesure, l'ordonnance attaquée ; <br/>
<br/>
              2°) statuant au titre de la procédure de référé, de faire droit à sa demande de remboursement des dépens ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat le versement de la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative ;    <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code général des impôts ;<br/>
<br/>
              Vu le décret n° 2011-1202 du 28 septembre 2011 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Maryline Saleix, Maître des Requêtes en service extraordinaire,  <br/>
<br/>
              - les observations de la SCP Rocheteau, Uzan-Sarano, avocat de M. C et de Me Spinosi, avocat de la ville de Caen,<br/>
<br/>
              - les conclusions de M. Benoît Bohnert, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Rocheteau, Uzan-Sarano, avocat de M. C et à Me Spinosi, avocat de la ville de Caen ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'aux termes du premier alinéa de l'article R. 761-1 du code de justice administrative, dans sa rédaction qui était en vigueur à la date de la demande soumise au juge des référés par M. C et résultant de l'article 17 du décret du 28 septembre 2011 relatif au droit affecté au fonds d'indemnisation de la profession d'avoué près les cours d'appel et à la contribution pour l'aide juridique : " Les dépens comprennent la contribution pour l'aide juridique prévue à l'article 1635 bis Q du code général des impôts, ainsi que les frais d'expertise, d'enquête et de toute autre mesure d'instruction dont les frais ne sont pas à la charge de l'Etat. "<br/>
<br/>
              2. Considérant que, pour rejeter les conclusions par lesquelles M. C demandait, sur le fondement de ces dispositions, que les dépens qu'il avait exposés dans l'instance soient mis à la charge de la commune, le juge des référés du tribunal administratif de Caen a relevé que le requérant ne justifiait pas avoir engagé des frais d'expertise, d'enquête et de toute autre mesure d'instruction dont les frais ne sont pas à la charge de l'Etat ;<br/>
<br/>
              3. Considérant, toutefois, qu'il ressort des pièces du dossier soumis au juge des référés que M. C avait, conformément à l'article R. 411-2 du code de justice administrative, apposé sur sa demande le timbre fiscal de 35 euros représentant la contribution pour l'aide juridique prévue à l'article 1635 bis Q du code général des impôts et rendue obligatoire, en application de l'article 21-III du décret du 28 septembre 2011 relatif au droit affecté au fonds d'indemnisation de la profession d'avoué près les cours d'appel et à la contribution pour l'aide juridique, à toutes les requêtes introduites à compter du 1er octobre 2011 ; que par suite, en retenant ce motif, alors que la demande de M. C avait été introduite le 15 novembre 2011 et était dûment revêtue du timbre fiscal exigé par les dispositions de l'article R. 411-2 du code de justice administrative, le juge des référés a entaché son ordonnance d'une erreur de droit ; que M. C est, dès lors, fondé à demander l'annulation de cette ordonnance en tant qu'elle a rejeté ses conclusions tendant à ce que les dépens qu'il a engagés dans la procédure soient mis à la charge de la commune de Caen ;<br/>
<br/>
              Considérant que, dans les circonstances de l'espèce, il y a lieu pour le Conseil d'Etat de régler l'affaire au titre de la procédure de référé engagée, en application des dispositions de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              Considérant qu'en vertu du second alinéa de l'article R. 761-1 du code de justice administrative, les dépens sont mis à la charge de la partie perdante, sous réserve de dispositions particulières ou de circonstances particulières justifiant qu'ils soient mis à la charge d'une autre partie ou partagés entre les parties ; que la commune de Caen n'est pas la partie perdante ; que, par suite, en l'absence de disposition particulière ou de circonstance particulière justifiant que les dépens soient mis partiellement ou totalement à la charge de la commune, les conclusions de M. C tendant à ce que la commune supporte, au titre des dispositions de l'article R. 761-1 du code de justice administrative, la somme qu'il a exposée au titre de la contribution pour l'aide juridique ne peuvent qu'être rejetées ;<br/>
<br/>
<br/>
<br/>		D E C I D E :<br/>
              		--------------<br/>
<br/>
Article 1er : L'ordonnance du 8 décembre 2011 du juge des référés du tribunal administratif de Caen est annulée en tant qu'elle a rejeté les conclusions de M. C présentées au titre de l'article R. 761-1 du code de justice administrative.<br/>
<br/>
Article 2 : Les conclusions de la demande de M. C tendant à ce que la commune de Caen supporte la contribution pour l'aide juridique prévue à l'article 1635 bis Q du code général des impôts sont rejetées.<br/>
<br/>
Article 3 : La présente décision sera notifiée à M. Alain C et à la commune de Caen. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
