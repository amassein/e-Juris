<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033391570</ID>
<ANCIEN_ID>JG_L_2016_10_000000386664</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/39/15/CETATEXT000033391570.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème chambre, 27/10/2016, 386664, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-10-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>386664</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MASSE-DESSEN, THOUVENIN, COUDRAY</AVOCATS>
<RAPPORTEUR>Mme Pauline Pannier</RAPPORTEUR>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CECHS:2016:386664.20161027</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme AY...H...et 424 autres salariés de la société LFoundry Rousset ont demandé au tribunal administratif de Marseille d'annuler pour excès de pouvoir la décision du 25 février 2014 par laquelle la directrice régionale des entreprises, de la concurrence, de la consommation, du travail et de l'emploi de Provence-Alpes-Côte d'Azur a homologué le document élaboré par MeLW..., agissant en qualité de liquidateur judiciaire de la société LFoundry Rousset, fixant le contenu du plan de sauvegarde de l'emploi. Par un jugement n° 1401877, 1401880, 1402008 du 6 juin 2014, le tribunal administratif de Marseille a annulé cette décision.<br/>
<br/>
              Par un arrêt n° 13MA03521 du 24 octobre 2014, la cour administrative d'appel de Marseille a rejeté l'appel du ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social dirigé contre ce jugement. <br/>
<br/>
              Par un pourvoi enregistré le 23 décembre 2014 au secrétariat du contentieux du Conseil d'Etat, le ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social  demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de commerce ;<br/>
              - le code du travail ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Pauline Pannier, auditeur,  <br/>
<br/>
              - les conclusions de Mme Maud Vialettes, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Masse-Dessen, Thouvenin, Coudray, avocat de M OW...et autres ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, par un jugement du 26 décembre 2013, le tribunal de commerce de Paris a prononcé la liquidation judiciaire de la société LFoundry Rousset ; qu'à la demande de MeLW..., liquidateur judiciaire de la société, la directrice régionale des entreprises, de la concurrence, de la consommation, du travail et de l'emploi de Provence-Alpes-Côte d'Azur a, par une décision du 8 janvier 2014, homologué le document unilatéral de l'employeur fixant un plan de sauvegarde de l'emploi ; que, par un jugement du 6 juin 2014, le tribunal administratif de Marseille, faisant droit à la demande de plusieurs salariés de l'entreprise, a annulé cette décision ; que le ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social se pourvoit en cassation contre l'arrêt du 24 octobre 2014 par lequel la cour administrative d'appel de Marseille a rejeté son appel dirigé contre ce jugement ;<br/>
<br/>
              Sur la régularité de l'arrêt attaqué :<br/>
<br/>
              2. Considérant que le moyen tiré de ce que le sens des conclusions du rapporteur public qui avaient été mises en ligne aurait été modifié sans que les parties n'en aient été expressément prévenues n'est pas assorti des précisions permettant d'en apprécier le bien-fondé ;<br/>
<br/>
              3. Considérant qu'aucune règle ni aucun principe n'impose que, à l'issue de la séance publique, la juridiction informe les parties de la date de lecture de la décision ; que dès lors, le ministre ne peut utilement soutenir que l'absence de cette information aurait constitué une irrégularité de nature à entraîner l'annulation de l'arrêt attaqué ;<br/>
<br/>
              Sur le bien-fondé de l'arrêt attaqué :<br/>
<br/>
              4. Considérant qu'il ressort des termes de l'arrêt attaqué que la cour s'est fondée, pour rejeter l'appel du ministre chargé du travail contre le jugement ayant prononcé l'annulation de la décision du 8 janvier 2014, sur deux motifs, tirés, pour le premier de l'insuffisance de la motivation de cette décision et, pour le second, de ce que l'administration a commis une erreur de droit faute d'avoir pris en compte, pour apprécier le caractère suffisant des mesures contenues dans le plan, les moyens du groupe SLD auquel appartenait la société LFoundry Rousset ;<br/>
<br/>
              5. Considérant, en premier lieu, qu'aux termes de l'article L. 1233-57-4 du code du travail : " L'autorité administrative notifie à l'employeur la décision de validation dans un délai de quinze jours (...) et la décision d'homologation dans un délai de vingt et un jours (...). / Elle la notifie, dans les mêmes délais, au comité d'entreprise et, si elle porte sur un accord collectif, aux organisations syndicales représentatives signataires. La décision prise par l'autorité administrative est motivée " ; que si ces dispositions impliquent que la décision qui valide un accord collectif portant plan de sauvegarde de l'emploi, ou la décision qui homologue un document fixant le contenu d'un tel plan, doivent énoncer les éléments de droit et de fait qui en constituent le fondement, de sorte que les personnes auxquelles ces décisions sont notifiées puissent à leur seule lecture en connaître les motifs, elles n'impliquent ni que l'administration prenne explicitement parti sur le respect de chacune des règles dont il lui appartient d'assurer le contrôle en application des dispositions des articles L. 1233-57-2 et L. 1233-57-3 du même code, ni qu'elle retrace dans la motivation de sa décision les étapes de la procédure préalable à son édiction ;<br/>
<br/>
              6. Considérant, dès lors, qu'en jugeant que la motivation de la décision d'homologation doit attester de ce que l'administration a vérifié l'ensemble des points sur lesquels doit porter son contrôle, la cour a commis une erreur de droit ;<br/>
<br/>
              7. Considérant, toutefois, en second lieu, qu'ainsi qu'il a été dit au point 4, la cour s'est également fondée, pour rejeter l'appel du ministre, sur le motif tiré de ce que l'administration a commis une erreur de droit faute d'avoir pris en compte les moyens du groupe SLD auquel appartenait la société LFoundry Rousset pour apprécier le caractère suffisant des mesures contenues dans le plan ;<br/>
<br/>
              8. Considérant qu'il résulte des dispositions de l'article L. 1233-57-3 du code du travail que, lorsqu'elle est saisie d'une demande d'homologation d'un document élaboré en application de l'article L. 1233-24-4 du même code, il appartient à l'administration, sous le contrôle du juge de l'excès de pouvoir, de vérifier la conformité de ce document et du plan de sauvegarde de l'emploi dont il fixe le contenu aux dispositions législatives et aux stipulations conventionnelles applicables, en s'assurant notamment du respect par le plan de sauvegarde de l'emploi des dispositions des articles L. 1233-61 à L. 1233-63 du même code ; qu'à ce titre elle doit, au regard de l'importance du projet de licenciement, apprécier si les mesures contenues dans le plan sont précises et concrètes et si, à raison, pour chacune, de sa contribution aux objectifs de maintien dans l'emploi et de reclassement des salariés, elles sont, prises dans leur ensemble, propres à satisfaire à ces objectifs compte tenu, d'une part, des efforts de formation et d'adaptation déjà réalisés par l'employeur et, d'autre part, des moyens dont disposent l'entreprise et, le cas échéant, l'unité économique et sociale et le groupe ;<br/>
<br/>
              9. Considérant, d'une part, que la circonstance qu'un employeur effectue des démarches actives auprès du groupe auquel appartient l'entreprise pour que celui-ci abonde les mesures du plan de sauvegarde de l'emploi est, par elle-même, sans incidence sur l'appréciation à porter sur le caractère suffisant de ces mesures, laquelle n'a légalement à tenir compte que des moyens, notamment financiers, que l'entreprise et le groupe auquel elle appartient sont susceptibles de consacrer aux différentes mesures contenues dans le plan ; qu'ainsi, la cour a pu, sans dénaturer les pièces du dossier qui lui était soumis ni entacher son arrêt d'erreur de droit, juger que la seule prise en compte, par le directeur régional des entreprises, de la concurrence, de la consommation, du travail et de l'emploi, des démarches effectuées en ce sens par le liquidateur de la société LFoundry Rousset, n'établissait pas que l'administration avait porté l'appréciation qui lui incombe au regard des moyens du groupe ;<br/>
<br/>
              10. Considérant, d'autre part, que la cour n'a pas davantage commis d'erreur de droit en jugeant que la brièveté du délai imparti au liquidateur après le prononcé du jugement de cession, en vue de procéder aux licenciements dans des conditions garantissant l'intervention de l'assurance prévue par l'article L. 3253-6 du code du travail, était sans incidence sur l'appréciation que doit porter l'administration sur le caractère suffisant des mesures contenues dans le plan au regard des moyens du groupe ; qu'elle ne s'est pas, sur ce point, méprise sur la portée des écritures du ministre chargé du travail ;<br/>
<br/>
              11. Considérant, dès lors, que c'est sans erreur de droit et par une appréciation souveraine qui n'est pas entachée de dénaturation, que la cour a estimé qu'il ressortait des pièces du dossier qui lui était soumis, et notamment de la motivation de la décision litigieuse, que l'administration avait omis de prendre en compte les moyens du groupe auquel appartient la société LFoundry Rousset pour apprécier le caractère suffisant des mesures contenues dans le plan de sauvegarde de l'emploi ; qu'un tel motif justifiait nécessairement, à lui seul, le dispositif de l'arrêt attaqué ; <br/>
<br/>
              12. Considérant qu'il résulte de tout ce qui précède que le ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social n'est pas fondé à demander l'annulation de l'arrêt du 24 octobre 2014 de la cour administrative d'appel de Marseille ;<br/>
<br/>
              Sur les conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              13. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 100 euros chacun à verser à M. JP...et autres au titre de ces dispositions ; <br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er : Le pourvoi du ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social est rejeté.<br/>
Article 2 : L'Etat versera la somme de 100 euros chacun à M. MQ...JP..., M. VQ... DW..., M. FZ...OW..., M. CE...JY..., M. CC...KB..., Mme CD...P..., M. OD... P..., Mme AX...WB..., M. PU...BK..., Mme UH...SR..., M. PU... UR..., Mme HE...WH..., Mme LQ...FO..., M. ST...UT..., M. NS...V..., Mme PE...PT..., M. UP...FT..., M. CF...Y..., M. UJ...GA..., M. OC...QC..., M. LJ...LR..., M. BI...TH..., Mme BO...MC..., Mme JI...WA..., M. SF...QO..., M. HX...QQ..., M. LI...NE..., M. Z...HU..., Mme QM...NJ..., M. NM...E..., M. MQ... RQ...et à M. GY...AT...au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée à la ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social et à M. MQ...JP..., premier défendeur dénommé. Les autres défendeurs ayant produit seront informés de la présente décision par la SCP Masse-Dessen, Thouvenin, Coudray, avocat au Conseil d'Etat et à la Cour de cassation, qui les représente devant le Conseil d'Etat. <br/>
Copie en sera adressée à MeLW..., en qualité de liquidateur de la société LFoundry Rousset, à Mme AY...H..., M. AJ...DX..., M. AE...-FR...DY..., Mme EX...I..., M. AE...-FF...OQ..., Mme EZ...VX..., M. QA...K..., M. FF... EB..., M. IQ...OS..., M. OC...JD..., Mme CH...AZ..., Mme SU...JE..., M. UG...BA..., M. MQ...M..., Mme OL...EC..., Mme UI...OT..., Mme SY...ED..., Mme UX...JF..., Mme KT...EF..., M. FF...JG..., M. FR...SH..., M. LU... EG..., M. BR...A..., Mme PL...OU..., Mme GQ...JH..., M. GY...BB..., MmeHT..., Ayuste, M. J...EH..., M. EE...JK..., M. AE...-UP...VH..., M. IC...WG..., M. AE...-UG...BC..., M. BS...EI..., M. TO... EJ..., M. RE...BD..., Mme KT...UK..., Mme PQ...JM..., M. OM...JN..., M. KV... EK..., M. GC...EL..., M. AE...-HN...JO..., M. J...SI..., Mme PZ...UL..., M. AF...BE..., M. AE...-AJ...JR..., M. UP...JS..., Mme SU...JT..., M. LE...EM..., M. X...EN..., Mme FV...JU..., M. RE... JU..., M. HN...SJ..., M. GH...OV..., M. DO...VI..., M. UG... SK..., M. MQ...UM..., Mme JB...JV..., Mme QT...JV..., M. OC...O..., M. OC...OX..., M. G...JW..., M. NM...EO..., M. OH...EP..., M. AE...WO...EQ..., Mme UH...SL..., M. HN...OY..., M. LX...BF..., M. J...BG..., M. OM...JX..., M. J...BG..., M. NM...ER..., Mme RC...JZ..., Mme KT...OZ..., M. QA...PA..., M. IC...ES..., M. AE...-C...BH..., Mme SG... KA..., M. OC...KC..., M. HN...ET..., M. UP...PC..., M. UP... PD..., M. OD...KD..., M. OD...KE..., M. AE...-AJ...KF..., M. OH... EU..., M. OC...BJ..., Mme GQ...KG..., Mme L...EV..., M. X...EV..., M. AM...KH..., M. NM...KJ..., Mme PQ...PF..., M. EA...EY..., Mme UO...PG..., M. RE...KK..., M. JU... Q..., M. HN...SN..., Mme SE...SO..., Mme DF...BL..., M. MQ...SP..., M. AE...-PZ...BM..., M. UG...BM..., M. C...SQ..., M. NH...PH..., M. RE...KL..., Mme UH...KN..., M. AE...-C...UN..., M. MM...R..., Mme UH...KP..., M. OW...FC..., Mme ND...VJ..., M. NM...BN..., M. NH...PJ..., M. AJ...SS..., M. NH...PK..., M. OC...KQ..., M. MQ...FG..., M. BQ...PM..., M. DO... UQ..., M. LE...S..., M. GI...FH..., M. CC...KR..., M. X...FI..., M. QA...PN..., M. IN...KS..., M. AE...BP..., M. X...FK..., Mme UO...SV..., M. EE...T..., M. J...FL..., M. BS...PO..., M. VY... KU..., M. LU...KW..., M. MQ...VR..., M. LE...FN..., M. NL...US..., M. GC...FP..., Mme IV...KX..., M. BR...SW..., M. TI... KY..., à Mme KT...KZ..., Mme QM...PP..., Mme PB...LA..., M. IC...FQ..., Mme UO...LB..., Mme L... -RD... VM...BY..., M. AJ...PR..., Mme L...-WN...PS..., Mme GX...BT..., Mme WS...VM...SA..., M. FM... LC..., M. HN...FS..., M. MI...LD..., Mme DF...UU..., Mme KO...FT..., M. DE...BU..., M. AW...FU..., M. UP...W..., M. HN...FW..., Mme CQ...FY..., M. VZ... SZ..., Mme HT...BW..., Mme PI...BW..., Mme NI...BW..., M. HX...TA..., Mme PE...LF..., M. X...LG..., Mme GV...UV..., M. CC...LH..., M. OM...TB..., Mme TF...PW..., M. DJ... GB..., M. JJ...BX..., M. UP...LK..., M. PU...LK..., M. UP... LL..., M. NH...PY..., M. J...LM..., M. FZ...UW..., M. OH... LN..., M. OC...LP..., M. OH...WD..., M. NM...GD..., M. DV... GE..., M. PZ...QB..., M. NH...TC..., M. QA...GF..., M. LJ...LS..., M. OM...GG..., M. UP...LT..., M. NF...TD..., Mme FV...TE..., M. PU...B..., M. UP...QD..., M. GI... VS..., Mme SC...QE..., Mme RL...LV..., Mme  UH...GJ..., Mme NT...QF..., Mme RC...GK..., M. AR...TG..., Mme PX...GM..., Mme RO...BZ..., M. GW... GN..., M. FR... LY..., M. BS...LZ..., M. NX...MA..., M. AE...-AW...QH..., Mme FX...MB..., Mme IV...QI..., Mme HR...TJ..., Mme PX...CA..., Mme CR...GO..., Mme U...CB..., Mme RC...VK..., Mme  WE... GP..., Mme SM...AB..., Mme RD...TK..., M. NC... GR..., Mme HG...GS..., M. NM...MD..., M. UP...TL..., Mme EX...TM..., M. IP...AC..., M. OO...GU..., Mme FB...VL..., M. GT...ME..., M. X...MF..., Mme KO...TN..., Mme FB...MG..., M. AD...MH..., M. BQ...MJ..., M. AE...-RE...MK..., Mme DL...ML..., Mme PL...UY..., M. HA...GZ..., Mme QL...WK..., M. OH...CG..., M. MQ...MN..., Mme KT...MO..., Mme HF...QN..., M. IP...TP..., Mme HV...HB..., Mme BV...MP..., M. NH...MQ..., Mme UO...CI..., M. AJ...CJ..., M. AA...CK..., M. JC...QP..., Mme RC...CL..., M. MQ...HC..., M. SF...MR..., Mme VT...-IC...WL..., M. AE...-PZ...MS..., M. IC...CM..., M. DZ...QR..., M. UG... QR..., M. TS...TQ..., M. NH...TR..., M. X...CN..., M. BI...MT..., M. QK...M'KM..., M. VN... AH..., M. AE...-AW...MU..., M. VY... HH..., M. ST...AI..., Mme SU...MV..., M. IQ...MW..., M. RE...QS..., M. HD...TT..., M. RE...UZ..., Mme CO...MX..., M. AJ... QV..., Mme SC...QX..., M. X...HJ..., M. AE...-WP...HJ..., Mme KO...MY..., M. UE...MY..., Mme QU...HK..., Mme EX...MZ..., M. X... TU..., M. NF...HL..., M. X...NA..., M. ON...HM..., M. BR...NB..., M. AL...HN..., M. OC...HN..., Mme GL...QZ..., M. HN...CP..., M. NG...CP..., Mme SM...TV..., Mme L...VU..., Mme N...WM..., M. RE... RA..., M. FA...NE..., M. AF...HO..., M. NM... HP..., M. NH...RB..., Mme SM...HQ..., M. JU...NH..., Mme VT...-GQ...HW..., M. LE...CS..., M. IC...-NH...AK..., M. NC...CT..., M. CC...CU..., M. OH...CV..., M. OH...NK..., Mme OR...HY..., M. RE...CX..., M. QY...HZ..., MmeWJ..., M. AE...-AJ...CY..., M. HX...CY..., Mme CW...CY..., Mme OG...CY..., M. MI... NN..., Mme FB...IA..., Mme PV...IB..., M. CC...IB..., Mme EX...WQ..., M. GI...TW..., M. QG...VA..., M. UP...TX..., Mme KI...CZ..., Mme ND...TY..., M. JU...VB..., M. J...RF..., M. GH... RG..., M. AG...ID..., M. WI...DA..., M. FA...VV..., Mme JL...IE..., M. OD...IF..., M. UG...TZ..., M. NF...IG..., Mme RO...UA..., M. J...DB..., M. FJ...IH..., Mme PE...UB..., M. X... II..., M. HN...VC..., Mme SE...UC..., M. ST... RH..., Mme RO...UD..., Mme IV...NO..., M. DO...DC..., M. UG...DD..., M. UG... DF..., M. RE...DG..., M. UP...RI..., Mme JQ...IJ..., M. MQ... AN..., M. QA...IK..., Mme GQ...NQ..., M. BR...NR..., M. D...NS..., M. UP...RJ..., M. NF...DH..., Mme HI...IL..., Mme SC...IM..., M. UP...VD..., M. AJ...RK..., M. AV...AO..., M. FR...IN..., M. NH... DI..., M. QJ...IO..., M. RE... IO..., Mme EW...RM..., M. NF...RN..., M. JU...AP..., Mme EW...AP..., M. PZ...IR..., Mme FE...AQ..., M. NF... DK..., M. NL... NU..., M. NP...NV..., M. C...UF..., M. WR...-MY..., Mme IS...WF..., M. AE...-PZ...RP..., M. AE...DM..., M. BR... NW..., Mme QW...DN..., M. J...NY..., M. NM...DP..., M. NH... NZ..., M.WC..., M. AE...-IC...VE..., M. EE...DQ..., MmeVW..., M. VO... OA..., Mme EW...OB..., M. FM...IT..., M. NM... RR..., Mme RC...IU..., M. OM...DR..., M. RE...OE..., Mme IV...VF..., M. UP... RS..., M. NM...OF..., M. X...F..., M. OD... RT..., Mme BS...RU..., M. GH...IW..., M. SX...IX..., M. LO...OI..., M. X...IY..., Mme FB...IZ..., M. MI...RV..., M. FD... RW..., M. RX...AS..., M. RE...OJ..., M. OO...OK..., M. UP... DS..., Mme SC...JA..., M. OC...DT..., M. NM...RY..., M. ST... RZ..., M. GC...VG..., M.VP..., M. JJ...SB..., M. AE...-RE...DU..., Mme HS...SD..., M. X...AU...et à Mme QU...OP.... <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
