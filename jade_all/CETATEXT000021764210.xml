<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000021764210</ID>
<ANCIEN_ID>J0_L_2009_12_000000900356</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/21/76/42/CETATEXT000021764210.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Cour Administrative d'Appel de Versailles, 4ème Chambre, 29/12/2009, 09VE00356, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2009-12-29</DATE_DEC>
<JURIDICTION>Cour Administrative d'Appel de Versailles</JURIDICTION>
<NUMERO>09VE00356</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème Chambre</FORMATION>
<TYPE_REC>excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. BROTONS</PRESIDENT>
<AVOCATS>LAVANANT</AVOCATS>
<RAPPORTEUR>Mme Corinne  SIGNERIN-ICRE</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme JARREAU</COMMISSAIRE_GVT>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 9 février 2009, et le mémoire complémentaire, enregistré le 19 juin 2009, présentés pour Mlle Marie Lona A, demeurant chez Mme B, ..., par Me Lavanant ; Mlle A demande à la Cour : <br/>
<br/>
       1°) d'annuler l'ordonnance n° 0812686 du 7 janvier 2009 par laquelle le président du Tribunal administratif de Cergy-Pontoise a rejeté sa demande tendant à l'annulation de l'arrêté du 22 octobre 2008 du préfet du Val-d'Oise refusant de lui délivrer un titre de séjour et lui faisant obligation de quitter le territoire français à destination de son pays d'origine ;<br/>
<br/>
       2°) d'annuler pour excès de pouvoir cet arrêté ;<br/>
<br/>
       3°) d'enjoindre au préfet du Val-d'Oise de lui délivrer un titre de séjour temporaire ou, à défaut, une autorisation provisoire de séjour en attendant qu'il soit statué sur sa demande de titre de séjour ;<br/>
<br/>
       4°) de mettre à la charge de l'Etat le paiement à Me Lavanant de la somme de 2 000 euros au titre au titre des articles 37 et 75 de la loi du 10 juillet 1991 et L. 761-1 du code de justice administrative et lui donner acte de ce qu'elle s'engage à renoncer au bénéfice de l'aide juridictionnelle ;<br/>
<br/>
       Elle soutient, en premier lieu, que l'ordonnance attaquée n'est pas suffisamment motivée ; en deuxième lieu, que la décision portant obligation de quitter le territoire français ne cite pas les textes sur lesquels elle est fondée et est insuffisamment motivée ; en troisième lieu, que les décisions de refus de titre de séjour et d'obligation de quitter le territoire français sont intervenues en violation de l'article L. 313-11-7 du code de l'entrée et du séjour des étrangers et du droit d'asile ; qu'en effet, elle réside en France depuis le 22 septembre 2007, soit depuis plus de deux ans, entretient des relations suivies avec ses deux soeurs qui résident en France, l'une étant de nationalité française, l'autre en possession d'une carte de résident, et a tissé de nombreux liens privés en France ; en quatrième lieu, que les décisions attaquées ont méconnu l'article L. 313-14 du code de l'entrée et du séjour des étrangers et du droit d'asile dès lors qu'une partie de la famille de l'exposante réside sur le territoire français ; en cinquième lieu, que ces décisions sont intervenues en violation de l'article 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ; enfin, que l'arrêté attaqué est entaché d'une erreur manifeste d'appréciation de ses conséquences sur la situation de l'exposante compte tenu de ses attaches familiales et privées en France ; qu'elle ne peut retourner dans son pays d'origine sans risque pour sa sécurité ; <br/>
<br/>
...........................................................................................................................................................<br/>
<br/>
       Vu les autres pièces du dossier ;<br/>
<br/>
       Vu la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
<br/>
       Vu le code de l'entrée et du séjour des étrangers et du droit d'asile ; <br/>
<br/>
       Vu la loi n° 91-647 du 10 juillet 1991 ;<br/>
<br/>
       Vu le code de justice administrative ;<br/>
<br/>
       Les parties ayant été régulièrement averties du jour de l'audience ;<br/>
<br/>
       Après avoir entendu au cours de l'audience publique du 15 décembre 2009 :<br/>
<br/>
       - le rapport de Mme Signerin-Icre, président assesseur,<br/>
       - les conclusions de Mme Jarreau, rapporteur public,<br/>
       - et les observations de Me Lavanant, pour Mlle A ;<br/>
<br/>
<br/>
       Considérant que Mlle A, ressortissante haïtienne, fait appel de l'ordonnance du 7 janvier 2009 par laquelle le président du Tribunal administratif de Cergy-Pontoise a rejeté sa demande tendant à l'annulation de l'arrêté du 22 octobre 2008 du préfet du Val-d'Oise refusant de lui délivrer un titre de séjour et lui faisant obligation de quitter le territoire français à destination de son pays d'origine ;<br/>
<br/>
<br/>
       Sur la régularité de l'ordonnance attaquée :<br/>
<br/>
       Considérant que, contrairement à ce que soutient Mlle A, l'ordonnance attaquée est suffisamment motivée compte tenu de la teneur de l'argumentation soulevée devant le tribunal administratif ;<br/>
<br/>
       Sur la légalité de l'arrêté attaqué :<br/>
<br/>
       Considérant, en premier lieu, qu'aux termes de l'article L. 511-1 du code de l'entrée et du séjour des étrangers et du droit d'asile :  L'autorité administrative qui refuse la délivrance ou le renouvellement d'un titre de séjour (...) peut assortir sa décision d'une obligation de quitter le territoire français (...). L'obligation de quitter le territoire français n'a pas à faire l'objet d'une motivation  ; que, dès lors, Mlle A ne peut utilement soutenir que l'obligation de quitter le territoire français, dont a été assorti le refus de séjour qui lui a été opposé, ne serait pas suffisamment motivée ;<br/>
<br/>
       Considérant, en deuxième lieu, qu'aux termes de l'article 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales :  1° Toute personne a droit au respect de sa vie privée et familiale, de son domicile et de sa correspondance. / 2° Il ne peut y avoir ingérence d'une autorité publique dans l'exercice de ce droit que pour autant que cette ingérence est prévue par la loi et qu'elle constitue une mesure qui, dans une société démocratique, est nécessaire à la sécurité nationale, à la sûreté publique, au bien-être économique du pays, à la défense de l'ordre et à la prévention des infraction pénales, à la protection de la santé ou de la morale, ou à la protection des droits et libertés d'autrui  ; qu'aux termes de l'article L. 313-11 du code de l'entrée et du séjour des étrangers et du droit d'asile :  Sauf si sa présence constitue une menace pour l'ordre public, la carte de séjour temporaire portant la mention  vie privée et familiale  est délivrée de plein droit : (...) 7° A l'étranger ne vivant pas en état de polygamie, qui n'entre pas dans les catégories précédentes ou dans celles qui ouvrent droit au regroupement familial, dont les liens personnels et familiaux en France, appréciés notamment au regard de leur intensité, de leur ancienneté et de leur stabilité, des conditions d'existence de l'intéressé, de son insertion dans la société française ainsi que de la nature de ses liens avec la famille restée dans le pays d'origine, sont tels que le refus d'autoriser son séjour porterait à son droit au respect de sa vie privée et familiale une atteinte disproportionnée au regard des motifs du refus, sans que la condition prévue à l'article L. 311-7 soit exigée. L'insertion de l'étranger dans la société française est évaluée en tenant compte notamment de sa connaissance des valeurs de la République  ;<br/>
<br/>
       Considérant que Mlle A fait valoir que, résidant en France depuis le 22 septembre 2007, elle a tissé de nombreux liens privés dans ce pays et fait état de la présence de ses deux soeurs, dont l'une est de nationalité française et l'autre titulaire d'une carte de résident ; que, toutefois, il ne ressort pas des pièces du dossier que, dans les circonstances de l'espèce, compte tenu de la courte durée du séjour en France de la requérante et de la circonstance qu'elle n'établit pas être dépourvue de toute attache dans son pays d'origine où, selon ses propres déclarations, réside notamment sa fille, l'arrêté du 22 octobre 2008 par lequel le préfet du Val-d'Oise a refusé de lui délivrer un titre de séjour et lui a fait obligation de quitter le territoire français, aurait porté au droit de l'intéressée au respect de sa vie familiale une atteinte disproportionnée aux buts en vue desquels il a été pris, en violation des stipulations de l'article 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ou des dispositions du 7° de l'article L. 313-11 du code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
<br/>
       Considérant, en troisième lieu, qu'il ressort des pièces du dossier que Mlle A n'a pas sollicité la délivrance d'un titre de séjour sur le fondement de l'article L. 313-14 du code de l'entrée et du séjour des étrangers et du droit d'asile ; que, par suite, elle ne peut utilement se prévaloir des dispositions dudit article, qui ne prévoient pas la délivrance d'un titre de séjour de plein droit, pour contester la légalité de l'arrêté attaqué ;<br/>
<br/>
       Considérant, en quatrième lieu, qu'il ne ressort pas des pièces du dossier que le préfet du Val-d'Oise aurait commis une erreur manifeste d'appréciation des conséquences de son arrêté sur la situation personnelle de la requérante ;<br/>
<br/>
       Considérant, enfin, que si Mlle A soutient qu'en raison de l'engagement politique de son concubin, qui aurait été assassiné en mai 2006, elle serait exposée à des risques pour sa sécurité en cas de retour en Haïti, elle n'apporte aucun élément probant de nature à établir la réalité de ces risques, dont ni l'Office français de protection des réfugiés et apatrides, ni la Commission des recours des réfugiés n'ont d'ailleurs retenu l'existence ; qu'ainsi, le moyen tiré de ce que l'arrêté contesté, en tant qu'il prévoit son renvoi à destination de son pays d'origine, serait contraire aux stipulations de l'article 3 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ne peut qu'être écarté ;<br/>
<br/>
       Considérant qu'il résulte de ce qui précède que Mlle A n'est pas fondée à soutenir que c'est à tort que, par l'ordonnance attaquée, le président du Tribunal administratif de Cergy-Pontoise a rejeté sa demande ; que, par voie de conséquence, ses conclusions aux fins d'injonction et celles tendant à l'application de l'article L. 761-1 du code de justice administrative doivent également être rejetées ;<br/>
<br/>
<br/>
DECIDE :<br/>
<br/>
<br/>
       Article 1er : La requête de Mlle A est rejetée.<br/>
<br/>
''<br/>
''<br/>
''<br/>
''<br/>
N° 09VE00356	2<br/>
<br/>
<br/>
<br/>
<br/>
</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
