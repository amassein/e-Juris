<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037942853</ID>
<ANCIEN_ID>JG_L_2018_12_000000410016</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/94/28/CETATEXT000037942853.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème chambre, 31/12/2018, 410016, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-12-31</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>410016</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FOUSSARD, FROGER ; LE PRADO ; SCP THOUVENIN, COUDRAY, GREVY ; SCP SEVAUX, MATHONNET</AVOCATS>
<RAPPORTEUR>Mme Louise Cadin</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Cécile Barrois de Sarigny</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:410016.20181231</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Procédure contentieuse antérieure<br/>
<br/>
              M. A...B...a demandé au tribunal administratif de Nantes de condamner le centre hospitalier de Saint-Nazaire à lui verser la somme de 277 000 euros en réparation du préjudice que lui a causé une complication postopératoire consécutive à une intervention chirurgicale pratiquée dans cet établissement le 19 octobre 2004. Par un jugement n° 0701710 du 15 juin 2011, le tribunal administratif de Nantes a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 11NT02450 du 20 décembre 2012, la cour administrative d'appel de Nantes a rejeté l'appel formé par M. B...contre ce jugement.<br/>
<br/>
              Par une décision n° 366415 du 30 décembre 2014, le Conseil d'Etat, statuant au contentieux, a annulé cet arrêt et renvoyé l'affaire à la cour administrative d'appel de Nantes.<br/>
<br/>
              Par un arrêt n° 15NT00255 du 22 février 2017, la cour administrative d'appel de Nantes a condamné le centre hospitalier de Saint-Nazaire à verser la somme de 44 900 euros à M. B..., la somme de 90 000 euros à la caisse primaire d'assurance maladie de Loire-Atlantique et la somme de 1 500 euros à la Mutuelle nationale territoriale.<br/>
<br/>
              Procédure devant le Conseil d'Etat<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 24 avril et 21 juillet 2017 au secrétariat du contentieux du Conseil d'Etat, le centre hospitalier de Saint-Nazaire demande au Conseil d'Etat d'annuler cet arrêt.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de la sécurité sociale ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Louise Cadin, auditrice,<br/>
<br/>
              - les conclusions de Mme Cécile Barrois de Sarigny, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Le Prado, avocat du centre hospitalier de Saint-Nazaire, à la SCP Thouvenin, Coudray, Grévy, avocat de M.B..., à la SCP Foussard, Froger, avocat de la caisse primaire d'assurance maladie de la Loire-Atlantique et à la SCP Sevaux, Mathonnet, avocat de l'ONIAM.<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que M. A... B..., né le 3 mai 1958, a subi le 19 octobre 2004, au centre hospitalier de Saint-Nazaire, une intervention chirurgicale consistant en un pontage fémoro-poplité sur la jambe gauche ; qu'après sa sortie de l'hôpital le 22 octobre suivant, il a été victime d'un érysipèle veino-lymphatique dont il a conservé d'importantes séquelles ; que M. B...a saisi le tribunal administratif de Nantes d'une demande tendant à la condamnation du centre hospitalier à lui verser la somme de 277 000 euros en réparation de ses préjudices ; que, par un jugement du 15 juin 2011, le tribunal administratif a rejeté sa demande ; que, par un arrêt du 20 décembre 2012, la cour administrative d'appel de Nantes a rejeté l'appel formé par M. B... contre ce jugement ; que, par une décision du 30 décembre 2014, le Conseil d'Etat statuant au contentieux a annulé cet arrêt et renvoyé l'affaire à la cour ; que, par un arrêt du 22 février 2017, celle-ci a retenu que la responsabilité du centre hospitalier de Saint-Nazaire était engagée à raison de l'infection nosocomiale contractée par M. B...lors de l'intervention du 19 octobre 2004 puis a, d'une part, estimé que l'intéressé ne justifiait d'aucun préjudice patrimonial et, en particulier, n'avait subi ni perte de revenus, ni incidence professionnelle, et, d'autre part, évalué ses préjudices extra-patrimoniaux à la somme de 44 900 euros qu'elle a condamné l'établissement à lui verser ; que la cour a, par ailleurs, accordé à la caisse primaire d'assurance maladie (CPAM) de Loire-Atlantique une somme de 90 000 euros correspondant à des dépenses de santé prises en charge par la caisse et à une pension d'invalidité ainsi que des indemnités journalières versées par elle à M. B...; que le centre hospitalier de Saint-Nazaire se pourvoit en cassation contre cet arrêt, en soutenant que les juges du fond ne pouvaient le condamner à rembourser à la caisse la pension d'invalidité et les indemnités journalières alors qu'elle retenait que la victime n'avait subi aucun préjudice professionnel ; qu'en réponse à la communication du pourvoi, M. B...a demandé que l'arrêt soit annulé en tant qu'il statue sur son préjudice professionnel ; <br/>
<br/>
              2. Considérant qu'il résulte des dispositions de l'article L. 376-1 du code de la sécurité sociale que les prestations versées par un organisme de sécurité sociale à la suite d'un accident corporel ne peuvent être mis à la charge du responsable que par imputation sur le ou les postes de préjudice que ces prestations ont eu pour objet de réparer ; que les indemnités journalières ont pour objet de réparer les pertes de revenus subies par la victime pendant la période d'incapacité temporaire ; qu'eu égard à la finalité de réparation d'une incapacité permanente de travail qui lui est assignée par les dispositions de l'article L. 341-1 du code de la sécurité sociale et à son mode de calcul, en fonction du salaire, fixé par l'article R. 341-4 du même code, la pension d'invalidité doit être regardée comme ayant pour objet exclusif de réparer, sur une base forfaitaire, les préjudices subis par la victime dans sa vie professionnelle en conséquence de l'accident, c'est-à-dire les pertes de revenus professionnels résultant de l'incapacité permanente et l'incidence professionnelle de cette incapacité ; <br/>
<br/>
              3. Considérant qu'il appartenait, par suite, aux juges du fond, pour statuer sur les conclusions de M. B...tendant à la réparation de ses préjudices professionnels et sur celles de la CPAM de Loire-Atlantique tendant au remboursement des indemnités journalières et de la pension d'invalidité, de déterminer si l'infection nosocomiale avait causé, d'une part, des pertes de revenus professionnels pendant la période d'incapacité temporaire et, d'autre part, des pertes de revenus et une incidence professionnelle après consolidation de l'état de santé de la victime ; que, dans l'affirmative, il y avait lieu d'évaluer chacun de ces postes de préjudice, sans tenir compte à ce stade du fait qu'ils avaient donné lieu à des indemnités journalières et à une pension d'invalidité, et de mettre à la charge du centre hospitalier de Saint-Nazaire, dont l'entière responsabilité était engagée, le versement à M.B..., le cas échéant, de la part de ces postes qui n'avait pas été réparée par ces prestations et le versement à la caisse du surplus de chaque poste, s'il existait ; <br/>
<br/>
              4. Considérant qu'en retenant que M. B...n'avait subi aucun préjudice professionnel, puis en mettant à la charge du centre hospitalier de Saint-Nazaire le versement à la CPAM de Loire-Atlantique d'une indemnité de 90 000 euros correspondant pour partie à des indemnités journalières et à une pension d'invalidité, la cour n'a pas suivi la méthode définie ci-dessus ; qu'elle a ainsi commis une erreur de droit qui justifie que son arrêt soit annulé en tant qu'il se prononce sur les préjudices patrimoniaux imputables à l'infection nosocomiale et statue sur les sommes dues à ce titre tant à la CPAM de Loire-Atlantique qu'à M.B... ; que l'arrêt doit également être annulé, par voie de conséquence, en tant qu'il statue sur la demande de la caisse primaire tendant au versement de l'indemnité forfaitaire de gestion et sur ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
              5. Considérant que, dès lors que l'arrêt est annulé en tant qu'il se prononce sur les préjudices patrimoniaux imputables à l'infection nosocomiale et statue sur les sommes dues à ce titre tant à la CPAM de Loire-Atlantique qu'à M.B..., les conclusions présentées par ce dernier, tendant à ce qu'il soit annulé dans la même mesure, perdent leur objet ; <br/>
<br/>
              6. Considérant qu'aux termes du second alinéa de l'article L. 821-2 du code de justice administrative : " Lorsque l'affaire fait l'objet d'un second pourvoi en cassation, le Conseil d'Etat statue définitivement sur cette affaire " ; que, le Conseil d'Etat étant saisi, en l'espèce, d'un second pourvoi en cassation, il lui incombe de régler l'affaire au fond ; que, toutefois, il y a lieu de surseoir à statuer sur ce règlement afin de procéder à un complément d'instruction ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Nantes du 22 février 2017 est annulé en tant qu'il se prononce sur les préjudices patrimoniaux de M. B...et statue sur les sommes dues à ce titre tant à la CPAM de Loire-Atlantique qu'à M. B...et en tant qu'il statue sur la demande de la caisse primaire tendant au versement de l'indemnité forfaitaire de gestion et sur ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 2 : Il n'y a pas lieu de statuer sur le pourvoi de M.B....<br/>
Article 3 : Il est sursis à statuer sur le règlement au fond de l'affaire.<br/>
Article 4 : La présente décision sera notifiée au centre hospitalier de Saint-Nazaire, à la caisse primaire d'assurance maladie de Loire-Atlantique et à M. A...B....<br/>
Copie en sera adressée à la Mutuelle nationale territoriale et à l'Office national d'indemnisation des accidents médicaux, des affections iatrogènes et des infections nosocomiales.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
