<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043096239</ID>
<ANCIEN_ID>JG_L_2021_02_000000437834</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/09/62/CETATEXT000043096239.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème chambre, 03/02/2021, 437834, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-02-03</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>437834</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>LE PRADO</AVOCATS>
<RAPPORTEUR>M. Vincent Daumas</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Laurent Cytermann</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:437834.20210203</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              M. B... A... a demandé au tribunal administratif de Cergy-Pontoise de prononcer la décharge, en droits et pénalités, des cotisations supplémentaires d'impôt sur le revenu et de contributions sociales auxquelles il a été assujetti au titre des années 2010, 2011 et 2012. Par un jugement n° 1510985 du 22 juin 2018, le tribunal administratif de Cergy-Pontoise a réduit ses bases d'imposition à l'impôt sur le revenu et aux contributions sociales à hauteur de 247,30 euros au titre de l'année 2010, de 369,24 euros au titre de l'année 2011 et de 372,64 euros au titre de l'année 2012, l'a déchargé des cotisations supplémentaires d'impôt sur le revenu et de contributions sociales à concurrence, et a rejeté le surplus de sa demande.<br/>
<br/>
              Par un arrêt n° 18VE02532 du 21 novembre 2019, la cour administrative d'appel de Versailles a prononcé un non-lieu à statuer à hauteur de 34 875 euros, a rejeté le surplus de l'appel formé par M. A... et a partiellement fait droit à l'appel incident du ministre de l'action et des comptes publics.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 21 janvier, 21 avril et 4 décembre 2020 au secrétariat du contentieux du Conseil d'Etat, M. A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Vincent Daumas, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Laurent Cytermann, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à Me Le Prado, avocat de M. A... ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond qu'à la suite d'un contrôle sur pièces, l'administration a regardé M. A..., associé principal de la société à responsabilité limitée (SARL) Ditis qui a été successivement le gérant de droit puis le gérant de fait de cette société, comme ayant été le bénéficiaire des revenus réputés distribués correspondant notamment à des dépenses de la société dont la déductibilité avait été remise en cause à la suite d'une vérification de comptabilité et, par voie de conséquence, a assujetti l'intéressé à des suppléments d'impôt sur le revenu et de contributions sociales au titre des années 2010 à 2012, ainsi qu'à des majorations pour manquement délibéré. Par un jugement du 22 juin 2018, le tribunal administratif de Cergy-Pontoise n'a que très partiellement fait droit à la demande de décharge de M. A.... Celui-ci se pourvoit en cassation contre l'arrêt du 21 novembre 2019 par lequel la cour administrative d'appel de Versailles, après avoir prononcé un non-lieu à statuer partiel faisant suite à un dégrèvement en cours d'instance, a rejeté le surplus des conclusions de sa requête d'appel.<br/>
<br/>
              2. En premier lieu, en vertu du paragraphe 5 du chapitre III de la charte des droits et obligations du contribuable vérifié, si le vérificateur a maintenu totalement ou partiellement les redressements envisagés, des éclaircissements supplémentaires peuvent être fournis au contribuable qui le demande par l'inspecteur principal. Ces dispositions assurent au contribuable la garantie substantielle de pouvoir obtenir, avant la clôture de la procédure de redressement, un débat avec le supérieur hiérarchique du vérificateur sur les points où persiste un désaccord avec ce dernier. L'utilité d'un tel débat n'est pas affectée par la circonstance que ledit supérieur hiérarchique ait, éventuellement, signé ou visé l'un des documents qui ont été notifiés au contribuable depuis l'engagement de la procédure de redressement. La cour administrative d'appel n'a, par suite et en tout état de cause, pas commis d'erreur de droit en jugeant que M. A... n'avait pas été privé de la garantie prévue par les dispositions de la charte du fait qu'en l'espèce, le supérieur hiérarchique auquel il lui avait été indiqué qu'il pourrait faire appel avait signé ou visé l'un des documents qui lui ont été notifiés. <br/>
<br/>
              3. En deuxième lieu, aux termes du 1 de l'article 109 du code général des impôts, dont les dispositions sont relatives aux revenus assimilés aux produits d'actions ou de parts sociales : " Sont considérés comme revenus distribués : / 1° Tous les bénéfices ou produits qui ne sont pas mis en réserve ou incorporés au capital ; / (...) ". Aux termes de l'article 111 de ce code : " Sont notamment considérés comme revenus distribués : (...) c. Les rémunérations et avantages occultes (...) ". En cas de refus des propositions de rectifications par le contribuable qu'elle entend imposer comme bénéficiaire de sommes regardées comme distribuées par une société, il incombe à l'administration d'apporter la preuve que celui-ci en a effectivement disposé, sauf à démontrer qu'il peut être regardé comme le maître de l'affaire. <br/>
<br/>
              4. La cour administrative d'appel a relevé, par une appréciation souveraine exempte de dénaturation, s'agissant de chacune des six séries de dépenses de la société Ditis dont la déductibilité a été remise en cause, à savoir des frais de réception et de restauration, des frais de déplacement, des charges de copropriété d'un appartement où M. A... avait établi son domicile personnel, des frais de logistiques liés au stockage de documents dans une de ses résidences secondaires, l'acquisition auprès de lui de documents d'information et des remboursements de frais, qu'elles avaient directement profité à M. A.... Dès lors, après s'être prononcée sur l'existence et le montant des sommes en cause, la cour n'était pas tenue, pour établir qu'elles avaient été appréhendées par l'intéressé, de rechercher s'il était le maître de l'affaire. Par suite, les moyens d'erreur de droit, d'insuffisance de motivation, d'erreur de qualification juridique et de dénaturation qui sont dirigés contre le motif, nécessairement surabondant, par lequel la cour a relevé que le ministre avait soutenu, sans être contredit, que M. A... était associé majoritaire au sein de la SARL Ditis dont il a été le gérant de droit au cours d'une partie des années d'impositions en litige, et qu'il bénéficiait de la signature sociale sur ses comptes bancaires, ne peuvent qu'être écartés comme inopérants.<br/>
<br/>
              5. En troisième lieu, la cour n'a dénaturé les pièces du dossier qui lui était soumis ni en estimant que la partie des frais de réception et de restauration correspondant à des déplacements le week-end en Ardèche, où M. A... disposait de résidences secondaires, n'était pas liée au projet d'implantation d'un pôle d'activité numérique dont la société Ditis avait la charge, ni en jugeant, sans commettre par ailleurs d'erreur de droit, que l'intérêt économique de stocker des documents de cette société à plusieurs centaines de kilomètres de son siège social, dans l'une des résidences secondaire de M. A... en Ardèche, n'était pas établi. <br/>
<br/>
              6. En quatrième et dernier lieu, en se fondant, pour regarder comme justifiée l'application de la majoration de 40 % prévue au a de l'article 1729 du code général des impôts aux impositions relatives aux deux dernières séries de dépenses mentionnées au point 4, sur la circonstance que l'administration faisait valoir, sans être sérieusement contredite, qu'au regard des conditions d'exploitation de la société Ditis, dont M. A..., titulaire de la signature sur les comptes bancaires sociaux, était à la fois l'associé principal et le gérant de droit, puis de fait au cours des années d'imposition en litige, celui-ci ne pouvait ignorer l'absence de caractère professionnel des nombreuses charges supportées par la société, la cour n'a pas inexactement qualifié les faits qui lui étaient soumis et n'a pas entaché son arrêt d'insuffisance de motivation. <br/>
<br/>
              7. Il résulte de tout ce qui précède que M. A... n'est pas fondé à demander l'annulation de l'arrêt qu'il attaque. <br/>
<br/>
              8. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante. <br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de M. A... est rejeté.<br/>
Article 2 : La présente décision sera notifiée à M. B... A... et au ministre de l'économie, des finances et de la relance.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
