<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029255207</ID>
<ANCIEN_ID>JG_L_2014_07_000000368784</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/25/52/CETATEXT000029255207.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  4ème sous-section jugeant seule, 16/07/2014, 368784, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-07-16</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>368784</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 4ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Benjamin de Maillard</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Gaëlle Dumortier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2014:368784.20140716</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête et le mémoire complémentaire, enregistrés les 23 mai et 13 juin 2013 au secrétariat du contentieux du Conseil d'Etat, présentés par la SAS La Tourelle, dont le siège est 30-32, rue de la Tourelle à Longpont-sur-Orge (91310), représentée par son président directeur général en exercice ; la SAS La Tourelle demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision n° 1645T-1672T du 26 février 2013 par laquelle la Commission nationale d'aménagement commercial, sur recours des sociétés Sadef, Martichel, Alcyon et Equata a refusé à la SAS La Tourelle l'autorisation de procéder à la création d'un magasin de bricolage de l'enseigne " Brico Dépôt " de 7 310 m² de surface de vente à Benet (Vendée) ; <br/>
<br/>
              2°) d'enjoindre à la Commission nationale d'aménagement commercial de statuer à nouveau sur la demande dans un délai de quatre mois à compter de la notification de l'arrêt à intervenir ;<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de commerce ;<br/>
<br/>
              Vu la loi n° 73-1193 du 27 décembre 1973 ;<br/>
<br/>
              Vu la loi n° 2008-776 du 4 août 2008 ;<br/>
<br/>
              Vu le décret n° 2008-1212 du 24 novembre 2008 ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Benjamin de Maillard, auditeur,  <br/>
<br/>
              - les conclusions de Mme Gaëlle Dumortier, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>Sur la légalité de la décision attaquée : <br/>
<br/>
              1. Considérant qu'il appartient aux commissions d'aménagement commercial, lorsqu'elles se prononcent sur un projet d'exploitation commerciale soumis à autorisation en application de l'article L. 752-1 du code de commerce, d'apprécier la conformité de ce projet aux objectifs prévus à l'article 1er de la loi du 27 décembre 1973 et à l'article L. 750-1 du code de commerce, au vu des critères d'évaluation mentionnés à l'article L. 752-6 du code de commerce et précisés à l'article R. 752-7 du même code ; que l'autorisation ne peut être refusée que si, eu égard à ses effets, le projet compromet la réalisation de ces objectifs ; <br/>
<br/>
              2. Considérant qu'il ressort des pièces du dossier que le terrain d'assiette du projet se situe en bordure d'un axe routier et à proximité immédiate d'une zone d'activités économiques, qui a vocation, en vertu du plan local d'urbanisme, à être urbanisé à court et moyen terme pour y installer des activités industrielles et commerciales ; qu'il permettra de résorber une friche industrielle et agricole ; qu'en développant une offre commerciale de proximité, il contribuera à limiter les déplacements de la clientèle vers les pôles commerciaux plus éloignés ; qu'ainsi, la requérante est fondée à soutenir qu'en refusant l'autorisation aux motifs que le projet " entraînera un étalement urbain important " et " ne participera pas à un aménagement équilibré du territoire ", la commission nationale a fait une inexacte application des dispositions précitées ; <br/>
<br/>
              3. Considérant, que si le projet n'est pas inséré dans les réseaux de transport collectifs et n'est pas desservi par les pistes cyclables, cette circonstance ne justifie pas, en l'espèce, compte tenu notamment de la nature des produits vendus dans ce magasin de bricolage et de l'aménagement prévu d'un cheminement piétonnier, le refus de l'autorisation sollicitée ; qu'il ne ressort pas des pièces du dossier que la réalisation du projet comporte un risque spécifique d'imperméabilisation des sols ; que la société pétitionnaire a prévu la réalisation d'aménagements permettant la conservation des espaces naturels ainsi que la création d'une noue d'infiltration en vue d'améliorer la gestion de l'écoulement des eaux ; que l'insertion paysagère du projet est suffisamment assurée par les caractéristiques architecturales des bâtiments, ainsi que par la place accordée aux espaces verts, qui représenteront plus du tiers de la superficie totale du terrain d'implantation ; que, par suite, le projet ne compromet pas l'objectif de développement durable ; que, dès lors, la requérante est fondée à soutenir qu'en refusant l'autorisation aux motifs que le projet " entraînera une imperméabilisation importante des sols " et que son insertion dans l'" environnement est insuffisante ", la commission nationale a fait une inexacte application des dispositions précitées ; <br/>
<br/>
              5. Considérant qu'il résulte de tout ce qui précède que la requérante est fondée à soutenir que la commission nationale a fait une inexacte application des dispositions précitées en estimant que le projet aurait compromis la réalisation des objectifs prévus par la loi ; <br/>
<br/>
              6. Considérant que si les sociétés Alcyon et Martichel demandent qu'aux motifs erronés de la décision attaqué soit substitué un motif tiré de ce qu'aucune dérogation au titre de l'article L. 122-2 du code de l'urbanisme n'avait été accordée à la SAS La Tourelle, il n'y a pas lieu de procéder à une telle substitution de motif, qui ne peut être demandée au juge de l'excès de pouvoir que par l'administration auteur de la décision attaquée, laquelle s'est abstenue de produire à l'instance ; qu'au demeurant, le moyen tiré de l'absence de dérogation manque en fait ;<br/>
<br/>
              7. Considérant qu'il résulte de ce qui précède que, sans qu'il soit besoin d'examiner les autres moyens de la requête, la SAS La Tourelle est fondée à demander l'annulation de la décision attaquée ;<br/>
<br/>
              Sur les conclusions aux fins d'injonction : <br/>
<br/>
              8. Considérant que la présente décision implique nécessairement que la Commission nationale d'aménagement commercial procède à un nouvel examen de la demande d'autorisation dont elle se trouve à nouveau saisie, dans un délai de quatre mois à compter de la notification de la présente décision ;<br/>
<br/>
              Sur les conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              9. Considérant que ces dispositions font obstacle à ce que les sommes que demandent la société Sadef, la société Alcyon et la société Martichel soient mises à la charge de la SAS La Tourelle, qui n'est pas, dans la présente instance, la partie perdante ; <br/>
<br/>
<br/>				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er : La décision de la Commission nationale d'aménagement commercial du 26 février 2013 est annulée.<br/>
<br/>
Article 2 : La Commission nationale d'aménagement commercial réexaminera, dans un délai de quatre mois à compter de la notification de la présente décision, la demande d'autorisation de la SAS La Tourelle. <br/>
<br/>
Article 3 : Les conclusions présentées par la société Sadef et par les sociétés Alcyon et Martichel au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 4 : La présente décision sera notifiée à la SAS La Tourelle, à la société Sadef, à la société Alcyon et à la société Martichel.<br/>
Copie en sera adressée pour information à la société Equata et à la Commission nationale d'aménagement commercial. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
