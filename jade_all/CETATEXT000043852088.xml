<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043852088</ID>
<ANCIEN_ID>JG_L_2021_07_000000441352</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/85/20/CETATEXT000043852088.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère chambre, 22/07/2021, 441352, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-07-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>441352</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP THOUVENIN, COUDRAY, GREVY ; SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR>Mme Manon Chonavel</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Vincent Villette</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:441352.20210722</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 22 juin et 22 septembre 2020 et 7 avril 2021 au secrétariat du contentieux du Conseil d'Etat, le syndicat national unitaire travail, emploi, formation, insertion (SNU-TEFI), le comité social et économique de l'établissement de Pôle emploi de la région Occitanie et le syndicat CGT Pôle emploi de la région Occitanie demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir l'instruction interne n° 2020-10 du 20 avril 2020 du directeur général adjoint de Pôle emploi chargé des ressources humaines et des relations sociales, intitulée " Prise de JRTT/JNTP/CET et congés pendant la crise sanitaire du covid 19 " ;<br/>
<br/>
              2°) de mettre à la charge de Pôle emploi la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la Constitution, notamment son Préambule ;<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales et son premier protocole additionnel ; <br/>
              - la charte des droits fondamentaux de l'Union européenne ;<br/>
              - la directive 2003/88/CE du Parlement européen et du Conseil du 4 novembre 2003 ;<br/>
              - l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
              - le code du travail ;<br/>
              - l'ordonnance n° 2020-323 du 25 mars 2020 ;<br/>
              - l'ordonnance n° 2020-389 du 1er avril 2020 ;<br/>
              - l'ordonnance n° 2020-430 du 15 avril 2020 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme A... B..., auditrice,  <br/>
<br/>
              - les conclusions de M. Vincent Villette, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Thouvenin, Coudray, Grevy, avocat du syndicat national unitaire travail, emploi, formation, insertion et autres et à la SCP Piwnica, Molinié, avocat de Pôle emploi ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. L'émergence d'un nouveau coronavirus, responsable de la maladie à coronavirus 2019 ou covid-19 et particulièrement contagieux, et sa propagation sur le territoire français à compter du début de l'année 2020 ont conduit les pouvoirs publics à prendre diverses mesures de lutte contre l'épidémie. Par un décret du 16 mars 2020 motivé par les circonstances exceptionnelles découlant de l'épidémie de covid-19, le Premier ministre a interdit, à compter du lendemain midi, le déplacement de toute personne hors de son domicile, sous réserve d'exceptions limitativement énumérées et devant être dûment justifiées. Dans le même temps, l'activité de nombreuses administrations a été réduite aux missions les plus essentielles dans le cadre de la mise en oeuvre de plans de continuité d'activité, les agents dont la présence sur leur lieu de travail n'était pas nécessaire à cette fin étant invités à télétravailler ou, en cas d'impossibilité, autorisés à s'absenter. Par l'article 4 de la loi du 23 mars 2020 d'urgence pour faire face à l'épidémie de covid-19, a été déclaré l'état d'urgence sanitaire pour une durée de deux mois sur l'ensemble du territoire national.<br/>
<br/>
              2. L'article 11 de la même loi a autorisé le Gouvernement, pendant trois mois, à prendre par ordonnances, dans les conditions prévues à l'article 38 de la Constitution, " toute mesure, pouvant entrer en vigueur, si nécessaire, à compter du 12 mars 2020, relevant du domaine de la loi (...) : / 1° Afin de faire face aux conséquences économiques, financières et sociales de la propagation de l'épidémie de covid-19 et aux conséquences des mesures prises pour limiter cette propagation, et notamment afin de prévenir et limiter la cessation d'activité des personnes physiques et morales exerçant une activité économique et des associations ainsi que ses incidences sur l'emploi (...) / b) En matière de droit du travail, de droit de la sécurité sociale et de droit de la fonction publique ayant pour objet : / (...) - de permettre à tout employeur d'imposer ou de modifier unilatéralement les dates des jours de réduction du temps de travail, des jours de repos prévus par les conventions de forfait et des jours de repos affectés sur le compte épargne temps du salarié, en dérogeant aux délais de prévenance et aux modalités d'utilisation définis au livre Ier de la troisième partie du code du travail, par les conventions et accords collectifs ainsi que par le statut général de la fonction publique (...) ". Cette habilitation est arrivée à son terme le 23 juin 2020.<br/>
<br/>
              3. Sur le fondement de cette habilitation, le Gouvernement a pris l'ordonnance du 25 mars 2020 portant mesures d'urgence en matière de congés payés, de durée du travail et de jours de repos, qui déroge aux délais de prévenance et aux modalités d'utilisation définis par le code du travail et les conventions et accords collectifs, et l'ordonnance du 15 avril 2020 relative à la prise de jours de réduction du temps de travail ou de congés dans la fonction publique de l'Etat et la fonction publique territoriale au titre de la période d'urgence sanitaire.<br/>
<br/>
              4. Les requérants demandent l'annulation pour excès de pouvoir de l'instruction n° 2020-10 du 20 avril 2020 prise à la suite de ces deux ordonnances par le directeur général adjoint de Pôle emploi chargé des ressources humaines et des relations sociales et relative aux jours de repos des agents de l'institution pendant la crise sanitaire liée à l'épidémie de covid-19.<br/>
<br/>
              Sur les dispositions relatives aux agents de droit privé :<br/>
<br/>
              5. L'article 2 de l'ordonnance du 25 mars 2020 portant mesures d'urgence en matière de congés payés, de durée du travail et de jours de repos prévoit que, lorsque l'intérêt de l'entreprise le justifie eu égard aux difficultés économiques liées à la propagation du covid-19, l'employeur peut, sous réserve de respecter un délai de prévenance d'au moins un jour franc, imposer la prise, à des dates déterminées par lui, de jours de réduction du temps de travail ou modifier unilatéralement les dates de prise de ces jours. Les articles 3 et 4 de l'ordonnance comportent des dispositions identiques respectivement pour les jours de repos prévus par les conventions de forfait et pour les jours de repos affectés sur un compte épargne temps.<br/>
<br/>
              6. En application de ces dispositions, l'instruction attaquée prévoit que les agents de droit privé de Pôle emploi placés en " absence autorisée payée " entre le 16 mars et le 7 mai 2020 prendront au cours de cette période dix jours de réduction du temps de travail, de jours de repos prévus par une convention de forfait ou de jours de repos affectés sur un compte épargne temps.<br/>
<br/>
              En ce qui concerne la légalité externe : <br/>
<br/>
              7. Aux termes de l'article 5 de l'ordonnance du 25 mars 2020 portant mesures d'urgence en matière de congés payés, de durée du travail et de jours de repos, dans sa rédaction résultant de l'ordonnance du 1er avril 2020 portant mesures d'urgence relatives aux instances représentatives du personnel : " L'employeur qui use de la faculté offerte aux articles 2, 3 ou 4 de la présente ordonnance en informe le comité social et économique sans délai et par tout moyen. L'avis du comité est rendu dans le délai d'un mois à compter de cette information. Il peut intervenir après que l'employeur a fait usage de cette faculté ".<br/>
<br/>
              8. Il résulte de ces dispositions que le comité social et économique peut être consulté après que l'employeur a fait usage de la faculté offerte par les articles 2, 3 ou 4 de l'ordonnance du 25 mars 2020. En l'espèce, le comité social et économique central de Pôle emploi a été informé de la décision de l'employeur le 21 avril 2020 et a rendu son avis le 20 mai 2020. Par suite, les requérants ne sont pas fondés à soutenir que l'instruction attaquée, dont la légalité s'apprécie en tout état de cause à la date à laquelle elle a été prise, aurait été adoptée au terme d'une procédure irrégulière faute de consultation du comité social et économique.<br/>
<br/>
              En ce qui concerne la méconnaissance par l'ordonnance du 25 mars 2020 de droits et libertés garantis par la Constitution :<br/>
<br/>
              9. Lorsque le délai d'habilitation est expiré, la contestation, au regard des droits et libertés que la Constitution garantit, des dispositions d'une ordonnance relevant du domaine de la loi n'est recevable qu'au travers d'une question prioritaire de constitutionnalité, qui doit être transmise au Conseil constitutionnel si les conditions fixées par les articles 23-2, 23-4 et 23-5 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel sont remplies.<br/>
<br/>
              10. Les dispositions de l'ordonnance du 25 mars 2020 contestées par la voie de l'exception interviennent dans le domaine de la loi en dérogeant aux dispositions législatives du code du travail et aux stipulations des conventions et accords collectifs relatives au régime des jours de réduction du temps de travail, des jours de repos prévus par une convention de forfait et des jours ou de jours de repos affectés sur un compte épargne temps. Ainsi qu'il a été dit au point 2, le délai d'habilitation est expiré. Par suite, les requérants, qui n'ont pas soulevé de question prioritaire de constitutionnalité par un mémoire distinct, ne sont pas recevables à soutenir que l'ordonnance du 25 mars 2020 porte atteinte aux droits et libertés garantis par la Constitution.  <br/>
<br/>
              En ce qui concerne les autres moyens de légalité interne : <br/>
<br/>
              11. En premier lieu, aux termes de l'article 1er du premier protocole additionnel à la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales : " Toute personne physique ou morale a droit au respect de ses biens. Nul ne peut être privé de sa propriété que pour cause d'utilité publique et dans les conditions prévues par la loi et les principes généraux du droit international. / Les dispositions précédentes ne portent pas atteinte au droit que possèdent les États de mettre en vigueur les lois qu'ils jugent nécessaires pour réglementer l'usage des biens conformément à l'intérêt général ou pour assurer le paiement des impôts ou d'autres contributions ou des amendes ".<br/>
<br/>
              12. L'ordonnance est sans incidence sur le nombre de jours auxquels ont droit les salariés mais se borne, durant l'état d'urgence sanitaire, à permettre à l'employeur de leur imposer de prendre un congé. La circonstance que les déplacements hors du domicile aient été interdits au cours de la période concernée, sous réserve d'exceptions limitativement énumérées et devant être dûment justifiées, ne conduit pas à considérer que les jours pris au cours de cette période n'étaient pas des jours consacrés au repos, à la détente et aux loisirs. La seule circonstance que les salariés peuvent se voir imposer de prendre des congés à des dates qu'ils n'ont pas choisies ne caractérise pas une atteinte à un bien au sens des stipulations de l'article 1er du premier protocole additionnel à la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales. Le moyen tiré de la méconnaissance de ces stipulations doit, dès lors, être écarté.<br/>
<br/>
              13. En deuxième lieu, aux termes du deuxième paragraphe de l'article 31 de la charte des droits fondamentaux de l'Union européenne : " Tout travailleur a droit (...) à une période annuelle de congés payés ". Aux termes de l'article 7 de la directive 2003/88/CE du Parlement européen et du Conseil du 4 novembre 2003 concernant certains aspects de l'aménagement du temps de travail : " 1. Les États membres prennent les mesures nécessaires pour que tout travailleur bénéficie d'un congé annuel payé d'au moins quatre semaines, conformément aux conditions d'obtention et d'octroi prévues par les législations et/ou pratiques nationales / 2. La période minimale de congé annuel payé ne peut être remplacée par une indemnité financière, sauf en cas de fin de relation de travail ".<br/>
<br/>
              14. Les dispositions des articles 2 à 4 de l'ordonnance du 25 mars 2020 ne portant pas sur les congés annuels des salariés, les requérants ne peuvent utilement soutenir qu'elles méconnaîtraient l'article 31 de la charte des droits fondamentaux de l'Union européenne et l'article 7 de la directive 2003/88/CE.<br/>
<br/>
              15. En troisième lieu, si l'instruction attaquée indique que les agents de droit privé devront prendre dix jours de repos entre le 16 mars et le 7 mai 2020, elle n'a ni pour objet ni pour effet de permettre que des congés soient imposés pour la période antérieure à son entrée en vigueur mais elle se borne à prévoir qu'il sera tenu compte des jours de repos déjà pris au cours de cette période dans le décompte des dix jours à prendre entre le 16 mars et le 7 mai 2020. Par suite, le moyen tiré de ce que l'instruction attaquée serait entachée d'une rétroactivité illégale doit être écarté.<br/>
<br/>
              16. En quatrième lieu, les dispositions de l'ordonnance du 25 mars 2020 ont pour objet de permettre aux employeurs de salariés de tenir compte des besoins de leur activité au cours de la période d'état d'urgence sanitaire et de diminuer le nombre de jours susceptibles d'être pris à la reprise d'activité dans des conditions normales. Elles doivent ainsi être interprétées comme permettant à Pôle emploi, dont l'activité a été bouleversée durant la période d'état d'urgence sanitaire, d'imposer des jours de repos à ses agents de droit privé au cours de cette période, comme il l'a fait pour ses agents de droit public en vertu de l'ordonnance du 15 avril 2020. Par suite, le moyen tiré de ce que l'instruction attaqué méconnaîtrait les articles 2 à 4 de l'ordonnance du 25 mars 2020 doit être écarté.<br/>
<br/>
              17. En dernier lieu, l'instruction attaquée ne modifie pas le nombre de jours de repos auxquels ont droit les agents de droit privé de Pôle emploi mais se borne à imposer à ceux d'entre eux qui ont été placés en " absence autorisée payée " entre le 16 mars et le 7 mai 2020 de prendre au moins dix jours de repos au cours de cette période. Si les requérants font valoir qu'il n'était pas nécessaire à Pôle emploi de recourir aux facultés offertes par les articles 2 à 4 de l'ordonnance du 25 mars 2020 pour garantir la disponibilité des agents lors de la reprise d'activité dans des conditions normales, dès lors que l'employeur peut modifier la période de prise de congés sur le fondement de l'article L. 3141-16 du code du travail, l'instruction attaquée vise à tenir compte des besoins du service au cours de la période d'état d'urgence sanitaire et à diminuer le nombre de jours susceptibles d'être pris à la reprise. Par suite, le moyen tiré de ce que l'instruction attaquée serait entachée d'erreur manifeste d'appréciation doit être écarté.<br/>
<br/>
              Sur les dispositions relatives aux agents de droit public : <br/>
<br/>
              18. L'article 1er de l'ordonnance du 15 avril 2020 prévoit que les fonctionnaires et agents contractuels de droit public de la fonction publique de l'Etat, les personnels ouvriers de l'Etat ainsi que les magistrats de l'ordre judiciaire en autorisation spéciale d'absence entre le 16 mars 2020 et le terme de l'état d'urgence sanitaire déclaré par la loi du 23 mars 2020 ou, si elle est antérieure, la date de reprise par l'agent de son service dans des conditions normales, prennent dix jours de réduction du temps de travail ou de congés annuels, dont cinq jours de réduction du temps de travail au cours d'une première période allant du 16 mars au 16 avril 2020 et cinq autres jours de réduction du temps de travail ou de congés annuels au cours d'une seconde période allant du 17 avril 2020 au terme de l'état d'urgence sanitaire ou à la date, si elle est antérieure, de reprise du service dans des conditions normales. Il précise que s'ils ne disposent pas de cinq jours de réduction du temps de travail pouvant être pris au cours de la première période, ces jours sont complétés à due concurrence par la prise d'un ou plusieurs jours de congés au cours de la seconde période, dans la limite totale de six jours de congés annuels au titre des deux périodes.<br/>
<br/>
              19. L'instruction attaquée indique, par des dispositions impératives, dans quelles conditions les dispositions de l'ordonnance du 15 avril 2020 relatives aux agents placés en autorisation spéciale d'absence s'appliquent aux agents de droit public de Pôle emploi. Les requérants se bornent à contester, par la voie de l'exception, la conformité de l'ordonnance du 15 avril 2020 à des normes juridique supérieures.<br/>
<br/>
              20. D'une part, les dispositions de l'ordonnance du 15 avril 2020 interviennent dans le domaine de la loi. Ainsi qu'il a été dit au point 2, le délai d'habilitation est expiré. Par suite, les requérants, qui n'ont pas soulevé de question prioritaire de constitutionnalité par un mémoire distinct, ne peuvent soutenir que l'ordonnance du 15 avril 2020 porte atteinte aux droits et libertés garantis par la Constitution.<br/>
<br/>
              21. D'autre part, l'ordonnance du 15 avril 2020 ne modifie pas le nombre de jours de réduction du temps de travail et de congés annuels auxquels ont droit les agents concernés mais se borne, durant l'état d'urgence sanitaire, à leur imposer de prendre un congé au cours de la période pendant laquelle ils sont rémunérés en l'absence de service fait. La circonstance que les déplacements hors du domicile aient été interdits au cours de la période concernée, sous réserve d'exceptions limitativement énumérées et devant être dûment justifiées, ne conduit pas à considérer que les jours de congés annuels pris au cours de cette période n'étaient pas des jours consacrés au repos, à la détente et aux loisirs. Ainsi, les moyens tirés de la méconnaissance de l'article 31 de la charte des droits fondamentaux de l'Union européenne et de l'article 7 de la directive 2003/88/CE, qui ne peuvent être utilement invoqués que dans la mesure où les dispositions de l'ordonnance du 15 avril 2020 sont susceptibles d'avoir une incidence sur des jours de congé annuel inclus dans la période minimale de quatre semaines régie par la directive, doivent être écartés.<br/>
<br/>
              22. Enfin, la seule circonstance qu'il est imposé aux agents concernés de prendre des congés, sans modifier le nombre de jours de réduction du temps et de travail et de congés annuels auxquels ils ont droits, à des dates qu'ils n'ont pas choisies ne caractérise pas une atteinte à un bien au sens des stipulations de l'article 1er du premier protocole additionnel à la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales. Par suite, et alors même que l'article 3 de l'ordonnance précise que les jours de réduction du temps de travail imposés aux agents peuvent être pris parmi ceux épargnés sur le compte épargne-temps, le moyen tiré de la méconnaissance des stipulations de l'article 1er du premier protocole additionnel à la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales doit être écarté.<br/>
<br/>
              23. Il résulte de tout ce qui précède que les requérants ne sont pas fondés à demander l'annulation de l'instruction qu'ils attaquent.<br/>
<br/>
              Sur les frais liés au litige : <br/>
<br/>
              24. Il n'y a pas lieu, dans les circonstances de l'espèce, de mettre à la charge des requérants la somme demandée par Pôle emploi au titre de l'article L. 761-1 du code de justice administrative. Ces dispositions font obstacle à ce qu'une somme soit mise à la charge de Pôle emploi, qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête du syndicat national unitaire travail, emploi, formation, insertion et autres est rejetée.<br/>
Article 2 : Les conclusions présentées par Pôle emploi au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La présente décision sera notifiée au syndicat national unitaire travail, emploi, formation, insertion, premier requérant dénommé, et à Pôle emploi.<br/>
Copie en sera adressée à la ministre du travail, de l'emploi et de l'insertion.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
