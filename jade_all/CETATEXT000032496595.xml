<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032496595</ID>
<ANCIEN_ID>JG_L_2016_05_000000384368</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/49/65/CETATEXT000032496595.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère chambre, 06/05/2016, 384368, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-05-06</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>384368</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère chambre</FORMATION>
<TYPE_REC>Contentieux des pensions</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ODENT, POULET ; SCP MARLANGE DE LA BURGADE</AVOCATS>
<RAPPORTEUR>M. Yannick Faure</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Jean Lessi</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2016:384368.20160506</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A...B...a demandé au tribunal administratif de Lille :<br/>
              - à titre principal, de surseoir à statuer et de saisir la Cour de justice de l'Union européenne d'une question préjudicielle portant sur la conformité au droit de l'Union européenne des articles L. 24 et R. 37 du code des pensions civiles et militaires de retraite ;<br/>
              - à titre subsidiaire, d'annuler la décision du 1er décembre 2010 par laquelle le directeur général de la Caisse nationale de retraites des agents des collectivités locales a refusé de l'admettre à la retraite avec jouissance immédiate de sa pension à compter du 31 mai 2011 en sa qualité de père de trois enfants ;<br/>
              - d'enjoindre à l'administration de réexaminer sa demande et de le faire bénéficier d'une pension de retraite assortie de la bonification prévue par le b de l'article L. 12 du même code.<br/>
<br/>
              Par un jugement n° 1107113 du 11 juillet 2014, le tribunal administratif de Lille a rejeté sa demande. <br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 10 septembre 2014, 3 décembre 2014 et 20 janvier 2016, M. B...demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler ce jugement du tribunal administratif de Lille du 11 juillet 2014 ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande d'annulation et d'injonction, en enjoignant en outre à l'administration de tenir compte pour la liquidation de sa pension des droits acquis en cours de procédure ; <br/>
<br/>
              3°) à titre subsidiaire, de saisir la Cour de justice de l'Union européenne à titre préjudiciel ;<br/>
<br/>
              4°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le traité sur le fonctionnement de l'Union européenne ; <br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales et son premier protocole additionnel ;<br/>
              - le code des pensions civiles et militaires de retraite ; <br/>
              - la loi n° 2010-1130 du 9 novembre 2010 ; <br/>
              - la décision C-173/13 du 17 juillet 2014 de la Cour de justice de l'Union européenne ;<br/>
              - le code de justice administrative ; <br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Yannick Faure, auditeur,  <br/>
<br/>
              - les conclusions de M. Jean Lessi, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Marlange, de la Burgade, avocat de M.B..., et à la SCP Odent, Poulet, avocat de la Caisse des dépôts et consignations ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que, le 24 novembre 2010, M.B..., fonctionnaire territorial ayant accompli quinze années de services effectifs, a saisi son administration d'une demande de départ anticipé à la retraite avec jouissance immédiate de sa pension, à compter du 31 mai 2011, sur le fondement des dispositions du 3° du I de l'article L. 24 du code des pensions civiles et militaires de retraite. Sa demande a été rejetée, au motif qu'il ne justifiait pas avoir interrompu son activité durant au moins deux mois pour chacun de ses enfants ou des enfants qu'il avait élevés. M. B...se pourvoit en cassation contre le jugement du 11 juillet 2014 par lequel le tribunal administratif de Lille a rejeté sa demande tendant à l'annulation de cette décision de refus.<br/>
<br/>
              2. Les droits du fonctionnaire relatifs au point de départ de la jouissance de sa pension de retraite doivent être appréciés à la date à compter de laquelle le fonctionnaire demande à bénéficier de cette pension. Il en résulte que les droits à pension de M. B...doivent s'apprécier au regard des dispositions législatives et réglementaires applicables à la date du 31 mai 2011. En faisant application à la situation de M. B...des dispositions en vigueur à la date de sa demande, le tribunal administratif de Lille a méconnu le champ d'application de la loi.<br/>
<br/>
              3. Il résulte de ce qui précède que M. B...est fondé à demander l'annulation du jugement qu'il attaque. Le moyen retenu suffisant à entraîner cette annulation, il n'est pas nécessaire d'examiner le moyen de son pourvoi.<br/>
<br/>
              4. Dans les circonstances de l'espèce, il y a lieu, en application de l'article L. 821-2 du code de justice administrative, de régler l'affaire au fond.<br/>
<br/>
              5. Aux termes du III de l'article 44 de la loi du 9 novembre 2010 portant réforme des retraites : " Par dérogation à l'article L. 24 du code des pensions civiles et militaires de retraite, le fonctionnaire civil et le militaire ayant accompli quinze années de services civils ou militaires effectifs avant le 1er janvier 2012 et parent à cette date de trois enfants vivants, ou décédés par faits de guerre, conserve la possibilité de liquider sa pension par anticipation à condition d'avoir, pour chaque enfant, interrompu ou réduit son activité dans des conditions fixées par un décret en Conseil d'Etat. / Sont assimilées à l'interruption ou à la réduction d'activité mentionnée au premier alinéa du présent III les périodes n'ayant pas donné lieu à cotisation obligatoire dans un régime de retraite de base, dans des conditions fixées par le décret en Conseil d'Etat mentionné au deuxième alinéa du 3° du I et au 1° bis du II de l'article L. 24 du code des pensions civiles et militaires de retraite dans sa rédaction antérieure à l'entrée en vigueur de la présente loi. / Sont assimilés aux enfants mentionnés au premier alinéa du présent III les enfants énumérés au II de l'article L. 18 du même code que l'intéressé a élevés dans les conditions prévues au III de ce même article ". Ces dispositions sont, comme celles du I de l'article L. 24 du code des pensions civiles et militaires de retraite en vertu du I de l'article 25 du décret du 26 décembre 2003 relatif au régime de retraite des fonctionnaires affiliés à la Caisse nationale de retraites des agents des collectivités locales, applicables aux agents de la fonction publique territoriale. Elles sont entrées en vigueur le 1er janvier 2011, à la suite de la publication, au Journal officiel de la République française, du décret du 30 décembre 2010 pris notamment pour leur application. L'article R. 37 du code des pensions civiles et militaires de retraite, applicable, en vertu de l'article 2 de ce décret du 30 décembre 2010, aux fonctionnaires mentionnés au III de l'article 44 de la loi du 9 novembre 2010, prévoit que le bénéfice de ces dispositions est subordonné soit à une interruption d'activité d'une durée continue au moins égale à deux mois, pendant la période comprise entre le premier jour de la quatrième semaine précédant la naissance ou l'adoption et le dernier jour du trente-sixième mois suivant la naissance ou l'adoption, dans le cadre d'un congé pour maternité, d'un congé de paternité, d'un congé d'adoption, d'un congé parental, d'un congé de présence parentale ou d'une disponibilité pour élever un enfant de moins de huit ans, soit à une réduction d'activité constituée d'une période de service à temps partiel d'une durée continue d'au moins quatre mois pour une quotité de temps de travail de 50 %, d'au moins cinq mois pour une quotité de 60 % et d'au moins sept mois pour une quotité de 70 %.<br/>
<br/>
              6. Aux termes de l'article 141 du traité instituant la Communauté européenne, devenu l'article 157 du traité sur le fonctionnement de l'Union européenne : " 1. Chaque État membre assure l'application du principe de l'égalité des rémunérations entre travailleurs masculins et travailleurs féminins pour un même travail ou un travail de même valeur. / 2. Aux fins du présent article, on entend par rémunération, le salaire ou traitement ordinaire de base ou minimum, et tous autres avantages payés directement ou indirectement, en espèces ou en nature, par l'employeur au travailleur en raison de l'emploi de ce dernier. / L'égalité de rémunération, sans discrimination fondée sur le sexe, implique : / a) que la rémunération accordée pour un même travail payé à la tâche soit établie sur la base d'une même unité de mesure ; / b) que la rémunération accordée pour un travail payé au temps soit la même pour un même poste de travail (...). / 4. Pour assurer concrètement une pleine égalité entre hommes et femmes dans la vie professionnelle, le principe de l'égalité de traitement n'empêche pas un État membre de maintenir ou d'adopter des mesures prévoyant des avantages spécifiques destinés à faciliter l'exercice d'une activité professionnelle par le sexe sous-représenté ou à prévenir ou compenser des désavantages dans la carrière professionnelle ". Il résulte de ces dispositions, telles qu'interprétées par la Cour de justice de l'Union européenne, que le principe d'égalité des rémunérations s'oppose non seulement à l'application de dispositions qui établissent des discriminations directement fondées sur le sexe mais également à l'application de dispositions qui maintiennent des différences de traitement entre travailleurs masculins et travailleurs féminins sur la base de critères non fondés sur le sexe, dès lors que ces différences de traitement ne peuvent s'expliquer par des facteurs objectivement justifiés et étrangers à toute discrimination fondée sur le sexe, et qu'il y a discrimination indirecte en raison du sexe lorsque l'application d'une mesure nationale, bien que formulée de façon neutre, désavantage en fait un nombre beaucoup plus élevé de travailleurs d'un sexe par rapport à l'autre. <br/>
<br/>
              7. Par un arrêt du 17 juillet 2014, la Cour de justice de l'Union européenne, statuant sur renvoi préjudiciel de la cour administrative d'appel de Lyon, a estimé que l'article 141 doit être interprété en ce sens que, sauf à pouvoir être justifié par des facteurs objectifs étrangers à toute discrimination fondée sur le sexe, tels qu'un objectif légitime de politique sociale, et à être propre à garantir l'objectif invoqué et nécessaire à cet effet, un régime de départ anticipé à la retraite tel que celui résultant des dispositions des articles L. 24 et R. 37 du code des pensions civiles et militaires de retraite, en tant qu'elles prévoient la prise en compte du congé de maternité dans les conditions ouvrant droit au bénéfice en cause, introduirait une différence de traitement entre les travailleurs féminins et les travailleurs masculins contraire à cet article. Cependant, la Cour de justice de l'Union européenne a rappelé que, s'il lui revenait de donner des " indications " " de nature à permettre à la juridiction nationale de statuer ", il revient exclusivement au juge national, qui est seul compétent pour apprécier les faits et pour interpréter la législation nationale, de déterminer si et dans quelle mesure les dispositions concernées sont justifiées par de tels facteurs objectifs. <br/>
<br/>
              8. Si, pendant son congé de maternité, la femme fonctionnaire ou militaire conserve légalement ses droits à avancement et à promotion et si la maternité est ainsi normalement neutre sur sa carrière, il résulte néanmoins de l'instruction et des données disponibles qu'une femme ayant eu un ou plusieurs enfants connaît, de fait, une moindre progression de carrière que ses collègues masculins et perçoit en conséquence une pension plus faible en fin de carrière. En particulier, les arrêts de travail liés à la maternité contribuent à empêcher une femme de bénéficier des mêmes possibilités de carrière que les hommes. De plus, les mères de famille ont dans les faits plus systématiquement interrompu leur carrière que les hommes, ponctuellement ou non, en raison des contraintes résultant de la présence d'un ou plusieurs enfants au foyer. Les éléments produits par M. B...ne contredisent pas ces données mais expliquent également une part sensible des inégalités de rémunération entre hommes et femmes constatées dans la fonction publique sur la période allant de 1994 à 2011 par l'incidence de la naissance d'enfants, entraînant plus fréquemment pour les femmes que pour les hommes un passage au temps partiel et une diminution du nombre d'heures supplémentaires effectuées, et à plus long terme un moindre accès à des postes à responsabilité. Le niveau de la pension constaté des femmes ayant eu des enfants résulte d'une situation passée, consécutive à leur déroulement de carrière, qui ne peut être modifiée au moment de la liquidation. <br/>
<br/>
              9. Par la loi du 9 novembre 2010, le législateur a procédé à une extinction progressive de la mesure pour les parents de trois enfants. Ce faisant, le législateur a entendu non pas prévenir les inégalités de fait entre les hommes et les femmes fonctionnaires et militaires dans le déroulement de leur carrière et leurs incidences en matière de retraite telles qu'exposées ci-dessus, mais compenser à titre transitoire ces inégalités normalement appelées à disparaître. Dans ces conditions, la disposition litigieuse relative au choix d'un départ anticipé avec jouissance immédiate, prise afin d'offrir, dans la mesure du possible, une compensation des conséquences de la naissance et de l'éducation d'enfants sur le déroulement de la carrière d'une femme, en l'état de la société française d'alors, est objectivement justifiée par un objectif légitime de politique sociale et est propre à garantir cet objectif et nécessaire à cet effet. Par suite, les dispositions en cause ne méconnaissent pas le principe d'égalité des rémunérations tel que défini à l'article 157 du traité sur le fonctionnement de l'Union européenne. <br/>
<br/>
              10. Pour les mêmes motifs, ces dispositions ne méconnaissent ni les objectifs de la directive 2006/54/CE du Parlement européen et du Conseil du 5 juillet 2006 relative à la mise en oeuvre du principe de l'égalité des chances et de l'égalité de traitement entre hommes et femmes en matière d'emploi et de travail, ni les stipulations combinées de l'article 14 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales et de l'article 1er du premier protocole additionnel à cette convention.<br/>
<br/>
              11. Les dispositions transitoires issues de la loi du 9 novembre 2010 et de son décret d'application prévoient des conditions d'ouverture du droit à la jouissance immédiate de la pension plus favorables que celles du code des pensions civiles et militaires de retraite précédemment en vigueur. M. B...n'est donc pas fondé à soutenir que l'application à sa situation des dispositions issues de la loi du 9 novembre 2010 porterait atteinte à une espérance légitime de continuer de bénéficier des dispositions antérieures et méconnaîtrait ainsi les stipulations combinées des articles 14 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales et 1er du premier protocole additionnel à cette convention.<br/>
<br/>
              12. Les droits à pension de M. B...devant être appréciés au regard de la loi du 9 novembre 2010 et non de la loi du 30 décembre 2004 de finances rectificative pour 2004, les moyens qu'il soulève, tirés de la contrariété de cette dernière loi avec les engagements internationaux de la France et de son application rétroactive, sont sans incidence sur le présent litige.<br/>
<br/>
              13. L'article L. 12 du code des pensions civiles et militaires de retraite n'est pas applicable aux fonctionnaires, comme M.B..., auxquels s'applique le décret du 26 décembre 2003 relatif au régime de retraite des fonctionnaires affiliés à la Caisse nationale de retraites des agents des collectivités locales. Si le décret du 26 décembre 2003 comporte, à son article 15, des dispositions ayant le même objet, qui renvoient à l'article R. 13 du même code, ces dispositions ne peuvent être utilement invoquées dans le présent litige, dès lors que M. B... ne remplit pas les conditions prévues par la loi du 9 novembre 2010 pour obtenir la liquidation de sa pension par anticipation. Par suite, il ne peut soutenir de façon utile que les dispositions des articles L. 12 et R. 13 du code des pensions civiles et militaires de retraite seraient contraires aux engagements internationaux de la France.<br/>
<br/>
              14. Il résulte de tout ce qui précède que, sans qu'il y ait lieu de saisir la Cour de justice de l'Union européenne d'une question préjudicielle, les conclusions de M. B...tendant à l'annulation de la décision implicite par laquelle l'administration a rejeté sa demande du 24 novembre 2010 tendant à son admission à la retraite avec jouissance immédiate de sa pension doivent être rejetées.<br/>
<br/>
              15. Par suite, ses conclusions aux fins d'injonction ne peuvent qu'être également rejetées.<br/>
<br/>
              16. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'il soit fait droit aux conclusions présentées à ce titre par M. B...tant devant le tribunal administratif de Lille que devant le Conseil d'Etat.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du tribunal administratif de Lille du 11 juillet 2014 est annulé.<br/>
Article 2 : La demande présentée par M. B...devant le tribunal administratif de Lille et le surplus de ses conclusions devant le Conseil d'Etat sont rejetés.<br/>
Article 3 : La présente décision sera notifiée à M. A...B...et à la Caisse des dépôts et consignations. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
