<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026807342</ID>
<ANCIEN_ID>JG_L_2012_12_000000354947</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/80/73/CETATEXT000026807342.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème et 5ème sous-sections réunies, 19/12/2012, 354947</TITRE>
<DATE_DEC>2012-12-19</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>354947</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème et 5ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Louis Dutheillet de Lamothe</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Rémi Keller</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2012:354947.20121219</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 16 décembre 2011 au secrétariat du contentieux du Conseil d'Etat, présentée par le Groupe d'Information et de Soutien des Immigrés (GISTI), dont le siège est 3 villa Marcès, à Paris (75011) ; le Groupe d'Information et de Soutien des Immigrés (GISTI) demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret n° 2011-1305 du 14 octobre 2011 relatif aux modalités d'attribution et de calcul des bourses nationales de collège et au retrait des demandes de bourses nationales d'études du second degré de lycée à Mayotte ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 1 500 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de l'éducation ; <br/>
<br/>
              Vu l'ordonnance n° 2002-149 du 7 février 2002 ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Louis Dutheillet de Lamothe, Auditeur,  <br/>
<br/>
              - les conclusions de M. Rémi Keller, rapporteur public ;<br/>
<br/>
<br/>
<br/>1. Considérant que le décret attaqué a inséré au code de l'éducation un article D. 562-8-1 adaptant aux particularités du département de Mayotte les dispositions réglementaires régissant l'attribution des bourses nationales de collège établies par l'article L. 531-1 du même code ; que les dispositions du I du nouvel article D. 562-8-1 du code de l'éducation précisent les informations et documents à fournir par la famille pour l'obtention d'une bourse nationale de collège et celles du II du même article adaptent les niveaux de ressources pris en compte pour le versement de ces bourses ; <br/>
<br/>
              Sur les conclusions tendant à l'annulation du I de l'article D. 562-8-1 du code de l'éducation : <br/>
<br/>
              2. Considérant que la requête du Groupe d'Information et de Soutien des Immigrés doit être regardée comme ne tendant à l'annulation de ces dispositions qu'en tant qu'elles prévoient que, pour l'obtention d'une bourse nationale de collège à Mayotte, le nombre d'enfants à charge doit être justifié par l'attestation de prestations sociales ; <br/>
<br/>
              3. Considérant, d'une part, qu'aux termes de l'article L. 531-1 du code de l'éducation : " Pour chaque enfant à charge inscrit dans un collège public, un collège privé ayant passé avec l'Etat l'un des contrats prévus aux articles L. 442-5 et L. 442-12 du présent code ou dans un collège privé habilité à recevoir des boursiers nationaux, une bourse nationale de collège est attribuée aux familles dont les ressources ne dépassent pas un plafond variable selon le nombre d'enfants à charge (...) " ; qu'il résulte de ces dispositions, qui sont applicables à Mayotte, que le législateur n'a pas pris de disposition spécifique relative au bénéfice des bourses nationales de collège accordées dans ce département ;<br/>
<br/>
              4. Considérant que le décret attaqué prévoit que, dans le département de Mayotte, le nombre d'enfants à charge ouvrant droit à l'obtention d'une bourse nationale de collège doit être déterminé par l'attestation de prestations sociales ; que l'article 4 de l'ordonnance du 7 février 2002 relative à l'extension et la généralisation des prestations familiales et à la protection sociale dans le département de Mayotte subordonne, s'agissant des ressortissants étrangers résidant dans ce département, le bénéfice des prestations familiales à la condition qu'ils soient titulaires soit de l'une des cartes de résident prévues, pour les citoyens de l'Union européenne, à l'article 13 de l'ordonnance du 26 avril 2000 relative aux conditions d'entrée et de séjour des étrangers à Mayotte ou, pour les autres ressortissants étrangers justifiant d'une résidence régulière non interrompue d'au moins cinq années, aux articles 19 et 20 de cette ordonnance, soit d'un titre de séjour régulièrement délivré avant l'entrée en vigueur de cette ordonnance ; <br/>
<br/>
              5. Considérant qu'il résulte de ce qui précède que le requérant est fondé à soutenir qu'en prévoyant que le nombre d'enfants à charge doit être justifié par l'attestation de prestations sociales régie par l'ordonnance du 7 février 2002, le décret attaqué a illégalement restreint le bénéfice des bourses nationales de collège à Mayotte aux seules catégories de ressortissants étrangers susceptibles de détenir cette attestation ; que, par suite, et sans qu'il soit besoin d'examiner les autres moyens de la requête à l'appui de ces conclusions, il y a lieu d'annuler le décret en tant qu'il prévoit qu'il faut justifier du nombre d'enfants à charge par l'attestation de prestations familiales ; <br/>
<br/>
              Sur les conclusions tendant à l'annulation du II de l'article D. 562-8-1 du code de l'éducation : <br/>
<br/>
              6. Considérant, en premier lieu, que, si le requérant soutient que la diminution de 20 % des plafonds de ressources pour l'attribution des bourses à Mayotte est injustifiée, il ressort des pièces du dossier que, ainsi que le fait valoir le ministre de l'éducation nationale, de la jeunesse et de la vie associative, cette diminution a pour objet de compenser le fait que le revenu fiscal de référence des résidents de Mayotte, qui est retenu pour apprécier les ressources de la famille de l'élève en application des dispositions de l'article D. 531-4 du code de l'éducation, est calculé en appliquant une retenue d'un cinquième sur les revenus effectivement perçus ; que le montant de cette diminution des plafonds de ressources n'est pas manifestement injustifié ; <br/>
<br/>
              Considérant, en second lieu, que si le requérant soutient que le calcul des plafonds de ressources applicables à Mayotte aurait dû prendre en compte le fait que le coût de la vie y est plus élevé qu'en métropole, le principe d'égalité n'impose pas de traiter différemment des personnes dans des situations différentes ; que, par suite, ce principe n'imposait pas à l'auteur du décret d'adapter les plafonds de ressources au pouvoir d'achat des familles des enfants bénéficiant des bourses nationales de collège ; <br/>
<br/>
              7. Considérant que, dès lors, il y a lieu de rejeter les conclusions du requérant tendant à l'annulation des dispositions du II de l'article D. 562-8-1 du code de l'éducation ; <br/>
<br/>
              Sur les conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative : <br/>
<br/>
              8. Considérant que ces dispositions font obstacle à ce qu'il soit fait droit aux conclusions présentées à ce titre par le Groupe d'Information et de Soutien des Immigrés, qui ne justifie d'aucun frais ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Les phrases " Le nombre d'enfants à charge est justifié par l'attestation de paiement de prestations familiales " et " Les enfants à charge considérés pour l'étude du droit à bourse sont les enfants mineurs ou infirmes et les enfants majeurs célibataires tels qu'ils figurent sur l'attestation de paiement de prestations familiales " de l'article D. 562-8-1 du code de l'éducation sont annulées. <br/>
Article 2 : Le surplus des conclusions de la requête du Groupe d'Information et de Soutien des Immigrés est rejeté.<br/>
Article 3 : La présente décision sera notifiée au Groupe d'Information et de Soutien des Immigrés (GISTI), au Premier ministre et au ministre de l'éducation nationale. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">30-01-03-05 ENSEIGNEMENT ET RECHERCHE. QUESTIONS GÉNÉRALES. QUESTIONS GÉNÉRALES CONCERNANT LES ÉLÈVES. BOURSES. - ATTRIBUTION DES BOURSES NATIONALES DE COLLÈGE - DÉCRET DU 14 OCTOBRE 2011 AYANT INSÉRÉ AU CODE DE L'ÉDUCATION UN ARTICLE D. 562-8-1 PORTANT ADAPTATION AUX PARTICULARITÉS DU DÉPARTEMENT DE MAYOTTE  - RESTRICTION ILLÉGALE DE LEUR BÉNÉFICE AUX SEULES CATÉGORIES DE RESSORTISSANTS ÉTRANGERS SUSCEPTIBLES DE DÉTENIR L'ATTESTATION DE PRESTATIONS SOCIALES RÉGIE PAR L'ORDONNANCE DU 7 FÉVRIER 2002.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">46-01-01-01 OUTRE-MER. DROIT APPLICABLE DANS LES COLLECTIVITÉS D'OUTRE-MER ET EN NOUVELLE-CALÉDONIE. GÉNÉRALITÉS. DÉPARTEMENTS D'OUTRE-MER. - ATTRIBUTION DES BOURSES NATIONALES DE COLLÈGE - DÉCRET DU 14 OCTOBRE 2011 AYANT INSÉRÉ AU CODE DE L'ÉDUCATION UN ARTICLE D. 562-8-1 PORTANT ADAPTATION AUX PARTICULARITÉS DU DÉPARTEMENT DE MAYOTTE  - RESTRICTION ILLÉGALE DE LEUR BÉNÉFICE AUX SEULES CATÉGORIES DE RESSORTISSANTS ÉTRANGERS SUSCEPTIBLES DE DÉTENIR L'ATTESTATION DE PRESTATIONS SOCIALES RÉGIE PAR L'ORDONNANCE DU 7 FÉVRIER 2002.
</SCT>
<ANA ID="9A"> 30-01-03-05 Décret n° 2011-1305 du 14 octobre 2011 ayant inséré au code de l'éducation un article D. 562-8-1 adaptant aux particularités du département de Mayotte les dispositions réglementaires régissant l'attribution des bourses nationales de collège établies par l'article L. 531-1 du même code. En prévoyant que le nombre d'enfants à charge doit être justifié par l'attestation de prestations sociales régie par l'ordonnance n° 2002-149 du 7 février 2002, ce décret a illégalement restreint le bénéfice des bourses nationales de collège à Mayotte aux seules catégories de ressortissants étrangers susceptibles de détenir cette attestation.</ANA>
<ANA ID="9B"> 46-01-01-01 Décret n° 2011-1305 du 14 octobre 2011 ayant inséré au code de l'éducation un article D. 562-8-1 adaptant aux particularités du département de Mayotte les dispositions réglementaires régissant l'attribution des bourses nationales de collège établies par l'article L. 531-1 du même code. En prévoyant que le nombre d'enfants à charge doit être justifié par l'attestation de prestations sociales régie par l'ordonnance n° 2002-149 du 7 février 2002, ce décret a illégalement restreint le bénéfice des bourses nationales de collège à Mayotte aux seules catégories de ressortissants étrangers susceptibles de détenir cette attestation.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
