<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000035743965</ID>
<ANCIEN_ID>JG_L_2001_03_0000231735</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/35/74/39/CETATEXT000035743965.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 27/03/2001, 231735, Publié au recueil Lebon</TITRE>
<DATE_DEC>2001-03-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>231735</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC/>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2001:231735.20010327</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le recours enregistré au secrétariat du contentieux du Conseil d'Etat le 23 mars 2001, présenté par le MINISTRE DE L'INTERIEUR qui demande, en application du second alinéa de l'article L. 523-1 du code de justice administrative, au juge des référés du Conseil d'Etat :<br/>
               1°) l'annulation de l'ordonnance du 9 mars 2001 par laquelle le juge des référés du tribunal administratif de Montpellier, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, a suspendu l'exécution de la décision du préfet des Pyrénées-Orientales ayant fixé l'Algérie comme pays de destination pour l'exécution de la mesure d'interdiction judiciaire du territoire visant M. A...B...;<br/>
               2°) le rejet de la demande de suspension présentée par M. B...devant le juge des référés ;<br/>
<br/>
<br/>
              le ministre de l'intérieur expose que M. A...B..., ressortissant algérien, né le 10 novembre 1971, est entré en France à l'âge de huit ans ; qu'à la suite de différentes condamnations à des peines d'emprisonnement, il a fait l'objet d'un arrêté d'expulsion le 11 août 1997, dont le tribunal administratif de Montpellier a prononcé l'annulation le 8 juin 2000 au motif que la mesure d'éloignement prise au titre de l'article 26 de l'ordonnance du 2 novembre 1945 ne constituait pas une nécessité impérieuse pour la sécurité publique ; qu'entre temps, l'intéressé a été condamné par le tribunal de grande instance de Montpellier, statuant en matière correctionnelle, le 21 mars 2000, à une peine d'emprisonnement de deux ans et un mois et à cinq ans d'interdiction du territoire ; qu'à la suite de réquisitions présentées le 19 février 2001 par le procureur de la République, le préfet des Pyrénées-Orientales a décidé, le 27 février 2001, de mettre à exécution la mesure judiciaire d'éloignement vers l'Algérie ; que l'ordonnance du juge des référés du tribunal administratif de Montpellier prescrivant la suspension de cette décision encourt la censure en la forme, faute d'être suffisamment motivée ; que, sur le fond, c'est à tort que le premier juge a estimé que la décision préfectorale du 27 février 2001 avait été prise en violation de l'article 8 du décret du 28 novembre 1983 car, comme l'a précisé la jurisprudence et ainsi que l'a confirmé la loi du 12 avril 2000, la procédure instituée par ce texte n'est pas applicable aux décisions pour lesquelles des dispositions législatives ont instauré une procédure contradictoire particulière ; que le préfet des Pyrénées-Orientales a fait une exacte application des dispositions de l'article 27 de l'ordonnance du 2 novembre 1945 en vertu desquelles l'interdiction du territoire français emporte de plein droit reconduite à la frontière ; que M. B... n'a pas fait valoir que son éloignement à destination de l'Algérie l'exposerait à des risques de traitements inhumains ou dégradants ;<br/>
<br/>
<br/>
              	Vu le mémoire en défense, enregistré le 25 mars 2001, présenté pour M. A... B..., qui conclut au rejet du recours ; M. B...soutient tout d'abord que s'il se vérifie que le tribunal administratif a été saisi d'une demande d'annulation de la décision préfectorale du 27 février 2001, l'ordonnance du juge des référés doit être regardée comme étant intervenue sur le fondement de l'article L. 521-1 du code de justice administrative et non au titre de l'article L. 521-2 ; que le pourvoi du ministre s'analyserait alors comme un recours en cassation dépourvu de moyens sérieux justifiant son admission ; qu'en admettant même que le pourvoi du ministre constitue un appel, il ne conteste pas utilement l'ordonnance du juge des référés car ce dernier pouvait, dans le cadre de l'article L. 521-2 du code, prescrire "toutes mesures nécessaires" ; que les conditions mises à l'application de cet article sont réunies ; qu'il y a tout d'abord urgence ; que la décision préfectorale est illégale car elle a été prise en méconnaissance des dispositions de l'article 8 du décret du 28 novembre 1983 qui exigent que les mesures qui doivent être motivées en vertu de la loi du 11 juillet 1979 soient précédées d'une procédure préalable contradictoire ; qu'ainsi que l'a estimé le juge des référés du premier degré il y a atteinte à la liberté fondamentale d'aller et venir ; qu'en outre, dans le climat actuel, le renvoi vers l'Algérie de M. B...l'exposerait à des traitements prohibés par l'article 3 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ; qu'il y a également méconnaissance de son droit au respect de sa vie familiale garanti par l'article 8 de ladite convention ;<br/>
<br/>
               Vu les autres pièces du dossier ;<br/>
<br/>
               Vu la Constitution du 4 octobre 1958, notamment son Préambule ;<br/>
<br/>
               Vu la loi n  73-1227 du 31 décembre 1973 autorisant la ratification de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, ensemble le décret n  74-630 du 3 mai 1974 portant publication de cette convention ;<br/>
<br/>
               Vu les articles 707 à 709 du code de procédure pénale ;<br/>
<br/>
               Vu l'ordonnance n  45-2658 du 2 novembre 1945 modifiée, relative aux conditions d'entrée et de séjour des étrangers en France, notamment ses articles 27, 27 bis et 27 ter ;<br/>
<br/>
               Vu l'article 1er de la loi n  79-587 du 11 juillet 1979 modifié par la loi n  86-76 du 17 janvier 1986, ensemble l'article 8 du décret n  83-1025 du 28 novembre 1983 ;<br/>
<br/>
               Vu les articles 24 et 43 de la loi n  2000-321 du 12 avril 2000 ;<br/>
<br/>
               Vu le code de justice administrative, notamment ses articles L. 511-2 (alinéa 2) L. 521-1, L. 521-2, L. 522-1, L. 523-1 ; R. 522-6 et R. 522-8 ;<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique le ministre de l'intérieur (direction des libertés publiques et des affaires juridiques) et M. A...B...;<br/>
<br/>
               Vu le procès-verbal de l'audience publique du 26 mars 2001 à 15 heures à laquelle a été entendue Me Hélène Farge, avocat au Conseil d'Etat et à la Cour de cassation, avocat de M. A...B...;<br/>
<br/>
<br/>
<br/>
              Considérant que M. A...B..., ressortissant algérien, a demandé au juge des référés du tribunal administratif de Montpellier d'ordonner la suspension de la décision du 27 février 2001 par laquelle le préfet des Pyrénées-Orientales a désigné l'Algérie comme le pays à destination duquel il doit être renvoyé à la suite du jugement du 21 mars 2000 du tribunal de grande instance de Montpellier statuant en matière correctionnelle, prononçant à son encontre, à titre de peine complémentaire, une interdiction du territoire français durant cinq ans ; que le ministre de l'intérieur a contesté, par la voie de l'appel, l'ordonnance du juge des référés qui a fait droit aux conclusions à fin de suspension de la décision préfectorale ;<br/>
<br/>
               - Sur la fin de non-recevoir opposée par M. B...:<br/>
<br/>
               Considérant qu'aux termes de l'article L. 521-2 du code de justice administrative : "Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public (...) aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. (...)" ;<br/>
<br/>
               Considérant que la procédure régie par l'article L. 521-2 du code précité est distincte de celle instituée par l'article L. 521-1 du même code qui permet à l'auteur d'un recours en annulation ou en réformation d'une décision administrative, de demander au juge des référés de suspendre l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision ; que l'article L. 521-2 peut ainsi être mis en oeuvre, quand bien même le demandeur pourrait tout aussi bien engager une action au titre de l'article L. 521-1 ;<br/>
<br/>
               Considérant qu'il suit de là que même si M. B...a, indépendamment de la demande de suspension qu'il a présentée au juge des référés, saisi le tribunal administratif de Montpellier d'un recours pour excès de pouvoir dirigé contre la décision du préfet des Pyrénées-Orientales désignant l'Algérie comme le pays à destination duquel sera mise à exécution la peine d'interdiction du territoire prononcée par le juge pénal, cette circonstance ne faisait pas obstacle à ce que le juge des référés, après avoir fait préciser au cours de l'audience publique du 9 mars 2001 par le conseil de l'intéressé que celui-ci entendait fonder son action sur l'article L. 521-2 du code de justice administrative, appréciât le bien-fondé de la demande au regard des dispositions dudit article ; qu'il suit de là que M. B...n'est pas fondé à soutenir que l'appel du ministre de l'intérieur devrait être regardé comme un pourvoi en cassation qui, faute de moyens de cassation sérieux, ne devrait pas même être admis ;<br/>
<br/>
               - Sur l'argumentation du ministre de l'intérieur :<br/>
<br/>
               Considérant que la mise en oeuvre de la protection juridictionnelle particulière prévue par l'article L. 521-2 du code de justice administrative implique que soient cumulativement réunies les conditions qui en autorisent l'application ;<br/>
<br/>
               Considérant que l'interdiction du territoire français prononcée par le juge pénal à l'encontre d'un ressortissant étranger sur le fondement de l'article 27 de l'ordonnance du 2 novembre 1945, "emporte de plein droit reconduite du condamné à la frontière" ; que l'exécution d'une telle mesure ne nécessite, même si elle ne l'exclut pas, l'intervention d'aucun arrêté préfectoral de reconduite à la frontière en application de l'article 22 de ladite ordonnance ; que, cependant, la détermination par le préfet du pays de renvoi pour assurer l'exécution de la peine ainsi prononcée est une mesure prise au titre de la police des étrangers qui doit être motivée en la forme en vertu de l'article 1er de la loi n  79-587 du 11 juillet 1979 ; que l'édiction d'une telle mesure, dès lors qu'elle se distingue de la procédure spécifique de reconduite administrative à la frontière, est subordonnée, eu égard aux dispositions de l'article 24 de la loi n° 2000-321 du 12 avril 2000, lesquelles se sont substituées à compter du 1er novembre 2000 à celles de l'article 8 du décret du 28 novembre 1983, au respect de la procédure contradictoire prévue par ledit article 24, sous la seule réserve des exceptions qu'il définit et qui ne trouvent pas à s'appliquer en l'espèce ; que, pour ces motifs, le juge des référés du tribunal administratif de Montpellier a pu, à bon droit, estimer que la décision du préfet des Pyrénées-Orientales du 27 février 2001, avait été prise à la suite d'une procédure irrégulière, faute pour M. B...d'avoir été mis à même de présenter ses observations sur la désignation du pays de renvoi ;<br/>
<br/>
               Mais considérant que cette irrégularité ne porte pas une "atteinte grave" à une liberté fondamentale ; qu'en effet, l'atteinte à la liberté d'aller et venir relevée par le premier juge découle en réalité du prononcé par le juge pénal de la peine d'interdiction du territoire frappant M.B..., qui fait obstacle à sa liberté de circulation sur le territoire de la République française et lui interdit temporairement d'y revenir, et non dans la décision préfectorale qui se borne à renvoyer l'intéressé dans son pays d'origine à partir duquel il pourra gagner d'autres destinations ; que, pour les mêmes motifs, il ne peut être valablement soutenu que l'exécution par l'autorité préfectorale de l'interdiction judiciaire du territoire contrevient au droit au respect de la vie familiale de M.B..., sans même qu'il soit besoin de rechercher si un tel droit entre dans le champ des prévisions de l'article L. 521-2 du code de justice administrative ; qu'enfin, s'il est vrai que la fixation du pays de renvoi est susceptible d'affecter gravement la liberté personnelle d'un ressortissant étranger dans le cas où il se trouverait, de ce fait, exposé à des risques de la nature de ceux visés par l'article 3 de la  convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, une telle éventualité ne se vérifie nullement en l'espèce ;<br/>
<br/>
               Considérant qu'il résulte de ce qui précède que le ministre de l'intérieur est fondé à soutenir que le juge des référés du tribunal administratif de Montpellier, en estimant que le préfet des Pyrénées-Orientales avait porté une atteinte grave à une liberté fondamentale, a fait une fausse application des dispositions de l'article L. 521-2 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : L'ordonnance du juge des référés du tribunal administratif de Montpellier du 9 mars 2001 est annulée.<br/>
<br/>
 Article 2 : La demande présentée par M. A...B...devant le juge des référés du tribunal administratif de Montpellier est rejetée.<br/>
<br/>
 Article 3 : La présente ordonnance sera notifiée à M. A...B..., au préfet des Pyrénées-Orientales et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">26-03-05 DROITS CIVILS ET INDIVIDUELS. LIBERTÉS PUBLIQUES ET LIBERTÉS DE LA PERSONNE. LIBERTÉ D'ALLER ET VENIR. - LIBERTÉ FONDAMENTALE D'ALLER ET VENIR - ATTEINTE GRAVE AU SENS DE L'ARTICLE L. 521-2 DU CODE DE JUSTICE ADMINISTRATIVE - ABSENCE - IRRÉGULARITÉ ENTACHANT LA DÉCISION PRÉFECTORALE PORTANT DÉSIGNATION DU PAYS DE DESTINATION D'UN ÉTRANGER POUR L'EXÉCUTION DE LA DÉCISION DE LA MESURE D'INTERDICTION JUDICIAIRE DU TERRITOIRE LE FRAPPANT, PRISE SANS QUE L'INTÉRESSÉ AIT ÉTÉ MIS À MÊME DE PRÉSENTER SES OBSERVATIONS SUR LE PAYS DE RENVOI.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-035-03-03-01-02 PROCÉDURE. PROCÉDURES INSTITUÉES PAR LA LOI DU 30 JUIN 2000. RÉFÉRÉ TENDANT AU PRONONCÉ DE MESURES NÉCESSAIRES À LA SAUVEGARDE D'UNE LIBERTÉ FONDAMENTALE (ART. L. 521-2 DU CODE DE JUSTICE ADMINISTRATIVE). CONDITIONS D'OCTROI DE LA MESURE DEMANDÉE. ATTEINTE GRAVE ET MANIFESTEMENT ILLÉGALE À UNE LIBERTÉ FONDAMENTALE. ATTEINTE GRAVE ET MANIFESTEMENT ILLÉGALE. - IRRÉGULARITÉ ENTACHANT LA DÉCISION PRÉFECTORALE PORTANT DÉSIGNATION DU PAYS DE DESTINATION D'UN ÉTRANGER POUR L'EXÉCUTION DE LA DÉCISION DE LA MESURE D'INTERDICTION JUDICIAIRE DU TERRITOIRE LE FRAPPANT, PRISE SANS QUE L'INTÉRESSÉ AIT ÉTÉ MIS À MÊME DE PRÉSENTER SES OBSERVATIONS SUR LE PAYS DE RENVOI - LIBERTÉ FONDAMENTALE D'ALLER ET VENIR - ABSENCE.
</SCT>
<ANA ID="9A"> 26-03-05 L'irrégularité résultant de ce que la décision préfectorale fixant le pays de destination d'un étranger pour l'exécution de la décision de la mesure d'interdiction judiciaire du territoire dont il a fait l'objet a été prise sans qu'il ait été mis à même de présenter des observations sur la désignation de ce pays ne porte pas une atteinte grave à une liberté fondamentale. L'atteinte à la liberté d'aller et venir découle, non de cette décision qui se borne à renvoyer l'intéressé dans son pays d'origine à partir duquel il pourra gagner d'autres destinations, mais du prononcé par le juge pénal de la peine d'interdiction du territoire, qui fait obstacle à la libre circulation de l'intéressé sur le territoire de la République française et lui interdit temporairement d'y revenir. Pour les mêmes raisons, absence de contravention du fait de l'exécution par l'autorité préfectorale de l'interdiction judiciaire du territoire au droit au respect de la vie familiale de l'intéressé, sans même qu'il soit besoin de rechercher si un tel droit entre dans les prévisions de l'article L. 521-2 du code de justice administrative.</ANA>
<ANA ID="9B"> 54-035-03-03-01-02 L'irrégularité résultant de ce que la décision préfectorale fixant le pays de destination d'un étranger pour l'exécution de la décision de la mesure d'interdiction judiciaire du territoire dont il a fait l'objet a été prise sans qu'il ait été mis à même de présenter des observations sur la désignation de ce pays ne porte pas une atteinte grave à une liberté fondamentale. L'atteinte à la liberté d'aller et venir découle, non de cette décision qui se borne à renvoyer l'intéressé dans son pays d'origine à partir duquel il pourra gagner d'autres destinations, mais du prononcé par le juge pénal de la peine d'interdiction du territoire, qui fait obstacle à la libre circulation de l'intéressé sur le territoire de la République française et lui interdit temporairement d'y revenir. Pour les mêmes raisons, absence de contravention du fait de l'exécution par l'autorité préfectorale de l'interdiction judiciaire du territoire au droit au respect de la vie familiale de l'intéressé, sans même qu'il soit besoin de rechercher si un tel droit entre dans les prévisions de l'article L. 521-2 du code de justice administrative.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
