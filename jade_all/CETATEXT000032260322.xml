<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032260322</ID>
<ANCIEN_ID>JG_L_2016_03_000000383984</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/26/03/CETATEXT000032260322.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème SSJS, 16/03/2016, 383984, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-03-16</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>383984</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème SSJS</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BARADUC, DUHAMEL, RAMEIX</AVOCATS>
<RAPPORTEUR>M. Jean-Luc Matt</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emilie Bokdam-Tognetti</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2016:383984.20160316</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Le directeur général des finances publiques a, par application de l'article R. 199-1 du livre des procédures fiscales, déféré d'office au tribunal administratif de Paris la réclamation de la Société Hôtelière de la Porte de Sèvres tendant à la réduction des cotisations de taxe professionnelle et de taxes additionnelles auxquelles elle a été assujettie au titre de l'année 2009 dans les rôles de la ville de Paris à raison des locaux exploités sous les enseignes " Sofitel Porte de Sèvres ", puis " Pullman Rive Gauche " à Paris (15ème arrondissement). Par un jugement n° 1105725/2-3 du 17 janvier 2013, le tribunal administratif de Paris a fixé la valeur locative unitaire de l'hôtel à 10,81 euros le mètre carré et déchargé la société de la somme correspondant à la différence entre le montant de la taxe professionnelle et des taxes additionnelles auxquelles elle a été assujettie au titre de l'année 2009 et celui qui résulte de la base d'imposition ainsi déterminée.<br/>
<br/>
              Par un arrêt n° 13PA01596 du 3 juillet 2014, la cour administrative d'appel de Paris a, sur recours du ministre, annulé ce jugement, puis fixé la valeur locative de l'hôtel à 10,81 euros le mètre carré et déchargé la société de la somme correspondant à la différence entre le montant de la taxe professionnelle et des taxes additionnelles auxquelles elle a été assujettie au titre de l'année 2009 et celui qui résulte de la base d'imposition ainsi déterminée.<br/>
<br/>
              Par un pourvoi et un mémoire en réplique, enregistrés les 27 août 2014 et 8 septembre 2015 au secrétariat du contentieux du Conseil d'Etat, le ministre des finances et des comptes publics demande au Conseil d'Etat d'annuler cet arrêt.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de la construction et de l'habitation ;<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Luc Matt, maître des requêtes,<br/>
<br/>
              - les conclusions de Mme Emilie Bokdam-Tognetti, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Baraduc, Duhamel, Rameix, avocat de la Société Hôtelière de la Porte de Sèvres ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que la Société Hôtelière de la Porte de Sèvres, qui exploite l'hôtel Pullman, anciennement Sofitel, situé 8, rue Louis Armand à Paris (15ème arrondissement), a demandé vainement à l'administration fiscale la réduction des cotisations de taxe professionnelle et de taxes additionnelles auxquelles elle a été assujettie au titre de l'année 2009. Par un jugement du 17 janvier 2013, le tribunal administratif de Paris a jugé que l'imposition en litige serait déterminée par référence à la valeur locative cadastrale du Grand Hôtel Intercontinental situé 12, boulevard des Capucines à Paris (9ème arrondissement), a déchargé en conséquence la Société Hôtelière de la Porte de Sèvres de la différence entre les impositions auxquelles elle a été assujettie et celles résultant de la base d'imposition ainsi réduite et rejeté le surplus de sa demande. Le ministre se pourvoit en cassation contre l'arrêt du 3 juillet 2014, par lequel la cour administrative d'appel de Paris a, après évocation, accordé à la société la même réduction de ses cotisations. <br/>
<br/>
              2. Aux termes de l'article 1469 du code général des impôts, dans sa rédaction alors en vigueur, la valeur locative servant de base à la taxe professionnelle : " (...) est déterminée comme suit : 1° Pour les biens passibles d'une taxe foncière, elle est calculée suivant les règles fixées pour l'établissement de cette taxe ; ". Aux termes de l'article 1498 du même code : " La valeur locative de tous les biens autres que les locaux d'habitation ou à usage professionnel visés au I de l'article 1496 et que les établissements industriels visés à l'article 1499 est déterminée au moyen de l'une des méthodes indiquées ci-après : (...)/ 2° a. Pour les biens loués à des conditions de prix anormales ou occupés par leur propriétaire, occupés par un tiers à un autre titre que la location, vacants ou concédés à titre gratuit, la valeur locative est déterminée par comparaison./ Les termes de comparaison sont choisis dans la commune. Ils peuvent être choisis hors de la commune pour procéder à l'évaluation des immeubles d'un caractère particulier ou exceptionnel ;/ b. La valeur locative des termes de comparaison est arrêtée :/ - soit en partant du bail en cours à la date de référence de la révision lorsque l'immeuble type était loué normalement à cette date ;/ - soit, dans le cas contraire, par comparaison avec des immeubles similaires situés dans la commune ou dans une localité présentant, du point de vue économique, une situation analogue à celle de la commune en cause et qui faisaient l'objet à cette date de locations consenties à des conditions de prix normales ;/ 3° A défaut de ces bases, la valeur locative est déterminée par voie d'appréciation directe ". En vertu de l'article 324 AA de l'annexe III à ce code : " La valeur locative cadastrale des biens loués à des conditions anormales ou occupés par leur propriétaire, occupés par un tiers à un titre autre que celui de locataire, vacants ou concédés à titre gratuit est obtenue en appliquant aux données relatives à leur consistance - telles que superficie réelle, nombre d'éléments - les valeurs unitaires arrêtées pour le type de la catégorie correspondante (...) ". En vertu de l'article 324 Z de la même annexe : " I. L'évaluation par comparaison consiste à attribuer à un immeuble ou à un local donné une valeur locative proportionnelle à celle qui a été adoptée pour d'autres biens de même nature pris comme types. / II. Les types dont il s'agit doivent correspondre aux catégories dans lesquelles peuvent être rangés les biens de la commune visés aux articles 324 Y à 324 AC, au regard de l'affectation, de la situation, de la nature de la construction, de son importance, de son état d'entretien et de son aménagement. / Ils sont inscrits au procès-verbal des opérations de la révision ".<br/>
<br/>
              3. En vertu de l'article R. 122-2 du code de la construction et de l'habitation : " Constitue un immeuble de grande hauteur, pour l'application du présent chapitre, tout corps de bâtiment dont le plancher bas du dernier niveau est situé, par rapport au niveau du sol le plus haut utilisable pour les engins des services publics de secours et de lutte contre l'incendie : / - à plus de 50 mètres pour les immeubles à usage d'habitation, tels qu'ils sont définis par l'article R. 111-1 ; / - à plus de 28 mètres pour tous les autres immeubles (...) ".<br/>
<br/>
              4. Pour l'application de la méthode d'évaluation de la valeur locative des locaux commerciaux et biens divers prévue au 2° de l'article 1498 du code général des impôts, les immeubles de grande hauteur, eu égard à leurs spécificités, ne peuvent être évalués que par comparaison avec d'autres immeubles de grande hauteur ou, à défaut, par voie d'appréciation directe en application du 3° du même article. A la date du 1er janvier 1970 retenue pour l'évaluation des valeurs locatives cadastrales servant de base à la taxe foncière sur les propriétés bâties, les seuls immeubles que leur hauteur exceptionnelle rendait spécifiques en France, pour l'évaluation de leur valeur locative, se situaient dans le quartier de La Défense en banlieue parisienne et avaient une hauteur de 100 mètres. En conséquence, doit être regardé comme un immeuble de grande hauteur, pour l'application de la règle mentionnée ci-dessus, un immeuble dont la hauteur est proche de cette hauteur ou lui est supérieure. Il n'y a, en revanche, pas lieu de se référer à la catégorie des immeubles de grande hauteur définie par le code de la construction et de l'habitation, notamment par son article R. 122-2 précité, qui inclut des immeubles qui ne présentent pas, par la nature de leur construction, de spécificité telle, au regard de la loi fiscale, qu'elle empêche la comparaison avec un immeuble n'appartenant pas à cette catégorie.<br/>
<br/>
              5. Il résulte de ce qui précède qu'en jugeant que la seule circonstance qu'un immeuble appartienne à la catégorie des immeubles de grande hauteur définie par le code de la construction et de l'habitation ne faisait pas obstacle à ce que sa valeur locative puisse être évaluée par comparaison avec un immeuble n'appartenant pas à la même catégorie, la cour administrative d'appel n'a pas commis d'erreur de droit. La cour n'a, par voie de conséquence, pas davantage commis d'erreur de droit en jugeant que la méthode d'évaluation par comparaison prévue au 2° de l'article 1498 du code général des impôts pouvait être appliquée et en retenant comme terme de comparaison le Grand Hôtel Intercontinental, alors même que ce dernier n'appartient pas à la catégorie des immeubles de grande hauteur au sens du code de la construction et de l'habitation. Par suite, le pourvoi du ministre des finances et des comptes publics doit être rejeté.<br/>
<br/>
              6. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat une somme de 3 500 euros à verser à la Société Hôtelière de la Porte de Sèvres au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
Article 1er : Le pourvoi du ministre des finances et des comptes publics est rejeté.<br/>
Article 2 : L'Etat versera à la Société Hôtelière de la Porte de Sèvres la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée au ministre des finances et des comptes publics et à la Société Hôtelière de la Porte de Sèvres.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
