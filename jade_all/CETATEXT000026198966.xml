<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026198966</ID>
<ANCIEN_ID>JG_L_2012_07_000000343866</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/19/89/CETATEXT000026198966.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème et 5ème sous-sections réunies, 11/07/2012, 343866</TITRE>
<DATE_DEC>2012-07-11</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>343866</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème et 5ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Jacques Arrighi de Casanova</PRESIDENT>
<AVOCATS>SCP ROCHETEAU, UZAN-SARANO ; SCP LE BRET-DESACHE</AVOCATS>
<RAPPORTEUR>M. Olivier Talabardon</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Rémi Keller</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2012:343866.20120711</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 18 octobre 2010 et 18 janvier 2011 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. Antoine A, demeurant ... ; M. A demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n°s 06BX02224, 06BX02394 du 30 mars 2010 par lequel la cour administrative d'appel de Bordeaux a annulé le jugement n° 031686 du 19 septembre 2006 du tribunal administratif de Toulouse annulant la décision du 24 décembre 2002 de l'inspectrice du travail de la Haute-Garonne autorisant son licenciement pour faute, la décision du 13 mars 2003 de l'inspectrice du travail rejetant son premier recours gracieux, la décision implicite de l'inspectrice du travail rejetant son second recours gracieux, la décision du 21 mars 2003 du ministre des affaires sociales, du travail et de la solidarité rejetant son premier recours hiérarchique et la décision du ministre du 28 avril 2003 rejetant son second recours hiérarchique ; <br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter les appels de la société Teuchos et du ministre de l'emploi, de la cohésion sociale et du logement ; <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ; <br/>
<br/>
              Vu le code du travail ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Olivier Talabardon, Maître des Requêtes en service extraordinaire,  <br/>
<br/>
              - les observations de la SCP Le Bret-Desaché, avocat de M. A et de la SCP Rocheteau, Uzan-Sarano, avocat de la société Safran Engineering Services,<br/>
<br/>
              - les conclusions de M. Rémi Keller, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Le Bret-Desaché, avocat de M. A et à la SCP Rocheteau, Uzan-Sarano, avocat de la société Safran Engineering Services ;<br/>
<br/>
<br/>
<br/>
              Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, par une décision du 24 décembre 2002, l'inspectrice du travail de la Haute-Garonne a autorisé la société Teuchos, devenue la société Safran Engineering Services, à licencier M. A, ingénieur système et délégué du personnel, au motif que, par son comportement et, notamment, le non-respect de plusieurs règles internes à l'entreprise, il perturbait le fonctionnement de celle-ci ; que M. A a formé un recours gracieux contre cette décision, que l'inspectrice du travail a rejeté par une décision du 13 mars 2003 ; que, par une décision du 21 mars 2003, le ministre des affaires sociales, du travail et de la solidarité a rejeté un premier recours hiérarchique formé par M. A, au motif de sa tardiveté ; que celui-ci a formé simultanément un second recours gracieux et un second recours hiérarchique, qui ont été rejetés, l'un par une décision implicite de l'inspectrice du travail, l'autre par une décision expresse du ministre en date du 28 avril 2003 ; que, par un jugement du 19 septembre 2006, le tribunal administratif de Toulouse a annulé l'ensemble de ces décisions ; que la cour administrative d'appel de Bordeaux, par l'arrêt attaqué du 30 mars 2010, a annulé ce jugement et rejeté les conclusions présentées en première instance par M. A ; <br/>
<br/>
              Sur l'arrêt attaqué en tant qu'il statue sur la légalité des décisions de l'inspectrice du travail :<br/>
<br/>
              Considérant qu'en vertu des dispositions du code du travail, les salariés légalement investis de fonctions représentatives bénéficient, dans l'intérêt de l'ensemble des travailleurs qu'ils représentent, d'une protection exceptionnelle ; que, lorsque leur licenciement est envisagé, celui-ci ne doit pas être en rapport avec les fonctions représentatives normalement exercées ou avec leur appartenance syndicale ; que, dans le cas où la demande de licenciement est motivée par un comportement fautif, il appartient à l'inspecteur du travail saisi et, le cas échéant, au ministre compétent de rechercher, sous le contrôle du juge de l'excès de pouvoir, si les faits reprochés au salarié sont d'une gravité suffisante pour justifier le licenciement, compte tenu de l'ensemble des règles applicables au contrat de travail de l'intéressé et des exigences propres à l'exécution normale du mandat dont il est investi ;  <br/>
<br/>
              Considérant que, si M. A soutient que la cour administrative d'appel de Bordeaux a omis de répondre au moyen tiré de ce que son refus d'utiliser les bons de délégation ne pouvait constituer un motif de licenciement dès lors que le dispositif de ces bons avait été illégalement mis en place par la société Teuchos en l'absence de consultation préalable des organisations syndicales et des institutions représentatives du personnel, il ressort des termes mêmes de l'arrêt attaqué, énonçant que le dispositif organisant l'utilisation des bons de délégation avait été mis en place au sein de la société le 1er mai 2002 après consultation du comité d'entreprise le 23 avril 2002, que la cour a répondu, en l'écartant, au moyen ainsi soulevé ; que, par ailleurs, en estimant qu'il n'était pas établi que le dispositif des bons de délégation ait été mis en place pour faire obstacle aux fonctions représentatives de M. A, la cour, qui a suffisamment motivé son arrêt sur ce point, n'a pas dénaturé les pièces du dossier qui lui était soumis ; <br/>
<br/>
              Considérant qu'aux termes de l'article R. 436-4 du code du travail, alors en vigueur, devenu l'article R. 2421-11 du même code : " L'inspecteur du travail procède à une enquête contradictoire au cours de laquelle le salarié peut, sur sa demande, se faire assister d'un représentant de son syndicat. / (...) " ;  que cette disposition implique, pour le salarié dont le licenciement est envisagé, le droit d'être entendu personnellement et individuellement par l'inspecteur du travail ; que, toutefois, un tel droit n'est pas méconnu lorsque le salarié, régulièrement convoqué par l'inspecteur du travail, s'abstient de donner suite à la convocation sans motif légitime ;<br/>
<br/>
              Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que l'inspectrice du travail de la Haute-Garonne a demandé à M. A de se présenter le 23 décembre 2002 en vue d'un entretien ; que, devant les juges du fond, l'intéressé n'a produit, à l'appui de ses allégations selon lesquelles son état de santé ne lui permettait pas de se présenter à cet entretien, qu'une lettre du médecin du travail informant son médecin traitant qu'il l'avait orienté vers un praticien spécialisé et que le recours à l'arrêt maladie pouvait se justifier ; qu'après avoir estimé souverainement que cette seule production n'établissait pas que l'état de santé de M. A faisait obstacle à ce qu'il se rendît à l'entretien avec l'inspectrice du travail, la cour, qui a suffisamment motivé son arrêt sur ce point, a pu en déduire, sans erreur de droit, que l'enquête contradictoire avait été régulièrement menée par cette dernière ;  <br/>
<br/>
              Considérant que, pour contester l'appréciation portée par la cour sur le caractère fautif des faits retenus par l'inspectrice du travail au soutien de sa décision autorisant son licenciement, M. A se borne à soutenir que le juge d'appel aurait dénaturé ces faits ; que, cependant, il ressort du dossier qui lui était soumis qu'en estimant qu'eu égard à leur contenu, les messages électroniques, que l'intéressé avait adressés à des salariés de la société Teuchos pour critiquer certains représentants du personnel et mettre en cause la direction, étaient de nature à provoquer des perturbations et une gêne dans le fonctionnement de l'entreprise, la cour n'a pas entaché son arrêt d'une dénaturation des faits de l'espèce ; <br/>
<br/>
              Considérant que, si M. A soutient que la cour a dénaturé les faits en estimant qu'il n'était pas établi qu'il ait fait l'objet d'une discrimination de la part de son employeur dans l'exercice de ses fonctions représentatives, ce moyen n'est pas assorti des précisions permettant d'en apprécier le bien-fondé ;<br/>
<br/>
              Considérant qu'il résulte de ce qui précède que M. A n'est pas fondé à demander l'annulation de l'arrêt attaqué en tant qu'il a rejeté ses conclusions tendant à l'annulation des décisions de l'inspectrice du travail ; <br/>
<br/>
              Sur l'arrêt attaqué en tant qu'il statue sur la légalité des décisions ministérielles rejetant les recours hiérarchiques de M. A :<br/>
<br/>
              Considérant qu'aux termes de l'article R. 436-6 du code du travail, alors en vigueur, devenu l'article R. 2422-1 du même code : " Le ministre compétent peut annuler ou réformer la décision de l'inspecteur du travail sur le recours de l'employeur, du salarié ou du syndicat que ce salarié représente ou auquel il a donné mandat à cet effet. / Ce recours doit être introduit dans un délai de deux mois à compter de la notification de la décision de l'inspecteur. (...) " ;  <br/>
<br/>
              Considérant que M. A soutenait devant les juges du fond qu'en vertu de l'article 16 de la loi du 12 avril 2000 relative aux droits des citoyens dans leurs relations avec l'administration, la date du recours hiérarchique formé par le salarié contre la décision de l'inspecteur du travail est celle du dépôt du pli aux services postaux et non celle de sa réception par le ministre compétent et qu'ainsi, en l'espèce, le premier recours hiérarchique qu'il avait adressé au ministre, par courrier posté le 26 février 2003, n'était pas tardif, contrairement à ce qu'ont retenu les décisions litigieuses des 21 mars et 28 avril 2003 ; <br/>
<br/>
              Considérant, cependant, que, lorsque le ministre rejette le recours hiérarchique qui lui est présenté contre la décision de l'inspecteur du travail statuant sur la demande d'autorisation de licenciement formée par l'employeur, sa décision ne se substitue pas à celle de l'inspecteur ; que, par suite, s'il appartient au juge administratif, saisi d'un recours contre ces deux décisions, d'annuler, le cas échéant, celle du ministre par voie de conséquence de l'annulation de celle de l'inspecteur, des moyens critiquant les vices propres dont serait entachée la décision du ministre ne peuvent être utilement invoqués, au soutien des conclusions dirigées contre cette décision ; qu'ainsi, le moyen de M. A, tiré de ce que le ministre lui a opposé à tort la tardiveté de ses recours hiérarchiques, était, en tout état de cause, inopérant ; qu'il convient de l'écarter pour ce motif, qui doit être substitué au motif retenu par l'arrêt attaqué ; qu'il en résulte que M. A n'est pas fondé à demander l'annulation de cet arrêt en tant qu'il a rejeté ses conclusions tendant à l'annulation des décisions du ministre chargé du travail ;<br/>
<br/>
              Sur les conclusions tendant à l'application des dispositions de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              Considérant qu'il n'y a pas lieu, dans les circonstances de l'espèce, de mettre à la charge de M. A la somme réclamée par la société Safran Engineering Services au titre des frais exposés par elle et non compris dans les dépens ; <br/>
<br/>
<br/>
<br/>
<br/>
              			D E C I D E : <br/>
              			---------------<br/>
<br/>
            Article 1er : Le pourvoi de M. A est rejeté.<br/>
<br/>
Article 2 : Les conclusions de la société Safran Engineering Services présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
<br/>
Article 3 : La présente décision sera notifiée à M. Antoine A, à la société Safran Engineering Services et au ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social. <br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-07-01-04-03 PROCÉDURE. POUVOIRS ET DEVOIRS DU JUGE. QUESTIONS GÉNÉRALES. MOYENS. MOYENS INOPÉRANTS. - EXISTENCE - MOYENS CRITIQUANT LES VICES PROPRES DONT SERAIT AFFECTÉE LA DÉCISION REJETANT UN RECOURS HIÉRARCHIQUE CONTRE UN ACTE ADMINISTRATIF, LORSQUE, OUTRE L'ANNULATION DE CETTE DÉCISION, EST DEMANDÉE CELLE DE L'ACTE EN QUESTION [RJ1].
</SCT>
<ANA ID="9A"> 54-07-01-04-03 Requête à fin d'annulation, à la fois, d'une décision individuelle (autorisation de licenciement) et du refus de faire droit au recours hiérarchique présenté à l'encontre de cette même décision. Les moyens critiquant les vices propres dont la décision de rejet du recours hiérarchique serait entachée ne peuvent être utilement invoqués à l'appui d'une telle requête.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., dans le cas où est attaqué un décret réglementaire et le refus de faire droit au recours gracieux dirigé contre ce décret, CE, Section, 6 mars 2009, Syndicat national des ingénieurs de l'industrie et des mines, n° 309922, p. 93 ; s'agissant des décisions liant un plein contentieux, CE, 11 juin 2003, Colin, n° 248865, T. p. 899.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
