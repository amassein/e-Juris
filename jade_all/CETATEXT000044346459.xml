<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044346459</ID>
<ANCIEN_ID>JG_L_2021_11_000000440802</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/34/64/CETATEXT000044346459.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère - 4ème chambres réunies, 19/11/2021, 440802</TITRE>
<DATE_DEC>2021-11-19</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>440802</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère - 4ème chambres réunies</FORMATION>
<TYPE_REC/>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROCHETEAU, UZAN-SARANO</AVOCATS>
<RAPPORTEUR>Mme Manon Chonavel</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Vincent Villette</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:440802.20211119</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme D... C... a demandé au tribunal administratif de Lyon d'annuler la décision du 29 juillet 2019 du préfet du Rhône refusant d'autoriser son engagement dans un parcours de sortie de la prostitution et d'insertion sociale et professionnelle. Par un jugement n° 1907431 du 11 février 2020, le tribunal a rejeté cette demande.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 25 mai et 18 août 2020 au secrétariat du contentieux du Conseil d'Etat, Mme C... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 000 euros à verser à la SCP Rocheteau et Uzan-Sarano, son avocat, au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ; <br/>
<br/>
              Vu : <br/>
              - le code de l'action sociale et des familles ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - la loi n° 2016-444 du 13 avril 2016 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Manon Chonavel, auditrice,  <br/>
<br/>
              - les conclusions de M. Vincent Villette, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Rocheteau, Uzan-Sarano, avocat de Mme C... ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que, par une décision du 29 juillet 2019, le préfet de la région Auvergne Rhône-Alpes, préfet du Rhône, a refusé à Mme C... l'autorisation qu'elle avait sollicitée en vue de s'engager dans un parcours de sortie de la prostitution et d'insertion sociale et professionnelle en application de l'article L. 121-9 du code de l'action sociale et des familles. Par un jugement du 11 février 2020, contre lequel Mme C... se pourvoit en cassation, le tribunal administratif de Lyon a rejeté sa demande tendant à l'annulation de cette décision. <br/>
<br/>
              2. D'une part, aux termes de l'article L. 121-9 du code de l'action sociale et des familles : " I. - Dans chaque département, l'Etat assure la protection des personnes victimes de la prostitution, du proxénétisme ou de la traite des êtres humains et leur fournit l'assistance dont elles ont besoin (...). Une instance chargée d'organiser et de coordonner l'action en faveur des victimes de la prostitution, du proxénétisme et de la traite des êtres humains est créée dans chaque département. Elle met en œuvre le présent article. Elle est présidée par le représentant de l'Etat dans le département. Elle est composée de représentants de l'Etat, notamment des services de police et de gendarmerie, de représentants des collectivités territoriales, d'un magistrat, de professionnels de santé et de représentants d'associations. II. - Un parcours de sortie de la prostitution et d'insertion sociale et professionnelle est proposé à toute personne victime de la prostitution, du proxénétisme et de la traite des êtres humains aux fins d'exploitation sexuelle. Il est défini en fonction de l'évaluation de ses besoins sanitaires, professionnels et sociaux, afin de lui permettre d'accéder à des alternatives à la prostitution. Il est élaboré et mis en œuvre, en accord avec la personne accompagnée, par une association mentionnée à l'avant-dernier alinéa du présent II [c'est-à-dire toute association choisie par la personne concernée qui aide et accompagne les personnes en difficulté, en particulier les personnes prostituées, dès lors qu'elle remplit les conditions d'agrément fixées par décret en Conseil d'Etat]. L'engagement de la personne dans le parcours de sortie de la prostitution et d'insertion sociale et professionnelle est autorisé par le représentant de l'Etat dans le département, après avis de l'instance mentionnée au second alinéa du I et de l'association mentionnée au premier alinéa du présent II. / (...) Le renouvellement du parcours de sortie de la prostitution et d'insertion sociale et professionnelle est autorisé par le représentant de l'Etat dans le département, après avis de l'instance mentionnée au second alinéa du I et de l'association mentionnée au premier alinéa du présent II. La décision de renouvellement tient compte du respect de ses engagements par la personne accompagnée, ainsi que des difficultés rencontrées (...) ".<br/>
<br/>
              3. D'autre part, en vertu de l'article R. 121-12-9 du même code : " Les situations individuelles des personnes qui présentent une demande d'engagement dans un parcours de sortie de la prostitution ou qui en demandent le renouvellement font l'objet d'une instruction par l'association agréée. Celle-ci présente les engagements de la personne concernée, les actions prévues dans le cadre du projet d'insertion sociale et professionnelle, leur durée, les résultats attendus ou réalisés et émet un avis sur sa situation. La commission rend un avis sur la mise en place et le renouvellement des parcours de sortie de la prostitution et d'insertion sociale et professionnelle qui lui sont soumis. / Lors du renouvellement du parcours de sortie de la prostitution, la commission examine la mise en œuvre des actions menées au bénéfice de la personne et tient compte du respect des engagements figurant dans le document de suivi du parcours de sortie de la prostitution et d'insertion sociale et professionnelle prévu à l'article R. 121-12-12, ainsi que des difficultés rencontrées par la personne ". Selon l'article R. 121-12-10 du même code : " Après avis de la commission, le préfet de département autorise ou refuse d'autoriser l'engagement de la personne dans le parcours de sortie de la prostitution et d'insertion sociale et professionnelle ou son renouvellement. Il lui notifie sa décision, ainsi qu'à l'association en charge de l'instruction de la demande ".<br/>
<br/>
              4. En premier lieu, lorsqu'il statue sur un recours dirigé contre une décision refusant l'autorisation d'engagement d'une personne dans le parcours de sortie de la prostitution et d'insertion sociale et professionnelle, il appartient au juge administratif, ainsi que l'a d'ailleurs jugé le tribunal administratif, eu égard tant à la finalité de son intervention qu'à sa qualité de juge de plein contentieux, non de se prononcer sur les éventuels vices propres de la décision attaquée, mais d'examiner la situation de l'intéressé, en tenant compte de l'ensemble des circonstances de fait qui résultent de l'instruction. Au vu de ces éléments, il lui appartient d'annuler, s'il y a lieu, cette décision, en accueillant lui-même la demande de l'intéressé s'il apparaît, à la date à laquelle il statue, qu'un défaut d'autorisation d'engagement conduirait à une méconnaissance des dispositions du code de l'action sociale et des familles relatives à la protection des personnes victimes de la prostitution, du proxénétisme ou de la traite des êtres humains et en renvoyant le cas échéant l'intéressé devant l'administration afin qu'elle précise les modalités de ce parcours.<br/>
<br/>
              5. En second lieu, il résulte des dispositions mentionnées aux points 2 et 3, éclairées par les travaux préparatoires de la loi du 13 avril 2016 visant à renforcer la lutte contre le système prostitutionnel et à accompagner les personnes prostituées, dont l'article L. 121-9 du code de l'action sociale et des familles est issu, que le dispositif créé vise à offrir à toute personne victime de la prostitution, du proxénétisme ou de la traite des êtres humains aux fins d'exploitation sexuelle la possibilité d'accéder à des alternatives à la prostitution en suivant un parcours de sortie de la prostitution et d'insertion sociale et professionnelle, défini en fonction de l'évaluation de ses besoins sanitaires, professionnels et sociaux. Ce parcours est élaboré et mis en œuvre, en accord avec la personne accompagnée, par une association agréée, qui instruit, préalablement à la saisine de la commission compétente, la demande d'engagement dans le parcours ou son renouvellement en présentant les engagements de la personne concernée, les actions prévues dans le cadre du projet d'insertion sociale et professionnelle, leur durée ainsi que les résultats attendus ou réalisés lorsqu'il s'agit d'un renouvellement, et en émettant un avis sur la situation de l'intéressé. Le préfet de département, qui se prononce sur la demande initiale d'engagement dans le parcours au vu de l'instruction et de l'avis de l'association agréée et de l'avis de la commission compétente, prend sa décision en considération des mêmes éléments et doit vérifier la réalité de l'engagement de la personne à sortir de la prostitution. Lorsqu'il se prononce sur une demande de renouvellement, il tient compte du respect de ses engagements par la personne accompagnée ainsi que des difficultés rencontrées, au vu desquels la commission, après avoir examiné la mise en œuvre des actions menées au bénéfice de la personne, a rendu son avis.<br/>
<br/>
              6. Il résulte de ce qui précède qu'en prenant en considération, pour juger que le préfet était fondé à refuser à Mme C... l'autorisation d'engagement dans le parcours de sortie de la prostitution et d'insertion sociale et professionnelle qu'elle sollicitait, les circonstances qu'elle n'avait pas encore arrêté de se prostituer et qu'elle n'avait pas déposé de plainte à raison d'infractions portant sur la traite des êtres humains ou le proxénétisme, le tribunal administratif de Lyon s'est fondé sur des éléments qui ne pouvaient, contrairement à ce qu'il a jugé, caractériser l'absence de réalité de l'engagement de la personne et a, par suite, commis une erreur de droit. <br/>
<br/>
              7. Par suite, sans qu'il soit besoin de se prononcer sur les autres moyens du pourvoi, Mme C... est fondée à demander l'annulation du jugement qu'elle attaque. <br/>
<br/>
              8. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat une somme de 3 000 euros à verser à la SCP Rocheteau, Uzan-Sarano, avocat de Mme C..., au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991, sous réserve que cette société renonce à percevoir la somme correspondant à la part contributive de l'Etat.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : Le jugement du 11 février 2020 du tribunal administratif de Lyon est annulé. <br/>
Article 2 : L'affaire est renvoyée au tribunal administratif de Lyon. <br/>
Article 3 : L'Etat versera à la SCP Rocheteau, Uzan-Sarano, avocat de Mme C..., une somme de 3 000 euros au titre du deuxième alinéa de l'article 37 de la loi du 10 juillet 1991 et de l'article L. 761-1 du code de justice administrative, sous réserve que cette société renonce à percevoir la somme correspondant à la part contributive de l'Etat.<br/>
Article 4 : La présente décision sera notifiée à Mme D... C... et au ministre des solidarités et de la santé. <br/>
Copie en sera adressée au ministre de l'intérieur.<br/>
              Délibéré à l'issue de la séance du 20 octobre 2021 où siégeaient : Mme Christine Maugüé, présidente adjointe de la Section du Contentieux, présidant ; Mme A... O..., Mme F... N..., présidentes de chambre ; M. B... M..., Mme E... H..., Mme J... L..., M. K... I..., M. Damien Botteghi, conseillers d'Etat et Mme Manon Chonavel, auditrice-rapporteure. <br/>
<br/>
Rendu le 19 novembre 2021.<br/>
<br/>
<br/>
                 La présidente : <br/>
                 Signé : Mme Christine Maugüé<br/>
 		La rapporteure : <br/>
      Signé : Mme Manon Chonavel<br/>
                 La secrétaire :<br/>
                 Signé : Mme P... G...<br/>
La République mande et ordonne au ministre des solidarités et de la santé en ce qui le concerne ou à tous huissiers de justice à ce requis en ce qui concerne les voies de droit commun contre les parties privées, de pourvoir à l'exécution de la présente décision.<br/>
			Pour expédition conforme,<br/>
			Pour le secrétaire du contentieux, par délégation :<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">04-02 AIDE SOCIALE. - DIFFÉRENTES FORMES D'AIDE SOCIALE. - AIDE À LA SORTIE DE LA PROSTITUTION - 1) RECOURS CONTRE LE REFUS D'AUTORISER L'ENGAGEMENT DANS LE PARCOURS DE SORTIE DE LA PROSTITUTION - PLEIN CONTENTIEUX [RJ1] - EXISTENCE - OFFICE DU JUGE - 2) ELÉMENTS D'APPRÉCIATION PAR L'ADMINISTRATION - A) DE LA DEMANDE INITIALE D'ENGAGEMENT - B) DE LA DEMANDE DE RENOUVELLEMENT.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">04-04-01 AIDE SOCIALE. - CONTENTIEUX DE L'AIDE SOCIALE ET DE LA TARIFICATION. - CONTENTIEUX DE L'ADMISSION À L'AIDE SOCIALE. - RECOURS CONTRE LE REFUS D'AUTORISER L'ENGAGEMENT DANS LE PARCOURS DE SORTIE DE LA PROSTITUTION - PLEIN CONTENTIEUX [RJ1] - EXISTENCE - OFFICE DU JUGE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">54-02-02-01 PROCÉDURE. - DIVERSES SORTES DE RECOURS. - RECOURS DE PLEIN CONTENTIEUX. - RECOURS AYANT CE CARACTÈRE. - RECOURS CONTRE LE REFUS D'AUTORISER L'ENGAGEMENT DANS LE PARCOURS DE SORTIE DE LA PROSTITUTION - PLEIN CONTENTIEUX [RJ1] - OFFICE DU JUGE.
</SCT>
<ANA ID="9A"> 04-02 1) Lorsqu'il statue sur un recours dirigé contre une décision refusant l'autorisation d'engagement d'une personne dans le parcours de sortie de la prostitution et d'insertion sociale et professionnelle, il appartient au juge administratif, eu égard tant à la finalité de son intervention qu'à sa qualité de juge de plein contentieux, non de se prononcer sur les éventuels vices propres de la décision attaquée, mais d'examiner la situation de l'intéressé, en tenant compte de l'ensemble des circonstances de fait qui résultent de l'instruction.......Au vu de ces éléments, il lui appartient d'annuler, s'il y a lieu, cette décision, en accueillant lui-même la demande de l'intéressé s'il apparaît, à la date à laquelle il statue, qu'un défaut d'autorisation d'engagement conduirait à une méconnaissance des dispositions du code de l'action sociale et des familles (CASF) relatives à la protection des personnes victimes de la prostitution, du proxénétisme ou de la traite des êtres humains et en renvoyant le cas échéant l'intéressé devant l'administration afin qu'elle précise les modalités de ce parcours.......2) Il résulte des articles L. 121-9, R. 121-12-9 et R. 121-12-10 du CASF, éclairés par les travaux préparatoires de la loi n° 2016-444 du 13 avril 2016, dont l'article L. 121-9 est issu, que le dispositif créé vise à offrir à toute personne victime de la prostitution, du proxénétisme ou de la traite des êtres humains aux fins d'exploitation sexuelle la possibilité d'accéder à des alternatives à la prostitution en suivant un parcours de sortie de la prostitution et d'insertion sociale et professionnelle, défini en fonction de l'évaluation de ses besoins sanitaires, professionnels et sociaux. Ce parcours est élaboré et mis en œuvre, en accord avec la personne accompagnée, par une association agréée, qui instruit, préalablement à la saisine de la commission compétente, la demande d'engagement dans le parcours ou son renouvellement en présentant les engagements de la personne concernée, les actions prévues dans le cadre du projet d'insertion sociale et professionnelle, leur durée ainsi que les résultats attendus ou réalisés lorsqu'il s'agit d'un renouvellement, et en émettant un avis sur la situation de l'intéressé. ......a) Le préfet de département, qui se prononce sur la demande initiale d'engagement dans le parcours au vu de l'instruction et de l'avis de l'association agréée et de l'avis de la commission compétente, prend sa décision en considération des mêmes éléments et doit vérifier la réalité de l'engagement de la personne à sortir de la prostitution. ......b) Lorsqu'il se prononce sur une demande de renouvellement, il tient compte du respect de ses engagements par la personne accompagnée ainsi que des difficultés rencontrées, au vu desquels la commission, après avoir examiné la mise en œuvre des actions menées au bénéfice de la personne, a rendu son avis.</ANA>
<ANA ID="9B"> 04-04-01 Lorsqu'il statue sur un recours dirigé contre une décision refusant l'autorisation d'engagement d'une personne dans le parcours de sortie de la prostitution et d'insertion sociale et professionnelle, il appartient au juge administratif, eu égard tant à la finalité de son intervention qu'à sa qualité de juge de plein contentieux, non de se prononcer sur les éventuels vices propres de la décision attaquée, mais d'examiner la situation de l'intéressé, en tenant compte de l'ensemble des circonstances de fait qui résultent de l'instruction.......Au vu de ces éléments, il lui appartient d'annuler, s'il y a lieu, cette décision, en accueillant lui-même la demande de l'intéressé s'il apparaît, à la date à laquelle il statue, qu'un défaut d'autorisation d'engagement conduirait à une méconnaissance des dispositions du code de l'action sociale et des familles (CASF) relatives à la protection des personnes victimes de la prostitution, du proxénétisme ou de la traite des êtres humains et en renvoyant le cas échéant l'intéressé devant l'administration afin qu'elle précise les modalités de ce parcours.</ANA>
<ANA ID="9C"> 54-02-02-01 Lorsqu'il statue sur un recours dirigé contre une décision refusant l'autorisation d'engagement d'une personne dans le parcours de sortie de la prostitution et d'insertion sociale et professionnelle, il appartient au juge administratif, eu égard tant à la finalité de son intervention qu'à sa qualité de juge de plein contentieux, non de se prononcer sur les éventuels vices propres de la décision attaquée, mais d'examiner la situation de l'intéressé, en tenant compte de l'ensemble des circonstances de fait qui résultent de l'instruction.......Au vu de ces éléments, il lui appartient d'annuler, s'il y a lieu, cette décision, en accueillant lui-même la demande de l'intéressé s'il apparaît, à la date à laquelle il statue, qu'un défaut d'autorisation d'engagement conduirait à une méconnaissance des dispositions du code de l'action sociale et des familles (CASF) relatives à la protection des personnes victimes de la prostitution, du proxénétisme ou de la traite des êtres humains et en renvoyant le cas échéant l'intéressé devant l'administration afin qu'elle précise les modalités de ce parcours.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf., sur la nature de plein contentieux du recours dirigé contre une décision de l'administration déterminant les droits d'une personne en matière d'aide ou d'action sociale et sur l'office du juge, CE, Section, 3 juin 2019, Mme Vainqueur, n° 423001, p. 194.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
