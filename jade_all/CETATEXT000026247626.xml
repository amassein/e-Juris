<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026247626</ID>
<ANCIEN_ID>JG_L_2012_08_000000357207</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/24/76/CETATEXT000026247626.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  7ème sous-section jugeant seule, 01/08/2012, 357207, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2012-08-01</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>357207</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 7ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Rémy Schwartz</PRESIDENT>
<AVOCATS>SCP MASSE-DESSEN, THOUVENIN</AVOCATS>
<RAPPORTEUR>M. Stéphane Bouchard</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Bertrand Dacosta</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2012:357207.20120801</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la requête sommaire et le mémoire complémentaire, enregistrés les 29 février et 25 mai 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour la fédération générale agroalimentaire CFDT ; la fédération générale agroalimentaire<br/>
CFDT demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler le décret n° 2011-2093 du 30 décembre 2011 fixant les modalités de création de services communs entre les établissements du réseau des chambres d'agriculture ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat le versement de la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
              Vu la Constitution ;<br/>
<br/>
              Vu le code rural et de la pêche maritime ;<br/>
<br/>
	Vu la loi n° 2010-874 du 27 juillet 2010 ;<br/>
	Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Stéphane Bouchard, Maître des Requêtes en service extraordinaire, <br/>
<br/>
              - les observations de la SCP Masse-Dessen, Thouvenin, avocat de la fédération générale agroalimentaire CFDT,<br/>
<br/>
              - les conclusions de M. Bertrand Dacosta, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Masse-Dessen, Thouvenin, avocat de la fédération générale agroalimentaire CFDT ;<br/>
<br/>
<br/>
<br/>
              1. Considérant que l'article 70 de la loi du 27 juillet 2010 de modernisation de l'agriculture et de la pêche a inséré dans le III de l'article L. 514-2 du code rural et de la pêche maritime, relatif aux chambres d'agriculture, un alinéa aux termes duquel : " Les établissements du réseau peuvent créer entre eux, notamment pour l'exercice de missions de service public réglementaires, de fonctions de gestion ou d'administration interne, des services communs dont les règles de fonctionnement et de financement sont fixées par décret " ; que la fédération générale agroalimentaire CFDT demande l'annulation pour excès de pouvoir du décret du 30 décembre 2011 fixant les modalités de création de services communs entre les établissements du réseau des chambres d'agriculture, pris en application de cette disposition ; <br/>
<br/>
              Sur la légalité externe :<br/>
<br/>
              2. Considérant, en premier lieu, qu'aux termes de l'article 22 de la Constitution : " Les actes du Premier ministre sont contresignés, le cas échéant, par les ministres chargés de leur exécution.  " ; que, s'agissant d'un acte réglementaire, les ministres chargés de son exécution sont ceux qui ont compétence pour signer ou contresigner les mesures réglementaires ou individuelles que comporte nécessairement l'exécution de cet acte ; que le décret attaqué n'appelle aucune mesure réglementaire ou individuelle d'exécution de la part du ministre chargé de l'industrie ; que, par suite, le défaut de contreseing de ce ministre, qui n'était pas chargé de l'exécution du décret attaqué, n'entache pas celui-ci d'irrégularité ; <br/>
<br/>
              3. Considérant, en second lieu, qu'aux termes de l'article L. 514-3 du code rural et de la pêche maritime : " Une commission nationale de concertation et de proposition examine toutes questions relatives aux conditions d'emploi, de travail et de garanties sociales des personnels des chambres d'agriculture. (...) / La commission nationale de concertation et de proposition est habilitée à faire toute proposition à la commission nationale paritaire instituée par la loi n° 52-1311 du 10 décembre 1952 relative à l'établissement obligatoire d'un statut du personnel administratif des chambres d'agriculture, des chambres de commerce et d'industrie territoriales et des chambres de métiers. / (...) / La commission nationale paritaire peut saisir la commission nationale de concertation et de proposition de toute question entrant dans les attributions de ladite commission. / (...). " ; que ces dispositions ne prévoient aucune obligation à la charge du Premier ministre de saisir la commission nationale de concertation et de proposition de projets d'actes réglementaires ; que, par suite, le moyen tiré du défaut de consultation de cette commission doit être écarté ; <br/>
<br/>
              Sur la légalité interne :<br/>
<br/>
              4. Considérant qu'aux termes du II de l'article L. 514-2 du code rural et de la pêche maritime : " Chaque établissement du réseau des chambres d'agriculture est doté d'un budget unique. Il prévoit et autorise la totalité des dépenses et des recettes de l'établissement affectées à son fonctionnement et aux actions retracées dans les programmes d'intérêt général, y compris celles relatives à ses activités industrielles et commerciales. " ; qu'aux termes de l'article D. 514-27 du même code, dans sa rédaction résultant du décret attaqué : " Les opérations financières réalisées par chaque service commun font l'objet d'un suivi budgétaire spécifique à l'intérieur du budget de la chambre d'agriculture à laquelle il est rattaché, sous forme d'un programme spécifique. Conformément aux règles budgétaires applicables aux chambres d'agriculture, ses recettes et ses dépenses détaillent les opérations de fonctionnement et les opérations financières. / Le compte rendu annuel d'activité, les budgets et le compte financier de chaque service commun sont annexés aux budgets et au compte financier de l'établissement auquel il est rattaché. Ils sont transmis pour information à chaque établissement participant et à son agent comptable ainsi qu'à l'Assemblée permanente des chambres d'agriculture et aux autorités de tutelle de l'établissement auquel le service est rattaché. / (...) " ; <br/>
<br/>
              5. Considérant que les dispositions du décret attaqué, si elles prévoient un suivi budgétaire spécifique des opérations financières réalisées par chaque service commun, ne dérogent pas au principe de l'unicité du budget de chaque établissement du réseau des chambres d'agriculture, dès lors que ce suivi est réalisé à l'intérieur du budget de la chambre d'agriculture à laquelle sont rattachés les services communs, sous forme d'un programme spécifique ; que, par suite, le moyen tiré de la méconnaissance des dispositions de l'article L. 514-2 du code rural et de la pêche maritime n'est pas fondé ; <br/>
<br/>
              6. Considérant qu'il résulte de ce qui précède que la fédération générale agroalimentaire CFDT n'est pas fondée à demander l'annulation du décret qu'elle attaque ; que, par voie de conséquence, ses conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative doivent être rejetées ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de la fédération générale agroalimentaire CFDT est rejetée.<br/>
<br/>
Article 2 : La présente décision sera notifiée à la fédération générale agroalimentaire CFDT, au Premier ministre et au ministre de l'agriculture, de l'agroalimentaire et de la forêt.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
