<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032484115</ID>
<ANCIEN_ID>JG_L_2016_05_000000375579</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/48/41/CETATEXT000032484115.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème chambre, 02/05/2016, 375579, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-05-02</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>375579</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP RICHARD</AVOCATS>
<RAPPORTEUR>M. Timothée Paris</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Edouard Crépey</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2016:375579.20160502</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante:<br/>
<br/>
          M. B...A...a demandé au tribunal administratif de Versailles de prononcer la restitution des droits de taxe sur la valeur ajoutée qu'il a acquittés au cours de la période allant de 1990 à 2007. Par un jugement n° 0805103 du 2 octobre 2012, le tribunal a rejeté cette demande.<br/>
<br/>
          Par un arrêt n° 12VE04004 du 19 décembre 2013, la cour administrative d'appel de Versailles a rejeté l'appel formé par M. A...contre ce jugement.<br/>
<br/>
          Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 19 février et 19 mai 2014, M. A...demande au Conseil d'Etat :<br/>
<br/>
          1°) d'annuler cet arrêt ;<br/>
<br/>
          2°) de mettre à la charge de l'Etat la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
              Vu :<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - la directive 77/388/CE du Conseil des Communautés européennes du 17 mai 1977 ; <br/>
              - le code général des impôts et le livre des procédures fiscales ; <br/>
              - la loi n° 2002-303 du 4 mars 2002, notamment son article 75 ; <br/>
              - le décret n° 2007-435 du 25 mars 2007 ; <br/>
              - le décret n° 2007-437 du 25 mars 2007 ; <br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Timothée Paris, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Edouard Crépey, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Richard, avocat de M. B...A...;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Contrairement à ce qui est soutenu, la cour administrative d'appel, qui a rejeté au fond l'appel de M.A..., n'avait pas à répondre au moyen invoqué par le ministre en défense, tiré de ce que le délai prévu par l'article R. 196-1 du Livre de procédures fiscales n'était pas opposable à la demande.<br/>
<br/>
              2. Aux termes de l'article 13, A, paragraphe 1 de la sixième directive du Conseil du 17 mai 1977 en matière d'harmonisation des législations des Etats membres relatives aux taxes sur le chiffre d'affaires : " Sans préjudice d'autres dispositions communautaires, les Etats membres exonèrent, dans les conditions qu'ils fixent en vue d'assurer l'application correcte et simple des exonérations prévues ci-dessous et de prévenir toute fraude, évasion et abus éventuels : / (...) c) les prestations de soins à la personne effectuées dans le cadre de l'exercice des professions médicales et paramédicales telles qu'elles sont définies par l'Etat membre concerné (...) " et, en vertu du 1° du 4 de l'article 261 du code général des impôts, dans sa rédaction applicable à la période d'imposition en litige, sont exonérés de la taxe sur la valeur ajoutée : " Les soins dispensés aux personnes par les membres des professions médicales et paramédicales réglementées (...) ". En limitant l'exonération qu'elles prévoient aux soins dispensés par les membres des professions médicales et paramédicales soumises à réglementation, ces dispositions ne méconnaissent pas l'objectif poursuivi par l'article 13, A, paragraphe 1, sous c) de la sixième directive, précité, qui est de garantir que l'exonération s'applique uniquement aux prestations de soins à la personne fournies par des prestataires possédant les qualifications professionnelles requises. En effet, la directive renvoie à la réglementation interne des Etats membres la définition de la notion de professions paramédicales, des qualifications requises pour exercer ces professions et des activités spécifiques de soins à la personne qui relèvent de telles professions. Toutefois, conformément à l'interprétation des dispositions de la sixième directive qui résulte de l'arrêt rendu le 27 avril 2006 par la Cour de justice des Communautés européennes dans les affaires C-443/04 et C-444/04, l'exclusion d'une profession ou d'une activité spécifique de soins à la personne de la définition des professions paramédicales retenue par la réglementation nationale aux fins de l'exonération de la taxe sur la valeur ajoutée prévue à l'article 13, A, paragraphe 1, sous c) de cette directive, serait contraire au principe de neutralité fiscale inhérent au système commun de taxe sur la valeur ajoutée s'il pouvait être démontré que les personnes exerçant cette profession ou cette activité disposent, pour la fourniture de telles prestations de soins, de qualifications professionnelles propres à assurer à ces prestations un niveau de qualité équivalente à celles fournies par des personnes bénéficiant, en vertu de la réglementation nationale, de l'exonération.<br/>
<br/>
              3. Aux termes de l'article 75 de la loi du 4 mars 2002 relative aux droits des malades et à la qualité du système de santé, dans sa version applicable au présent litige : " L'usage professionnel du titre d'ostéopathe (...) est réservé aux personnes titulaires d'un diplôme sanctionnant une formation spécifique à l'ostéopathie ou à la chiropraxie délivrée par un établissement de formation agréé par le ministre chargé de la santé dans des conditions fixées par décret. Le programme et la durée des études préparatoires et des épreuves après lesquelles peut être délivré ce diplôme sont fixés par voie réglementaire. (...) Les praticiens en exercice, à la date d'application de la présente loi, peuvent se voir reconnaître le titre d'ostéopathe ou de chiropracteur s'ils satisfont à des conditions de formation ou d'expérience professionnelle analogues à celles des titulaires du diplôme mentionné au premier alinéa. Ces conditions sont déterminées par décret. (...) Un décret établit la liste des actes que les praticiens justifiant du titre d'ostéopathe ou de chiropracteur sont autorisés à effectuer, ainsi que les conditions dans lesquelles ils sont appelés à les accomplir. / Ces praticiens ne peuvent exercer leur profession que s'ils sont inscrits sur une liste dressée par le représentant de l'Etat dans le département de leur résidence professionnelle, qui enregistre leurs diplômes, certificats, titres ou autorisations ".<br/>
<br/>
              4. Jusqu'à la date d'entrée en vigueur, le 25 mars 2007, des décrets relatifs aux actes et aux conditions d'exercice de l'ostéopathie et à la formation des ostéopathes et à l'agrément des établissements de formation en ostéopathie, ainsi que de l'arrêté intervenu en application de ces deux décrets, les actes dits d'ostéopathie ne pouvaient être pratiqués que par les docteurs en médecine, et le cas échéant, pour certains actes seulement et sur prescription médicale, par les autres professionnels de santé habilités à les réaliser.<br/>
<br/>
              5. Saisie d'une demande de restitution des droits de taxe sur la valeur ajoutée acquittés par une personne exerçant l'activité d'ostéopathe sur ses prestations d'ostéopathie antérieurement à l'entrée en vigueur des décrets et de l'arrêté du 25 mars 2007, la cour devait vérifier que celle-ci disposait, pour la fourniture de ces prestations, de qualifications professionnelles propres à leur assurer un niveau de qualité équivalente à celles fournies, selon le cas, par un médecin ou par un masseur-kinésithérapeute. Or, une telle appréciation ne peut être portée qu'au vu de la nature des actes accomplis sous la dénomination d'actes d'ostéopathie et, s'agissant des actes susceptibles de comporter des risques en cas de contre-indication médicale, en considération des conditions dans lesquelles ils ont été effectués. Etait sans incidence, à cet égard, la circonstance invoquée par M.A..., tirée de ce qu'il avait, ultérieurement, obtenu le droit d'user professionnellement du titre d'ostéopathe dans les conditions prévues par les dispositions mentionnées au point 3. <br/>
<br/>
              6. Dans ces conditions, il appartenait à M. A..., pour mettre le juge du fond à même de s'assurer que la condition tenant à la qualité des actes était remplie, de produire, d'une part, et sous réserve de l'occultation des noms des patients, des éléments relatifs à sa pratique permettant d'appréhender, sur une période significative, la nature des actes accomplis et les conditions dans lesquelles ils l'ont été et, d'autre part, tous éléments utiles relatifs à ses qualifications professionnelles. Il suit de là que c'est sans entacher son arrêt d'insuffisance de motivation ni d'erreur de droit que la cour administrative d'appel de Versailles a relevé, au terme d'une appréciation qui n'est pas arguée de dénaturation,  que M. A...ne produisait aucun élément relatif à sa pratique au cours de la période en litige et, et en a déduit que, alors même que l'intéressé se prévalait de l'autorisation d'user professionnellement du titre d'ostéopathe obtenue postérieurement, qu'il n'établissait pas que les actes d'ostéopathie qu'il avait accomplis auraient pu être considérés comme d'une qualité équivalente à ceux qui, s'ils avaient été effectués par un médecin, auraient bénéficié de l'exonération de taxe sur la valeur ajoutée. Ce faisant, la cour n'a en rien méconnu les stipulations de l'article 1er du premier protocole additionnel à la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales.<br/>
<br/>
              7. Il résulte de tout ce qui précède que M. A... n'est pas fondé à demander l'annulation de l'arrêt qu'il attaque. Ses conclusions présentées sur le fondement de l'article L. 761-1 du code de justice administrative ne peuvent, par suite, qu'être rejetées.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de M. A... est rejeté.<br/>
Article 2 : La présente décision sera notifiée à M. B... A...et au ministre des finances et des comptes publics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
