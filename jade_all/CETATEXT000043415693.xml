<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043415693</ID>
<ANCIEN_ID>JG_L_2021_04_000000445328</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/41/56/CETATEXT000043415693.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère chambre, 22/04/2021, 445328, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-04-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>445328</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR>Mme Bénédicte Fauvarque-Cosson</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Vincent Villette</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:445328.20210422</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. D... B... a demandé au juge des référés du tribunal administratif de Grenoble :<br/>
              - de suspendre, sur le fondement de l'article L. 521-1 du code de justice administrative, l'exécution de la décision du 15 juillet 2020 par laquelle le président du conseil départemental de l'Isère a refusé de maintenir sa prise en charge au titre de l'aide sociale à l'enfance, jusqu'à l'intervention de la décision administrative prise à la suite de son recours préalable obligatoire ;<br/>
              - d'enjoindre au président du conseil départemental de lui accorder le bénéfice d'une mesure administrative en faveur des jeunes majeurs, dans un délai de 48 heures à compter de la notification de l'ordonnance à intervenir, sous astreinte de 100 euros par jour de retard ou, à défaut, de réexaminer sa demande dans un délai de deux mois à compter de la notification de l'ordonnance à intervenir, sous astreinte de 100 euros par jour de retard ;<br/>
              - dans l'attente, d'enjoindre au département de poursuivre sa prise en charge par les services de l'aide sociale à l'enfance, dans un délai de 24 heures à compter de la notification de l'ordonnance à intervenir, sous astreinte de 100 euros par jour de retard.<br/>
<br/>
              Par une ordonnance n° 2005246 du 29 septembre 2020, le juge des référés du tribunal administratif de Grenoble a suspendu l'exécution de la décision du 15 juillet 2020 et enjoint au président du conseil départemental de l'Isère de reprendre en charge M. B... dans un délai de cinq jours. <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 14 et 29 octobre 2020 au secrétariat du contentieux du Conseil d'Etat, le département de l'Isère demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) statuant en référé, de rejeter la demande de M. B....<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de l'action sociale et des familles ;<br/>
              - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme A... C..., conseillère d'Etat,  <br/>
<br/>
              - les conclusions de M. Vincent Villette, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Piwinica, Molinié, avocat du département de l'Isère ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis au juge des référés du tribunal administratif que M. B..., ressortissant malien né le 12 février 2001 et entré en France en octobre 2017, a été confié au service de l'aide sociale à l'enfance de l'Isère par le juge des enfants du tribunal de grande instance de Grenoble, par une ordonnance de placement provisoire du 23 octobre 2017 puis un jugement du 10 novembre 2017, jusqu'à sa majorité. Le département de l'Isère, qui a décidé, à la demande de l'intéressé, de poursuivre sa prise en charge par l'aide sociale à l'enfance en application des dispositions du sixième alinéa de l'article L. 222-5 du code de l'action sociale et des familles jusqu'au 31 août 2019, dans le cadre du dispositif "contrat jeune majeur ", a maintenu celle-ci au-delà de cette date, en raison du contexte sanitaire. Par une décision du 15 juillet 2020, le président du conseil départemental de l'Isère a rejeté sa demande de renouvellement de sa prise en charge et fixé le terme de celle-ci au 31 août 2020. Par une ordonnance du 29 septembre 2020, le juge des référés du tribunal administratif de Grenoble, saisi par M. B... sur le fondement de l'article L. 521-1 du code de justice administrative, a suspendu l'exécution de cette décision et enjoint au département de l'Isère de le reprendre en charge. Le département de l'Isère se pourvoit en cassation contre cette ordonnance.<br/>
<br/>
              2. Aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision ".<br/>
<br/>
              3. Aux termes de l'article L. 221-1 du code de l'action sociale et des familles : " Le service de l'aide sociale à l'enfance est un service non personnalisé du département chargé des missions suivantes : / 1° Apporter un soutien matériel, éducatif et psychologique tant aux mineurs et à leur famille ou à tout détenteur de l'autorité parentale, confrontés à des difficultés risquant de mettre en danger la santé, la sécurité, la moralité de ces mineurs ou de compromettre gravement leur éducation ou leur développement physique, affectif, intellectuel et social, qu'aux mineurs émancipés et majeurs de moins de vingt et un ans confrontés à des difficultés familiales, sociales et éducatives susceptibles de compromettre gravement leur équilibre (...) ". L'article L. 222-5 du même code détermine les personnes susceptibles, sur décision du président du conseil départemental, d'être prises en charge par le service de l'aide sociale à l'enfance, parmi lesquelles, au titre du 1° de cet article, les mineurs qui ne peuvent demeurer provisoirement dans leur milieu de vie habituel et dont la situation requiert un accueil à temps complet ou partiel et, au titre de son 3°, les mineurs confiés au service par le juge des enfants parce que leur protection l'exige. Aux termes des sixième et septième alinéas de cet article : " Peuvent être également pris en charge à titre temporaire par le service chargé de l'aide sociale à l'enfance les mineurs émancipés et les majeurs âgés de moins de vingt et un ans qui éprouvent des difficultés d'insertion sociale faute de ressources ou d'un soutien familial suffisants. / Un accompagnement est proposé aux jeunes mentionnés au 1° du présent article devenus majeurs et aux majeurs mentionnés à l'avant-dernier alinéa, au-delà du terme de la mesure, pour leur permettre de terminer l'année scolaire ou universitaire engagée ".<br/>
<br/>
              4. Sous réserve de l'hypothèse dans laquelle un accompagnement doit être proposé au jeune pour lui permettre de terminer l'année scolaire ou universitaire engagée, le président du conseil départemental dispose d'un large pouvoir d'appréciation pour accorder ou maintenir la prise en charge par le service de l'aide sociale à l'enfance d'un jeune majeur de moins de vingt et un ans éprouvant des difficultés d'insertion sociale faute de ressources ou d'un soutien familial suffisants et peut à ce titre, notamment, prendre en considération les perspectives d'insertion qu'ouvre une prise en charge par ce service compte tenu de l'ensemble des circonstances de l'espèce, y compris le comportement du jeune majeur.<br/>
<br/>
              5. Lorsqu'il statue sur un recours dirigé contre une décision refusant une prise en charge par le service de l'aide sociale à l'enfance ou mettant fin à une telle prise en charge, il appartient au juge administratif, eu égard tant à la finalité de son intervention qu'à sa qualité de juge de plein contentieux, non de se prononcer sur les éventuels vices propres de la décision attaquée, mais d'examiner la situation de l'intéressé, en tenant compte de l'ensemble des circonstances de fait qui résultent de l'instruction et, notamment, du dossier qui lui est communiqué en application de l'article R. 772-8 du code de justice administrative. Au vu de ces éléments, il lui appartient d'annuler, s'il y a lieu, cette décision en accueillant lui-même la demande de l'intéressé s'il apparaît, à la date à laquelle il statue, eu égard à la marge d'appréciation dont dispose le président du conseil départemental dans leur mise en oeuvre, qu'un défaut de prise en charge conduirait à une méconnaissance des dispositions du code de l'action sociale et des familles relatives à la protection de l'enfance et en renvoyant l'intéressé devant l'administration afin qu'elle précise les modalités de cette prise en charge sur la base des motifs de son jugement. Saisi d'une demande de suspension de l'exécution d'une telle décision, il appartient, ainsi, au juge des référés de rechercher si, à la date à laquelle il se prononce, ces éléments font apparaître, en dépit de cette marge d'appréciation, un doute sérieux quant à la légalité d'un défaut de prise en charge.<br/>
<br/>
              6. Il ressort des pièces du dossier soumis au juge des référés du tribunal administratif que pour mettre fin, par la décision du 15 juillet 2020, à la prise en charge de M. B..., le président du conseil départemental de l'Isère a relevé qu'il avait été pris en charge par le département pendant deux ans et neuf mois, dont un an et cinq mois depuis sa majorité, qu'il était en bonne santé, célibataire et sans enfant à charge, qu'il disposait de relations amicales et demeurait en contact avec son maître d'apprentissage, et s'est fondé notamment sur l'aboutissement de sa formation marquée par l'obtention en juin 2020 d'un certificat d'aptitude professionnelle " carrelage mosaïque " et sur l'arrêté du 20 juin 2019 lui faisant obligation de quitter le territoire français. Pour juger que le moyen tiré de ce que cette décision serait entachée d'une erreur manifeste d'appréciation était de nature à créer un doute sérieux, le juge des référés du tribunal administratif s'est seulement fondé sur l'annulation, par un jugement du tribunal administratif de Grenoble du 31 août 2020, de l'obligation qui avait été faite à M. B... de quitter le territoire français, sur l'injonction faite par ce jugement au préfet de l'Isère de lui délivrer une autorisation provisoire de travail, et sur la nécessité de lui apporter une solution d'hébergement à court terme afin de lui permettre d'effectuer les démarches nécessaires à son accès à l'autonomie. En statuant ainsi, le juge des référés du tribunal administratif ne peut être regardé comme ayant tenu compte, pour apprécier l'existence d'un doute sérieux quant à la légalité de la décision mettant fin à la prise en charge, comme il le lui appartient ainsi qu'il a été dit au point précédent, de la marge d'appréciation dont dispose le président du conseil départemental pour accorder ou maintenir la prise en charge par le service de l'aide sociale à l'enfance d'un jeune majeur de moins de vingt et un ans, sous réserve de l'hypothèse dans laquelle un accompagnement doit être proposé à ce dernier pour lui permettre de terminer l'année scolaire ou universitaire engagée. Il a, par suite, commis une erreur de droit.<br/>
<br/>
              7. Il résulte de ce qui précède que le département de l'Isère est fondé à demander pour ce motif l'annulation de l'ordonnance qu'il attaque. Le moyen retenu suffisant à entraîner cette annulation, il n'est pas nécessaire d'examiner les autres moyens du pourvoi.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
Article 1er : L'ordonnance du juge des référés du tribunal administratif de Paris du 29 septembre 2020 est annulée. <br/>
Article 2 : L'affaire est renvoyée au tribunal administratif de Grenoble.<br/>
Article 3 : La présente décision sera notifiée au département de l'Isère.<br/>
Copie en sera adressée à M. D... B....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
