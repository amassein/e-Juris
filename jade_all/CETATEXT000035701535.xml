<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000035701535</ID>
<ANCIEN_ID>JG_L_2017_10_000000399753</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/35/70/15/CETATEXT000035701535.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère - 6ème chambres réunies, 02/10/2017, 399753</TITRE>
<DATE_DEC>2017-10-02</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>399753</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère - 6ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ZRIBI, TEXIER ; LE PRADO</AVOCATS>
<RAPPORTEUR>M. Frédéric Pacoud</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Charles Touboul</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:399753.20171002</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
Procédure contentieuse antérieure<br/>
<br/>
              M. A...C...a demandé au tribunal administratif de Bordeaux d'annuler les décisions verbales des 28 mai et 1er juin 2010 par lesquelles le centre hospitalier Charles Perrens de Bordeaux a refusé de lui accorder le droit de rendre visite à son fils majeur, hospitalisé d'office dans cet établissement. Par une ordonnance n° 1002843 du 13 octobre 2011, le président de la 4ème chambre du tribunal administratif de Bordeaux a rejeté sa demande comme portée devant une juridiction incompétente pour en connaître.<br/>
<br/>
              Par un arrêt n° 12BX02532 du 11 février 2014, la cour administrative d'appel de Bordeaux a rejeté l'appel formé par M. C...contre cette ordonnance.<br/>
<br/>
              Par une décision n° 381648 du 26 juin 2015, le Conseil d'Etat statuant au contentieux a annulé cet arrêt et renvoyé l'affaire à la cour administrative d'appel de Bordeaux. <br/>
<br/>
              Par un arrêt n° 15BX02216 du 8 décembre 2015, statuant sur renvoi du Conseil d'Etat, la cour administrative d'appel de Bordeaux a annulé l'ordonnance du président de la 4ème chambre du tribunal administratif de Bordeaux et rejeté la demande de M.C....<br/>
<br/>
Procédure devant le Conseil d'Etat<br/>
<br/>
              Par un pourvoi, enregistré le 12 mai 2016 au secrétariat du contentieux du Conseil d'Etat, M. C...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt de la cour administrative d'appel de Bordeaux du 8 décembre 2015 ;<br/>
<br/>
              2°) statuant au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge du centre hospitalier Charles Perrens la somme de 4 000 euros à verser à MeD..., son avocat, au titre des dispositions des articles 37 de la loi 10 juillet 1991 et L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de la santé publique ;<br/>
              - la loi n° 79-587 du 11 juillet 1979 ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Frédéric Pacoud, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Charles Touboul, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Zribi, Texier, avocat de M. C...et à Me Le Prado, avocat du centre hospitalier Charles Perrens.<br/>
<br/>
              Vu la note en délibéré, enregistrée le 14 septembre 2017, présentée pour M. A... C.... <br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que le fils majeur de M. C...a été hospitalisé avec son consentement au centre hospitalier Charles Perrens de Bordeaux le 21 juillet 2008. Par un arrêté du 26 mai 2010, le préfet de la Gironde a ordonné son hospitalisation d'office au sein du même établissement jusqu'au 26 juin 2010, à la suite d'un acte de violence commis à l'encontre d'un membre du personnel hospitalier. M. C...s'étant présenté au centre hospitalier pour voir son fils, les 28 mai et 1er juin 2010, il lui a été verbalement signifié qu'il n'était pas autorisé à lui rendre visite. M. C...a contesté ces décisions de refus sans succès devant le tribunal administratif de Bordeaux. Il se pourvoit en cassation contre l'arrêt du 8 décembre 2015 par lequel la cour administrative d'appel de Bordeaux a rejeté son appel, sur lequel elle statuait de nouveau après renvoi de l'affaire par le Conseil d'Etat, en jugeant notamment que ces décisions n'avaient pas à être motivées et n'étaient pas entachées d'erreur manifeste d'appréciation.<br/>
<br/>
              Sur la régularité de l'arrêt attaqué :<br/>
<br/>
              2. Aux termes des deux premiers alinéas de l'article L. 1110-4 du code de la santé publique, dans sa rédaction applicable à la procédure devant la cour : " Toute personne prise en charge par un professionnel, un établissement, un réseau de santé ou tout autre organisme participant à la prévention et aux soins a droit au respect de sa vie privée et du secret des informations la concernant. / Excepté dans les cas de dérogation, expressément prévus par la loi, ce secret couvre l'ensemble des informations concernant la personne venues à la connaissance du professionnel de santé, de tout membre du personnel de ces établissements ou organismes et de toute autre personne en relation, de par ses activités, avec ces établissements ou organismes. Il s'impose à tout professionnel de santé, ainsi qu'à tous les professionnels intervenant dans le système de santé ". Il résulte de ces dispositions que le secret médical s'oppose à ce que soient divulguées, sans l'accord du malade hospitalisé, les raisons médicales de la décision refusant à un tiers le droit de lui rendre visite. Toutefois, ce secret n'est pas opposable à la personne que le malade a autorisée à accéder à son dossier médical, <br/>
<br/>
              3. D'une part, la circonstance qu'un établissement de santé, dans un contentieux l'opposant à un proche d'un patient, ait produit des pièces de sa propre initiative, en méconnaissance du secret médical qui s'impose à lui, n'est pas par elle-même de nature à affecter la régularité ou le bien-fondé de la décision du juge. <br/>
<br/>
              4. D'autre part, le juge, auquel il incombe, dans la mise en oeuvre de ses pouvoirs d'instruction, de veiller au respect des droits des parties, d'assurer l'égalité des armes entre elles et de garantir, selon les modalités propres à chacun d'entre eux, les secrets protégés par la loi, ne peut régulièrement se fonder sur de telles pièces qu'à la condition d'avoir pu préalablement les soumettre au débat contradictoire.<br/>
<br/>
              5. En l'espèce, il ressort des pièces du dossier soumis aux juges du fond que, par un document daté du 28 mai 2010, M. B...C...a autorisé son père, M. A... C..., à accéder à son dossier médical détenu par le centre hospitalier Charles Perrens, puis, par un document daté du 17 février 2011, à produire en justice les pièces de ce dossier qui lui sembleraient indispensables, dossier dont sont issus le bilan médical du 26 mai 2010 ainsi que les informations relatives à l'état clinique et aux soins dispensés et les comptes rendus établis entre le 25 mai et le 10 juin 2010 que M. C...a joints à ses mémoires devant le tribunal administratif de Bordeaux. La cour, qui n'a pas dénaturé les pièces du dossier en relevant que le mandat donné à M. C... par son fils lui permettait de produire des pièces de son dossier médical dans le cadre du recours contentieux engagé et que celui-ci avait ainsi accepté leur soumission au débat contradictoire, n'a pas entaché d'irrégularité la procédure au terme de laquelle elle a statué en prenant en considération pour apprécier la légalité des décisions attaquées, après l'avoir communiqué à M.C..., le certificat médical du 3 juin 2010, adressé à la direction des usagers du centre hospitalier par le chef du pôle de psychiatrie générale et établissant les motifs de l'interdiction de visites, que ce centre avait joint à son mémoire en défense devant le tribunal.<br/>
<br/>
              Sur le bien-fondé de l'arrêt attaqué :<br/>
<br/>
              6. En premier lieu, après l'expiration du délai de recours contre un acte administratif, sont irrecevables, sauf s'ils sont d'ordre public, les moyens soulevés par le demandeur qui relèvent d'une cause juridique différente de celle à laquelle se rattachent les moyens invoqués dans sa demande avant l'expiration de ce délai. Ce délai de recours commence, en principe, à courir à compter de la publication ou de la notification complète et régulière de l'acte attaqué. Toutefois, à défaut, il court, au plus tard, à compter, pour ce qui concerne un demandeur donné, de l'introduction de son recours contentieux contre cet acte.<br/>
<br/>
              7. Il ressort des pièces du dossier soumis au juge du fond que M. C...a soulevé le moyen de légalité externe tiré de l'absence de motivation des décisions verbales par lesquelles le centre hospitalier lui a refusé le droit de rendre visite à son fils dans un mémoire enregistré le 3 novembre 2010 au greffe du tribunal administratif de Bordeaux, soit plus de deux mois après l'introduction de son recours, le 29 juillet 2010, alors qu'il n'avait jusque là soulevé aucun moyen se rattachant à la même cause juridique. Un tel moyen, qui n'est pas d'ordre public, était, dès lors, irrecevable. Par suite, sans qu'il soit besoin d'examiner le moyen d'erreur de droit soulevé par M.C..., il y a lieu de substituer ce motif, dont l'examen n'implique l'appréciation d'aucune circonstance de fait et qui justifie sur ce point le dispositif de l'arrêt attaqué, au motif retenu par la cour pour écarter la méconnaissance de la loi du 11 juillet 1979 relative à la motivation des actes administratifs et à l'amélioration des relations entre l'administration et le public. <br/>
<br/>
              8. En second lieu, aux termes du premier alinéa de l'article L. 3211-3 du code de la santé publique, dans sa rédaction applicable à la date des décisions en litige : " Lorsqu'une personne atteinte de troubles mentaux est hospitalisée sans son consentement en application des dispositions des chapitres II et III du présent titre ou est transportée en vue de cette hospitalisation, les restrictions à l'exercice de ses libertés individuelles doivent être limitées à celles nécessitées par son état de santé et la mise en oeuvre de son traitement. En toutes circonstances, la dignité de la personne hospitalisée doit être respectée et sa réinsertion recherchée ". Aux termes de l'article R. 1112-47 du même code : " Les visiteurs ne doivent pas troubler le repos des malades ni gêner le fonctionnement des services. Lorsque cette obligation n'est pas respectée, l'expulsion du visiteur et l'interdiction de visite peuvent être décidées par le directeur. / (...) / Les malades peuvent demander aux cadres infirmiers du service de ne pas permettre aux personnes qu'ils désignent d'avoir accès à eux ". <br/>
<br/>
              9. Il résulte de ces dispositions qu'il peut être interdit au proche d'un patient hospitalisé sans son consentement de rendre visite à celui-ci au motif, notamment, qu'une telle visite n'est pas compatible avec l'état de santé du patient ou la mise en oeuvre de son traitement. En jugeant que les décisions des 28 mai et 1er juin 2010 avaient pu légalement se fonder sur l'état de santé du fils de M.C..., eu égard tant aux documents médicaux figurant au dossier qu'aux dates de ces décisions, intervenues trois et cinq jours après la crise ayant justifié son hospitalisation d'office, la cour n'a pas commis d'erreur de droit. <br/>
<br/>
              10. Il résulte de tout ce qui précède que M. C...n'est pas fondé à demander l'annulation de l'arrêt de la cour administrative d'appel de Bordeaux qu'il attaque. <br/>
<br/>
              Sur les frais exposés par les parties à l'occasion du litige :<br/>
<br/>
              11. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à la charge du centre hospitalier Charles Perrens, qui n'est pas, dans la présente instance, la partie perdante. En revanche il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de M. C...une somme de 1 000 euros à verser au centre hospitalier Charles Perrens, au titre de ces mêmes dispositions. <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de M. C...est rejeté. <br/>
Article 2 : M. C...versera au centre hospitalier Charles Perrens une somme de 1 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
Article 3 : La présente décision sera notifiée à M. A...C...et au centre hospitalier Charles Perrens.<br/>
Copie en sera adressée à la ministre des solidarités et de la santé. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">26-03-10 DROITS CIVILS ET INDIVIDUELS. LIBERTÉS PUBLIQUES ET LIBERTÉS DE LA PERSONNE. SECRET DE LA VIE PRIVÉE. - SECRET MÉDICAL (ART. L. 1110-4 DU CSP) - PRODUCTION, DE SA PROPRE INITIATIVE, DE PIÈCES COUVERTES PAR CE SECRET PAR UN ÉTABLISSEMENT MÉDICAL À UN TIERS - 1) CIRCONSTANCE  DE NATURE À ENTACHER LA RÉGULARITÉ OU LE BIEN FONDÉ DE LA DÉCISION DU JUGE - ABSENCE - 2) POSSIBILITÉ POUR LE JUGE DE SE FONDER SUR DE TELLES PIÈCES - CONDITIONS [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">49-05-01-01 POLICE. POLICES SPÉCIALES. POLICE DES ALIÉNÉS (VOIR AUSSI : SANTÉ PUBLIQUE). PLACEMENT D'OFFICE. - POSSIBILITÉ DE REFUSER UNE VISITE À UN PATIENT HOSPITALISÉ (ART. R. 1112-47 DU CSP) - EXISTENCE, NOTAMMENT SI LA VISITE N'EST PAS COMPATIBLE AVEC L'ÉTAT DE SANTÉ DU PATIENT OU LA MISE EN OEUVRE DE SON TRAITEMENT.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">54-06-01 PROCÉDURE. JUGEMENTS. RÈGLES GÉNÉRALES DE PROCÉDURE. - SECRET MÉDICAL (ART. L. 1110-4 DU CSP) - PRODUCTION DE SA PROPRE INITIATIVE DE PIÈCES COUVERTES PAR CE SECRET PAR UN ÉTABLISSEMENT MÉDICAL À UN TIERS - 1) CIRCONSTANCE  DE NATURE À ENTACHER LA RÉGULARITÉ OU LE BIEN FONDÉ DE LA DÉCISION DU JUGE - ABSENCE - 2) POSSIBILITÉ POUR LE JUGE DE SE FONDER SUR DE TELLES PIÈCES - CONDITIONS [RJ1].
</SCT>
<SCT ID="8D" TYPE="PRINCIPAL">54-07-01-04-01 PROCÉDURE. POUVOIRS ET DEVOIRS DU JUGE. QUESTIONS GÉNÉRALES. MOYENS. MOYENS D'ORDRE PUBLIC À SOULEVER D'OFFICE. - OBLIGATION DE COMMUNIQUER UN MOP (ART. 611-7 DU CJA) - CASSATION - MOYEN IRRECEVABLE POUR TARDIVETÉ ÉCARTÉ AU FOND PAR LES JUGES DU FOND - MOTIF SUBSTITUÉ EN CASSATION AU MOTIF RETENU PAR LES JUGES DU FOND - ABSENCE D'OBLIGATION DE COMMUNIQUER (SOL. IMPL.) [RJ2].
</SCT>
<SCT ID="8E" TYPE="PRINCIPAL">54-07-01-06 PROCÉDURE. POUVOIRS ET DEVOIRS DU JUGE. QUESTIONS GÉNÉRALES. SUBSTITUTION DE MOTIFS. - OFFICE DU JUGE DE CASSATION - MOYEN IRRECEVABLE POUR TARDIVETÉ ÉCARTÉ AU FOND PAR LES JUGES DU FOND - MOTIF SUBSTITUÉ EN CASSATION AU MOTIF RETENU PAR LES JUGES DU FOND - OBLIGATION DE COMMUNIQUER UN MOP (ART. 611-7 DU CJA) - ABSENCE (SOL. IMPL.) [RJ2].
</SCT>
<ANA ID="9A"> 26-03-10 1) La circonstance qu'un établissement de santé, dans un contentieux l'opposant à un proche d'un patient, ait produit des pièces de sa propre initiative, en méconnaissance du secret médical qui s'impose à lui, n'est pas par elle-même de nature à affecter la régularité ou le bien-fondé de la décision du juge.... ,,2) Le  juge, auquel il incombe, dans la mise en oeuvre de ses pouvoirs d'instruction, de veiller au respect des droits des parties, d'assurer l'égalité des armes entre elles et de garantir, selon les modalités propres à chacun d'entre eux, les secrets protégés par la loi, ne peut régulièrement se fonder sur de telles pièces qu'à la condition d'avoir pu préalablement les soumettre au débat contradictoire.</ANA>
<ANA ID="9B"> 49-05-01-01 Il résulte des dispositions du premier alinéa de l'article L. 3211-3 du code de la santé publique (CSP) et de l'article R. 1112-47 du même code, qu'il peut être interdit au proche d'un patient hospitalisé sans son consentement de rendre visite à celui-ci au motif, notamment, qu'une telle visite n'est pas compatible avec l'état de santé du patient ou la mise en oeuvre de son traitement.</ANA>
<ANA ID="9C"> 54-06-01 1) La circonstance qu'un établissement de santé, dans un contentieux l'opposant à un proche d'un patient, ait produit des pièces de sa propre initiative, en méconnaissance du secret médical qui s'impose à lui, n'est pas par elle-même de nature à affecter la régularité ou le bien-fondé de la décision du juge.... ,,2) Le  juge, auquel il incombe, dans la mise en oeuvre de ses pouvoirs d'instruction, de veiller au respect des droits des parties, d'assurer l'égalité des armes entre elles et de garantir, selon les modalités propres à chacun d'entre eux, les secrets protégés par la loi, ne peut régulièrement se fonder sur de telles pièces qu'à la condition d'avoir pu préalablement les soumettre au débat contradictoire.</ANA>
<ANA ID="9D"> 54-07-01-04-01 Cas d'un moyen tiré de l'absence de motivation de la décision de l'administration en litige, écarté au fond par les juges du fond alors que ce moyen, soulevé tardivement, était irrecevable. En substituant au motif retenu par la cour le motif tiré de l'irrecevabilité du moyen soulevé devant les juges du fond, dont l'examen n'implique l'appréciation d'aucune circonstance de fait et qui justifie sur ce point le dispositif de l'arrêt attaqué, le juge de cassation n'est pas tenu d'en informer préalablement les parties sur le fondement de l'article R. 611-7 du code de justice administrative (CJA) relatif à la communication des moyens d'ordre public (MOP).</ANA>
<ANA ID="9E"> 54-07-01-06 Cas d'un moyen tiré de l'absence de motivation de la décision de l'administration en litige, écarté au fond par les juges du fond alors que ce moyen, soulevé tardivement, était irrecevable. En substituant au motif retenu par la cour le motif tiré de l'irrecevabilité du moyen soulevé devant les juges du fond, dont l'examen n'implique l'appréciation d'aucune circonstance de fait et qui justifie sur ce point le dispositif de l'arrêt attaqué, le juge de cassation n'est pas tenu d'en informer préalablement les parties sur le fondement de l'article R. 611-7 du code de justice administrative (CJA) relatif à la communication des moyens d'ordre public (MOP).</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., CE, 8 novembre 1998, Election cantonale de Bruz, n° 201966, p. 345 ; CE, 18 janvier 2017, M.,, n° 394562, aux Tables sur un autre point.,,[RJ2] Rappr., CE, 24 juin 2015, Société AIG-FP Capital Preservation Corp., n° 368443, T. pp. 613-632-814-842.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
