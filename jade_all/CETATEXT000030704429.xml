<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030704429</ID>
<ANCIEN_ID>JG_L_2015_06_000000383729</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/70/44/CETATEXT000030704429.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème SSJS, 03/06/2015, 383729, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-06-03</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>383729</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème SSJS</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MASSE-DESSEN, THOUVENIN, COUDRAY</AVOCATS>
<RAPPORTEUR>M. Camille Pascal</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Xavier Domino</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2015:383729.20150603</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A...B...a demandé au tribunal administratif de Lyon d'annuler pour excès de pouvoir les décisions du préfet du Rhône du 15 octobre 2012 lui refusant la délivrance d'un titre de séjour, l'obligeant à quitter le territoire français et désignant le pays à destination duquel il sera reconduit. Par un jugement n° 1207281 du 6 février 2013, le tribunal administratif a rejeté sa demande. <br/>
<br/>
              Par un arrêt n° 13LY00725 du 20 février 2014, la cour administrative d'appel de Lyon a rejeté l'appel formé par M. B...contre ce jugement et ses conclusions tendant à ce qu'il soit enjoint au préfet du Rhône, à titre principal, de lui délivrer une carte de séjour portant la mention " vie privée et familiale " et, à titre subsidiaire, de réexaminer sa situation administrative dans le délai d'un mois et, dans l'attente de ce réexamen, de lui délivrer une autorisation provisoire de séjour.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 18 août et 17 novembre 2014 au secrétariat du contentieux du Conseil d'Etat, M. B...demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 500 euros à verser à la SCP Masse-Dessen-Touvenin-Coudray, son avocat, au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Camille Pascal, conseiller d'Etat, <br/>
<br/>
              - les conclusions de M. Xavier Domino, rapporteur public,<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Masse-Dessen, Thouvenin, Coudray, avocat de M. B...; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1.	Considérant qu'aux termes de l'article 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales : " 1. Toute personne a droit au respect de sa vie privée et familiale (...). / 2. Il ne peut y avoir ingérence d'une autorité publique dans l'exercice de ce droit que pour autant que cette ingérence est prévue par la loi et qu'elle constitue une mesure qui, dans une société démocratique, est nécessaire à la sécurité nationale, à la sûreté publique, au bien-être économique du pays, à la défense de l'ordre et à la prévention des infractions pénales, à la protection de la santé ou de la morale, ou à la protection des droits et libertés d'autrui. " ;<br/>
<br/>
              2.	Considérant qu'en application de ces stipulations, il appartient à l'autorité administrative qui envisage de procéder à l'éloignement d'un ressortissant étranger en situation irrégulière d'apprécier si, eu égard notamment à la durée et aux conditions de son séjour en France, ainsi qu'à la nature et à l'ancienneté de ses liens familiaux sur le territoire français, l'atteinte que cette mesure porterait à sa vie familiale serait disproportionnée au regard des buts en vue desquels cette décision serait prise ; que la circonstance que l'étranger relèverait, à la date de cet examen, des catégories ouvrant droit au regroupement familial ne saurait, par elle-même, intervenir dans l'appréciation portée par l'administration sur la gravité de l'atteinte à la situation de l'intéressé ; que cette dernière peut, en revanche, tenir compte le cas échéant, au titre des buts poursuivis par la mesure d'éloignement, de ce que le ressortissant étranger en cause ne pouvait légalement entrer en France pour y séjourner qu'au seul bénéfice du regroupement familial et qu'il n'a pas respecté cette procédure ;<br/>
<br/>
              3.	Considérant que, pour confirmer le jugement du 6 février 2013 du tribunal administratif de Lyon et rejeter la demande de M. B...tendant à l'annulation pour excès de pouvoir des décisions du 15 octobre 2012 par lesquelles le préfet du Rhône avait refusé la délivrance d'un titre de séjour, lui avait fait obligation de quitter le territoire français et désigné le pays à destination duquel il serait reconduit, la cour administrative d'appel de Lyon s'est fondée sur ce que cette décision ne méconnaissait pas l'article 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, dès lors que M.B..., dont une première demande de regroupement familial avait été rejetée, n'établissait pas l'impossibilité de formuler utilement une nouvelle demande à cette fin ; qu'il résulte de ce qui a été dit ci-dessus que la cour a, en statuant ainsi, commis une erreur de droit ; que son arrêt doit dès lors, sans qu'il soit besoin d'examiner l'autre moyen du pourvoi, être annulé ;<br/>
<br/>
              4.	Considérant que M. B...a obtenu le bénéfice de l'aide juridictionnelle ; que, par suite, son avocat peut se prévaloir des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 ; qu'il y a lieu, dans les circonstances de l'espèce, et sous réserve que la SCP Masse-Dessen-Touvenin-Coudray, avocat de M.B..., renonce à percevoir la somme correspondant à la part contributive de l'Etat, de mettre à la charge de l'Etat la somme de 3 000 euros à verser à cette société ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Lyon du 20 février 2014 est annulé.<br/>
<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Lyon.<br/>
<br/>
Article 3 : L'Etat versera à la SCP Masse-Dessen-Touvenin-Coudray, avocat de M. B..., une somme de 3 000 euros en application des dispositions du deuxième alinéa de l'article 37 de la loi du 10 juillet 1991, sous réserve que cette société renonce à percevoir la somme correspondant à la part contributive de l'Etat.<br/>
<br/>
Article 4 : La présente décision sera notifiée à M. A...B...et au ministre de l'intérieur.<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
