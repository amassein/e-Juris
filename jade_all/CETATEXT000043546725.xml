<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043546725</ID>
<ANCIEN_ID>JG_L_2021_05_000000436815</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/54/67/CETATEXT000043546725.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème - 6ème chambres réunies, 27/05/2021, 436815</TITRE>
<DATE_DEC>2021-05-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>436815</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème - 6ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Flavie Le Tallec</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:436815.20210527</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B... A... a demandé au tribunal administratif de Rouen, d'une part, d'annuler pour excès de pouvoir l'arrêté du 23 avril 2018 par lequel le préfet de l'Eure a prononcé la suspension de son permis de conduire pour une durée de trois mois et, d'autre part, de le décharger du paiement de l'amende mise à sa charge. Par un jugement n° 1801984 du 16 octobre 2019, le tribunal administratif a annulé cet arrêté et rejeté le surplus de ses conclusions.<br/>
<br/>
              Par un pourvoi, enregistré le 17 décembre 2019 au secrétariat du contentieux du Conseil d'Etat, le ministre de l'intérieur demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler ce jugement en tant qu'il annule l'arrêté du 23 avril 2018 ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter la demande de M. A....<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de procédure pénale ;<br/>
              - le code de la route ;<br/>
              - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Flavie Le Tallec, maître des requêtes en service extraordinaire ;<br/>
<br/>
              - les conclusions de M. B... Polge, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que M. A..., intercepté à Brionne (Eure), le 20 avril 2018, pour un excès de vitesse supérieur à 40 km/h, a fait l'objet d'un arrêté du 23 avril 2018 du préfet de l'Eure suspendant son permis de conduire pour une durée de trois mois. Le ministre de l'intérieur se pourvoit en cassation contre le jugement du 16 octobre 2019 du tribunal administratif de Rouen en tant que, par ce jugement, le tribunal a annulé pour excès de pouvoir cet arrêté.<br/>
<br/>
              2. L'autorité de la chose jugée appartenant aux décisions des juges répressifs devenues définitives qui s'impose aux juridictions administratives s'attache à la constatation matérielle des faits mentionnés dans le jugement et qui sont le support nécessaire du dispositif. Le moyen tiré de la méconnaissance de cette autorité, qui présente un caractère absolu, est d'ordre public et peut être invoqué pour la première fois devant le Conseil d'Etat, juge de cassation. <br/>
<br/>
              3. Il résulte des termes mêmes du jugement attaqué que, pour annuler l'arrêté du 23 avril 2018 du préfet de l'Eure suspendant le permis de conduire de M. A..., le tribunal administratif, devant lequel l'intéressé contestait la réalité de l'excès de vitesse qui lui était reproché, s'est, sans se méprendre sur son office contrairement à ce que soutenait le préfet de l'Eure devant lui, prononcé sur l'existence de cet excès de vitesse et a estimé que celle-ci n'était pas établie.<br/>
<br/>
              4. Toutefois, il ressort des pièces produites en cassation devant le Conseil d'Etat par le ministre de l'intérieur que M. A... a été condamné, par une ordonnance pénale du président du tribunal de police d'Evreux du 14 juin 2018, devenue définitive, pour ces mêmes faits d'excès de vitesse, au paiement d'une amende de 200 euros et à une peine complémentaire de suspension de son permis de conduire d'une durée d'un mois. <br/>
<br/>
              5. Par suite, l'autorité de la chose jugée qui s'attache aux constatations matérielles, retenues par le juge pénal, d'un excès de vitesse compris entre 40 et 50 km/h au-dessus de la vitesse autorisée fait obstacle à ce que puisse être maintenu le jugement attaqué qui écarte la réalité de l'excès de vitesse commis par l'intéressé. Il y a lieu, par suite, d'annuler ce jugement en tant qu'il annule l'arrêté du préfet de l'Eure du 23 avril 2018.<br/>
<br/>
              6. Il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative.<br/>
<br/>
              7. Ainsi qu'il a été dit au point 4, il résulte de l'ordonnance pénale du président du tribunal de police d'Evreux du 14 juin 2018 que l'unique moyen présenté par M. A... à l'appui de ses conclusions d'annulation de l'arrêté préfectoral attaqué, tiré de ce que les faits d'excès de vitesse qui lui sont reprochés ne seraient pas établis, ne peut qu'être écarté. M. A... n'est, par suite, pas fondé à demander l'annulation de l'arrêté qu'il attaque.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : L'article 2 du jugement du tribunal administratif de Rouen est annulé. <br/>
<br/>
Article 2 : Les conclusions de M. A... tendant à l'annulation de l'arrêté du 23 avril 2018 du préfet de l'Eure suspendant son permis de conduire pour une durée de trois mois, sont rejetées. <br/>
<br/>
Article 3 : La présente décision sera notifiée au ministre de l'intérieur et à M. B... A....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-06-06-02-02 PROCÉDURE. JUGEMENTS. CHOSE JUGÉE. CHOSE JUGÉE PAR LA JURIDICTION JUDICIAIRE. CHOSE JUGÉE PAR LE JUGE PÉNAL. - AUTORITÉ DE CHOSE JUGÉE S'ATTACHANT À UNE DÉCISION PÉNALE DEVENUE DÉFINITIVE - MOYEN D'ORDRE PUBLIC POUVANT ÊTRE INVOQUÉ POUR LA PREMIÈRE FOIS EN CASSATION - EXISTENCE, MÊME SI LE JUGEMENT PÉNAL EST PRODUIT POUR LA PREMIÈRE FOIS EN CASSATION [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-08-02-004-03-02 PROCÉDURE. VOIES DE RECOURS. CASSATION. RECEVABILITÉ. RECEVABILITÉ DES MOYENS. MOYEN SOULEVÉ POUR LA PREMIÈRE FOIS DEVANT LE JUGE DE CASSATION. - AUTORITÉ DE CHOSE JUGÉE S'ATTACHANT À UNE DÉCISION PÉNALE DEVENUE DÉFINITIVE - MOYEN D'ORDRE PUBLIC POUVANT ÊTRE INVOQUÉ POUR LA PREMIÈRE FOIS EN CASSATION - EXISTENCE, MÊME SI LE JUGEMENT PÉNAL EST PRODUIT POUR LA PREMIÈRE FOIS EN CASSATION [RJ1].
</SCT>
<ANA ID="9A"> 54-06-06-02-02 L'autorité de la chose jugée appartenant aux décisions des juges répressifs devenues définitives qui s'impose aux juridictions administratives s'attache à la constatation matérielle des faits mentionnés dans le jugement et qui sont le support nécessaire du dispositif.,,,Le moyen tiré de la méconnaissance de cette autorité, qui présente un caractère absolu, est d'ordre public et peut être invoqué pour la première fois devant le Conseil d'Etat, juge de cassation.,,,Il en va ainsi même si le jugement pénal est produit pour la première fois devant le Conseil d'Etat.</ANA>
<ANA ID="9B"> 54-08-02-004-03-02 L'autorité de la chose jugée appartenant aux décisions des juges répressifs devenues définitives qui s'impose aux juridictions administratives s'attache à la constatation matérielle des faits mentionnés dans le jugement et qui sont le support nécessaire du dispositif.,,,Le moyen tiré de la méconnaissance de cette autorité, qui présente un caractère absolu, est d'ordre public et peut être invoqué pour la première fois devant le Conseil d'Etat, juge de cassation.,,,Il en va ainsi même si le jugement pénal est produit pour la première fois devant le Conseil d'Etat.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, Section, 16 février 2018, Mme,, n° 395371, p. 41.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
