<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031631178</ID>
<ANCIEN_ID>JG_L_2015_12_000000380909</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/63/11/CETATEXT000031631178.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème SSJS, 14/12/2015, 380909, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-12-14</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>380909</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème SSJS</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BORE, SALVE DE BRUNETON</AVOCATS>
<RAPPORTEUR>M. Vincent Villette</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Edouard Crépey</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2015:380909.20151214</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme B...A...a demandé au tribunal administratif de Versailles de prononcer la décharge des cotisations supplémentaires d'impôt sur le revenu auxquelles elle a été assujettie au titre de l'année 2003 à raison des investissements réalisés par les sociétés en participation (SEP) Faucon 2, 3 et 5, au titre de l'année 2004 à raison des investissements réalisés par les SEP Erable 1, 2, 4 et 5, et au titre de l'année 2005 à raison des investissements des SEP Marguerite 2 et 4, ainsi que des pénalités correspondantes. Par un jugement n° 0810723-0901306-0901307-1004323-1004666-1004667-l004669 du 2 octobre 2012, le tribunal administratif de Versailles a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 12VE04223 du 3 avril 2014, la cour administrative d'appel de Versailles a, sur appel de MmeA..., déchargé celle-ci des cotisations supplémentaires à l'impôt sur le revenu auxquelles elle avait été assujettie au titre de l'année 2004 à raison des investissements effectués par la SEP Erable 1, ainsi que des pénalités correspondantes et a rejeté le surplus de ses conclusions.<br/>
<br/>
              1° Sous le n° 380909, par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 3 juin et 3 septembre 2014 au secrétariat du contentieux du Conseil d'Etat, Mme A...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt en tant qu'il rejette le surplus de ses conclusions ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat le versement d'une somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              2° Sous le n° 380910, par un pourvoi et un mémoire en réplique enregistrés les 3 juin 2014 et 16 mars 2015, le ministre des finances et des comptes publics demande au Conseil d'Etat d'annuler les articles 1er et 2 de ce même arrêt n° 12VE04223 de la cour administrative d'appel de Versailles  du 12 juin 2014.<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu :<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Vincent Villette, auditeur,<br/>
<br/>
              - les conclusions de M. Edouard Crépey, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Boré, Salve de Bruneton, avocat de Mme A...;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Les pourvois de Mme A...et du ministre des finances et des comptes publics sont dirigés contre la même décision. Il y a lieu de les joindre pour statuer par une seule décision.<br/>
<br/>
              2. Il ressort des pièces du dossier soumis aux juges du fond que Mme A...est devenue associée de plusieurs sociétés en participation (SEP), dont les SEP Faucon 2, 3 et 5 et Erable 1, 2, 4 et 5, en vue de réaliser dans le département de La Réunion des investissements productifs ouvrant droit à la réduction d'impôt prévue par les dispositions de l'article 199 undecies B du code général des impôts. Par un jugement du 2 octobre 2012, le tribunal administratif de Versailles a rejeté sa demande tendant à la décharge des cotisations supplémentaires d'impôt sur le revenu auxquelles elle a été assujettie au titre des années 2003, 2004 et 2005 à la suite de la remise en cause, par l'administration, de la réduction d'impôt dont elle entendait bénéficier en raison de ces investissements. Statuant sur son appel, la cour administrative d'appel de Versailles a confirmé le jugement, dans sa partie attaquée, par un arrêt du 3 avril 2014, sauf en ce qui concerne le bien-fondé du redressement relatif à l'investissement de la SEP Erable 1 au titre de l'année 2004. Se pourvoient contre cet arrêt, d'une part, Mme A... en tant qu'il a rejeté le surplus de ses conclusions, d'autre part, le ministre des finances et des comptes publics, en tant qu'il a prononcé la décharge des cotisations relatives aux investissements effectués par la SEP Erable 1.<br/>
<br/>
              Sur les conclusions du pourvoi n° 380909 :<br/>
<br/>
              3. Aux termes de l'article L. 57 du livre des procédures fiscales : " L'administration adresse au contribuable une notification de redressement qui doit être motivée de manière à lui permettre de formuler ses observations ou de faire connaître son acceptation. (...) Lorsque l'administration rejette les observations du contribuable sa réponse doit également être motivée "; Aux termes de l'article L. 53 du même livre : " En ce qui concerne les sociétés dont les associés sont personnellement soumis à l'impôt pour la part des bénéfices correspondant à leurs droits dans la société, la procédure de vérification des déclarations déposées par la société est suivie entre l'administration des impôts et la société elle-même ". Il résulte de la combinaison de ces dispositions qu'en ce qui concerne les sociétés régies par l'article 8 du code général des impôts, dont les bénéfices sont imposables entre les mains de leurs membres, la procédure contradictoire de redressement de l'imposition doit être suivie entre la société et l'administration fiscale et non entre cette dernière et chacun des membres de la société. En revanche, l'administration ne peut légalement mettre de suppléments d'imposition à la charge personnelle des associés sans leur avoir notifié, dans les conditions prévues à l'article L. 57, les corrections apportées aux déclarations qu'ils ont eux-mêmes souscrites, en motivant cette notification au moins par une référence aux rehaussements apportés aux bénéfices sociaux de la société et par l'indication de la quote-part de ces bénéfices à raison de laquelle les intéressés seront imposés.<br/>
<br/>
              4. Par suite, en estimant que l'administration n'était pas tenue de suivre une procédure contradictoire à l'égard de la requérante auquel elle notifiait des rehaussements à proportion de ses parts dans les sociétés considérées et en contrôlant l'effectivité et le contenu des motivations de ces notifications à l'intéressée, la cour n'a pas commis d'erreur de droit.<br/>
<br/>
              5. Il résulte des pièces du dossier soumis aux juges du fond que, dans chacune des propositions de rectifications qu'elle a adressées les 11 décembre 2006, 23 janvier 2007 et 19 décembre 2007 à la requérante, l'administration a non seulement précisé le motif, le montant et les modes de calcul des majorations d'impôt sur le revenu envisagées mais a encore joint en annexe de larges extraits de la proposition de rectification adressée aux SEP concernées dont la requérante est l'associée, ainsi que tous les textes qu'elle se proposait d'appliquer. En relevant que l'administration avait suffisamment motivé ses propositions de rectifications et que la contribuable avait été pleinement mise à même de faire connaître ses observations, comme elle l'a du reste fait, la cour n'a donc commis ni erreur de droit, ni dénaturation des pièces du dossier.<br/>
<br/>
              6. Il découle de ce qui précède que Mme A...n'est pas fondée à demander l'annulation de l'arrêt qu'elle attaque en tant qu'il a rejeté le surplus de ses conclusions. Son pourvoi doit dès lors être rejeté, y compris ses conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Sur les conclusions du pourvoi n° 383910 :<br/>
<br/>
              7. D'une part, le premier alinéa du I de l'article 199 undecies B du code général des impôts permet aux contribuables domiciliés en France de bénéficier d'une réduction d'impôt sur le revenu à raison des investissements productifs neufs qu'ils réalisent dans les départements d'outre-mer. Selon le 1 du II du même article, les investissements mentionnés au I et dont le montant total par programme et par exercice est supérieur à 300 000 euros, lorsque le contribuable ne participe pas à l'exploitation, ne peuvent ouvrir droit à réduction que s'ils ont reçu un agrément préalable du ministre chargé du budget dans les conditions prévues au III de l'article 217 undecies du même code. Si le vingt-sixième alinéa du I de l'article 199 undecies B du code général des impôts dispose que la réduction d'impôt s'applique aux investissements productifs mis à la disposition d'une entreprise dans le cadre d'un contrat de location à la condition, mentionnée au seizième alinéa du I de l'article 217 undecies, que " l'entreprise locataire aurait pu bénéficier de la déduction (...) si, imposable en France, elle avait acquis directement le bien ", cette condition n'est pas relative à l'agrément délivré dans les conditions prévues au III de l'article 217 undecies du même code. Il résulte des termes mêmes du second alinéa du 1 du II de l'article 199 undecies B que, lorsque le contribuable ne participe pas à l'exploitation du bien investi, c'est au niveau de l'entreprise qui a inscrit l'investissement à l'actif de son bilan que s'apprécie le seuil au-delà duquel un agrément est exigé.<br/>
<br/>
              8. D'autre part, le 2 du II du même article 199 undecies B prévoit que, pour ouvrir droit à réduction et par dérogation aux dispositions du 1, les investissements mentionnés au I doivent, lorsqu'ils sont réalisés dans le secteur des transports, avoir reçu l'agrément préalable du ministre chargé du budget dans les conditions prévues au III de l'article 217 undecies. Toutefois, le 3 du III de cet article 217 undecies prévoit que les investissements réalisés dans ce secteur et dont le montant total n'excède pas 300 000 euros par programme et par exercice sont dispensés de la procédure d'agrément préalable lorsqu'ils sont réalisés par une entreprise qui exerce son activité dans ces départements depuis au moins deux ans. Il précise qu'il en est de même lorsque ces investissements sont donnés en location à une telle entreprise.<br/>
<br/>
              9. La cour administrative d'appel a relevé que la SEP Érable 1 avait effectué et inscrit à l'actif de son bilan au titre de l'année 2004 un investissement d'une valeur n'excédant pas 300 000 euros correspondant à un matériel qu'elle avait donné en location simple à l'entreprise de transports Mounichy située à La Réunion. Elle a jugé que le critère du secteur mentionné par le III de l'article 217 undecies et dans lequel se réalise l'investissement devait s'apprécier non au regard de la situation de l'exploitant mais de celle de l'entreprise, société ou groupement qui inscrit l'investissement à l'actif de son bilan. Ce faisant, et alors que l'administration n'alléguait pas que les autres conditions fixées par les dispositions législatives applicables ne seraient pas satisfaites, la cour administrative n'a pas commis d'erreur de droit en déchargeant Mme A...des cotisations relatives aux investissements effectués par la SEP Erable 1. <br/>
<br/>
              10. Il résulte de ce qui précède que le ministre n'est pas fondé à demander l'annulation de l'arrêt qu'il attaque. Il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par Mme A...au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Les pourvois n° 380909 et n° 380910 sont rejetés.<br/>
Article 2 : Les conclusions présentées par MmeA..., sous le n° 380910, au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
Article 3 : La présente décision sera notifiée à Mme B...A...et au ministre des finances et des comptes publics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
