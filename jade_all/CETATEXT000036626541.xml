<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036626541</ID>
<ANCIEN_ID>JG_L_2018_02_000000413658</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/62/65/CETATEXT000036626541.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème - 8ème chambres réunies, 20/02/2018, 413658, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-02-20</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>413658</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème - 8ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP WAQUET, FARGE, HAZAN</AVOCATS>
<RAPPORTEUR>M. Sylvain Monteillet</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Vincent Daumas</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:413658.20180220</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              La commune de Villers-Saint-Paul (Somme) a demandé au tribunal administratif d'Amiens, d'une part, d'annuler pour excès de pouvoir la décision implicite née le 7 juin 2012 par laquelle le directeur départemental des finances publiques de l'Oise aurait rejeté sa demande tendant à la réévaluation du montant de la compensation relais du produit théorique de la taxe professionnelle 2010 et, d'autre part, de lui enjoindre, sous astreinte, d'intégrer les rôles supplémentaires au titre des années  2008 et 2009 de la taxe professionnelle dans le calcul de la compensation relais. <br/>
<br/>
              Par un jugement n° 1202286 du 11 décembre 2014, le tribunal administratif d'Amiens a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 15DA00256 du 21 mai 2015, la cour administrative d'appel de Douai a refusé de transmettre au Conseil d'Etat la question prioritaire de constitutionnalité, présentée par la commune de Villers-Saint-Paul à l'appui de son appel dirigé contre ce jugement, et relative à la conformité aux droits et libertés garantis par la Constitution des I et II du 1.4 de l'article 78 de la loi n° 2009-1673 du 30 décembre 2009 de finances pour l'année 2010, dans leur rédaction issue de la loi n° 2011-900 du 29 juillet 2011 de finances pour 2011.<br/>
<br/>
              Par un arrêt n° 15DA00255 du 15 juin 2017, la cour administrative d'appel de Douai a rejeté la requête d'appel de la commune de Villers-Saint-Paul.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 23 août et 23 novembre 2017 au secrétariat du contentieux du Conseil d'Etat, la commune de Villers-Saint-Paul demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 15DA00255 du 15 juin 2017 ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat une somme de 4 000 euros au titre de l'article L.761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la Constitution, notamment son Préambule et son article 61-1 ;<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - la loi n° 2009-1673 du 30 décembre 2009, notamment son article 78 ;<br/>
              - la loi n° 2011-900 du 29 juillet 2011, notamment son article 36 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Sylvain Monteillet, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Vincent Daumas, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Waquet, Farge, Hazan, avocat de la commune de Villers-Saint-Paul ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Dans le cadre de la réforme de la fiscalité locale, les collectivités territoriales et les établissements publics de coopération intercommunale dotés d'une fiscalité propre ont reçu au titre de l'année 2010, en lieu et place de la taxe professionnelle, une compensation relais prévue par le II de l'article 1640 B du code général des impôts. L'article 78 de la loi du 30 décembre 2009 de finances pour 2010 a institué, à compter de l'année 2011, une dotation de compensation de la réforme de la taxe professionnelle (DCRTP) et créé un fonds national de garantie individuelle des ressources (FNGIR). En vertu du 1 et du 2 de cet article, les montants de la DCRTP et des prélèvements et reversements au FNGIR sont déterminés en intégrant notamment le montant de la compensation du produit théorique de la taxe professionnelle 2010.<br/>
<br/>
              2. Il ressort des pièces du dossier soumis aux juges du fond que la commune de Villers-Saint-Paul (Somme) a perçu, en 2010, une compensation relais en remplacement de la taxe professionnelle et a reçu une notification, en 2011, des montants de la DCRTP et du FNGIR qui la concernaient. Par courrier en date du 6 avril 2012, elle a demandé au directeur départemental des finances publiques de l'Oise de prendre en compte, dans le calcul de ces montants, les rôles supplémentaires de taxe professionnelle qui ont été émis depuis cette notification. Par un jugement du 11 décembre 2014, le tribunal administratif d'Amiens a rejeté sa demande tendant, d'une part, à annuler la décision implicite née le 7 juin 2012 par laquelle le directeur départemental des finances publiques de l'Oise aurait rejeté sa demande et, d'autre part, à lui enjoindre, sous astreinte, d'intégrer les rôles supplémentaires au titre des années 2008 et 2009 de la taxe professionnelle dans le calcul de la compensation relais. La commune de Villers-Saint-Paul se pourvoit en cassation contre l'arrêt du 15 juin 2017 par lequel la cour administrative d'appel de Douai a rejeté l'appel qu'elle a formé contre ce jugement. Elle conteste, par un mémoire distinct, l'arrêt du 21 mai 2015 par lequel la même cour a refusé de transmettre au Conseil d'Etat la question prioritaire de constitutionnalité soulevée en appel et portant sur la conformité à la Constitution des paragraphes I et II du 1.4 de l'article 78 de la loi du 30 décembre 2009 de finances pour 2010, dans sa rédaction issue de la loi du 29 juillet 2011 de finances pour 2011. Elle soulève, enfin, à l'appui de son pourvoi en cassation contre l'arrêt du 15 juin 2017, une question prioritaire de constitutionnalité portant sur la conformité à la Constitution des deuxième et quatrième alinéas du I du 1.4 de ce même article 78.<br/>
<br/>
              Sur la question prioritaire de constitutionnalité soulevée devant le Conseil d'Etat :<br/>
<br/>
              3. Aux termes du premier alinéa de l'article 23-5 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel : " Le moyen tiré de ce qu'une disposition législative porte atteinte aux droits et libertés garantis par la Constitution peut être soulevé, y compris pour la première fois en cassation, à l'occasion d'une instance devant le Conseil d'Etat (...) ". Il résulte des dispositions de ce même article que le Conseil constitutionnel est saisi de la question prioritaire de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux.<br/>
<br/>
              4. Aux termes des deuxième et quatrième alinéas du I du 1.4 de l'article 78 de la loi de finances pour 2010, dans leur rédaction issue de la loi de finances pour 2011 applicable au litige : " En tant que de besoin, le montant de la compensation relais prévue au II de l'article 1640 B du code général des impôts est corrigé sur la base des impositions à la taxe professionnelle et à la cotisation foncière des entreprises émises jusqu'au 30 juin 2011 et des dégrèvements de taxe professionnelle et de cotisation foncière des entreprises ordonnancés jusqu'à la même date. (...)/ Le montant définitif des dotations, prélèvements et reversements mentionnés au premier alinéa du présent I est calculé à partir des impositions établies, des dégrèvements ordonnancés et des produits perçus jusqu'au 30 juin 2011 et actualisé en fonction des redressements opérés par les services fiscaux sur les bases de la taxe professionnelle de 2010, pendant le délai de reprise visé à l'article L. 174 du livre des procédures fiscales ". La commune de Villers-Saint-Paul soutient que ces dispositions, en tant qu'elles retiennent en particulier comme date limite le 30 juin 2011, méconnaissent le principe d'égalité devant la loi garanti par l'article 6 de la Déclaration des droits de l'homme et du citoyen de 1789. <br/>
<br/>
              5. Aux termes de l'article 6 de la Déclaration de 1789 : " La loi (...) doit être la même pour tous, soit qu'elle protège, soit qu'elle punisse (...) ". Le principe d'égalité ne s'oppose ni à ce que le législateur règle de façon différente des situations différentes, ni à ce qu'il déroge à l'égalité pour des raisons d'intérêt général, pourvu que, dans l'un et l'autre cas, la différence de traitement qui en résulte soit en rapport direct avec l'objet de la loi qui l'établit. Il n'en résulte pas pour autant que le principe d'égalité oblige à traiter différemment des personnes se trouvant dans des situations différentes.<br/>
<br/>
              6. En ne prévoyant qu'une seule date au-delà de laquelle ne peut plus être prise en compte, pour l'ensemble des collectivités territoriales concernées, l'émission de rôles supplémentaires de taxe professionnelle et de cotisation foncière des entreprises pour corriger le montant de la compensation relais, hors le cas des actualisations opérées par les services fiscaux sur le fondement du quatrième alinéa du I du 1.4 de l'article 78 de la loi de finances pour 2010, les dispositions contestées n'instituent aucune différence de traitement. Ces dispositions ne sont, ainsi, pas de nature à méconnaître le principe d'égalité devant la loi.<br/>
<br/>
              7. Il résulte de ce qui précède que la question soulevée, qui n'est pas nouvelle, ne présente pas un caractère sérieux. Par suite, il n'y a pas lieu de la renvoyer au Conseil constitutionnel. <br/>
<br/>
              Sur le refus de transmission de la question prioritaire de constitutionnalité soulevée devant la cour administrative d'appel :<br/>
<br/>
              8. Les dispositions de l'article 23-2 de l'ordonnance portant loi organique du 7 novembre 1958 prévoient que, lorsqu'une juridiction relevant du Conseil d'Etat est saisie de moyens contestant la conformité d'une disposition législative aux droits et libertés garantis par la Constitution, elle transmet au Conseil d'Etat la question de constitutionnalité ainsi posée à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et qu'elle ne soit pas dépourvue de caractère sérieux.<br/>
<br/>
              9. Devant la cour administrative d'appel de Douai, la commune de Villers-Saint-Paul soutenait que les dispositions des deuxième et quatrième alinéas du I du 1.4 de l'article 78 de la loi de finances pour 2010, dans leur rédaction issue de la loi de finances pour 2011 applicable au litige, citées au point 4, méconnaissaient le principe d'égalité devant les charges publiques, garanti par l'article 13 de la Déclaration des droits de l'homme et du citoyen, et le principe de libre administration des collectivités territoriales, à raison de la perte de ressources fiscales qu'entraînerait l'application de la date du 30 juin 2011 pour pouvoir corriger le montant de la compensation relais.<br/>
<br/>
              10. Il ressort des termes de l'arrêt du 21 mai 2015 que pour écarter le moyen tiré de la méconnaissance du principe d'égalité devant les charges publiques, la cour a relevé que les dispositions contestées plaçaient l'ensemble des collectivités territoriales et établissements publics dans la même situation au regard de critères objectifs en lien avec les buts poursuivis par le législateur, sans toutefois indiquer quels étaient ces buts. En statuant ainsi, la cour a insuffisamment motivé son arrêt. Il s'ensuit que la commune de Villers-Saint-Paul est fondée, sans qu'il soit besoin d'examiner les autres moyens dirigés contre cet arrêt, à demander l'annulation du refus de transmission de la question prioritaire de constitutionnalité opposé par la cour.<br/>
<br/>
              11. Il y a lieu, en application des dispositions de l'article L. 821-2 du code de justice administrative, d'examiner la question prioritaire de constitutionnalité soulevée devant la cour.<br/>
<br/>
              12. En premier lieu, selon l'article 13 de la Déclaration des droits de l'homme et du citoyen de 1789 : " Pour l'entretien de la force publique, et pour les dépenses d'administration, une contribution commune est indispensable : elle doit être également répartie entre tous les citoyens, en raison de leurs facultés ". En vertu de l'article 34 de la Constitution, il appartient au législateur de déterminer, dans le respect des principes constitutionnels et compte tenu des caractéristiques de chaque impôt, les règles selon lesquelles doivent être appréciées les facultés contributives. En particulier, pour assurer le respect du principe d'égalité, il doit fonder son appréciation sur des critères objectifs et rationnels en fonction des buts qu'il se propose. Cette appréciation ne doit cependant pas entraîner de rupture caractérisée de l'égalité devant les charges publiques.<br/>
<br/>
              13. D'une part, par les dispositions contestées, éclairées par les travaux préparatoires de la loi du 30 décembre 2009 de finances pour 2010, le législateur a entendu assurer aux collectivités territoriales et aux établissements publics de coopération intercommunale à fiscalité propre un niveau de ressources voisin de celui précédant la suppression de la taxe professionnelle, sans pour autant garantir une compensation intégrale, et tout en évitant un suivi complexe du dispositif pendant plusieurs années. D'autre part, aux termes du III de l'article 1640 B du code général des impôts, dans sa rédaction en vigueur à l'époque des dispositions contestées : " Les services fiscaux opèrent sur les bases de taxe professionnelle de 2010 les contrôles qu'ils auraient opérés si la taxe professionnelle avait été acquittée en 2010. La compensation relais versée en 2010 aux collectivités territoriales en application du II fait l'objet d'une actualisation correspondant à ces contrôles, pendant le délai de reprise mentionné à l'article L. 174 du livre des procédures fiscales ". Compte tenu de l'objet spécifique de ces contrôles et des délais dans lesquels ils interviennent, le législateur a entendu, dans ce cas particulier, tenir compte du délai de reprise de l'article L. 174 du livre des procédures fiscales pour actualiser, le cas échéant, le montant des dotations. Ainsi, en retenant, au deuxième alinéa du I du 1.4 de l'article 78 de la loi de finances pour 2010, la date du 30 juin 2011 au-delà de laquelle il n'est plus possible de corriger le montant de la compensation relais, hors les cas des actualisations opérées par les services fiscaux sur le fondement du quatrième alinéa du I du 1.4 de l'article 78 de la loi de finances pour 2010, le législateur a retenu des critères objectifs et rationnels en rapport avec l'objectif poursuivi. Il n'en résulte pas une rupture caractérisée de l'égalité devant les charges publiques.<br/>
<br/>
              14. En second lieu, il résulte du troisième alinéa de l'article 72 de la Constitution que si les collectivités territoriales " s'administrent librement par des conseils élus ", chacune d'elles le fait " dans les conditions prévues par la loi ". En outre, l'article 34 de la Constitution réserve au législateur la détermination des principes fondamentaux de la libre administration des collectivités territoriales, de leurs compétences et de leurs ressources. Les règles fixées par la loi sur le fondement de ces dispositions ne sauraient avoir pour effet de restreindre les ressources des collectivités territoriales ou d'accroître les obligations mises à leur charge au point d'entraver leur libre administration. <br/>
<br/>
              15. Les dispositions contestées n'ont pas pour effet d'empêcher toute correction du montant de la compensation relais et, par voie de conséquence, des dotations au titre de la DCRTP et du FNGIR, mais d'encadrer cette possibilité dans le temps. Eu égard à ce qui est dit au point 13 et au caractère circonscrit des effets induits par l'application de la date du 30 juin 2011, le dispositif prévu par le I du 1.4 de l'article 78 de la loi de finances pour 2010 n'est pas de nature à restreindre les ressources des communes ou des établissements publics de coopération intercommunale au point d'entraver leur libre administration. <br/>
<br/>
              16. Il résulte de tout ce qui précède que la question soulevée, qui n'est pas nouvelle, ne présente pas un caractère sérieux. Ainsi, il n'y a pas lieu de la renvoyer au Conseil constitutionnel.<br/>
<br/>
              Sur les autres moyens du pourvoi :<br/>
<br/>
              17. En premier lieu, il ressort des pièces du dossier soumis aux juges du fond que la commune de Villers-Saint-Paul avait demandé, à l'appui de sa requête de première instance, qu'il soit enjoint à l'administration d'intégrer les rôles supplémentaires 2008 et 2009 de la taxe professionnelle dans le calcul de la compensation relais. Dans sa requête d'appel, elle demandait également à la cour que le montant de la compensation relais soit actualisé à hauteur de 1 075 619 euros et que la DCRTP et les reversements au titre du FNGIR soient majorés du même montant. Eu égard aux termes utilisés dans la requête d'appel, la cour ne s'est pas méprise sur la portée des conclusions qui lui étaient soumises et n'a pas commis d'erreur de droit en regardant ces conclusions comme distinctes de la demande d'injonction sous astreinte dont étaient assorties les conclusions principales d'excès de pouvoir présentées devant les premiers juges et en jugeant qu'elles étaient nouvelles en appel.<br/>
<br/>
              18. En second lieu, il ressort des pièces du dossier soumis aux juges du fond que, par courrier du 6 avril 2012 adressé à la direction départementale des finances publiques de l'Oise et portant l'objet " correction de la garantie individuelle de la ressource ", la commune de Villers-Saint-Paul faisait état de " rôles supplémentaires (...) émis pour les années 2008 et 2009 ", concernant notamment la société Esiane. Elle estimait, en outre, qu'il était " acquis que la compensation relais (produit théorique de la taxe professionnelle 2010) doit être réévaluée du montant correspondant au rôle supplémentaire de 2009 " et que " ces rectifications impliquent de calculer à nouveau les garanties individuelles de ressources (DCRTP et FNGIR) " et elle demandait que lui soient indiquées " les modalités d'application de ces corrections à apporter aux versements attendus à compter de 2012 ". Ce courrier signé conjointement avec la communauté de l'agglomération creilloise, auquel était joint une réponse ministérielle à une question de la sénatrice Caroline Cayeux sur les modalités de prise en compte des rôles supplémentaires de taxe professionnelle dans le calcul de la compensation relais et du FNGIR, doit être regardé comme sollicitant la prise en compte, pour le calcul des montants de la DCRTP et du FNGIR qui la concernaient à compter de 2012, à raison des corrections que la commune de Villers-Saint-Paul estimait devoir être apportées au montant de la compensation relais qui lui avait été initialement notifié. En estimant que ce courrier présentait " le caractère d'une simple demande d'information ", eu égard à sa formulation et à la circonstance que, ultérieurement, l'administration avait invité son auteur à présenter ses observations sur les montants de la DCRTP et du FNGIR qui la concernaient, pour en déduire que la réponse de l'administration ne faisait pas grief, la cour a donné aux faits une qualification juridique erronée. Sans qu'il soit besoin d'examiner l'autre moyen dirigé contre la même partie de l'arrêt, celui-ci doit être annulé en tant qu'il a statué sur les conclusions d'excès de pouvoir présentées en première instance.<br/>
<br/>
              19. Il résulte de tout ce qui précède que la commune de Villers-Saint-Paul est seulement fondée à demander l'annulation de l'arrêt qu'elle attaque en tant que la cour s'est prononcée sur les conclusions d'excès de pouvoir présentées en première instance.<br/>
<br/>
              20. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat le versement à la commune de Villers-Saint-Paul d'une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er: Il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée devant le Conseil d'Etat par la commune de Villers-Saint-Paul.<br/>
Article 2 : L'arrêt n° 15DA00256 de la cour administrative de Douai du 21 mai 2015 est annulé.<br/>
Article 3 : Il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée devant la cour administrative d'appel de Douai par la commune de Villers-Saint-Paul.<br/>
Article 4 : L'arrêt n° 15DA00255 de la cour administrative de Douai du 15 juin 2017 est annulé en tant que la cour s'est prononcée sur les conclusions d'excès de pouvoir présentées en première instance.<br/>
Article 5 : L'affaire est renvoyée, dans cette mesure, à la cour administrative d'appel de Douai. <br/>
Article 6 : L'Etat versera à la commune de Villers-Saint-Paul une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 7 : Le surplus des conclusions de la commune de Villers-Saint-Paul est rejeté.<br/>
Article 8 : La présente décision sera notifiée à la commune de Villers-Saint-Paul, au Premier ministre, au ministre d'Etat, ministre de l'intérieur et au ministre de l'action et des comptes publics.<br/>
Copie en sera adressée au Conseil constitutionnel.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
