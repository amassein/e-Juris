<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032408989</ID>
<ANCIEN_ID>JG_L_2016_04_000000385152</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/40/89/CETATEXT000032408989.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème chambre, 15/04/2016, 385152, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-04-15</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>385152</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP CELICE, BLANCPAIN, SOLTNER, TEXIDOR ; SCP ROUSSEAU, TAPIE</AVOCATS>
<RAPPORTEUR>Mme Pauline Jolivet</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Aurélie Bretonneau</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2016:385152.20160415</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B...a demandé au tribunal administratif de Basse-Terre d'enjoindre à la société d'économie mixte à Saint-Martin (Semsamar), sous astreinte de 180 euros par jour de retard, de prendre les mesures nécessaires à l'exécution du jugement n° 0700070 du 15 décembre 2009 annulant la décision implicite du président de cette société lui refusant la communication de documents administratifs. Par un jugement n° 1100058 du 20 novembre 2013, le tribunal a rejeté cette demande.<br/>
<br/>
              Par un arrêt n° 14BX00212 du 15 juillet 2014, la cour administrative d'appel de Bordeaux a fait droit à l'appel formé par M. B...contre ce jugement, en l'annulant et en enjoignant à la Semsamar d'effectuer les diligences et les démarches nécessaires tant dans ses services qu'auprès de la commune de Gourbeyre et de la direction de l'environnement, de l'aménagement et du logement de la Guadeloupe, à l'effet de retrouver les documents sollicités dans un délai de trois mois à compter de la notification de l'arrêt, sous astreinte de 100 euros par jour de retard.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire enregistrés les 15 octobre 2014 et 15 janvier 2015 au secrétariat du contentieux du Conseil d'Etat, la société d'économie mixte à Saint-Martin (Semsamar) demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de M. B...la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la loi n° 78-753 du 17 juillet 1978 ;<br/>
- le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Pauline Jolivet, maître des requêtes en service extraordinaire,<br/>
<br/>
              - les conclusions de Mme Aurélie Bretonneau, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Célice, Blancpain, Soltner, Texidor, avocat de la société économie mixte à Saint Martin et à la SCP Rousseau, Tapie, avocat de M. B...;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis au juge du fond que M. B...a demandé au président de la société d'économie mixte à Saint-Martin (Semsamar), sur le fondement de la loi du 17 juillet 1978 portant diverses mesures d'amélioration des relations entre l'administration et le public, la communication de plusieurs documents élaborés dans le cadre de procédures de passation de marchés publics pour la réalisation de quatre programmes de constructions de logements sociaux réalisés, pour deux d'entre eux, en 1992, l'un d'entre eux en 1997 et le dernier en 2002 ; que M. B...a saisi le tribunal administratif de Basse-Terre d'une demande tendant à l'annulation de la décision implicite, née du silence gardé par le président de la Semsamar sur cette demande ; que, par un jugement du 15 décembre 2009, le tribunal, après avoir jugé qu'il n'y avait plus lieu de statuer sur les conclusions de la requête dont il était saisi, en tant que celle-ci portait sur les documents communiqués en cours d'instance, a annulé cette décision ; que M. B...a saisi le tribunal administratif de Basse-Terre d'une demande tendant à assurer l'exécution du jugement du 15 décembre 2009 ; que, par un jugement du 20 novembre 2013, le tribunal a rejeté cette demande d'exécution ; que par un arrêt du 15 juillet 2014, la cour administrative d'appel de Bordeaux a fait droit à l'appel formé par M. B...contre ce jugement et a enjoint à la Semsamar d'effectuer les diligences et les démarches nécessaires à l'effet de retrouver les documents sollicités dans un délai de trois mois à compter de la lecture de son arrêt, sous astreinte de 100 euros par jour de retard ; que la Semsamar se pourvoit en cassation contre cet arrêt ; <br/>
<br/>
              Sur l'arrêt attaqué : <br/>
<br/>
              2. Considérant qu'il résulte des dispositions combinées du deuxième alinéa de l'article R. 811-1 et du 3° de l'article R. 222-13 du code de justice administrative, dans leur rédaction applicable à la date à laquelle le tribunal administratif s'est prononcé, que celui-ci statue en premier et dernier ressort sur les litiges en matière de communication de documents administratifs ; que la procédure d'exécution prévue par l'article L. 911-4 du code de justice administrative se rattache à la même instance contentieuse que celle qui a donné lieu à la décision juridictionnelle dont il est demandé au juge d'assurer l'exécution ; qu'ainsi les voies de recours ouvertes contre la décision prise en application de cet article sont les mêmes que celles qui sont prévues contre la décision dont il est demandé au juge d'assurer l'exécution ;<br/>
<br/>
              3. Considérant qu'en application de ces dispositions, le tribunal administratif de Basse-Terre a statué en premier et dernier ressort sur la demande présentée par M. B...sur le fondement de l'article L. 911-4 du code de justice administrative, tendant à assurer l'exécution du jugement du 15 décembre 2009 du même tribunal statuant en matière de communication de documents administratifs ; qu'en exerçant un recours contre le jugement du 20 novembre 2013, qui n'était pas susceptible d'appel, M. B...devait être regardé comme formant un pourvoi en cassation qu'il appartenait à la cour de renvoyer au Conseil d'Etat ; qu'en statuant néanmoins sur les conclusions de l'intéressé, la cour a méconnu l'étendue de sa compétence ; que, par suite, sans qu'il soit besoin d'examiner les moyens de la Semsamar, l'arrêt du 15 juillet 2014 de la cour administrative d'appel de Bordeaux doit être annulé ; <br/>
<br/>
              4. Considérant qu'il appartient au Conseil d'Etat, par l'effet de cette annulation, de statuer, en tant que juge de cassation, sur les conclusions présentées par M. B... contre le jugement du 20 novembre 2013 du tribunal administratif de Basse-Terre ;<br/>
<br/>
              Sur le pourvoi en cassation de M. B...contre le jugement du 20 novembre 2013 :<br/>
<br/>
              5. Considérant que, contrairement à ce que soutient M.B..., le jugement attaqué mentionne les diligences accomplies par la Semsamar afin de retrouver les documents dont la communication avait été demandée, avant de relever que l'ensemble des diligences nécessaires à l'effet de retrouver ces documents ont été accomplies ; qu'il est ainsi suffisamment motivé ; <br/>
<br/>
              6.	Considérant, en deuxième lieu, que les autorités mentionnées à l'article 1er de la loi du 17 juillet 1978 portant diverses mesures d'amélioration des relations entre l'administration et le public ne peuvent s'exonérer de leur obligation d'assurer l'exécution d'une décision de justice annulant une décision de refus de communication de documents administratifs et de celle de communiquer les documents sollicités dans les conditions prévues par cette décision qu'à la condition d'établir, d'une part, que des faits postérieurs à celle-ci ou des faits dont elles ne pouvaient faire état avant son prononcé rendent impossible cette communication et, d'autre part, qu'elles ont accompli toutes les diligences nécessaires pour assurer l'exécution de cette décision compte-tenu de la date d'élaboration des documents demandés et de la précision de cette demande ; <br/>
<br/>
              7. Considérant qu'il résulte de ce qui vient d'être dit que le tribunal administratif de Basse-Terre, saisi sur le fondement des dispositions de l'article L. 911-4 du code de justice administrative d'une demande d'exécution du jugement qu'il avait rendu le 15 décembre 2009 annulant la décision du président de la Semsamar refusant de communiquer les documents administratifs sollicités par M.B..., pouvait apprécier si, au regard des diligences accomplies par la Semsamar depuis l'intervention de ce jugement, celle-ci était dans l'impossibilité matérielle de communiquer ces documents ; qu'il n'a ainsi pas méconnu l'autorité de chose jugée qui s'attachait au jugement du 15 décembre 2009 ni entaché son jugement d'erreur de droit ;<br/>
<br/>
              8. Considérant que le tribunal pouvait, sans erreur de droit et sans dénaturer les pièces du dossier, juger qu'il ressortait des pièces du dossier qui lui était soumis et, notamment, des documents élaborés par la Semsamar elle-même, que celle-ci avait accompli l'ensemble des diligences nécessaires pour assurer l'exécution du jugement du 15 décembre 2009 ; qu'il pouvait légalement en déduire que la Semsamar devait être regardée comme ayant entièrement exécuté ce jugement ; <br/>
<br/>
              9. Considérant qu'il résulte de ce qui précède que M. B...n'est pas fondé à demander l'annulation du jugement du tribunal administratif de Basse-Terre du 20 novembre 2013 ; que, dans les circonstances de l'espèce, il n'y a pas lieu de faire droit aux conclusions présentées par la Semsamar au titre de l'article L. 761-1 du code de justice administrative ; que ces mêmes dispositions font obstacle à ce qu'une somme soit mise à sa charge au titre des frais exposés par M. B...et non compris dans les dépens ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 15 juillet 2014 de la cour administrative d'appel de Bordeaux est annulé.<br/>
Article 2 : Le pourvoi de M. B...est rejeté.<br/>
Article 3 : Les conclusions présentées par la Semsamar au titre de l'article L. 761-1 du code de justice administrative sont rejetées.  <br/>
Article 4 : La présente décision sera notifiée à la société d'économie mixte à Saint-Martin et à M. A...B....<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
