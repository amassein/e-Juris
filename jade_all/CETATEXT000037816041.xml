<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037816041</ID>
<ANCIEN_ID>JG_L_2018_12_000000409521</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/81/60/CETATEXT000037816041.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème chambre, 14/12/2018, 409521, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-12-14</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>409521</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Cécile Viton</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emilie Bokdam-Tognetti</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:409521.20181214</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et neuf mémoires en réplique, enregistrés les 3 avril, 15 juin, 7 août, 14 et 27 septembre et 16 octobre 2017 ainsi que les 4, 5 et 19 janvier et 15 février 2018 au secrétariat du contentieux du Conseil d'Etat, la société MEI Partners demande au Conseil d'Etat :<br/>
<br/>
              1°) d'écarter comme irrecevable le mémoire en défense du Premier ministre du 9 juin 2017 ;<br/>
<br/>
              2°) d'annuler pour excès de pouvoir la convention du 9 mai 2012 conclue entre l'Etat, l'Agence de l'environnement et de la maîtrise de l'énergie (ADEME) et la Caisse des Dépôts et des Consignations (CDC) ainsi que les décisions implicites du Premier ministre :<br/>
              - refusant de notifier à la Commission européenne l'aide d'Etat octroyée à la CDC par cette convention, révélée par l'ordonnance du juge des référés du tribunal administratif de Paris du 10 février 2017, ou par le courrier de la Commission européenne du 21 février 2017, <br/>
              - refusant le paiement des astreintes du 27 juin 2017, <br/>
              - refusant la récupération de l'aide illégale objet de la convention litigieuse du 22 juillet 2017,<br/>
              - refusant de payer une avance sur la récupération de l'aide illégale et des dommages intérêts des 22 et 26 juillet 2017,<br/>
              - refusant de procéder à des saisie-attributions sur soi-même du 4 septembre 2017 ;<br/>
<br/>
              3°) d'enjoindre à l'Etat, sous astreinte, d'émettre des titres de perception à l'encontre de la CDC et de son ancienne filiale CDC Entreprises devenue BPI France, pour un montant de 1 065 390 975 euros, et de verser les produits recouvrés sur un compte dédié ouvert à la Banque de France ;<br/>
<br/>
              4°) d'ordonner que les recours contre ces titres de perception ne produiraient aucun effet suspensif ;<br/>
<br/>
              5°) d'autoriser la Banque de France à garantir l'imputation de la créance qu'elle détient sur l'État sur les produits recouvrés et déposés dans ses livres,  pour un montant au plus égal à 1 052 400 000 euros ;<br/>
<br/>
              6°) de mettre à la charge de l'Etat la somme de 3 000 euros en application de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le règlement (UE) 2015/1589 du Conseil du 13 juillet 2015 ;<br/>
              - le décret n°2005-850 du 27 juillet 2005 ;<br/>
              - le décret n° 2010-80 du 22 janvier 2010 ;<br/>
              - les décrets du 13 septembre 2014 et du 24 mai 2017 portant délégation de signature (Commissariat général à l'investissement) ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Cécile Viton, maître des requêtes en service extraordinaire,<br/>
<br/>
              - les conclusions de Mme Emilie Bokdam-Tognetti, rapporteur public.<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Eu égard aux termes de sa requête et dans le dernier état de ses écritures, la société MEI Partners doit être regardée comme demandant l'annulation pour excès de pouvoir de la convention du 9 mai 2012 entre l'Etat, l'Agence de l'environnement et de la maîtrise de l'énergie (ADEME) et la Caisse des dépôts et des consignations (CDC), relative au programme d'investissements d'avenir, de la décision implicite du Premier ministre de refuser de notifier l'aide d'Etat consentie par cette convention à la CDC, ainsi que des décisions implicites du Premier ministre des 27 juin, 22 juillet, 26 juillet et 4 septembre 2017 refusant de payer des astreintes, de récupérer l'aide en litige auprès de la Caisse des dépôts et des consignations, de lui verser des dommages et intérêts et de procéder à des saisies-attributions sur l'Etat. <br/>
<br/>
              Sur le moyen tiré de l'irrecevabilité des mémoires en défense présentés par le Premier ministre :<br/>
<br/>
              2. Aux termes de l'article 1er du décret du 27 juillet 2005 relatif aux délégations de signature des membres du Gouvernement : " A compter du jour suivant la publication au Journal officiel de la République française de l'acte les nommant dans leurs fonctions ou à compter du jour où cet acte prend effet, si ce jour est postérieur, peuvent signer, au nom du ministre ou du secrétaire d'Etat et par délégation, l'ensemble des actes, à l'exception des décrets, relatifs aux affaires des services placés sous leur autorité : / 1° Les secrétaires généraux des ministères, les directeurs d'administration centrale, les chefs des services à compétence nationale mentionnés au premier alinéa de l'article 2 du décret du 9 mai 1997 susvisé et les chefs des services que le décret d'organisation du ministère rattache directement au ministre ou au secrétaire d'Etat ; / (...) ".<br/>
<br/>
              3. Aux termes de l'article 2 du décret du 22 janvier 2010 relatif au commissaire général à l'investissement dans sa rédaction antérieure à sa modification par le décret du 18 décembre 2017 relatif au secrétaire général pour l'investissement, le commissaire général est assisté d'un adjoint, qui le supplée en tant que de besoin. Aux termes de l'article 2 du décret du 13 septembre 2014 et de l'article 2 du décret du 24 mai 2017 portant délégation de signature (Commissariat général à l'investissement), M. Thierry Francq, commissaire général adjoint à l'investissement, reçoit délégation à l'effet de signer, au nom du Premier ministre, les décisions et avenants mentionnés à l'article 1er de chacun de ces deux décrets, lesquels ne comprennent pas les actes relatifs aux actions en justice engagées au titre des compétences du commissariat général.<br/>
<br/>
              4. Les mémoires en défense présenté au nom du Premier ministre sont signés par M. Thierry Francq, commissaire général adjoint au commissariat général à l'investissement (CGI). S'il n'est pas contesté que le CGI est un service directement rattaché au Premier ministre dont le commissaire général est au nombre des chefs de services qui peuvent signer les actes relatif aux affaires des services placés sous leur autorité en application du 1° de l'article 1er du décret du 27 juillet 2005 précitées, il ne résulte ni des dispositions du décret du 22 janvier 2010 relatif au commissaire général à l'investissement ni des décrets du 13 septembre 2014 et du 24 mai 2017 portant délégation de signature que le commissaire général adjoint serait délégataire de la signature du commissaire général pour les actes relatifs aux actions en justice engagée au titre des compétences du CGI. Dès lors, les mémoires en défense du Premier ministre doivent être écartés des débats comme irrecevables.<br/>
<br/>
              Sur les conclusions dirigées contre la convention du 9 mai 2012 entre l'Etat, l'ADEME et la CDC relative au programme d'investissements d'avenir :<br/>
<br/>
              5. Aux termes de l'article R. 421-1 du code de justice administrative : " Sauf en matière de travaux publics, la juridiction ne peut être saisie que par voie de recours formé contre une décision, et ce, dans les deux mois à partir de la notification ou de la publication de la décision attaquée ".<br/>
<br/>
              6. La convention attaquée a été publiée au Journal officiel de la République française le 10 mai 2012. Dès lors, les conclusions dirigées contre cette convention, contenues dans la requête enregistrée le 3 avril 2017, ont été présentées après l'expiration du délai de recours fixé à l'article R. 421-1 du code de justice administrative. Le délai de prescription de dix ans de l'action en récupération d'une aide d'Etat par la Commission européenne, prévu à l'article 17 du règlement du 13 juillet 2015 portant modalités d'application de l'article 108 du traité sur le fonctionnement de l'Union européenne, invoqué par la société requérante, qui ne concerne que les créances de la Commission européenne, n'a ni pour objet, ni pour effet, de proroger ce délai de recours.<br/>
<br/>
              7. Il résulte de ce qui précède que les conclusions tendant à l'annulation de la convention du 9 mai 2012 sont irrecevables et doivent dès lors être rejetées.<br/>
<br/>
              Sur les conclusions dirigées contre la décision de refus de notifier une aide d'Etat à la Commission européenne et les conclusions accessoires : <br/>
<br/>
              8. Aux termes de l'article R. 311-1 du code de justice administrative : " Le Conseil d'Etat est compétent pour connaître en premier et dernier ressort : (...) 2° Des recours dirigés contre les actes réglementaires des ministres et des autres autorités à compétence nationale et contre leurs circulaires et instructions de portée générale (...) ". La société demande l'annulation de la décision du Gouvernement de ne pas notifier à la Commission européenne l'aide individuelle qu'elle considère avoir été octroyée à la Caisse des dépôts et des consignations par les stipulations de la convention du 9 mai 2012. Cette décision ne présente pas le caractère d'un acte réglementaire et le recours dirigé contre cet acte ne relève donc pas de la compétence du Conseil d'Etat en application de l'article R. 311-1 du code de justice administrative. Par suite, il y  lieu, en application de l'article R. 351-1 du code de justice administrative, d'en attribuer le jugement au tribunal administratif de Paris.<br/>
<br/>
              9. Les conclusions dirigées contre les décisions implicites du Premier ministre de refuser la récupération de l'aide en litige, le paiement de dommages et intérêts et l'exécution de saisies-attributions sur soi-même, ainsi que les conclusions dirigées contre le refus de payer une astreinte de 1 000 000 d'euros par jour de manquement à l'obligation alléguée de l'Etat de procéder à la récupération d'une aide d'Etat illégale, à raison de l'illégalité alléguée de la décision de refus de notifier l'aide, qui sont accessoires des conclusions mentionnées au point 8, sont pour les mêmes motifs renvoyés à ce tribunal.<br/>
<br/>
              10. Dans les circonstances de l'espèce, il n'y a pas lieu de mettre à la charge de la société MEI Partners les sommes demandées par la Caisse des dépôts et des consignations au titre de l'article L. 761-1 du code de justice administrative. Les dispositions de cet article font obstacle à ce qu'une somme soit mise, à ce titre, à la charge de l'Etat, qui n'est pas la partie perdante dans la présente instance.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Les mémoires du Premier ministre enregistrés les 9 mai et 15 septembre 2017 et le 27 février 2018 sont écartés des débats. <br/>
Article 2 : Les conclusions de la requête de la société MEI Partners qui tendent à l'annulation de la convention du 9 mai 2012 sont rejetées. <br/>
Article 3 : Le jugement du surplus des conclusions de la requête de la société MEI Partners est attribué au tribunal administratif de Paris.<br/>
Article 4 : La présente décision sera notifiée à la SAS MEI Partners, au Premier ministre et à la Caisse des dépôts et consignations.<br/>
Copie en sera adressée au ministre de l'économie et des finances et à l'Agence de l'environnement et de la maîtrise de l'énergie (ADEME).<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
