<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044293899</ID>
<ANCIEN_ID>JG_L_2021_11_000000449941</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/29/38/CETATEXT000044293899.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème - 2ème chambres réunies, 05/11/2021, 449941, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-11-05</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>449941</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème - 2ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Mélanie Villiers</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Marc Pichon de Vendeuil</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:449941.20211105</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés les 19 février et 2 juillet 2021 au secrétariat du contentieux du Conseil d'Etat, le syndicat national des enseignements de second degré (SNES) demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir, à titre principal, le a) du 5° de l'article 1er du décret n° 2020-1632 du 21 décembre 2020 portant diverses mesures de simplification dans le domaine de l'éducation, à titre subsidiaire, l'ensemble de l'article 1er du même décret ou l'ensemble du décret ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 2 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'éducation ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Mélanie Villiers, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Marc Pichon de Vendeuil, rapporteur public ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Le syndicat requérant doit être regardé comme demandant l'annulation pour excès de pouvoir des dispositions du 5° de l'article 1er du décret contesté du 21 décembre 2020 portant diverses mesures de simplification dans le domaine de l'éducation, en tant qu'il modifie les articles R. 421-25 et R. 421-96 du code de l'éducation pour prévoir que " Le chef d'établissement fixe l'ordre du jour, les dates et heures des séances du conseil d'administration en tenant compte, au titre des questions diverses, des demandes d'inscription que lui ont adressées les membres du conseil (...) " et supprime le dernier alinéa de ces articles qui prévoyait que l'ordre du jour est adopté en début de séance du conseil d'administration. <br/>
<br/>
              Sur la légalité externe :<br/>
<br/>
              2. Aucune disposition, non plus qu'aucun principe, n'impose que le décret attaqué porte mention de la date à laquelle il a été examiné par la section de l'administration du Conseil d'Etat. Par suite, le moyen tiré de ce que le décret serait entaché d'irrégularité pour cette raison ne peut qu'être écarté. <br/>
<br/>
              Sur la légalité interne :<br/>
<br/>
              3. En premier lieu, aux termes de l'article L. 421-1 du code l'éducation : " Les collèges, les lycées et les établissements d'éducation spéciale sont des établissements publics locaux d'enseignement. (...) ". Aux termes de l'article L. 421-2 du même code : " Les établissements publics locaux mentionnés à l'article L. 421-1 sont administrés par un conseil d'administration (...) ". L'article L. 421-3 de ce code dispose que : " Les établissements publics locaux d'enseignement sont dirigés par un chef d'établissement. / Le chef d'établissement est désigné par l'autorité de l'Etat. / Il représente l'Etat au sein de l'établissement. / Il préside le conseil d'administration et exécute ses délibérations. (...) / En cas de difficultés graves dans le fonctionnement d'un établissement, le chef d'établissement peut prendre toutes dispositions nécessaires pour assurer le bon fonctionnement du service public. / Le chef d'établissement expose, dans les meilleurs délais, au conseil d'administration les décisions prises et en rend compte à l'autorité académique, au maire, au président du conseil départemental ou du conseil régional ". Aux termes de l'article L. 421-4 du même code : " Le conseil d'administration règle par ses délibérations les affaires de l'établissement. / A ce titre, il exerce notamment les attributions suivantes : / 1° Il fixe, dans le respect des dispositions législatives et réglementaires en vigueur et des objectifs définis par les autorités compétentes de l'Etat, les principes de mise en œuvre de l'autonomie pédagogique et éducative dont disposent les établissements et, en particulier, les règles d'organisation de l'établissement ; / 2° Il établit chaque année un rapport sur le fonctionnement pédagogique de l'établissement, les résultats obtenus et les objectifs à atteindre ; / 3° Il adopte le budget dans les conditions fixées par le présent chapitre ; / 4° Il se prononce sur le contrat d'objectifs conclu entre l'établissement, l'autorité académique et, lorsqu'elle souhaite y être partie, la collectivité territoriale de rattachement ; / 5° Il établit chaque année un bilan des actions menées à destination des parents des élèves de l'établissement. / Le conseil d'administration peut déléguer certaines de ses attributions à une commission permanente ".<br/>
<br/>
              4. Les dispositions contestées du décret attaqué ne peuvent être regardées comme portant, par elles-mêmes, atteinte aux attributions du conseil d'administration résultant des articles L. 421-2 et L. 421-4 du code de l'éducation. Au demeurant, elles prévoient qu'il appartient au chef d'établissement, lorsqu'il fixe l'ordre du jour du conseil d'administration, de tenir compte, au titre des questions diverses, des demandes qui lui sont adressées par les membres du conseil. Il résulte, en outre, de l'article R. 421-25 du code de l'éducation que le conseil d'administration est réuni en séance extraordinaire à la demande de la moitié au moins de ses membres sur un ordre du jour déterminé. <br/>
<br/>
              5. Par ailleurs, les dispositions contestées ne sauraient être regardées comme méconnaissant les prérogatives dévolues au chef d'établissement.<br/>
<br/>
              6. En deuxième lieu, le simple fait que le chef d'établissement fixe l'ordre du jour du conseil d'administration ne saurait conduire, par lui-même, à une méconnaissance des dispositions des articles L. 421-6, L. 421-7, L. 421-8 et L. 421-9 qui permettent aux établissements publics locaux d'enseignement de dispenser des actions de formation par apprentissage, d'organiser des contacts et des échanges avec leur environnement économique, culturel et social, de s'associer au sein de réseaux d'établissements, de créer des liens avec des partenaires extérieurs impliqués dans la lutte contre l'exclusion ou de conclure des accords de coopération avec des établissements universitaires.<br/>
<br/>
              7. En troisième lieu, les dispositions contestées ne sont pas contradictoires avec les dispositions de l'article R. 421-9 du code de l'éducation qui fixent les compétences du chef d'établissement en qualité d'organe exécutif de l'établissement.<br/>
<br/>
              8. Il résulte de ce qui précède que le syndicat national des enseignements de second degré n'est pas fondé à demander l'annulation pour excès de pouvoir des dispositions du décret du 21 décembre 2020 qu'il attaque.<br/>
<br/>
              9. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête du syndicat national des enseignements de second degré est rejetée.<br/>
Article 2 : La présente décision sera notifiée au syndicat national des enseignements de second degré, au ministre de l'éducation nationale, de la jeunesse et des sports et au Premier ministre.<br/>
              Délibéré à l'issue de la séance du 13 octobre 2021 où siégeaient : M. Jacques-Henri Stahl, président adjoint de la section du contentieux, présidant ; M. H... J..., M. Olivier Japiot, présidents de chambre ; M. I... L..., Mme A... K..., M. D... G..., M. E... M..., M. Jean-Yves Ollier, conseillers d'Etat et Mme Mélanie Villiers, maître des requêtes-rapporteure. <br/>
<br/>
              Rendu le 5 novembre 2021. <br/>
<br/>
                                   Le président : <br/>
                                   Signé : M. N... C...<br/>
<br/>
La rapporteure : <br/>
Signé : Mme Mélanie Villiers<br/>
<br/>
                                   La secrétaire :<br/>
                                   Signé : Mme F... B... <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
