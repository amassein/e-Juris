<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033314153</ID>
<ANCIEN_ID>JG_L_2016_10_000000387834</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/31/41/CETATEXT000033314153.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère chambre jugeant seule, 27/10/2016, 387834, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-10-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>387834</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère chambre jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Florence Marguerite</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Jean Lessi</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2016:387834.20161027</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              1° Sous le n° 387834, par une requête et deux mémoires en réplique, enregistrés les 6 février 2015, 15 février 2016 et 20 septembre 2016 au secrétariat du contentieux du Conseil d'Etat, M. B...A...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret n° 2014-1446 du 3 décembre 2014 portant modification du décret n° 49-580 du 22 avril 1949 relatif au régime d'assurance vieillesse complémentaire des pharmaciens ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 14 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              2° Sous le n° 391709, par une requête et deux mémoires en réplique, enregistrés les 13 juillet 2015, 15 février 2016 et 20 septembre 2016 au secrétariat du contentieux du Conseil d'Etat, M. A...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir l'arrêté du ministre des affaires sociales, de la santé et des droits des femmes du 4 juin 2015 portant approbation des modifications apportées aux statuts du régime d'assurance vieillesse complémentaire des pharmaciens (CAVP) ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 14 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu :<br/>
              - la Constitution ;<br/>
              - le code de la sécurité sociale ;<br/>
              - la loi n° 94-637 du 25 juillet 1994 ;<br/>
              - la loi n° 2003-775 du 21 août 2003 et la décision du Conseil constitutionnel n° 2003-483 DC du 14 août 2003 ;<br/>
              - le décret n° 49-580 du 22 avril 1949 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Florence Marguerite, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Jean Lessi, rapporteur public.<br/>
<br/>
              Vu la note en délibéré, enregistrée le 3 octobre 2016, présentée par M.A... ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 644-1 du code de la sécurité sociale : " A la demande du conseil d'administration de la caisse nationale d'assurance vieillesse des professions libérales et après consultation par référendum des assujettis au régime de base, des décrets peuvent instituer un régime d'assurance vieillesse complémentaire fonctionnant à titre obligatoire dans le cadre soit de l'ensemble du groupe professionnel, soit d'une activité professionnelle particulière. (...) / Le mode de calcul des cotisations complémentaires destinées à financer les régimes institués en application du premier alinéa et, le cas échéant, leurs montants annuels sont déterminés par décret après avis de la Caisse nationale d'assurance vieillesse des professions libérales (...) ". En application de ces dispositions, le décret du 22 avril 1949 relatif au régime d'assurance vieillesse complémentaire des pharmaciens, validé dans sa rédaction alors applicable par l'article 41 de la loi du 25 juillet 1994 relative à la sécurité sociale, a institué pour cette profession un régime d'assurance vieillesse complémentaire obligatoire. L'article 4 de ce décret renvoie aux statuts de la section professionnelle des pharmaciens de la caisse nationale d'assurance vieillesse des professions libérales le soin de préciser les conditions du régime qu'il institue. Enfin, aux termes du dernier alinéa de l'article L. 641-5 du code de la sécurité sociale, dans sa rédaction demeurée applicable à la date de l'arrêté attaqué : " Les statuts des sections professionnelles sont approuvés par arrêté ministériel ".<br/>
<br/>
              2. Le décret du 3 décembre 2014 modifie l'article 2 du décret du 22 avril 1949 relatif au régime d'assurance vieillesse complémentaire des pharmaciens pour prévoir que, à compter du 1er juillet 2015 et sous réserve d'une période transitoire précisée par les statuts du régime, chaque assujetti est inscrit dans une classe de cotisation non plus choisie par lui mais déterminée en fonction de la moyenne annuelle de ses revenus non salariés de la quatrième à la deuxième année précédentes et qu'à chacune des classes de cotisation, au nombre de six, correspond un taux de cotisation allant de sept à dix-sept fois la cotisation de référence. L'arrêté du 4 juin 2015 approuve les modifications apportées aux statuts du régime d'assurance vieillesse complémentaire des pharmaciens pour, notamment, tirer les conséquences du décret du 3 décembre 2014 sur les stipulations des statuts relatives à la détermination de la cotisation à ce régime. En particulier, l'article 4 de ces statuts, dans leur rédaction ainsi approuvée, prévoit, ainsi qu'il le faisait déjà, que chaque classe comprend une fraction de cotisation gérée par répartition, à hauteur de cinq fois la cotisation de référence, le solde de la cotisation étant géré par capitalisation. <br/>
<br/>
              3. Par deux requêtes qu'il y a lieu de joindre, M. A...demande l'annulation du décret du 3 décembre 2014 et de l'arrêté du 4 juin 2015.<br/>
<br/>
              Sur l'intervention de la Caisse d'assurance vieillesse des pharmaciens : <br/>
<br/>
              4. La Caisse d'assurance vieillesse des pharmaciens justifie d'un intérêt suffisant au maintien du décret attaqué. Ainsi, son intervention dans l'affaire n° 387834 est recevable.<br/>
<br/>
              Sur la légalité du décret du 3 décembre 2014 :<br/>
<br/>
              5. En premier lieu, les dispositions citées ci-dessus de l'article L. 644-1 du code de la sécurité sociale habilitent le pouvoir réglementaire, agissant par voie de décret, à déterminer le mode de calcul des cotisations complémentaires destinées à financer un régime d'assurance vieillesse complémentaire fonctionnant à titre obligatoire pour l'une des activités professionnelles entrant dans le champ du régime d'assurance vieillesse de base des professionnels libéraux. Or le décret attaqué se borne à modifier le mode de calcul des cotisations destinées à financer le régime complémentaire d'assurance vieillesse des pharmaciens. Par suite, le requérant ne peut utilement soutenir qu'il aurait méconnu la compétence réservée au législateur par l'article 34 de la Constitution. <br/>
<br/>
              6. En second lieu, M. A...critique la légalité du décret du 3 décembre 2014 en tant qu'il accroîtrait la part des cotisations obligatoires au régime d'assurance vieillesse complémentaire des pharmaciens qui sont gérées par capitalisation. Toutefois, le décret attaqué est, par lui-même, sans incidence sur les modalités de gestion, par répartition ou par capitalisation, des cotisations à ce régime, lesquelles sont déterminées par les statuts du régime. Par suite, les moyens tirés de ce que le décret attaqué méconnaîtrait, pour ce motif, les articles L. 111-1 et L. 112-2-1 du code de la sécurité sociale, l'article 13 de la loi du 17 janvier 1948 instituant une allocation de vieillesse pour les personnes non salariées, les articles 1er, 3 et 107 de la loi du 21 août 2003 portant réforme des retraites, le droit de la concurrence et la liberté du commerce et de l'industrie, ainsi que les principes du traité sur l'Union européenne, ne peuvent qu'être écartés.<br/>
<br/>
              Sur la légalité de l'arrêté du 4 juin 2015 :<br/>
<br/>
              7. En premier lieu, il résulte de ce qui a été dit au point 5 que le pouvoir réglementaire avait compétence, sur le fondement de l'article L. 644-1 du code de la sécurité sociale, pour prendre les dispositions du décret du 3 décembre 2014. Par suite, M. A...n'est pas fondé à soutenir que l'arrêté du 4 juin 2015 approuverait des statuts pris pour l'application d'un décret qui méconnaîtrait la compétence du législateur.<br/>
<br/>
              8. En deuxième lieu, en vertu des dispositions du onzième alinéa du Préambule de la Constitution de 1946, la Nation " garantit à tous, notamment à l'enfant, à la mère et aux vieux travailleurs, la protection de la santé, la sécurité matérielle, le repos et les loisirs. Tout être humain qui, en raison de son âge, (...) se trouve dans l'incapacité de travailler a le droit d'obtenir de la collectivité des moyens convenables d'existence ". Ces dispositions ne font pas obstacle à ce qu'un régime de retraite complémentaire obligatoire soit financé par des cotisations dont une fraction est gérée par capitalisation. Par suite, le requérant n'est pas fondé à soutenir que les dispositions critiquées méconnaîtraient les exigences découlant du onzième alinéa du Préambule de la Constitution de 1946. <br/>
<br/>
              9. En troisième lieu, ni le principe de solidarité nationale rappelé par l'article  L. 111-1 du code de la sécurité sociale ni, à supposer qu'elles soient applicables aux régimes de retraite complémentaire et qu'elles aient une portée normative, les dispositions de l'article 1er de la loi du 21 août 2003 portant réforme des retraites et du II de l'article L. 111-2-1 du code de la sécurité sociale ne font obstacle à ce qu'une fraction des cotisations versées à titre obligatoire à un régime de retraite complémentaire soit gérée par capitalisation.<br/>
<br/>
              10. En quatrième lieu, selon l'article 3 de la loi du 21 août 2003 portant réforme des retraites : " Les assurés doivent pouvoir bénéficier d'un traitement équitable au regard de la retraite, quels que soient leurs activités professionnelles passées et le ou les régimes dont ils relèvent ". Appelé à se prononcer sur la conformité à la Constitution de cet article, le Conseil constitutionnel a jugé, par sa décision du 14 août 2003, que ces dispositions étaient dépourvues de valeur normative. Le requérant ne peut donc s'en prévaloir pour soutenir que l'arrêté attaqué serait illégal.<br/>
<br/>
              11. En cinquième lieu, l'article 107 de la même loi dispose que : " En complément des régimes de retraite obligatoires par répartition, toute personne a accès, à titre privé ou dans le cadre de son activité professionnelle, à un ou plusieurs produits d'épargne réservés à la retraite, dans des conditions de sécurité financière et d'égalité devant l'impôt ". D'une part, ces dispositions ne font pas obstacle à ce qu'un régime de retraite complémentaire obligatoire soit en partie fondé sur la capitalisation. D'autre part, l'arrêté attaqué ne porte pas atteinte au droit des assujettis au régime d'assurance vieillesse complémentaire des pharmaciens d'accéder à un produit d'épargne réservé à la retraite. Par suite, M. A...n'est pas fondé à soutenir que l'arrêté attaqué méconnaîtrait l'article 107 de la loi du 21 août 2003.<br/>
<br/>
              12. En sixième lieu, M. A...ne peut utilement se prévaloir à l'encontre de l'arrêté attaqué des dispositions de l'article 13 de la loi du 17 janvier 1948 instituant une allocation de vieillesse pour les personnes non salariées, ultérieurement codifiées à l'article L. 642-2 du code de la sécurité sociale et, au demeurant, plusieurs fois modifiées, qui s'appliquent uniquement aux régimes de base.<br/>
<br/>
              13. En dernier lieu, d'une part, la circonstance qu'un régime de retraite complémentaire obligatoire gère par capitalisation une fraction des cotisations versées par les affiliés ne méconnaît pas, par elle-même, le principe de la liberté du commerce et de l'industrie. D'autre part, les moyens tirés d'une atteinte au droit de la concurrence et de la violation des " principes du traité sur l'Union européenne " ne sont pas assortis des précisions suffisantes pour en apprécier le bien-fondé. <br/>
<br/>
              14. Il résulte de tout ce qui précède que M. A...n'est pas fondé à demander l'annulation du décret du 3 décembre 2014 et de l'arrêté du 4 juin 2015 qu'il attaque.<br/>
<br/>
              Sur les frais exposés par les parties à l'occasion du litige :<br/>
<br/>
              15. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat, qui n'est pas, dans la présente instance, la partie perdante. Elles font de même obstacle à ce que soit accueillie la demande présentée à ce titre par la Caisse d'assurance vieillesse des pharmaciens, qui n'a pas la qualité de partie au présent litige. <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'intervention de la Caisse d'assurance vieillesse des pharmaciens dans l'affaire n° 387834 est admise.<br/>
Article 2 : Les requêtes de M. A...sont rejetées. <br/>
Article 3 : Les conclusions de la Caisse d'assurance vieillesse des pharmaciens présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée à M. B...A..., à la Caisse d'assurance vieillesse des pharmaciens, au Premier ministre et à la ministre des affaires sociales et de la santé.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
