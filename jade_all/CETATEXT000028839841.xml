<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028839841</ID>
<ANCIEN_ID>JG_L_2014_04_000000364326</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/83/98/CETATEXT000028839841.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère SSJS, 09/04/2014, 364326, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-04-09</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>364326</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère SSJS</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GARREAU, BAUER-VIOLAS, FESCHOTTE-DESBOIS ; SCP CELICE, BLANCPAIN, SOLTNER</AVOCATS>
<RAPPORTEUR>M. Philippe Combettes</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Alexandre Lallet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2014:364326.20140409</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>VU LA PROCEDURE SUIVANTE :<br/>
<br/>
 Procédure contentieuse antérieure<br/>
<br/>
              M. B...A...a demandé au tribunal administratif de Dijon d'annuler l'arrêté du 12 septembre 2011 par lequel le maire d'Auxonne l'a placé en surnombre dans la collectivité pendant un an à compter du 1er juin 2011. Par un jugement n° 1200108 du 2 octobre 2012, le tribunal administratif de Dijon a rejeté sa demande.<br/>
<br/>
 Procédure devant le Conseil d'Etat<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 6 décembre 2012, 1er mars 2013 et 3 janvier 2014 au secrétariat du contentieux du Conseil d'Etat, M. A...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement n° 1200108 du tribunal administratif de Dijon du 2 octobre 2012 ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ; <br/>
<br/>
              3°) de mettre à la charge de la commune d'Auxonne la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Il soutient que :<br/>
              - la procédure a été irrégulière, le sens des conclusions du rapporteur public ayant été mis à disposition des parties tardivement ;<br/>
              - le jugement est insuffisamment motivé ;<br/>
              - le tribunal, en inversant la charge de la preuve de l'absence de poste vacant dans un autre cadre d'emploi, a commis une erreur de droit ; <br/>
              - en estimant qu'il avait refusé un poste à mi-temps, il a dénaturé les pièces du dossier. <br/>
<br/>
              Par deux mémoires en défense, enregistrés les 31 octobre 2013 et 24 janvier 2014, la commune d'Auxonne conclut au rejet du pourvoi et à ce que la somme de 3 000 euros soit mise à la charge de M. A...au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Elle soutient que les moyens du pourvoi ne sont pas fondés et, en outre, que le dernier moyen est inopérant.<br/>
<br/>
              Vu : <br/>
              - les autres pièces du dossier ;<br/>
              - le code de justice administrative.<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Philippe Combettes, Maître des Requêtes en service extraordinaire,<br/>
<br/>
              - les conclusions de M. Alexandre Lallet, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Garreau, Bauer-Violas, Feschotte-Desbois, avocat de M.A..., et à la SCP Célice, Blancpain, Soltner, avocat de la commune d'Auxonne.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>CONSIDERANT CE QUI SUIT :<br/>
<br/>
              1. Aux termes du premier alinéa de l'article R. 711-3 du code de justice administrative : " Si le jugement de l'affaire doit intervenir après le prononcé de conclusions du rapporteur public, les parties ou leurs mandataires sont mis en mesure de connaître, avant la tenue de l'audience, le sens de ces conclusions sur l'affaire qui les concerne ". La communication aux parties du sens des conclusions, prévue par ces dispositions, a pour objet de mettre les parties en mesure d'apprécier l'opportunité d'assister à l'audience publique, de préparer, le cas échéant, les observations orales qu'elles peuvent y présenter, après les conclusions du rapporteur public, à l'appui de leur argumentation écrite et d'envisager, si elles l'estiment utile, la production, après la séance publique, d'une note en délibéré. En conséquence, les parties ou leurs mandataires doivent être mis en mesure de connaître, dans un délai raisonnable avant l'audience, l'ensemble des éléments du dispositif de la décision que le rapporteur public compte proposer à la formation de jugement d'adopter, à l'exception de la réponse aux conclusions qui revêtent un caractère accessoire, notamment celles qui sont relatives à l'application de l'article L. 761-1 du code de justice administrative. Cette exigence s'impose à peine d'irrégularité de la décision rendue sur les conclusions du rapporteur public.<br/>
<br/>
              2. Il ressort du relevé de l'application " Sagace " que le sens des conclusions du rapporteur public sur l'affaire litigieuse a été porté à la connaissance des parties le 17 septembre 2012 à 19 h 15 alors que l'audience du tribunal administratif se tenait le lendemain à 9 h 30. M. A...ne peut, dans les circonstances de l'espèce, être regardé comme ayant été mis en mesure de connaître, dans un délai raisonnable avant l'audience, le sens des conclusions du rapporteur public.<br/>
<br/>
              3. Il s'ensuit que le jugement attaqué a été rendu au terme d'une procédure irrégulière, alors même que l'avocat de M. A...a été présent à l'audience et a produit une note en délibéré après l'audience. Il doit, dès lors, être annulé, sans qu'il soit besoin d'examiner les autres moyens du pourvoi.<br/>
<br/>
              4. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la commune d'Auxonne une somme de 1 500 euros à verser à MA..., au titre des dispositions de l'article L. 761-1 du code de justice administrative. Ces dispositions font en revanche obstacle à ce qu'une somme soit mise à ce titre à la charge de M.A..., qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du tribunal administratif de Dijon du 2 octobre 2012 est annulé.<br/>
Article 2 : L'affaire est renvoyée au tribunal administratif de Dijon.<br/>
Article 3 : La commune d'Auxonne versera une somme de 1 500 euros à M. A...au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Les conclusions de la commune d'Auxonne présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée à M. B...A...et à la commune d'Auxonne.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
