<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030618760</ID>
<ANCIEN_ID>JG_L_2015_05_000000375208</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/61/87/CETATEXT000030618760.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  8ème sous-section jugeant seule, 20/05/2015, 375208, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-05-20</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>375208</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 8ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP NICOLAY, DE LANOUVELLE, HANNOTIN</AVOCATS>
<RAPPORTEUR>Mme Emmanuelle Petitdemange</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Nathalie Escaut</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2015:375208.20150520</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société Golf Landes Compagnie a demandé au tribunal administratif de Pau de réduire la cotisation de taxe foncière sur les propriétés bâties à laquelle elle a été assujettie au titre de l'année 2010 dans les rôles de la commune de Biscarosse, à raison du club house du golf dont elle est propriétaire.  <br/>
<br/>
              Par un jugement n° 1201605 du 3 décembre 2013, ce tribunal a rejeté cette demande.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 5 février et 6 mai 2014 au secrétariat du contentieux du Conseil d'Etat, la société Golf Landes Compagnie demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat le versement d'une somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu  les autres pièces du dossier ;<br/>
<br/>
<br/>
              Vu :<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Emmanuelle Petitdemange, auditeur,  <br/>
<br/>
              - les conclusions de Mme Nathalie Escaut, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Nicolaÿ, de Lanouvelle, Hannotin, avocat de la SNC Golf Landes Compagnie ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que la société Golf Landes Compagnie a été assujettie à une cotisation de taxe foncière sur les propriétés bâties au titre de l'année 2010 à raison du club house du golf de Biscarosse dont elle est propriétaire et dont la valeur locative a été déterminée par voie d'appréciation directe en application du 3° de l'article 1498 du code général des impôts ; qu'elle se pourvoit en cassation contre le jugement du 3 décembre 2013 par lequel le tribunal administratif de Pau n'a pas fait droit à sa demande tendant à la réduction de cette imposition ;  <br/>
<br/>
              2. Considérant qu'aux termes de l'article 1498 du code général des impôts : " La valeur locative de tous les biens autres que les locaux visés au I de l'article 1496 et que les établissements industriels visés à l'article 1499 est déterminée au moyen de l'une des méthodes indiquées ci-après : 1° Pour les biens donnés en location à des conditions de prix normales, la valeur locative est celle qui ressort de cette location ; 2° a. Pour les biens loués à des conditions de prix anormales ou occupés par leur propriétaire, occupés par un tiers à un autre titre que la location, vacants ou concédés à titre gratuit, la valeur locative est déterminée par comparaison. Les termes de comparaison sont choisis dans la commune. Ils peuvent être choisis hors de la commune pour procéder à l'évaluation des immeubles d'un caractère particulier ou exceptionnel ; b. La valeur locative des termes de comparaison est arrêtée : Soit en partant du bail en cours à la date de référence de la révision lorsque l'immeuble type était loué normalement à cette date, Soit, dans le cas contraire, par comparaison avec des immeubles similaires situés dans la commune ou dans une localité présentant, du point de vue économique, une situation analogue à celle de la commune en cause et qui faisaient l'objet à cette date de locations consenties à des conditions de prix normales ; 3° A défaut de ces bases, la valeur locative est déterminée par voie d'appréciation directe " ; que peuvent être retenus, comme termes de comparaison, des locaux commerciaux construits après le 1er janvier 1970, date de la révision générale, dès lors qu'il n'est pas contesté que la valeur locative de ces locaux-types a été arrêtée selon la méthode prévue au b du 2° de l'article 1498 précité ;<br/>
<br/>
              3. Considérant que le tribunal administratif de Pau, qui a écarté six des termes de comparaison que proposait la société requérante au seul motif que ces six golfs n'existaient pas en 1970, sans vérifier si ces locaux n'avaient pas été évalués par rapport à des termes de comparaison loués au 1er janvier 1970, conformément au b du 2° de l'article 1498 du code général des impôts, a commis une erreur de droit ; que dès lors et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, la société Golf Landes Compagnie est fondée à demander pour ce motif l'annulation du jugement qu'elle attaque ;<br/>
<br/>
              4. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat le versement à la société Golf Landes Compagnie de la somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : Le jugement du 3 décembre 2013 du tribunal administratif de Pau est annulé.<br/>
<br/>
Article 2 : L'affaire est renvoyée au tribunal administratif de Pau.<br/>
<br/>
Article 3 : L'Etat versera à la société Golf Landes Compagnie la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à la société Golf Landes Compagnie et au ministre des finances et des comptes publics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
