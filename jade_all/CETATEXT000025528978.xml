<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000025528978</ID>
<ANCIEN_ID>JG_L_2012_03_000000354165</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/25/52/89/CETATEXT000025528978.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème et 2ème sous-sections réunies, 12/03/2012, 354165, Publié au recueil Lebon</TITRE>
<DATE_DEC>2012-03-12</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>354165</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème et 2ème sous-sections réunies</FORMATION>
<TYPE_REC>Autres</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Nicolas Polge</RAPPORTEUR>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CESSR:2012:354165.20120312</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le jugement n° 1104420 du 10 novembre 2011, enregistré le 21 novembre 2011 au secrétariat du contentieux du Conseil d'Etat, par lequel le tribunal administratif de Versailles, avant de statuer sur la demande de M. A... B...tendant à l'annulation pour excès de pouvoir de la décision du 2 août 2011 par laquelle le préfet des Yvelines l'a obligé à quitter le territoire français sans délai, a fixé le pays de destination et a prononcé à son encontre une interdiction du territoire français pendant une durée d'un an à compter de la notification de la décision, a décidé, par application des dispositions de l'article L. 113-1 du code de justice administrative, de transmettre le dossier de cette demande au Conseil d'Etat, en soumettant à son examen les questions suivantes :<br/>
<br/>
              1°) Pour assortir l'obligation de quitter le territoire français d'une interdiction de retour, l'autorité administrative doit-elle successivement et distinctement motiver le principe d'une telle décision puis la détermination de sa durée ' <br/>
<br/>
              2°) L'autorité administrative doit-elle se fonder sur l'ensemble des critères dont la loi a prévu qu'elle tienne compte et doit-elle expliciter formellement dans la motivation de la décision la pondération qu'elle a retenue pour chaque critère '<br/>
<br/>
              3°) Quelle est l'intensité du contrôle qu'exerce le juge administratif tant sur le principe même de cette décision que sur la fixation de la durée de l'interdiction de retour '<br/>
<br/>
              4°) Comment définir l'articulation de ce contrôle avec l'exercice du contrôle de proportionnalité sur les conséquences de la mesure sur le droit à la vie privée et familiale du requérant garanti par l'article 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales '<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
<br/>
              Vu la directive 2008/115/CE du Parlement européen et du Conseil du 16 décembre 2008 ;<br/>
<br/>
              Vu le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
- le rapport de M. Nicolas Polge, Maître des Requêtes, <br/>
- les conclusions de M. Nicolas Boulouis, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>REND L'AVIS SUIVANT :<br/>
<br/>
              Aux termes du III de l'article L. 511-1 du code de l'entrée et du séjour des étrangers et du droit d'asile : " L'autorité administrative peut, par une décision motivée, assortir l'obligation de quitter le territoire français d'une interdiction de retour sur le territoire français ( ...) / Lorsque l'étranger ne faisant pas l'objet d'une interdiction de retour s'est maintenu sur le territoire au-delà du délai de départ volontaire, l'autorité administrative peut prononcer une interdiction de retour pour une durée maximale de deux ans à compter de sa notification. / Lorsqu'aucun délai de départ volontaire n'a été accordé à l'étranger obligé de quitter le territoire français, l'autorité administrative peut prononcer l'interdiction de retour pour une durée maximale de trois ans à compter de sa notification. / Lorsqu'un délai de départ volontaire a été accordé à l'étranger obligé de quitter le territoire français, l'autorité administrative peut prononcer l'interdiction de retour, prenant effet à l'expiration du délai, pour une durée maximale de deux ans à compter de sa notification. / Lorsque l'étranger faisant l'objet d'une interdiction de retour s'est maintenu sur le territoire au-delà du délai de départ volontaire ou alors qu'il était obligé de quitter sans délai le territoire français ou, ayant déféré à l'obligation de quitter le territoire français, y est revenu alors que l'interdiction de retour poursuit ses effets, l'autorité administrative peut prolonger cette mesure pour une durée maximale de deux ans. / L'interdiction de retour et sa durée sont décidées par l'autorité administrative en tenant compte de la durée de présence de l'étranger sur le territoire français, de la nature et de l'ancienneté de ses liens avec la France, de la circonstance qu'il a déjà fait l'objet ou non d'une mesure d'éloignement et de la menace pour l'ordre public que représente sa présence sur le territoire français (...) ".<br/>
<br/>
              1. Il ressort des termes mêmes de ces dispositions que l'autorité compétente doit, pour décider de prononcer à l'encontre de l'étranger soumis à l'obligation de quitter le territoire français une interdiction de retour et en fixer la durée, tenir compte, dans le respect des principes constitutionnels, des principes généraux du droit et des règles résultant des engagements internationaux de la France, des quatre critères qu'elles énumèrent, sans pouvoir se limiter à ne prendre en compte que l'un ou plusieurs d'entre eux.<br/>
<br/>
              2. La décision d'interdiction de retour doit comporter l'énoncé des considérations de droit et de fait qui en constituent le fondement, de sorte que son destinataire puisse à sa seule lecture en connaître les motifs. Si cette motivation doit attester de la prise en compte par l'autorité compétente, au vu de la situation de l'intéressé, de l'ensemble des critères prévus par la loi, aucune règle n'impose que le principe et la durée de l'interdiction de retour fassent l'objet de motivations distinctes, ni que soit indiquée l'importance accordée à chaque critère.<br/>
<br/>
              3. Il appartient au juge de l'excès de pouvoir, saisi d'un moyen en ce sens, de rechercher si les motifs qu'invoque l'autorité compétente sont de nature à justifier légalement dans son principe et sa durée la décision d'interdiction de retour et si la décision ne porte pas au droit de l'étranger au respect de sa vie privée et familiale garanti par l'article 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales une atteinte disproportionnée aux buts en vue desquels elle a été prise.<br/>
<br/>
              Le présent avis sera notifié au tribunal administratif de Versailles, à M. A... B... et au ministre de l'intérieur, de l'outre-mer, des collectivités territoriales et de l'immigration.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-03-01-02-02-02 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - FORME ET PROCÉDURE. QUESTIONS GÉNÉRALES. MOTIVATION. MOTIVATION SUFFISANTE. EXISTENCE. - INTERDICTION DE RETOUR EN FRANCE (ART. L. 511-1 DU CESEDA) - MOTIVATION FAISANT APPARAÎTRE LA PRISE EN COMPTE DES QUATRE CRITÈRES ÉNUMÉRÉS PAR LA LOI SANS DISTINGUER LES MOTIFS JUSTIFIANT LE PRINCIPE ET LA DURÉE DE LA DÉCISION NI INDIQUER L'IMPORTANCE ACCORDÉE À CHAQUE CRITÈRE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">335-03 ÉTRANGERS. OBLIGATION DE QUITTER LE TERRITOIRE FRANÇAIS (OQTF) ET RECONDUITE À LA FRONTIÈRE. - OQTF ASSORTIE D'UNE INTERDICTION DE RETOUR - MOTIFS DE L'INTERDICTION DE RETOUR - 1) CRITÈRES ÉNUMÉRÉS AU 7E ALINÉA DU III DE L'ARTICLE L. 511-1 DU CESEDA - OBLIGATION POUR L'AUTORITÉ ADMINISTRATIVE DE TENIR COMPTE DE CHACUN DE CES QUATRE CRITÈRES - A) EXISTENCE - B) CONSÉQUENCES SUR LA MOTIVATION DE LA DÉCISION - 2) CONTRÔLE DU JUGE DE L'EXCÈS DE POUVOIR - CONTRÔLE NORMAL.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">54-07-02-03 PROCÉDURE. POUVOIRS ET DEVOIRS DU JUGE. CONTRÔLE DU JUGE DE L'EXCÈS DE POUVOIR. APPRÉCIATIONS SOUMISES À UN CONTRÔLE NORMAL. - PRONONCÉ D'UNE MESURE D'INTERDICTION DE RETOUR EN FRANCE (ART. L. 511-1 DU CESEDA).
</SCT>
<ANA ID="9A"> 01-03-01-02-02-02 La motivation d'une décision d'interdiction de retour en France prise sur le fondement de l'article L. 511-1 du code de l'entrée et du séjour des étrangers en France et du droit d'asile (CESEDA), si elle doit attester de la prise en compte par l'autorité compétence de l'ensemble des quatre critères énumérés au septième alinéa de cet article (durée de présence de l'étranger en France, nature et ancienneté de ses liens avec la France, circonstance qu'il a déjà fait l'objet ou non d'une mesure d'éloignement et menace à l'ordre public que représente sa présence en France), n'a pas à distinguer les motifs justifiant le principe de l'interdiction prononcée de ceux justifiant sa durée ni à indiquer l'importance accordée à chacun des quatre critères.</ANA>
<ANA ID="9B"> 335-03 1) a) Il ressort des termes mêmes de l'article L. 511-1 du code de l'entrée et du séjour des étrangers en France et du droit d'asile (CESEDA) que l'autorité compétente pour prononcer à l'encontre d'un étranger soumis à l'obligation de quitter le territoire français (OQTF) une interdiction de retour et en fixer la durée a l'obligation de tenir compte (dans le respect des principes constitutionnels et conventionnels et des principes généraux du droit) des quatre critères énumérés par son septième alinéa (durée de présence de l'étranger en France, nature et ancienneté de ses liens avec la France, circonstance qu'il a déjà fait l'objet ou non d'une mesure d'éloignement et menace à l'ordre public que représente sa présence en France). b) Pour autant, la motivation de la décision d'interdiction de retour, si elle doit attester de la prise en compte de l'ensemble de ces critères, n'a pas à distinguer les motifs justifiant le principe de l'interdiction prononcée de ceux justifiant sa durée ni à indiquer l'importance accordée à chacun des quatre critères. 2) Le juge de l'excès de pouvoir exerce un contrôle normal sur les motifs de nature à justifier l'interdiction de retour, tant dans son principe que dans sa durée.</ANA>
<ANA ID="9C"> 54-07-02-03 Le juge de l'excès de pouvoir exerce un contrôle normal sur les motifs de nature à justifier le prononcé à l'encontre d'un étranger soumis à l'obligation de quitter le territoire français (OQTF) d'une interdiction de retour sur le fondement de l'article L. 511-1 du code de l'entrée et du séjour des étrangers en France et du droit d'asile (CESEDA), tant dans son principe que dans sa durée.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
