<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026664248</ID>
<ANCIEN_ID>JG_L_2012_11_000000352368</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/66/42/CETATEXT000026664248.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  4ème sous-section jugeant seule, 21/11/2012, 352368, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2012-11-21</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>352368</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 4ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Marc Dandelot</PRESIDENT>
<AVOCATS>FOUSSARD ; SCP THOUIN-PALAT, BOUCARD</AVOCATS>
<RAPPORTEUR>M. Jérôme Marchand-Arvier</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Rémi Keller</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2012:352368.20121121</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi et le mémoire complémentaire enregistrés les 2 septembre 2011 et 29 février 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour la société Star Players, dont le siège est 75, avenue des Ternes à Paris (75017), représentée par son gérant en exercice ; la société Star Players demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 09PA07103 du 30 juin 2011 par lequel la cour administrative d'appel de Paris a rejeté sa requête tendant à l'annulation du jugement n° 0705131 du 8 octobre 2009 par lequel le tribunal administratif de Paris a annulé la décision de la commission départementale d'équipement commercial du 1er février 2007 lui accordant l'autorisation d'étendre un magasin spécialisé en équipement de tapis sis 17, rue de Sèvres à Paris pour une surface de vente de 985 m² ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ; <br/>
<br/>
              3°) de mettre à la charge de l'Institut national du tapis distribution une somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative <br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de commerce ;<br/>
<br/>
              Vu la loi n° 73-1193 du 27 décembre 1973 modifiée ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jérôme Marchand-Arvier, Maître des Requêtes,  <br/>
<br/>
              - les observations de Me Foussard, avocat de la Société Star Players et de la SCP Thouin-Palat, Boucard, avocat de l'Institut national du tapis distribution,<br/>
<br/>
              - les conclusions de M. Rémi Keller, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à Me Foussard, avocat de la société Star Players et à la SCP Thouin-Palat, Boucard, avocat de l'Institut national du tapis distribution ;<br/>
<br/>
<br/>
<br/>Sans qu'il soit besoin d'examiner les autres moyens du pourvoi ;<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges de fond que, par une décision du 1er février 2007 la commission départementale d'équipement commercial de Paris a délivré à la société Star Players l'autorisation préalable requise en vue de l'extension d'un magasin spécialisé en équipement de tapis à Paris, afin de porter sa surface de vente de 414 m² à 1399 m² ; que, par l'arrêt attaqué du 30 juin 2011, la cour administrative d'appel de Paris a rejeté la requête de la société Star Players tendant à l'annulation du jugement du 8 octobre 2009 par lequel le tribunal administratif de Paris a, à la demande de l'Institut national du tapis distribution, annulé cette autorisation ; que, pour admettre l'intérêt pour agir de l'Institut national du tapis distribution contre la décision de la commission départementale, la cour a estimé qu'ayant la qualité de syndicat au sens des dispositions de l'article L. 411-1 du code du travail, cet institut avait un objet social suffisamment précis, qu'il n'était pas établi que les intérêts des professionnels de la vente de tapis fussent représentés par un autre organisme doté de la personnalité morale et que les intérêts en cause n'étaient pas différents de ceux définis par son objet social ;<br/>
<br/>
              2. Considérant que l'intérêt pour agir des groupements et associations s'apprécie au regard de leur objet statutaire et de l'étendue géographique de leur action ;<br/>
<br/>
              3. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que l'objet social de l'Institut national du tapis distribution tel qu'il ressort de ses statuts est " l'étude et la défense des intérêts économiques, matériels et moraux des professionnels de la vente des tapis, moquettes et revêtements de sol, à partir d'un magasin ou d'une surface spécialisée " ; qu'eu égard à la généralité de son objet et à l'étendue nationale de son champ d'action, cet institut ne justifie pas d'un intérêt lui donnant qualité pour demander l'annulation d'une décision autorisant, au titre de la loi du 27 décembre 1973 modifiée, l'extension d'un commerce et qui n'a d'effets que dans le cadre d'une aire géographique limitée ; que, dès lors, la société Star Players est fondée à soutenir que la cour administrative d'appel a entaché son arrêt d'erreur de qualification juridique et à en demander l'annulation ;<br/>
<br/>
              4. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative ; <br/>
<br/>
              5. Considérant qu'ainsi qu'il a été dit ci-dessus l'objet statutaire de l'Institut national du tapis distribution ne lui confère pas qualité pour demander l'annulation de la décision par laquelle la commission départementale d'aménagement commercial de Paris a autorisé la société Star Players à étendre son magasin spécialisé en équipement de tapis ; que, par suite, la société Star Players est fondée à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Paris a déclaré recevable la requête de l'Institut national du tapis distribution et a annulé la décision du 1er février 2007 de la commission départementale d'équipement commercial de Paris ; <br/>
<br/>
              6. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Institut national du tapis distribution la somme de 4 500 euros à verser à la société Star Players, au titre des dispositions de l'article L. 761-1 du code de justice administrative ; que ces dispositions font obstacle à ce qu'une somme soit mise à ce titre à la charge de la société Star Players qui n'est pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 30 juin 2011 de la cour administrative d'appel de Paris et le jugement du 8 octobre 2009 du tribunal administratif de Paris sont annulés.<br/>
Article 2 : La requête de l'Institut national du tapis distribution est rejetée.<br/>
Article 3 : L'Institut national du tapis distribution versera à la société Star Players la somme de 4 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Les conclusions de l'Institut national du tapis distribution présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée à la société Star Players, à l'Institut national du tapis distribution et à la Commission nationale d'aménagement commercial.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
