<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042074710</ID>
<ANCIEN_ID>JG_L_2020_07_000000426263</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/07/47/CETATEXT000042074710.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème - 10ème chambres réunies, 01/07/2020, 426263</TITRE>
<DATE_DEC>2020-07-01</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>426263</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème - 10ème chambres réunies</FORMATION>
<TYPE_REC>Contentieux des pensions</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BAUER-VIOLAS, FESCHOTTE-DESBOIS, SEBAGH</AVOCATS>
<RAPPORTEUR>M. Nicolas Agnoux</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emilie Bokdam-Tognetti</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:426263.20200701</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A... B... a demandé au tribunal administratif de Rennes, en premier lieu, d'annuler le titre de perception du 6 août 2015 émis à raison de l'existence d'un trop-perçu de pension civile de retraite et de le décharger de l'obligation de payer cette somme, en deuxième lieu, de condamner l'Etat à l'indemniser des préjudices qu'il a subis et à titre subsidiaire de prononcer une compensation entre la somme réclamée au titre du trop-perçu et ces préjudices et, en troisième lieu, d'enjoindre au ministre des finances de procéder à un nouveau calcul de sa pension de retraite. Par un jugement n° 1602185 du 1er octobre 2018, le tribunal administratif de Rennes a condamné l'Etat à verser la somme de 5 000 euros à M. B... au titre de son préjudice moral et rejeté le surplus des conclusions de la demande.<br/>
<br/>
              Par une ordonnance n° 18NT04100 du 7 décembre 2018, enregistrée le même jour au secrétariat du contentieux du Conseil d'Etat, la présidente de la cour administrative d'appel de Nantes a transmis au Conseil d'Etat, en application de l'article R. 351-2 du code de justice administrative, le pourvoi formé par M. B... contre ce jugement, enregistré le 22 novembre 2018 au greffe de la cour administrative d'appel de Nantes. <br/>
<br/>
              Par ce pourvoi et un nouveau mémoire enregistré le 4 février 2019 au secrétariat du contentieux du Conseil d'Etat, M. B... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
               2°) réglant l'affaire au fond, à titre principal, d'annuler le titre de perception, de le décharger de l'obligation de payer la somme de 67 111 euros et d'enjoindre au ministre de le rétablir dans ses droits à pension à hauteur des sommes versées avant abattement, complétées par la majoration pour enfants et, à titre subsidiaire, de le décharger partiellement de l'obligation de payer cette même somme, de condamner l'Etat à réparer les préjudices qu'il a subis à hauteur de 19 117 euros au titre des cotisations versées à perte, 118 901,16 euros au titre de ses droits à pension, 10 000 euros au titre de son préjudice moral et d'enjoindre au ministre de procéder à un nouveau calcul de sa pension ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - vu le Traité sur le fonctionnement de l'Union européenne ;<br/>
              - le code des pensions civiles et militaires de retraite ;<br/>
              - la loi n° 2002-73 du 17 janvier 2002 ;<br/>
              - le code de justice administrative ;<br/>
              - le code de justice administrative et l'ordonnance n° 2020-305 du 25 mars 2020 ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Nicolas Agnoux, maître des requêtes,<br/>
<br/>
              - les conclusions de Mme Emilie Bokdam-Tognetti, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Bauer-Violas, Feschotte-Desbois, Sebagh, avocat de M. A... B... ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que M. B..., attaché d'administration centrale au ministère de l'industrie, a été détaché auprès de la Commission européenne du 1er janvier 1984 au 30 août 2011, avant de faire valoir cette même année ses droits à la retraite auprès de l'administration française et de la Commission européenne. Sa pension de droit français a été mise en paiement à compter du 1er septembre 2011. Toutefois, par courrier du 27 avril 2015, la direction générale des finances publiques a informé M. B... que cette pension devait, conformément à l'article L. 87 du code des pensions civiles et militaires de retraite applicable aux agents ayant opté pour une double cotisation durant leur détachement à compter du 1er janvier 2002, être réduite à due concurrence de la pension versée au titre du régime des pensions de l'Union européenne, au prorata de la période ayant donné lieu à double cotisation. Par conséquent, l'administration a suspendu le versement de sa pension de droit français à hauteur du dépassement constaté et émis le 6 août 2015 un titre de perception d'un montant de 67 111 euros pour obtenir le remboursement des indus de pension pour la période du 1er septembre 2011 au 30 avril 2015. M. B... a saisi le tribunal administratif de Rennes d'une demande tendant à l'annulation du titre de perception, à l'indemnisation des préjudices résultant de la faute de l'administration, à ce qu'il soit enjoint à l'administration de procéder à un nouveau calcul de sa pension de retraite et, à titre subsidiaire, à ce que soit opérée une compensation entre la somme réclamée au titre du trop-perçu et les préjudices subis. Par un jugement du 1er octobre 2018, le tribunal a fait partiellement droit à sa demande indemnitaire en condamnant l'Etat à lui verser la somme de 5 000 euros au titre du préjudice moral et a rejeté le surplus de ses demandes. M. B... se pourvoit en cassation contre ce jugement. Eu égard aux moyens qu'il soulève, il doit être regardé comme en demandant l'annulation en tant qu'il rejette ses conclusions principales aux fins d'annulation et d'injonction et le surplus de ses conclusions indemnitaires.<br/>
<br/>
              Sur les conclusions tendant à l'annulation du jugement en tant qu'il statue sur les conclusions indemnitaires :<br/>
<br/>
              2. Aux termes de l'article R. 811-1 du code de justice administrative : " Toute partie présente dans une instance devant le tribunal administratif ou qui y a été régulièrement appelée, alors même qu'elle n'aurait produit aucune défense, peut interjeter appel contre toute décision juridictionnelle rendue dans cette instance. / Toutefois, le tribunal administratif statue en premier et dernier ressort : / (...)  7° Sur les litiges en matière de pensions de retraite des agents publics (...) ". Une action indemnitaire engagée par un agent public à raison du défaut d'information ou de renseignements erronés délivrés par le service en charge de ses droits à pension de retraite ne relève pas des litiges en matière de pension au sens de ces dispositions.<br/>
<br/>
              3. M. B... a saisi le tribunal administratif de Rennes d'une demande indemnitaire tendant à la réparation des préjudices que lui a causé, selon lui, le défaut d'information, par l'administration, sur le droit d'option dont il bénéficiait en application de l'article L. 87 du code des pensions civiles et militaires de retraite. Les conclusions de son pourvoi tendant à l'annulation du jugement en tant qu'il ne fait que partiellement droit à sa demande indemnitaire doivent être regardées comme des conclusions d'appel relevant de la compétence de la cour administrative d'appel de Nantes.<br/>
<br/>
              Sur les conclusions tendant à l'annulation du jugement en tant qu'il statue sur les conclusions aux fins d'injonction :<br/>
<br/>
              4. Par une décision dont le requérant a été informé le 27 juin 2019, le ministre de l'action et des comptes publics a procédé à un nouveau calcul de la pension de M. B... en mettant fin, pour les arrérages dus à compter du 6 octobre 2016, à l'application de l'abattement litigieux. Le pourvoi du requérant en tant qu'il porte sur sa demande tendant à ce qu'il soit enjoint au ministre de procéder à un nouveau calcul de sa pension a, dès lors, perdu son objet en tant qu'il porte sur les arrérages dus à compter du 6 octobre 2016. Il n'y a par suite plus lieu de statuer sur le pourvoi dans cette mesure.<br/>
<br/>
              Sur les conclusions tendant à l'annulation du jugement en tant qu'il statue sur les conclusions aux fins d'annulation et le surplus des conclusions aux fins d'injonction :<br/>
<br/>
              5. Aux termes de l'article L. 87 du code des pensions civiles et militaires de retraite dans sa version issue de l'article 20 de la loi du 17 janvier 2002 de modernisation sociale : " Dans le cas où le fonctionnaire ou le militaire détaché dans une administration ou un organisme implanté sur le territoire d'un Etat étranger ou auprès d'un organisme international au cours de sa carrière a opté pour la poursuite de la retenue prévue à l'article L. 61, le montant de la pension acquise au titre de ce code, ajouté au montant de la pension éventuellement servie au titre des services accomplis en position de détachement, ne peut être supérieur à la pension qu'il aurait acquise en l'absence de détachement et la pension du présent code est, le cas échéant, réduite à concurrence du montant de la pension acquise lors de ce détachement./ (...) ". Aux termes du VI de l'article 20 de la loi du 17 janvier 2002 de modernisation sociale : " Les dispositions du présent article s'appliquent aux agents en cours de détachement. Par dérogation aux dispositions de la première phrase de l'article L. 64 du code des pensions civiles et militaires de retraite, les agents qui ont effectué une période de détachement auprès d'une administration ou d'un organisme implanté sur le territoire d'un Etat étranger ou auprès d'un organisme international avant la date d'entrée en vigueur de la présente loi et non radiés des cadres à cette date peuvent demander le remboursement du montant des cotisations versées durant ces périodes au titre du régime spécial français dont ils relevaient, en contrepartie d'un abattement sur leur pension française à concurrence du montant de la pension acquise lors du détachement susvisé. A défaut d'une telle demande, leur pension française ne fera l'objet d'aucun abattement. (...) / La date d'application du présent article est fixée au 1er janvier 2002. "<br/>
<br/>
              6. Les dispositions précitées de l'article L. 87 du code des pensions civiles et militaires de retraite offrent la possibilité aux fonctionnaires détachés auprès d'un organisme international de continuer à cotiser au régime prévu par ce code, en prévoyant, dans ce cas, que la pension servie au titre de ce régime ne pourra compléter la pension acquise au titre du régime propre à l'organisme international que dans la limite de la pension qu'ils auraient acquise en l'absence de détachement. Le cas échéant, la pension française est réduite à hauteur du montant de la pension servie par l'organisme international. Toutefois, il résulte des dispositions du VI de l'article 20 de la loi du 17 janvier 2002, éclairées par leurs travaux préparatoires, que, pour les agents en cours de détachement au 1er janvier 2002 qui n'ont pas demandé le remboursement des cotisations versées avant cette date au régime français, la pension française ne fait l'objet d'aucun abattement au titre des droits acquis avant le 1er janvier 2002. Par suite, seule la fraction de la pension correspondant aux droits acquis après cette date, déterminée à proportion de la durée de services correspondante, est susceptible d'être réduite en application de l'article L. 87 précité.<br/>
<br/>
              7. Il ressort des pièces soumises aux juges du fond que M. B..., qui était détaché auprès de la Commission européenne à la date du 1er janvier 2002, n'a pas demandé le remboursement des cotisations versées avant cette date et a continué à cotiser au régime français pendant six ans, jusqu'au 31 décembre 2007. L'administration a fait application de la règle d'écrêtement prévue à l'article L. 87 du code des pensions civiles et militaires de retraite précité en réduisant la pension française de M. B... à proportion d'environ deux tiers.<br/>
<br/>
              8. Devant le tribunal administratif de Rennes, M. B... a soutenu que l'administration avait à tort appliqué l'écrêtement à l'intégralité de sa pension de droit français et non, conformément à la règle énoncée au point 6, à la seule fraction correspondant aux droits acquis à compter du 1er janvier 2002. Le tribunal a écarté ce moyen au motif que le requérant n'établissait pas l'existence d'une erreur de l'administration. En statuant par ces motifs, alors qu'il ressortait des titres de pension produits au dossier que seules six des trente-quatre années de service validées dans le régime français avaient été accomplies après le 1er janvier 2002 et que sa pension française n'était dès lors susceptible d'être réduite qu'à hauteur de la proportion correspondante, le tribunal a dénaturé les pièces du dossier et commis une erreur de droit.<br/>
<br/>
              9. Il résulte de ce qui précède, sans qu'il soit besoin de se prononcer sur les autres moyens du pourvoi, que M. B... est fondé à demander l'annulation du jugement qu'il attaque en tant qu'il statue sur ses conclusions tendant à l'annulation du titre de perception et à ce qu'il soit enjoint au ministre de procéder à un nouveau calcul de sa pension en ce qui concerne les arrérages dus antérieurement au 6 octobre 2016.<br/>
<br/>
              10. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat le versement à M. B... de la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Il n'y a pas lieu de statuer sur les conclusions du pourvoi de M. B... dirigées contre le jugement du tribunal administratif de Rennes du 1er octobre 2018 en tant qu'il rejette sa demande tendant à ce qu'il soit enjoint au ministre de l'action et des comptes publics de procéder à un nouveau calcul de sa pension au titre des arrérages dus postérieurement au 6 octobre 2016.<br/>
Article 2 : Le jugement des conclusions présentées par M. B... tendant à l'annulation du jugement attaqué en tant qu'il ne fait que partiellement droit à ses conclusions indemnitaires est attribué à la cour administrative d'appel de Nantes.<br/>
Article 3 : Le jugement du tribunal administratif de Rennes est annulé en tant qu'il statue sur les conclusions de M. B... tendant à l'annulation du titre de perception du 6 août 2015 et sur le surplus de ses conclusions à fins d'injonction.<br/>
Article 4 : L'affaire est renvoyée au tribunal administratif de Rennes dans la limite de la cassation ainsi prononcée.<br/>
Article 5 : L'Etat versera à M. B... la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 6 : La présente décision sera notifiée à M. A... B..., au ministre de l'action et des comptes publics et au président de la cour administrative d'appel de Nantes.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">48-02-01-08 PENSIONS. PENSIONS CIVILES ET MILITAIRES DE RETRAITE. QUESTIONS COMMUNES. CUMULS. - FONCTIONNAIRES DÉTACHÉS AUPRÈS D'UN ORGANISME INTERNATIONAL - CUMUL DE LA PENSION FRANÇAISE AVEC LA PENSION DE DÉTACHEMENT (ART. L. 87 DU CPCMR) - 1) MODALITÉS DE CALCUL - 2) CAS DES AGENTS N'AYANT PAS DEMANDÉ LE REMBOURSEMENT DES COTISATIONS VERSÉES AU RÉGIME FRANÇAIS AVANT LE 1ER JANVIER 2002 - ABSENCE D'ABATTEMENT APPLIQUÉ SUR LA PENSION FRANÇAISE AU TITRE DES DROITS ACQUIS AVANT CETTE DATE.
</SCT>
<ANA ID="9A"> 48-02-01-08 1) Les dispositions de l'article L. 87 du code des pensions civiles et militaires de retraite (CPCMR) offrent la possibilité aux fonctionnaires détachés auprès d'un organisme international de continuer à cotiser au régime prévu par ce code, en prévoyant, dans ce cas, que la pension servie au titre de ce régime ne pourra compléter la pension acquise au titre du régime propre à l'organisme international que dans la limite de la pension qu'ils auraient acquise en l'absence de détachement. Le cas échéant, la pension française est réduite à hauteur du montant de la pension servie par l'organisme international.... ,,2) Toutefois, il résulte des dispositions du VI de l'article 20 de la loi n° 2002-73 du 17 janvier 2002, éclairées par leurs travaux préparatoires, que, pour les agents en cours de détachement au 1er janvier 2002 qui n'ont pas demandé le remboursement des cotisations versées avant cette date au régime français, la pension française ne fait l'objet d'aucun abattement au titre des droits acquis avant le 1er janvier 2002. Par suite, seule la fraction de la pension correspondant aux droits acquis après cette date, déterminée à proportion de la durée de services correspondante, est susceptible d'être réduite en application de l'article L. 87 précité.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
