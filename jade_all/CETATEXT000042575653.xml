<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042575653</ID>
<ANCIEN_ID>JG_L_2020_11_000000415545</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/57/56/CETATEXT000042575653.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème - 10ème chambres réunies, 27/11/2020, 415545, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-11-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>415545</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème - 10ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR>M. Aurélien Caron</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Céline  Guibé</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:415545.20201127</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu les procédures suivantes :<br/>
<br/>
              1° Sous le n° 415545, par une requête sommaire et un mémoire complémentaire, enregistrés le 8 novembre 2017 et le 7 février 2018 au secrétariat du contentieux du Conseil d'Etat, l'Association nationale des opérateurs détaillants en énergie (ANODE) demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision implicite du Premier ministre rejetant la demande qu'elle a présentée le 12 juillet 2017 tendant à l'abrogation des articles R. 445-1 à R. 445-7 du code de l'énergie ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              2° Sous le n° 416853, par une requête sommaire et un mémoire complémentaire, enregistrés les 26 décembre 2017 et 26 mars 2018 au secrétariat du contentieux du Conseil d'Etat, la société Direct Energie demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision implicite du Premier ministre rejetant la demande qu'elle a présentée le 29 août 2017 tendant à l'abrogation des articles R. 445-1 à R. 445-7 du code de l'énergie ;<br/>
<br/>
              2°) de condamner l'Etat à lui verser la somme de 513 millions d'euros en réparation du préjudice qu'elle a subi à raison du maintien de la réglementation relative aux tarifs réglementés de vente de gaz naturel, incompatible avec le droit de l'Union européenne ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu : <br/>
              - le traité sur le fonctionnement de l'Union européenne ; <br/>
              - la directive 2009/73/CE du Parlement européen et du Conseil du 13 juillet 2009 ;<br/>
              - le code de commerce ;<br/>
              - le code de l'énergie ;<br/>
              - le code des relations entre le public et l'administration ;<br/>
              - la loi n° n° 2019-1147 du 8 novembre 2019 relative à l'énergie et au climat, et notamment son article 63 ;<br/>
              - l'arrêt de la Cour de justice de l'Union européenne du 20 avril 2010, aff. <br/>
C-265/08 ;<br/>
              - l'arrêt de la Cour de justice de l'Union européenne du 28 juillet 2016, aff. <br/>
C-379/15 ;<br/>
              - l'arrêt de la Cour de justice de l'Union européenne du 7 septembre 2016, aff. C-121/15 ;<br/>
              - la décision n° 370321 du 19 juillet 2017 du Conseil d'Etat statuant au contentieux;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Aurélien Caron, auditeur,  <br/>
<br/>
              - les conclusions de Mme Céline Guibé, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Rousseau, Tapie, avocat de la société Direct Energie ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
              1. Les requêtes de l'Association nationale des opérateurs détaillants en énergie (ANODE) et de la société Direct Energie présentent à juger des questions communes. Il y a lieu de les joindre pour statuer par une même décision.<br/>
<br/>
              2. L'ANODE et la société Direct Energie ont saisi le Premier ministre de demandes tendant à l'abrogation des articles R. 445-1 à R. 445-7 du code de l'énergie fixant les règles de détermination des tarifs réglementés de vente de gaz naturel. Elles demandent l'annulation pour excès de pouvoir des refus implicites opposés à ces demandes. La société Direct Energie demande en outre que l'Etat soit condamné à lui verser la somme de 513 millions d'euros en réparation du préjudice qu'elle estime avoir subi du fait du maintien de ces dispositions en dépit de leur incompatibilité avec le droit de l'Union européenne.<br/>
<br/>
              Sur les conclusions indemnitaires présentées par la société Direct Energie :<br/>
<br/>
              3. Aux termes de l'article L. 311-1 du code de justice administrative : " Les tribunaux administratifs sont, en premier ressort, juges de droit commun du contentieux administratif, sous réserve des compétences que l'objet du litige ou l'intérêt d'une bonne administration de la justice conduisent à attribuer à une autre juridiction administrative ". Aux termes de l'article R. 311-1 du même code : " Le Conseil d'Etat est compétent pour connaître en premier et dernier ressort : (...) 2° Des recours dirigés contre les actes réglementaires des ministres (...) ". Aux termes de l'article R. 341-1 du même code : " Lorsque le Conseil d'Etat est saisi de conclusions relevant de sa compétence de premier ressort, il est également compétent pour connaître de conclusions connexes relevant normalement de la compétence de premier ressort d'un tribunal administratif ou d'une cour administrative d'appel ".<br/>
<br/>
              4. Par lettre du 29 août 2017, la société Direct Energie a demandé au Premier ministre la réparation du préjudice qu'elle estime avoir subi, à compter du 1er janvier 2010, en raison de l'incompatibilité de la réglementation relative aux tarifs réglementés de vente de gaz naturel avec le droit de l'Union européenne. Cette demande a été présentée postérieurement à la décision n° 370321 du 19 juillet 2017, par laquelle le Conseil d'Etat statuant au contentieux a jugé que les dispositions législatives du code de l'énergie relatives à ces tarifs sont incompatibles avec les objectifs de la directive 2009/73/CE du Parlement européen et du Conseil du 13 juillet 2009 concernant des règles communes pour le marché intérieur du gaz naturel et a annulé le décret du 16 mai 2013 modifiant le décret du 18 décembre 2009 pris pour leur application, dont les dispositions sont aujourd'hui codifiées aux articles R. 445-1 à R. 445-7 du code de l'énergie. Dans ces conditions, les conclusions indemnitaires de la société Direct Energie ne sont pas connexes aux conclusions par lesquelles elle demande l'annulation pour excès de pouvoir de la décision résultant du silence gardé par le Premier ministre sur sa demande du 12 juillet 2017 tendant à l'abrogation des articles R. 445-1 à R. 445-7 du code de l'énergie. Le Conseil d'Etat n'étant pas compétent pour connaître en premier ressort de ces conclusions, il y a lieu de les transmettre au tribunal administratif de Paris.<br/>
<br/>
              Sur les conclusions dirigées contre le refus d'abroger les articles R. 445-1 à R. 445-7 du code de l'énergie :<br/>
<br/>
              5. La société Engie justifie d'un intérêt suffisant au maintien des dispositions réglementaires du code de l'énergie dont les requérantes ont demandé l'abrogation. Par suite, ses interventions en défense sont, dans cette mesure, recevables. <br/>
<br/>
              6. Lorsqu'il est saisi de conclusions aux fins d'annulation du refus d'abroger un acte réglementaire, le juge de l'excès de pouvoir est conduit à apprécier la légalité de l'acte réglementaire dont l'abrogation a été demandée au regard des règles applicables à la date de sa décision. <br/>
<br/>
              7. Afin de remédier à l'incompatibilité de la réglementation relative aux tarifs réglementés de vente de gaz naturel avec le droit de l'Union européenne, constatée par la décision n° 370321 du 19 juillet 2017 du Conseil d'Etat statuant au contentieux, le législateur a organisé la suppression progressive de ces tarifs selon les modalités exposées ci-après. Le 11° du I de l'article 63 de la loi du 8 novembre 2019 relative à l'énergie et au climat a abrogé, à compter du 10 novembre 2019, les articles L. 445-1 à L. 445-4 du code de l'énergie relatifs aux tarifs réglementés de vente de gaz naturel. Cet article prévoit toutefois que ces dispositions législatives, ainsi que les dispositions réglementaires prises pour leur application, restent applicables, dans leur rédaction antérieure, aux contrats de fourniture de gaz aux tarifs réglementés en cours d'exécution à la date de publication de la loi, jusqu'au premier jour du treizième mois suivant cette publication pour les consommateurs finals non domestiques consommant moins de 30 000 kilowattheures par an, et jusqu'au 30 juin 2023 pour les consommateurs finals domestiques consommant moins de 30 000 kilowattheures par an ainsi que pour les propriétaires uniques d'un immeuble à usage principal d'habitation consommant moins de 150 000 kilowattheures par an et les syndicats des copropriétaires d'un tel immeuble. Il prévoit également que les mêmes dispositions restent applicables aux clients ayant précédemment souscrit un contrat de fourniture de gaz aux tarifs réglementés qui ont vu ce contrat résilié à la suite d'une erreur commise par le gestionnaire du réseau ou par un fournisseur, lors du traitement d'une demande de résiliation émanant d'un autre consommateur.<br/>
<br/>
              8. Les dispositions de l'article 63 de la loi du 8 novembre 2019 relative à l'énergie et au climat analysées au point 7, qui maintiennent l'application des dispositions du code de l'énergie relatives aux tarifs réglementés de vente de gaz naturel aux contrats en cours d'exécution, jusqu'au 1er décembre 2020 pour les clients professionnels et jusqu'au 30 juin 2023 pour les clients domestiques, imposent au pouvoir réglementaire de maintenir en vigueur les articles R. 445-1 à R. 445-7 de ce code jusqu'à la fin de cette période transitoire. Par suite, le Premier ministre était tenu de rejeter les demandes tendant à leur abrogation et les moyens soulevés par les requérants doivent en conséquence être écartés comme inopérants. Dès lors et sans qu'il soit besoin de se prononcer sur la fin de non-recevoir soulevée par la société Engie à l'encontre de la requête de l'ANODE, cette dernière et la société Direct Energie ne sont pas fondées à demander l'annulation des décisions par lesquelles le Premier ministre a rejeté leurs demandes tendant à l'abrogation des articles R. 445-1 à R. 445-7 du code de l'énergie. Les conclusions de l'ANODE aux fins d'injonction doivent également, par voie de conséquence, être rejetées. <br/>
<br/>
              9. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise, à ce titre, à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante. Ces dispositions font également obstacle à ce qu'il soit fait droit aux conclusions présentées à ce titre par la société Engie, qui n'est pas partie à l'instance.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Les interventions de la société Engie sont admises.<br/>
Article 2 : Le jugement des conclusions indemnitaires de la société Direct Energie est attribué au tribunal administratif de Paris.<br/>
Article 3 : La requête de l'ANODE et le surplus de la requête de la société Direct Energie sont rejetés.<br/>
Article 4 : Les conclusions présentées par la société Engie au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée à l'Association nationale des opérateurs détaillants en énergie, à la société Direct Energie, au Premier ministre, à la ministre de la transition écologique, au ministre de l'économie, des finances et de la relance et à la société Engie.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
