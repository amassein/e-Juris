<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028170420</ID>
<ANCIEN_ID>JG_L_2013_11_000000362092</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/17/04/CETATEXT000028170420.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème et 10ème sous-sections réunies, 07/11/2013, 362092</TITRE>
<DATE_DEC>2013-11-07</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>362092</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème et 10ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Maïlys Lange</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Claire Legras</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:362092.20131107</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 21 août 2012 au secrétariat du contentieux du Conseil d'Etat, présentée par la société Transport et Infrastructures Gaz France (TIGF), dont le siège est 49 avenue Dufau, BP 522 à Pau (64010), représentée par son président directeur général en exercice ; la société TIGF demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler pour excès de pouvoir, d'une part, la délibération de la Commission de régulation de l'énergie du 22 novembre 2011 portant mise à jour des tarifs d'utilisation des réseaux de transport de gaz naturel, d'autre part, la délibération du 21 juin 2012 de cette Commission portant décision relative à son recours gracieux en date du 24 avril 2012, en tant qu'elles rejettent sa demande de couvrir, dans le cadre du tarif en vigueur, les charges liées à l'évolution du cadre réglementaire relatif à l'imposition forfaitaire des entreprises de réseaux ; <br/>
<br/>
              2°) de mettre à la charge de la Commission de régulation de l'énergie la somme de 15 000 euros en application de l'article L. 761-1 du code de justice administrative ainsi que les entiers dépens ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 30 octobre 2013, présentée par la société Transport et Infrastructures Gaz France (TIGF) ;<br/>
<br/>
              Vu le code de l'énergie ;<br/>
<br/>
              Vu la loi n° 2003-8 du 3 janvier 2003 ;<br/>
<br/>
              Vu l'ordonnance n° 2011-504 du 9 mai 2011 ;<br/>
<br/>
              Vu le décret n° 2005-607 du 27 mai 2005 ;<br/>
<br/>
              Vu l'arrêté du 6 octobre 2008 approuvant les tarifs d'utilisation des réseaux de transport de gaz naturel ; <br/>
<br/>
              Vu l'arrêté du 3 mars 2011 approuvant les tarifs d'utilisation des réseaux de transport de gaz naturel ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Maïlys Lange, Auditeur,  <br/>
<br/>
              - les conclusions de Mme Claire Legras, rapporteur public ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier que, par un arrêté du 6 octobre 2008, les ministres chargés de l'économie et de l'énergie ont approuvé la proposition tarifaire de la Commission de régulation de l'énergie du 10 juillet 2008 pour l'utilisation des réseaux de transport de gaz naturel ; que ce cadre tarifaire, entré en vigueur le 1er janvier 2009, définit pour chaque gestionnaire de réseau de transport de gaz naturel une trajectoire de revenu autorisé prenant en considération, d'une part, ses charges d'exploitation nécessaires au bon fonctionnement et à la sécurité des réseaux, d'autre part, ses charges en capital ; qu'il définit également les postes de charges et revenus couverts par le mécanisme du " compte de régulation des charges et produits " (CRCP), destiné à corriger les écarts entre les prévisions réalisées au moment de l'élaboration de la trajectoire de revenu autorisé et les charges et produits effectivement constatés pendant la période d'application de cette trajectoire, l'écart constaté étant pris en compte pour établir le revenu autorisé du gestionnaire de réseau de transport sur la période tarifaire ultérieure ; que la trajectoire de revenu autorisé a été définie sur une période tarifaire fixée à deux ans pour la société Transport et Infrastructures Gaz France (TIGF) et à quatre ans pour la société GRTgaz, la grille tarifaire de cette dernière société étant mise à jour annuellement ; que, par une délibération en date du 28 octobre 2010, approuvée par un arrêté des ministres chargés de l'économie et de l'énergie du 3 mars 2011, la Commission de régulation de l'énergie a proposé de nouveaux tarifs d'utilisation des réseaux de transport de gaz naturel, conçus pour s'appliquer du 1er avril 2011 au 31 mars 2012 pour GRTgaz et du 1er avril 2011 au 31 mars 2013 pour TIGF ; que, dans sa délibération du 22 novembre 2011 portant mise à jour des tarifs d'utilisation des réseaux de transport de gaz naturel, la Commission de régulation de l'énergie a rejeté la demande de la société TIGF tendant à modifier les charges d'exploitation retenues dans le cadre tarifaire en vigueur afin de couvrir de nouvelles charges liées à l'évolution du cadre règlementaire relatif à l'imposition forfaitaire des entreprises de réseaux de gaz (IFER) ; que la société TIGF demande l'annulation pour excès de pouvoir de cette délibération en tant qu'elle a rejeté sa demande ; <br/>
<br/>
              2. Considérant qu'aux termes de l'article L. 452-3 du code de l'énergie : " La Commission de régulation de l'énergie délibère sur les évolutions tarifaires ainsi que sur celles des prestations annexes réalisées exclusivement par les gestionnaires de ces réseaux ou de ces installations avec, le cas échéant, les modifications de niveau et de structure des tarifs qu'elle estime justifiées au vu notamment de l'analyse de la comptabilité des opérateurs et de l'évolution prévisible des charges de fonctionnement et d'investissement. Ces délibérations, qui peuvent avoir lieu à la demande des gestionnaires de réseaux de transport ou de distribution de gaz naturel ou des gestionnaires d'installations de gaz naturel liquéfié, peuvent prévoir un encadrement pluriannuel de l'évolution des tarifs ainsi que des mesures incitatives appropriées à court ou long terme pour encourager les opérateurs à améliorer leurs performances (...) " ; que ces dispositions, qui donnent compétence à la Commission de régulation de l'énergie pour délibérer sur l'évolution des tarifs d'utilisation des réseaux de transport de gaz naturel, ont pu entrer en vigueur sans l'intervention de mesures d'application et sans qu'y fasse obstacle l'abrogation différée, par l'article 4 de l'ordonnance du 9 mai 2011 portant codification de la partie législative du code de l'énergie, des références aux " ministres chargées de l'économie et de l'énergie " figurant dans la loi du 3 janvier 2003 relative aux marchés du gaz et de l'électricité et au service public de l'énergie, notamment au I de l'article 7 de cette loi, repris à l'article L. 452-3 du code de l'énergie ; qu'il suit de là que la société TIGF n'est pas fondée à soutenir que les délibérations attaquées, intervenues postérieurement à l'entrée en vigueur du code de l'énergie, le 1er juin 2011, sont entachées d'incompétence ;<br/>
<br/>
              3. Considérant qu'aux termes de l'article L. 452-1 du code de l'énergie, reprenant le III de l'article 7 de la loi du 3 janvier 2003 : " Les tarifs d'utilisation des réseaux de transport et de distribution de gaz naturel (...) sont établis de manière transparente et non discriminatoire afin de couvrir l'ensemble des coûts supportés par [les] gestionnaires, dans la mesure où ces coûts correspondent à ceux d'un gestionnaire de réseau (...) efficace. (...) " ; que selon l'article 1er du décret du 27 mai 2005 relatif aux règles de tarification applicables à l'utilisation des réseaux de transport de gaz naturel : " Les tarifs d'utilisation des réseaux de transport de gaz naturel sont déterminés, selon les modalités prévues à l'article 7 de la loi du 3 janvier 2003 susvisée, pour chaque gestionnaire de réseau de gaz naturel autorisé (...) en fonction de l'ensemble de ses charges d'exploitation et de ses charges d'investissement " ; qu'il résulte de la combinaison de ces dispositions que la rémunération d'un gestionnaire de réseau de transport de gaz naturel doit au moins couvrir ses charges d'exploitation et ses charges d'investissement, prises dans leur ensemble, en tenant compte en outre des gains de productivité attendus d'un gestionnaire de réseau efficace  ; <br/>
<br/>
              4. Considérant qu'il appartient à la Commission de régulation de l'énergie, d'office ou à la demande du gestionnaire de réseau intéressé, de modifier, au besoin en cours de période tarifaire, le niveau et la structure des tarifs d'utilisation des réseaux de transport de gaz naturel, si elle constate, en fonction des éléments dont elle dispose, qu'un écart significatif s'est produit ou est susceptible de se produire entre le revenu autorisé d'un gestionnaire et ses coûts ; que la Commission ne saurait s'affranchir de cette règle au seul motif que l'écart serait dû à l'apparition d'une charge ou d'un produit dont la prise en compte imposerait de modifier le compte de régulation des charges et produits défini pour la période tarifaire en cours ;<br/>
<br/>
              5. Considérant qu'il ressort des pièces du dossier qu'à la date de la délibération attaquée, aucun des éléments dont disposait la Commission de régulation de l'énergie ne faisait apparaître que les charges globales de la société requérante n'auraient pas été couvertes, ni que les charges supplémentaires liées à l'évolution du cadre règlementaire relatif à l'imposition forfaitaire des entreprises de réseaux de gaz auraient pour effet d'entraîner un déséquilibre global entre le tarif et les coûts ; que, par suite, en rejetant la demande de la société TIGF tendant à ce que soit prise en compte dans le cadre du tarif en vigueur la charge supplémentaire liée à cette imposition, la Commission, qui ne s'est pas fondée sur des faits matériellement inexacts, n'a entaché sa décision ni d'erreur de droit ni d'erreur manifeste d'appréciation ;<br/>
<br/>
              6. Considérant qu'il résulte de ce qui précède, et sans qu'il soit besoin de statuer sur la fin de non-recevoir opposée par la Commission de régulation de l'énergie, que la société TIGF n'est pas fondée à demander l'annulation des délibérations qu'elle attaque ; que sa requête doit, par suite, être rejetée, y compris ses conclusions tendant à l'application des dispositions de l'article L. 761-1 du code de justice administrative ; que, dans les circonstances de l'espèce, il y a lieu de laisser la contribution pour l'aide juridique à la charge de cette société ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : La requête de la société TIGF est rejetée.<br/>
Article 2 : La présente décision sera notifiée à la société Transport et Infrastructures Gaz France et à la Commission de régulation de l'énergie.<br/>
Copie en sera adressée pour information au ministre de l'économie et des finances et au ministre de l'écologie, du développement durable et de l'énergie.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">29-06-01 ENERGIE. MARCHÉ DE L'ÉNERGIE. COMMISSION DE RÉGULATION DE L'ÉNERGIE. - OBLIGATION DE LA CRE DE MODIFIER LE NIVEAU ET LA STRUCTURE DES TARIFS D'UTILISATION DES RÉSEAUX DE TRANSPORT DE GAZ NATUREL SI ELLE CONSTATE QU'UN ÉCART SIGNIFICATIF S'EST PRODUIT OU EST SUSCEPTIBLE DE SE PRODUIRE ENTRE LE REVENU AUTORISÉ D'UN GESTIONNAIRE ET SES COÛTS - EXISTENCE - CIRCONSTANCE QUE L'ÉCART SERAIT DÛ À L'APPARITION D'UNE CHARGE OU D'UN PRODUIT DONT LA PRISE EN COMPTE IMPOSERAIT DE MODIFIER LE CRCP DÉFINI POUR LA PÉRIODE TARIFAIRE EN COURS - INCIDENCE - ABSENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">29-06-02-02-01 ENERGIE. MARCHÉ DE L'ÉNERGIE. TARIFICATION. GA. TRANSPORT. - TARIFS D'UTILISATION DES RÉSEAUX DE TRANSPORT DE GAZ NATUREL - 1) EXIGENCE QUE LA RÉMUNÉRATION D'UN GESTIONNAIRE DE RÉSEAU COUVRE AU MOINS SES CHARGES D'EXPLOITATION ET SES CHARGES D'INVESTISSEMENT, PRISES DANS LEUR ENSEMBLE - EXISTENCE - 2) OBLIGATION DE LA CRE DE MODIFIER LE NIVEAU ET LA STRUCTURE DES TARIFS SI ELLE CONSTATE QU'UN ÉCART SIGNIFICATIF S'EST PRODUIT OU EST SUSCEPTIBLE DE SE PRODUIRE ENTRE LE REVENU AUTORISÉ D'UN GESTIONNAIRE ET SES COÛTS - EXISTENCE - CIRCONSTANCE QUE L'ÉCART SERAIT DÛ À L'APPARITION D'UNE CHARGE OU D'UN PRODUIT DONT LA PRISE EN COMPTE IMPOSERAIT DE MODIFIER LE CRCP DÉFINI POUR LA PÉRIODE TARIFAIRE EN COURS - INCIDENCE - ABSENCE.
</SCT>
<ANA ID="9A"> 29-06-01 Il appartient à la Commission de régulation de l'énergie (CRE), d'office ou à la demande du gestionnaire de réseau intéressé, de modifier, au besoin en cours de période tarifaire, le niveau et la structure des tarifs d'utilisation des réseaux de transport de gaz naturel, si elle constate, en fonction des éléments dont elle dispose, qu'un écart significatif s'est produit ou est susceptible de se produire entre le revenu autorisé d'un gestionnaire et ses coûts. La Commission ne saurait s'affranchir de cette règle au seul motif que l'écart serait dû à l'apparition d'une charge ou d'un produit dont la prise en compte imposerait de modifier le compte de régulation des charges et produits (CRCP) défini pour la période tarifaire en cours.</ANA>
<ANA ID="9B"> 29-06-02-02-01 1) Il résulte de la combinaison des dispositions de l'article L. 452-1 du code de l'énergie et de l'article 1er du décret n° 2005-607 du 27 mai 2005 relatif aux règles de tarification applicables à l'utilisation des réseaux de transport de gaz naturel que la rémunération d'un gestionnaire de réseau de transport de gaz naturel doit au moins couvrir ses charges d'exploitation et ses charges d'investissement, prises dans leur ensemble, en tenant compte en outre des gains de productivité attendus d'un gestionnaire de réseau efficace.,,,2) Il appartient à la Commission de régulation de l'énergie (CRE), d'office ou à la demande du gestionnaire de réseau intéressé, de modifier, au besoin en cours de période tarifaire, le niveau et la structure des tarifs d'utilisation des réseaux de transport de gaz naturel, si elle constate, en fonction des éléments dont elle dispose, qu'un écart significatif s'est produit ou est susceptible de se produire entre le revenu autorisé d'un gestionnaire et ses coûts. La Commission ne saurait s'affranchir de cette règle au seul motif que l'écart serait dû à l'apparition d'une charge ou d'un produit dont la prise en compte imposerait de modifier le compte de régulation des charges et produits (CRCP) défini pour la période tarifaire en cours.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
