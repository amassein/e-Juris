<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029288261</ID>
<ANCIEN_ID>JG_L_2014_07_000000366775</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/28/82/CETATEXT000029288261.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  3ème sous-section jugeant seule, 23/07/2014, 366775, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-07-23</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>366775</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 3ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>BLONDEL ; SCP ROUSSEAU, TAPIE</AVOCATS>
<RAPPORTEUR>M. Fabrice Benkimoun</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Vincent Daumas</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2014:366775.20140723</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
Procédure contentieuse antérieure<br/>
<br/>
              Le 28 mars 2011, la SNC Roccapina 2006 a demandé au tribunal administratif de la Polynésie française de prononcer la décharge de la majoration de 80% dont ont été assortis les rappels de taxe sur la valeur ajoutée qui lui ont été réclamés au titre de la période du 1er janvier 2007 au 31 décembre 2009.<br/>
<br/>
              Par un jugement n° 1100159 du 7 juin 2011, le tribunal administratif de la Polynésie française a rejeté la demande de la SNC Roccapina 2006.<br/>
<br/>
              Par un arrêt n° 11PA04087 du 7 décembre 2012, la cour administrative d'appel de Paris a rejeté l'appel formé par la SNC Roccapina 2006 contre le jugement du tribunal administratif de la Polynésie française.<br/>
<br/>
Procédure devant le Conseil d'Etat<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 13 mars et 13 juin 2013 au secrétariat du contentieux du Conseil d'Etat, la SNC Roccapina 2006 demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt de la cour administrative d'appel de Paris du 7 décembre 2012 ;<br/>
<br/>
              2°) de mettre à la charge de la Polynésie française la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative, ainsi que les dépens.<br/>
<br/>
<br/>
	Vu :<br/>
              - les autres pièces du dossier ;<br/>
              - le code des impôts de la Polynésie française ;<br/>
              - le code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Fabrice Benkimoun, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Vincent Daumas, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Rousseau, Tapie, avocat de la SNC Roccapina 2006  et à Me Blondel, avocat de la présidence de la Polynésie française ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. En premier lieu, le moyen tiré de ce que la cour administrative d'appel aurait inexactement interprété les écritures de la SNC Roccapina 2006 en jugeant qu' elle ne contestait pas ne pas avoir acquis auprès de la SARL Funéraires Min Chiu les aménagements d'un hall d'exposition est inopérant. Par suite, il doit être écarté.<br/>
<br/>
              2. En second lieu, en vertu des articles 345-1 et 345-4 du code des impôts de la Polynésie française, tout assujetti à la taxe sur la valeur ajoutée qui déduit de la taxe exigible la taxe qu'il a lui-même acquittée à raison des biens et services utilisés pour les besoins de ses opérations taxables est tenu d'en mentionner les montants dans des déclarations périodiques de chiffres d'affaires. Aux termes de l'article 511-5 du même code : " 1. Lorsque la déclaration (...) fait apparaître une base d'imposition ou des éléments servant à la liquidation de l'un des impôts (...) insuffisants, inexacts ou incomplets, le montant des droits mis à la charge du contribuable est assorti (...) d'une majoration de 40% si la mauvaise foi de l'intéressé est établie, ou de 80% s'il s'est rendu coupable de manoeuvres frauduleuses (...) ".<br/>
<br/>
              3. Un contribuable se livre à des opérations constitutives de manoeuvres frauduleuses au sens de l'article 511-5 du code des impôts de la Polynésie française s'il a non seulement méconnu les obligations prévues par le code des impôts de la Polynésie française, en vue d'éluder l'impôt, mais aussi commis ou participé à des agissements ayant pour objet d'égarer l'administration ou de restreindre son pouvoir de contrôle.<br/>
<br/>
              4. La cour administrative d'appel a relevé qu'il résulte de l'instruction, d'une part, que l'opération d'acquisition des éléments d'aménagement d'un hall d'exposition par la SNC Roccapina 2006 auprès de la SARL Funéraires Min Chiu n'est pas établie et n'ouvre dès lors pas droit à la déduction de la taxe sur la valeur ajoutée ayant grevé le prix d'acquisition de ces éléments et, d'autre part, que la SNC a conclu avec la SARL un contrat d'acquisition, puis un contrat de location de tels éléments, alors que la SARL disposait déjà d'un hall d'exposition. En déduisant de ces énonciations que la SNC Roccapina 2006 a participé à un montage destiné à donner une apparence de réalité à cette opération d'acquisition d'éléments d'aménagement d'un hall d'exposition et en jugeant qu'en conséquence, le service des contributions de la Polynésie française était fondé à assortir les rappels de taxe sur la valeur ajoutée résultant de la remise en cause du droit à déduction de la majoration de 80% prévue à l'article 511-5 du code des impôts de la Polynésie française, la cour administrative d'appel a suffisamment motivé son arrêt et n'a commis ni erreur de qualification juridique, ni erreur de droit.<br/>
<br/>
              5. Il résulte de tout ce qui précède que la SNC Roccapina 2006 n'est pas fondée à demander l'annulation de l'arrêt attaqué.<br/>
<br/>
              6. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de la Polynésie française, qui n'est pas, dans la présente instance, la partie perdante. En revanche, il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la SNC Roccapina 2006 la somme de 3 000 euros, à verser à la Polynésie française, au titre de ces mêmes dispositions.<br/>
<br/>
<br/>
<br/>
<br/>DECIDE :<br/>
Article 1er : Le pourvoi de la SNC Roccapina 2006 est rejeté.<br/>
Article 2 : La SNC Roccapina 2006 versera à la Polynésie française une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée à la SNC Roccapina 2006 et au président de la Polynésie française.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
