<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000035213352</ID>
<ANCIEN_ID>JG_L_2017_07_000000404558</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/35/21/33/CETATEXT000035213352.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème chambre, 10/07/2017, 404558, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-07-10</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>404558</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Guillaume Leforestier</RAPPORTEUR>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CECHS:2017:404558.20170710</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête sommaire, un mémoire complémentaire et un mémoire en réplique enregistrés, les 19 octobre et 17 décembre 2016 et le 29 mai 2017 au secrétariat du contentieux du Conseil d'Etat, la société à responsabilité limitée (SARL) Astumania demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision implicite du Premier ministre rejetant sa demande présentée le 4 juillet 2016, tendant à l'abrogation de l'article 1er du décret n°2015-743 du 24 juin 2015 relatif à la lutte contre l'insécurité routière, ainsi que la décision du 3 novembre 2016 signée par le délégué interministériel à la sécurité routière rejetant explicitement cette demande ;<br/>
<br/>
              2°) d'enjoindre au Premier ministre d'abroger ou, à tout le moins, de modifier les dispositions que l'article 1er dudit décret a introduites à l'article R. 412-6-1 du code de la route, et ce dans un délai d'un mois à compter de la notification de la décision à intervenir et sous astreinte de 75 euros par jour de retard ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat le versement d'une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
<br/>
	Vu : <br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - le code de la route ; <br/>
              - le code de justice administrative ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 12 juin 2017, présentée par le ministre de l'intérieur ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Guillaume Leforestier, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Laurence Marion, rapporteur public ;<br/>
<br/>
<br/>
<br/>1. Considérant que, par  son article 1er, le décret du 24 juin 2015 relatif à la lutte contre l'insécurité routière a, notamment, inséré à l'article R. 412-6-1 du code de la route, après le premier alinéa, deux alinéas ainsi rédigés : " Est également interdit le port à l'oreille, par le conducteur d'un véhicule en circulation, de tout dispositif susceptible d'émettre du son, à l'exception des appareils électroniques correcteurs de surdité. / Les dispositions du deuxième alinéa ne sont pas applicables aux conducteurs des véhicules d'intérêt général prioritaire prévus à l'article R. 311-1, ni dans le cadre de l'enseignement de la conduite des cyclomoteurs, motocyclettes, tricycles et quadricycles à moteur ou de l'examen du permis de conduire ces véhicules " ; que la SARL Astumania, qui commercialise un dispositif destiné à permettre au conducteur d'une motocyclette d'entendre la voix de son passager, transmise par un tuyau souple jusqu'à un embout placé dans son oreille, a demandé au Premier ministre, le 4 juillet 2016, d'abroger ces dispositions ; qu'elle présente un recours pour excès de pouvoir contre le refus qui lui a été opposé par une décision implicite, confirmée par une décision explicite du 3 novembre 2016 signée par le délégué interministériel à la sécurité routière ;<br/>
<br/>
              2. Considérant, en premier lieu, que le principe de légalité des délits et l'objectif de valeur constitutionnelle de clarté et d'intelligibilité de la norme de droit impliquent que, pour les sanctions dont le régime relève de sa compétence, le pouvoir réglementaire définisse les infractions en des termes suffisamment clairs et précis pour exclure l'arbitraire ; qu'en interdisant, sous peine de l'amende prévue pour les contraventions de la quatrième classe, " le port à l'oreille, par le conducteur d'un véhicule en circulation, de tout dispositif susceptible d'émettre du son, à l'exception des appareils électroniques correcteurs de surdité ", le Premier ministre a satisfait à cette exigence ; qu'en particulier, il résulte des termes mêmes de ces dispositions que l'interdiction qu'elles prévoient est limitée aux dispositifs émettant du son portés à l'oreille et ne s'étend donc pas aux dispositifs installés dans l'habitacle d'un véhicule ou intégrés dans le casque du conducteur d'une motocyclette ou d'un autre véhicule mentionné à l'article R. 431-1 du code de la route et ne comportant pas d'élément inséré dans l'oreille ou accroché à celle-ci ;<br/>
<br/>
              3. Considérant, en second lieu, qu'il ressort des pièces du dossier que les dispositifs émettant du son portés à l'oreille ont en commun, quelle que soit la technique utilisée, de distraire l'attention du conducteur tout en l'isolant, dans une certaine mesure, de son environnement sonore ; que, compte tenu de l'impact avéré de ces dispositifs sur la sécurité des usagers de la route, le décret attaqué a, en les prohibant, édicté une interdiction nécessaire et proportionnée aux buts poursuivis ; que le moyen tiré de ce que le décret serait entaché d'une erreur d'appréciation doit, par suite, être écarté ; qu'à supposer même que l'interdiction litigieuse puisse être regardée comme restreignant le droit de communiquer et de recevoir des informations, garanti par l'article 10 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, elle n'est pas contraire aux stipulations de cet article dès lors qu'elle est nécessaire pour assurer la sécurité des usagers de la route ; <br/>
<br/>
              4. Considérant, enfin, que la circonstance, à la supposer établie, que les systèmes qui diffusent le son dans l'habitacle du véhicule, ou ceux qui sont intégrés dans le casque du conducteur d'une motocyclette ou d'un autre véhicule mentionné à l'article R. 431-1 du code de la route, dont l'utilisation n'est pas interdite, présenteraient des risques comparables à ceux qui sont portés à l'oreille est sans incidence sur la légalité de l'interdiction contestée ; que, par suite, le moyen pris de la violation du principe d'égalité devant la loi est inopérant ;<br/>
<br/>
              5. Considérant qu'il résulte de ce qui précède, que la SARL Astumania n'est pas fondée à demander l'annulation du décret qu'elle attaque ; que ses conclusions à fin d'injonction et d'astreinte, ainsi que celles qu'elle présente au titre des dispositions de l'article L .761-1 du code de justice administrative, doivent en conséquence être rejetées ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de la SARL Astumania est rejetée.<br/>
<br/>
Article 2 : La présente décision sera notifiée à la SARL Astumania, au Premier ministre et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
