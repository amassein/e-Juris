<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030538067</ID>
<ANCIEN_ID>JG_L_2015_04_000000372356</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/53/80/CETATEXT000030538067.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème / 4ème SSR, 29/04/2015, 372356</TITRE>
<DATE_DEC>2015-04-29</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>372356</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème / 4ème SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROUSSEAU, TAPIE</AVOCATS>
<RAPPORTEUR>M. Lionel Collet</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:372356.20150429</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B...A...a demandé au tribunal administratif de Châlons-en-Champagne d'annuler l'arrêté du 1er juin 2010 par lequel le préfet de l'Aube a prononcé la saisie définitive de ses armes et munitions et lui a interdit d'acquérir des armes et munitions des 1ère, 4ème, 5ème, 6ème et 7ème catégories. Par un jugement n° 1101809 du 22 novembre 2012, le tribunal a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 13NC00145 du 1er août 2013, la cour administrative d'appel de Nancy a rejeté l'appel formé contre ce jugement par M.A....<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 24 septembre et 24 décembre 2013 au secrétariat du contentieux du Conseil d'Etat, M. A...demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de la défense ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Lionel Collet, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Rousseau, Tapie, avocat de M. A... ;<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'aux termes de l'article L. 2336-4 du code de la défense en vigueur à la date des décisions litigieuses, dont les dispositions ont ultérieurement été reprises aux articles L. 312-7 et suivants du code de la sécurité intérieure : " I. Si le comportement ou l'état de santé d'une personne détentrice d'armes et de munitions présente un danger grave pour elle-même ou pour autrui, le préfet peut lui ordonner, sans formalité préalable ni procédure contradictoire, de les remettre à l'autorité administrative, quelle que soit leur catégorie. / II. L'arme et les munitions faisant l'objet de la décision prévue au I doivent être remises immédiatement par le détenteur ou, le cas échéant, par un membre de sa famille ou par une personne susceptible d'agir dans son intérêt, aux services de police ou de gendarmerie. Le commissaire de police ou le commandant de la brigade de gendarmerie peut procéder, sur autorisation du juge des libertés et de la détention, à la saisie de l'arme et des munitions entre 6 heures et 22 heures au domicile du détenteur. / III. La conservation de l'arme et des munitions remises ou saisies est confiée pendant une durée maximale d'un an aux services de la police nationale ou de la gendarmerie nationale territorialement compétents. (...) / Durant cette période, le préfet décide, après que la personne intéressée a été mise à même de présenter ses observations, soit la restitution de l'arme et des munitions, soit la saisie définitive de celles-ci. / IV. - Il est interdit aux personnes dont l'arme et les munitions ont été saisies en application du I ou du III d'acquérir ou de détenir des armes et des munitions, quelle que soit leur catégorie. / Le préfet peut cependant décider de limiter cette interdiction à certaines catégories ou à certains types d'armes. / (...) " ;<br/>
<br/>
              2. Considérant qu'il incombe au juge de l'excès de pouvoir d'exercer un entier contrôle sur les décisions prises par l'autorité préfectorale en application de ces dispositions législatives ; <br/>
<br/>
              3. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond qu'après avoir ordonné, le 6 février 2009, la saisie provisoire d'armes de 5ème catégorie et de munitions des 5ème et 7ème catégories appartenant à M.A..., le préfet de l'Aube a, par un arrêté du 1er juin 2010 pris en application de l'article L. 2336-4 du code de la défense, d'une part, prononcé la saisie définitive et la vente aux enchères de ces armes et munitions et, d'autre part, interdit à M. A...d'acquérir ou de détenir des armes et munitions relevant des 1ère, 4ème, 5ème, 6ème et 7ème catégories ; que pour confirmer le rejet, par le jugement du tribunal administratif de Châlons-en-Champagne du 22 novembre 2012, de la demande de M. A...tendant à l'annulation de l'arrêté du 1er juin 2010, la cour administrative d'appel de Nancy s'est bornée, par l'arrêt attaqué du 1er août 2013, à rechercher si le préfet s'était livré, en prenant la décision litigieuse, à une appréciation manifestement erronée des faits retenus à l'encontre de M.A... ; qu'il résulte de ce qui a été dit au point 2 qu'elle a, ce faisant,  commis une erreur de droit ; que, dès lors, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, son arrêt doit être annulé ;<br/>
<br/>
              4. Considérant qu'il y a lieu, dans les circonstances de  l'espèce, de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              5. Considérant que M. A...n'a contesté en première instance que la légalité interne de l'arrêté attaqué ; que s'il soulève pour la première fois en appel un moyen tiré de l'irrégularité de la procédure suivie par l'administration, ce moyen, qui se rattache à une cause juridique distincte de celle qui fondait la demande de première instance, revêt le caractère d'une demande nouvelle en appel ; qu'il est, dès lors, irrecevable ; <br/>
<br/>
              6. Considérant qu'il ressort des pièces du dossier que M. A...a eu de manière répétée, au cours des dix-huit mois précédant l'arrêté litigieux, un comportement violent, qu'il a été condamné à trois mois de prison ferme du chef de " violences avec usage ou menace d'une arme " pour des faits commis le 17 novembre 2008 et qu'il a été mis en cause dans divers signalements et procédures se rapportant notamment à des faits de violences ou de menaces de mort ainsi qu'au port d'une carabine, alors que la détention d'une telle arme lui était interdite ; que les certificats médicaux des 27 août et 20 septembre 2012, produits par l'intéressé, sont postérieurs de plus de deux ans à l'arrêté critiqué ; que, dès lors, en estimant que la détention d'armes par l'intéressé présentait, eu égard à son comportement, un danger grave pour autrui, au sens des dispositions précitées de l'article L. 2336-4 du code de la défense, le préfet de l'Aube n'a pas commis d'erreur d'appréciation ; que, pour ce motif, il a pu légalement prononcer la saisie définitive des armes et munitions appartenant à M.A..., en lui interdisant d'acquérir ou de détenir des armes des armes et munitions des 1ère, 4ème, 5ème, 6ème et 7ème catégories ;<br/>
<br/>
              7. Considérant qu'il résulte de ce qui précède que M. A...n'est pas fondé à se plaindre de ce que, par le jugement attaqué, le tribunal administratif de Châlons-en-Champagne a rejeté sa demande tendant à l'annulation de l'arrêté du préfet de l'Aube du 1er juin 2010 ; <br/>
<br/>
              8. Considérant que les dispositions de l'article L 761-1 du code de justice administrative font obstacle à ce que la somme qui est demandée à ce titre par M. A...soit mise à la charge de l'Etat, qui n'est pas la partie perdante dans la présente instance ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Nancy du 1er août 2013 est annulé.<br/>
<br/>
Article 2 : La requête d'appel de M. A...et le surplus des conclusions de son pourvoi sont rejetés.<br/>
<br/>
Article 3 : La présente décision sera notifiée à M. B...A...et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">49-05-05 POLICE. POLICES SPÉCIALES. POLICE DU PORT ET DE LA DÉTENTION D'ARMES. - INJONCTION DE REMISE DES ARMES ET INTERDICTION DE DÉTENTION D'ARMES - CONTRÔLE DU JUGE DE L'EXCÈS DE POUVOIR - CONTRÔLE ENTIER [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-07-02-03 PROCÉDURE. POUVOIRS ET DEVOIRS DU JUGE. CONTRÔLE DU JUGE DE L'EXCÈS DE POUVOIR. APPRÉCIATIONS SOUMISES À UN CONTRÔLE NORMAL. - POLICE DES ARMES - INJONCTION DE REMISE DES ARMES ET INTERDICTION DE DÉTENTION D'ARMES [RJ1].
</SCT>
<ANA ID="9A"> 49-05-05 Régime de police des armes institué par l'article L. 2336-4 du code de la défense, repris aux articles L. 312-7 et suivants du code de la sécurité intérieure, permettant au préfet d'ordonner la remise des armes et d'interdire à une personne d'acquérir ou de détenir certaines armes. Le juge de l'excès de pouvoir exerce un entier contrôle sur les décisions prises par l'autorité préfectorale en application de ces dispositions.</ANA>
<ANA ID="9B"> 54-07-02-03 Régime de police des armes institué par l'article L. 2336-4 du code de la défense, repris aux articles L. 312-7 et suivants du code de la sécurité intérieure, permettant au préfet d'ordonner la remise des armes et d'interdire à une personne d'acquérir ou de détenir certaines armes. Le juge de l'excès de pouvoir exerce un entier contrôle sur les décisions prises par l'autorité préfectorale en application de ces dispositions.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., s'agissant du refus d'autorisation de commerce de matériels de guerre, CE, 3 mars 2010, Ministre de la défense, n° 318716, T. pp. 667-925-928.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
