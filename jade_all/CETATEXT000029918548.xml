<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029918548</ID>
<ANCIEN_ID>JG_L_2014_12_000000369035</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/91/85/CETATEXT000029918548.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème / 1ère SSR, 17/12/2014, 369035</TITRE>
<DATE_DEC>2014-12-17</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>369035</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème / 1ère SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BARTHELEMY, MATUCHANSKY, VEXLIARD, POUPOT</AVOCATS>
<RAPPORTEUR>Mme Sophie Roussel</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Xavier De Lesquen</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:369035.20141217</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 4 juin et 4 septembre 2013 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. B... A..., demeurant ... ; M. A... demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler la décision n° 2012-10 D du 4 avril 2013 par laquelle le Haut conseil du commissariat aux comptes a rejeté son recours en révision contre la décision rendue le 22 mai 2008 par la même juridiction et prononçant à son encontre la sanction disciplinaire d'interdiction d'exercice temporaire pour une durée d'un an ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 6 000 euros en application de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ; <br/>
<br/>
              Vu le code de commerce ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Sophie Roussel, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Xavier de Lesquen, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Barthélemy, Matuchansky, Vexliard, Poupot, avocat de M. A...;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'en vertu d'une règle générale de procédure découlant des exigences de la bonne administration de la justice, la voie du recours en révision est ouverte devant les juridictions administratives qui ne relèvent pas du code de justice administrative et pour lesquelles aucun texte n'a prévu un tel recours ; que le recours en révision peut alors être formé à l'égard d'une décision passée en force de chose jugée, dans l'hypothèse où cette décision l'a été sur pièces fausses ou si elle l'a été faute pour la partie perdante d'avoir produit une pièce décisive qui était retenue par son adversaire ; que cette possibilité est ouverte à toute partie à l'instance, dans un délai de deux mois courant à compter du jour où la partie a eu connaissance de la cause de révision qu'elle invoque ;<br/>
<br/>
              2. Considérant, en premier lieu, que si l'un des membres de la formation disciplinaire du Haut conseil du commissariat aux comptes ayant délibéré sur la décision attaquée du 4 avril 2013 était, à cette même date, directeur du contrôle de gestion de la société Cap Gemini, et si le cabinet dont M. A...était l'un des associés était, jusqu'en 2001, l'un des commissaires aux comptes de cette société, ce seul fait ne permet pas, par lui-même, de caractériser une violation du principe d'impartialité, eu égard au délai écoulé entre 2001 et 2013 et à l'absence de circonstance particulière alléguée ; <br/>
<br/>
              3. Considérant, en deuxième lieu, que pour juger que la lettre du 16 juillet 2012 du président de la compagnie régionale des commissaires aux comptes de Paris, invoquée par M. A... à l'appui de son recours en révision, ne pouvait être regardée comme une pièce retenue par un adversaire au sens des règles rappelées ci-dessus, le Haut conseil du commissariat aux comptes s'est fondé sur le fait que celle-ci avait été produite dans le cadre d'une sommation interpellative à l'initiative de M. A...; qu'en statuant ainsi, le Haut conseil du commissariat aux comptes, qui n'était pas tenu de répondre à l'argument tiré de ce que les termes de ce courrier pouvaient laisser penser que la compagnie régionale des commissaires aux comptes de Paris était en possession d'informations qu'elle retenait à dessein, n'a pas méconnu l'exigence de motivation de ses décisions, rappelée par l'article R. 822-50 du code de commerce ; <br/>
<br/>
              4. Considérant, en troisième lieu, qu'il ressort des termes mêmes de la décision attaquée que les autres motifs par lesquels le Haut conseil du commissariat aux comptes a estimé que la lettre du 12 juillet 2012 ne pouvait être regardée comme une pièce décisive retenue par un adversaire présentent un caractère surabondant ; que par suite, les moyens dirigés contre ces motifs sont inopérants ; <br/>
<br/>
              5. Considérant, en dernier lieu, qu'à l'appui de son recours en révision, M. A... soutenait également que la décision du 22 mai 2008 du Haut conseil au commissariat aux comptes avait été rendue sur pièces fausses, se prévalant à cet égard d'une télécopie du 11 décembre 2006 du syndic de la compagnie régionale des commissaires aux comptes de Paris ainsi que de la lettre du 12 juillet 2012, déjà mentionnée, du président de cette même compagnie, de nature selon lui à établir que la situation de perte d'indépendance vis-à-vis de la société Ixcore, objet des poursuites disciplinaires engagées à son encontre, n'avait jamais existé ; <br/>
<br/>
              6. Considérant, toutefois, que ne constitue une pièce fausse susceptible de fonder un recours en révision qu'un document délibérément falsifié ; qu'en se fondant sur la circonstance qu'à la supposer établie, il n'était pas démontré que cette omission eût été faite délibérément avec l'intention de nuire, le Haut conseil du commissariat aux comptes n'a pas commis d'erreur de droit, ni méconnu le droit d'accès à un juge dont se prévaut le requérant ; qu'en estimant, en outre, qu'il n'était pas établi que des informations eussent été omises par le syndic de la compagnie régionale des commissaires aux comptes de Paris et par le président de cette même compagnie dans les deux documents en cause, le Haut conseil du commissariat aux comptes a porté sur les pièces qui lui étaient soumise une appréciation souveraine, exempte de dénaturation ; <br/>
<br/>
              7. Considérant qu'il résulte de tout ce qui précède M. A...n'est pas fondé à demander l'annulation de la décision attaquée ; que ses conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative ne peuvent, par suite, qu'être rejetées ; <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de M. A...est rejeté.<br/>
<br/>
      Article 2 : La présente décision sera notifiée à M. B...A....<br/>
Copie en sera adressée pour information au Haut conseil du commissariat aux comptes.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-08-06 PROCÉDURE. VOIES DE RECOURS. RECOURS EN RÉVISION. - CONDITION TENANT À CE QUE LA DÉCISION AIT ÉTÉ RENDUE SUR UNE PIÈCE FAUSSE - NOTION DE PIÈCE FAUSSE - DOCUMENT DÉLIBÉRÉMENT FALSIFIÉ [RJ1].
</SCT>
<ANA ID="9A"> 54-08-06 Ne constitue une pièce fausse susceptible de fonder un recours en révision qu'un document délibérément falsifié.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1]Cf. CE, 11 janvier 1961, Sieur David, n° 44104, p. 2.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
