<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042612685</ID>
<ANCIEN_ID>JG_L_2020_12_000000426564</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/61/26/CETATEXT000042612685.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème chambre, 03/12/2020, 426564, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-12-03</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>426564</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Laurent Roulaud</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Alexandre Lallet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2020:426564.20201203</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu les procédures suivantes :<br/>
<br/>
              Par une requête et trois mémoires enregistrés le 21 décembre 2018, le 2 mai 2019, les 26 février et 4 juillet 2020 au secrétariat du contentieux du Conseil d'Etat, l'association La Cimade demande au Conseil d'Etat :<br/>
<br/>
              1°) avant-dire droit, d'enjoindre au directeur général de l'Office français de l'immigration et de l'intégration (OFII) de communiquer son instruction diffusée le 29 août 2018 et relative au rétablissement des conditions d'accueil pour les demandeurs d'asile qui ont été regardés comme en fuite et dont la demande est requalifiée ;<br/>
<br/>
              2°) d'annuler cette instruction ;<br/>
<br/>
              3°) d'enjoindre au directeur général de l'OFII de prescrire à ses services une instruction conforme aux objectifs du droit européen ;<br/>
<br/>
              4°) de saisir le Cour de justice de l'Union européenne, en application de l'article 267 du Traité sur le fonctionnement de l'Union européenne et des articles 105 et suivants du règlement de procédure de la Cour, d'une question préjudicielle aux fins de faire préciser si l'article 1er de la charte des droits fondamentaux de l'Union européenne et la directive 2013/33/UE du Parlement européen et du Conseil du 26 juin 2013 permettent d'une part, de suspendre ou de retirer la totalité des conditions matérielles d'accueil au demandeur d'asile qui abandonne le lieu de résidence fixé par l'autorité compétente sans en avoir informé ladite autorité ou qui ne s'est pas rendue à une convocation relative à l'examen de sa demande d'asile et, d'autre part, de conditionner le rétablissement des conditions matérielles d'accueil à une demande préalable du demandeur d'asile et à l'appréciation de la situation particulière du demandeur à la date de la demande de rétablissement au regard notamment de sa vulnérabilité, de ses besoins en matière d'accueil ainsi que, le cas échéant, des raisons pour lesquelles il n'a pas respecté les obligations auxquelles il avait consenti au moment de l'acceptation initiale des conditions matérielles d'accueil ;<br/>
<br/>
              5°) de mettre à la charge de l'OFII le versement d'une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le règlement (UE) n° 604/2013 du Parlement européen et du Conseil du 26 juin 2013 ;<br/>
              - la directive (UE) n° 2013/33/UE du Parlement européen et du Conseil du 26 juin 2013 ;<br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Laurent Roulaud, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Alexandre Lallet, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. L'association La Cimade demande, sans la produire, l'annulation pour excès de pouvoir d'une instruction du directeur général de l'Office français de l'immigration et de l'intégration (OFII) qui aurait été diffusée à ses services le 29 août 2018 et qui prescrirait, d'une part, de notifier une lettre d'intention de suspension des conditions matérielles d'accueil à tout demandeur d'asile qui a été déclaré en fuite en application de l'article 29 du règlement du 26 juin 2013 dit " Dublin III " et, d'autre part, de conditionner le rétablissement des conditions matérielles d'accueil ainsi suspendues à une demande préalable de la part du demandeur d'asile et au constat de sa vulnérabilité au sens de l'article L. 744-6 du code de l'entrée et du séjour des étrangers et du droit d'asile. <br/>
<br/>
              2. Aux termes de l'article R. 412-1 du code de justice administrative, dans sa rédaction alors applicable : " La requête doit, à peine d'irrecevabilité, être accompagnée, sauf impossibilité justifiée, de la décision attaquée [...] ". Il résulte de ces dispositions qu'une requête est irrecevable et doit être rejetée comme telle lorsque son auteur n'a pas, en dépit d'une invitation à régulariser, produit la décision attaquée ou, en cas d'impossibilité, tout document justifiant des diligences qu'il a accomplies pour en obtenir la communication.<br/>
<br/>
<br/>
              3. Il ne ressort pas des pièces du dossier que l'association requérante, qui se borne à soutenir que l'instruction qu'elle attaque, et dont l'existence est contestée par l'OFII, serait notamment révélée par des éléments statistiques relatifs au nombre de suspensions des conditions matérielles d'accueil prononcées par l'OFII et par les termes d'ordonnances de juges des référés de tribunaux administratifs, aurait accompli les diligences qu'il lui appartenait d'effectuer auprès de l'OFII afin de se procurer, si celle-ci existait, la décision attaquée. Par suite, il y a lieu de rejeter sa requête comme manifestement irrecevable, faute pour elle d'avoir produit l'instruction contestée, y compris les conclusions tendant à ce qu'une question préjudicielle soit posée à la Cour de justice de l'Union européenne et celles présentées au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de l'association La Cimade est rejetée. <br/>
Article 2 : La présente décision sera notifiée à l'association La Cimade et à l'Office français de l'immigration et de l'intégration.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
