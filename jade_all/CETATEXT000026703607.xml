<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026703607</ID>
<ANCIEN_ID>JG_L_2012_11_000000358537</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/70/36/CETATEXT000026703607.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère sous-section jugeant seule, 29/11/2012, 358537, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2012-11-29</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>358537</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère sous-section jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>JACOUPY ; SCP MONOD, COLIN</AVOCATS>
<RAPPORTEUR>Mme Julia Beurton</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Maud Vialettes</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2012:358537.20121129</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 13 et 30 avril 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour la commune de Valbonne, représentée par son maire ; la commune de Valbonne demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'ordonnance n° 1200719 du 29 mars 2012 par laquelle le juge des référés du tribunal administratif de Nice, statuant sur le fondement de l'article L. 521-1 du code de justice administrative, a, à la demande de M. Jean-Luc A, suspendu l'exécution de l'arrêté du 9 septembre 2011 par lequel le maire de Valbonne a accordé un permis de construire à la SCCV Vallis Bona l'autorisant à édifier neuf bâtiments de soixante-trois logements ; <br/>
<br/>
              2°) statuant en référé, de rejeter la demande de M. A ; <br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
              Vu le code de l'urbanisme ;<br/>
	Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Julia Beurton, Auditeur,  <br/>
<br/>
              - les observations de la SCP Monod, Colin, avocat de la commune de Valbonne et de Me Jacoupy, avocat de M. A,<br/>
<br/>
              - les conclusions de Mme Maud Vialettes, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Monod, Colin, avocat de la commune de Valbonne ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision. " ;<br/>
<br/>
              2. Considérant que, par une ordonnance du 29 mars 2012, le juge des référés du tribunal administratif de Nice a, à la demande de M. A, ordonné la suspension de l'exécution du permis de construire accordé le 9 septembre 2011 par le maire de Valbonne à la SCCV Vallis Bona ; que la commune de Valbonne se pourvoit en cassation contre cette ordonnance ;<br/>
<br/>
              3. Considérant qu'il ressort des pièces du dossier soumis au juge des référés que les parcelles d'assiette de la construction litigieuse, classées en zone urbaine (UBd) à la suite d'une révision du plan local d'urbanisme de la commune de Valbonne du 12 décembre 2006, étaient, en vertu du plan d'occupation des sols précédemment en vigueur, classées en zone naturelle d'urbanisation future (NAf) ; que, dès lors, en jugeant qu'était, en l'état de l'instruction, propre à créer un doute sérieux quant à la légalité du permis de construire attaqué le moyen tiré de l'illégalité de ce classement en zone Ubd d'un terrain d'assiette situé antérieurement en zone naturelle N, le juge des référés a entaché son ordonnance d'une inexactitude matérielle de nature à en justifier l'annulation ; qu'il suit de là que, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, la commune de Valbonne est fondée à demander l'annulation de l'ordonnance attaquée ; <br/>
<br/>
              4. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de statuer sur la demande de suspension en application des dispositions de l'article L. 821-2 du code de justice administrative ; <br/>
<br/>
              5. Considérant qu'à l'appui de sa demande de suspension, M. A soutient que le dossier de demande de permis de construire attaqué est entaché de plusieurs erreurs et lacunes, en ce qu'il ne comprend pas toutes les pièces obligatoires, que la notice architecturale ne précise pas suffisamment le traitement apporté à l'oliveraie qui existe sur le terrain d'assiette, que le projet architectural exigé par l'article R. 431-10 du code de l'urbanisme est incomplet, que les plans joints comportent des lacunes, que la superficie hors oeuvre nette totale calculée est erronée, que les deux plans d'accès pompiers joints présentent des tracés différents ; que le permis de construire attaqué est entaché d'illégalité par voie d'exception de l'illégalité du classement des parcelles d'assiette de la construction litigieuse en zone UBd, lequel, d'une part, méconnaît la protection dont celles-ci bénéficient au titre de l'espace boisé classé et de la zone naturelle forestière à laquelle elles appartiennent, d'autre part, est intervenu alors que les parcelles auraient dû être maintenues en zone naturelle ; que ce classement est également contraire aux dispositions du projet d'aménagement et de développement durables et du schéma de cohérence territoriale de la commune de Valbonne ; que le permis attaqué est entaché d'une erreur manifeste d'appréciation du fait de l'atteinte grave qu'il cause à l'équilibre biologique de l'ensemble forestier de Valbonne ; qu'il méconnaît les dispositions de l'article R. 111-5 du code de l'urbanisme, ainsi que celles du plan de prévention des risques d'incendie de forêt de la commune de Valbonne ; <br/>
<br/>
              6. Considérant qu'aucun de ces moyens n'est, en l'état de l'instruction, de nature à créer un doute sérieux quant à la légalité du permis de construire attaqué ; que la demande de suspension doit dès lors, et sans qu'il soit besoin de statuer sur la fin de non-recevoir opposée par la commune de Valbonne, être rejetée ;<br/>
<br/>
              7. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de la commune de Valbonne et de la SCCV Vallis Bona, qui ne sont pas, dans la présente instance, les parties perdantes ; qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par la commune de Valbonne et la SCCV Vallis Bona au même titre ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'ordonnance du 29 mars 2012 du juge des référés du tribunal administratif de Nice est annulée.<br/>
Article 2 : La demande de suspension présentée par M. A devant le juge des référés du tribunal administratif de Nice ainsi que ses conclusions présentées devant le Conseil d'Etat au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : Les conclusions de la commune de Valbonne et de la SCCV Vallis Bona présentées sur le fondement de l'article L.761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée à la commune de Valbonne, à la SCCV Vallis Bona et à M. Jean-Luc A.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
