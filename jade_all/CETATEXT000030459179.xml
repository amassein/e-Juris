<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030459179</ID>
<ANCIEN_ID>JG_L_2015_04_000000388213</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/45/91/CETATEXT000030459179.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème SSJS, 03/04/2015, 388213, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-04-03</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>388213</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème SSJS</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Clémence Olsina</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Xavier de Lesquen</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2015:388213.20150403</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu les mémoires, enregistrés le 23 février 2015 au secrétariat du contentieux du Conseil d'Etat, présentés, en application de l'article 23-5 de l'ordonnance n° 58-1067 du 7 novembre 1958, pour la société UBER FRANCE, dont le siège est 11, rue de Cambrai à Paris (75019), et la société UBER BV, dont le siège est 68-78 Vijezlstraat à Amsterdam (Pays-Bas) ; les sociétés requérantes demandent au Conseil d'Etat, à l'appui de leur requête tendant à l'annulation pour excès de pouvoir du décret n° 2014-1725 du 30 décembre 2014 relatif au transport public particulier de personnes, de renvoyer au Conseil constitutionnel la question de la conformité aux droits et libertés garantis par la Constitution du III de l'article L. 3120-2 et des articles L. 3122-2 et L. 3122-9 du code des transports ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la Constitution, notamment son Préambule et son article 61-1 ;<br/>
<br/>
              Vu l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
<br/>
              Vu le code des transports, notamment ses articles L. 3120-2, L. 3122-2 et L. 3122-9 ;<br/>
<br/>
              Vu le code de justice administrative, notamment son article R. 771-18 ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Clémence Olsina, auditeur,  <br/>
<br/>
              - les conclusions de M. Xavier de Lesquen, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes du premier alinéa de l'article 23-5 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel : " Le moyen tiré de ce qu'une disposition législative porte atteinte aux droits et libertés garantis par la Constitution peut être soulevé (...) à l'occasion d'une instance devant le Conseil d'Etat (...) " ; qu'il résulte des dispositions de ce même article que le Conseil constitutionnel est saisi de la question prioritaire de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux ;<br/>
<br/>
              2. Considérant qu'aux termes du III de l'article L. 3120-2 du code des transports : " III. - Sont interdits aux personnes réalisant des prestations mentionnées à l'article L. 3120-1 et aux intermédiaires auxquels elles ont recours : / 1° Le fait d'informer un client, avant la réservation mentionnée au 1° du II du présent article, quel que soit le moyen utilisé, à la fois de la localisation et de la disponibilité d'un véhicule mentionné au I quand il est situé sur la voie ouverte à la circulation publique sans que son propriétaire ou son exploitant soit titulaire d'une autorisation de stationnement mentionnée à l'article L. 3121-1 ; / 2° Le démarchage d'un client en vue de sa prise en charge dans les conditions mentionnées au 1° du II du présent article ; / 3° Le fait de proposer à la vente ou de promouvoir une offre de prise en charge effectuée dans les conditions mentionnées au même 1°. " ; qu'aux termes de l'article L. 3122-2 du même code : " Les conditions mentionnées à l'article L. 3122-1 incluent le prix total de la prestation, qui est déterminé lors de la réservation préalable mentionnée au 1° du II de l'article L. 3120-2. Toutefois, s'il est calculé uniquement en fonction de la durée de la prestation, le prix peut être, en tout ou partie, déterminé après la réalisation de cette prestation, dans le respect de l'article L. 113-3-1 du code de la consommation. " ; qu'aux termes de l'article L. 3122-9 du même code : " Dès l'achèvement de la prestation commandée au moyen d'une réservation préalable, le conducteur d'une voiture de transport avec chauffeur dans l'exercice de ses missions est tenu de retourner au lieu d'établissement de l'exploitant de cette voiture ou dans un lieu, hors de la chaussée, où le stationnement est autorisé, sauf s'il justifie d'une réservation préalable ou d'un contrat avec le client final. " ; <br/>
<br/>
              3. Considérant que les dispositions du III de l'article L. 3120-2 et des articles L. 3122-2 et L. 3122-9 du code des transports sont applicables au litige ; qu'elles n'ont pas été déclarées conformes à la Constitution par le Conseil constitutionnel ; que la question de savoir si elles portent atteinte aux droits et libertés garantis par la Constitution, notamment à la liberté d'entreprendre et au principe d'égalité, présente un caractère sérieux ; qu'ainsi, il y a lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité invoquée ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La question de la conformité à la Constitution du III de l'article L. 3120-2 et des articles L. 3122-2 et L. 3122-9 du code des transports est renvoyée au Conseil constitutionnel. <br/>
Article 2 : Il est sursis à statuer sur la requête des sociétés UBER France et UBER BV jusqu'à ce que le Conseil constitutionnel ait tranché la question de constitutionnalité ainsi soulevée. <br/>
<br/>
Article 3 : La présente décision sera notifiée aux sociétés UBER France et UBER BV.<br/>
Copie en sera adressée, pour information, au Premier ministre, au ministre de l'intérieur et au ministre de l'économie, du redressement productif et du numérique.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
