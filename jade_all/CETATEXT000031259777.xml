<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031259777</ID>
<ANCIEN_ID>JG_L_2015_10_000000372030</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/25/97/CETATEXT000031259777.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème / 1ère SSR, 01/10/2015, 372030, Publié au recueil Lebon</TITRE>
<DATE_DEC>2015-10-01</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>372030</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème / 1ère SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FOUSSARD, FROGER ; SCP SEVAUX, MATHONNET</AVOCATS>
<RAPPORTEUR>M. Cyrille Beaufils</RAPPORTEUR>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CESSR:2015:372030.20151001</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A...B...a demandé au tribunal administratif de Versailles, d'une part, d'annuler pour excès de pouvoir la délibération du 26 janvier 2009 du conseil municipal d'Orgerus autorisant le maire à signer un nouvel arrêté d'attribution d'un logement de fonction pour utilité de service du 1er janvier 2006 au 9 janvier 2007 pour une redevance de 450 euros hors charges, l'arrêté du 10 février 2009 du maire d'Orgerus lui attribuant ce logement de fonction et les titres exécutoires des 10 février 2009 et 10 juin 2010 et, d'autre part, de le décharger du paiement des sommes correspondantes. Par un jugement nos 0903188, 0903974, 0903973, 1005604 du 5 juillet 2013, le tribunal administratif de Versailles a fait droit à sa demande.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 9 septembre et 6 décembre 2013 et le 30 août 2015 au secrétariat du contentieux du Conseil d'Etat, la commune d'Orgerus demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter les demandes de M. B...;<br/>
<br/>
              3°) de mettre à la charge de M. B...la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative et de le condamner aux entiers dépens, y compris les frais de timbre fiscal.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code général des collectivités territoriales ;<br/>
              - le code général de la propriété des personnes publiques ;<br/>
              - la loi n° 84-16 du 26 janvier 1984 ;<br/>
              - la loi n° 90-1067 du 28 novembre 1990, notamment son article 21 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Cyrille Beaufils, auditeur,  <br/>
<br/>
              - les conclusions de M. Xavier de Lesquen, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Foussard, Froger, avocat de la commune d'Orgerus et à la SCP Sevaux, Mathonnet, avocat de M. B... ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis au juge du fond que M.B..., agent de maîtrise et responsable des services techniques de la commune d'Orgerus, s'est vu attribuer par un arrêté du 28 octobre 1998 un logement par nécessité absolue de service ; qu'après avoir, par une délibération du 19 juillet 2005, décidé que ce logement ne serait plus attribué que par utilité de service moyennant une redevance, le conseil municipal en a fixé le montant mensuel à 450 euros hors charges par une délibération du 26 janvier 2009 ; que M. B...a saisi le tribunal administratif de Versailles de recours contre cette délibération, ainsi que contre l'arrêté du maire du 10 février lui attribuant ce logement par utilité de service moyennant ce montant de redevance et contre deux titres exécutoires mettant à sa charge les sommes dues au titre de la période de janvier 2006 à janvier 2007 ; que, par le jugement attaqué du 5 juillet 2013, le tribunal administratif de Versailles a fait droit à ces demandes ;<br/>
<br/>
              2. Considérant, en premier lieu, que contrairement à ce que soutient la commune, la minute du jugement qu'elle attaque comporte les signatures requises ;<br/>
<br/>
              3. Considérant, en deuxième lieu, qu'aux termes de l'article 21 de la loi du 28 novembre 1990 relative à la fonction publique territoriale et portant modification de certains articles du code des communes : " Les organes délibérants des collectivités territoriales et de leurs établissements publics fixent la liste des emplois pour lesquels un logement de fonction peut être attribué gratuitement ou moyennant une redevance par la collectivité ou l'établissement public concerné, en raison notamment des contraintes liées à l'exercice de ces emplois. / (...) La délibération précise les avantages accessoires liés à l'usage du logement. / Les décisions individuelles sont prises en application de cette délibération par l'autorité territoriale ayant le pouvoir de nomination (...) " ; que ces dispositions confèrent aux collectivités territoriales et à leurs établissements publics compétence pour déterminer, dans le respect des critères fixés par la loi, les emplois auxquels peut être attachée l'attribution d'un logement de fonction et l'étendue de l'avantage ainsi accordé ; que ces délibérations ne revêtant pas le caractère de simples actes préparatoires à la décision individuelle attribuant le logement, prise par l'autorité territoriale ayant le pouvoir de nomination, les bénéficiaires de ces logements sont recevables à les contester, notamment en tant qu'elles prévoient d'assortir cette attribution du paiement d'une redevance ; qu'il suit de là que M. B...était recevable à contester la délibération du conseil municipal d'Orgerus du 26 janvier 2009 ; qu'ainsi, le tribunal administratif de Versailles n'a pas commis d'erreur de droit en écartant la fin de non-recevoir opposée par la commune à ces conclusions ;<br/>
<br/>
              4. Considérant, en troisième lieu, que, sous réserve du principe de parité énoncé à l'article 88 de la loi du 26 janvier 1984 portant dispositions statutaires relatives à la fonction publique territoriale, il appartient à l'autorité municipale de fixer le montant de la redevance d'occupation d'un logement de fonction concédé par utilité de service à l'un de ses agents en tenant compte des caractéristiques du bien, des valeurs locatives constatées pour des logements comparables situés dans le même secteur géographique et des conditions particulières de l'occupation du logement, notamment des sujétions éventuellement imposées à l'agent ; que l'appréciation ainsi portée ne peut être censurée par le juge de l'excès de pouvoir que dans le cas où elle est entachée d'une erreur manifeste ; <br/>
<br/>
              5. Considérant que le tribunal administratif a, par un jugement suffisamment motivé, estimé que le conseil municipal avait commis une erreur manifeste d'appréciation en retenant un montant de redevance situé dans la fourchette des loyers pratiqués pour des logements de composition et de superficie analogues, alors que la prise en compte des nuisances liées aux caractéristiques propres de l'appartement, situé au-dessus des ateliers municipaux et à proximité immédiate d'un terrain occupé sans autorisation ni aménagements par des gens du voyage, aurait dû le conduire à minorer ce montant par rapport aux loyers de biens comparables exempts de telles nuisances ; qu'en statuant ainsi, le tribunal n'a pas commis d'erreur de droit et s'est livré à une appréciation souveraine des faits de l'espèce qui, dès lors qu'elle est exempte de dénaturation, ne saurait être discutée devant le juge de cassation ; qu'il n'a pas non plus commis d'erreur de droit en annulant, par voie de conséquence, l'arrêté du 10 février 2009 ainsi que les deux titres exécutoires des 10 février 2009 et 10 juin 2010 pris sur le fondement de cette délibération ; que, par suite, la requérante n'est pas fondée à demander l'annulation du jugement qu'elle attaque ;<br/>
<br/>
              6. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de M. B...qui n'est pas, dans la présente instance, la partie perdante ; qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la commune d'Orgerus la somme de 3 000 euros à verser à M. B...au même titre et de laisser à sa charge la contribution pour l'aide juridique ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de la commune d'Orgerus est rejeté.<br/>
<br/>
Article 2 : La commune d'Orgerus versera à M. A...B...la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 3 : La présente décision sera notifiée à la commune d'Orgerus et à M. A...B.... <br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">36-07-01-03 FONCTIONNAIRES ET AGENTS PUBLICS. STATUTS, DROITS, OBLIGATIONS ET GARANTIES. STATUT GÉNÉRAL DES FONCTIONNAIRES DE L'ÉTAT ET DES COLLECTIVITÉS LOCALES. DISPOSITIONS STATUTAIRES RELATIVES À LA FONCTION PUBLIQUE TERRITORIALE (LOI DU 26 JANVIER 1984). - ATTRIBUTION DE LOGEMENTS DE FONCTION (ART. 21 DE LA LOI DU 28 NOVEMBRE 1990) - FIXATION DU MONTANT DE LA REDEVANCE D'OCCUPATION D'UN LOGEMENT DE FONCTION CONCÉDÉ PAR UTILITÉ DE SERVICE - 1) ELÉMENTS DONT L'AUTORITÉ MUNICIPALE DOIT TENIR COMPTE - CARACTÉRISTIQUES DU BIEN, VALEURS LOCATIVES CONSTATÉES POUR DES LOGEMENTS COMPARABLES SITUÉS DANS LE MÊME SECTEUR GÉOGRAPHIQUE ET CONDITIONS PARTICULIÈRES DE L'OCCUPATION DU LOGEMENT [RJ1] - 2) CONTRÔLE DU JUGE DE L'EXCÈS DE POUVOIR SUR L'APPRÉCIATION PORTÉE PAR L'AUTORITÉ MUNICIPALE - CONTRÔLE RESTREINT.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">36-07-10-03 FONCTIONNAIRES ET AGENTS PUBLICS. STATUTS, DROITS, OBLIGATIONS ET GARANTIES. GARANTIES ET AVANTAGES DIVERS. LOGEMENT DE FONCTION. - ATTRIBUTION DE LOGEMENTS DE FONCTION (ART. 21 DE LA LOI DU 28 NOVEMBRE 1990) - FIXATION DU MONTANT DE LA REDEVANCE D'OCCUPATION D'UN LOGEMENT DE FONCTION CONCÉDÉ PAR UTILITÉ DE SERVICE - 1) ELÉMENTS DONT L'AUTORITÉ MUNICIPALE DOIT TENIR COMPTE - CARACTÉRISTIQUES DU BIEN, VALEURS LOCATIVES CONSTATÉES POUR DES LOGEMENTS COMPARABLES SITUÉS DANS LE MÊME SECTEUR GÉOGRAPHIQUE ET CONDITIONS PARTICULIÈRES DE L'OCCUPATION DU LOGEMENT [RJ1] - 2) CONTRÔLE DU JUGE DE L'EXCÈS DE POUVOIR SUR L'APPRÉCIATION PORTÉE PAR L'AUTORITÉ MUNICIPALE - CONTRÔLE RESTREINT.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">54-07-02-04 PROCÉDURE. POUVOIRS ET DEVOIRS DU JUGE. CONTRÔLE DU JUGE DE L'EXCÈS DE POUVOIR. APPRÉCIATIONS SOUMISES À UN CONTRÔLE RESTREINT. - FIXATION DU MONTANT DE LA REDEVANCE D'OCCUPATION D'UN LOGEMENT DE FONCTION CONCÉDÉ PAR UTILITÉ DE SERVICE.
</SCT>
<ANA ID="9A"> 36-07-01-03 1) Sous réserve du principe de parité énoncé à l'article 88 de la loi du 26 janvier 1984, il appartient à l'autorité municipale de fixer le montant de la redevance d'occupation d'un logement de fonction concédé par utilité de service à l'un de ses agents en tenant compte des caractéristiques du bien, des valeurs locatives constatées pour des logements comparables situés dans le même secteur géographique et des conditions particulières de l'occupation du logement, notamment des sujétions éventuelles imposées à l'agent.,,,2) Le juge de l'excès de pouvoir exerce un contrôle restreint sur l'appréciation portée par l'autorité municipale pour fixer le montant de la redevance d'occupation d'un logement de fonction concédé par utilité de service.</ANA>
<ANA ID="9B"> 36-07-10-03 1) Sous réserve du principe de parité énoncé à l'article 88 de la loi du 26 janvier 1984, il appartient à l'autorité municipale de fixer le montant de la redevance d'occupation d'un logement de fonction concédé par utilité de service à l'un de ses agents en tenant compte des caractéristiques du bien, des valeurs locatives constatées pour des logements comparables situés dans le même secteur géographique et des conditions particulières de l'occupation du logement, notamment des sujétions éventuelles imposées à l'agent.,,,2) Le juge de l'excès de pouvoir exerce un contrôle restreint sur l'appréciation portée par l'autorité municipale pour fixer le montant de la redevance d'occupation d'un logement de fonction concédé par utilité de service.</ANA>
<ANA ID="9C"> 54-07-02-04 Le juge de l'excès de pouvoir exerce un contrôle restreint sur l'appréciation portée par l'autorité municipale pour fixer le montant de la redevance d'occupation d'un logement de fonction concédé par utilité de service.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, Assemblée, 2 décembre 1994, Préfet de la région Nord-Pas-de-Calais, Préfet du Nord, n° 147962, p. 529.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
