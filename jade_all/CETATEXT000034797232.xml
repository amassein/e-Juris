<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034797232</ID>
<ANCIEN_ID>JG_L_2017_05_000000402189</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/79/72/CETATEXT000034797232.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème - 1ère chambres réunies, 19/05/2017, 402189, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-05-19</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>402189</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème - 1ère chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Cyrille Beaufils</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Louis Dutheillet de Lamothe</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:402189.20170519</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête sommaire, un mémoire complémentaire et deux nouveaux mémoires, enregistrés les 5 août et 27 octobre 2016 et les 18 et 25 avril 2017 au secrétariat du contentieux du Conseil d'Etat, la chambre départementale et régionale des huissiers de justice de Paris demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir l'arrêté du 3 juin 2016 du garde des sceaux, ministre de la justice, relatif à la mise en oeuvre par voie électronique de la procédure simplifiée de recouvrement des petites créances ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code civil ;<br/>
              - le code des procédures civiles d'exécution ;<br/>
              - la loi n° 2015-990 du 6 août 2015 ;<br/>
              - l'ordonnance n° 45-2592 du 2 novembre 1945 ;<br/>
              - le décret n° 2005-850 du 27 juillet 2005 ;<br/>
              - le décret n° 2008-689 du 9 juillet 2008 ; <br/>
              - le décret n° 2016-285 du 9 mars 2016 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Cyrille Beaufils, auditeur,  <br/>
<br/>
              - les conclusions de M. Louis Dutheillet de Lamothe, rapporteur public.<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant que l'article 208 de la loi du 6 août 2015 pour la croissance, l'activité et l'égalité des chances économiques a créé l'article 1244-4 du code civil, en vigueur à la date de l'arrêté attaqué et désormais repris à l'article L. 125-1 du code des procédures civiles d'exécution, qui institue la procédure simplifiée de recouvrement des petites créances ; que cette procédure peut être mise en oeuvre par un huissier de justice à la demande du créancier pour le paiement d'une créance ayant une cause contractuelle ou résultant d'une obligation de caractère statutaire et inférieure à un montant défini par décret en Conseil d'Etat, l'accord du créancier et du débiteur sur le montant et les modalités du paiement lui permettant de délivrer, sans autre formalité, un titre exécutoire ; qu'aux termes de l'article 2 du décret du 9 mars 2016 relatif à la procédure simplifiée de recouvrement des petites créances, pris pour l'application de ces dispositions : " Un arrêté du garde des sceaux, ministre de la justice, précise les modalités techniques et les garanties relatives au mode de communication électronique susceptible d'être utilisé par les huissiers de justice pour la mise en oeuvre de la procédure simplifiée de recouvrement des petites créances, dans le cadre d'un système de traitement, de conservation et de transmission de l'information placé sous la responsabilité de la chambre nationale des huissiers de justice " ; que, sur le fondement de ces dispositions, le garde des sceaux, ministre de la justice a, par l'arrêté attaqué, défini les modalités de la mise en oeuvre par voie électronique de la procédure simplifiée de recouvrement des petites créances, au moyen d'un système placé sous la responsabilité de la chambre nationale des huissiers de justice ; <br/>
<br/>
              2. Considérant que la chambre nationale des huissiers de justice justifie d'un intérêt suffisant au maintien de l'arrêté attaqué ; qu'ainsi son intervention est recevable ;<br/>
<br/>
              3. Considérant, en premier lieu, qu'aux termes de l'article 1er du décret du 27 juillet 2005 relatif aux délégations de signature des membres du Gouvernement : " A compter du jour suivant la publication au Journal officiel de la République française de l'acte les nommant dans leurs fonctions ou à compter du jour où cet acte prend effet, si ce jour est postérieur, peuvent signer, au nom du ministre ou du secrétaire d'Etat et par délégation, l'ensemble des actes, à l'exception des décrets, relatifs aux affaires des services placés sous leur autorité : / 1° Les secrétaires généraux des ministères, les directeurs d'administration centrale (...) " ; que l'article 2 du décret du 9 juillet 2008 relatif à l'organisation du ministère de la justice prévoit notamment que le secrétaire général de ce ministère " met en oeuvre la politique de l'informatique et des technologies de l'information et de la communication au sein du ministère " ; qu'eu égard à son objet, limité aux modalités techniques de mise en oeuvre, par voie électronique, de la procédure simplifiée de recouvrement des petites créances, et compte tenu du lien existant entre le système informatique qu'il prévoit et celui utilisé pour les transmissions électroniques effectuées en matière de procédure civile, relevant du ministère de la justice, M. Eric Lucas, secrétaire général du ministère de la justice, avait bien compétence pour signer, au nom du garde des sceaux, ministre de la justice, l'arrêté du 3 juin 2016 attaqué ;<br/>
<br/>
              4. Considérant, en deuxième lieu, que les dispositions de l'article 1244-4 du code civil, qui prévoient qu'une procédure simplifiée de recouvrement des petites créances " peut être mise en oeuvre par un huissier de justice à la demande du créancier ", ne font pas obstacle à ce que le système informatique utilisé en cas de recours à un mode de communication électronique entre l'huissier de justice, le créancier et le débiteur pour les besoins de la procédure qu'elles instituent soit placé sous la responsabilité de la chambre nationale des huissiers de justice ; qu'il s'ensuit qu'en prévoyant cette modalité de mise en oeuvre à son article 2, le décret du 9 mars 2016 précité n'a pas méconnu ces dispositions ; qu'eu égard, notamment, à l'objectif d'unification des procédures dématérialisées auquel ce système centralisé répond et à la nécessité de garantir la fiabilité et la sécurité des échanges réalisés dans ce cadre, le moyen tiré de ce que ce décret porterait une atteinte disproportionnée à la liberté d'entreprendre doit, en tout état de cause, être écarté ;  <br/>
<br/>
              5. Considérant, enfin, qu'il résulte de ce qui a été dit ci-dessus que le principe de la responsabilité de la chambre nationale des huissiers de justice pour l'administration de ce système informatique résulte de l'article 2 du décret du 9 mars 2016 ; que les moyens tirés de ce que l'arrêté attaqué méconnaîtrait, en prévoyant lui-même cette responsabilité, cet article 2 ainsi que l'article 1244-4 du code civil doivent par suite être écartés ; qu'il en va de même, en tout état de cause, du moyen tiré de qu'il porterait, pour ce motif, une atteinte disproportionnée à la liberté d'entreprendre ;<br/>
<br/>
              6. Considérant qu'il résulte de tout ce qui précède, sans qu'il soit besoin de statuer sur la fin de non-recevoir opposée par la chambre nationale des huissiers de justice, que la chambre départementale et régionale des huissiers de justice de Paris n'est pas fondée à demander l'annulation de l'arrêté qu'elle attaque ; que sa requête doit être rejetée, y compris ses conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'intervention de la chambre nationale des huissiers de justice est admise. <br/>
<br/>
Article 2 : La requête de la chambre départementale et régionale des huissiers de justice de Paris est rejetée. <br/>
<br/>
Article 3 : La présente décision sera notifiée à la chambre départementale et régionale des huissiers de justice de Paris, au ministre d'Etat, garde des sceaux, ministre de la justice, et à la chambre nationale des huissiers de justice. <br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
