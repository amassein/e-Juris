<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034487019</ID>
<ANCIEN_ID>JG_L_2017_04_000000409418</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/48/70/CETATEXT000034487019.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 07/04/2017, 409418, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-04-07</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>409418</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2017:409418.20170407</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A...C...et Mme B...D...ont demandé au juge des référés du tribunal administratif de Châlons-en-Champagne, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, d'enjoindre au préfet de la Marne de leur procurer, dans un délai de vingt-quatre heures à compter de la notification de l'ordonnance, un lieu d'hébergement et de leur indiquer un lieu où ils pourront se procurer sans frais nourriture et habillement. Par une ordonnance n° 1700409 du 2 mars 2017, le juge des référés du tribunal administratif de Châlons-en-Champagne a rejeté leur demande. <br/>
<br/>
              Par une requête, enregistrée le 30 mars 2017 au secrétariat du contentieux du Conseil d'Etat, M. C...et Mme D...demandent au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative :<br/>
<br/>
              1°) d'annuler cette ordonnance ; <br/>
<br/>
              2°) de faire droit à sa demande de première instance ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 1 500 euros, au titre des dispositions de l'article 37 de la loi du 10 juillet 1991 et de l'article L. 761-1 du code de justice administrative, sous réserve du renoncement au bénéfice de l'aide juridictionnelle.<br/>
<br/>
<br/>
              Ils soutiennent que : <br/>
              - la condition d'urgence est remplie ; <br/>
              - il est porté une atteinte grave et manifestement illégale au droit d'asile, et, à titre subsidiaire, au principe de dignité humaine et au droit à un hébergement d'urgence, dès lors que, d'une part, le préfet de la Marne n'a pas mis d'hébergement à leur disposition et que, d'autre part, un examen de leur vulnérabilité n'est pas intervenu dans les conditions prévues par la directive 2003/9/CE du Conseil du 27 janvier 2003, le code de l'action sociale et des familles et le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              -  l'ordonnance contestée est entachée d'une erreur de droit dès lors que, d'une part, le juge des référés, n'a pas considéré que la privation des conditions d'accueil constituait une méconnaissance manifeste des exigences qui découlent du droit d'asile, alors qu'une atteinte au droit d'accueil, dont il est le corollaire, a été établie et, d'autre part, a subordonné l'illégalité de l'atteinte au droit d'accueil aux conditions tenant aux " moyens dont dispose l'autorité administrative compétente " ou à la " situation personnelle " du demandeur d'asile, en méconnaissance des dispositions de la directive 2003/9/CE du 27 janvier 2003. <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la directive 2003/9/CE du Conseil du 27 janvier 2003 relative à des normes minimales pour l'accueil des demandeurs d'asile dans les États membres ;<br/>
              - la directive 2013/33/UE du Parlement Européen et du Conseil du 26 juin 2013 établissant des normes pour l'accueil des personnes demandant la protection internationale ; <br/>
              -  le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              -  le code de l'action sociale et des familles ;<br/>
              -  la loi n° 91-647 du 10 juillet 1991 relative à l'aide juridique ;<br/>
              -  le code de justice administrative.<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. Le juge des référés se prononce dans un délai de quarante-huit heures ". En vertu de l'article L. 522-3 du même code, le juge des référés peut rejeter une requête par une ordonnance motivée, sans instruction contradictoire ni audience publique, lorsque la condition d'urgence n'est pas remplie ou lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable ou qu'elle est mal fondée. À cet égard, il appartient au juge d'appel de prendre en considération les éléments recueillis par le juge du premier degré dans le cadre de la procédure écrite et orale qu'il a diligentée.<br/>
<br/>
              2. Il résulte de l'instruction diligentée par le juge des référés du tribunal administratif de Châlons-en-Champagne en première instance que M. C...et Mme D..., ressortissants arméniens, disposent d'une attestation préfectorale du 9 février 2017 qui leur reconnaît la qualité de demandeur d'asile. Ils perçoivent l'allocation de demandeur d'asile et ont accès à l'aide médicale d'Etat mais sont dépourvus de logement. Ils ont saisi le juge des référés du tribunal administratif de Châlons-en-Champagne, sur le fondement de l'article L. 521-2 du code de justice administrative, d'une demande tendant à ce qu'il soit enjoint au préfet de la Marne de leur procurer, dans un délai de vingt-quatre heures à compter de la notification de l'ordonnance à intervenir, un lieu d'hébergement et de leur indiquer un lieu où ils pourront se procurer sans frais nourriture et habillement. Par une ordonnance n° 1700409 du 2 mars 2017, le juge des référés a rejeté leur demande. M. C...et Mme D... relèvent appel de cette ordonnance. <br/>
              3. Si, d'une part, la privation du bénéfice des mesures prévues par la loi afin de garantir aux demandeurs d'asile des conditions matérielles d'accueil décentes, jusqu'à ce qu'il ait été statué sur leur demande, est susceptible de constituer une atteinte grave et manifestement illégale à la liberté fondamentale que constitue le droit d'asile, le caractère grave et manifestement illégal d'une telle atteinte s'apprécie en tenant compte des moyens dont dispose l'autorité administrative compétente et de la situation du demandeur. Ainsi, le juge des référés, qui apprécie si les conditions prévues par l'article L. 521-2 du code de justice administrative sont remplies à la date à laquelle il se prononce, ne peut faire usage des pouvoirs qu'il tient de cet article en adressant une injonction à l'administration que dans le cas où, d'une part, le comportement de celle-ci fait apparaître une méconnaissance manifeste des exigences qui découlent du droit d'asile et où, d'autre part, il résulte de ce comportement des conséquences graves pour le demandeur d'asile, compte tenu notamment de son âge, de son état de santé ou de sa situation de famille.<br/>
<br/>
              4. Il appartient, d'autre part, aux autorités de l'Etat de mettre en oeuvre le droit à l'hébergement d'urgence reconnu par la loi à toute personne sans abri qui se trouve en situation de détresse médicale, psychique et sociale. Une carence caractérisée dans l'accomplissement de cette tâche peut faire apparaître, pour l'application de l'article L. 521-2 du code de justice administrative, une atteinte grave et manifestement illégale à une liberté fondamentale lorsqu'elle entraîne des conséquences graves pour la personne intéressée. Il incombe au juge des référés d'apprécier dans chaque cas les diligences accomplies par l'administration en tenant compte des moyens dont elle dispose ainsi que de l'âge, de l'état de la santé et de la situation de famille de la personne intéressée.<br/>
<br/>
              5. Les requérants, qui sont âgés de quarante-trois ans, n'apportent en appel aucun élément de nature à infirmer l'appréciation portée, au regard des critères mentionnés aux points 3 et 4 et compte tenu des indications données devant lui par le préfet de la Marne, par le juge des référés de première instance. Ainsi que l'a constaté à bon droit le juge des référés du tribunal administratif de Châlons-en-Champagne et pour les motifs qu'il a retenus, aucune méconnaissance grave et manifeste des obligations qui s'imposent en la matière à l'administration ne peut être donc retenue en l'espèce.<br/>
              6. Il résulte de ce qui précède qu'il est manifeste que l'appel de M. C...et Mme D... ne peut être accueilli. Leur requête, y compris les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative et de l'article 37 de la loi du 10 juillet 1991, doit dès lors être rejetée selon la procédure prévue par l'article L. 522-3 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de M. C...et Mme D... est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à M. A...C...et Mme B...D....<br/>
Copie en sera adressée au préfet de la Marne.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
