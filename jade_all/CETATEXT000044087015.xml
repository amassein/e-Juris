<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044087015</ID>
<ANCIEN_ID>JG_L_2021_08_000000455526</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/08/70/CETATEXT000044087015.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 23/08/2021, 455526, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-08-23</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>455526</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2021:455526.20210823</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, enregistrée le 13 août 2021 au secrétariat du contentieux du Conseil d'Etat, M. B... A... demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative :<br/>
<br/>
              1°) de suspendre l'exécution des dispositions du 9° du II de l'article 47-1 du décret n° 2021-699 du 1er juin 2021, tel qu'il résulte du décret n° 2021-1059 du 7 août 2021 ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 100 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              M. A... soutient que :<br/>
              - il justifie d'un intérêt à agir ; <br/>
              - la condition d'urgence est satisfaite en ce qu'il est susceptible d'être victime d'un accident ou d'un incident de santé nécessitant son admission dans un établissement de santé ; <br/>
              - il est porté une atteinte grave et manifestement illégale aux libertés fondamentales garanties par le 11ème alinéa du Préambule de la Constitution de 1946, par les articles 8, 9 et 14 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales et à l'article 1er de son 12ème protocole additionnel.<br/>
<br/>
              Par un mémoire distinct, enregistré le 13 août 2021, présenté en application de l'article 23-5 de l'ordonnance du 7 novembre 1958, M. A... demande au juge des référés du Conseil d'Etat de renvoyer au Conseil constitutionnel la question de la conformité aux droits et libertés garantis par la Constitution des dispositions du " d du 2° du II " de l'article 1er de la loi n° 2021-1040 du 5 août 2021 relative à la gestion de la crise sanitaire. Il soutient que ces dispositions sont applicables au litige, qu'elles n'ont pas été déclarées conformes à la Constitution et que la question de leur conformité au droit à la protection de la santé garanti par le 11ème alinéa du Préambule de la Constitution de 1946 est sérieuse.<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la Constitution, et notamment son préambule ; <br/>
              - la convention européenne de sauvegarde des droits de l'homme et libertés fondamentales ; <br/>
              - la loi n° 2021-689 du 31 mai 2021 relative à la gestion de la sortie de crise sanitaire ;<br/>
              - la loi n° 2021-1040 du 5 août 2021 relative à la gestion de la crise sanitaire ;<br/>
              - la décision du Conseil constitutionnel n° 2021-824 DC du 5 août 2021 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. (...) ". En vertu de l'article L. 522-3 du même code, le juge des référés peut, par une ordonnance motivée, rejeter une requête sans instruction ni audience lorsque la condition d'urgence n'est pas remplie ou lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable ou qu'elle est mal fondée.<br/>
<br/>
              2. Par une requête introduite sur le fondement de ces dispositions, M. A... demande au juge des référés du Conseil d'Etat de suspendre l'exécution des dispositions du 9° du II de l'article 47-1 du décret du 1er juin 2021 prescrivant les mesures générales nécessaires à la gestion de la sortie de la crise sanitaire, telles qu'elles résultent du décret n° 2021-1059 du 7 août 2021 les ayant modifiées.<br/>
<br/>
              Sur la question prioritaire de constitutionnalité :<br/>
<br/>
              3. Dans les motifs et le dispositif de sa décision n° 2021-824 DC du 5 août 2021, le Conseil constitutionnel a déclaré conformes à la Constitution, sous la réserve énoncée au paragraphe 54 de cette même décision, le 2° du A du paragraphe II de l'article 1er de la loi n° 2021-689 du 31 mai 2021 relative à la gestion de la sortie de crise sanitaire, dans sa rédaction résultant de l'article 1er de la loi du 5 août 2021 relative à la gestion de la crise sanitaire. Aucun changement de circonstances n'étant invoqué par M. A..., le moyen tiré de ce que les dispositions du 2° du A du paragraphe II de l'article 1er de cette loi méconnaissent le droit à la protection de la santé garantie par le 11ème alinéa du Préambule de la Constitution de 1946 ne peut ainsi qu'être écarté, sans qu'il soit besoin de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité qu'il soulève.<br/>
              Sur les autres moyens :<br/>
<br/>
              4. M. A... se borne à soutenir, sans présenter aucun argument à l'appui de ses allégations, que les dispositions qu'il conteste méconnaissent le 11ème alinéa du Préambule de la Constitution de 1946, les articles 8, 9 et 14 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales et l'article 1er de son 12ème protocole additionnel. Il n'est, par suite, manifestement pas fondé à soutenir qu'elles porteraient une atteinte grave et manifestement illégale à une liberté fondamentale.<br/>
<br/>
              5. Il résulte de tout ce qui précède que, sans qu'il soit besoin de se prononcer sur la condition d'urgence posée par les dispositions rappelées au point 1, la requête de M. A... doit être rejetée, y compris, par voie de conséquence, les conclusions qu'elle présente au titre de l'article L. 761-1 du code de justice administrative, selon la procédure prévue par l'article L. 522-3 du même code.<br/>
<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : Il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée par M. A.... <br/>
Article 2 : la requête de M. A... est rejetée. <br/>
Article 3 : La présente ordonnance sera notifiée à M. B... A....<br/>
Copie en sera adressée au Premier ministre et au Conseil constitutionnel.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
