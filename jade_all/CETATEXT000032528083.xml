<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032528083</ID>
<ANCIEN_ID>JG_L_2016_05_000000390077</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/52/80/CETATEXT000032528083.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème chambre, 11/05/2016, 390077, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-05-11</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>390077</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Guillaume Déderen</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Suzanne von Coester</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2016:390077.20160511</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés le 11 mai 2015 et le 3 février 2016, au secrétariat du Conseil d'Etat Mme B...A...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision en date du 24 février 2015 par laquelle la commission d'avancement statuant en matière d'intégration directe dans la magistrature a émis un avis défavorable sur sa candidature ;<br/>
<br/>
              2°) d'enjoindre au Garde des Sceaux, ministre de la justice, de procéder à sa nomination, à titre principal, à la fonction de magistrat du premier grade et, à titre subsidiaire, à la fonction de magistrat du second grade ; <br/>
<br/>
              3°) de condamner l'Etat à lui verser la somme de 200 000 euros, en réparation du préjudice subi du fait de la décision contestée, avec intérêts de droit et capitalisation ;<br/>
<br/>
              4°) de mettre à la charge de l'Etat le versement de la somme de 2 500 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - l'ordonnance n° 58-1270 du 22 décembre 1958 ;<br/>
              - la loi n° 79-587 du 11 juillet 1979 ;<br/>
              - la loi n° 2000-221 du 12 avril 2000 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Guillaume Déderen, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Suzanne von Coester, rapporteur public ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'il résulte des dispositions de l'article 22 de l'ordonnance du 22 décembre 1958 portant loi organique relative au statut de la magistrature que peuvent être nommées directement aux fonctions du second grade de la hiérarchie judiciaire, à condition d'être âgées de trente-cinq ans au moins, les personnes remplissant les conditions prévues à l'article 16, titulaires d'un diplôme et justifiant de sept années au moins d'exercice professionnel les qualifiant particulièrement pour exercer des fonctions judiciaires ; qu'il résulte des dispositions de l'article 23 de la même ordonnance que peuvent être nommées directement aux fonction de premier grade de la hiérarchie judiciaire, les personnes remplissant les conditions prévues à l'article 16 et justifiant de dix-sept années au moins d'exercice professionnel les qualifiant particulièrement pour exercer des fonctions judiciaires ; que selon l'article 25-2 de la même ordonnance, les nominations à ces titres interviennent après avis conforme de la commission d'avancement prévue à l'article 34 de l'ordonnance ;<br/>
<br/>
              2. Considérant que Mme A...a déposé une demande d'intégration dans le corps judiciaire sur le fondement de l'article 22 de l'ordonnance du 22 décembre 1958, à titre principal, et de l'article 23, à titre subsidiaire ; que, par une décision notifiée le 13 mars 2015, la commission d'avancement a émis un avis défavorable sur cette demande ; que Mme A... demande l'annulation pour excès de pouvoir de cet avis ;<br/>
<br/>
              3. Considérant, en premier lieu, qu'en  prononçant  l'avis contesté, lequel lie le garde des sceaux en application des dispositions de l'article 25-2 de l'ordonnance du 22 décembre 1958, la commission n'a pas entendu rejeter directement la candidature de Mme A... et se substituer ainsi au ministre ; que, par suite, la requérante n'est pas fondée à soutenir que cette commission aurait méconnu l'étendue de sa compétence ;<br/>
<br/>
              4. Considérant, en deuxième lieu, que par les dispositions citées ci-dessus de l'ordonnance du 22 décembre 1958, le législateur organique a entendu investir la commission d'avancement d'un large pouvoir dans l'appréciation de l'aptitude des candidats à exercer les fonctions de magistrat ; que les dispositions des articles 22, 23 et 25-2 de l'ordonnance du 22 décembre 1958 ne créent, au profit des candidats à l'intégration directe des corps judiciaires, aucun droit à être nommés à des fonctions du second grade ou du premier grade de la hiérarchie judiciaire ; qu'il suit de là que le rejet de leur candidature par la commission d'avancement  instituée par l'article 34 de la même ordonnance ne saurait être regardé comme le refus d'une autorisation ou d'un avantage dont l'attribution constitue un droit au sens de l'article 1er de la loi du 11 juillet 1979 relative à la motivation des actes administratifs ; qu'aucune disposition de cette loi ni aucune autre disposition législative ou réglementaire n'imposent que l'avis rendu par la commission d'avancement soit motivé ; que dès lors, le moyen tiré du défaut de motivation de la décision attaquée doit être écarté ; <br/>
<br/>
              5. Considérant en troisième lieu, qu'il est satisfait aux exigences découlant des prescriptions de l'article 4 de la loi du 12 avril 2000 relative aux droits des citoyens dans leurs relations avec les administrations dès lors que la décision prise par un organisme collégial comporte la signature de son président, accompagnée des mentions prévues par cet article ; que s'agissant de la commission d'avancement, les dispositions de l'article 35 de l'ordonnance du 22 décembre 1958 qui fixent sa composition prévoient qu'elle est présidée par le doyen des présidents de chambre de la Cour de cassation, président, suppléé dans ces fonctions, en cas d'empêchement, par le plus ancien des premiers avocats généraux à la Cour de cassation, vice-président ; que si la notification par lettre du 24 février 2015 de l'avis de la commission d'avancement statuant en matière d'intégration directe dans le corps judiciaire qui doit être regardée comme une décision rendue par cette commission, ne comporte pas les mentions requises par les dispositions de l'article 4 de la loi du 12 avril 2000, il ressort des pièces du dossier que le doyen des présidents de chambre de la Cour de cassation présidait les travaux de la commission d'avancement dont la composition est rappelée par ce même procès-verbal ; qu'ainsi, dans les circonstances de l'espèce, la méconnaissance des dispositions de l'article 4 de la loi du 12 avril 2000 n'a pas privé l'intéressée d'une garantie pouvant justifier l'annulation de l'avis attaqué ; <br/>
<br/>
              6. Considérant, en quatrième lieu, qu'il ressort des pièces du dossier que si Mme A... fait valoir qu'elle a exercé la profession d'avocat, d'abord comme stagiaire depuis le 30 juin 2008, puis comme collaboratrice depuis le 1er janvier 2010, après avoir notamment occupé différentes fonctions dans des services juridiques de l'administration, elle n'a pas justifié, eu égard aux fonctions ainsi exercées, d'une expérience de sept années au moins d'exercice professionnel la qualifiant particulièrement pour exercer des fonctions judiciaires ; que, par suite, la commission d'avancement, après avoir examiné et évalué son expérience professionnelle, n'a pas commis d'erreur manifeste d'appréciation en rejetant sa candidature comme irrecevable ;<br/>
<br/>
              7. Considérant qu'il résulte de ce qui précède que Mme A... n'est pas fondée à demander l'annulation de l'avis par lequel la commission d'avancement statuant en matière d'intégration directe dans la magistrature a rejeté sa candidature ; que, par suite, ses conclusions tendant à ce qu'il soit enjoint au garde des sceaux de procéder à sa nomination, à titre principal, à la fonction de magistrat du premier grade, à titre subsidiaire, à la fonction du magistrat du second grade ne peuvent qu'être elles aussi rejetées ;<br/>
<br/>
              8. Considérant qu'il y a lieu de rejeter, par voie de conséquence du rejet des conclusions de Mme A...tendant à l'annulation de l'avis de la commission d'avancement, ses conclusions tendant à la réparation des préjudices résultant de l'illégalité de cet avis ;<br/>
<br/>
              9. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
		Article 1 : La requête de Mme A...est rejetée.<br/>
Article 2 : La présente décision sera notifiée à Mme B...A...et au garde des sceaux, ministre de la justice et des libertés.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
