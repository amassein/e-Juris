<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044110231</ID>
<ANCIEN_ID>JG_L_2021_09_000000448985</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/11/02/CETATEXT000044110231.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème - 2ème chambres réunies, 27/09/2021, 448985</TITRE>
<DATE_DEC>2021-09-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>448985</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème - 2ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Frédéric Gueudar Delahaye</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Marc Pichon de Vendeuil</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:448985.20210927</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et un nouveau mémoire, enregistrés les 21 janvier et 26 mai 2021 au secrétariat du contentieux du Conseil d'Etat, l'Union nationale des syndicats CGT de la protection judiciaire de la jeunesse (UNS CGT-PJJ) demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler la note du 24 décembre 2020 du ministre de la justice relative au report de jours de congés non pris au titre de l'année 2020 ; <br/>
<br/>
              2°) d'enjoindre au ministre de rétablir les agents lésés dans leurs droits afin de leur donner la possibilité de déposer 20 jours sur leur compte épargne-temps au titre de l'année 2020 ; <br/>
<br/>
              3°) de condamner l'Etat aux entiers dépens.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code des relations entre le public et l'administration ;<br/>
              - le décret n° 84-972 du 26 octobre 1984 ;<br/>
              - le décret n° 2002-634 du 29 avril 2002 ; <br/>
              - l'arrêté du 28 août 2009 pris pour l'application du décret n° 2002-634 du 29 avril 2002 modifié portant création du compte épargne-temps dans la fonction publique de l'Etat et dans la magistrature ;<br/>
              - l'arrêté du 30 décembre 2009 relatif à la mise en œuvre du compte épargne-temps pour les agents du ministère de la justice et des libertés, de la grande chancellerie de la Légion d'honneur et pour les magistrats de l'ordre judiciaire ;<br/>
              - l'arrêté du 11 mai 2020 relatif à la mise en œuvre de dispositions temporaires en matière de compte épargne-temps dans la fonction publique de l'Etat et dans la magistrature afin de faire face aux conséquences de l'épidémie de covid-19 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Frédéric Gueudar Delahaye, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Marc Pichon de Vendeuil, rapporteur public ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. En vertu de l'arrêté du 11 mai 2020 relatif à la mise en œuvre de dispositions temporaires en matière de compte épargne-temps dans la fonction publique de l'Etat et dans la magistrature afin de faire face aux conséquences de l'épidémie de covid-19, le nombre maximal de jours supplémentaires pouvant être inscrits sur un compte épargne-temps, au titre de l'année 2020, est porté de 10 à 20 jours et le plafond global pouvant être inscrit sur un tel compte porté de 60 à 70 jours. La note contestée du 24 décembre 2020 de la secrétaire générale du ministère de la justice précise les conditions exceptionnelles de report de congés au titre de l'année 2020 et rappelle les modalités d'alimentation du compte épargne-temps dans ce cadre. Eu égard aux moyens soulevés, la requête doit être regardée comme dirigée contre cette note en tant qu'elle prévoit que ce compte ne peut être alimenté qu'à la condition que l'agent ait pris dans l'année au moins 20 jours de congés annuels, y compris les " jours de fractionnement ", en excluant du calcul de ce seuil les jours de réduction du temps de travail.<br/>
<br/>
              2. En premier lieu, le syndicat requérant ne peut utilement soutenir que la note en litige méconnaîtrait les dispositions de l'article L. 212-1 du code des relations entre le public et l'administration, relatives à la signature des décisions et aux mentions relatives à leur auteur, dès lors que les dispositions contestées ne revêtent pas le caractère d'une décision.<br/>
<br/>
              3. En deuxième lieu, aux termes de l'article 1er du décret du 26 octobre 1984 relatif aux congés annuels des fonctionnaires de l'Etat : " Tout fonctionnaire de l'Etat en activité a droit (...) à un congé annuel d'une durée égale à cinq fois ses obligations hebdomadaires de service. Cette durée est appréciée en nombre de jours effectivement ouvrés. / Un jour de congé supplémentaire est attribué à l'agent dont le nombre de jours de congé pris en dehors de la période du 1er mai au 31 octobre est de cinq, six ou sept jours ; il est attribué un deuxième jour de congé supplémentaire lorsque ce nombre est au moins égal à huit jours. / (...) ". Aux termes de l'article 3 du décret du 29 avril 2002 portant création d'un compte épargne-temps dans la fonction publique de l'Etat et dans la magistrature : " Le compte épargne-temps est alimenté par le report de jours de réduction du temps de travail et par le report de congés annuels, tels que prévus par le décret du 26 octobre 1984 susvisé, sans que le nombre de jours de congés pris dans l'année puisse être inférieur à 20 ". Aux termes de l'article 3 de l'arrêté du 30 décembre 2009 relatif à la mise en œuvre du compte épargne-temps pour les agents du ministère de la justice et des libertés, de la grande chancellerie de la Légion d'honneur et pour les magistrats de l'ordre judiciaire : " Le compte épargne-temps peut être alimenté exclusivement : / - par des jours de congés annuels, sans que le nombre de congés pris dans l'année puisse être inférieur à 20. Le compte peut également être alimenté par le ou les jours dits de fractionnement visés au deuxième alinéa de l'article 1er du décret du 26 octobre 1984 susvisé ; / - par des jours de réduction du temps de travail ; / - par des jours de repos compensateurs réglementaires prévus à l'article 5 de l'arrêté du 26 décembre 2001 (...) ". <br/>
<br/>
              4. Il résulte de ces dispositions, notamment de l'article 3 du décret du 29 avril 2002, que le nombre de 20 jours de congés devant, au minimum, avoir été pris dans l'année pour ouvrir droit à l'alimentation du compte épargne-temps s'apprécie uniquement au regard des jours de congés annuels ainsi que, le cas échéant, des jours de congés supplémentaires dits " de fractionnement " mentionnés au deuxième alinéa de l'article 1er du décret du 26 octobre 1984, sans que puissent être pris en compte les jours de repos institués en contrepartie de la réduction du temps de travail, qui ne sont pas des jours de congés. Par suite, c'est sans commettre d'erreur de droit que l'auteur de la note contestée s'est borné à rappeler cette règle.<br/>
<br/>
              5. En dernier lieu, contrairement à ce qui est soutenu, la note attaquée n'énonce pas que le compte épargne-temps ne peut être alimenté qu'à hauteur d'un maximum de 20 jours par an, mais se borne à rappeler que s'agissant des jours de congés annuels, le compte épargne-temps ne peut être alimenté que si le nombre de jours de congés pris dans l'année n'a pas été inférieur à 20. Si l'application de cette note ne permet pas à tous les agents d'un même corps de pouvoir alimenter leur compte épargne-temps de la même façon, ceci tient à ce que les agents concernés bénéficient de régimes de temps de travail différents, eux-mêmes liés à des types d'activité différents. Par suite, le moyen tiré de la méconnaissance du principe d'égalité ne peut qu'être écarté.<br/>
<br/>
              6. Il résulte de ce qui précède que la requête doit être rejetée, y compris les conclusions à fin d'injonction.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de l'Union nationale des syndicats CGT de la protection judiciaire de la jeunesse est rejetée.<br/>
<br/>
Article 2 : La présente décision sera notifiée à l'Union nationale des syndicats CGT de la protection judiciaire de la jeunesse et au garde des sceaux, ministre de la justice.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">36-07-01-02 FONCTIONNAIRES ET AGENTS PUBLICS. - STATUTS, DROITS, OBLIGATIONS ET GARANTIES. - STATUT GÉNÉRAL DES FONCTIONNAIRES DE L'ÉTAT ET DES COLLECTIVITÉS LOCALES. - DISPOSITIONS STATUTAIRES RELATIVES À LA FONCTION PUBLIQUE DE L'ÉTAT (LOI DU 11 JANVIER 1984). - COMPTE-ÉPARGNE TEMPS (DÉCRET DU 29 AVRIL 2002 ) POUR LES AGENTS DU MINISTÈRE DE LA JUSTICE ET LES MAGISTRATS JUDICIAIRES - ALIMENTATION - CONDITION TENANT À CE QUE 20 JOURS DE CONGÉ AIENT ÉTÉ PRIS DANS L'ANNÉE - PRISE EN COMPTE DES SEULS JOURS DE CONGÉS ANNUELS ET DE FRACTIONNEMENT - CONSÉQUENCE - PRISE EN COMPTE DES JOURS DE REPOS CONTREPARTIE DE LA RTT - ABSENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">36-08-03 FONCTIONNAIRES ET AGENTS PUBLICS. - RÉMUNÉRATION. - INDEMNITÉS ET AVANTAGES DIVERS. - COMPTE ÉPARGNE-TEMPS DANS LA FONCTION PUBLIQUE DE L'ETAT (DÉCRET DU 29 AVRIL 2002) POUR LES AGENTS DU MINISTÈRE DE LA JUSTICE ET LES MAGISTRATS JUDICIAIRES - ALIMENTATION - CONDITION TENANT À CE QUE 20 JOURS DE CONGÉ AIENT ÉTÉ PRIS DANS L'ANNÉE - PRISE EN COMPTE DES SEULS JOURS DE CONGÉS ANNUELS ET DE FRACTIONNEMENT - CONSÉQUENCE - PRISE EN COMPTE DES JOURS DE REPOS CONTREPARTIE DE LA RTT - ABSENCE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">37-04-02-01 JURIDICTIONS ADMINISTRATIVES ET JUDICIAIRES. - MAGISTRATS ET AUXILIAIRES DE LA JUSTICE. - MAGISTRATS DE L'ORDRE JUDICIAIRE. - STATUT, DROITS, OBLIGATIONS ET GARANTIES. - COMPTE ÉPARGNE-TEMPS (DÉCRET DU 29 AVRIL 2002) - ALIMENTATION - CONDITION TENANT À CE QUE 20 JOURS DE CONGÉ AIENT ÉTÉ PRIS DANS L'ANNÉE - PRISE EN COMPTE DES SEULS JOURS DE CONGÉS ANNUELS ET DE FRACTIONNEMENT - CONSÉQUENCE - PRISE EN COMPTE DES JOURS DE REPOS CONTREPARTIE DE LA RTT - ABSENCE.
</SCT>
<ANA ID="9A"> 36-07-01-02 Il résulte de l'article 3 du décret n° 2002-634 du 29 avril 2002 et de l'article 3 de l'arrêté du 30 décembre 2009 que le nombre de 20 jours de congés devant, au minimum, avoir été pris dans l'année pour ouvrir droit à l'alimentation du compte épargne-temps s'apprécie uniquement au regard des jours de congés annuels ainsi que, le cas échéant, des jours de congés supplémentaires dits de fractionnement mentionnés au deuxième alinéa de l'article 1er du décret n° 84-972 du 26 octobre 1984, sans que puissent être pris en compte les jours de repos institués en contrepartie de la réduction du temps de travail (RTT), qui ne sont pas des jours de congés.</ANA>
<ANA ID="9B"> 36-08-03 Il résulte de l'article 3 du décret n° 2002-634 du 29 avril 2002 et de l'article 3 de l'arrêté du 30 décembre 2009 que le nombre de 20 jours de congés devant, au minimum, avoir été pris dans l'année pour ouvrir droit à l'alimentation du compte épargne-temps s'apprécie uniquement au regard des jours de congés annuels ainsi que, le cas échéant, des jours de congés supplémentaires dits de fractionnement mentionnés au deuxième alinéa de l'article 1er du décret n° 84-972 du 26 octobre 1984, sans que puissent être pris en compte les jours de repos institués en contrepartie de la réduction du temps de travail (RTT), qui ne sont pas des jours de congés.</ANA>
<ANA ID="9C"> 37-04-02-01 Il résulte de l'article 3 du décret n° 2002-634 du 29 avril 2002 et de l'article 3 de l'arrêté du 30 décembre 2009 que le nombre de 20 jours de congés devant, au minimum, avoir été pris dans l'année pour ouvrir droit à l'alimentation du compte épargne-temps s'apprécie uniquement au regard des jours de congés annuels ainsi que, le cas échéant, des jours de congés supplémentaires dits de fractionnement mentionnés au deuxième alinéa de l'article 1er du décret n° 84-972 du 26 octobre 1984, sans que puissent être pris en compte les jours de repos institués en contrepartie de la réduction du temps de travail (RTT), qui ne sont pas des jours de congés.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
