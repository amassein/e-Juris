<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000041853440</ID>
<ANCIEN_ID>JG_L_2020_04_000000440179</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/41/85/34/CETATEXT000041853440.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 30/04/2020, 440179, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-04-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>440179</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP COLIN-STOCLET</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2020:440179.20200430</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Par une requête, un mémoire en réplique et un nouveau mémoire, enregistrés le 21 avril 2020 et le 27 avril 2020 à 16h10 et 18h04 au secrétariat du contentieux du Conseil d'Etat, la Fédération française des usagers de la bicyclette demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative :<br/>
<br/>
              1°) d'enjoindre au Premier ministre, au ministre de l'intérieur et à la ministre des sports, sous astreinte de 1 000 euros par jour de retard, de publier, dans les vingt-quatre heures à compter du prononcé de la décision, sur leurs sites internet, sur leurs comptes sur réseaux sociaux (Twitter et Facebook) et par voie d'affichage un communiqué autorisant expressément l'utilisation du vélo pour tous les motifs de déplacement indiqués dans l'article 3 du décret n° 2020-293 du 23 mars 2020, en spécifiant clairement que le vélo à titre d'activité physique individuelle, est autorisé, et en retirant toute information contraire ;<br/>
<br/>
              2°) d'enjoindre au Préfet de police, au préfet d'Ille-et-Vilaine, au préfet de l'Hérault, au préfet d'Indre-et-Loire, au préfet de Loire-Atlantique, au préfet du Lot-et Garonne, au préfet de Haute-Marne, au préfet du Nord, au préfet des Hauts-de-Seine, au préfet du Bas-Rhin, au préfet d'Occitanie, au préfet de Seine-Saint-Denis, à la police et à la gendarmerie nationales, sous astreinte de 1 000 euros par jour de retard, de rouvrir, dans les vingt-quatre heures à compter du prononcé de la décision, les pistes cyclables fermées sans nécessité stricte et, le cas échéant, de mettre en oeuvre des mesures permettant la continuité cyclable ;<br/>
<br/>
              3°) d'enjoindre au Premier ministre, sous astreinte de 1 000 euros par jour de retard, d'émettre, dans les vingt-quatre heures à compter du prononcé de la décision, une circulaire aux détenteurs du pouvoir de police de circulation leur ordonnant de ne fermer les aménagements cyclables qu'en cas de nécessité stricte ;<br/>
<br/>
              4°) d'enjoindre au Premier ministre, sous astreinte de 1 000 euros par jour de retard, d'émettre, dans les vingt-quatre heures à compter du prononcé de la décision, une circulaire à la police et à la gendarmerie nationales, leur ordonnant d'autoriser l'utilisation du vélo pour tous les motifs de déplacement indiqués dans l'article 3 du décret n° 2020-293 du 23 mars 2020 ;<br/>
<br/>
              5°) d'enjoindre au ministère public, sous astreinte de 1 000 euros par jour de retard, de cesser de poursuivre, dans les vingt-quatre heures à compter du prononcé de la décision, les verbalisations ayant pour motif l'usage du vélo ;<br/>
<br/>
              6°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              La Fédération française des usagers de la bicyclette soutient :<br/>
              - que la condition d'urgence est remplie ;<br/>
              - que l'interprétation, à la fois restrictive et incohérente, que donnent les autorités de l'Etat des dispositions du décret du 23 mars 2020, entraîne des verbalisations abusives de la pratique individuelle de la bicyclette et des décisions de fermeture de pistes cyclables, de la part de maires et de préfets, qui portent une atteinte grave et manifestement illégale à la liberté d'aller et venir, à la liberté individuelle, au droit à la sûreté et au principe de légalité des délits et des peines ;<br/>
              - que plusieurs verbalisations ont été effectuées à l'égard de cyclistes dont le comportement ne méconnaissait pas les dispositions du décret du 23 mars 2020 ;<br/>
              - que plusieurs pistes cyclables ont été fermées à tort.<br/>
<br/>
<br/>
              Par un mémoire en défense, enregistré le 24 avril 2020, le ministre de l'intérieur conclut au rejet de la requête. Il soutient que le juge des référés du Conseil d'Etat n'est pas compétent pour connaître en premier et dernier ressort des effets d'arrêtés préfectoraux ni pour adresser des injonctions à l'autorité judiciaire, que la condition d'urgence n'est pas remplie et que les moyens ne sont pas fondés, la pratique de la bicyclette n'étant interdite pour aucun des déplacements autorisés par le décret du 23 mars 2020.<br/>
              La requête a été communiquée au Premier ministre, à la ministre des sports et à la ministre de la transition écologique et solidaire, qui n'ont pas produit de mémoire en défense.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la Constitution, notamment son Préambule ;<br/>
              - le code de la santé publique ;<br/>
              - la loi n° 2020-290 du 23 mars 2020 ;<br/>
              - le décret n° 2020-293 du 23 mars 2020 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, la Fédération française des usagers de la bicyclette et, d'autre part, le Premier ministre, le ministre de l'intérieur, la ministre des sports et la ministre de la transition écologique et solidaire ;<br/>
<br/>
              Ont été entendus lors de l'audience publique du 29 avril 2020, à 10 heures : <br/>
<br/>
              - Me Stoclet, avocat au Conseil d'Etat et à la Cour de cassation, avocat de la Fédération française des usagers de la bicyclette ;<br/>
<br/>
              - les représentants de la Fédération française des usagers de la bicyclette ;<br/>
<br/>
              - les représentants du ministre de l'intérieur ;<br/>
<br/>
              à l'issue de laquelle le juge des référés a prononcé une clôture de l'instruction au 30 avril 2020 à 12h00 ;<br/>
<br/>
              Vu la note en délibéré, présentée par le ministre de l'intérieur, enregistrée le 30 avril 2020 à 12h29 après la clôture de l'instruction ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              Sur le cadre du litige :<br/>
<br/>
              1. Aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. Le juge des référés se prononce dans un délai de quarante-huit heures ". La liberté d'aller et venir et le droit de chacun au respect de sa liberté personnelle, qui implique en particulier qu'il ne puisse subir de contraintes excédant celles qu'imposent la sauvegarde de l'ordre public ou le respect des droits d'autrui, constituent des libertés fondamentales au sens de cet article.<br/>
<br/>
              2. Lorsqu'il est saisi sur le fondement des dispositions citées ci-dessus et qu'il constate une atteinte grave et manifestement illégale portée par une personne morale de droit public à une liberté fondamentale, résultant de l'action ou de la carence de cette personne publique, il appartient au juge des référés de prescrire les mesures qui sont de nature à faire disparaître les effets de cette atteinte, dès lors qu'existe une situation d'urgence caractérisée justifiant le prononcé de mesures de sauvegarde à très bref délai. Le juge des référés peut ordonner à l'autorité compétente de prendre, à titre provisoire, des mesures d'organisation des services placés sous son autorité, dès lors qu'il s'agit de mesures d'urgence qui lui apparaissent nécessaires pour sauvegarder, à très bref délai, la liberté fondamentale à laquelle il est gravement, et de façon manifestement illégale, porté atteinte. <br/>
<br/>
              3. L'article L. 3131-15 du code de la santé publique, introduit dans ce code par la loi du 23 mars 2020 d'urgence pour faire face à l'épidémie de covid-19 dispose que, dans les circonscriptions territoriales où l'état d'urgence sanitaire est déclaré, le Premier ministre peut notamment : " 1° Restreindre ou interdire la circulation des personnes et des véhicules dans les lieux et aux heures fixés par décret ; 2° Interdire aux personnes de sortir de leur domicile, sous réserve des déplacements strictement indispensables aux besoins familiaux ou de santé ; (...) Les mesures prescrites en application des 1° à 10° du présent article sont strictement proportionnées aux risques sanitaires encourus et appropriées aux circonstances de temps et de lieu (...) ". L'article L. 3136-1 du même code prévoit les sanctions pénales encourues en cas de violation des interdictions édictées en application de l'article L. 3131-15 et dispose que l'application de ces sanctions pénales ne fait pas obstacle à l'exécution d'office, par l'autorité administrative, des mesures prescrites en application de ces mêmes interdictions.<br/>
<br/>
              4. Sur le fondement des dispositions citées ci-dessus, l'article 3 du décret du 23 mars 2013 prescrivant les mesures générales nécessaires pour faire face à l'épidémie de covid-19 dans le cadre de l'état d'urgence sanitaire, modifié et complété à plusieurs reprises, interdit, en dernier lieu jusqu'au 11 mai 2020, tout déplacement de personne hors de son domicile, à l'exception de certains déplacements obéissant aux motifs qu'il énumère. Au nombre de ceux-ci figurent notamment, au 5° de cet article, les " déplacements brefs, dans la limite d'une heure quotidienne et dans un rayon maximal d'un kilomètre autour du domicile, liés soit à l'activité physique individuelle des personnes, à l'exclusion de toute pratique sportive collective et de toute proximité avec d'autres personnes, soit à la promenade avec les seules personnes regroupées dans un même domicile, soit aux besoins des animaux de compagnie ". Il résulte des termes mêmes de cet article 3 que l'usage, pour un déplacement qu'il autorise, d'un moyen de déplacement particulier, notamment d'une bicyclette, ne saurait, à lui seul, caractériser une violation de l'interdiction qu'il édicte.<br/>
<br/>
              5. La fédération requérante soutient toutefois que certaines autorités ministérielles, préfectorales ou d'administration centrale ont, dans l'interprétation de ces dispositions qu'elles ont rendue publique, expressément exclu que les déplacements autorisés puissent, en particulier dans le cas de l'activité physique individuelle prévu au 5° de l'article 3, s'effectuer en bicyclette. Elle soutient également que si d'autres autorités de l'Etat ont publiquement indiqué que les moyens de déplacement restent libres, l'existence de prises de position contradictoires et l'absence de clarification entre ces interprétations divergentes est à l'origine de nombreux procès-verbaux de contravention dressés à l'encontre de cyclistes qui respectaient pourtant les dispositions en question, ainsi que de plusieurs décisions de maires ou de préfets interdisant, sans autre fondement qu'une interprétation erronée de l'article 3 du décret du 23 mars 2020, l'accès à certaines pistes cyclables.<br/>
<br/>
              Sur les conclusions de la requête :<br/>
<br/>
              En ce qui concerne l'information générale sur l'usage de la bicyclette :<br/>
<br/>
              6. Il résulte de l'instruction, notamment de l'information apportée, au cours de l'audience publique, par le représentant du ministre de l'intérieur, quant à l'existence et au contenu d'un relevé de décision du 24 avril 2020 de la cellule interministérielle de crise placée auprès du Premier ministre, que l'interprétation des dispositions de l'article 3 du décret du 23 mars 2020 retenue par le gouvernement et devant être diffusée à l'ensemble des agents chargés de leur application est, en premier lieu que " ne sont réglementés que les motifs de déplacement et non les moyens de ces déplacements qui restent libres. La bicyclette est donc autorisée à ce titre comme tout autre moyen de déplacement, et quel que soit le motif du déplacement ", en deuxième lieu que " les verbalisations résultant de la seule utilisation d'une bicyclette, à l'occasion d'un déplacement autorisé, sont injustifiées " et, en troisième lieu, que les restrictions de temps et de distance imposées par les dispositions du 5° de l'article 3 privent en principe d'intérêt l'usage de la bicyclette pour un déplacement exclusivement motivé par l'activité physique individuelle et que, dans un tel cas, le risque plus important de commission d'une infraction liée au dépassement de la distance autorisée doit conduire, tout en en rappelant la possibilité juridique d'utiliser la bicyclette pour tout motif de déplacement, à " en dissuader l'usage au titre de l'activité physique ".<br/>
<br/>
              7. Toutefois, il résulte également de l'instruction que, malgré l'existence de cette position de principe, dont la légalité n'est pas contestée par la fédération requérante, plusieurs autorités de l'Etat continuent de diffuser sur les réseaux sociaux ou dans des réponses à des " foires aux questions ", l'information selon laquelle la pratique de la bicyclette est interdite dans le cadre des loisirs et de l'activité physique individuelle " à l'exception des promenades pour aérer les enfants où il est toléré que ceux-ci se déplacent à vélo, si l'adulte accompagnant est à pied ", ainsi qu'un pictogramme exprimant cette même interdiction.<br/>
<br/>
              8. Or, d'une part, la faculté de se déplacer en utilisant un moyen de locomotion dont l'usage est autorisé constitue, au titre de la liberté d'aller et venir et du droit de chacun au respect de sa liberté personnelle, une liberté fondamentale au sens des dispositions de l'article L. 521-2 du code de justice administrative.<br/>
<br/>
              9. D'autre part, si les cyclistes qui s'estiment verbalisés à tort peuvent, devant le juge judiciaire, contester l'infraction qui leur est reprochée, la faculté reconnue à l'administration, en vertu des dispositions rappelées au point 3, d'exécuter d'office les mesures prescrites en application du décret du 23 mars 2020 est de nature à conduire, en cas d'interdiction de déplacement opposée, à tort, à raison du seul usage d'une bicyclette, à ce que le cycliste contrôlé soit tenu de descendre de son véhicule et de poursuivre son trajet à pied.<br/>
<br/>
              10. Dans ces conditions, compte tenu de l'incertitude qui s'est installée, à raison des contradictions relevées dans la communication de plusieurs autorités publiques, sur la portée des dispositions de l'article 3 du décret du 23 mars 2020, particulièrement en ce qui concerne l'activité physique, quant à l'usage de la bicyclette et des conséquences de cette incertitude pour les personnes qui utilisent la bicyclette pour leurs déplacements autorisés, l'absence de diffusion publique de la position gouvernementale mentionnée au point 6 doit être regardée, en l'espèce, comme portant une atteinte grave et manifestement illégale à une liberté fondamentale justifiant que le juge du référé-liberté fasse usage des pouvoirs qu'il tient de l'article L. 521-2 du code de justice administrative et enjoigne au Premier ministre de rendre publique, sous vingt-quatre heures, par un moyen de communication à large diffusion, la position en question.<br/>
<br/>
              11. Il n'y a pas lieu, en revanche, de faire droit au surplus des conclusions présentées sur ce point par la fédération requérante et, notamment, d'assortir cette injonction d'une astreinte.<br/>
              En ce qui concerne les fermetures de plusieurs pistes cyclables :<br/>
<br/>
              12. Le juge des référés du Conseil d'Etat ne peut être régulièrement saisi, en premier et dernier ressort, d'une requête tendant à la mise en oeuvre de l'une des procédures régies par le livre V du code de justice administrative que pour autant que le litige principal auquel se rattache ou est susceptible de se rattacher la mesure d'urgence qu'il lui est demandé de prendre ressortit lui-même de la compétence directe du Conseil d'Etat.<br/>
<br/>
              13. La fédération requérante demande qu'il soit enjoint à plusieurs autorités préfectorales de procéder à la réouverture de certaines pistes cyclables de leur département. Ces conclusions ne sont pas au nombre de celles qui relèvent, en premier et dernier ressort, du juge des référés du Conseil d'Etat. Par suite, en application des dispositions de l'article R. 522-8-1 du code de justice administrative qui dérogent aux dispositions du titre V du livre III du même code relatif au règlement des questions de compétence au sein de la juridiction administrative, elles ne peuvent qu'être rejetées.<br/>
<br/>
              14. Par ailleurs, compte tenu de la mesure prononcée au point 10, les conclusions de la fédération requérante tendant à ce qu'une instruction générale soit adressée aux autorités détentrices du pouvoir de police de la circulation sur les motifs légaux de fermeture d'une piste cyclable doivent également être rejetées.<br/>
              En ce qui concerne les infractions relevées à l'encontre de certains cyclistes :<br/>
<br/>
              15. Les dispositions de l'article L. 521-2 du code de justice administrative n'habilitent pas le juge des référés à adresser une injonction à l'autorité judiciaire. Les conclusions de la fédération requérante tendant à ce qu'il soit enjoint à celle-ci d'interrompre toutes les poursuites engagées contre les cyclistes ayant fait l'objet d'un procès-verbal d'infraction aux dispositions de l'article 3 du décret du 23 mars 2020 ne peuvent, par suite, qu'être rejetées.<br/>
<br/>
              16. Dans les circonstances de l'espèce, il y a lieu de mettre à la charge de l'Etat une somme de 3 000 euros à verser à la Fédération française des usagers de la bicyclette, au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : Il est enjoint au Premier ministre de rendre publique sous vingt-quatre heures, par un moyen de communication à large diffusion, la position du gouvernement, mentionnée au point 6 de la présente ordonnance et exprimée par le représentant de l'Etat au cours de l'audience publique, relative à l'usage de la bicyclette lors des déplacements autorisés par l'article 3 du décret n° 2020-293 du 23 mars 2020.<br/>
Article 2 : L'Etat versera à la Fédération française des usagers de la bicyclette une somme de 3 000 euros, au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : Le surplus des conclusions de la requête est rejeté.<br/>
Article 4 : La présente ordonnance sera notifiée au Premier ministre, à la Fédération française des usagers de la bicyclette, au ministre de l'intérieur, à la ministre des sports et à la ministre de la transition écologique et solidaire.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
