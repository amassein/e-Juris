<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030445544</ID>
<ANCIEN_ID>JG_L_2015_03_000000361673</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/44/55/CETATEXT000030445544.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème / 3ème SSR, 27/03/2015, 361673, Publié au recueil Lebon</TITRE>
<DATE_DEC>2015-03-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>361673</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème / 3ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BORE, SALVE DE BRUNETON ; SCP BARTHELEMY, MATUCHANSKY, VEXLIARD, POUPOT</AVOCATS>
<RAPPORTEUR>M. Mathieu Herondart</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Nathalie Escaut</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:361673.20150327</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A...B...et la société Titaua limited compagny ont demandé au tribunal administratif de Marseille de condamner la commune de Port-de-Bouc à leur verser à chacun la somme de 528 350 euros au titre de dommages et intérêts.<br/>
<br/>
              Par un jugement n°s 0601718, 0603671 du 20 octobre 2009, le tribunal administratif de Marseille, après avoir donné acte du désistement de M. A...B..., a rejeté la demande de la société Titaua limited compagny et les conclusions reconventionnelles de la commune de Port-de-Bouc.<br/>
<br/>
              Par un arrêt n° 09MA04621 du 6 avril 2012, la cour administrative d'appel de Marseille a rejeté l'appel formé contre ce jugement par la société Titaua limited compagny.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 6 août et 6 novembre 2012 et le 8 décembre 2014 au secrétariat du contentieux du Conseil d'Etat, la société Titaua limited compagny demande au Conseil d'Etat :<br/>
<br/>
               1°) d'annuler cet arrêt ; <br/>
<br/>
              2°)  réglant l'affaire au fond, de faire droit à son appel ; <br/>
<br/>
              3°) de mettre à la charge de la commune de Port-de-Bouc la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code général des collectivités territoriales ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Mathieu Herondart, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Nathalie Escaut, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Boré, Salve de Bruneton, avocat de la société Titaua Limited Compagny et à la SCP Barthélemy, Matuchansky, Vexliard, Poupot, avocat de la commune de Port-de-Bouc ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'un bien immeuble résultant d'un aménagement et qui est directement affecté à un service public a la qualité d'ouvrage public ; que, dans le cas où un ouvrage implanté sur le domaine public fait l'objet d'une convention d'occupation de ce domaine dont les stipulations prévoient expressément son affectation à une personne privée afin qu'elle y exerce une activité qui n'a pas le caractère d'un service public, le bien en cause ne peut plus être qualifié d'ouvrage tant qu'il n'est pas de nouveau affecté à une activité publique, alors même que, n'ayant fait l'objet d'aucune procédure de déclassement, il n'a pas cessé de relever du domaine public ;<br/>
<br/>
              2. Considérant que, par l'arrêt attaqué du 6 avril 2012, la cour administrative d'appel de Marseille a relevé que le port autonome de Marseille avait, par autorisation d'occupation temporaire du domaine public maritime du 31 mars 1989, modifiée le 4 décembre 1991, mis à la disposition de la commune de Port-de-Bouc un ensemble de biens, comprenant des terrains, plans d'eau et bâtiments, situés quai des Agglomérés sur le territoire de la commune et destinés à " la réalisation et la gestion d'installations principalement liées à la mer " et que le hangar de 3 600 m², qui faisait partie de ces biens, avait, par une convention d'occupation temporaire du domaine public du 11 juillet 2000, été mis à la disposition de la société Petter quality yachts qui devait construire un catamaran pour le compte de la société Titaua limited compagny ;<br/>
<br/>
               3. Considérant que, pour juger que ce bâtiment ne pouvait, à la date de l'incendie dont il a fait l'objet le 5 janvier 2004, être regardé comme un ouvrage public, la cour s'est fondée, d'une part, sur la circonstance qu'en application des articles 3 et 5 de la convention du 11 juillet 2000, le bâtiment était destiné à une activité de " construction et réparation de bateaux de plaisance, vente de bateau de plaisance, menuiserie et électricité " et que les parties convenaient qu'il s'agissait d'une activité exclusivement privée, d'autre part, sur le refus opposé par la commune, par un courrier du 13 novembre 2003, de prendre en compte la demande de résiliation de la convention formulée par le liquidateur de la société Petter quality yachts, lequel en avait les clefs et conservé la garde, ainsi qu'il ressortait de l'arrêt du 31 mai 2007 de la cour d'appel d'Aix-en-Provence ; qu'en en déduisant que, malgré son implantation sur le domaine public, le local ne pouvait être regardé comme un ouvrage public, la cour a exactement qualifié les faits qui lui étaient soumis et n'a commis aucune erreur de droit ; <br/>
<br/>
              4. Considérant que, pour juger que la société Titaua limited compagny n'établissait pas l'existence de manquements susceptibles d'engager la responsabilité de la commune de Port-de-Bouc à son égard, la cour a, d'une part, relevé que la convention n'avait pas été résiliée en l'absence de remise des clefs et de restitution des lieux libres de toute occupation et, d'autre part, que la commune avait procédé d'urgence à la fermeture des portes du hangar à la suite de l'effraction constatée le 2 décembre 2003 ; qu'elle en a déduit, par des motifs suffisants qui ne sont pas entachés de contradiction, qu'en se bornant à reprocher à la commune l'absence de mise en sécurité et de fermeture de l'accès des lieux, la société ne caractérisait pas l'existence de manquements de la part de la collectivité à son obligation de sécurité, à l'origine de l'incendie déclenché le 5 janvier 2004, dont la cause demeure inconnue ; qu'en statuant ainsi, la cour a exactement qualifié les faits, dont la réalité n'est pas contestée, et n'a commis aucune erreur de droit ; <br/>
<br/>
              5. Considérant qu'il résulte de ce qui précède que le pourvoi de la société Titaua limited compagny doit être rejeté, y compris ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative ; qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées au même titre par la commune de Port-de-Bouc ;<br/>
<br/>
<br/>
<br/>
<br/>                    D E C I D E :<br/>
                                   --------------<br/>
<br/>
Article 1er : Le pourvoi de la société Titaua limited compagny est rejeté.<br/>
<br/>
Article 2 : Les conclusions de la commune de Port-de-Bouc présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 3 : La présente décision sera notifiée à la société Titaua limited compagny et à la commune de Port-de-Bouc.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">24-01-02-03 DOMAINE. DOMAINE PUBLIC. RÉGIME. CONSÉQUENCES DU RÉGIME DE LA DOMANIALITÉ PUBLIQUE SUR D'AUTRES LÉGISLATIONS. - OUVRAGE IMPLANTÉ SUR LE DOMAINE PUBLIC - CONVENTION D'OCCUPATION DE CE DOMAINE PRÉVOYANT SON AFFECTATION À UNE PERSONNE PRIVÉE AFIN QU'ELLE Y EXERCE UNE ACTIVITÉ QUI N'A PAS LE CARACTÈRE D'UN SERVICE PUBLIC - OUVRAGE N'AYANT PLUS LA QUALITÉ D'OUVRAGE PUBLIC, ALORS MÊME QU'IL N'A PAS CESSÉ DE RELEVER DU DOMAINE PUBLIC [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">67-01-02-02 TRAVAUX PUBLICS. NOTION DE TRAVAIL PUBLIC ET D'OUVRAGE PUBLIC. OUVRAGE PUBLIC. OUVRAGE NE PRÉSENTANT PAS CE CARACTÈRE. - OUVRAGE IMPLANTÉ SUR LE DOMAINE PUBLIC AYANT FAIT L'OBJET D'UNE CONVENTION D'OCCUPATION DE CE DOMAINE PRÉVOYANT SON AFFECTATION À UNE PERSONNE PRIVÉE AFIN QU'ELLE Y EXERCE UNE ACTIVITÉ QUI N'A PAS LE CARACTÈRE D'UN SERVICE PUBLIC [RJ1].
</SCT>
<ANA ID="9A"> 24-01-02-03 Un bien immeuble résultant d'un aménagement et qui est directement affecté à un service public a la qualité d'ouvrage public. Dans le cas où un ouvrage implanté sur le domaine public fait l'objet d'une convention d'occupation de ce domaine dont les stipulations prévoient expressément son affectation à une personne privée afin qu'elle y exerce une activité qui n'a pas le caractère d'un service public, le bien en cause ne peut plus être qualifié d'ouvrage public tant qu'il n'est pas de nouveau affecté à une activité publique, alors même que, n'ayant fait l'objet d'aucune procédure de déclassement, il n'a pas cessé de relever du domaine public.</ANA>
<ANA ID="9B"> 67-01-02-02 Un bien immeuble résultant d'un aménagement et qui est directement affecté à un service public a la qualité d'ouvrage public. Dans le cas où un ouvrage implanté sur le domaine public fait l'objet d'une convention d'occupation de ce domaine dont les stipulations prévoient expressément son affectation à une personne privée afin qu'elle y exerce une activité qui n'a pas le caractère d'un service public, le bien en cause ne peut plus être qualifié d'ouvrage public tant qu'il n'est pas de nouveau affecté à une activité publique, alors même que, n'ayant fait l'objet d'aucune procédure de déclassement, il n'a pas cessé de relever du domaine public.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, Assemblée, avis, 29 avril 2010, M. et Mme Béligaud, n°323179, p. 126. Comp., pour le cas où l'ouvrage public inutilisé n'a pas reçu d'affectation privée, CE, 29 décembre 2011, Mme Lahiton, n°333756, T. p. 847-1186.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
