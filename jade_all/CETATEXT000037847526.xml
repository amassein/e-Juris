<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037847526</ID>
<ANCIEN_ID>JG_L_2018_12_000000419483</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/84/75/CETATEXT000037847526.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème chambre, 21/12/2018, 419483, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-12-21</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>419483</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROCHETEAU, UZAN-SARANO</AVOCATS>
<RAPPORTEUR>M. Laurent Domingo</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Romain Victor</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:419483.20181221</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>La société par actions simplifiée Premium Automobiles a demandé au tribunal administratif de Châlons-en-Champagne de prononcer la restitution des cotisations de taxe sur les surfaces commerciales qu'elle a acquittées au titre des années 2010 et 2011 à raison de son établissement situé à Barberey-Saint-Sulpice (Aube). Par un jugement n° 1201949 du 24 mai 2016, ce tribunal a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 16NC01604 du 1er février 2018, enregistré le 30 mars 2018 au secrétariat du contentieux du Conseil d'Etat, la cour administrative d'appel de Nancy a transmis au Conseil d'État, en application de l'article R. 351-2 du code de justice administrative, le pourvoi formé par la société Premium Automobiles contre ce jugement, en tant qu'il concerne les impositions établies au titre de l'année 2011.<br/>
<br/>
              Par ce pourvoi et deux nouveaux mémoires, enregistrés le 25 juillet 2016 et le 22 mai 2017 au greffe de la cour administrative d'appel et le 26 juin 2018 au secrétariat du contentieux du Conseil d'Etat, la société Premium Automobiles demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement en tant qu'il a statué sur les impositions établies au titre de l'année 2011 ;<br/>
<br/>
              2°) réglant l'affaire au fond dans cette mesure, de faire droit à sa demande ; <br/>
<br/>
              3°) subsidiairement, d'ordonner à l'administration de produire, dans le respect des secrets protégés par la loi, tous documents constitutifs d'un échantillon représentatif de sa pratique en matière de détermination des surfaces taxables en matière de taxe sur les surfaces commerciales appliquée à des concessions automobiles ;<br/>
<br/>
              4°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code général des impôts et le livre des procédures fiscales ; <br/>
              - la loi n° 72-657 du 12 juillet 1972 ;<br/>
              - le décret n° 95-85 du 26 janvier 1995 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Laurent Domingo, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Romain Victor, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Rocheteau, Uzan-Sarano, avocat de la société Premium Automobiles.<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. La société Premium Automobiles, qui exerce une activité de vente de véhicules automobiles au sein d'un établissement situé à Barberey-Saint-Sulpice (Aube), a fait l'objet d'une vérification de comptabilité à l'issue de laquelle l'administration fiscale l'a assujettie à des rappels de taxe sur les surfaces commerciales au titre des années 2010 et 2011. Elle se pourvoit en cassation contre le jugement du 24 mai 2016 par lequel le tribunal administratif de Châlons-en-Champagne a rejeté sa demande, en tant qu'elle tendait à la décharge des impositions dues au titre de l'année 2011.<br/>
<br/>
              Sur la régularité de la procédure d'imposition :<br/>
<br/>
              2. Il ressort des écritures de la société devant le tribunal administratif que celle-ci ne contestait la motivation de la proposition de rectification que pour ce qui concerne les surfaces et le chiffre d'affaires à retenir pour établir et liquider la taxe en litige. Elle n'est par suite fondée à soutenir ni que le tribunal administratif aurait omis de répondre à un moyen tiré de ce que la proposition de rectification était irrégulière pour ne pas indiquer les raisons de droit et de fait pour lesquelles le vérificateur considérait qu'une concession automobile constitue un commerce de détail au sens de la loi du 13 juillet 1972, ni que le tribunal administratif aurait entaché son jugement de dénaturation des pièces du dossier et d'erreur de droit en jugeant la proposition de rectification suffisamment motivée sur ce point.<br/>
<br/>
              Sur le bien-fondé des impositions :<br/>
<br/>
              3. En premier lieu, aux termes de l'article 3 de la loi du 13 juillet 1972 instituant des mesures en faveur de certaines catégories de commerçants et artisans âgés, dans sa rédaction applicable aux années d'imposition en litige : " Il est institué une taxe sur les surfaces commerciales assise sur la surface de vente des magasins de commerce de détail, dès lors qu'elle dépasse 400 mètres carrés des établissements ouverts à partir du 1er janvier 1960 quelle que soit la forme juridique de l'entreprise qui les exploite. (...) / La surface de vente des magasins de commerce de détail, prise en compte pour le calcul de la taxe, et celle visée à l'article L. 720-5 du code de commerce, s'entendent des espaces affectés à la circulation de la clientèle pour effectuer ses achats, de ceux affectés à l'exposition des marchandises proposées à la vente, à leur paiement, et de ceux affectés à la circulation du personnel pour présenter les marchandises à la vente (...) / Si ces établissements, à l'exception de ceux dont l'activité principale est la vente ou la réparation de véhicules automobiles, ont également une activité de vente au détail de carburants, l'assiette de la taxe comprend en outre une surface calculée forfaitairement en fonction du nombre de position de ravitaillement dans la limite de 70 mètres carrés par position de ravitaillement. Le décret prévu à l'article 20 fixe la surface forfaitaire par emplacement à un montant compris entre 35 et 70 mètres carrés. / Pour les établissements dont le chiffre d'affaires au mètre carré est inférieur à 3 000 euros, le taux de cette taxe est de 5,74 euros au mètre carré de surface définie au troisième alinéa. Pour les établissements dont le chiffre d'affaires au mètre carré est supérieur à 12 000 euros, le taux est fixé à 34, 12 euros. / A l'exclusion des établissements qui ont pour activité principale la vente ou la réparation de véhicules automobiles, les taux mentionnés à l'alinéa précédent sont respectivement portés à 8,32 euros ou 35,70 euros (...). Un décret prévoira, par rapport aux taux ci-dessus, des réductions pour les professions dont l'exercice requiert des superficies de vente anormalement élevées ou, en fonction de leur chiffre d'affaires au mètre carré, pour les établissements dont la surface des locaux de vente destinés à la vente au détail est comprise entre 400 et 600 mètres carrés ". Aux termes de l'article 3 du décret du 26 janvier 1995 : " A. - La réduction de taux prévue au troisième alinéa de l'article 3 de la loi du 13 juillet 1972 susvisée en faveur des professions dont l'exercice requiert des superficies de vente anormalement élevées est fixée à 30 p. 100 en ce qui concerne la vente exclusive des marchandises énumérées ci-après (...) - véhicules automobiles ". Il résulte de la lettre même de ces dispositions, qui prévoient notamment des règles propres à ces établissements, que les établissements dont l'activité principale est la vente ou la réparation de véhicules automobiles, qu'ils soient neufs ou d'occasion, sont inclus dans le champ d'application de la taxe sur les surfaces commerciales. Le tribunal administratif, dont le jugement n'est pas insuffisamment motivé sur ce point, n'a par suite, contrairement à ce que soutient la société requérante, pas entaché son arrêt d'erreur de droit en jugeant que la vente de véhicules automobiles constituait une activité de commerce de détail au sens et pour l'application des dispositions précitées de la loi du 13 juillet 1972, sans qu'aient d'incidence à cet égard ni la circonstance que le bien vendu soit adapté aux exigences du client, ni la fourniture de services complémentaires.<br/>
<br/>
              4. En deuxième lieu, après avoir relevé par une appréciation souveraine non arguée de dénaturation, d'une part, que l'aire d'accueil du public et les bureaux de vente étaient des espaces affectés à la circulation de la clientèle pour y effectuer ses achats et, d'autre part, que l'espace d'attente contigu aux bureaux de vente représentait une zone indissociable de ces derniers, le tribunal administratif a pu légalement en déduire, par un jugement suffisamment motivé sur ce point, que cette partie des locaux de l'établissement était incluse dans les surfaces de vente au sens de la loi du 13 juillet 1972, devant être prises en compte pour l'assujettissement à la taxe.  <br/>
<br/>
               5. En troisième lieu, il résulte des dispositions précitées de la loi du 13 juillet 1972 et du décret du 26 janvier 1995, que le chiffre d'affaires à prendre en compte pour le calcul de la taxe sur les surfaces commerciales s'entend de celui correspondant à l'ensemble des ventes au détail en l'état réalisées par l'établissement, sans qu'il y ait lieu notamment de distinguer selon que ces ventes concernent ou non des biens qui sont présentés ou stockés dans cet établissement. Le tribunal, qui ne s'est pas mépris sur la portée des écritures de la société, n'a par suite pas commis d'erreur de droit en jugeant qu'il y avait lieu en l'espèce, pour calculer le chiffre d'affaires permettant de déterminer le taux applicable, de retenir les ventes réalisées par l'établissement correspondant à des véhicules qui, commandés et livrés ultérieurement, ne figuraient pas dans ses stocks. <br/>
<br/>
              6. Il résulte de tout ce qui précède que le pouvoir de la société Premium Automobiles doit être rejeté, y compris ses conclusions tendant à la mise en oeuvre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>                      D E C I D E :<br/>
                                    --------------<br/>
<br/>
Article 1er : Le pourvoi de la société Premium Automobiles est rejeté.<br/>
<br/>
Article 2 : La présente décision sera notifiée à la société par actions simplifiée Premium Automobiles et au ministre de l'action et des comptes publics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
