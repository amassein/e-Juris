<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032137613</ID>
<ANCIEN_ID>J6_L_2016_02_000001504962</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/13/76/CETATEXT000032137613.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>CAA de MARSEILLE, , 25/02/2016, 15MA04962, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-02-25</DATE_DEC>
<JURIDICTION>CAA de MARSEILLE</JURIDICTION>
<NUMERO>15MA04962</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>VINCENSINI</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
       Procédure contentieuse antérieure :<br/>
<br/>
       M. A... a demandé au tribunal administratif de Marseille d'annuler l'arrêté du 18 juin 2015 par lequel le préfet des Bouches-du-Rhône a refusé de lui délivrer un titre de séjour, a assorti ce refus d'une obligation de quitter le territoire français dans le délai de trente jours à compter de sa notification et a fixé le pays de destination.<br/>
<br/>
       Par un jugement n° 1505660 du 15 octobre 2015, le tribunal administratif de Marseille a rejeté sa demande.<br/>
<br/>
       Procédure devant la Cour :<br/>
<br/>
       Par une requête enregistrée le 24 décembre 2015, M. A..., représenté par Me C..., demande à la Cour :<br/>
<br/>
       1°) d'annuler ce jugement du 15 octobre 2015 du tribunal administratif de Marseille ;<br/>
<br/>
       2°) d'annuler l'arrêté en 18 juin 2015 du préfet des Bouches-du-Rhône ;<br/>
<br/>
       3°) d'enjoindre au préfet des Bouches-du-Rhône de lui délivrer un titre de séjour portant la mention " vie privée et familiale " et, à titre subsidiaire, d'instruire à nouveau sa demande dans un délai de quatre mois et de prendre une décision dans les quatre mois de la notification de la décision à intervenir, sous astreinte de 150 euros par jour de retard, ladite astreinte courant pendant un délai de trois mois, après lequel elle pourra être liquidée et une nouvelle astreinte fixée ;<br/>
<br/>
       4°) de mettre à la charge de l'Etat la somme de 1 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
       Il soutient que :<br/>
       - l'arrêté attaqué méconnaît les stipulations de l'article 8 de la convention européenne des droits de l'homme et des libertés fondamentales du 4 novembre 1950 et les dispositions de l'article L. 313-14 code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
       - il réside en France de façon continue depuis 2003, à l'exception d'un aller-retour de deux jours hors du territoire en 2010, quand il a été reconduit en Italie ; <br/>
       - il a construit sa vie familiale sur le territoire national puisque ses deux enfants y sont nés ; <br/>
       - il justifie d'une excellente insertion professionnelle ;<br/>
       - cette décision méconnait également les stipulations de l'article 3 de la convention internationale relative aux droits de l'enfant signée à New York du 26 janvier 1990, dès lors que ses enfants ne peuvent le suivre en Turquie, pays qu'ils ne connaissent pas.<br/>
<br/>
       Vu les autres pièces du dossier.<br/>
<br/>
       Vu : <br/>
       - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales du 4 novembre 1950 ;<br/>
       - la convention internationale relative aux droits de l'enfant signée à New York le 26 janvier 1990 ;<br/>
       - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
       - le code de justice administrative.<br/>
<br/>
<br/>
       Par décision du 1er septembre 2015, le président de la Cour a notamment désigné M. Jean-Louis Bédier, président, pour statuer, dans les conditions prévues par le deuxième alinéa de l'article R. 776-9 du code de justice administrative sur les litiges mentionnés à l'article R. 776-1 du même code.<br/>
<br/>
       1. Considérant qu'aux termes de l'article R. 776-9 du code de justice administrative : " (...) Le président de la cour administrative d'appel ou le magistrat qu'il désigne à cet effet peut statuer par ordonnance dans les cas prévus à l'article R. 222-1. Il peut, dans les mêmes conditions, rejeter les requêtes qui ne sont manifestement pas susceptibles d'entraîner l'infirmation de la décision attaquée " ;<br/>
<br/>
       2. Considérant que M. A..., de nationalité turque, demande à la Cour d'annuler le jugement du 15 octobre 2015 par lequel le tribunal administratif de Marseille a rejeté sa demande tendant à l'annulation de l'arrêté du 18 juin 2015 par lequel le préfet des Bouches-du-Rhône a refusé de lui délivrer un titre de séjour, a assorti ce refus d'une obligation de quitter le territoire français dans le délai de trente jours à compter de sa notification et a fixé le pays de destination ;<br/>
<br/>
       3. Considérant qu'aux termes de l'article 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales : " 1° Toute personne a droit au respect de sa vie privée et familiale, de son domicile et de sa correspondance ; 2° Il ne peut y avoir ingérence d'une autorité publique dans l'exercice de ce droit que pour autant que cette ingérence est prévue par la loi et qu'elle constitue une mesure qui, dans une société démocratique, est nécessaire à la sécurité nationale, à la sûreté publique, au bien-être économique du pays, à la défense de l'ordre et à la prévention des infractions pénales, à la protection de la santé ou de la morale ou à la protection des droits et libertés d'autrui " et qu'aux termes de l'article L. 313-14 du code de l'entrée et du séjour des étrangers et du droit d'asile : " La carte de séjour temporaire mentionnée à l'article L. 313-11 ou la carte de séjour temporaire mentionnée au 1° de l'article L. 313-10 sur le fondement du troisième alinéa de cet article peut être délivrée, sauf si sa présence constitue une menace pour l'ordre public, à l'étranger ne vivant pas en état de polygamie dont l'admission au séjour répond à des considérations humanitaires ou se justifie au regard des motifs exceptionnels qu'il fait valoir, sans que soit opposable la condition prévue à l'article L. 311-7 (...) " ;<br/>
<br/>
<br/>
       4. Considérant, en premier lieu, que M. A... soutient résider en France de façon continue depuis 2003 ; que, toutefois, sa présence en France n'est pas attestée au cours de l'année 2005 ; qu'elle ne peut non plus être regardée comme établie pour la période allant de décembre 2011 à septembre 2013 au vu d'une facture du 2 février 2012 surchargée de façon manuscrite et de courriers de divers organismes ou de relances pour factures impayées ; qu'en tout état de cause, la durée de présence dont fait état le requérant ne lui ouvrirait aucun droit particulier au séjour en France ; que si M. A... se prévaut d'être père de deux enfants nés sur le territoire français le 30 décembre 2010 et le 23 juillet 2012 où l'aînée est scolarisée, il n'est pas contesté, ainsi que cela ressort des pièces du dossier que sa compagne, mère de ses deux enfants et également de nationalité turque, se trouve elle-même en situation irrégulière ; que rien ne s'oppose, dans ces conditions, à la reconstitution de la cellule familiale en Turquie où M. A... n'établit ni même n'allègue être dépourvu d'attaches familiales ; qu'il s'ensuit que le requérant n'est pas fondé à soutenir que le préfet des Bouches-du-Rhône, en prenant l'arrêté attaqué, aurait porté à son droit au respect de sa vie privée et familiale une atteinte excessive et disproportionnée au but en vue duquel cette mesure a été décidée et que la décision de refus de séjour qui lui a été opposée méconnaîtrait les stipulations de l'article 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
<br/>
<br/>
       5. Considérant, en deuxième lieu, que les circonstances que l'intéressé ferait preuve d'un comportement un exempt de reproches et serait inconnu des services de police demeurent... ; qu'elles ne sauraient non plus constituer des considérations humanitaires ou des motifs exceptionnels au sens des dispositions précitées de l'article L. 313-14 du code de l'entrée et du séjour des étrangers et du droit d'asile justifiant une admission à titre exceptionnel au séjour ; que la décision du préfet ne se trouve, par suite, pas entachée d'erreur manifeste d'appréciation ;<br/>
<br/>
<br/>
       6. Considérant, en troisième lieu, qu'aux termes de l'article 3-1 de la convention relative aux droits de l'enfant signée à New York le 26 janvier 1990 : " Dans toutes les décisions qui concernent les enfants, qu'elles soient le fait d'institutions politiques ou privées de protection sociale, des tribunaux, des autorités administratives ou des organes législatifs, l'intérêt supérieur de l'enfant doit être une considération primordiale " ; <br/>
<br/>
<br/>
       7. Considérant que si M. A... soutient que ses enfants ne pourraient le suivre en Turquie, pays qu'ils ne connaissent pas, il ressort des pièces du dossier que ceux-ci étaient âgés à la date de l'arrêté attaqué de quatre et deux ans ; qu'en outre, et ainsi qu'il a été précisé au point 4, sa compagne est elle-même en situation irrégulière sur le territoire français ; qu'en conséquence rien ne s'oppose, à la reconstitution de la cellule familiale et de la scolarisation de leurs deux enfants en Turquie ; qu'ainsi l'arrêté attaqué n'a pas méconnu les stipulations précitées ;<br/>
<br/>
       8. Considérant qu'il résulte de tout ce qui précède que la requête de M. A... n'est manifestement pas susceptible d'entraîner l'infirmation du jugement attaqué ; que, par suite, ses conclusions à fin d'annulation de ce jugement doivent, en application de l'article R. 776-9 du code de justice administrative, être rejetées ; que, par voie de conséquence, il y a lieu de rejeter également ses conclusions aux fins d'injonction et d'astreinte ainsi que celles présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
ORDONNE :<br/>
Article 1er : La requête de M. A... est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à M. B... A.sans incidence sur la légalité de la décision en cause de même que le fait qu'il été embauché à plusieurs reprises en qualité de carreleur<br/>
<br/>
Copie en sera adressée au préfet des Bouches-du-Rhône.<br/>
Fait à Marseille, le 25 février 2016.<br/>
''<br/>
''<br/>
''<br/>
''<br/>
2<br/>
N° 15MA04962<br/>
<br/>
<br/>
<br/>
</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">335-01-03 Étrangers. Séjour des étrangers. Refus de séjour.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">335-03 Étrangers. Obligation de quitter le territoire français (OQTF) et reconduite à la frontière.
</SCT>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
