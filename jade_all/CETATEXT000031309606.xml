<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031309606</ID>
<ANCIEN_ID>JG_L_2015_10_000000374492</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/30/96/CETATEXT000031309606.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème et 10ème sous-sections réunies, 09/10/2015, 374492, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-10-09</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>374492</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème et 10ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROCHETEAU, UZAN-SARANO</AVOCATS>
<RAPPORTEUR>Mme Séverine Larere</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Aladjidi</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:374492.20151009</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B...A...a demandé au tribunal administratif de Besançon de prononcer la décharge des cotisations supplémentaires d'impôt sur le revenu et de contributions sociales auxquelles il a été assujetti au titre des années 2006 et 2007. Par un jugement n°1000219 du 26 mai 2011, le tribunal a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 11NC01232 du 21 novembre 2013, la cour administrative d'appel de Nancy a annulé le jugement du tribunal administratif de Besançon du 26 mai 2011 et a déchargé M. A...des impositions et pénalités contestées. <br/>
<br/>
              Par un pourvoi enregistré le 8 janvier 2014 au secrétariat du contentieux du Conseil d'Etat, le ministre délégué chargé du budget demande au Conseil d'Etat d'annuler cet arrêt.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - la loi n° 2008-1443 du 30 décembre 2008 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Séverine Larere, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Frédéric Aladjidi, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Rocheteau, Uzan-Sarano, avocat de M. A...;<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que M.A..., qui était l'actionnaire et le dirigeant de la société anonyme Machines A...a cédé, le 31 octobre 2006, l'intégralité des actions qu'il détenait dans cette société à la société Financière Daniel A...(FDP), dont il était actionnaire à hauteur de 86,70 % du capital, réalisant une plus-value de 571 464 euros ; que, le 20 décembre 2006, il a cédé la totalité des titres de la société FDP à la société Technologie Robotique A...(TRP), constituée le 6 novembre 2006, dont il était également actionnaire mais dont l'assemblée générale avait décidé, le 11 décembre précédent, de transformer les actions qu'il détenait en actions de préférence ne donnant ni droit de vote ni droit aux dividendes ; qu'à l'occasion de cette seconde cession, il a réalisé une plus-value de 14 615 189 euros, à laquelle s'est ajouté un complément de prix de 1 684 580 euros perçu en 2007 ; que, dans les déclarations de revenus qu'il a souscrites au titre des années 2006 et 2007, M.A..., qui a fait valoir ses droits à la retraite en avril 2007, a appliqué aux plus-values réalisées un abattement de 100 % en application des dispositions de l'article 150-0 D ter du code général des impôts ; qu'à l'issue d'un contrôle fiscal, l'administration a, toutefois, remis en cause le bénéfice de cet abattement et l'a assujetti à des cotisations supplémentaires d'impôt sur le revenu, assorties de pénalités, que M. A...a contestées ; que, par un jugement du 26 mai 2011, le tribunal administratif de Besançon a rejeté sa demande de décharge ; que, par l'arrêt attaqué du 21 novembre 2013, la cour administrative d'appel de Nancy a annulé ce jugement et l'a déchargé des impositions et pénalités contestées ;<br/>
<br/>
              Sur l'arrêt attaqué en tant qu'il statue sur l'imposition de la plus-value réalisée lors de la cession du 31 octobre 2006 :<br/>
<br/>
              2. Considérant qu'aux termes de l'article 150-0 D bis du code général des impôts dans sa rédaction alors applicable : " I. - 1. Les gains nets mentionnés au 1 de l'article 150-0 D et déterminés dans les conditions du même article retirés des cessions à titre onéreux d'actions, de parts de sociétés ou de droits démembrés portant sur ces actions ou parts sont réduits d'un abattement d'un tiers pour chaque année de détention au-delà de la cinquième, lorsque les conditions prévues au II sont remplies (...) " ; qu'aux termes du I de l'article 150-0 D ter du même code, dans sa rédaction alors applicable : " L'abattement prévu à l'article 150-0 D bis s'applique dans les mêmes conditions... aux gains nets réalisés lors de la cession à titre onéreux d'actions, de parts ou de droits démembrés portant sur ces actions ou parts, acquis ou souscrits avant le 1er janvier 2006, si les conditions suivantes sont remplies : (...) 4° En cas de cession des titres ou droits à une entreprise, le cédant ne doit pas détenir, directement ou indirectement, de droits de vote ou de droits dans les bénéfices sociaux de l'entreprise cessionnaire " ; que le IV de cet article précise que : " En cas de non-respect de la condition prévue au 4° du I à un moment quelconque au cours des trois années suivant la cession des titres ou droits, l'abattement prévu au même I est remis en cause au titre de l'année au cours de laquelle la condition précitée cesse d'être remplie. " ;<br/>
<br/>
              3. Considérant qu'il résulte des dispositions précitées de l'article 150-0 D ter du code général des impôts que la condition fixée au 4° du I de cet article s'apprécie à la date de la cession des titres ou droits visés par ces dispositions ; qu'il en résulte qu'en jugeant que cette condition devait être satisfaite, non à la date de la cession, mais au 31 décembre de l'année au cours de laquelle était intervenue la cession à l'origine de la plus-value imposable à l'impôt sur le revenu, la cour a commis une erreur de droit ; que son arrêt, doit, par suite, être annulé en tant qu'il a statué sur les conclusions de la requête de M. A...tendant à la décharge des impositions relatives à la plus-value réalisée lors de la cession du 31 octobre 2006 ;<br/>
<br/>
              Sur l'arrêt attaqué en tant qu'il statue sur l'imposition de la plus-value réalisée lors de la cession du 20 décembre 2006 :<br/>
<br/>
              4. Considérant qu'aux termes de l'article L. 64 du livre des procédures fiscales, dans sa rédaction antérieure à la loi du 30 décembre 2008 de finances rectificative pour 2008 : " Ne peuvent être opposés à l'administration des impôts les actes qui dissimulent la portée véritable d'un contrat ou d'une convention à l'aide de clauses ... qui déguisent soit une réalisation, soit un transfert de bénéfices ou de revenus ... L'administration est en droit de restituer son véritable caractère à l'opération litigieuse. En cas de désaccord sur les rectifications notifiées sur le fondement du présent article, le litige est soumis, à la demande du contribuable, à l'avis du comité consultatif pour la répression des abus de droit. L'administration peut également soumettre le litige à l'avis du comité dont les avis rendus feront l'objet d'un rapport annuel. / Si l'administration ne s'est pas conformée à l'avis du comité, elle doit apporter la preuve du bien-fondé du redressement." ; qu'il résulte de ces dispositions que, lorsque l'administration use de la faculté qu'elles lui confèrent dans des conditions telles que la charge de la preuve lui incombe, elle est fondée à écarter comme ne lui étant pas opposables certains actes passés par le contribuable, dès lors qu'elle établit que ces actes ont un caractère fictif ou que, recherchant le bénéfice d'une application littérale des textes à l'encontre des objectifs poursuivis par leurs auteurs, ils n'ont pu être inspirés par aucun autre motif que celui d'éluder ou d'atténuer les charges fiscales que l'intéressé, s'il n'avait pas passé ces actes, auraient normalement supportées, eu égard à sa situation ou à ses activités réelles ;<br/>
<br/>
              5. Considérant qu'aux termes du même article, dans sa rédaction issue du I de l'article 35 de la loi du 30 décembre 2008 de finances rectificative pour 2008 : " Afin d'en restituer le véritable caractère, l'administration est en droit d'écarter, comme ne lui étant pas opposables, les actes constitutifs d'un abus de droit, soit que ces actes ont un caractère fictif, soit que, recherchant le bénéfice d'une application littérale des textes ou de décisions à l'encontre des objectifs poursuivis par leurs auteurs, ils n'ont pu être inspirés par aucun autre motif que celui d'éluder ou d'atténuer les charges fiscales que l'intéressé, si ces actes n'avaient pas été passés ou réalisés, aurait normalement supportées eu égard à sa situation ou à ses activités réelles. / En cas de désaccord sur les rectifications notifiées sur le fondement du présent article, le litige est soumis, à la demande du contribuable, à l'avis du comité de l'abus de droit fiscal. L'administration peut également soumettre le litige à l'avis du comité. / Si l'administration ne s'est pas conformée à l'avis du comité, elle doit apporter la preuve du bien-fondé de la rectification. / Les avis rendus font l'objet d'un rapport annuel qui est rendu public " ; <br/>
<br/>
              6. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond, notamment de la proposition de rectification adressée à M. A... le 13 janvier 2009 et de la réponse aux observations du contribuable du 27 mars 2009, que l'administration a remis en cause l'abattement pratiqué par le contribuable, en application de l'article 150-0 D ter du code général des impôts, sur la plus-value réalisée à l'occasion de la cession de titres du 20 décembre 2006, au motif que la conversion des actions qu'il détenait dans la société TRP en actions dépourvues de droit de vote et de droit à dividendes, réalisée le 11 décembre 2006, était constitutive d'un montage ayant pour but exclusif d'éluder l'impôt ; qu'il résulte des dispositions citées aux points 4 et 5 qu'un tel motif, s'il était fondé, était de nature à justifier le redressement sur le fondement l'article L. 64 du livre des procédures fiscales, avant comme après la modification de cet article par l'article 35 de la loi du 30 décembre 2008 ; que, dès lors, la circonstance que l'administration s'est, à tort, référée, tant au cours de la procédure d'imposition que devant les juges du fond, aux dispositions de cet article dans leur rédaction antérieure à la loi de finances rectificative pour 2008, alors que, la proposition de rectification ayant été notifiée à M. A...postérieurement au 1er janvier 2009, ces dispositions n'étaient plus applicables, était sans incidence sur le bien-fondé des impositions en litige ; qu'il en résulte qu'en jugeant que ces impositions étaient, du fait de cette erreur, dépourvues de base légale, la cour a entaché son arrêt d'erreur de droit ;<br/>
<br/>
              7. Considérant qu'il résulte de tout ce qui précède que, sans qu'il soit besoin d'examiner l'autre moyen de son pourvoi, le ministre délégué, chargé du budget est fondé à demander l'annulation de l'arrêt qu'il attaque ; que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise, à ce titre, à la charge de l'Etat, qui n'est pas la partie perdante dans la présente instance ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Nancy du 21 novembre 2013 est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Nancy.<br/>
Article 3 : Les conclusions de M. A...présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée au ministre des finances et des comptes publics et à M. B... A....<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
