<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032750874</ID>
<ANCIEN_ID>JG_L_2016_06_000000395584</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/75/08/CETATEXT000032750874.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème chambre, 22/06/2016, 395584, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-06-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>395584</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Thomas Odinot</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Gilles Pellissier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2016:395584.20160622</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une protestation et un mémoire en réplique, enregistrés les 24 décembre 2015 et 4 mars 2016 au secrétariat du contentieux du Conseil d'Etat, M. F...G...demande au Conseil d'Etat de condamner MmeB..., tête de la liste " Notre sud en commun ", à fournir le coût entier d'une publication litigieuse sous astreinte de 1000 euros par jour de retard à compter de la notification de la décision à intervenir, de juger que ce coût doit être intégré dans les comptes de campagne de MmeB..., de demander à la Commission nationale des comptes de campagne et des financements politiques le rejet du compte de campagne de MmeB..., d'annuler l'ensemble des opérations électorales qui se sont déroulées les 6 et 13 décembre 2015 dans la région Languedoc-Roussillon et Midi-Pyrénées et de prononcer l'inéligibilité de Mme B....<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ; <br/>
<br/>
              Vu :<br/>
              - le code électoral ;<br/>
              - la loi n° 2015-29 du 16 janvier 2015 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Thomas Odinot, auditeur,  <br/>
<br/>
              - les conclusions de M. Gilles Pellissier, rapporteur public ;<br/>
<br/>
<br/>
<br/>Sur la fin de non-recevoir opposée par Mme B...à la protestation de M. G... :<br/>
<br/>
              1. Considérant qu'aux termes du premier alinéa de l'article L. 361 du code électoral : " Les élections au conseil régional peuvent être contestées dans les dix jours suivant la proclamation des résultats par tout candidat ou tout électeur de la région devant le Conseil d'Etat statuant au contentieux " ;<br/>
<br/>
              2. Considérant qu'il résulte de l'instruction que les résultats du second tour des opérations électorales qui ont eu lieu le 13 décembre 2015 pour la désignation des conseillers régionaux de la région Languedoc-Roussillon et Midi-Pyrénées ont été proclamés le 14 décembre 2015 ; qu'en vertu des dispositions précitées de l'article L. 361 du code électoral, le délai de recours contre ces opérations expirait le 24 décembre 2015 à minuit ; que, par les pièces jointes à sa note en délibéré, produite après l'audience du 18 mai 2016 et qui a conduit à la réouverture de l'instruction, M. G...établit avoir déposé et envoyé par télécopie sa protestation au Conseil d'Etat le 24 décembre 2015 ; que, par suite, Mme B...n'est pas fondée à soutenir que la protestation de M. G...serait irrecevable au motif qu'elle serait tardive ; <br/>
<br/>
              Sur le bien fondé des griefs soulevés : <br/>
<br/>
              3. Considérant, en premier lieu, qu'aux termes du second alinéa de l'article L. 52-1 du code électoral : " Pendant les six mois précédant le premier jour du mois d'une élection et jusqu'à la date du tour de scrutin où celle-ci est acquise, l'utilisation à des fins de propagande électorale de tout procédé de publicité commerciale par la voie de la presse ou par tout moyen de communication audiovisuelle est interdite. A compter du premier jour du sixième mois précédant le mois au cours duquel il doit être procédé à des élections générales, aucune campagne de promotion publicitaire des réalisations ou de la gestion d'une collectivité ne peut être organisée sur le territoire des collectivités intéressées par le scrutin (...) " ; qu'aux termes du second alinéa de l'article L. 52-8 du même code : " Les personnes morales, à l'exception des partis ou groupements politiques, ne peuvent participer au financement de la campagne électorale d'un candidat, ni en lui consentant des dons sous quelque forme que ce soit, ni en lui fournissant des biens, services ou autres avantages directs ou indirects à des prix inférieurs à ceux qui sont habituellement pratiqués (...) " ; <br/>
<br/>
              4. Considérant qu'il résulte de l'instruction qu'une plaquette présentant le projet de création d'un parc naturel régional " Comminges Pyrénées " élaborée par l'association pour le développement en Comminges Pyrénées, constituée de onze intercommunalités du secteur, a été éditée en février 2015 ; que cette plaquette, qui se borne à décrire la vocation du projet de parc et son périmètre et à expliciter le financement et le fonctionnement d'un parc naturel régional, ne fait aucune mention de l'action éventuelle du conseil régional de Midi-Pyrénées à ce sujet, ni des futures opérations électorales dans la nouvelle région ; que si Mme B... a publié un article de soutien dans cette plaquette, elle a signé l'article en sa seule qualité de secrétaire d'Etat chargée du commerce, de l'artisanat, de la consommation et de l'économie sociale et solidaire et, selon les termes même de la publication contestée, " en tant qu'élue de ce territoire ", sans faire état de sa qualité de candidate aux prochaines élections et en se bornant à décrire les raisons de son soutien au projet ; qu'une telle publication, qui ne comporte aucun élément de bilan ou de polémique électorale et dont le requérant n'apporte au demeurant aucun élément de nature à montrer l'ampleur de la diffusion postérieurement au 1er juin, date du premier jour du sixième mois précédant les élections régionales, ne saurait, par son contenu, être regardée comme revêtant le caractère d'une campagne de promotion publicitaire des actions du conseil régional au sens des dispositions du second alinéa de l'article L. 52-1 du code électoral citées au point 3 ; qu'il suit de là que les dépenses exposées par l'association pour la publication et la diffusion de cette plaquette ne sauraient être regardées comme des dons ou des avantages en nature consentis par une personne morale à la liste conduite par Mme B...en violation des dispositions de l'article L. 52-8 du code électoral ; <br/>
<br/>
              5. Considérant, en second lieu, que le grief tiré de ce que les dispositions de l'article L. 52-4 du code électoral, qui dispose que pendant l'année qui précède le premier jour du mois de l'élection, un mandataire déclaré par tout candidat doit comptabiliser les recettes et les dépenses en vue de l'élection dans un compte de campagne, ont été méconnues par Mme B... n'est assorti d'aucune précision de nature à en établir le bien-fondé ; que ce grief doit, par conséquent, être écarté ; <br/>
<br/>
              6. Considérant qu'il résulte de ce qui précède que la protestation de M. G... doit être rejetée ; <br/>
<br/>
              Sur les conclusions tendant à l'application des dispositions de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              7. Considérant qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par Mme B...au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La protestation de M. G...est rejetée.<br/>
Article 2 : Les conclusions de Mme B...présentées sur le fondement des dispositions de l'article L. 761-1 du code de justice administrative  sont rejetées. <br/>
Article 3 : La présente décision sera notifiée à M. F...G..., à Mme D...B..., à M. H...E..., à M. C...A...et au ministre de l'intérieur.<br/>
Copie en sera adressée pour information à la Commission nationale des comptes de campagne et des financements politiques.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
