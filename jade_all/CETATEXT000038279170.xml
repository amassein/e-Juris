<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038279170</ID>
<ANCIEN_ID>JG_L_2019_03_000000426472</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/27/91/CETATEXT000038279170.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Section, 27/03/2019, 426472, Publié au recueil Lebon</TITRE>
<DATE_DEC>2019-03-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>426472</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Section</FORMATION>
<TYPE_REC>Autres</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>LE PRADO ; SCP GOUZ-FITOUSSI</AVOCATS>
<RAPPORTEUR>M. Alexandre Lallet</RAPPORTEUR>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CESEC:2019:426472.20190327</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par un jugement n° 1700229 du 11 décembre 2018, le tribunal administratif de Châlons-en-Champagne, avant de statuer sur la demande de M. et Mme D...tendant à la condamnation du centre hospitalier universitaire de Reims à réparer les préjudices qu'ils auraient subis du fait d'une infection nosocomiale contractée dans cet établissement, a décidé, par application des dispositions de l'article L. 113-1 du code de justice administrative, de transmettre le dossier de cette demande au Conseil d'Etat, en soumettant à son examen les questions suivantes :<br/>
<br/>
              1°) Lorsqu'une demande indemnitaire a été adressée à l'administration avant la saisine du juge administratif, mais qu'à la date de cette saisine aucune décision statuant sur cette demande n'est encore intervenue - notamment pas une décision implicite de rejet -, les dispositions du second alinéa de l'article R. 421-1 du code de justice administrative doivent-elles être interprétées en ce sens qu'elles excluent toute possibilité de régularisation par la liaison du contentieux en cours d'instance '<br/>
<br/>
              2°) En cas de réponse négative à la première question :<br/>
<br/>
              a. La circonstance que l'administration aurait opposé une fin de non-recevoir, notamment tirée du défaut de liaison du contentieux, avant l'intervention d'une décision sur la demande préalable s'opposerait-elle à la régularisation de la requête du fait de l'intervention en cours d'instance de cette décision '<br/>
<br/>
              b. La régularisation de la requête résulterait-elle seulement de l'intervention en cours d'instance de cette décision, ou nécessiterait-elle que le requérant présente des écritures réitérant ses conclusions indemnitaires '<br/>
<br/>
              3°) En cas de réponse positive à la première question, le juge devrait-il se borner à constater que des écritures présentées par le requérant en cours d'instance, après l'intervention d'une décision statuant sur sa demande préalable, à l'effet de réitérer ses conclusions indemnitaires, sont sans incidence sur l'irrecevabilité de la requête, ou lui appartiendrait-il alors de regarder ces écritures comme constituant une nouvelle requête, à traiter comme telle ' <br/>
<br/>
              Les consorts D...ont présenté des observations enregistrées le 22 janvier 2019 au secrétariat du contentieux du Conseil d'Etat.<br/>
<br/>
              Le centre hospitalier universitaire de Reims a présenté des observations enregistrées le 31 janvier 2019 au secrétariat du contentieux du Conseil d'Etat. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de justice administrative, notamment ses articles L. 113-1 et R. 421-1 ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Alexandre Lallet, maître des requêtes, <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public ;<br/>
<br/>
              - La parole ayant été donnée, avant et après les conclusions, à la SCP Gouz-Fitoussi, avocat des Consorts D...et à Me Le Prado, avocat du centre hospitalier universitaire de Reims ;<br/>
<br/>
<br/>
<br/>REND L'AVIS SUIVANT :<br/>
<br/>
              1. Aux termes de l'article R. 421-1 du code de justice administrative, dans sa rédaction résultant du décret n° 2016-1480 du 2 novembre 2016 portant modification du code de justice administrative : " La juridiction ne peut être saisie que par voie de recours formé contre une décision, et ce, dans les deux mois à partir de la notification ou de la publication de la décision attaquée. / Lorsque la requête tend au paiement d'une somme d'argent, elle n'est recevable qu'après l'intervention de la décision prise par l'administration sur une demande préalablement formée devant elle ". <br/>
<br/>
              2. Il résulte de ces dispositions qu'en l'absence d'une décision de l'administration rejetant une demande formée devant elle par le requérant ou pour son compte, une requête tendant au versement d'une somme d'argent est irrecevable et peut être rejetée pour ce motif même si, dans son mémoire en défense, l'administration n'a pas soutenu que cette requête était irrecevable, mais seulement que les conclusions du requérant n'étaient pas fondées.<br/>
<br/>
              3. En revanche, les termes du second alinéa de l'article R. 421-1 du code de justice administrative n'impliquent pas que la condition de recevabilité de la requête tenant à l'existence d'une décision de l'administration s'apprécie à la date de son introduction. Cette condition doit être regardée comme remplie si, à la date à laquelle le juge statue, l'administration a pris une décision, expresse ou implicite, sur une demande formée devant elle. Par suite, l'intervention d'une telle décision en cours d'instance régularise la requête, sans qu'il soit nécessaire que le requérant confirme ses conclusions et alors même que l'administration aurait auparavant opposé une fin de non-recevoir fondée sur l'absence de décision.<br/>
<br/>
              Le présent avis sera notifié au tribunal administratif de Châlons-en-Champagne, à Mme E...D..., M. B...D..., Mme C...D...et M. A...D..., au centre hospitalier universitaire de Reims, à la caisse primaire d'assurance maladie des Pyrénées-Atlantiques, à la caisse primaire d'assurance maladie du Maine et Loire, à la garde des sceaux, ministre de la justice et à la ministre des solidarités et de la santé. Il sera publié au Journal officiel de la République française.<br/>
<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-01-02 PROCÉDURE. INTRODUCTION DE L'INSTANCE. LIAISON DE L'INSTANCE. - OBLIGATION DE FAIRE NAÎTRE UNE DÉCISION ADMINISTRATIVE PRÉALABLE À L'INTRODUCTION D'UNE REQUÊTE TENDANT AU VERSEMENT D'UNE SOMME D'ARGENT (ART. 421-1 DU CJA) - EXIGENCE À PEINE D'IRRECEVABILITÉ DE LA REQUÊTE, MÊME SI L'ADMINISTRATION S'EST BORNÉE À DÉFENDRE AU FOND [RJ1] - RÉGULARISATION DE LA REQUÊTE EN CAS D'INTERVENTION DE LA DÉCISION EN COURS D'INSTANCE [RJ2].
</SCT>
<ANA ID="9A"> 54-01-02 Il résulte de l'article R. 421-1 du code de justice administrative (CJA), dans sa rédaction résultant du décret n° 2016-1480 du 2 novembre 2016 (dit JADE), qu'en l'absence d'une décision de l'administration rejetant une demande formée devant elle par le requérant ou pour son compte, une requête tendant au versement d'une somme d'argent est irrecevable et peut être rejetée pour ce motif même si, dans son mémoire en défense, l'administration n'a pas soutenu que cette requête était irrecevable, mais seulement que les conclusions du requérant n'étaient pas fondées.,,En revanche, les termes du second alinéa de l'article R. 421-1 du CJA n'impliquent pas que la condition de recevabilité de la requête tenant à l'existence d'une décision de l'administration s'apprécie à la date de son introduction. Cette condition doit être regardée comme remplie si, à la date à laquelle le juge statue, l'administration a pris une décision, expresse ou implicite, sur une demande formée devant elle. Par suite, l'intervention d'une telle décision en cours d'instance régularise la requête, sans qu'il soit nécessaire que le requérant confirme ses conclusions et alors même que l'administration aurait auparavant opposé une fin de non-recevoir fondée sur l'absence de décision.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Ab. jur., sur ce point, CE, 18 février 1959, Ville de Roubaix, n° 37634, p. 125 ; CE, Assemblée, 23 avril 1965,,, n° 60721, p. 231.,,[RJ1] Cf., avant l'intervention du décret dit JADE, CE, 20 février 2002,,, n° 217057, T. pp. 841-945 ; CE, 11 avril 2008, Etablissement français du sang, n° 281374, p. 168.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
