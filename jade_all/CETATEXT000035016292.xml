<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000035016292</ID>
<ANCIEN_ID>JG_L_2017_06_000000404362</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/35/01/62/CETATEXT000035016292.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère chambre, 26/06/2017, 404362, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-06-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>404362</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BRIARD</AVOCATS>
<RAPPORTEUR>M. Yannick Faure</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Rémi Decout-Paolini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2017:404362.20170626</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              M. B...A...a demandé au tribunal administratif de Toulouse d'annuler :<br/>
              - la décision du 19 avril 2013 par laquelle le président du conseil général de la Haute-Garonne a rejeté, pour ce qui concerne le département, son recours dirigé contre la décision du directeur de la caisse d'allocations familiales de ce département du 6 février 2013 lui notifiant des indus de revenu de solidarité active pour la période du 1er février 2011 au 30 novembre 2012 et ne lui a accordé qu'une remise partielle de l'indu constaté à l'égard du département, ramené à 9 352,41 euros ;<br/>
              - la décision du 24 avril 2013 par laquelle le directeur de la caisse d'allocations familiales de la Haute-Garonne a rejeté, pour ce qui concerne l'Etat, son recours dirigé contre la décision du 6 février 2013 et ne lui a accordé qu'une remise partielle de l'indu constaté à l'égard de l'Etat, ramené à 2 987,42 euros.<br/>
<br/>
              Par un jugement n° 1304048 du 5 août 2016, le tribunal administratif de Toulouse a fait droit à sa demande.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 11 octobre 2016 et 11 janvier 2017 au secrétariat du contentieux du Conseil d'Etat, le département de la Haute-Garonne demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter la demande de M.A... ;<br/>
<br/>
              3°) de mettre à la charge de M. A...la somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de l'action sociale et des familles ;<br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Yannick Faure, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Rémi Decout-Paolini, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Briard, avocat du département de la Haute-Garonne.<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis au juge du fond que, suite à une décision du président du conseil général de la Haute-Garonne du 3 décembre 2012 estimant que M.A..., ressortissant roumain, ne remplissait pas la condition de droit de séjour à laquelle le bénéfice du revenu de solidarité active est subordonné, la caisse d'allocations familiales de ce département a notifié à M.A..., par décision du 6 février 2013, des indus d'un montant de 34 487,79 euros, pour la période du 1er février 2011 au 30 novembre 2012, au titre du revenu de solidarité active, ainsi que de la prime exceptionnelle de fin d'année et des prestations familiales. Par une lettre du 25 mars 2013, M. A...a formé un recours administratif contre la décision de cette caisse et sollicité une remise gracieuse de ces indus. Par décision du 19 avril 2013, le président du conseil général de la Haute-Garonne, statuant au titre du recours administratif préalable obligatoire institué par l'article L. 262-47 du code de l'action sociale et des familles, a ramené d'un montant de 11 690,52 euros à un montant de 9 352,41 euros l'indu de revenu de solidarité active, dit " socle ", constaté au titre du montant forfaitaire mentionné au 2° de l'article L. 262-2 du même code. Par une décision du 24 avril 2013, la caisse d'allocations familiales de Haute-Garonne, agissant pour le compte de l'Etat, a ramené l'indu de revenu de solidarité active, dit " activité ", constaté au titre de la fraction de ses revenus professionnels mentionnée au 1° de l'article L. 262-2, d'un montant de 3 734,27 euros à un montant de 2 987,42 euros. Par un jugement du 5 août 2016, à la demande de M.A..., le tribunal administratif de Toulouse a annulé les décisions confirmant le bien-fondé des indus de revenu de solidarité active réclamés. Le département de la Haute-Garonne doit être regardé comme formant un pourvoi en cassation contre ce jugement en tant qu'il statue sur l'indu de revenu de solidarité active dit " socle ", ramené par le président du conseil général à un montant de 9 352,41 euros.<br/>
<br/>
              2. Lorsque le juge administratif est saisi d'un recours dirigé contre une décision qui, remettant en cause des paiements déjà effectués, ordonne la récupération d'un indu de revenu de solidarité active, il entre dans son office d'apprécier, au regard de l'argumentation du requérant, le cas échéant, de celle développée par le défendeur et, enfin, des moyens d'ordre public, en tenant compte de l'ensemble des circonstances de fait qui résultent de l'instruction, la régularité comme le bien-fondé de la décision de récupération d'indu. Pour apprécier le bien-fondé de cette décision, il examine les droits du requérant au revenu de solidarité active au cours de la période ayant donné lieu au constat d'un indu, au regard des textes applicables à cette période.<br/>
<br/>
              3. En se fondant, pour faire droit à la demande présentée par M.A..., sur la circonstance que les mesures transitoires, en vertu desquelles l'article L. 121-2 du code de l'entrée et du séjour des étrangers et du droit d'asile imposait la détention d'un titre de séjour, avaient pris fin depuis le 1er janvier 2014 pour les ressortissants roumains, alors que l'indu litigieux concernait la période du 1er février 2011 au 30 novembre 2012, durant lesquelles ces mesures transitoires demeuraient applicables, le tribunal administratif de Toulouse a commis une erreur de droit.<br/>
<br/>
              4. Il résulte de ce qui précède, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, que le département de la Haute-Garonne est fondé à demander l'annulation du jugement qu'il attaque en tant qu'il statue sur l'indu de revenu de solidarité active constaté au titre du montant forfaitaire mentionné au 2° de l'article L. 262-2 du code de l'action sociale et des familles.<br/>
<br/>
              5. Il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions du département de la Haute-Garonne présentées au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'article 1er du jugement du tribunal administratif de Toulouse du 5 août 2016 est annulé en tant qu'il statue sur l'indu de revenu de solidarité active constaté au titre du montant forfaitaire mentionné au 2° de l'article L. 262-2 du code de l'action sociale et des familles, ramené à un montant de 9 352,41 euros par décision du président du conseil général de la Haute-Garonne du 19 avril 2013.<br/>
Article 2 : L'affaire est renvoyée, dans la mesure de la cassation ainsi prononcée, au tribunal administratif de Toulouse.<br/>
Article 3 : Les conclusions du département de la Haute-Garonne présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée au département de la Haute-Garonne et à M. B...A....<br/>
Copie en sera adressée à la ministre des solidarités et de la santé et à la caisse d'allocations familiales de la Haute-Garonne. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
