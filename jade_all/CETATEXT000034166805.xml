<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034166805</ID>
<ANCIEN_ID>JG_L_2017_03_000000408394</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/16/68/CETATEXT000034166805.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 06/03/2017, 408394, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-03-06</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>408394</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2017:408394.20170306</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, enregistrée le 27 février 2017 au secrétariat du contentieux du Conseil d'Etat, M. D... A...et M. C...B...demandent au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative :<br/>
<br/>
              1°) d'ordonner la suspension de l'exécution du décret n° 2016-1460 du 28 octobre 2016 autorisant la création d'un traitement de données à caractère personnel relatif aux passeports et aux cartes nationales d'identité ;<br/>
<br/>
              2°) d'ordonner la suspension de l'exécution de l'arrêté du 9 février 2017 portant application du décret n° 2016-1460 du 28 octobre 2016 autorisant la création d'un traitement de données à caractère personnel relatif aux passeports et aux cartes nationales d'identité ;<br/>
              3°) de mettre à la charge de l'Etat la somme de 1 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Ils soutiennent que :<br/>
              - leur requête est recevable dès lors que, d'une part, le Conseil d'Etat est compétent en premier et dernier ressort pour connaître des recours dirigés contre les décrets, d'autre part, ils justifient d'un intérêt à agir en tant que citoyens français titulaires d'un passeport et/ou d'une carte d'identité et, enfin, les recours en annulation introduits au fond contre les actes contestés sont recevables ; <br/>
              - la condition d'urgence est remplie dès lors que l'exécution des actes contestés, et particulièrement de l'arrêté du 9 février 2017, crée, d'une part, une situation difficilement réversible et, d'autre part, porte atteinte, en premier lieu, au droit à la vie privée des requérants, en deuxième lieu, à la situation que M. A...entend défendre et, en dernier lieu, à l'intérêt général ;<br/>
              - il existe un doute sérieux quant à la légalité des actes contestés ;<br/>
              - le décret et l'arrêté attaqués sont entachés d'un vice de procédure en ce que l'avis conforme de la direction interministérielle des systèmes d'information et de communication de l'Etat n'a pas été recueilli ; <br/>
              - ils sont entachés d'une erreur de droit dès lors qu'ils méconnaissent la loi du 6 janvier 1978 en n'assurant pas le consentement de la personne concernée au traitement de données à caractère personnel ; <br/>
              - ils méconnaissent le droit au respect de la vie privée, protégé par la Constitution, la convention européenne de sauvegarde des droits de l'Homme et libertés fondamentales et la charte des droits fondamentaux de l'Union européenne et le règlement n° 2252/2004 ; <br/>
              - ils sont illégaux dès lors que le décret du 28 octobre 2016 modifie de façon substantielle la définition du contenu des titres électroniques sécurisés prévue par la loi du 27 mars 2012 ;<br/>
              - ils sont entachés d'un détournement de pouvoir dès lors que leur objectif réel est le fichage généralisé de la population française ; <br/>
              - ils sont entachés d'une erreur de droit en ce qu'ils méconnaissent le principe de proportionnalité ;<br/>
              - ils sont entachés d'une erreur de fait dès lors que le ministre de l'intérieur n'a pas pris en compte la réalité de l'insécurité permanente provoquée par la création d'un fichier généralisé et n'a pas vérifié si des solutions alternatives plus sûres étaient disponibles sur le marché. <br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la Constitution, notamment son Préambule ;<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - la charte des droits fondamentaux de l'Union européenne ; <br/>
              - le règlement (CE) n° 2254/2004 du Conseil du 13 décembre 2004 ;<br/>
              - la loi n° 78-17 du 6 janvier 1978 ;<br/>
              - la loi n° 2012-410 du 27 mars 2012 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>1. Aux termes de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision ". En vertu de l'article L. 522-3 du même code, le juge des référés peut, par une ordonnance motivée, rejeter une requête sans instruction ni audience lorsque la condition d'urgence n'est pas remplie ou lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable ou qu'elle est mal fondée. L'urgence justifie que soit prononcée la suspension d'un acte administratif lorsque l'exécution de celui-ci porte atteinte, de manière suffisamment grave et immédiate, à un intérêt public, à la situation du requérant ou aux intérêts qu'il entend défendre. Il appartient au juge des référés d'apprécier concrètement, compte tenu des justifications fournies par le requérant, si les effets de l'acte litigieux sont de nature à caractériser une urgence justifiant que, sans attendre le jugement de la requête au fond, l'exécution de la décision soit suspendue. <br/>
<br/>
              2. En premier lieu, les requérants soutiennent que leur demande de suspension de l'exécution du décret du 28 octobre 2016 est recevable. Toutefois, il ressort des pièces du dossier que la demande tendant à l'annulation de ce décret, publié le 30 octobre 2016, n'a été enregistrée au secrétariat du contentieux du Conseil d'Etat que le 24 février 2017, soit après l'expiration du délai de deux mois prévu à l'article R. 421-1 du code de justice administrative. Cette demande est tardive et, par suite, irrecevable. Dans ces conditions, la demande de suspension de l'exécution de ce décret ne peut être accueillie.  <br/>
<br/>
              3. En second lieu, pour justifier l'urgence qui s'attache à ce que soit ordonnée la suspension de l'exécution de l'arrêté du 9 février 2017, les requérants soutiennent que celui-ci porte une atteinte grave et immédiate à leur intérêt personnel, aux intérêts qu'entend défendre M. A... et à l'intérêt général. Toutefois, les dispositions de caractère règlementaire de l'arrêté du 9 février 2017 ne font apparaître, par elles-mêmes, aucune situation d'urgence ni au regard de l'intérêt général ni au regard des intérêts des requérants. <br/>
              4. Il résulte de tout ce qui précède que M. A...et M. B...ne sont pas fondés à demander la suspension de l'exécution des actes contestés. Par suite, leur requête, y compris les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative, doit être rejetée selon la procédure prévue par l'article L. 522-3 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de M. A...et M. B...est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à M. D...A...et M. C...B....<br/>
Copie en sera adressée au Premier ministre et au ministre de l'intérieur. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
