<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029476922</ID>
<ANCIEN_ID>JG_L_2014_09_000000362660</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/47/69/CETATEXT000029476922.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème / 5ème SSR, 19/09/2014, 362660</TITRE>
<DATE_DEC>2014-09-19</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>362660</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème / 5ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MASSE-DESSEN, THOUVENIN, COUDRAY ; SCP GATINEAU, FATTACCINI</AVOCATS>
<RAPPORTEUR>Mme Pauline Pannier</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Gaëlle Dumortier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:362660.20140919</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 11 septembre et 10 décembre 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour Mme A...B...demeurant ...; Mme B... demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler l'arrêt n° 11MA00206 du 10 juillet 2012 par lequel la cour administrative d'appel de Marseille a, d'une part, annulé le jugement n° 0301740 du 10 février 2006 par lequel le tribunal administratif de Nice a annulé les décisions des 7 et 8 août 2002 de l'inspectrice du travail de la 2ème section des Alpes-Maritimes autorisant le licenciement pour faute de Mme B...et la décision du ministre des affaires sociales, du travail et de la solidarité du 7 février 2003 confirmant cette autorisation et, d'autre part, rejeté la demande de première instance de Mme B...tendant à l'annulation de ces décisions ; <br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de la société Amadeus ;<br/>
<br/>
              3°) de mettre à la charge de la société Amadeus et de l'Etat la somme de 4 500 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code du travail ;<br/>
<br/>
              Vu la loi n° 2002-1062 du 6 août 2002 ;<br/>
<br/>
              Vu le code de justice administrative ; <br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Pauline Pannier, auditeur,<br/>
<br/>
              - les conclusions de Mme Gaëlle Dumortier, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Masse-Dessen, Thouvenin, Coudray, avocat de Mme B...et à la SCP Gatineau, Fattaccini, avocat de la société Amadeus ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, dans le cadre d'une première demande d'autorisation de licenciement pour faute, la société Amadeus, a convoqué MmeB..., salariée protégée, à un entretien préalable fixé le 26 mai 2000, auquel elle ne s'est pas présentée ; que, par une décision du 30 octobre 2000, l'inspecteur du travail a autorisé son licenciement qui a été prononcé le 13 novembre 2000 ; que, cette décision ayant été annulée par le tribunal administratif de Nice le 19 mars 2002 au motif que la demande d'autorisation avait omis de mentionner l'un des mandats de l'intéressée, Mme B... a été réintégrée dans l'entreprise le 24 mai 2002 ; que la société Amadeus a alors présenté une nouvelle demande d'autorisation de licenciement le 12 juin 2002 ; que l'inspecteur du travail a délivré cette autorisation par une décision du 8 août 2002, confirmée par le ministre le 7 février 2003 ; que, saisi par MmeB..., le tribunal administratif de Nice a annulé ces décisions d'autorisation par un jugement du 10 février 2006, confirmé par la cour administrative d'appel de Marseille le 22 mai 2008 ; que cet arrêt a fait l'objet d'un pourvoi en cassation de la société Amadeus devant le Conseil d'Etat qui, par une décision du 15 décembre 2010, l'a annulé au motif qu'il était entaché d'une erreur de droit quant au point de départ du délai de prescription prévu à l'article L. 122-44 du code du travail, et a renvoyé l'affaire devant la même cour ; que par un arrêt du 10 juillet 2012, la cour a annulé le jugement du tribunal administratif de Nice et rejeté la demande de première instance de Mme B..., qui se pourvoit en cassation contre cet arrêt ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article L. 1232-2 du code du travail, " l'employeur qui envisage de licencier un salarié le convoque, avant toute décision, à un entretien préalable " ; qu'en vertu de l'article R. 2421-8 du même code, cet entretien doit précéder la présentation de la demande d'autorisation de licenciement à l'inspecteur du travail ; <br/>
<br/>
              Sur l'obligation pour l'employeur d'organiser un nouvel entretien préalable :<br/>
<br/>
              3. Considérant que, si Mme B...soutient que la cour administrative d'appel a commis une erreur de droit en jugeant que la société Amadeus n'était pas tenue de la convoquer à un entretien préalable dans le cadre de la seconde procédure de licenciement pour motif disciplinaire, il ressort des pièces du dossier que les motifs invoqués dans la demande d'autorisation présentée le 12 juin 2002 étaient identiques à ceux de la demande du 29 juin 2000 pour laquelle la salariée avait été régulièrement convoquée à un entretien préalable ; que, dans ces circonstances, la cour a pu, sans erreur de droit, juger que l'écoulement d'un délai de deux ans entre les deux demandes d'autorisation ne constituait pas à lui seul une circonstance de fait nouvelle imposant que soit organisé un nouvel entretien préalable ; <br/>
<br/>
              Sur la méconnaissance de la loi d'amnistie :<br/>
<br/>
              4. Considérant qu'aux termes de l'article 11 de la loi du 6 août 2002, dont les dispositions sont entrées en vigueur le 10 août 2002 : " sont amnistiés les faits commis avant le 17 mai 2002 en tant qu'ils constituent des fautes passibles de sanctions disciplinaires ou professionnelles " ; qu'aux termes de l'article 12 de la loi : " sont amnistiés, dans les conditions prévues à l'article 11, les faits retenus ou susceptibles d'être retenus comme motifs de sanctions prononcées par un employeur. / L'inspection du travail veille à ce qu'il ne puisse être fait état des faits amnistiés " ;<br/>
<br/>
              5. Considérant que Mme B...soutient que la cour administrative d'appel de Marseille a commis une erreur de droit en ne relevant pas d'office que le ministre qui, par sa décision du 7 février 2003, ne s'est pas borné à confirmer l'autorisation délivrée par l'inspecteur du travail le 8 août 2002, s'est prononcé lui-même au fond sans prendre en compte les effets de la loi d'amnistie du 6 août 2002 entrée en vigueur le 10 août 2002 ; qu'en tout état de cause, il ressort des pièces du dossier que la décision du ministre ne fait que confirmer la décision de l'inspecteur du travail, qui a statué avant l'entrée en vigueur de la loi d'amnistie ; que le ministre, qui devait se prononcer sur la légalité de la décision de l'inspecteur du travail, n'aurait pu sans commettre d'erreur de droit fonder son appréciation en tenant compte de la loi d'amnistie ; qu'ainsi, le moyen invoqué par Mme B...n'est pas fondé ;<br/>
<br/>
              Sur la gravité suffisante des faits reprochés :<br/>
<br/>
              6. Considérant qu'en vertu des dispositions du code du travail, les salariés légalement investis de fonctions représentatives bénéficient, dans l'intérêt de l'ensemble des travailleurs qu'ils représentent, d'une protection exceptionnelle ; que, lorsque le licenciement d'un de ces salariés est envisagé, ce licenciement ne doit pas être en rapport avec les fonctions représentatives normalement exercées ou l'appartenance syndicale de l'intéressé ; que, dans le cas où la demande de licenciement est motivée par un comportement fautif, il appartient à l'inspecteur du travail et, le cas échéant, au ministre, de rechercher, sous le contrôle du juge de l'excès de pouvoir, si les faits reprochés au salarié sont d'une gravité suffisante pour justifier son licenciement compte tenu de l'ensemble des règles applicables au contrat de travail de l'intéressé et des exigences propres à l'exécution normale du mandat dont il est investi ; <br/>
<br/>
              7. Considérant que la cour administrative d'appel de Marseille, pour rejeter la demande de Mme B...tendant à l'annulation de son autorisation de licenciement, a relevé qu'il ressortait des pièces du dossier que la réalité de la plupart des griefs avancés par l'employeur à l'égard de la salariée, à savoir un important absentéisme non-justifié, des refus de se rendre à son entretien annuel d'évaluation et de fixation de ses objectifs, des refus de remplir des rapports permettant à l'entreprise d'effectuer son contrôle de gestion, et un refus de respecter les voies hiérarchiques normales, était établie ; que la cour, qui ne s'est pas fondée sur les absences justifiées de la salariée, n'a pas commis d'erreur de qualification juridique en estimant que les faits reprochés à l'intéressée constituaient une faute de nature à justifier une mesure de licenciement ;<br/>
<br/>
              Sur le lien avec les mandats :<br/>
<br/>
              8. Considérant que, si la requérante soutient que la cour a commis une erreur de droit en se fondant sur les seuls griefs invoqués à son encontre pour écarter l'existence d'un lien entre la demande d'autorisation de licenciement et les mandats qu'elle exerçait, il ressort des énonciations mêmes de l'arrêt que la cour a fondé son appréciation sur l'ensemble des éléments qui lui étaient soumis, notamment sur les décisions de justice invoquées par Mme B... et qu'elle a pris en considération ses allégations relatives au fonctionnement des institutions représentatives dans l'entreprise ; qu'ainsi le moyen doit être écarté ; <br/>
<br/>
              Sur les conclusions présentées au titre des dispositions  de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              9. Considérant qu'il résulte de ce qui précède que le pourvoi de Mme B... doit être rejeté ; que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de la société Amadeus, qui n'est pas la partie perdante ; qu'il n'y a pas lieu, dans les circonstances de l'espèce, de mettre à la charge de Mme B...une somme au titre des frais exposés par la société Amadeus et non compris dans les dépens ; <br/>
<br/>
<br/>
<br/>
<br/>
              				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er : Le pourvoi de Mme B...est rejeté.<br/>
<br/>
Article 2 : Les conclusions présentées par la société Amadeus au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 3 : La présente décision sera notifiée à Mme A...B...et à la société Amadeus.<br/>
Copie en sera adressée pour information au ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social. <br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-06-07-005 PROCÉDURE. JUGEMENTS. EXÉCUTION DES JUGEMENTS. EFFETS D'UNE ANNULATION. - ANNULATION CONTENTIEUSE D'UNE AUTORISATION DE LICENCIEMENT DISCIPLINAIRE D'UN SALARIÉ PROTÉGÉ - OBLIGATION D'ORGANISER À NOUVEAU L'ENTRETIEN PRÉALABLE - ABSENCE - CONDITIONS.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">66-07-01-02-01 TRAVAIL ET EMPLOI. LICENCIEMENTS. AUTORISATION ADMINISTRATIVE - SALARIÉS PROTÉGÉS. PROCÉDURE PRÉALABLE À L'AUTORISATION ADMINISTRATIVE. ENTRETIEN PRÉALABLE. - LICENCIEMENT DISCIPLINAIRE - OBLIGATION D'ORGANISER À NOUVEAU L'ENTRETIEN À LA SUITE DE L'ANNULATION CONTENTIEUSE D'UNE DÉCISION ADMINISTRATIVE D'AUTORISATION DU LICENCIEMENT - ABSENCE - CONDITIONS.
</SCT>
<ANA ID="9A"> 54-06-07-005 A la suite de l'annulation contentieuse d'une décision autorisant le licenciement disciplinaire d'un salarié protégé, en raison d'un défaut de motivation, l'employeur qui entend reprendre la procédure de licenciement disciplinaire n'est pas tenu d'organiser un nouvel entretien préalable dès lors que les mêmes griefs sont retenus et en l'absence d'un changement de circonstances.</ANA>
<ANA ID="9B"> 66-07-01-02-01 A la suite de l'annulation contentieuse d'une décision autorisant le licenciement disciplinaire d'un salarié protégé, en raison d'un défaut de motivation, l'employeur qui entend reprendre la procédure de licenciement disciplinaire n'est pas tenu d'organiser un nouvel entretien préalable dès lors que les mêmes griefs sont retenus et en l'absence d'un changement de circonstances.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
