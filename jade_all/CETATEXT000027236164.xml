<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027236164</ID>
<ANCIEN_ID>JG_L_2013_03_000000342709</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/23/61/CETATEXT000027236164.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  4ème sous-section jugeant seule, 27/03/2013, 342709, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-03-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>342709</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 4ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Contentieux des pensions</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MONOD, COLIN</AVOCATS>
<RAPPORTEUR>M. Jérôme Marchand-Arvier</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Rémi Keller</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2013:342709.20130327</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi, enregistré le 24 août 2010 au secrétariat du contentieux du Conseil d'Etat, présenté par le ministre de la défense ; le ministre demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 09/03256 du 1er juillet 2010 par lequel la cour régionale des pensions de Pau a confirmé le jugement n° 0800026 du 13 août 2009 du tribunal départemental des pensions des Pyrénées-Atlantiques en tant qu'il a accordé à M. B...A...la revalorisation de sa pension militaire d'invalidité, calculée initialement au grade d'adjudant-chef de l'armée de terre, sur la base de l'indice afférent au grade équivalent dans la marine nationale ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code des pensions militaires d'invalidité et des victimes de la guerre ;<br/>
<br/>
              Vu la loi n° 91-647 du 10 juillet 1991 ;<br/>
<br/>
              Vu le décret n° 56-913 du 5 septembre 1956 ;<br/>
<br/>
              Vu le décret n° 59-327 du 20 février 1959 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jérôme Marchand-Arvier, Maître des Requêtes,  <br/>
<br/>
              - les observations de la SCP Monod, Colin, avocat de M.A...,<br/>
<br/>
              - les conclusions de M. Rémi Keller, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Monod, Colin, avocat de M. A... ;<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des termes mêmes de l'arrêt attaqué que, pour rejeter l'appel formé par le ministre de la défense contre le jugement du tribunal départemental des pensions des Pyrénées-Atlantiques faisant droit à la demande de M. A...tendant à ce que sa pension soit revalorisée sur la base de l'indice afférent au grade équivalent dans la marine nationale, la cour régionale des pensions de Pau ne s'est pas fondée sur un motif tenant à ce que cette demande aurait relevé d'une des hypothèses prévues par l'article L. 78 du code des pensions militaires d'invalidité et des victimes de la guerre dans lesquelles la révision d'une telle pension peut être demandée sans condition de délai ; que, par suite, le ministre de la défense et des anciens combattants ne peut utilement soutenir, ni que la cour aurait entaché son arrêt d'une insuffisance de motivation à défaut de répondre au moyen tiré de ce que la demande de M. A...n'entrait pas dans le champ d'application des dispositions de cet article L. 78 ni qu'elle aurait commis une erreur de droit en faisant à tort application de ce texte à la demande de M.A... ;  <br/>
<br/>
              2. Considérant que si le principe d'égalité ne s'oppose pas à ce que l'autorité investie du pouvoir réglementaire règle de façon différente des situations différentes ni à ce qu'elle déroge à l'égalité pour des raisons d'intérêt général, pourvu que dans l'un comme dans l'autre cas, la différence de traitement qui en résulte soit en rapport avec l'objet de la norme qui l'établit et ne soit pas manifestement disproportionnée, les dispositions de l'article L. 1 du code des pensions militaires d'invalidité et des victimes de la guerre prévoient l'octroi d'une pension militaire d'invalidité aux militaires, quel que soit leur corps d'appartenance, aux fins d'assurer une réparation des conséquences d'une infirmité résultant de blessures reçues par suite d'événements de guerre ou d'accidents dont ils ont été victimes à l'occasion du service ou de maladies contractées par le fait ou à l'occasion du service ; que le décret du 5 septembre 1956 relatif à la détermination des indices des pensions et accessoires de pensions alloués aux invalides au titre du code des pensions militaires d'invalidité et des victimes de la guerre a fixé les indices de la pension d'invalidité afférents aux grades des sous-officiers de l'armée de terre, de l'armée de l'air et de la gendarmerie à un niveau inférieur aux indices attachés aux grades équivalents dans la marine nationale ; que, par suite, le ministre de la défense et des anciens combattants n'ayant pas invoqué de considérations d'intérêt général de nature à justifier que le montant de la pension militaire d'invalidité concédée diffère à grades équivalents, selon les corps d'appartenance des bénéficiaires des pensions, la cour régionale des pensions de Pau n'a pas commis d'erreur de droit en estimant que le décret du 5 septembre 1956 était contraire, sur ce point, au principe d'égalité ;<br/>
<br/>
              3. Considérant qu'il résulte de ce qui précède que le ministre de la défense n'est pas fondé à demander l'annulation de l'arrêt attaqué ;<br/>
<br/>
              4. Considérant  que M. A...a obtenu le bénéfice de l'aide juridictionnelle ; que, par suite, son avocat peut se prévaloir des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 ; qu'il y a lieu, dans les circonstances de l'espèce, et sous réserve que la SCP Monod-Colin, son avocat, renonce à percevoir la somme correspondant à la part contributive de l'Etat, de mettre à la charge de ce dernier la somme de 1 000 euros à verser à cette société ;<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi du ministre de la défense est rejeté.<br/>
Article 2 : L'Etat versera à la SCP Monod-Colin, avocat de M.A..., une somme de 1 000 euros en application des dispositions du deuxième alinéa de l'article 37 de la loi du 10 juillet 1991, sous réserve que cette société renonce à percevoir la somme correspondant à la part contributive de l'Etat.<br/>
Article 3 : La présente décision sera notifiée au ministre de la défense et à M. B...A....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
