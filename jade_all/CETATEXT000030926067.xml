<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030926067</ID>
<ANCIEN_ID>JG_L_2015_07_000000374434</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/92/60/CETATEXT000030926067.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème / 5ème SSR, 22/07/2015, 374434, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-07-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>374434</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème / 5ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BARTHELEMY, MATUCHANSKY, VEXLIARD, POUPOT</AVOCATS>
<RAPPORTEUR>M. Laurent Huet</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Maud Vialettes</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:374434.20150722</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu les procédures suivantes :<br/>
<br/>
              1° Sous le n° 374434, par une requête et un mémoire en réplique, enregistrés le 7 janvier 2014 et le 30 janvier 2015 au secrétariat du contentieux du Conseil d'Etat, M. A...B...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la note de service n° 2013-168 du 28 octobre 2013 relative aux règles et procédures du mouvement national à gestion déconcentrée pour la rentrée 2014 des personnels enseignants du second degré, publiée au bulletin officiel de l'éducation nationale n° 41 du 7 novembre 2013 ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat le versement d'une somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de  justice administrative ;<br/>
<br/>
              3°) d'enjoindre au ministre de l'éducation nationale de tirer toutes les conséquences des motifs d'illégalité ayant conduit à l'annulation contentieuse des circulaires des 21 octobre 2004, 31 octobre 2007, 29 octobre 2008, 28 octobre 2009, 20 octobre 2010, 25 octobre 2011 et 30 octobre 2012 relatives aux règles et procédures applicables au mouvement national à gestion déconcentrée des enseignants du second degré.<br/>
<br/>
<br/>
<br/>
              2° Sous le n° 387086, par une requête et un mémoire en réplique, enregistrés les 13 janvier et 28 mai 2015 au secrétariat du contentieux du Conseil d'Etat, M. B...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la note de service n° 2014-145 du 6 novembre 2014 relative aux règles et procédures du mouvement national à gestion déconcentrée pour la rentrée 2015 des personnels enseignants du second degré, publiée au bulletin officiel de l'éducation nationale n° 42 du 13 novembre 2014 ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat le versement d'une somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de  justice administrative ;<br/>
<br/>
              3°) d'enjoindre au ministre de l'éducation nationale de tirer toutes les conséquences des motifs d'illégalité ayant conduit à l'annulation contentieuse des circulaires des 21 octobre 2004, 31 octobre 2007, 29 octobre 2008, 28 octobre 2009, 20 octobre 2010, 25 octobre 2011 et 30 octobre 2012 relatives aux règles et procédures applicables au mouvement national à gestion déconcentrée des enseignants du second degré.<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu la loi n° 84-16 du 11 janvier 1984 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 1er juillet 2015, présentée par le ministre de l'éducation nationale, de l'enseignement supérieur et de la recherche ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Laurent Huet, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Maud Vialettes, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Barthélemy, Matuchansky, Vexliard, Poupot, avocat de M. B...;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant que M. B...demande l'annulation pour excès de pouvoir des notes de service du 28 octobre 2013 et du 6 novembre 2014 par lesquelles le ministre de l'éducation nationale, de l'enseignement supérieur et de la recherche a défini les règles et procédures relatives au mouvement national à gestion déconcentrée des personnels enseignants du second degré et des personnels d'éducation et d'orientation pour les rentrées scolaires de septembre 2014 et septembre 2015 ; que les deux requêtes de M. B...présentent à juger des questions semblables ; qu'il y a lieu de les joindre pour statuer par une seule décision ;<br/>
<br/>
              Sur les fins de non-recevoir opposées par le ministre de l'éducation nationale, de l'enseignement supérieur et de la recherche :<br/>
<br/>
              2. Considérant, en premier lieu, que les notes de service attaquées prévoient des critères précis à prendre en compte pour le classement des demandes de mutation, assortis de barèmes de points à appliquer, ainsi que des règles permettant de départager des candidats en cas d'égalité de barème ; qu'elles prévoient, en outre, des possibilités de bonification par des points liés à des situations individuelles, familiales ou d'ancienneté ; qu'elles présentent ainsi un caractère impératif et constituent, dès lors, des actes susceptibles d'être contestés devant le juge de l'excès de pouvoir ;<br/>
<br/>
              3. Considérant, en second lieu, que M.B..., qui est professeur d'éducation physique et dont les demandes de mutation présentées au titre des rentrées scolaires de septembre 2014 et 2015 ont d'ailleurs été rejetées, justifie, contrairement à ce que soutient le ministre de l'éducation nationale, de l'enseignement supérieur et de la recherche, d'un intérêt lui donnant qualité pour agir contre les deux notes de service attaquées qui étaient applicables, pour la préparation de ces rentrées scolaires, aux membres de son corps ;  <br/>
<br/>
              Sur la légalité des notes de service attaquées :<br/>
<br/>
              4. Considérant qu'aux termes de l'article 60 de la loi du 11 janvier 1984 portant dispositions statutaires relatives à la fonction publique de l'Etat : " L'autorité compétente procède aux mouvements des fonctionnaires après avis des commissions administratives paritaires (...) / Dans toute la mesure compatible avec le bon fonctionnement du service, les affectations prononcées doivent tenir compte des demandes formulées par les intéressés et de leur situation de famille. Priorité est donnée aux fonctionnaires séparés de leur conjoint pour des raisons professionnelles, aux fonctionnaires séparés pour des raisons professionnelles du partenaire avec lequel ils sont liés par un pacte civil de solidarité (...), aux fonctionnaires handicapés (...) et aux fonctionnaires qui exercent leurs fonctions, pendant une durée et selon des modalités fixées par décret en Conseil d'Etat, dans un quartier urbain où se posent des problèmes sociaux et de sécurité particulièrement difficiles. Priorité est également donnée aux fonctionnaires placés en situation de réorientation professionnelle pour les emplois correspondant à leur projet personnalisé d'évolution professionnelle (...) " ;<br/>
<br/>
              5. Considérant qu'en fixant des règles assorties d'un barème à appliquer pour le classement des demandes de mutation, indivisibles des autres dispositions qu'elles comportent, et en établissant à cette fin des priorités non prévues par les dispositions citées ci-dessus de l'article 60 de la loi du 11 janvier 1984, les notes de service attaquées ajoutent illégalement aux dispositions de cet article ; qu'elles doivent, pour ce motif et sans qu'il soit besoin d'examiner les autres moyens des requêtes, être annulées dans leur intégralité ;<br/>
<br/>
<br/>
              Sur les conclusions aux fins d'injonction :<br/>
<br/>
              6. Considérant qu'il résulte des dispositions des articles L. 911-1 et L. 911-2 du code de justice administrative que, lorsque sa décision l'implique nécessairement, il appartient à la juridiction saisie de conclusions en ce sens de prescrire, selon le cas, une mesure d'exécution dans un sens déterminé ou l'intervention, après une nouvelle instruction, d'une nouvelle décision, le cas échéant en assortissant cette injonction d'un délai d'exécution ; que si M. B... demande ainsi au Conseil d'Etat d'enjoindre au ministre de l'éducation nationale, de l'enseignement supérieur et de la recherche de " tirer toutes les conséquences " de l'annulation de la note de service du 6 novembre 2014 prononcée par la présente décision, cette annulation n'implique toutefois ni que l'administration prenne une mesure d'exécution dans un sens déterminé ni qu'elle prenne une nouvelle décision après une nouvelle instruction ; que cependant, si cette annulation est par elle-même sans incidence sur les décisions individuelles devenues définitives qui auraient été prises sur le fondement de la note de service du 6 novembre 2014, l'administration ne saurait, sans méconnaître l'autorité de la chose jugée qui s'attache à la présente décision d'annulation et aux motifs qui en sont le soutien nécessaire, reprendre, pour la rentrée scolaire de septembre 2015, un acte à caractère impératif relatif au mouvement national à gestion déconcentrée des personnels enseignants du second degré et des personnels d'éducation et d'orientation comportant des critères de priorité non prévus par la loi, sauf à ce qu'un fondement légal leur ait préalablement été donné ;<br/>
<br/>
              Sur les conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              7. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 3 000 euros à verser à M. B...au titre de ces dispositions ; <br/>
<br/>
<br/>
<br/>
<br/>
              				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er : Les notes de service du ministre de l'éducation nationale de l'enseignement supérieur et de la recherche n° 2013-168 du 28 octobre 2013 et n° 2014-145 du 6 novembre 2014 sont annulées. <br/>
Article 2 : Les conclusions aux fins d'injonction de M. B...sont rejetées.<br/>
Article 3 : L'Etat versera à M. B...une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
Article 4 : La présente décision sera notifiée à M. A...B...et à la ministre de l'éducation nationale, de l'enseignement supérieur et de la recherche.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
