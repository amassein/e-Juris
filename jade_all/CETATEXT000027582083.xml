<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027582083</ID>
<ANCIEN_ID>JG_L_2013_06_000000354226</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/58/20/CETATEXT000027582083.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère / 6ème SSR, 19/06/2013, 354226, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-06-19</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>354226</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère / 6ème SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP WAQUET, FARGE, HAZAN</AVOCATS>
<RAPPORTEUR>M. Pascal Trouilly</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Alexandre Lallet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:XX:2013:354226.20130619</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire, le mémoire complémentaire et le nouveau mémoire, enregistrés les 22 novembre 2011, 21 février 2012 et 28 mars 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. B...A..., demeurant... ; M. A...demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler l'arrêt n° 09MA00986 du 14 décembre 2010 de la cour administrative d'appel de Marseille en tant qu'il n'a que partiellement fait droit à sa requête tendant à l'annulation du jugement n° 0508939 du 22 janvier 2009 par lequel le tribunal administratif de Marseille a rejeté sa demande tendant à l'annulation pour excès de pouvoir de l'arrêté du 10 juin 2005 de l'inspecteur d'académie des Bouches-du-Rhône en tant que celui-ci a prononcé sa radiation des cadres ainsi que de la décision de la même autorité rejetant implicitement son recours gracieux formé le 5 septembre 2005 ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat le versement de la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
              Vu le code des pensions civiles et militaires de retraite ; <br/>
<br/>
              Vu la loi n° 79-587 du 11 juillet 1979 ;<br/>
<br/>
              Vu la loi n° 84-16 du 11 janvier 1984 ; <br/>
<br/>
              Vu le décret n° 86-442 du 14 mars 1986 ; <br/>
	Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Pascal Trouilly, Maître des Requêtes,  <br/>
<br/>
              - les conclusions de M. Alexandre Lallet, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Waquet, Farge, Hazan, avocat de M.A... ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que M.A..., instituteur, a été radié des cadres, le 12 avril 2002, par l'inspecteur d'académie des Bouches-du-Rhône, pour inaptitude absolue et définitive à l'exercice de ses fonctions, non imputable au service, la date d'effet de cette radiation étant fixée au 24 août 2002 ; que cette décision a été annulée pour un motif tiré de l'irrégularité de la procédure suivie, par un jugement, devenu définitif, du 20 janvier 2005 du  tribunal administratif de Marseille ; qu'à la suite de cette annulation, l'inspecteur d'académie des Bouches-du-Rhône a pris un nouvel arrêté le 10 juin 2005, après avoir consulté une nouvelle fois la commission de réforme ; que cet arrêté, d'une part, a réintégré rétroactivement M. A...dans le corps des instituteurs à compter du 24 août 2002, d'autre part, l'a de nouveau radié des cadres, à compter de cette même date, en l'admettant d'office à la retraite ; que M. A...a demandé l'annulation de cet arrêté, sans succès, au tribunal administratif de Marseille ; que, par un arrêt du 14 décembre 2010, la cour administrative d'appel de Marseille a annulé l'arrêté du 10 juin 2005 en tant seulement que la nouvelle mesure de radiation avait une portée rétroactive ; que M. A...se pourvoit en cassation contre cet arrêt en tant qu'il n'a pas annulé entièrement l'arrêté litigieux ; <br/>
<br/>
              2. Considérant qu'aux termes de l'article 19 du décret du 14 mars 1986 : "  (...) La commission de réforme doit être saisie de tous témoignages, rapports et constatations propres à éclairer son avis. / Elle peut faire procéder à toutes mesures d'instruction, enquêtes et expertises qu'elle estime nécessaires. /  Le fonctionnaire (...) peut fournir des certificats médicaux " ; qu'il résulte de ces dispositions qu'il appartient à l'administration qui saisit la commission de réforme de fournir à cette dernière les éléments médicaux lui permettant de se prononcer sur l'aptitude au service de l'agent ; que si ces éléments sont insuffisants, la commission peut toutefois valablement statuer, après avoir fait procéder à des mesures d'instruction complémentaires ; que l'agent n'est pas tenu de produire lui-même des pièces médicales ; que lorsque la saisine de la commission de réforme intervient après l'annulation d'une première décision de radiation des cadres d'un agent, les éléments fournis par l'administration doivent être de nature à éclairer suffisamment la commission sur l'état de santé de l'agent à la date de cette nouvelle saisine ; qu'ainsi, en estimant que, dès lors que M. A...n'avait lui-même fourni aucun élément nouveau relatif à son état de santé, la commission de réforme avait pu régulièrement, sur la seule base du dossier qui lui avait été déjà soumis en 2002 par l'administration, se prononcer en 2005 sur l'aptitude ou l'inaptitude de l'intéressé à reprendre ses fonctions, sans rechercher si ce dossier  était, eu égard notamment à sa date, de nature à l'éclairer suffisamment, la cour a commis une erreur de droit ; que l'article 3 de son arrêt doit, dès lors, pour ce motif, être annulé ; <br/>
<br/>
              3. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application de l'article L. 821-2 du code de justice administrative, dans la mesure de la cassation prononcée ; <br/>
<br/>
              4. Considérant qu'aux termes de l'article 47 du décret du 14 mars 1986 : " Le fonctionnaire ne pouvant à l'expiration de la dernière période de congé de longue maladie ou de longue durée, reprendre son service est soit reclassé dans un autre emploi...soit mis en disponibilité, soit admis à la retraite " ; <br/>
<br/>
              5. Considérant, en premier lieu, que la circonstance que la durée de certaines périodes de congé de longue maladie ou de longue durée accordées à M. A...antérieurement à sa première radiation des cadres décidée le 12 avril 2002 aurait excédé la durée maximale de ces congés prévue à l'article 36 du décret du 14 mars 1986 est sans incidence sur la légalité de la décision de radiation des cadres du 10 juin 2005 ; <br/>
<br/>
              6. Considérant, en deuxième lieu, que M. A...soutient que l'avis de la commission de réforme du 4 mai 2005 est, à plusieurs titres, irrégulier, au regard des dispositions de l'article 19 du décret du 14 mars 1986 ; que compte tenu notamment de l'ancienneté du dossier soumis à la commission de réforme, l'administration aurait dû compléter ce dossier ; qu'à défaut, la commission elle-même aurait dû procéder à un complément d'instruction ; qu'il ressort toutefois des pièces du dossier, et notamment du rapport d'examen psychiatrique effectué à la demande de l'intéressé le 8 novembre 2006, que l'état de santé de M.A..., reconnu définitivement inapte au service par deux médecins agréés en 2001 et 2002, n'avait pas connu d'évolution entre 2002 et 2005 et qu'un reclassement dans un autre emploi ou un autre corps n'était pas envisageable ; qu'ainsi, les conditions dans lesquelles la commission de réforme s'est prononcée le 4 mai 2005, n'ont pas eu pour effet de priver M. A...d'une garantie, et n'ont pas exercé, en l'espèce, d'influence sur le sens de la décision prise par l'administration ; <br/>
<br/>
              7. Considérant, en troisième lieu, que l'arrêté du 10 juin 2005 est suffisamment motivé au regard des exigences des articles 1er et 3 de la loi du 11 juillet 1979 ; que par ailleurs, il résulte des termes mêmes de l'article 5 de cette loi qu'une décision implicite de rejet n'est pas illégale du seul fait qu'elle est dépourvue de motifs ; que, par suite, le moyen tiré de l'absence de motivation de la décision implicite de rejet du recours gracieux formé le 5 septembre 2005 doit être écarté ; <br/>
<br/>
              8. Considérant, en quatrième lieu, que si M. A...soutient que la pathologie qui est à l'origine de la radiation des cadres décidée le 10 juin 2005 serait apparue en 1999 seulement et serait ainsi différente de celle dont il a souffert entre 1992 et 1994, de sorte qu'il n'aurait pas épuisé, à la date de la radiation des cadres en litige, ses droits  au bénéfice de congés de longue durée, il n'assortit ce moyen d'aucune pièce médicale probante ; <br/>
<br/>
              9. Considérant, en dernier lieu, que, comme il a été dit ci-dessus, l'inspecteur d'académie a pu légalement prononcer la radiation des cadres de M. A...à compter du 10 juin 2005 dès lors qu'à cette date, celui-ci était définitivement inapte à reprendre ses fonctions et qu'il ne pouvait bénéficier d'un reclassement ; que, par ailleurs, en admettant même que les difficultés rencontrées par M. A...dans sa carrière, et notamment le déplacement d'office dont il a fait l'objet en 1999, aient pu avoir une influence sur son état de santé, ces circonstances ne peuvent être regardées comme la cause première et prépondérante de ses troubles, M. A...n'ayant d'ailleurs pas présenté, dans le délai de quatre mois suivant la date de la première constatation de la maladie prévu à l'article 32 du décret du 14 mars 1986, de demande de reconnaissance de l'origine professionnelle de sa pathologie ; qu'ainsi, l'inspecteur d'académie n'a pas entaché son arrêté d'une erreur d'appréciation ou d'un détournement de pouvoir en estimant que cette inaptitude n'était pas imputable au service ; <br/>
<br/>
              9. Considérant qu'il résulte de tout ce qui précède que, sans qu'il y ait lieu d'ordonner une expertise médicale, M. A...n'est pas fondé à soutenir que c'est à tort que, par le jugement du 22 janvier 2009, en tant qu'il n'a pas déjà été annulé par l'article 1er de l'arrêt du 14 décembre 2010 de la cour administrative d'appel de Marseille, le tribunal administratif de Marseille a rejeté sa demande ; que ses conclusions présentées au titre de l'article  L. 761-1 du code de justice administrative ne peuvent, en conséquence, qu'être rejetées ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'article 3 de l'arrêt de la cour administrative d'appel de Marseille du 14 décembre 2010 est annulé.<br/>
Article 2 : Les conclusions d'appel de M. A...tendant à l'annulation du jugement du 22 janvier 2009 en tant qu'il n'a pas déjà été annulé par l'article 1er de l'arrêt de la cour administrative d'appel de Marseille du 14 décembre 2010, ainsi que ses conclusions présentées devant le Conseil d'Etat au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La présente décision sera notifiée à M. B...A...et au ministre de l'éducation nationale.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
