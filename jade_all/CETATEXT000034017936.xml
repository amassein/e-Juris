<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034017936</ID>
<ANCIEN_ID>JG_L_2017_02_000000404993</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/01/79/CETATEXT000034017936.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème - 7ème chambres réunies, 08/02/2017, 404993</TITRE>
<DATE_DEC>2017-02-08</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>404993</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème - 7ème chambres réunies</FORMATION>
<TYPE_REC>Autres</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Cécile Barrois de Sarigny</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Béatrice Bourgeois-Machureau</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:404993.20170208</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A...B..., à l'appui de sa demande tendant à l'annulation de l'arrêté du 6 novembre 2016 du préfet des Pyrénées-Orientales l'obligeant à quitter le territoire français sans délai avec interdiction de retour pendant une durée de trois ans et fixant le pays à destination duquel il serait éloigné, a produit un mémoire, enregistré le 9 novembre 2016 au greffe du tribunal administratif, en application de l'article 23-1 de l'ordonnance n° 58-1067 du 7 novembre 1958, par lequel il soulève une question prioritaire de constitutionnalité mettant en cause la conformité aux droits et libertés garantis par la Constitution des a) et b) du 4° du I de l'article 27 de la loi du 7 mars 2016 relative au droit des étrangers en France.<br/>
<br/>
              Par une ordonnance n° 1605490 QPC du 10 novembre 2016, enregistrée le 10 novembre 2016 au secrétariat du contentieux du Conseil d'Etat, le magistrat désigné du tribunal administratif de Montpellier a décidé, par application des dispositions de l'article 23-2 de l'ordonnance n° 58-1067 du 7 novembre 1958, de transmettre au Conseil d'Etat la question prioritaire de constitutionnalité soulevée. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ; <br/>
<br/>
              Vu : <br/>
              - la Constitution, notamment son Préambule et son article 61-1 ;  <br/>
              - l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
              - la directive 2008/115/CE du Parlement européen et du Conseil du 16 décembre 2008 ; <br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              - la loi n° 2011-672 du 16 juin 2011 ; <br/>
              - la loi n° 2016-274 du 7 mars 2016 ;<br/>
              - la décision du Conseil constitutionnel n° 2010-79 QPC du <br/>
17 décembre 2010 ;<br/>
              - la décision du Conseil constitutionnel n° 2011-631 DC du 9 juin 2011 ;<br/>
              - le code de justice administrative ; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Cécile Barrois de Sarigny, maître des requêtes, <br/>
<br/>
              - les conclusions de Mme Béatrice Bourgeois-Machureau, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1.	Considérant que par un arrêté du 6 novembre 2016, le préfet des<br/>
Pyrénées-Orientales a pris à l'encontre de M. B...une décision l'obligeant à quitter le territoire français sans délai avec interdiction de retour sur le territoire français pendant une durée de trois ans et fixant le pays à destination duquel il serait éloigné ; que l'intéressé a demandé l'annulation de la décision portant interdiction de retour devant le magistrat désigné du tribunal administratif de Montpellier ; que M. B...a soulevé, à l'appui de sa demande, en application de l'article 23-1 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel, le moyen tiré de ce que les a) et b) du 4° du I de l'article 27 de la loi du 7 mars 2016 relative au droit des étrangers en France, modifiant les quatre premiers alinéas du III de l'article L. 511-1 du code de l'entrée et du séjour des étrangers et du droit d'asile, relatifs à l'interdiction de retour sur le territoire français, méconnaîtraient les droits et libertés garantis par la Constitution ; que par un jugement en date du 9 novembre 2016, le magistrat désigné du tribunal administratif de Montpellier, statuant sur le litige, a annulé la décision d'interdiction de retour sur le territoire français prise à l'encontre de M. B...; qu'il a ensuite, par une ordonnance du 10 novembre 2016, transmis au Conseil d'Etat la question prioritaire de constitutionnalité soulevée ;<br/>
<br/>
              2.	Considérant qu'il résulte des dispositions de l'article 23-4 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel que, lorsqu'une juridiction relevant du Conseil d'Etat a transmis à ce dernier, en application de l'article 23-2 de cette même ordonnance, la question de la conformité à la Constitution d'une disposition législative, le Conseil d'Etat se prononce sur le renvoi de la question au Conseil constitutionnel dans un délai de trois mois, sans qu'il lui appartienne, dans ce cadre, de statuer sur la régularité de la décision juridictionnelle qui lui a transmis la question ; <br/>
<br/>
              3.	Considérant qu'en vertu de l'article 23-4, le Conseil constitutionnel est saisi de la question de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux ;<br/>
<br/>
              4.	Considérant, d'une part, qu'aux termes de l'article 88-1 de la Constitution : " La République participe à l'Union européenne constituée d'États qui ont choisi librement d'exercer en commun certaines de leurs compétences en vertu du traité sur l'Union européenne et du traité sur le fonctionnement de l'Union européenne, tels qu'ils résultent du traité signé à Lisbonne le 13 décembre 2007 " ; qu'en l'absence de mise en cause d'une règle ou d'un principe inhérent à l'identité constitutionnelle de la France, le Conseil constitutionnel juge qu'il n'est pas compétent pour contrôler la conformité aux droits et libertés que la Constitution garantit de dispositions législatives qui se bornent à tirer les conséquences nécessaires de dispositions inconditionnelles et précises d'une directive de l'Union européenne et qu'en ce cas, il n'appartient qu'au juge de l'Union européenne, saisi le cas échéant à titre préjudiciel, de contrôler le respect par cette directive des droits fondamentaux garantis par l'article 6 du Traité sur l'Union européenne ; <br/>
<br/>
              5.	Considérant qu'en l'absence de mise en cause, à l'occasion d'une question prioritaire de constitutionnalité soulevée sur des dispositions législatives se bornant à tirer les conséquences nécessaires de dispositions précises et inconditionnelles d'une directive de l'Union européenne, d'une règle ou d'un principe inhérent à l'identité constitutionnelle de la France, une telle question n'est pas au nombre de celles qu'il appartient au Conseil d'Etat de transmettre au Conseil constitutionnel sur le fondement de l'article 23-4 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel ;<br/>
<br/>
              6.	Considérant qu'en l'espèce, les dispositions des quatre premiers alinéas du III de l'article L. 511-1 du code de l'entrée et du séjour des étrangers et du droit d'asile, dans leur rédaction résultant du a) du 4° du I de l'article 27 de la loi du 7 mars 2016, dont la conformité à la Constitution est contestée, se bornent à tirer les conséquences nécessaires des dispositions précises et inconditionnelles de la directive 2008/115/CE du Parlement européen et du Conseil du 16 décembre 2008 relative aux normes et procédures communes applicables dans les États membres au retour des ressortissants de pays tiers en séjour irrégulier, sans mettre en cause une règle ou un principe inhérent à l'identité constitutionnelle de la France ; <br/>
<br/>
              7.	Considérant, d'autre part, que la modification apportée par le b) du 4° du I de l'article 27 de la loi du 7 mars 2016 se borne à changer la référence aux textes relatifs au système d'information Schengen dans des dispositions, issues de l'article 37 de la loi du 16 juin 2011 relative à l'immigration, à l'intégration et à la nationalité, qui ont été déclarées conformes à la Constitution par la décision n° 2011-631 DC du 9 juin 2011 du Conseil constitutionnel ; que cette modification de référence ne constitue par un changement de circonstances de nature à justifier que la conformité aux droits et libertés garantis par la Constitution de la disposition législative en cause soit à nouveau soumise au Conseil constitutionnel ; <br/>
<br/>
              8.	Considérant qu'il résulte de tout ce qui précède qu'il n'y a pas lieu de renvoyer au Conseil constitutionnel la question de la conformité aux droits et libertés garantis par la Constitution des a) et b) du 4° du I de l'article 27 de la loi du 7 mars 2016 relative au droit des étrangers en France ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité transmise par le magistrat désigné du tribunal administratif de Montpellier.<br/>
<br/>
Article 2 : La présente décision sera notifiée à M. A...B...et au ministre de l'intérieur. <br/>
<br/>
Copie en sera adressée au Conseil constitutionnel, au Premier ministre, ainsi qu'au tribunal administratif de Montpellier.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-10-05 PROCÉDURE. - OFFICE DU JUGE DU FILTRE - EXAMEN DE LA RÉGULARITÉ DE LA DÉCISION JURIDICTIONNELLE AYANT TRANSMIS LA QPC - ABSENCE.
</SCT>
<ANA ID="9A"> 54-10-05 Il n'appartient pas au Conseil d'Etat, juge du filtre, de se prononcer sur la régularité de la décision juridictionnelle qui lui a transmis une question prioritaire de constitutionnalité en application de l'article 23-2 de l'ordonnance du 7 novembre 1958.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
