<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044559188</ID>
<ANCIEN_ID>JG_L_2021_12_000000448360</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/55/91/CETATEXT000044559188.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère chambre, 23/12/2021, 448360, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-12-23</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>448360</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère chambre</FORMATION>
<TYPE_REC/>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>CABINET COLIN - STOCLET ; SCP MARLANGE, DE LA BURGADE ; SCP BORE, SALVE DE BRUNETON, MEGRET</AVOCATS>
<RAPPORTEUR>M. Pierre Boussaroque</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Vincent Villette</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:448360.20211223</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. et Mme AB... X... et d'autres requérants, d'une part, M. et Mme B... A... d'autre part, ont demandé au tribunal administratif de Rouen d'annuler pour excès de pouvoir l'arrêté du 19 décembre 2016 et l'arrêté modificatif du 20 juin 2018 par lesquels le maire du Mesnil-Esnard a autorisé la société civile de construction vente European Homes 48 à construire, après démolition des constructions existantes, un immeuble collectif d'habitation. Par un jugement nos 1700202, 1701836, 1802887, 1803048 du 28 février 2019, le tribunal administratif de Rouen a rejeté ces demandes.<br/>
<br/>
              Par un arrêt n° 19DA00965 du 3 novembre 2020, la cour administrative d'appel de Douai a rejeté l'appel formé contre ce jugement par M. X... et certains autres requérants de première instance.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire, un nouveau mémoire et un mémoire en réplique et un nouveau mémoire, enregistrés les 4 janvier, 6 avril, 17 mai, 5 octobre 2021 et 3 décembre 2021 au secrétariat du contentieux du Conseil d'Etat, M. AB... X..., M. et Mme K... H..., AD... M..., M. et Mme I... N..., M. et Mme R... O..., M. AG... Q..., M. et Mme D... S..., AC... J..., AI... L..., M. et Mme Y... C..., M. et Mme D... W..., AE... AF..., M. et Mme AA... U..., M. et Mme V... P..., M. et Mme Z... E... et AH... T... demandent au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) de mettre à la charge de la société European Homes 48 et de la commune du Mesnil-Esnard la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'urbanisme ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Pierre Boussaroque, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Vincent Villette, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, au Cabinet Colin - Stoclet, avocat de M. X... et autres, à la SCP Boré, Salve de Bruneton, Mégret, avocat de la commune du Mesnil-Esnard, et à la SCP Marlange, de la Burgade, avocat de la SCCV European Homes 48 ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que, par deux arrêtés des 19 décembre 2016 et 20 juin 2018, le maire du Mesnil-Esnard a délivré à la société European Homes 48 un permis de construire et un permis modificatif en vue de l'édification d'un immeuble d'habitation collectif. M. et Mme X... et d'autres requérants ont demandé au tribunal administratif de Rouen d'annuler ces arrêtés pour excès de pouvoir et ce tribunal a rejeté leurs demandes par un jugement du 28 février 2019. M. X... et autres se pourvoient en cassation contre l'arrêt du 3 novembre 2020 par lequel la cour administrative d'appel de Douai a rejeté l'appel qu'ils avaient formé contre ce jugement.<br/>
<br/>
              2. Aux termes de l'article R. 431-4 du code de l'urbanisme : " La demande de permis de construire comprend : / a) Les informations mentionnées aux articles R. 431-5 à R. 431-12 (...). Aux termes de l'article R. 431-8 du même code : " Le projet architectural comprend une notice précisant : / 1° L'état initial du terrain et de ses abords indiquant, s'il y a lieu, les constructions, la végétation et les éléments paysagers existants ; / 2° Les partis retenus pour assurer l'insertion du projet dans son environnement et la prise en compte des paysages, faisant apparaître, en fonction des caractéristiques du projet : / a) L'aménagement du terrain, en indiquant ce qui est modifié ou supprimé ; / (...) e) Le traitement des espaces libres, notamment les plantations à conserver ou à créer (...). Aux termes de l'article R. 431-9 : " Le projet architectural comprend également un plan de masse des constructions à édifier ou à modifier coté dans les trois dimensions. Ce plan de masse fait apparaître les travaux extérieurs aux constructions, les plantations maintenues, supprimées ou créées et, le cas échéant, les constructions existantes dont le maintien est prévu. Il indique également, le cas échéant, les modalités selon lesquelles les bâtiments ou ouvrages seront raccordés aux réseaux publics ou, à défaut d'équipements publics, les équipements privés prévus, notamment pour l'alimentation en eau et l'assainissement (...) ". Enfin, aux termes de l'article R. 431-10 : " Le projet architectural comprend également : (...) c) Un document graphique permettant d'apprécier l'insertion du projet de construction par rapport aux constructions avoisinantes et aux paysages, son impact visuel ainsi que le traitement des accès et du terrain ; d) Deux documents photographiques permettant de situer le terrain respectivement dans l'environnement proche et, sauf si le demandeur justifie qu'aucune photographie de loin n'est possible, dans le paysage lointain. Les points et les angles des prises de vue sont reportés sur le plan de situation et le plan de masse ".<br/>
<br/>
              3. La circonstance que le dossier de demande de permis de construire ne comporterait pas l'ensemble des documents exigés par les dispositions du code de l'urbanisme, ou que les documents produits seraient insuffisants, imprécis ou comporteraient des inexactitudes, n'est susceptible d'entacher d'illégalité le permis de construire qui a été accordé que dans le cas où les omissions, inexactitudes ou insuffisances entachant le dossier ont été de nature à fausser l'appréciation portée par l'autorité administrative sur la conformité du projet à la réglementation applicable.<br/>
<br/>
              4. Il ressort des pièces du dossier soumis aux juges du fond que la demande de permis modificatif présentée par la société European Homes 48 avait notamment pour objet de réaménager les espaces verts du projet et qu'à l'appui de cette demande étaient produits, d'une part, une notice prévoyant le remplacement des trente-huit arbres dont l'abattage était prévu et la plantation de vingt-six arbres supplémentaires et, d'autre part, un plan, intitulé " plan des espaces verts ", localisant les arbres conservés et les arbres plantés, distinguant parmi ces derniers vingt-six arbres de haute tige et trente-huit arbres de taille plus modeste. Il ressort de la comparaison de ce plan avec la vue en trois dimensions de la zone de liaison entre les deux bâtiments, figurant dans le même dossier, que l'implantation d'arbres à cet endroit n'est ni prévue ni vraisemblable et de sa comparaison avec le plan de repérage des sous-bassins, relatif aux dispositifs de gestion des eaux pluviales, que quatre arbres de haute tige indiqués comme conservés se situent à l'emplacement où il est prévu d'enterrer un module de stockage des eaux pluviales. Par ailleurs, un autre plan versé au même dossier, intitulé " A3 espaces verts " indique, à l'inverse, que les arbres de haute tige situés à l'endroit prévu pour l'implantation du module de stockage des eaux pluviales seraient abattus. Dans ces conditions, la cour a dénaturé les pièces du dossier qui lui était soumis en retenant que les éléments du dossier de demande de permis de construire modificatif répondaient aux exigences des articles R. 431-8, R. 431-9 et R. 431-10 du code de l'urbanisme et en écartant le moyen tiré de ce qu'ils ne mettaient pas l'autorité administrative en mesure de porter, en connaissance de cause, son appréciation sur le respect des dispositions des articles UC 13.4 et UC 13.5 du règlement du plan local d'urbanisme imposant, d'une part, le remplacement de la totalité des arbres abattus et, d'autre part, qu'un minimum de 50 % de la surface au sol de la parcelle soit traité en espaces verts et en écartant le moyen tiré de la méconnaissance, par le permis initial comme par le permis modificatif, de ces dispositions.<br/>
<br/>
              5. Il résulte de tout ce qui précède, sans qu'il soit besoin de se prononcer sur les autres moyens du pourvoi, que M. X... et autres sont fondés à demander l'annulation de l'arrêt qu'ils attaquent.<br/>
<br/>
              Sur les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              6. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la commune du Mesnil-Esnard et de la société European Homes 48 une somme totale de 1 500 euros à verser chacune aux requérants au titre de l'article L. 761-1 du code de justice administrative. Les mêmes dispositions font obstacle à ce qu'une somme soit mise à ce titre à la charge des requérants, qui ne sont pas la partie perdante dans la présente instance. <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 3 novembre 2020 de la cour administrative d'appel de Douai est annulé.<br/>
Article 2 :  L'affaire est renvoyée à la cour administrative d'appel de Douai.<br/>
Article 3 : La commune du Mesnil-Esnard et la société European Homes 48 verseront chacune une somme totale de 1 500 euros à M. X... et aux autres requérants.<br/>
Article 4 : Les conclusions de la commune du Mesnil-Esnard et de la société European Homes présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
Article 5 : La présente décision sera notifiée à M. AB... X..., représentant unique désigné, pour l'ensemble des requérants, à la commune du Mesnil-Esnard et à la société civile de construction vente European Homes 48.<br/>
<br/>
Délibéré à l'issue de la séance du 10 décembre 2021 où siégeaient : Mme Gaëlle Dumortier, présidente de chambre, présidant ; M. Damien Botteghi, conseiller d'Etat et M. Pierre Boussaroque, conseiller d'Etat-rapporteur. <br/>
<br/>
              Rendu le 23 décembre 2021.<br/>
<br/>
<br/>
                 La présidente : <br/>
                 Signé : Mme Gaëlle Dumortier<br/>
 		Le rapporteur : <br/>
      Signé : M. Pierre Boussaroque<br/>
                 La secrétaire :<br/>
                 Signé : Mme F... G...<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
