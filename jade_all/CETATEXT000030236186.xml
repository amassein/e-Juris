<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030236186</ID>
<ANCIEN_ID>JG_L_2015_02_000000370030</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/23/61/CETATEXT000030236186.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  5ème sous-section jugeant seule, 13/02/2015, 370030, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-02-13</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>370030</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 5ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GASCHIGNARD</AVOCATS>
<RAPPORTEUR>M. Jean-Dominique Langlais</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2015:370030.20150213</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu le pourvoi, enregistré le 9 juillet 2013 au secrétariat du contentieux du Conseil d'Etat, présenté pour M. et Mme B...A..., demeurant... ; M. et Mme A... demandent au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler l'article 3 de l'ordonnance du 7 mai 2013 par laquelle le magistrat désigné par le président du tribunal administratif de Cergy-Pontoise, après avoir liquidé l'astreinte prononcée par un jugement du 3 juillet 2012, a rejeté leurs conclusions tendant à ce que le montant de cette astreinte soit majoré pour l'avenir et à ce qu'il soit enjoint sous astreinte au préfet des Hauts-de-Seine d'exécuter le jugement du 3 juillet 2012, en tant qu'il condamne l'Etat à leur verser la somme de 800 euros au titre de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à leur demande ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat le versement d'une somme de 1 500 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de la construction et de l'habitation ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Dominique Langlais, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Gaschignard, avocat de M. et Mme A...;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'aux termes de l'article L. 300-1 du code de la construction et de l'habitation : " Le droit à un logement décent et indépendant (...) est garanti par l'Etat à toute personne qui (...) n'est pas en mesure d'y accéder par ses propres moyens ou de s'y maintenir. / Ce droit s'exerce par un recours amiable puis, le cas échéant, par un recours contentieux dans les conditions et selon les modalités fixées par le présent article et les articles L. 441-2-3 et L. 441-2-3-1 " ; qu'aux termes du II de l'article L. 441-2-3 du même code : "  (...) Dans un délai fixé par décret, la commission de médiation désigne les demandeurs qu'elle reconnaît prioritaires et auxquels un logement doit être attribué en urgence (...) / La commission de médiation transmet au représentant de l'Etat dans le département la liste des demandeurs auxquels doit être attribué en urgence un logement / (...) Le représentant de l'Etat dans le département désigne chaque demandeur à un organisme bailleur disposant de logements correspondant à la demande (...) / En cas de refus de l'organisme de loger le demandeur, le représentant de l'Etat dans le département qui l'a désigné procède à l'attribution d'un logement correspondant aux besoins et aux capacités du demandeur sur ses droits de réservation " ; qu'aux termes du premier alinéa du I de l'article L. 441-2-3-1 : " Le demandeur qui a été reconnu par la commission de médiation comme prioritaire et comme devant être logé d'urgence et qui n'a pas reçu, dans un délai fixé par décret, une offre de logement tenant compte de ses besoins et de ses capacités peut introduire un recours devant la juridiction administrative tendant à ce que soit ordonné son logement ou son relogement " ; qu'aux termes des quatre derniers alinéas du même I : " Le président du tribunal administratif ou le magistrat qu'il désigne statue en urgence, dans un délai de deux mois à compter de sa saisine (...). / Le président du tribunal administratif ou le magistrat qu'il désigne, lorsqu'il constate que la demande a été reconnue comme prioritaire par la commission de médiation et doit être satisfaite d'urgence et que n'a pas été offert au demandeur un logement tenant compte de ses besoins et de ses capacités, ordonne le logement ou le relogement de celui-ci par l'Etat et peut assortir son injonction d'une astreinte. / Le montant de cette astreinte est déterminé en fonction du loyer moyen du type de logement considéré comme adapté aux besoins du demandeur par la commission de médiation. / Le produit de l'astreinte est versé au fonds national d'accompagnement vers et dans le logement, institué en application de l'article L. 300-2 " ; <br/>
<br/>
              2. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que M. et MmeA..., occupant avec cinq enfants un logement de 55 mètres carrés, ont été reconnus comme prioritaires et devant être relogés en urgence par une décision du 12 octobre 2011 de la commission de médiation du département des Hauts-de-Seine ; que, par un jugement du 3 juillet 2012, devenu définitif, le tribunal administratif de Cergy-Pontoise a enjoint au préfet des Hauts-de-Seine d'assurer leur relogement avant le 21 août 2012 sous astreinte de 35 euros par jour de retard et mis à la charge de l'Etat une somme de 800 euros au titre de l'article L. 761-1 du code de justice administrative ; que M. et Mme A...ont demandé le 16 novembre 2012 au tribunal administratif de Cergy-Pontoise de procéder à la liquidation provisoire de cette astreinte, d'en majorer le montant pour l'avenir et d'enjoindre au préfet des Hauts-de-Seine d'ordonner le versement par l'Etat de la somme mise à sa charge au titre de l'article L. 761-1 ; qu'ils se pourvoient en cassation contre l'ordonnance du magistrat désigné par le président du tribunal administratif de Cergy-Pontoise du 7 mai 2013 en tant que, après avoir procédé à la liquidation provisoire de l'astreinte, elle rejette le surplus de leurs conclusions ; <br/>
<br/>
              Sur l'exécution du jugement du 3 juillet 2012 en tant qu'il met à la charge de l'Etat le versement d'une somme au titre de l'article L. 761-1 du code de justice administrative : <br/>
<br/>
              3. Considérant que M. et Mme A...déclarent se désister de leurs conclusions relatives à l'exécution du jugement du 3 juillet 2012, en tant qu'il met à la charge de l'Etat le versement d'une somme au titre de l'article L. 761-1 du code de justice administrative ; que ce désistement est pur et simple ; que rien ne s'oppose à ce qu'il en soit donné acte ; <br/>
<br/>
              Sur la majoration pour l'avenir de l'astreinte prononcée par ce jugement :<br/>
<br/>
              4. Considérant qu'il résulte des dispositions du code de la construction et de l'habitation citées au point 1 qu'en cas de reconnaissance du caractère urgent et prioritaire de la demande par la commission de médiation et en l'absence d'offre de logement dans un certain délai, le demandeur peut exercer un recours contentieux devant le tribunal administratif ; que le juge saisi d'un tel recours peut, lorsqu'il constate la carence de l'administration, ordonner le logement de l'intéressé en assortissant, le cas échéant, cette injonction d'une astreinte, que l'Etat versera à un fonds d'aménagement urbain régional et dont le montant est déterminé en fonction du loyer moyen du type de logement considéré comme adapté aux besoins du demandeur par la commission de médiation ; qu'en précisant que le montant de l'astreinte devait être déterminé en fonction du loyer moyen du type de logement adapté aux besoins du demandeur, le législateur n'a pas entendu limiter le montant de cette astreinte au montant du loyer moyen d'un tel logement mais permettre que le juge la module, selon les circonstances de l'espèce, en fonction de ce montant, calculé sur la même période que l'astreinte, ainsi que de critères tenant notamment à la taille de la famille, à la vulnérabilité particulière du demandeur et à la célérité et aux diligences de l'Etat ; que rien ne s'oppose à ce que le juge qui, constatant que l'injonction précédemment prononcée n'a pas été suivie d'effet, liquide l'astreinte pour le passé en modifie le montant pour l'avenir ; qu'il suit de là qu'en jugeant que les conclusions par lesquelles M. et Mme A...lui demandaient de faire usage de ce pouvoir en majorant pour l'avenir le montant de l'astreinte fixée par le jugement du 3 juillet 2012 étaient irrecevables comme étrangères à la procédure suivie devant lui, le magistrat désigné par le président du tribunal administratif de Cergy-Pontoise a commis une erreur de droit ; que M. et Mme A...sont fondés à demander l'annulation de son ordonnance en tant qu'elle rejette leurs conclusions relatives à la majoration pour l'avenir de l'astreinte prononcée par le jugement du 3 juillet 2012 ;<br/>
<br/>
              5. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler, dans cette mesure, l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              6. Considérant qu'aux termes de l'article R* 441-14-1 du code de la construction et de l'habitation : " Peuvent être désignées par la commission comme prioritaires et devant être logées d'urgence en application du II de l'article L. 441-2-3 les personnes (...) qui répondent aux caractéristiques suivantes : / (...) avoir à leur charge au moins un enfant mineur, et occuper un logement (...) d'une surface habitable inférieure aux surfaces mentionnées au 2° de l'article D. 542-14 du code de la sécurité sociale " ; qu'entrent dans les prévisions de ce dernier article les logements présentant " une surface habitable globale au moins égale à seize mètres carrés pour un ménage sans enfant ou deux personnes, augmentée de neufs mètres carrés par personne en plus dans la limite de soixante-dix mètres carrés pour huit personnes et plus " ;<br/>
<br/>
              7. Considérant qu'il ressort des pièces produites par le ministre de l'égalité des territoires et du logement qu'un appartement d'une surface de 62 mètres carrés a été proposé le 21 décembre 2012 par la société Coopération et famille à M. et MmeA... ; qu'en les mettant à même d'accepter ce logement, dont la superficie leur aurait permis, eu égard au nombre de personnes composant leur foyer, de ne plus être considérés comme prioritaires au sens des dispositions précitées et dont il n'apparaît pas qu'il ait été, par ailleurs, inadapté à leurs besoins et capacités, le préfet des Hauts-de-Seine doit être regardé comme s'étant acquitté de l'obligation mise à sa charge par les dispositions précitées du code de la construction et de l'habitation  ; qu'ainsi, la demande des époux A...tendant à ce que l'injonction faite au préfet des Hauts-de-Seine d'assurer leur relogement soit assortie d'une astreinte majorée a perdu son objet ; que, par suite, il n'y a pas lieu d'y statuer ; <br/>
<br/>
              8. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante ; <br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Il est donné acte à M. et Mme A...du désistement de leurs conclusions relatives à l'exécution du jugement du 3 juillet 2012, en tant qu'il met à la charge de l'Etat le versement d'une somme au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 2 : L'ordonnance du 7 mars 2013 du magistrat désigné par le président du tribunal administratif de Cergy-Pontoise est annulée, en tant qu'elle rejette les conclusions de M. et Mme A... relatives à la majoration pour l'avenir de l'astreinte prononcée par le jugement du même tribunal du 3 juillet 2012.<br/>
<br/>
Article 3 : Il n'y a pas lieu de statuer sur les conclusions présentées par M. et Mme A...devant le tribunal administratif de Cergy-Pontoise, tendant à la majoration de l'astreinte prononcée par le jugement du 3 juillet 2012.<br/>
<br/>
Article 4 : Les conclusions présentées par M. et Mme A...au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 5 : La présente décision sera notifiée à M. et Mme B... A...et à la ministre du logement, de l'égalité des territoires et de la ruralité. Copie pour information en sera adressée au préfet des Hauts-de-Seine. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
