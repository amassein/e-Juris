<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043852067</ID>
<ANCIEN_ID>JG_L_2021_07_000000433103</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/85/20/CETATEXT000043852067.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème chambre, 21/07/2021, 433103, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-07-21</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>433103</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>CABINET BRIARD</AVOCATS>
<RAPPORTEUR>M. Nicolas Agnoux</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emilie Bokdam-Tognetti</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:433103.20210721</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B... A... a demandé au tribunal administratif de Paris de prononcer la décharge, en droits et pénalités, des cotisations supplémentaires d'impôt sur le revenu et de contributions sociales auxquelles il a été assujetti au titre des années 2012, 2013 et 2014. Par un jugement n° 1712322 du 2 mai 2018, le tribunal administratif de Paris a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 18PA02240 du 29 mai 2019, la cour administrative d'appel de Paris a, sur appel de M. A..., annulé ce jugement puis rejeté sa demande présentée devant le tribunal administratif de Paris. <br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 29 juillet et 29 octobre 2019 et le 18 décembre 2020 au secrétariat du contentieux du Conseil d'Etat, M. A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt en tant qu'il rejette sa demande en décharge ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Nicolas Agnoux, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Emilie Bokdam-Tognetti, rapporteure publique ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, au Cabinet Briard, avocat de M. A... ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que M. A... détient 85 % du capital de la société Wingate dont il est également le gérant. A l'issue d'une vérification de comptabilité de cette société et d'un contrôle sur pièces, l'administration a rehaussé les revenus déclarés par ce dernier des sommes non déclarées dans la catégorie des traitements et salaires, au titre de chacune des années 2012, 2013 et 2014. Elle a également retenu que les frais de location versés par la société Wingate pour l'occupation d'une partie du domicile de M. A... ainsi que les remboursements de frais de déplacement, au cours des années 2013 et 2014, devaient être regardés comme des rémunérations et avantages occultes imposables entre ses mains dans la catégorie des revenus de capitaux mobiliers, sur le fondement du c de l'article 111 du code général des impôts. Par un jugement du 2 mai 2018, le tribunal administratif de Paris a rejeté la demande présentée par M. A... tendant à la décharge des cotisations supplémentaires d'impôt sur le revenu et de contributions sociales ainsi que des pénalités correspondantes mises à sa charge en conséquence de ces rectifications. M. A... se pourvoit en cassation contre l'arrêt du 29 mai 2019 par lequel la cour administrative d'appel de Paris, après avoir annulé ce jugement, a rejeté sa demande en décharge.<br/>
<br/>
              Sur l'arrêt en tant qu'il statue sur les traitements et salaires :<br/>
<br/>
              2. Pour contester le montant des rehaussements correspondant aux traitements et salaires versés par la société Wingate, M. A... a fait valoir devant la cour que l'administration aurait retenu, en méconnaissance du principe d'annualité de l'impôt, le montant inscrit au compte courant ouvert à son nom à la clôture de chaque exercice, soit au 30 juin de chaque année, et non le montant des rémunérations versées au cours de chaque année civile considérée. Toutefois, les extraits de comptabilité reproduits dans la proposition de rectification jointe au dossier soumis aux juges du fond, qui faisaient apparaître la date des versements, permettaient de les rattacher à chacune des trois années d'imposition en litige. Par suite, en se fondant sur les mentions de la proposition de rectification pour écarter le moyen invoqué par M. A..., la cour, qui a suffisamment motivé son arrêt, n'a pas dénaturé les pièces du dossier ni méconnu le principe d'annualité de l'impôt.<br/>
<br/>
              Sur l'arrêt en tant qu'il statue sur les revenus distribués au titre des loyers :<br/>
<br/>
              3. Aux termes de l'article 111 du code général des impôts : " Sont notamment considérés comme revenus distribués : / (...) / c. Les rémunérations et avantages occultes ; / (...) ".<br/>
<br/>
              4. Il ressort des pièces du dossier soumis aux juges du fond que l'administration fiscale a imposé entre les mains de M. A..., en tant que revenus distribués sur le fondement du c de l'article 111 du code général des impôts, les loyers que la société Wingate, dont il est le gérant majoritaire, lui a versés en 2013 et 2014 au titre de locaux situés à son domicile privé. Pour justifier ce chef de redressement, l'administration a soutenu que le versement des frais de location était dépourvu d'intérêt pour la société qui disposait déjà à son siège de locaux de 200 mètres carrés permettant de recevoir les clients. M. A... s'est alors prévalu d'attestations de clients de la société Wingate, décrivant en termes stéréotypés l'intérêt que pouvait présenter les locaux en litige sans faire état de leur utilisation effective, ainsi que de factures adressées à la société Wingate Corporate Finance, lesquelles ne faisaient pas mention de ces mêmes locaux. En déduisant de l'ensemble de ces éléments qu'elle a souverainement appréciés sans les dénaturer, que l'administration devait être regardée comme apportant la preuve, qui lui incombait, de l'absence d'intérêt pour la société Wingate des charges en litige, la cour a suffisamment motivé son arrêt sans méconnaître les règles de dévolution de la charge de la preuve ni commettre d'erreur de droit. <br/>
<br/>
              Sur l'arrêt en tant qu'il statue sur les revenus distribués au titre des indemnités kilométriques :<br/>
<br/>
              5. Il ressort des pièces du dossier soumis aux juges du fond que l'administration a également imposé entre les mains de M. A... les sommes versées en 2013 et 2014 par la société Wingate au titre du remboursement de frais kilométriques, en remettant en cause la réalité de ces frais. A l'appui de ses écritures devant les premiers juges, M. A... a produit un tableau retraçant, pour chaque déplacement, l'identité des clients rencontrés, les dates, lieux et objet des rencontres, ainsi que le nombre de kilomètres parcourus. En confirmant le bien-fondé de ce chef de redressement au motif que M. A... ne démontrait pas, en produisant ce tableau, la réalité du kilométrage qu'il aurait parcouru au titre de ses déplacements, la cour a insuffisamment motivé son arrêt. <br/>
<br/>
              6. Il résulte de ce qui précède, sans qu'il soit besoin d'examiner les autres moyens du pourvoi se rattachant à ce chef de redressement, que M. A... est fondé à demander l'annulation de l'arrêt qu'il attaque, en tant qu'il statue sur les revenus distribués au titre des indemnités kilométriques. <br/>
<br/>
              Sur l'arrêt en tant qu'il statue sur la majoration pour manquement délibéré :<br/>
<br/>
              7. Aux termes de l'article 1729 du code général des impôts : " Les inexactitudes ou les omissions relevées dans une déclaration ou un acte comportant l'indication d'éléments à retenir pour l'assiette ou la liquidation de l'impôt ainsi que la restitution d'une créance de nature fiscale dont le versement a été indûment obtenu de l'Etat entraînent l'application d'une majoration de : / a. 40 % en cas de manquement délibéré ; (...) ". Aux termes de l'article L. 195 A du livre des procédures fiscales : " En cas de contestation des pénalités fiscales appliquées à un contribuable au titre des impôts directs (...), la preuve de la mauvaise foi et des manoeuvres frauduleuses incombe à l'administration ". <br/>
<br/>
              8. Il ressort des énonciations de l'arrêt attaqué que l'administration avait relevé, en ce qui concerne les sommes réintégrées dans le revenu imposable de M. A... dans la catégorie des traitements et salaires, que le contribuable ne pouvait ignorer la nature et le caractère imposable des sommes perçues à ce titre, ni le montant des sommes à déclarer et, en ce qui concerne les sommes requalifiées en revenus distribués, qu'il ne pouvait ignorer que les dépenses dont il avait personnellement bénéficié n'avaient pas été engagées dans l'intérêt de la société Wingate. En déduisant de ces motifs que l'administration devait être regardée comme établissant l'intention délibérée du contribuable de minorer l'impôt et qu'elle était dès lors fondée à appliquer une majoration de 40 %, la cour, qui ne s'est pas, contrairement à ce qu'affirme le pourvoi, fondée uniquement sur l'importance du redressement et la qualification de dirigeant de la société de l'intéressé, n'a pas méconnu le a de l'article 1729 du code général des impôts ni l'article L. 195 A du livre des procédures fiscales. Enfin, à supposer qu'il invoque une erreur de qualification juridique des faits, M. A... ne l'établit pas, ni en faisant valoir que sa rémunération annuelle nette était particulièrement complexe à déterminer, alors qu'il exerçait les fonctions de gérant d'une société d'expertise comptable et de commissariat aux comptes, ni en invoquant les circonstances, inopérantes, tirées de ce que le montant de la rémunération qui lui a été versée était inscrit dans la comptabilité de la société et de ce que celle-ci a été déchargée par un jugement du tribunal administratif de Paris de la majoration pour manquement délibéré initialement mise à sa charge.<br/>
<br/>
              9. En revanche, il résulte de ce qui a été dit au point 6 ci-dessus que le requérant est fondé à demander l'annulation de l'arrêt en tant qu'il statue sur la majoration se rapportant aux revenus distribués au titre des indemnités kilométriques.<br/>
<br/>
              10. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 1 500 euros à verser à M. A... au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : L'arrêt du 29 mai 2019 de la cour administrative d'appel de Paris est annulé en tant qu'il statue sur les revenus distribués au titre des indemnités kilométriques, ainsi que sur les pénalités correspondantes.<br/>
Article 2 : L'affaire est renvoyée, dans cette mesure, à la cour administrative d'appel de Paris.<br/>
Article 3 : L'Etat versera la somme de 1 500 euros à M. A... au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Le surplus des conclusions du pourvoi est rejeté.<br/>
Article 5 : La présente décision sera notifiée à M. B... A... et au ministre de l'économie, des finances et de la relance.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
