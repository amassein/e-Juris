<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033789059</ID>
<ANCIEN_ID>JG_L_2016_12_000000401464</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/78/90/CETATEXT000033789059.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère - 6ème chambres réunies, 28/12/2016, 401464</TITRE>
<DATE_DEC>2016-12-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>401464</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère - 6ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>DELAMARRE ; SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR>Mme Sandrine Vérité</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Rémi Decout-Paolini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2016:401464.20161228</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par un mémoire, enregistré le 20 octobre 2016 au secrétariat du contentieux du Conseil d'Etat, présenté en application de l'article 23-5 de l'ordonnance n° 58-1067 du 7 novembre 1958, M. et Mme A...demandent au Conseil d'Etat, à l'appui de leur pourvoi tendant à l'annulation de l'arrêt n° 14LY00996 du 31 mai 2016 par lequel la cour administrative d'appel de Lyon a rejeté leur appel formé contre le jugement n° 1106752 du 6 février 2014 du tribunal administratif de Grenoble rejetant leur demande tendant à l'annulation de la décision du 20 septembre 2011, confirmée sur recours gracieux le 8 décembre 2011, par laquelle le maire de la commune d'Echirolles a décidé de préempter l'immeuble cadastré AY n° 331, de renvoyer au Conseil constitutionnel la question de la conformité aux droits et libertés garantis par la Constitution des articles L. 210-1, L. 211-1, L. 211-2, L. 213-1, L. 213-2, L. 213-2-1, L. 213-3, L. 213-4 et L. 213-7 du code de l'urbanisme, dans leur rédaction applicable au litige.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la Constitution, notamment son article 61-1 ;<br/>
              - l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
              - le code de l'urbanisme, notamment ses articles L. 210-1, L. 211-1, L. 211-2, L. 213-1, L. 213-2, L. 213-2-1, L. 213-3, L. 213-4 et L. 213-7 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Sandrine Vérité, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Rémi Decout-Paolini, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Delamarre, avocat de M. et Mme A...;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'aux termes de l'article 23-1 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel : " Devant les juridictions relevant du Conseil d'Etat ou de la Cour de cassation, le moyen tiré de ce qu'une disposition législative porte atteinte aux droits et libertés garantis par la Constitution est, à peine d'irrecevabilité, présenté dans un écrit distinct et motivé (...) " ; qu'aux termes de l'article 23-2 de la même ordonnance : " La juridiction statue sans délai par une décision motivée sur la transmission de la question prioritaire de constitutionnalité au Conseil d'Etat ou à la Cour de cassation. Il est procédé à cette transmission si les conditions suivantes sont remplies : / 1° La disposition contestée est applicable au litige ou à la procédure, ou constitue le fondement des poursuites ; / 2° Elle n'a pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances ; / 3° La question n'est pas dépourvue de caractère sérieux. (...) Le refus de transmettre la question ne peut être contesté qu'à l'occasion d'un recours contre la décision réglant tout ou partie du litige " ; que, selon l'article 23-5 de cette ordonnance : " Le moyen tiré de ce qu'une disposition législative porte atteinte aux droits et libertés garantis par la Constitution peut être soulevé, y compris pour la première fois en cassation, à l'occasion d'une instance devant le Conseil d'Etat ou la Cour de cassation. Le moyen est présenté, à peine d'irrecevabilité, dans un mémoire distinct et motivé (...) " ; qu'enfin, aux termes du premier alinéa de l'article R. 771-16 du code de justice administrative : " Lorsque l'une des parties entend contester devant le Conseil d'Etat, à l'appui d'un appel ou d'un pourvoi en cassation formé contre la décision qui règle tout ou partie du litige, le refus de transmission d'une question prioritaire de constitutionnalité précédemment opposé, il lui appartient, à peine d'irrecevabilité, de présenter cette contestation avant l'expiration du délai de recours dans un mémoire distinct et motivé, accompagné d'une copie de la décision de refus de transmission (...) " ; <br/>
<br/>
              2. Considérant qu'il résulte de ces dispositions que, lorsqu'une cour administrative d'appel a refusé de transmettre au Conseil d'Etat la question prioritaire de constitutionnalité qui lui a été soumise, il appartient à l'auteur de cette question de contester ce refus, à l'occasion du pourvoi en cassation formé contre l'arrêt qui statue sur le litige, dans le délai de recours contentieux et par un mémoire distinct et motivé, que le refus de transmission précédemment opposé l'ait été par une décision distincte de l'arrêt, dont il joint alors une copie, ou directement par cet arrêt ; que les dispositions de l'article 23-5 de l'ordonnance du 7 novembre 1958 n'ont ni pour objet ni pour effet de permettre à celui qui a déjà présenté une question prioritaire de constitutionnalité devant une juridiction statuant en dernier ressort de s'affranchir des conditions, définies par les dispositions citées ci-dessus de la loi organique et du code de justice administrative, selon lesquelles le refus de transmission peut être contesté devant le juge de cassation ;<br/>
<br/>
              3. Considérant que, par son arrêt du 31 mai 2016, la cour administrative d'appel de Lyon a refusé de transmettre au Conseil d'Etat la question, soulevée par M. et Mme A...à l'appui de leur appel, de la conformité aux droits et libertés garantis par la Constitution des dispositions de plusieurs articles du code de l'urbanisme, dans leur rédaction applicable à la date de la décision en litige, au motif, s'agissant des articles L. 211-2, L. 213-2-1, L. 213-3, L. 213-4 et L. 213-7, qu'ils n'étaient pas applicables au litige, au sens et pour l'application de l'article 23-2 de l'ordonnance du 7 novembre 1958, et, s'agissant des articles L. 210-1, L. 211-1, L. 213-1 et L. 213-2, que les questions étaient dépourvues de caractère sérieux ;<br/>
<br/>
              4. Considérant, en premier lieu, que si, par un mémoire enregistré le 20 octobre 2016, après l'expiration du délai de pourvoi, M. et Mme A...soulèvent de nouveau des moyens tirés de ce que les dispositions des articles L. 211-2, L. 213-2-1, L. 213-3, L. 213-4 et L. 213-7 du code de l'urbanisme portent  atteinte aux droits et libertés garantis par la Constitution, ils n'ont pas contesté, dans le délai de pourvoi, le refus de transmission des questions prioritaires de constitutionnalité qu'ils avaient formées contre ces dispositions devant la cour, laquelle a jugé ces dispositions inapplicables au litige ; que, par suite, il ne peut être fait droit à leur demande de renvoi au Conseil constitutionnel des questions qu'ils dirigent contre ces dispositions, quels que soient les moyens qu'ils soulèvent ;<br/>
<br/>
              5. Considérant, en second lieu, que, pour demander au Conseil d'Etat de renvoyer au Conseil constitutionnel la question de la conformité aux droits et libertés garantis par la Constitution des articles L. 210-1, L. 211-1, L. 213-1 et L. 213-2 du code de l'urbanisme, M. et Mme A...soutiennent que ces dispositions méconnaissent les articles 2 et 17 de la Déclaration des droits de l'homme et du citoyen de 1789 en ce qu'elles ne précisent pas le délai de réalisation de l'objet qui justifie l'exercice du droit de préemption, portent atteinte au droit du propriétaire de choisir l'acquéreur de son bien et portent atteinte au droit de propriété du propriétaire et de l'acquéreur sans que l'utilité publique du projet soit déclarée ; qu'il ne peut être fait droit à ces questions prioritaires de constitutionnalité, qui portent, par les mêmes moyens, sur les mêmes questions que celles soumises à la cour ;<br/>
<br/>
              6. Considérant qu'ainsi, sans qu'il soit besoin de renvoyer au Conseil constitutionnel les questions prioritaires de constitutionnalité invoquées, les moyens tirés de ce que les articles L. 210-1, L. 211-1, L. 211-2, L. 213-1, L. 213-2, L. 213-2-1, L. 213-3, L. 213-4 et L. 213-7 du code de l'urbanisme portent atteinte aux droits et libertés garantis par la Constitution doivent être écartés ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
                           --------------<br/>
<br/>
Article 1er : Il n'y a pas lieu de renvoyer au Conseil constitutionnel les questions prioritaires de constitutionnalité soulevées par M. et MmeA....<br/>
Article 2 : La présente décision sera notifiée à M. et Mme A...et à la commune d'Echirolles.<br/>
Copie en sera adressée au Conseil constitutionnel, au Premier ministre et à la ministre du logement et de l'habitat durable.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-10-02 PROCÉDURE. - QUESTION POSÉE DEVANT UNE JURIDICTION QUI A REFUSÉ SA TRANSMISSION AU CONSEIL D'ETAT AU MOTIF QUE LA DISPOSITION N'ÉTAIT PAS APPLICABLE AU LITIGE - RECOURS CONTRE LA DÉCISION DE CETTE JURIDICTION STATUANT SUR LE LITIGE - QUESTION SOULEVÉE À NOUVEAU, DEVANT LE JUGE SAISI DE CE RECOURS - IRRECEVABILITÉ, ALORS MÊME QUE SONT SOULEVÉS DES MOYENS NOUVEAUX [RJ1].
</SCT>
<ANA ID="9A"> 54-10-02 Cour administrative d'appel ayant refusé de transmettre une QPC au Conseil d'Etat au motif que la disposition contestée était inapplicable au litige.... ,,Le requérant, qui n'a pas contesté, dans le délai de pourvoi, le refus de transmission de cette QPC, ne peut former devant le Conseil d'Etat une QPC portant sur la même disposition, y compris en soulevant des moyens nouveaux.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1]Cf. CE, 1er février 2011, SARL Prototype Technique Industrie (Prototech), n° 342536, p. 24 ; CE, 16 novembre 2016, M.,, n° 398262, à mentionner aux Tables. Comp. CE, 1er février 2012, Région Centre, n° 351795, T. p. 957.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
