<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043456960</ID>
<ANCIEN_ID>JG_L_2021_04_000000441652</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/45/69/CETATEXT000043456960.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème chambre, 27/04/2021, 441652, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-04-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>441652</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DE NERVO, POUPET</AVOCATS>
<RAPPORTEUR>M. Jonathan  Bosredon</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Karin Ciavaldini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:441652.20210427</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société d'études et de développement patrimonial de la RATP a demandé au tribunal administratif de Cergy-Pontoise de prononcer la décharge de la redevance pour création de bureaux, de locaux commerciaux et de locaux de stockage à laquelle elle a été assujettie à raison des locaux qu'elle a été autorisée à bâtir à Bagneux (Hauts-de-Seine) en vertu d'un permis de construire qui lui a été accordé le 13 octobre 2014. <br/>
<br/>
              Par un jugement n° 1704694 du 28 janvier 2020, ce tribunal administratif de Cergy-Pontoise a prononcé la décharge sollicitée.<br/>
<br/>
              Par un pourvoi, enregistré le 7 juillet 2020 au secrétariat du contentieux du Conseil d'Etat, la ministre de la cohésion des territoires et des relations avec les collectivités territoriales demande au Conseil d'Etat d'annuler ce jugement.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'urbanisme ;<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jonathan Bosredon, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Karin Ciavaldini, rapporteure publique ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP de Nervo, Poupet, avocat de la société d'études et de développement patrimonial de la RATP ;<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que la société d'études et de développement patrimonial de la RATP a été assujettie à la redevance pour création de bureaux, de locaux commerciaux et de locaux de stockage en Ile-de-France prévue par l'article L. 520-1 du code de l'urbanisme à raison d'un permis de construire qui lui a été délivré le 13 octobre 2014 pour la construction d'un centre de traitement des données, dit " data center ", sur le territoire de la commune de Bagneux. La ministre de la cohésion des territoires et des relations avec les collectivités territoriales se pourvoit en cassation contre le jugement du 28 janvier 2020 par lequel le tribunal administratif de Cergy-Pontoise a, sur demande de la société, prononcé la décharge de cette redevance.<br/>
<br/>
              2. D'une part, aux termes de l'article L. 520-1 du code de l'urbanisme, dans sa rédaction applicable au litige : " En région d'Ile-de-France, une redevance est perçue à l'occasion de la construction, de la reconstruction ou de l'agrandissement des locaux à usage de bureaux, des locaux commerciaux et des locaux de stockage définis au III de l'article 231 ter du code général des impôts ". Le III de l'article 231 ter du code général des impôts dispose que la taxe qu'il institue est due, notamment, " (...) 3° Pour les locaux de stockage, qui s'entendent des locaux ou aires couvertes destinés à l'entreposage de produits, de marchandises ou de biens et qui ne sont pas intégrés topographiquement à un établissement de production ". <br/>
<br/>
              3. Pour prononcer la décharge sollicitée, le tribunal administratif a relevé que le site en litige devait être regardé, eu égard au rôle prépondérant des installations techniques, matériels et outillages mis en oeuvre pour les besoins de l'activité qui y était exercée, comme constituant un établissement industriel au sens de l'article 1499 du code général des impôts et en a déduit que ces locaux ne pouvaient être qualifiés de locaux de stockage au sens de l'article L. 520-1 du code de l'urbanisme. <br/>
<br/>
              4. En se fondant ainsi, pour apprécier si les locaux en litige entraient dans le champ de la redevance prévue à l'article L. 520-1 du code de l'urbanisme, sur la seule circonstance que ces locaux devaient être qualifiés d'établissement industriel au sens et pour l'application des dispositions de l'article 1499 du code général des impôts relatives à la détermination de la valeur locative à retenir pour l'établissement de la taxe foncière sur les propriétés bâties, sans rechercher s'ils constituaient des locaux destinés à l'entreposage de produits, de marchandises ou de biens non intégrés topographiquement à un établissement de production, au sens du 3° du III de l'article 231 ter du même code auquel renvoient les dispositions applicables du code de l'urbanisme, le tribunal administratif a entaché son jugement d'une erreur de droit.<br/>
<br/>
              5. Il résulte de ce qui précède que la ministre de la cohésion des territoires et des relations avec les collectivités territoriales est fondée, sans qu'il soit besoin de se prononcer sur les autres moyens de son pourvoi, à demander l'annulation du jugement qu'elle attaque.<br/>
<br/>
              6. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du 28 janvier 2020 du tribunal administratif de Cergy-Pontoise est annulé.<br/>
Article 2 : L'affaire est renvoyée au tribunal administratif de Cergy-Pontoise.<br/>
Article 3 : Les conclusions de la société d'études et de développement patrimonial de la RATP présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée à la ministre de la cohésion des territoires et des relations avec les collectivités territoriales et à la société d'études et de développement patrimonial de la RATP.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
