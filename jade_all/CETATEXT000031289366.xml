<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031289366</ID>
<ANCIEN_ID>JG_L_2015_10_000000391347</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/28/93/CETATEXT000031289366.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  7ème sous-section jugeant seule, 07/10/2015, 391347, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-10-07</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>391347</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 7ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FOUSSARD, FROGER ; SCP COUTARD, MUNIER-APAIRE</AVOCATS>
<RAPPORTEUR>Mme Sophie Roussel</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Gilles Pellissier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2015:391347.20151007</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              Mme B...A...a demandé au juge des référés du tribunal administratif de Pau, sur le fondement de l'article L. 521-3 du code de justice administrative, d'enjoindre à la société Electricité Réseau Distribution France (ERDF), sous astreinte de 1 000 euros par jour de retard à compter de la notification de la décision à intervenir, de procéder à l'enfouissement d'une ligne électrique endommagée hors de sa propriété ou à la réalisation de tout autre ouvrage alternatif, de sécuriser les lieux et de procéder à l'enlèvement de tous les éléments d'ouvrage traversant sa propriété.<br/>
<br/>
              Par une ordonnance n° 1500569 du 10 juin 2015, le juge des référés du tribunal administratif de Pau a rejeté sa demande.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 26 juin et 10 juillet 2015 au secrétariat du contentieux du Conseil d'Etat, Mme A...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance ; <br/>
<br/>
              2°) statuant en référé, de faire droit à sa demande ;<br/>
<br/>
              3°) de mettre à la charge de la société ERDF une somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
              Vu :<br/>
              - le code de l'énergie ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Sophie Roussel, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Gilles Pellissier, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Foussard, Froger, avocat de Mme B...A...et à la SCP Coutard, Munier-Apaire, avocat de la société ERDF ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article L. 521-3 du code de justice administrative : " En cas d'urgence et sur simple requête qui sera recevable même en l'absence de décision administrative préalable, le juge des référés peut ordonner toutes mesures utiles sans faire obstacle à l'exécution d'aucune mesure administrative " ; que pour prévenir ou faire cesser un dommage dont l'imputabilité à un ouvrage public ne se heurte à aucune contestation sérieuse, le juge des référés peut, sur le fondement de ces dispositions, enjoindre au responsable du dommage de prendre des mesures conservatoires destinées à faire échec ou mettre un terme aux dangers immédiats présentés par l'état de l'immeuble ;<br/>
<br/>
              2. Considérant qu'il ressort des pièces du dossier soumis au juge des référés que des intempéries ont provoqué la chute de câbles électriques dans la propriété de Mme A... et ont endommagé les pylônes électriques la traversant, dont l'un menaçait de s'effondrer ;  <br/>
<br/>
              3. Considérant que le juge du référé statuant sur le fondement de l'article L. 521-3 du code de justice administrative est compétent pour enjoindre au propriétaire d'ouvrages publics de prendre toute mesure afin de remédier aux risques que ces ouvrages peuvent représenter pour la sécurité des occupants des parcelles sur lesquelles ils sont implantés, et en particulier toute mesure destinée à faire cesser et prévenir les dommages qui peuvent leur être causés lorsqu'ils sont de nature à remettre en cause leur sécurité, quand bien même des évènements naturels telles que des intempéries sont à l'origine de ces risques ; que, par suite, en rejetant la demande de Mme A...au motif que la cause des dommages affectant les ouvrages publics et susceptibles de menacer sa sécurité seraient des évènements naturels, le juge des référés du tribunal administratif de Pau a commis une erreur de droit ; que, par conséquent, sans qu'il soit besoin d'examiner les autres moyens de son pourvoi, Mme A...est fondée à demander l'annulation de l'ordonnance qu'elle attaque ;<br/>
<br/>
              4. Considérant que, dans les circonstances de l'espèce, il y a lieu, en application de l'article L. 821-2 du code de justice administrative, de régler l'affaire au titre de la procédure de référé engagée par Mme A...;<br/>
<br/>
              5. Considérant, en premier lieu, qu'il résulte de l'instruction que la société ERDF a commencé à prendre, dès la fin du mois de février 2015, des mesures destinées à supprimer les dangers que pouvaient représenter pour Mme A...et les autres occupants de sa propriété la chute des câbles électriques et le risque de chute de certains pylônes endommagés par les intempéries survenues à la fin du mois de janvier et au début du mois de février 2015 ; que, par suite, les conclusions de Mme A...tendant à ce qu'il soit enjoint à la société ERDF d'assurer la sécurité des lieux sont devenues sans objet ;<br/>
<br/>
              6. Considérant, en second lieu, qu'il résulte de l'instruction que, par deux arrêtés des 2 avril et 4 octobre 2013, le préfet des Hautes-Pyrénées a déclaré d'utilité publique le tracé de la ligne existante traversant la propriété de Mme A...et autorisé, au profit de la société ERDF, les servitudes légales concernant les trois parcelles constituant cette propriété, servitudes prévues aux articles L. 323-3 et suivants du code de l'énergie ; que l'exécution de ces décisions fait obstacle à ce qu'il soit enjoint à la société ERDF de procéder à l'enfouissement de la ligne électrique hors de sa propriété ou à la réalisation de tout autre ouvrage alternatif et de procéder à l'enlèvement de tous les éléments d'ouvrage traversant sa propriété ; que ces conclusions de Mme A...doivent en conséquence être rejetées ; <br/>
<br/>
              7. Considérant qu'il n'y a pas lieu, dans les circonstances de l'espèce, de mettre à la charge de la société ERDF la somme que demande Mme A...au titre des dispositions de l'article L. 761-1 du code de justice administrative ; qu'en revanche, il y a lieu, dans les circonstances de l'espèce, de mettre une somme de 2 000 euros à la charge de cette dernière au titre des frais exposés par la société ERDF, tant devant le Conseil d'Etat que le tribunal administratif de Pau ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er  : L'ordonnance du juge des référés du tribunal administratif de Pau du 10 juin 2015 est annulée.<br/>
<br/>
Article 2 : Il n'y a pas lieu de statuer sur les conclusions de Mme A...tendant à ce qu'il soit enjoint à la société ERDF d'assurer la sécurité de sa propriété.<br/>
<br/>
Article 3 : Le surplus des conclusions de la demande présentée par Mme A...devant le tribunal administratif de Pau et ses conclusions tendant à l'application des dispositions de l'article L. 761-1 du code de justice administrative sont rejetés.<br/>
<br/>
Article 4 : Mme A...versera à la société ERDF une somme de 2 000 euros en application des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 5 : La présente décision sera notifiée à Mme B...A...et à la société Electricité Réseau Distribution France.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
