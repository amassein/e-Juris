<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042528926</ID>
<ANCIEN_ID>JG_L_2020_11_000000431120</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/52/89/CETATEXT000042528926.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère - 4ème chambres réunies, 16/11/2020, 431120, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-11-16</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>431120</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère - 4ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>LE PRADO</AVOCATS>
<RAPPORTEUR>Mme Bénédicte Fauvarque-Cosson</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Vincent Villette</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:431120.20201116</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 27 mai et 27 août 2019 et le 14 janvier 2020 au secrétariat du contentieux du Conseil d'Etat, l'association pour la formation et l'enseignement en France de la chiropraxie, exerçant ses activités sous la dénomination d'Institut franco-européen de chiropraxie (IFEC), et l'association française de chiropraxie (AFC) demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision du collège de la Haute Autorité de santé du 27 mars 2019 adoptant la fiche mémo intitulée " Prise en charge du patient présentant une lombalgie commune " et les documents associés ;<br/>
<br/>
              2°) de mettre à la charge de la Haute Autorité de santé la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de la santé publique ;<br/>
              - le code de la sécurité sociale ;<br/>
              - le code des relations entre le public et l'administration ;<br/>
              - la loi n° 2002-303 du 4 mars 2002 ;<br/>
              - la loi n° 2017-55 du 20 janvier 2017 ;<br/>
              - le décret n° 2011-32 du 7 janvier 2011 ;<br/>
              - le décret n° 2013-413 du 21 mai 2013 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme A... B..., conseillère d'Etat,  <br/>
<br/>
              - les conclusions de M. Vincent Villette, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Le Prado, avocat de l'association pour la formation et l'enseignement en France de la chiropraxie et de l'association française de chiropraxie ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Aux termes de l'article L. 161-37 du code de la sécurité sociale : " La Haute Autorité de santé, autorité publique indépendante à caractère scientifique, est chargée de : / (...) 2° Elaborer les guides de bon usage des soins ou les recommandations de bonne pratique, procéder à leur diffusion et contribuer à l'information des professionnels de santé et du public dans ces domaines (...) ". <br/>
<br/>
              2. Il ressort des pièces du dossier que la Caisse nationale de l'assurance maladie et la direction générale de l'offre de soins du ministère des solidarités et de la santé ont saisi la Haute Autorité de santé en vue d'élaborer des recommandations de bonne pratique pour la prise en charge du patient présentant une lombalgie commune, à destination des professionnels de santé concernés. En mars 2019, un groupe de travail constitué par la Haute Autorité a rédigé une " fiche mémo " intitulée " Prise en charge du patient présentant une lombalgie commune ", adoptée, en même temps qu'un arbre décisionnel de prise en charge du patient et le rapport d'élaboration associé, par une délibération du collège de la Haute Autorité du 27 mars 2019. Cette " fiche mémo ", qui entre dans le champ des actes que la Haute Autorité peut prendre sur le fondement de l'article L. 161-37 du code de la sécurité sociale, comporte des recommandations visant à améliorer et harmoniser la prise en charge du patient présentant une lombalgie commune en guidant les professionnels de santé dans les différentes étapes du diagnostic et du traitement, afin de réduire le risque de chronicité de la lombalgie et de désinsertion professionnelle. Bien qu'élaborée dans un temps plus court et revêtant un caractère plus bref qu'une recommandation de bonne pratique et destinée à formuler un petit nombre de recommandations ou messages ciblés, elle présente également le caractère d'un acte faisant grief. L'association pour la formation et l'enseignement en France de la chiropraxie, exerçant ses activités sous la dénomination d'Institut franco-européen de chiropraxie, et l'association française de chiropraxie demandent au Conseil d'Etat d'annuler pour excès de pouvoir la délibération du collège de la Haute Autorité de santé et les documents qu'elle approuve, dont la " fiche mémo ". Elles critiquent cette fiche, plus particulièrement, en ce qu'elle prévoit, d'une part, que le diagnostic médical de lombalgie commune doit avoir été posé avant d'envisager une prise en charge non médicamenteuse et, d'autre part, que les techniques manuelles sont possibles en deuxième intention seulement. <br/>
<br/>
              Sur la procédure d'adoption de la " fiche mémo " :<br/>
<br/>
              3.  En premier lieu, la charte de l'expertise sanitaire annexée au décret du 21 mai 2013 portant approbation de la charte de l'expertise sanitaire prévue à l'article L. 1452-2 du code de la santé publique prévoit que : " L'organisme chargé de la réalisation de l'expertise est responsable de son organisation ainsi que du choix et de la mise en oeuvre des méthodes appropriées pour répondre aux questions posées. / L'expertise collective est une modalité à privilégier lorsque l'objet de l'expertise est particulièrement complexe ou nécessite une approche pluridisciplinaire. Dans toutes les hypothèses, y compris dans le cas où il est recouru à un expert unique, l'expertise doit s'appuyer sur : (...) la confrontation de différentes opinions, thèses ou écoles de pensées (...) ". Elle précise également que : " Chaque organisme chargé de la réalisation d'une expertise rend public son processus de désignation ou de sélection des experts ". A ce titre, d'une part, le guide méthodologique relatif à la phase de cadrage des recommandations, adopté par la Haute Autorité de santé en décembre 2010 et mis à jour en décembre 2014, prévoit la rédaction d'une note de cadrage par un chef de projet de l'autorité, présentant notamment le type de document et la composition des groupes envisagés, en vue de sa validation par le collège de l'autorité. D'autre part, le guide méthodologique relatif à l'élaboration des " fiches mémo " et des " fiches pertinence ", adopté par la Haute Autorité de santé en avril 2016, prévoit que le groupe de travail, qui a pour mission de discuter les recommandations ou messages clés de la fiche après avoir pris connaissance des données bibliographiques disponibles et de l'avis des parties prenantes, doit réunir les personnes concernées par le thème traité, qui peuvent être des professionnels de santé médecins et non médecins, des chercheurs, des membres d'associations de patients ou d'usagers du système de santé, ainsi que des représentants d'agences publiques si nécessaire.<br/>
<br/>
              4. Il ressort des pièces du dossier qu'après inscription du thème au programme de travail de la Haute Autorité de santé, la phase de cadrage a été mise en oeuvre, sans qu'une réunion de cadrage, envisagée à titre facultatif par le guide méthodologique mentionné ci-dessus, soit organisée avec les parties prenantes concernées, en l'absence de difficulté particulière. Le groupe de travail a été composé de médecins généralistes et spécialistes, de masseurs-kinésithérapeutes, d'un psychologue et de deux représentants d'usagers. Enfin, une version provisoire de la fiche mémo accompagnée de son rapport d'élaboration a été envoyée pour avis, le 22 janvier 2019, aux parties prenantes, au nombre desquelles la société française d'ostéopathie, la société française de médecine manuelle orthopédique et ostéopathique et l'association française de chiropraxie, dont les avis ont été discutés par le groupe de travail lors de sa  dernière séance, le 26 février 2019, au cours de laquelle a été arrêté le contenu de la fiche mémo soumise au collège de la Haute Autorité. Si les associations requérantes reprochent à cette autorité de ne pas avoir associé les chiropracteurs, ostéopathes et professionnels de l'activité physique à la phase de cadrage ni assuré leur représentation au sein du groupe de travail, les modalités d'élaboration de la " fiche mémo " ainsi retenues, qui ont permis la consultation d'organisations représentant ces professionnels en qualité de parties prenantes, n'ont méconnu aucune règle ni aucun principe applicables à l'élaboration de ce type de document. <br/>
<br/>
              5. En second lieu, l'article R. 161-85 du code de la sécurité sociale prévoit que les personnes collaborant occasionnellement aux travaux de la Haute Autorité de santé et les personnes qui apportent leur concours à son collège ne peuvent traiter une question dans laquelle elles auraient un intérêt direct ou indirect. En application des dispositions combinées de cet article et de l'article L. 1451-1 du code de la santé publique, elles ne peuvent prendre part ni aux travaux ni aux délibérations ni aux votes de l'instance au sein de laquelle elles siègent si elles ont un intérêt direct ou indirect à l'affaire examinée. Enfin, ainsi que le rappelle d'ailleurs la charte déontologique adoptée par le collège de la Haute Autorité de santé, il leur incombe de s'abstenir de toute prise de position publique qui serait de nature à compromettre le respect du principe d'impartialité.<br/>
<br/>
              6. Les associations requérantes font valoir que l'un des membres du groupe de travail chargé de l'élaboration de la fiche mémo, professeur de médecine, a publiquement fait part de son hostilité à la pratique de la chiropraxie à l'occasion d'un congrès organisé en juin 2018 ainsi que dans un courriel du 1er mai 2019, largement diffusé, expliquant les motifs de sa démission de l'une des sections de la société française de rhumatologie, ce qui entacherait d'irrégularité les travaux ayant abouti à l'adoption de ce document. Il ne ressort toutefois pas des pièces du dossier que ce praticien aurait été mû par un intérêt personnel ou aurait eu un intérêt, au sens de l'article L. 1451-1 du code de la santé publique, aux questions soumises au groupe de travail. Il n'en ressort pas davantage que les propos tenus en juin 2018, visant à dénoncer, au vu de son expérience et de sa pratique, les dérives de certaines personnes se réclamant de la chiropraxie, auraient fait obstacle à ce qu'il prenne part ultérieurement, dans le respect du principe d'impartialité qui s'impose à tous les organes administratifs, aux travaux du groupe de travail, ou auraient eu pour conséquence que la composition de ce groupe devrait être regardée comme manifestement déséquilibrée. Dans ces conditions, et alors que le courriel du 1er mai 2019 également invoqué est postérieur aux actes attaqués, les associations requérantes ne sont pas fondées à soutenir que ces actes auraient été pris au terme de travaux entachés de partialité.<br/>
<br/>
              Sur le contenu de la " fiche mémo " : <br/>
<br/>
              7. En premier lieu, aux termes du sixième alinéa de l'article 75 de la loi du 4 mars 2002 relative aux droits des malades et à la qualité du système de santé : " Un décret établit la liste des actes que les praticiens justifiant du titre d'ostéopathe ou de chiropracteur sont autorisés à effectuer, ainsi que les conditions dans lesquelles il sont appelés à les accomplir ". Aux termes de l'article 1er du décret du 7 janvier 2011 relatif aux actes et aux conditions d'exercice de la chiropraxie : " Les praticiens justifiant d'un titre de chiropracteur sont autorisés à pratiquer des actes de manipulation et mobilisation manuelles, instrumentales ou assistées mécaniquement, directes et indirectes, avec ou sans vecteur de force, ayant pour seul but de prévenir ou de remédier à des troubles de l'appareil locomoteur du corps humain et de leurs conséquences, en particulier au niveau du rachis, à l'exclusion des pathologies organiques qui nécessitent une intervention thérapeutique, médicale, chirurgicale, médicamenteuse ou par agents physiques. Ils exercent dans le respect des recommandations de bonnes pratiques établies par la Haute Autorité de santé. / Ces actes de manipulation et mobilisation sont neuro-musculo-squelettiques, exclusivement externes. Ils peuvent être complétés par des conseils ou des techniques non invasives, conservatrices et non médicamenteuses à visée antalgique ". Aux termes de l'article 2 de ce décret : " Les praticiens justifiant d'un titre de chiropracteur sont tenus, s'ils n'ont pas eux-mêmes la qualité de médecin, d'orienter le patient vers un médecin lorsque les symptômes nécessitent un diagnostic ou un traitement médical, lorsqu'il est constaté une persistance ou une aggravation de ces symptômes ou que les troubles présentés excèdent leur champ de compétences ". Enfin, aux termes du II de l'article 3 de ce décret : " Après un diagnostic établi par un médecin attestant l'absence de contre-indication médicale à la chiropraxie, le praticien justifiant d'un titre de chiropracteur est habilité à effectuer les manipulations du crâne, de la face et du rachis chez le nourrisson de moins de six mois ".<br/>
<br/>
              8. La " fiche mémo " décrit la lombalgie commune comme une douleur lombaire sans signes d'alerte permettant de suspecter une pathologie sous-jacente qui nécessiterait une prise en charge spécifique ou urgente et prévoit que le diagnostic médical de lombalgie commune doit avoir été posé avant que soit envisagée une prise en charge non médicamenteuse. Elle présente cette prise en charge comme comportant, en première intention, l'autogestion et la reprise des activités quotidiennes, les activités physiques adaptées et sportives et, en cas de lombalgie chronique ou à risque de chronicité, la kinésithérapie, en deuxième intention, l'éducation à la neurophysiologie de la douleur, les techniques manuelles telles que manipulations et mobilisations et les interventions psychologiques de type techniques cognitivo-comportementales et, en troisième intention, les programmes de réadaptation pluridisciplinaire physique, psychologique, sociale et professionnelle. En recommandant ainsi qu'un diagnostic médical de lombalgie commune soit posé au préalable et en faisant figurer les techniques manuelles en deuxième intention, la Haute Autorité de santé n'a pas modifié les conditions dans lesquelles les chiropracteurs sont autorisés à effectuer des actes mais précisé les bonnes pratiques applicables au traitement de cette pathologie, pour éviter un retard de diagnostic et une perte de chance pour le patient et pour adapter au mieux le type de prise en charge selon les caractéristiques, et notamment le risque de chronicité, de l'affection dont le patient est atteint. Par suite, les associations requérantes ne sont pas fondées à soutenir que la Haute Autorité de santé aurait excédé sa compétence ni que la " fiche mémo " méconnaîtrait les dispositions de la loi du 4 mars 2002 et du décret du 7 janvier 2011 citées ci-dessus au point 7.<br/>
<br/>
              9. En deuxième lieu, il ressort des pièces du dossier que les recommandations de la " fiche mémo " reposent sur l'analyse de l'ensemble des recommandations de bonne pratique françaises et étrangères publiées en français ou en anglais entre 2013 et 2018 et de toutes les revues systématiques d'essais contrôlés publiées dans l'une de ces deux langues entre 2016 et 2018 - les revues plus anciennes ayant été prises en compte dans les recommandations également analysées - et ne peuvent être regardées, contrairement à ce que soutiennent les associations requérantes, comme procédant d'une analyse incomplète ou biaisée des données scientifiques existantes. S'agissant, d'une part, de la recommandation de diagnostic médical préalable à une prise en charge non médicamenteuse, si les associations requérantes font valoir que différentes études ont montré l'absence de risque présenté par les manipulations vertébrales, ce constat ne contredit en rien l'utilité d'un diagnostic médical préalable, pour éviter un retard de diagnostic et une perte de chance pour le patient en cas de pathologie sous-jacente, et il ne ressort pas des pièces du dossier que cette recommandation, alors même qu'elle est seulement fondée sur un accord d'experts, constaté au sein du groupe de travail, serait entachée d'erreur manifeste d'appréciation. S'agissant, d'autre part, de la présentation des techniques manuelles en deuxième intention seulement parmi les prises en charge non médicamenteuses, il ressort des pièces du dossier, notamment des recommandations étrangères existantes, que la Haute Autorité de santé n'a pas commis d'erreur manifeste d'appréciation en estimant que l'exercice physique devait être regardé comme le traitement principal permettant une évolution favorable de la lombalgie commune et, dès lors, en proposant les techniques manuelles, dans le cadre d'une combinaison de traitements incluant un programme d'exercices supervisés, en deuxième intention. <br/>
<br/>
              10. En dernier lieu, les actes attaqués, qui visent à améliorer la prise en charge des patients présentant une lombalgie commune en précisant les bonnes pratiques recommandées, sans modifier la réglementation applicable ni bouleverser les conditions d'exercice des professionnels concernés, ne méconnaissent pas, en tout état de cause, le principe de sécurité juridique. <br/>
<br/>
              11. Il résulte de tout ce qui précède que les associations requérantes ne sont pas fondées à demander l'annulation des actes qu'elles attaquent. <br/>
<br/>
              Sur les frais liés au litige :<br/>
<br/>
              12. Par suite, les conclusions des associations requérantes présentées au titre de l'article L. 761-1 du code de justice administrative doivent être également rejetées.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de l'association pour la formation et l'enseignement en France de la chiropraxie et de l'association française de chiropraxie est rejetée.<br/>
Article 2 : La présente décision sera notifiée à l'association pour la formation et l'enseignement en France de la chiropraxie, à l'association française de chiropraxie et à la Haute Autorité de santé. <br/>
Copie sera transmise au ministre des solidarités et de la santé<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
