<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028110437</ID>
<ANCIEN_ID>JG_L_2013_10_000000347460</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/11/04/CETATEXT000028110437.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème et 1ère sous-sections réunies, 23/10/2013, 347460, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-10-23</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>347460</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème et 1ère sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MASSE-DESSEN, THOUVENIN, COUDRAY</AVOCATS>
<RAPPORTEUR>M. Eric Aubry</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Suzanne von Coester</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:347460.20131023</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête sommaire et le mémoire complémentaire, enregistrés les 14 mars et 14 juin 2011 au secrétariat du contentieux du Conseil d'Etat, présentés pour l'association nationale des enquêteurs sociaux, dont le siège est au Palais de Justice à Paris (75001) ; l'Association nationale des enquêteurs sociaux demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir l'arrêté du 13 janvier 2011 pris en application de l'article 12 du décret n° 2009-285 du 12 mars 2009 relatif aux enquêteurs sociaux et à la tarification des enquêtes sociales en matière civile ;<br/>
<br/>
              2°) d'enjoindre au garde des sceaux, ministre de la justice et au ministre du budget d'édicter des dispositions appelées à se substituer aux dispositions annulées dans les meilleurs délais ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ; <br/>
<br/>
              Vu le code de procédure civile ;<br/>
<br/>
              Vu le code de procédure pénale ;<br/>
<br/>
              Vu le décret n° 2009-285 du 12 mars 2009, modifié par le décret n° 2011-54 du 13 janvier 2011 et par le décret n° 2013-770 du 26 août 2013 ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Eric Aubry, Conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Suzanne von Coester, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Masse-Dessen, Thouvenin, Coudray, avocat de l'Association nationale des enquêteurs sociaux ;<br/>
<br/>
<br/>
<br/>1. Considérant, d'une part, qu'aux termes de l'article 1072 du code de procédure civile, relatif aux enquêtes en matière familiale : " Sans préjudice de toute autre mesure d'instruction et sous réserve des dispositions prévues au troisième alinéa de          l'article  373-2-12 du code civil, le juge peut, même d'office, ordonner une enquête sociale s'il s'estime insuffisamment informé par les éléments dont il dispose. L'enquête sociale porte sur la situation de la famille ainsi que, le cas échéant, sur les possibilités de réalisation du projet des parents ou de l'un d'eux quant aux modalités d'exercice de l'autorité parentale. Elle donne lieu à un rapport où sont consignées les constatations faites par l'enquêteur et les solutions proposées par lui. (...) " ; qu'aux termes de l'article 1171 du même code : " Le tribunal vérifie si les conditions légales de l'adoption sont remplies dans un délai de six mois à compter soit du dépôt de la requête, soit de sa transmission dans le cas prévu au second alinéa de l'article 1168. S'il y a lieu, il fait procéder à une enquête par toute personne qualifiée. Il peut commettre un médecin aux fins de procéder à tout examen qui lui paraîtrait nécessaire (...) " ; qu'enfin, aux termes de l'article 1221 du même code, relatif aux enquêtes réalisées dans le cadre de la protection juridique des mineurs et des majeurs: " Le juge peut, soit d'office, soit à la requête des parties ou du ministère public, ordonner toute mesure d'instruction. Il peut notamment faire procéder à une enquête sociale ou à des constatations par toute personne de son choix " ;<br/>
<br/>
              2. Considérant, d'autre part, qu'en vertu de l'article 12 du décret du 12 mars 2009 relatif aux enquêteurs sociaux et à la tarification des enquêtes sociales en matière civile, dans sa rédaction issue du décret du 13 janvier 2011 et applicable à la date de l'arrêté attaqué, le juge alloue aux enquêteurs sociaux désignés en application des dispositions précédemment citées du code de procédure civile une rémunération forfaitaire par enquête, fixée par arrêté conjoint du ministre chargé du budget et du garde des sceaux, ministre de la justice, qui peut être réduite en cas de retard dans l'accomplissement de la mission ou d'insuffisance du rapport ; que le même article prévoit l'allocation d'une indemnité de carence, dont les modalités sont fixées par le même arrêté, en cas d'impossibilité pour l'enquêteur d'accomplir sa mission pour une cause qui lui est étrangère ; qu'il est, en outre, prévu que, dans tous les cas, les enquêteurs sont remboursés de leurs frais de déplacement par une indemnité forfaitaire par enquête dont le montant est fixé par arrêté conjoint du garde des sceaux ministre de la justice et du ministre chargé du budget ; que l'article 12 du décret renvoie également à un arrêté du  garde des sceaux, ministre de la justice, le soin de déterminer un référentiel des exigences devant être accomplies lors d'une enquête sociale ; que l'arrêté conjoint attaqué, du 13 janvier 2011, modifie l'article A. 43-12 du code de procédure pénale pour prévoir que le tarif de l'enquête sociale est fixé à 600 euros pour une personne physique et 700 euros pour une personne morale, que l'indemnité de carence est fixée à 30 euros et, au troisième alinéa de cet article, que le montant de l'indemnité de déplacement est fixé à 50 euros ;<br/>
<br/>
              Sur l'exception d'illégalité de l'article 12 du décret du 12 mars 2009 relatif aux enquêteurs sociaux et à la tarification des enquêtes sociales en matière civile :<br/>
<br/>
              3. Considérant, d'une part, que pour les mêmes motifs que ceux qui ont déjà été portés à la connaissance de la requérante par la décision du Conseil d'Etat, statuant au contentieux, n° 327827 du 18 octobre 2010, l'article 12 du décret du 12 mars 2009 pouvait légalement donner à la rémunération des enquêteurs sociaux un caractère forfaitaire et renvoyer à un arrêté ministériel le soin de fixer le montant de cette rémunération ; qu'ainsi, le moyen tiré de ce que la fixation d'une même rémunération pour toutes les enquêtes serait contraire au principe d'égalité ne saurait être accueilli ; qu'est sans incidence sur ce point les conclusions d'un rapport administratif postérieur à la décision juridictionnelle rappelée ci-dessus ; que le forfait de rémunération correspond à des prestations définies par un référentiel commun fixant les diligences identiques à accomplir quel que soit le motif de l'enquête diligentée par le juge en matière civile ; que, dès lors, le moyen tiré d'une violation du principe d'égalité ne peut qu'être écarté ; <br/>
<br/>
              4. Considérant, d'autre part, que le caractère forfaitaire de la rémunération des enquêteurs sociaux n'est pas, pour les mêmes motifs que ceux indiqués dans la décision du 18 octobre 2010, de nature à compromettre la qualité des enquêtes sociales ; que l'association requérante ne saurait utilement soutenir que les dispositions litigieuses  méconnaissent le droit à un procès équitable mentionné à l'article 6 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ; <br/>
<br/>
              5. Considérant qu'il suit de là que le moyen tiré, par la voie de l'exception, de l'illégalité de l'article 12 du décret ne peut qu'être écarté ; <br/>
<br/>
              Sur les moyens directement articulés à l'encontre de l'arrêté du 13 janvier 2011 fixant le montant du forfait de rémunération et du forfait de remboursement des frais de déplacement par enquête sociale :<br/>
<br/>
              6. Considérant, qu'aucune disposition n'imposait au pouvoir réglementaire de procéder à des consultations préalables ; que si les associations représentant les enquêteurs sociaux ont été associées au rapport administratif mentionné au point 3, qui a conduit à la modification du décret du 12 mars 2009, l'administration n'a pas entendu pour autant instituer une règle de procédure lui imposant, à l'issue de cette étude, de soumettre les textes aux organisations représentatives de la profession ; que, par suite, l'association requérante n'est pas fondée à soutenir que l'arrêté qu'elle attaque a été pris à la suite d'une procédure irrégulière ; <br/>
<br/>
              7. Considérant que le forfait de rémunération de 600 ou 700 euros déterminé par l'arrêté attaqué, après concertation avec les associations représentatives, correspond à des prestations précisément définies par le référentiel établi sur le fondement du même article 12 du décret précité ; qu'il ne ressort pas des pièces du dossier que ces montants seraient entachés d'une erreur manifeste d'appréciation au regard du contenu et des conditions de réalisation des enquêtes ; <br/>
<br/>
              8. Considérant, en revanche, qu'en fixant à un montant limité à 50 euros, sans aucune possibilité de modulation, l'indemnité de déplacement, alors que les enquêteurs sociaux sont susceptibles d'exposer, pour les besoins de leurs missions, des frais de déplacement et d'hébergement beaucoup plus élevés, l'auteur de l'arrêté attaqué a entaché sa décision d'une erreur manifeste d'appréciation ; <br/>
<br/>
              9. Considérant qu'il résulte de tout ce qui précède que l'association requérante est seulement fondée à demander l'annulation de l'article 1er de l'arrêté attaqué en tant qu'il fixe le montant forfaitaire de l'indemnité de déplacement des enquêteurs sociaux ; <br/>
<br/>
              Sur les conclusions à fin d'injonction :<br/>
<br/>
              10. Considérant que le décret du 26 août 2013 relatif aux frais de justice a abrogé le dernier alinéa de l'article 12 du décret du 12 mars 2009, dans sa rédaction issue du décret du 13 janvier 2011, pour prévoir désormais que : " Lorsque les enquêteurs se déplacent, il leur est alloué, sur justification, des indemnités calculées dans les conditions fixées pour les déplacements des personnels civils de l'Etat " ; que, dès lors, les conclusions de l'association requérante tendant à ce qu'il soit enjoint à l'administration d'édicter de nouvelles dispositions relatives à l'indemnisation des frais de déplacement sont devenues sans objet ; qu'il n'y a, dès lors, plus lieu d'y statuer ;	<br/>
<br/>
              Sur les conclusions présentées au titre de l'article L 761-1 du code de justice administrative :<br/>
<br/>
              11. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 3 000 euros  à verser à l'Association nationale des enquêteurs sociaux au titre des dispositions de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'article 1er de l'arrêté du 13 janvier 2011 est annulé en tant qu'il fixe le montant forfaitaire de l'indemnité de déplacement des enquêteurs sociaux . <br/>
<br/>
Article 2 : Il n'y a pas lieu de statuer sur les conclusions à fin d'injonction présentée par l'association requérante.<br/>
<br/>
Article 3 : L'Etat versera à l'Association nationale des enquêteurs sociaux une somme de 3 000 euros au titre de l'article L.761-1 du code de justice administrative. <br/>
<br/>
      Article 4 : Le surplus des conclusions de la requête est rejeté.<br/>
<br/>
Article 5 : La présente décision sera notifiée à l'Association nationale des enquêteurs sociaux, à la garde des sceaux, ministre de la justice et au ministre de l'économie et des finances.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
