<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042105497</ID>
<ANCIEN_ID>JG_L_2020_07_000000431697</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/10/54/CETATEXT000042105497.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème chambre, 09/07/2020, 431697, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-07-09</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>431697</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>BALAT</AVOCATS>
<RAPPORTEUR>M. Olivier Gariazzo</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Romain Victor</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2020:431697.20200709</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A... B... et Mme C... B... ont demandé au tribunal administratif de Marseille, à titre principal, de prononcer la décharge des cotisations de taxe d'habitation et de contribution à l'audiovisuel public auxquelles ils ont été assujettis dans les rôles de la commune de Marseille (Bouches-du-Rhône) au titre des années 2014 et 2015 et, à titre subsidiaire, la réduction de celles-ci en application du I de l'article 1414 A du code général des impôts. Par un jugement n° 1702086 du 11 avril 2019, ce tribunal a réduit ces cotisations en tant qu'elles excèdent 719 euros en 2014 et 524 euros en 2015 et rejeté le surplus de cette demande.<br/>
<br/>
              Par une ordonnance n° 19MA02641 du 14 juin 2019, enregistrée le même jour au secrétariat du contentieux du Conseil d'Etat, la présidente de la cour administrative d'appel de Marseille a transmis au Conseil d'État, en application de l'article R. 351-2 du code de justice administrative, le pourvoi, enregistré le 12 juin 2019 au greffe de cette cour, formé par le ministre de l'action et des comptes publics contre ce jugement. <br/>
<br/>
              Par ce pourvoi, le ministre de l'action et des comptes publics demande au Conseil d'État :<br/>
<br/>
              1°) d'annuler l'article 1er de ce jugement ;<br/>
<br/>
              2°) réglant l'affaire au fond dans cette mesure, de remettre à la charge de M. et Mme B... les sommes de 495 euros au titre de l'année 2014 et de 620 euros au titre de l'année 2015.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
- le code de justice administrative et l'ordonnance n° 2020-305 du 25 mars 2020 ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Olivier Gariazzo, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Romain Victor, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Balat, avocat de M. et Mme B... ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que M. et Mme B... ont demandé au tribunal administratif de Marseille de prononcer la décharge des cotisations de taxe d'habitation et de contribution à l'audiovisuel public auxquelles ils ont été assujettis dans les rôles de la commune de Marseille (Bouches-du-Rhône) au titre des années 2014 et 2015 ou, à titre subsidiaire, leur réduction en application des dispositions du I de l'article 1414 A du code général des impôts, alors applicables. Par un jugement du 11 avril 2019, le tribunal administratif, faisant droit à leur demande subsidiaire, a prononcé la réduction des cotisations en litige à concurrence d'une somme de 2 358 euros et rejeté le surplus de leurs conclusions. Par une ordonnance du 14 juin 2019, la présidente de la cour administrative d'appel de Marseille a transmis au Conseil d'État, sur le fondement de l'article R. 351-2 du code de justice administrative, le pourvoi formé par le ministre contre l'article 1er de ce jugement.<br/>
<br/>
              2. Aux termes de l'article 1414 A du code général des impôts, dans sa rédaction alors applicable : " I.- Les contribuables autres que ceux mentionnés à l'article 1414, dont le montant des revenus de l'année précédente n'excède pas la limite prévue au II de l'article 1417, sont dégrevés d'office de la taxe d'habitation afférente à leur habitation principale pour la fraction de leur cotisation qui excède 3,44 % de leur revenu au sens du IV de l'article 1417 diminué d'un abattement fixé à : / (...) III.- 1. A compter de 2011, le montant du dégrèvement prévu au I est réduit d'un montant égal au produit de la base nette imposable au profit des communes et de leurs établissements publics de coopération intercommunale par la différence entre le taux global de taxe d'habitation constaté dans la commune au titre de l'année d'imposition et ce même taux global constaté en 2000, multiplié par un coefficient de 1,034. (...). / La majoration prévue au premier alinéa s'applique lorsque : / 1° La différence positive définie au même premier alinéa résulte de l'homogénéisation des abattements appliqués pour le calcul de la taxe d'habitation ; / 2° Le taux communal de taxe d'habitation issu de l'intégration fiscale progressive pour l'année où la création prend fiscalement effet est supérieur au taux moyen pondéré harmonisé de l'ensemble des communes participant à l'opération. Ce taux moyen pondéré harmonisé est égal au rapport entre, d'une part, la somme des produits de taxe d'habitation perçus par les communes participant à l'opération au titre de l'année précédente et, d'autre part, la somme des bases correspondantes après application des abattements harmonisés. / (...) 2. Lorsqu'une commune ou un établissement public de coopération intercommunale au profit desquels l'imposition est établie ont supprimé un ou plusieurs des abattements prévus au II de l'article 1411 et en vigueur en 2003 ou en ont réduit un ou plusieurs taux par rapport à ceux en vigueur en 2003, le montant du dégrèvement calculé dans les conditions prévues au II et au 1 du présent III est réduit d'un montant égal à la différence positive entre, d'une part, le montant du dégrèvement ainsi déterminé et, d'autre part, le montant de celui calculé dans les mêmes conditions en tenant compte de la cotisation déterminée en faisant application des taux d'abattement prévus aux 1, 2 et 3 du II de l'article 1411 et en vigueur en 2003 pour le calcul de la part revenant à la commune ou à l'établissement public de coopération intercommunale.(...) ". <br/>
<br/>
              3. Pour faire droit aux conclusions subsidiaires de M. et Mme B..., le tribunal administratif s'est fondé sur ce qu'ils satisfaisaient aux conditions de revenus prévues par le II de l'article 1417 du code général des impôts et a déterminé le dégrèvement auquel ils avaient droit au titre du I de l'article 1414 A du même code, sans rechercher s'il y avait lieu d'appliquer la réduction de ce dégrèvement à raison du gel des taux et des abattements, prévue par les 1 et 2 du III de cet article. Il a, par suite, commis une erreur de droit.<br/>
<br/>
              4. Le ministre est, dès lors, fondé à demander l'annulation de l'article 1er du jugement qu'il attaque. <br/>
<br/>
              5. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'État, qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : L'article 1er du jugement du tribunal administratif de Marseille du 11 avril 2019 est annulé.<br/>
<br/>
Article 2 : L'affaire est renvoyée, dans cette mesure, au tribunal administratif de Marseille. <br/>
<br/>
Article 3 : Les conclusions présentées par M. et Mme B... au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 4 : La présente décision sera notifiée au ministre de l'économie, des finances et de la relance et à M. et Mme A... et Christiane B....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
