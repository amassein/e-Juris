<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042115597</ID>
<ANCIEN_ID>JG_L_2020_07_000000424647</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/11/55/CETATEXT000042115597.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème - 1ère chambres réunies, 03/07/2020, 424647</TITRE>
<DATE_DEC>2020-07-03</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>424647</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème - 1ère chambres réunies</FORMATION>
<TYPE_REC>Contentieux des pensions</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ZRIBI, TEXIER</AVOCATS>
<RAPPORTEUR>Mme Yaël Treille</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Dieu</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:424647.20200703</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              M. A... B... a demandé au tribunal administratif de Besançon d'annuler la décision du 21 novembre 2016 par laquelle le ministre de l'éducation nationale, de l'enseignement et de la recherche lui a refusé le bénéfice d'une rente viagère d'invalidité, de dire que l'accident dont il a été victime le 15 octobre 2014 est imputable au service, de requalifier son congé de maladie ordinaire en congé de maladie professionnelle et de lui verser les salaires correspondant à la période comprise entre le 15 octobre 2014 et le 5 février 2016, et de condamner l'Etat à lui verser la somme de 23 327 euros en réparation des préjudices qu'il estime avoir subis. Par un jugement n° 1700163 du 5 juin 2018, le tribunal administratif de Besançon a rejeté sa demande.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 2 octobre 2018 et 2 janvier 2019 au secrétariat du contentieux du Conseil d'Etat, M. B... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement en tant qu'il a rejeté ses conclusions tendant à l'annulation de la décision du 21 novembre 2016 et à l'octroi d'une rente viagère d'invalidité ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 000 euros à verser à la SCP Zribi, Texier, son avocat, au titre des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991.<br/>
<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
              Vu : <br/>
              - le code des pensions civiles et militaires de retraite ; <br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - le décret n° 86-442 du 14 mars 1986 ; <br/>
              - le code de justice administrative et l'ordonnance n° 2020-305 du 25 mars 2020 ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Yaël Treille, auditeur,  <br/>
<br/>
              - les conclusions de M. Frédéric Dieu, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Zribi, Texier, avocat de M. B... ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis au juge du fond que M. A... B..., assistant ingénieur à l'université de Franche-Comté, a demandé à faire valoir ses droits à la retraite pour invalidité. A la suite d'un avis de la commission départementale de réforme du 28 avril 2016, qui a constaté l'inaptitude définitive et absolue de l'intéressé à toutes fonctions et retenu que son taux d'invalidité était de 30 %, dont 4% imputable au service par aggravation des troubles préexistants, M. B... a été admis à la retraite pour invalidité à compter du 5 février 2016, par arrêté du 19 août 2016. Sa pension a été liquidée, par un arrêté du 29 août 2016, sur le fondement de l'article L. 29 du code des pensions civiles et militaires de retraite. Par une décision du 21 novembre 2016, le ministre de l'éducation nationale, de l'enseignement supérieur et de la recherche a rejeté la demande de M. B... tendant à l'octroi d'une rente viagère d'invalidité en application de l'article L. 28 du code des pensions civiles et militaires de retraite. M. B... a demandé au tribunal administratif de Besançon d'annuler la décision du 21 novembre 2016, de dire que l'accident dont il a été victime le 15 octobre 2014 est imputable au service, de requalifier son congé de maladie ordinaire en congé de maladie professionnelle, de lui verser les salaires correspondant à la période comprise entre le 15 octobre 2014 et le 5 février 2016, et de condamner l'Etat à lui verser la somme de 23 327 euros en réparation des préjudices qu'il estime avoir subis. M. B... se pourvoit en cassation contre le jugement du 5 juin 2018 par lequel le tribunal administratif de Besançon a rejeté sa demande. Eu égard aux moyens soulevés, son pourvoi doit être regardé comme tendant à l'annulation de ce jugement en tant seulement qu'il a rejeté ses conclusions tendant à l'annulation de la décision du 21 novembre 2016 et à l'octroi d'une rente viagère d'invalidité. <br/>
<br/>
              2. Aux termes de l'article L. 27 du code des pensions civiles et militaires de retraite : " Le fonctionnaire civil qui se trouve dans l'incapacité permanente de continuer ses fonctions en raison d'infirmités résultant de blessures ou de maladie contractées ou aggravées (...) en service (...) peut être radié des cadres par anticipation (...) ". Aux termes de l'article L. 28 du même code : " Le fonctionnaire civil radié des cadres dans les conditions prévues à l'article L. 27 a droit à une rente viagère d'invalidité (...) ". Aux termes de l'article L. 29 du code des pensions civiles et militaires de retraite : " Le fonctionnaire civil qui se trouve dans l'incapacité permanente de continuer ses fonctions en raison d'une invalidité ne résultant pas du service peut être radié des cadres par anticipation soit sur sa demande, soit d'office (...) ". Aux termes de l'article R. 38 du même code : " Le bénéfice de la rente viagère d'invalidité prévue à l'article L. 28 est attribuable si la radiation des cadres ou le décès en activité surviennent avant la limite d'âge et sont imputables à des blessures ou maladies résultant par origine ou aggravation d'un fait précis et déterminé de service ou de l'une des autres circonstances énumérées à l'article L. 27 ". Il résulte de ces dispositions que le droit pour un fonctionnaire de bénéficier de la rente viagère d'invalidité prévue par l'article L. 28 du code des pensions civiles et militaires de retraite est subordonné à la condition que les blessures ou maladies contractées ou aggravées en service aient été de nature à entraîner, à elles seules ou non, la mise à la retraite de l'intéressé. Enfin, aux termes de l'article 13 du décret du 14 mars 1986 relatif à la désignation des médecins agréés, à l'organisation des comités médicaux et des commissions de réforme, aux conditions d'aptitude physique pour l'admission aux emplois publics et au régime de congés de maladie des fonctionnaires : " La commission de réforme est consultée notamment sur : (...) / 6. L'application des dispositions du code des pensions civiles et militaires de retraite ; (...) ".<br/>
<br/>
              3. Si les actes administratifs doivent être pris selon les formes et conformément aux procédures prévues par les lois et règlements, un vice affectant le déroulement d'une procédure administrative préalable, suivie à titre obligatoire ou facultatif, n'est de nature à entacher d'illégalité la décision prise que s'il ressort des pièces du dossier qu'il a été susceptible d'exercer, en l'espèce, une influence sur le sens de la décision prise ou qu'il a privé les intéressés d'une garantie.  <br/>
<br/>
              4. Il ressort des pièces du dossier soumis aux juges du fond et des termes mêmes du jugement attaqué que si, lors de sa séance du 20 avril 2016, la commission de réforme a fixé à 30 % le taux d'invalidité de M. B..., dont 4 % imputable au service, elle ne s'est pas prononcée sur la question de savoir si la part de l'invalidité aggravée en service était de nature, à elle seule ou non, à entraîner la mise à la retraite de l'intéressé, qui a seulement donné lieu à l'ajout par le médecin de la commission de réforme d'une mention manuscrite, postérieurement à la séance du 20 avril 2016. La procédure devant la commission de réforme était ainsi entachée d'irrégularité. En se bornant, pour juger que cette irrégularité n'était pas de nature à entacher d'illégalité la décision refusant l'octroi d'une rente viagère d'invalidité à M. B..., à relever qu'en l'espèce, l'absence d'avis de la commission de réforme sur cette question n'avait pas exercé d'influence sur le sens de la décision du ministre, sans rechercher si elle avait privé M. B... d'une garantie, le tribunal administratif de Besançon a commis une erreur de droit. Par suite, et sans qu'il soit besoin de se prononcer sur l'autre moyen de son pourvoi, son jugement doit être annulé en tant qu'il a statué sur les conclusions de M. B... tendant à l'annulation de la décision du 21 novembre 2016.<br/>
<br/>
              5. Il y a lieu, dans les circonstances de l'espèce, de régler, dans cette mesure, l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative. <br/>
<br/>
              6. Il résulte des dispositions citées au point 2 que la consultation de la commission de réforme constitue une garantie pour les fonctionnaires demandant à bénéficier de la rente viagère d'invalidité prévue par l'article L. 28 du code des pensions civiles et militaires de retraite. Or il résulte de l'instruction que, lors de sa séance du 20 avril 2016, la commission de réforme ne s'est pas prononcée sur la question de savoir si la part de l'invalidité de M. B... aggravée en service était de nature, à elle seule ou non, à entraîner sa mise à la retraite. L'omission de cette consultation, qui a privé M. B... d'une garantie, a constitué une irrégularité de nature à entacher d'illégalité la décision attaquée. Par suite, M. B... est fondé, sans qu'il soit besoin de se prononcer sur les autres moyens de sa demande, à demander l'annulation de la décision du 21 novembre 2016.<br/>
<br/>
              7. Dans les circonstances de l'espèce, il y a lieu d'enjoindre à la ministre de l'enseignement supérieur, de la recherche et de l'innovation de statuer à nouveau, sur la demande de M. B... tendant à l'octroi d'une rente viagère d'invalidité, après consultation de la commission de réforme sur la question de savoir si la part de l'invalidité aggravée en service était de nature, à elle seule ou non, à entraîner la mise à la retraite de l'intéressé. Dans l'affirmative, et alors même que l'invalidité aggravée en service ne serait pas l'unique cause de mise à la retraite, il appartiendra à la ministre, conformément à ce qui est dit au point 2, de proposer au ministre chargé des finances l'octroi d'une rente viagère d'invalidité à M. B..., dans un délai de quatre mois à compter de la notification de la présente décision. <br/>
<br/>
              8. M. B... a obtenu le bénéfice de l'aide juridictionnelle. Par suite, son avocat peut se prévaloir des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991. Il y a lieu, dans les circonstances de l'espèce et sous réserve que la SCP Zribi, Texier, avocat de M. B..., renonce à percevoir la somme correspondant à la part contributive de l'Etat, de mettre à la charge de l'Etat la somme de 3 000 euros à verser à cette société au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du tribunal administratif de Besançon du 5 juin 2018 est annulé en tant qu'il a statué sur les conclusions de M. B... tendant à l'annulation de la décision du 21 novembre 2016.<br/>
Article 2 : La décision du ministre de l'éducation nationale, de l'enseignement supérieur et de la recherche du 21 novembre 2016 est annulée.<br/>
Article 3 : Il est enjoint à la ministre de l'enseignement supérieur, de la recherche et de l'innovation de procéder au réexamen de la demande de M. B..., dans le délai de quatre mois à compter de la présente décision. <br/>
Article 4 : L'Etat versera à la SCP Zribi, Texier une somme de 3 000 euros au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991, sous réserve que cette société renonce à percevoir la somme correspondant à la part contributive de l'Etat.<br/>
Article 5  : La présente décision sera notifiée à M. A... B... et à la ministre de l'enseignement supérieur, de la recherche et de l'innovation.<br/>
Copie en sera adressée à la section du rapport et des études du Conseil d'Etat.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">17-05-012 COMPÉTENCE. COMPÉTENCE À L'INTÉRIEUR DE LA JURIDICTION ADMINISTRATIVE. COMPÉTENCE EN PREMIER ET DERNIER RESSORT DES TRIBUNAUX ADMINISTRATIFS. - LITIGES EN MATIÈRE DE PENSIONS DE RETRAITE DES AGENTS PUBLICS (7° DE L'ART. R. 811-1 DU CJA) - INCLUSION - LITIGES RELATIFS À L'OCTROI D'UNE RENTE VIAGÈRE D'INVALIDITÉ (ART. L. 28 DU CPCMR).
</SCT>
<ANA ID="9A"> 17-05-012 La demande d'un fonctionnaire tendant à l'annulation de la décision lui refusant, sur le fondement de l'article L. 28 du code des pensions civiles et militaires de retraite (CPCMR), l'octroi d'une rente viagère d'invalidité relève des litiges en matière de pensions au sens du 7° de l'article R. 811-1 du code de justice administrative (CJA), sur lesquels le tribunal administratif statue en premier et dernier ressort (sol. impl.).</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
