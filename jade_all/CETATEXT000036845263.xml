<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036845263</ID>
<ANCIEN_ID>JG_L_2018_04_000000414316</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/84/52/CETATEXT000036845263.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère chambre, 26/04/2018, 414316, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-04-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>414316</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Cécile Chaduteau-Monplaisir</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Rémi Decout-Paolini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:414316.20180426</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              Par une ordonnance n° 1705848 du 14 septembre 2017, enregistrée le même jour au secrétariat du contentieux du Conseil d'Etat, la présidente du tribunal administratif de Paris a transmis au Conseil d'Etat, en application de l'article R. 351-2 du code de justice administrative, la requête, enregistrée le 3 avril 2017 au greffe de ce tribunal, présentée par la société Socopi. Par cette requête, par un mémoire complémentaire, enregistré le 25 août 2017 au greffe de ce tribunal, et par un nouveau mémoire, enregistré le 14 décembre 2017 au secrétariat du contentieux du Conseil d'Etat, la société Socopi demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir l'arrêté du ministre des affaires sociales et de la santé et du secrétaire d'Etat chargé du budget et des comptes publics du 1er février 2017 modifiant l'arrêté du 24 juin 2016 portant homologation des prix de vente au détail des tabacs manufacturés en France, à l'exclusion des départements d'outre-mer ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code général des impôts ;<br/>
              - le code de la santé publique ;<br/>
              - la décision du Conseil d'Etat statuant au contentieux nos 401536, 401561, 401611, 401632 et 401668 du 10 mai 2017 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Cécile Chaduteau-Monplaisir, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Rémi Decout-Paolini, rapporteur public.<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier que la société Socopi, fournisseur agréé, a demandé l'homologation du prix de produits du tabac commercialisés sous la marque " Starbuzz " au cours du mois de décembre 2016. Eu égard aux moyens qu'elle soulève, elle doit être regardée comme demandant l'annulation pour excès de pouvoir de l'arrêté conjoint du ministre des affaires sociales et de la santé et du secrétaire d'Etat chargé du budget et des comptes publics du 1er février 2017, qui détermine la nomenclature des prix de vente au détail des tabacs manufacturés en France, en tant qu'il refuse implicitement d'homologuer le prix des produits commercialisés sous cette marque.<br/>
<br/>
              Sur la compétence du Conseil d'Etat :<br/>
<br/>
              2. En vertu du 2° de l'article R. 311-1 du code de justice administrative, le Conseil d'Etat est compétent pour connaître en premier et dernier ressort des recours dirigés contre les actes réglementaires des ministres. L'arrêté par lequel les ministres chargés de la santé et du budget homologuent le prix de détail de produits du tabac présente, en raison des effets qui s'y attachent pour l'ensemble des consommateurs, un caractère réglementaire. Ainsi, l'arrêté du ministre des affaires sociales et de la santé et du secrétaire d'Etat chargé du budget et des comptes publics du 1er février 2017 homologuant les prix de vente au détail des tabacs manufacturés, sans retenir les produits commercialisés sous la marque " Starbuzz " dont la société Socopi avait demandé que les prix soit homologués, constitue une décision de refus d'homologation qui revêt, de même, un caractère réglementaire. Dès lors, et contrairement à ce que soutient la société Socopi, le Conseil d'Etat est compétent pour connaître en premier et dernier ressort de sa requête.<br/>
<br/>
              Sur la fin de non-recevoir opposée par le ministre de l'action et des comptes publics :<br/>
<br/>
              3. La circonstance que la requête ait été présentée devant le tribunal administratif de Paris, alors que cette juridiction n'avait pas compétence pour en connaître, est sans incidence sur sa recevabilité.<br/>
<br/>
              Sur la légalité de l'arrêté attaqué :<br/>
<br/>
              4. En raison des effets qui s'y attachent, l'annulation pour excès de pouvoir d'un acte administratif, qu'il soit ou non réglementaire, emporte, lorsque le juge est saisi de conclusions recevables, l'annulation par voie de conséquence des décisions administratives consécutives qui n'auraient pu légalement être prises en l'absence de l'acte annulé ou qui sont en l'espèce intervenues en raison de l'acte annulé. Il en va ainsi, notamment, des décisions qui ont été prises en application de l'acte annulé et de celles dont l'acte annulé constitue la base légale. Il incombe au juge de l'excès de pouvoir, lorsqu'il est saisi de conclusions recevables dirigées contre de telles décisions consécutives, de prononcer leur annulation par voie de conséquence, le cas échéant en relevant d'office un tel moyen qui découle de l'autorité absolue de chose jugée qui s'attache à l'annulation du premier acte.<br/>
<br/>
              5. Aux termes du premier alinéa de l'article 572 du code général des impôts, dans sa rédaction issue de la loi du 26 janvier 2016 de modernisation de notre système de santé : " Le prix de détail de chaque produit exprimé aux 1 000 unités ou aux 1 000 grammes, est unique pour l'ensemble du territoire et librement déterminé par les fabricants et les fournisseurs agréés. Il est applicable après avoir été homologué par arrêté conjoint des ministres chargés de la santé et du budget, dans des conditions définies par décret en Conseil d'Etat. Il ne peut toutefois être homologué s'il est inférieur à la somme du prix de revient et de l'ensemble des taxes ". Si l'article 2 de l'ordonnance du 19 mai 2016 portant transposition de la directive 2014/40/UE sur la fabrication, la présentation et la vente des produits du tabac et des produits connexes a complété cet alinéa pour prévoir que : " Cet arrêté mentionne la marque et la dénomination commerciale des produits du tabac à la condition que ces dernières respectent les dispositions de l'article L. 3512-21 du code de la santé publique ", cet article a été annulé par une décision du Conseil d'Etat, statuant au contentieux, du 10 mai 2017.<br/>
<br/>
              6. Il ressort des pièces du dossier, notamment de la lettre adressée le 20 décembre 2016 par le directeur général de la santé à la société Socopi ainsi que de son annexe, que le refus opposé à la demande d'homologation de prix présentée en décembre 2016 par cette société est motivé par la circonstance que la marque commerciale " Starbuzz " des produits considérés contribue à la promotion d'un produit du tabac en suggérant que ces derniers ont des effets bénéfiques sur le mode de vie en ce qui concerne le statut social, en méconnaissance de l'article L. 3512-21 du code de la santé publique et de l'article R. 3512-30 du même code pris pour son application. Par suite, la décision attaquée de refus implicite d'homologation de prix, qui trouvait sa base légale dans les dispositions ajoutées à l'article 572 du code général des impôts par l'article 2 de l'ordonnance du 19 mai 2016, doit être annulée par voie conséquence de l'annulation de cet article 2. <br/>
<br/>
              7. Ce moyen suffisant à entraîner cette annulation, il n'est pas nécessaire d'examiner les autres moyens de la requête.<br/>
<br/>
              Sur les frais liés au litige :<br/>
<br/>
              8. Il y a lieu, dans les circonstances de l'espère, de mettre à la charge de l'Etat le versement à la société Socopi d'une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
                            D E C I D E :<br/>
                            --------------<br/>
<br/>
Article 1er : L'arrêté du 1er février 2017 modifiant l'arrêté du 24 juin 2016 portant homologation des prix de vente au détail des tabacs manufacturés en France, à l'exclusion des départements d'outre-mer, est annulé en tant qu'il refuse implicitement d'homologuer le prix des produits commercialisés sous la marque " Starbuzz ", dont la société Socopi avait demandé l'homologation de prix en décembre 2016.<br/>
Article 2 : L'Etat versera à la société Socopi une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée à la société Socopi, à la ministre des solidarités et de la santé et au ministre de l'action et des comptes publics.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
