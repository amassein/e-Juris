<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038438704</ID>
<ANCIEN_ID>JG_L_2019_04_000000420870</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/43/87/CETATEXT000038438704.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère chambre, 30/04/2019, 420870, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-04-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>420870</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP RICARD, BENDEL-VASSEUR, GHNASSIA</AVOCATS>
<RAPPORTEUR>M. Arnaud Skzryerbak</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Rémi Decout-Paolini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2019:420870.20190430</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés les 23 mai et 10 septembre 2018 au secrétariat du contentieux du Conseil d'Etat, la société Camaïeu International demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision implicite par laquelle la ministre des solidarités et de la santé a rejeté sa demande d'abrogation du sixième alinéa du I de l'article R. 242-1 du code de la sécurité sociale, formée par courrier du 7 février 2018 ; <br/>
<br/>
              2°) de saisir la Cour européenne des droits de l'homme, sur le fondement de l'article 1er du protocole n° 16 à la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, d'une demande d'avis portant sur l'atteinte susceptible d'être portée au droit à un procès équitable garanti par l'article 6, paragraphe 1, de cette convention par une interprétation jurisprudentielle ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.  <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ; <br/>
<br/>
              Vu :<br/>
              - la Constitution ;<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales et son protocole n° 16 ; <br/>
              - le code de la sécurité sociale ; <br/>
              -  le code du travail ;<br/>
              - la décision du 18 juillet 2018 par laquelle le Conseil d'Etat statuant au contentieux n'a pas renvoyé au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée par la société Camaïeu International ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Arnaud Skzryerbak, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Rémi Decout-Paolini, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Ricard, Bendel-Vasseur, Ghnassia, avocat de la société Camaïeu International ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Le premier alinéa de l'article L. 242-1 du code de la sécurité sociale dispose, dans sa rédaction applicable à la date de la décision attaquée, que : " Pour le calcul des cotisations de sécurité sociale dues pour les périodes au titre desquelles les revenus d'activité sont attribués, sont considérées comme rémunérations toutes les sommes versées aux travailleurs en contrepartie ou à l'occasion du travail (...) ". Le sixième alinéa du I de l'article R. 242-1 du même code prévoit que : " Le montant des rémunérations à prendre pour base de calcul des cotisations (...) ne peut être inférieur, en aucun cas, au montant cumulé, d'une part, du salaire minimum de croissance applicable aux travailleurs intéressés fixé en exécution de la loi n° 70-7 du 2 janvier 1970 et des textes pris pour son application et, d'autre part, des indemnités, primes et majorations s'ajoutant audit salaire minimum en vertu d'une disposition législative ou d'une disposition réglementaire ". La société requérante demande l'annulation pour excès de pouvoir de la décision par laquelle la ministre des solidarités et de la santé a rejeté sa demande tendant à l'abrogation de ces dispositions réglementaires. <br/>
<br/>
              2. D'une part, par les dispositions alors applicables du premier alinéa de l'article L. 242-1 du code de la sécurité sociale, le législateur n'a pas entendu limiter l'assiette des cotisations de sécurité sociale aux rémunérations qui auraient été effectivement versées mais prévoir la prise en considération de tous les éléments qui sont la contrepartie d'un travail ou sont versés à cette occasion. Par suite, en ce qu'elles fixent le montant minimum des rémunérations à prendre pour base du calcul des cotisations de sécurité sociale au niveau du minimum dû aux travailleurs intéressés en vertu de dispositions législatives ou réglementaires, les dispositions contestées du sixième alinéa du I de l'article R. 242-1 du code de la sécurité sociale, qui n'empiètent pas sur la compétence réservée au législateur par l'article 34 de la Constitution, ne méconnaissent pas les dispositions de l'article L. 242-1 de ce code. Elles ne sauraient davantage être regardées comme méconnaissant les articles L. 242-1-1 et L. 242-1-2 du même code qui régissent le calcul des cotisations sociales dues à la suite du constat des infractions mentionnées aux 1° à 4° de l'article L. 8211-1 du code du travail, notamment sur les rémunérations afférentes à un travail dissimulé, en ce qu'elles ne limitent pas l'application de ce montant minimum à l'existence d'une situation de travail dissimulé.<br/>
<br/>
              3. D'autre part, en ce qu'elles ne permettent pas à l'employeur qui n'a pas versé au salarié les rémunérations qu'il lui doit de s'exonérer aussi, pour ce motif et tant que le salarié n'aurait pas fait valoir sa créance, des cotisations de sécurité sociale afférentes, dues à l'organisme chargé du recouvrement des cotisations de sécurité sociale et d'allocations familiales, les dispositions contestées contribuent à préserver l'égalité entre les employeurs, qu'ils aient ou non effectivement versé les rémunérations dues à leurs salariés. Elles ne peuvent ainsi, en tout état de cause, être regardées comme créant une différence de traitement entre employeurs. Par suite, le moyen tiré de la méconnaissance du principe d'égalité devant les charges publiques, qui découle de l'article 13 de la Déclaration des droits de l'homme et du citoyen, ne peut qu'être écarté. <br/>
<br/>
              4. Il résulte de tout ce qui précède, sans qu'il y ait lieu en l'espèce d'adresser une demande d'avis consultatif à la Cour européenne des droits de l'homme sur le fondement du protocole n° 16 à la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, que la société requérante n'est pas fondée à demander l'annulation de la décision par laquelle la ministre des solidarités et de la santé a rejeté sa demande d'abrogation des dispositions du sixième alinéa du I de l'article R. 242-1 du code de la sécurité sociale.<br/>
<br/>
              5. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise, à ce titre, à la charge de l'Etat, qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de la société Camaïeu International est rejetée. <br/>
Article 2 : La présente décision sera notifiée à la société Camaïeu International et à la ministre des solidarités et de la santé.<br/>
Copie en sera adressée au Premier ministre.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
