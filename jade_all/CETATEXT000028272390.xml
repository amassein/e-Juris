<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028272390</ID>
<ANCIEN_ID>JG_L_2013_12_000000360445</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/27/23/CETATEXT000028272390.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  5ème sous-section jugeant seule, 02/12/2013, 360445, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-12-02</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>360445</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 5ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DELVOLVE, DELVOLVE</AVOCATS>
<RAPPORTEUR>Mme Marie Gautier-Melleray</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Fabienne Lambolez</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2013:360445.20131202</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 25 juin et 20 septembre 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour Mme A...B..., demeurant... ; Mme B...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 11NC01280 du 27 février 2012 par lequel la cour administrative d'appel de Nancy a, sur appel du préfet du Haut-Rhin, annulé le jugement n° 1102346 du 6 juillet 2011 du tribunal administratif de Strasbourg et rejeté sa demande tendant à l'annulation de l'arrêté du préfet du Haut-Rhin du 30 mars 2011 refusant de lui délivrer un titre de séjour et lui faisant obligation de quitter le territoire français ; <br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel du préfet du Haut-Rhin ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la convention européenne de sauvegarde des droits de l'homme et de libertés fondamentales ; <br/>
<br/>
              Vu le code de l'entrée et du séjour des étrangers et du droit d'asile ; <br/>
<br/>
              Vu la loi n° 91-647 du 10 juillet 1991 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Marie Gautier-Melleray, Maître des Requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Fabienne Lambolez, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Delvolvé, Delvolvé, avocat de MmeB... ;<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que Mme A...B..., ressortissante marocaine, a demandé la délivrance d'un titre de séjour portant la mention " vie privée et familiale ", qui lui a été refusée par un arrêté du préfet du Haut-Rhin du 30 mars 2011 assorti d'une obligation de quitter le territoire français ; que, par un jugement du 6 juillet 2011, le tribunal administratif de Strasbourg a annulé cet arrêté et enjoint au préfet du Haut-Rhin de délivrer le titre sollicité ; que Mme B...se pourvoit en cassation contre l'arrêt du 27 février 2012 par lequel la cour administrative d'appel de Nancy, sur appel du préfet du Haut-Rhin, a annulé le jugement du 6 juillet 2011 et rejeté sa demande tendant à l'annulation de l'arrêté du 30 mars 2011 ; <br/>
<br/>
              2. Considérant qu'aux termes de l'article 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentale : " 1. Toute personne a droit au respect de sa vie privée et familiale, de son domicile et de sa correspondance./ 2. Il ne peut y avoir ingérence d'une autorité publique dans l'exercice de ce droit que pour autant que cette ingérence est prévue par la loi et qu'elle constitue une mesure qui, dans une société démocratique, est nécessaire à la sécurité nationale, à la sûreté publique, au bien-être économique du pays, à la défense de l'ordre et à la prévention des infractions pénales, à la protection de la santé ou de la morale, ou à la protection des droits et libertés d'autrui " ; qu'aux termes de l'article L. 313-11 du code de l'entrée et du séjour des étrangers et du droit d'asile : "  (...) la carte de séjour temporaire portant la mention " vie privée et familiale " est délivrée de plein droit : " (...) 7° A l'étranger ne vivant pas en état de polygamie (...),  dont les liens personnels et familiaux en France, appréciés notamment au regard de leur intensité, de leur ancienneté et de leur stabilité, des conditions d'existence de l'intéressé, de son insertion dans la société française ainsi que de la nature de ses liens avec la famille restée dans le pays d'origine, sont tels que le refus d'autoriser son séjour porterait à son droit au respect de sa vie privée et familiale une atteinte disproportionnée au regard des motifs du refus (...) " ;<br/>
<br/>
              3. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que Mme A...B..., née en France le 30  mai 1980, y a vécu avant de retourner au Maroc en 1984 avec sa mère après le divorce de ses parents ; qu'en 1987, elle est revenue vivre en France avec son père après que sa mère eut renoncé à son droit de garde ; qu'à la suite du nouveau mariage de son père, elle a vécu au Maroc avec ses grands-parents paternels de 1990 jusqu'à leur décès ; qu'elle est revenue en France avec sa soeur en 2004, à l'âge de 24 ans, pour y rejoindre son père, titulaire d'une carte de résident, et ses deux frères, de nationalité française ; qu'elle ne possède plus d'attache effective au Maroc, ses relations avec sa mère qui continue d'y résider ayant pris fin depuis de nombreuses années ; qu'en estimant, dans ces conditions, que le refus de séjour litigieux ne portait pas une atteinte disproportionnée au droit de l'intéressée au respect de sa vie familiale, la cour a inexactement qualifié les faits de l'espèce ; que, par suite, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, Mme B...est fondée à demander l'annulation de l'arrêt attaqué ;  <br/>
<br/>
              4. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative ; <br/>
<br/>
              5. Considérant qu'ainsi qu'il a été dit ci-dessus, le refus de titre de séjour contesté porte au droit de Mme B...au respect de sa vie privée et familiale une atteinte disproportionnée au regard des motifs de ce refus ; que, par suite, le préfet du Haut-Rhin n'est pas fondé à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Strasbourg a annulé son arrêté du 30 mars 2011 et lui a enjoint de délivrer un titre de séjour à MmeB... ; <br/>
<br/>
              6. Considérant que Mme B...a obtenu l'aide juridictionnelle ; que, par suite, son avocat peut se prévaloir des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 ; qu'il y a lieu, dans les circonstances de l'espèce, et sous réserve que la SCP Guillaume et Antoine Delvolvé, avocat de MmeB..., renonce à percevoir la somme correspondant à la part contributive de l'Etat, de mettre à la charge de l'Etat le versement à cette société de la somme de 2 000 euros ;  <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Nancy du 27 février 2012 est annulé.<br/>
<br/>
Article 2 : L'appel du préfet du Haut-Rhin devant le tribunal administratif de Strasbourg est rejeté.<br/>
<br/>
Article 3 : L'Etat versera à la SCP Guillaume et Antoine Delvolvé, avocat de MmeB..., une somme de 2 000 euros en application des dispositions de l'article L. 761-1 du code de justice administrative et du deuxième alinéa de l'article 37 de la loi du 10 juillet 1991, sous réserve que cette société renonce à percevoir la somme correspondant à la part contributive de l'Etat. <br/>
<br/>
Article 4 : La présente décision sera notifiée à Mme A...B...et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
