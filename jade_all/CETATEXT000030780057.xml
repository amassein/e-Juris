<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030780057</ID>
<ANCIEN_ID>JG_L_2015_06_000000385755</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/78/00/CETATEXT000030780057.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème / 10ème SSR, 22/06/2015, 385755</TITRE>
<DATE_DEC>2015-06-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>385755</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème / 10ème SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Maïlys Lange</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Marie-Astrid Nicolazo de Barmon</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:385755.20150622</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A...D...a contesté, devant le tribunal administratif de Lille, les opérations électorales qui se sont déroulées le 30 mars 2014 dans la commune de Wasquehal. Par un jugement n° 1402184 du 15 octobre 2014, le tribunal a rejeté sa protestation.<br/>
<br/>
              Par une requête enregistrée le 17 novembre 2014 au secrétariat du contentieux du Conseil d'Etat, M. D...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) d'annuler ces opérations électorales ;<br/>
<br/>
              3°) de faire application des dispositions de l'article L. 118-1 du code électoral.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code électoral ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Maïlys Lange, maître des requêtes,<br/>
<br/>
              - les conclusions de Mme Marie-Astrid Nicolazo de Barmon, rapporteur public ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'à l'issue du scrutin organisé le 30 mars 2014 en vue de la désignation des conseillers municipaux et communautaires de la commune de Wasquehal (Nord), qui compte 20 216 habitants, la liste conduite par Mme B...a obtenu 3 764 voix et 24 sièges au conseil municipal, celle conduite par Mme E...2 151 voix et 4 sièges, celle conduite par M. G...2 127 voix et 4 sièges et celle conduite par M. D...883 voix et un siège ; <br/>
<br/>
              2. Considérant qu'aux termes de l'article R. 411-1 du code de justice administrative : " La juridiction est saisie par requête. La requête (...) contient l'exposé des faits et moyens, ainsi que l'énoncé des conclusions soumises au juge. " ; que la requête d'appel présentée par M. D... satisfait à ces exigences ; que la fin de non-recevoir soulevée par Mme B... et autres doit, dès lors, être écartée ; <br/>
<br/>
              3. Considérant qu'en vertu des dispositions du premier alinéa de l'article LO 247-1 du code électoral, applicables aux communes qui comptent plus de 1 000 habitants, " les bulletins de vote imprimés distribués aux électeurs comportent, à peine de nullité, en regard du nom des candidats ressortissants d'un Etat membre de l'Union européenne autre que la France, l'indication de leur nationalité " ; qu'il résulte des termes mêmes de cet article que l'omission sur les bulletins de vote de l'indication de la nationalité des candidats ressortissants d'un Etat membre de l'Union européenne autre que la France entache, à elle seule, ces bulletins de nullité ; <br/>
<br/>
              4. Considérant qu'il résulte de l'instruction que les bulletins de vote de la liste " Passion Wasquehal Autrement " conduite par Mme E...ne mentionnaient pas la nationalité belge de la candidate inscrite en vingt-troisième position sur cette liste ; qu'en dépit de la nullité dont les bulletins étaient entachés, les 2 151 suffrages qui se sont portés sur la liste conduite par Mme E...ont été pris en compte dans le dépouillement et ont conduit à ce que cette liste obtienne quatre sièges au conseil municipal ; que l'irrégularité résultant de la prise en compte de ces bulletins, qui auraient dû être tenus pour nuls, a été ainsi de nature à altérer la sincérité du scrutin ; <br/>
<br/>
              5. Considérant que, saisi par la protestation que lui avait soumise M.D..., qui faisait valoir que les bulletins de la liste conduite par Mme E...auraient dû être tenus pour nuls et qui lui demandait de tirer les conséquences de cette nullité en " révisant " le résultat des élections, le tribunal administratif de Lille était nécessairement, et à tout le moins, saisi de conclusions tendant à l'annulation de l'élection des quatre candidats de la liste de Mme E...proclamés élus en qualité de conseiller municipal ; qu'il appartenait dans un tel cas au tribunal administratif, dès lors qu'il jugeait que l'irrégularité commise entraînait une incertitude pour le décompte des voix obtenues par les listes en présence, compte tenu du nombre d'électeurs qui n'avaient pas été en mesure d'exprimer valablement leur suffrage pour la liste conduite par Mme E..., de prononcer, eu égard au mode de scrutin applicable dans les communes de plus de 1 000 habitants, l'annulation de l'ensemble des opérations électorales, quand bien même une telle annulation n'aurait pas été demandée par le protestataire ; qu'il suit de là que les conclusions de M.D..., présentées devant le Conseil d'Etat, tendant à l'annulation des opérations électorales qui se sont déroulées dans la commune de Wasquehal le 30 mars 2014, ne sauraient être regardées comme nouvelles en appel et qu'en rejetant la protestation de M. D... au motif qu'il n'était saisi que de conclusions tendant à la rectification des résultats, le tribunal administratif a méconnu son office de juge de l'élection ; qu'il y a lieu, dès lors, sans qu'il soit besoin d'examiner les moyens de la requête, d'annuler son jugement et, compte tenu de l'expiration du délai de trois mois qui lui était imparti par l'article R. 120 du code électoral pour se prononcer sur la protestation présentée par M.D..., de statuer immédiatement sur cette protestation ; <br/>
<br/>
              6. Considérant qu'aux termes de l'article R. 119 du code électoral : " Les réclamations contre les opérations électorales doivent être consignées au procès-verbal, sinon déposées, à peine d'irrecevabilité, au plus tard à dix-huit heures le cinquième jour qui suit l'élection, à la sous-préfecture ou à la préfecture (...) / Les protestations peuvent également être déposées directement au greffe du tribunal administratif dans le même délai. " ; qu'il résulte de l'instruction que la protestation de M. D...a été transmise au tribunal administratif de Lille par un courrier électronique reçu le 4 avril 2014, dans le délai de cinq jours prévu par les dispositions précitées, et que le requérant a ultérieurement confirmé être l'auteur de cette protestation par une lettre enregistrée au greffe du tribunal administratif le 10 avril 2014 ; que la fin de non-recevoir tirée du caractère tardif de sa protestation doit, par suite, être écartée ; <br/>
<br/>
              7. Considérant qu'ainsi qu'il a été dit au point 4, l'irrégularité résultant de la prise en compte des 2 151 bulletins entachés de nullité en vertu de l'article LO 247-1 du code électoral a été de nature à altérer la sincérité du scrutin ; qu'eu égard au mode de scrutin applicable dans les communes de plus de 1 000 habitants, il y a lieu de prononcer l'annulation de l'ensemble des opérations électorales qui se sont déroulées les 23 et 30 mars 2014 dans la commune de Wasquehal ; <br/>
<br/>
              8. Considérant qu'aux termes de l'article L. 118-1 du code électoral : " La juridiction administrative, en prononçant l'annulation d'une élection pour fraude, peut décider que la présidence d'un ou plusieurs bureaux de vote sera assurée par une personne désignée par le président du tribunal de grande instance lors de l'élection partielle consécutive à cette annulation " ; que le juge pouvant mettre en oeuvre d'office le pouvoir qu'il tire de ces dispositions lorsqu'il prononce l'annulation d'une élection pour fraude, les conclusions tendant à l'application de l'article L. 118-1 du code électoral sont recevables, même quand elles sont présentées pour la première fois en appel ; que toutefois, si l'irrégularité constatée est, dans les circonstances de l'espèce, de nature à entraîner l'annulation des opérations électorales contestées, elle ne résulte d'aucune manoeuvre frauduleuse ; que, par suite, il n'y a pas lieu de décider que la présidence d'un ou plusieurs bureaux de vote sera assurée par une personne désignée par le président du tribunal de grande instance lors de l'élection consécutive à la présente décision ;  <br/>
<br/>
              9. Considérant, enfin, que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de M. D..., qui n'est pas, dans la présente instance, la partie perdante ; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              D E C I D E :<br/>
              --------------<br/>
Article 1er : Le jugement du tribunal administratif de Lille du 15 octobre 2014 est annulé.<br/>
Article 2 : Les opérations électorales qui se sont déroulées les 23 et 30 mars 2014 pour la désignation des conseillers municipaux dans la commune de Wasquehal sont annulées. <br/>
Article 3 : Le surplus des conclusions de la requête de M. D...et les conclusions présentées par Mme B...et autres au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetés. <br/>
Article 4 : La présente décision sera notifiée à M. A...D..., à Mme H...B..., à Mme F...E..., à M. C...G...et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">28-08-05 ÉLECTIONS ET RÉFÉRENDUM. RÈGLES DE PROCÉDURE CONTENTIEUSE SPÉCIALES. POUVOIRS DU JUGE. - CAS OÙ LE JUGE DE PREMIÈRE INSTANCE N'A PAS ANNULÉ L'ENSEMBLE DES OPÉRATIONS ÉLECTORALES ALORS QU'IL AURAIT DÛ - IRRÉGULARITÉ DU JUGEMENT.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">28-08-05-04-03 ÉLECTIONS ET RÉFÉRENDUM. RÈGLES DE PROCÉDURE CONTENTIEUSE SPÉCIALES. POUVOIRS DU JUGE. ANNULATION D'UNE ÉLECTION. POUVOIRS SPÉCIAUX DU JUGE ÉLECTORAL. - POUVOIR DE DÉSIGNATION DU PRÉSIDENT DU BUREAU DE VOTE EN CAS D'ANNULATION POUR FRAUDE (ART. L. 118-1 DU CODE ÉLECTORAL) - POUVOIR POUVANT ÊTRE MIS EN &#140;UVRE D'OFFICE PAR LE JUGE - CONSÉQUENCE - CONCLUSIONS TENDANT À LA MISE EN &#140;UVRE DE CE POUVOIR RECEVABLES, MÊME POUR LA PREMIÈRE FOIS, EN APPEL.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">54-08-01-02-01 PROCÉDURE. VOIES DE RECOURS. APPEL. CONCLUSIONS RECEVABLES EN APPEL. CONCLUSIONS NOUVELLES. - CONTENTIEUX ÉLECTORAL - POUVOIR DE DÉSIGNATION DU PRÉSIDENT DU BUREAU DE VOTE EN CAS D'ANNULATION POUR FRAUDE (ART. L. 118-1 DU CODE ÉLECTORAL) - POUVOIR POUVANT ÊTRE MIS EN &#140;UVRE D'OFFICE PAR LE JUGE - CONSÉQUENCE - CONCLUSIONS TENDANT À LA MISE EN &#140;UVRE DE CE POUVOIR RECEVABLES, MÊME POUR LA PREMIÈRE FOIS, EN APPEL.
</SCT>
<SCT ID="8D" TYPE="PRINCIPAL">54-08-01-04-02 PROCÉDURE. VOIES DE RECOURS. APPEL. EFFET DÉVOLUTIF ET ÉVOCATION. ÉVOCATION. - CONTENTIEUX ÉLECTORAL - CAS OÙ LE JUGE DE PREMIÈRE INSTANCE, ALORS QU'IL L'AURAIT DÛ, N'A PAS ANNULÉ L'ENSEMBLE DES OPÉRATIONS ÉLECTORALES - IRRÉGULARITÉ DU JUGEMENT.
</SCT>
<ANA ID="9A"> 28-08-05 Le juge de première instance, alors qu'il aurait dû, n'a pas annulé l'ensemble des opérations électorales, méconnaissant ainsi son office. En pareil cas, le juge d'appel annule le jugement de première instance pour irrégularité et statue directement sur la protestation.</ANA>
<ANA ID="9B"> 28-08-05-04-03 L'article L. 118-1 du code électoral permet au juge administratif, lorsqu'il annule une élection pour fraude, de décider que la présidence d'un ou plusieurs bureaux de vote sera assurée par une personne désignée par le président du tribunal de grande instance lors de l'élection partielle consécutive à cette annulation. Le juge pouvant mettre en oeuvre d'office le pouvoir qu'il tire de ces dispositions lorsqu'il prononce l'annulation d'une élection pour fraude, les conclusions tendant à l'application de l'article L. 118-1 du code électoral sont recevables, même quand elles sont présentées pour la première fois en appel.</ANA>
<ANA ID="9C"> 54-08-01-02-01 L'article L. 118-1 du code électoral permet au juge administratif, lorsqu'il annule une élection pour fraude, de décider que la présidence d'un ou plusieurs bureaux de vote sera assurée par une personne désignée par le président du tribunal de grande instance lors de l'élection partielle consécutive à cette annulation. Le juge pouvant mettre en oeuvre d'office le pouvoir qu'il tire de ces dispositions lorsqu'il prononce l'annulation d'une élection pour fraude, les conclusions tendant à l'application de l'article L. 118-1 du code électoral sont recevables, même quand elles sont présentées pour la première fois en appel.</ANA>
<ANA ID="9D"> 54-08-01-04-02 Le juge de première instance, alors qu'il l'aurait dû, n'a pas annulé l'ensemble des opérations électorales, méconnaissant ainsi son office. En pareil cas, le juge d'appel annule le jugement de première instance pour irrégularité et statue directement sur la protestation.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
