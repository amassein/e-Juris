<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034478514</ID>
<ANCIEN_ID>JG_L_2017_04_000000409592</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/47/85/CETATEXT000034478514.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 11/04/2017, 409592, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-04-11</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>409592</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2017:409592.20170411</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B...A...a demandé au juge des référés du tribunal administratif de Paris, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, d'enjoindre au préfet de police de procéder à l'enregistrement de sa demande d'asile et de lui remettre une attestation de demande d'asile ainsi que le formulaire du demandeur d'asile dans un délai de trois jours à compter de la notification de l'ordonnance, sous astreinte de 150 euros par jour de retard. Par une ordonnance n° 1704518/9 du 24 mars 2017, le juge des référés a rejeté sa demande. <br/>
<br/>
              Par une requête, enregistrée 6 avril 2017 au secrétariat du contentieux du Conseil d'Etat, M. A...demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative :  <br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) de faire droit à sa demande ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat le versement d'une somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Il soutient que :<br/>
              - la condition d'urgence est remplie dès lors, d'une part, que le refus du préfet d'enregistrer sa demande d'asile l'empêche de présenter une demande d'asile auprès de l'Office français de protection des réfugiés et apatrides et, ainsi, de bénéficier des conditions d'accueil des demandeurs d'asile et, d'autre part, que le préfet s'apprête à procéder à sa remise aux autorités bulgares ; <br/>
              - le refus du préfet d'enregistrer sa demande et de renouveler son attestation de demande d'asile qui lui avait été initialement délivrée le prive du bénéfice du droit d'asile et porte une atteinte grave et manifestement illégale à la liberté fondamentale que constitue le droit de solliciter l'asile ;<br/>
              - le délai imparti par le règlement du 26 juin 2013 pour sa remise aux autorités bulgares expirant le 23 février 2017 et les conditions permettant une prolongation du délai n'étant pas remplies, la responsabilité de l'examen de sa demande d'asile relève désormais de la France ; <br/>
              - il ne se trouve pas en situation de fuite, dès lors qu'il ne s'est pas soustrait de manière intentionnelle et systématique au contrôle de l'autorité administrative mais au contraire s'est rendu à toutes les convocations de l'Office français de l'immigration et de l'intégration et de la préfecture, excepté celle du 13 février 2017 ; <br/>
              - contrairement à ce qu'a estimé le juge des référés, il était dans l'impossibilité de retirer en temps utile la convocation qui lui a été adressée pour le 13 février 2017.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le règlement n° 604/2013 du Parlement européen et du Conseil du 26 juin 2013 ;<br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Considérant qu'aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. Le juge des référés se prononce dans un délai de quarante-huit heures " ; qu'en vertu de l'article L. 522-3 du même code, le juge des référés peut rejeter une requête par une ordonnance motivée, sans instruction contradictoire ni audience publique, lorsque la condition d'urgence n'est pas remplie ou lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable ou qu'elle est mal fondée ; qu'il appartient au juge d'appel de prendre en considération les éléments recueillis par le juge du premier degré dans le cadre de la procédure écrite et orale qu'il a diligentée ;<br/>
<br/>
              2. Considérant que le droit constitutionnel d'asile, qui a le caractère d'une liberté fondamentale, a pour corollaire le droit de solliciter le statut de réfugié ; que s'il implique que l'étranger qui sollicite la reconnaissance de la qualité de réfugié soit en principe autorisé à demeurer sur le territoire jusqu'à ce qu'il ait été statué sur sa demande, ce droit s'exerce dans les conditions définies par le code de l'entrée et du séjour des étrangers et du droit d'asile, qui mettent notamment en oeuvre les dispositions du règlement (UE) n° 604/2013 du Parlement européen et du Conseil du 26 juin 2013 ; que l'article L. 742-3 du code de l'entrée et du séjour des étrangers et du droit d'asile prévoit que l'étranger, dont l'examen de la demande d'asile relève de la responsabilité d'un autre Etat en vertu des dispositions du règlement du 26 juin 2013, peut faire l'objet d'un transfert vers l'Etat responsable de l'examen de sa demande d'asile ; que ce transfert peut avoir lieu pendant une période de six mois, susceptible d'être prolongée à douze ou dix-huit mois dans les conditions prévues à l'article 29 du règlement du 26 juin 2013 ; <br/>
<br/>
              3. Considérant qu'il résulte de l'instruction que M. A..., ressortissant afghan, entré irrégulièrement sur le territoire français, a présenté une demande d'asile à la préfecture de police le 8 août 2016 ; qu'il est apparu que ses empreintes digitales avaient déjà été relevées en Bulgarie ; que les autorités bulgares, saisies le 10 août 2016 par les autorités françaises d'une demande de remise en application du règlement du 26 juin 2013, ont accepté le transfert de l'intéressé le 22 août 2016 ; que, par arrêté du 4 octobre 2016, le préfet de police a décidé le transfert de l'intéressé vers la Bulgarie, responsable de l'examen de sa demande d'asile, et lui a délivré un laissez-passer pour déférer à cette décision ; que M. A...n'a pas exécuté la décision et n'a fait état d'aucune difficulté pour gagner la Bulgarie ; qu'il a refusé l'aide au transfert volontaire vers la Bulgarie qui lui a été proposée par l'Office français de l'immigration et de l'intégration le 31 janvier 2017 ; qu'il ne s'est pas présenté le 13 février 2017 à la préfecture de police en réponse à la convocation qui lui avait été adressée par une lettre du 3 février 2017, expédiée le 6 février et mise à disposition le 7 février ; que les autorités françaises ont alors sollicité des autorités bulgares la prolongation de la période de transfert prévue par l'article 29 du règlement du 26 juin 2013 ; que les autorités bulgares ont accepté la prolongation jusqu'au 22 février 2018 ; qu'après avoir retiré le 14 février 2017 la lettre de convocation qui lui avait été adressée et avoir demandé au préfet de police d'être convoqué à une autre date, l'intéressé a attendu l'expiration du délai initial de transfert pour présenter une nouvelle demande d'asile le 28 février 2017, dont le préfet a refusé l'enregistrement ; <br/>
<br/>
              4. Considérant qu'en refusant, au vu de l'ensemble des circonstances de l'espèce, l'enregistrement de cette nouvelle demande d'asile, le préfet de police n'a pas porté d'atteinte grave et manifestement illégale au droit d'asile ; qu'il s'ensuit que M. A...n'est pas fondé à soutenir que c'est à tort que, par l'ordonnance attaquée, le juge des référés du tribunal administratif de Paris a rejeté sa demande ; qu'il y a lieu de rejeter sa requête selon la procédure prévue à l'article L. 522-3 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de M. A...est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à M. B...A...et au préfet de police.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
