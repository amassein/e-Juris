<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027592831</ID>
<ANCIEN_ID>JG_L_2013_06_000000348187</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/59/28/CETATEXT000027592831.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème et 4ème sous-sections réunies, 21/06/2013, 348187, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-06-21</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>348187</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème et 4ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP PIWNICA, MOLINIE ; SCP GATINEAU, FATTACCINI ; SCP ROGER, SEVAUX, MATHONNET</AVOCATS>
<RAPPORTEUR>Mme Domitille Duval-Arnould</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:348187.20130621</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu l'ordonnance n° 11PA00800 du 1er avril 2011, enregistrée le 6 avril 2011 au secrétariat du contentieux du Conseil d'Etat, par laquelle le président de la cour administrative d'appel de Paris a transmis au Conseil d'Etat, en application de l'article R. 351-2 du code de justice administrative, la requête présentée par l'Etablissement français du sang (EFS) ; <br/>
<br/>
              Vu la requête, enregistrée le 15 février 2011 au greffe de la cour administrative d'appel de Paris, et les mémoires complémentaires, enregistrés les 7 octobre 2011 et les 16 et 30 janvier 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour l'EFS, dont le siège est 20 avenue du Stade de France à La Plaine Saint-Denis (93218) ; l'EFS demande :<br/>
<br/>
              1°) d'annuler le jugement n° 0810066/6-1 du 17 décembre 2010 par lequel le tribunal administratif de Paris l'a condamné à verser à la caisse primaire d'assurance maladie (CPAM) de Paris la somme de 7 118,56 euros, assortie des intérêts au taux légal à compter du 7 avril 2008 ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter la demande de la  caisse ;<br/>
<br/>
              3°) de mettre à la charge de la CPAM de Paris le versement d'une somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de la santé publique ; <br/>
<br/>
              Vu le code de la sécurité sociale ;<br/>
<br/>
              Vu la loi n° 2008-1330 du 17 décembre 2008 ; <br/>
<br/>
              Vu le décret n° 2010-251 du 11 mars 2010 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Domitille Duval-Arnould, Maître des Requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Piwnica, Molinié, avocat de l'Etablissement français du sang, à la SCP Gatineau, Fattaccini, avocat de la caisse primaire d'assurance maladie de Paris et à la SCP Roger, Sevaux, avocat de l'Office national de l'indemnisation des accidents médicaux, des affections iatrogènes et des infections nosocomiales ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que M.A..., imputant sa contamination par le virus de l'hépatite C à des transfusions sanguines pratiquées en 1984 à l'hôpital de la Pitié-Salpêtrière, a recherché en 2003 la responsabilité de l'Etablissement français du sang (EFS), auquel ont été transférés les droits et obligations de l'Assistance Publique - Hôpitaux de Paris nés de l'activité du centre de transfusion ayant fourni les produits transfusés ; que par un jugement du 6 décembre 2005, devenu définitif, le tribunal administratif de Paris, retenant que la contamination de M. A... avait pour origine les transfusions litigieuses, a condamné l'EFS à indemniser les ayants droit de M.A..., entre temps décédé, ainsi que son épouse ; que, le 4 juin 2008, la caisse primaire d'assurance maladie de Paris a saisi le tribunal administratif de Paris d'une demande tendant à ce qu'une somme de 7 118,56 euros correspondant à des frais d'hospitalisation de M. A...soit mise à la charge de l'EFS ; que ce dernier se pourvoit en cassation contre le jugement du 17 décembre 2010 par lequel le tribunal administratif a fait droit à cette demande après avoir mis hors de cause l'Office national d'indemnisation des accidents médicaux, des affections iatrogènes et des infections nosocomiales (ONIAM) ; <br/>
<br/>
              2. Considérant que l'article 67 de la loi du 17 décembre 2008 a, en son paragraphe I, introduit dans le code de la santé publique l'article L. 1221-14, confiant à l'ONIAM, en lieu et place de l'EFS, l'indemnisation des victimes de préjudices résultant de contaminations par le virus de l'hépatite C causées par des transfusion de produits sanguins ou des injections de médicaments dérivés du sang d'origine transfusionnelle, et, en son paragraphe IV, prévu que : " A compter de la date d'entrée en vigueur du présent article, l'Office national d'indemnisation des accidents médicaux, des affections iatrogènes et des infections nosocomiales (ONIAM) se substitue à l'Etablissement français du sang dans les contentieux en cours au titre des préjudices mentionnés à l'article L. 1221-14 du code de la santé publique n'ayant pas donné lieu à une décision irrévocable. (...) " ; que ces dispositions sont entrées en vigueur le 1er juin 2010 à la même date que le décret du 11 mars 2010 pris pour leur application ; <br/>
<br/>
              3. Considérant qu'en se fondant sur le caractère définitif du jugement du 6 décembre 2005 pour écarter la substitution de l'ONIAM à l'EFS, sollicitée par l'EFS sur le fondement de l'article 67, sans rechercher si ce jugement avait été rendu dans une instance à laquelle la CPAM de Paris était partie, le tribunal administratif de Paris a entaché son jugement d'erreur de droit ; que, par suite, et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, l'EFS est fondé à demander l'annulation du jugement du 17 décembre 2010 ;<br/>
<br/>
              4. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond par application de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              5. Considérant qu'eu égard au lien établi par les dispositions de l'article L. 376-1 du code de la sécurité sociale entre les droits de la victime et ceux de la caisse de sécurité sociale qui lui a versé des prestations à la suite de l'accident, le juge qui statue sur les droits de la victime après avoir régulièrement mis en cause la caisse doit être regardé comme statuant également sur les droits de cette dernière, alors même qu'à la suite de sa mise en cause celle-ci n'a pas demandé le remboursement de ses dépenses ; <br/>
<br/>
              6. Considérant qu'il résulte de l'instruction que le tribunal administratif de Paris a, dans l'instance engagée par M.A..., régulièrement mis en  cause la caisse primaire d'assurance maladie de Paris ; qu'alors même que la caisse n'a pas, dans le cadre de cette instance, demandé le remboursement des frais exposés par elle en raison de la contamination de son assuré, le jugement, devenu définitif, rendu le 6 décembre 2005 par le tribunal administratif a mis fin au contentieux relatif à ces frais ; que si ce jugement fait obstacle à la substitution de l'ONIAM à l'EFS, l'EFS est fondé à soutenir que l'autorité de chose jugée qui s'y attache rend irrecevable la demande, tendant au remboursement de dépenses antérieures au jugement, dont la caisse a saisi le tribunal administratif le 4 juin 2008 ; que cette demande ne peut, dès lors, qu'être rejetée ; <br/>
<br/>
              7. Considérant qu'il y a lieu, dans les circonstances de l'affaire, de mettre à la charge de la caisse primaire d'assurance maladie de Paris la somme de 3 000 euros que demande l'EFS  au titre de l'article L. 761-1 du code de justice administrative ; que ces dispositions font obstacle à ce que la somme demandée par la caisse primaire soit mise à la charge de l'EFS ; qu'il n'y a pas lieu, dans les circonstances de l'espèce, de mettre à la charge de l'EFS la somme demandée par l'ONIAM ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du 17 décembre 2010 du tribunal administratif de Paris est annulé.<br/>
<br/>
Article 2 : La demande de la caisse primaire d'assurance maladie de Paris en remboursement de ses débours est rejetée.<br/>
<br/>
Article 3 : La caisse primaire d'assurance maladie de Paris versera à l'EFS une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : Les conclusions de l'ONIAM et de la caisse primaire d'assurance maladie de Paris présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 5 : La présente décision sera notifiée à l'Etablissement français du sang, à la caisse primaire d'assurance maladie de Paris et à l'Office national d'indemnisation des accidents médicaux, des affections iatrogènes et des infections nosocomiales.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
