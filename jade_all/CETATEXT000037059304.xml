<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037059304</ID>
<ANCIEN_ID>JG_L_2018_06_000000400001</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/05/93/CETATEXT000037059304.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème chambre, 13/06/2018, 400001, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-06-13</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>400001</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP POTIER DE LA VARDE, BUK LAMENT, ROBILLOT ; SCP WAQUET, FARGE, HAZAN</AVOCATS>
<RAPPORTEUR>M. Olivier  Fuchs</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Sophie-Justine Lieber</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:400001.20180613</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A...B...a demandé au tribunal administratif de Nouvelle-Calédonie d'annuler pour excès de pouvoir la décision du 9 janvier 2014 par laquelle l'inspecteur du travail de la 5ème section de Nouvelle Calédonie a autorisé son licenciement. Par un jugement n° 1400083 du 11 décembre 2014, le tribunal administratif a annulé cette décision.<br/>
<br/>
              Par un arrêt n° 15PA01020 du 23 février 2016, la cour administrative d'appel de Paris a rejeté la requête formée par la société Manutrans contre ce jugement.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 23 mai et 19 août 2016 au secrétariat du contentieux du Conseil d'Etat, la société Manutrans demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de M. B...la somme de 2 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
<br/>
              - le code du travail de Nouvelle-Calédonie ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Olivier Fuchs, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Sophie-Justine Lieber, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Potier de la Varde, Buk Lament, Robillot, avocat de la société Manutrans et à la SCP Waquet, Farge, Hazan, avocat de M. B...;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, par un jugement du 11 décembre 2014, le tribunal administratif de Nouvelle-Calédonie a annulé, à la demande de M.B..., la décision du 9 janvier précédent par laquelle l'inspecteur du travail compétent avait autorisé son licenciement par la société Manutrans ; que cette annulation se fonde sur la circonstance que, M. B...ayant été licencié verbalement par la société Manutrans dès le 12 novembre 2013, l'autorité administrative était, en conséquence, tenue de rejeter la demande d'autorisation de licenciement présentée par l'employeur le 27 novembre suivant ; que la société Manutrans demande l'annulation de l'arrêt du 23 février 2016 par lequel la cour administrative d'appel de Paris a rejeté son appel formé contre ce jugement ;<br/>
              2. Considérant, en premier lieu, qu'eu égard au caractère exécutoire de l'ordonnance de référé du 27 décembre 2013 par laquelle la vice-présidente du tribunal du travail de Nouméa a constaté, à la demande de M. B..., la rupture de son contrat de travail par un licenciement verbal le 12 novembre 2013, la cour administrative d'appel n'a pas commis d'erreur de droit en déduisant du dispositif de cette ordonnance que l'intéressé avait été licencié verbalement à cette date ;<br/>
<br/>
              3. Considérant, en deuxième lieu, qu'en se fondant, ainsi qu'il vient d'être dit, sur les termes de l'ordonnance de référé du 27 décembre 2013 pour retenir l'existence d'un licenciement de M. B...le 12 novembre 2013, la cour a implicitement écarté le moyen, au demeurant inopérant, tiré de ce que différentes circonstances de fait établissaient qu'aucun licenciement n'avait eu lieu ; que la société requérante n'est, par suite, pas fondée à soutenir que l'arrêt attaqué est, faute d'avoir répondu à ce moyen, entaché d'insuffisance de motivation ;<br/>
<br/>
              4. Considérant, enfin, que la cour n'ayant, pour rejeter l'appel de la société Manutrans, statué qu'à titre surabondant sur l'existence d'un lien entre la demande de licenciement et les mandats détenus par le salarié, la circonstance qu'elle aurait, ce faisant, commis une erreur de droit est, en tout état de cause, sans incidence sur le dispositif de son arrêt ; <br/>
<br/>
              5. Considérant qu'il résulte de tout ce qui précède que la société Manutrans n'est pas fondée à demander l'annulation de l'arrêt qu'elle attaque ; que son pourvoi doit, par suite, être rejeté, y compris, par voie de conséquence, ses conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative ; qu'en revanche, il y a lieu, dans les circonstances de l'espèce, de mettre à sa charge une somme de 3 000 euros à verser à M. B... au titre des mêmes dispositions ;<br/>
<br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er : Le pourvoi de la société Manutrans est rejeté.<br/>
Article 2 : La société Manutrans versera à M. B...la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 3 : La présente décision sera notifiée à la société Manutrans et à M. A... B....<br/>
Copie en sera adressée à la ministre du travail.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
