<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030286056</ID>
<ANCIEN_ID>JG_L_2015_02_000000368731</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/28/60/CETATEXT000030286056.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  5ème sous-section jugeant seule, 25/02/2015, 368731, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-02-25</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>368731</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 5ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP RICHARD ; SCP WAQUET, FARGE, HAZAN</AVOCATS>
<RAPPORTEUR>Mme Anissia Morel</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Fabienne Lambolez</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2015:368731.20150225</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Mme B...A...a demandé au tribunal administratif de Nîmes, d'une part, d'annuler la décision du 24 décembre 2009 du directeur du centre hospitalier de Montfavet la plaçant en congé de maladie ordinaire puis en disponibilité pour raison de santé et, d'autre part, de condamner le centre hospitalier à lui verser une indemnité de 10 000 euros en réparation des préjudices nés de cette décision. Par un jugement n° 1000515 du 7 février 2013 le tribunal administratif a rejeté sa demande. <br/>
<br/>
              Par une requête, enregistrée le 25 mars 2013 au greffe de la cour administrative d'appel de Marseille et transmise au Conseil d'Etat, en application de l'article R. 351-2 du code de justice administrative, par l'ordonnance n° 13MA01214 du 26 avril 2013 du président de cette cour, et par un mémoire complémentaire, enregistré le 27 septembre 2013 au secrétariat du contentieux du Conseil d'Etat, Mme A...demande :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande ;<br/>
<br/>
              3°) de mettre à la charge du centre hospitalier le versement d'une somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
<br/>
              - la loi n° 86-33 du 9 janvier 1986 ;<br/>
<br/>
              - le décret n° 88-386 du 19 avril 1988 ;<br/>
<br/>
              - le décret n° 88-976 du 13 octobre 1988 ;<br/>
<br/>
              - le décret n° 89-376 du 8 juin 1989 ;<br/>
<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Anissia Morel, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Fabienne Lambolez, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Richard, avocat de Mme  A...et à la SCP Waquet, Farge, Hazan, avocat du centre hospitalier de Montfavet ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que Mme A..., cadre de santé, a été recrutée le 1er octobre 1974 par le centre hospitalier de Montfavet ; qu'elle a dû interrompre son activité professionnelle le 26 décembre 2008 en raison de son état de santé ; qu'à la suite d'un avis rendu le 17 décembre 2009 par le comité médical départemental, le directeur de l'établissement, par une décision du 24 décembre 2009, l'a placée en congé de maladie ordinaire à compter du 26 décembre 2008, puis en disponibilité pour raison de santé pour une période de trois mois à compter du 26 décembre 2009, date d'épuisement de ses droits à congés de maladie ; que Mme A..., sur sa demande, a été admise à faire valoir ses droits à la retraite à compter du 1er avril 2010 par une décision du 14 janvier 2010 du directeur du centre hospitalier ; qu'elle se pourvoit en cassation contre le jugement du 7 février 2013 par lequel le tribunal administratif de Nîmes a rejeté sa demande tendant à l'annulation de la décision du 24 décembre 2009 et à la condamnation du centre hospitalier à l'indemniser des préjudices nés de cette décision ;<br/>
<br/>
              2. Considérant, d'une part, qu'aux termes de l'article 71 de la loi du 9 janvier 1986 portant dispositions statutaires relatives à la fonction publique hospitalière : " Lorsque les fonctionnaires sont reconnus, par suite d'altération de leur état physique, inaptes à l'exercice de leurs fonctions, le poste de travail auquel ils sont affectés est adapté à leur état physique. Lorsque l'adaptation du poste de travail n'est pas possible, ces fonctionnaires peuvent être reclassés dans des emplois d'un autre corps s'ils ont été déclarés en mesure de remplir les fonctions correspondantes. Le reclassement est subordonné à la présentation d'une demande par l'intéressé " ; qu'aux termes de l'article 2 du décret du 8 juin 1989, pris pour l'application de ces dispositions : " Dans les cas où l'état physique d'un fonctionnaire, sans lui interdire d'exercer toute activité, ne lui permet pas de remplir les fonctions correspondant aux emplois de son grade, l'intéressé peut présenter une demande de reclassement dans un emploi relevant d'un autre grade de son corps ou dans un emploi relevant d'un autre corps. L'autorité investie du pouvoir de nomination recueille l'avis du comité médical départemental " ; qu'aux termes de l'article 17 du décret du 19 avril 1988 relatif aux conditions d'aptitude physique et aux congés de maladie des agents de la fonction publique hospitalière : " Lorsque le fonctionnaire est dans l'incapacité de reprendre son service à l'expiration de la première période de six mois consécutifs de congé de maladie, le comité médical est saisi pour avis de toute demande de prolongation de ce congé dans la limite des six mois restant à courir. / Lorsqu'un fonctionnaire a obtenu pendant une période de douze mois consécutifs des congés de maladie d'une durée totale de douze mois, il ne peut, à l'expiration de sa dernière période de congé, reprendre son service qu'après l'avis favorable du comité médical. /Si l'avis du comité médical est défavorable, le fonctionnaire est soit mis en disponibilité, soit, s'il le demande, reclassé dans un autre emploi, soit, s'il est reconnu définitivement inapte à l'exercice de tout emploi, admis à la retraite après avis de la commission de réforme des agents des collectivités locales (...) " ; qu'aux termes du premier alinéa de l'article 36 du même décret : " La mise en disponibilité prévue aux articles 17 et 35 du présent décret est prononcée après avis du comité médical ou de la commission départementale de réforme sur l'inaptitude du fonctionnaire à reprendre ses fonctions " ;<br/>
<br/>
              3. Considérant qu'il ressort des pièces du dossier soumis au tribunal que, dans son avis du 17 décembre 2009, le comité médical départemental a recommandé que Mme A...soit, eu égard à son état de santé, placée en congé de maladie à compter du 26 décembre 2008 puis en disponibilité d'office pour trois mois à compter du 26 décembre 2009 ; qu'en se prononçant en faveur de cette mise en disponibilité, le comité a nécessairement estimé que l'intéressée était physiquement inapte à reprendre son service à l'expiration de ses droits à congé de maladie ; que, dès lors, en retenant que le comité ne s'était pas prononcé sur l'aptitude de Mme A...à reprendre son service, pour en déduire qu'il n'appartenait pas à l'établissement de lui proposer un poste de reclassement, le tribunal administratif a entaché son jugement de dénaturation ; qu'au surplus, le tribunal n'a pas répondu à un moyen, soulevé par l'intéressée dans un mémoire enregistré le 19 janvier 2011, tiré de ce que l'administration n'avait pas recherché la possibilité d'adapter son poste de travail ; que, par suite, sans qu'il soit besoin d'examiner l'autre moyen du pourvoi, Mme A...est fondée à demander l'annulation du jugement qu'elle attaque ;<br/>
<br/>
              4. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge du centre hospitalier de Montfavet le versement de la somme de 3 000 euros à Mme A... au titre des frais exposés par elle et non compris dans les dépens ; qu'en revanche, les dispositions de l'article L. 761-1 du code de justice administrative  font obstacle à ce que soit mise à la charge de MmeA..., qui n'est pas la partie perdante dans la présente instance, la somme demandée à ce titre par le centre hospitalier ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du 7 février 2013 du tribunal administratif de Nîmes est annulé.<br/>
<br/>
		Article 2 : L'affaire est renvoyée au tribunal administratif de Nîmes.<br/>
<br/>
Article 3 : Le centre hospitalier de Montfavet versera à Mme A...la somme de 3 000 euros en application de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : Les conclusions du centre hospitalier de Montfavet au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 5 : La présente décision sera notifiée à Mme B...A...et au centre hospitalier de Montfavet.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
