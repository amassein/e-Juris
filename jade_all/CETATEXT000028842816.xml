<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028842816</ID>
<ANCIEN_ID>JG_L_2014_04_000000346086</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/84/28/CETATEXT000028842816.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème / 10ème SSR, 11/04/2014, 346086</TITRE>
<DATE_DEC>2014-04-11</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>346086</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème / 10ème SSR</FORMATION>
<TYPE_REC>Contentieux des pensions</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DIDIER, PINET</AVOCATS>
<RAPPORTEUR>M. Olivier Gariazzo</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Aladjidi</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:346086.20140411</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 25 janvier et 26 avril 2011 au secrétariat du contentieux du Conseil d'État, présentés pour M. A... B... ; M. B... demande au Conseil d'État :<br/>
<br/>
              1°) d'annuler l'arrêt RG 09/00002 du 10 novembre 2010 de la cour régionale des pensions de Reims en tant que, par cet arrêt, la cour a, sur l'appel du ministre de la défense, annulé le jugement du 10 juillet 2009 du tribunal départemental des pensions des Ardennes lui accordant un droit à pension au taux de 50 % pour syndrome dépressif à compter du 26 juin 2006 ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel du ministre ;<br/>
<br/>
              3°) de mettre à la charge de l'État la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code des pensions militaires d'invalidité et des victimes de la guerre ;<br/>
<br/>
              Vu le code pénal ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Olivier Gariazzo, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Frédéric Aladjidi, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Didier, Pinet, avocat de M. B...;<br/>
<br/>
<br/>
<br/>1. Considérant, d'une part, qu'aux termes de l'article L. 2 du code des pensions militaires d'invalidité et des victimes de la guerre : " Ouvrent droit à pension : / (...) 2° Les infirmités résultant de maladies contractées par le fait ou à l'occasion du service " ; qu'aux termes de l'article L. 4 du même code : " Les pensions sont établies d'après le degré d'invalidité. / Sont prises en considération les infirmités entraînant une invalidité égale ou supérieure à 10 %. / Il est concédé une pension : / (...) 3° Au titre d'infirmité résultant exclusivement de maladie, si le degré d'invalidité qu'elles entraînent atteint ou dépasse : / 30 % en cas d'infirmité unique " ;<br/>
<br/>
              2. Considérant, d'autre part, qu'aux termes de l'article 432-11 du code pénal, dans sa rédaction applicable aux faits de l'espèce : " Est puni de dix ans d'emprisonnement et de 150 000 euros d'amende le fait, par une personne dépositaire de l'autorité publique, chargée d'une mission de service public, ou investie d'un mandat électif public, de solliciter ou d'agréer, sans droit, à tout moment, directement ou indirectement, des offres, des promesses, des dons, des présents ou des avantages quelconques pour elle-même ou pour autrui : / 1° Soit pour accomplir ou s'abstenir d'accomplir un acte de sa fonction, de sa mission ou de son mandat ou facilité par sa fonction, sa mission ou son mandat ; / 2° Soit pour abuser de son influence réelle ou supposée en vue de faire obtenir d'une autorité ou d'une administration publique des distinctions, des emplois, des marchés ou toute autre décision favorable " ;<br/>
<br/>
              3. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond qu'au mois de juin 2004, M. A... B..., qui était alors adjudant de gendarmerie à la brigade de Charleville-Mézières, a été soupçonné d'avoir diligenté une enquête à l'encontre d'un concurrent d'une personne qui lui aurait procuré divers avantages et d'avoir ainsi commis le délit prévu et réprimé par l'article 432-11 du code pénal ; qu'il a alors été mis en examen pour corruption passive et placé sous contrôle judiciaire, avant de bénéficier, le 24 décembre 2008, d'une ordonnance de non-lieu devenue, depuis lors, définitive ; qu'étant atteint d'un syndrome dépressif entraînant une invalidité au taux de 50 %, il a déposé, le 19 mai 2006, en se prévalant de l'imputabilité au service de son infirmité, une demande de pension d'invalidité sur le fondement des articles L. 2 et L. 4 du code des pensions militaires d'invalidité et des victimes de la guerre, qui a été rejetée par le ministre de la défense ; que par un jugement du 10 juillet 2009, le tribunal départemental des pensions des Ardennes lui a accordé une pension militaire d'invalidité au taux de 50 % à compter du 28 juin 2006 ; que M. B... se pourvoit en cassation contre l'arrêt du 10 novembre 2010 par lequel, sur l'appel du ministre de la défense, la cour régionale des pensions de Reims a infirmé ce jugement ;<br/>
<br/>
              4. Considérant que, pour refuser à M. B... le bénéfice d'une pension militaire d'invalidité, la cour a jugé que les faits à l'origine de sa mise en examen étaient constitués des avantages dont il aurait bénéficié pour l'achat d'un véhicule et dans le cadre de relations avec un club sportif et que ces avantages relevaient d'agissements personnels, par nature détachables du service ; qu'en statuant ainsi, alors que la mise en examen de M. B... était également motivée par la décision qu'il avait prise, dans l'exercice de ses fonctions, de diligenter une enquête, la cour a commis une erreur de droit au regard des dispositions citées ci-dessus ; que son arrêt doit, par suite et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, être annulé ;<br/>
<br/>
              5. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              6. Considérant qu'il résulte de l'instruction et n'est pas sérieusement contesté que le syndrome dépressif ayant entraîné l'invalidité de M. B... au taux de 50 % est consécutif à sa mise en examen pour corruption passive, laquelle n'a pu intervenir qu'à raison des fonctions qu'il exerçait ; que, compte notamment tenu de l'ordonnance de non-lieu rendue à son endroit à titre définitif, aucun fait personnel de M. B... n'est de nature à rompre le lien entre les actes qu'il a accomplis dans l'exercice de ses fonctions d'adjudant de gendarmerie et le service ; que dès lors, M. B... doit être regardé comme apportant la preuve de ce que l'infirmité invalidante dont il est atteint est imputable au service ;<br/>
<br/>
              7. Considérant qu'il résulte de ce qui précède que le ministre de la défense n'est pas fondé à se plaindre de ce que, par le jugement qu'il attaque, le tribunal départemental des pensions des Ardennes a accordé à M. B... une pension militaire d'invalidité au taux de 50 % à compter du 28 juin 2006 ;<br/>
<br/>
              8. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'État la somme de 3 500 euros à verser à M. B... au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : L'arrêt du 10 novembre 2010 de la cour régionale des pensions de Reims est annulé.<br/>
Article 2 : Le recours présenté par le ministre de la défense devant la cour régionale des pensions de Reims est rejeté.<br/>
Article 3 : L'État versera à M. B... une somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à M. A... B...et au ministre de la défense.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">48-01-02-03 PENSIONS. PENSIONS MILITAIRES D'INVALIDITÉ ET DES VICTIMES DE GUERRE. CONDITIONS D'OCTROI D'UNE PENSION. IMPUTABILITÉ. - INVALIDITÉ ENTRAÎNÉE PAR UN SYNDROME DÉPRESSIF CONSÉCUTIF À LA MISE EN EXAMEN DE L'INTÉRESSÉ POUR CORRUPTION PASSIVE - MISE EN EXAMEN N'AYANT PU INTERVENIR QU'À RAISON DES FONCTIONS QUE L'INTÉRESSÉ EXERÇAIT - ABSENCE DE FAIT PERSONNEL DE L'INTÉRESSÉ DE NATURE À ROMPRE LE LIEN ENTRE LES ACTES QU'IL A ACCOMPLIS DANS L'EXERCICE DE SES FONCTIONS ET LE SERVICE - CONSÉQUENCE - PREUVE DE L'IMPUTABILITÉ AU SERVICE APPORTÉE PAR L'INTÉRESSÉ - EXISTENCE.
</SCT>
<ANA ID="9A"> 48-01-02-03 Adjudant de gendarmerie dont l'invalidité a été entraînée par un syndrome dépressif consécutif à sa mise en examen pour corruption passive, laquelle n'a pu intervenir qu'à raison des fonctions qu'il exerçait.,,,Compte tenu notamment de l'ordonnance de non-lieu rendue à son endroit à titre définitif, aucun fait personnel de l'intéressé n'est de nature à rompre le lien entre les actes qu'il a accomplis dans l'exercice de ses fonctions et le service. Dès lors, l'intéressé doit être regardé comme apportant la preuve de ce que l'infirmité invalidante dont il est atteint est imputable au service.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
