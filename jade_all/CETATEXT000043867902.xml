<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043867902</ID>
<ANCIEN_ID>JG_L_2021_07_000000438286</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/86/79/CETATEXT000043867902.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème chambre, 13/07/2021, 438286, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-07-13</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>438286</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LYON-CAEN, THIRIEZ ; SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR>Mme Cécile Isidoro</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Marie-Gabrielle Merloz</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:438286.20210713</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              M. B... A... a demandé au tribunal administratif de Cergy-Pontoise, à titre principal, d'annuler les décisions des 30 mars et 3 avril 2014 par lesquelles le maire d'Argenteuil a prononcé son licenciement, d'enjoindre à la commune d'Argenteuil de le réintégrer dans ses fonctions, de condamner cette commune à lui verser une indemnité mensuelle de 2 501,85 euros depuis mai 2014 jusqu'à sa réintégration et la somme de 20 000 euros en réparation du préjudice moral subi, à titre subsidiaire, de condamner la commune d'Argenteuil à lui verser la somme de 60 024 euros en réparation du préjudice subi du fait de son licenciement abusif ou sans cause, la somme de 27 898,96 euros au titre des indemnités chômage, la somme de 4 281,40 euros au titre des congés payés, et la somme de 5 000 euros pour " résistance abusive " de l'administration. Par un jugement n° 1409184 du 17 novembre 2016, le tribunal administratif de Cergy-Pontoise a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 17VE00143 du 11 juillet 2019, la cour administrative d'appel de Versailles a annulé ce jugement en tant qu'il a rejeté les conclusions de M. A... tendant à l'annulation des décisions des 30 mars et 3 avril 2014 prononçant son licenciement et à la condamnation de la commune à lui verser la somme de 6 748,80 euros au titre de l'allocation d'aide au retour à l'emploi, annulé les décisions de licenciement, et rejeté les conclusions de M. A... tendant au versement de la somme de 6 748,80 euros au titre de l'allocation d'aide au retour à l'emploi ainsi que le surplus de ses conclusions. <br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 5 février, 22 mai et 10 novembre 2020 au secrétariat du contentieux du Conseil d'Etat, M. A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt en tant qu'il rejette ses demandes indemnitaires au titre de l'illégalité de son licenciement, d'une part, et au titre des congés payés dont il a été privé, d'autre part ;<br/>
<br/>
              2°) de faire droit à ses conclusions d'appel ;<br/>
<br/>
              3°) de mettre à la charge de la commune d'Argenteuil la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
<br/>
              - le code général des collectivités territoriales ;<br/>
              - la loi n° 84-53 du 26 janvier 1984 ;<br/>
              - le décret n° 87-1004 du 16 décembre 1987 ;<br/>
              - le décret n° 88-145 du 15 février 1988 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Cécile Isidoro, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Marie-Gabrielle Merloz, rapporteure publique ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Lyon-Caen, Thiriez, avocat de M. A... et à la SCP Piwnica, Molinié, avocat de la commune d'Argenteuil ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond qu'à la suite des élections municipales de 2014, M. A..., employé par la commune d'Argenteuil en vertu d'un contrat à durée indéterminée sur un emploi qualifié par ce contrat d'" assistant d'élus ", a été informé, par deux décisions des 30 mars et 3 avril 2014, qu'il serait mis fin à ses fonctions le 30 mai 2014. M. A... a demandé au tribunal administratif de Cergy-Pontoise d'une part, d'annuler ces deux décisions et à ce qu'il soit enjoint à la commune d'Argenteuil de le réintégrer dans ses fonctions et, d'autre part, à ce que cette commune soit condamnée à lui verser diverses indemnités. Par un jugement du 17 novembre 2016, le tribunal administratif de Cergy-Pontoise a rejeté sa demande. Par un arrêt du 11 juillet 2019, la cour administrative d'appel de Versailles a annulé ce jugement en tant qu'il a rejeté les conclusions de M. A... tendant à l'annulation des deux décisions prononçant son licenciement et à la condamnation de la commune d'Argenteuil à lui verser la somme de 6 748,80 euros au titre de l'allocation d'aide au retour à l'emploi et annulé les décisions du maire d'Argenteuil prononçant le licenciement de M. A.... La cour a en revanche rejeté les conclusions de celui-ci tendant au versement d'une somme d'argent au titre de l'allocation d'aide au retour à l'emploi, ainsi que le surplus de ses conclusions indemnitaires. M. A... se pourvoit en cassation contre cet arrêt en tant qu'il lui est défavorable.<br/>
<br/>
              En ce qui concerne les conclusions indemnitaires relatives au préjudice subi du fait de l'illégalité du licenciement :<br/>
<br/>
              2. D'une part, aux termes de l'article 110 de la loi du 26 janvier 1984 : "L'autorité territoriale peut, pour former son cabinet, librement recruter un ou plusieurs collaborateurs et mettre fin librement à leurs fonctions ". Aux termes de l'article 6 du décret du 16 décembre 1987 relatif aux collaborateurs de cabinet des autorités territoriales : " Les fonctions de collaborateur de cabinet prennent fin au plus tard en même temps que le mandat de l'autorité territoriale qui l'a recruté ". Pour apprécier le bien-fondé de la décision mettant fin aux fonctions d'un collaborateur de cabinet, le juge vérifie seulement qu'elle ne repose pas sur un motif matériellement inexact ou une erreur de droit et n'est pas entachée de détournement de pouvoir.<br/>
<br/>
              3. D'autre part, aux termes de l'article 42 du décret du 15 février 1988 pris pour l'application de l'article 136 de la loi du 26 janvier 1984 modifiée portant dispositions statutaires relatives à la fonction publique territoriale et relatif aux agents contractuels de la fonction publique territoriale : " Le licenciement ne peut intervenir qu'à l'issue d'un entretien préalable. La décision de licenciement est notifiée à l'intéressé par une lettre recommandée avec demande d'avis de réception. Cette lettre précise le ou les motifs du licenciement et la date à laquelle celui-ci doit intervenir compte tenu des droits à congés annuels restant à courir et de la durée du préavis ". Pour apprécier le bien-fondé du licenciement d'un agent contractuel, le juge vérifie non seulement qu'il ne repose pas sur un motif matériellement inexact ou une erreur de droit, et n'est pas entaché de détournement de pouvoir, mais contrôle le bien-fondé du motif invoqué, au regard notamment du comportement de l'agent ou de l'intérêt du service. A cet égard, lorsque cet agent occupe un emploi fonctionnel mentionné à l'article 53 de la loi du 26 janvier 1984, la circonstance qu'il ne dispose plus de la part de l'autorité territoriale de la confiance nécessaire au bon accomplissement de ses missions peut légalement justifier qu'il soit, pour ce motif, déchargé de ses fonctions.<br/>
<br/>
              4. Il ressort des énonciations de l'arrêt attaqué, que la cour a, d'une part, jugé que les fonctions exercées par M. A... au sein de la commune d'Argenteuil n'avaient pas pour effet de le soumettre aux dispositions du décret du 16 décembre 1987 relatif aux collaborateurs de cabinet, mais aussi que l'intéressé ne pouvait pas être regardé comme ayant occupé un emploi de rédacteur territorial, et, d'autre part, qu'elle s'est ensuite bornée, pour apprécier la justification de ce licenciement, à juger qu'il pouvait intervenir pour tout motif, qu'il ne reposait pas sur des faits matériellement inexacts et qu'il n'était entaché ni d'erreur de droit ni de détournement de pouvoir. En s'abstenant de se prononcer sur la position statutaire de l'intéressé, alors qu'il lui appartenait, pour exercer son contrôle sur le bien-fondé d'un tel licenciement, de qualifier juridiquement, au préalable, l'emploi occupé par M. A..., la cour a méconnu son office et commis une erreur de droit.<br/>
<br/>
              5. Si la commune d'Argenteuil soutient qu'en tout état de cause, il résulte de faits constants que M. A... atteignait la limite d'âge le 16 mai 2014, et que par conséquent son licenciement à compter du 30 mai 2014 n'a pu lui causer de préjudice, et demande que, le cas échéant, ce motif soit substitué à celui retenu par la cour, cette demande doit être écartée dès lors que, en tout état de cause, cette circonstance ne saurait exclure que l'illégalité de cette décision de licenciement, si elle était établie, puisse être regardée comme lui ayant causé un préjudice moral.<br/>
<br/>
              En ce qui concerne les conclusions relatives à l'octroi d'une indemnité compensatrice en raison des congés non pris du fait du licenciement :<br/>
<br/>
              6. Aux termes de l'article 5 du décret du 15 février 1988 : " L'agent contractuel en activité a droit, dans les conditions prévues par le décret n° 85-1250 du 26 novembre 1985 relatif aux congés annuels des fonctionnaires territoriaux, à un congé annuel dont la durée et les conditions d'attribution sont identiques à celles du congé annuel des fonctionnaires titulaires. / A la fin d'un contrat à durée déterminée ou en cas de licenciement n'intervenant pas à titre de sanction disciplinaire, l'agent qui, du fait de l'autorité territoriale, en raison notamment de la définition du calendrier des congés annuels, n'a pu bénéficier de tout ou partie de ses congés annuels a droit à une indemnité compensatrice. " <br/>
<br/>
              7. Il résulte de ces dispositions que l'agent non titulaire qui n'a pu bénéficier à la fin de son contrat à durée déterminée ou en cas de licenciement pour un motif autre que disciplinaire, de tout ou partie de ses congés annuels, faute pour l'administration de l'avoir informé de ses droits à congés et mis en mesure de les prendre ou en raison d'un empêchement imputable à celle-ci, a droit à une indemnité compensatrice pour les congés non pris. Il incombe à l'administration, lorsque l'agent établit que tout ou partie de ses congés accordés mais non pris restaient dus, de démontrer qu'elle a fait preuve de la diligence requise pour que celui-ci soit effectivement en mesure de prendre les congés annuels payés auxquels il avait droit.<br/>
<br/>
              8. Il ressort des pièces du dossier soumis aux juges du fond que M. A..., s'il sollicite le versement d'une indemnité compensatrice d'un montant de 4 281,40 euros équivalent à quarante-cinq (45) jours de congés, n'établit pas que tout ou partie de ses congés annuels en 2013 et 2014 lui restaient dus. Dès lors, en jugeant que M. A... ne justifiait nullement, par les pièces produites, qu'il n'aurait pu bénéficier du fait de l'administration des congés annuels auxquels il avait droit, la cour n'a ni dénaturé les pièces du dossier ni commis d'erreur de droit. <br/>
<br/>
              9. Il résulte de tout ce qui précède que M. A... est seulement fondé à demander l'annulation de l'arrêt attaqué en tant qu'il rejette ses conclusions relatives au préjudice subi du fait de l'illégalité de son licenciement.<br/>
<br/>
              10. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mise à la charge de M. A..., qui n'est pas la partie perdante dans la présente instance, la somme demandée par la commune d'Argenteuil. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la commune d'Argenteuil une somme de 1500 euros qui sera versée à M. A.... <br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Versailles du 11 juillet 2019 est annulé en tant qu'il rejette les conclusions de M. A... relatives à l'indemnisation du préjudice subi du fait de l'illégalité de son licenciement.<br/>
Article 2 : L'affaire est renvoyée, dans cette mesure, à la cour administrative d'appel de Versailles.<br/>
Article 3 : La commune d'Argenteuil versera à M. A... une somme de 1 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Le surplus des conclusions de M. A..., ainsi que les conclusions présentées par la commune d'Argenteuil au titre de l'article L. 761-1 du code de justice administrative sont rejetés. <br/>
Article 5 : La présente décision sera notifiée à M. B... A... et à la commune d'Argenteuil. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
