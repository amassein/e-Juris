<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043369326</ID>
<ANCIEN_ID>JG_L_2021_04_000000450888</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/36/93/CETATEXT000043369326.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 07/04/2021, 450888, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-04-07</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>450888</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2021:450888.20210407</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, un mémoire en réplique et un nouveau mémoire, enregistrés le 19 mars et les 1er et 2 avril 2021 au secrétariat du contentieux du Conseil d'Etat, la société Taking demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative :<br/>
<br/>
              1°) d'ordonner la suspension de l'exécution de l'article 37 du décret n° 2021-1310 du 29 octobre 2020 prescrivant les mesures générales nécessaires pour faire face à l'épidémie de covid-19 dans le cadre de l'état d'urgence sanitaire, dans sa rédaction issue du décret n° 2021-99 du 30 janvier 2021, en tant qu'il interdit de pratiquer la vente à emporter aux restaurants et débits de boisson qui sont intégrés dans des centres commerciaux dont la surface commerciale utile cumulée calculée est supérieure ou égale à vingt mille mètres carrés mais disposent d'un accès depuis une voie extérieure non close ni couverte ouverte à la circulation publique ;<br/>
<br/>
              2°) d'enjoindre au Premier ministre et au ministre des solidarités et de la santé de modifier l'article 37 du décret n° 2020-1310 du 29 octobre 2020 afin d'autoriser les restaurants et débits de boisson visés ci-dessus à pratiquer la vente à emporter ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Elle soutient que :<br/>
              - la condition d'urgence est satisfaite dès lors que, d'une part, les dispositions attaquées restreignent de manière importante l'exercice de la liberté d'entreprendre et de la liberté du commerce et de l'industrie, notamment en ce qu'elles ont pour effet de réduire l'activité économique de certains restaurants et débits de boisson et, d'autre part, les pertes financières qu'elle a subies du fait de ces dispositions sont importantes et de nature à compromettre la pérennité de son activité malgré les mesures de soutien décidées par l'exécutif ;<br/>
              - il existe un doute sérieux quant à la légalité des dispositions contestées ; <br/>
              - ces dispositions portent une atteinte illégale au principe d'égalité, en interdisant toute activité commerciale aux établissements concernés sans prendre en compte le fait que, bien qu'intégrés dans des centres commerciaux de plus de vingt mille mètres carrés, ils n'en partagent aucune des voies closes et couvertes et se trouvent ainsi dans une situation identique à celles d'autres établissements pour lesquels la vente à emporter reste autorisée ; <br/>
              - elles portent une atteinte disproportionnée à la liberté du commerce et de l'industrie et à la liberté d'entreprendre au regard de l'objectif de protection de santé publique qui les fonde puisque, comme l'ont rappelé le Premier ministre et le ministre des solidarités et de la santé lors de la conférence de presse tenue le 18 mars 2021 en se fondant sur une étude circonstanciée de l'Institut Pasteur, les risques de contamination en extérieur sont mineurs.<br/>
<br/>
              Par un mémoire en défense et un nouveau mémoire, enregistrés les 30 mars et 1er avril 2021, le ministre des solidarités et de la santé conclut au rejet de la requête. Il soutient que la condition d'urgence n'est pas satisfaite, et qu'il n'existe pas de doute sérieux quant à la légalité des dispositions contestées. <br/>
<br/>
              La requête a été communiquée au Premier ministre qui n'a pas produit d'observations. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la Constitution, et notamment son Préambule ; <br/>
              - le code de la santé publique ;<br/>
              - le décret n° 2020-1310 du 29 octobre 2020 ;<br/>
              - le décret n° 2021-99 du 30 janvier 2021 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
               Les parties ont été informées, sur le fondement de l'article 3 de l'ordonnance n° 2020-1402 du 18 novembre 2020 portant adaptation des règles applicables aux juridictions administratives, de ce qu'aucune audience ne se tiendrait et de ce que la clôture de l'instruction serait fixée le 2 avril 2021 à dix-huit heures. <br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision ".<br/>
<br/>
              2. Aux termes des II et II bis de l'article 37 du décret du 29 octobre susvisé, dans sa rédaction issue du décret du 30 janvier 2021 susvisé : " (...) les magasins de vente et centres commerciaux, comportant un ou plusieurs bâtiments dont la surface commerciale utile cumulée calculée dans les conditions du II bis est supérieure ou égale à vingt mille mètres carrés, ne peuvent accueillir du public. L'activité de retrait de commandes à l'intérieur des centres commerciaux relevant du présent alinéa, y compris pour les établissements mentionnés à l'article 40 du présent décret, est également interdite. (...) Il faut entendre par magasin de vente ou centre commercial tout établissement comprenant un ou plusieurs ensembles de magasins de vente, y compris lorsqu'ils ont un accès direct indépendant, notamment par la voie publique, et éventuellement d'autres établissements recevant du public pouvant communiquer entre eux, qui sont, pour leurs accès et leur évacuation, tributaires de mails clos. ".<br/>
<br/>
              3. La société requérante, qui exploite un restaurant situé dans un centre commercial à Aix-en-Provence dont la superficie est supérieure à 20 000 mètres carrés, a été mise en demeure, le 6 février 2021, par le préfet des Bouches-du-Rhône, de fermer son établissement et, ainsi, d'interrompre l'activité de vente à emporter qu'elle y avait maintenue. Se prévalant de ce que cet établissement dispose d'un accès sur la voie publique ne dépendant pas du mail clos du centre commercial, elle demande que soit ordonnée la suspension de l'exécution des dispositions citées au point 2 en tant qu'elles ne prévoient pas que les magasins de vente placés dans une telle situation, notamment les restaurants, puissent poursuivre leur activité de vente à emporter.<br/>
<br/>
              4. D'une part, il ressort des dispositions en question qu'elles ne visent que les magasins de vente qui sont exclusivement tributaires d'un mail clos pour leur accès et leur évacuation et ne concernent donc pas les magasins directement accessibles par la voie publique, pour autant que l'accès en question permette le respect des obligations de sécurité en vigueur, notamment pour l'évacuation de la clientèle et du personnel de l'établissement concerné et qu'un éventuel autre accès par un mail clos soit condamné. Il ressort, par ailleurs, des écritures du ministre des solidarités et de la santé que celui-ci partage expressément cette interprétation et qu'en conséquence il estime que la société requérante ne peut se voir interdire, sur le fondement de ces dispositions, d'exercer son activité de vente à emporter dans son établissement d'Aix-en-Provence.<br/>
<br/>
              5. D'autre part, il résulte de l'instruction que le préfet des Bouches-du-Rhône a, le 1er avril 2021, décidé de retirer la mise en demeure d'avoir à interrompre son activité dans cet établissement d'Aix-en-Provence qu'il avait adressée, le 6 février 2021, à la société requérante. Il suit de là que l'urgence invoquée n'est pas constituée. Il y a donc lieu, sans qu'il soit besoin de statuer sur les moyens de la requête, de la rejeter, y compris ses conclusions au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
              ------------------<br/>
<br/>
Article 1er : La requête de la société Taking est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à la société Taking et au ministre des solidarités et de la santé.<br/>
Copie en sera adressée au Premier ministre.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
