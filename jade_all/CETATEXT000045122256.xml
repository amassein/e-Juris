<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000045122256</ID>
<ANCIEN_ID>JG_L_2022_02_000000450606</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/45/12/22/CETATEXT000045122256.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème chambre, 03/02/2022, 450606, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2022-02-03</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>450606</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP THOUIN-PALAT, BOUCARD</AVOCATS>
<RAPPORTEUR>M. François Charmont</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2022:450606.20220203</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              M. A... B... a demandé à la commission du contentieux du stationnement payant d'annuler l'avis de paiement du forfait de post-stationnement mis à sa charge le 14 septembre 2020 par la ville de Paris. Par une ordonnance n° 20056179 du 11 janvier 2021, le président de chambre désigné par la présidente de la commission du contentieux du stationnement payant, statuant sur le fondement du 5° de l'article R. 2333-120-27 du code général des collectivités territoriales, a rejeté sa demande.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 11 mars et 11 juin 2021 au secrétariat du contentieux du Conseil d'Etat, M. B... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande ;<br/>
<br/>
              3°) de mettre à la charge de la ville de Paris la somme de 5 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code général des collectivités territoriales ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. François Charmont, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Thouin-Palat, Boucard, avocat de M. B....<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis au juge du fond que la ville de Paris a mis à la charge le 14 septembre 2020 de M. B... le paiement d'un forfait de post-stationnement de 35 euros, au motif que ce dernier aurait omis de payer une redevance due pour le stationnement de son véhicule. M. B... se pourvoit en cassation contre l'ordonnance du 11 janvier 2021 par laquelle le président de chambre désigné par la présidente de la commission du contentieux du stationnement payant a, sur le fondement du 5° de l'article R. 2333-120-27 du code général des collectivités territoriales, rejeté sa demande au motif que, les moyens présentés n'étant pas assortis des précisions permettant d'en apprécier le bien-fondé, elle était manifestement infondée.<br/>
<br/>
              2. Lorsque le titulaire du certificat d'immatriculation entend contester devant la commission du stationnement payant le bien-fondé d'un forfait de post-stationnement mis à sa charge, il doit présenter, en application de l'article R. 2333-120-30 du code général des collectivités territoriales, sa requête au moyen d'un formulaire, dont le modèle est fixé par arrêté du vice-président du Conseil d'Etat. En vertu des dispositions du 5° du I de l'article R. 2333-120-31 du même code, en cas de contestation de la décision rendue à l'issue du recours administratif préalable obligatoire, la requête doit être accompagnée notamment de la pièce justifiant du paiement préalable du montant du forfait de post-stationnement ou de l'avis de paiement rectificatif.<br/>
<br/>
              3. Il ressort des pièces du dossier soumis au juge du fond que M. B... se bornait à soutenir qu'il s'était acquitté de la redevance de stationnement litigieuse sans fournir, en dépit des indications notamment contenues dans le formulaire mentionné au point 2, aucune preuve du bien-fondé de sa demande de décharge du paiement du forfait de post-stationnement permettant de faire apparaître que son véhicule se trouvait en situation régulière de stationnement. M. B... ne peut, dès lors, utilement se prévaloir, pour la première fois devant le juge de cassation, de la copie d'écran de l'application PayByPhone pour justifier de son stationnement résidentiel payé par le biais de cette application. Dans ces conditions, le président de chambre désigné qui n'était pas tenu de l'inviter à régulariser sa requête en complétant ses productions a pu, sans dénaturer les pièces du dossier qui lui étaient soumises ni méconnaître son office, ni faire un usage abusif de la faculté que lui reconnaissent les dispositions du 5° de l'article R. 2333-120-27 du code général des collectivités territoriales, rejeter par ordonnance la demande dont il était saisi comme manifestement infondée.<br/>
<br/>
              4. Il résulte de tout ce qui précède que M. B... n'est pas fondé à demander l'annulation de la décision qu'il attaque. Son pourvoi doit, par suite, être rejeté, y compris par voie de conséquence ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
		Article 1er : Le pourvoi de M. B... est rejeté.<br/>
<br/>
Article 2 : La présente décision sera notifiée à M. A... B... et à la ville de Paris.<br/>
              Délibéré à l'issue de la séance du 13 janvier 2022 où siégeaient : M. Olivier Yeznikian, assesseur, présidant ; M. Jean-Philippe Mochon, conseiller d'Etat et M. François Charmont, maître des requêtes-rapporteur. <br/>
<br/>
              Rendu le 3 février 2022.<br/>
                 Le président : <br/>
                 Signé : M. Olivier Yeznikian<br/>
 		Le rapporteur : <br/>
      Signé : M. François Charmont<br/>
                 La secrétaire :<br/>
                 Signé : Mme D... C...<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
