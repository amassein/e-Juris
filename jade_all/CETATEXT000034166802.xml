<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034166802</ID>
<ANCIEN_ID>JG_L_2017_03_000000407768</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/16/68/CETATEXT000034166802.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 07/03/2017, 407768, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-03-07</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>407768</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FABIANI, LUC-THALER, PINATEL ; SCP ZRIBI, TEXIER</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2017:407768.20170307</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, enregistrée le 8 février 2017 au secrétariat du Conseil d'Etat, M. B...A...demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative :<br/>
<br/>
              1°) d'ordonner la suspension de l'exécution de la décision du 3 janvier 2017 par laquelle le président de l'université de Poitiers l'a suspendu de ses fonctions pour une durée de 4 mois ;<br/>
              2°) d'enjoindre à l'université de Poitiers d'exécuter l'ordonnance à intervenir en le réintégrant dans ses fonctions de professeur d'université, à compter du jour de la notification de cette ordonnance et sous astreinte de 500 euros par jour de retard ;<br/>
<br/>
              3°) de mettre à la charge de l'université de Poitiers la somme de 3 000 euros en application des dispositions de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Il soutient que : <br/>
              - la condition d'urgence est remplie dès lors que la décision contestée porte une atteinte grave et immédiate à sa situation personnelle et professionnelle ainsi qu'à l'intérêt public qui s'attache au bon fonctionnement du service universitaire ;<br/>
              - il existe un doute sérieux sur la légalité de la décision contestée ; <br/>
              - la décision contestée est entachée d'un détournement de procédure, prévue à l'article L. 951-4 du code de l'éducation, dès lors qu'elle a été prononcée après l'intervention de la sanction d'interdiction d'enseigner de la section disciplinaire de l'université de Poitiers du 19 juillet 2016, suspendue par le Conseil national de l'enseignement supérieur et de la recherche le 22 novembre 2016 ; <br/>
              - elle est insuffisamment motivée dès lors que, présentant le caractère d'une mesure disciplinaire déguisée en ce qu'elle est dictée par l'intention de le sanctionner et traduit une dégradation objective de sa situation professionnelle, elle doit, à l'instar de toute sanction, être motivée ;<br/>
              - elle est entachée d'une erreur de fait dès lors que la réalité des faits n'est pas établie ;<br/>
              - à titre subsidiaire, elle est entachée d'une erreur d'appréciation ;<br/>
              - elle est disproportionnée eu égard aux faits qui lui sont reprochés.<br/>
              Par un mémoire en défense et deux mémoires de production, enregistrés les 21 février et 27 février 2017, l'université de Poitiers conclut au rejet de la requête et à ce que soit mis à la charge de M. A...la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. Elle soutient que la condition d'urgence n'est pas remplie et que les moyens soulevés par le requérant ne sont pas fondés. <br/>
              Par un mémoire en observations, enregistré le 22 février 2017, la ministre de l'éducation nationale, de l'enseignement supérieur et de la recherche conclut au rejet de la requête. Elle soutient que la condition d'urgence n'est pas remplie et que les moyens soulevés par le requérant ne sont pas fondés. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la loi n° 83-634 du 13 juillet 1983 portant droits et obligations des fonctionnaires ;<br/>
              - l'arrêté du 10 février 2012 portant délégation de pouvoirs en matière de recrutement et de gestion de certains personnels enseignants des établissements publics d'enseignement supérieur et de recherche ;<br/>
              - le code de l'éducation, notamment son article L. 951-4 ; <br/>
              - le code des relations entre le public et l'administration ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, M. A..., d'autre part, l'université de Poitiers et la ministre de l'éducation nationale, de l'enseignement supérieur et de la recherche ; <br/>
<br/>
              Vu le procès-verbal de l'audience publique du lundi 27 février 2017 à 15 heures au cours de laquelle ont été entendus : <br/>
              - Me Texier, avocat au Conseil d'Etat et à la Cour de cassation, avocat de M. A... ;<br/>
<br/>
              - Me Luc-Thaler, avocat au Conseil d'Etat et à la Cour de cassation, avocate de l'université de Poitiers ; <br/>
<br/>
- le représentant de l'université de Poitiers ; <br/>
<br/>
              - les représentants de la ministre de l'éducation nationale, de l'enseignement supérieur et de la recherche ;<br/>
              et à l'issue de laquelle le juge des référés a clos l'instruction ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
              1. Aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision ". <br/>
<br/>
              2. Aux termes de l'article L. 951-4 du code de l'éducation : " Le ministre chargé de l'enseignement supérieur peut prononcer la suspension d'un membre du personnel de l'enseignement supérieur pour un temps qui n'excède pas un an, sans suspension de traitement ". La suspension d'un professeur des universités, sur la base de ces dispositions, est une mesure à caractère conservatoire, prise dans le souci de préserver l'intérêt du service public universitaire. Elle peut être prononcée lorsque les faits imputés à l'intéressé présentent un caractère suffisant de vraisemblance et de gravité. En l'absence de poursuites pénales, son maintien en vigueur ou sa prorogation sont subordonnés à l'engagement de poursuites disciplinaires dans un délai raisonnable après son édiction.<br/>
              3. L'article L. 951-3 du code de l'éducation autorise le ministre chargé de l'enseignement supérieur à déléguer aux présidents des universités tout ou partie de ses pouvoirs en matière de recrutement et de gestion des personnels relevant de son autorité. En application de ces dernières dispositions, l'article 2 de l'arrêté du 10 février 2012 portant délégation de pouvoirs en matière de recrutement et de gestion de certains personnels enseignants des établissements publics d'enseignement supérieur et de recherche, prévoit que : " Les présidents et les directeurs des établissements publics d'enseignement supérieur dont la liste est fixée à l'article 3 du présent arrêté reçoivent délégation des pouvoirs du ministre chargé de l'enseignement supérieur pour le recrutement et la gestion des personnels enseignants mentionnés à l'article 1er du présent arrêté en ce qui concerne : / [...] 24. La suspension ". L'article 3 du même arrêté vise notamment les établissements publics à caractère scientifique, culturel et professionnel mentionnés à l'article L. 711-2 du code de l'éducation, dont font partie les universités. <br/>
<br/>
              4. M. A...est professeur des universités à la faculté de droit de Poitiers où il enseigne à des étudiants de 1ère année de licence et de master. Le 19 juillet 2016, la section disciplinaire du conseil académique de l'université de Poitiers a prononcé à son encontre la sanction d'interdiction d'exercer toutes fonctions d'enseignement pendant une période de cinq ans avec privation de la moitié de son traitement. Cette sanction avait été prononcée pour des comportements inappropriés avec plusieurs de ses étudiantes. Par une décision du 22 novembre 2016, le Conseil national de l'enseignement supérieur et de la recherche a fait droit à sa demande de prononcer le sursis à exécution de la mesure de sanction. Mais, sur le fondement de l'article L. 951-4 du code de l'éducation, une nouvelle mesure de suspension, d'une durée de quatre mois, a été prononcée à son encontre le 3 janvier 2017 par le président de l'université de Poitiers, justifiée par l'existence de faits nouveaux. Par la présente requête, M. A... demande au juge des référés du Conseil d'Etat la suspension de l'exécution de cette décision. <br/>
<br/>
              En ce qui concerne le doute sérieux quant à la légalité de la décision attaquée : <br/>
<br/>
              5. Le président de l'université de Poitiers a motivé la décision dont la suspension est demandée par la survenance de " faits établissant une présomption sérieuse de faute grave également susceptible de constituer une infraction de droit commun ". Pour justifier une telle présomption, l'université produit trois messages anonymes, postérieurs à la première sanction d'interdiction en date du 19 juillet 2016 d'exercer toutes fonctions d'enseignement prononcée par la section disciplinaire de l'université de Poitiers. Il ressort de l'examen de ces messages, éclairé par les échanges lors de l'audience publique que le premier courriel en date du 22 octobre 2016 était rédigé de manière à laisser penser à toute étudiante, témoignant dans le cadre de la procédure disciplinaire visant M. A..., qu'elle serait passible de poursuites en diffamation et de mise en garde à vue. Bien qu'il ne soit pas établi avec certitude que M. A... en soit l'auteur, il apparaît suffisamment vraisemblable, au regard, d'une part, du climat de tension persistant avec certaines étudiantes de l'université de Poitiers, et d'autre part, du fait que le courriel ait spécifiquement visé deux d'entre elles en lien direct avec l'enquête, qu'un tel message était de nature à préjudicier à l'intérêt du service public universitaire, en particulier en période d'examen. Quant au second message du 2 janvier 2017, il se présentait comme émanant de Me David Sebag, avocat au barreau de Paris - qui démentira en être l'auteur - et cherchait à obtenir de la formation disciplinaire de l'université des renseignements sur les débats en son sein. Enfin, la conjonction, établie par le service informatique de l'université, entre les procédures ayant conduit à l'envoi des deux messages en date du 22 octobre 2016 et du 2 janvier 2017, permet de caractériser de manière suffisamment probante et circonstanciée l'existence d'un lien entre l'émetteur des deux messages et le requérant et d'élever le soupçon d'une usurpation d'identité, susceptible, à elle seule, de justifier une mesure de suspension. Dans ces conditions, la mesure attaquée, justifiée par l'intérêt du service universitaire, ne constitue ni un détournement de procédure ni une sanction disciplinaire déguisée. Dès lors, aucun des moyens soulevés par M. A...n'est de nature à faire naître un doute sérieux quant à la légalité de la décision attaquée ; <br/>
<br/>
              6. Il résulte de tout ce qui précède que, sans qu'il soit besoin de statuer sur l'urgence, la requête de M. A...doit être rejetée, y compris les conclusions aux fins d'injonction et celles présentées au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de M. A...est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à M. B...A..., à l'université de Poitiers et à la ministre de l'éducation nationale, de l'enseignement supérieur et de la recherche.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
