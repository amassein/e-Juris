<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037092064</ID>
<ANCIEN_ID>JG_L_2018_06_000000414139</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/09/20/CETATEXT000037092064.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème et 9ème chambres réunies, 21/06/2018, 414139, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-06-21</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>414139</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème et 9ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Marie Gautier-Melleray</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Edouard Crépey</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:414139.20180621</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés les 7 septembre 2017 et 19 janvier 2018 au secrétariat du contentieux du Conseil d'Etat, M. B...A...demande au Conseil d'Etat d'annuler pour excès de pouvoir la décision du 6 juillet 2017 par laquelle la présidente de la Commission nationale de l'informatique et des libertés (CNIL) l'a informé de la clôture de sa plainte relative à la conservation par le centre hospitalier national ophtalmologique des Quinze-vingt (CHNO), son employeur, de données relatives à sa vie sexuelle.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ; <br/>
<br/>
              Vu :<br/>
              - la loi n° 78-17 du 6 janvier 1978 ;<br/>
              - le code de justice administrative ; <br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Marie Gautier-Melleray, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Edouard Crépey, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier que M. A...a saisi la Commission nationale Informatique et Libertés (CNIL) d'une plainte dirigée contre le centre hospitalier national ophtalmologique des Quinze-vingt (CHNO), son employeur, au motif que ce dernier conserverait dans son dossier administratif un document comportant des informations relatives à sa vie sexuelle alors qu'il avait expressément demandé à ce que ce document soit retiré. Par courrier du 6 juillet 2017, la présidente de la CNIL a indiqué à M. A...que ses services avaient eu plusieurs échanges de courrier avec le CHNO dont il ressortait que le document objet de sa plainte n'était pas conservé dans son dossier administratif mais dans un dossier relatif aux procédures judiciaires engagées en 2014 et 2015 par M. A...contre des mesures de changement d'affectation motivées par des informations attestées par ledit document et que la CNIL avait rappelé au CHNO qu'il lui appartenait de se mettre en conformité avec les obligations déclaratives concernant la mise en oeuvre d'un traitement de données à des fins de gestion du contentieux, ce qui n'avait pas été fait, et de supprimer les données traitées dans le cadre d'un contentieux lorsque les voies de recours ne seraient plus ouvertes contre la décision rendue. Par le même courrier, la présidente de la CNIL a indiqué à l'intéressé que, compte tenu de ces éléments, elle ne donnerait pas d'autres suite à sa plainte. M. A...demande l'annulation de cette décision.<br/>
<br/>
              2. Aux termes de l'article 8 de la loi du 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés, dans sa rédaction applicable au litige, " I. Il est interdit de collecter ou de traiter des données à caractère personnel qui font apparaître, directement ou indirectement, les origines raciales ou ethniques, les opinions politiques, philosophiques ou religieuses ou l'appartenance syndicale des personnes, ou qui sont relatives à la santé ou à la vie sexuelle de celles-ci. / II. Dans la mesure où la finalité du traitement l'exige pour certaines catégories de données, ne sont pas soumis à l'interdiction prévue au I : (...) / 5°Les traitements nécessaires à la constatation, à l'exercice ou à la défense d'un droit en justice (...) ". Aux termes du 2° de l'article 11 de la même loi, la CNIL : " veille à ce que les traitements de données à caractère personnel soient mis en oeuvre conformément aux dispositions de la présente loi. A ce titre : / (...) c) Elle reçoit les réclamations, pétitions et plaintes relatives à la mise en oeuvre des traitements de données à caractère personnel et informe leurs auteurs des suites données à celles-ci ". Aux termes du I de l'article 40 de la même loi : " Toute personne physique justifiant de son identité peut exiger du responsable d'un traitement que soient, selon les cas, rectifiées, complétées, mises à jour, verrouillées ou effacées les données à caractère personnel la concernant, qui sont inexactes, incomplètes, équivoques, périmées, ou dont la collecte, l'utilisation, la communication ou la conservation est interdite ". Aux termes du I de l'article 45 de la même loi : " I. - Lorsque le responsable d'un traitement ne respecte pas les obligations découlant de la présente loi, le président de la Commission nationale de l'informatique et des libertés peut le mettre en demeure de faire cesser le manquement constaté dans un délai qu'il fixe. En cas d'extrême urgence, ce délai peut être ramené à vingt-quatre heures. /Si le responsable du traitement se conforme à la mise en demeure qui lui est adressée, le président de la commission prononce la clôture de la procédure. / Dans le cas contraire, la formation restreinte de la commission peut prononcer, après une procédure contradictoire, les sanctions suivantes : / 1° Un avertissement ; / 2° Une sanction pécuniaire, dans les conditions prévues à l'article 47, à l'exception des cas où le traitement est mis en oeuvre par l'Etat ; / 3° Une injonction de cesser le traitement, lorsque celui-ci relève de l'article 22, ou un retrait de l'autorisation accordée en application de l'article 25. / Lorsque le manquement constaté ne peut faire l'objet d'une mise en conformité dans le cadre d'une mise en demeure, la formation restreinte peut prononcer, sans mise en demeure préalable et après une procédure contradictoire, les sanctions prévues au présent I (...) ".<br/>
<br/>
              3. L'auteur d'une plainte peut déférer au juge de l'excès de pouvoir le refus de la CNIL d'engager à l'encontre de la personne visée par la plainte une procédure sur le fondement du I de l'article 45 de la loi du 6 janvier 1978 précité, y compris lorsque la CNIL procède à des mesures d'instruction ou constate l'existence d'un manquement aux dispositions de cette loi. Il appartient au juge de censurer ce refus en cas d'erreur de fait ou de droit, d'erreur manifeste d'appréciation ou de détournement de pouvoir. En revanche, lorsque la CNIL a décidé d'engager une procédure sur le fondement de l'article 45 précité, l'auteur de la plainte n'a intérêt à contester ni la décision prise à l'issue de cette procédure, quel qu'en soit le dispositif, ni le sort réservé à sa plainte à l'issue de cette procédure. Il est toutefois recevable à déférer, dans tous les cas, au juge de l'excès de pouvoir le défaut d'information par la CNIL des suites données à sa plainte.<br/>
<br/>
              4. Il ressort des termes du courrier adressé le 6 juillet 2017 par la présidente de la CNIL au requérant, que la CNIL a caractérisé un manquement du CHNO aux obligations posées par les dispositions de l'article 8 de la loi du 6 janvier 1978, citées au point 2, en constatant que si cet établissement pouvait conserver des informations relatives à l'orientation sexuelle réelle ou supposée du requérant dans un dossier relatif aux procédures judiciaires en cours, c'est à la condition d'avoir souscrit auprès de la CNIL la déclaration nécessaire au traitement de telles données et qu'en l'espèce, le CHNO n'avait pas respecté cette obligation déclarative. La CNIL a, ensuite, invité ce dernier à procéder à une telle déclaration et à effacer les données relatives au requérant dès la fin des procédures judiciaires pour le besoin desquelles elles étaient conservées. En estimant qu'il convenait seulement d'adresser au CHNO un rappel à la loi sans le mettre en demeure de faire cesser les manquements constatés sur le fondement du I de l'article 45 de la loi du 6 janvier 1978 précité, la CNIL n'a pas commis d'erreur manifeste d'appréciation dans la mise en oeuvre des compétences qui lui sont conférées par les dispositions de l'article 11 de la même loi citées au point 2. <br/>
<br/>
              5. Il résulte de ce qui précède, sans qu'il soit besoin de statuer sur la fin de non-recevoir soulevée par la CNIL, que le requérant n'est pas fondé à demander l'annulation de la décision du 6 juillet 2017 par laquelle la CNIL a clôturé sa plainte.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : La requête de M. A...est rejetée.  <br/>
Article 2 : La présente décision sera notifiée à M. B...A...et à la Commission nationale de l'informatique et des libertés.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
