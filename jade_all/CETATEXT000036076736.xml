<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036076736</ID>
<ANCIEN_ID>JG_L_2017_11_000000410002</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/07/67/CETATEXT000036076736.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère chambre, 22/11/2017, 410002, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-11-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>410002</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>DELAMARRE ; SCP ZRIBI, TEXIER</AVOCATS>
<RAPPORTEUR>Mme Sabine Monchambert</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Rémi Decout-Paolini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2017:410002.20171122</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme A...B...a demandé au tribunal administratif de Melun d'annuler la décision du 4 juillet 2014 du président du conseil général de Seine-et-Marne en tant qu'elle a implicitement rejeté sa demande de rétablissement au revenu de solidarité active et a expressément confirmé les indus d'allocation mis à sa charge pour la période d'octobre 2009 à septembre 2011 à hauteur d'un montant de 14 891,99 euros et pour la période du 1er octobre 2011 au 30 mai 2013 à hauteur de 1 272 euros et subsidiairement, d'annuler cette décision en tant qu'elle a rejeté sa demande de remise gracieuse des indus. Par un jugement n° 1502282 du 11 octobre 2016, le tribunal administratif de Melun a rejeté ces demandes.<br/>
<br/>
              Par un pourvoi et un mémoire, enregistrés les 24 avril et 7 juin 2017 au secrétariat du contentieux du Conseil d'Etat, Mme B...demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande ;<br/>
<br/>
              3°) de mettre à la charge du département de Seine-et-Marne la somme de 3 000 euros à verser à Me Delamarre, avocat de MmeB..., au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991.<br/>
<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'action sociale et des familles ; <br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Sabine Monchambert, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Rémi Decout-Paolini, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Delamarre, avocat de MmeB..., et à la SCP Zribi, Texier, avocat du département de Seine-et-Marne.<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis au juge du fond que la caisse d'allocations familiales de Seine-et-Marne a, par deux courriers des 6 octobre 2011 et 28 janvier 2014, notifié à Mme B...des trop-perçus de revenu de solidarité active d'un montant de 14 891, 99 euros pour la période du 1er octobre 2009 au 30 septembre 2011 et d'un montant de 1 272 euros pour la période du 1er octobre 2011 au 30 mai 2013. Par le jugement attaqué, le tribunal administratif de Melun a rejeté la demande de l'intéressée tendant à l'annulation de la décision du 4 juillet 2014 par laquelle le président du conseil général de Seine-et-Marne a rejeté son recours administratif obligatoire contre les décisions de la caisse d'allocations familiales et implicitement rejeté sa demande de rétablissement dans ses droits.<br/>
<br/>
              2. Aux termes de l'article R. 772-5 du code de justice administrative : " Sont présentées, instruites et jugées selon les dispositions du présent code, sous réserve des dispositions du présent chapitre, les requêtes relatives aux prestations, allocations ou droits attribués au titre de l'aide ou de l'action sociale, du logement ou en faveur des travailleurs privés d'emploi (...) ". Aux termes de l'article R. 772-9 du même code : " La procédure contradictoire peut être poursuivie à l'audience sur les éléments de fait qui conditionnent l'attribution de la prestation ou de l'allocation ou la reconnaissance du droit, objet de la requête. / L'instruction est close soit après que les parties ou leurs mandataires ont formulé leurs observations orales, soit, si ces parties sont absentes ou ne sont pas représentées, après appel de leur affaire à l'audience. Toutefois, afin de permettre aux parties de verser des pièces complémentaires, le juge peut décider de différer la clôture de l'instruction à une date postérieure dont il les avise par tous moyens. / L'instruction fait l'objet d'une réouverture en cas de renvoi à une autre audience ".<br/>
<br/>
              3. L'article R. 772-9 du code de justice administrative, qui déroge aux règles de droit commun de la procédure administrative contentieuse, tend, eu égard aux spécificités de l'office du juge en matière de contentieux sociaux, à assouplir les contraintes de la procédure écrite en ouvrant la possibilité à ce juge de poursuivre à l'audience la procédure contradictoire sur des éléments de fait et en décalant la clôture de l'instruction, laquelle est entièrement régie par les dispositions de son deuxième alinéa. Dès lors, les règles fixées par l'article R. 613-2 du même code, selon lesquelles l'instruction est close à la date fixée par une ordonnance de clôture ou, à défaut, trois jours francs avant la date de l'audience, ne sont pas applicables aux contentieux sociaux régis par les articles R. 772-5 et suivants du code de justice administrative. Il suit de là que le tribunal doit, pour juger les requêtes régies par ces articles, prendre en considération tant les éléments de fait invoqués oralement à l'audience qui conditionnent l'attribution de la prestation ou de l'allocation ou la reconnaissance du droit, objet de la requête, que tous les mémoires enregistrés jusqu'à la clôture de l'instruction, qui intervient, sous réserve de la décision du juge de la différer, après que les parties ou leurs mandataires ont formulé leurs observations orales ou, si ces parties sont absentes ou ne sont pas représentées, après appel de leur affaire à l'audience. <br/>
<br/>
              4. Si le jugement attaqué mentionne à tort que le mémoire en réplique présenté par Mme B...le 27 septembre 2016, soit la veille de l'audience, a été déposé " postérieurement à la clôture de l'instruction ", il ressort toutefois des termes de ce mémoire qu'il ne comportait aucun élément ou moyen nouveau auquel il n'aurait pas été répondu dans les motifs du jugement. Par suite, la requérante n'est pas fondée à soutenir qu'en l'espèce, le premier juge aurait, en ne prenant pas en compte ce mémoire, entaché son jugement d'irrégularité.<br/>
<br/>
              5. Il résulte de ce qui précède que Mme B...n'est pas fondée à demander l'annulation du jugement qu'elle attaque. Ses conclusions tendant au bénéfice des dispositions de l'article L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 ne peuvent, par voie de conséquence, qu'être rejetées.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de Mme B...est rejeté. <br/>
Article 2 : La présente décision sera notifiée à Mme A...B...et au département de Seine-et-Marne.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
