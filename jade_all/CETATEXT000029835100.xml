<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029835100</ID>
<ANCIEN_ID>JG_L_2014_12_000000364823</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/83/51/CETATEXT000029835100.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème et 8ème sous-sections réunies, 03/12/2014, 364823, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-12-03</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>364823</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème et 8ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>COPPER-ROYER</AVOCATS>
<RAPPORTEUR>Mme Angélique Delorme</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emmanuelle Cortot-Boucher</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:364823.20141203</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête sommaire et le mémoire complémentaire, enregistrés les 27 décembre 2012 et 19 mars 2013 au secrétariat du contentieux du Conseil d'Etat, présentés pour Me B...A..., agissant en qualité de liquidateur de la société Garnier Choiseul Holding dont le siège est 129/137, Boulevard Carnot à Le Vésinet (78110) ; Me A...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 11PA02093 du 9 novembre 2012 par lequel la cour administrative d'appel de Paris a rejeté l'appel de la société Garnier Choiseul Holding, venant aux droits de la société Etablissements Bellaby contre le jugement n° 0804734 du 16 mars 2011 par lequel le tribunal administratif de Paris avait rejeté la demande de la société Etablissements Bellaby, venant aux droits de la société Loriene, tendant à la décharge des rappels de taxe sur la valeur ajoutée, ainsi que des pénalités correspondantes, auxquels a été assujetti la société Loriene au titre de la période du 1er janvier 2000 au 31 décembre 2001 ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code général des impôts et le livre des procédures fiscales ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Angélique Delorme, auditeur,  <br/>
<br/>
              - les conclusions de Mme Emmanuelle Cortot-Boucher, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Copper-Royer, avocat de Me A...;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond qu'à l'issue d'une vérification de comptabilité de la société Loriene portant sur la période du 1er janvier 2000 au 31 décembre 2001, l'administration a remis en cause la déduction de la taxe sur la valeur ajoutée ayant grevé des honoraires facturés par sa société mère, la société Loriene Holding ; que, par un jugement du 16 mars 2011, le tribunal administratif de Paris a rejeté la demande de la société Etablissements Bellaby, venant aux droits de la société Loriene, tendant à la décharge des droits et pénalités procédant de ce redressement ; que MeA..., agissant en qualité de liquidateur de la société Garnier Choiseul Holding, se pourvoit en cassation contre l'arrêt du 9 novembre 2012 par lequel la cour administrative d'appel de Paris a rejeté l'appel que la société Garnier Choiseul Holding, venant aux droits de la société Etablissements Bellaby, a interjeté de ce jugement ;<br/>
<br/>
              2. Considérant qu'en vertu des dispositions combinées des articles 271, 272 et 283 du code général des impôts et de l'article 230 de l'annexe II à ce code, dans leur rédaction applicable au litige, un contribuable n'est pas en droit de déduire de la taxe sur la valeur ajoutée dont il est redevable à raison de ses propres opérations la taxe mentionnée sur une facture établie à son nom par une personne qui ne lui a fourni aucun bien ou aucune prestation de services ; que dans le cas où l'auteur de la facture était régulièrement inscrit au registre du commerce et des sociétés et assujetti à la taxe sur la valeur ajoutée, il appartient à l'administration, si elle entend refuser à celui qui a reçu la facture le droit de déduire la taxe qui y était mentionnée, d'établir qu'il s'agissait d'une facture fictive ou d'une facture de complaisance ; que si l'administration apporte des éléments suffisants permettant de penser que la facture ne correspond pas à une opération réelle, il appartient alors au contribuable d'apporter toutes justifications utiles sur la réalité de cette opération ;  <br/>
<br/>
              3. Considérant qu'il ressort des motifs de l'arrêt attaqué que, pour juger que la société Loriene n'était pas en droit de déduire la taxe sur la valeur ajoutée en litige, la cour s'est bornée à faire sienne l'affirmation de l'administration selon laquelle l'accomplissement des prestations par la société mère ne serait pas établi ; qu'en mettant ainsi à la charge de la société vérifiée la justification de la réalité des opérations, alors qu'il appartenait à l'administration d'apporter préalablement des éléments suffisants pour permettre de penser que les factures d'honoraires de la société Loriene Holding  ne correspondaient pas à des opérations réelles et que ne pouvaient être regardés comme des éléments suffisants les allégations, dont faisait également état l'administration, selon lesquelles la société mère serait une société " holding pure de gestion " et que les prestations auraient pu être assurées par la société vérifiée elle-même,  la cour a commis une erreur de droit ; que Me A...est fondé, pour ce motif et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, à demander l'annulation de l'arrêt qu'il attaque ;<br/>
<br/>
              4. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat le versement à Me A...de la somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 9 novembre 2012 de la cour administrative d'appel de Paris est annulé. <br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Paris. <br/>
Article 3 : L'Etat versera à Me A...la somme de 3 000 euros au titre de l'article L.  761-1 du code de justice administrative. <br/>
Article 4 : La présente décision sera notifiée à Me B...A...et au ministre de des finances et des comptes publics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
