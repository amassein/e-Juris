<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037802629</ID>
<ANCIEN_ID>JG_L_2018_11_000000418884</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/80/26/CETATEXT000037802629.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère chambre, 28/11/2018, 418884, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-11-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>418884</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROUSSEAU, TAPIE</AVOCATS>
<RAPPORTEUR>M. Jean-Luc Nevache</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Charles Touboul</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:418884.20181128</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme B...A...a formé opposition, devant le tribunal administratif de Châlons-en-Champagne, à la contrainte qui lui a été délivrée le 9 août 2017 par le directeur de l'agence de Pôle emploi de Troyes Rolland afin d'obtenir le remboursement d'une somme de 13 353,40 euros correspondant à un indu d'allocation de solidarité spécifique pour la période du 1er janvier 2015 au 31 mars 2017. Par une ordonnance n° 1701958 du 20 novembre 2017, le président de la 3e chambre du tribunal administratif de Châlons-en-Champagne a rejeté son opposition. <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 9 mars et 7 juin 2018 au secrétariat du contentieux du Conseil d'Etat, Mme A...demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cette ordonnance ; <br/>
<br/>
              2°) de mettre à la charge de Pôle emploi la somme de 3 000 euros à verser à la SCP Rousseau, Tapie, son avocat, au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code du travail ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - le décret n° 91-1266 du 19 décembre 1991 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Luc Nevache, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Charles Touboul, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Rousseau, Tapie, avocat de MmeA.ou par lettre recommandée avec demande d'avis de réception adressée au secrétariat dudit tribunal dans les quinze jours à compter de la notification<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. D'une part, aux termes de l'article L. 5426-8-2 du code du travail : " Pour le remboursement des allocations, aides, ainsi que de toute autre prestation indûment versées par Pôle emploi pour son propre compte, pour le compte de l'organisme chargé de la gestion du régime d'assurance chômage mentionné à l'article L. 5427-1, pour le compte de l'Etat ou des employeurs mentionnés à l'article L. 5424-1, le directeur général de Pôle emploi ou la personne qu'il désigne en son sein peut, dans les délais et selon les conditions fixés par voie réglementaire, et après mise en demeure, délivrer une contrainte qui, à défaut d'opposition du débiteur devant la juridiction compétente, comporte tous les effets d'un jugement et confère le bénéfice de l'hypothèque judiciaire ". L'article R. 5426-22 du même code précise que : " Le débiteur peut former opposition par inscription au secrétariat du tribunal compétent dans le ressort duquel il est domicilié... ". <br/>
<br/>
              2. D'autre part, aux termes de l'article 38 du décret du 19 décembre 1991 portant application de la loi n° 91-647 du 10 juillet 1991 relative à l'aide juridique, applicable aux tribunaux administratifs : " Lorsqu'une action en justice ou un recours doit être intenté avant l'expiration d'un délai devant les juridictions de première instance ou d'appel, l'action ou le recours est réputé avoir été intenté dans le délai si la demande d'aide juridictionnelle s'y rapportant est adressée au bureau d'aide juridictionnelle avant l'expiration dudit délai et si la demande en justice ou le recours est introduit dans un nouveau délai de même durée à compter : (...) / c) De la date à laquelle le demandeur à l'aide juridictionnelle ne peut plus contester la décision d'admission ou de rejet de sa demande en application du premier alinéa de l'article 56 et de l'article 160 ou, en cas de recours de ce demandeur, de la date à laquelle la décision relative à ce recours lui a été notifiée ; / d) Ou, en cas d'admission, de la date, si elle est plus tardive, à laquelle un auxiliaire de justice a été désigné (...) ". <br/>
<br/>
              3. Il ressort des pièces du dossier soumis au juge du fond que Pôle emploi a délivré à MmeA..., le 9 août 2017, une contrainte pour le recouvrement d'une somme de 13 353,40 euros qui lui aurait été indûment versée au titre de l'allocation de solidarité spécifique et que cette contrainte lui a été signifiée par acte d'huissier de justice le 22 août 2017. Mme A...a saisi le bureau d'aide juridictionnelle près le tribunal de grande instance de Troyes, le 31 août 2017, d'une demande d'aide juridictionnelle à laquelle il a été fait droit par une décision du 29 septembre 2017. Dès lors, la demande d'aide juridictionnelle de Mme A...avait interrompu le délai durant lequel elle pouvait former opposition à la contrainte qui lui avait été délivrée et le président de la troisième chambre du tribunal administratif de Châlons-en-Champagne a commis une erreur de droit en jugeant tardive l'opposition à contrainte qu'elle a formée le 11 octobre 2017 devant cette juridiction.<br/>
<br/>
              4. Il résulte de ce qui précède que Mme A...est fondée à demander l'annulation de l'ordonnance qu'elle attaque.<br/>
<br/>
              5. Il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées sur le fondement des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 par la SCP Rousseau, Tapie, avocat de Mme A.ou par lettre recommandée avec demande d'avis de réception adressée au secrétariat dudit tribunal dans les quinze jours à compter de la notification <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'ordonnance du président de la 3e chambre du tribunal administratif de Châlons-en-Champagne du 20 novembre 2017 est annulée.<br/>
Article 2 : L'affaire est renvoyée au tribunal administratif de Châlons-en-Champagne.<br/>
Article 3 : Les conclusions de la SCP Rousseau, Tapie présentées au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 sont rejetées.<br/>
Article 4 : La présente décision sera notifiée à Mme B...A...et à Pôle emploi. <br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
