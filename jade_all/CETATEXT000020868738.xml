<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000020868738</ID>
<ANCIEN_ID>JG_L_2009_05_000000292564</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/20/86/87/CETATEXT000020868738.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème et 9ème sous-sections réunies, 15/05/2009, 292564</TITRE>
<DATE_DEC>2009-05-15</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>292564</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème et 9ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Daël</PRESIDENT>
<AVOCATS>SCP GATINEAU, FATTACCINI ; FOUSSARD</AVOCATS>
<RAPPORTEUR>M. Brice  Bohuon</RAPPORTEUR>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CESSR:2009:292564.20090515</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 18 avril et 20 juin 2006 au secrétariat du contentieux du Conseil d'Etat, présentés pour Mlle Bernadette A demeurant chez Mlle Aziza A ... ; Mlle A demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler la décision en date 17 février 2006 par laquelle la Commission des recours des réfugiés a refusé de lui reconnaître la qualité de réfugiée ; <br/>
<br/>
              2°) réglant l'affaire au fond, de lui reconnaître la qualité de réfugié ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat une somme de 2 500 euros en application des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
                          Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la convention de Genève du 28 juillet 1951 et le protocole signé à New-York le 21 janvier 1967 ;<br/>
<br/>
              Vu le code de l'entrée et du séjour des étrangers et du droit d'asile, notamment son livre VII ;<br/>
<br/>
                          Vu le décret n° 2004-814 du 14 août 2004 ;<br/>
<br/>
                          Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Brice Bohuon, Auditeur,  <br/>
<br/>
              - les observations de la SCP Gatineau, Fattaccini, avocat de Mlle Bernadette A et de Me Foussard, avocat de l'Office français de protection des réfugiés et apatrides, <br/>
<br/>
              - les conclusions de Mme Julie Burguburu, Rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Gatineau, Fattaccini, avocat de Mlle Bernadette A et à Me Foussard, avocat de l'Office français de protection des réfugiés et apatrides ;<br/>
<br/>
<br/>
<br/>
<br/>
              Sans qu'il soit besoin d'examiner les autres moyens du pourvoi ;<br/>
<br/>
              Considérant qu'aux termes du paragraphe 2 du A de l'article 1er de la convention de Genève du 28 juillet 1951 sur le statut des réfugiés, modifié par le paragraphe 2 de l'article 1er du protocole signé le 31 janvier 1967 à New-York, la qualité de réfugié est reconnue à " toute personne qui, craignant avec raison d'être persécutée du fait de sa race, de sa religion, de sa nationalité ou de son appartenance à un certain groupe social ou de ses opinions politiques, se trouve hors du pays dont elle a la nationalité et qui ne peut ou, du fait de cette crainte, ne veut se réclamer de la protection de ce pays " ; qu'aux termes des articles L. 712-1 et L. 712-3 du code de l'entrée et du séjour des étrangers et du droit d'asile : " Sous réserve des dispositions de l'article L. 712-2, le bénéfice de la protection subsidiaire est accordé à toute personne qui ne remplit pas les conditions pour se voir connaître la qualité de réfugié mentionnées à l'article L. 711-1 et qui établit qu'elle est exposée dans son pays à l'une des menaces graves suivantes : a) La peine de mort ; b) La torture ou des peines ou traitements inhumains et dégradants ; c) S'agissant d'un civil, une menace grave, directe ou individuelle contre sa vie ou sa personne en raison d'une violence généralisée résultant d'une situation de conflit armé interne ou international (...). Le bénéfice de la protection subsidiaire est accordé pour une période d'un an renouvelable. Le renouvellement peut être refusé à chaque échéance lorsque les circonstances ayant justifié l'octroi de la protection ont cessé d'exister ou ont connu un changement suffisamment profond pour que celle-ci ne soit plus requise (...) " ; qu'aux termes de l'article L. 713-2 du même code : " Les persécutions prises en compte dans l'octroi de la qualité de réfugié et les menaces graves pouvant donner lieu au bénéfice de la protection subsidiaire peuvent être le fait des autorités de l'Etat, de partis ou d'organisations qui contrôlent l'Etat ou une partie substantielle du territoire de l'Etat, ou d'acteurs non étatiques dans les cas où les autorités définies à l'alinéa suivant refusent ou ne sont pas en mesure d'offrir une protection. / Les autorités susceptibles d'offrir une protection peuvent être les autorités de l'Etat et des organisations internationales et régionales " ; <br/>
<br/>
              Considérant que pour rejeter le recours de Mlle A contre la décision du directeur de l'Office français de protection des réfugiés et apatrides en date du 19 juillet 2002 refusant de lui reconnaître la qualité de réfugiée, la commission a estimé que les circonstances ayant provoqué le départ d'Irak de l'intéressée, non plus qu'aucune circonstance ultérieure, ne se rattachaient à l'un des motifs prévus par la convention de Genève, mais qu'en revanche, en raison de l'appartenance de Mlle A à la communauté assyro-chaldéenne, de sa situation de femme isolée et de son aisance supposée, elle pouvait prétendre à l'octroi de la protection subsidiaire sur le fondement des dispositions du c) de l'article L. 712-1 précité ; que les menaces graves, directes et individuelles énoncées à ce c) doivent être regardées comme ne concernant que des civils, quels que soient leur origine, leur statut social ou leur conviction, lorsque le degré de violence atteint par un conflit dans un pays permet d'établir qu'ils ont des raisons sérieuses de croire que le retour dans leur pays leur ferait courir le risque d'être exposés aux menaces susmentionnées, en particulier de la part de groupes armés ou d'éléments incontrôlés de la population ; que si la commission, après avoir souverainement constaté le climat de violence généralisée qui prévalait en Irak, pouvait tenir compte de l'ensemble des éléments caractérisant la situation de Mlle A, pour lui accorder la protection subsidiaire, elle n'a, toutefois, pas tiré les conséquences légales de ses propres constatations et a commis une erreur de droit en lui déniant la qualité de réfugié après avoir relevé par un motif non surabondant que les menaces dont elle était susceptible de faire l'objet trouvaient leur origine dans son appartenance à la communauté assyro-chaldéenne, sans indiquer en quoi n'étaient pas satisfaites les autres conditions auxquelles est subordonné le bénéfice de la protection conventionnelle ; que, dès lors, Mlle A est fondée à demander l'annulation de la décision attaquée de la Commission des recours des réfugiés refusant de lui reconnaître la qualité de réfugiée ;<br/>
<br/>
              Sur les conclusions de Mlle A tendant à l'application des dispositions de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              Considérant qu'il y a lieu, dans les circonstances de l'espèce, de faire application de ces dispositions et mettre à la charge de l'Office français de protection des réfugiés et apatrides le versement à Mlle A de la somme de 2 500 euros au titre des frais exposés par elle et non compris dans les dépens ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La décision en date du 17 février 2006 de la Commission des recours des réfugiés refusant de reconnaître à Mlle A la qualité de réfugiée est annulée.<br/>
<br/>
Article 2 : L'affaire est renvoyée devant la Cour nationale du droit d'asile.<br/>
Article 3 : L'Office français de protection des réfugiés et apatrides versera à Mlle A une somme de 2 500 euros en application des dispositions de l'article L. 761-1 du code de justice administrative. <br/>
Article 4 : La présente décision sera notifiée à Mlle Bernadette A, à l'Office français de protection des refugiés et apatrides, à la Cour nationale du droit d'asile et au ministre de l'immigration, de l'intégration, de l'identité nationale et du développement solidaire.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">335-05-01 ÉTRANGERS. RÉFUGIÉS ET APATRIDES. QUALITÉ DE RÉFUGIÉ OU D'APATRIDE. - RECONNAISSANCE - 1) EXISTENCE D'UN CLIMAT DE VIOLENCE GÉNÉRALISÉ (C DE L'ARTICLE L. 712-1 DU CESEDA) - APPRÉCIATION SOUVERAINE DES JUGES DU FOND - CONSÉQUENCE - CONTRÔLE DE CASSATION - DÉNATURATION - 2) OCTROI DE LA PROTECTION SUBSIDIAIRE PAR UN MOTIF QUI DEVAIT CONDUIRE À LA RECONNAISSANCE DE LA QUALITÉ DE RÉFUGIÉ - ERREUR DE DROIT EN CONSÉQUENCE À AVOIR REFUSÉ LE STATUT DE RÉFUGIÉ EN RAISON D'UNE MÉCONNAISSANCE DU CARACTÈRE SUBSIDIAIRE DE LA PROTECTION SUBSIDIAIRE (ART. L. 712-1 ET L. 712-3 DU CESEDA).
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-08-02-02-01-03 PROCÉDURE. VOIES DE RECOURS. CASSATION. CONTRÔLE DU JUGE DE CASSATION. RÉGULARITÉ INTERNE. APPRÉCIATION SOUVERAINE DES JUGES DU FOND. - CONTENTIEUX DES RÉFUGIÉS - APPRÉCIATION DE L'EXISTENCE D'UN CLIMAT DE VIOLENCE GÉNÉRALISÉ (C DE L'ARTICLE L. 712-1 DU CESEDA).
</SCT>
<ANA ID="9A"> 335-05-01 1) L'appréciation de l'existence d'un climat de violence généralisé, visé au c de l'article L. 712-1 du code de l'entrée et du séjour des étrangers et du droit d'asile (CESEDA), est souveraine. Le contrôle du juge de cassation relève de la dénaturation. 2) La commission de recours des réfugiés commet une erreur de droit quand elle refuse la reconnaissance du statut de réfugié mais accorde la protection subsidiaire par un motif reposant sur des menaces à raison de l'appartenance de l'intéressée à une communauté, qui devrait conduire à l'octroi de la qualité de réfugié. Elle méconnaît, ce faisant, le caractère subsidiaire de la protection subsidiaire, prévue aux articles L. 712-1 et L. 712-3 du CESEDA.</ANA>
<ANA ID="9B"> 54-08-02-02-01-03 L'appréciation de l'existence d'un climat de violence généralisé, visé au c de l'article L. 712-1 du code de l'entrée et du séjour des étrangers et du droit d'asile (CESEDA), est souveraine. Le contrôle du juge de cassation relève de la dénaturation.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
