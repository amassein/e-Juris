<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039442438</ID>
<ANCIEN_ID>JG_L_2019_12_000000430898</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/44/24/CETATEXT000039442438.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème chambre, 02/12/2019, 430898, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-12-02</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>430898</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Dominique Bertinotti</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Sophie Roussel</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2019:430898.20191202</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés les 20 mai et 24 octobre 2019 au secrétariat du contentieux du Conseil d'Etat, M. B... A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision du 22 mars 2019 par laquelle le ministre de l'intérieur a refusé de modifier le décret du 6 juillet 2018 lui accordant la nationalité française pour y porter le nom de ses enfants, Sandrine Said Mohamed A... et Natidja Said Mohamed A... ;<br/>
<br/>
              2°) d'enjoindre au ministre de l'intérieur de réexaminer la situation et de modifier le décret ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code civil ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Dominique Bertinotti, conseiller d'Etat,<br/>
<br/>
              - les conclusions de Mme Sophie Roussel, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1.	Aux termes de l'article 22-1 du code civil : " L'enfant mineur dont l'un des deux parents acquiert la nationalité française, devient français de plein droit s'il a la même résidence habituelle que ce parent ou s'il réside alternativement avec ce parent dans le cas de séparation ou divorce. / Les dispositions du présent article ne sont applicables à l'enfant d'une personne qui acquiert la nationalité française par décision de l'autorité publique ou par déclaration de nationalité que si son nom est mentionné dans le décret ou dans la déclaration ". Un enfant ne peut devenir français de plein droit par l'effet du décret qui confère la nationalité française à l'un de ses parents que s'il est mineur. En l'absence de prescription en disposant autrement, cette condition d'âge s'apprécie à la date de signature des décrets pris sur son fondement.<br/>
<br/>
              2.	Il résulte de ces dispositions qu'un enfant mineur ne peut devenir français de plein droit par l'effet du décret qui confère la nationalité française à l'un de ses parents qu'à condition, d'une part, que ce parent ait porté son existence, sauf impossibilité ou force majeure, à la connaissance de l'administration chargée d'instruire la demande préalablement à la signature du décret et, d'autre part, qu'il ait, à la date du décret, résidé avec ce parent de manière stable et durable sous réserve, le cas échéant, d'une résidence en alternance avec l'autre parent en cas de séparation ou de divorce.<br/>
<br/>
              3.	M. A... a acquis la nationalité française par l'effet d'un décret du 6 juillet 2018. Le 30 septembre 2018, il a demandé au ministre de l'intérieur de modifier ce décret pour y porter mention du nom de ses enfants, Sandrine A... Said Mohamed et Natidja A... Said Mohamed, afin de leur faire bénéficier de la nationalité française en vertu de l'effet collectif attaché à sa naturalisation. Par une décision du 22 mars 2019, le ministre de l'intérieur a rejeté la demande de M. A... au motif qu'à la date où est intervenu le décret lui accordant la nationalité française, ses deux filles ne résidaient pas habituellement avec lui. M. A... demande l'annulation pour excès de pouvoir de cette décision.  <br/>
<br/>
              4.	Il ressort des pièces du dossier et n'est pas contesté qu'à la date du décret du 6 juillet 2018, les filles mineures de M. A... résidaient chez leur oncle à Villefontaine (Isère) où elles étaient scolarisées. Si M. A... déclare avoir toujours résidé chez son frère et s'occuper seul de ses enfants, il n'apporte aucun élément de nature à établir qu'il aurait, à la date du décret attaqué, résidé habituellement à Villefontaine, alors qu'il est constant qu'entre le 21 juin 2017 et le 31 juin 2017 puis entre le 26 février 2018 et le 9 mars 2018, il résidait à Sainte Geneviève-des-Bois (Essonne) pour effectuer une formation à Paris. En outre, il ressort des pièces du dossier que son passeport délivré le 25 juillet 2018, ainsi que sa déclaration d'impôt sur le revenu pour 2018 mentionnent Sainte-Geneviève-des-Bois comme commune de résidence. Au surplus, si M. A... produit une attestation de paiement du directeur de la caisse d'allocation familiale de l'Isère pour le mois de décembre 2018, cette circonstance, postérieure au décret du 6 juillet 2018 lui ayant accordé la nationalité française, est sans influence sur la légalité de la décision attaquée. Par suite, les enfants Sandrine A... Said Mohamed et Natidja A... Said Mohamed ne peuvent être regardés, à la date du décret attaqué, comme résidant habituellement chez leur père au sens de dispositions précitées de l'article 22-1 du code civil.<br/>
<br/>
              5.	Il résulte de ce qui précède que M. A... n'est pas fondé à demander l'annulation pour excès de pouvoir de la décision par laquelle le ministre de l'intérieur a refusé de faire droit à sa demande de modification du décret du 6 juillet 2018 pour y porter le nom de ses enfants. Ses conclusions aux fins d'injonction, ainsi que celles présentées au titre de l'article L. 761-1 du code de justice administrative ne peuvent, en conséquence, qu'être rejetées. <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : La requête de M. A... est rejetée.<br/>
<br/>
Article 2 : La présente décision sera notifiée à M. B... A... et au ministre de l'intérieur. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
