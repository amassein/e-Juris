<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043183559</ID>
<ANCIEN_ID>JG_L_2021_02_000000435729</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/18/35/CETATEXT000043183559.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème chambre, 19/02/2021, 435729, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-02-19</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>435729</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BOULLOCHE</AVOCATS>
<RAPPORTEUR>M. François Charmont</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Cécile Barrois de Sarigny</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:435729.20210219</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. C... A... a demandé au tribunal administratif de Cergy-Pontoise, d'une part d'annuler les décisions par lesquelles le ministre de l'intérieur a procédé à des retraits de points de son permis de conduire, en raison d'infractions commises les 20 février 2007, 28 septembre 2008, 13 janvier 2009, 27 mars 2014, 13 juillet 2015, 17 juillet 2015 à 4h45, 6h52, 11h49 et 14h48, 12 août 2015 à 1h33 et 21h50, 21 août 2015 et 7 septembre 2015 et, d'autre part, d'enjoindre au ministre de l'intérieur de lui restituer les points irrégulièrement retirés au titre des infractions commises les 5 juillet 2013, 27 mars 2014, 21 juin 2015, 27 juin 2015, 13 juillet 2015, 17 juillet 2015 à 6h52, 11h49, 14h48 et 4h45, 12 août 2015 à 21h50 et 1h33, 21 août 2015, 7 septembre 2015, 26 septembre 2015, 3 octobre 2015, 4 octobre 2015 et 3 décembre 2015. Par un jugement n° 1702515, 1808627 du 19 septembre 2019, le tribunal administratif a annulé les décisions relatives aux retraits de points opérés à la suite des infractions constatées les 20 février 2007, 28 septembre 2008, 13 janvier 2009, 13 juillet 2015, 17 juillet 2015 à 6h52, 17 juillet 2015 à 11h49 et 17 juillet 2015 à 4h45,  enjoint au ministre de l'intérieur de reconnaître à M. A... le bénéfice des dix-huit points correspondant aux décisions annulées et a rejeté le surplus de ses conclusions.<br/>
<br/>
              Par un pourvoi, enregistré le 4 novembre 2019 au secrétariat du contentieux du Conseil d'Etat, le ministre de l'intérieur demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler ce jugement en tant qu'il fait droit à la demande de M. A... ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter la demande de M. A.... <br/>
<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
              Vu : <br/>
              - le code de la route ;<br/>
              - le code de procédure pénale ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. François Charmont, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme B... D..., rapporteur public.<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Boulloche, avocat de M. A....<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Le ministre de l'intérieur demande l'annulation du jugement du 19 septembre 2019 du tribunal administratif de Cergy-Pontoise en tant qu'il annule les retraits de points opérés sur le permis de conduire de M. A..., consécutifs à des infractions commises les 20 février 2007, 28 septembre 2008, 13 janvier 2009, 13 juillet 2015, 17 juillet 2015 à 6h52, 17 juillet 2015 à 11h49 et 17 juillet 2015 à 4h45 et lui enjoint de restituer à M. A... les dix-huit points correspondants.<br/>
<br/>
              Sur les décisions de retraits de points consécutifs aux infraction des 20 février 2007, 28 septembre 2008 et 13 janvier 2009 :<br/>
<br/>
              2. Le principe de sécurité juridique, qui implique que ne puissent être remises en cause sans condition de délai des situations consolidées par l'effet du temps, fait obstacle à ce que puisse être contestée indéfiniment une décision administrative individuelle qui a été notifiée à son destinataire, ou dont il est établi, à défaut d'une telle notification, que celui-ci a eu connaissance. Dans une telle hypothèse, le destinataire de la décision ne peut exercer de recours juridictionnel au-delà d'un délai raisonnable. En règle générale et sauf circonstances particulières dont se prévaudrait le requérant, ce délai ne saurait, sous réserve de l'exercice de recours administratifs pour lesquels les textes prévoient des délais particuliers, excéder un an à compter de la date à laquelle une décision expresse lui a été notifiée ou de la date à laquelle il est établi qu'il en a eu connaissance. <br/>
<br/>
              3. Il ressort des pièces du dossier soumis aux juges du fond et n'est d'ailleurs pas contesté que les retraits de points concernant les infractions des 20 février 2007, 28 septembre 2008 et 13 janvier 2009 étaient récapitulés sur une décision référencée " 48 SI " du 3 octobre 2009 qui enjoignait à M. A... de restituer son titre de conduite invalidé pour solde de points nuls et que M. A... a procédé à la restitution de ce titre de conduite le 4 décembre 2009. M. A... devant, par suite, être regardé comme ayant eu connaissance au plus tard à cette date de la décision constatant la perte de validité de son titre et des retraits de points qui y étaient récapitulés, le ministre de l'intérieur est fondé à soutenir que la demande d'annulation de ces décisions, introduite 16 mars 2017 devant le tribunal administratif de Cergy-Pontoise, était présentée au-delà du délai raisonnable mentionné au point précédent.<br/>
<br/>
              4. Il résulte de ce qui précède que le ministre de l'intérieur est fondé à soutenir qu'en écartant la fin de non-recevoir qui était soulevée devant lui, sur ce point, le tribunal administratif a entaché son jugement d'une erreur de droit. Il y a lieu, par suite, d'annuler le jugement attaqué en tant qu'il statue sur ces retraits de points.<br/>
<br/>
              Sur les autres décisions de retraits de points :<br/>
<br/>
              5. Il résulte des termes mêmes du jugement attaqué que, pour écarter la fin de non-recevoir soulevée par le ministre de l'intérieur tirée du caractère tardif de la demande de M. A... dirigée contre les décisions de retraits de points consécutives aux infractions des 13 juillet 2015, 17 juillet 2015 à 6h52, 17 juillet 2015 à 11h49 et 17 juillet 2015 à 4h45, le tribunal administratif a jugé que, si ce dernier devait être regardé comme ayant reçu notification, le 1er décembre 2016, date de la présentation à son domicile d'une décision référencée " 48 SI " qui récapitulait ces décisions de retraits de points, cette notification n'avait pas fait courir le délai de recours contentieux, faute pour le ministre d'apporter la preuve que le document envoyé à l'intéressé comportait la mention des voies et délais de recours.<br/>
<br/>
              6. En statuant ainsi, alors que les décisions référencées " 48 SI " constatant la perte de validité du permis de conduire pour solde de points nul, dont l'administration n'est pas en mesure d'éditer des copies, doivent être regardées, sauf preuve contraire, comme conformes au modèle qui sert de base à leur édition automatisée par l'Imprimerie nationale, lequel comporte la mention des voies et délais de recours, le tribunal administratif a entaché sa décision d'une autre erreur de droit.<br/>
<br/>
              7. Si M. A... soutient, en défense, que le délai de recours contentieux a, en tout état de cause, été interrompu par son recours gracieux du 14 mars 2017, l'introduction d'un tel recours après l'expiration du délai de recours contentieux n'est pas de nature à faire obstacle à la tardiveté de sa demande.<br/>
<br/>
              8. Le ministre de l'intérieur est, par suite, fondé à demander l'annulation du jugement qu'il attaque en tant qu'il statue sur ces retraits de points.<br/>
<br/>
              9. Il résulte de tout ce qui précède, notamment de ce qui a été dit aux points 4 et 8, que le ministre de l'intérieur est fondé à demander l'annulation des articles 1er, 2, 3 et 4 du jugement du 19 septembre 2019 du tribunal administratif de Cergy-Pontoise. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soient mises à la charge de l'Etat, qui n'est pas la partie perdante, la somme que demande, à ce titre, M. A....<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Les articles 1er, 2, 3 et 4 du jugement du tribunal administratif de Cergy-Pontoise du 19 septembre 2019 sont annulés.<br/>
Article 2 : L'affaire est renvoyée, dans cette mesure, au tribunal administratif de Cergy-Pontoise.<br/>
Article 3 : Les conclusions présentées par M. A... au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 sont rejetées.<br/>
Article 4 : La présente décision sera notifiée au ministre de l'intérieur et à M. C... A....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
