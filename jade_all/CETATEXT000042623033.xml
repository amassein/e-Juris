<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042623033</ID>
<ANCIEN_ID>JG_L_2020_12_000000446988</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/62/30/CETATEXT000042623033.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 02/12/2020, 446988, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-12-02</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>446988</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2020:446988.20201202</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, enregistrée le 28 novembre 2020 au secrétariat du contentieux du Conseil d'Etat, M. D... A... C... demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative : <br/>
<br/>
              1°) d'ordonner la suspension de l'exécution de la décision du ministre de l'intérieur, révélée par deux " tweets " des 5 et 27 novembre 2020 parus sur le " compte twitter " du ministère de l'intérieur, ainsi que par sa communication sur le site internet de celui-ci intitulée " Attestation de déplacement ", en date du 27 novembre 2020 ; <br/>
<br/>
              2°) d'enjoindre au ministre de l'intérieur, sous astreinte de 1 000 euros par jour de retard, de publier des " visuels et messages similaires à ceux querellés indiquant que ces attestations sont facultatives, ne constituent qu'un modèle et que l'absence de cette attestation officielle ne pourra être sanctionnée pénalement, sous réserve des dispositions prévues par le décret du 29 octobre 2020, modifié par le décret n° 20201454 du 27 novembre 2020 " ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 500 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Il soutient que : <br/>
              - la condition d'urgence est satisfaite eu égard, d'une part, à la situation d'état d'urgence sanitaire et, d'autre part, au caractère immédiat de l'atteinte portée à la liberté d'aller et venir en ce que les dispositions litigieuses sont de nature à faire obstacle aux déplacements autorisés ; <br/>
              - la décision révélée par les tweets et par la communication du ministre de l'intérieur est susceptible de recours ; <br/>
              - il existe un doute sérieux quant à sa légalité ;  <br/>
              - la décision du ministre de l'intérieur est entachée d'incompétence ; <br/>
              - cette décision est manifestement contraire au décret n° 2020-1310, modifié par le décret n° 2020-1454, dont le II du 4 ne prévoit pas que le document permettant de justifier le déplacement considéré entre dans le champ des exceptions prévues par cet article devrait obligatoirement prendre la forme d'un modèle fourni par le gouvernement ; <br/>
              - elle méconnaît la liberté d'aller et venir ainsi que le principe de la présomption d'innocence.<br/>
<br/>
              Vu les autres pièces du dossier ; <br/>
<br/>
              Vu : <br/>
              - la Constitution, et notamment son Préambule ; <br/>
              - le code de la santé publique ; <br/>
              - le décret n° 2020-1310 du 29 octobre 2020 ;<br/>
              - le décret n° 2020-1454 du 27 novembre 2020 ;  <br/>
              - le code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>Considérant que : <br/>
<br/>
              1. Aux termes de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision ". En vertu de l'article L. 522-3 du même code, le juge des référés peut, par une ordonnance motivée, rejeter une requête sans instruction ni audience lorsque la condition d'urgence n'est pas remplie ou lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable ou qu'elle est mal fondée.<br/>
<br/>
              2. Le I de l'article 4 du décret du 29 octobre 2020 prescrivant les mesures générales nécessaires pour faire face à l'épidémie de covid-19 dans le cadre de l'état d'urgence sanitaire, modifié par le décret du 27 novembre 2020, interdit tout déplacement de personne hors de son lieu de résidence à l'exception des déplacements qu'il mentionne. Le premier alinéa du II de cet article prévoit que : " Les personnes souhaitant bénéficier de l'une de ces exceptions doivent se munir, lors de leurs déplacements hors de leur domicile, d'un document leur permettant de justifier que le déplacement considéré entre dans le champ de l'une de ces exceptions. "<br/>
<br/>
              3. M. A... C... soutient que les " tweets " des 5 et 27 novembre 2020 parus sur le " compte tweeter " du ministère de l'intérieur, ainsi que les communications publiées sur le site internet de ce même ministère révèlent une décision du ministre de l'intérieur d'imposer l'utilisation, au titre du justificatif de déplacement prévu par les dispositions citées au point précédent, du modèle d'attestation " officiel " disponible sur le site du ministère de l'intérieur et sur le site service-public.fr qui, selon lui, ajoute illégalement à ces dispositions. Il demande au juge des référés du Conseil d'Etat, sur le fondement de l'article L. 521-1 du code de justice administrative, de suspendre l'exécution de cette décision et de la communication la révélant et d'enjoindre au ministre de l'intérieur d'indiquer que l'utilisation des attestations disponibles sur ces sites est facultative, qu'elles ne constituent qu'un modèle, et que l'absence d'utilisation du modèle " officiel " ne pourra être sanctionnée pénalement, sous réserve que soient respectées les dispositions du décret du 29 octobre 2020. <br/>
<br/>
              4. Toutefois, il ne résulte d'aucune des pièces produites, qui se bornent à rappeler l'obligation de se munir d'un justificatif dans les conditions mentionnées au point 2 et dont plusieurs rappellent que l'attestation de déplacement dérogatoire peut être établie sur papier libre, qu'elles révèleraient une telle décision. La requête présentée par M. A... C... ne peut, par suite, qu'être rejetée, selon la procédure prévue à l'article L. 522-3 du code de justice administrative, y compris les conclusions présentées au titre de l'article L. 761-1 du même code. <br/>
<br/>
<br/>
<br/>			O R D O N N E :<br/>
              			------------------<br/>
<br/>
Article 1er : La requête de M. B... C... est rejetée. <br/>
Article 2 : La présente ordonnance sera notifiée à M. D... B... C.... <br/>
Copie en sera adressée au ministre de l'intérieur et au ministre des solidarités et de la santé.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
