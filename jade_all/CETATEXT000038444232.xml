<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038444232</ID>
<ANCIEN_ID>JG_L_2019_05_000000408517</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/44/42/CETATEXT000038444232.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème et 1ère chambres réunies, 06/05/2019, 408517</TITRE>
<DATE_DEC>2019-05-06</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>408517</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème et 1ère chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LYON-CAEN, THIRIEZ ; SCP SPINOSI, SUREAU</AVOCATS>
<RAPPORTEUR>M. Bruno Bachini</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Dieu</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:408517.20190506</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Procédure contentieuse antérieure<br/>
<br/>
              Le syndicat des chirurgiens-dentistes des Bouches-du-Rhône et le conseil départemental des Bouches-du-Rhône de l'ordre des chirurgiens-dentistes ont porté plainte contre M. B...A...devant la chambre disciplinaire de première instance de Provence-Alpes-Côte d'Azur Corse de l'ordre des chirurgiens-dentistes. Par une décision n° 1423 du 20 juillet 2015, la chambre disciplinaire de première instance a infligé à M. A...la sanction d'interdiction d'exercer la profession de chirurgien-dentiste pendant un mois, dont quinze jours avec sursis.  <br/>
<br/>
              Par une décision n° 2440 du 22 novembre 2016, la chambre disciplinaire nationale de l'ordre des chirurgiens-dentistes, saisie en appel par M. A... contre cette décision, a refusé de transmettre au Conseil d'Etat la question de la conformité aux droits et libertés garantis par la Constitution des articles L. 4142-4, L. 4124-7 et L. 4122-3 du code de la santé publique.<br/>
<br/>
              Par une décision n° 2439/2444 du 29 décembre 2016, la chambre disciplinaire nationale de l'ordre des chirurgiens-dentistes a rejeté l'appel de M. A...et fixé au 1er avril 2017 la date d'effet de la fraction de la sanction prononcée en première instance qui n'était pas assortie du sursis.  <br/>
<br/>
              Procédure devant le Conseil d'Etat<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire, un mémoire en réplique et un nouveau mémoire, enregistrés les 28 février 2017, 13 mars 2017, 2 mars 2018 et 18 septembre 2018 au secrétariat du contentieux du Conseil d'Etat, M. A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette décision ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge du Conseil national de l'ordre des chirurgiens-dentistes et du conseil départemental des Bouches-du-Rhône de l'ordre des chirurgiens-dentistes la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la Constitution, notamment son Préambule et son article 61-1 ;<br/>
              - l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
              - le code de la santé publique ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Bruno Bachini, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Frédéric Dieu, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Spinosi, Sureau, avocat de M. B...A...et à la SCP Lyon-Caen, Thiriez, avocat du Conseil départemental de l'Ordre des chirurgiens-dentistes des Bouches-du-Rhône ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que, par une décision du 20 juillet 2015, la chambre disciplinaire de première instance de Provence-Alpes-Côte d'Azur Corse de l'ordre des chirurgiens-dentistes a, sur la plainte du syndicat des chirurgiens-dentistes des Bouches-du-Rhône et du conseil départemental des Bouches-du-Rhône de l'ordre des chirurgiens-dentistes, infligé à M.A..., chirurgien-dentiste, la sanction d'interdiction d'exercer sa profession pendant un mois, dont quinze jours avec sursis, à raison de faits de publicité commis par un organisme qu'il préside et qui gère un centre de santé dentaire, dans lequel il n'exerce pas. Par une décision du 29 décembre 2016 contre laquelle M. A... se pourvoit en cassation, la chambre disciplinaire nationale de l'ordre des chirurgiens-dentistes a rejeté l'appel qu'il avait formé contre la décision de la chambre disciplinaire de première instance. <br/>
<br/>
              2. Par ailleurs, par une décision du 22 novembre 2016 dont M.A... demande l'annulation par le même pourvoi, la chambre disciplinaire nationale de l'ordre des chirurgiens-dentistes a refusé de transmettre au Conseil d'Etat la question prioritaire de constitutionnalité relative aux dispositions des articles L. 4142-4, L. 4124-7 et L. 4122-3 du code de la santé publique soulevée par M. A...au soutien de son appel.<br/>
<br/>
              Sur le refus de transmission de la question prioritaire de constitutionnalité :<br/>
<br/>
              3. Les dispositions de l'article 23-2 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel prévoient que, lorsqu'une juridiction relevant du Conseil d'Etat est saisie de moyens contestant la conformité d'une disposition législative aux droits et libertés garantis par la Constitution, elle statue sans délai par une décision motivée et transmet au Conseil d'Etat la question de constitutionnalité ainsi posée à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement de circonstances, et qu'elle ne soit pas dépourvue de caractère sérieux. <br/>
<br/>
              4. En premier lieu,  la circonstance que la chambre disciplinaire nationale des chirurgiens-dentistes aurait statué sur la question prioritaire de constitutionnalité soulevée par M. A... dans un délai que le requérant estime excessif n'est pas de nature à justifier l'annulation de sa décision du 22 novembre 2016.<br/>
<br/>
              5. En deuxième lieu, cette décision est, contrairement à ce que soutient le requérant, suffisamment motivée.<br/>
<br/>
              6. En dernier lieu, si M. A...soutenait, à l'appui de sa question prioritaire de constitutionnalité, que les articles L. 4142-4, L. 4124-7 et L. 4122-3 du code de la santé publique sont contraires aux principes d'impartialité et d'indépendance de toute juridiction garantis par l'article 16 de la Déclaration des droits de l'homme et du citoyen de 1789, il ressort de l'ensemble des textes applicables à la chambre disciplinaire nationale de l'ordre des chirurgiens-dentistes, en particulier des articles L. 4122-3 et L. 4126-2 du même code, qu'aucun membre de cette juridiction ne peut siéger lorsqu'il a antérieurement eu connaissance des faits de la cause à raison de l'exercice d'autres fonctions ordinales et que la faculté de récusation est ouverte aux parties. Dès lors, la question soulevée par M. A...ne présentait pas un caractère sérieux. Par suite, ce motif, dont l'examen n'implique l'appréciation d'aucune circonstance de fait, doit être substitué au motif retenu par la décision attaquée refusant de transmettre au Conseil d'Etat la question prioritaire de constitutionnalité, dont il justifie le dispositif. <br/>
<br/>
              7. Il résulte de tout ce qui précède que M. A...n'est pas fondé à demander l'annulation du refus de transmission de sa question prioritaire de constitutionnalité. <br/>
<br/>
              Sur le pourvoi :<br/>
<br/>
              8. Dans sa rédaction applicable à l'espèce, l'article L. 6323-1 du code de la santé publique dispose : " Les centres de santé sont des structures sanitaires de proximité dispensant principalement des soins de premier recours. Ils assurent des activités de soins sans hébergement et mènent des actions de santé publique ainsi que des actions de prévention, d'éducation pour la santé, d'éducation thérapeutique des patients et des actions sociales (...) / Ils sont créés et gérés soit par des organismes à but non lucratif, soit par des collectivités territoriales, soit par des établissements de santé (...) ". A la différence des sociétés d'exercice libéral et des sociétés civiles professionnelles ayant pour objet l'exercice en commun de la profession de chirugien-dentiste, qui, en vertu respectivement des articles R. 4113-4 et R. 4113-28 du même code, ne sont constituées que sous la réserve de leur inscription, en tant que société, au tableau de l'ordre, l'ouverture d'un centre de santé n'est pas subordonnée à son inscription au tableau du ou des ordres auxquels appartiennent les praticiens qui y exercent.<br/>
<br/>
              9. D'une part, il résulte de ce qui précède que les centres de santé ne sont pas soumis aux obligations fixées par les codes de déontologie élaborés, en application des dispositions de l'article L. 4127-1 du code de la santé publique, pour chacune des professions de médecin, chirurgien-dentiste et sage-femme. Par suite, en jugeant, pour prononcer à l'encontre de M. A...la sanction litigieuse, que le centre de santé dont il préside l'organisme gestionnaire avait, en publiant des messages promotionnels, méconnu l'obligation déontologique posée par l'article R. 4127-215 du même code en vertu duquel " la profession dentaire ne doit pas être pratiquée comme un commerce ", la chambre disciplinaire nationale a entaché sa décision d'erreur de droit. <br/>
<br/>
              10. D'autre part, en jugeant que M. A...avait, du seul fait de la publication de ces messages promotionnels, lui-même méconnu l'interdiction posée à l'article R. 4127-215 du code de la santé publique, sans rechercher si des agissements lui étaient personnellement imputables à ce titre, la chambre disciplinaire nationale a également entaché sa décision d'erreur de droit.<br/>
<br/>
              11. Il résulte des motifs mentionnés aux points 9 et 10, qui, contrairement à ce qui est soutenu en défense, ne répondent pas à une argumentation nouvelle en cassation, que M. A... est fondé, sans qu'il soit besoin d'examiner les autres moyens de son pourvoi, à demander l'annulation de la décision qu'il attaque.<br/>
<br/>
              12. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge du conseil départemental des Bouches-du-Rhône de l'ordre des chirurgiens-dentistes la somme de 3 500 euros à verser à M. A...au titre des dispositions de l'article L. 761-1 du code de justice administrative. En revanche, ces dispositions font obstacle à ce qu'une somme soit mise à ce titre à la charge de M. A...qui n'est pas, dans la présente instance, la partie perdante et du Conseil national de l'ordre des chirurgiens-dentistes, qui n'a pas la qualité de partie dans la présente instance.    <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Les conclusions du pourvoi de M. A...dirigées contre la décision de la chambre disciplinaire nationale de l'ordre des chirurgiens-dentistes du 22 novembre 2016 refusant de transmettre sa question prioritaire de constitutionnalité sont rejetées.<br/>
Article 2 : La décision de la chambre disciplinaire nationale de l'ordre des chirurgiens-dentistes du 29 décembre 2016 est annulée.<br/>
Article 3 : L'affaire est renvoyée à la chambre disciplinaire nationale de l'ordre des chirurgiens-dentistes. <br/>
Article 4 : Le conseil départemental des Bouches-du-Rhône de l'ordre des chirurgiens-dentistes versera à M. A...une somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
Article 5 : Le surplus des conclusions du pourvoi de M. A...et les conclusions du conseil départemental des Bouches-du-Rhône de l'ordre des chirurgiens-dentistes présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetés. <br/>
Article 6 : La présente décision sera notifiée à M. B...A..., au syndicat des chirurgiens-dentistes des Bouches-du-Rhône, au conseil départemental des Bouches-du-Rhône de l'ordre des chirurgiens-dentistes, au Conseil national de l'ordre des chirurgiens-dentistes et à la ministre des solidarités et de la santé.<br/>
Copie en sera adressée au Conseil constitutionnel.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-07-01-04-03 PROCÉDURE. POUVOIRS ET DEVOIRS DU JUGE. QUESTIONS GÉNÉRALES. MOYENS. MOYENS INOPÉRANTS. - MOYEN TIRÉ DE CE QUE LE PREMIER JUGE AURAIT STATUÉ SUR UNE QPC DANS UN DÉLAI EXCESSIF.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-10 PROCÉDURE. - MOYEN, SOULEVÉ À L'APPUI D'UN RECOURS CONTRE LA DÉCISION DU PREMIER JUGE, TIRÉ DE CELUI-CI AURAIT STATUÉ SUR LA QPC DANS UN DÉLAI EXCESSIF - MOYEN INOPÉRANT.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">55-03-02 PROFESSIONS, CHARGES ET OFFICES. CONDITIONS D'EXERCICE DES PROFESSIONS. CHIRURGIENS-DENTISTES. - APPLICABILITÉ AUX CENTRES DE SANTÉ DES OBLIGATIONS FIXÉES PAR LES CODES DE DÉONTOLOGIE PRÉVUS À L'ARTICLE L. 4127-1 DU CSP - ABSENCE [RJ1].
</SCT>
<SCT ID="8D" TYPE="PRINCIPAL">55-04-02-01-02 PROFESSIONS, CHARGES ET OFFICES. DISCIPLINE PROFESSIONNELLE. SANCTIONS. FAITS DE NATURE À JUSTIFIER UNE SANCTION. CHIRURGIENS-DENTISTES. - APPLICABILITÉ AUX CENTRES DE SANTÉ DES OBLIGATIONS FIXÉES PAR LES CODES DE DÉONTOLOGIE PRÉVUS À L'ARTICLE L. 4127-1 DU CSP - ABSENCE [RJ1].
</SCT>
<ANA ID="9A"> 54-07-01-04-03 La circonstance que la chambre disciplinaire nationale des chirurgiens-dentistes aurait statué sur la question prioritaire de constitutionnalité (QPC) dans un délai que le requérant estime excessif n'est pas de nature à justifier l'annulation de sa décision.</ANA>
<ANA ID="9B"> 54-10 La circonstance que la chambre disciplinaire nationale des chirurgiens-dentistes aurait statué sur la question prioritaire de constitutionnalité (QPC) dans un délai que le requérant estime excessif n'est pas de nature à justifier l'annulation de sa décision.</ANA>
<ANA ID="9C"> 55-03-02 A la différence des sociétés d'exercice libéral et des sociétés civiles professionnelles ayant pour objet l'exercice en commun de la profession de chirugien-dentiste, qui, en vertu respectivement des articles R. 4113-4 et R. 4113-28 du code de la santé publique (CSP), ne sont constituées que sous la réserve de leur inscription, en tant que société, au tableau de l'ordre, l'ouverture d'un centre de santé n'est pas subordonnée à son inscription au tableau du ou des ordres auxquels appartiennent les praticiens qui y exercent.,,Il en résulte que les centres de santé ne sont pas soumis aux obligations fixées par les codes de déontologie élaborés, en application des dispositions de l'article L. 4127-1 du CSP, pour chacune des professions de médecin, chirurgien-dentiste et sage-femme. Par suite, en jugeant, pour prononcer à l'encontre de l'intéressé la sanction litigieuse, que le centre de santé dont il préside l'organisme gestionnaire avait, en publiant des messages promotionnels, méconnu l'obligation déontologique posée par l'article R. 4127-215 du même code en vertu duquel la profession dentaire ne doit pas être pratiquée comme un commerce, la chambre disciplinaire nationale a entaché sa décision d'erreur de droit.</ANA>
<ANA ID="9D"> 55-04-02-01-02 A la différence des sociétés d'exercice libéral et des sociétés civiles professionnelles ayant pour objet l'exercice en commun de la profession de chirugien-dentiste, qui, en vertu respectivement des articles R. 4113-4 et R. 4113-28 du code de la santé publique (CSP), ne sont constituées que sous la réserve de leur inscription, en tant que société, au tableau de l'ordre, l'ouverture d'un centre de santé n'est pas subordonnée à son inscription au tableau du ou des ordres auxquels appartiennent les praticiens qui y exercent.,,Il en résulte que les centres de santé ne sont pas soumis aux obligations fixées par les codes de déontologie élaborés, en application des dispositions de l'article L. 4127-1 du CSP, pour chacune des professions de médecin, chirurgien-dentiste et sage-femme. Par suite, en jugeant, pour prononcer à l'encontre de l'intéressé la sanction litigieuse, que le centre de santé dont il préside l'organisme gestionnaire avait, en publiant des messages promotionnels, méconnu l'obligation déontologique posée par l'article R. 4127-215 du même code en vertu duquel la profession dentaire ne doit pas être pratiquée comme un commerce, la chambre disciplinaire nationale a entaché sa décision d'erreur de droit.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., Cass. civ. 1ère, 26 avril 2017, n°s 16-14.036 16-15.278, Bull. 2017, I, n° 93.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
