<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043129585</ID>
<ANCIEN_ID>JG_L_2021_02_000000441895</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/12/95/CETATEXT000043129585.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème chambre, 11/02/2021, 441895, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-02-11</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>441895</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP SPINOSI, SUREAU ; SOLTNER</AVOCATS>
<RAPPORTEUR>M. Yves Doutriaux</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Sophie Roussel</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:441895.20210211</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              La société Free Mobile a demandé au juge des référés du tribunal administratif de Montreuil, sur le fondement de l'article L. 521-1 du code de justice administrative, d'une part de suspendre l'exécution de la décision du 12 mars 2020 par laquelle le maire de Montfermeil a sursis à statuer sur sa déclaration préalable d'installation d'une antenne relais de téléphonie mobile sur le territoire de cette commune et, d'autre part, d'enjoindre au maire d'instruire cette déclaration préalable de travaux.<br/>
<br/>
              Par une ordonnance n° 2004901 du 30 juin 2020, le juge des référés du tribunal administratif de Montreuil a rejeté sa demande.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 16 et 30 juillet et 27 novembre 2020 au secrétariat du contentieux du Conseil d'Etat, la société Free mobile demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance ; <br/>
<br/>
              2°) statuant en référé, de faire droit à sa demande ; <br/>
<br/>
              3°) de mettre à la charge de la commune de Montfermeil la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Elle soutient que le juge des référés du tribunal administratif de Montreuil a commis une erreur de droit et dénaturé les pièces du dossier en jugeant que le moyen tiré de l'erreur d'appréciation de la commune dans l'application de l'article L. 424-1 du code de l'urbanisme n'était pas de nature à faire naître un doute sérieux sur la légalité de la décision attaquée.<br/>
<br/>
              Par un mémoire en défense, enregistré le 15 novembre 2020 au secrétariat du contentieux du Conseil d'Etat, la commune de Montfermeil conclut au rejet du pourvoi et à ce que la somme de 4 000 euros soit mise à la charge de la société Free Mobile au titre de l'article L.761-1 du code de justice administrative. Elle soutient que les moyens soulevés ne sont pas fondés.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
               le code de l'urbanisme ; <br/>
               le code de justice administrative et le décret n°2020-1406 du 18 novembre 2020 ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              -	Le rapport de M. Yves Doutriaux, conseiller d'Etat,<br/>
<br/>
              -	Les conclusions de Mme A... B..., rapporteure publique,<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Spinosi, Sureau, avocat de la société Free Mobile et à Me Soltner, avocat de la commune de Montfermeil ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis au juge des référés que la commune de Montfermeil a engagé un projet de réaménagement de son centre-ville. Ce projet a été " pris en considération " par l'établissement public territorial Grand Paris-Grand Est par une délibération du 18 décembre 2019 du conseil de territoire de l'établissement public qui a instauré un " périmètre de prise en considération d'étude " de ce projet. Sur le fondement de cette délibération et du 3° l'article L. 424-1 du code de l'urbanisme, saisi par la société Free Mobile d'une déclaration préalable d'implantation d'une station relais sur le toit d'un immeuble situé          9-11 avenue des Primevères, le maire de Montfermeil y a sursis à statuer par une décision du 12 mars 2020. La société Free mobile se pourvoit en cassation contre l'ordonnance par laquelle le juge des référés du tribunal administratif de Montreuil a rejeté sa demande de suspension de l'exécution de cette décision.<br/>
<br/>
              2. Aux termes de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision ".<br/>
              3. Aux termes de l'article L. 424-1 du code de l'urbanisme, l'autorité compétente pour se prononcer sur une déclaration préalable peut sursoir à statuer " 3° Lorsque des travaux, constructions ou installations sont susceptibles de compromettre ou de rendre plus onéreuse la réalisation d'une opération d'aménagement, dès lors que le projet d'aménagement a été pris en considération par la commune ou l'établissement public de coopération intercommunale compétent et que les terrains affectés par ce projet ont été délimités, sauf pour les zones d'aménagement concerté pour lesquelles l'article L. 311-2 du présent code prévoit qu'il peut être sursis à statuer à compter de la publication de l'acte créant la zone d'aménagement concerté. ".<br/>
<br/>
              4. Pour rejeter la demande dont il était saisi, le juge des référés a considéré que n'était pas sérieux le moyen tiré de ce que le maire, qui avait sursis à statuer sur le fondement des dispositions citées ci-dessus de l'article L. 424-1 du code de l'urbanisme avait commis une erreur d'appréciation en estimant que les travaux étaient susceptibles d'aggraver le coût de réalisation du projet d'aménagement du centre-ville. En statuant ainsi, alors qu'il ressortait des pièces du dossier qui lui était soumis que le projet en question s'étend sur une zone de 45 hectares et se présente sous forme d'objectifs généraux et que la déclaration de travaux portait sur l'installation d'un dispositif de faible ampleur posé sur la toiture d'un immeuble, le juge des référés a dénaturé les faits de l'espèce.   <br/>
<br/>
              5. Il y a lieu, en application de l'article L. 821-2 du code de justice administrative, de régler l'affaire au titre de la procédure de référé engagée.<br/>
<br/>
              6. En premier lieu, eu égard à l'intérêt public qui s'attache à la couverture du territoire national par le réseau de téléphonie mobile tant 3G que 4G et aux intérêts propres de la société Free Mobile qui est soumise à un cahier des charges lui imposant notamment un taux de couverture de la population métropolitaine de 98 % au 1er janvier 2027, et en particulier à la circonstance que le territoire de la commune de Montfermeil n'est que partiellement couvert par les réseaux de téléphonie mobile de la société requérante, la condition d'urgence doit être regardée comme remplie.<br/>
<br/>
              7. En second lieu, est de nature à faire naître un doute sérieux quant à la légalité de la décision contestée le moyen tiré de ce que le maire de Montfermeil a entaché d'une erreur d'appréciation sa décision de sursoir à statuer, sur le fondement de l'article L. 424-1 du code de l'urbanisme, sur la déclaration préalable d'installation d'une station relais.<br/>
<br/>
              8. Il résulte de ce qui précède que la société Free Mobile est fondée à demander la suspension de l'exécution de la décision qu'elle attaque. <br/>
<br/>
              9. Il y a lieu d'enjoindre au maire de Montfermeil de procéder à une nouvelle instruction de la déclaration préalable de travaux présentée par la société Free Mobile dans un délai d'un mois à compter de la notification de la présente décision.<br/>
<br/>
              10. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la commune de Montfermeil le versement à la société Free Mobile de la somme de 3 000 euros au titre des frais exposés devant le Conseil d'Etat et devant le tribunal administratif de Montreuil et non compris dans les dépens. En revanche, les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à la charge de la société Free Mobile, qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>
<br/>			D E C I D E :<br/>
              			--------------<br/>
<br/>
Article 1er : L'ordonnance du juge des référés du tribunal administratif de Montreuil du 30 juin 2020 est annulée.<br/>
<br/>
Article 2 : L'exécution de la décision du 12 mars 2020 par laquelle le maire Montfermeil a sursis à statuer sur la déclaration préalable d'installation d'une antenne relais de téléphonie mobile sur le territoire de cette commune est suspendue.<br/>
<br/>
Article 3 : Il est enjoint à la commune de Montfermeil de procéder à un nouvel examen de la déclaration préalable de travaux présentée par la société Free Mobile dans un délai d'un mois à compter de la notification de la présente décision.<br/>
<br/>
Article 4 : La commune de Montfermeil versera à la société Free Mobile la somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
Article 5 : Les conclusions de la commune de Montfermeil tendant à l'application de l'article               L. 761-1 du code de justice administrative sont rejetées. <br/>
Article 6 : La présente décision sera notifiée à la société Free Mobile et à la commune de Montfermeil.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
