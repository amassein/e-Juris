<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028839600</ID>
<ANCIEN_ID>J0_L_2014_04_000001303178</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/83/96/CETATEXT000028839600.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>CAA de VERSAILLES, 7ème chambre, 03/04/2014, 13VE03178, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-04-03</DATE_DEC>
<JURIDICTION>CAA de VERSAILLES</JURIDICTION>
<NUMERO>13VE03178</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème chambre</FORMATION>
<TYPE_REC>excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>Mme HELMHOLTZ</PRESIDENT>
<AVOCATS>VITEL</AVOCATS>
<RAPPORTEUR>Mme Céline  VAN MUYLDER</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme GARREC</COMMISSAIRE_GVT>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 23 octobre 2013, présentée pour M. A...B..., demeurant..., par Me Vitel, avocat ; <br/>
<br/>
       M. B...demande à la Cour : <br/>
<br/>
       1°) d'annuler le jugement n° 1303673 en date du 27 septembre 2013 par lequel le Tribunal administratif de Cergy-Pontoise a rejeté sa demande tendant à l'annulation de l'arrêté en date du 4 avril 2013 par lequel le préfet du Val-d'Oise a refusé de lui délivrer un titre de séjour, l'a obligé à quitter le territoire français dans un délai de trente jours et a fixé le pays de renvoi ; <br/>
<br/>
       2°) d'annuler pour excès de pouvoir l'arrêté préfectoral en date du 4 avril 2013 ;<br/>
<br/>
       3°) d'enjoindre au préfet du Val-d'Oise de lui délivrer un titre de séjour sous astreinte de 200 euros par jour de retard à compter de la décision à intervenir, en application des dispositions des articles L. 911-1 à L. 911-3 du code de justice administrative et, subsidiairement de réexaminer sa situation dans un délai de quinze jours à compter de la décision à intervenir sous astreinte de 200 euros par jour de retard et de lui délivrer une autorisation provisoire de séjour durant cet examen ;<br/>
<br/>
       4°) de mettre à la charge de l'Etat une somme de 2 000 euros sur le fondement des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
       Il soutient que :<br/>
<br/>
       En ce qui concerne le refus de titre de séjour : <br/>
<br/>
       - la décision attaquée est insuffisamment motivée ;<br/>
       - le préfet a commis une erreur de droit en ajoutant illégalement aux stipulations de l'article 6-2 de l'accord franco-algérien une condition tirée de la continuité du séjour qui ne résulte pas de ce texte ;<br/>
       - le préfet a commis une erreur manifeste d'appréciation en ne procédant pas à sa régularisation au titre de son pouvoir discrétionnaire ;<br/>
       - le préfet a méconnu les dispositions de l'article L. 311-9 du code de l'entrée et du séjour des étrangers et du droit d'asile et a commis une erreur de fait en lui faisant conclure un contrat d'accueil et d'intégration ;<br/>
       - le préfet a méconnu les stipulations de l'article 6-2 de l'accord franco-algérien ;<br/>
       - le préfet a méconnu les stipulations de l'article 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales et de l'article 6-5 de l'accord franco-algérien ;<br/>
       - le préfet a commis une erreur manifeste d'appréciation quant aux conséquences de sa décision sur sa situation personnelle ;<br/>
<br/>
       En ce qui concerne la décision portant obligation de quitter le territoire français :<br/>
<br/>
       - la décision attaquée est illégale par voie de conséquence de l'illégalité de la décision portant refus de délivrance d'un titre de séjour ;<br/>
       - la décision attaquée est entachée d'un défaut de motivation ;<br/>
       - le préfet a commis une erreur de droit en ne procédant pas à un examen particulier de sa situation ;<br/>
       - le préfet a méconnu les stipulations de l'article 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
       - le préfet a commis une erreur manifeste d'appréciation quant aux conséquences de sa décision sur sa situation personnelle ;<br/>
<br/>
       En ce qui concerne la décision fixant le délai de départ volontaire :<br/>
<br/>
       - la décision attaquée est illégale par voie de conséquence de l'illégalité des décisions portant refus de délivrance d'un titre de séjour et obligation de quitter le territoire français ;<br/>
       - la décision attaquée est entachée d'un défaut de motivation ;<br/>
       - le préfet a méconnu les stipulations de l'article 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
       - le préfet a commis une erreur manifeste d'appréciation quant aux conséquences de sa décision sur sa situation personnelle ;<br/>
<br/>
       En ce qui concerne la décision fixant le pays de destination :<br/>
<br/>
       - la décision attaquée est illégale par voie de conséquence de l'illégalité de la décision portant refus de délivrance d'un titre de séjour ;<br/>
       - le préfet a méconnu les stipulations de l'article 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
       - le préfet a commis une erreur manifeste d'appréciation quant aux conséquences de sa décision sur sa situation personnelle ;<br/>
<br/>
       ..........................................................................................................<br/>
<br/>
       Vu les autres pièces du dossier ;<br/>
<br/>
       Vu l'accord franco-algérien du 27 décembre 1968 modifié ;<br/>
<br/>
       Vu la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
<br/>
       Vu le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
<br/>
       Vu le code de justice administrative ;<br/>
<br/>
       Vu la décision du président de formation de jugement de dispenser le rapporteur public, sur sa proposition, de prononcer des conclusions à l'audience ;<br/>
<br/>
       Les parties ayant été régulièrement averties du jour de l'audience ;<br/>
<br/>
       Après avoir entendu au cours de l'audience publique du 20 mars 2014 :<br/>
<br/>
       - le rapport de Mme Van Muylder, premier conseiller,<br/>
       - et les observations Me C...substituant Me Vitel, pour M. B...;<br/>
<br/>
<br/>
       1. Considérant que M.B..., ressortissant algérien né le 23 mai 1966, a sollicité la délivrance d'un certificat de résidence sur le fondement de l'article 6-2 de l'accord <br/>
franco-algérien susvisé invoquant sa qualité de conjoint de ressortissant de nationalité française ; qu'il relève appel du jugement en date du 27 septembre 2013 par lequel le Tribunal administratif de Cergy-Pontoise a rejeté sa demande tendant à l'annulation de l'arrêté en date du 4 avril 2013 par lequel le préfet du Val-d'Oise a refusé de lui délivrer un titre de séjour, l'a obligé à quitter le territoire français dans un délai de trente jours et a fixé le pays de renvoi ; <br/>
<br/>
       En ce qui concerne la décision portant refus de délivrance d'un titre de séjour :<br/>
<br/>
       2. Considérant que l'arrêté qui comporte l'énoncé des considérations de droit et de fait est suffisamment motivé ; <br/>
<br/>
       3. Considérant qu'aux termes de l'article 6 de l'accord franco-algérien du 27 décembre 1968 : " Le certificat de résidence d'un an portant la mention " vie privée et familiale " est délivré de plein droit : (...) 2° Au ressortissant algérien, marié avec un ressortissant de nationalité française, à condition que son entrée sur le territoire français ait été régulière, que le conjoint ait conservé la nationalité française et, lorsque le mariage a été célébré à l'étranger, qu'il ait été transcrit préalablement sur les registres de l'état civil français (...) " ; qu'il résulte de ces stipulations que la circonstance qu'un ressortissant algérien, régulièrement entré en France sous un visa de court séjour, ait fait l'objet, au-delà de la durée de validité de ce visa, de décisions de refus de titre de séjour assorties d'invitation à quitter le territoire et d'une mesure de reconduite à la frontière, régulièrement notifiées, ne fait pas obstacle à ce que la condition d'entrée régulière en France continue d'être regardée comme remplie, dès lors que l'étranger s'est maintenu sur le territoire ; <br/>
<br/>
       4. Considérant que M. B...soutient être entré en France le 1er novembre 2000 sous couvert d'un visa de court séjour ; qu'il ne ressort toutefois pas des pièces du dossier qu'il se serait maintenu, même illégalement, sur le territoire français depuis lors et que cette date correspondrait à sa dernière entrée sur le territoire français ; que dans ces conditions, compte tenu de l'ancienneté de la date d'entrée invoquée et de l'absence d'établissement d'une résidence habituelle sur le territoire français, notamment pour les années 2002 à 2006, le préfet du <br/>
Val-d'Oise a pu, à bon droit, refuser de lui délivrer un certificat de résidence sur le fondement des stipulations précitées ; <br/>
<br/>
       5. Considérant qu'aux termes de l'article 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales : " Toute personne a droit au respect de sa vie privée et familiale, de son domicile et de sa correspondance. Il ne peut y avoir ingérence d'une autorité publique dans l'exercice de ce droit que pour autant que cette ingérence est prévue par la loi et qu'elle constitue une mesure qui, dans une société démocratique, est nécessaire à la sécurité nationale, à la sûreté publique, au bien-être économique du pays, à la défense de l'ordre et à la prévention des infractions pénales, à la protection de la santé ou de la morale, ou à la protection des droits et libertés d'autrui " ; qu'aux termes de l'article 6 de l'accord franco-algérien du 27 décembre 1968 modifié : " (...) Le certificat de résidence d'un an portant la mention " vie privée et familiale " est délivré de plein droit : (...) / 5) au ressortissant algérien, qui n'entre pas dans les catégories précédentes ou dans celles qui ouvrent droit au regroupement familial, dont les liens personnels et familiaux en France sont tels que le refus d'autoriser son séjour porterait à son droit au respect de sa vie privée et familiale une atteinte disproportionnée au regard des motifs du refus ; (...) " ;<br/>
<br/>
       6. Considérant que M. B...fait valoir qu'il a ses attaches personnelles et familiales sur le territoire national et réside depuis son entrée le 1er novembre 2000, qu'il s'est marié le 15 mai 2012 avec une conjointe de nationalité française, qu'il est dépourvu d'attaches en Algérie dès lors que ses parents sont décédés, qu'il est parfaitement intégré et qu'il ne trouble pas l'ordre public ; que les pièces produites ne sont toutefois pas de nature à établir ses allégations, notamment sa résidence habituelle sur le territoire français pour les années 2002 à 2006 et la réalité de sa vie commune avec son épouse ; qu'il ressort en outre de sa demande de titre de séjour, que ses deux soeurs et son frère résident en Algérie, pays où il a vécu au moins jusqu'à l'âge de 34 ans ; que, dès lors, dans les circonstances de l'espèce et compte tenu de la possibilité pour l'intéressé de revenir régulièrement sur le territoire français en sa qualité de conjoint de ressortissant de nationalité française, M. B...n'est pas fondé à soutenir que la décision attaquée porterait une atteinte disproportionnée à son droit au respect de sa vie privée et familiale et aurait ainsi méconnu les stipulations de l'article 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales et de l'article 6-5 de l'accord franco-algérien ; que, par suite, les moyens doivent être écartés ; que, pour les mêmes motifs, il ne ressort pas des pièces du dossier que le préfet aurait commis une erreur manifeste dans l'appréciation des conséquences de sa décision sur la situation personnelle du requérant ; <br/>
<br/>
       7. Considérant qu'aux termes de l'article L. 311-9 du code de l'entrée et du séjour des étrangers et du droit d'asile : " L'étranger admis pour la première fois au séjour en France ou qui entre régulièrement en France entre l'âge de seize ans et l'âge de dix-huit ans, et qui souhaite s'y maintenir durablement, prépare son intégration républicaine dans la société française. / A cette fin, il conclut avec l'Etat un contrat d'accueil et d'intégration, traduit dans une langue qu'il comprend, par lequel il s'oblige à suivre une formation civique et, lorsque le besoin en est établi, linguistique. L'étranger pour lequel l'évaluation du niveau de connaissance de la langue prévue à l'article L. 411-8 et au deuxième alinéa de l'article L. 211-2-1 n'a pas établi le besoin d'une formation est réputé ne pas avoir besoin d'une formation linguistique. La formation civique comporte une présentation des institutions françaises et des valeurs de la République, notamment l'égalité entre les hommes et les femmes et la laïcité, ainsi que la place de la France en Europe. La formation linguistique est sanctionnée par un titre ou un diplôme reconnus par l'Etat. L'étranger bénéficie d'une session d'information sur la vie en France et d'un bilan de compétences professionnelles. Toutes ces formations et prestations sont dispensées gratuitement et financées par l'Office français de l'immigration et de l'intégration. Lorsque l'étranger est âgé de seize à dix-huit ans, le contrat d'accueil et d'intégration doit être cosigné par son représentant légal régulièrement admis au séjour en France. (...) " ;<br/>
<br/>
       8. Considérant que si M. B...soutient que le préfet du Val-d'Oise lui a fait signer un contrat d'accueil et d'intégration, lui a fait passer un contrôle médical, et l'a soumis à un test de connaissances en langue française, lui a délivré une information sur la vie en France et l'a invité à participer à la formation civique, ces circonstances qui n'ont eu ni pour effet, ni pour objet, de lui ouvrir droit à la délivrance d'un titre de séjour, sont sans incidence sur la légalité de la décision attaquée ; <br/>
<br/>
       En ce qui concerne la décision portant obligation de quitter le territoire français :<br/>
<br/>
       9. Considérant qu'aux termes du I de l'article L. 511-1 du code de l'entrée et du séjour des étrangers et du droit d'asile : " L'autorité administrative peut obliger à quitter le territoire français un étranger non ressortissant d'un Etat membre de l'Union européenne, d'un autre Etat partie à l'accord sur l'Espace économique européen ou de la Confédération suisse et qui n'est pas membre de la famille d'un tel ressortissant au sens des 4° et 5° de l'article L. 121-1, lorsqu'il se trouve dans l'un des cas suivants : (...) 3° Si la délivrance ou le renouvellement d'un titre de séjour a été refusé à l'étranger ou si le titre de séjour qui lui avait été délivré lui a été retiré (...) La décision énonçant l'obligation de quitter le territoire français est motivée. Elle n'a pas à faire l'objet d'une motivation distincte de celle de la décision relative au séjour dans les cas prévus aux 3° et 5° du présent I, sans préjudice, le cas échéant, de l'indication des motifs pour lesquels il est fait application des II et III." ; qu'en application de ces dispositions, la décision portant obligation de quitter le territoire français n'a pas à faire l'objet d'une motivation distincte de celle de la décision relative au séjour, dès lors que la décision de refus de séjour est suffisamment motivée, ainsi qu'il a été dit au point 2 ; que par suite, le moyen doit être écarté ;<br/>
<br/>
       10. Considérant qu'il résulte de ce qui précède M. B...n'établit pas que le refus de titre de séjour qui lui a été opposé serait entaché d'illégalité ; qu'il n'est, par suite, pas fondé à soutenir que l'obligation de quitter le territoire français qui assortit ce refus serait dépourvue de base légale ; que, dès lors, le moyen doit être écarté ;<br/>
<br/>
       11. Considérant que contrairement à ce que soutient le requérant, il ne ressort pas des pièces du dossier que le préfet n'aurait pas procédé à un examen particulier de sa situation personnelle ; que, dès lors le moyen doit être écarté ;<br/>
<br/>
       12. Considérant qu'il résulte de ce qui a été dit au point 6 que le moyen tiré de la méconnaissance des stipulations de l'article 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, qui reprend ce qui a été précédemment développé à l'appui des conclusions tendant à l'annulation de la décision de refus de délivrance d'un titre de séjour, doit être écarté pour les mêmes motifs que précédemment ;<br/>
<br/>
       13. Considérant qu'il ne ressort pas des pièces du dossier que le préfet aurait commis une erreur manifeste d'appréciation quant aux conséquences de sa décision sur la situation personnelle du requérant ; que, dès lors, le moyen doit être écarté ;<br/>
       En ce qui concerne la décision fixant un délai de départ volontaire de trente jours :<br/>
<br/>
       14. Considérant qu'aux termes du II de l'article L. 511-1 du code de l'entrée et du séjour des étrangers et du droit d'asile : " II. - Pour satisfaire à l'obligation qui lui a été faite de quitter le territoire français, l'étranger dispose d'un délai de trente jours à compter de sa notification et peut solliciter, à cet effet, un dispositif d'aide au retour dans son pays d 'origine. Eu égard à la situation personnelle de l'étranger, l'autorité administrative peut accorder, à titre exceptionnel, un délai de départ volontaire supérieur à trente jours. / Toutefois, l'autorité administrative peut, par une décision motivée, décider que l'étranger est obligé de quitter sans délai le territoire français (...) " ; qu'il résulte de ces dispositions que seule la décision de refus d'accorder un délai de départ volontaire doit être motivée ; que, dès lors, le moyen est inopérant et ne peut qu'être écarté ;<br/>
<br/>
       15. Considérant qu'il résulte de ce qui précède M. B...n'établit pas que les décisions portant refus de titre de séjour et obligation de quitter le territoire français qui lui ont été opposées seraient entachées d'illégalité ; qu'il n'est, par suite, pas fondé à soutenir que la décision fixant un délai de départ volontaire de trente jours qui assortit l'obligation de quitter le territoire français serait dépourvue de base légale ; que, dès lors, le moyen doit être écarté ;<br/>
<br/>
       16. Considérant qu'il résulte de ce qui a été dit au point 6 que le moyen tiré de la méconnaissance des stipulations de l'article 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, qui reprend ce qui a été précédemment développé à l'appui des conclusions tendant à l'annulation de la décision de refus de délivrance d'un titre de séjour, doit être écarté pour les mêmes motifs que précédemment ;<br/>
<br/>
       17. Considérant qu'il ne ressort pas des pièces du dossier que le préfet aurait commis une erreur manifeste d'appréciation quant aux conséquences de sa décision sur la situation personnelle du requérant en fixant un délai de départ volontaire à trente jours ; que, dès lors, le moyen doit être écarté ;<br/>
<br/>
       En ce qui concerne la décision fixant le pays de destination :<br/>
<br/>
       18. Considérant qu'il résulte de ce qui précède M. B...n'établit pas que les décisions portant refus de titre de séjour et obligation de quitter le territoire français qui lui ont été opposées seraient entachées d'illégalité ; qu'il n'est, par suite, pas fondé à soutenir que la décision fixant le pays à destination duquel il pourra être reconduit d'office serait dépourvue de base légale ; que, dès lors, le moyen doit être écarté ;<br/>
<br/>
       19. Considérant qu'il résulte de ce qui a été dit au point 6 que le moyen tiré de la méconnaissance des stipulations de l'article 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, qui reprend ce qui a été précédemment développé à l'appui des conclusions tendant à l'annulation de la décision de refus de délivrance d'un titre de séjour, doit être écarté pour les mêmes motifs que précédemment ;<br/>
<br/>
       20. Considérant qu'il ne ressort pas des pièces du dossier que le préfet aurait commis une errer manifeste d'appréciation quant aux conséquences de sa décision sur la situation personnelle du requérant en fixant le pays à destination duquel il pourra être reconduit d'office ; que, dès lors, le moyen doit être écarté ;<br/>
<br/>
<br/>
       21. Considérant qu'il résulte de ce qui précède que M. B...n'est pas fondé à soutenir que c'est à tort que, par le jugement attaqué, le Tribunal administratif de Cergy-Pontoise a rejeté sa demande tendant à l'annulation de l'arrêté attaqué ; qu'il y a lieu, par voie de conséquence, de rejeter les conclusions aux fins d'injonction et celles présentées sur le fondement des dispositions de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
DECIDE :<br/>
<br/>
<br/>
Article 1er : La requête de M. B...est rejetée.<br/>
''<br/>
''<br/>
''<br/>
''<br/>
2<br/>
N° 13VE03178<br/>
<br/>
<br/>
<br/>
</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">335-01-03 Étrangers. Séjour des étrangers. Refus de séjour.
</SCT>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
