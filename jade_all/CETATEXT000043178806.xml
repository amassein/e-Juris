<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043178806</ID>
<ANCIEN_ID>JG_L_2021_02_000000432994</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/17/88/CETATEXT000043178806.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème - 6ème chambres réunies, 19/02/2021, 432994</TITRE>
<DATE_DEC>2021-02-19</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>432994</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème - 6ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. François Charmont</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:432994.20210219</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés au secrétariat du contentieux du Conseil d'Etat les 18 juillet 2019 et 14 janvier 2020, Mme A... B... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision du 22 mai 2019 par laquelle le Conseil national de l'ordre des masseurs-kinésithérapeutes a refusé de reconnaître le diplôme universitaire d'études complémentaires " micronutrition " qui lui a été délivré par l'université de Strasbourg au titre de l'année universitaire 2017-2018 ;<br/>
<br/>
              2°) d'enjoindre au Conseil national de l'ordre des masseurs-kinésithérapeutes de reconnaître ce diplôme.<br/>
<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
- le code de la santé publique ;<br/>
              - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. François Charmont, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public.<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Mme B..., masseur-kinésithérapeute, demande l'annulation pour excès de pouvoir de la décision du 22 mai 2019 par laquelle le Conseil national de l'ordre des masseurs-kinésithérapeutes a refusé de reconnaître le diplôme d'université d'études complémentaires en " micronutrition ", délivré en 2018 par l'université de Strasbourg.<br/>
<br/>
              2. D'une part, aux termes de l'article L. 4321-1 du code de la santé publique : " La pratique de la masso-kinésithérapie comporte la promotion de la santé, la prévention, le diagnostic kinésithérapique et le traitement :/ 1° Des troubles du mouvement ou de la motricité de la personne ;/ 2° Des déficiences ou des altérations des capacités fonctionnelles./ Le masseur-kinésithérapeute peut également concourir à la formation initiale et continue ainsi qu'à la recherche./ Le masseur-kinésithérapeute exerce son activité en toute indépendance et en pleine responsabilité conformément au code de déontologie mentionné à l'article L. 4321-21./ Dans le cadre des pathologies héréditaires, congénitales ou acquises, stabilisées ou évolutives impliquant une altération des capacités fonctionnelles, le masseur-kinésithérapeute met en oeuvre des moyens manuels, instrumentaux et éducatifs et participe à leur coordination./ Dans l'exercice de son art, seul le masseur-kinésithérapeute est habilité à utiliser les savoirs disciplinaires et les savoir-faire associés d'éducation et de rééducation en masso-kinésithérapie qu'il estime les plus adaptés à la situation et à la personne, dans le respect du code de déontologie précité./ La définition des actes professionnels de masso-kinésithérapie, dont les actes médicaux prescrits par un médecin, est précisée par un décret en Conseil d'Etat, après avis de l'Académie nationale de médecine. (...) " Aux termes de l'article R. 4321-1 du même code : " La masso-kinésithérapie consiste en des actes réalisés de façon manuelle ou instrumentale, notamment à des fins de rééducation, qui ont pour but de prévenir l'altération des capacités fonctionnelles, de concourir à leur maintien et, lorsqu'elles sont altérées, de les rétablir ou d'y suppléer. Ils sont adaptés à l'évolution des sciences et des techniques. "<br/>
<br/>
              3. D'autre part, aux termes de l'article R. 4321-122 du code de la santé publique : " Les indications qu'un masseur-kinésithérapeute est autorisé à mentionner sur ses documents professionnels:/ (...) 5° Ses diplômes, titres, grades et fonctions lorsqu'ils ont été reconnus par le conseil national de l'ordre ; (...) ". Aux termes de l'article R. 4321-123 du même code : " Les indications qu'un masseur-kinésithérapeute est autorisé à faire figurer dans les annuaires à usage du public, dans la rubrique : masseurs-kinésithérapeutes, quel qu'en soit le support, sont:/ (...) 3° La qualification, les titres reconnus conformément au règlement de qualification, les titres et les diplômes d'études complémentaires reconnus par le conseil national de l'ordre. (...) ". Pour faire l'objet d'une reconnaissance en application de ces dispositions, le diplôme soumis au Conseil national de l'ordre des masseurs-kinésithérapeutes doit porter sur des connaissances ayant un lien suffisant avec la pratique de la masso-kinésithérapie, définie par les textes cités au point 2.<br/>
<br/>
              4. Il ressort des pièces du dossier que le diplôme " micronutrition " délivré en 2018 par l'université de Strasbourg sanctionne une formation relative à l'usage de compléments alimentaires dits " micronutriments " et s'adressant indistinctement, selon les termes mêmes de ses documents de présentation, aux " docteurs en pharmacie, docteurs en médecine, infirmières, sages-femmes, diététiciens, kinésithérapeutes, psychothérapeutes ". En estimant que cette formation, qui vise à ce que ces différents praticiens ajoutent à leurs compétences propres une faculté de conseiller leurs patients sur la " micronutrition ", ne présentait pas, avec l'exercice de la masso-kinésithérapie, un lien suffisant justifiant sa mention sur les annuaires, plaques et documents professionnels des masseurs-kinésithérapeutes, le conseil national de l'ordre a fait une exacte application des dispositions des articles R. 4321-122 et R. 4321-123 du code de la santé publique. <br/>
<br/>
              5. Il résulte de ce qui précède que Mme B... n'est pas fondée à demander l'annulation de la décision du Conseil national de l'ordre des masseurs-kinésithérapeutes du 22 mai 2019. Dans les circonstances de l'espèce, il n'y a pas lieu de mettre à sa charge la somme que le Conseil national de l'ordre des masseurs-kinésithérapeutes demande au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              			D E C I D E :<br/>
              			--------------<br/>
<br/>
		Article 1er : La requête de Mme B... est rejetée.<br/>
<br/>
Article 2 : Les conclusions du Conseil national de l'ordre des masseurs-kinésithérapeutes, présentées au titre de l'article L. 761-1 du code de justice administrative, sont rejetées.<br/>
<br/>
Article 3 : La présente décision sera notifiée à Mme A... B... et au Conseil national de l'ordre des masseurs-kinésithérapeutes.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">55-01-02-018 PROFESSIONS, CHARGES ET OFFICES. ORDRES PROFESSIONNELS - ORGANISATION ET ATTRIBUTIONS NON DISCIPLINAIRES. QUESTIONS PROPRES À CHAQUE ORDRE PROFESSIONNEL. - RECONNAISSANCE D'UN DIPLÔME PAR LE CONSEIL NATIONAL DE L'ORDRE - 1) CONDITIONS - INCLUSION - LIEN SUFFISANT AVEC LA PRATIQUE DE LA MASSO-KINÉSITHÉRAPIE [RJ1] - 2) DIPLÔME AYANT POUR OBJET LA MICRONUTRITION ET S'ADRESSANT INDISTINCTEMENT À PLUSIEURS PROFESSIONS MÉDICALES ET PARAMÉDICALES - LIEN SUFFISANT AVEC LA PRATIQUE DE LA MASSO-KINÉSITHÉRAPIE - ABSENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">61-035 SANTÉ PUBLIQUE. PROFESSIONS MÉDICALES ET AUXILIAIRES MÉDICAUX. - MASSEURS-KINÉSITHÉRAPEUTES - RECONNAISSANCE D'UN DIPLÔME PAR LE CONSEIL NATIONAL DE L'ORDRE - 1) CONDITIONS - INCLUSION - LIEN SUFFISANT AVEC LA PRATIQUE DE LA MASSO-KINÉSITHÉRAPIE [RJ1] - 2) DIPLÔME AYANT POUR OBJET LA MICRONUTRITION ET S'ADRESSANT INDISTINCTEMENT À PLUSIEURS PROFESSIONS MÉDICALES ET PARAMÉDICALES - LIEN SUFFISANT AVEC LA PRATIQUE DE LA MASSO-KINÉSITHÉRAPIE - ABSENCE.
</SCT>
<ANA ID="9A"> 55-01-02-018 1) Pour faire l'objet d'une reconnaissance en application des articles R. 4321-122 et R. 4321-123 du code de la santé publique (CSP), le diplôme soumis au Conseil national de l'ordre des masseurs-kinésithérapeutes doit porter sur des connaissances ayant un lien suffisant avec la pratique de la masso-kinésithérapie, définie par les articles L. 4321-1 et R. 4321-1 du même code.,,,2) Diplôme micronutrition délivré par une université sanctionnant une formation relative à l'usage de compléments alimentaires dits micronutriments et s'adressant indistinctement, selon les termes mêmes de ses documents de présentation, aux docteurs en pharmacie, docteurs en médecine, infirmières, sages-femmes, diététiciens, kinésithérapeutes, psychothérapeutes. Un tel diplôme ne présente pas, avec l'exercice de la masso-kinésithérapie, un lien suffisant justifiant sa mention sur les annuaires, plaques et documents professionnels des masseurs-kinésithérapeutes.</ANA>
<ANA ID="9B"> 61-035 1) Pour faire l'objet d'une reconnaissance en application des articles R. 4321-122 et R. 4321-123 du code de la santé publique (CSP), le diplôme soumis au Conseil national de l'ordre des masseurs-kinésithérapeutes doit porter sur des connaissances ayant un lien suffisant avec la pratique de la masso-kinésithérapie, définie par les articles L. 4321-1 et R. 4321-1 du même code.,,,2) Diplôme micronutrition délivré par une université sanctionnant une formation relative à l'usage de compléments alimentaires dits micronutriments et s'adressant indistinctement, selon les termes mêmes de ses documents de présentation, aux docteurs en pharmacie, docteurs en médecine, infirmières, sages-femmes, diététiciens, kinésithérapeutes, psychothérapeutes. Un tel diplôme ne présente pas, avec l'exercice de la masso-kinésithérapie, un lien suffisant justifiant sa mention sur les annuaires, plaques et documents professionnels des masseurs-kinésithérapeutes.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., s'agissant de la légalité de la décision du Conseil national de l'ordre des chirurgiens-dentistes exigeant de tout diplôme reconnu qu'il présente un intérêt dans la pratique quotidienne du praticien, CE, 2 juin 2010,,, n° 316735, T. pp. 954-957.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
