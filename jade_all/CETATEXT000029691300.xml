<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029691300</ID>
<ANCIEN_ID>JG_L_2014_11_000000366630</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/69/13/CETATEXT000029691300.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  4ème sous-section jugeant seule, 03/11/2014, 366630, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-11-03</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>366630</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 4ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP CELICE, BLANCPAIN, SOLTNER ; SCP SPINOSI, SUREAU</AVOCATS>
<RAPPORTEUR>M. Bruno Bachini</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Gaëlle Dumortier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2014:366630.20141103</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 6 mars et 6 juin 2013 au secrétariat du contentieux du Conseil d'Etat, présentés pour la société d'exploitation de la résidence Antinéa, dont le siège est situé allée du Grand Pin à La Redorte (11700), représentée par son gérant en exercice ; la société d'exploitation de la résidence Antinéa demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 09MA02516-09MA02669 du 2 mai 2011 par lequel la cour administrative d'appel de Marseille a rejeté sa requête tendant, d'une part, à l'annulation du jugement n° 0700290 du tribunal administratif de Montpellier du 7 mai 2009 ayant annulé, à la demande de M. B...A..., la décision du 17 novembre 2006 par laquelle l'inspecteur du travail a autorisé son licenciement, d'autre part, au rejet de la demande présentée par M. A... devant le tribunal ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de M. A...la somme de 1 500 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code du travail ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Bruno Bachini, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Gaëlle Dumortier, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Célice, Blancpain, Soltner, avocat de la société d'exploitation de la résidence Antinéa et à la SCP Spinosi, Sureau, avocat de M. A...;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que la société d'exploitation de la résidence Antinéa, établissement d'accueil pour personnes âgées valides et dépendantes, a sollicité, le 12 octobre 2006, l'autorisation de licencier pour faute M. A..., salarié protégé, en raison, d'une part, de son comportement irrespectueux et dangereux à l'égard des résidents de l'établissement et, d'autre part, de son comportement agressif à l'égard de ses collègues ; que cette autorisation lui a été accordée par une décision de l'inspecteur du travail de la 1ère section de l'Aude en date du 17 novembre 2006 ; que, saisi par M.A..., le tribunal administratif de Montpellier a annulé pour excès de pouvoir cette décision par un jugement du 7 mai 2009 que la cour administrative d'appel de Marseille a confirmé par un arrêt du 2 mai 2011 contre lequel la société d'exploitation de la résidence Antinéa se pourvoit en cassation ;<br/>
<br/>
              2. Considérant qu'aux termes du premier alinéa de l'article L. 122-44 du code du travail, alors applicable, aujourd'hui repris à l'article L. 1332-4 du même code : " Aucun fait fautif ne peut donner lieu à lui seul à l'engagement de poursuites disciplinaires au-delà d'un délai de deux mois à compter du jour où l'employeur en a eu connaissance, à moins que ce fait ait donné lieu dans le même délai à l'exercice de poursuites pénales. (...) " ; que ce délai commence à courir lorsque l'employeur a une connaissance exacte de la réalité, de la nature et de l'ampleur des faits reprochés au salarié protégé ;<br/>
<br/>
              3. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que si l'employeur avait adressé le 27 décembre 2005, le 16 mai 2006, le 30 mai 2006 et le 21 août 2006 des avertissements à M.A..., fondés notamment sur son comportement irrespectueux et dangereux à l'égard des résidents de l'établissement, l'employeur n'a eu connaissance de certains des faits reprochés à M.A..., notamment un attouchement sexuel sur la personne d'une résidente citée par l'employeur à l'appui de sa demande, qu'à l'occasion d'attestations qui lui ont été adressées le 10 octobre 2006 par des membres du personnel ; qu'ainsi, en relevant que l'employeur avait eu une connaissance exacte de l'ampleur des faits reprochés au plus tard le 31 mai 2006, la cour administrative d'appel a dénaturé les faits de l'espèce ; que, par suite, la société requérante est fondée à demander l'annulation de l'arrêt attaqué ;<br/>
<br/>
              4. Considérant que les dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 font obstacle à ce qu'il soit fait droit aux conclusions présentées sur le fondement de ces dispositions par Me Spinosi, avocat, de M. A... ; qu'il n'y a pas lieu non plus, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par la société d'exploitation de la résidence Antinéa au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Marseille du 2 mai 2011 est annulé. <br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Marseille.<br/>
Article 3 : Le surplus des conclusions du pourvoi et les conclusions présentées par M. A...au titre des dispositions de l'article L. 761-1 du code de justice administrative et de l'article 37 de la loi du 10 juillet 1991 sont rejetés.<br/>
Article 4 : La présente décision sera notifiée à la société d'exploitation de la résidence Antinéa et à M. B...A....<br/>
Copie en sera adressée pour information au ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
