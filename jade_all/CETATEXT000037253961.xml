<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037253961</ID>
<ANCIEN_ID>JG_L_2018_07_000000409240</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/25/39/CETATEXT000037253961.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème chambre, 26/07/2018, 409240, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-07-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>409240</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP NICOLAY, DE LANOUVELLE, HANNOTIN</AVOCATS>
<RAPPORTEUR>Mme Marie-Gabrielle Merloz</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emilie Bokdam-Tognetti</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:409240.20180726</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La SAS L'Immobilière Groupe Casino a demandé au tribunal administratif de Poitiers de prononcer la réduction des cotisations de taxe foncière sur les propriétés bâties auxquelles elle a été assujettie au titre des années 2012 et 2013 dans les rôles de la commune de Champniers (Charente) à raison d'un centre commercial. Par un jugement n° 1403040 du 24 janvier 2017, le tribunal a rejeté sa requête.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 27 mars et 28 juin 2017 au secrétariat du contentieux du Conseil d'Etat, la société L'Immobilière Groupe Casino demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code général des impôts ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Marie-Gabrielle Merloz, maître des requêtes,<br/>
<br/>
              - les conclusions de Mme Emilie Bokdam-Tognetti, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Nicolaÿ, de Lanouvelle, Hannotin, avocat de la société L'Immobilière Groupe Casino.<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que la société L'Immobilière Groupe Casino est propriétaire du centre commercial dit " Les Grandes Chaumes " situé sur la commune de Champniers (Charente) et constitué d'un hypermarché, d'une station-service, d'une station de lavage et d'un parking. L'administration a évalué la valeur locative de cet immeuble par voie d'appréciation directe en vue de l'établissement de la taxe foncière. La société requérante se pourvoit en cassation contre le jugement du 24 janvier 2017 par lequel le tribunal administratif de Poitiers a rejeté sa demande tendant à la réduction des impositions en litige.<br/>
<br/>
              2. Aux termes de l'article 1498 du code général des impôts : " La valeur locative de tous les biens autres que les locaux visés au I de l'article 1496 et que les établissements industriels visés à l'article 1499 est déterminée au moyen de l'une des méthodes indiquées ci-après : / (...) 2° a. Pour les biens loués à des conditions de prix anormales ou occupés par leur propriétaire, occupés par un tiers à un autre titre que la location, vacants ou concédés à titre gratuit, la valeur locative est déterminée par comparaison. / Les termes de comparaison sont choisis dans la commune. Ils peuvent être choisis hors de la commune pour procéder à l'évaluation des immeubles d'un caractère particulier ou exceptionnel ; / b. La valeur locative des termes de comparaison est arrêtée : / Soit en partant du bail en cours à la date de référence de la révision lorsque l'immeuble type était loué normalement à cette date, / Soit, dans le cas contraire, par comparaison avec des immeubles similaires situés dans la commune ou dans une localité présentant, du point de vue économique, une situation analogue à celle de la commune en cause et qui faisaient l'objet à cette date de locations consenties à des conditions de prix normales ". Il résulte de ces dispositions que la valeur locative d'un immeuble ne peut légalement être déterminée par la voie d'appréciation directe que s'il est impossible de la fixer par voie de comparaison.<br/>
<br/>
              3. Après avoir écarté le local-type situé à Saintes proposé par la société requérante pour l'évaluation de l'hypermarché, au seul motif, au demeurant erroné quant au régime de preuve applicable en l'espèce, qu'elle n'établissait pas que la commune de Saintes présentait, du point de vue économique, une situation analogue à celle de Champniers, le tribunal a jugé qu'il y avait lieu de calculer, en application du 3° de l'article 1498 précité du code général des impôts, la valeur locative de l'immeuble en litige par voie d'appréciation directe. En ne recherchant pas, au besoin en procédant à une mesure d'instruction, s'il existait des termes de comparaison dans d'autres communes présentant une situation économique analogue à celle de la commune de Champniers, alors que l'administration, dans ses écritures, se bornait à affirmer ne pas avoir trouvé de tels termes de comparaison à Champniers ou dans les communes avoisinantes, le tribunal administratif de Poitiers a commis une erreur de droit. Par suite, la société requérante est fondée à demander l'annulation du jugement attaqué.<br/>
<br/>
              4. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat le versement à la société L'Immobilière Groupe Casino d'une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du tribunal administratif de Poitiers du 24 janvier 2017 est annulé.<br/>
Article 2 : L'affaire est renvoyée devant le tribunal administratif de Poitiers.<br/>
Article 3 : L'Etat versera à la société L'Immobilière Groupe Casino une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : La présente décision sera notifiée à la SAS L'Immobilière Groupe Casino et au ministre de l'action et des comptes publics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
