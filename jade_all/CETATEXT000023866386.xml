<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000023866386</ID>
<ANCIEN_ID>JG_L_2011_04_000000308014</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/23/86/63/CETATEXT000023866386.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème et 3ème sous-sections réunies, 15/04/2011, 308014</TITRE>
<DATE_DEC>2011-04-15</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>308014</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème et 3ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ODENT, POULET ; SCP TIFFREAU, CORLAY, MARLANGE</AVOCATS>
<RAPPORTEUR>Mme Marie-Astrid  Nicolazo de Barmon</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Escaut Nathalie</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2011:308014.20110415</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 30 juillet et 29 octobre 2007 au secrétariat du contentieux du Conseil d'Etat, présentés pour la SOCIETE NATIONALE DES CHEMINS DE FER FRANÇAIS (SNCF), dont le siège est 34, rue du Commandant Mouchotte à Paris (75699 Cedex 14) ; la SNCF demande au Conseil d'Etat d'annuler l'arrêt n° 04PA01865 du 24 mai 2007 par lequel la cour administrative d'appel de Paris a rejeté sa requête tendant, en premier lieu, à l'annulation du jugement n° 0308040 du 11 mars 2004 du tribunal administratif de Paris rejetant sa demande tendant à ce que la société France Télécom soit condamnée à réparer le préjudice résultant de l'occupation sans titre du domaine public ferroviaire de 1991 à 1996, en deuxième lieu, à ce qu'il soit constaté que la société France Télécom a été occupante sans titre du domaine public ferroviaire du 1er janvier 1991 au 31 décembre 1996, en troisième lieu, à la condamnation de la société France Télécom à lui verser une somme égale à la redevance qu'elle aurait dû verser au titre de cette période, soit une part fixe de 165 046 641 euros, cette somme étant capitalisée à la date du 30 juin 2004, et une part variable égale à un pourcentage du chiffre d'affaires réalisé par l'exploitation de son réseau de fibres optiques occupant le domaine public à déterminer par un expert, en quatrième lieu, à ce que soit enjoint à la société France Télécom de lui communiquer le montant du chiffre d'affaires afférent à l'exploitation de l'ensemble de ce réseau entre 1991 et 1996, et, en dernier lieu, à la désignation d'un expert ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 16 mars 2011, présentée pour la société France Télécom ; <br/>
<br/>
              Vu loi n° 82-1153 du 30 décembre 1982 ;<br/>
<br/>
              Vu la loi n° 90-568 du 2 juillet 1990 ;<br/>
<br/>
              Vu la loi n° 96-659 du 26 juillet 1996 ;<br/>
<br/>
              Vu la loi n° 97-135 du 13 févier 1997 ;<br/>
<br/>
              Vu le code du domaine de l'Etat, notamment son article L. 28 ;<br/>
<br/>
              Vu le code des postes et télécommunications, devenu le code des postes et des communications électroniques ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Marie-Astrid Nicolazo de Barmon, Auditeur, <br/>
<br/>
              - les observations de la SCP Odent, Poulet, avocat de la SNCF et de la SCP Tiffreau, Corlay, avocat de la société France Télécom, <br/>
<br/>
              - les conclusions de Mme Nathalie Escaut, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Odent, Poulet, avocat de la SNCF et à la SCP Tiffreau, Corlay, avocat de la société France Télécom ; <br/>
<br/>
<br/>
<br/>
<br/>
              Considérant que, par l'arrêt attaqué, la cour administrative d'appel de Paris a confirmé le jugement du 11 mars 2004 du tribunal administratif de Paris rejetant la demande de la SOCIETE NATIONALE DES CHEMINS DE FER FRANÇAIS (SNCF) tendant à la réparation du préjudice qu'elle estime avoir subi du fait de l'occupation illégale du domaine public ferroviaire, pour la période du 1er janvier 1991 au 31 décembre 1996, à raison de la présence, sur les emprises de ce domaine, de câbles de télécommunications, d'une longueur totale estimée à 2 600 kilomètres, appartenant à l'exploitant public France Télécom, devenu la société France Télécom ;<br/>
<br/>
              Considérant, en premier lieu, qu'aux termes de l'article L. 28 du code du domaine de l'Etat, alors en vigueur : " Nul ne peut, sans autorisation délivrée par l'autorité compétente, occuper une dépendance du domaine public national ou l'utiliser dans des limites excédant le droit d'usage qui appartient à tous. " ; que l'occupation sans droit ni titre d'une dépendance du domaine public constitue une faute commise par l'occupant et qui l'oblige à réparer le dommage causé au gestionnaire de ce domaine par cette occupation irrégulière ; que, si l'autorité gestionnaire du domaine public n'a pas mis en demeure l'occupant irrégulier de quitter les lieux, ne l'a pas invité à régulariser sa situation ou a entretenu à son égard une ambiguïté sur la régularité de sa situation, ces circonstances sont de nature, le cas échéant, à constituer une cause exonératoire de la responsabilité de l'occupant, dans la mesure où ce comportement du gestionnaire serait constitutif d'une faute, mais elles ne sauraient faire obstacle, dans son principe, au droit du gestionnaire du domaine public à la réparation du dommage résultant de cette occupation irrégulière ;<br/>
<br/>
              Considérant, en deuxième lieu et d'une part, qu'aux termes de l'article 18 de la loi du 30 décembre 1982 d'orientation des transports intérieurs, dans sa rédaction applicable au litige : " Il est créé, à compter du 1er janvier 1983, un établissement public industriel et commercial qui prend le nom de " Société nationale des chemins de fer français ". Cet établissement a pour objet d'exploiter, d'aménager et de développer, selon les principes du service public, le réseau ferré national " ; qu'aux termes de l'article 20 de la même loi, dans sa rédaction applicable au litige : " Les biens immobiliers affectés au service public du transport ferroviaire et aménagés spécialement à cette fin ont le caractère de domaine public. / Sous réserve des dispositions législatives applicables aux ouvrages déclarés d'intérêt général ou d'utilité publique, l'établissement public exerce tous pouvoirs de gestion sur les biens immobiliers qui lui sont remis ou qu'il acquiert ; il peut notamment accorder des autorisations d'occupation, consentir des baux, fixer et encaisser à son profit le montant des redevances, loyers et produits divers " ; <br/>
<br/>
              Considérant, d'autre part, qu'aux termes de l'article 22 de la loi du 2 juillet 1990 relative à l'organisation du service public de la poste et des télécommunications et créant notamment l'exploitant public France Télécom à compter du 1er janvier 1991 : " Les droits et obligations de l'Etat attachés aux services relevant (...) de la direction générale des télécommunications sont transférés de plein droit (...) à France Télécom. / L'ensemble des biens immobiliers du domaine public ou privé de l'Etat attachés aux services relevant (...) de la direction générale des télécommunications, ainsi que les biens mobiliers de ces services, sont transférés de plein droit et en pleine propriété (...) à France Télécom " ; qu'aux termes de l'article L. 47 du code des postes et télécommunications, dans sa rédaction alors applicable : " l'exploitant public peut exécuter sur le sol et le sous-sol des chemins publics et de leurs dépendances tous travaux nécessaires à la construction et à l'entretien des lignes télégraphiques ou téléphoniques " ;<br/>
<br/>
              Considérant que, ni les dispositions de l'article 22 de la loi du 2 juillet 1990 ni l'article L. 47 du code des postes et télécommunications, qui ne concerne que la voirie, ni aucune autre disposition de cette loi n'ont eu pour objet ou pour effet de transférer à l'exploitant public France Télécom le droit d'occuper sans autorisation et au surplus à titre gratuit le domaine public ferroviaire, droit que l'Etat ne tenait auparavant que de sa qualité de propriétaire de ce domaine public et qui n'était nullement attaché aux services relevant de la direction générale des télécommunications ; que, par suite, France Télécom ne pouvait occuper régulièrement ce domaine, à compter du 1er janvier 1991, sans y être autorisé expressément par la SNCF qui était alors le gestionnaire de ce domaine en vertu de l'article 20 de la loi du 30 décembre 1982 et qui, en cette qualité et en application de ces mêmes dispositions, devait percevoir les redevances dues à raison de son occupation par les personnes autres que l'Etat, resté propriétaire des dépendances domaniales remises en dotation à la SNCF par cette loi ; <br/>
<br/>
              Considérant, en troisième lieu, qu'aux termes du premier alinéa de l'article 6 de la loi du 13 février 1997 portant création de l'établissement public " Réseau ferré de France " : " Réseau ferré de France est substitué à la Société nationale des chemins de fer français pour les droits et obligations liés aux biens qui lui sont apportés, à l'exception de ceux afférents à des dommages constatés avant le 1er janvier 1997 et à des impôts ou taxes dont le fait générateur est antérieur à cette même date " ; que le dommage occasionné au domaine public à raison de son occupation irrégulière se réalise et, par suite, est réputé se constater, eu égard à sa nature, au fur et à mesure de cette occupation ; qu'il en résulte que l'indemnité tendant à réparer le préjudice subi par le gestionnaire du fait d'une occupation irrégulière du domaine public ferroviaire, au cours d'une période antérieure au 1er janvier 1997, est au nombre des droits mentionnés par l'article 6 de la loi du 13 février 1997, afférents à des dommages constatés avant l'entrée en vigueur de cette loi, et liés aux biens qui, remis en dotation à la SNCF en application de la loi du 30 décembre 1982, sont désormais apportés à Réseau ferré de France ; que, par suite, cet établissement public n'a pas été substitué à la SNCF pour l'exercice de ces droits ;<br/>
<br/>
              Considérant que, pour refuser de faire droit à la demande d'indemnisation dirigée par la SNCF contre France Télécom, la cour administrative d'appel de Paris a jugé que la SNCF ne pouvait être regardée comme ayant subi un dommage résultant de l'absence de perception de redevances d'occupation domaniale, dès lors qu'elle n'avait pas institué de telles redevances et que celles-ci n'étaient prévues par aucune disposition avant l'intervention de la loi du 26 juillet 1996 ; qu'en statuant ainsi, alors qu'il ressortait des pièces du dossier qui lui était soumis que la SNCF n'avait délivré aucun titre à l'exploitant public France Télécom au cours de la période en litige et que, si elle avait toléré la présence de cet exploitant, lui avait appliqué la convention pour l'établissement, l'entretien et l'exploitation des lignes de télécommunications, passée initialement avec l'Etat le 2 mai 1973 et avait conclu avec lui, le 7 février 1992, une convention-cadre relative à la mise à disposition réciproque de fibres optiques en emprise ferroviaire, ces circonstances ne pouvaient être regardées comme ayant conféré un titre à cet exploitant public, de sorte que celui-ci devait être regardé comme occupant sans titre du domaine public ferroviaire au cours de cette période, ayant ainsi causé un dommage devant être regardé comme constaté avant le 1er janvier 1997, au sens de l'article 6 de la loi du 13 février 1997, la cour a commis une erreur de droit ; que, dès lors, et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, la SNCF est fondée, pour ce motif, à demander l'annulation de l'arrêt attaqué ; <br/>
<br/>
              Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la société France Télécom le versement à la SNCF de la somme de 3 000 euros en application des dispositions de l'article L. 761-1 du code de justice administrative ; que ces dispositions font obstacle à ce que soit mis à la charge de la SNCF, qui n'est pas la partie perdante dans la présente instance, le versement d'une somme au titre des frais exposés par la société France Télécom et non compris dans les dépens ;<br/>
<br/>
<br/>
<br/>			D E C I D E :<br/>
              			--------------<br/>
<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Paris du 24 mai 2007 est annulé.<br/>
<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Paris.<br/>
<br/>
Article 3 : La société France Télécom versera à la SNCF la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : Les conclusions de la société France Télécom tendant à l'application des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 5 : La présente décision sera notifiée à la SOCIETE NATIONALE DES CHEMINS DE FER FRANÇAIS (SNCF) et à la société France Télécom.<br/>
Copie en sera adressée, pour information, à la ministre de l'écologie, du développement durable, des transports et du logement.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">24-01-02-01 DOMAINE. DOMAINE PUBLIC. RÉGIME. OCCUPATION. - OCCUPATION SANS DROIT NI TITRE - 1) FAUTE ENGAGEANT LA RESPONSABILITÉ DE L'OCCUPANT [RJ1] - 2) PASSIVITÉ OU COMPORTEMENT AMBIGU DE L'AUTORITÉ GESTIONNAIRE - CAUSE EXONÉRATOIRE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">24-01-02-04 DOMAINE. DOMAINE PUBLIC. RÉGIME. CONTENTIEUX DE LA RESPONSABILITÉ. - OCCUPATION SANS DROIT NI TITRE D'UNE DÉPENDANCE DOMANIALE - 1) FAUTE ENGAGEANT LA RESPONSABILITÉ DE L'OCCUPANT [RJ1] - 2) PASSIVITÉ OU COMPORTEMENT AMBIGU DE L'AUTORITÉ GESTIONNAIRE - CAUSE EXONÉRATOIRE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">51-02-004 POSTES ET COMMUNICATIONS ÉLECTRONIQUES. COMMUNICATIONS ÉLECTRONIQUES. - LIGNES TÉLÉPHONIQUES ET TÉLÉGRAPHIQUES IMPLANTÉES SUR LE DOMAINE PUBLIC FERROVIAIRE - INDEMNISATION DU DOMMAGE CAUSÉ PAR L'OCCUPATION SANS TITRE - BÉNÉFICIAIRE (ART. 6 DE LA LOI DU 13 FÉVRIER 1997).
</SCT>
<SCT ID="8D" TYPE="PRINCIPAL">65-01-005-05 TRANSPORTS. TRANSPORTS FERROVIAIRES. LIGNES DE CHEMIN DE FER. - OCCUPATION DU DOMAINE PUBLIC FERROVIAIRE PAR DES LIGNES TÉLÉPHONIQUES ET TÉLÉGRAPHIQUES - INDEMNISATION DU DOMMAGE CAUSÉ PAR L'OCCUPATION SANS TITRE - BÉNÉFICIAIRE (ART. 6 DE LA LOI DU 13 FÉVRIER 1997).
</SCT>
<SCT ID="8E" TYPE="PRINCIPAL">65-01-04 TRANSPORTS. TRANSPORTS FERROVIAIRES. RÉSEAU FERRÉ DE FRANCE. - DROITS TRANSFÉRÉS PAR L'ARTICLE 6 DE LA LOI DU 13 FÉVRIER 1997 - DROITS AFFÉRENTS AUX DOMMAGES CONSTATÉS AVANT LE 1ER JANVIER 1997 -  EXCLUSION - DROIT À INDEMNISATION DU DOMMAGE CAUSÉ PAR L'OCCUPATION SANS TITRE DU DOMAINE PUBLIC FERROVIAIRE PAR DES LIGNES TÉLÉPHONIQUES ET TÉLÉGRAPHIQUES AU COURS D'UNE PÉRIODE ANTÉRIEURE AU 1ER JANVIER 1997.
</SCT>
<ANA ID="9A"> 24-01-02-01 1) L'occupation sans droit ni titre d'une dépendance du domaine public constitue une faute commise par l'occupant qui l'oblige à réparer le dommage causé au gestionnaire de ce domaine par cette occupation irrégulière.,,2) Si l'autorité gestionnaire du domaine public n'a pas mis l'occupant irrégulier en demeure de quitter les lieux, ne l'a pas invité à régulariser sa situation ou a entretenu à son égard une ambiguïté sur la régularité de sa situation, ces circonstances sont de nature, le cas échéant, à constituer une cause exonératoire de la responsabilité de l'occupant, dans la mesure où ce comportement du gestionnaire serait constitutif d'une faute, mais elles ne sauraient faire obstacle, dans son principe, au droit du gestionnaire du domaine public à la réparation du dommage résultant de cette occupation irrégulière.</ANA>
<ANA ID="9B"> 24-01-02-04 1) L'occupation sans droit ni titre d'une dépendance du domaine public constitue une faute commise par l'occupant qui l'oblige à réparer le dommage causé au gestionnaire de ce domaine par cette occupation irrégulière.,,2) Si l'autorité gestionnaire du domaine public n'a pas mis l'occupant irrégulier en demeure de quitter les lieux, ne l'a pas invité à régulariser sa situation ou a entretenu à son égard une ambiguïté sur la régularité de sa situation, ces circonstances sont de nature, le cas échéant, à constituer une cause exonératoire de la responsabilité de l'occupant, dans la mesure où ce comportement du gestionnaire serait constitutif d'une faute, mais elles ne sauraient faire obstacle, dans son principe, au droit du gestionnaire du domaine public à la réparation du dommage résultant de cette occupation irrégulière.</ANA>
<ANA ID="9C"> 51-02-004 Aux termes du premier alinéa de l'article 6 de la loi n° 97-135 du 13 février 1997 portant création de l'établissement public « Réseau ferré de France » (RFF) : « Réseau ferré de France est substitué à la Société nationale des chemins de fer français pour les droits et obligations liés aux biens qui lui sont apportés, à l'exception de ceux afférents à des dommages constatés avant le 1er janvier 1997 (...) ». Le dommage occasionné au domaine public à raison de son occupation irrégulière se réalise et, par suite, est réputé se constater, eu égard à sa nature, au fur et à mesure de cette occupation. Il en résulte que l'indemnité tendant à réparer le préjudice subi par le gestionnaire du fait d'une occupation irrégulière du domaine public ferroviaire, au cours d'une période antérieure au 1er janvier 1997, est au nombre des droits mentionnés par cet article 6, afférents à des dommages constatés avant l'entrée en vigueur de la loi du 13 février 1997, et liés aux biens qui, remis en dotation à la SNCF en application de la loi du 30 décembre 1982, sont désormais apportés à RFF. Par suite, cet établissement public n'a pas été substitué à la SNCF pour l'exercice de ces droits.</ANA>
<ANA ID="9D"> 65-01-005-05 Aux termes du premier alinéa de l'article 6 de la loi n° 97-135 du 13 février 1997 portant création de l'établissement public « Réseau ferré de France » (RFF) : « Réseau ferré de France est substitué à la Société nationale des chemins de fer français pour les droits et obligations liés aux biens qui lui sont apportés, à l'exception de ceux afférents à des dommages constatés avant le 1er janvier 1997 (...) ». Le dommage occasionné au domaine public à raison de son occupation irrégulière se réalise et, par suite, est réputé se constater, eu égard à sa nature, au fur et à mesure de cette occupation. Il en résulte que l'indemnité tendant à réparer le préjudice subi par le gestionnaire du fait d'une occupation irrégulière du domaine public ferroviaire, au cours d'une période antérieure au 1er janvier 1997, est au nombre des droits mentionnés par cet article 6, afférents à des dommages constatés avant l'entrée en vigueur de la loi du 13 février 1997, et liés aux biens qui, remis en dotation à la SNCF en application de la loi du 30 décembre 1982, sont désormais apportés à RFF. Par suite, cet établissement public n'a pas été substitué à la SNCF pour l'exercice de ces droits.</ANA>
<ANA ID="9E"> 65-01-04 Aux termes du premier alinéa de l'article 6 de la loi n° 97-135 du 13 février 1997 portant création de l'établissement public « Réseau ferré de France » (RFF) : « Réseau ferré de France est substitué à la Société nationale des chemins de fer français pour les droits et obligations liés aux biens qui lui sont apportés, à l'exception de ceux afférents à des dommages constatés avant le 1er janvier 1997 (...) ». Le dommage occasionné au domaine public à raison de son occupation irrégulière se réalise et, par suite, est réputé se constater, eu égard à sa nature, au fur et à mesure de cette occupation. Il en résulte que l'indemnité tendant à réparer le préjudice subi par le gestionnaire du fait d'une occupation irrégulière du domaine public ferroviaire, au cours d'une période antérieure au 1er janvier 1997, est au nombre des droits mentionnés par cet article 6, afférents à des dommages constatés avant l'entrée en vigueur de la loi du 13 février 1997, et liés aux biens qui, remis en dotation à la SNCF en application de la loi du 30 décembre 1982, sont désormais apportés à RFF. Par suite, cet établissement public n'a pas été substitué à la SNCF pour l'exercice de ces droits.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1]Cf. CE, Section, 25 mars 1960, SNCF c/ Dame Barbey, n° 44533, p. 222.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
