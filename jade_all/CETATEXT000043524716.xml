<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043524716</ID>
<ANCIEN_ID>JG_L_2021_05_000000448288</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/52/47/CETATEXT000043524716.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère chambre, 21/05/2021, 448288, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-05-21</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>448288</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Arnaud Skzryerbak</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Marie Sirinelli</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:448288.20210521</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. F... E..., Mme D... G... et M. H... A... ont demandé au tribunal administratif de Versailles d'annuler les opérations électorales qui se sont déroulées le 15 mars 2020 pour l'élection des conseillers municipaux et communautaires de la commune de Chilly-Mazarin (Essonne). Par un jugement n° 2002257 du 1er décembre 2020, le tribunal administratif de Versailles a rejeté cette protestation.<br/>
<br/>
              Par une requête, enregistrée le 31 décembre 2020 au secrétariat du contentieux du Conseil d'Etat, Mme G... et M. A... demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) de faire droit à leur protestation.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code électoral ;<br/>
              - la loi n° 2020-290 du 23 mars 2020 ;<br/>
              - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Arnaud Skzryerbak, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Marie Sirinelli, rapporteure publique ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Mme G... et M. A... relèvent appel du jugement du 1er décembre 2020 par lequel le tribunal administratif de Versailles a rejeté leur protestation tendant à l'annulation des opérations électorales qui se sont déroulées le 15 mars 2020 pour l'élection des conseillers municipaux et communautaires de la commune de Chilly-Mazarin. A l'issue de ce scrutin, la liste conduite par Mme B... C... a recueilli 2 588 voix, soit 55,12 % des suffrages exprimés et la liste conduite par M. F... E..., 2 107 voix, soit 44,87 % des suffrages exprimés.<br/>
<br/>
              Sur la régularité du jugement :<br/>
<br/>
              2. Aux termes de l'article R. 611-8 du code des tribunaux administratifs : " Lorsqu'il lui apparaît au vu de la requête que la solution de l'affaire est d'ores et déjà certaine, le président du tribunal administratif ou le président de la formation de jugement (...) peut décider qu'il n'y a pas lieu à instruction ". <br/>
<br/>
              3. La circonstance qu'il ait été fait application de ces dispositions devant le tribunal administratif en dispensant d'instruction la protestation des requérants n'affecte pas le respect du caractère contradictoire de la procédure à leur égard et ne saurait, dès lors, être utilement invoquée par eux. Au demeurant, le président de la 5ème chambre du tribunal administratif de Versailles n'a pas fait une inexacte application des dispositions précitées en estimant, au vu de la protestation, que la solution du litige dont il était saisi par M. E..., Mme G... et M. A... était d'ores et déjà certaine et qu'il y avait lieu de statuer sans instruction sur cette protestation.<br/>
<br/>
              Sur le litige:<br/>
<br/>
              4. D'une part, l'émergence d'un nouveau coronavirus, responsable de la maladie à coronavirus 2019 ou covid-19 et particulièrement contagieux, a été qualifiée d'urgence de santé publique de portée internationale par l'Organisation mondiale de la santé le 30 janvier 2020, puis de pandémie le 11 mars 2020. La propagation du virus sur le territoire français a conduit le ministre des solidarités et de la santé à prendre, à compter du 4 mars 2020, des mesures de plus en plus strictes destinées à réduire les risques de contagion. Dans ce contexte, le Premier ministre a adressé à l'ensemble des maires le 7 mars 2020 une lettre présentant les mesures destinées à assurer le bon déroulement des élections municipales et communautaires prévues les 15 et 22 mars 2020. Ces mesures ont été précisées par une circulaire du ministre de l'intérieur du 9 mars 2020 relative à l'organisation des élections municipales des 15 et 22 mars 2020 en situation d'épidémie de coronavirus covid-19, formulant des recommandations relatives à l'aménagement des bureaux de vote et au respect des consignes sanitaires, et par une instruction de ce ministre, du même jour, destinée à faciliter l'exercice du droit de vote par procuration. Après consultation par le Gouvernement du conseil scientifique mis en place pour lui donner les informations scientifiques utiles à l'adoption des mesures nécessaires pour faire face à l'épidémie de covid-19, les 12 et 14 mars 2020, le premier tour des élections municipales a eu lieu comme prévu le 15 mars 2020.<br/>
<br/>
              5. D'autre part, aux termes de l'article L. 262 du code électoral, applicable aux communes de mille habitants et plus : " Au premier tour de scrutin, il est attribué à la liste qui a recueilli la majorité absolue des suffrages exprimés un nombre de sièges égal à la moitié du nombre des sièges à pourvoir, arrondi, le cas échéant, à l'entier supérieur lorsqu'il y a plus de quatre sièges à pourvoir et à l'entier inférieur lorsqu'il y a moins de quatre sièges à pourvoir. Cette attribution opérée, les autres sièges sont répartis entre toutes les listes à la représentation proportionnelle suivant la règle de la plus forte moyenne, sous réserve de l'application des dispositions du troisième alinéa ci-après. / Si aucune liste n'a recueilli la majorité absolue des suffrages exprimés au premier tour, il est procédé à un deuxième tour (...) ".<br/>
<br/>
              6. Ni par ces dispositions, ni par celles de la loi du 23 mars 2020 d'urgence pour faire face à l'épidémie de covid-19, le législateur n'a subordonné à un taux de participation minimal la répartition des sièges au conseil municipal dans les communes de mille habitants et plus lorsqu'une liste a recueilli la majorité absolue des suffrages exprimés à l'issue du premier tour de scrutin. Le niveau de l'abstention n'est ainsi, par lui-même, pas de nature à remettre en cause les résultats du scrutin, s'il n'a pas altéré, dans les circonstances de l'espèce, sa sincérité.<br/>
<br/>
              7. En l'espèce, Mme G... et M. A... soutiennent que l'abstention, résultant de la crise sanitaire, aurait été particulièrement marquée dans les classes d'âge d'électeurs favorables à la liste conduite par M. E..., sur laquelle ils figuraient. S'ils produisent un certain nombre d'attestations d'électeurs selon lesquelles ceux-ci se seraient abstenus de voter en raison du contexte sanitaire, ils n'invoquent aucune autre circonstance relative au déroulement de la campagne électorale ou du scrutin dans la commune qui montrerait, en particulier, qu'il aurait été porté atteinte au libre exercice du droit de vote ou à l'égalité entre les candidats. Dans ces conditions, le niveau de l'abstention constatée ne peut être regardé comme ayant altéré la sincérité du scrutin.<br/>
<br/>
              8. Il résulte de tout ce qui précède que Mme G... et M. A... ne sont pas fondés à se soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Versailles a rejeté leur protestation.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de Mme G... et M. A... est rejetée.<br/>
Article 2 : La présente décision sera notifiée à Mme D... G... et à M. H... A..., à Mme B... C..., ainsi qu'au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
