<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028272363</ID>
<ANCIEN_ID>JG_L_2013_12_000000352399</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/27/23/CETATEXT000028272363.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  3ème sous-section jugeant seule, 04/12/2013, 352399, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-12-04</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>352399</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 3ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>BALAT ; SCP ODENT, POULET</AVOCATS>
<RAPPORTEUR>M. Christophe Pourreau</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emmanuelle Cortot-Boucher</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2013:352399.20131204</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi, enregistré le 5 septembre 2011 au secrétariat du contentieux du Conseil d'Etat, présenté pour l'établissement national des produits de l'agriculture et de la mer (FranceAgriMer), dont le siège est 12, rue Henri-Rol-Tanguy, TSA 20002, à Montreuil-sous-Bois Cedex (93555) ; FranceAgriMer demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 09NT01572 du 23 juin 2011 par lequel la cour administrative d'appel de Nantes a rejeté l'appel qu'il a interjeté du jugement n° 04-2697 du 19 mai 2009 par lequel le tribunal administratif de Rennes, faisant droit à la demande de la société coopérative agricole Solarenn, a annulé le titre de recettes, d'un montant de 64 347 euros, émis le 16 mars 2004 par le directeur de l'office national interprofessionnel des fruits, des légumes et de l'horticulture (Oniflhor) à l'encontre de cette société ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de la société coopérative agricole Solarenn la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le traité sur le fonctionnement de l'Union européenne ;<br/>
<br/>
              Vu le règlement (CE) n° 4045/89 du Conseil du 21 décembre 1989 ;<br/>
<br/>
              Vu le règlement (CE) n° 3094/94 du Conseil du 12 décembre 1994 ;<br/>
<br/>
              Vu le règlement (CE, Euratom) n° 2988/95 du Conseil du 18 décembre 1995 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Christophe Pourreau, Maître des Requêtes,  <br/>
<br/>
              - les conclusions de Mme Emmanuelle Cortot-Boucher, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Balat, avocat de Franceagrimer et à la SCP Odent, Poulet, avocat de la société coopérative agricole Solarenn ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond qu'à l'issue d'un contrôle intervenu en novembre 2001, réalisé par l'Agence centrale des organismes d'intervention dans le secteur agricole sur le fondement du règlement (CEE) n° 4045/89 du Conseil du 21 décembre 1989 relatif aux contrôles, par les Etats membres, des opérations faisant partie du système de financement par le FEOGA, section " garantie ", le directeur de l'Office national interprofessionnel des fruits, des légumes et de l'horticulture (Oniflhor) a émis le 16 mars 2004 à l'encontre de la société coopérative agricole Solarenn, organisation de producteurs, un titre de perception d'un montant de 64 347 euros, représentant une partie du montant de l'aide financière versée à la société au titre du fonds opérationnel 1999, majorée partiellement de la pénalité de 20 % prévue par l'article 15  du règlement (CE) n° 609/2001 du 28 mars 2001 portant modalités d'application du règlement (CE) n° 2200/96 du Conseil en ce qui concerne les programmes opérationnels, les fonds opérationnels et l'aide financière communautaire ; que le tribunal administratif de Rennes, par un jugement du 5 juin 2007, a annulé ce titre de perception ; que l'établissement national des produits de l'agriculture et de la mer (FranceAgriMer), venu aux droits de l'Office national interprofessionnel des fruits, des légumes, des vins et de l'horticulture (Viniflhor), lui-même venu aux droits de l'Oniflhor, se pourvoit en cassation contre l'arrêt du 23 juin 2011 par lequel la cour administrative d'appel de Nantes a rejeté l'appel qu'il a interjeté de ce jugement ; <br/>
<br/>
              2. Considérant qu'aux termes de l'article 2 du règlement (CEE) n° 4045/89 du Conseil du 21 décembre 1989 relatif aux contrôles, par les Etats membres, des opérations faisant partie du système de financement par le FEOGA, section " garantie ", dans sa rédaction issue du règlement (CE) n° 3094/94 du Conseil du 12 décembre 1994 : " 1. Les Etats membres procèdent à des contrôles des documents commerciaux des entreprises (...). / 2. (...) Pour chaque période de contrôle (...), les Etats membres sélectionnent les entreprises à contrôler (...). / Les Etats membres soumettent à la Commission leur proposition relative à l'utilisation de l'analyse des risques. Cette proposition (...) est présentée au plus tard le 1er décembre de l'année précédant le début de la période de contrôle à laquelle elle doit s'appliquer (...). / 4. La période de contrôle se situe entre le 1er juillet et le 30 juin de l'année suivante. / Le contrôle porte sur une période d'au moins douze mois s'achevant au cours de la période de contrôle précédente ; il peut être étendu pour des périodes, à déterminer par l'Etat membre, précédant ou suivant la période de douze mois. " ;<br/>
<br/>
              3. Considérant qu'il résulte de l'arrêt FranceAgriMer de la Cour de justice de l'Union européenne du 13 juin 2013, C-671/11 à C-676/11, que les dispositions précitées du second alinéa du paragraphe 4 de l'article 2 du règlement (CE) n° 4045/89 doivent être interprétées en ce sens que, dès lors que cet article ne confère pas aux opérateurs un droit leur permettant de s'opposer à des contrôles autres ou plus étendus que ceux qu'il prévoit, la seule circonstance qu'un contrôle porte uniquement sur une période s'achevant antérieurement à la période de contrôle précédant celle durant laquelle il est effectué n'est pas de nature à rendre ce contrôle irrégulier à l'égard des opérateurs contrôlés ;<br/>
<br/>
              4. Considérant qu'il suit de là qu'en jugeant, après avoir relevé que la société coopérative agricole Solarenn avait fait l'objet, en novembre 2001, d'un contrôle portant sur l'année 1999, que ce contrôle était irrégulier au motif qu'il avait porté sur une période s'achevant antérieurement à la période de contrôle précédant celle durant laquelle il a été effectué, la cour administrative d'appel de Nantes a commis une erreur de droit ; que son arrêt doit, pour ce motif, être annulé ; <br/>
<br/>
              5. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de FranceAgriMer, qui n'est pas, dans la présente instance, la partie perdante ; qu'en revanche, il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la société coopérative agricole Solarenn la somme de 3 000 euros à verser à FranceAgriMer au titre des mêmes dispositions ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Nantes du 23 juin 2011 est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Nantes.<br/>
Article 3 : La société coopérative agricole Solarenn versera une somme de 3 000 euros à FranceAgriMer au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Les conclusions présentées par la société coopérative agricole Solarenn au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée à l'établissement national des produits de l'agriculture et de la mer (FranceAgriMer) et à la société coopérative agricole Solarenn.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
