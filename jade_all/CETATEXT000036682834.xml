<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036682834</ID>
<ANCIEN_ID>JG_L_2018_03_000000403309</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/68/28/CETATEXT000036682834.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère chambre, 07/03/2018, 403309</TITRE>
<DATE_DEC>2018-03-07</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>403309</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MATUCHANSKY, POUPOT, VALDELIEVRE</AVOCATS>
<RAPPORTEUR>M. Frédéric Pacoud</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Charles Touboul</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:403309.20180307</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société polyclinique Vauban a demandé au tribunal administratif de Lille d'annuler la décision du 14 janvier 2013 par laquelle le directeur général de l'agence régionale de santé du Nord-Pas-de-Calais lui a infligé une sanction financière d'un montant de 70 000 euros. Par un jugement n° 1301516 du 1er avril 2015, le tribunal administratif de Lille a annulé cette décision.<br/>
<br/>
              Par un arrêt n° 15DA01390 du 5 juillet 2016, la cour administrative d'appel de Douai a rejeté l'appel formé contre ce jugement par le ministre des affaires sociales, de la santé et des droits des femmes.<br/>
<br/>
              Par un pourvoi, enregistré le 7 septembre 2016 au secrétariat du contentieux du Conseil d'Etat, le ministre des affaires sociales et de la santé demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de la sécurité sociale ;<br/>
              - l'arrêté du 27 février 2007 relatif à la classification et à la prise en charge des prestations d'hospitalisation pour les activités de médecine, chirurgie, obstétrique et odontologie et pris en application de l'article L. 162-22-6 du code de la sécurité sociale ;<br/>
              - l'arrêté du 25 février 2008 modifiant l'arrêté du 27 février 2007 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Frédéric Pacoud, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Charles Touboul, rapporteur public.<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que, par une décision du 14 janvier 2013, le directeur général de l'agence régionale de santé du Nord-Pas-de-Calais a infligé à la société polyclinique Vauban une sanction financière d'un montant de 70 000 euros, prise sur le fondement de l'article L. 162-22-18 du code de la sécurité sociale, à la suite d'un contrôle de la facturation des séjours de l'année 2008. Par un jugement du 1er avril 2015, le tribunal administratif de Lille a annulé cette décision au motif que l'agence régionale de santé avait commis une erreur de droit en portant, pour prononcer cette sanction financière, une appréciation sur la pertinence des soins dispensés. Le ministre des affaires sociales et de la santé se pourvoit en cassation contre l'arrêt du 5 juillet 2016 par lequel la cour administrative d'appel de Douai a rejeté son appel contre le jugement du tribunal administratif de Lille.<br/>
<br/>
              2. L'article L. 162-22-18 du code de la sécurité sociale, dans sa rédaction applicable à la décision en litige, dispose que : " Les établissements de santé sont passibles, après qu'ils ont été mis en demeure de présenter leurs observations, d'une sanction financière en cas de manquement aux règles de facturation fixées en application des dispositions de l'article L. 162-22-6, d'erreur de codage ou d'absence de réalisation d'une prestation facturée. / Cette sanction est prise par le directeur général de l'agence régionale de santé, à la suite d'un contrôle réalisé sur pièces et sur place par les médecins inspecteurs de santé publique (...) ou les praticiens-conseils des organismes d'assurance maladie en application du programme de contrôle régional (...) ". Selon le dernier alinéa de l'article L. 162-22-6 du même code, un décret en Conseil d'Etat précise les catégories de prestations d'hospitalisation sur la base desquelles les ministres chargés de la santé et de la sécurité sociale arrêtent la classification des prestations, tenant compte notamment des moyens techniques, matériels et humains mis en oeuvre pour la prise en charge des patients, qui donnent lieu à une prise en charge par les régimes obligatoires de sécurité sociale, et les modalités de facturation des prestations d'hospitalisation faisant l'objet d'une prise en charge par l'assurance maladie. <br/>
<br/>
              3. Il résulte des dispositions des articles L. 162-22-18 et L. 162-22-6 du code de la sécurité sociale que le directeur général d'une agence régionale de santé peut prendre à l'encontre d'un établissement de santé une sanction financière lorsqu'un contrôle de la tarification à l'activité réalisé dans cet établissement met en évidence des manquements aux règles de facturation fixées en application des dispositions de l'article L. 162-22-6, des erreurs de codage ou l'absence de réalisation de prestations facturées. Ce contrôle, qui porte sur la réalité des prestations facturées et la correcte application des règles de codage et de facturation, exclut toute appréciation quant à la pertinence médicale des soins dispensés aux patients. <br/>
<br/>
              4. Aux termes de l'article 6 de l'arrêté du 27 février 2007 relatif à la classification et à la prise en charge des prestations d'hospitalisation, pris en application de l'article R. 162-31-3 du code de la sécurité sociale, dans leur rédaction applicable à la date des activités sur lesquelles a porté le contrôle fondant la décision de sanction du 14 janvier 2013 : " (...) / I.- Les forfaits " groupes homogènes de séjours " sont facturés dans les conditions suivantes : / (...) 3° La prise en charge du patient dans une unité d'hospitalisation de courte durée définie au 4° de l'article D. 6124-22 du code de la santé publique, non suivie d'une hospitalisation dans un service de médecine, chirurgie, obstétrique ou odontologie, donne lieu, quelle que soit la durée de séjour dans cette unité, à facturation d'un GHS correspondant à un GHM de la catégorie majeure 24 définie à l'annexe I de l'arrêté du 22 février 2008 susvisé, lorsque, à l'issue de son passage dans l'espace d'examen et de soins de la structure des urgences, l'état de santé du patient : / - présente un caractère instable ou que le diagnostic reste incertain ; / - nécessite une surveillance médicale et un environnement paramédical qui ne peuvent être délivrés que dans le cadre d'une hospitalisation ; / - nécessite la réalisation d'examens complémentaires ou d'actes thérapeutiques. / Lorsque l'une de ces conditions n'est pas remplie, la prise en charge du patient donne lieu à facturation du forfait ATU mentionné au I de l'article 6 ".<br/>
<br/>
              5. Il ressort des pièces du dossier soumis aux juges du fond que la décision de sanction litigieuse est motivée par des manquements aux règles de facturation et des erreurs de codage relevés lors du contrôle effectué sur les facturations établies notamment au titre des prestations classées dans quatre groupes homogènes de malades relevant de la catégorie majeure 24, correspondant aux séjours de moins de deux jours. Sur les 308 anomalies constatées, 240 portaient, s'agissant de trois de ces groupes, sur des actes réalisés au service des urgences facturés au titre d'une hospitalisation de courte durée alors que, selon les constatations du contrôle, ils auraient dû l'être, en vertu de l'article 6 de l'arrêté du 27 février 2007, au titre d'un forfait " accueil et traitement des urgences " ou " ATU ". Il ressort de la motivation de la décision litigieuse ainsi que du rapport de synthèse établi à l'issue du contrôle que les facturations regardées comme anormales l'ont été au motif que les actes réalisés portaient sur " une pathologie ne relevant pas médicalement d'une prise en charge en hospitalisation de courte durée, aucune trace n'ayant été retrouvée dans le dossier médical de la nécessité ni de la réalisation d'une surveillance spécifique ", ou sur une " pathologie susceptible de justifier une prise en charge dans les lits de la zone de surveillance de très courte durée mais pour lesquels aucune trace de surveillance n'a été retrouvée au dossier ". Si une partie de ces anomalies a été cursivement répertoriée, dans les tableaux de synthèse établis lors du contrôle, sous l'expression de " prestation non médicalement justifiée ", il ressort du rapport de synthèse que les contrôleurs se sont ainsi bornés à vérifier la cohérence de la facturation avec le contenu du dossier médical établi par la polyclinique. Enfin, les constats dits " supplémentaires " énumérés en annexe de ce rapport n'ont pas servi de fondement à la sanction prononcée. En retenant ces manquements, l'administration n'a pas remis en cause la pertinence de la prise en charge médicale des patients mais estimé, au vu des éléments figurant aux dossiers médicaux, que les conditions posées par l'article 6 de l'arrêté du 27 février 2007 pour la facturation d'un groupe homogène de séjour correspondant à un groupe homogène de malades de la catégorie majeure 24 n'étaient pas remplies. Par suite, la cour a inexactement qualifié les faits de l'espèce en jugeant que de tels manquements n'étaient pas au nombre de ceux que le directeur général de l'agence régionale de santé de Nord-Pas-de-Calais pouvait retenir pour prendre une sanction sur le fondement des dispositions précitées de l'article L. 162-22-18 du code de la sécurité sociale. <br/>
<br/>
              6. Il résulte de ce qui précède que le ministre des affaires sociales et de la santé est fondé à demander l'annulation de l'arrêt qu'il attaque. <br/>
<br/>
              7. Les conclusions de la société polyclinique Vauban présentées au titre de l'article L. 761-1 du code de justice administrative ne peuvent, dès lors, qu'être rejetées. <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Douai du 5 juillet 2016 est annulé. <br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Douai.<br/>
Article 3 : Les conclusions de la société polyclinique Vauban présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée à la ministre des solidarités et de la santé et à la société polyclinique Vauban. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">61-06-02-01-01 SANTÉ PUBLIQUE. ÉTABLISSEMENTS PUBLICS DE SANTÉ. FONCTIONNEMENT. FINANCEMENT. - SANCTIONS FINANCIÈRES EN CAS DE MANQUEMENTS AUX RÈGLES DE TARIFICATION DES SOINS - NATURE DES MANQUEMENTS SUSCEPTIBLES D'ÊTRE SANCTIONNÉS - NON RESPECT DES RÈGLES DE FACTURATION, ERREUR DE CODAGE OU ABSENCE DE RÉALISATION D'UNE PRESTATION FACTURÉE, À L'EXCLUSION DE TOUTE APPRÉCIATION DE LA PERTINENCE MÉDICALE DES SOINS DISPENSÉS.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">61-09-02-01 SANTÉ PUBLIQUE. ADMINISTRATION DE LA SANTÉ. - SANCTIONS FINANCIÈRES PRISES PAR LE DIRECTEUR GÉNÉRAL D'UNE ARS À L'ENCONTRE DES ÉTABLISSEMENTS DE SANTÉ EN CAS DE MANQUEMENTS AUX RÈGLES DE TARIFICATION DES SOINS - NATURE DES MANQUEMENTS SUSCEPTIBLES D'ÊTRE SANCTIONNÉS - NON RESPECT DES RÈGLES DE FACTURATION, ERREUR DE CODAGE OU ABSENCE DE RÉALISATION D'UNE PRESTATION FACTURÉE, À L'EXCLUSION DE TOUTE APPRÉCIATION DE LA PERTINENCE MÉDICALE DES SOINS DISPENSÉS.
</SCT>
<ANA ID="9A"> 61-06-02-01-01 Il résulte des dispositions des articles L. 162-22-18 (transféré à l'art. L. 162-23-13 par la loi n° 2015-1702 du 21 décembre 2015) et L. 162-22-6 du code de la sécurité sociale (CSS) que le directeur général d'une agence régionale de santé (ARS) peut prendre à l'encontre d'un établissement de santé une sanction financière lorsqu'un contrôle de la tarification à l'activité réalisé dans cet établissement met en évidence des manquements aux règles de facturation fixées en application des dispositions de l'article L. 162-22-6, des erreurs de codage ou l'absence de réalisation de prestations facturées. Ce contrôle, qui porte sur la réalité des prestations facturées et la correcte application des règles de codage et de facturation, exclut toute appréciation quant à la pertinence médicale des soins dispensés aux patients. En l'espèce, pas de remise en cause de la pertinence de la prise en charge médicale des patients.</ANA>
<ANA ID="9B"> 61-09-02-01 Il résulte des dispositions des articles L. 162-22-18 (transféré à l'art. L. 162-23-13 par la loi n° 2015-1702 du 21 décembre 2015) et L. 162-22-6 du code de la sécurité sociale (CSS) que le directeur général d'une agence régionale de santé (ARS) peut prendre à l'encontre d'un établissement de santé une sanction financière lorsqu'un contrôle de la tarification à l'activité réalisé dans cet établissement met en évidence des manquements aux règles de facturation fixées en application des dispositions de l'article L. 162-22-6, des erreurs de codage ou l'absence de réalisation de prestations facturées. Ce contrôle, qui porte sur la réalité des prestations facturées et la correcte application des règles de codage et de facturation, exclut toute appréciation quant à la pertinence médicale des soins dispensés aux patients. En l'espèce, pas de remise en cause de la pertinence de la prise en charge médicale des patients.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
