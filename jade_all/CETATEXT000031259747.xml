<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031259747</ID>
<ANCIEN_ID>JG_L_2015_09_000000386360</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/25/97/CETATEXT000031259747.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème SSJS, 30/09/2015, 386360, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-09-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>386360</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème SSJS</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP POTIER DE LA VARDE, BUK LAMENT</AVOCATS>
<RAPPORTEUR>M. Bruno Bachini</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Gaëlle Dumortier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2015:386360.20150930</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés les 10 décembre 2014 et 10 septembre 2015 au secrétariat du contentieux du Conseil d'Etat, la commune de Nice, représentée par son maire, demande au Conseil d'Etat :  <br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision implicite née du silence gardé par le Premier ministre sur sa demande du 12 septembre 2014 tendant à l'abrogation du décret n° 2013-77 du 24 janvier 2013 relatif à l'organisation du temps scolaire dans les écoles maternelles et élémentaires et du décret n° 2014-457 du 7 mai 2014 portant autorisation d'expérimentations relatives à l'organisation des rythmes scolaires dans les écoles maternelles et élémentaires ; <br/>
<br/>
              2°) d'enjoindre au Premier ministre d'abroger ces décrets dans le délai d'un mois ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la Constitution, notamment son article 72-2 ;<br/>
              - le code de l'éducation ;<br/>
              - la loi n° 2000-321 du 12 avril 2000 ;<br/>
              - la loi n° 2013-595 du 8 juillet 2013, notamment son article 32 ;<br/>
              - le décret n° 2011-184 du 15 février 2011 ;<br/>
              - le décret n° 2013-77 du 24 janvier 2013 ;<br/>
              - le décret n° 2014-457 du 7 mai 2014 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Bruno Bachini, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Gaëlle Dumortier, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Potier de la Varde, Buk Lament, avocat de la Commune de Nice ;<br/>
<br/>
<br/>
<br/>
<br/>Sur la décision implicite du Premier ministre refusant d'abroger le décret du 24 janvier 2013 : <br/>
<br/>
              1. Considérant que le décret du 24 janvier 2013, dont l'abrogation a été demandée au Premier ministre par la commune requérante, modifie l'organisation de la semaine scolaire des écoles maternelles et élémentaires, régie par les articles D. 521-10 à D. 521-13 du code de l'éducation ; qu'il prévoit que cette semaine comporte vingt-quatre heures d'enseignement, réparties sur neuf demi-journées, et que le conseil d'école, la commune ou l'établissement public de coopération intercommunale peuvent transmettre un projet d'organisation de la semaine scolaire au directeur académique des services de l'éducation nationale qui est chargé, par délégation du recteur d'académie, d'arrêter l'organisation de la semaine scolaire dans chaque école du département dont il a la charge ; qu'aux termes du deuxième alinéa de l'article D. 521-10 du code de l'éducation, dans sa rédaction issue de ce décret : " Les heures d'enseignement sont organisées les lundi, mardi, jeudi et vendredi et le mercredi matin, à raison de cinq heures trente maximum par jour et de trois heures trente maximum par demi-journée " ; que les élèves peuvent, en vertu de l'article D. 521-13 du code, bénéficier d'activités pédagogiques complémentaires arrêtées par l'inspecteur de l'éducation nationale et destinées à aider les élèves rencontrant des difficultés à accompagner leur travail personnel ou à les encadrer dans le cadre d'une activité prévue par le projet d'école ; <br/>
<br/>
              2. Considérant qu'aux termes de l'article L. 230-2, alors en vigueur, du code de l'éducation : " Le Haut Conseil de l'éducation émet un avis et peut formuler des propositions à la demande du ministre chargé de l'éducation nationale sur les questions relatives à la pédagogie, aux programmes, aux modes d'évaluation des connaissances des élèves, à l'organisation et aux résultats du système éducatif et à la formation des enseignants (...) " ; qu'il résulte de ces dispositions que le Haut Conseil de l'éducation ne devait rendre ses avis que sur demande du ministre chargé de l'éducation nationale ; que, par suite, la commune requérante n'est, en tout état de cause, pas fondée à soutenir que l'adoption de ce décret relatif à l'organisation de la semaine scolaire aurait dû, à peine d'illégalité, être précédée de la consultation de cette instance ;<br/>
<br/>
              3. Considérant qu'il ressort des pièces du dossier, et notamment du décompte des votes mentionné dans l'avis rendu le 8 janvier 2013 par le Conseil supérieur de l'éducation ainsi que de l'attestation de passage au comité technique ministériel institué auprès du ministre chargé de l'éducation nationale en date du 11 janvier 2013, que les règles de quorum applicables à ces deux instances, sur les fondements respectifs de l'article R. 231-12 du code de l'éducation et de l'article 46 du décret du 15 février 2011 relatif aux comités techniques dans les administrations de l'Etat, ont été respectées en l'espèce ; que, dès lors, le moyen tiré de ce que le décret aurait été pris au terme d'une procédure irrégulière doit être écarté ;  <br/>
<br/>
              4. Considérant qu'il ne ressort pas des pièces du dossier que le décret litigieux aurait, par ses effets sur le bien-être des élèves, méconnu les objectifs généraux de l'éducation découlant des articles L. 111-1, L. 111-2, L. 121-1 et L. 122-1-1 du code de l'éducation ; <br/>
<br/>
              5. Considérant que le décret dont l'abrogation a été demandée a pour seul objet de répartir un nombre d'heures d'enseignement inchangé sur neuf demi-journées au lieu des huit demi-journées prévues par la réglementation antérieure et n'a que des conséquences limitées sur les dépenses liées à l'utilisation des bâtiments scolaires dès lors que le nombre d'heures d'enseignement hebdomadaire reste inchangé ; qu'il n'opère ainsi aucun transfert de compétences vers les communes qui aurait impliqué, en vertu de l'article 72-2 de la Constitution, une compensation financière ; qu'il ne ressort pas davantage des pièces du dossier que le pouvoir réglementaire aurait commis une erreur manifeste dans l'appréciation des conséquences financières de ces nouvelles dispositions ; <br/>
<br/>
              Sur la décision implicite du Premier ministre refusant d'abroger le décret du 7 mai 2014 :<br/>
<br/>
              6. Considérant que le décret du 7 mai 2014, dont l'abrogation a été également demandée au Premier ministre par la commune requérante, prévoit la possibilité pour le recteur d'académie d'autoriser à titre expérimental, pour une durée de trois ans et sur proposition conjointe d'une commune ou d'un établissement public de coopération intercommunale et d'un ou plusieurs conseils d'école, des adaptations à l'organisation de la semaine scolaire dans les écoles maternelles et élémentaires dérogeant aux dispositions des premier, deuxième et quatrième alinéas de l'article D. 521-10 du code de l'éducation issues du décret du 24 janvier 2013 relatif à l'organisation du temps scolaire dans les écoles maternelles et élémentaires ainsi qu'aux dispositions de l'article D. 521-2 du même code ;<br/>
<br/>
              7. Considérant que les dispositions de l'article L. 230-2 du code de l'éducation citées au point 2, relatives à la procédure de consultation préalable du Haut Conseil de l'éducation, ont été abrogées par l'article 32 de la loi du 8 juillet 2013 d'orientation et de programmation pour la refondation de l'école de la République et n'étaient, dès lors, plus applicables à la date de l'adoption du décret ; que, par suite, le moyen tiré de ce que l'absence d'une telle consultation aurait entaché d'illégalité ce décret ne peut, en tout état de cause, qu'être écarté ; <br/>
<br/>
              8. Considérant qu'il ressort des pièces du dossier, et notamment du décompte des votes mentionné dans l'avis rendu le 5 mai 2014 par le Conseil supérieur de l'éducation ainsi que de l'attestation de passage au comité technique ministériel institué auprès du ministre chargé de l'éducation nationale en date du 5 mai 2014, que les règles de quorum applicables à ces deux instances, sur les fondements respectifs de l'article R. 231-12 du code de l'éducation et de l'article 46 du décret du 15 février 2011 relatif aux comités techniques dans les administrations de l'Etat, ont été respectées en l'espèce ; que, dès lors, le moyen tiré de ce que le décret aurait été pris au terme d'une procédure irrégulière doit être écarté ; <br/>
<br/>
              9. Considérant que le décret attaqué a, comme il a été dit plus haut, pour seul objet d'introduire la possibilité, sur autorisation du recteur saisi d'une proposition en ce sens, de déroger, à titre expérimental et pour une durée limitée, aux dispositions de droit commun régissant l'organisation des rythmes scolaires dans les écoles maternelles et élémentaires ; qu'il n'opère ainsi aucun transfert de compétences de l'Etat vers les communes impliquant une compensation financière au titre de l'article 72-2 de la Constitution ; qu'eu égard à son objet, il ne saurait davantage avoir méconnu les objectifs généraux de l'éducation mentionnés au point 4 ni être entaché d'une erreur manifeste dans l'appréciation de l'effet des dispositions auxquelles il permet de déroger sur la situation financière des communes ; <br/>
<br/>
              10. Considérant qu'il résulte de tout ce qui précède que la commune requérante n'est fondée à soutenir ni que les décisions qu'elle attaque méconnaissent le principe, rappelé par l'article 16-1 de la loi du 12 avril 2000 sur les droits des citoyens dans leurs relations avec l'administration, selon lequel l'autorité administrative est tenue de faire droit à une demande d'abrogation d'un règlement illégal, ni à demander l'annulation de ces refus ; que ses conclusions à fin d'injonction ne peuvent, par suite, qu'être également rejetées ;  <br/>
<br/>
              Sur les conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative : <br/>
<br/>
              11. Considérant que ces dispositions font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante ; <br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er : La requête de la commune de Nice est rejetée.<br/>
<br/>
Article 2 : La présente décision sera notifiée à la commune de Nice et à la ministre de l'éducation nationale, de l'enseignement supérieur et de la recherche.<br/>
Copie en sera adressée au Premier ministre.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
