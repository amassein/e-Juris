<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032571718</ID>
<ANCIEN_ID>JG_L_2016_05_000000385383</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/57/17/CETATEXT000032571718.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère - 6ème chambres réunies, 20/05/2016, 385383, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-05-20</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>385383</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère - 6ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ODENT, POULET</AVOCATS>
<RAPPORTEUR>Mme Florence Marguerite</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Jean Lessi</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2016:385383.20160520</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés les 28 octobre 2014 et 15 septembre 2015 au secrétariat du contentieux du Conseil d'Etat, la société Stallergènes demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret n° 2014-1022 du 8 septembre 2014 relatif aux conditions de prise en charge par l'assurance maladie et de fixation du prix des allergènes préparés spécialement pour un seul individu ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de la santé publique ;<br/>
              - le code de la sécurité sociale ; <br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Florence Marguerite, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Jean Lessi, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Odent, Poulet, avocat de la société Stallergènes ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>Sur les consultations :<br/>
<br/>
              1. Considérant que le décret attaqué a été pris après consultation de la section sociale du Conseil d'Etat ; qu'il ressort des pièces produites par le ministre des affaires sociales, de la santé et des droits des femmes que ce décret ne contient aucune disposition différant à la fois de celles qui figuraient dans le projet soumis par le Gouvernement au Conseil d'Etat et de celles qui ont été adoptées par ce dernier ; que, dès lors, la société requérante n'est pas fondée à soutenir que les règles qui gouvernent l'examen par le Conseil d'Etat des projets de décret ont été méconnues ;<br/>
<br/>
              2. Considérant qu'aucune disposition législative ou réglementaire ne faisait obligation de procéder à la consultation du conseil d'administration de la caisse nationale du régime social des indépendants ; qu'une telle obligation ne résulte pas davantage de la circonstance que le Gouvernement a consulté le conseil central d'administration de la mutualité sociale agricole, conformément au II bis de l'article L. 723-12 du code rural et de la pêche maritime ; que, par suite, le moyen tiré du défaut de consultation du conseil d'administration de la caisse nationale du régime social des indépendants doit être écarté ;<br/>
<br/>
              Sur l'article 1er du décret attaqué : <br/>
<br/>
              3. Considérant qu'aux termes des deux premiers alinéas de l'article L. 162-16-4-1 du code de la sécurité sociale : " Le prix de vente au public des allergènes spécialement préparés pour un seul individu, (...) pris en charge par les organismes d'assurance maladie, est établi par convention entre la personne autorisée à les préparer et à les délivrer et le Comité économique des produits de santé ou, à défaut, par décision du comité, sauf opposition conjointe des ministres chargés de la santé et de la sécurité sociale, qui arrêtent dans ce cas le prix dans un délai de quinze jours après la décision du comité. / La fixation de ce prix tient principalement compte des prix des produits comparables, des volumes de vente prévus ou constatés et des conditions prévisibles et réelles d'utilisation " ; que le troisième alinéa du même article renvoie à un décret en Conseil d'Etat la détermination des procédures et délais de fixation du prix ; qu'aux termes du deuxième alinéa du I de l'article R. 163-14-1 du même code, issu du décret attaqué, la demande de fixation du prix de vente " est adressée au comité économique des produits de santé, accompagnée d'un dossier comportant les éléments définis par ce comité et dont la liste est publiée sur son site internet ainsi que les informations nécessaires à la négociation de la convention mentionnée à l'article L. 162-16-4-1 du présent code et à la fixation du prix " ; que le renvoi ainsi opéré à ce comité, organe administratif doté par la loi du pouvoir de fixer le prix des médicaments et autres produits de santé, du soin de définir les éléments devant composer le dossier de demande de fixation du prix de vente ne porte que sur les modalités d'instruction de décisions dont les conditions d'intervention sont fixées par le décret attaqué ; que, dès lors, le Premier ministre n'a pas illégalement délégué la compétence qu'il tenait des dispositions législatives précitées ; que la société requérante ne peut, en tout état de cause, utilement se prévaloir des éventuelles illégalités qui affecteraient la définition des éléments du dossier arrêtée par le comité économique des produits de santé en application de cette délégation, pour contester la légalité de celle-ci ;<br/>
<br/>
              4. Considérant que le dernier alinéa de l'article L. 162-16-4-1 du code de la sécurité sociale renvoie également à un décret en Conseil d'Etat le soin de déterminer " les règles selon lesquelles certains de ces allergènes peuvent être exclus de la prise en charge par l'assurance maladie " ; qu'aux termes de l'article R. 163-14-3, dans sa rédaction issue de l'article 1er du décret attaqué : " I.- Les allergènes préparés spécialement pour un seul individu sont pris en charge par l'assurance maladie, conformément au 11° de l'article R. 322-1, sur prescription médicale, sauf lorsque ces allergènes remplissent au moins l'un des critères suivants : / 1° Allergènes utilisés à des fins diagnostiques et pris en charge ou remboursés par l'assurance maladie au titre des actes inscrits sur la liste prévue à l'article L. 162-1-7 ; / 2° Allergènes réalisés à l'aide d'extraits allergéniques de préparations mères ne disposant pas de l'autorisation prévue à l'article L. 4211-6 du code de la santé publique ou utilisés pour un usage autre que celui défini dans ladite autorisation ; / 3° Allergènes ne constituant qu'une alternative à l'utilisation d'une spécialité pharmaceutique remboursable par l'assurance maladie, composée des mêmes extraits allergéniques, et susceptibles d'entraîner des dépenses injustifiées pour l'assurance maladie, au regard notamment du prix demandé par l'entreprise en application des articles R. 163-14-1 et R. 163-14-2 du présent code ; / 4° Allergènes susceptibles, au regard de leur intérêt pour la santé publique et du prix demandé par l'entreprise en application des articles R. 163-14-1 et R. 163-14-2, d'entraîner des dépenses injustifiées pour l'assurance maladie. / II. La décision de refus de prise en charge par l'assurance maladie d'un allergène préparé spécialement pour un individu est prise par les ministres chargés de la santé et de la sécurité sociale (...) " ; <br/>
<br/>
              5. Considérant, en premier lieu, qu'il résulte des termes mêmes du dernier alinéa de l'article L. 162-16-4-1 du code de la sécurité sociale que certains allergènes préparés spécialement pour un seul individu peuvent être exclus de la prise en charge par l'assurance maladie ; que cette exclusion doit résulter d'une décision de l'autorité administrative ; que, contrairement à ce que soutient la société requérante, les dispositions de l'article R. 163-14-3 du code de la sécurité sociale n'ont ni pour objet, ni pour effet de lier la compétence des ministres chargés de la santé et de la sécurité sociale à cet effet, en méconnaissance des dispositions de l'article L. 162-16-4-1 du même code ; <br/>
<br/>
              6. Considérant, en deuxième lieu, qu'il résulte de l'économie générale des dispositions des articles R. 163-14-1 et R. 163-14-3 que les ministres chargés de la santé et de la sécurité sociale sont susceptibles d'exclure la prise en charge par l'assurance maladie d'un allergène préparé spécialement pour un individu, à l'occasion de la procédure par laquelle l'entreprise concernée demande la fixation du prix de vente au public de ce produit ; que la notion d'entreprise demanderesse, au III et au IV de l'article R. 163-14-3, fait ainsi nécessairement référence à l'entreprise auteur de la demande de fixation de prix mentionnée à l'article R. 163-14-1 ; que, ce faisant, contrairement à ce que soutient la société requérante, le pouvoir réglementaire n'a pas subordonné la prise en charge par l'assurance maladie des allergènes préparés spécialement pour un individu à une demande des entreprises intéressées ;<br/>
<br/>
              7. Considérant, en troisième lieu, qu'en prévoyant que la prise en charge d'un allergène préparé spécialement pour un seul individu peut être refusée lorsqu'elle est susceptible d'entraîner des dépenses injustifiées pour l'assurance maladie, au regard de l'existence d'une spécialité pharmaceutique remboursable propre à constituer une alternative thérapeutique, le pouvoir réglementaire a fixé un critère approprié tenant compte de l'objectif de maîtrise des dépenses de l'assurance maladie ; que, pour refuser une prise en charge sur ce fondement, il appartiendra aux ministres compétents d'apprécier, au cas par cas et en tenant compte notamment des avantages liés à l'adaptation des dosages propre aux allergènes préparés spécialement pour un seul individu, la réalité de l'équivalence entre les deux produits et la pertinence de l'alternative, compte tenu du service médical rendu respectivement par les deux produits ; que, par suite, la société requérante n'est pas fondée à soutenir que le pouvoir réglementaire a commis une erreur de droit en prévoyant un tel critère sans en fixer les conditions d'appréciation ; qu'elle ne peut, en tout état de cause, utilement soutenir qu'il aurait ainsi créé une distinction contraire au principe d'égalité entre allergènes préparés spécialement pour un seul individu et spécialités pharmaceutiques, dès lors qu'il résulte des termes des articles L. 162-16-4-1 et L. 162-17 du code de la sécurité sociale que le législateur a soumis la prise en charge par l'assurance maladie de ces deux catégories de produits à des régimes différents ; <br/>
<br/>
              8. Considérant, en quatrième lieu, qu'en prévoyant que la prise en charge d'un allergène préparé spécialement pour un seul individu peut être refusée lorsqu'elle est susceptible d'entraîner des dépenses injustifiées pour l'assurance maladie, au regard de l'intérêt qu'il présente pour la santé publique, le pouvoir réglementaire doit être regardé comme ayant implicitement mais nécessairement entendu se référer, à l'instar des dispositions des articles R. 162-52-1, R. 163-3 et R. 165-2 du code de la sécurité sociale relatifs à la prise en charge par l'assurance maladie des actes de soins, des médicaments et des autres produits de santé, à l'impact que le produit, compte tenu de son effet thérapeutique ainsi que de ses effets indésirables ou des risques liés à son utilisation, est susceptible d'avoir sur la santé de la population, en termes de mortalité, de morbidité et de qualité de vie, à sa capacité à répondre à un besoin thérapeutique non couvert, eu égard à la gravité de la pathologie, à son impact sur le système de soins et à son impact sur les politiques et programmes de santé publique ; que le pouvoir réglementaire a, ce faisant, fixé un critère approprié, tenant compte de l'objectif de maîtrise des dépenses de l'assurance maladie, qui n'est pas entaché d'erreur de droit ; <br/>
<br/>
              9. Considérant, en cinquième lieu, que si la société requérante allègue que l'article R. 163-14-3 du code de la sécurité sociale entraînera un " déremboursement massif " des allergènes préparés spécialement pour un seul individu, il ne ressort pas des pièces du dossier que l'application des critères prévus aux 2°, 3° et 4° de l'article R. 163-14-3 du code de la sécurité sociale serait propre à exclure la prise en charge par l'assurance maladie de l'ensemble des allergènes spécialement préparés pour un seul individu ou d'une proportion élevée d'entre eux et, en tout état de cause, à entraîner ainsi le risque d'un arrêt de leur production ; que, par suite, la société requérante n'est pas fondée à soutenir que ces dispositions seraient, pour ce motif, entachées d'une erreur de droit ou d'une erreur manifeste d'appréciation ;<br/>
<br/>
              Sur l'article 2 du décret attaqué : <br/>
<br/>
              10. Considérant que la décision de fixation du prix de vente d'un allergène préparé spécialement pour un seul individu, y compris par voie conventionnelle, en application des dispositions de l'article L. 162-16-4-1 du code de la sécurité sociale et du décret attaqué, présente un caractère règlementaire ; que les dispositions applicables avant l'entrée en vigueur de ce décret ne prévoyaient pas davantage la fixation de ce prix de vente par un acte présentant une nature contractuelle ; que, du reste, la requérante ne produit, à l'appui de ces allégations, aucun document contractuel dont découlerait la fixation d'un tel prix ; qu'elle n'est, ainsi, pas fondée à soutenir que le décret attaqué s'appliquerait à des situations contractuelles en cours ; qu'au demeurant, le décret attaqué a prévu des dispositions transitoires impartissant un délai de six mois aux entreprises concernées pour présenter un dossier de demande de fixation de prix de vente au public des allergènes préparés spécialement pour un seul individu déjà pris en charge par l'assurance maladie ; que, par suite, la société requérante n'est pas fondée à soutenir que l'article 2 du décret attaqué méconnaîtrait le principe de sécurité juridique ou serait entaché d'erreur de droit ;<br/>
<br/>
              11. Considérant qu'il résulte de tout ce qui précède que la requête de la société Stallergènes doit être rejetée, y compris ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de la société Stallergènes est rejetée.<br/>
Article 2 : La présente décision sera notifiée à la société Stallergènes, au Premier ministre et à la ministre des affaires sociales et de la santé.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
