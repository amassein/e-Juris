<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042230857</ID>
<ANCIEN_ID>JG_L_2020_07_000000437377</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/23/08/CETATEXT000042230857.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème chambre, 22/07/2020, 437377, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-07-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>437377</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BAUER-VIOLAS, FESCHOTTE-DESBOIS, SEBAGH ; SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR>M. Paul Bernard</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Sophie Roussel</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2020:437377.20200722</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu les procédures suivantes :<br/>
<br/>
              Par une première demande, enregistrée le 17 mai 2017, la société Broadband Pacifique a demandé au tribunal administratif de Wallis et Futuna de condamner in solidum, en tant que de besoin, le territoire des îles Wallis et Futuna et l'Etat à lui verser la somme de 3 118 000 000 francs CFP, assortie des intérêts au taux légal à compter du 30 décembre 2016. <br/>
<br/>
              Par un jugement n° 1760006 du 12 octobre 2017, le tribunal administratif de Wallis et Futuna a rejeté sa demande. <br/>
<br/>
              Par une seconde demande enregistrée le 21 novembre 2017, la société Broadband Pacifique a demandé au même tribunal, sur renvoi du Conseil d'Etat, de condamner les mêmes personnes à lui verser la même indemnité, assortie des intérêts au taux légal à compter du 30 décembre 2013 avec capitalisation annuelle à compter du 4 octobre 2017, des intérêts, et 14 979 648 francs CFP avec intérêts au taux légal à compter du 21 novembre 2017 pour le reste des demandes, avec capitalisation. <br/>
<br/>
              Par un jugement n° 1760019 du 12 octobre 2018, le tribunal administratif de Wallis et Futuna a rejeté sa demande. <br/>
<br/>
              Par un arrêt du 5 novembre 2019, n° 17PA03803, 18PA03893, la cour administrative d'appel de Paris, sur les appels de la société Broadband Pacifique contre ces deux jugements, a condamné le territoire des îles Wallis et Futuna à verser à cette société la somme de 276 millions de francs CFP, portant intérêt à compter du 31 décembre 2013, avec capitalisation des intérêts échus le 28 septembre 2017 à chaque échéance annuelle à compter de cette date, en réparation des préjudices subis du fait du refus fautif de l'administrateur supérieur du territoire des îles Wallis et Futuna de lui attribuer des ressources de numérotation et de permettre l'interconnexion de son réseau au réseau de l'opérateur public, et 6 515 602 francs CFP au titre des dépens, portant intérêt à compter du 30 décembre 2016.  <br/>
<br/>
<br/>
              1° Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 6 janvier et 5 mai 2020 au secrétariat du contentieux du Conseil d'Etat sous le numéro 437377, le territoire des îles Wallis et Futuna demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter les conclusions d'appel de la société Broadband Pacifique ;<br/>
<br/>
              3°) de mettre à la charge de la société Broadband Pacifique une somme de 7 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              2° Par une requête, enregistrée le 6 mai 2020 au secrétariat du contentieux du Conseil d'Etat, sous le numéro 440431, le territoire des îles Wallis et Futuna demande en outre au Conseil d'Etat :<br/>
<br/>
              1°) d'ordonner qu'il soit sursis à l'exécution de ce même arrêt ;<br/>
<br/>
              2°) de mettre à la charge de la société Broadband Pacifique la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu : <br/>
              - le code des postes et communications électroniques ;<br/>
              - la loi n° 61-814 du 29 juillet 1961 conférant aux îles Wallis et Futuna le statut de territoire d'outre-mer ; <br/>
              - le code de justice administrative et l'ordonnance n° 2020-305 du 25 mars 2020 ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Paul Bernard, maître des requêtes, <br/>
<br/>
              - les conclusions de Mme Sophie Roussel, rapporteur public,<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Bauer-Violas, Feschotte-Desbois, Sebagh, avocat du Territoire des îles Wallis et Futuna ; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1.	Le pourvoi et la requête à fin de sursis présentés par le territoire des îles Wallis et Futuna sont dirigés contre le même arrêt de la cour administrative d'appel de Paris. Il y a lieu de les joindre pour statuer par une seule décision.<br/>
<br/>
              Sur le pourvoi dirigé contre l'arrêt attaqué : <br/>
<br/>
              2.	Aux termes de l'article L. 822-1 du code de justice administrative : " Le pourvoi en cassation devant le Conseil d'Etat fait l'objet d'une procédure préalable d'admission. L'admission est refusée par décision juridictionnelle si le pourvoi est irrecevable ou n'est fondé sur aucun moyen sérieux ".<br/>
<br/>
              3.	Pour demander l'annulation de l'arrêt qu'il attaque, le territoire des îles Wallis et Futuna soutient que la cour administrative d'appel de Paris a : <br/>
              - entaché sa décision d'irrégularité et d'erreur de droit en le condamnant à verser une indemnité à la société Broadband Pacifique, sans annuler le jugement en sens contraire du tribunal administratif de Wallis et Futuna qui était contesté devant elle ;  <br/>
              - dénaturé les pièces du dossier en estimant que le refus de faire droit aux demandes de la société Broadband Pacifique était exclusivement motivé par la crainte de voir la société développer une offre mobile, en omettant les craintes relatives à la proposition d'une offre de communications extérieures ; <br/>
              - commis une erreur de droit et de qualification juridique des faits en retenant l'existence d'une illégalité fautive engageant sa responsabilité ;<br/>
              - commis une erreur de droit en ne recherchant pas si la perte de chance invoquée par la société Broadband Pacifique présentait un caractère sérieux. <br/>
<br/>
              4.	Aucun de ces moyens n'est de nature à permettre l'admission du pourvoi.<br/>
<br/>
              Sur la requête à fin de sursis à exécution :<br/>
<br/>
              5.	Il résulte de ce qui précède que le pourvoi formé par le territoire des îles Wallis et Futuna contre l'arrêt du 5 novembre 2019 de la cour administrative d'appel de Paris n'est pas admis. Par suite, les conclusions à fin de sursis à exécution de cet arrêt sont devenues sans objet. Les conclusions présentées par le territoire au titre de l'article L. 761-1 du code de justice administrative ne peuvent, par conséquent, qu'être rejetées.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : Le pourvoi n° 437377 du territoire des îles Wallis et Futuna n'est pas admis.<br/>
<br/>
Article 2 : Il n'y a pas lieu de statuer sur les conclusions de la requête n° 440431. <br/>
<br/>
Article 3 : Les conclusions présentées par le territoire des îles Wallis et Futuna au titre de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
<br/>
Article 4 : La présente décision sera notifiée au territoire des îles Wallis et Futuna et à la société Broadband Pacifique. Copie en sera adressée au ministre des outre-mer. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
