<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037549017</ID>
<ANCIEN_ID>JG_L_2018_10_000000411855</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/54/90/CETATEXT000037549017.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème et 1ère chambres réunies, 24/10/2018, 411855, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-10-24</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>411855</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème et 1ère chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Jean-François de Montgolfier</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Dieu</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:411855.20181024</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête sommaire, un mémoire complémentaire, un mémoire en réplique et un nouveau mémoire, enregistrés les 22 juin et 15 septembre 2017 et les 2 janvier et 9 octobre 2018 au secrétariat du contentieux du Conseil d'Etat, M. A... B...et l'association démocratie et transparence à l'université de Lyon (ADTUL) demandent au Conseil d'Etat d'annuler pour excès de pouvoir la décision du Premier ministre n° 2017IDEX/I-SITE-01 du 24 mars 2017.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la loi n° 2010-237 du 9 mars 2010 ;<br/>
              - la loi n°1013-1278 du 29 décembre 2013 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-François de Montgolfier, maître des requêtes ;  <br/>
<br/>
              - les conclusions de M. Frédéric Dieu, rapporteur public ;<br/>
<br/>
               Vu la note en délibéré, enregistrée le 17 octobre 2018, présentée par M. B... et l'ADTUL ; <br/>
<br/>
<br/>
<br/>1.  Considérant que, pour la mise en oeuvre des " programmes pour l'investissement d'avenir ", l'article 8 de la loi du 9 mars 2010 de finances rectificative pour 2010, dans sa rédaction issue de la loi du 29 décembre 2013 de finances pour 2014, dispose que : " I. - La gestion des fonds versés à partir des programmes créés par la présente loi de finances rectificative et des programmes créés par la loi n° 2013-1278 du 29 décembre 2013 de finances pour 2014 peut être confiée, dans les conditions prévues par le présent article et nonobstant toute disposition contraire de leurs statuts, à l'Agence nationale de la recherche ainsi qu'à d'autres établissements publics de l'Etat et à des sociétés dans lesquelles l'Etat détient directement ou indirectement une majorité du capital ou des droits de vote (...) / II. - A. - Pour chaque action financée par des crédits ouverts sur les programmes mentionnés au I, les conditions de gestion et d'utilisation des fonds mentionnés au I font préalablement à tout versement l'objet d'une convention entre l'Etat et chacun des organismes gestionnaires (...) " ; que, sur le fondement de ces dispositions, l'Etat et l'Agence nationale de la recherche ont conclu le 23 juin 2014, une convention relative à la mise en oeuvre du deuxième Programme d'investissement d'avenir, dite action " initiative d'excellence, initiative science-innovation-territoires-économie " (IDEX/I-SITE) ; que, par la décision du 24 mars 2017, dont M. B... et l'association démocratie et transparence à l'université de Lyon (ADTUL) demandent l'annulation pour excès de pouvoir, le Premier ministre a désigné les projets sélectionnés au titre de cette action IDEX/I-SITE, déterminé le montant de la dotation non consommable et de la " subvention complémentaire " dévolues à chacun des laboratoires ou initiatives ainsi sélectionnés et, enfin, autorisé l'Agence nationale de la recherche à conclure les conventions attributives d'aide prévues par la convention du 23 juin 2014 en prévoyant que celles-ci formaliseront les engagements pris lors du processus de sélection, reprendront les recommandations du jury à mettre en oeuvre à l'issue de la période probatoire de quatre ans et, dans certains cas, les conditions à deux ans dont le respect conditionnera la suite du projet ; <br/>
<br/>
              2.  Considérant qu'aux termes de l'article R. 311-1 du code de justice administrative : " Le Conseil d'Etat est compétent pour connaître en premier et dernier ressort : / (...) 2° Des recours dirigés contre les actes réglementaires des ministres et des autres autorités à compétence nationale et contre leurs circulaires et instructions à portée générale (...) " ; que la décision attaquée, par laquelle le Premier ministre sélectionne une liste de projets, détermine les montants qui leur sont alloués et autorise l'Agence nationale de la recherche à conclure les conventions attributives d'aide en indiquant que celles-ci définiront les conditions, propres à chaque projet, auxquelles le versement des aides sera subordonné ne revêt pas de caractère réglementaire ; qu'en particulier, contrairement à ce que soutiennent les requérants, elle n'a pas pour objet l'organisation du service public ; que, par suite, ni les dispositions citées ci-dessus du code de justice administrative ni aucune autre disposition ne confère au Conseil d'Etat la compétence pour connaître, en premier et dernier ressort, du recours pour excès de pouvoir formé contre cette décision ;<br/>
<br/>
              3.  Considérant que, pour demander l'annulation de la décision litigieuse, M. B..., membre du conseil d'administration de l'Ecole normale supérieure (ENS) de Lyon et l'ADTUL, dont les statuts prévoient qu'elle a pour objet de  " défendre les intérêts matériels et moraux des personnels des établissements membres du pôle de Recherche et d'enseignement supérieur Lyon - Saint Etienne intitulé Université de Lyon (ou toute structure qui lui serait substituée), vis-à-vis de ce PRES ''Université de Lyon'' et de veiller à ce que son fonctionnement soit démocratique, transparent et respectueux des règlements ", invoquent l'atteinte portée aux intérêts qu'ils ont vocation à défendre par la sélection, au titre de l'action IDEX/I-SITE, du projet IDEXLYON, présenté par douze établissements publics d'enseignement supérieur membres ou associés à la communauté d'universités et d'établissements " université de Lyon ", parmi lesquels figure l'ENS de Lyon ; que, toutefois, cette décision de sélection et d'attribution prévisionnelle de crédits au titre de l'action IDEX/I-SITE ne saurait porter, par elle-même, aucune atteinte aux intérêts des établissements concernés ou à ceux de leurs personnels ; que si, ainsi qu'il a été dit, la décision attaquée prévoit que le versement des dotations affectées au projet IDEXLYON sera subordonné, lors de la signature de la convention avec l'Agence nationale de la recherche, à la mise en oeuvre d'engagements pris lors du processus de sélection, et si ces engagements portent notamment sur des modifications de gouvernance au sein de la communauté d'universités et d'établissements " université de Lyon ", la décision attaquée n'a pas, par elle-même, pour effet d'opérer les changements en question ; <br/>
<br/>
              4. Considérant qu'il résulte de ce qui précède que le Premier ministre est fondé à soutenir que M. B... et l'ADTUL n'ont pas intérêt à demander l'annulation de la décision qu'ils attaquent ; que, les conclusions de leur requête étant entachées d'une irrecevabilité manifeste insusceptible d'être couverte en cours d'instance, il y a lieu, en application de l'article R. 351-4 du code de justice administrative, de les rejeter ;<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de M. B... et de l'association démocratie et transparence à l'université de Lyon est rejetée.<br/>
Article 2 : La présente décision sera notifiée à M. A... B..., à l'association démocratie et transparence à l'université de Lyon et au Premier ministre.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
