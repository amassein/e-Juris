<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044635977</ID>
<ANCIEN_ID>JG_L_2021_12_000000452612</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/63/59/CETATEXT000044635977.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème chambre, 29/12/2021, 452612, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-12-29</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>452612</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Audrey Prince</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Mireille Le Corre</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:452612.20211229</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et deux nouveaux mémoires, enregistrés les 17 mai, 4 octobre et 16 novembre 2021 au secrétariat du contentieux du Conseil d'Etat, M. C... A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision du 15 mars 2021 par laquelle l'autorité militaire de premier niveau lui a infligé la sanction disciplinaire de 7 jours d'arrêts ; <br/>
<br/>
              2°) d'enjoindre à la ministre des armées d'effacer cette sanction de son dossier administratif dans un délai d'un mois à compter de la décision à intervenir ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de de la défense ;<br/>
              - la loi du 22 avril 1905 portant fixation du budget des dépenses et des recettes de l'exercice 1905 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Audrey Prince, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Mireille Le Corre, rapporteure publique ;<br/>
<br/>
<br/>
<br/>
              Considérant ce qu'il suit :<br/>
<br/>
              1. Il ressort des pièces du dossier que M. A..., officier de l'armée de terre, s'est vu infliger le 15 mars 2021 une sanction de sept jours d'arrêts en raison de son style de commandement inadapté à l'égard de ses subordonnés.<br/>
<br/>
              Sur la légalité externe de la décision attaquée :<br/>
<br/>
              2. En premier lieu, aux termes de l'article 65 de la loi du 22 avril 1905 portant fixation du budget des dépenses et des recettes de l'exercice 1905 : " Tous les fonctionnaires civils et militaires (...) ont droit à la communication personnelle et confidentielle de toutes les notes, feuilles signalétiques et tous autres documents composant leur dossier, (...) avant d'être l'objet d'une mesure disciplinaire ou d'un déplacement d'office (...) ". Aux termes de l'article R. 4137-15 du code de la défense : " Avant qu'une sanction ne lui soit infligée, le militaire a le droit de s'expliquer oralement ou par écrit, (...) devant l'autorité militaire de premier niveau dont il relève. (...) / Avant d'être reçu par l'autorité militaire de premier niveau dont il relève, le militaire a connaissance de l'ensemble des pièces et documents au vu desquels il est envisagé de le sanctionner. " <br/>
<br/>
              3. Il ressort des pièces du dossier que M. A... a signé trois bordereaux par lesquels il atteste avoir pris connaissance des pièces composant son dossier disciplinaire. En outre, il ne peut utilement se prévaloir de l'absence de communication de fiches relatives aux dix-huit entretiens individuels réalisés en octobre 2018 dans le cadre de l'audit des conditions de travail de la musique des parachutistes, dans la mesure où il n'est pas établi que ces derniers auraient fait l'objet de comptes rendus individuels. Enfin, la circonstance que ne lui auraient pas été communiqués une audition téléphonique non retranscrite de l'Adjudant d'Unité ainsi que des éléments relatifs à un contentieux impliquant son ancienne ajointe, sans lien avec la procédure litigieuse, n'est pas de nature à entacher cette dernière d'irrégularité. Il suit de là que le moyen tiré de ce que le dossier disciplinaire qui lui a été communiqué aurait été incomplet ne peut qu'être écarté.<br/>
<br/>
              4. En second lieu, il ne ressort ni des termes ni des conditions d'élaboration du rapport d'enquête de commandement, remis le 25 janvier 2021, qui synthétise les avis des supérieurs hiérarchiques, des collègues et des subordonnés du requérant, que son auteur aurait manqué à ses devoirs d'impartialité et d'objectivité.<br/>
<br/>
<br/>
<br/>
<br/>
              Sur la légalité interne de la décision attaquée :<br/>
<br/>
              5. En premier lieu, pour prononcer la sanction litigieuse, l'autorité investie du pouvoir disciplinaire s'est fondée sur la circonstance, d'une part, que le style de commandement de M. A... a directement contribué à dégrader les conditions de travail de ses subordonnés par son comportement inapproprié et ses propos vexatoires, d'autre part, qu'en dépit de recommandations et de rappels de ses chefs de corps successifs, le requérant n'a pas su améliorer son comportement et sa communication envers ses subordonnés, suscitant une ambiance délétère, ce qui a entraîné des répercussions sur le bon fonctionnement du service. Il ne ressort pas des pièces du dossier que de telles constatations reposeraient sur des faits matériellement inexacts.<br/>
<br/>
              6. En second lieu, aux termes de l'article L. 4137-2 du code de la défense : " Les sanctions disciplinaires applicables aux militaires sont réparties en trois groupes : / 1° Les sanctions du premier groupe sont : / (...) e) Les arrêts (...) ". Eu égard à la qualité d'officier du requérant, aux recommandations et rappels à l'ordre dont il a fait l'objet de la part de sa hiérarchie pour adapter son comportement à l'égard de ses subordonnés, ainsi qu'à la circonstance que celui-ci a eu des conséquences négatives sur les conditions de travail de ses subordonnés et le bon fonctionnement de l'unité qu'il dirigeait, et alors même que, par ailleurs, sa manière de servir donnerait pleinement satisfaction, l'autorité investie du pouvoir disciplinaire n'a pas, dans les circonstances de l'espèce et au regard du pouvoir d'appréciation dont elle disposait, pris une sanction disproportionnée en lui infligeant la sanction du premier groupe de sept jours d'arrêts.<br/>
<br/>
              7. Il résulte de ce qui précède que la requête de M. A... doit être rejetée, y compris ses conclusions à fin d'injonction et celles présentées au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de M. A... est rejetée.<br/>
Article 2: La présente décision sera notifiée à M. C... A... et à la ministre des armées.<br/>
              Délibéré à l'issue de la séance du 15 décembre 2021 où siégeaient : M. Gilles Pellissier, assesseur, présidant ; M. Benoît Bohnert, conseiller d'Etat et Mme Audrey Prince, maître des requêtes en service extraordinaire-rapporteure. <br/>
<br/>
              Rendu le 29 décembre 2021.<br/>
<br/>
<br/>
                 Le président : <br/>
                 Signé : M. Gilles Pellissier<br/>
 		La rapporteure : <br/>
      Signé : Mme Audrey Prince<br/>
                 La secrétaire :<br/>
                 Signé : Mme D... B...<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
