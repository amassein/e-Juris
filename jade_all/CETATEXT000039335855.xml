<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039335855</ID>
<ANCIEN_ID>JG_L_2019_11_000000419245</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/33/58/CETATEXT000039335855.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère chambre, 06/11/2019, 419245, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-11-06</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>419245</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BORE, SALVE DE BRUNETON, MEGRET</AVOCATS>
<RAPPORTEUR>M. Damien Pons</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Rémi Decout-Paolini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2019:419245.20191106</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A... B... a demandé au tribunal administratif de Melun, d'une part, d'annuler la décision du 12 août 2016 par laquelle le président du conseil départemental de Seine-et-Marne a rejeté son recours administratif contre la décision de la caisse d'allocations familiales de Seine-et-Marne du 30 janvier 2016 mettant fin à son droit au revenu de solidarité active et, d'autre part, de le rétablir dans ses droits au revenu de solidarité active depuis le 1er octobre 2015. Par un jugement n° 1608505 du 28 décembre 2017, le tribunal administratif de Melun a annulé la décision du 12 août 2016 et renvoyé M. B... devant le département de Seine-et-Marne pour la fixation de ses droits au revenu de solidarité active à compter du 1er octobre 2015.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 26 mars et 26 juin 2018 au secrétariat du contentieux du Conseil d'Etat, le département de Seine-et-Marne demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ; <br/>
<br/>
              2°) de rejeter la demande présentée par M. B... devant le tribunal administratif ;<br/>
<br/>
              3°) de mettre à la charge de M. B... la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'action sociale et des familles ;<br/>
              - le code de la sécurité sociale ;<br/>
              - la loi n° 2008-1249 du 1er décembre 2008 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Damien Pons, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Rémi Decout-Paolini, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Boré, Salve de Bruneton, Mégret, avocat du département de Seine-et-Marne ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis au juge du fond que M. et Mme B..., bénéficiaires du revenu de solidarité active depuis le 11 décembre 2013, ont atteint l'âge de soixante-cinq ans respectivement le 2 juin et le 5 juin 2015. A la suite de courriers les invitant à accomplir les démarches nécessaires pour faire valoir leurs droits à une pension de retraite et à l'allocation de solidarité aux personnes âgées, la caisse d'allocations familiales de Seine-et-Marne a mis fin à leur droit au revenu de solidarité active à compter du 1er octobre 2015, par une décision du 30 janvier 2016 que le président du conseil départemental de Seine-et-Marne a confirmée le 12 août 2016. Le département de Seine-et-Marne se pourvoit en cassation contre le jugement du 28 décembre 2017 par lequel le tribunal administratif de Melun a annulé la décision du 12 août 2016 et renvoyé M. B... devant le département pour la fixation de ses droits au revenu de solidarité active à compter du 1er octobre 2015.<br/>
<br/>
              2. Aux termes de l'article L. 262-2 du code de l'action sociale et des familles, dans sa rédaction en vigueur au 1er octobre 2015 : " Toute personne résidant en France de manière stable et effective, dont le foyer dispose de ressources inférieures à un revenu garanti, a droit au revenu de solidarité active dans les conditions définies au présent chapitre. / Le revenu garanti est calculé, pour chaque foyer, en faisant la somme : / 1° D'une fraction des revenus professionnels des membres du foyer ; / 2° D'un montant forfaitaire, dont le niveau varie en fonction de la composition du foyer et du nombre d'enfants à charge. / Le revenu de solidarité active est une allocation qui porte les ressources du foyer au niveau du revenu garanti (...) ". Aux termes de l'article L. 262-10 du même code, dans sa rédaction en vigueur à la même date : "  droit à la part de revenu de solidarité active correspondant à la différence entre le montant forfaitaire mentionné au 2° de l'article L. 262-2 applicable au foyer et les ressources de celui-ci est subordonné à la condition que le foyer fasse valoir ses droits aux prestations sociales, législatives, réglementaires et conventionnelles, à l'exception des allocations mensuelles mentionnées à l'article L. 222-3 et, sauf pour les personnes reconnues inaptes au travail dont l'âge excède celui mentionné au premier alinéa de l'article L. 351-1 du code de la sécurité sociale, des pensions de vieillesse des régimes légalement obligatoires. (...) ". Ainsi qu'il résulte des travaux préparatoires de la loi du 1er décembre 2008 généralisant le revenu de solidarité active et réformant les politiques d'insertion dont elles sont issues, le législateur a entendu, par ces dispositions, permettre aux bénéficiaires du revenu de solidarité active ayant atteint l'âge d'ouverture du droit à pension de retraite, mais ne justifiant pas de la durée requise d'assurance pour bénéficier d'un taux plein, d'attendre, pour liquider leur pension, l'âge auquel ils bénéficieraient de ce taux.<br/>
<br/>
              3. Aux termes de l'article L. 815-1 du code de la sécurité sociale, dans sa rédaction en vigueur au 1er octobre 2015 : " Toute personne justifiant d'une résidence stable et régulière sur le territoire métropolitain ou dans un département mentionné à l'article L. 751-1 et ayant atteint un âge minimum bénéficie d'une allocation de solidarité aux personnes âgées dans les conditions prévues par le présent chapitre. Cet âge minimum est abaissé en cas d'inaptitude au travail. (...) ". Cet âge est fixé par l'article R. 815-1 du même code à soixante-cinq ans et est abaissé à l'âge d'ouverture du droit à une pension de retraite prévu à l'article L. 161-17-2 de ce code pour les personnes qui, en vertu de son article L. 351-8, bénéficient dès cet âge du taux plein quelle que soit leur durée d'assurance. Aux termes de l'article L. 815-5 du même code : " La personne âgée et, le cas échéant, son conjoint ou concubin ou partenaire lié par un pacte civil de solidarité doivent faire valoir en priorité les droits en matière d'avantages de vieillesse auxquels ils peuvent prétendre au titre de dispositions législatives ou réglementaires françaises ou étrangères, des conventions internationales, ainsi que des régimes propres aux organisations internationales ". Enfin, en vertu de l'article L. 815-9 du même code, l'allocation de solidarité aux personnes âgées est une allocation différentielle qui porte le total de cette allocation et des ressources personnelles de l'intéressé et de son conjoint, concubin ou partenaire lié par un pacte civil de solidarité au niveau de plafonds fixés par décret. <br/>
<br/>
              4. Si le bénéfice de l'allocation de solidarité aux personnes âgées, qui revêt le caractère d'une prestation sociale au sens de l'article L. 262-10 du code de l'action sociale et des familles, est subordonné à la condition d'avoir fait valoir ses droits en matière d'avantages de vieillesse, elle ne peut toutefois être regardée comme une pension de vieillesse. Par suite, il résulte de la combinaison des dispositions mentionnées aux points 2 et 3 que le droit au revenu de solidarité active est subordonné, pour les personnes qui remplissent les conditions pour en bénéficier, à la condition de faire valoir leurs droits à cette allocation, sauf à ce qu'elles ne remplissent pas encore les conditions pour bénéficier de la liquidation d'une pension de retraite à taux plein. <br/>
<br/>
              5. Il suit de là que le tribunal administratif de Melun a commis une erreur de droit en jugeant que le droit au revenu de solidarité active ne pouvait être subordonné à la condition que l'intéressé fasse valoir ses droits à l'allocation de solidarité aux personnes âgées. Le département de Seine-et-Marne est fondé, sans qu'il soit besoin d'examiner les autres moyens de son pourvoi, à demander l'annulation de son jugement pour ce motif. <br/>
<br/>
              6. Dans les circonstances de l'espèce, il n'y a pas lieu de mettre à la charge de M. B... la somme demandée par le département de Seine-et-Marne au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : Le jugement du tribunal administratif de Melun du 28 décembre 2017 est annulé. <br/>
Article 2 : L'affaire est renvoyée au tribunal administratif de Melun.<br/>
Article 3 : Les conclusions du département de Seine-et-Marne présentées au titre de l'article            L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée à M. A... B... et au département de Seine-et-Marne.<br/>
Copie en sera adressée à la ministre des solidarités et de la santé.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
