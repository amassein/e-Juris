<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044316305</ID>
<ANCIEN_ID>JG_L_2021_11_000000450899</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/31/63/CETATEXT000044316305.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème chambre, 10/11/2021, 450899, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-11-10</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>450899</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. François Charmont</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:450899.20211110</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A... C... a demandé au tribunal administratif de Rouen d'annuler la décision référencée 48 SI par laquelle le ministre de l'intérieur a constaté la perte de validité de son permis de conduire pour solde de points nul ainsi que la décision implicite de rejet de son recours gracieux du 14 juin 2019 et d'enjoindre au ministre de l'intérieur de lui restituer son permis de conduire dans un délai de huit jours. <br/>
<br/>
              Par un jugement n° 1903313 du 28 janvier 2021, le tribunal administratif a annulé la décision référencée 48 SI, enjoint au ministre de l'intérieur de restituer, dans un délai d'un mois, à M. C... son permis de conduire crédité de quatre points à la suite du stage de sensibilisation à la sécurité routière suivi les 6 et 7 septembre 2019 et des points retirés à la suite des infractions relevées les 11 octobre 2015, 15 mai 2016, 8 juin 2016, 16 août 2017, 31 août 2017, 11 mars 2018, 6 avril 2018, 14 avril 2018, 5 juin 2018, 25 juin 2018, 21 août 2018, sous réserve que M. C... n'ait pas commis de nouvelles infractions ayant entraîné des retraits de points faisant obstacle à cette restitution, dans la limite du plafond de points affectés au permis de conduire, et rejeté le surplus des conclusions de la demande.<br/>
<br/>
              Par un pourvoi, enregistré le 22 mars 2021 au secrétariat du contentieux du Conseil d'Etat, le ministre de l'intérieur demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter la demande de M. C....<br/>
<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
              Vu : <br/>
              - le code de la route ;<br/>
              - le code de justice administrative.<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Dominique Langlais, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public.<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis au juge du fond que, par une décision référencée 48 SI, le ministre de l'intérieur a notifié à M. C... un retrait de points consécutif à une infraction relevée le 21 août 2018, récapitulé les précédentes décisions portant retrait de points de son permis de conduire et constaté la perte de validité de ce permis pour solde de points nul. Le ministre de l'intérieur se pourvoit en cassation contre le jugement du 28 janvier 2021 par lequel le tribunal administratif de Rouen a, sur la demande de M. C..., annulé cette décision et lui a enjoint de restituer son permis de conduire à l'intéressé.<br/>
<br/>
              2. La notification d'une décision relative au permis de conduire doit être regardée comme régulière lorsqu'elle est faite à une adresse correspondant effectivement à une résidence de l'intéressé.<br/>
<br/>
              3. Il ressort des termes mêmes du jugement attaqué que, pour écarter la fin de non-recevoir soulevée devant lui par le ministre de l'intérieur et tirée de la tardiveté de la demande, le tribunal administratif s'est fondé sur ce que, la date de vaine présentation du pli recommandé notifiant la décision contestée n'apparaissant pas sur l'avis de réception de ce pli retourné à l'administration, sa notification régulière n'était pas établie. Il ressort cependant des pièces du dossier qui était soumis au tribunal administratif que l'avis de réception en cause porte la mention de la réexpédition du pli vers une adresse de réexpédition (" REEX "), de ce que le pli a été reçu par les services postaux correspondant à cette adresse (" Avisé Evreux centre 13/3/2019 ") et de ce qu'il a été retourné à l'administration avec l'information " pli avisé et non réclamé ". En estimant que la preuve d'une notification régulière de la décision n'était pas apportée, le tribunal administratif a, dès lors, dénaturé les pièces du dossier soumis à son appréciation. Par suite, son jugement doit être annulé.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du tribunal administratif de Rouen du 28 janvier 2021 est annulé.<br/>
Article 2 : L'affaire est renvoyée au tribunal administratif de Rouen.<br/>
Article 3 : La présente décision sera notifiée au ministre de l'intérieur et à M. A... C....<br/>
              Délibéré à l'issue de la séance du 14 octobre 2021 où siégeaient : M. Olivier Yeznikian, assesseur, présidant ; M. Jean-Philippe Mochon, conseiller d'Etat et M. Jean-Dominique Langlais, conseiller d'Etat-rapporteur. <br/>
<br/>
              Rendu le 10 novembre 2021.<br/>
<br/>
<br/>
                 Le président : <br/>
                 Signé : M. Olivier Yeznikian<br/>
 		Le rapporteur : <br/>
      Signé : M. Jean-Dominique Langlais<br/>
                 Le secrétaire :<br/>
                 Signé : M. B... D...<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
