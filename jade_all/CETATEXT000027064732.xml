<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027064732</ID>
<ANCIEN_ID>JG_L_2013_02_000000349766</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/06/47/CETATEXT000027064732.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème sous-section jugeant seule, 13/02/2013, 349766, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-02-13</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>349766</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SPINOSI</AVOCATS>
<RAPPORTEUR>Mme Véronique Rigal</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Edouard Crépey</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2013:349766.20130213</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 31 mai et 31 août 2011 au secrétariat du contentieux du Conseil d'Etat, présenté pour le Centre d'accueil universel, dont le siège est 254, rue Faubourg Saint-Martin à Paris (75010) ; le Centre d'accueil universel demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 10PA01452 du 31 mars 2011 par lequel la cour administrative d'appel de Paris, faisant droit au recours du ministre de l'intérieur, de l'outre-mer, des collectivités territoriales, a annulé le jugement du 28 janvier 2010 du tribunal administratif de Paris annulant la décision par laquelle le ministre a implicitement refusé de supprimer trois mentions contenues dans le dossier le concernant détenu par la direction centrale des renseignements généraux et justifiant le classement par l'Assemblée nationale de l'association exposante parmi les sectes dans son rapport du 22 décembre 1995 ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel du ministre ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la loi n° 78-17 du 6 janvier 1978 ;<br/>
<br/>
              Vu la loi n° 78-753 du 17 juillet 1978 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Véronique Rigal, Maître des Requêtes en service extraordinaire,  <br/>
<br/>
              - les observations de Me Spinosi, avocat du Centre d'accueil universel,<br/>
<br/>
              - les conclusions de M. Edouard Crépey, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à Me Spinosi, avocat du Centre d'accueil universel ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant que par un arrêt du 1er décembre 2005, devenu définitif, la cour administrative d'appel de Paris a annulé la décision implicite du ministre de l'intérieur refusant de communiquer au Centre d'accueil universel, alors dénommé Eglise universelle du royaume de dieu, le dossier détenu par la direction centrale des renseignements généraux justifiant le classement de cette association par l'Assemblée nationale parmi les sectes dans son rapport n° 2468 du 10 janvier 1996 et enjoint au ministre de communiquer ce document ; qu'après avoir, en exécution de cet arrêt, obtenu la communication de la fiche la concernant, le Centre d'accueil universel a demandé au ministre de l'intérieur, par courrier du 31 mars 2008, la suppression des mentions : " déstabilisation mentale ", " exigences financières exorbitantes " et " atteintes à l'intégrité physique " figurant dans ce document ; que, n'ayant pas obtenu de réponse, elle a saisi le tribunal administratif de Paris de conclusions tendant à l'annulation de cette décision implicite de rejet, à ce qu'il soit enjoint au ministre à titre principal de supprimer les mentions précitées et, à titre subsidiaire, de les rectifier, de les verrouiller et de lui communiquer toute information sur l'origine de ces mentions ; que, par jugement du 28 janvier 2010, le tribunal administratif a annulé la décision attaquée et enjoint en conséquence au ministre de supprimer les mentions litigieuses ; que le Centre d'accueil universel se pourvoit en cassation contre l'arrêt du 31 mars 2011 par lequel la cour administrative d'appel a annulé ce jugement et, statuant par la voie de l'évocation, rejeté sa demande présentée au tribunal administratif ;<br/>
<br/>
              2. Considérant qu'il ressort des pièces du dossier soumis aux juge du fond et des termes de leur décision que pour annuler le jugement du tribunal administratif de Paris dont elle était saisie, la cour administrative d'appel de Paris a jugé " qu'il ressortait de l'examen du dossier de première instance " que l'association requérante " n'avait pas soulevé de moyen relatif à l'exercice du droit de rectification dans le cadre de la loi n° 78-753 du 17 juillet 1978 portant diverses mesures d'amélioration des relations entre l'administration et le public et diverses dispositions d'ordre administratif, social et fiscal [et] que, par suite, en examinant un tel moyen, les premiers juges [avaient] statué ultra petita " ; qu'en statuant ainsi alors qu'il ressort des pièces du dossier soumis au juge du fond et notamment des  termes de la requête introductive d'instance présentée par le Centre d'accueil universel au tribunal administratif de Paris qu'il concluait à l'annulation de la décision implicite du ministre refusant de supprimer des mentions figurant dans le document dont il avait obtenu communication et à ce qu'il soit enjoint au ministre de procéder à cet effacement, en visant non seulement la loi 6 janvier 1978 relative à l'informatique, aux fichiers et aux libertés, mais également celle du 17 juillet 1978, la cour administrative d'appel de Paris a dénaturé les termes de la requête ; que son arrêt doit, par suite et sans qu'il soit besoin d'examiner l'autre moyen du pourvoi, être annulé ; <br/>
<br/>
              3. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de faire application des dispositions de l'article L. 761-1 du code de justice administrative et de mettre à la charge de l'Etat le versement au Centre d'accueil universel de la somme de 3 000 euros ;<br/>
<br/>
<br/>
<br/>				D E C I D E  :<br/>
              				--------------<br/>
<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Paris du 31 mars 2011 est annulé.<br/>
<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Paris.<br/>
Article 3 : L'Etat versera la somme de 3 000 euros au Centre d'accueil universel au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée au Centre d'accueil universel et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
