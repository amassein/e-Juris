<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029702421</ID>
<ANCIEN_ID>JG_L_2014_10_000000372133</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/70/24/CETATEXT000029702421.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  4ème sous-section jugeant seule, 24/10/2014, 372133, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-10-24</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>372133</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 4ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GATINEAU, FATTACCINI</AVOCATS>
<RAPPORTEUR>M. Benjamin de Maillard</RAPPORTEUR>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CESJS:2014:372133.20141024</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu l'ordonnance n° 1201292 du 5 septembre 2013, enregistrée le 12 septembre 2013 au secrétariat du contentieux du Conseil d'Etat, par laquelle le président du tribunal administratif de Cayenne a transmis au Conseil d'Etat, en application de l'article R. 351-2 du code de justice administrative, la requête de M. B...A...; <br/>
<br/>
              Vu la requête, enregistrée le 31 août 2012 au greffe du tribunal administratif de Cayenne, présentée pour M.A..., demeurant ... ; M. A... demande au juge administratif : <br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision implicite par laquelle le ministre de l'enseignement supérieur et de la recherche a retiré sa nomination en qualité de professeur des universités, intervenue le 16 juillet 2011 et la décision du président de l'université des Antilles et de la Guyane refusant de lui attribuer un poste de professeur des universités ; <br/>
<br/>
              2°) d'enjoindre à l'université des Antilles et de la Guyane de réexaminer son dossier ; <br/>
<br/>
              3°) de condamner solidairement l'Etat et l'université à lui verser la somme de 15 000 euros à titre de dommages et intérêts en raison du préjudice subi ; <br/>
<br/>
              4°) de mettre solidairement à la charge de l'Etat et de l'université la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de l'éducation ; <br/>
<br/>
              Vu la loi n° 79-587 du 11 juillet 1979 ; <br/>
<br/>
              Vu la loi n° 2000-321 du 12 avril 2000 ; <br/>
<br/>
              Vu le décret n° 84-431 du 6 juin 1984 ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Benjamin de Maillard, auditeur,<br/>
<br/>
              - les conclusions de M. Rémi Keller, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Gatineau, Fattaccini, avocat de M. A...;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'en 2011, M. A...a présenté sa candidature à un poste de professeur des universités en sciences économiques ouvert par l'université des Antilles et de la Guyane aux personnes titulaires d'une habilitation à diriger les recherches au titre du 1° de l'article 46 du décret du 6 juin 1984 ; qu'après qu'il a été classé premier pour ce poste par le comité de sélection, le conseil d'administration de cette université a proposé sa nomination au ministre de l'enseignement supérieur et de la recherche et le requérant a été informé le 16 juillet 2011, par un avis signé du chef du service des personnels enseignants et publié dans l'application informatique " Galaxie ", utilisée par le ministère pour gérer les procédures de recrutement et d'affectation des enseignants chercheurs, qu'il était affecté sur l'emploi en cause et que cette décision ferait " prochainement l'objet d'un décret de nomination " ; que, toutefois, par deux lettres des 26 juillet et 1er septembre 2011, le président de l'université a informé M. A...que le ministère lui avait demandé " d'interrompre la procédure de recrutement pour non-conformité aux dispositions réglementaires régissant l'accès au corps des professeurs des universités " et que, en conséquence, il ne pouvait être nommé ; <br/>
<br/>
              Sur les conclusions d'excès de pouvoir : <br/>
<br/>
              2. Considérant, en premier lieu, que les dispositions de l'article 42 du décret du 6 juin 1984 portant statut du corps des professeurs des universités prévoient quatre voies différentes de recrutement par concours dans ce corps ; qu'aux termes de l'article 48 du même décret : " Dans les disciplines juridiques, politiques, économiques et de gestion, les professeurs des universités sont recrutés par la voie de concours nationaux d'agrégation et par concours organisés en application des dispositions du 3° et du 4° de l'article 46 " ; qu'il résulte de ces dispositions que la voie de concours définie au 1° de l'article 46, destinée aux personnes titulaires d'une habilitation à diriger des recherches, n'est pas ouverte dans les matières énumérées à l'article 48 ;<br/>
<br/>
              3. Considérant que les candidats reçus à un concours ne peuvent se prévaloir d'un droit à être nommé et que l'administration est tenue d'exercer, à tous les stades d'une procédure de nomination, un contrôle de légalité ; que dans l'exercice de ce contrôle, les services du ministère de l'enseignement supérieur et de la recherche ont estimé, à bon droit, que les dispositions précitées de l'article 48 du décret du 6 juin 1984 faisaient obstacle à ce qu'un professeur de sciences économiques soit recruté par la voie d'un concours organisé au titre du 1° de l'article 46 de ce décret ; que, par suite, le ministre de l'enseignement supérieur et de la recherche était tenu d'interrompre la procédure en refusant de transmettre, pour nomination, au Président de la République la proposition du conseil d'administration de l'université des Antilles et de la Guyane de nommer M. A...; que, dès lors que le ministre se trouvait en situation de compétence liée, les moyens tirés de ce que la décision devait être motivée en application de l'article 1er de la loi du 11 juillet 1979 et faire l'objet d'une procédure contradictoire sont inopérants ; <br/>
<br/>
              4. Considérant, en deuxième lieu, que la décision d'interrompre la procédure de recrutement par concours ne constitue pas le retrait d'une décision créatrice de droit ; que la circonstance qu'ait été publié dans l'application informatique " Galaxie " un avis informant M. A... de son " affectation " alors même que son décret de nomination n'était pas paru, pour regrettable qu'elle soit, ne peut être regardée comme la décision de nomination et d'affectation d'un professeur des universités susceptible de créer des droits pour l'intéressé, mais constitue un simple avis d'information sur l'état de la procédure ; <br/>
<br/>
              5. Considérant, en troisième lieu, que la décision du ministre, eu égard à ses motifs, impliquait nécessairement que le concours ne soit pas repris ; que, par suite, l'université a pu, sans erreur de droit, refuser de reprendre la procédure ;  <br/>
<br/>
              6. Considérant qu'il résulte de ce qui précède que les conclusions du requérant tendant à l'annulation pour excès de pouvoir des décisions attaquées doivent être rejetées ; que, dès lors, ses conclusions à fin d'injonction ne peuvent qu'être également rejetées ; <br/>
<br/>
              Sur les conclusions indemnitaires : <br/>
<br/>
              7. Considérant qu'en vertu de l'article R. 421-1 du code de justice administrative, la juridiction administrative ne peut, sauf en matière de travaux publics, être saisie que par voie de recours formé contre une décision ; qu'il résulte des pièces du dossier que, si M. A...soutient détenir sur l'administration une créance de 66 276 euros en raison du préjudice qu'il aurait subi par sa faute, il n'a jamais demandé à l'administration le règlement de cette somme ; que, par suite, ses conclusions indemnitaires, qui ne sont dirigées contre aucune décision explicite ou implicite de refus d'acquitter cette somme, sont, ainsi que le soutient le ministre, irrecevables ; <br/>
<br/>
              Sur les conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative : <br/>
<br/>
              8. Considérant que ces dispositions font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat ou de l'université des Antilles et de la Guyane qui ne sont pas, dans la présente instance, les parties perdantes ;<br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er : La requête de M. A...est rejetée. <br/>
Article 2 : La présente décision sera notifiée à M. B...A..., à l'université des Antilles et de la Guyane et à la ministre de l'éducation nationale, de l'enseignement supérieur et de la recherche.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
