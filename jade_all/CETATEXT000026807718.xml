<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026807718</ID>
<ANCIEN_ID>JG_L_2012_12_000000342315</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/80/77/CETATEXT000026807718.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  9ème sous-section jugeant seule, 20/12/2012, 342315, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2012-12-20</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>342315</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 9ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DELAPORTE, BRIARD, TRICHET</AVOCATS>
<RAPPORTEUR>M. Olivier Gariazzo</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Claire Legras</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2012:342315.20121220</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 9 août et 9 novembre 2010 au secrétariat du contentieux du Conseil d'Etat, présentés pour la Compagnie financière AP, dont le siège social est situé 20, rue de Cantolle à Genève (1205) en Suisse ; la Compagnie financière AP demande au Conseil d'Etat d'annuler l'arrêt n° 08PA03890 du 7 mai 2010 par lequel la cour administrative d'appel de Paris a rejeté sa requête tendant à l'annulation du jugement n° 0105523/2 du 25 mars 2008 du tribunal administratif de Paris rejetant sa demande tendant à la décharge de la cotisation d'impôt sur les sociétés à laquelle elle a été assujettie au titre de l'année 1993 ainsi que des pénalités correspondantes ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code général des impôts et le livre des procédures fiscales ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Olivier Gariazzo, Maître des Requêtes en service extraordinaire,  <br/>
<br/>
              - les observations de la SCP Delaporte, Briard, Trichet, avocat de la Compagnie financière AP,<br/>
<br/>
              - les conclusions de Mme Claire Legras, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Delaporte, Briard, Trichet, avocat de la Compagnie financière AP ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que la société anonyme Compagnie financière AP, résidente fiscale de Suisse, et la société à responsabilité limitée (SARL) Fidair International ont constitué ensemble en 1993 la société en participation (SEP) Beech Air 1390, dissoute la même année, dont elles détenaient respectivement 75 % et 25 % des parts, en vue de l'acquisition et de la revente d'un avion ; que l'administration a procédé le 12 décembre 1996 à la vérification de comptabilité de la SARL Fidair International au titre des exercices clos en 1993, 1994 et 1995 ; que la SARL Fidair International, gérante de la SEP Beech Air 1390, a retracé dans sa propre comptabilité les opérations d'achat, de location et de revente de l'avion réalisées par la SEP Beech Air 1390 en 1993, dont l'administration a pris connaissance à l'occasion de la vérification ; que l'administration a également pris connaissance lors de cette vérification du bénéfice dégagé par ces opérations, dont la SARL Fidair International avait déclaré sa quote-part, égale à 25 % ; que la SEP Beech Air 1390 a par la suite fait l'objet d'un contrôle ayant abouti à l'envoi d'une notification de redressement à sa gérante, la SARL Fidair International, le 20 décembre 1996 ; que l'administration a adressé le même jour à la compagnie financière AP une notification de redressement relative à l'impôt sur les sociétés au titre de l'année 1993 à raison de sa part dans les bénéfices redressés de la SEP Beech Air 1390 ; que la compagnie financière AP se pourvoit en cassation contre l'arrêt par lequel la cour administrative d'appel de Paris a confirmé le jugement rejetant sa demande tendant à la décharge de cette imposition et des pénalités correspondantes ;<br/>
<br/>
              2. Considérant que la compagnie financière AP soutient que la cour a commis une erreur de droit et a inexactement qualifié les faits dont elle était saisie en jugeant que le contrôle dont la SEP Beech Air 1390 a fait l'objet ne constituait pas une vérification de comptabilité, et n'avait donc pas à être précédé de l'envoi d'un avis de vérification ; <br/>
<br/>
              3. Considérant qu'aux termes de l'article L. 13 du livre des procédures fiscales dans sa rédaction applicable à l'imposition en litige : " Les agents de l'administration des impôts vérifient sur place (...) la comptabilité des contribuables astreints à tenir et à présenter des documents comptables " ; qu'aux termes de l'article R. 13-1 du même livre : " Les vérifications de comptabilité mentionnées à l'article L. 13 comportent notamment : / a) La comparaison des déclarations souscrites par les contribuables avec les écritures comptables et avec les registres et documents de toute nature (...) ; / b) L'examen de la régularité, de la sincérité et du caractère probant de la comptabilité à l'aide particulièrement des renseignements recueillis à l'occasion de l'exercice du droit de communication, et de contrôles matériels " ; qu'aux termes de l'article L. 47 du même livre, dans sa rédaction applicable à l'imposition en litige : " Un examen contradictoire de l'ensemble de la situation fiscale personnelle d'une personne physique au regard de l'impôt sur le revenu ou une vérification de comptabilité ne peut être engagée sans que le contribuable en ait été informé par l'envoi ou la remise d'un avis de vérification " ;<br/>
<br/>
              4. Considérant qu'il résulte de ces dispositions que l'administration procède à la vérification de comptabilité d'une entreprise lorsqu'en vue d'assurer l'établissement d'impôts ou de taxes totalement ou partiellement éludés par les intéressés, elle contrôle sur place la sincérité des déclarations fiscales souscrites par cette entreprise en les comparant avec les écritures comptables ou les pièces justificatives dont elle prend alors connaissance ; qu'en revanche, ne constitue pas une vérification de comptabilité le contrôle sur pièces par le service, dans ses propres locaux, des déclarations ou de l'absence de déclarations à l'impôt sur les sociétés d'un contribuable, à l'aide d'éléments en possession de l'administration, qui peuvent avoir été recueillis lors de la vérification de comptabilité d'un autre contribuable ;<br/>
<br/>
              5. Considérant que la cour administrative d'appel de Paris a jugé, par une appréciation souveraine non arguée de dénaturation, que c'est en recoupant des constatations issues des déclarations de la SEP Beech Air 1390 et de la SARL Fidair International que l'administration a déterminé le bénéfice non déclaré de la SEP duquel a résulté l'imposition de la Compagnie financière AP ; que si l'administration a nécessairement pu examiner des écritures comptables relatives à des opérations réalisées par la SEP Beech Air 1390 dans le cadre de la vérification de comptabilité de la SARL Fidair International, compte tenu de ce que cette dernière retraçait ces opérations, cet examen ne saurait être regardé comme ayant eu pour objet de contrôler la sincérité des déclarations fiscales souscrites par la SEP en les comparant avec ses écritures comptables ; qu'ainsi la cour administrative d'appel a pu, sans commettre d'erreur de droit ni d'erreur de qualification juridique, juger que les redressements contestés ne procédaient pas d'une vérification de comptabilité qui aurait été irrégulière au regard des dispositions précitées de l'article L. 47 du livre des procédures fiscale faute d'avoir été précédée de l'envoi d'un avis de vérification ;<br/>
<br/>
              6. Considérant qu'il résulte de ce qui précède que le pourvoi de la compagnie financière AP doit être rejeté, y compris ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : Le pourvoi de la compagnie financière AP est rejeté. <br/>
Article 2 : La présente décision sera notifiée à la compagnie financière AP et au ministre de l'économie et des finances.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
