<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032374819</ID>
<ANCIEN_ID>JG_L_2016_04_000000389821</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/37/48/CETATEXT000032374819.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème - 5ème chambres réunies, 06/04/2016, 389821, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-04-06</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>389821</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème - 5ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ODENT, POULET ; SCP ROCHETEAU, UZAN-SARANO</AVOCATS>
<RAPPORTEUR>M. Benjamin de Maillard</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Gaëlle Dumortier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2016:389821.20160406</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Le président de l'université Lumière Lyon 2 a saisi la section disciplinaire du conseil d'administration de l'université Lumière Lyon 2 d'une plainte à l'encontre de M. A...B..., maître de conférences. Par une décision du 24 septembre 2013, la section disciplinaire du conseil d'administration de l'université Lumière Lyon 2 a infligé à M. B...la sanction de l'exclusion temporaire de fonctions pour une durée de trois mois avec privation de la moitié de son traitement. <br/>
<br/>
              Par une décision n° 1038 du 10 février 2015, le Conseil national de l'enseignement supérieur et de la recherche statuant en matière disciplinaire a, sur appel du président de l'université Lumière Lyon 2 et de la rectrice de l'académie de Lyon, annulé cette décision et prononcé à l'encontre de M. B...la sanction du blâme.<br/>
<br/>
              Par un pourvoi et un mémoire en réplique, enregistrés les 28 avril et 15 septembre 2015 au secrétariat du contentieux du Conseil d'Etat, l'université Lumière Lyon 2 demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cette décision ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de M. B...la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de l'éducation ;<br/>
              - la loi n° 83-634 du 13 juillet 1983 ;<br/>
              - la loi n° 84-16 du 11 janvier 1984 ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Benjamin de Maillard, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Gaëlle Dumortier, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Odent, Poulet, avocat de l'université Lumière Lyon 2 et à la SCP Rocheteau, Uzan-Sarano, avocat de M.  B...; <br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes des dispositions de l'article R. 712-43 du code de l'éducation : " L'appel et l'appel incident peuvent être formés devant le Conseil national de l'enseignement supérieur et de la recherche, statuant en matière disciplinaire contre les décisions des sections disciplinaires des universités, par les personnes à l'encontre desquelles ces décisions ont été rendues, par leurs représentants légaux, par le président de l'université, par le recteur d'académie ou par le ministre chargé de l'enseignement supérieur lorsque les poursuites concernent le président de l'université " ; <br/>
<br/>
              2. Considérant, d'une part, qu'aux termes des dispositions de l'article 25 de la loi du 11 janvier 1984 portant dispositions statutaires relatives à la fonction publique de l'Etat : " I.-Les fonctionnaires et agents non titulaires de droit public consacrent l'intégralité de leur activité professionnelle aux tâches qui leur sont confiées. Ils ne peuvent exercer à titre professionnel une activité privée lucrative de quelque nature que ce soit. / (...) Les fonctionnaires et agents non titulaires de droit public peuvent toutefois être autorisés à exercer, dans des conditions fixées par décret en Conseil d'Etat, à titre accessoire, une activité, lucrative ou non, auprès d'une personne ou d'un organisme public ou privé, dès lors que cette activité est compatible avec les fonctions qui leur sont confiées et n'affecte pas leur exercice. / (...) V.-Sans préjudice de l'application de l'article 432-12 du code pénal, la violation du présent article donne lieu au reversement des sommes indûment perçues, par voie de retenue sur le traitement " ; que, d'autre part, en vertu des dispositions de l'article L. 952-8 du code de l'éducation, le blâme constitue la plus faible des sept sanctions susceptibles d'être infligées aux enseignants-chercheurs ; <br/>
<br/>
              3. Considérant que si le choix de la sanction relève de l'appréciation des juges du fond au vu de l'ensemble des circonstances de l'espèce, il appartient au juge de cassation de vérifier que la sanction retenue n'est pas hors de proportion avec la faute commise et qu'elle a pu dès lors être légalement prise ;<br/>
<br/>
              4. Considérant qu'il résulte des termes de la décision attaquée du Conseil national de l'enseignement supérieur et de la recherche que, pour justifier la prise d'une sanction disciplinaire à l'égard de M.B..., le conseil national a relevé que celui-ci avait signé une convention avec un organisme privé de Guadeloupe au nom de l'université Lumière Lyon 2 sans avoir eu l'accord de son conseil d'administration, qu'il avait exercé une activité rémunérée auprès de cet organisme privé sans avoir obtenu, ni même demandé, d'autorisation de cumul et qu'il n'avait pas respecté les conditions d'inscription en licence et en master des étudiants issus de cet organisme ni les conditions de délivrance de ces diplômes à ces mêmes étudiants ; qu'il résulte également des termes de la décision attaquée que, pour limiter la sanction à un blâme, le conseil national a retenu que les dysfonctionnements de l'université et les relations conflictuelles entre M. B...et le président de l'université expliquaient en partie les manquements de l'intéressé relatifs aux inscriptions des étudiants et à la délivrance des diplômes et qu'une retenue sur traitement avait déjà privé M. B...du bénéfice qu'il avait tiré de l'exercice d'une activité privée lucrative ; <br/>
<br/>
              5. Considérant qu'eu égard au fait que la nécessité d'obtenir une autorisation de cumul avant d'exercer une activité privée lucrative est une obligation qui s'impose à tout fonctionnaire et agent public en application de dispositions législatives établies de longue date, que le remboursement des sommes perçues sans autorisation n'a pas le caractère d'une sanction et eu égard à la gravité des manquements relevés aux obligations des enseignants-chercheurs en ce qui concerne la délivrance des diplômes, alors même que des dysfonctionnements peuvent être imputés à l'université Lumière Lyon 2, le Conseil national de l'enseignement supérieur et de la recherche, en n'infligeant à M. B...qu'une sanction du niveau le plus faible de celles susceptibles d'être infligées à un enseignant-chercheur, a retenu une sanction hors de proportion avec les fautes commises ; que, par suite, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, sa décision doit être annulée ;<br/>
<br/>
              6. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'université Lumière Lyon 2 qui n'est pas, dans la présente instance, la partie perdante ; qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de M. B...une somme de 3 000 euros à verser à l'université Lumière Lyon 2 en application des mêmes dispositions ;<br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er : La décision du Conseil national de l'enseignement supérieur et de la recherche statuant en matière disciplinaire du 10 février 2015 est annulée. <br/>
<br/>
Article 2 : L'affaire est renvoyée au Conseil national de l'enseignement supérieur et de la recherche statuant en matière disciplinaire.<br/>
<br/>
Article 3 : Les conclusions présentées par M. B...au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 4 : M B...versera à l'université Lumière Lyon 2 la somme de 3 000 euros en application des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 5 : La présente décision sera notifiée à l'université Lumière Lyon 2 et à M. A...B....<br/>
Copie en sera adressée à la ministre de l'éducation nationale, de l'enseignement supérieur et de la recherche.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
