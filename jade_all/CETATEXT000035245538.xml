<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000035245538</ID>
<ANCIEN_ID>JG_L_2017_07_000000400387</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/35/24/55/CETATEXT000035245538.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème - 9ème chambres réunies, 19/07/2017, 400387</TITRE>
<DATE_DEC>2017-07-19</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>400387</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème - 9ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GARREAU, BAUER-VIOLAS, FESCHOTTE-DESBOIS</AVOCATS>
<RAPPORTEUR>M. Arno Klarsfeld</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Aurélie Bretonneau</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2017:400387.20170719</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A...B...a demandé à la Cour nationale du droit d'asile d'annuler la décision du directeur général de l'Office français de protection des réfugiés et apatrides du 18 juillet 2014 rejetant sa demande d'asile. Par une décision n° 14024686 du 31 août 2015 la Cour nationale du droit d'asile a rejeté sa demande. <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire enregistrés les 3 juin et 5 septembre 2016 au secrétariat du contentieux du Conseil d'Etat, M. A...B...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette décision ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à ses conclusions devant la Cour ;<br/>
<br/>
              3°) de mettre à la charge de l'OFPRA une somme de 3 500 euros à verser à la SCP Garreau, Bauer-Violas, Feschotte-Desbois au titre de l'article L. 761-1 du code de justice administrative et de l'article 37 de la loi du 10 juillet 1991.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 29 juin 2017, présentée par MB....<br/>
<br/>
              Vu :<br/>
              - la convention de Genève du 28 juillet 1951 relative au statut des réfugiés et le protocole signé à New York le 31 janvier 1967 relatif au statut des réfugiés ;<br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ; <br/>
              - le décret n° 91-1266 du 19 décembre 1991 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Arno Klarsfeld, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Aurélie Bretonneau, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Garreau, Bauer-Violas, Feschotte-Desbois, avocat de M. A...B...;<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. Aux termes du deuxième alinéa de l'article L. 733-1 du code de l'entrée et du séjour des étrangers et du droit d'asile : " Afin d'assurer une bonne administration de la justice et de faciliter la faculté ouverte aux intéressés de présenter leurs explications à la cour, le président de cette juridiction peut prévoir que la salle d'audience de la cour est reliée, en direct, par un moyen de communication audiovisuelle qui garantit la confidentialité de la transmission avec une salle d'audience spécialement aménagée à cet effet ouverte au public et située dans des locaux relevant du ministère de la justice plus aisément accessibles par le demandeur, dans des conditions respectant les droits de l'intéressé prévues par le premier alinéa.  [ ...] Ces opérations donnent lieu à l'établissement d'un procès-verbal dans chacune des salles d'audience ou à un enregistrement audiovisuel ou sonore ... ". Aux termes de l'article R 733-23 du même code : " Sauf dans le cas où il est procédé à un enregistrement audiovisuel ou sonore de l'audience, un procès-verbal est rédigé par l'agent chargé du greffe dans chacune des deux salles d'audience./ Chacun de ces procès-verbaux mentionne : - le nom et la qualité de l'agent chargé de sa rédaction ...- la date et l'heure du début de la communication audiovisuelle ; -les éventuels incidents techniques relevés lors de l'audience, susceptibles d'avoir perturbé la communication ; -l'heure de la fin de la communication audiovisuelle. / Le cas échéant, sont également mentionnés le nom de l'avocat et le nom de l'interprète sur le procès-verbal établi dans la salle d'audience où ils se trouvent. / Ces procès-verbaux attestent de l'ouverture au public des deux salles d'audience ... ". <br/>
<br/>
              2. Il ressort des pièces du dossier de la Cour nationale du droit d'asile que la décision attaquée rejetant le recours de M. B...contre la décision de l'Office français de protection des réfugiés et apatrides du 18 juillet 2014 rejetant sa demande d'asile et lui refusant le bénéfice de la protection subsidiaire, a été rendue à la suite d'une audience au cours de laquelle l'intéressé a présenté ses explications à la cour par " vidéo- audience ", en application  des dispositions, citées au point 1, du deuxième alinéa de l'article L. 733-1 du code de l'entrée et du séjour des étrangers et du droit d'asile. L'audience a ainsi eu lieu, le 15 juillet 2015, simultanément dans la salle d'audience de la cour à Montreuil et dans une salle d'audience à Mayotte, où se trouvait M.B.... Mais un seul procès-verbal d'audience, signé par la secrétaire d'audience présente au siège de la cour à Montreuil, a été dressé. Le second procès verbal d'audience, requis par les dispositions précitées, n'a pas été dressé par l'agent chargé du greffe de la salle d'audience à Mayotte où était présent le requérant. Eu égard à la portée de l'article R. 733-23 du code de justice administrative, l'absence de ce procès verbal, à elle seule, entache la régularité de la procédure. Par suite, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, M. B...est fondé à demander l'annulation de la décision qu'il attaque. <br/>
<br/>
              3. M. B...a obtenu le bénéfice de l'aide juridictionnelle. Par suite, son avocat peut se prévaloir des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991. Il y a donc lieu, dans les circonstances de l'espèce, et sous réserve que la SCP Garreau Bauer-Violas Feschotte-Desbois renonce à percevoir la somme correspondant à la part contributive de l'Etat, de mettre à la charge de l'Office français de protection des réfugiés et apatrides le versement à cette SCP de la somme de 3 000 euros.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La décision de la Cour nationale du droit d'asile du 31 août 2015 est annulée. <br/>
Article 2 : L'affaire est renvoyée à la Cour nationale du droit d'asile. <br/>
Article 3 : L'Office français de protection des réfugiés et apatrides versera à l'avocat de M. B..., la SCP Garreau Bauer-Violas Feschotte-Desbois, sous réserve qu'elle renonce à l'indemnité due au titre de l'aide juridictionnelle totale, la somme de 3 000 euros sur le fondement des dispositions du 2ème alinéa de l'article 37 de la loi du 10 juillet 1991. <br/>
Article 4 : La présente décision sera notifiée à M. A...B...et au directeur général de l'Office français de protection des réfugiés et apatrides.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">095-08-04-03 - OBLIGATION DE DRESSER UN SECOND PROCÈS VERBAL D'AUDIENCE EN CAS DE VIDÉO-AUDIENCE - MÉCONNAISSANCE - CONSÉQUENCE - IRRÉGULARITÉ DE LA PROCÉDURE.
</SCT>
<ANA ID="9A"> 095-08-04-03 Décision de la CNDA rendue à la suite d'une audience au cours de laquelle l'intéressé a présenté ses explications à la cour par vidéo-audience, depuis Mayotte, en application du deuxième alinéa de l'article L. 733-1 du code de l'entrée et du séjour des étrangers et du droit d'asile (CESEDA). Un seul procès-verbal d'audience, signé par la secrétaire d'audience présente au siège de la cour à Montreuil, a été dressé, le second procès verbal d'audience, requis par le deuxième alinéa de l'article L. 733-1 du CESEDA, n'ayant pas été dressé par l'agent chargé du greffe de la salle d'audience à Mayotte où était présent le requérant. L'absence de ce procès verbal, à elle seule, entache d'irrégularité de la procédure.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
