<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039217428</ID>
<ANCIEN_ID>JG_L_2019_10_000000419254</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/21/74/CETATEXT000039217428.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  9ème chambre jugeant seule, 14/10/2019, 419254, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-10-14</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>419254</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 9ème chambre jugeant seule</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP CELICE, SOLTNER, TEXIDOR, PERIER</AVOCATS>
<RAPPORTEUR>M. Nicolas Agnoux</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emilie Bokdam-Tognetti</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2019:419254.20191014</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société Air Limousines a demandé au tribunal administratif de Montreuil de prononcer la décharge, en droits et pénalités, des rappels de taxe sur la valeur ajoutée qui lui ont été réclamés au titre de la période du 1er janvier 2009 au 31 décembre 2011. Par un jugement n° 1402992 du 5 octobre 2015, le tribunal administratif de Montreuil a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 15VE03388 du 25 janvier 2018, la cour administrative d'appel de Versailles a rejeté l'appel formé par la société contre ce jugement. <br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 26 mars et 26 juin 2018 au secrétariat du contentieux du Conseil d'Etat, la société Air Limousines demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Nicolas Agnoux, maître des requêtes,<br/>
<br/>
              - les conclusions de Mme Emilie Bokdam-Tognetti, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Célice, Soltner, Texidor, Perier, avocat de la société Air Limousines ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que la société Air Limousines, qui exerce une activité dite de " grande remise ", a fait l'objet d'une vérification de comptabilité au titre de la période comprise entre le 1er janvier 2009 et le 31 décembre 2011 à l'issue de laquelle l'administration fiscale a remis en cause l'application à certaines des prestations qu'elle fournit du taux réduit de taxe sur la valeur ajoutée (TVA) de 5,5 %. La société se pourvoit en cassation contre l'arrêt du 25 janvier 2018 par lequel la cour administrative d'appel de Versailles a rejeté son appel contre le jugement du 5 octobre 2015 du tribunal administratif de Montreuil rejetant sa demande en décharge des rappels de TVA qui lui ont été réclamés à la suite de cette rectification.<br/>
<br/>
              2. Aux termes de l'article 279 du code général des impôts dans sa version applicable au litige : " La taxe sur la valeur ajoutée est perçue au taux réduit de 5,50 % en ce qui concerne : / (...) b quater les transports de voyageurs (...) ". Le taux réduit de 5,5 % s'applique aux mises à disposition, avec chauffeur, de véhicules conçus pour le transport de personnes lorsque ces opérations procèdent de l'exécution de contrats qui peuvent être qualifiés de contrats de transport, compte tenu notamment de leurs stipulations relatives à l'assurance et à la responsabilité du propriétaire ainsi qu'aux conditions concrètes d'exploitation de l'activité, en particulier des stipulations relatives à la tarification et à la maîtrise du déplacement par le propriétaire du véhicule. Ne relèvent pas d'une telle qualification, faute d'accord préalable sur les trajets à effectuer, les mises à disposition, avec chauffeur, de véhicules conçus pour le transport de personnes facturées à l'heure, pour lesquelles le tarif est totalement indépendant de la distance parcourue, voire de l'existence ou non d'un déplacement, comme les prestations assorties d'un kilométrage illimité ou celles dont les tarifs sont calculés exclusivement en fonction de la tranche horaire et de la durée de la prestation.<br/>
<br/>
              3. En jugeant que les prestations en litige ne pouvaient bénéficier du taux réduit de TVA au motif que leur facturation se fondait principalement sur une durée d'utilisation et non sur une distance effectivement parcourue ou sur un itinéraire précis établi à l'avance, alors qu'il résulte du point 2 ci-dessus que seule une facturation fondée exclusivement sur la durée d'utilisation permettait d'écarter la qualification de contrat de transports au sens du b quater de l'article 279 du code général des impôts et qu'il lui appartenait donc de vérifier si le tarif de chacune des prestations en cause remplissait ce critère, la cour a commis une erreur de droit.<br/>
<br/>
              4. Il résulte de ce qui précède, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, que la société requérante est fondée à demander l'annulation de l'arrêt qu'elle attaque.<br/>
<br/>
              5. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 3 000 euros à verser à la société Air Limousines au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Versailles du 25 janvier 2018 est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Versailles.<br/>
Article 3 : L'Etat versera une somme de 3 000 euros à la société Air Limousines au titre de l'article L. 761-1 du code de justice administrative. <br/>
Article 4 : La présente décision sera notifiée à la société Air Limousines et au ministre de l'action et des comptes publics.<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
