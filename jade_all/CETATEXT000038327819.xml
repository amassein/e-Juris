<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038327819</ID>
<ANCIEN_ID>JG_L_2019_04_000000417372</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/32/78/CETATEXT000038327819.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème chambre, 01/04/2019, 417372, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-04-01</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>417372</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP THOUVENIN, COUDRAY, GREVY ; SCP CELICE, SOLTNER, TEXIDOR, PERIER</AVOCATS>
<RAPPORTEUR>Mme Céline Roux</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Dieu</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2019:417372.20190401</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>M. Amadou Ibrahim-Boubou a demandé au tribunal administratif de Dijon d'annuler pour excès de pouvoir la décision du 2 février 2015 par laquelle le ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social a autorisé l'association Unidom 21 à le licencier. Par un jugement n° 1500946 du 16 février 2016, le tribunal administratif a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 16LY01393 du 16 novembre 2017, la cour administrative d'appel de Lyon a rejeté l'appel formé par M. A... contre ce jugement.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 16 janvier et 16 avril 2018 au secrétariat du contentieux du Conseil d'Etat, M. A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat et de l'association Unidom 21 la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
                      Vu les autres pièces du dossier ;<br/>
<br/>
                      Vu :<br/>
<br/>
                      - le code du travail ;<br/>
                      - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Céline Roux, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Frédéric Dieu, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Thouvenin, Coudray, Grevy, avocat de M. A...et à la SCP Célice, Soltner, Texidor, Perier, avocat de l'association Unidom 21 ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1.  Il ressort des pièces du dossier soumis aux juges du fond que, par une décision du 2 février 2015, le ministre du travail, de l'emploi, de la formation professionnelle et du dialogue social a autorisé l'association Unidom 21 à licencier M. A..., salarié protégé. Par un jugement du 16 février 2016, le tribunal administratif de Dijon a rejeté la demande de M. A...tendant à l'annulation pour excès de pouvoir de cette décision. Celui-ci se pourvoit en cassation contre l'arrêt du 16 novembre 2017 par lequel la cour administrative d'appel de Lyon a rejeté son appel formé contre ce jugement.<br/>
<br/>
              2. Aux termes de l'article R. 611-1 du code de justice administrative : " (...) La requête, le mémoire complémentaire annoncé dans la requête et le premier mémoire de chaque défendeur sont communiqués aux parties avec les pièces jointes (...). / Les répliques, autres mémoires et pièces sont communiqués s'ils contiennent des éléments nouveaux ". Par ailleurs, l'article R. 611-8-2 du même code dispose, dans sa rédaction alors applicable : " Toute juridiction  peut  adresser  par le moyen de l'application informatique   mentionnée  à  l'article R.414-1 à une partie ou à un mandataire qui y est inscrit, toutes les communications et notifications prévues par le présent livre pour tout dossier./Les parties ou leur mandataire sont réputés avoir reçu la communication ou la notification à la date de première consultation du document qui leur a été ainsi adressé, certifiée par l'accusé de réception délivré par l'application informatique, ou, à défaut de consultation dans un délai de huit jours à compter de la date de mise à disposition du document dans l'application, à l'issue de ce délai. Sauf demande contraire de leur part, les parties ou leur mandataire sont alertés de toute nouvelle communication ou notification par un message électronique envoyé à l'adresse choisie par eux (...) ". Enfin, l'article R. 613-2 de ce code dispose que : " Si le président de la formation de jugement n'a pas pris une ordonnance de clôture, l'instruction est close trois jours francs avant la date de l'audience (...) ". <br/>
<br/>
               3. Il ressort des termes mêmes de l'arrêt attaqué que, pour rejeter l'appel de M. A..., la cour administrative d'appel s'est notamment fondée sur des pièces produites devant elles pour la première fois par la ministre du travail, le 10 octobre 2017, notamment le rapport de contre-enquête diligenté dans le cadre de l'instruction du recours hiérarchique.<br/>
<br/>
              4. Or ces pièces ont été communiquées aux parties, le jour même de leur production, au moyen de l'application informatique mentionnée à l'article R. 414-1 du code de justice administrative. Par suite, M. A... et son mandataire, qui n'ont pas consulté cette application dans le délai de huit jours alors prévu par les dispositions de l'article R. 611-8-2 du même code, n'étaient, en application des mêmes dispositions, pas réputés en avoir reçu communication avant le 18 octobre 2017.<br/>
<br/>
              5. Par suite, l'instruction ayant été clôturée, en application des dispositions de l'article R. 613-2 du code de justice administrative, trois jours avant l'audience du 19 octobre 2017, soit le 16 octobre, M. A...est fondé à soutenir que l'arrêt qu'il attaque est entaché d'irrégularité et qu'il doit, sans qu'il soit besoin d'examiner les autres moyens de son pourvoi, être annulé.<br/>
<br/>
              6. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de M. A..., qui n'est pas la partie perdante dans la présente instance. Il n'y a pas lieu, dans les circonstances de l'espèce, de mettre à la charge de l'association Unidom 21 et de l'Etat la somme que M. A... demande au titre de ces mêmes dispositions.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Paris du 16 novembre 2017 est annulé.<br/>
Article 2 : L'affaire est renvoyée devant la cour administrative d'appel de Paris.<br/>
Article 3 : Les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative, tant par M. A... que par l'association Unidom 21, sont rejetées.<br/>
Article 4 : La présente décision sera notifiée à M. B... et à l'association Unidom 21.<br/>
Copie en sera adressée à la ministre du travail.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
