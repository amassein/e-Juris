<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029214508</ID>
<ANCIEN_ID>JG_L_2014_07_000000363521</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/21/45/CETATEXT000029214508.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème / 3ème SSR, 02/07/2014, 363521, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-07-02</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>363521</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème / 3ème SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP CELICE, BLANCPAIN, SOLTNER ; SCP MASSE-DESSEN, THOUVENIN, COUDRAY</AVOCATS>
<RAPPORTEUR>M. Jean-Marc Anton</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Benoît Bohnert</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:363521.20140702</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 23 octobre 2012 et 23 janvier 2013 au secrétariat du contentieux du Conseil d'Etat, présentés pour le Centre technique des industries mécaniques, dont le siège est 52, avenue Félix-Louat BP 80067 à Senlis (60304 cedex) ; le Centre technique des industries mécaniques demande au Conseil d'Etat :<br/>
<br/>
               1°) d'annuler l'arrêt n° 11DA00271 du 13 août 2012 par lequel la cour administrative d'appel de Douai, statuant sur la requête de la société anonyme Deville, a annulé le jugement n° 0802081 du 2 décembre 2010 du tribunal administratif d'Amiens et déchargé cette société des rappels de taxe pour le développement des industries du secteur mécanique auxquels elle a été assujettie au titre de la période du 1er juillet 2004 au 31 décembre 2005 ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;  <br/>
<br/>
              3°) de mettre à la charge de la société Deville la somme de 4 500 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code général des impôts et le livre des procédures fiscales ;<br/>
<br/>
              Vu la loi n° 2003-1312 du 30 décembre 2003, notamment l'article 71 ;<br/>
<br/>
              Vu le décret n° 2002-1622 du 31 décembre 2002 ;<br/>
<br/>
              Vu l'arrêté du 22 janvier 2004  fixant la liste des produits et services soumis aux taxes affectées aux actions collectives de développement économique et techniques de certains secteurs industriels modifié ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Marc Anton, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Benoît Bohnert, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Masse-Dessen, Thouvenin, Coudray, avocat du Centre technique des industries mécaniques et à la SCP Célice, Blancpain, Soltner, avocat de la société Deville ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant que le Centre technique des industries mécaniques se pourvoit en cassation contre l'arrêt du 13 août 2012 par lequel la cour administrative d'appel de Douai a déchargé la société Deville des rappels de taxe pour le développement des industries du secteur mécanique auxquels elle a été assujettie au titre de la période du 1er juillet 2004 au 31 décembre 2005 à raison de sa qualité de fabricant de ferrures et de pièces métalliques usinées ;<br/>
<br/>
              2. Considérant qu'aux termes du E de l'article 71 de la loi n° 2003-1312 du 30 décembre 2003, dans sa rédaction applicable au litige : " I. Il est institué une taxe pour le développement des industries des secteurs d'activités suivants : / 1° Mécanique ; / 2° Matériels et consommables de soudage ; / 3° Décolletage ; / 4° Construction métallique ; / 5° Matériels aérauliques et thermiques. / Le produit de cette taxe est affecté (...) aux centres techniques industriels couvrant ces secteurs, qui sont respectivement le Centre technique des industries mécaniques, l'Institut de la soudure, le Centre technique de l'industrie du décolletage, le Centre technique industriel de la construction métallique et le Centre technique des industries aérauliques et thermiques. / (...) II. La taxe est due par les fabricants, établis en France, des produits des secteurs d'activités mentionnés au I. Ces produits sont recensés, pour chacun de ces secteurs, par voie réglementaire et par référence au décret n° 2002-1622 du 31 décembre 2002 portant approbation des nomenclatures d'activités et de produits. (...) " ; <br/>
<br/>
              3. Considérant que ne  figure dans l'arrêté ministériel du 22 janvier 2004 qui recense les produits et services soumis à cette taxe aucune des rubriques d'activités économiques et de produits relatives à la fabrication ou à l'équipement des automobiles énumérées au décret du 31 décembre 2002 ; que, dans les cas où cet arrêté mentionne des produits susceptibles d'être utilisés dans la fabrication d'automobiles, il exclut cette destination, comme c'est le cas des rubriques 25.72.1, qui mentionne les " serrures et ferrures (...) à l'exclusion des serrures spécifiques à l'automobile " et 28.11, qui mentionne les " moteurs et turbines, à l'exclusion des moteurs pour (...) automobiles " ;<br/>
<br/>
              4. Considérant que la cour administrative d'appel de Douai a relevé que l'activité en litige consistait à fabriquer des boîtiers métalliques exclusivement affectés à l'installation d'" airbags " pour automobiles et des éléments de ceintures de sécurité tels que des renvois de ceinture ou des corps d'enrouleur ; qu'elle en a déduit que cette activité concourait exclusivement à la fabrication de pièces ou composants utilisés en l'état pour la fabrication d'automobiles, devant être classée dans les rubriques des " autres équipements mécaniques pour automobile " s'agissant des pièces pour " airbags " et des " équipements pour carrosserie automobile " s'agissant des éléments de ceinture ; qu'elle a relevé que ces rubriques n'étaient pas mentionnées dans l'arrêté du 22 janvier 2004  ; qu'elle n'a, dès lors, pas commis d'erreur de droit en jugeant que les produits en litige, fabriqués par la société Deville, ne relevaient pas du champ d'application de la taxe pour le développement des industries du secteur mécanique alors même qu'auraient été mises en oeuvre, pour leur fabrication, des techniques figurant dans des catégories d'opérations ou utilisées pour la fabrication de catégories de produits mentionnés à l'arrêté du 22 janvier 2004 fixant la liste des produits et services soumis à cette taxe ; <br/>
<br/>
              5. Considérant qu'il résulte de ce qui précède que le pourvoi du Centre technique des industries mécaniques doit être rejeté, y compris ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative ; qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de ce centre le versement à la société Deville d'une somme de 3 000 euros au titre de ces dispositions ;<br/>
<br/>
<br/>
<br/>                   D E C I D E :<br/>
                                  --------------<br/>
<br/>
 Article 1er : Le pourvoi du Centre technique des industries mécaniques est rejeté.<br/>
<br/>
 Article 2 : Le Centre technique des industries mécaniques versera à la société Deville une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
 Article 3 : La présente décision sera notifiée au Centre technique des industries mécaniques  et à la société anonyme Deville. <br/>
 Copie en sera adressée, pour information, au ministre des finances et des comptes publics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
