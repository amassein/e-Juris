<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037834611</ID>
<ANCIEN_ID>JG_L_2018_12_000000418331</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/83/46/CETATEXT000037834611.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème chambre, 19/12/2018, 418331, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-12-19</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>418331</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MATUCHANSKY, POUPOT, VALDELIEVRE</AVOCATS>
<RAPPORTEUR>M. Olivier Gariazzo</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Karin Ciavaldini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:418331.20181219</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme lgnazia B...a demandé au tribunal administratif de Nice, par deux requêtes distinctes, de prononcer la décharge des cotisations supplémentaires d'impôt sur le revenu et de contributions sociales auxquelles elle a été assujettie au titre des années 2008 à 2010, ainsi que des pénalités correspondantes. Par un jugement n° 1403693, 1501335 du 9 juin 2016, ce tribunal a réduit ses bases d'imposition à l'impôt sur le revenu et aux contributions sociales d'une somme de 4 500 euros au titre de l'année 2008, de 9 520 euros au titre de l'année 2009 et de 9 520 euros au titre de l'année 2010, lui a accordé la décharge des droits et pénalités correspondant à ces réductions de bases et a rejeté le surplus des conclusions de ses demandes.<br/>
<br/>
              Par un arrêt n° 16MA03364 du 19 décembre 2017 la cour administrative d'appel de Marseille a rejeté l'appel formé par Mme B... contre ce jugement en tant qu'il n'avait pas entièrement fait droit à ses demandes.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 19 février et 22 mai 2018 au secrétariat du contentieux du Conseil d'État, Mme B... demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 6 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Olivier Gariazzo, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Karin Ciavaldini, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Matuchansky, Poupot, Valdelièvre, avocat de MmeB....<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond qu'à la suite d'une vérification de comptabilité de la société luxembourgeoise Severland, Mme B..., associée de cette dernière, a été assujettie à des cotisations supplémentaires d'impôt sur le revenu et de contributions sociales au titre des années 2008 à 2010 à raison de sommes, regardées comme constitutives d'un avantage occulte en application des dispositions du c de l'article 111 du code général des impôts, correspondant à la mise à sa disposition à titre gratuit de la villa " La Gardiole " située à Biot. Mme B... a saisi le tribunal administratif de Nice d'une demande tendant à la décharge de ces impositions et des pénalités correspondantes. Par un jugement du 9 juin 2016, ce tribunal a réduit les bases d'imposition à l'impôt sur le revenu et aux contributions sociales d'une somme de 4 500 euros au titre de l'année 2008, de 9 520 euros au titre de l'année 2009 et de 9 520 euros au titre de l'année 2010, a accordé à Mme B... la décharge des droits et pénalités correspondant à ces réductions de bases et a rejeté le surplus des conclusions de sa demande. Mme B... se pourvoit en cassation contre l'arrêt du 19 décembre 2017 par lequel la cour administrative d'appel de Marseille a rejeté l'appel qu'elle avait formé contre ce jugement en tant qu'il n'avait pas entièrement fait droit à ses demandes. <br/>
<br/>
              2. En premier lieu, il ressort des écritures produites en appel par la requérante que celle-ci se bornait, pour contester avoir bénéficié d'un avantage lié à la mise à disposition gratuite à son profit de la villa " La Gardiole " par la société Severland, à soutenir qu'elle n'habitait pas cette villa, sans soulever aucune argumentation relative aux conditions de mise en oeuvre des dispositions du c de l'article 111 du code général des impôts. Par suite, la requérante n'est pas fondée à soutenir que la cour administrative d'appel aurait insuffisamment motivé son arrêt en omettant d'exposer les raisons pour lesquelles elle considérait que l'avantage en cause revêtait un caractère occulte et ne peut utilement soulever, pour la première fois en cassation, des moyens, qui ne sont pas d'ordre public, tirés de ce que la cour administrative aurait commis une erreur de droit et inexactement qualifié les faits de l'espèce en regardant les sommes en litige comme un avantage occulte sans que soit caractérisée leur dissimulation aux autorités fiscales luxembourgeoises, compte tenu des obligations comptables incombant aux sociétés luxembourgeoises en matière d'avantages en nature accordés à des tiers.<br/>
<br/>
              3. En deuxième lieu, après avoir relevé que, faute de disposer de termes de comparaison pertinents auxquels elle aurait pu se référer, l'administration avait déterminé la valeur locative de la villa " La Gardiole " non par voie de comparaison avec les loyers de propriétés similaires, mais par voie d'appréciation directe, à partir de sa valeur vénale, la cour administrative d'appel a écarté la contestation de Mme B... relative au recours à cette méthode au motif qu'elle ne faisait état d'aucune propriété similaire dans la même zone géographique, susceptible d'être utilisée comme terme de comparaison. Alors qu'il ressort des pièces du dossier soumis aux juges du fond que la requérante se bornait en appel à contester dans son principe le recours à la méthode d'appréciation directe sans proposer aucun terme de comparaison, celle-ci n'est fondée à soutenir ni que la cour aurait insuffisamment motivé son arrêt en ne précisant pas quelle était la zone géographique pertinente pour rechercher des éléments de comparaison, ni qu'elle aurait commis une erreur de droit en limitant cette zone au territoire de la seule commune de Biot. <br/>
<br/>
              4. En troisième lieu, en jugeant que l'administration n'avait pas fait une appréciation inexacte de la valeur locative de la villa en litige en appliquant un taux de rendement de 4 % à sa valeur vénale, la cour administrative d'appel n'a entaché son arrêt ni d'erreur de droit ni, compte tenu de l'argumentation qui lui était soumise sur ce point, laquelle se limitait à une affirmation selon laquelle ce taux était " incompatible avec la réalité du marché immobilier ", d'insuffisance de motivation.<br/>
<br/>
              5. Enfin, en jugeant que la requérante n'établissait pas le caractère exagéré de la valeur locative, d'environ 5 000 euros par mois, retenue par l'administration en produisant un contrat de location du bien en litige, conclu en 2011 par la société Severland et mentionnant un loyer de 3 000 euros par mois, alors qu'elle produisait simultanément une annonce diffusée dans une revue spécialisée proposant le même bien à la location pour une somme comprise entre 2 000 euros et 4 000 euros par semaine, la cour n'a ni entaché son arrêt de dénaturation des faits et des pièces du dossier, ni commis d'erreur de droit.<br/>
<br/>
              6. Il résulte de tout ce qui précède que le pourvoi de Mme B... doit être rejeté, y compris ses demandes présentées au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de Mme B... est rejeté.<br/>
Article 2 : La présente décision sera notifiée à Mme A... B...et au ministre de l'action et des comptes publics. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
