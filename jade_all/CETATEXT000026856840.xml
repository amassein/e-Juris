<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026856840</ID>
<ANCIEN_ID>JG_L_2012_12_000000353785</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/85/68/CETATEXT000026856840.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  5ème sous-section jugeant seule, 28/12/2012, 353785, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2012-12-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>353785</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 5ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>BALAT</AVOCATS>
<RAPPORTEUR>Mme Domitille Duval-Arnould</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2012:353785.20121228</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu 1°) le pourvoi sommaire et le mémoire complémentaire, enregistrés les 2 novembre 2011 et 2 février 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour la commune de Jettingen, représentée par son maire ; la commune de Jettingen demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler le jugement n° 0800406 du 30 août 2011 par lequel le tribunal administratif de Strasbourg, statuant sur la demande de M. Thierry C et de Mme Carole C, agissant en qualité de représentants légaux de leur fils Lucas, l'a condamnée à leur verser la somme de 3 000 euros au titre du dommage causé à ce dernier par la chute de la table de ping-pong du terrain de jeux de la commune ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter la demande présentée par M. et Mme C devant le tribunal administratif de Strasbourg ;<br/>
<br/>
              3°) à titre subsidiaire, de condamner la société Novadal, fabricant de la table, et la société Atout Sport, fournisseur de la table, à la garantir de toute condamnation prononcée à son encontre ;<br/>
<br/>
              4°) de mettre à la charge de M. et Mme C, de la société Novadal et de la société Atout Sport la somme de 1 500 euros chacun, au titre de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
<br/>
<br/>
              Vu 2°) le pourvoi sommaire et le mémoire complémentaire, enregistrés les 2 novembre 2011 et 31 janvier 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. Thierry C, demeurant ... et Mme Carole C, demeurant ..., agissant en qualité de représentants légaux de leur fils mineur Lucas ; M. et Mme C demandent au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler le jugement n° 0800406 du 30 août 2011 par lequel le tribunal administratif de Strasbourg a condamné la commune de Jettingen à leur verser la somme de 3 000 euros au titre du préjudice causé à Lucas C par la chute d'une table de ping-pong du terrain de jeu de cette commune ; <br/>
<br/>
              2°) de mettre à la charge de la commune de Jettingen le versement d'une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
<br/>
	Vu les autres pièces des dossiers ;<br/>
	Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Domitille Duval-Arnould, Maître des Requêtes en service extraordinaire,  <br/>
<br/>
              - les observations de Me Balat, avocat de la commune de Jettingen et de la SCP Fabiani, Luc-Thaler, avocat de M. et Mme C,<br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à Me Balat, avocat de la commune de Jettingen et à la SCP Fabiani, Luc-Thaler, avocat de M. et Mme C ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant que les requêtes de la commune de Jettingen et de M. et Mme C présentent à juger les mêmes questions ; qu'il y a lieu de les joindre pour statuer par une seule décision ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article R. 222-13 du code de justice administrative : " Le président du tribunal administratif ou le magistrat qu'il désigne à cette fin et ayant atteint au moins le grade de premier conseiller statue en audience publique et après audition du rapporteur public : ...7° Sur les actions indemnitaires, lorsque le montant des indemnités demandées est inférieur au montant déterminé par les articles R. 222-14 et R. 222-15 " ; que ce montant est fixé à 10 000 euros par l'article R. 222-14 ; que l'article R. 222-15 prévoit, dans son premier alinéa, qu'il est déterminé " par la valeur totale des sommes demandées dans la requête introductive d'instance " ; qu'il résulte du deuxième alinéa de l'article R. 811-1 que le tribunal administratif statue en premier et dernier ressort sur les litiges mentionnés, notamment, au 7° de l'article R. 222-13 ;<br/>
<br/>
              3. Considérant que les actions indemnitaires dont les conclusions, dans la requête introductive d'instance, n'ont pas donné lieu à une évaluation chiffrée ou ont donné lieu à une demande d'un montant inférieur à 10 000 euros entrent dans le champ d'application des dispositions du 7° de l'article R. 222-13 et du deuxième alinéa de l'article R. 811-1 du code de justice administrative ; que ces dispositions ne sauraient, toutefois, trouver application quand le requérant, dans sa requête introductive d'instance, a expressément demandé qu'une expertise soit ordonnée afin de déterminer l'étendue de son préjudice, ou s'est expressément référé à une demande d'expertise présentée par ailleurs, en se réservant de fixer le montant de sa demande au vu du rapport de l'expert ; que le jugement rendu sur une telle requête, qui doit l'être par une formation collégiale, est susceptible d'appel quel que soit le montant de la provision que le demandeur a, le cas échéant, sollicitée dans sa requête introductive d'instance comme celui de l'indemnité qu'il a chiffrée à l'issue de l'expertise ;<br/>
<br/>
              4. Considérant que, dans leur requête introductive d'instance enregistrée au greffe du tribunal administratif de Strasbourg le 29 janvier 2008, M. C et Mme C, agissant en qualité de représentants légaux de leur fils Lucas C, ont présenté des conclusions tendant à la condamnation de la commune de Jettingen à réparer les préjudices résultant de l'accident dont a été victime ce dernier, le 19 juillet 2004, sur une aire de jeux de cette commune ; qu'ils ont également demandé une provision de 3 000 euros à valoir sur les préjudices définitifs dans l'attente d'un nouvel examen médical de l'enfant par l'expert qui avait précédemment été désigné pour évaluer ses préjudices ; qu'ainsi le litige n'entrait pas dans le champ d'application des dispositions du 7° de l'article R. 222-13 du code de justice administrative et du deuxième alinéa de l'article R. 811-1 du même code et n'était donc pas au nombre de ceux sur lesquels le tribunal administratif statue en premier et dernier ressort ; qu'il suit de là que les requêtes de la commune de Jettingen et de M. C et de Mme C tendant à l'annulation du jugement par lequel le tribunal administratif de Strasbourg a statué sur les conclusions de M. C et de Mme C doivent être regardées comme des appels et ressortissent à la compétence de la cour administrative d'appel de Nancy ; qu'il y a lieu, dès lors, d'attribuer le jugement de ces requêtes à cette juridiction ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement des requêtes de la commune de Jettingen et de M. C et Mme C est attribué à la cour administrative d'appel de Nancy.<br/>
<br/>
Article 2 : La présente décision sera notifiée à la commune de Jettingen, à M. Thierry C, à Mme Carole C, à la Muta Santé, à la SAS Novadal et à la SARL Atout Sport.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
