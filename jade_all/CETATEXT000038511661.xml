<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038511661</ID>
<ANCIEN_ID>JG_L_2019_05_000000426461</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/51/16/CETATEXT000038511661.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème - 7ème chambres réunies, 27/05/2019, 426461, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-05-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>426461</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème - 7ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP NICOLAY, DE LANOUVELLE, HANNOTIN ; SCP MATUCHANSKY, POUPOT, VALDELIEVRE</AVOCATS>
<RAPPORTEUR>M. Bertrand Mathieu</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Guillaume Odinet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:426461.20190527</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par un mémoire, enregistré le 21 mars 2019 au secrétariat du contentieux du Conseil d'Etat, M. B...A...demande au Conseil d'Etat, en application de l'article 23-5 de l'ordonnance n° 58-1067 du 7 novembre 1958 et à l'appui de sa requête tendant à l'annulation de la décision de l'Agence française de lutte contre le dopage du 7 juin 2018, de renvoyer au Conseil constitutionnel la question de la conformité aux droits et libertés garantis par la Constitution du 1° de l'article L. 232-22 du code du sport. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la Constitution, notamment son Préambule et son article 61-1 ; <br/>
              - l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
              - le code du sport et notamment son article L. 232-22 ;<br/>
              - la loi n° 2012-158 du 1er février 2012 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Bertrand Mathieu, conseiller d'Etat en service extraordinaire,<br/>
<br/>
              - les conclusions de M. Guillaume Odinet, rapporteur public,<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Nicolaÿ, de Lanouvelle, Hannotin, avocat de M.A..., et à la SCP Matuchansky, Poupot, Valdelièvre, avocat de l'Agence française de lutte contre le dopage ; <br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1.	Aux termes du premier alinéa de l'article 23-5 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel : " Le moyen tiré de ce qu'une disposition législative porte atteinte aux droits et libertés garantis par la Constitution peut être soulevé (...) à l'occasion d'une instance devant le Conseil d'Etat (...) ". Il résulte des dispositions de ce même article que le Conseil constitutionnel est saisi de la question prioritaire de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux.<br/>
<br/>
              2.	Sur le fondement de ces dispositions, M. A...demande, à l'appui du recours qu'il a formé contre la décision du 7 juin 2018 par laquelle l'Agence française de lutte contre le dopage lui a infligé la sanction d'interdiction de participer pendant quatre ans à toute manifestation sportive donnant lieu à la remise de prix en argent ou en nature, à toute manifestation organisée ou autorisée par une fédération sportive française, ainsi qu'aux entrainements y préparant, que soit renvoyée au Conseil constitutionnel la question de la conformité aux droits et libertés garantis par la Constitution des dispositions du 1° de l'article L. 232-22 du code du sport.<br/>
<br/>
              3.	Aux termes de l'article L. 232-22 du code du sport, issu de l'ordonnance du 14 avril 2010 relative à la santé des sportifs et à la mise en conformité du code du sport avec les principes du code mondial antidopage, ratifiée par la loi du 1er février 2012 visant à renforcer l'éthique du sport et les droits des sportifs, et dans sa rédaction applicable au litige : " En cas d'infraction aux dispositions des articles L. 232-9, L. 232 9-1, L. 232-10, L. 232-14-5, L. 232-15, L. 232-15-1 ou L. 232-17, l'Agence française de lutte contre le dopage exerce un pouvoir de sanction dans les conditions suivantes : / (...)1° Elle est compétente pour infliger des sanctions disciplinaires aux personnes non licenciées ". <br/>
<br/>
              4.	Ces dispositions législatives, dans la rédaction alors en vigueur, sont applicables au litige au sens et pour l'application de l'article 23-5 de l'ordonnance du 7 novembre 1958, elles n'ont pas déjà été déclarées conformes à la Constitution par les motifs et le dispositif d'une décision du Conseil constitutionnel.<br/>
<br/>
              5.	Le moyen tiré de ce qu'elles portent atteinte aux droits et libertés garantis par la Constitution, notamment aux principes d'indépendance et d'impartialité découlant de l'article 16 de la Déclaration des droits de l'homme et du citoyen, soulève au regard, notamment de la décision du Conseil constitutionnel n° 2017-688 QPC du 2 février 2018, une question présentant un caractère sérieux compte tenu du fait qu'elles peuvent conduire à une confusion, au sein de l'Agence française de lutte contre le dopage, entre, d'une part, les fonctions d'appréciation de l'opportunité des poursuites et, d'autre part, les fonctions de jugement.<br/>
<br/>
              6.	Il y a lieu, par suite, de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité invoquée. <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : La question de la conformité à la Constitution des dispositions du 1° de l'article L. 232-22 du code du sport, dans leur rédaction issue de l'ordonnance du 14 avril 2010 relative à la santé des sportifs et à la mise en conformité du code du sport avec les principes du code mondial antidopage, est renvoyée au Conseil constitutionnel.<br/>
<br/>
Article 2 : Il est sursis à statuer sur la requête de M. A...jusqu'à ce que le Conseil constitutionnel ait tranché la question de constitutionnalité ainsi soulevée.<br/>
<br/>
Article 3 : La présente décision sera notifiée à M. B...A...et à l'Agence française de lutte contre le dopage. Copie en sera adressée au Premier ministre et à la ministre des sports.  <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
