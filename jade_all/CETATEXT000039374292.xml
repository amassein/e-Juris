<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039374292</ID>
<ANCIEN_ID>JG_L_2019_11_000000417176</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/37/42/CETATEXT000039374292.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème chambre, 13/11/2019, 417176, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-11-13</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>417176</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FOUSSARD, FROGER ; SCP DELVOLVE ET TRICHET</AVOCATS>
<RAPPORTEUR>M. Marc Firoud</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Gilles Pellissier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2019:417176.20191113</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société Entreprise Georges Cazeaux a demandé au tribunal administratif de Lille d'annuler la décision de résiliation du marché conclu avec elle par la commune de Bouvines pour la restauration de l'église Saint-Pierre, d'ordonner la reprise des relations contractuelles et de condamner la commune de Bouvines à lui verser la somme de 500 000 euros en réparation du préjudice commercial subi, la somme de 360 416,40 euros à titre d'indemnité pour exécution de travaux complémentaires, la somme de 77 155,79 euros au titre du solde du marché, la somme de 18 213,97 euros au titre du solde de la retenue de garantie ainsi que la somme de 29 400,35 euros au titre de la perte de marge sur les tranches conditionnelles. Par un jugement n° 1206489 du 29 février 2016, le tribunal administratif de Lille a jugé qu'il n'y avait pas lieu de statuer sur les conclusions tendant à la reprise des relations contractuelles et a rejeté le surplus de sa demande.<br/>
<br/>
              Par un arrêt n° 16DA00820 du 9 novembre 2017, la cour administrative d'appel de Douai a rejeté l'appel formé par Me A... B..., agissant en qualité de liquidateur de la société Entreprise Georges Cazeaux, contre ce jugement.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 9 janvier, 9 avril 2018 et 18 octobre 2019 au secrétariat du contentieux du Conseil d'Etat, Me B... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de la commune de Bouvines la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code général des collectivités territoriales ;<br/>
              - le code des marchés publics ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Marc Firoud, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Gilles Pellissier, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Foussard, Froger, avocat de Me B... et à la SCP Delvolvé et Trichet, avocat de la commune de Bouvines ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que, par un acte d'engagement signé le 20 janvier 2011, la commune de Bouvines a confié à la société Entreprise Georges Cazeaux l'exécution du lot n° 1 " échafaudages, maçonneries, pierres de taille, ravalement, étanchéité pieds de murs, vitraux et peinturage " du marché de restauration de l'église Saint-Pierre. La date contractuelle d'achèvement de la première tranche des travaux, qui devait durer dix mois, a été fixée au 1er février 2012. Compte tenu du retard d'exécution constaté, le maire a adressé à la société, le 16 juillet 2012, une mise en demeure de terminer les travaux pour le 31 juillet 2012. Cette mise en demeure étant restée infructueuse, la commune a décidé de résilier le marché par une décision du 10 septembre 2012. La société Entreprise Georges Cazeaux a demandé au tribunal administratif de Lille d'annuler cette décision de résiliation, d'ordonner la reprise des relations contractuelles et de condamner la commune de Bouvines à lui verser diverses sommes au titre du règlement du solde du marché et de la réparation du préjudice subi. Par un jugement du 29 février 2016, le tribunal administratif de Lille a jugé qu'il n'y avait pas lieu de statuer sur les conclusions tendant à la reprise des relations contractuelles et a rejeté le surplus de sa demande. Me B..., agissant en qualité de liquidateur de la société Entreprise Georges Cazeaux, a relevé appel de ce jugement en tant seulement qu'il n'a pas fait droit aux conclusions indemnitaires de cette société. Il se pourvoit en cassation contre l'arrêt du 9 novembre 2017 par lequel la cour administrative d'appel de Douai a rejeté cet appel.<br/>
<br/>
              2. En premier lieu, il ressort des pièces du dossier soumis aux juges du fond, ainsi que l'a relevé la cour dans l'arrêt attaqué, qu'à la date de la décision de résiliation en litige, le retard d'exécution de la première tranche du marché confié à la société Entreprise Georges Cazeaux était de neuf mois, soit près du double du délai contractuellement fixé de dix mois, et qu'un tel retard, s'il se prolongeait, était de nature à remettre en cause l'objectif de la commune d'achever la restauration de l'église avant la commémoration des 800 ans de la bataille de Bouvines en juillet 2014, compte tenu du délai d'exécution prévisionnel des trois autres tranches. La cour administrative d'appel de Douai n'a pas dénaturé les faits en estimant que les causes des retards constatés n'étaient pas extérieures au titulaire du marché. Elle n'a pas davantage entaché son arrêt d'une erreur de qualification juridique des faits en jugeant que la décision de résiliation du marché, bien que prise par une autorité incompétente, était justifiée sur le fond. <br/>
<br/>
              3. En deuxième lieu, le caractère global et forfaitaire du prix du marché ne fait pas obstacle à ce que l'entreprise cocontractante sollicite une indemnisation au titre de travaux supplémentaires effectués, même sans ordre de service, dès lors que ces travaux étaient indispensables à la réalisation de l'ouvrage dans les règles de l'art. En rejetant la demande d'indemnisation au titre de travaux supplémentaires au motif que la société Entreprise Georges Cazeaux n'apportait aucun élément précis relatif à la nature exacte des travaux en cause, qui correspondent pour l'essentiel, ainsi qu'il a été dit au point 2, à la réparation de ses propres négligences ou de celles de son sous-traitant, la cour administrative d'appel de Douai, qui a suffisamment motivé son arrêt sur ce point, n'a dénaturé ni les faits ni les pièces du dossier.<br/>
<br/>
              4. En dernier lieu, le moyen tiré de ce que la cour a commis une erreur de droit en jugeant que la commune de Bouvines pouvait mettre à la charge de la société titulaire du marché les frais liés au marché de substitution alors qu'elle avait admis que la résiliation du marché initial pour faute était irrégulière, n'a pas été invoqué devant la cour administrative d'appel de Douai. Il n'est pas né de l'arrêt attaqué et n'est pas d'ordre public. Par suite, Me B... ne peut utilement soulever ce moyen, nouveau en cassation, pour contester le bien-fondé de l'arrêt qu'il attaque.<br/>
<br/>
              5. Il résulte de ce qui précède que le pourvoi de Me B... doit être rejeté.<br/>
<br/>
              6. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de la commune de Bouvines qui n'est pas, dans la présente instance, la partie perdante. En revanche, il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de Me B... la somme de 3 000 euros à verser à la commune de Bouvines, au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de Me B... est rejeté.<br/>
Article 2 : Me B... versera à la commune de Bouvines une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée à Me A... B..., agissant en qualité de liquidateur de la société Entreprise Georges Cazeaux et à la commune de Bouvines.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
