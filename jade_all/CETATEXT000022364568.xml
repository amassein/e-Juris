<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000022364568</ID>
<ANCIEN_ID>JG_L_2010_06_000000315076</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/22/36/45/CETATEXT000022364568.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère et 6ème sous-sections réunies, 18/06/2010, 315076</TITRE>
<DATE_DEC>2010-06-18</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>315076</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère et 6ème sous-sections réunies</FORMATION>
<TYPE_REC>Contentieux des pensions</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Arrighi de Casanova</PRESIDENT>
<AVOCATS>BOUTHORS</AVOCATS>
<RAPPORTEUR>M. Jean  Lessi</RAPPORTEUR>
<COMMISSAIRE_GVT>Mlle Courrèges Anne</COMMISSAIRE_GVT>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi, enregistré le 14 avril 2008 au secrétariat du contentieux du Conseil d'Etat, présenté par le MINISTRE DU BUDGET, DES COMPTES PUBLICS ET DE LA FONCTION PUBLIQUE ; le ministre demande au Conseil d'Etat d'annuler le jugement du 6 mars 2008 du tribunal administratif de Limoges en tant que, d'une part, il a annulé son arrêté du 13 juin 2005 concédant à Mme A une pension de réversion, en ce qu'il n'a pas pris en compte la période s'étendant du 9 novembre 1995 au 16 avril 2005 pour la détermination de la part lui revenant de la pension de réversion de son époux décédé et, d'autre part, il lui a enjoint, dans le délai d'un mois à compter de la notification du jugement, de réexaminer la demande de pension de réversion de l'intéressée en déterminant sa part sur la base d'une durée allant du 9 novembre 1995 au 12 mai 2005 ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales et son premier protocole additionnel ;<br/>
<br/>
              Vu le code civil ;<br/>
<br/>
              Vu le code des pensions civiles et militaires de retraite ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean Lessi, Auditeur,  <br/>
<br/>
              - les observations de Me Bouthors, avocat de Mme A, <br/>
<br/>
              - les conclusions de Mlle Anne Courrèges, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à Me Bouthors, avocat de Mme A ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>Considérant qu'aux termes du premier alinéa de l'article L. 38 du code des pensions civiles et militaires de retraite :  Les conjoints d'un fonctionnaire civil ont droit à une pension de réversion égale à 50 % de la pension obtenue par le fonctionnaire ou qu'il aurait pu obtenir au jour de son décès.  ; que l'article L. 45 du même code dispose que :  Lorsque, au décès du fonctionnaire, il existe plusieurs conjoints, divorcés ou survivants, ayant droit à la pension définie au premier alinéa de l'article L. 38, la pension est répartie entre ces conjoints au prorata de la durée respective de chaque mariage.  ; qu'en vertu de l'article L. 47, ces dispositions sont applicables aux ayants droit des anciens militaires ;<br/>
<br/>
              Considérant qu'il ressort des pièces du dossier soumis au juge du fond que l'époux de Mme A, qui avait été admis au bénéfice d'une pension de retraite à compter du 1er mars 2000, est décédé le 12 mai 2005 ; que s'il vivait avec l'intéressée depuis le mois de mai 1980, il n'a contracté mariage avec elle que le 16 avril 2005 ; que son premier mariage n'avait été dissout que par un jugement de divorce en date du 9 novembre 1995 ; que, pour répartir les droits à pension entre Mme A et la première épouse de son mari, le service des pensions n'a pris en compte, sur le fondement de l'article L. 45 du code des pensions civiles et militaires de retraite précité, que la période comprise entre le 16 avril 2005, date de son mariage, et le 12 mai 2005, date du décès de son mari ; que, par l'application combinée des dispositions de cet article et de la règle d'arrondi prévue à l'article R. 47 bis du code, le montant de la pension de réversion qui a été concédée à Mme A par arrêté du 13 juin 2005 du ministre de l'économie, des finances et de l'industrie, est nul ; que Mme A a déféré cet arrêté au tribunal administratif de Limoges, en tant qu'il fixait au 16 avril 2005 la date à retenir comme point de départ pour la détermination de la durée à prendre en compte pour la répartition de la pension, et demandé que soit prise en compte la totalité de la période de vie commune avec son époux, soit la période courant du mois de mai 1980 à la date du décès de ce dernier ; <br/>
<br/>
              Considérant que, par le jugement attaqué, le tribunal administratif de Limoges a estimé que le critère relatif à l'état matrimonial des personnes prévu à l'article L. 45 n'instituait pas une discrimination prohibée par les stipulations de l'article 14 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales et de l'article 1er du premier protocole additionnel à cette convention pour les périodes durant lesquelles le conjoint divorcé et le conjoint survivant ont entretenu des liens d'une intensité différente avec le pensionné décédé, et a rejeté en conséquence la demande de Mme A tendant à la prise en compte de la période s'étendant du mois de mai 1980 au 9 novembre 1995 ; qu'en revanche, il a jugé qu'il en allait différemment pour la période durant laquelle le conjoint survivant a eu, avec le bénéficiaire de la pension, postérieurement à un divorce de celui-ci et avant leur mariage, une vie stable et continue dont sont issus des enfants reconnus, soit, en l'espèce, celle s'étendant du 9 novembre 1995 au 16 avril 2005 ; <br/>
<br/>
              Considérant qu'en vertu des dispositions du code civil, les conjoints sont assujettis à une solidarité financière et à un ensemble d'obligations légales, telles que la contribution aux charges de la vie commune, qui ne pèsent pas sur les personnes vivant en concubinage ; que cette différence de situation justifie, au regard de l'objet de la loi, la différence de traitement qu'elle institue entre les couples vivant en concubinage et ceux unis par les liens du mariage pour l'attribution du droit à une pension de réversion ainsi que pour la répartition de ce droit entre les conjoints, divorcés ou survivants ; que, dès lors, en jugeant que le critère de la durée du mariage, relatif à l'état matrimonial de personnes, constituait une discrimination prohibée par les stipulations de l'article 14 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales et de l'article 1er du premier protocole additionnel à cette convention lorsqu'il s'appliquait à des périodes de vie commune stable et continue avec un concubin, suivant un divorce et précédant le remariage et dont sont issus des enfants reconnus, le tribunal administratif a commis une erreur de droit ; que, par suite, le MINISTRE DU BUDGET, DES COMPTES PUBLICS ET DE LA FONCTION PUBLIQUE est fondé à demander l'annulation des articles 1er, 2 et 4 du jugement attaqué ;<br/>
<br/>
              Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler, dans cette mesure, l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative ; <br/>
<br/>
              Considérant qu'il résulte de ce qui a été dit ci-dessus que Mme A n'est pas fondée à soutenir que les dispositions de l'article L. 45 du code des pensions civiles et militaires de retraite introduiraient une discrimination prohibée par les stipulations de l'article 14 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales et de l'article 1er du premier protocole additionnel à cette convention ; que contrairement à ce qu'elle soutient, ces mêmes dispositions ne portent pas atteinte au principe du droit au respect de la vie privée et familiale résultant de l'article 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, au motif qu'elles ne prennent en compte que les périodes de mariage pour la répartition du droit à pension de réversion entre les conjoints divorcés et survivants ; que la période de vie commune des futurs époux avant la célébration de leur mariage ne peut être prise en compte pour le calcul de la pension de réversion, alors même que des enfants reconnus par le père sont nés durant cette période ; que, par suite, Mme A n'est pas fondée à demander l'annulation de l'arrêté du 13 juin 2005 du ministre de l'économie, des finances et de l'industrie en tant qu'il n'a pas pris en compte la période s'étendant du 9 novembre 1995, date du divorce de son mari, au 16 avril 2005, date de leur mariage ; que ses conclusions à fin d'injonction ainsi que celles présentées devant le tribunal administratif et le Conseil d'Etat au titre de l'article L. 761-1 du code de justice administrative doivent être rejetées ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Les articles 1er, 2 et 4 du jugement du tribunal administratif de Limoges du 6 mars 2008 sont  annulés.<br/>
Article 2 : Les conclusions de la demande de Mme A devant le tribunal administratif de Limoges tendant à l'annulation de l'arrêté du 13 juin 2005 du ministre de l'économie, de finances et de l'industrie en tant qu'il n'a pas pris en compte, pour la détermination de sa part de la pension de réversion due au titre de son époux décédé, la période s'étendant du 9 novembre 1995 au 16 avril 2005, ainsi que ses conclusions à fin d'injonction et celles présentées au titre de l'article L. 761-1 du code de justice administrative, sont rejetées.<br/>
Article  3 : La présente décision sera notifiée au MINISTRE DU BUDGET, DES COMPTES PUBLICS ET DE LA REFORME DE L'ETAT et à Mme Pierrette A.<br/>
<br/>
</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">26-055-02-01 DROITS CIVILS ET INDIVIDUELS. CONVENTION EUROPÉENNE DES DROITS DE L'HOMME. DROITS GARANTIS PAR LES PROTOCOLES. DROIT AU RESPECT DE SES BIENS (ART. 1ER DU PREMIER PROTOCOLE ADDITIONNEL). - COMBINAISON AVEC LES STIPULATIONS DE L'ARTICLE 14 DE LA CONVENTION - VIOLATION - ABSENCE - DISPOSITIONS DU CPCMR PRÉVOYANT, EN CAS DE PARTAGE ENTRE CONJOINTS D'UNE PENSION DE RÉVERSION, UN PRORATA EN FONCTION DE LA DURÉE RESPECTIVE DE CHAQUE MARIAGE, EXCLUANT LA PRISE EN COMPTE D'UNE PÉRIODE DE CONCUBINAGE [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">48-02-01-09 PENSIONS. PENSIONS CIVILES ET MILITAIRES DE RETRAITE. QUESTIONS COMMUNES. AYANTS-CAUSE. - PENSION DE RÉVERSION - DROIT OUVERT AU CONJOINT (ART. L. 38 CPCMR) ET RÉPARTITION ENTRE LES CONJOINTS AYANTS DROIT AU PRORATA DE LA DURÉE RESPECTIVE DE CHAQUE MARIAGE (ART. L. 45 CPCMR) - FONCTIONNAIRE AYANT, APRÈS UN DIVORCE, VÉCU EN CONCUBINAGE AVANT DE SE MARIER - ABSENCE DE PRISE EN COMPTE DE LA PÉRIODE DE CONCUBINAGE - DISCRIMINATION PROHIBÉE PAR LES STIPULATIONS DE L'ARTICLE 14 DE LA CONVENTION EDH ET DE L'ARTICLE 1ER DE SON PREMIER PROTOCOLE ADDITIONNEL - ABSENCE [RJ1].
</SCT>
<ANA ID="9A"> 26-055-02-01 En vertu des articles L. 38, L. 45 et L. 47 du code des pensions civiles et militaires de retraite (CPCMR), la pension de réversion à laquelle ont droit les conjoints d'un fonctionnaire civil ou militaire est répartie entre ces derniers au prorata de la durée respective de chaque mariage. Ce critère de la durée du mariage, qui a pour effet d'exclure la prise en compte de la période au cours de laquelle un fonctionnaire, après avoir divorcé, a vécu en concubinage avec une personne avec laquelle il s'est par la suite marié, ne constitue pas une discrimination prohibée par les stipulations de l'article 14 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales et de l'article 1er de son premier protocole additionnel.</ANA>
<ANA ID="9B"> 48-02-01-09 En vertu des articles L. 38, L. 45 et L. 47 du code des pensions civiles et militaires de retraite (CPCMR), la pension de réversion à laquelle ont droit les conjoints d'un fonctionnaire civil ou militaire est répartie entre ces derniers au prorata de la durée respective de chaque mariage. Ce critère de la durée du mariage, qui a pour effet d'exclure la prise en compte de la période au cours de laquelle un fonctionnaire, après avoir divorcé, a vécu en concubinage avec une personne avec laquelle il s'est par la suite marié, ne constitue pas une discrimination prohibée par les stipulations de l'article 14 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales et de l'article 1er de son premier protocole additionnel.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., s'agissant de l'exclusion des concubins du droit à percevoir une pension de réversion, 6 décembre 2006, Ligori, n° 262096, p. 496. Comp., s'agissant de la notion d'enfants issus du mariage au sens de l'article L. 39 CPCMR, incluant ceux nés avant le mariage et ultérieurement reconnus par le fonctionnaire, 30 janvier 2008, Many, n° 274898, p. 27.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
