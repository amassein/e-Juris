<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036976481</ID>
<ANCIEN_ID>JG_L_2018_06_000000417340</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/97/64/CETATEXT000036976481.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère chambre jugeant seule, 01/06/2018, 417340, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-06-01</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>417340</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère chambre jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GARREAU, BAUER-VIOLAS, FESCHOTTE-DESBOIS ; SCP WAQUET, FARGE, HAZAN</AVOCATS>
<RAPPORTEUR>M. Yannick Faure</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Charles Touboul</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:417340.20180601</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société Descas Père et Fils a demandé au juge des référés du tribunal administratif de Bordeaux, sur le fondement des dispositions de l'article L. 521-1 du code de justice administrative, de suspendre l'exécution de la délibération du 7 novembre 2017 par laquelle le conseil de la communauté de communes Jalle-Eau Bourde a exercé le droit de préemption urbain sur la parcelle cadastrée BM n° 36 dans la commune de Saint-Jean-d'Illac (Gironde). Par une ordonnance n° 1705176 du 27 décembre 2017, le juge des référés de ce tribunal a suspendu l'exécution de cette délibération.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 15 et 30 janvier 2018 au secrétariat du contentieux du Conseil d'Etat, la communauté de communes Jalle-Eau Bourde demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) statuant en référé, de rejeter la demande de la société Descas Père et Fils ;<br/>
<br/>
              3°) de mettre à la charge de la société Descas Père et Fils la somme 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'urbanisme ;<br/>
              - le code général des collectivités territoriales ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Yannick Faure, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Charles Touboul, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Garreau, Bauer-Violas, Feschotte-Desbois, avocat de la communauté de communes Jalle-Eau Bourde, et à la SCP Waquet, Farge, Hazan, avocat de la société Descas Père et Fils ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision ".<br/>
<br/>
              2. Il ressort des pièces du dossier soumis au juge des référés du tribunal administratif de Bordeaux que, par une délibération du 7 novembre 2017, le conseil communautaire de la communauté de communes Jalle-Eau Bourde a exercé le droit de préemption urbain, que le conseil municipal de la commune de Saint-Jean-d'Illac lui avait préalablement délégué par délibération du 25 octobre 2017, sur un ensemble immobilier à usage industriel situé avenue de Pierroton dans cette commune. Par une ordonnance du 27 décembre 2017, contre laquelle la communauté de communes Jalle-Eau Bourde se pourvoit en cassation, le juge des référés a fait droit à la demande présentée par la société Descas Père et Fils, acquéreur évincé, et ordonné la suspension de l'exécution de cette délibération du 7 novembre 2017.<br/>
<br/>
              3. Aux termes du premier alinéa de l'article L. 213-3 du code de l'urbanisme : " Le titulaire du droit de préemption peut déléguer son droit à l'État, à une collectivité locale, à un établissement public y ayant vocation ou au concessionnaire d'une opération d'aménagement. Cette délégation peut porter sur une ou plusieurs parties des zones concernées ou être accordée à l'occasion de l'aliénation d'un bien. Les biens ainsi acquis entrent dans le patrimoine du délégataire ". Aux termes de l'article L. 2122-22 du code général des collectivités territoriales : " Le maire peut, (...) par délégation du conseil municipal, être chargé, en tout ou partie, et pour la durée de son mandat : (...) / 15° D'exercer, au nom de la commune, les droits de préemption définis par le code de l'urbanisme, que la commune en soit titulaire ou délégataire, de déléguer l'exercice de ces droits à l'occasion de l'aliénation d'un bien selon les dispositions prévues à l'article L. 211-2 ou au premier alinéa de l'article L. 213-3 de ce même code dans les conditions que fixe le conseil municipal ". Il résulte de ces dispositions que le conseil municipal a la possibilité de déléguer au maire, le cas échéant aux conditions qu'il détermine, d'une part, l'exercice des droits de préemption dont la commune est titulaire ou délégataire, afin d'acquérir des biens au profit de cette collectivité, et, d'autre part, le pouvoir de déléguer l'exercice de ces droits à l'occasion de l'aliénation d'un bien particulier, pour permettre au délégataire de l'acquérir à son profit.  <br/>
<br/>
              4. Il ressort des pièces du dossier soumis au juge des référés que, par délibération du 29 mars 2014, le conseil municipal de Saint-Jean-d'Illac a délégué au maire de la commune la compétence pour " exercer au nom de la commune les droits de préemption définis par le code de l'urbanisme, que la commune en soit titulaire ou délégataire, (...) déléguer l'exercice de ces droits à l'occasion de l'aliénation d'un bien selon les dispositions prévues au premier alinéa de l'article L. 213-3 de ce même code dans les limites d'un plafond de 16 000 euros par acquisition ". Il en résulte nécessairement que le conseil municipal demeurait compétent pour déléguer le droit de préemption de la commune aux personnes mentionnées au premier alinéa de l'article L. 213-3 du code de l'urbanisme à l'occasion de l'aliénation d'un bien pour un montant excédant le plafond de 16 000 euros.<br/>
<br/>
              5. Par suite, en jugeant qu'était de nature à créer un doute sérieux, quant à la légalité de la délibération du 7 novembre 2017, le moyen tiré de ce que le conseil municipal ne pouvait déléguer l'exercice du droit de préemption à la communauté de communes sans rapporter préalablement sa délibération du 29 mars 2014, alors qu'il ressort des pièces du dossier soumis au juge des référés que la préemption s'effectuait au prix, mentionné par la déclaration d'intention d'aliéner, de 1 600 000 euros, le juge des référés du tribunal administratif de Bordeaux a commis une erreur de droit.<br/>
<br/>
              6. Il résulte de ce qui précède que la communauté de communes Jalle-Eau  Bourde est fondée à demander l'annulation de l'ordonnance qu'elle attaque. Le moyen retenu suffisant à entraîner cette annulation, il n'est pas nécessaire de se prononcer sur les autres moyens de son pourvoi.<br/>
<br/>
              7. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de la communauté de communes Jalle-Eau Bourde, qui n'est pas, dans la présente instance, la partie perdante. Il n'y a pas lieu, dans les circonstances de l'espèce, de mettre à la charge de la société Descas Père et Fils une somme au titre de ces mêmes dispositions.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'ordonnance du juge des référés du tribunal administratif de Bordeaux du 27 décembre 2017 est annulée.<br/>
Article 2 : L'affaire est renvoyée au juge des référés du tribunal administratif de Bordeaux.<br/>
Article 3 : Les conclusions présentées par les parties au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée à la communauté de communes Jalle-Eau Bourde et à la société Descas Père et Fils.<br/>
Copie en sera adressée à la société Mondi Lembacel.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
