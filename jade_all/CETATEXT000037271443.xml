<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037271443</ID>
<ANCIEN_ID>JG_L_2018_07_000000422071</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/27/14/CETATEXT000037271443.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 19/07/2018, 422071, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-07-19</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>422071</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2018:422071.20180719</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A...B...a demandé au juge des référés du tribunal administratif de Mayotte, sur le fondement de l'article L. 521-2 du code de justice administrative, d'enjoindre au préfet de Mayotte de lui remettre dans un délai de 24 heures un récépissé portant enregistrement de sa demande de carte de résident sous astreinte de 100 euros par jour de retard. Par une ordonnance n° 1800886 du 4 juillet 2018, le juge des référés a rejeté sa demande. <br/>
<br/>
              Par une requête enregistrée le 9 juillet 2018 au secrétariat du contentieux du Conseil d'Etat, M. B...demande au juge des référés du Conseil d'Etat : <br/>
<br/>
              1°) de l'admettre au bénéfice de l'aide juridictionnelle provisoire ; <br/>
<br/>
              2°) d'annuler l'ordonnance du 4 juillet 2018 du juge des référés du tribunal administratif de Mayotte ; <br/>
<br/>
              3°) d'enjoindre au préfet de Mayotte de lui remettre dans un délai de 24 heures une autorisation provisoire de séjour portant la mention " réfugié " sous astreinte de 100 euros par jour de retard ;<br/>
<br/>
              4°) d'enjoindre au préfet de Mayotte d'enregistrer sa demande de délivrance de carte de résident sur le fondement de l'article L. 314-11 du code de l'entrée et du séjour des étrangers et du droit d'asile sous astreinte de 100 euros par jour de retard ; <br/>
<br/>
              5°) à titre subsidiaire, d'enjoindre au préfet de Mayotte d'organiser son transfert vers la métropole pour qu'il puisse solliciter son admission au séjour ; <br/>
<br/>
              6°) de mettre à la charge de l'Etat la somme de 3 000 euros en application des articles 37 de la loi du 10 juillet 1991 et L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
              Il soutient qu'en jugeant que le refus de délivrance d'une autorisation provisoire de séjour ne portait pas atteinte aux droits attachés au statut de réfugié, le juge des référés a commis une erreur de droit ; que la condition d'urgence exigée par les dispositions de l'article L. 521-2 du code de justice administrative est satisfaite dès lors que le refus de lui délivrer une autorisation provisoire de séjour le prive d'un accès au marché au travail alors qu'il est sans ressources et sans logement ; que le refus de lui délivrer une autorisation provisoire de séjour et d'enregistrer sa demande de carte de résident porte une atteinte grave et manifestement illégale au droit d'asile, à sa liberté de travailler, à sa dignité, à son droit de mener une vie familiale normale et à sa liberté d'aller et venir. <br/>
<br/>
              Par un mémoire en intervention enregistré le 12 juillet 2018, la Cimade, service oecuménique d'entraide, conclut à ce qu'il soit fait droit à l'appel de M.B....<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. M.B..., ressortissant du Burundi, s'est vu reconnaître la qualité de réfugié par décision du 20 février 2018 du directeur de l'Office français de protection des réfugiés et apatrides. Il a saisi le juge des référés du tribunal administratif de Mayotte, sur le fondement de l'article L. 521-2 du code de justice administrative, d'une demande tendant à ce qu'il soit enjoint au préfet de Mayotte d'enregistrer sa demande de carte de résident et de lui délivrer un récépissé de demande de titre de séjour. Par ordonnance du 4 juillet 2018, le juge des référés a rejeté sa demande. <br/>
<br/>
              2. La Cimade a intérêt à ce qu'il soit fait droit à la demande de M.B.... Son intervention doit, par suite, être admise. <br/>
<br/>
              3. Aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. Le juge des référés se prononce dans un délai de quarante-huit heures ". En vertu de l'article L. 522-3 du même code, le juge des référés peut rejeter une requête par une ordonnance motivée, sans instruction contradictoire ni audience publique, lorsque la condition d'urgence n'est pas remplie ou lorsqu'il apparaît manifeste, au vu de la demande, que celle-ci ne relève pas de la compétence de la juridiction administrative, qu'elle est irrecevable ou qu'elle est mal fondée. A cet égard, il appartient au juge d'appel de prendre en considération les éléments recueillis par le juge du premier degré dans le cadre de la procédure écrite et orale qu'il a diligentée.<br/>
<br/>
              4. Il résulte de l'instruction qu'en raison des dysfonctionnements du service des étrangers de la préfecture de Mayotte, M. B...n'a pas été en mesure, malgré la reconnaissance de sa qualité de réfugié, de déposer une demande de carte de résident et d'obtenir le récépissé prévu à l'article L. 311-5-1 du code de l'entrée et du séjour des étrangers et du droit d'asile. Il fait valoir que cette impossibilité porte atteinte à son exercice du droit d'asile, à sa liberté de travailler, à sa dignité, à son droit de mener une vie familiale normale et à sa liberté d'aller et venir. Il ne fait toutefois pas état de circonstances particulières démontrant que la situation dans laquelle il est placé constitue une situation d'urgence imminente justifiant le recours aux dispositions de l'article L. 521-2 du code de justice administrative, alors qu'il indique par ailleurs être aidé par des compatriotes et des associations dans l'attente de la réalisation de ses démarches. Par suite, et alors qu'il tient des dispositions de l'article L. 314-11 du code de l'entrée et du séjour des étrangers et du droit d'asile un droit à la délivrance d'une carte de résident, son appel doit être rejeté, par application des dispositions de l'article L. 522-3 du code de justice administrative.<br/>
<br/>
              5. L'aide juridictionnelle devant le Conseil d'Etat ne peut être demandée et, le cas échéant, obtenue, que pour recourir à l'assistance d'un avocat au Conseil d'Etat et à la Cour de cassation. Dès lors, les conclusions de la requête tendant à l'admission provisoire à l'aide juridictionnelle et, en tout état de cause, les conclusions de Me Ghaem, avocat au barreau de Mayotte, tendant à au versement par l'Etat à son profit d'une somme au titre de les articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991, ne peuvent qu'être rejetées. <br/>
<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : L'intervention de la Cimade est admise.<br/>
Article 2 : La requête d'appel de M. B...est rejetée.<br/>
Article 3 : La présente ordonnance sera notifiée M. A...B..., à la Cimade et au ministre d'Etat, ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
