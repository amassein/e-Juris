<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036506397</ID>
<ANCIEN_ID>JG_L_2018_01_000000410449</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/50/63/CETATEXT000036506397.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème - 7ème chambres réunies, 17/01/2018, 410449</TITRE>
<DATE_DEC>2018-01-17</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>410449</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème - 7ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FOUSSARD, FROGER</AVOCATS>
<RAPPORTEUR>Mme Louise Bréhier</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Guillaume Odinet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:410449.20180117</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante : <br/>
<br/>
              M. B...A...a demandé au tribunal administratif de Melun, d'une part, d'annuler la décision du 2 mars 2017 par laquelle le directeur général de l'Office français de protection des réfugiés et apatrides a refusé d'enregistrer, comme tardive, sa demande de réouverture d'examen de la demande d'asile qu'il avait présentée, et, d'autre part, d'enjoindre à l'Office d'accueillir la demande de réouverture d'examen de sa demande d'asile et d'accuser réception de sa demande d'asile sans délai en l'informant du caractère complet de son dossier. <br/>
<br/>
              Par une ordonnance n° 1702726 du 11 mai 2017, la présidente du tribunal administratif de Melun a, en application du second alinéa de l'article R. 351-3 du code de justice administrative, transmis ce dossier au président de la section du contentieux du Conseil d'Etat. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Louise Bréhier, auditrice,  <br/>
<br/>
              - les conclusions de M. Guillaume Odinet, rapporteur public,<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Foussard, Froger, avocat de l'Office français de protection des réfugiés et apatrides ; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1.	Considérant, d'une part, que selon l'article L. 731-2 du code de l'entrée et du séjour des étrangers et du droit d'asile : " La Cour nationale du droit d'asile statue sur les recours formés contre les décisions de l'Office français de protection des réfugiés et apatrides prises en application des articles L. 711-1 à L. 711-4, L. 711-6, L. 712-1 à L. 712-3, L. 713-1 à L. 713-4, L. 723-1 à L. 723-8, L. 723-11, L. 723-15 et L. 723-16 (...) " ; <br/>
<br/>
              2.	Considérant, d'autre part, qu'aux termes de l'article L. 723-13 du même code : " L'office peut prendre une décision de clôture d'examen d'une demande dans les cas suivants : / 1° Le demandeur, sans motif légitime, n'a pas introduit sa demande à l'office dans les délais prévus par décret en Conseil d'Etat et courant à compter de la remise de son attestation de demande d'asile ou ne s'est pas présenté à l'entretien à l'office ; / 2° Le demandeur refuse, de manière délibérée et caractérisée, de fournir des informations essentielles à l'examen de sa demande en application de l'article L. 723-4 ; / 3° Le demandeur n'a pas informé l'office, dans un délai raisonnable, de son lieu de résidence ou de son adresse et ne peut être contacté aux fins d'examen de sa demande d'asile ; / L'office notifie par écrit sa décision motivée en fait et en droit au demandeur d'asile. Cette notification précise les voies et délais de recours. " ; qu'aux termes de l'article L. 723-14 du même code : " Si, dans un délai inférieur à neuf mois à compter de la décision de clôture [d'examen], le demandeur sollicite la réouverture de son dossier ou présente une nouvelle demande, l'office rouvre le dossier et reprend l'examen de la demande au stade auquel il avait été interrompu. Le dépôt par le demandeur d'une demande de réouverture de son dossier est un préalable obligatoire à l'exercice d'un recours devant les juridictions administratives de droit commun, à peine d'irrecevabilité de ce recours (...) " ;<br/>
<br/>
              3.	Considérant que, s'agissant des décisions susceptibles d'être prises par l'Office français de protection des réfugiés et apatrides dans le cadre des dispositions des sections 1 à 4 du chapitre III du titre II du livre VII du code de l'entrée et du séjour des étrangers et du droit d'asile, il résulte des dispositions de l'article L. 731-2 de ce code, comme des indications données par les travaux parlementaires préalables à l'adoption de la loi du 29 juillet 2015, que le législateur a fait le choix de ne donner compétence à la Cour nationale du droit d'asile qu'à l'égard des décisions prises par l'Office en application des articles L. 723-1 à L. 723-8, L. 723-11, L. 723-15 et L. 723-16 ; que la Cour n'a, dès lors, pas reçu compétence pour se prononcer sur les recours formés contre les décisions prises par l'Office sur le fondement des articles L. 723-13 et L. 723-14, lesquels ne peuvent qu'être portés devant les tribunaux administratifs, juges de droit commun en premier ressort du contentieux administratif ; <br/>
<br/>
              4.	Considérant que la demande de M. A...tend à l'annulation de la décision par laquelle le directeur général de l'Office français de protection des réfugiés et apatrides a, sur le fondement de l'article L. 723-14 du code de l'entrée et du séjour des étrangers et du droit d'asile, refusé de rouvrir l'examen de sa demande d'asile, cette demande de réouverture ayant été présentée au-delà du délai imparti par l'article R. 723-14 ; qu'il résulte de ce qui a été dit précédemment que ce recours relève non de la compétence de la Cour nationale du droit d'asile mais de celle des juridictions administratives de droit commun ; qu'il y a lieu d'en attribuer le jugement au tribunal administratif de Melun, compétent pour en connaître en premier ressort en vertu de l'article R. 312-1 du code de justice administrative ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : Le jugement de la demande de M. A...est attribué au tribunal administratif de Melun. <br/>
<br/>
Article 2 : La présente décision sera notifiée à M. B...A..., à l'Office français de protection des réfugiés et apatrides, à la présidente du tribunal administratif de Melun et à la présidente de la Cour nationale du droit d'asile. <br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">095-07-01-02 - RECOURS CONTRE LES DÉCISIONS PRISES PAR LE DIRECTEUR GÉNÉRAL DE L'OFPRA SUR LE FONDEMENT DES ARTICLES L. 723-13 ET L. 723-14 DU CESEDA - EXCLUSION [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">17-05-01-01 COMPÉTENCE. COMPÉTENCE À L'INTÉRIEUR DE LA JURIDICTION ADMINISTRATIVE. COMPÉTENCE EN PREMIER RESSORT DES TRIBUNAUX ADMINISTRATIFS. COMPÉTENCE MATÉRIELLE. - RECOURS CONTRE LES DÉCISIONS PRISES PAR LE DIRECTEUR GÉNÉRAL DE L'OFPRA SUR LE FONDEMENT DES ARTICLES L. 723-13 ET L. 723-14 DU CESEDA - INCLUSION [RJ1].
</SCT>
<ANA ID="9A"> 095-07-01-02 La Cour nationale du droit d'asile (CNDA) n'est pas compétente pour se prononcer sur les recours contre les décisions de clôture ou de refus de réouverture de l'examen d'une demande d'asile prises par le directeur général de l'Office français de protection des réfugiés et apatrides (OFPRA) sur le fondement des articles L. 723-13 et L. 723-14 du code de l'entrée et du séjour des étrangers et du droit d'asile (CESEDA). Le jugement de ces recours relève donc des juridictions administratives de droit commun.</ANA>
<ANA ID="9B"> 17-05-01-01 Les tribunaux administratifs sont compétents pour connaître des recours dirigés contre les décisions de clôture ou de refus de réouverture de l'examen d'une demande d'asile prises par le directeur général de l'Office français de protection des réfugiés et apatrides (OFPRA) sur le fondement des articles L. 723-13 et L. 723-14 du code de l'entrée et du séjour des étrangers et du droit d'asile (CESEDA).</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Comp. CE, 23 décembre 2016,,, n° 403971, T. p. 647 ; CE, 23 décembre 2016,,, n° 403975, T. p. 647. Rappr., décision du même jour,,, n° 412292, à mentionner aux Tables.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
