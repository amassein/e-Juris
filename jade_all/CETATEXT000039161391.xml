<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039161391</ID>
<ANCIEN_ID>JG_L_2019_09_000000421665</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/16/13/CETATEXT000039161391.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème - 6ème chambres réunies, 30/09/2019, 421665, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-09-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>421665</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème - 6ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROCHETEAU, UZAN-SARANO</AVOCATS>
<RAPPORTEUR>Mme Louise Cadin</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Cécile Barrois de Sarigny</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:XX:2019:421665.20190930</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés les 21 juin et 18 décembre 2018 au secrétariat du contentieux du Conseil d'Etat, le Syndicat des radios indépendantes (SIRTI) demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision implicite par laquelle le Conseil supérieur de l'audiovisuel (CSA) a rejeté sa demande tendant à ce qu'il définisse un " outil " permettant aux services radiophoniques de connaître l'acception qu'il retient de la notion de " musique de variétés " au sens du 2° bis de l'article 28 de la loi du 30 septembre 1986 ;<br/>
<br/>
              2°) d'enjoindre au Conseil supérieur de l'audiovisuel de procéder à une telle définition dans un délai de deux mois à compter de la décision à intervenir, sous astreinte de 500 euros par jour de retard ;<br/>
<br/>
              3°) de mettre à la charge du Conseil supérieur de l'audiovisuel la somme de 2 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la loi n° 86-1067 du 30 septembre 1986 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme B... A..., auditrice,  <br/>
<br/>
              - les conclusions de Mme Cécile Barrois de Sarigny, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Rocheteau, Uzan-Sarano, avocat du Syndicat des radios indépendantes ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. L'article 28 de la loi du 30 septembre 1986 relative à la liberté de communication subordonne la délivrance de l'autorisation d'usage de la ressource radioélectrique à tout service diffusé par voie hertzienne terrestre, autre que ceux exploités par les sociétés nationales de programme, à la conclusion d'une convention passée entre le Conseil supérieur de l'audiovisuel (CSA) et la personne qui demande l'autorisation. Aux termes du 2° bis de cet article, cette convention porte notamment sur : " La proportion substantielle d'oeuvres musicales d'expression française ou interprétées dans une langue régionale en usage en France, qui doit atteindre un minimum de 40 % de chansons d'expression française, dont la moitié au moins provenant de nouveaux talents ou de nouvelles productions, diffusées aux heures d'écoute significative par chacun des services de radio autorisés par le Conseil supérieur de l'audiovisuel, pour la part de ses programmes composée de musique de variétés (...) ".<br/>
<br/>
              2. Il ressort des pièces du dossier que le Syndicat des radios indépendantes (SIRTI) a adressé au Conseil supérieur de l'audiovisuel une demande tendant à ce que ce dernier mette à la disposition des éditeurs de services radiophoniques, aux fins d'application des dispositions citées ci-dessus, un " outil leur permettant de connaître précisément la définition qu'il retient de la notion de musique de variétés et de savoir à quel genre musical se rattache l'ensemble des phonogrammes disponibles sur le marché ". Le SIRTI demande l'annulation pour excès de pouvoir de la décision implicite de rejet née du silence gardé par le CSA sur cette demande.<br/>
<br/>
              3. D'une part, aucune disposition de la loi du 30 septembre 1986 ni aucun autre texte ne confère au CSA compétence pour édicter des dispositions réglementaires pour l'application des dispositions législatives citées au point 1. Par suite, le CSA était tenu de rejeter la demande dont il était saisi, en tant que celle-ci pouvait tendre à ce qu'il fixe des critères réglementaires définissant la " musique de variétés ".<br/>
<br/>
              4. D'autre part, s'il est loisible à une autorité administrative de prendre une circulaire ou un acte à caractère général visant à faire connaître l'interprétation qu'elle retient de l'état du droit, elle n'est jamais tenue de le faire. Il s'ensuit que le refus opposé à la demande du SIRTI, en tant que celle-ci pouvait tendre à l'édiction d'un tel acte, ne constitue pas une décision susceptible d'être déférée au juge de l'excès de pouvoir. En l'absence de toute indication de portée générale quant à la notion de " musique de variétés " au sens des dispositions citées au point 1, il appartient seulement au Conseil supérieur de l'audiovisuel, à chaque fois qu'il est conduit à statuer par voie de décisions individuelles au regard de ces dispositions, de faire application de la notion en cause sous le contrôle du juge.<br/>
<br/>
              5. Il résulte de tout ce qui précède que la requête du SIRTI ne peut qu'être rejetée.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : La requête du Syndicat des radios indépendantes est rejetée.<br/>
Article 2 : La présente décision sera notifiée au Syndicat des radios indépendantes et au Conseil supérieur de l'audiovisuel.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
