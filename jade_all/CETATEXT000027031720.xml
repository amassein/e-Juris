<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027031720</ID>
<ANCIEN_ID>JG_L_2013_02_000000335589</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/03/17/CETATEXT000027031720.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème et 9ème sous-sections réunies, 04/02/2013, 335589</TITRE>
<DATE_DEC>2013-02-04</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>335589</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème et 9ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP VINCENT, OHL</AVOCATS>
<RAPPORTEUR>Mme Isabelle Lemesle</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Delphine Hedary</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:335589.20130204</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi, enregistré le 15 janvier 2010 au secrétariat du contentieux du Conseil d'Etat, présenté pour la commune de Saint-Lanne, représentée par son maire ; la commune demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 08BX02720-08BX02722 du 10 novembre 2009 par lequel la cour administrative d'appel de Bordeaux a rejeté sa requête, ainsi que le recours du ministre de l'écologie, de l'énergie, du développement durable et de l'aménagement du territoire tendant à l'annulation du jugement n° 0602061 du 5 août 2008 du tribunal administratif de Pau qui avait annulé, à la demande de M. et MmeA..., l'arrêté du 27 septembre 2006 du maire de la commune de Saint-Lanne délivrant à la société civile immobilière Peleyre un permis de construire ;<br/>
<br/>
              2°) de mettre à la charge de M. et Mme A...la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de la construction et de l'habitation ;<br/>
<br/>
              Vu le code de l'urbanisme ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Isabelle Lemesle, Maître des Requêtes,<br/>
<br/>
              - les observations de la SCP Vincent, Ohl, avocat de la commune de Saint-Lanne,<br/>
<br/>
              - les conclusions de Mme Delphine Hedary, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Vincent, Ohl, avocat de la commune de Saint-Lanne ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis au juges du fond que le maire de Saint-Lanne (Hautes-Pyrénées) a délivré au nom de l'Etat, le 27 septembre 2006, un permis de construire à la SCI Peleyre aux fins de rénovation et transformation d'une grange ; que, par jugement du 5 août 2008, le tribunal administratif de Pau a annulé cet arrêté ; que, par l'arrêt attaqué du 10 novembre 2009, la cour administrative d'appel de Bordeaux, saisie, d'une part, d'une requête de la commune de Saint-Lanne et, d'autre part, d'un recours du ministre de l'écologie, de l'énergie, du développement durable et de l'aménagement du territoire, a confirmé ce jugement ; <br/>
<br/>
              Sur la recevabilité des conclusions de la commune de Saint-Lanne et du ministre de l'écologie, du développement durable, des transports et du logement :<br/>
<br/>
              2. Considérant, d'une part, que si la commune sur le territoire de laquelle un permis de construire a été délivré au nom de l'Etat n'a pas, devant le tribunal administratif, la qualité de partie dans une instance relative à la contestation de ce permis, alors même qu'elle a été appelée en cause pour produire des observations et si, par suite, elle n'est pas recevable à interjeter appel de ce jugement, la voie du recours en cassation est ouverte, en vertu des règles générales de la procédure, aux parties à l'instance d'appel ; qu'à ce titre, l'appelant a qualité pour déférer au juge de cassation l'arrêt qui a rejeté son appel ;<br/>
<br/>
              3. Considérant qu'il résulte de ce qui précède que la commune de Saint-Lanne est recevable à se pourvoir en cassation contre l'arrêt attaqué en ce qu'il a rejeté son appel, alors même que celui-ci, dirigé contre un jugement qui avait annulé un permis de construire délivré par son maire au nom de l'Etat, n'était pas recevable ;<br/>
<br/>
              4. Considérant, d'autre part, que le ministre chargé de l'écologie dont, ainsi qu'il a été dit, le recours a également été rejeté par l'arrêt attaqué avait de ce fait qualité pour se pourvoir en cassation contre cet arrêt ; que les conclusions par lesquelles, dans un mémoire enregistré au secrétariat du contentieux du Conseil d'Etat le 26 septembre 2011, il demande l'annulation de cet arrêt doivent être regardées comme un pourvoi en cassation ; qu'aucune tardiveté ne pouvant lui être opposée, dès lors qu'il ne ressort pas des pièces du dossier que l'arrêt contesté lui ait été régulièrement notifié, son pourvoi est recevable ; <br/>
<br/>
              Sur l'arrêt attaqué :<br/>
<br/>
              5. Considérant qu'il résulte de la combinaison des articles R. 123-14, R. 123-19 et R. 123-22 du code de la construction et de l'habitation, dans leur rédaction alors en vigueur, que le permis de construire un bâtiment destiné à relever de la 5e catégorie des établissements recevant du public n'est pas soumis à la consultation préalable de la commission de sécurité prévue à l'article R 123-22 lorsque la construction envisagée ne comporte pas de locaux d'hébergement du public ;<br/>
<br/>
              6. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond, et qu'il n'est d'ailleurs pas contesté, que la construction autorisée par le permis litigieux est un établissement recevant du public qui relève de la 5e catégorie, telle que définie par l'article R. 123-19 du code de la construction et de l'habitation ; que, par suite, la cour administrative d'appel de Bordeaux a entaché l'arrêt attaqué d'une erreur de droit en jugeant que la commission de sécurité devait être consultée en application des dispositions figurant désormais à l'article L. 425-3 du code de l'urbanisme ; que, toutefois, dès lors qu'il ressortait des pièces du dossier soumis à son examen que l'autorité ayant délivré le permis avait, alors qu'elle n'y était pas tenue, décidé de consulter la commission de sécurité, l'annulation du permis pouvait résulter de ce que cette consultation facultative avait été opérée, comme la cour l'a jugé, selon une procédure irrégulière de nature à entraîner son illégalité ; <br/>
<br/>
              7. Considérant toutefois que, lorsque la délivrance d'une autorisation d'urbanisme intervient après une consultation subordonnée à la production d'éléments d'information ou de documents précis, leur caractère incomplet, lorsqu'il n'est pas d'une ampleur telle qu'il permettrait de les regarder comme n'ayant pas été produits, ne constitue pas nécessairement une irrégularité de nature à entacher d'illégalité l'autorisation délivrée ; qu'il appartient au juge de l'excès de pouvoir de rechercher si ce caractère incomplet a fait obstacle à ce que l'autorité compétente dispose des éléments nécessaires pour se prononcer en connaissance de cause ;<br/>
<br/>
              8. Considérant que, dès lors, en se bornant à relever le caractère incomplet de la notice de sécurité jointe à la demande de permis pour confirmer l'annulation du permis de construire par le tribunal administratif de Pau, sans rechercher si cette carence dans la description soumise à la commission de sécurité avait pu avoir, en l'espèce, une influence sur le sens de la décision prise, la cour administrative d'appel de Bordeaux a entaché son arrêt d'une erreur de droit ;<br/>
<br/>
              9. Considérant qu'il résulte de ce qui précède que, sans qu'il soit besoin d'examiner les autres moyens des pourvois, l'arrêt attaqué doit être annulé ; <br/>
<br/>
              Sur l'application des dispositions de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              10. Considérant qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées au titre de ces dispositions par la commune de Saint-Lanne ;<br/>
<br/>
<br/>
<br/>				D E C I D E  :<br/>
              				--------------<br/>
<br/>
Article 1er : L'arrêt du 10 novembre 2009 de la cour administrative d'appel de Bordeaux est annulé.<br/>
<br/>
Article 2 : L'affaire est renvoyée devant la cour administrative d'appel de Bordeaux.<br/>
<br/>
Article 3 : Le surplus des conclusions du pourvoi de la commune de Saint-Lanne est rejeté.<br/>
<br/>
Article 4 : La présente décision sera notifiée à la commune de Saint-Lanne et à la ministre de l'égalité des territoires et du logement.<br/>
Copie en sera adressée à M. B...A..., à Mme C...A...et à la société civile immobilière Peleyre.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">135-02-05-02 COLLECTIVITÉS TERRITORIALES. COMMUNE. RÈGLES DE PROCÉDURE CONTENTIEUSE SPÉCIALES. DIVERS. - COMMUNE IRRECEVABLE À FAIRE APPEL CONTRE UN JUGEMENT RELATIF À UN PERMIS DÉLIVRÉ SUR SON TERRITOIRE  AU NOM DE L'ETAT [RJ1] - QUALITÉ POUR SE POURVOIR EN CASSATION CONTRE L'ARRÊT AYANT REJETÉ SON APPEL - EXISTENCE [RJ2].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-08-01-01 PROCÉDURE. VOIES DE RECOURS. APPEL. RECEVABILITÉ. - COMMUNE IRRECEVABLE À FAIRE APPEL CONTRE UN JUGEMENT RELATIF À UN PERMIS DÉLIVRÉ SUR SON TERRITOIRE  AU NOM DE L'ETAT [RJ1] - QUALITÉ POUR SE POURVOIR EN CASSATION CONTRE L'ARRÊT AYANT REJETÉ SON APPEL - EXISTENCE [RJ2].
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">54-08-02-004-01 PROCÉDURE. VOIES DE RECOURS. CASSATION. RECEVABILITÉ. RECEVABILITÉ DES POURVOIS. - COMMUNE IRRECEVABLE À FAIRE APPEL CONTRE UN JUGEMENT RELATIF À UN PERMIS DÉLIVRÉ SUR SON TERRITOIRE  AU NOM DE L'ETAT [RJ1] - QUALITÉ POUR SE POURVOIR EN CASSATION CONTRE L'ARRÊT AYANT REJETÉ SON APPEL - EXISTENCE [RJ2].
</SCT>
<SCT ID="8D" TYPE="PRINCIPAL">68-06-01 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. RÈGLES DE PROCÉDURE CONTENTIEUSE SPÉCIALES. INTRODUCTION DE L'INSTANCE. - COMMUNE IRRECEVABLE À FAIRE APPEL CONTRE UN JUGEMENT RELATIF À UN PERMIS DÉLIVRÉ SUR SON TERRITOIRE  AU NOM DE L'ETAT [RJ1] - QUALITÉ POUR SE POURVOIR EN CASSATION CONTRE L'ARRÊT AYANT REJETÉ SON APPEL - EXISTENCE [RJ2].
</SCT>
<ANA ID="9A"> 135-02-05-02 Si la commune sur le territoire de laquelle un permis de construire a été délivré au nom de l'Etat n'a pas, devant le tribunal administratif, la qualité de partie dans une instance relative à la contestation de ce permis, alors même qu'elle a été appelée en cause pour produire des observations et si, par suite, elle n'est pas recevable à interjeter appel de ce jugement, la voie du recours en cassation est ouverte, en vertu des règles générales de la procédure, aux parties à l'instance d'appel. A ce titre, une commune a qualité pour déférer au juge de cassation l'arrêt qui a rejeté son appel.</ANA>
<ANA ID="9B"> 54-08-01-01 Si la commune sur le territoire de laquelle un permis de construire a été délivré au nom de l'Etat n'a pas, devant le tribunal administratif, la qualité de partie dans une instance relative à la contestation de ce permis, alors même qu'elle a été appelée en cause pour produire des observations et si, par suite, elle n'est pas recevable à interjeter appel de ce jugement, la voie du recours en cassation est ouverte, en vertu des règles générales de la procédure, aux parties à l'instance d'appel. A ce titre, une commune a qualité pour déférer au juge de cassation l'arrêt qui a rejeté son appel.</ANA>
<ANA ID="9C"> 54-08-02-004-01 Si la commune sur le territoire de laquelle un permis de construire a été délivré au nom de l'Etat n'a pas, devant le tribunal administratif, la qualité de partie dans une instance relative à la contestation de ce permis, alors même qu'elle a été appelée en cause pour produire des observations et si, par suite, elle n'est pas recevable à interjeter appel de ce jugement, la voie du recours en cassation est ouverte, en vertu des règles générales de la procédure, aux parties à l'instance d'appel. A ce titre, une commune a qualité pour déférer au juge de cassation l'arrêt qui a rejeté son appel.</ANA>
<ANA ID="9D"> 68-06-01 Si la commune sur le territoire de laquelle un permis de construire a été délivré au nom de l'Etat n'a pas, devant le tribunal administratif, la qualité de partie dans une instance relative à la contestation de ce permis, alors même qu'elle a été appelée en cause pour produire des observations et si, par suite, elle n'est pas recevable à interjeter appel de ce jugement, la voie du recours en cassation est ouverte, en vertu des règles générales de la procédure, aux parties à l'instance d'appel. A ce titre, une commune a qualité pour déférer au juge de cassation l'arrêt qui a rejeté son appel.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf., sur l'absence de qualité de la commune dans une configuration telle qu'en l'espèce, CE, 11 mars 1988, Commune d'Erstein c/ Epoux Lecerf et Kempf, n° 80612, T. pp. 979-1094.,  ,[RJ2] Rappr., s'agissant de l'irrecevabilité du pourvoi de la commune dans un cas où l'appel émanait du demandeur de première instance, CE, 10 février 2010, Ville de Porto Vecchio, n° 313870, inédite.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
