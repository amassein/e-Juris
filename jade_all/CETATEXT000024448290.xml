<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000024448290</ID>
<ANCIEN_ID>JG_L_2011_07_000000324728</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/24/44/82/CETATEXT000024448290.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème et 1ère sous-sections réunies, 26/07/2011, 324728</TITRE>
<DATE_DEC>2011-07-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>324728</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème et 1ère sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Jacques Arrighi de Casanova</PRESIDENT>
<AVOCATS>SCP PEIGNOT, GARREAU</AVOCATS>
<RAPPORTEUR>M. Raphaël Chambon</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Mattias Guyomar</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2011:324728.20110726</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi et les mémoires complémentaires, enregistrés les 3 février, 4 mai et  9 octobre 2009 au secrétariat du contentieux du Conseil d'Etat, présentés pour la SOCIETE LANVIN S.A., dont le siège est situé zone industrielle, rue nouvelle à Eppeville (80400) ; la SOCIETE LANVIN S.A. demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 06DA00475 du 27 novembre 2008 par lequel la cour administrative d'appel de Douai a annulé le jugement du tribunal administratif d'Amiens du 7 février 2006 et partiellement fait droit à sa demande d'annulation de l'arrêté du préfet de la Somme du 23 septembre 2002, en tant qu'il a rejeté ses conclusions tendant à l'annulation de cet arrêté en ce qu'il la mettait en demeure de régulariser sa situation administrative en déposant un dossier de demande d'autorisation au titre de la législation sur les installations classées pour une station de traitement de déchets émargeant à la rubrique 167 c de la nomenclature et pour la fabrication d'engrais et de supports de culture à partir de matières organiques émargeant à la rubrique 2170 ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la directive 2006/12/CE du 5 avril 2006 ;<br/>
<br/>
              Vu la directive 2008/98/CE du 19 novembre 2008 ;<br/>
<br/>
              Vu le code de l'environnement ;<br/>
<br/>
              Vu le code rural et de la pêche maritime ;<br/>
<br/>
              Vu le décret n° 2009-1341 du 29 octobre 2009 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Raphaël Chambon, Auditeur,  <br/>
<br/>
              - les observations de la SCP Peignot, Garreau, avocat de la SOCIÉTÉ LANVIN S.A., <br/>
<br/>
              - les conclusions de M. Mattias Guyomar, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Peignot, Garreau, avocat de la SOCIÉTÉ LANVIN S.A. ;<br/>
<br/>
<br/>
<br/>Considérant qu'aux termes du premier alinéa de l'article L. 511-1 du code de l'environnement, dans sa version applicable à la date de l'arrêt attaqué : " Sont soumis aux dispositions du présent titre les usines, ateliers, dépôts, chantiers et, d'une manière générale, les installations exploitées ou détenues par toute personne physique ou morale, publique ou privée, qui peuvent présenter des dangers ou des inconvénients soit pour la commodité du voisinage, soit pour la santé, la sécurité, la salubrité publiques, soit pour l'agriculture, soit pour la protection de la nature et de l'environnement, soit pour la conservation des sites et des monuments ainsi que des éléments du patrimoine archéologique " ; qu'aux termes de l'article L. 511-2 du même code, dans sa version applicable à la même date : " Les installations visées à l'article L. 511-1 sont définies dans la nomenclature des installations classées établie par décret en Conseil d'Etat, pris sur le rapport du ministre chargé des installations classées, après avis du Conseil supérieur des installations classées. Ce décret soumet les installations à autorisation ou à déclaration suivant la gravité des dangers ou des inconvénients que peut présenter leur exploitation " ;<br/>
<br/>
              Considérant que la SOCIETE LANVIN S.A., qui exploite sur le territoire de la commune d'Eppeville une unité de mélange et de compostage de sciures et d'écorces avec des sels d'ammonium (chlorure d'ammonium) provenant de l'industrie, pour produire un amendement organique, a été mise en demeure le 23 septembre 2002 par le préfet de la Somme, du fait de l'évolution de la nature des produits entrant dans son procédé de fabrication par rapport à l'autorisation initiale, de déposer un dossier de demande d'autorisation au titre des rubriques de la nomenclature des installations classées : 167 c (traitement de déchets provenant d'installations classées), 2170 (fabrication d'engrais et de supports de culture à partir de matières organiques) et 2260 (broyage et criblage de matières végétales) ; que cette société a demandé l'annulation de cet arrêté ; qu'en dernier lieu, par l'arrêt attaqué, la cour administrative d'appel de Douai, statuant par la voie de l'évocation après annulation du jugement du tribunal administratif d'Amiens, a partiellement fait droit à la demande de la requérante mais confirmé le bien-fondé de l'arrêté du 23 septembre 2002, notamment en tant qu'il a mis cette société en demeure de régulariser sa situation administrative en déposant un dossier de demande d'autorisation au titre de la législation sur les installations classées pour une " station de traitement de déchets émargeant à la rubrique 167 c de la nomenclature " et pour la " fabrication d'engrais et de supports de culture à partir de matières organiques émargeant à la rubrique 2170 " ;<br/>
<br/>
              Considérant, en premier lieu, que la SOCIETE LANVIN S.A. faisait valoir devant la cour qu'elle fabriquait non un engrais mais un amendement organique et qu'ainsi son activité ne relevait pas de la rubrique n° 2170 de la nomenclature des installations classées recouvrant la " fabrication des engrais et supports de culture à partir de matières organiques " ; que la cour, pour confirmer le bien-fondé de la mise en demeure adressée à la société requérante de déposer une demande d'autorisation au titre de cette rubrique, s'est bornée à relever que la circonstance que la société mettrait sur le marché un produit homologué n'affectait nullement la qualité d'installation classée soumise à déclaration, sans se prononcer sur ce moyen, qui n'était pas inopérant ; qu'ainsi, son arrêt est entaché d'insuffisance de motivation sur ce point ;<br/>
<br/>
              Considérant, en second lieu, que pour l'application de la législation relative aux installations classées, doit être regardée comme déchet toute substance qui n'a pas été recherchée comme telle dans le processus de production dont elle est issue, à moins que son utilisation ultérieure, sans transformation préalable, soit certaine ; qu'en jugeant que l'activité de fabrication d'un amendement organique de la SOCIETE LANVIN S.A. relevait de la rubrique n° 167 c de la nomenclature des installations classées au seul motif qu'entrait dans la composition du produit fini une solution de sulfate d'ammonium qui était un sous-produit de l'industrie chimique, sans rechercher si la réutilisation de cette substance, possible sans transformation préalable, était certaine, de sorte qu'elle échapperait à la qualification de déchet, la cour administrative d'appel de Douai a entaché son arrêt d'une erreur de droit ; <br/>
<br/>
              Considérant qu'il résulte de ce qui précède que l'arrêt attaqué doit être annulé en tant qu'il a rejeté les conclusions de la SOCIETE LANVIN S.A dirigées contre l'arrêté du 23 septembre 2002 en ce qu'il la met en demeure de déposer un dossier de demande d'autorisation au titre des rubriques n° 167 c et 2170 de la nomenclature des installations classées ;<br/>
<br/>
              Considérant qu'aux termes du second alinéa de l'article L. 821-2 du code de justice administrative : " Lorsque l'affaire fait l'objet d'un second pourvoi en cassation, le Conseil d'Etat statue définitivement sur cette affaire " ; qu'il y a lieu, par suite, de régler l'affaire au fond dans la mesure précisée ci-dessus ;<br/>
<br/>
              Considérant, en premier lieu, qu'il résulte des dispositions de l'article L. 255-1 du code rural et de la pêche maritime et des normes techniques applicables que si un amendement organique constitue, au même titre qu'un engrais, une matière fertilisante, il s'en distingue cependant par sa plus faible teneur en azote, phosphore, anhydride phosphorique ou oxyde de potassium ; qu'il ne peut non plus être regardé comme un support de culture, lequel est destiné à servir de milieu de culture à certains végétaux ; qu'ainsi, le préfet de la Somme ne pouvait légalement mettre en demeure le 23 septembre 2002 la SOCIETE LANVIN S.A. de déposer une demande d'autorisation au titre de la rubrique n° 2170 (fabrication des engrais et supports de culture à partir de matières organiques) de la nomenclature des installations classées ;<br/>
<br/>
              Considérant, en second lieu, qu'il résulte de l'instruction que la société Chamtor, dont la SOCIETE LANVIN S.A. acquiert le sulfate d'ammonium produit à l'occasion de la déminéralisation du sirop de glucose, a fait procéder à des analyses montrant la conformité de cette substance à la norme applicable aux engrais basse teneur ; qu'il en résulte que le sulfate d'ammonium ainsi produit, qui peut être utilisé sans transformation préalable et a une valeur économique qui en rend l'utilisation certaine, ne peut être regardé comme un déchet ; que, par ailleurs, la SOCIETE LANVIN S.A. était déjà autorisée au titre de l'utilisation de déchets de scierie ; qu'ainsi, le préfet de la Somme ne pouvait légalement mettre en demeure le 23 septembre 2002 la SOCIETE LANVIN S.A. de déposer une demande d'autorisation au titre de la rubrique n° 167 c (traitement ou incinération de déchets industriels provenant d'installations classées) ;<br/>
<br/>
              Considérant toutefois que le juge administratif, lorsqu'il statue en application de la législation relative aux installations classées pour la protection de l'environnement, fait application des dispositions législatives et réglementaires en vigueur à la date de sa décision ; que, par décret du 29 octobre 2009 modifiant la nomenclature des installations classées, la rubrique n° 2170 de la nomenclature a été modifiée pour comprendre également la fabrication d'amendements à partir de matières organiques ; que ce même décret a créé la rubrique n° 2780 " Installations de traitement aérobie (compostage ou stabilisation biologique) de déchets non dangereux ou matière végétale brute, ayant le cas échéant subi une étape de méthanisation " ; qu'une installation relevant de cette dernière rubrique ne peut relever également de la rubrique n° 2170 ; qu'il résulte de l'instruction que la fabrication d'amendements organiques par la SOCIETE LANVIN S.A. utilise désormais des matières premières issues de l'industrie agro-alimentaire ; que, par suite, cette activité relève dorénavant de la rubrique n° 2780 de la nomenclature des installations classées, qui vise notamment le compostage de rebuts de fabrication de denrées alimentaires végétales et de boues d'industries agroalimentaires ; que la quantité de matières traitées est supérieure à vingt tonnes par jour ; qu'il suit de là que la société requérante doit régulariser sa situation administrative en déposant une demande d'autorisation au titre de la rubrique n° 2780 ; qu'il y a lieu de la mettre en demeure de déposer une telle demande dans le délai de trois mois à compter de la notification de la présente décision ;<br/>
<br/>
              Considérant qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées pour la SOCIETE LANVIN S.A. au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>			D E C I D E :<br/>
              			--------------<br/>
<br/>
Article 1er : L'article 3 de l'arrêt de la cour administrative d'appel de Douai du 27 novembre 2008 est annulé en tant qu'il rejette les conclusions de la SOCIETE LANVIN S.A. dirigées contre l'arrêté du préfet de la Somme du 23 septembre 2002 en ce qu'il la met en demeure de déposer une demande d'autorisation au titre des rubriques n°s 167 c (traitement ou incinération de déchets industriels provenant d'installations classées) et 2170 (fabrication des engrais et supports de culture à partir de matières organiques) de la nomenclature des installations classées.<br/>
<br/>
Article 2 : La SOCIETE LANVIN S.A. est mise en demeure de déposer dans un délai de trois mois une demande d'autorisation au titre de la rubrique n° 2780 de la nomenclature des installations classées.<br/>
<br/>
Article 3 : L'arrêté du préfet de la Somme du 23 septembre 2002 est réformé en ce qu'il a de contraire à la présente décision. <br/>
<br/>
		Article 4 : Le surplus des conclusions de la SOCIETE LANVIN S.A. est rejeté.<br/>
<br/>
Article 5 : La présente décision sera notifiée à la SOCIETE LANVIN S.A. et à la ministre de l'écologie, du développement durable, des transports et du logement.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">44-02-01-02-01 NATURE ET ENVIRONNEMENT. INSTALLATIONS CLASSÉES POUR LA PROTECTION DE L'ENVIRONNEMENT. CHAMP D'APPLICATION DE LA LÉGISLATION. INSTALLATIONS ENTRANT DANS LE CHAMP D'APPLICATION DE LA LOI N° 76-663 DU 19 JUILLET 1976. - NOTION DE DÉCHETS EMPLOYÉE DANS LA NOMENCLATURE - DÉFINITION.
</SCT>
<ANA ID="9A"> 44-02-01-02-01 Pour l'application de la législation relative aux installations classées, doit être regardée comme déchet toute substance qui n'a pas été recherchée comme telle dans le processus de production dont elle est issue, à moins que son utilisation ultérieure, sans transformation préalable, soit certaine.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
