<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032188988</ID>
<ANCIEN_ID>JG_L_2016_03_000000384970</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/18/89/CETATEXT000032188988.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème SSJS, 09/03/2016, 384970, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-03-09</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>384970</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème SSJS</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FABIANI, LUC-THALER, PINATEL ; SCP ROCHETEAU, UZAN-SARANO</AVOCATS>
<RAPPORTEUR>Mme Anne Iljic</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Aurélie Bretonneau</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2016:384970.20160309</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme B...A...a demandé au tribunal administratif de Toulon : <br/>
              1°) d'annuler la décision du 16 août 2013 par laquelle le maire de la commune du Beausset (Var) a refusé de lui communiquer un ensemble de pièces faisant état de certains manquements dans l'exercice de ses fonctions ainsi qu'un rapport établi par l'un des agents de surveillance de la commune ; <br/>
              2°) d'enjoindre, sous astreinte de 50 euros par jour de retard, à la commune du Beausset de lui communiquer les documents sollicités dans un délai de quinze jours à compter de la notification de son jugement ; <br/>
              3°) de condamner la commune du Beausset à lui verser une somme de 120 000 euros en réparation des préjudices financier, moral et professionnel qu'elle aurait subis. <br/>
<br/>
              Par un jugement n° 1302820-1400348 du 7 mai 2014, le tribunal administratif de Toulon a annulé la décision du 16 août 2013 du maire de la commune du Beausset en tant qu'elle portait sur des documents faisant état de manquements qu'aurait commis Mme A...dans l'exercice de ses fonctions et enjoint à la commune du Beausset de communiquer ces documents à la requérante dans un délai de quinze jours à compter de la notification de son jugement. <br/>
<br/>
              Par une ordonnance n° 14MA03217 du 29 septembre 2014, enregistrée le 3 octobre 2014 au secrétariat du contentieux du Conseil d'Etat, le président de la cour administrative d'appel de Marseille a transmis au Conseil d'Etat, en application des articles R. 351-2 et R. 811-1 du code de justice administrative, le pourvoi, enregistré le 7 juillet 2014 au greffe de cette cour, présenté par la commune du Beausset. Par ce pourvoi et par un mémoire enregistré le 17 décembre 2014 au secrétariat du contentieux, la commune du Beausset demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler les articles 3 et 4 de ce jugement n° 1302820-1400348 du 7 mai 2014 du tribunal administratif de Toulon ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter la demande de Mme A...;<br/>
<br/>
              3°) de mettre à la charge de Mme A...la somme de 4 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la loi n° 78-753 du 17 juillet 1978 ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Anne Iljic, maître des requêtes ; <br/>
<br/>
              - les conclusions de Mme Aurélie Bretonneau, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Fabiani, Luc-Thaler, Pinatel, avocat de la commune du Beausset et à la SCP Rocheteau, Uzan-Sarano, avocat de Mme B...A...;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que MmeA..., adjoint administratif territorial de seconde classe de la commune du Beausset, a demandé au maire de cette commune la communication d'un ensemble de documents faisant état de manquements qu'elle aurait commis dans l'exercice de ses fonctions, dont l'avocat de la commune indiquait, dans une note du 15 juillet 2009, qu'ils lui avaient été transmis par le maire dans le cadre d'une consultation relative aux suites à donner au comportement de Mme A...; que, saisie par MmeA..., la commission d'accès aux documents administratifs a rendu le 26 septembre 2013 un avis favorable à la communication de ces documents, sous réserve qu'ils existent et qu'ils émanent d'une autorité administrative, ou, dans le cas où ils proviendraient d'une personne physique, sous réserve de l'occultation des mentions permettant une identification directe ou indirecte de leur auteur ; que la commune du Beausset se pourvoit en cassation contre les articles 3 et 4 du jugement du 7 mai 2014 par lequel le tribunal administratif de Toulon, faisant partiellement droit à la demande que lui avait présentée MmeA..., a annulé la décision du maire de la commune du Beausset du 16 août 2013 lui refusant communication des documents en litige et lui a enjoint de procéder à leur communication dans un délai de deux mois ; que, par la voie d'un pourvoi incident, Mme A...demande l'annulation de l'article 5 du même jugement, par lequel le tribunal administratif a rejeté ses conclusions à fins d'astreinte et ses conclusions indemnitaires ; <br/>
<br/>
              Sur le pourvoi principal :<br/>
<br/>
              2. Considérant que, devant les juges du fond, la commune du Beausset soutenait qu'elle ne disposait plus des documents dont la communication était demandée par Mme A...au motif qu'elle les avait transmis à son avocat ; que les juges du fond se sont abstenus de prendre en compte ces circonstances de fait en estimant qu'aucun élément ne pouvait donner à penser que la commune serait dans l'impossibilité matérielle de communiquer les documents demandés ; qu'ainsi, le jugement attaqué est entaché d'erreur de droit au regard de la portée de l'obligation de communication résultant des dispositions de la loi du 17 juillet 1978, désormais codifiée au livre III du code des relations entre le public et l'administration, laquelle ne saurait imposer la transmission d'un document malgré une impossibilité matérielle ; qu'il suit de là que, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, la commune du Beausset est fondée à demander l'annulation des articles 3 et 4 du jugement attaqué ;<br/>
<br/>
              3. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler, dans cette mesure, l'affaire au fond, en application de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              4. Considérant que, compte tenu de l'impossibilité matérielle de communiquer les documents demandés alléguée par la commune et faute d'élément de nature à la remettre en cause, Mme A...n'est pas fondée à demander l'annulation de la décision du 16 août 2013 par laquelle le maire de la commune du Beausset lui a refusé la communication de ces documents ; <br/>
<br/>
              Sur le pourvoi incident :<br/>
<br/>
              5. Considérant que l'annulation de l'article 4 du jugement faisant injonction à la commune de communiquer à Mme A...les documents en litige prive d'objet le pourvoi incident présenté par cette dernière en tant qu'il est dirigé contre le rejet des conclusions à fin d'astreinte qu'elle avait présentées en première instance ; <br/>
<br/>
              6. Considérant que les conclusions présentées par Mme A...tendant à l'annulation de l'article 5 du jugement attaqué en tant qu'il rejette ses conclusions tendant à la réparation des préjudices financier, moral et professionnel qu'elle aurait subis relèvent d'un litige distinct de la communication de documents administratifs ; qu'elles doivent, pour ce motif, être rejetées ; <br/>
<br/>
              Sur les conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              7. Considérant qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par la commune du Beausset au titre des dispositions de l'article L. 761-1 du code de justice administrative ; que ces dispositions ainsi que celles de l'article 37 de la loi du 10 juillet 1991 font obstacle à ce qu'ils soit fait droit aux conclusions présentées sur leur fondement par Mme A...; <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : Les articles 3 et 4 du jugement du tribunal administratif de Toulon en date du 7 mai 2014 sont annulés.<br/>
<br/>
Article 2 : Il n'y a pas lieu de statuer sur les conclusions présentées par Mme A...en tant qu'elles concernent le refus de prononcer une astreinte.<br/>
<br/>
Article 3 : Les conclusions de Mme A...sont rejetées en tant qu'elles sont dirigées contre l'irrecevabilité opposée à ses conclusions indemnitaires.<br/>
<br/>
Article 4 : La demande de Mme A...est rejetée en tant qu'elle porte sur le refus du maire de la commune du Beausset de lui communiquer les documents faisant état de ce qu'elle aurait commis certains manquements dans l'exercice de ses fonctions.<br/>
<br/>
Article 5 : Les conclusions présentées par la commune du Beausset au titre des dispositions de l'article L. 761-1 du code de justice administrative et par la SCP Rocheteau, Uzan-Sarano, avocat de MmeA..., au titre des dispositions de l'article 37 de la loi du 10 juillet 1991, sont rejetées.  <br/>
<br/>
Article 6 : La présente décision sera notifiée à la commune du Beausset et à Mme B...A....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
