<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000024942955</ID>
<ANCIEN_ID>JG_L_2011_12_000000343327</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/24/94/29/CETATEXT000024942955.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème et 1ère sous-sections réunies, 09/12/2011, 343327</TITRE>
<DATE_DEC>2011-12-09</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>343327</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème et 1ère sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Christian Vigouroux</PRESIDENT>
<AVOCATS/>
<RAPPORTEUR>M. Didier Ribes</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Cyril Roger-Lacan</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2011:343327.20111209</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la requête, enregistrée le 16 septembre 2010 au secrétariat du contentieux du Conseil d'État, présentée par Mme Frédérique C, demeurant ..., Mme Michèle A, demeurant ..., M. Patrick B, demeurant ... ; Mme C et autres demandent au Conseil d'État :<br/>
<br/>
              1°) d'annuler le jugement n° 1002441-1002446 du 17 août 2010 par lequel le tribunal administratif de Nice a rejeté leur protestation dirigée contre les opérations électorales qui se sont déroulées le 25 juin 2010 proclamant élus Mme Maty D et M. Joseph E, en qualité de délégués communautaires de la commune de Nice à la communauté urbaine Nice Côte d'Azur ;<br/>
<br/>
              2°) d'annuler l'élection de Mme D et de M. E en qualité de délégués communautaires de la commune de Nice à la communauté urbaine Nice Côte d'Azur ;<br/>
<br/>
              3°) de prononcer la suspension du mandat de conseiller communautaire de Mme D et de M. E ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code électoral ; <br/>
<br/>
              Vu le code général des collectivités territoriales ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Didier Ribes, Maître des Requêtes,  <br/>
<br/>
<br/>
              - les conclusions de M. Cyril Roger-Lacan, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant que Mme C et autres font appel du jugement du tribunal administratif de Nice du 17 août 2010 en tant qu'il a rejeté leur protestation tendant à l'annulation des opérations électorales qui se sont déroulées le 25 juin 2010 en vue de désigner deux délégués supplémentaires de la commune de Nice au sein de l'organe délibérant de la communauté urbaine Nice Côte d'azur ;<br/>
<br/>
              Sans qu'il soit besoin d'examiner les autres moyens de la requête ;<br/>
<br/>
              Considérant qu'aux termes de l'article L. 2121-33 du code général des collectivités territoriales : " Le conseil municipal procède à la désignation de ses membres ou de délégués pour siéger au sein d'organismes extérieurs dans les cas et conditions prévus par les dispositions du présent code et des textes régissant ces organismes. (...) " ; que, s'agissant des communautés urbaines, l'article L. 5215-10 du même code dispose que " L'élection des délégués s'effectue selon les modalités suivantes : (...) 2°  (...) les délégués des communes au conseil de la communauté sont élus au scrutin de liste à un tour, sans adjonction ni suppression de noms et sans modification de l'ordre de présentation. La répartition des sièges entre les listes est opérée selon les règles de la représentation proportionnelle à la plus forte moyenne. (...) " ; qu'aux termes de l'article L. 5215-6 du même code : " (...) Lorsque le périmètre d'une communauté urbaine est étendu en application des dispositions de l'article L. 5215-40 ou L. 5215-40-1, le conseil de communauté peut être composé, jusqu'à son prochain renouvellement général, par un nombre de délégués supérieur à celui prévu aux alinéas précédents. Ce nombre, fixé de telle sorte que chaque nouvelle commune dispose au moins d'un siège, est arrêté par accord des deux tiers au moins des conseils municipaux des communes intéressées représentant plus de la moitié de la population totale (...) " ; <br/>
<br/>
              Considérant qu'il résulte de ces dispositions que les délégués de la commune au sein du conseil de la communauté urbaine sont élus au scrutin de liste à un tour selon les règles de la représentation proportionnelle ; que, compte tenu de la nature de ce scrutin, il convient de procéder à une nouvelle désignation de l'ensemble des délégués de la commune lorsque le nombre de sièges dont dispose la commune au sein du conseil de la communauté urbaine est augmenté ;<br/>
<br/>
              Considérant qu'il ressort des pièces du dossier qu'à la suite de l'adhésion de deux communes à la communauté urbaine Nice Côte d'azur, un accord des conseils municipaux des communes membres de la communauté urbaine, entériné par un arrêté préfectoral du 26 avril 2010, a procédé, conformément aux dispositions de l'article L. 5215-6 précité, à une nouvelle répartition des délégués au sein du conseil de la communauté urbaine et a notamment attribué deux sièges supplémentaires à la commune de Nice ; que, lors de sa réunion extraordinaire du 25 juin 2010, le conseil municipal de la commune de Nice n'a procédé qu'à la désignation des seuls deux délégués supplémentaires, et non à celle de l'ensemble des délégués de la commune au sein du conseil de la communauté urbaine ; que, contrairement à ce qu'a jugé le tribunal administratif de Nice, le conseil municipal, en procédant ainsi, a méconnu les dispositions du 2° de l'article L. 5215-10 du code général des collectivités territoriales ;<br/>
<br/>
              Considérant qu'il résulte de ce qui précède que les requérants sont fondés à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Nice a rejeté leur protestation tendant à l'annulation des opérations électorales du 25 juin 2010 en vue de la désignation de deux délégués supplémentaires de la commune de Nice au sein de l'organe délibérant de la communauté urbaine Nice Côte d'azur ; qu'il suit également de là qu'il y a lieu d'annuler les opérations électorales du 25 juin 2010 ;<br/>
<br/>
<br/>
<br/>			D E C I D E :<br/>
              			--------------<br/>
Article 1er : Le jugement n° 1002441-1002446 du 17 août 2010 du tribunal administratif de Nice est annulé.<br/>
Article 2 : Les opérations électorales qui se sont déroulées le 25 juin 2010 sont annulées.<br/>
<br/>
Article 3 : La présente décision sera notifiée à Mme Frédérique C, à Mme Michèle A, à M. Patrick B, à Mme Maty D et à M. Joseph E.<br/>
Copie en sera adressée pour information au ministre de l'intérieur, de l'outre-mer, des collectivités territoriales et de l'immigration.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">135-05-01 COLLECTIVITÉS TERRITORIALES. COOPÉRATION. ÉTABLISSEMENTS PUBLICS DE COOPÉRATION INTERCOMMUNALE - QUESTIONS GÉNÉRALES. - DÉSIGNATION DES REPRÉSENTANTS DE LA COMMUNE QUAND LEUR NOMBRE AUGMENTE.
</SCT>
<ANA ID="9A"> 135-05-01 Compte tenu de la nature du scrutin mentionné à L. 5215-10 du code général des collectivités territoriales, à la représentation proportionnelle, il convient, lorsque le nombre de sièges dont dispose une commune au sein du conseil de la communauté urbaine est augmenté, de procéder à une nouvelle désignation de l'ensemble des délégués de la commune.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
