<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028135661</ID>
<ANCIEN_ID>JG_L_2013_10_000000356108</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/13/56/CETATEXT000028135661.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème et 1ère sous-sections réunies, 29/10/2013, 356108</TITRE>
<DATE_DEC>2013-10-29</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>356108</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème et 1ère sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SPINOSI ; SCP VINCENT, OHL</AVOCATS>
<RAPPORTEUR>M. Marc Pichon de Vendeuil</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Xavier de Lesquen</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:356108.20131029</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête et le mémoire complémentaire, enregistrés les 23 janvier et 23 avril 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour la SAS EIM France, dont le siège est 21, place de la Madeleine à Paris (75008), représentée par son président directeur général en exercice ; la société demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler la décision du 21 octobre 2011 par laquelle la commission des sanctions de l'Autorité des marchés financiers (AMF) lui a infligé une sanction pécuniaire de 300 000 euros et a ordonné la publication de sa décision sur son site internet et dans le recueil de ses décisions ;<br/>
<br/>
              2°) de mettre à la charge de l'Autorité des marchés financiers la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, notamment son article 6 ;<br/>
<br/>
              Vu le code monétaire et financier ;<br/>
<br/>
              Vu le décret n° 2007-1206 du 10 août 2007 ;<br/>
<br/>
              Vu les arrêtés des 18 mars 2008 portant homologation de modifications du règlement général de l'Autorité des marchés financiers ;<br/>
<br/>
              Vu le règlement général de l'Autorité des marchés financiers ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Marc Pichon de Vendeuil, Maître des Requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Xavier de Lesquen, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Spinosi, avocat de la SAS EIM France et à la SCP Vincent, Ohl, avocat de l'Autorité des marchés financiers ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant que la SAS EIM France, société de gestion de portefeuille agréée exerçant notamment des activités de " multigestion alternative " consistant à créer puis à gérer des organismes de placement collectif en valeurs mobilières (OPCVM) qui investissent dans des fonds sous-jacents, connus sous la dénomination " hedge funds ", dont la gestion déroge aux principes classiques de répartition des risques, demande l'annulation de la décision du 21 octobre 2011 par laquelle la commission des sanctions de l'Autorité des marchés financiers (AMF) lui a infligé une sanction pécuniaire de 300 000 euros et a ordonné la publication de sa décision sur son site internet et dans le recueil de ses décisions, au motif, d'une part, d'un défaut de diligence et de professionnalisme dans le contrôle des risques liés aux investissements effectués par cette société dans certains fonds de mars 2006 à août 2008 et, d'autre part, de la méconnaissance des règles d'éligibilité des fonds et des ratios réglementaires ;<br/>
<br/>
              Sur la régularité de la décision attaquée :<br/>
<br/>
              2. Considérant qu'aux termes de l'article 6 § 1 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales : " Toute personne a droit à ce que sa cause soit entendue (...) publiquement (...) par un tribunal indépendant et impartial établi par la loi, qui décidera, soit des contestations sur ses droits et obligations de caractère civil, soit du bien-fondé de toute accusation en matière pénale dirigée contre elle. Le jugement doit être rendu publiquement (...) " ; qu'aux termes du V de l'article L. 621-15 du code monétaire et financier : " La décision de la commission des sanctions est rendue publique dans les publications, journaux ou supports qu'elle désigne, dans un format proportionné à la faute commise et à la sanction infligée. Les frais sont supportés par les personnes sanctionnées. Toutefois, lorsque la publication risque de perturber gravement les marchés financiers ou de causer un préjudice disproportionné aux parties en cause, la décision de la commission peut prévoir qu'elle ne sera pas publiée. " ; qu'il résulte de ces dernières dispositions que la décision de publication éventuellement prise par la commission des sanctions constitue un élément de la décision attaquée, qui fait grief aux parties sanctionnées et dont la contestation relève, comme le reste de la décision, d'un recours de plein contentieux ;<br/>
<br/>
              3. Considérant, d'une part, que lorsqu'elle est saisie d'agissements pouvant donner lieu aux sanctions prévues par le code monétaire et financier, la commission des sanctions de l'Autorité des marchés financiers doit être regardée comme décidant du bien-fondé d'accusations en matière pénale au sens des stipulations citées ci-dessus de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, alors même qu'elle n'est pas une juridiction au regard du droit interne ; que, toutefois, tant les modalités de publication prévues par l'article L. 621-15 du code monétaire et financier cité ci-dessus que l'examen devant le Conseil d'Etat d'un recours de plein contentieux assurent le respect des garanties de l'article 6 § 1 de la convention, notamment celle de la publicité des décisions ; que, par suite, la société requérante n'est pas fondée à soutenir que l'absence de lecture publique de la décision de la commission méconnaîtrait ces stipulations ;<br/>
<br/>
              4. Considérant, d'autre part, qu'il résulte de l'instruction qu'après avoir été délibérée à l'issue de la séance publique du 21 octobre 2011, la décision attaquée a été publiée le 23 novembre 2011 sur le site internet de l'AMF ; qu'un tel mode de publication est de nature à assurer au public un accès approprié au texte de la décision ;<br/>
<br/>
              5. Considérant, enfin, que si la commission a, alors qu'elle n'était pas tenue de faire, également visé dans sa décision des pièces qui, communiquées par la société requérante postérieurement à la date de ce délibéré, étaient dépourvues d'incidence sur le sens de la décision prise, cette circonstance n'a pas eu pour effet d'entacher d'irrégularité la procédure suivie ; que, dès lors, le moyen tiré de ce que la décision attaquée serait irrégulière faute d'avoir date certaine doit être écarté ;<br/>
<br/>
              Sur le bien-fondé de la décision attaquée :<br/>
<br/>
              En ce qui concerne le premier grief retenu par la commission des sanctions :<br/>
<br/>
              6. Considérant qu'aux termes de l'article L. 214-3 du code monétaire et financier, dans sa rédaction alors applicable : " Les organismes de placement collectif en valeurs mobilières, le dépositaire et la société de gestion doivent agir au bénéfice exclusif des souscripteurs. Ils doivent présenter des garanties suffisantes en ce qui concerne leur organisation, leurs moyens techniques et financiers, l'honorabilité et l'expérience de leurs dirigeants. Ils doivent prendre les dispositions propres à assurer la sécurité des opérations. Les organismes mentionnés aux articles L. 214-15, L. 214-16 et L. 214-24 doivent agir de façon indépendante. " ; que l'article L. 533-4 du même code disposait, pour les faits antérieurs au 1er novembre 2007, que : " Les prestataires de services d'investissement (...) sont tenus de respecter les règles de bonne conduite destinées à garantir la protection des investisseurs et la régularité des opérations. / Ces règles sont établies par l'Autorité des marchés financiers. (...) Elles obligent notamment à : (...) 2. Exercer leur activité avec la compétence, le soin et la diligence qui s'imposent, au mieux des intérêts de leurs clients et de l'intégrité du marché / 3. Etre doté des ressources et des procédures nécessaires pour mener à bien leurs activités et mettre en oeuvre ces ressources et procédures avec un souci d'efficacité (...) " ; que pour les faits postérieurs au 1er novembre 2007, l'article L. 533-1 du même code dispose que : " Les prestataires de services d'investissement agissent d'une manière honnête, loyale et professionnelle, qui favorise l'intégrité du marché " ; que, pour les faits survenus entre le 20 mars 2006 et le 1er novembre 2007, l'article 322-12 du règlement général de l'AMF disposait : " la société de gestion de portefeuille doit en permanence disposer de moyens, d'une organisation et de procédures de contrôle et de suivi en adéquation avec les activités exercées (...). La société de gestion de portefeuille doit disposer, selon des modalités adaptées à la nature au volume et aux risques de l'ensemble de ses activités, quel que soit leur lieu d'exercice, ainsi qu'à son organisation, des éléments suivants : (...) 2° Un système de mesure des résultats dégagés par les portefeuilles gérés pour le compte de tiers et un système de mesure, de surveillance et de maîtrise des risques encourus par lesdits portefeuilles, permettant de satisfaire aux dispositions de l'article 322-15 ", lequel précisait que " la société de gestion de portefeuille (...) doit pouvoir mesurer à tout moment les risques associés à ses positions et la contribution de ces positions au profil de risque général du portefeuille de l'OPCVM ou du mandant " ; que pour la période postérieure au 1er novembre 2007, ces obligations ont été reprises, en substance, par les articles 313-60, 313-61 et 313-54 du règlement général de l'AMF ; qu'enfin, l'arrêté du 18 mars 2008 a ajouté au II de ce dernier article que " (...) Dans le cadre des activités de gestion collective de la société de gestion de portefeuille, ces procédures de prise de décision incluent en particulier les diligences qui président à la sélection, au suivi et au contrôle des risques associés aux instruments financiers dans lesquels l'OPCVM investit " ;<br/>
<br/>
              7. Considérant, en premier lieu, que le principe de légalité des délits et des peines, lorsqu'il est appliqué à des sanctions qui n'ont pas le caractère de sanctions pénales, ne fait pas obstacle à ce que les infractions soient définies par référence aux obligations auxquelles est soumise une personne en raison de l'activité qu'elle exerce, de la profession à laquelle elle appartient ou de l'institution dont elle relève, ainsi que le rappellent les dispositions du II de l'article L. 621-15 du code monétaire et financier, selon lesquelles la commission des sanctions peut infliger une sanction aux personnes manquant à leurs obligations professionnelles définies par les lois, règlements et règles professionnelles approuvées par l'AMF ;<br/>
<br/>
              8. Considérant, d'une part, que la commission des sanctions a relevé que " les sociétés de gestion doivent élaborer des procédures et mettre en oeuvre des politiques qui permettent, tant lors de la sélection que du suivi des fonds, d'identifier les risques liés à leurs activités et, le cas échéant, de déterminer le niveau des risques qui peuvent être tolérés " et que, dans le domaine de la multigestion alternative, où l'information sur certains fonds sous-jacents, " non ou peu régulés ", n'est pas librement et publiquement disponible, où les stratégies sont complexes et variables dans le temps et où l'utilisation de leviers importants est fréquente, ces contrôles doivent être faits avec une particulière rigueur " ; qu'elle a ajouté " qu'il convient de rechercher, in concreto, si les prestations accomplies par EIM à partir du 20 mars 2006 lui ont ou non permis d'apprécier avec suffisamment de précision, au regard notamment des moyens mis en oeuvre par la société de gestion des fonds sous-jacents, du type de gestion retenu et de ses résultats, de l'organisation du processus décisionnel, des dispositifs de contrôle des risques et de contrôle interne, enfin, de la qualité des contrôles externes, les caractéristiques des fonds et les risques associés aux investissements qu'elle a décidé de faire ou de maintenir " ; que, ce faisant, la commission des sanctions s'est bornée, sans méconnaître le principe de légalité des délits et des peines, à détailler les obligations professionnelles de diligence qui, sur le fondement des dispositions rappelées au point 6, s'imposaient en l'espèce à la société requérante au vu de la nature et de l'importance de ses activités ; <br/>
<br/>
              9. Considérant, d'autre part, qu'il ressort notamment de ceux des textes cités au point 6 applicables à l'époque des faits reprochés, notamment de l'article 322-12 du règlement général de l'AMF dans sa rédaction issue de l'arrêté du 9 mars 2006 portant homologation de modifications du règlement général de l'Autorité des marchés financiers, que toute société de gestion de portefeuille devait en permanence disposer de moyens, d'une organisation et de procédures de contrôle et de suivi en adéquation avec les activités exercées afin de vérifier, dans l'intérêt de ses clients, le niveau de risques auxquels s'exposaient les investissements qu'elle réalisait ; que, d'une part, si l'arrêté du 18 mars 2008 portant homologation de modifications du règlement général de l'Autorité des marchés financiers a modifié le II de l'article 313-54 de ce règlement pour préciser que les procédures de prise de décision qu'une société de gestion de portefeuille doit établir et maintenir opérationnelles " incluent en particulier les diligences qui président à la sélection, au suivi et au contrôle des risques associés aux instruments financiers dans lesquels l'OPCVM investit ", de telles dispositions, qui ne sont au demeurant que partiellement postérieures aux faits reprochés, n'ont pas eu pour effet de créer une exigence nouvelle au titre de laquelle la société requérante ne pouvait être sanctionnée à la date des faits reprochés ; que, d'autre part, il en va de même, en tout état de cause, des prescriptions de la note de synthèse de l'AMF du 4 décembre 2008 qui, sans méconnaître ni ajouter aux dispositions législatives et réglementaires rappelées précédemment, évoquent en particulier la nécessité pour toute société de gestion de portefeuille d'établir et maintenir des procédures adéquates visant à détecter tout risque de non-conformité à leurs obligations professionnelles, y compris en cas d'investissement dans un " hedge fund " très demandé ; qu'il résulte de ce qui précède que la requérante n'est pas fondée à soutenir que le principe de non-rétroactivité de la loi pénale plus sévère aurait été méconnu ;<br/>
<br/>
              10. Considérant, enfin, que la décision attaquée se fonde sur les dispositions du code monétaire et financier et du règlement général de l'AMF rappelées au point 6, qui sont dénuées d'ambiguïté et connues des professionnels, prévoyant notamment que toute société de gestion de portefeuille doit en permanence disposer de moyens, d'une organisation et de procédures de contrôle et de suivi en adéquation avec les activités exercées afin de vérifier, dans l'intérêt de ses clients, le niveau de risques auxquels s'exposaient les investissements qu'elle réalise ; qu'il résulte au surplus de l'instruction que l'AMF en avait déjà donné l'interprétation dans d'autres instances ; qu'en les appliquant comme elle l'a fait au cas qui lui était soumis, en se bornant à déduire de l'absence ou de l'insuffisance des procédures d'évaluation et d'identification des risques présentés par les fonds dans lesquels la société EIM France avait décidé d'investir un manquement aux obligations de contrôle et de suivi incombant à cette dernière, la commission des sanctions n'a ni énoncé une règle nouvelle qui n'aurait pas été raisonnablement prévisible par les professionnels ni, en tout état de cause, méconnu le principe de sécurité juridique ;<br/>
<br/>
              11. Considérant, en deuxième lieu, qu'il ressort des propres termes de la décision attaquée, qui relève que les procédures mises en place par la société EIM France étaient insuffisantes au regard des exigences légales et réglementaires qu'elle vise et cite par ailleurs, que la commission des sanctions n'a pas fondé sa décision sur les prescriptions de la note du 4 décembre 2008 mentionnée au point 9 ; que, dès lors, le moyen tiré de ce que la commission ne pouvait, au regard de la date des faits reprochés, s'appuyer sur cette note manque en fait ;<br/>
<br/>
              12. Considérant, en troisième lieu, qu'il résulte de l'instruction, s'agissant d'une part des investissements réalisés dans le fonds " Kingate ", qu'en dépit de ses propres procédures internes de sélection des fonds, qui prévoyaient notamment une rencontre annuelle entre ses analystes et les gérants des fonds souscrits, la société s'est trouvée dans l'impossibilité d'avoir accès aux dirigeants du fonds Kingate pendant toute la période considérée du fait des refus opposés par ces derniers ; que la SAS EIM France n'a ainsi pu procéder par elle-même à aucun contrôle régulier des équipes, des moyens techniques et du fonctionnement de cette société ; que malgré ces obstacles qui témoignaient pourtant d'un manque de transparence manifeste de la part du gérant du fonds, elle s'est abstenue de tirer les conséquences de ce défaut de contrôle et a poursuivi ses investissements ; que si la SAS EIM France a certes mis en oeuvre des rendez-vous annuels avec la société FIM Advisers LLP, promoteur du fonds Kingate, afin de recueillir des informations relatives à ce fonds, elle n'a toutefois soumis cette société à aucune vérification particulière alors qu'au regard même de son intérêt de promoteur, les informations qu'elle fournissait ne pouvaient être regardées à elles seules comme suffisamment objectives et indépendantes des données qu'elle recevait elle-même de la société gérant le fonds Kingate ; que, s'agissant, d'autre part, des investissements réalisés dans les fonds " Santa Clara I, Santa Clara II et Santa Barbara ", il résulte de l'instruction que les souscriptions intervenues initialement dans ces fonds ont été effectuées préalablement à la mise en oeuvre de toutes vérifications de base par la société EIM France ; que si ses analystes ont réalisé des études plus solides à compter de janvier 2007, celles-ci concluaient au demeurant à un manque d'information patent, ce qui n'a pas empêché la poursuite des investissements dans ces fonds jusqu'en septembre 2008 ; que, par suite, en estimant que ces éléments caractérisaient des manquements de la SAS EIM France aux obligations de diligence prévues par les dispositions citées au point 6, la commission des sanctions de l'Autorité des marchés financiers n'a pas inexactement qualifié les faits qui lui étaient soumis ;<br/>
<br/>
              En ce qui concerne le second grief retenu par la commission des sanctions :<br/>
<br/>
              13. Considérant que, dans sa rédaction applicable aux faits litigieux, l'article R. 214-5 du code monétaire et financier prévoyait que : " (...) l'actif d'un organisme de placement collectifs en valeurs mobilières peut également comprendre dans la limite de 10% : (...) 5° Des actions ou parts de fonds d'investissement de droit étranger répondant aux critères fixés par le règlement général de l'Autorité des marchés financiers " ; que, pour l'application de ces dispositions, l'article 411-34 du règlement général de l'AMF énonçait en particulier que : " Les fonds d'investissement au sens de l'article R. 214-5 du code monétaire et financier répondent en permanence aux critères suivants : (...) / 2° Leurs actifs sont conservés, au sens de l'article 323-2, de manière distincte des actifs propres du conservateur et de ses mandataires (...) " ; que, cependant, à compter de l'entrée en vigueur du décret du 10 août 2007 adaptant les règles applicables aux organismes de placement collectif et aux sociétés d'investissement à capital fixe, qui a notamment modifié l'article R. 214-36 du code monétaire et financier, certains OPCVM dits " Aria III " pouvaient détenir des fonds étrangers jusqu'à hauteur de 10% de leurs actifs sans avoir à respecter ces critères ; <br/>
<br/>
              14. Considérant, d'une part, que pour caractériser une violation par la société EIM France des règles d'inéligibilité de certains fonds de droit étranger posées par les dispositions citées ci-dessus, la commission des sanctions a relevé que le propre prospectus du fonds Kingate faisait expressément état de ce que BMIS, en sa qualité d'" Investment Advisor " du fonds, assurait également la conservation des actifs gérés ; que si la société requérante soutient que, ce faisant, la commission des sanctions n'a pas tenu compte des difficultés d'interprétation et d'application des critères d'éligibilité, alors en particulier qu'une "interprétation " formulée en 2006 par l'Association française de la gestion financière retenait que l'enregistrement d'une société d'investissement auprès de la Securities and Exchanges Commission (SEC) et du National Association of Securities Dealers (NASD) emportait respect automatique de la condition de ségrégation des actifs, il résulte de l'instruction que, quelle que soit au demeurant la valeur juridique de cette " interprétation ", la portée de la règle posée par l'article 411-34 du règlement général de l'AMF était suffisamment claire pour que, en présence de documents attestant de l'absence de séparation des actifs entre le conservateur et ses mandataires, la société EIM France s'abstienne d'investir dans un fonds d'investissement de droit étranger inéligible ; <br/>
<br/>
              15. Considérant, d'autre part, qu'il résulte de l'instruction et qu'il n'est d'ailleurs même pas contesté que le fonds " CIMR Développement " géré par la société EIM France ne pouvait pas détenir à son actif des parts de fonds d'investissement étrangers ne respectant pas les termes de l'article 411-34 du règlement général de l'AMF ; que, pourtant, CIMR Développement a souscrit, à partir du 28 février 2007, des parts du fonds Kingate représentant, en décembre 2008, 5,61% de ses actifs nets ; que, dès lors, en relevant que si elles sont de nature à atténuer la gravité du manquement, l'erreur de compréhension des actifs éligibles aux investissements de CIMR Développement, qui a été détectée par EIM France dans le cadre de son contrôle des risques et l'indemnisation des porteurs pour une valeur totale de 650 000 euros sont sans effet sur la constitution du grief, la commission des sanctions n'a pas commis d'erreur de droit ;<br/>
<br/>
              16. Considérant, enfin, s'agissant de la gestion par EIM France des fonds " Pleyel ", " Diversifié Rendement Absolu " et " Eucalyptus ", qu'il résulte de l'instruction que la société requérante avait réalisé et a maintenu pendant une période non couverte par la prescription des investissement dans le fonds Kingate avant que celui-ci ne devienne éligible à leurs actifs respectifs en raison de leur transformation en fonds de type Aria III ; que, dès lors, la commission des sanctions a légalement pu estimer que EIM France avait méconnu les règles d'éligibilité alors applicables ; que la circonstance que, par ailleurs, la commission des sanctions ait observé que la société n'a pas modifié, comme elle aurait dû le faire, les prospectus d'information pour faire état de la possibilité de souscrire des parts de fonds étrangers ne respectant pas les critères d'éligibilité est sans incidence sur la caractérisation de ce  manquement ; que, par suite, la société EIM France ne peut utilement soutenir que la commission des sanctions a commis une erreur de droit en estimant que la société était tenue de modifier les prospectus d'information pour faire état de la possibilité de souscrire des parts de fonds étrangers ne respectant pas les critères d'éligibilité ;<br/>
<br/>
              En ce qui concerne le caractère proportionné de la sanction pécuniaire prononcée :<br/>
<br/>
              17. Considérant que s'il appartient au juge administratif, saisi d'une requête dirigée contre une sanction pécuniaire prononcée par la commission des sanctions de l'AMF, de vérifier que son montant était, à la date à laquelle elle a été infligée, proportionné tant aux manquements commis qu'à la situation, notamment financière, de la personne sanctionnée, il ne résulte pas de l'instruction qu'en infligeant à la société EIM France une sanction pécuniaire de 300 000 euros, la commission des sanctions ait, dans les circonstances de l'espèce, infligé une sanction disproportionnée au regard de la gravité et de la nature des manquements reprochés ainsi que de la situation financière de cette société ; qu'est sans incidence à cet égard la circonstance que, à la date de publication de la décision de la commission des sanctions, l'AMF a fait paraître un communiqué de presse destiné à résumer le contenu de la décision rendue et faisant état de ce que " la commission des sanctions a entendu rappeler l'importance qu'elle attachait au respect des règles de prudence qui s'imposent aux sociétés de gestion de portefeuille réalisant des investissements pour le compte de tiers " ;<br/>
<br/>
              18. Considérant qu'il résulte de tout ce qui précède que la société EIM France n'est pas fondée à demander l'annulation ni la réformation de la décision attaquée ;<br/>
<br/>
              Sur les conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              19. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Autorité des marchés financiers qui n'est pas, dans la présente instance, la partie perdante ; qu'il y a lieu en revanche, dans les circonstances de l'espèce, de mettre à la charge de la société EIM France la somme de 3 000 euros à verser à l'Autorité des marchés financiers, au titre des mêmes dispositions ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : La requête de la société EIM France est rejetée.<br/>
Article 2 : La société EIM France versera à l'Autorité des marchés financiers une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée à la SAS EIM France, à l'Autorité des marchés financiers et au ministre de l'économie et des finances.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">13-01-02-01 CAPITAUX, MONNAIE, BANQUES. CAPITAUX. OPÉRATIONS DE BOURSE. AUTORITÉ DES MARCHÉS FINANCIERS. - COMMISSION DES SANCTIONS - DÉCISION DE SANCTION - VISA SUPERFLU DE PIÈCES PRODUITES POSTÉRIEUREMENT À LA DATE DU DÉLIBÉRÉ ET SANS INCIDENCE SUR LE SENS DE LA DÉCISION PRISE - IRRÉGULARITÉ DE LA PROCÉDURE - ABSENCE.
</SCT>
<ANA ID="9A"> 13-01-02-01 La circonstance que la commission des sanctions de l'Autorité des marchés financiers a, alors qu'elle n'était pas tenue de faire, visé dans sa décision de sanction des pièces qui, communiquées par la société requérante postérieurement à la date du délibéré, étaient dépourvues d'incidence sur le sens de la décision prise, n'entache pas d'irrégularité la procédure suivie.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
