<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000025401550</ID>
<ANCIEN_ID>J1_L_2012_02_000001100384</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/25/40/15/CETATEXT000025401550.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Cour administrative d'appel de Paris, 9ème Chambre, 09/02/2012, 11PA00384, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2012-02-09</DATE_DEC>
<JURIDICTION>Cour administrative d'appel de Paris</JURIDICTION>
<NUMERO>11PA00384</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème Chambre</FORMATION>
<TYPE_REC>excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>Mme MONCHAMBERT</PRESIDENT>
<AVOCATS>DAHHAN</AVOCATS>
<RAPPORTEUR>Mme Françoise  VERSOL</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme BERNARD </COMMISSAIRE_GVT>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 21 janvier 2011, présentée pour M. Xinji A, demeurant chez ... ..., par Me Dahhan ; M. A demande à la Cour :<br/>
<br/>
       1°) d'annuler le jugement n° 1009065/6-2 du 4 janvier 2011 par lequel le Tribunal administratif de Paris a rejeté sa demande tendant à l'annulation de l'arrêté du préfet de police du 28 avril 2010 rejetant sa demande d'admission exceptionnelle au séjour, l'obligeant à quitter le territoire français dans le délai d'un mois et fixant le pays de destination ; <br/>
<br/>
       2°) d'annuler cet arrêté ;<br/>
<br/>
.....................................................................................................................<br/>
<br/>
       Vu les pièces du dossier attestant que la requête de M. A a été communiquée le 21 février 2011 au préfet de police qui n'a pas produit de mémoire en défense ;<br/>
       Vu les autres pièces du dossier ;<br/>
<br/>
       Vu le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
<br/>
       Vu le code de justice administrative ; <br/>
<br/>
       Les parties ayant été régulièrement averties du jour de l'audience ;<br/>
<br/>
       Après avoir entendu au cours de l'audience publique du 26 janvier 2012 :<br/>
<br/>
       - le rapport de Mme Versol, <br/>
<br/>
       - et les conclusions de Mme Bernard, rapporteur public ;<br/>
<br/>
       Considérant que M. A, de nationalité chinoise, a sollicité son admission exceptionnelle au séjour sur le fondement de l'article L. 313-14 du code de l'entrée et du séjour des étrangers et du droit d'asile ; que par une décision du 28 avril 2010, le préfet de police a opposé un refus à sa demande de titre de séjour et a assorti ce refus d'une obligation de quitter le territoire français ; que M. A relève appel du jugement du 4 janvier 2011 par lequel le Tribunal administratif de Paris a rejeté sa demande tendant à l'annulation de cet arrêté ;<br/>
<br/>
       Considérant, en premier lieu, qu'il ne ressort ni des motifs de l'arrêté contesté, ni des autres pièces du dossier et il n'est pas établi que le préfet de police n'aurait pas procédé à un examen particulier de la situation de M. A ; que le moyen tiré du défaut d'un tel examen doit, par suite, être écarté ;<br/>
<br/>
       Considérant, en second lieu, qu'aux termes de l'article L. 313-14 du code de l'entrée et du séjour des étrangers et du droit d'asile :  La carte de séjour temporaire mentionnée à l'article L. 313-11 ou la carte de séjour temporaire mentionnée au 1° de l'article L. 313-10 sur le fondement du troisième alinéa de cet article peut être délivrée, sauf si sa présence constitue une menace pour l'ordre public, à l'étranger ne vivant pas en état de polygamie dont l'admission au séjour répond à des considérations humanitaires ou se justifie au regard des motifs exceptionnels qu'il fait valoir, sans que soit opposable la condition prévue à l'article L. 311-7 . (...) L'autorité administrative est tenue de soumettre pour avis à la commission mentionnée à l'article L. 312-1 la demande d'admission exceptionnelle au séjour formée par l'étranger qui justifie par tout moyen résider en France habituellement depuis plus de dix ans (...)  ; <br/>
<br/>
       Considérant que M. A fait valoir qu'entré en France en 1999, il s'y maintient depuis cette date ; que ses trois enfants, avec lesquels il est toujours en relation, résident également sur le territoire ainsi que son épouse dont il est divorcé depuis 2006 ; qu'il occupe un emploi et a régulièrement déclaré ses revenus à l'administration fiscale ; qu'il comprend suffisamment le français pour être autonome et n'a jamais causé de trouble à l'ordre public ; que pour ces motifs, la commission du titre de séjour saisie de sa demande a émis un avis favorable à sa régularisation ; que, toutefois, l'ancienneté de la présence en France de M. A ne peut être regardée comme répondant par elle-même à une considération humanitaire ou comme constituant un motif exceptionnel au sens des dispositions précitées de l'article L. 313-14 du code de l'entrée et du séjour des étrangers et du droit d'asile ; qu'en outre, il ne ressort pas des pièces du dossier que M. A, âgé de quarante et un ans à la date de son entrée en France, entretiendrait des relations avec ses trois enfants majeurs, ni d'ailleurs que ces derniers résideraient en situation régulière sur le territoire ; que, dans ces conditions, l'arrêté contesté ne méconnaît pas les dispositions de l'article L. 313-14 du code de l'entrée et du séjour des étrangers et du droit d'asile ; que le préfet de police n'a pas davantage entaché l'arrêté contesté d'une erreur manifeste dans l'appréciation des conséquences qu'il emporte sur la situation personnelle de l'intéressé ; <br/>
<br/>
       Considérant qu'il résulte de tout ce qui précède que M. A n'est pas fondé à soutenir que c'est à tort que, par le jugement attaqué, le Tribunal administratif de Paris a rejeté sa demande ;<br/>
<br/>
<br/>
       D É C I D E :<br/>
<br/>
Article 1er : La requête de M. A est rejetée.<br/>
''<br/>
''<br/>
''<br/>
''<br/>
2<br/>
N° 11PA00384<br/>
<br/>
<br/>
<br/>
</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">335-01-03 Étrangers. Séjour des étrangers. Refus de séjour.
</SCT>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
