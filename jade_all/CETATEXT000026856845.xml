<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026856845</ID>
<ANCIEN_ID>JG_L_2012_12_000000363303</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/85/68/CETATEXT000026856845.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème et 10ème sous-sections réunies, 28/12/2012, 363303, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2012-12-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>363303</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème et 10ème sous-sections réunies</FORMATION>
<TYPE_REC>Autres</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LAUGIER, CASTON</AVOCATS>
<RAPPORTEUR>Mme Séverine Larere</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Aladjidi</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2012:363303.20121228</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu l'ordonnance n° 1201538 du 8 octobre 2012, enregistrée le 9 octobre 2012 au secrétariat du contentieux du Conseil d'Etat, par laquelle le président du tribunal administratif de Châlons-en-Champagne, avant qu'il soit statué sur la demande de la SARL Majestic Champagne, tendant à la décharge de la taxe additionnelle à la cotisation sur la valeur ajoutée des entreprises à laquelle elle a été assujettie au titre de l'année 2011, a décidé, par application des dispositions de l'article 23-2 de l'ordonnance n° 58-1067 du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel, de transmettre au Conseil d'Etat la question de la conformité aux droits et libertés garantis par la Constitution de l'article 1600 du code général des impôts ;<br/>
<br/>
              Vu le mémoire, enregistré le 7 septembre 2012 au greffe du tribunal administratif de Châlons-en-Champagne, présenté pour la SARL Majestic Champagne, dont le siège est 16 rue Blaise Pascal, BP 10100 à Perigny Cedex (17185) en application de l'article 23-1 de l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la Constitution, notamment son article 61-1 ;<br/>
<br/>
              Vu l'ordonnance n° 58-1067 du 7 novembre 1958 ; <br/>
<br/>
              Vu le code général des impôts ;<br/>
<br/>
              Vu la loi n° 2010-853 du 23 juillet 2010 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Séverine Larere, Maître des Requêtes,  <br/>
<br/>
              - les observations de la SCP Laugier, Caston, avocat de la SARL Majestic Champagne,<br/>
<br/>
              - les conclusions de M. Frédéric Aladjidi, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Laugier, Caston, avocat de la SARL Majestic Champagne ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'il résulte des dispositions de l'article 23-4 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel que, lorsqu'une juridiction relevant du Conseil d'Etat a transmis à ce dernier, en application de l'article 23-2 de cette même ordonnance, la question de la conformité à la Constitution d'une disposition législative, le Conseil constitutionnel est saisi de cette question de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article 34 de la Constitution : "La loi fixe les règles concernant... l'assiette, le taux et les modalités de recouvrement des impositions de toutes natures " ; qu'il résulte de ces dispositions que, lorsqu'il définit une imposition, le législateur doit déterminer ses modalités de recouvrement, lesquelles comprennent les règles régissant le contrôle, le recouvrement, le contentieux, les garanties et les sanctions applicables à cette imposition ; que la méconnaissance par le législateur de sa propre compétence peut être invoquée à l'appui d'une question prioritaire de constitutionnalité dans le cas où est affecté un droit ou une liberté que la Constitution garantit ;<br/>
<br/>
              3. Considérant que la SARL Majestic Champagne soutient notamment que, faute de préciser les modalités de recouvrement de la taxe additionnelle à la cotisation sur la valeur ajoutée des entreprises, l'article 1600 du code général des impôts, dans sa rédaction antérieure à celle qui résulte de  l'article 39 de la loi de finances rectificative pour 2012 du 16 août 2012, est entaché d'une incompétence négative qui porte atteinte au droit de propriété garanti par les articles 2 et 17 de la Déclaration des droits de l'homme et du citoyen ; <br/>
<br/>
              4. Considérant que l'article 1600 du code général des impôts, dans sa rédaction antérieure à celle qui résulte de  l'article 39 de la loi du 16 août 2012, est applicable au litige dont est saisi le tribunal administratif de Châlons-en-Champagne ; qu'il n'a pas déjà été déclaré conforme à la Constitution par le Conseil constitutionnel ; que le moyen tiré de ce qu'il porte atteinte aux droits et libertés garantis par la Constitution, et notamment au droit de propriété, soulève une question présentant un caractère sérieux ; qu'ainsi, il y a lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité invoquée ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : La question de la conformité à la Constitution de l'article 1600 du code général des impôts dans sa rédaction applicable antérieurement à sa modification par  l'article 39 de la loi n°2012-958 du 16 août 2012 est renvoyée au Conseil constitutionnel.<br/>
Article 2 : La présente décision sera notifiée à la SARL Majestic Champagne, au Premier ministre et au ministre de l'économie et des finances.<br/>
Copie en sera adressée au tribunal administratif de Châlons-en-Champagne.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
