<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043926981</ID>
<ANCIEN_ID>JG_L_2021_08_000000434903</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/92/69/CETATEXT000043926981.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème chambre, 06/08/2021, 434903, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-08-06</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>434903</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP LYON-CAEN, THIRIEZ ; SCP OHL, VEXLIARD</AVOCATS>
<RAPPORTEUR>Mme Coralie Albumazard</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Olivier  Fuchs</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:434903.20210806</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 25 septembre et 26 décembre 2019 et 19 novembre 2020 au secrétariat du contentieux du Conseil d'Etat, M. A... B... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler la décision n° 2019-005 FCI-E du 18 juillet 2019 par laquelle le Haut conseil du commissariat aux comptes, saisi par le rapporteur général près ce Haut conseil, délibérant hors la présence de la formation restreinte, a suspendu l'inscription de M. B..., associé unique de la SASU Circée consulting, à compter du 1er août 2019, pour une durée de six mois, de la liste des commissaires aux comptes ;<br/>
<br/>
              2°) de mettre à la charge du Haut conseil du commissariat aux comptes la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de commerce ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Coralie Albumazard, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Olivier Fuchs, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Lyon-Caen, Thiriez, avocat de M. B... et à la SCP Ohl, Vexliard, avocat du Haut conseil du commissariat aux comptes ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Aux termes du 1er alinéa de l'article L. 824-7 du code de commerce : " Lorsque des faits d'une particulière gravité apparaissent de nature à justifier des sanctions pénales ou disciplinaires, le rapporteur général peut, lorsque l'urgence et l'intérêt public le justifient, et après que l'intéressé a été mis en mesure de présenter ses observations, saisir le Haut conseil qui délibère hors la présence de la formation restreinte d'une demande de suspension provisoire d'une durée maximale de six mois, d'un commissaire aux comptes, personne physique ". En application de ces dispositions, par une décision du 18 juillet 2019, le Haut conseil du commissariat aux comptes a suspendu l'inscription de M. B..., commissaire aux comptes et associé unique de la SASU Circée consulting, société de commissariat aux comptes, à compter du 1er août 2019, pour une durée de six mois, de la liste des commissaires aux comptes. M. B... demande l'annulation de cette décision.<br/>
<br/>
Sur la régularité de la décision attaquée :<br/>
<br/>
              2. En premier lieu, aux termes de l'article R. 824-8 du code de commerce : " I - Lorsque le rapporteur général envisage de saisir le Haut conseil d'une demande de suspension provisoire d'un commissaire aux comptes en application de l'article L. 824-7, il le notifie avec l'indication des griefs à l'intéressé et met ce dernier en demeure de présenter ses observations dans un délai de quinze jours à compter de la réception de la notification. En cas d'urgence, ce délai est ramené à soixante-douze heures. (...) ". <br/>
<br/>
              3. Il résulte de l'instruction que, par un courrier du 9 juillet 2019, le rapporteur général près le Haut conseil du commissariat aux comptes a invité M. B... à produire ses observations, dans un délai de soixante-douze heures à compter de la notification, sur les griefs retenus contre lui en vue de la saisine du Haut conseil sur le fondement de l'article L. 824-7 du code de commerce cité précédemment. Eu égard à la nature et à la gravité des griefs en cause, tirés de la mise en examen, le 22 mai 2019, de l'intéressé pour blanchiment aggravé, faux et usage de faux en écriture privée pour des faits commis entre 2014 et 2015 et de son placement, par ordonnance du même jour, sous contrôle judiciaire avec interdiction d'exercer l'activité d'expert-comptable et de gérer, diriger ou administrer une société à compter du 1er août 2019, l'urgence à saisir le Haut conseil afin qu'il se prononce sur une mesure de suspension des activités de commissaire aux comptes était, en l'espèce, établie. Au demeurant, M. B... a été en mesure de présenter ses observations par un courrier du 16 juillet 2019. Le moyen tiré de la méconnaissance de l'article R. 824-8 du code de commerce et du principe du caractère contradictoire de la procédure doit donc être écarté.<br/>
<br/>
              4. En second lieu, aux termes de l'article 13 du règlement intérieur du Haut conseil du commissariat aux comptes : " L'ordre du jour est adressé aux membres du haut conseil ainsi qu'au commissaire du Gouvernement par voie électronique au plus tard cinq jours avant la séance ". Il résulte de l'instruction que la demande de suspension de M. B... a été adressée aux membres du Haut conseil le 17 juillet 2019, la veille de la séance pendant laquelle la décision attaquée a été prise. Cependant, eu égard à l'urgence de soumettre la situation de l'intéressé au Haut conseil et à la nature de la demande, la méconnaissance du délai, qui n'est pas prescrit à peine de nullité, n'entache pas la décision attaquée d'irrégularité.<br/>
<br/>
              Sur le bien-fondé de la décision attaquée :<br/>
<br/>
               5. En premier lieu, aux termes de l'article 1er de l'annexe 8-1 du code de déontologie de la profession de commissaire aux comptes : " Le commissaire aux comptes exerce une mission d'intérêt général dans les conditions fixées par la loi. / Le présent code définit la déontologie à laquelle est soumis le commissaire aux comptes dans l'accomplissement de sa mission. Ses dispositions s'imposent à tout commissaire aux comptes, quel que soit son mode d'exercice. / Le respect des dispositions du présent code fait l'objet de vérifications lors des inspections et des contrôles auxquels sont soumis les commissaires aux comptes. " Aux termes de l'article 2 de ce code de déontologie : " Le commissaire aux comptes doit se conformer aux lois et règlements ainsi qu'aux dispositions du présent code. ".<br/>
<br/>
              6. Il résulte des termes mêmes de la décision attaquée que le Haut conseil s'est fondé sur la gravité des faits reprochés à l'intéressé, tant en raison de leur nature que de leur réalisation à l'occasion de l'exercice de l'activité d'expert-comptable ainsi que sur les obligations qui s'imposent à la profession de commissaire aux comptes, résultant des dispositions précitées, notamment celles de l'exercer avec honnêteté et droiture et de s'abstenir, en toutes circonstances, de tout agissement contraire à l'honneur et à la probité. Par suite, le Haut conseil n'a pas commis d'erreur de droit en en déduisant que l'urgence et l'intérêt public justifiaient la mesure de suspension litigieuse.<br/>
<br/>
              7. En second lieu, la mesure prévue par l'article L. 824-7 du code de commerce est une mesure de caractère conservatoire, prononcée dans le souci de préserver l'intérêt public, permettant de suspendre à titre provisoire et pour une durée limitée les activités d'un commissaire aux comptes lorsque des faits d'une particulière gravité apparaissent de nature à justifier des sanctions pénales ou disciplinaires. Par suite, eu égard à ce qui a été dit plus haut, le Haut conseil a pu légalement prendre la mesure attaquée sans attendre que la chambre de l'instruction de Versailles se soit prononcée sur l'appel que M. B... a formé contre l'ordonnance le plaçant sous contrôle judiciaire.<br/>
<br/>
              8. Il résulte de ce qui précède que M. B... n'est pas fondé à demander l'annulation de la décision du Haut conseil du commissariat aux comptes le suspendant pour une durée de six mois de la liste des commissaires aux comptes.<br/>
<br/>
              9. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge du Haut conseil du commissariat aux comptes qui n'est pas, dans la présente instance, la partie perdante. <br/>
<br/>
              10. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de M. B... la somme de 3 000 euros à verser au Haut conseil du commissariat aux comptes au titre de ces mêmes dispositions.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de M. B... est rejetée.<br/>
Article 2 : M. B... versera au Haut conseil du commissariat aux comptes la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
Article 3 : La présente décision sera notifiée à M. A... B... et au Haut conseil du commissariat aux comptes.<br/>
Copie en sera adressée au garde des sceaux, ministre de la justice.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
