<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026335480</ID>
<ANCIEN_ID>JG_L_2002_12_0000238602</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/33/54/CETATEXT000026335480.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème et 8ème sous-sections réunies, 18/12/2002, 238602, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2002-12-18</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>238602</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème et 8ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Stirn</PRESIDENT>
<AVOCATS>SCP BORE, XAVIER ET BORE</AVOCATS>
<RAPPORTEUR>Mme Aurélie  Robineau-Israël</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Séners</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2002:238602.20021218</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la requête sommaire et le mémoire complémentaire, enregistrés les 1er octobre 2001 et 1er février 2002 au secrétariat du contentieux du Conseil d'Etat, présentés pour Mme Danielle A, demeurant ... ; Mme A demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler l'arrêt du 29 juin 2001 par lequel la cour administrative d'appel de Nantes a rejeté sa requête tendant à l'annulation du jugement du 5 mai 1998 du tribunal administratif d'Orléans rejetant sa demande en décharge des suppléments d'impôt sur le revenu et des pénalités y afférentes auxquels elle a été assujettie au titre de l'année 1990 ;<br/>
<br/>
              2°) de condamner l'Etat à lui verser la somme de 2 300 euros en application de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code général des impôts et le livre des procédures fiscales ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Robineau-Israël, Auditeur,  <br/>
<br/>
              - les observations de la SCP Boré, Xavier et Boré, avocat de Mme A, <br/>
<br/>
              - les conclusions de M. Séners, Commissaire du gouvernement ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant qu'à l'occasion d'une vérification de comptabilité, l'administration a estimé que le prix unitaire de 3 020 F (460,40 euros) auquel la société CPA/Paris a acquis, le 8 septembre 1990, 200 parts de la société CPA/Angers, dont 48 parts détenues par Mme A, était excessif à hauteur de 2 920 F (445,15 euros) et traduisait dans cette mesure un acte anormal de gestion ; qu'elle a, par suite, assujetti Mme A, au titre de l'année 1990, à des suppléments d'impôt sur le revenu résultant de l'imposition, en tant que revenu distribué au sens de l'article 109-1-2° du code général des impôts, d'une somme de 140 160 F (21 367,25 euros) ; que Mme A se pourvoit en cassation contre l'arrêt du 29 juin 2001 par lequel la cour administrative d'appel de Nantes a rejeté sa requête tendant à l'annulation du jugement du 5 mai 1998 du tribunal administratif d'Orléans rejetant sa demande en décharge des impositions litigieuses ;<br/>
<br/>
              Considérant qu'en jugeant que Mme A ne démontrait pas le caractère normal de l'acquisition litigieuse en faisant état de ce que sa transaction avec la société CPA/Paris était issue d'une "véritable négociation", la cour a répondu à l'argumentation de la requérante tirée de ce que la société CPA/Paris avait un intérêt économique à prendre le contrôle de la société CPA/Angers ; que son arrêt est, par conséquent, suffisamment motivé ;<br/>
<br/>
              Considérant qu'aux termes de l'article L. 11 du livre des procédures fiscales : "A moins qu'un délai plus long ne soit prévu par le présent livre, le délai accordé aux contribuables pour répondre aux demandes de renseignements, de justifications ou d'éclaircissements et, d'une manière générale, à toute notification émanant d'un agent de l'administration des impôts est fixé à trente jours à compter de la réception de cette notification" ; qu'aux termes de l'article L. 54 B du même livre : "La notification d'une proposition de redressement doit mentionner, sous peine de nullité, que le contribuable a la faculté de se faire assister d'un conseil de son choix pour discuter la proposition de redressement ou pour y répondre" ; qu'aux termes de l'article L. 57 du livre des procédures fiscales : "L'administration adresse au contribuable une notification de redressement qui doit être motivée de manière à lui permettre de formuler ses observations ou de faire connaître son acceptation (...)" ; qu'aux termes, enfin, du premier alinéa de l'article R*. 194-1 du même livre : "Lorsque, ayant donné son accord au redressement ou s'étant abstenu de répondre dans le délai légal à la notification de redressement, le contribuable présente cependant une réclamation faisant suite à une procédure contradictoire de redressement, il peut obtenir la décharge ou la réduction de l'imposition, en démontrant son caractère exagéré" ;<br/>
<br/>
              Considérant que l'article R*. 194-1 du livre des procédures fiscales, en assimilant à une acceptation le silence conservé par le contribuable pendant le délai qui lui est imparti pour répondre à une notification de redressement, et en lui attribuant dans ce cas la charge d'établir l'exagération de l'imposition, ne fait que tirer les conséquences des dispositions précitées des articles L. 11, L. 54 B et L. 57 du livre des procédures fiscales ; que le moyen tiré de ce que la cour aurait commis une erreur de droit en faisant application des dispositions, dépourvues de fondement légal, de cet article doit, par conséquent, être écarté ;<br/>
<br/>
              Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que la société CPA/Paris, société d'expertise comptable dont Mme A était associée, a souscrit le 28 février 1989, au prix unitaire de 100 F (15,24 euros), la totalité des 300 nouvelles parts émises par la société CPA/Angers, dont la requérante détenait 48 des 200 parts existantes ; qu'ainsi qu'il a été dit, la société CPA/Paris a racheté ces 200 autres parts sociales au prix unitaire de 3 020 F (460,40 euros) le 8 septembre 1990 ; <br/>
<br/>
              Considérant que, faute d'apporter des éléments permettant d'établir que le prix unitaire de 100 F auquel ont été émises les parts de la société CPA/Angers à l'occasion de l'augmentation de capital ne reflétait pas leur valeur réelle et que les perspectives bénéficiaires de la CPA/Angers et l'importance ou la qualité de sa clientèle justifiaient le prix retenu pour la transaction du 8 septembre 1990, Mme A n'apporte pas la preuve, dont elle a la charge, du caractère exagéré des impositions litigieuses ;<br/>
<br/>
              Considérant qu'il résulte de ce qui précède que Mme A n'est pas fondée à demander l'annulation  de l'arrêt attaqué ;<br/>
<br/>
              Sur les conclusions tendant à l'application de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              Considérant que les dispositions de l'article L.761-1 du code de justice administrative font obstacle à ce que l'Etat, qui n'est pas dans la présente instance la partie perdante, soit condamné à payer à Mme A la somme qu'elle demande au titre des frais exposés par elle et non compris dans les dépens ;<br/>
<br/>
<br/>
<br/>                     D E C I D E :<br/>
                                     --------------<br/>
<br/>
Article 1er : La requête de Mme A est rejetée.<br/>
Article 2 : La présente décision sera notifiée à Mme Danielle A et au ministre de l'économie, des finances et de l'industrie.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">335-005 Étrangers. Entrée en France.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-07-02-03 Procédure. Pouvoirs et devoirs du juge. Contrôle du juge de l'excès de pouvoir. Appréciations soumises à un contrôle normal.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">54-08-02-02-01-02 Procédure. Voies de recours. Cassation. Contrôle du juge de cassation. Régularité interne. Qualification juridique des faits.
</SCT>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
