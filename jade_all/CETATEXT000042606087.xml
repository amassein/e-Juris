<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042606087</ID>
<ANCIEN_ID>JG_L_2020_12_000000426692</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/60/60/CETATEXT000042606087.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème chambre, 02/12/2020, 426692, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-12-02</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>426692</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP HEMERY, THOMAS-RAQUIN, LE GUERER</AVOCATS>
<RAPPORTEUR>Mme Françoise Tomé</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Raphaël Chambon</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2020:426692.20201202</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête sommaire et un mémoire complémentaire enregistrés les 27 décembre 2018 et 26 mars 2019 au secrétariat du contentieux du Conseil d'Etat, M. B... D... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision du 24 octobre 2018 par laquelle le Conseil national de l'ordre des chirurgiens-dentistes a refusé de reconnaître le diplôme d'université d'hypnose médicale et clinique délivré par l'université de la Réunion ;<br/>
<br/>
              2°) d'enjoindre au Conseil national de l'ordre des chirurgiens-dentistes de réexaminer sa demande dans un délai de deux mois ;<br/>
<br/>
              3°) de mettre à la charge du Conseil national de l'ordre des chirurgiens-dentistes la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code des relations entre le public et l'administration ;<br/>
              - le code de la santé publique ;<br/>
              - le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
              - le code de justice administratif ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme C... A..., conseillère d'Etat,  <br/>
<br/>
              - les conclusions de M. Raphaël Chambon, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Hémery, Thomas-Raquin, Le Guerer, avocat de M. D... ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article R. 4127-216 du code de la santé publique : " Les seules indications que le chirurgien-dentiste est autorisé à mentionner sur ses imprimés professionnels, notamment ses feuilles d'ordonnances, notes d'honoraires et cartes professionnelles, sont : (...) / 3° Les titres et fonctions reconnus par le Conseil national de l'ordre (...) ". Aux termes de l'article R. 4127-218 du même code : " Les seules indications qu'un chirurgien-dentiste est autorisé à faire figurer sur une plaque professionnelle à la porte de son immeuble ou de son cabinet sont (...)  les diplômes, titres ou fonctions reconnus par le Conseil national de l'ordre ".<br/>
<br/>
              2. Par la décision attaquée, le Conseil national de l'ordre des chirurgiens-dentistes a refusé de reconnaître le diplôme d'université d'hypnose médicale et clinique délivré par l'université de la Réunion au titre de l'année universitaire 2016-2017, dont M. D..., qui a obtenu ce diplôme, avait demandé la reconnaissance.<br/>
<br/>
              3. Aux termes de l'article L. 221-2 du code des relations entre le public et l'administration : " l'entrée en vigueur d'un acte réglementaire est subordonnée à l'accomplissement de formalités adéquates de publicité (...) ". <br/>
<br/>
              4. Il ressort des pièces du dossier que la décision attaquée est fondée sur la décision à caractère réglementaire du Conseil national de l'ordre des chirurgiens-dentistes établissant le " protocole d'examen des demandes de reconnaissance des diplômes, titres et fonctions " qui fixe notamment les critères et la procédure selon lesquels ce conseil examine les demandes de reconnaissance des diplômes, titres et fonctions présentés par des chirurgiens-dentistes. La seule circonstance alléguée par le Conseil national de l'ordre des chirurgiens-dentistes que ce protocole ait fait l'objet d'un article dans la " lettre de l'Ordre n° 119 de juillet-août 2013 " qui constitue un mensuel d'informations professionnelles pour les chirurgiens-dentistes publié sur le site internet de l'ordre des chirurgiens-dentistes, ne permet pas de retenir que cet acte réglementaire a fait l'objet de mesures de publication adéquates permettant son entrée en vigueur et, par suite, son application à la date de la décision attaquée. Par suite, M. D... est fondé à soutenir que le Conseil national de l'ordre des chirurgiens-dentistes n'a pu légalement se fonder sur ce protocole pour prendre la décision attaquée. <br/>
<br/>
              5. Il résulte de ce qui précède, sans qu'il soit besoin d'examiner les autres moyens de la requête, que M. D... est fondé à demander l'annulation pour excès de pouvoir de la décision du Conseil national de l'ordre des chirurgiens-dentistes du 24 octobre 2018 qu'il attaque.<br/>
<br/>
              6. L'exécution de la présente décision implique que la demande de reconnaissance de diplôme de M. D... soit réexaminée. Il y a lieu, par suite, d'enjoindre au Conseil national de l'ordre des chirurgiens-dentistes de procéder à ce réexamen dans un délai de deux mois à compter de la notification de la présente décision.<br/>
<br/>
              7. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge du Conseil national de l'ordre des chirurgiens-dentistes une somme de 3 000 euros à verser à D... au titre des dispositions de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : La décision du Conseil national de l'ordre des chirurgiens-dentistes du 24 octobre 2018 est annulée. <br/>
Article 2 : Il est enjoint au Conseil national de l'ordre des chirurgiens-dentistes de procéder à un réexamen de la demande de reconnaissance de diplôme de M. D... dans un délai de deux mois à compter de la notification de la présente décision. <br/>
Article 3 : Le Conseil national de l'ordre des chirurgiens-dentistes versera une somme de 3 000 euros à M. D... au titre de l'article L. 761-1 du code de justice administrative. <br/>
Article 4 : La présente décision sera notifiée à M. B... D... et au Conseil national de l'ordre des chirurgiens-dentistes.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
