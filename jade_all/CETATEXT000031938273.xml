<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031938273</ID>
<ANCIEN_ID>JG_L_2016_01_000000378078</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/93/82/CETATEXT000031938273.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème - 10ème SSR, 20/01/2016, 378078, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-01-20</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>378078</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème - 10ème SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MONOD, COLIN, STOCLET</AVOCATS>
<RAPPORTEUR>Mme Séverine Larere</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emilie Bokdam-Tognetti</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2016:378078.20160120</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Procédure contentieuse antérieure<br/>
<br/>
              Mme A...B...a demandé au tribunal administratif de Rouen de prononcer la décharge des cotisations supplémentaires d'impôt sur le revenu auxquelles elle a été assujettie au titre de l'année 1995 ainsi que des intérêts de retard et intérêts légaux correspondants. Par un jugement n° 0000622, 0101327, 0200683, 0300732 du 20 décembre 2005, le tribunal administratif de Rouen n'a que partiellement fait droit à ses demandes.<br/>
<br/>
              Par un arrêt n° 06DA00292 du 14 mai 2009, la cour administrative d'appel de Douai, après avoir prononcé un non-lieu partiel à statuer, a accordé à Mme B...une décharge partielle d'imposition et a rejeté le surplus des conclusions de sa requête. <br/>
<br/>
              Par une décision n° 329821 du 12 décembre 2012 le Conseil d'Etat, statuant au contentieux, a annulé cet arrêt en tant qu'il a statué sur les conclusions de la requête de Mme B... relatives à l'application des intérêts de retard à la fraction de la cotisation d'impôt sur le revenu résultant des redressements liés à l'imposition de la plus-value de cession de titres constatée en 1995 et a renvoyé, dans cette mesure, l'affaire à cette cour.<br/>
<br/>
              Par un arrêt n° 13DA00023 du 20 février 2014, la cour administrative d'appel de Douai a déchargé Mme B...des intérêts de retard relatifs à la fraction de la cotisation d'impôt sur le revenu résultant des redressements liés à l'imposition de la plus-value de cession de titres constatée en 1995 et a réformé, en ce sens, le jugement du tribunal administratif de Rouen du 20 décembre 2005. <br/>
<br/>
              Procédure devant le Conseil d'Etat<br/>
<br/>
              Par un pourvoi et un mémoire en réplique, enregistrés les 18 avril 2014 et 17 juin 2015 au secrétariat du contentieux du Conseil d'Etat, le ministre des finances et des comptes publics demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 13DA00023 du 20 février 2014 de la cour administrative d'appel de Douai ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter les conclusions d'appel de Mme B....<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code général des impôts ;<br/>
              - le livre des procédures fiscales ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Séverine Larere, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Emilie Bokdam-Tognetti, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Monod, Colin, Stoclet, avocat de Mme B...;<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article L. 48 du livre des procédures fiscales, dans sa rédaction applicable à la présente procédure : " A l'issue d'un examen contradictoire de la situation fiscale personnelle au regard de l'impôt sur le revenu ou d'une vérification de comptabilité, lorsque des redressements sont envisagés, l'administration doit indiquer, avant que le contribuable présente ses observations ou accepte les redressements proposés, dans la notification prévue à l'article L. 57, le montant des droits, taxes et pénalités résultant de ces redressements (...) " ; qu'il résulte de ces dispositions que l'administration n'est tenue d'indiquer le montant des droits, taxes et pénalités résultant des redressements proposés que dans les notifications qui font suite à un examen contradictoire de la situation fiscale personnelle ou à une vérification de comptabilité ; que cette obligation ne s'applique pas, en revanche, aux redressements consécutifs à un contrôle sur pièces ;<br/>
<br/>
              2. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond qu'à l'occasion de la vérification de comptabilité de la société anonyme Square, l'administration fiscale a constaté que MmeB..., qui détenait 150 000 bons de souscription d'actions de cette société, avait réalisé, lors de la cession, le 15 décembre 1995, de ces bons à la société Stell, une plus-value qui n'avait pas été déclarée ; qu'après l'avoir mise en demeure, le 30 juillet 1997, de souscrire une déclaration, ce que l'intéressée a fait le 29 août 1997, l'administration lui a adressé, le 15 septembre 1997, une première notification de redressement en matière d'impôt sur le revenu au titre de l'année 1995 puis, le 3 avril 1998, une seconde notification de redressement réduisant le montant du rehaussement précédemment notifié ; <br/>
<br/>
              3. Considérant qu'il ressort des pièces du dossier soumis au juge du fond que le redressement dont a fait l'objet Mme B...était consécutif à un contrôle sur pièces et ne résultait pas d'un examen contradictoire de sa situation fiscale personnelle ; que la circonstance que l'administration a utilisé, pour établir ce redressement, des informations qu'elle avait recueillies dans le cadre de la vérification de la comptabilité de la société Square est sans incidence sur la nature de la procédure de contrôle suivie, distincte de celles prévues à l'article L. 48 du livre des procédures fiscales ; que la cour a, par suite, commis une erreur de droit en jugeant que la notification de redressement adressée à Mme B...était irrégulière faute de comporter l'indication, prévue par les dispositions précitées de l'article L. 48 du livre des procédures fiscales, du montant des intérêts de retard encourus en complément des droits ; que le ministre des finances et des comptes publics est, par suite, fondé à demander l'annulation de l'arrêt attaqué ;<br/>
<br/>
              4. Considérant qu'aux termes du second alinéa de l'article L. 821-2 du code de justice administrative : " Lorsque l'affaire fait l'objet d'un second pourvoi en cassation, le Conseil d'Etat statue définitivement sur cette affaire " ; qu'il y a lieu, par suite, de régler l'affaire au fond ; <br/>
<br/>
              5. Considérant, d'une part, qu'il résulte de ce qui a été dit au point 3 ci-dessus que les dispositions de l'article L. 48 précité du livre des procédures fiscales n'étaient pas applicables à la procédure d'imposition suivie avec MmeB... ; que le moyen tiré de la méconnaissance de ces dispositions doit, par suite, être écarté ; <br/>
<br/>
              6. Considérant, d'autre part, qu'aux termes de l'article 1727 du code général des impôts, dans sa rédaction alors applicable : " Le défaut ou l'insuffisance dans le paiement ou le versement tardif de l'un des impôts, droits, taxes, redevances ou sommes établis ou recouvrés par la direction générale des impôts donnent lieu au versement d'un intérêt de retard qui est dû indépendamment de toutes sanctions. (...) Le taux de l'intérêt de retard est fixé à 0,75 %. Il s'applique sur le montant des sommes mises à la charge du contribuable ou dont le versement a été différé. " ; qu'aux termes de l'article 1729 du même code, dans sa rédaction alors applicable : " 1. Lorsque la déclaration ou l'acte mentionnés à l'article 1728 font apparaître une base d'imposition ou des éléments servant à la liquidation de l'impôt insuffisants, inexacts ou incomplets, le montant des droits mis à la charge du contribuable est assorti de l'intérêt de retard visé à l'article 1727 et d'une majoration de 40 % si la mauvaise foi de l'intéressé est établie ou de 80 % s'il s'est rendu coupable de manoeuvres frauduleuses ou d'abus de droit au sens de l'article L. 64 du livre des procédures fiscales. / 2. Le décompte de l'intérêt de retard est arrêté au dernier jour du mois de la notification de redressement ou, en cas d'échelonnement des impositions supplémentaires, au dernier jour du mois au cours duquel le rôle doit être mis en recouvrement. (...) " ;<br/>
<br/>
              7. Considérant, en premier lieu, que si MmeB..., après une mise en demeure, a déposé une déclaration de plus-value sur imprimé cerfa n° 2049, cette déclaration spéciale n'a pas eu pour effet de régulariser l'insuffisance de sa déclaration de revenu global mais a, au contraire, mis en lumière l'insuffisance des revenus déclarés par l'intéressée au titre de l'année 1995 qui ne faisaient pas mention de cette plus-value ; que, par suite, Mme B...n'est pas fondée à soutenir que l'administration fiscale ne pouvait mettre à sa charge des intérêts de retard sur le fondement des dispositions précitées de l'article 1729 du code général des impôts ;<br/>
<br/>
              8. Considérant, en deuxième lieu, qu'il résulte des dispositions de l'article 1727 du code général des impôts que les intérêts de retard sont calculés par application du taux de cet intérêt sur le montant des sommes mises à la charge du contribuable ou dont le versement a été différé, soit la totalité des impositions éludées ; que, par suite, le moyen tiré de ce que l'intérêt de retard ne pouvait être appliqué que sur la somme correspondant à l'imposition qui aurait été réclamée à MmeB..., en cas de paiement fractionné de l'impôt, à la date de souscription d'une déclaration de plus-value, et non sur le total de l'imposition due, doit être écarté ;<br/>
<br/>
              9. Considérant, enfin, que le dernier jour du mois de la notification de redressement mentionné au 2 de l'article 1729 du code général des impôts correspond au dernier jour du mois de la première notification de redressement régulière reçue par le contribuable ; qu'en l'espèce, il résulte de l'instruction que la notification de redressement du 3 avril 1998 n'a pas annulé la notification de redressement du 15 septembre 1997 mais s'est bornée à modifier, dans un sens favorable au contribuable, le montant du rehaussement retenu par l'administration ; que, par suite, le décompte de l'intérêt de retard devait être arrêté au dernier jour du mois de la notification de redressement du 15 septembre 1997, soit le 30 septembre 1997 et non le 30 avril 1998 ;<br/>
<br/>
              10. Considérant qu'il résulte de ce qui précède que Mme B...est seulement fondée à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Rouen a rejeté les conclusions de sa demande tendant à la décharge des intérêts de retard afférents à la fraction de la cotisation d'impôt sur le revenu résultant des redressements liés à l'imposition de la plus-value de cession de titres constatée en 1995 en tant que ces conclusions portaient sur la fraction des intérêts de retard réclamés au titre de la période postérieure au 30 septembre 1997 ;<br/>
<br/>
              11. Considérant qu'il n'y a pas lieu, dans les circonstances de l'espèce, de mettre une somme à la charge de l'Etat au titre de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Douai du 20 février 2014 est annulé.<br/>
Article 2 : Mme B...est déchargée des intérêts de retard afférents à la fraction de la cotisation d'impôt sur le revenu résultant des redressements liés à l'imposition de la plus-value de cession de titres constatées en 1995 calculés sur la période postérieure au 30 septembre 1997.<br/>
Article 3 : Le jugement du tribunal administratif de Rouen du 20 décembre 2005 est réformé en ce sens.<br/>
Article 4 : Le surplus de la requête d'appel de Mme B...est rejeté.<br/>
Article 5 : La présente décision sera notifiée au ministre des finances et des comptes publics et à Mme A...B....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
