<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042606101</ID>
<ANCIEN_ID>JG_L_2020_12_000000438698</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/60/61/CETATEXT000042606101.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème chambre, 02/12/2020, 438698, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-12-02</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>438698</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Thomas Pez-Lavergne</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Mireille Le Corre</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2020:438698.20201202</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une ordonnance n° 1906257 du 10 février 2020, enregistrée le 14 février 2020 au secrétariat du contentieux du Conseil d'Etat, la présidente du tribunal administratif de Montpellier a transmis au Conseil d'Etat, en application de l'article R. 351-2 du code de justice administrative, la requête présentée à ce tribunal par M. B... A... enregistrée le 22 novembre 2019 au greffe de ce tribunal. <br/>
<br/>
              Par cette requête, enregistrée le 14 février 2020 au secrétariat du contentieux du Conseil d'Etat, M. A... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler la décision du 27 février 2019 par laquelle la ministre des armées lui a infligé la sanction de blâme du ministre ;<br/>
<br/>
              2°) d'annuler sa notation établie en 2019 pour la période du 26 janvier 2018 au 22 janvier 2019 ;<br/>
<br/>
              3°) de condamner l'Etat à réparer son préjudice.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
<br/>
              Vu : <br/>
              - le code de la défense ;<br/>
              - le code de justice administrative ; <br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Thomas Pez-Lavergne, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Mireille Le Corre, rapporteur public ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. M. B... A..., lieutenant de gendarmerie, s'est vu infliger le 27 février 2019 par la ministre des armées la sanction de blâme du ministre, en raison de son comportement inapproprié à l'égard d'une maréchale des logis au cours de l'année 2018. Par une décision du 3 juillet 2019, le directeur général de la gendarmerie nationale a rejeté le recours hiérarchique qu'il avait formé contre cette sanction. En l'absence de réponse au terme du délai de quatre mois, son recours administratif préalable obligatoire devant la commission des recours militaires a fait l'objet d'une décision implicite de rejet née le 22 septembre 2019. Par une ordonnance du 10 février 2020, la présidente du tribunal administratif de Montpellier a transmis au Conseil d'Etat la requête de M. A... tendant à l'annulation de cette sanction et de sa notation établie en 2019 ainsi qu'à la réparation de son préjudice psychologique.<br/>
<br/>
              En ce qui concerne les conclusions aux fins d'annulation de la sanction disciplinaire :<br/>
<br/>
              2. Aux termes de l'article L. 4137-2 du code de la défense : " Les sanctions disciplinaires applicables aux militaires sont réparties en trois groupes : / 1° Les sanctions du premier groupe sont : / a) L'avertissement ; / b) La consigne ; / c) La réprimande ; / d) Le blâme ; / e) Les arrêts ; / f) Le blâme du ministre (...) ".<br/>
<br/>
              3. Il appartient au juge de l'excès de pouvoir, saisi de moyens en ce sens, de rechercher si les faits reprochés à un agent public ayant fait l'objet d'une sanction disciplinaire constituent des fautes de nature à justifier une sanction et si la sanction retenue est proportionnée à la gravité de ces fautes.<br/>
<br/>
              4. En premier lieu, il ressort des pièces du dossier que le requérant a adopté, à plusieurs reprises, des comportements inappropriés à l'égard du personnel féminin de la gendarmerie, ce dont ses collègues ont été témoins. Par suite, le moyen tiré de ce que la matérialité des faits reprochés à M. A... ne serait pas établie ne peut qu'être écarté.<br/>
<br/>
              5. En second lieu, eu égard aux responsabilités de M. A..., à la gravité et aux conséquences préjudiciables de son comportement sur les conditions de travail de ses subordonnées et alors même que sa manière de servir par ailleurs aurait donné satisfaction, l'autorité investie du pouvoir disciplinaire n'a pas, dans les circonstances de l'espèce et au regard du pouvoir d'appréciation dont elle disposait, pris une sanction disproportionnée en lui infligeant la sanction du blâme du ministre.<br/>
<br/>
              6. Il résulte de ce qui précède que les conclusions de la requête de M. A... tendant à l'annulation de la sanction disciplinaire qui lui a été infligée le 27 février 2019 doivent être rejetées, ainsi que celles, présentées en dehors de toute demande indemnitaire, tendant à ce que le juge se prononce sur l'éventualité d'un droit à indemnisation, qui sont manifestement irrecevables.<br/>
<br/>
              En ce qui concerne les conclusions aux fins d'annulation de sa notation établie en 2019 :<br/>
<br/>
              7. Aux termes de l'article L. 311-1 du code de justice administrative : " Les tribunaux administratifs sont, en premier ressort, juges de droit commun du contentieux administratif, sous réserve des compétences que l'objet du litige ou l'intérêt d'une bonne administration de la justice conduisent à attribuer à une autre juridiction administrative ". Selon l'article R. 311-1 du code de justice administrative : " Le Conseil d'Etat est compétent pour connaître en premier et dernier ressort : / (...) 3° Des litiges concernant le recrutement et la discipline des agents publics nommés par décret du Président de la République en vertu des dispositions de l'article 13 (troisième alinéa) de la Constitution et des articles 1er et 2 de l'ordonnance n° 58-1136 du 28 novembre 1958 portant loi organique concernant les nominations aux emplois civils et militaires de l'Etat ".<br/>
<br/>
              8. Les conclusions de M. A... tendant à l'annulation de sa notation établie en 2019, qui n'est relative ni au recrutement ni à la discipline d'un agent public nommé par décret du Président de la République, ne relèvent ni du 3° de l'article R. 311-1 du code de justice administrative ni d'aucun des autres cas de compétence directe du Conseil d'Etat en premier et dernier ressort énumérés à cet article. Ainsi, il n'appartient pas au Conseil d'Etat de connaître de la demande d'annulation de cette décision.<br/>
<br/>
              9. Le tribunal administratif territorialement compétent pour connaître de la requête de M. A... est, en vertu des dispositions de l'article R. 312-12 du code de justice administrative, celui dans le ressort duquel se trouve le lieu d'affectation du fonctionnaire ou agent que la décision attaquée concerne. La requête doit être attribuée au tribunal administratif de Nîmes, dans le ressort duquel se trouve le lieu d'affectation de M. A....<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement des conclusions de la requête de M. A... dirigées contre sa notation établie en 2019 est attribué au tribunal administratif de Nîmes.<br/>
Article 2 : Le surplus des conclusions de la requête est rejeté.<br/>
Article 3 : La présente décision sera notifiée à M. B... A..., à la ministre des armées et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
