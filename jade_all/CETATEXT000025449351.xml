<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000025449351</ID>
<ANCIEN_ID>JG_L_2012_03_000000346673</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/25/44/93/CETATEXT000025449351.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème et 2ème sous-sections réunies, 01/03/2012, 346673</TITRE>
<DATE_DEC>2012-03-01</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>346673</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème et 2ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MASSE-DESSEN, THOUVENIN ; SCP ROCHETEAU, UZAN-SARANO</AVOCATS>
<RAPPORTEUR>M. Frédéric Dieu</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Bertrand Dacosta</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2012:346673.20120301</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 14 février et 16 mai 2011 au secrétariat du contentieux du Conseil d'Etat, présentés pour la SCI STEMO, dont le siège est 6 chemin de Laval à Tourbes (34120) ; la SCI STEMO demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 08MA02777 du 13 décembre 2010 par lequel la cour administrative d'appel de Marseille a rejeté sa requête tendant à l'annulation du jugement n° 0505326 du 21 mars 2008 par lequel le tribunal administratif de Montpellier a rejeté sa demande tendant à ce que la commune de Pézenas soit condamnée à lui verser la somme de 38 700 euros au titre de la perte de chance d'acquérir un bâtiment à usage d'atelier-relais et celle de 31 706 euros au titre de la clause pénale prévue au contrat de location avec option d'achat conclu avec la commune, sommes assorties des intérêts légaux à compter de sa demande préalable ainsi que de leur capitalisation ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de la commune de Pézenas la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code général des collectivités territoriales ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Frédéric Dieu, Maître des Requêtes,  <br/>
<br/>
              - les observations de la SCP Masse-Dessen, Thouvenin, avocat de la SCI STEMO et de la SCP Rocheteau, Uzan-Sarano, avocat de la commune de Pézenas, <br/>
<br/>
              - les conclusions de M. Bertrand Dacosta, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Masse-Dessen, Thouvenin, avocat de la SCI STEMO et à la SCP Rocheteau, Uzan-Sarano, avocat de la commune de Pézenas ; <br/>
<br/>
<br/>
<br/>Considérant qu'il ressort des pièces du dossier soumis au juge du fond que, par contrat de location avec option d'achat conclu le 20 novembre 1991, la commune de Pézenas a mis à disposition de la SCI STEMO un bâtiment à usage professionnel appartenant à cette collectivité, pour une durée de quinze ans à compter du 1er janvier 1990 ; que, par un courrier daté du 2 mai 1997, le maire a informé cette société de ce qu'il avait décidé de soumettre à la prochaine réunion du conseil municipal une proposition de cession de ce bâtiment pour un montant correspondant au capital restant dû et aux loyers impayés dont le paiement devait être échelonné jusqu'à la fin de l'année 2000 ; que, le maire n'ayant pas soumis au conseil municipal cette proposition, la SCI STEMO a demandé au tribunal administratif de Montpellier de condamner la commune de Pézenas à réparer le préjudice causé par la perte de chance d'acquérir le bâtiment qu'elle occupait ; que, par un jugement du 21 mars 2008, le tribunal administratif de Montpellier a jugé que le maire de la commune n'avait commis aucune faute de nature à engager la responsabilité de la commune et rejeté la demande de la SCI STEMO ; que, par l'arrêt attaqué, la cour administrative d'appel de Marseille, après avoir estimé que le maire avait commis une telle faute, a cependant rejeté la requête de la société ;<br/>
<br/>
              Considérant que, si la décision du maire de Pézenas de soumettre ou non à l'approbation du conseil municipal la vente à la SCI STEMO du bâtiment dont elle était locataire a constitué un acte de disposition, pris indépendamment des stipulations liant la commune à la SCI, et si la faute commise par le maire en ne tenant pas sa promesse de soumettre au conseil municipal ce projet vente était susceptible d'engager la responsabilité extracontractuelle de la commune, la société ne pouvait prétendre qu'à la réparation du préjudice directement causé par cette faute, tel que celui correspondant, le cas échéant, aux dépenses qu'elle avait pu engager sur la foi de cette promesse ; qu'ainsi, en jugeant que les seuls préjudices invoqués, tenant d'une part à la différence constatée entre le prix de vente proposé par le maire dans le courrier du 2 mai 1997 et le prix plus élevé auquel le bâtiment avait été effectivement vendu deux ans plus tard à une autre société et, d'autre part, à l'application de la clause pénale du contrat de location consécutivement à sa résiliation et à l'existence de loyers demeurés impayés par la SCI STEMO, étaient sans lien direct avec le non respect par le maire de sa promesse de soumettre à l'approbation du conseil municipal le projet de lui vendre ce bâtiment, la cour administrative d'appel de Marseille, dont l'arrêt est suffisamment motivé, a exactement qualifié les faits et n'a pas commis d'erreur de droit ; <br/>
<br/>
              Considérant qu'il résulte de ce qui précède que le pourvoi de la SCI STEMO doit être rejeté, y compris ses conclusions présentées au titre des dispositions de l'article <br/>
L. 761-1 du code de justice administrative ; qu'en revanche, il y a lieu, dans les circonstances de l'espèce, de mettre à sa charge le versement à la commune de Pézenas d'une somme de 3 000 euros au même titre ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de la SCI STEMO est rejeté.<br/>
Article 2 : La SCI STEMO versera à la commune de Pézenas une somme de 3 000 euros en application des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée à la SCI STEMO et à la commune de Pézenas.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">17-03-02-02-01-01 COMPÉTENCE. RÉPARTITION DES COMPÉTENCES ENTRE LES DEUX ORDRES DE JURIDICTION. COMPÉTENCE DÉTERMINÉE PAR UN CRITÈRE JURISPRUDENTIEL. DOMAINE. DOMAINE PRIVÉ. ALIÉNATION DU DOMAINE PRIVÉ. - PROMESSE FAITE PAR UNE COLLECTIVITÉ PUBLIQUE À UN COCONTRACTANT PRIVÉ DE LUI ALIÉNER UN IMMEUBLE DE SON DOMAINE PRIVÉ FAISANT L'OBJET D'UN CONTRAT DE LOCATION AVEC OPTION D'ACHAT, EN DEHORS DES PRÉVISIONS DE CE CONTRAT - ACTE DE DISPOSITION - PROMESSE NON TENUE - RECHERCHE DE LA RESPONSABILITÉ QUASI-DÉLICTUELLE DE LA COMMUNE - COMPÉTENCE DE LA JURIDICTION ADMINISTRATIVE (SOL. IMPL.) [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">17-03-02-05-01-01 COMPÉTENCE. RÉPARTITION DES COMPÉTENCES ENTRE LES DEUX ORDRES DE JURIDICTION. COMPÉTENCE DÉTERMINÉE PAR UN CRITÈRE JURISPRUDENTIEL. RESPONSABILITÉ. RESPONSABILITÉ EXTRA-CONTRACTUELLE. COMPÉTENCE ADMINISTRATIVE. - ACTE DE DISPOSITION DU DOMAINE - RESPONSABILITÉ QUASI-DÉLICTUELLE D'UNE COLLECTIVITÉ PUBLIQUE EN RAISON DE SON REFUS D'ALIÉNER AU PROFIT D'UN COCONTRACTANT UN IMMEUBLE DE SON DOMAINE PRIVÉ FAISANT L'OBJET D'UN CONTRAT DE LOCATION AVEC OPTION D'ACHAT, EN DEHORS DES PRÉVISIONS DE CE CONTRAT (SOL. IMPL.) [RJ1].
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">24-02-03-01-03 DOMAINE. DOMAINE PRIVÉ. CONTENTIEUX. COMPÉTENCE DE LA JURIDICTION ADMINISTRATIVE. CONTENTIEUX DE LA RESPONSABILITÉ. - RESPONSABILITÉ QUASI-DÉLICTUELLE D'UNE COLLECTIVITÉ PUBLIQUE EN RAISON DE SON REFUS D'ALIÉNER AU PROFIT D'UN COCONTRACTANT UN IMMEUBLE DE SON DOMAINE PRIVÉ FAISANT L'OBJET D'UN CONTRAT DE LOCATION AVEC OPTION D'ACHAT, EN DEHORS DES PRÉVISIONS DE CE CONTRAT (SOL. IMPL.) [RJ1].
</SCT>
<SCT ID="8D" TYPE="PRINCIPAL">60-04-01-03-01 RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. RÉPARATION. PRÉJUDICE. CARACTÈRE DIRECT DU PRÉJUDICE. ABSENCE. - PROMESSE NON TENUE PAR UNE COLLECTIVITÉ PUBLIQUE D'ALIÉNER UN BIEN DE SON DOMAINE PRIVÉ AU TITULAIRE D'UN CONTRAT DE LOCATION AVEC OPTION D'ACHAT, EN DEHORS DES STIPULATIONS DE CE CONTRAT - PRÉJUDICE INVOQUÉ TENANT À LA PERTE DE CHANCE D'ACQUÉRIR LE BÂTIMENT.
</SCT>
<ANA ID="9A"> 17-03-02-02-01-01 Collectivité publique liée à une personne privée par un contrat de location avec option d'achat portant sur un immeuble de son domaine privé. La promesse faite par la collectivité publique à son cocontractant de lui céder l'immeuble en cause alors même que les conditions mises à l'option d'achat  prévue dans le contrat n'étaient pas remplies constitue un acte de disposition du domaine pris indépendamment des stipulations du contrat. La faute commise par la collectivité qui n'a pas tenu sa promesse est donc susceptible d'engager sa responsabilité extracontractuelle devant le juge administratif.</ANA>
<ANA ID="9B"> 17-03-02-05-01-01 Collectivité publique liée à une personne privée par un contrat de location avec option d'achat portant sur un immeuble de son domaine privé. La promesse faite par la collectivité publique à son cocontractant de lui céder l'immeuble en cause alors même que les conditions mises à l'option d'achat  prévue dans le contrat n'étaient pas remplies constitue un acte de disposition du domaine pris indépendamment des stipulations du contrat. La faute commise par la collectivité qui n'a pas tenu sa promesse est donc susceptible d'engager sa responsabilité extracontractuelle devant le juge administratif.</ANA>
<ANA ID="9C"> 24-02-03-01-03 Collectivité publique liée à une personne privée par un contrat de location avec option d'achat portant sur un immeuble de son domaine privé. La promesse faite par la collectivité publique à son cocontractant de lui céder l'immeuble en cause alors même que les conditions mises à l'option d'achat  prévue dans le contrat n'étaient pas remplies constitue un acte de disposition du domaine pris indépendamment des stipulations du contrat. La faute commise par la collectivité qui n'a pas tenu sa promesse est donc susceptible d'engager sa responsabilité extracontractuelle devant le juge administratif.</ANA>
<ANA ID="9D"> 60-04-01-03-01 Il n'existe pas de lien direct entre, d'une part, la faute consistant, pour une collectivité publique liée à une personne privée par un contrat de location avec option d'achat portant sur un immeuble de son domaine privé, à ne pas tenir la promesse faite à son cocontractant de lui céder l'immeuble en cause alors même que les conditions mises à l'option d'achat prévue dans le contrat n'étaient pas remplies et, d'autre part, les préjudices liés à la différence entre le prix de vente proposé lors de la promesse et celui effectivement constaté lors d'une vente ultérieure ou au versement des sommes correspondant à la clause pénale du contrat de location.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr. TC, 22 novembre 2010, SARL Brasserie du Théâtre c/ Commune de Reims, n° 3764, p. 590.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
