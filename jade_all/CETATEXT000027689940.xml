<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027689940</ID>
<ANCIEN_ID>JG_L_2013_07_000000356911</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/68/99/CETATEXT000027689940.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème et 7ème sous-sections réunies, 10/07/2013, 356911</TITRE>
<DATE_DEC>2013-07-10</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>356911</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème et 7ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ODENT, POULET</AVOCATS>
<RAPPORTEUR>M. Camille Pascal</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Damien Botteghi</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:356911.20130710</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 20 février et 21 mai 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour Mme B...A..., demeurant ...; Mme A...demande au Conseil d'Etat d'annuler l'arrêt n° 11BX00858 du 18 octobre 2011 par lequel la cour administrative d'appel de Bordeaux a rejeté sa requête tendant, d'une part, à l'annulation du jugement n° 1000960 du 30 septembre 2010 par lequel le tribunal administratif de Limoges a rejeté sa demande tendant à l'annulation de l'arrêté du préfet de la Haute-Vienne en date du 1er avril 2010 refusant de lui délivrer un titre de séjour, lui faisant obligation de quitter le territoire français dans le délai d'un mois et fixant le pays de renvoi, ainsi que du rejet de son recours gracieux, d'autre part, à l'annulation de cet arrêté et du rejet de son recours gracieux et à ce qu'il soit enjoint au préfet de lui délivrer un titre de séjour ou, subsidiairement, de statuer à nouveau sur sa situation dans le délai d'un mois ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
<br/>
              Vu le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Camille Pascal, Conseiller d'Etat,<br/>
<br/>
              - les conclusions de M. Damien Botteghi, Rapporteur public,<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Odent, Poulet, avocat de Mme A...;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1.	Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que MmeA..., ressortissante guinéenne, est entrée régulièrement en France le 13 juin 2003 munie d'un visa de court séjour ; qu'elle s'est maintenue irrégulièrement sur le territoire français après l'expiration de ce visa ; qu'elle a déposé une demande de titre de séjour le 27 février 2004 auprès du préfet des Hauts-de-Seine en faisant valoir que son état de santé nécessitait une prise en charge médicale en France ; que cette demande a été rejetée ; qu'elle a présenté une nouvelle demande au même titre au préfet de la Haute-Vienne le 25 octobre 2007 ; que ce préfet lui a délivré une carte de séjour temporaire sur le fondement du 11° de l'article L. 313-11 du code de l'entrée et du séjour des étrangers et du droit d'asile, dont la durée de validité courait du 19 juin au 18 décembre 2008 ; qu'elle a sollicité le renouvellement de ce titre de séjour à l'expiration de sa période de validité, en invoquant son état de santé ainsi que sa situation personnelle et professionnelle en France ; que cette demande a été implicitement rejetée ; que le 22 juin 2009, Mme A...a réitéré sa demande de titre de séjour ; que, par un arrêté du 26 août 2009, le préfet de la Vienne a rejeté sa demande ; que, par un jugement du 11 mars 2010, le tribunal administratif de Limoges a annulé cette décision au motif que le préfet n'avait pas examiné la demande de Mme A...au regard des dispositions de l'article L. 313-10 du code de l'entrée et du séjour des étrangers et du droit d'asile, relatives à la délivrance de la carte de séjour temporaire en qualité de salarié ; que, par un arrêté du 1er avril 2010, le préfet a rejeté la demande de titre de séjour en examinant la situation de l'intéressée au regard de l'article L. 313-10, du 11° de l'article L. 313-11 et de l'article L. 313-14 du même code, en assortissant sa décision d'une obligation de quitter le territoire français dans un délai d'un mois et en fixant le pays de renvoi ; que, par un jugement du 30 septembre 2010, le tribunal administratif de Limoges a rejeté le recours formé par Mme A...contre cet arrêté du 1er avril 2010 ; que, par arrêt du 18 octobre 2011, la cour administrative d'appel de Bordeaux a rejeté l'appel de Mme A...; que Mme A...se pourvoit en cassation contre cet arrêt ;<br/>
<br/>
              2.	Considérant que si, en vertu de l'article L. 311-7 du code de l'entrée et du séjour des étrangers et du droit d'asile, la première délivrance d'une carte de séjour temporaire est, en principe, sous réserve des engagements internationaux de la France et des exceptions prévues par la loi - en particulier pour la délivrance d'une carte de séjour temporaire " vie privée et familiale " sur le fondement du 11° de l'article L. 313-11 -, subordonnés à la production par l'étranger d'un visa d'une durée supérieure à trois mois, il en va différemment pour l'étranger déjà admis à séjourner en France et qui sollicite le renouvellement, même sur un autre fondement, de la carte de séjour temporaire dont il est titulaire ; que, par suite, en jugeant que le préfet de la Haute-Vienne pouvait légalement refuser à Mme A...la délivrance de la carte de séjour temporaire qu'elle sollicitait en qualité de salariée au seul motif qu'elle ne pouvait présenter un visa d'une durée supérieure à trois mois, alors même qu'elle avait séjourné régulièrement sur le territoire national en vertu d'une carte de séjour temporaire délivrée sur le fondement du 11° de l'article L. 313-11, la cour administrative d'appel de Bordeaux a commis une erreur de droit ; que Mme A...est, dès lors, sans qu'il soit besoin d'examiner les autres moyens de son pourvoi, fondée à demander l'annulation de l'arrêt attaqué ; <br/>
<br/>
              3.	Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              4.	Considérant, d'une part, qu'il ressort des pièces du dossier que Mme A...était titulaire d'une carte de séjour temporaire délivrée à raison de son état de santé au titre du 11° de l'article L. 313-11 du code de l'entrée et du séjour des étrangers et du droit d'asile, dont la durée de validité courait du 19 juin au 18 décembre 2008, et dont elle a sollicité le renouvellement avant l'expiration de cette période de validité, en invoquant son état de santé ainsi que sa situation personnelle et professionnelle en France ; qu'en statuant par l'arrêté du 1er avril 2010 sur cette demande de renouvellement, qui tendait notamment à l'octroi d'une carte de séjour temporaire en qualité de salarié, le préfet ne pouvait, ainsi qu'il a été dit ci-dessus, se fonder légalement sur la circonstance que Mme A...ne pouvait présenter un visa d'une durée supérieure à trois mois ; <br/>
<br/>
              5.	Considérant, d'autre part, qu'il ressort des pièces du dossier que, contrairement à ce qu'a également retenu le préfet de la Haute-Vienne dans les motifs de sa décision de refus, le contrat de travail de l'intéressée avait été visé par l'autorité compétente ;<br/>
<br/>
              6.	Considérant qu'il suit de là que Mme A...est fondée à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Limoges a rejeté sa demande tendant à l'annulation de l'arrêté du 1er avril 2010 par lequel le préfet de la Haute-Vienne lui a refusé la délivrance d'un titre de séjour et l'a obligée à quitter le territoire français dans un délai d'un mois ; <br/>
<br/>
              7.	Considérant que l'annulation pour excès de pouvoir de l'arrêté du 1er avril 2010 résultant de la présente décision n'implique pas nécessairement la délivrance du titre de séjour sollicité ; qu'il y a lieu d'enjoindre au préfet de la Haute-Vienne de réexaminer la situation de Mme A...dans un délai de deux mois à compter de la notification de la présente décision, sans qu'il y ait lieu d'assortir cette injonction d'une astreinte ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Bordeaux du 18 octobre 2011 est annulé.<br/>
<br/>
Article 2 : Le jugement du tribunal administratif de Limoges du 30 septembre 2010 est annulé.<br/>
<br/>
Article 3 : L'arrêté du préfet de la Haute-Vienne du 1er avril 2010 est annulé.<br/>
<br/>
Article 4 : Il est enjoint au préfet de la Haute-Vienne de réexaminer la situation de Mme A...dans un délai de deux mois à compter de la notification de la présente décision.<br/>
<br/>
Article 5 : La présente décision sera notifiée à Mme B...A...et au ministre de l'intérieur.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">335-01-02 ÉTRANGERS. SÉJOUR DES ÉTRANGERS. AUTORISATION DE SÉJOUR. - DÉLIVRANCE D'UNE CARTE DE SÉJOUR TEMPORAIRE - CONDITION - PRODUCTION PAR L'ÉTRANGER D'UN VISA D'UNE DURÉE SUPÉRIEURE À TROIS MOIS (ART. L. 311-7 DU CESEDA) - CONDITION UNIQUEMENT OPPOSABLE POUR LA PREMIÈRE DÉLIVRANCE D'UNE TELLE CARTE - EXISTENCE - CONSÉQUENCE - CONDITION INOPPOSABLE À L'ÉTRANGER DEMANDANT UN RENOUVELLEMENT DE CARTE, MÊME SUR UN AUTRE FONDEMENT.
</SCT>
<ANA ID="9A"> 335-01-02 Si, en vertu de l'article L. 311-7 du code de l'entrée et du séjour des étrangers et du droit d'asile (CESEDA), la première délivrance d'une carte de séjour temporaire est, en principe, sous réserve des engagements internationaux de la France et des exceptions prévues par la loi - en particulier pour la délivrance d'une carte de séjour temporaire vie privée et familiale sur le fondement du 11° de l'article L. 313-11, subordonnée à la production par l'étranger d'un visa d'une durée supérieure à trois mois, il en va différemment pour l'étranger déjà admis à séjourner en France et qui sollicite le renouvellement, même sur un autre fondement, de la carte de séjour temporaire dont il est titulaire.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
