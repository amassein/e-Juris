<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036715406</ID>
<ANCIEN_ID>JG_L_2018_03_000000414350</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/71/54/CETATEXT000036715406.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème chambre, 16/03/2018, 414350, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-03-16</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>414350</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MATUCHANSKY, POUPOT, VALDELIEVRE</AVOCATS>
<RAPPORTEUR>M. Didier Ribes</RAPPORTEUR>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CECHS:2018:414350.20180316</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par un mémoire distinct et un nouveau mémoire, enregistrés les 18 décembre 2017 et 23 janvier 2018 au secrétariat du contentieux du Conseil d'État, M. B...A...demande au Conseil d'État, en application de l'article 23-5 de l'ordonnance n° 58-1067 du 7 novembre 1958 et à l'appui de sa requête tendant à l'annulation de la décision du 12 juillet 2017 par laquelle le Conseil supérieur de la magistrature a prononcé à son encontre un blâme avec inscription au dossier, de renvoyer au Conseil constitutionnel la question de la conformité aux droits et libertés garantis par la Constitution de la loi organique n° 94-100 du 5 février 1994 sur le Conseil supérieur de la magistrature.<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
	Vu :<br/>
	-	la Constitution, notamment son Préambule et ses articles 61-1 et 65 ;<br/>
                       -	l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
                       -	l'ordonnance n° 58-1270 du 22 décembre 1958 ;<br/>
                       -	la loi organique n° 94-100 du 5 février 1994 ;<br/>
                       -	la loi organique n° 2010-830 du 22 juillet 2010 ;<br/>
                       -	le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Didier Ribes, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Louis Dutheillet de Lamothe, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Matuchansky, Poupot, Valdelièvre, avocat de M.A.... <br/>
<br/>
              Vu la note en délibéré, enregistrée le 9 mars 2018, présentée par M.A....<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1.	Aux termes du premier alinéa de l'article 23-5 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel : " Le moyen tiré de ce qu'une disposition législative porte atteinte aux droits et libertés garantis par la Constitution peut être soulevé (...) à l'occasion d'une instance devant le Conseil d'Etat (...) ". Il résulte des dispositions de ce même article que le Conseil constitutionnel est saisi de la question prioritaire de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux.<br/>
<br/>
              2.	Aux termes de l'article 65 de la Constitution : " Le Conseil supérieur de la magistrature comprend une formation compétente à l'égard des magistrats du siège et une formation compétente à l'égard des magistrats du parquet. / La formation compétente à l'égard des magistrats du siège est présidée par le premier président de la Cour de cassation. Elle comprend, en outre, cinq magistrats du siège et un magistrat du parquet, un conseiller d'État désigné par le Conseil d'État, un avocat ainsi que six personnalités qualifiées qui n'appartiennent ni au Parlement, ni à l'ordre judiciaire, ni à l'ordre administratif. Le Président de la République, le Président de l'Assemblée nationale et le Président du Sénat désignent chacun deux personnalités qualifiées. La procédure prévue au dernier alinéa de l'article 13 est applicable aux nominations des personnalités qualifiées. Les nominations effectuées par le président de chaque assemblée du Parlement sont soumises au seul avis de la commission permanente compétente de l'assemblée intéressée. / (...) / La formation du Conseil supérieur de la magistrature compétente à l'égard des magistrats du siège statue comme conseil de discipline des magistrats du siège. Elle comprend alors, outre les membres visés au deuxième alinéa, le magistrat du siège appartenant à la formation compétente à l'égard des magistrats du parquet.   /  (...) / La loi organique détermine les conditions d'application du présent article. " ; <br/>
<br/>
              3.	Aux termes de l'article 48 de l'ordonnance du 22 décembre 1958 portant loi organique relative au statut de la magistrature : " Le conseil de discipline des magistrats du siège est composé conformément aux dispositions de l'article 65 de la Constitution et de l'article 14 de la loi organique n° 94-100 du 5 février 1994 sur le Conseil supérieur de la magistrature. ". L'article 14 de la loi organique du 5 février 1994 dispose que : " En cas d'empêchement, le premier président de la Cour de cassation et le procureur général près ladite cour peuvent être suppléés respectivement par le magistrat visé au 1° de l'article 1er et par le magistrat visé au 1° de l article 2. / Pour délibérer valablement lorsqu'elles siègent en matière disciplinaire, la formation compétente à l'égard des magistrats du siège et celle compétente à l'égard des magistrats du parquet comprennent, outre le président de séance, au moins sept de leurs membres. Dans les autres matières, chaque formation du Conseil supérieur délibère valablement si elle comprend, outre le président de séance, au moins huit de ses membres. / Les propositions et avis de chacune des formations du Conseil supérieur sont formulés à la majorité des voix. " <br/>
<br/>
              4.	D'une part, M. A...soutient que la loi organique du 5 février 1994 sur le Conseil supérieur de la magistrature méconnaît les principes d'indépendance et d'impartialité qui découlent de l'article 16 de la Déclaration des droits de l'homme et du citoyen en tant qu'elle ne prévoit pas de règles permettant de garantir la représentation paritaire des membres magistrats et non magistrats du Conseil supérieur de la magistrature statuant comme conseil de discipline des magistrats du siège. Or, il résulte des dispositions citées aux points 2 et 3 que la composition du Conseil supérieur de la magistrature statuant dans cette formation est fixée par l'article 65 de la Constitution et par l'article 14 de cette loi organique. La question prioritaire de constitutionnalité soulevée doit donc être regardée comme portant sur les dispositions de ce dernier article. <br/>
<br/>
              5.	D'autre part, les dispositions initiales de l'article 14 de la loi organique du 5 février 1994 et leur modification par la loi organique du 22 juillet 2010 relative à l'application de l'article 65 de la Constitution ont été déclarées conformes à la Constitution, respectivement par les décisions n° 93-337 DC du 27 janvier 1994 et n° 2010-611 DC du 19 juillet 2010 du Conseil constitutionnel. Dans cette dernière décision, le Conseil constitutionnel a, en outre, déclaré contraire à la Constitution l'article 15 de la loi organique déférée qui imposait que les formations disciplinaires du Conseil supérieur de la magistrature ne puissent siéger que dans une composition comprenant autant de magistrats que de membres n'appartenant pas à l'autorité judiciaire. L'arrêt de la Cour européenne des droits de l'homme Ramos Nuñes de Carvalho e Sà c. Portugal, nos 55391/13, 57728/13 et 74041/13, du 21 juin 2016, au demeurant non définitif, invoqué par le requérant ne constitue pas, en tout état de cause, un changement de circonstances de nature à justifier que la question de la conformité à la Constitution de ces dispositions soit de nouveau soumise au Conseil constitutionnel.<br/>
<br/>
              6.	Ainsi, sans qu'il soit besoin de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité invoquée, le moyen tiré de ce que l'article 14 de la loi organique du 5 février 1994 porte atteinte aux droits et libertés garantis par la Constitution doit être écarté.<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée par M.A....<br/>
Article 2 : La présente décision sera notifiée à M. B...A...et à la garde des sceaux, ministre de la justice.<br/>
Copie en sera adressée au Conseil constitutionnel et au Premier ministre.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
