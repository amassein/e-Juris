<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026230141</ID>
<ANCIEN_ID>JG_L_2012_07_000000361143</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/23/01/CETATEXT000026230141.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 19/07/2012, 361143, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2012-07-19</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>361143</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP WAQUET, FARGE, HAZAN</AVOCATS>
<RAPPORTEUR>M. Edmond Honorat</RAPPORTEUR>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2012:361143.20120719</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 17 juillet 2012 au secrétariat du contentieux du Conseil d'Etat, présentée pour M. Zoran B, incarcéré au ... ; M. B demande au juge des référés du Conseil d'Etat, sur le fondement des dispositions de l'article L. 521-2 du code de justice administrative, d'enjoindre au garde des sceaux, ministre de la justice, de ne pas mettre à exécution le décret du 23 février 2009 par lequel le Premier ministre a accordé son extradition aux autorités suisses ;<br/>
<br/>
<br/>
              il soutient que :<br/>
              - la condition d'urgence est remplie dès lors que l'exécution du décret attaqué est imminente ; <br/>
              - en ayant méconnu les dispositions de l'article 695-21 du code de procédure pénale, le décret litigieux a porté une atteinte grave et manifestement illégale au principe de spécialité de l'extradition ;<br/>
              - le décret est, par suite, entaché d'inexistence ; <br/>
<br/>
<br/>
              Vu le décret attaqué ;<br/>
<br/>
              Vu le mémoire en défense, enregistré le 19 juillet 2012, présenté par le garde des sceaux, ministre de la justice, qui conclut au rejet de la requête ; il soutient que :<br/>
              - la condition d'urgence n'est pas remplie dès lors que le Gouvernement entend suivre la pratique constante selon laquelle un décret d'extradition n'est pas mis à exécution avant que le Conseil d'Etat ait statué lorsqu'il est saisi d'un recours en annulation contre le décret, comme c'est le cas en l'espèce ;<br/>
              - conformément au II de l'article 695-21 du code de procédure pénale, le consentement des autorités néerlandaises a été obtenu préalablement à l'exécution du décret d'extradition ; <br/>
<br/>
              Vu les autres pièces du dossier ; <br/>
<br/>
              Vu la convention européenne d'extradition du 13 décembre 1957 ;<br/>
<br/>
              Vu la convention d'application de l'accord de Schengen du 19 juin 1990 ;<br/>
<br/>
              Vu l'accord entre le Gouvernement de la République française et le Conseil fédéral suisse relatif à la procédure simplifiée d'extradition et complétant la convention européenne d'extradition du 13 décembre 1957, signé à Berne le 10 février 2003 ; <br/>
<br/>
              Vu le code de procédure pénale, notamment ses articles 695-21 et 696-18 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, M. B et, d'autre part, le garde des sceaux, ministre de la justice ; <br/>
<br/>
              Vu le procès-verbal de l'audience publique du 19 juillet 2012 à 14 heures au cours de laquelle ont été entendus : <br/>
<br/>
              - Me  Farge, avocat au Conseil d'Etat et à la Cour de cassation, avocat de M. B ;<br/>
<br/>
              - le représentant de M. B ;<br/>
<br/>
              - les représentants du garde des sceaux, ministre de la justice ;<br/>
<br/>
              et à l'issue de laquelle l'instruction a été close ; <br/>
<br/>
<br/>
<br/>
              1. Considérant que l'article L. 521-2 du code de justice administrative subordonne l'usage par le juge des référés des pouvoirs qu'il lui confère à une situation d'urgence caractérisée, impliquant qu'une mesure visant à sauvegarder une liberté fondamentale soit prise dans les quarante-huit heures ;<br/>
<br/>
              2. Considérant que M. B demande, sur le fondement de ces dispositions, la suspension de l'exécution du décret du 23 février 2009 accordant son extradition aux autorités suisses ; qu'il a formé un recours pour excès de pouvoir contre ce décret le 17 juillet 2012 ; qu'indépendamment de la tardiveté susceptible d'être opposée à ce recours, eu égard au délai de recours d'un mois prévu par le deuxième alinéa de l'article 696-18 du code de procédure et aux conditions de notification du décret, le garde des sceaux, ministre de la justice a indiqué dans son mémoire écrit que, conformément à l'usage constant selon lequel un décret d'extradition n'est pas mis à exécution tant que le délai de recours n'est pas expiré et tant que le Conseil d'Etat, saisi d'un recours, n'a pas statué sur celui-ci, le Gouvernement ne procédera pas à l'exécution du décret contesté avant que le Conseil d'Etat ait statué sur le recours introduit par M. B ; que cet engagement a été confirmé au cours de l'audience publique par le représentant du garde des sceaux, ministre de la justice ; qu'il a été indiqué que des instructions avaient d'ores et déjà été données afin qu'aucune mesure d'exécution ne soit mise en oeuvre et que les autorités suisses en avaient été averties ; qu'enfin, la procédure de protection particulière de l'article L. 521-2 du code de justice administrative, alors même qu'elle est susceptible de recevoir application indépendamment de tout recours contre une décision, ne saurait, en principe, être utilement invoquée dans une situation où l'intéressé s'est abstenu de déférer au juge de la légalité dans le délai requis une décision lui faisant grief et se borne à contester les mesures qu'implique son exécution, à moins que ces mesures ne comportent des effets excédant, en raison de changements dans les circonstances de droit ou de fait, le cadre normal de cette exécution, ce qui n'est pas le cas en l'espèce ; que, dans ces conditions, il n'existe pas une situation d'urgence caractérisée justifiant l'intervention du juge des référés dans le délai de quarante-huit heures ;<br/>
<br/>
              3. Considérant qu'il suit de là que la requête de M. B doit être rejetée ;<br/>
<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de M. B est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à M. Zoran B et à la garde des sceaux, ministre de la justice.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
