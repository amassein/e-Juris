<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036777275</ID>
<ANCIEN_ID>JG_L_2018_04_000000411526</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/77/72/CETATEXT000036777275.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème chambre, 06/04/2018, 411526, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-04-06</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>411526</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FABIANI, LUC-THALER, PINATEL</AVOCATS>
<RAPPORTEUR>M. Grégory Rzepski</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Olivier Henrard</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2018:411526.20180406</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme B...A...a demandé au tribunal administratif de Grenoble, d'une part, d'annuler l'arrêté préfectoral du 19 décembre 2016 aux termes duquel le préfet de la Haute-Savoie lui a refusé la délivrance d'un titre de séjour et l'a obligée à quitter le territoire français dans le délai de trente jours et, d'autre part, d'enjoindre au préfet de la Haute-Savoie de lui délivrer une carte de séjour " vie privée et familiale " sur le fondement du 11° de l'article L. 313-11 du code de l'entrée et du séjour des étrangers et du droit d'asile dans un délai de quinze jours à compter de la notification de l'arrêt à intervenir et sous astreinte de 150 euros par jour de retard, ou, à titre subsidiaire, de réexaminer sa situation dans le même délai et, dans l'attente, de lui délivrer une autorisation provisoire de séjour dans un délai de huit jours. Par un jugement n° 1700371 du 21 février 2017, le tribunal administratif de Grenoble a, d'une part, annulé la décision d'éloignement contestée et enjoint au préfet de la Haute-Savoie de délivrer à Mme A...une autorisation provisoire de séjour et, d'autre part, renvoyé en formation collégiale les conclusions dirigées contre les décisions portant refus de titre de séjour.<br/>
<br/>
              Par une ordonnance n° 17LY01148 du 13 avril 2017, le président de la 2ème chambre de la cour administrative d'appel de Lyon a rejeté l'appel formé par le préfet de la Haute-Savoie contre ce jugement.<br/>
<br/>
              Par un pourvoi, enregistré le 15 juin 2017 au secrétariat du contentieux du Conseil d'Etat, le ministre de l'intérieur demande au Conseil d'Etat :<br/>
<br/>
              1°) à titre principal, d'annuler cette ordonnance, et, réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              2°) à titre subsidiaire, de procéder à un règlement de juges afin de régler la contrariété entre les jugements rendus par le tribunal administratif sur l'obligation de quitter le territoire et le refus de titre de séjour et l'ordonnance de la cour administrative d'appel.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Grégory Rzepski, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Olivier Henrard, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Fabiani, Luc-Thaler, Pinatel, avocat de MmeA....<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article L. 313-11 du code de l'entrée et du séjour des étrangers et du droit d'asile, dans sa rédaction alors applicable : " Sauf si sa présence constitue une menace pour l'ordre public, la carte de séjour temporaire portant la mention "vie privée et familiale" est délivrée de plein droit : (...) 11° A l'étranger résidant habituellement en France dont l'état de santé nécessite une prise en charge médicale dont le défaut pourrait entraîner pour lui des conséquences d'une exceptionnelle gravité, sous réserve de l'absence d'un traitement approprié dans le pays dont il est originaire, sauf circonstance humanitaire exceptionnelle appréciée par l'autorité administrative après avis du directeur général de l'agence régionale de santé, sans que la condition prévue à l'article L. 311-7 soit exigée. La décision de délivrer la carte de séjour est prise par l'autorité administrative, après avis du médecin de l'agence régionale de santé de la région de résidence de l'intéressé, désigné par le directeur général de l'agence (...) " ;<br/>
<br/>
              2. Considérant que MmeA..., ressortissante pakistanaise, est entrée régulièrement en France le 23 mai 2014 sous couvert d'un visa touristique ; qu'elle s'est maintenue sur le territoire français au-delà de la validité de son visa ; qu'elle a ensuite obtenu deux autorisations provisoires de séjour au regard de son état de santé ; qu'elle a formé une demande de carte de séjour temporaire, déposée le 11 mars 2016, sur le fondement des dispositions du 11° de l'article L. 313-11 du code de l'entrée et du séjour des étrangers et du droit d'asile ; que le préfet de la Haute-Savoie, par un arrêté en date du 19 décembre 2016, a rejeté la demande de l'intéressée et lui a fait obligation de quitter le territoire français dans le délai de trente jours ; que, par un jugement du 21 février 2017, le magistrat désigné par le président du tribunal administratif de Grenoble, a annulé la décision obligeant Mme A...à quitter le territoire français et a renvoyé en formation collégiale les conclusions dirigées contre le refus de titre de séjour ; que, par une ordonnance en date du 13 avril 2017, contre laquelle le ministre de l'intérieur se pourvoit en cassation, le président de la 2ème chambre de la cour administrative d'appel de Lyon a rejeté l'appel du préfet comme manifestement dépourvu de fondement, en application de l'article R. 222-1 du code de justice administrative ;<br/>
<br/>
              3. Considérant que pour rejeter la requête du préfet, la cour administrative d'appel de Lyon a estimé que si Mme A...pouvait être regardée comme guérie de l'hépatite chronique dont elle avait été affectée, elle demeurait atteinte d'une " cirrhose sur hépatite C " du foie, maladie nécessitant un traitement dont il ne ressortait ni des pièces du dossier, ni des pièces produites par le préfet, qu'il serait disponible au Pakistan ; <br/>
<br/>
              4. Considérant, toutefois, qu'il ressort des pièces produites devant les juges du fond par le préfet de la Haute-Savoie, notamment du courriel du conseiller santé auprès du directeur général des étrangers en France portant sur la situation précise de Mme A...et des fiches médicales jointes, que le traitement ainsi que la surveillance semestrielle dont a besoin Mme A...pour prévenir tout risque de graves complications peuvent être assurés au Pakistan ; qu'il suit de là que le préfet de la Haute-Savoie est fondé à soutenir que la cour administrative d'appel de Lyon a dénaturé les pièces du dossier ; que, dès lors, et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, son ordonnance doit être annulée ;<br/>
<br/>
              5. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              6. Considérant, comme il a été dit au point 4, qu'il ressort des pièces du dossier, notamment du courriel du conseiller santé auprès du directeur général des étrangers en France portant sur la situation précise de Mme A...et des fiches médicales jointes, que le traitement ainsi que la surveillance semestrielle dont a besoin Mme A...pour prévenir tout risque de graves complications peuvent être assurés au Pakistan ; qu'ainsi, c'est à tort que le tribunal administratif de Grenoble, pour annuler la décision d'éloignement et enjoindre au préfet de délivrer à Mme A...une autorisation provisoire de séjour, s'est fondé sur le motif que les pièces présentées par le préfet, tout comme son argumentation en défense, ne concernaient que l'hépatite, guérie, mais nullement la cirrhose, persistante et que, dans ces conditions, le préfet ne contredisait pas utilement l'avis du médecin de l'agence régionale de santé selon lequel l'état de santé de l'intéressée ne pouvait être pris en charge dans son pays d'origine ;<br/>
<br/>
              7. Considérant, toutefois, qu'il appartient au Conseil d'Etat, saisi de l'ensemble du litige par l'effet dévolutif de l'appel, d'examiner les autres moyens soulevés par Mme A...devant le tribunal administratif de Grenoble ;<br/>
<br/>
              8. Considérant qu'aux termes de l'article 3 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales : " Nul ne peut être soumis à la torture ni à des peines ou traitements inhumains ou dégradants " ; qu'aux termes de l'article 8 de cette convention : " 1. Toute personne a droit au respect de sa vie privée et familiale, de son domicile et de sa correspondance. 2. Il ne peut y avoir ingérence d'une autorité publique dans l'exercice de ce droit que pour autant que cette ingérence est prévue par la loi et qu'elle constitue une mesure qui, dans une société démocratique, est nécessaire à la sécurité nationale, à la sûreté publique, au bien-être économique du pays, à la défense de l'ordre et à la prévention des infractions pénales, à la protection de la santé ou de la morale, ou à la protection des droits et libertés d'autrui " ;<br/>
<br/>
              9. Considérant que si Mme A...fait valoir que l'ensemble de sa famille réside en France et en Europe, à savoir son fils et les trois enfants de celui-ci, son frère et la famille de celui-ci et que son époux est décédé en 2001, elle n'apporte pas suffisamment d'éléments, eu égard aux contestations du préfet de la Haute-Savoie sur ce point, de nature à la faire regarder comme isolée en cas de retour dans son pays d'origine ; que, compte tenu de l'ensemble des circonstances de l'espèce, la décision du préfet de la Haute-Savoie du 19 décembre 2016 n'a pas porté au droit de l'intéressée au respect de sa vie privée et familiale une atteinte disproportionnée aux buts en vue desquels elle a été prise ; qu'elle n'a, par suite, pas méconnu les stipulations de l'article 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
<br/>
              10. Considérant qu'il ne ressort pas des pièces du dossier que le retour de Mme A... au Pakistan l'exposera à un traitement inhumain ou dégradant ; que le traitement ainsi que la surveillance semestrielle dont elle a besoin pour prévenir tout risque de graves complications peuvent être assurés au Pakistan ; qu'il suit de là que la décision du préfet de la Haute-Savoie du 19 décembre 2016 n'a pas méconnu les stipulations de l'article 3 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
<br/>
              11. Considérant qu'il résulte de tout ce qui précède que le préfet de la Haute-Savoie est fondé à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Grenoble a annulé la décision obligeant Mme A...à quitter le territoire français dans le délai de trente jours et lui a enjoint de délivrer à l'intéressée une autorisation provisoire de séjour ;<br/>
<br/>
              12. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'ordonnance du 13 avril 2017 du président de la 2ème chambre de la cour administrative d'appel de Lyon et le jugement du 21 février 2017 du tribunal administratif de Grenoble sont annulés.<br/>
Article 2 : La demande présentée par Mme A...devant le tribunal administratif de Grenoble est rejetée.<br/>
Article 3 : Les conclusions de Mme A...tendant à l'application de l'article 37 de la loi du 10 juillet 1991 et de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée au ministre d'Etat, ministre de l'intérieur et à Mme B...A....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
