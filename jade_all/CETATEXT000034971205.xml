<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000034971205</ID>
<ANCIEN_ID>JG_L_2017_06_000000404497</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/34/97/12/CETATEXT000034971205.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème chambre, 19/06/2017, 404497, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-06-19</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>404497</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Paul-François Schira</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Edouard Crépey</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2017:404497.20170619</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A...B...a demandé au tribunal administratif de Mayotte d'annuler la décision implicite du préfet de Mayotte lui refusant le bénéfice de la majoration de traitement instituée par le décret n° 2013-964 du 28 octobre 2013 et d'enjoindre au préfet de Mayotte de lui accorder le bénéfice de la majoration de traitement sollicitée.<br/>
<br/>
              Par une ordonnance n° 1500498 du 29 janvier 2016, le président de la 2ème chambre du tribunal administratif de Mayotte a fait droit à sa demande.<br/>
<br/>
              Par une ordonnance n° 16BX01459 du 23 septembre 2016, enregistrée au secrétariat du contentieux du Conseil d'Etat le 28 septembre 2016, la présidente de la cour administrative d'appel de Bordeaux a transmis au Conseil d'Etat, en application de l'article R. 351-2 du code de justice administrative, le pourvoi enregistré le 5 avril 2016 au greffe de cette cour par lequel le ministre de l'intérieur demande l'annulation de cette ordonnance en tant qu'elle a enjoint à l'administration, sur le fondement de l'article L. 911-1 du code de justice administrative, d'accorder au requérant le bénéfice de la majoration de traitement jusqu'à la fin de son affectation à Mayotte.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
<br/>
              - le décret n° 78-1159 du 12 décembre 1978 ;<br/>
              - le décret n° 2013-964 du 28 octobre 2013 ;<br/>
              - le décret n° 2013-965 du 28 octobre 2013 ;<br/>
              - le décret n° 2015-804 du 1er juillet 2015 ;<br/>
              - le code de justice administrative, en particulier l'article R. 811-1 dans sa rédaction applicable au présent litige ; <br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Paul-François Schira, auditeur,  <br/>
<br/>
              - les conclusions de M. Edouard Crépey, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que M.B..., brigadier-chef de police, a demandé l'annulation de la décision implicite par laquelle le préfet de Mayotte a rejeté sa demande tendant à bénéficier, à compter du 1er mai 2015, de la majoration de traitement instituée par le décret n° 2013-964 du 28 octobre 2013 portant création d'une majoration du traitement allouée aux fonctionnaires de l'Etat et de la fonction publique hospitalière et aux magistrats en service dans le département de Mayotte. Le pourvoi du ministre doit être regardé comme dirigé contre l'ordonnance du 29 janvier 2016 qui a fait droit à cette demande en tant que le président de la 2ème chambre du tribunal administratif a par ailleurs enjoint à l'administration, sur le fondement de l'article L. 911-1 du code de justice administrative, d'accorder au requérant le bénéfice de la majoration de traitement jusqu'à la fin de son affectation à Mayotte.<br/>
<br/>
              2. Aux termes du I de l'article 2 du décret du 1er juillet 2015 portant attribution de l'indemnité de sujétion géographique aux fonctionnaires actifs des services de la police nationale en service à Mayotte : " I.- A titre transitoire, les fonctionnaires actifs des services de la police nationale, dont le centre des intérêts matériels et moraux n'est pas à Mayotte, affectés à Mayotte avant l'entrée en vigueur du présent décret conservent, pour les fractions dues et non échues, le bénéfice de l'indemnité spéciale d'éloignement prévu par le décret du 12 décembre 1978 susvisé dans sa version applicable avant l'entrée en vigueur du présent décret. / Ils ne bénéficient pas de la majoration de traitement prévue par le décret n° 2013-964 du 28 octobre 2013 susvisé, au titre des années civiles ouvrant droit au bénéfice de l'indemnité spéciale d'éloignement ni de l'indemnité de sujétion géographique prévue par le présent décret (...) ". Aux termes de son article 5 : " Le présent décret entre en vigueur à compter du 1er septembre 2015. ". Il résulte de ces dispositions que si le cumul, d'une part, de l'indemnité spéciale d'éloignement prévue par le décret du 12 décembre 1978 fixant le régime de rémunération des magistrats et des fonctionnaires de l'Etat à Mayotte et, d'autre part, de la majoration de traitement prévue par le décret du 1er juillet 2015, est ouvert aux fonctionnaires actifs des services de la police nationale, dont le centre des intérêts matériels et moraux n'est pas à Mayotte, affectés à Mayotte avant le 1er septembre 2015, ils ne peuvent en bénéficier que jusqu'à l'entrée en vigueur du décret du 1er juillet 2015 qui a étendu, par le deuxième alinéa du I de son article 2, la règle de non-cumul posée par l'article 8 du décret n° 2013-965 du 28 octobre 2013 portant application de l'indemnité de sujétion géographique aux fonctionnaires de l'Etat titulaires et stagiaires et aux magistrats affectés à Mayotte.<br/>
<br/>
              3. Il suit de là que c''est sans erreur de droit que le tribunal administratif de Mayotte a jugé qu'était illégal le refus d'accorder au requérant le bénéfice de la majoration de traitement tant que n'était pas entré en vigueur le décret du 1er juillet 2015 et l'a annulé pour ce motif. La mesure qu'implique nécessairement cette annulation est l'octroi au requérant du bénéfice de la majoration de traitement jusqu'à la fin de son affectation et, au plus tard, jusqu'à la date d'entrée en vigueur du décret  du 1er juillet 2015, soit le 1er septembre 2015. Il s'en suit qu'en enjoignant à l'administration d'accorder au requérant le bénéfice de la majoration de traitement instituée par le décret n° 2013-964 du 28 octobre 2013 portant création d'une majoration du traitement allouée aux fonctionnaires de l'Etat et de la fonction publique hospitalière et aux magistrats en service dans le département de Mayotte à compter du 1er mai 2015 et jusqu'à la fin de son affectation à Mayotte sans en avoir limité la durée, au plus tard, à la date d'entrée en vigueur du décret du 1er juillet 2015, le tribunal administratif de Mayotte, qui n'a pas statué au-delà des conclusions dont il était saisi en fixant un terme à l'injonction qu'il a prononcée, a entaché son ordonnance d'une erreur de droit. Il résulte de ce qui précède que le ministre de l'intérieur est fondé à en demander, dans cette mesure, l'annulation.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : L'ordonnance du tribunal administratif de Mayotte du 29 janvier 2016 est annulée en tant qu'elle enjoint à l'administration d'accorder au requérant le bénéfice de la majoration de traitement au-delà du 1er septembre 2015.<br/>
<br/>
Article 2 : La présente décision sera notifiée au ministre d'Etat, ministre de l'intérieur et à M. A... B....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
