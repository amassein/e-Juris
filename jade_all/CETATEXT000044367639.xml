<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044367639</ID>
<ANCIEN_ID>JG_L_2021_10_000000452881</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/36/76/CETATEXT000044367639.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème chambre, 20/10/2021, 452881, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-10-20</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>452881</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP RICHARD</AVOCATS>
<RAPPORTEUR>M. Edouard Solier</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Dieu</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:452881.20211020</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu les procédures suivantes :<br/>
<br/>
              La caisse primaire d'assurance maladie de Paris a porté plainte contre M. B... C... devant la chambre disciplinaire de première instance d'Île-de-France de l'ordre des chirurgiens-dentistes. Par une décision du 5 septembre 2018, la chambre disciplinaire de première instance a infligé à M. C... la sanction de l'interdiction d'exercer la profession de chirurgien-dentiste pendant une durée de trois ans, dont un an assorti du sursis.<br/>
<br/>
              Par une décision du 26 mars 2021, la chambre disciplinaire nationale de l'ordre des chirurgiens-dentistes a, sur appel de M. C..., fixé à deux ans dont un an assorti du sursis la sanction d'interdiction d'exercer la profession de chirurgien-dentiste qui lui avait été infligée en première instance.<br/>
<br/>
              1° Sous le numéro 452881, par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 21 mai et 23 août 2021, au secrétariat du contentieux du Conseil d'Etat, M. C... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette décision ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de la caisse primaire d'assurance maladie de Paris la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              2° Sous le numéro 453028, par une requête et un mémoire complémentaire, enregistrés les 27 mai et 23 août 2021 au secrétariat du contentieux du Conseil d'Etat, M. C... demande au Conseil d'Etat d'ordonner qu'il soit sursis à l'exécution de la même décision de la chambre disciplinaire nationale de l'ordre des chirurgiens-dentistes.<br/>
<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu : <br/>
              - le code de la santé publique ;<br/>
              - le code de la sécurité sociale ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Edouard Solier, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Frédéric Dieu, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Richard, avocat de M. C... ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
<br/>
              1. Le pourvoi par lequel M. C... demande l'annulation de la décision de la chambre disciplinaire nationale de l'ordre des chirurgiens-dentistes du 26 mars 2021 et sa requête tendant à ce qu'il soit sursis à l'exécution de cette décision présentent à juger les mêmes questions. Il y a lieu de les joindre pour statuer par une seule décision.<br/>
<br/>
              2. Aux termes de l'article L. 822-1 du code de justice administrative : " Le pourvoi en cassation devant le Conseil d'Etat fait l'objet d'une procédure préalable d'admission. L'admission est refusée par décision juridictionnelle si le pourvoi est irrecevable ou n'est fondé sur aucun moyen sérieux ".<br/>
<br/>
              3. Pour demander l'annulation de la décision de la chambre disciplinaire nationale de l'ordre des chirurgiens-dentistes qu'il attaque, M. C... soutient qu'elle est entachée :<br/>
<br/>
              - d'erreur de droit et d'insuffisance de motivation en ce qu'elle juge que les moyens tirés des irrégularités entachant la phase d'analyse préalable à la saisine de la chambre disciplinaire de première instance ne peuvent qu'être écartés ;  <br/>
              - de dénaturation des pièces du dossier en ce qu'elle juge qu'est établi le grief tiré de la facturation d'actes non réalisés ;<br/>
              - d'insuffisance de motivation en ce qu'elle s'abstient de répondre à l'argumentation par laquelle il faisait valoir que s'agissant du dossier n°10, les pièces qu'il avait produites, à savoir l'attestation du patient concerné et l'extrait de son dossier médical, étaient de nature à établir la réalisation des deux actes qu'il avait facturés ;<br/>
              - d'insuffisance de motivation en ce qu'elle retient le grief tiré de la facturation d'actes non-remboursables, sans préciser la nature des actes en cause ;<br/>
              - d'insuffisance de motivation en ce qu'elle retient le grief tiré de la surcotation de certains actes, sans préciser la cotation retenue, ni celle qui devait s'appliquer aux actes en cause ;<br/>
              - d'insuffisance de motivation en ce qu'elle lui reproche d'avoir facturé à deux ou plusieurs reprises le même acte sans se prononcer sur les relevés bancaires et les bordereaux de tiers-payant qu'il a produits et qui étaient de nature à établir l'absence du grief retenu à son encontre ;<br/>
              - d'erreur de droit et d'insuffisance de motivation en ce qu'elle juge établi le grief tiré de la réalisation d'actes non conformes aux données acquises de la science ; <br/>
              - d'insuffisance de motivation en ce qu'elle s'abstient de répondre à l'argumentation par laquelle il faisait valoir que s'agissant du grief tiré de la réalisation d'actes non conformes aux données acquises de la science retenu à son encontre dans le dossier n° 26, les radiographies qu'il avait produites prouvaient qu'il avait réalisé un traitement complet sur la dent du patient concerné ;<br/>
              - d'insuffisance de motivation en ce qu'elle s'abstient de répondre à l'argumentation par laquelle il faisait valoir que s'agissant du grief tiré de la réalisation d'actes au-delà des besoins en soins du patient retenu à son encontre dans les dossiers nos 27 et 84, la fragilité des dentitions des patients concernés justifiait la réalisation des soins qu'il avait pratiqués.<br/>
<br/>
              Il soutient, en outre, qu'elle lui inflige une sanction hors de proportion avec la gravité des fautes retenues.<br/>
<br/>
              4. Aucun de ces moyens n'est de nature à permettre l'admission du pourvoi.<br/>
<br/>
              5. Le pourvoi de M. C... n'étant pas admis, les conclusions de sa requête tendant à ce qu'il soit sursis à l'exécution de la décision attaquée sont devenues sans objet. Il n'y a donc pas lieu d'y statuer.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
Article 1er : Le pourvoi de M. C... n'est pas admis.<br/>
Article 2 : Il n'y a pas lieu de statuer sur les conclusions de la requête de M. C... tendant à ce qu'il soit sursis à l'exécution de la décision de la chambre disciplinaire nationale de l'ordre des chirurgiens-dentistes du 26 mars 2021.<br/>
Article 3 : La présente décision sera notifiée à M. B... C... et à la caisse primaire d'assurance maladie de Paris.<br/>
Copie en sera adressée au Conseil national de l'ordre des chirurgiens-dentistes.<br/>
              Délibéré à l'issue de la séance du 23 septembre 2021 où siégeaient : Mme Maud Vialettes, président de chambre, présidant ; Mme Carine Soulay, conseillère d'Etat et M. Edouard Solier, maître des requêtes-rapporteur. <br/>
<br/>
<br/>
              Rendu le 20 octobre 2021.<br/>
<br/>
<br/>
                 La présidente : <br/>
                 Signé : Mme Maud Vialettes<br/>
<br/>
<br/>
 		Le rapporteur : <br/>
      Signé : M. Edouard Solier<br/>
<br/>
                 La secrétaire :<br/>
                 Signé : Mme A... D...<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
