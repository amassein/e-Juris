<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029291702</ID>
<ANCIEN_ID>JG_L_2014_07_000000375829</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/29/17/CETATEXT000029291702.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème / 4ème SSR, 23/07/2014, 375829, Publié au recueil Lebon</TITRE>
<DATE_DEC>2014-07-23</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>375829</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème / 4ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP SPINOSI, SUREAU ; SCP ROGER, SEVAUX, MATHONNET</AVOCATS>
<RAPPORTEUR>M. Jean-Dominique Langlais</RAPPORTEUR>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CESSR:2014:375829.20140723</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu le mémoire, enregistré le 26 mai 2014 au secrétariat du contentieux du Conseil d'Etat, présenté pour Mme B...A...en application de l'article 23-5 de l'ordonnance n° 58-1067 du 7 novembre 1958 ; MmeA... demande au Conseil d'Etat, à l'appui de son pourvoi tendant à l'annulation de l'arrêt n°12MA02470 du 17 octobre 2013 de la cour administrative d'appel de Marseille, de renvoyer au Conseil constitutionnel la question de la conformité aux droits et libertés garantis par la Constitution des dispositions de l'article L. 1142-28 du code de la santé publique, en tant qu'elles excluraient les victimes d'une contamination transfusionnelle du bénéfice de la prescription décennale qu'elles instituent ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la Constitution, notamment son Préambule et son article 61-1 ;<br/>
<br/>
              Vu l'ordonnance n° 58-1067 du 7 novembre 1958 ; <br/>
<br/>
              Vu le code de la santé publique et notamment son article L. 1142-28 ;<br/>
<br/>
              Vu la loi n° 68-1250 du 31 décembre 1968 ; <br/>
<br/>
              Vu la loi n° 2002-303 du 4 mars 2002 ;<br/>
<br/>
              Vu la loi n° 2002-1577 du 30 décembre 2002 ;<br/>
<br/>
              Vu la loi n° 2004-806 du 9 août 2004 ;<br/>
<br/>
              Vu la loi n° 2008-1330 du 17 décembre 2008 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Dominique Langlais, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Fabienne Lambolez, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Spinosi, Sureau, avocat de Mme A...et à la SCP Roger, Sevaux, Mathonnet, avocat de l'Office national d'indemnisation des accidents médicaux, des affections iatrogènes et des infections nosocomiales ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'aux termes du premier alinéa de l'article 23-5 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel : "  Le moyen tiré de ce qu'une disposition législative porte atteinte aux droits et libertés garantis par la Constitution peut être soulevé, y compris pour la première fois en cassation, à l'occasion d'une instance devant le Conseil d'Etat (...) " ; qu'il résulte des dispositions de ce même article que le Conseil constitutionnel est saisi de la question prioritaire de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article L. 1142-28 du code de la santé publique : " Les actions tendant à mettre en cause la responsabilité des professionnels de santé ou des établissements de santé publics ou privés à l'occasion d'actes de prévention, de diagnostic ou de soins se prescrivent par dix ans à compter de la consolidation du dommage " ; <br/>
<br/>
              Sur le champ d'application de ces dispositions législatives :<br/>
<br/>
              3. Considérant qu'il résulte des termes mêmes de l'article L. 1142-28 du code de la santé publique, issu de la loi du 4 mars 2002 relative aux droits des malades et à la qualité du système de santé, que la prescription décennale qu'il institue s'applique aux actions en responsabilité tendant à la réparation de dommages résultant d'actes de prévention, de diagnostic ou de soins et dirigées contre des professionnels de santé ou des établissements de santé publics ou privés ; qu'il résulte par ailleurs de l'ensemble des dispositions du chapitre II du titre IV du livre Ier de la première partie du code de la santé publique, issu de la même loi et complété par la loi du 30 décembre 2002 relative à la responsabilité civile médicale, que le législateur a entendu soumettre également à la prescription décennale les actions engagées contre l'Office national d'indemnisation des accidents médicaux, des affections iatrogènes et des infections nosocomiales (ONIAM), sur le fondement du II de l'article L. 1142-1 ou sur le fondement de l'article L. 1142-1-1 du code de la santé publique, afin d'obtenir la réparation des conséquences anormales d'un acte médical ou des préjudices d'une particulière gravité résultant d'une infection nosocomiale, l'office étant appelé, dans le cadre de ces dispositions, à indemniser en lieu et place d'un professionnel ou d'un établissement de santé la victime d'un dommage que celui-ci a causé dans l'accomplissement d'actes de prévention, de diagnostic ou de soins ; <br/>
<br/>
              4. Considérant, en revanche, que le législateur n'a pas entendu rendre la prescription décennale applicable aux actions en responsabilité tendant à la réparation de dommages liés à des actes médicaux mais dirigées contre des personnes autres que des professionnels ou des établissements de santé ; qu'en particulier, cette prescription n'a pas été rendue applicable aux actions en responsabilité dirigées contre l'Etat, sur le fondement de l'article L. 3111-9 du code de la santé publique, au titre des dommages imputables aux vaccinations obligatoires, qui sont demeurées soumises à la prescription quadriennale prévue par la loi du 31 décembre 1968 relative à la prescription des créances sur l'Etat, les départements, les communes et les établissements publics ; que si, dans sa rédaction issue de la loi du 9 août 2004 relative à la politique de santé publique, l'article L. 3111-9 prévoit que les victimes de tels dommages sont indemnisées par l'ONIAM au titre de la solidarité nationale, la prescription quadriennale demeure applicable aux actions fondées sur cet article ; <br/>
<br/>
              5. Considérant, de même, que la loi du 4 mars 2002 n'a pas rendu la prescription décennale applicable aux actions par lesquelles les victimes de contaminations d'origine transfusionnelle recherchaient la responsabilité du centre de transfusion sanguine ayant élaboré les produits sanguins transfusés, sous réserve du cas où ce centre n'aurait pas eu une personnalité morale distincte de celle d'un établissement de santé ; que si l'article L. 1221-14 du code de la santé publique, issu de la loi du 17 décembre 2008 de financement de la sécurité sociale pour 2009, et l'article L. 3122-1 du même code, dans sa rédaction résultant de la même loi, confient à l'ONIAM l'indemnisation des personnes contaminées par certains agents pathogènes à l'occasion de transfusions de produits sanguins ou d'injections de médicaments dérivés du sang, les actions fondées sur ces dispositions ne peuvent être regardées comme entrant dans le champ de la prescription décennale dès lors que l'ONIAM n'est pas appelé à assurer une réparation en lieu et place du professionnel ou de l'établissement de santé qui a procédé à l'administration des produits sanguins, la responsabilité de ce professionnel ou de cet établissement n'étant pas normalement engagée en pareil cas ; que, dès lors que l'ONIAM est un établissement public doté d'un comptable public, ces actions sont soumises à la prescription quadriennale ;<br/>
<br/>
              Sur la question prioritaire de constitutionnalité :<br/>
<br/>
              6. Considérant que, pour demander au Conseil d'Etat de renvoyer au Conseil constitutionnel la question de la conformité aux droits et libertés garantis par la Constitution de l'article L. 1142-28 du code de la santé publique, dont elle a demandé le bénéfice devant les juges du fond, Mme A...soutient que ces dispositions méconnaissent le principe constitutionnel d'égalité devant la loi, faute d'inclure dans le champ d'application de la prescription décennale les actions engagées contre l'ONIAM sur le fondement de l'article L. 1221-14 du même code en vue d'obtenir l'indemnisation au titre de la solidarité nationale des préjudices résultant d'une contamination par le virus de l'hépatite B ou C ou le virus T-lymphotropique humain causée par l'administration de produits sanguins ; <br/>
<br/>
              7. Considérant que l'article 6 de la Déclaration des droits de l'homme et du citoyen de 1789 dispose que la loi " doit être la même pour tous, soit qu'elle protège, soit qu'elle punisse " ; que le principe d'égalité ne s'oppose ni à ce que le législateur règle de façon différente des situations différentes ni à ce qu'il déroge à l'égalité pour des raisons d'intérêt général, pourvu que, dans l'un et l'autre cas, la différence de traitement qui en résulte soit en rapport direct avec l'objet de la loi qui l'établit ;<br/>
<br/>
              8. Considérant qu'en instituant, par la loi du 4 mars 2002, la prescription décennale prévue à l'article L. 1142-28 du code de la santé publique, le législateur a entendu soumettre à un même délai de prescription les actions tendant à la réparation par les professionnels et établissements de santé des dommages qu'ils causent dans l'accomplissement d'actes de prévention, de diagnostic ou de soins, alors qu'auparavant le délai différait selon que le responsable était un établissement public ou un professionnel ou un établissement privé ; qu'au regard de l'objet ainsi poursuivi par le législateur, les personnes engageant une action tendant à la prise en charge par l'ONIAM, en lieu et place du fournisseur des produits sanguins, des dommages résultant d'une contamination d'origine transfusionnelle ne sont pas dans la même situation que celles qui recherchent la réparation d'un dommage imputable à un professionnel ou un établissement de santé ; que, dans ces conditions, la circonstance que l'article L. 1142-28 du code de la santé publique n'inclue pas dans son champ d'application les actions prévues à l'article L. 1221-14 du même code n'implique pas une méconnaissance du principe de l'égalité devant la loi ; <br/>
<br/>
              9. Considérant qu'il résulte de ce qui précède que la question soulevée, qui n'est pas nouvelle, ne présente pas un caractère sérieux ; que, dès lors, il n'y a pas lieu de renvoyer cette question au Conseil constitutionnel ;<br/>
<br/>
              Sur les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              10. Considérant que la présente décision se borne à statuer sur le renvoi d'une question prioritaire de constitutionnalité au Conseil constitutionnel ; que les conclusions présentées par l'ONIAM au titre de l'article L. 761-1 du code de justice administrative, qui ne peuvent être portées que devant le juge saisi du litige à l'occasion duquel la question prioritaire de constitutionnalité a été soulevée, sont, par suite, irrecevables à ce stade ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Il n'y a pas lieu de renvoyer au Conseil constitutionnel la question de la conformité à la Constitution de l'article L. 1142-28 du code de la santé publique.<br/>
<br/>
Article 2 : Les conclusions présentées par l'ONIAM au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 3 : La présente décision sera notifiée à Mme B...A...et à l'Office national d'indemnisation des accidents médicaux, des affections iatrogènes et des infections nosocomiales. <br/>
Copie en sera adressée au Premier ministre et à la ministre des affaires sociales et de la santé.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">18-04-02-01 COMPTABILITÉ PUBLIQUE ET BUDGET. DETTES DES COLLECTIVITÉS PUBLIQUES - PRESCRIPTION QUADRIENNALE. RÉGIME DE LA LOI DU 31 DÉCEMBRE 1968. CHAMP D'APPLICATION. - ACTIONS TENDANT À LA RÉPARATION DE DOMMAGES LIÉS À DES ACTES MÉDICAUX - 1) EXCLUSION, AU PROFIT DE LA PRESCRIPTION DÉCENNALE PRÉVUE À L'ARTICLE L. 1142-28 DU CSP - A) CAS GÉNÉRAL - ACTIONS TENDANT À LA RÉPARATION DES DOMMAGES RÉSULTANT D'ACTES DE PRÉVENTION, DE DIAGNOSTIC OU DE SOINS DIRIGÉES CONTRE DES PROFESSIONNELS OU ÉTABLISSEMENTS DE SANTÉ - B) CAS PARTICULIER - ACTIONS TENDANT À LA RÉPARATION DES CONSÉQUENCES ANORMALES D'UN ACTE MÉDICAL OU DES PRÉJUDICES GRAVES RÉSULTANT D'UNE INFECTION NOSOCOMIALE ENGAGÉES CONTRE L'ONIAM - 2) INCLUSION - ACTIONS EN RESPONSABILITÉ DIRIGÉES CONTRE L'ETAT AU TITRE DES DOMMAGES IMPUTABLES AUX VACCINATIONS OBLIGATOIRES [RJ1] - ACTIONS EN RESPONSABILITÉ DES VICTIMES DE CONTAMINATIONS D'ORIGINE TRANSFUSIONNELLE DIRIGÉES CONTRE LE CENTRE DE TRANSFUSION SANGUINE AYANT ÉLABORÉ LES PRODUITS.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">60-02-01 RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. RESPONSABILITÉ EN RAISON DES DIFFÉRENTES ACTIVITÉS DES SERVICES PUBLICS. SERVICE PUBLIC DE SANTÉ. - ACTIONS TENDANT À LA RÉPARATION DE DOMMAGES LIÉS À DES ACTES MÉDICAUX - RÉGIMES DE PRESCRIPTION APPLICABLES - CHAMPS RESPECTIFS - 1) PRESCRIPTION DÉCENNALE PRÉVUE À L'ARTICLE L. 1142-28 DU CSP - A) CAS GÉNÉRAL - ACTIONS TENDANT À LA RÉPARATION DES DOMMAGES RÉSULTANT D'ACTES DE PRÉVENTION, DE DIAGNOSTIC OU DE SOINS DIRIGÉES CONTRE DES PROFESSIONNELS OU ÉTABLISSEMENTS DE SANTÉ - B) CAS PARTICULIER - ACTIONS TENDANT À LA RÉPARATION DES CONSÉQUENCES ANORMALES D'UN ACTE MÉDICAL OU DES PRÉJUDICES GRAVES RÉSULTANT D'UNE INFECTION NOSOCOMIALE ENGAGÉES CONTRE L'ONIAM - 2) PRESCRIPTION QUADRIENNALE PRÉVUE PAR LA LOI DU 31 DÉCEMBRE 1968 - ACTIONS EN RESPONSABILITÉ DIRIGÉES CONTRE L'ETAT AU TITRE DES DOMMAGES IMPUTABLES AUX VACCINATIONS OBLIGATOIRES [RJ1] - ACTIONS EN RESPONSABILITÉ DES VICTIMES DE CONTAMINATIONS D'ORIGINE TRANSFUSIONNELLE DIRIGÉES CONTRE LE CENTRE DE TRANSFUSION SANGUINE AYANT ÉLABORÉ LES PRODUITS.
</SCT>
<ANA ID="9A"> 18-04-02-01 1) a) Il résulte des termes mêmes de l'article L. 1142-28 du code de la santé publique (CSP), issu de la loi n° 2002-303 du 4 mars 2002 relative aux droits des malades et à la qualité du système de santé, que la prescription décennale qu'il institue s'applique aux actions en responsabilité tendant à la réparation de dommages résultant d'actes de prévention, de diagnostic ou de soins et dirigées contre des professionnels de santé ou des établissements de santé publics ou privés.... ,,b) Il résulte par ailleurs de l'ensemble des dispositions du chapitre II du titre IV du livre Ier de la première partie du CSP, issu de la même loi et complété par la loi n° 2002-1577 du 30 décembre 2002 relative à la responsabilité civile médicale, que le législateur a entendu soumettre également à la prescription décennale les actions engagées contre l'Office national d'indemnisation des accidents médicaux, des affections iatrogènes et des infections nosocomiales (ONIAM), sur le fondement du II de l'article L. 1142-1 ou sur le fondement de l'article L. 1142-1-1 du CSP, afin d'obtenir la réparation des conséquences anormales d'un acte médical ou des préjudices d'une particulière gravité résultant d'une infection nosocomiale, l'office étant appelé, dans le cadre de ces dispositions, à indemniser en lieu et place d'un professionnel ou d'un établissement de santé la victime d'un dommage que celui-ci a causé dans l'accomplissement d'actes de prévention, de diagnostic ou de soins.... ,,2) En revanche, le législateur n'a pas entendu rendre la prescription décennale applicable aux actions en responsabilité tendant à la réparation de dommages liés à des actes médicaux mais dirigées contre des personnes autres que des professionnels ou des établissements de santé.... ,,En particulier, cette prescription n'a pas été rendue applicable aux actions en responsabilité dirigées contre l'Etat, sur le fondement de l'article L. 3111-9 du CSP, au titre des dommages imputables aux vaccinations obligatoires, qui sont demeurées soumises à la prescription quadriennale prévue par la loi n° 68-1250 du 31 décembre 1968 relative à la prescription des créances sur l'Etat, les départements, les communes et les établissements publics. Si, dans sa rédaction issue de la loi n° 2004-806 du 9 août 2004 relative à la politique de santé publique, l'article L. 3111-9 prévoit que les victimes de tels dommages sont indemnisées par l'ONIAM au titre de la solidarité nationale, la prescription quadriennale demeure applicable aux actions fondées sur cet article.,,,De même, la loi du 4 mars 2002 n'a pas rendu la prescription décennale applicable aux actions par lesquelles les victimes de contaminations d'origine transfusionnelle recherchaient la responsabilité du centre de transfusion sanguine ayant élaboré les produits sanguins transfusés, sous réserve du cas où ce centre n'aurait pas eu une personnalité morale distincte de celle d'un établissement de santé. Si l'article L. 1221-14 du CSP, issu de la loi n° 2008-1330 du 17 décembre 2008 de financement de la sécurité sociale pour 2009, et l'article L. 3122-1 du même code, dans sa rédaction résultant de la même loi, confient à l'ONIAM l'indemnisation des personnes contaminées par certains agents pathogènes à l'occasion de transfusions de produits sanguins ou d'injections de médicaments dérivés du sang, les actions fondées sur ces dispositions ne peuvent être regardées comme entrant dans le champ de la prescription décennale dès lors que l'ONIAM n'est pas appelé à assurer une réparation en lieu et place du professionnel ou de l'établissement de santé qui a procédé à l'administration des produits sanguins, la responsabilité de ce professionnel ou de cet établissement n'étant pas normalement engagée en pareil cas. Dès lors que l'ONIAM est un établissement public doté d'un comptable public, ces actions sont soumises à la prescription quadriennale.</ANA>
<ANA ID="9B"> 60-02-01 1) a) Il résulte des termes mêmes de l'article L. 1142-28 du code de la santé publique (CSP), issu de la loi n° 2002-303 du 4 mars 2002 relative aux droits des malades et à la qualité du système de santé, que la prescription décennale qu'il institue s'applique aux actions en responsabilité tendant à la réparation de dommages résultant d'actes de prévention, de diagnostic ou de soins et dirigées contre des professionnels de santé ou des établissements de santé publics ou privés.... ,,b) Il résulte par ailleurs de l'ensemble des dispositions du chapitre II du titre IV du livre Ier de la première partie du CSP, issu de la même loi et complété par la loi n° 2002-1577 du 30 décembre 2002 relative à la responsabilité civile médicale, que le législateur a entendu soumettre également à la prescription décennale les actions engagées contre l'Office national d'indemnisation des accidents médicaux, des affections iatrogènes et des infections nosocomiales (ONIAM), sur le fondement du II de l'article L. 1142-1 ou sur le fondement de l'article L. 1142-1-1 du CSP, afin d'obtenir la réparation des conséquences anormales d'un acte médical ou des préjudices d'une particulière gravité résultant d'une infection nosocomiale, l'office étant appelé, dans le cadre de ces dispositions, à indemniser en lieu et place d'un professionnel ou d'un établissement de santé la victime d'un dommage que celui-ci a causé dans l'accomplissement d'actes de prévention, de diagnostic ou de soins.... ,,2) En revanche, le législateur n'a pas entendu rendre la prescription décennale applicable aux actions en responsabilité tendant à la réparation de dommages liés à des actes médicaux mais dirigées contre des personnes autres que des professionnels ou des établissements de santé.... ,,En particulier, cette prescription n'a pas été rendue applicable aux actions en responsabilité dirigées contre l'Etat, sur le fondement de l'article L. 3111-9 du CSP, au titre des dommages imputables aux vaccinations obligatoires, qui sont demeurées soumises à la prescription quadriennale prévue par la loi n° 68-1250 du 31 décembre 1968 relative à la prescription des créances sur l'Etat, les départements, les communes et les établissements publics. Si, dans sa rédaction issue de la loi n° 2004-806 du 9 août 2004 relative à la politique de santé publique, l'article L. 3111-9 prévoit que les victimes de tels dommages sont indemnisées par l'ONIAM au titre de la solidarité nationale, la prescription quadriennale demeure applicable aux actions fondées sur cet article.,,,De même, la loi du 4 mars 2002 n'a pas rendu la prescription décennale applicable aux actions par lesquelles les victimes de contaminations d'origine transfusionnelle recherchaient la responsabilité du centre de transfusion sanguine ayant élaboré les produits sanguins transfusés, sous réserve du cas où ce centre n'aurait pas eu une personnalité morale distincte de celle d'un établissement de santé. Si l'article L. 1221-14 du CSP, issu de la loi n° 2008-1330 du 17 décembre 2008 de financement de la sécurité sociale pour 2009, et l'article L. 3122-1 du même code, dans sa rédaction résultant de la même loi, confient à l'ONIAM l'indemnisation des personnes contaminées par certains agents pathogènes à l'occasion de transfusions de produits sanguins ou d'injections de médicaments dérivés du sang, les actions fondées sur ces dispositions ne peuvent être regardées comme entrant dans le champ de la prescription décennale dès lors que l'ONIAM n'est pas appelé à assurer une réparation en lieu et place du professionnel ou de l'établissement de santé qui a procédé à l'administration des produits sanguins, la responsabilité de ce professionnel ou de cet établissement n'étant pas normalement engagée en pareil cas. Dès lors que l'ONIAM est un établissement public doté d'un comptable public, ces actions sont soumises à la prescription quadriennale.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 13 juillet 2011, Office national d'indemnisation des accidents médicaux, des affections iatrogènes et des infections nosocomiales, n° 345756, p. 362.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
