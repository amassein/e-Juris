<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027666354</ID>
<ANCIEN_ID>JG_L_2013_07_000000356660</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/66/63/CETATEXT000027666354.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère et 6ème sous-sections réunies, 05/07/2013, 356660</TITRE>
<DATE_DEC>2013-07-05</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>356660</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère et 6ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Julia Beurton</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Maud Vialettes</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:356660.20130705</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 10 février 2012 au secrétariat du contentieux du Conseil d'Etat, présentée par le syndicat Solidaires Douanes, dont le siège est Boîte 29 - 80, rue de Montreuil à Paris (75011) ; le syndicat requérant demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le paragraphe " Direction générale des douanes et droits indirects " de l'annexe III de l'arrêté du 13 décembre 2011 du ministre de l'économie, des finances et de l'industrie, du ministre du budget, des comptes publics et de la réforme de l'Etat et du ministre de la fonction publique fixant la liste des organisations syndicales habilitées à désigner des représentants aux comités d'hygiène, de sécurité et des conditions de travail de ces trois ministères ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 500 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
              Vu le décret n° 82-453 du 28 mai 1982, notamment modifié par le décret n° 2011-774 du 28 juin 2011 ; <br/>
<br/>
              Vu le décret n° 2011-184 du 15 février 2011 ;<br/>
	Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Julia Beurton, Auditeur,  <br/>
<br/>
              - les conclusions de Mme Maud Vialettes, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article 42 du décret du 28 mai 1982 relatif à l'hygiène et la sécurité du travail ainsi qu'à la prévention médicale dans la fonction publique, dans sa rédaction issue du décret du 28 juin 2011 : " Les représentants du personnel au sein des comités d'hygiène, de sécurité et des conditions de travail sont désignés librement par les organisations syndicales de fonctionnaires (...) / A cet effet, pour chaque (...) direction (...), une liste des organisations syndicales habilitées à désigner des représentants ainsi que le nombre de sièges auxquels elles ont droit est arrêtée, proportionnellement au nombre de voix obtenues lors de l'élection ou de la désignation des représentants du personnel dans les comités techniques (...) " ; qu'aux termes de l'article 21 du décret du 15 février 2011 relatif aux comités techniques dans les administrations et les établissements publics de l'Etat : " I. (...) Les candidatures peuvent être communes à plusieurs organisations syndicales (...) " ; qu'aux termes de l'article 32 de ce même décret : " Lorsqu'une candidature de liste ou de sigle commune a été établie par des organisations syndicales, la répartition entre elles des suffrages exprimés se fait sur la base indiquée et rendue publique par les organisations syndicales concernées lors du dépôt de leur candidature. A défaut d'indication, la répartition des suffrages se fait à part égale entre les organisations concernées. Cette répartition est mentionnée sur les candidatures affichées dans les sections de vote " ;<br/>
<br/>
              2. Considérant qu'il résulte de ces dispositions, applicables à compter des élections intervenant en 2011, qu'en cas de présentation d'une liste commune par des organisations syndicales en vue de l'élection des représentants du personnel siégeant au sein des comités techniques, il appartient au ministre, au vu du résultat obtenu par cette liste, de répartir les suffrages exprimés sur la base indiquée et rendue publique par ces organisations syndicales ou, à défaut, à part égale entre elles ; qu'il en va de même pour la répartition des sièges aux comités d'hygiène, de sécurité et des conditions de travail, qui est arrêtée proportionnellement au nombre de voix obtenues lors de l'élection ou de la désignation des représentants du personnel dans les comités techniques ; qu'ainsi, les ministres ne pouvaient légalement attribuer des sièges aux comités d'hygiène, de sécurité et des conditions de travail relevant de la direction générale des douanes et droits indirects indistinctement à l'UNSA Douanes et à la CFTC Douanes, organisations syndicales qui avaient présenté une liste commune, sans procéder à une répartition des sièges tenant compte de la règle de partage des suffrages qu'elles avaient établie ou qui a été retenue en vue de l'élection des représentants du personnel  dans les comités techniques ;<br/>
<br/>
              3. Considérant qu'il résulte de ce qui précède que le syndicat Solidaires Douanes est fondé à demander l'annulation du paragraphe " Direction générale des douanes et droits indirects " de l'annexe III de l'arrêté du 13 décembre 2011 du ministre de l'économie, des finances et de l'industrie, du ministre du budget, des comptes publics et de la réforme de l'Etat et du ministre de la fonction publique, qui est divisible du reste des dispositions de cet arrêté ;<br/>
<br/>
              4. Considérant qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par le syndicat Solidaires Douanes au titre des dispositions de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le paragraphe " Direction générale des douanes et droits indirects " de l'annexe III de l'arrêté du 13 décembre 2011 du ministre de l'économie, des finances et de l'industrie, du ministre du budget, des comptes publics et de la réforme de l'Etat et du ministre de la fonction publique est annulé.<br/>
Article 2 : Les conclusions présentées par le syndicat Solidaires Douanes au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La présente décision sera notifiée au syndicat Solidaires Douanes, au ministre de l'économie et des finances et au ministre de la réforme de l'Etat, de la décentralisation et de la fonction publique.<br/>
Copie en sera adressée pour information au syndicat UNSA Douanes et au syndicat CFTC Douanes.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">54-05-04-03 PROCÉDURE. INCIDENTS. DÉSISTEMENT. DÉSISTEMENT D'OFFICE. - INDICATIONS VALANT ANNONCE D'UN MÉMOIRE COMPLÉMENTAIRE - ABSENCE - MÉMOIRE MOTIVÉ RÉGULIÈREMENT PRÉSENTÉ SANS MINISTÈRE D'AVOCAT - MÉMOIRE PRODUIT SOUS RÉSERVE DE PRODUCTIONS COMPLÉMENTAIRES (SOL. IMPL.)  [RJ1].
</SCT>
<ANA ID="9A"> 54-05-04-03 Un mémoire motivé, régulièrement présenté par un requérant sans avocat et formulant des conclusions sous réserve de tous autres éléments de droit ou de fait à produire ultérieurement par mémoire complémentaire, et sous réserve de tout autre recours, ne doit pas être regardé comme annonçant la production d'un mémoire complémentaire. Pas de désistement d'office à l'expiration du délai de trois mois.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Comp. CE, 13 mai 1997, Ministre de l'intérieur c/ commune de Mons-en-Baroeul, n° 82389, T. p. 886 ; CE, 1er octobre 1993, Commune de Velleron c/ Paoli, n° 129350, inédite au Recueil.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
