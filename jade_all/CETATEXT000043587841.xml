<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043587841</ID>
<ANCIEN_ID>JG_L_2021_06_000000427730</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/58/78/CETATEXT000043587841.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème chambre, 01/06/2021, 427730, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-06-01</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>427730</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP WAQUET, FARGE, HAZAN ; SCP NICOLAY, DE LANOUVELLE, HANNOTIN</AVOCATS>
<RAPPORTEUR>Mme Thalia Breton</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Raphaël Chambon</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:427730.20210601</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme C... D... a demandé au tribunal administratif de Lyon de condamner l'Ecole nationale des travaux publics de l'Etat (ENTPE) à lui verser la somme de 143 643,43 euros en réparation des préjudices qu'elle estime avoir subis du fait de sa situation administrative, outre les intérêts légaux à compter du 17 mars 2014. Par un jugement n° 1405896 du 2 novembre 2016, le tribunal administratif a condamné l'ENTPE à verser à Mme D... la somme de 106 917 euros avec intérêts légaux à compter du 17 mars 2014.<br/>
<br/>
              Par un arrêt n° 17LY00182 du 22 octobre 2018, la cour administrative d'appel de Lyon a, sur l'appel formé par l'ENTPE et sur l'appel incident de Mme D..., réformé le jugement du tribunal administratif de Lyon en ramenant à la somme de 50 000 euros l'indemnité que l'ENTPE avait été condamnée à verser à Mme D... et a rejeté le surplus des conclusions de la requête de l'ENTPE ainsi que l'appel incident de Mme D....<br/>
<br/>
<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 6 février, 6 mai 2019 et 18 décembre 2019 au secrétariat du contentieux du Conseil d'Etat, Mme D... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter la requête de l'ENTPE et de faire droit à sa requête d'appel ;<br/>
<br/>
              3°) de mettre à la charge de l'ENTPE le versement à la SCP Waquet, Farge, Hazan, son avocat, de la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative et de l'article 37 de la loi du 10 juillet 1991.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la loi n° 83-634 du 13 juillet 1983 ;<br/>
              - la loi n° 84-16 du 11 janvier 1984 ; <br/>
              - la loi n° 84-834 du 13 septembre 1984 ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ; <br/>
              - la loi n° 2010-1330 du 9 novembre 2010 ;<br/>
              - la loi n° 2012-347 du 12 mars 2012 ;<br/>
              - le décret n° 2011-754 du 28 juin 2011 ;<br/>
              - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme B... A..., auditrice,  <br/>
<br/>
              - les conclusions de M. Raphaël Chambon, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Waquet, Farge, Hazan, avocat de Mme D... et à la SCP Nicolaÿ, de Lanouvelle, Hannotin, avocat de l'Ecole nationale des travaux publics de l'Etat ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que Mme D... a été employée par l'Ecole nationale des travaux publics de l'Etat (ENTPE) en qualité de " chargée de cours d'espagnol vacataire " à compter du 1er janvier 1980. A la suite du rejet implicite, par l'ENTPE, de la demande de prolongation d'activité de l'intéressée au-delà de la limite d'âge, l'ENTPE a mis fin à la relation de travail à la date anniversaire de ses soixante-cinq ans, le 2 décembre 2012. Par un jugement du 6 février 2013, devenu définitif, le tribunal administratif de Lyon a rejeté la demande de Mme D... tendant à l'annulation de la décision du 31 août 2010 par laquelle l'ENTPE avait rejeté la demande de l'intéressée tendant à ce qu'elle soit titularisée. Mme D..., se prévalant des motifs du jugement du 6 février 2013 selon lesquels, du 1er janvier 1980 au 1er septembre 2009, elle devait être regardée comme ayant eu la qualité d'agent contractuel occupant un emploi permanent à temps incomplet, a présenté à l'ENTPE le 14 mars 2014 une demande préalable d'indemnisation à hauteur de 143 643,43 euros résultant des préjudices qu'elle estime avoir subis, d'une part, au titre de la perte de rémunération issue de la différence entre la rémunération qu'elle aurait perçue si elle avait été employée comme agent contractuel en contrat à durée indéterminée entre le 1er janvier 1980 et le 1er décembre 2012 et celle qu'elle a effectivement perçu en qualité d'agent vacataire pendant la même période, et d'autre part, au titre de la perte de rémunération résultant de la décision de l'ENTPE de mettre fin à la relation de travail à compter du 2 décembre 2012, date à laquelle l'intéressée était atteinte par la limite d'âge, alors qu'elle avait demandé à bénéficier d'une " prolongation d'activité " du fait du caractère incomplet de sa carrière. A la suite du rejet de sa demande indemnitaire, par décision du directeur de l'ENTPE du 20 mai 2014, Mme D... en a saisi le tribunal administratif de Lyon qui, par un jugement du 2 novembre 2016, y a partiellement fait droit en condamnant l'ENTPE à lui verser la somme de 106 917 euros avec intérêts au taux légal à compter du 17 mars 2014. Sur appel de l'ENTPE, la cour administrative d'appel de Lyon, par un arrêt du 22 octobre 2018 contre lequel Mme D... se pourvoit en cassation, a réformé ce jugement en ramenant à la somme de 50 000 euros l'indemnité que l'ENTPE avait été condamnée à verser à l'intéressée et a rejeté le surplus des conclusions dont elle était saisie. <br/>
<br/>
              2. En premier lieu, aux termes de l'article 1er de la loi du 13 novembre 1984 relative à la limite d'âge dans la fonction publique et le secteur public, dans sa rédaction applicable, issue de la loi du 9 novembre 2010 portant réforme des retraites : " Sous réserve des reculs de limite d'âge pouvant résulter des textes applicables à l'ensemble des agents de l'Etat, la limite d'âge des fonctionnaires civils de l'Etat est fixée à soixante-sept ans lorsqu'elle était, avant l'intervention de la loi n°2010-1330 du 9 novembre 2010 portant réforme des retraites, fixée à soixante-cinq ans ". Ces dispositions étant applicables, ainsi qu'elles le prévoient, aux seuls fonctionnaires et n'ayant pas été rendues, par d'autres dispositions, applicables aux agents non titulaires de l'Etat et aux agents qui assurent des vacations pour son compte, la cour administrative d'appel de Lyon a commis une erreur de droit en en faisant application à Mme D..., alors qu'elle avait relevé que cette dernière avait depuis le mois d'octobre 2009 la qualité d'agent vacataire de l'ENTPE. <br/>
<br/>
              3. En second lieu, ni les dispositions alors en vigueur de la loi du 11 janvier 1984 portant dispositions statutaires relatives à la fonction publique de l'Etat, ni le décret du 17 janvier 1986 relatif aux dispositions générales applicables aux agents non titulaires de l'Etat, ni aucune autre disposition législative ou réglementaire, n'interdisent à l'administration de calculer la rémunération de ses agents contractuels, même employés dans des conditions correspondant à un emploi permanent, en fonction d'un taux de vacations horaires. Par suite, en jugeant que Mme D..., dont elle avait relevé qu'elle avait la qualité d'agent contractuel de l'ENTPE du 1er janvier 1980 à la fin du mois de septembre 2009, n'était pas fondée à demander une indemnité en réparation du préjudice de rémunération qu'elle alléguait avoir subi en étant rémunérée, durant cette période, sur la base d'un taux horaire malgré son statut d'agent non titulaire de l'ENTPE, la cour administrative d'appel de Lyon n'a pas entaché son arrêt d'erreur de droit, ni dénaturé les pièces du dossier qui lui était soumis. <br/>
<br/>
              4. Il résulte de tout ce qui précède que Mme D... n'est fondée à demander l'annulation de l'arrêt qu'elle attaque qu'en tant qu'il se prononce sur ses conclusions tendant à l'indemnisation du préjudice qu'elle estime avoir subi du fait de ce qu'il a été mis fin, malgré sa demande de prolongation, à ses fonctions à son soixante-cinquième anniversaire. <br/>
<br/>
              5. Mme D... a obtenu le bénéfice de l'aide juridictionnelle. Par suite, son avocat peut se prévaloir des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991. Il y a lieu, dans les circonstances de l'espèce et sous réserve que la SCP Waquet, Farge, Hazan, avocat de Mme D..., renonce à percevoir la somme correspondant à la part contributive de l'Etat, de mettre à la charge de l'Ecole nationale des travaux publics de l'Etat une somme de 3 000 euros à verser à la SCP Waquet, Farge, Hazan au titre des dispositions précitées. <br/>
<br/>
              6. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de Mme D... qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Lyon du 22 octobre 2018 est annulé en ce qu'il se prononce sur ses conclusions de Mme D... tendant à l'indemnisation du préjudice qu'elle estime avoir subi du fait de ce qu'il a été mis fin, malgré sa demande de prolongation, à ses fonctions à son soixante-cinquième anniversaire.<br/>
Article 2 : L'affaire est renvoyée, dans la limite de la cassation ainsi prononcée, à la cour administrative d'appel de Lyon.<br/>
Article 3 : L'Ecole nationale des travaux publics de l'Etat (ENTPE) versera à la SCP Waquet, Farge, Hazan une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative et du deuxième alinéa de l'article 37 de la loi du 10 juillet 1991, sous réserve que cette société renonce à percevoir la somme correspondant à la part contributive de l'Etat.  <br/>
Article 4 : Le surplus des conclusions du pourvoi est rejeté. <br/>
Article 5 : Les conclusions de l'ENTPE présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 6 : La présente décision sera notifiée à Mme C... D... et à l'Ecole nationale des travaux publics de l'Etat.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
