<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044271059</ID>
<ANCIEN_ID>JG_L_2021_10_000000442828</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/27/10/CETATEXT000044271059.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème chambre, 28/10/2021, 442828, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-10-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>442828</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>CABINET MUNIER-APAIRE ; SARL MEIER-BOURDEAU, LECUYER ET ASSOCIES</AVOCATS>
<RAPPORTEUR>Mme Catherine Calothy</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Stéphane Hoynck</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:442828.20211028</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La commune de Pellevoisin, l'association " Vivre au Boischaut Nord ", Mme O..., M. et Mme D..., A... G..., A... G..., A... K..., M. et Mme L..., M. N... et Mme H..., M. M..., M. E..., M. B... et Mme Q..., M. I..., M. F..., M. J..., la société Beaulieu International Group ont demandé au tribunal administratif de Limoges d'annuler l'arrêté du 18 décembre 2014 par lequel le préfet de la région Centre a autorisé la société Centrale éolienne du Nord Val de l'Indre à exploiter des installations de production d'électricité utilisant l'énergie mécanique du vent sur le territoire des communes d'Argy et de Sougé (Indre). Par un jugement nos 1501075 du 28 décembre 2017, le tribunal administratif a fait droit à leur demande.<br/>
<br/>
              Par un arrêt n° 18BX00855, 18BX00903 du 16 juin 2020, la cour administrative d'appel de Bordeaux a, sur appel de la société Centrale éolienne du Nord Val de l'Indre et du ministre de la transition écologique et solidaire, annulé ce jugement et rejeté les conclusions présentées en première instance et en appel par la commune de Pellevoisin et autres.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique enregistrés les 17 août et 13 novembre 2020 et le 30 juillet 2021 au secrétariat du contentieux du Conseil d'Etat, la commune de Pellevoisin et autres demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de la Centrale éolienne du Nord Val de l'Indre et du ministre de la transition écologique et solidaire ;<br/>
<br/>
              3°) de mettre à la charge solidaire des défendeurs la somme de 6 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la directive 85/337/CEE du Conseil du 27 juin 1985 ;<br/>
              - la directive 2001/42/CE du 27 juin 2001 du Parlement européen et du Conseil du 27 juin 2001;<br/>
              - le code de l'environnement ;<br/>
              - l'ordonnance n° 2017-80 du 26 janvier 2017 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Catherine Calothy, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Stéphane Hoynck, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, au cabinet Munier-Apaire, avocat de la commune de Pellevoisin et autres et à la SARL Meier-Bourdeau, Lecuyer et associés, avocat de la société Centrale Eolienne du Nord Val de l'Indre ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que, par un arrêté du 18 décembre 2014, le préfet de la région Centre a autorisé la société Centrale éolienne du Nord Val de l'Indre à exploiter six aérogénérateurs et un poste de livraison sur le territoire des communes d'Argy et de Sougé (Indre). La commune de Pellevoisin et autres ont demandé au tribunal administratif de Limoges d'annuler cet arrêté. Par un jugement du 28 décembre 2017, le tribunal administratif a fait droit à leur demande. Par un arrêt du 16 juin 2020, contre lequel la commune de Pellevoisin et autres se pourvoient en cassation, la cour administrative d'appel de Bordeaux a fait droit à l'appel formé contre ce jugement par la société Centrale éolienne du Nord Val de l'Indre et par la ministre de la transition écologique et solidaire, et a rejeté les conclusions présentées en première instance et en appel par la commune de Pellevoisin et autres. <br/>
<br/>
              2. Aux termes du 1 de l'article 6 de la directive 85/337/CEE du Conseil du 27 juin 1985 concernant l'évaluation des incidences de certains projets publics et privés sur l'environnement,  dans sa rédaction issue de la directive 97/11/CE du Conseil du 3 mars 1997 : " Les Etats membres prennent les mesures nécessaires pour que les autorités susceptibles d'être concernées par le projet, en raison de leurs responsabilités spécifiques en matière d'environnement, aient la possibilité de donner leur avis sur les informations fournies par le maître d'ouvrage et sur la demande d'autorisation. A cet effet, les Etats membres désignent les autorités à consulter, d'une manière générale ou cas par cas. Celles-ci reçoivent les informations recueillies en vertu de l'article 5. Les modalités de cette consultation sont fixées par les Etats membres ". Aux termes de l'article L. 122-1 du code de l'environnement, dans sa rédaction alors applicable : " I.- Les projets de travaux, d'ouvrages ou d'aménagements publics et privés qui, par leur nature, leurs dimensions ou leur localisation sont susceptibles d'avoir des incidences notables sur l'environnement ou la santé humaine sont précédés d'une étude d'impact. / (...) / III.- Dans le cas d'un projet relevant des catégories d'opérations soumises à étude d'impact, le dossier présentant le projet, comprenant l'étude d'impact et la demande d'autorisation, est transmis pour avis à l'autorité administrative de l'Etat compétente en matière d'environnement. (...) ". En vertu du III de l'article R. 122-1-1 du même code, dans sa rédaction alors applicable : " Dans les cas ne relevant pas du I ou du II ci-dessus, l'autorité administrative de l'État compétente en matière d'environnement mentionnée à l'article L. 122-1 est le préfet de la région sur le territoire de laquelle le projet de travaux, d'ouvrage ou d'aménagement doit être réalisé ".<br/>
<br/>
              3. La directive du 27 juin 1985 concernant l'évaluation des incidences de certains projets publics et privés sur l'environnement a pour finalité de garantir qu'une autorité compétente et objective en matière d'environnement soit en mesure de rendre un avis sur l'étude d'impact des projets, publics ou privés, susceptibles d'avoir des incidences notables sur l'environnement, avant de statuer sur une demande d'autorisation, afin de permettre la prise en compte de ces incidences. Eu égard à l'interprétation des dispositions de l'article 6 de la directive 2001/42/CE du 27 juin 2001 relative à l'évaluation des incidences de certains plans et programmes sur l'environnement donnée par la Cour de justice de l'Union européenne dans son arrêt rendu le 20 octobre 2011 dans l'affaire C-474/10, et à la finalité identique des dispositions des deux directives du 27 juin 1985 et du 27 juin 2001 relatives au rôle " des autorités susceptibles d'être concernées par le projet, en raison de leurs responsabilités spécifiques en matière d'environnement ", il résulte clairement des dispositions de l'article 6 de la directive du 27 juin 1985 citées au point 2 que, si ces dispositions ne font pas obstacle à ce que l'autorité publique compétente pour autoriser un projet ou en assurer la maîtrise d'ouvrage soit en même temps chargée de la consultation en matière environnementale, elles imposent cependant que, dans une telle situation, une séparation fonctionnelle soit organisée au sein de cette autorité, de manière à ce qu'une entité administrative, interne à celle-ci, disposant d'une autonomie réelle qui implique notamment qu'elle soit pourvue de moyens administratifs et humains qui lui sont propres, soit en mesure de remplir la mission de consultation qui lui est confiée et de donner un avis objectif sur le projet concerné.<br/>
<br/>
              4. Après avoir relevé que le préfet de région était à la fois l'auteur de l'avis rendu le 4 avril 2013 en qualité d'autorité environnementale et l'autorité compétente qui a délivré les permis et autorisation attaqués et que l'avis ainsi émis par le préfet de région n'avait pas été rendu par une autorité disposant d'une autonomie effective dans des conditions garantissant son objectivité, la cour a néanmoins estimé que l'avis résultait d'une analyse précise, critique et indépendante du dossier et qu'il mettait en exergue aussi bien les lacunes que les qualités du dossier. En en déduisant que, dans les circonstances de l'espèce, l'avis, versé au dossier d'enquête publique, qui avait pourtant été rendu dans des conditions qui méconnaissaient les exigences de la directive, avait permis une bonne information de l'ensemble des personnes intéressées par l'opération et que son irrégularité n'avait pas été susceptible d'exercer une influence sur le sens de la décision de l'autorité administrative, la cour a commis une erreur de droit.<br/>
<br/>
              5. Il résulte de ce qui précède, sans qu'il soit besoin de se prononcer sur les autres moyens du pourvoi, que la commune de Pellevoisin et autres sont fondés à demander l'annulation de l'arrêt attaqué.<br/>
<br/>
              6. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat et de la société Centrale éolienne du Nord Val de l'Indre la somme de 1 500 euros chacun à verser à la commune de Pellevoisin et autres au titre des dispositions de l'article L. 761-1 du code de justice administrative. En revanche, ces mêmes dispositions font obstacle à ce qu'une somme soit mise, à ce titre, à la charge de la commune de Pellevoisin et autres qui ne sont pas, dans la présente instance, les parties perdantes. <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Bordeaux du 16 juin 2020 est annulé.<br/>
<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Bordeaux. <br/>
<br/>
Article 3 : L'Etat et la société Centrale éolienne du Nord Val de l'Indre verseront chacun à la commune de Pellevoisin et autres une somme de 1 500 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : Les conclusions présentées par la société Centrale éolienne du Nord Val de l'Indre au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 5 : La présente décision sera notifiée à la commune de Pellevoisin, première dénommée pour l'ensemble des requérants, à la société Centrale éolienne du Nord Val de l'Indre et à la ministre de la transition écologique.<br/>
              Délibéré à l'issue de la séance du 23 septembre 2021 où siégeaient : M. Cyril Roger-Lacan, assesseur, présidant ; Mme Suzanne von Coester, conseillère d'Etat et Mme Catherine Calothy, maître des requêtes en service extraordinaire-rapporteure. <br/>
<br/>
              Rendu le 28 octobre 2021.<br/>
<br/>
<br/>
                 Le président : <br/>
                 Signé : M. Cyril Roger-Lacan<br/>
 		La rapporteure : <br/>
      Signé : Mme Catherine Calothy<br/>
                 La secrétaire :<br/>
                 Signé : Mme P... C...<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
