<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033685068</ID>
<ANCIEN_ID>JG_L_2016_12_000000391968</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/68/50/CETATEXT000033685068.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème chambre, 23/12/2016, 391968, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-12-23</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>391968</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BOULLEZ</AVOCATS>
<RAPPORTEUR>Mme Séverine Larere</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emilie Bokdam-Tognetti</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2016:391968.20161223</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. et Mme B... A...ont demandé au tribunal administratif de Châlons-en-Champagne de prononcer la décharge des cotisations supplémentaires d'impôt sur le revenu et de contributions sociales auxquelles ils ont été assujettis au titre de l'année 2005. Par un jugement n° 1200738 du 29 avril 2014, le tribunal administratif de Châlons-en-Champagne n'a que partiellement fait droit à leur demande.<br/>
<br/>
              Par un arrêt n° 14NC01360 du 13 mai 2015, la cour administrative d'appel de Nancy, sur appel de M. et Mme A..., a réformé ce jugement et rejeté le surplus de leurs conclusions. <br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire, un mémoire en réplique et un nouveau mémoire, enregistrés les 22 juillet, 23 octobre et 16 décembre 2015 et 8 août 2016 au secrétariat du contentieux du Conseil d'Etat, M. et Mme  A...demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'article 3 de cet arrêt ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 8 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - la décision du 30 décembre 2015 par laquelle le Conseil d'Etat statuant au contentieux n'a pas renvoyé au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée par M. et Mme A...;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Séverine Larere, maître des requêtes ;<br/>
<br/>
              - les conclusions de Mme Emilie Bokdam-Tognetti, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Boullez, avocat de M. et Mme A...;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que, le 1er juillet 2005, M. A... a cédé à chacune des sociétés civiles Nathalie A...et Mélanie A... l'usufruit temporaire, pour une durée de neuf ans et six mois, de 8 550 parts qu'il détenait dans le capital de la société civile d'exploitation viticole (SCEV) ChampagneA..., société relevant du régime des sociétés de personnes conformément aux articles 8 et 8 ter du code général des impôts, pour un prix unitaire de 5,75 euros par part, soit un prix de 49 162,50 euros. A l'issue de la vérification de comptabilité dont la SCEV Champagne A...a fait l'objet, l'administration a estimé que ce prix était très inférieur à la valeur vénale réelle de l'usufruit des titres cédés, évaluée à 43,32 euros par part, soit un prix de 370 350 euros pour 8 550 parts, en retenant une moyenne arithmétique des valeurs obtenues par deux méthodes d'évaluation, fondées, pour la première, sur l'actualisation des flux futurs et, pour la seconde, sur la valeur en pleine propriété des titres, calculée à partir de la valeur mathématique et de la valeur de rendement, l'usufruit étant déterminé à partir du taux de rendement des titres sur la durée de l'usufruit. L'administration en a déduit que M. A...avait consenti une libéralité et a rehaussé en conséquence le montant de la plus-value professionnelle déclaré par M. et MmeA..., imposable dans la catégorie des bénéfices agricoles en application des articles 151 nonies I, 39 duodecies et 72 du code général des impôts et taxée au taux de 16 % conformément à l'article 39 quindecies du même code. M. et Mme A...se pourvoient en cassation contre l'article 3 de l'arrêt de la cour administrative d'appel de Nancy du 13 mai 2015 rejetant le surplus de leurs conclusions d'appel. <br/>
<br/>
              2. La valeur vénale des titres d'une société non admis à la négociation sur un marché réglementé doit être appréciée compte tenu de tous les éléments dont l'ensemble permet d'obtenir un chiffre aussi voisin que possible de celui qu'aurait entraîné le jeu normal de l'offre et de la demande à la date où la cession est intervenue. L'évaluation des titres d'une telle société doit être effectuée, par priorité, par référence au prix d'autres transactions intervenues dans des conditions équivalentes et portant sur les titres de la même société ou, à défaut, de sociétés similaires. En l'absence de telles transactions, celle-ci peut légalement se fonder sur la combinaison de plusieurs méthodes alternatives.<br/>
<br/>
              Sur les motifs de l'arrêt relatifs à la régularité de la procédure d'imposition :<br/>
<br/>
              3. Par une appréciation souveraine des faits non arguée de dénaturation, la cour a estimé que la proposition de rectification du 28 avril 2008 adressée à M. et Mme A...comportait des motifs suffisamment explicites pour permettre aux requérants d'engager une discussion contradictoire avec l'administration conformément aux exigences de l'article L. 57 du livre des procédures fiscales. La cour n'a commis aucune erreur de droit en jugeant, après avoir rappelé que la régularité d'une proposition de rectification ne dépend pas du bien-fondé de ses motifs, que les critiques des requérants quant au choix et à la pertinence des méthodes d'évaluation des titres litigieux, relatives au bien-fondé des impositions, étaient sans incidence sur le caractère suffisant de la motivation de cette proposition de rectification.  <br/>
<br/>
              Sur les motifs de l'arrêt relatifs au recours à des méthodes d'évaluation alternatives à la méthode par comparaison :<br/>
<br/>
              4. Contrairement à ce qui est soutenu, la cour a répondu au moyen tiré de ce que l'administration était légalement tenue d'avoir recours au préalable à la méthode par comparaison en relevant, d'une part, à l'occasion de l'examen de la régularité de la motivation de la proposition de rectification, qu'un tel moyen relevait du bien-fondé de l'imposition, d'autre part, à l'occasion de l'examen du bien-fondé des impositions, qu'en l'absence de transactions comparables, l'administration était en droit de déterminer la valeur vénale de l'usufruit des titres litigieux en recourant à la combinaison d'évaluations alternatives. L'arrêt attaqué est par ailleurs suffisamment motivé au regard de l'argumentation des requérants qui ne se prévalaient d'aucun élément de comparaison pour évaluer les titres litigieux. Enfin, la cour n'a pas commis d'erreur de droit au regard des règles de dévolution de la charge de la preuve en se prononçant au vu de l'instruction, à partir des éléments fournis par les parties. <br/>
<br/>
              Sur les motifs de l'arrêt relatifs à la valeur de l'usufruit déterminée à partir de la valeur en pleine propriété des titres de la SCEV ChampagneA... :<br/>
<br/>
              5. Il résulte des énonciations de l'arrêt attaqué que l'administration a déterminé la valeur en pleine propriété d'un titre de la SCEV Champagne A...à partir de la valeur unitaire moyenne des parts sociales issues de la combinaison de deux méthodes, celle de la valeur mathématique, correspondant à la réévaluation de l'actif net y compris les éléments incorporels, et celle de la valeur de rendement, consistant à capitaliser le dividende net moyen calculé sur une période de trois ans par application d'un taux correspondant au pourcentage de distribution observé. Elle a ensuite déterminé la valeur future de la pleine propriété en affectant à cette valeur un coefficient d'actualisation, puis a déterminé la valeur de l'usufruit comme composante de la pleine propriété après avoir calculé le taux de rendement des parts.<br/>
<br/>
              En ce qui concerne la valeur mathématique :<br/>
<br/>
              6. En premier lieu, les requérants estiment que la cour a commis une erreur de droit en jugeant que l'administration avait, à bon droit, évalué les stocks de vins en fonction de leur valeur vénale et non de leur prix de revient. Toutefois  la circonstance que la Cour de cassation aurait adopté une solution contraire dans un arrêt du 7 octobre 2008, au demeurant dans des circonstances très différentes, est sans incidence sur le présent litige. Par ailleurs, alors que les requérants ne faisaient état d'aucune circonstance particulière de nature à établir que l'évaluation des stocks de vins à leur prix de revient était plus proche du prix qu'aurait entraîné le jeu normal de l'offre et la demande à la date où la cession des titres est intervenue, la cour a pu, sans commettre d'erreur de droit, se référer aux usages synthétisés dans le guide de l'évaluation des entreprises. La cour a en outre indiqué, par une appréciation souveraine des faits non arguée de dénaturation, qu'il ne résultait pas de l'instruction que la valorisation des stocks de vins clairs à leur prix de revient aurait été différente de celle réalisée par l'administration.<br/>
<br/>
              7. En deuxième lieu, les requérants soutiennent que la cour aurait insuffisamment motivé son arrêt et commis une erreur de droit en retenant que les constructions de la SCEV ChampagneA... devaient être comptabilisées pour leur coût d'acquisition, sans rechercher si, comme ils l'y invitaient, la prise en considération du jeu de l'offre et de la demande n'imposait pas de tenir compte de la valeur vénale et de la fiscalité latente, au besoin en ordonnant une expertise. Il résulte toutefois des énonciations de l'arrêt attaqué que la cour a validé l'évaluation de ces constructions à leur valeur d'acquisition et non à leur valeur vénale en se fondant sur l'absence de termes de comparaison avec des constructions équivalentes. Elle a pu, dans le cadre de son pouvoir souverain d'appréciation, estimer disposer d'éléments suffisants au vu du dossier qui lui était soumis, sans avoir recours à une mesure supplémentaire d'instruction. Elle a par ailleurs indiqué qu'il n'y avait pas lieu de prendre en compte la fiscalité latente correspondant à ces immobilisations, dès lors que les constructions de la SCEV ChampagneA... ne pouvaient être cédées sans nuire au bon déroulement de l'activité de la société. Ce faisant, la cour a suffisamment motivé son arrêt et n'a pas commis l'erreur de droit invoquée. <br/>
<br/>
              8. En troisième lieu, après avoir relevé que si la rentabilité des actifs corporels engagés dans une société est supérieure à la rémunération attendue selon le taux de référence du marché financier majoré d'une prime de risque tenant compte des spécificités de l'activité économique de la société, il apparaît ainsi une survaleur, qu'elle a nommée " superprofit ", traduisant l'existence d'un actif incorporel qu'il convient le cas échéant de valoriser pour déterminer la valeur mathématique d'une entreprise, la cour a recherché si les différents éléments retenus par l'administration étaient de nature à déterminer que les éléments propres à la SCEV Champagne A...étaient de nature à révéler l'existence d'une telle survaleur. Ce n'est qu'après avoir écarté chacun des arguments invoqués par les requérants et examiné les circonstances particulières de l'espèce que la cour, par une appréciation souveraine des faits non arguée de dénaturation, a confirmé l'existence de cette survaleur à l'actif de la SCEV ChampagneA.... En statuant ainsi, la cour n'a pas commis d'erreur de droit. <br/>
<br/>
              En ce qui concerne la valeur de rendement :<br/>
<br/>
              9. Il ressort des énonciations de l'arrêt attaqué que l'administration a déterminé une valeur de rendement calculée d'après un dividende de référence, correspondant à la moyenne des bénéfices distribués par la SCEV ChampagneA... après impôt fictif, tels qu'ils auraient pu être distribués si la société  avait été imposable à l'impôt sur les sociétés, en appliquant à ce dividende un taux de capitalisation par référence à un taux de rendement lui-même déterminé à partir du taux de rendement des obligations assimilables du Trésor, déflaté pour tenir compte de l'érosion monétaire, auquel elle a ajouté une prime de risque propre à la société dont les titres ont été évalués.<br/>
<br/>
              10.  En premier lieu, si les requérants soutiennent que la cour aurait omis de répondre au moyen tiré de ce que la valeur de rendement était une méthode viciée dans son principe même dès lors qu'elle aboutissait à ce paradoxe qu'un bien coûte d'autant plus cher qu'il rapporte peu, ce moyen, développé au soutien de la contestation de la régularité de la proposition de rectification, était inopérant. Comme il est dit au point 3 ci-dessus, la cour a d'ailleurs expressément écarté les critiques portant sur les méthodes mises en oeuvre par l'administration, dont celle de la valeur de rendement, comme étant relatives au bien-fondé des impositions, et dès lors sans incidence sur le caractère suffisant de la motivation de la proposition de rectification. Cette critique d'ordre général était, en tout état de cause, également sans incidence sur le bien-fondé des impositions, dès lors que les titres litigieux ont été évalués par la combinaison de plusieurs méthodes. <br/>
<br/>
              11. En second lieu, il ressort des écritures d'appel de M. et Mme A...que s'ils demandaient que soit exclue, pour le calcul de la valeur de rendement, du bénéfice moyen de la SCEV Champagne A...la rémunération des deux associés exploitants, à hauteur de 70 000 euros chacun, ils ont indiqué dans un dernier mémoire que cette somme correspondait " à la rémunération d'un cadre du champagne " et " devrait, pour les besoins de l'évaluation, être déduite à hauteur de ce montant (...) quand bien même aucune rémunération n'aurait-elle été servie réellement ". Dans ces conditions, la cour a pu estimer, sans dénaturer les écritures, ni méconnaître son office, que les requérants n'assortissaient leur demande d'aucune précision permettant d'en apprécier le bien-fondé.<br/>
<br/>
              12. En troisième lieu, contrairement à ce que soutiennent les requérants, la cour n'a validé la valeur de rendement calculée par l'administration qu'après avoir écarté les différentes objections qu'ils avaient soulevées. Son arrêt est par suite suffisamment motivé sur ce point. S'ils soutiennent par ailleurs que la cour a commis une erreur de droit dès lors que selon eux cette méthode n'est pas appropriée pour l'évaluation des titres d'une société de personnes, ce moyen est nouveau en cassation et, par suite, inopérant. <br/>
<br/>
              Sur les motifs de l'arrêt relatifs à la valeur de l'usufruit déterminée selon la méthode de l'actualisation des fruits futurs :<br/>
<br/>
              13. La cour n'était pas tenue de répondre à tous les arguments soulevés par les requérants. Elle a, en tout état de cause, suffisamment répondu à leur argumentation tirée de la baisse du chiffre d'affaires du secteur en estimant qu'en se bornant à faire état de prévisions pessimistes quant à l'évolution du marché du champagne, ils n'établissaient pas que l'évaluation de la valeur de l'usufruit des titres de la SCEV Champagne A...à partir de ses résultats des cinq exercices antérieurs serait, en l'absence de circonstances particulières propres à cette société, de nature à vicier la méthode mise en oeuvre par le service. Elle a également suffisamment motivé son arrêt en relevant, pour refuser de pratiquer un abattement pour absence de liquidité, que l'objet de cette méthode est d'appréhender des dividendes attendus, indépendamment de la liquidité des titres. <br/>
<br/>
              Sur les motifs de l'arrêt relatifs aux pénalités pour mauvaise foi :<br/>
<br/>
              14. En jugeant, par une appréciation souveraine des faits non arguée de dénaturation, que l'administration établissait que M. et Mme A...avaient délibérément minoré la valeur des titres cédés en vue d'éluder l'impôt et en en déduisant qu'elle avait, à bon droit, fait application des majorations prévues à l'article 1729 du code général des impôts, la cour n'a pas commis d'erreur de droit. <br/>
<br/>
              15. Il résulte de tout ce qui précède que M. et Mme A...ne sont pas fondés à demander l'annulation de l'arrêt qu'ils attaquent.<br/>
<br/>
              16. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat. <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de M. et Mme A...est rejeté. <br/>
Article 2 : La présente décision sera notifiée à M. et Mme B... A...et au ministre des finances et des comptes publics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
