<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031281289</ID>
<ANCIEN_ID>JG_L_2015_10_000000386603</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/28/12/CETATEXT000031281289.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème et 2ème sous-sections réunies, 05/10/2015, 386603, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2015-10-05</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>386603</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème et 2ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. François Lelièvre</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Bertrand Dacosta</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:386603.20151005</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu les procédures suivantes :<br/>
<br/>
              1° Sous le n° 386603, par une requête, un mémoire en réplique et un nouveau mémoire, enregistrés le 21 décembre 2014 et les 19 avril et 9 septembre 2015 au secrétariat du contentieux du Conseil d'Etat, le syndicat CGT du personnel de la CCI Paris Ile-de-France et la fédération CGT Commerce, distribution, services demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le relevé de décisions de la commission paritaire nationale (CPN) des chambres de commerce et d'industrie du 22 septembre 2014 modifiant le statut du personnel administratif des chambres de commerce et d'industrie ainsi que la décision du ministre de l'économie, de l'industrie et du numérique rejetant son recours tendant à qu'il ne notifie pas ce relevé de décisions ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat le versement de la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              2° Sous le n° 386608, par une requête et un mémoire en réplique, enregistrés les 19 décembre 2014 et 11 septembre 2015 au secrétariat du contentieux du Conseil d'Etat, la CFE-CGC Réseaux consulaires demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le même relevé de décisions de la commission paritaire nationale des chambres de commerce et d'industrie ;<br/>
<br/>
<br/>
              2°) de mettre à la charge de l'Etat le versement de la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu :<br/>
              - la Constitution, notamment son Préambule ;<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - le code de commerce ;<br/>
              - la loi n° 52-1311 du 10 décembre 1952 ;<br/>
              - la loi n° 71-1130 du 31 décembre 1971 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. François Lelièvre, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Bertrand Dacosta, rapporteur public ;<br/>
<br/>
<br/>
<br/>1. Considérant que, d'une part, aux termes de l'article 1er de la loi du 10 décembre 1952 relative à l'établissement obligatoire d'un statut du personnel administratif des chambres d'agriculture, de commerce et des chambres de métiers : " La situation du personnel administratif des chambres (...) de commerce (...) est déterminée par un statut établi par des commissions paritaires nommées (...) par le ministre de tutelle " ; que, d'autre part, l'établissement public " CCI France ", placé, en vertu de l'article L. 711-15  du code de commerce à la tête du réseau des chambres de commerce et d'industrie, " définit et suit ", aux termes du 6° de l'article L. 711-16 du même code, " la mise en oeuvre de la politique générale du réseau en matière de gestion des personnels des chambres, négocie et signe les accords nationaux en matière sociale applicables aux personnels des chambres qui sont soumis à un agrément dans des conditions fixées par décret en Conseil d'Etat s'ils ont un impact sur les rémunérations(...) " ; qu'enfin, aux termes de l'article R. 711-55-1 de ce code : " L'agrément par l'autorité de tutelle, mentionné au 6° de l'article L. 711-16, de ceux des accords de portée nationale en matière sociale susceptibles d'avoir un impact sur les rémunérations résulte de leur inscription sur le relevé de décision de la Commission paritaire nationale. En cas de désaccord, l'autorité de tutelle peut demander à CCI France de reprendre les négociations sur l'ensemble ou une partie des points sur lesquels porte l'accord. Les décisions non agréées ne sont pas inscrites sur ce relevé de décisions même si elles ont fait l'objet d'un vote favorable des partenaires sociaux. " ;<br/>
<br/>
              2. Considérant qu'en application de ces dispositions, le ministre de l'économie, de l'industrie et du numérique a notifié le 23 octobre 2014 aux présidents des établissements du réseau des chambres de commerce et d'industrie le relevé des décisions adoptées par la commission paritaire nationale des chambres de commerce et d'industrie dans sa séance du 22 septembre 2014 ; que les requêtes du syndicat CGT du personnel de la CCI Paris Ile-de-France, de la fédération CGT Commerce, distribution, services et de la CFE-CGC Réseaux consulaires sont dirigées contre les mêmes décisions de la commission paritaire nationale des chambres de commerce et d'industrie relatives au statut des personnels administratifs de ces chambres ; qu'il y a lieu de les joindre pour statuer par une seule décision ;<br/>
<br/>
              Sur la légalité externe :<br/>
<br/>
              3. Considérant, en premier lieu, qu'aux termes de l'article 2 de la loi du 10 décembre 1952 : " Chaque commission se compose : / D'un représentant du ministre de tutelle, président ; / De six présidents de chambres désignés par le bureau de l'assemblée des présidents de chambres, dont son président ; / De six représentants du personnel des chambres désignés par les organisations syndicales les plus représentatives. " ; que, selon l'article A. 711-1 du code de commerce dans sa rédaction alors en vigueur, la commission paritaire prévue par ces dispositions " est composée comme suit : / a) Un représentant du ministre chargé de leur tutelle, président de la commission ; / b) Une délégation patronale (...) ; / c) Une délégation du personnel composée de six représentants du personnel et répartie en trois collèges : cadres, agents de maîtrise et employés. Les membres de la délégation sont désignés à l'issue de la consolidation des résultats du premier tour aux élections des commissions paritaires des établissements du réseau, en application de l'article R. 712-11-1, par les organisations représentatives appelées à siéger à la commission paritaire nationale, parmi leurs candidats aux élections des commissions paritaires des établissements du réseau des chambres de commerce et d'industrie. / Le nombre de sièges attribué à chaque collège s'établit selon la règle du quotient électoral et au plus fort reste (...) " ;<br/>
<br/>
              4. Considérant que les dispositions de l'article 1er de la loi du 10 décembre 1952, qui disposent que le ministre de tutelle " nomme " la commission paritaire prévue par cet article, doivent être interprétées comme attribuant compétence à ce ministre non seulement pour nommer individuellement les membres de cette commission, mais également pour prendre les dispositions réglementaires nécessaires pour mettre en oeuvre les dispositions de l'article 2 de la même loi relatives à la composition de la commission ; que, par suite, les syndicats requérants ne sont pas fondés à soutenir que l'article A. 711-1 du code de commerce, issu d'un arrêté du ministre chargé du commerce et de l'industrie et sur le fondement duquel a été composée la délégation du personnel à la commission paritaire nationale, aurait été pris par une autorité incompétente, notamment en ce qu'il dispose que cette délégation est composée de trois collèges ;<br/>
              5. Considérant qu'il est constant que, compte tenu de ses résultats électoraux et eu égard au nombre de sièges fixé pour chacun des trois collèges de la délégation du personnel de la commission paritaire nationale, le syndicat CGT n'a obtenu et ne pouvait obtenir aucun siège ; qu'ainsi, alors même qu'il a obtenu près de 16 % des voix en moyenne, ce syndicat n'était pas au nombre des organisations syndicales les plus représentatives du personnel des chambres pour l'application des articles 1er et 2 de la loi du 10 décembre 1952 qui confient aux commissions paritaires qu'elles instituent le pouvoir de négocier et d'établir le statut des personnels administratifs ; que, par suite, les moyen tirés de ce qu'en ne l'associant pas aux réunions préparatoires et aux négociations conduisant à l'adoption des décisions attaquées, l'administration aurait méconnu le principe d'égalité, le principe de participation des travailleurs à la détermination de leurs conditions de travail résultant du huitième alinéa du préambule de la Constitution du 27 octobre 1946 et le droit de mener des négociations collectives avec l'employeur découlant des stipulations de l'article 11 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ne peuvent qu'être écartés ;<br/>
<br/>
              6. Considérant, en deuxième lieu, que les dispositions de l'article 5.1 de l'annexe 1 à l'article 6 du statut du personnel administratif des chambres de commerce et d'industrie ont pour seul objet de déterminer l'ordre d'examen des questions inscrites à l'ordre du jour des réunions de la commission paritaire nationale " ordinaire " ; qu'elles n'ont ni pour objet ni pour effet de priver cette commission de la possibilité d'adopter un texte rejeté par une délégation à l'occasion des réunions préparatoires ; que, par suite, la circonstance que les membres de la délégation du personnel appartenant à la CFE-CGC se soient opposés, lors de réunions préparatoires, à l'adoption d'une partie du projet qui leur était présenté, est sans incidence sur la régularité des décisions attaquées ;<br/>
<br/>
              7. Considérant, en troisième lieu, qu'aux termes de l'article 4.3 de l'annexe 1 à l'article 6 du statut du personnel administratif des chambres de commerce et d'industrie : " Tout projet de texte modifiant le Statut proposé au vote en CPN doit impérativement avoir fait l'objet d'une discussion en réunion préparatoire et être finalisé " ; qu'il ne ressort pas des pièces du dossier que le projet de texte modifiant le statut du personnel proposé au vote en commission paritaire nationale le 22 septembre 2014 aurait traité de questions nouvelles, différentes de celles qui ont fait l'objet d'une discussion lors de la réunion préparatoire du 30 juin 2014 ; que, par suite, le moyen tiré de ce que ces dispositions auraient été méconnues doit être écarté ;<br/>
<br/>
              8. Considérant, en quatrième lieu, que des discussions collectives organisées avec certaines seulement des organisations syndicales représentatives sont, si elles ont constitué une étape essentielle de la négociation et non pas de simples consultations informelles, de nature à entacher d'irrégularité les décisions de la commission paritaire nationale ; que toutefois, en l'espèce, les membres de la délégation du personnel appartenant à la CFE-CGC ont notamment été mis à même de participer à toutes les réunions préparatoires prévues par le statut du personnel ; qu'ainsi, contrairement à ce que soutient la CFE-CGC, la circonstance que des discussions informelles ont été menées avec les seuls deux autres syndicats représentatifs du personnel des chambres n'est pas de nature à entacher d'illégalité les décisions attaquées ; <br/>
<br/>
              Sur la légalité interne :<br/>
<br/>
              9. Considérant, d'une part, que figure, parmi les décisions adoptées, un " accord relatif à la mutation géographique à l'initiative de l'employeur " ayant pour objet de définir, à la suite du transfert de certains agents des chambres territoriales aux chambres régionales d'industrie en application de la loi du 23 juillet 2010 relative aux réseaux consulaires, la procédure et les mesures d'accompagnement des décisions de mutation géographique et les conséquences d'un refus de mutation géographique ; qu'il ressort des termes mêmes de l'article 2 de cette décision que " la décision de mutation géographique est prise dans le respect de la vie familiale et personnelle de l'agent concerné " ; que la circonstance que la décision en cause ne garantisse pas, par principe, le " reclassement interne " des agents refusant une mutation géographique pour des raisons familiales ou personnelles n'est pas de nature à établir qu'elle porterait, par elle-même, aux droits des agents au respect de leur vie privée et familiale une atteinte disproportionnée au regard des buts en vue desquels elle a été prise ; qu'aucun texte n'impose que l'employeur d'un agent consulaire soit tenu, avant de procéder à une mutation géographique, de rechercher les possibilités de " reclassement interne " de l'intéressé ; <br/>
<br/>
              10. Considérant, d'autre part, que l'article 4 de cette décision dispose qu'en cas de refus, " dans le respect des principes relatifs aux droits de la défense ", l'agent concerné est convoqué à un entretien préalable et peut se faire accompagner par tout agent de son choix appartenant à la chambre de commerce et d'industrie ; que ces dispositions n'ont ni pour objet ni pour effet d'écarter l'application aux agents consulaires de la règle de portée générale énoncée à l'article 6 de la loi du 31 décembre 1971 portant réforme de certaines professions judiciaires et juridiques selon laquelle " les avocats peuvent assister et représenter autrui devant les administrations publiques, sous réserve des dispositions législatives et réglementaires " ; que la disposition attaquée a pu légalement prévoir que la personne, autre qu'un avocat, qui accompagne l'agent en cause doit appartenir à la chambre ; <br/>
<br/>
              11. Considérant qu'il résulte de ce qui précède que les requêtes du syndicat CGT du personnel de la CCI Paris Ile-de-France, de la fédération CGT Commerce, distribution, services et de la CFE-CGC Réseaux consulaires doivent être rejetées, y compris leurs conclusions tendant à l'application de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er  : Les requêtes du syndicat CGT du personnel de la CCI Paris Ile-de-France, de la fédération CGT Commerce, distribution, services et de la CFE-CGC Réseaux consulaires sont rejetées.<br/>
<br/>
Article 2 : La présente décision sera notifiée au syndicat CGT du personnel de la CCI Paris Ile-de-France, à la fédération CGT Commerce, distribution, services, à la CFE-CGC Réseaux consulaires, à CCI France et au ministre de l'économie, de l'industrie et du numérique.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
