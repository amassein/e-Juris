<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043209054</ID>
<ANCIEN_ID>JG_L_2021_03_000000438538</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/20/90/CETATEXT000043209054.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème chambre, 02/03/2021, 438538, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-03-02</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>438538</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>LE PRADO</AVOCATS>
<RAPPORTEUR>Mme Sophie-Caroline de Margerie</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Guillaume Odinet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:438538.20210302</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et trois mémoires en réplique, enregistrés les 10 février et <br/>
20 avril, 27 octobre et 1er décembre 2020 au secrétariat du contentieux du Conseil d'Etat, <br/>
M. C... A... demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret du 23 décembre 2019 lui refusant l'acquisition de la nationalité française ; <br/>
<br/>
              2°) d'enjoindre au ministre de l'intérieur de procéder à l'enregistrement de sa déclaration souscrite en vue d'acquérir la nationalité française dans un délai de deux mois ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat le versement de la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - le code civil ;<br/>
              - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020.<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme D... B..., conseillère d'Etat, <br/>
<br/>
              - les conclusions de M. Guillaume Odinet, rapporteur public,<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à Me Le Prado, avocat de M. A... ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. L'article 21-2 du code civil dispose : " L'étranger ou apatride qui contracte mariage avec un conjoint de nationalité française peut, après un délai de quatre ans à compter du mariage, acquérir la nationalité française par déclaration à condition qu'à la date de cette déclaration la communauté de vie tant affective que matérielle n'ait pas cessé entre les époux depuis le mariage et que le conjoint français ait conservé sa nationalité ". L'article 21-4 du même code prévoit toutefois que : " Le Gouvernement peut s'opposer par décret en Conseil d'Etat, pour indignité ou défaut d'assimilation, à l'acquisition de la nationalité française par le conjoint étranger dans un délai d'un an à compter de la date du récépissé prévu au deuxième alinéa de l'article 26 ou, si l'enregistrement a été refusé, à compter du jour où la décision judiciaire admettant la régularité de la déclaration est passée en force de chose jugée. En cas d'opposition du Gouvernement, l'intéressé est réputé n'avoir jamais acquis la nationalité française. Toutefois, la validité des actes passés entre la déclaration et le décret d'opposition ne pourra être contestée pour le motif que l'auteur n'a pu acquérir la nationalité française ".<br/>
<br/>
              2. M. A..., de nationalité marocaine, a souscrit le 12 juillet 2017 une déclaration d'acquisition de la nationalité française à raison de son mariage. Par le décret attaqué, le Premier ministre s'est opposé à l'acquisition de la nationalité française au motif qu'il ne pouvait être regardé comme digne de l'acquérir. M. A... demande l'annulation pour excès de pouvoir de ce décret.<br/>
<br/>
              3. En premier lieu, le récépissé de la déclaration souscrite par le requérant en vue d'acquérir la nationalité française lui a été délivré le 26 décembre 2017. Le décret portant opposition à son acquisition de la nationalité française a été pris le 23 décembre 2019. Par suite, et alors même qu'il n'a été notifié à l'intéressé que le 6 janvier 2020 et que le récépissé de la déclaration souscrite par le requérant en vue d'acquérir la nationalité française ne lui a pas été délivré le jour même de cette déclaration, le décret attaqué est intervenu dans les conditions de délai prévues à l'article 21-4 du code civil. <br/>
<br/>
              4. En deuxième lieu, le principe de présomption d'innocence ne fait pas obstacle à ce que le Gouvernement s'oppose, pour indignité, à l'acquisition de la nationalité française par le conjoint étranger en se fondant sur des faits qui n'ont pas donné lieu à une condamnation pénale devenue définitive, dès lors que ces faits sont établis. <br/>
<br/>
              5. En troisième lieu, il ressort des pièces du dossier qu'à la date du décret attaqué, M. A... a fait l'objet d'une ordonnance du 10 mai 2019 de renvoi devant le tribunal correctionnel de Nanterre au motif qu'il existe, à son encontre, des charges suffisantes d'avoir, à Châtenay-Malabry (Hauts-de-Seine), Angers (Maine-et-Loire) et au Maroc, entre juin et octobre 2015, trompé les sociétés SFR, GCA TRANS, PSA PEUGEOT CITROEN, et de les avoir déterminées à remettre des fonds, valeurs ou bien quelconque à leur préjudice, faits prévus et réprimés par les article 313-1, 313-7, 313-8 et 131-26-2 du code pénal. <br/>
<br/>
              6. M. A..., qui conteste les faits qui lui sont reprochés dans cette ordonnance, ne saurait soutenir que le Premier ministre a, en se fondant, à la date du décret attaqué, sur ces circonstances pour estimer qu'il devait être regardé, en l'état, comme indigne d'acquérir la nationalité française, compte tenu des faits en cause, de leur gravité, et de leur caractère encore récent à la date du décret, fait une inexacte application de l'article 21-4 du code civil. En outre, les circonstances que M. A... ait obtenu la mainlevée totale du contrôle judiciaire sous lequel il était placé et que, par un jugement du 8 octobre 2020, postérieur à l'édiction du décret, le tribunal correctionnel de Nanterre ait prononcé sa relaxe sur ces faits, sont sans influence sur l'appréciation du Premier ministre à la date de signature du décret contesté.  <br/>
<br/>
              7. En dernier lieu, le décret ne porte, par lui-même, pas atteinte au droit de l'intéressé au respect de sa vie privée et familiale. Ainsi, le moyen tiré de la méconnaissance des stipulations de l'article 8 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ne peut être accueilli.<br/>
<br/>
              8. Il résulte de ce qui précède que M. A... n'est pas fondé à demander l'annulation pour excès de pouvoir du décret du 23 décembre 2019 lui refusant l'acquisition de la nationalité française. Ses conclusions à fin d'injonction, ainsi que celles présentées au titre de l'article L. 761-1 du code de justice administrative ne peuvent, en conséquence, qu'être rejetées.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La requête de M. A... est rejetée.<br/>
Article 2 : La présente décision sera notifiée à M. C... A... et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
