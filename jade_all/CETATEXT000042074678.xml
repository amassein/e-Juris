<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042074678</ID>
<ANCIEN_ID>JG_L_2020_06_000000440944</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/07/46/CETATEXT000042074678.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 19/06/2020, 440944, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-06-19</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>440944</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DE NERVO, POUPET</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2020:440944.20200619</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés les 29 mai et 18 juin 2020 au secrétariat du contentieux du Conseil d'Etat, l'Association des écologistes pour le nucléaire (AEPN), l'association " Fessenheim notre Energie " (FNE), l'association " Initiatives pour le climat et l'énergie " (ICE) et l'Association de défense des actionnaires salariés d'EDF (ADAS) demandent au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative :<br/>
<br/>
              1°) d'ordonner la suspension de l'exécution du décret n° 2020-129 du 18 février 2020 portant abrogation de l'autorisation d'exploiter la centrale nucléaire de Fessenheim, jusqu'à ce que le Conseil d'Etat ait statué au fond sur la requête en annulation formée contre ce même décret ;<br/>
<br/>
              2°) d'enjoindre à la société Electricité de France (EDF) de prendre les mesures nécessaires à la poursuite de l'exploitation de la centrale nucléaire de Fessenheim dans les plus brefs délais ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Elles soutiennent que :<br/>
              - leur requête est recevable dès lors que le Conseil d'Etat est compétent pour en connaître en premier et dernier ressort ;<br/>
              - elles justifient d'un intérêt à agir ;<br/>
              - la condition d'urgence est remplie eu égard, en premier lieu, à l'atteinte grave et immédiate que porte le décret attaqué aux intérêts qu'elles entendent défendre en ce qu'il induit l'arrêt définitif de l'installation nucléaire au 30 juin 2020, en deuxième lieu, à l'atteinte grave et immédiate que porte l'exécution du décret attaqué à un intérêt public en ce qu'elle présente un risque financier pour l'Etat et une perte économique - y compris en tenant compte des investissements à réaliser pour prolonger la vie de la centrale -, affecte l'indépendance énergétique de la France et l'équilibre de sa balance commerciale et entraîne des émissions additionnelles de CO2, en troisième lieu, à l'absence d'urgence à exécuter le décret litigieux, dès lors que la centrale de Fessenheim fonctionne en conformité avec les exigences environnementales et les normes de sécurité et, en dernier lieu, à la circonstance qu'aucun défaut de diligence ne saurait être reproché aux associations demanderesses ;<br/>
              - il existe un doute sérieux quant à la légalité du décret attaqué, dès lors que :<br/>
              * il a été adopté en méconnaissance des dispositions de l'article L. 593-26 du code de l'environnement, dès lors que moins de six mois séparent la déclaration d'arrêt définitif d'exploitation par la société Electricité de France (EDF) de la centrale nucléaire, intervenue le 27 septembre 2019, et la cessation des activités de production, soit un délai insuffisant pour permettre aux autorités compétentes d'exercer un quelconque contrôle, particulièrement nécessaire dans un contexte de réorientation industrielle ;<br/>
              * il a été adopté en méconnaissance des exigences d'information du public prévues par les dispositions combinées des articles L. 593-26 et R. 593-66 du code de l'environnement et de l'article 7 de la Charte de l'environnement, dès lors que la déclaration d'arrêt définitif a d'abord été mise à disposition du public, seulement quelques mois avant la cessation envisagée des activités et sans mise à jour du plan de démantèlement, lequel n'a, en définitive, été communiqué au public que le 6 février 2020, soit seulement quinze jours avant l'arrêt définitif, privant ainsi le public de la garantie de pouvoir prendre connaissance de cette information pour y réagir le cas échéant ;<br/>
              * il est entaché d'une erreur manifeste d'appréciation, dès lors que le plafond de production totale d'électricité d'origine nucléaire fixé à l'article L. 311-5-5 du code de l'énergie ne sera dépassé qu'à la fin de l'année 2022, date à laquelle un nouveau réacteur de la centrale nucléaire de Flamanville sera mis en service ;<br/>
              * il est entaché d'une erreur manifeste d'appréciation au regard des objectifs fixés par les dispositions de l'article L. 311-5 du code de l'énergie, dès lors que la fermeture anticipée de la centrale nucléaire, qui ne répond à aucune raison objective, conduit à une diminution des ressources publiques, menace la sécurité d'approvisionnement électrique de la France et n'est pas en accord avec la programmation pluriannuelle de l'énergie ;<br/>
              * il méconnaît les objectifs fixés par la directive 2008/50/CE du Parlement européen et du Conseil du 21 mai 2008 en ce qu'il entraîne des émissions additionnelles de CO2 de 4 à 10 millions de tonnes par an.<br/>
<br/>
<br/>
              Par un mémoire en défense, enregistré le 12 juin 2020, la ministre de la transition écologique et solidaire conclut au rejet de la requête. Elle soutient que les demanderesses n'ont pas d'intérêt à agir et que les moyens de la requête ne sont pas fondés.<br/>
<br/>
              La requête a été communiquée au Premier ministre, qui n'a pas produit d'observations.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la Constitution, notamment son Préambule ;<br/>
              - le code de l'énergie ;<br/>
              - le code de l'environnement ; <br/>
              - le code de justice administrative et l'ordonnance n° 2020-305 du 25 mars 2020 ;<br/>
              Les parties ont été informées, sur le fondement de l'article 9 de l'ordonnance du 25 mars 2020 portant adaptation des règles applicables devant les juridictions de l'ordre administratif, de ce qu'aucune audience ne se tiendrait et de ce que la clôture de l'instruction serait fixée le 19 juin 2020 à 12 heures.<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
              1. L'article L. 511-1 du code de justice administrative dispose que : " Le juge des référés statue par des mesures qui présentent un caractère provisoire. Il n'est pas saisi du principal et se prononce dans les meilleurs délais. " Aux termes de l'article L. 521-1 du même code : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision. "<br/>
<br/>
              2. Il résulte de l'instruction que, par un courrier du 27 septembre 2019, la société Electricité de France (EDF) a déclaré à la ministre de la transition écologique et solidaire mettre à l'arrêt définitif les deux réacteurs de la centrale nucléaire de Fessenheim : le 22 février 2020 pour le réacteur numéro 1 et le 30 juin 2020 pour le réacteur numéro 2. Par un autre courrier, daté du 30 septembre 2019, la société EDF a demandé à la ministre de la transition écologique et solidaire de prononcer l'abrogation avec effet au plus tard au 31 décembre 2020 de l'autorisation d'exploiter la centrale nucléaire de Fessenheim dont elle était titulaire. Par le décret n° 2020-129 du 18 février 2020, dont la suspension est demandée au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative, par l'Association des écologistes pour le nucléaire (AEPN), l'association " Fessenheim notre Energie " (FNE), l'association " Initiatives pour le climat et l'énergie " (ICE) et l'Association de défense des actionnaires salariés d'EDF (ADAS), le Premier ministre a abrogé l'autorisation d'exploiter la centrale nucléaire de Fessenheim dont la société EDF était titulaire, et ce à compter des dates d'arrêt définitif déclarées par la société pour chacun des deux réacteurs.<br/>
<br/>
              3. En premier lieu, aux termes de l'article L. 311-5 du code de l'énergie : " L'autorisation d'exploiter une installation de production d'électricité est délivrée par l'autorité administrative (...) ". Aux termes du second alinéa de l'article L. 311-6 du code du même code : " (...) Les installations existantes, régulièrement établies au 11 février 2000, sont également réputées autorisées. ". Aux termes de son article L. 311-5-5 : " L'autorisation mentionnée à l'article L. 311-1 ne peut être délivrée lorsqu'elle aurait pour effet de porter la capacité totale autorisée de production d'électricité d'origine nucléaire au-delà de 63,2 gigawatts. / L'autorité administrative, pour apprécier la capacité totale autorisée, prend en compte les abrogations prononcées par décret à la demande du titulaire d'une autorisation, y compris si celle-ci résulte de l'application du second alinéa de l'article L. 311-6. ". Aux termes de son article L. 311-8 : " L'octroi d'une autorisation au titre de la présente section ne dispense pas son bénéficiaire d'obtenir les titres requis par d'autres législations. " Il résulte de ces dispositions qu'en l'absence de tout manquement de la part de l'exploitant, l'abrogation d'une autorisation d'exploiter une installation de production d'électricité d'origine nucléaire ne peut intervenir que sur demande de son titulaire. Il en résulte également que l'octroi ou l'abrogation d'une telle autorisation demeure sans incidence sur les exigences pouvant peser sur l'exploitant au titre d'autres législations, notamment celle relative aux installations nucléaires de base figurant au chapitre III, titre IX, livre V du code de l'environnement.<br/>
<br/>
              4. En second lieu, aux termes de l'article L. 593-25 du code de l'environnement : " Lorsque le fonctionnement d'une installation nucléaire de base ou d'une partie d'une telle installation est arrêté définitivement, son exploitant procède à son démantèlement dans un délai aussi court que possible (...). Les délais et conditions de réalisation du démantèlement sont fixés par le décret mentionné à l'article L. 593-28. " Aux termes de l'article L. 593-26 du même code : " Lorsque l'exploitant prévoit d'arrêter définitivement le fonctionnement de son installation ou d'une partie de son installation, il le déclare au ministre chargé de la sûreté nucléaire et à l'Autorité de sûreté nucléaire. Il indique dans sa déclaration la date à laquelle cet arrêt doit intervenir (...). / L'exploitant n'est plus autorisé à faire fonctionner l'installation à compter de cette date. / Jusqu'à l'entrée en vigueur du décret de démantèlement mentionné à l'article L. 593-28, l'installation reste soumise aux dispositions de son autorisation mentionnée à l'article L. 593-7 et aux prescriptions définies par l'Autorité de sûreté nucléaire, ces dernières pouvant être complétées ou modifiées en tant que de besoin. " Aux termes de l'article L. 593-28 du même code : " Le démantèlement de l'installation nucléaire de base ou de la partie d'installation à l'arrêt définitif est (...) prescrit par décret pris après avis de l'Autorité de sûreté nucléaire et après l'accomplissement d'une enquête publique (...). / Le décret fixe les caractéristiques du démantèlement, son délai de réalisation et, le cas échéant, les opérations à la charge de l'exploitant après le démantèlement. " Il résulte de la lettre même de ces dispositions que l'arrêt définitif du fonctionnement d'une installation nucléaire de base résulte de la déclaration faite en ce sens par l'exploitant au ministre chargé de la sécurité nucléaire et que l'exploitant n'est plus autorisé à faire fonctionner l'installation à compter de la date d'arrêt définitif qu'il a lui-même déclarée. Il résulte en outre de ces dispositions que si l'arrêt définitif du fonctionnement d'une installation nucléaire de base doit entraîner son démantèlement dans un délai aussi court que possible, ce démantèlement ne peut intervenir que sur le fondement d'un décret pris après avis de l'Autorité de sûreté nucléaire et après l'accomplissement d'une enquête publique et que, dans l'intervalle, l'installation demeure soumise aux prescriptions de son autorisation initiale complétée, le cas échéant, de celles de l'Autorité de sûreté nucléaire.<br/>
<br/>
              5. Il se déduit de ce qui a été dit au point précédent que l'arrêt définitif des deux réacteurs de la centrale nucléaire de Fessenheim, le 22 février pour l'un et le 30 juin 2020 pour l'autre, résulte de la déclaration faite en ce sens le 27 septembre 2019 à la ministre de la transition écologique et solidaire par le président de la société EDF, sur le seul fondement de l'article L. 593-26 du code l'environnement. Par suite, à la supposer justifiée, la suspension, sur le fondement de l'article L. 521-1 du code de justice administrative, de l'exécution du décret portant abrogation de l'autorisation d'exploiter les deux réacteurs serait dépourvue de tout effet sur le fonctionnement de la centrale, dont l'arrêt définitif comme, d'ailleurs, le jour venu, le démantèlement, résultent ainsi qu'il vient d'être dit d'actes distincts, relevant d'une autre législation que celle relative à l'exploitation d'une installation de production d'électricité. Cette suspension serait en conséquence sans effet sur les intérêts que les demandeurs défendent. Il n'apparaît pas plus, en l'état de l'instruction et pour le même motif, qu'une telle suspension permettrait de prévenir une éventuelle atteinte grave et immédiate aux intérêts publics dont les demandeurs font état.<br/>
<br/>
              6. Il résulte de tout ce qui précède, sans qu'il soit besoin d'examiner les autres moyens soulevés, que l'Association des écologistes pour le nucléaire (AEPN), l'association " Fessenheim notre Energie " (FNE), l'association " Initiatives pour le climat et l'énergie " (ICE) et l'Association de défense des actionnaires salariés d'EDF (ADAS) ne sont pas fondées à demander la suspension de l'exécution du décret du 18 février 2020 portant abrogation de l'autorisation d'exploiter la centrale nucléaire de Fessenheim. Par suite, leurs conclusions tendant à ce qu'il soit enjoint à la société EDF de prendre les mesures nécessaires à la poursuite de l'exploitation de la centrale nucléaire de Fessenheim ne peuvent, en tout état de cause, qu'être rejetées. Il en va de même, par voie de conséquence, de leurs conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête présentée par l'Association des écologistes pour le nucléaire (AEPN), l'association " Fessenheim notre Energie " (FNE), l'association " Initiatives pour le climat et l'énergie " (ICE) et l'Association de défense des actionnaires salariés d'EDF (ADAS) est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à l'Association des écologistes pour le nucléaire, premier dénommé pour l'ensemble des requérants, au Premier ministre et à la ministre de la transition écologique et solidaire.<br/>
Copie en sera adressée à l'Autorité de sûreté nucléaire et à la société Electricité de France.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
