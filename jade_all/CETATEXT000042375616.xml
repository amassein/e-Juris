<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042375616</ID>
<ANCIEN_ID>JG_L_2020_09_000000424542</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/37/56/CETATEXT000042375616.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème chambre, 28/09/2020, 424542, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-09-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>424542</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FOUSSARD, FROGER ; SCP ROUSSEAU, TAPIE</AVOCATS>
<RAPPORTEUR>M. Olivier Rousselle</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Cécile Barrois de Sarigny</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2020:424542.20200928</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société Embe a demandé au tribunal administratif de Bordeaux d'annuler pour excès de pouvoir l'arrêté du 23 juin 2014 par lequel le maire de la commune de Floirac l'a mise en demeure de mettre en oeuvre plusieurs mesures de sécurité sur des parcelles dont elle est propriétaire. Par un jugement n° 1403564 du 31 octobre 2016, le tribunal administratif a rejeté sa demande. <br/>
<br/>
              Par un arrêt n°16BX04287 du 27 juillet 2018, la cour administrative d'appel de Bordeaux a, sur appel de la société Embe, annulé ce jugement et l'arrêté du 23 juin 2014.<br/>
<br/>
              Par un pourvoi et un mémoire complémentaire, enregistrés les 17 septembre et 27 décembre 2018 au secrétariat du contentieux du Conseil d'Etat, la commune de Floirac demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter l'appel de la société Embe ;<br/>
<br/>
              3°) de mettre à la charge de la société Embe la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code général des collectivités territoriales ;<br/>
              - la loi n° 79-587 du 11 juillet 1979 <br/>
              - la loi n° 2000-321 du 12 avril 2000 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Olivier Rousselle, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Cécile Barrois de Sarigny, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Foussard, Froger, avocat de la commune de Floirac et à la SCP Rousseau, Tapie, avocat de la société Embe.<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond qu'à la suite d'un signalement émanant de la société occupant les terrains dont la société Embe est propriétaire dans la commune de Floirac (Gironde), le maire de la commune a demandé à plusieurs reprises à la société Embe d'effectuer des travaux de mise en sécurité du site puis, par un arrêté du 23 juin 2014, a mis cette société et son gérant en demeure de mettre en oeuvre à leurs frais plusieurs mesures visant à faire cesser les éboulements de terre et de rochers sur le terrain. La commune de Floirac demande l'annulation de l'arrêt du 27 juillet 2018 par lequel la cour administrative d'appel de Bordeaux a, sur appel de la société Embe dirigé contre le jugement du 31 octobre 2016 du tribunal administratif de Bordeaux, annulé l'arrêté du 23 juin 2014 au motif qu'il avait été pris sans qu'ait été respectée la procédure contradictoire prévue par l'article 24 de la loi du 12 avril 2000 relative aux droits des citoyens dans leurs relations avec les administrations.<br/>
<br/>
              2. Aux termes de cet article 24 de la loi du 12 avril 2000, applicable au présent litige : " Exception faite des cas où il est statué sur une demande, les décisions individuelles qui doivent être motivées en application des articles 1er et 2 de la loi n° 79-587 du 11 juillet 1979 relative à la motivation des actes administratifs et à l'amélioration des relations entre l'administration et le public n'interviennent qu'après que la personne intéressée a été mise à même de présenter des observations écrites et, le cas échéant, sur sa demande, des observations orales ". Ces dispositions impliquent que la personne intéressée ait été avertie de la mesure que l'administration envisage de prendre, des motifs sur lesquels elle se fonde et qu'elle bénéficie d'un délai suffisant pour présenter ses observations.<br/>
<br/>
              3. En premier lieu, en jugeant que l'arrêté litigieux n'était pas intervenu sur demande de la société Embe, avait le caractère d'une mesure de police et qu'il était, par suite, soumis à la procédure contradictoire prévue par les dispositions de l'article 24 de la loi du 12 avril 2000, la cour n'a pas dénaturé les pièces du dossier qui lui était soumis et n'a pas commis d'erreur de droit. <br/>
<br/>
              4. En deuxième lieu, en estimant que ni les demandes adressées, ainsi qu'il a été dit au point 1, à la société Embe et à son gérant par la commune, ni les autres échanges relatifs aux travaux de mise en sécurité du site n'avaient informé la société, préalablement à l'édiction de l'arrêté litigieux, de la teneur exacte des prescriptions que le maire entendait lui imposer, la cour n'a pas dénaturé les pièces du dossier. En en déduisant que les dispositions de l'article 24 de la loi du 12 avril 2000, citées ci-dessus, avaient été méconnues, elle n'a pas commis d'erreur de droit.<br/>
<br/>
              5. Enfin, en jugeant que la mise en oeuvre de la procédure contradictoire prévue par les dispositions de l'article 24 de la loi du 12 avril 2000 constituait une garantie pour la société Embe et que leur méconnaissance entraînait, par suite, l'irrégularité de l'arrêté litigieux, la cour n'a ni insuffisamment motivé son arrêt ni commis d'erreur de droit.<br/>
<br/>
              6. Il résulte de tout ce qui précède que la commune de Floirac n'est pas fondée à demander l'annulation de l'arrêt de cour administrative d'appel de Bordeaux du 27 juillet 2018.<br/>
<br/>
              7. Les dispositions de l'article L.761-1 du code de justice administrative font obstacle à ce que soit mise à la charge de la société Embe, qui n'est pas la partie perdante dans la présente instance, la somme que demande à ce titre la commune de Floirac. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la commune de Floirac une somme de 3 000 euros à verser, au même titre, à la société Embe.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
		Article 1er : Le pourvoi de la commune de Floirac est rejeté.<br/>
<br/>
Article 2 :  La commune de Floirac versera à la société Embe la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 3 : La présente décision sera notifiée à la commune de Floirac et à la société Embe.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
