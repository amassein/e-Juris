<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039161392</ID>
<ANCIEN_ID>JG_L_2019_09_000000421889</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/16/13/CETATEXT000039161392.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème - 6ème chambres réunies, 30/09/2019, 421889</TITRE>
<DATE_DEC>2019-09-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>421889</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème - 6ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP L. POULET, ODENT ; SCP NICOLAY, DE LANOUVELLE, HANNOTIN</AVOCATS>
<RAPPORTEUR>M. Florian Roussel</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Cécile Barrois de Sarigny</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:XX:2019:421889.20190930</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société du Mouliès a demandé au tribunal administratif de Pau de condamner la commune de Parentis-en-Born à lui verser une indemnité de 1 610 255 euros en réparation des préjudices qu'elle estime avoir subis du fait de l'annulation de la délibération du 18 décembre 2006 de son conseil municipal approuvant le plan local d'urbanisme de la commune. Par un jugement n° 1201646 du 16 décembre 2014, le tribunal administratif a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 15BX00479 du 3 mai 2018, la cour administrative d'appel de Bordeaux a rejeté l'appel formé par la société du Mouliès contre ce jugement.  <br/>
<br/>
              Par un pourvoi sommaire et deux mémoires complémentaires, enregistrés les 2 juillet et 27 septembre 2018 et le 29 mars 2019 au secrétariat du contentieux du Conseil d'Etat, la société du Mouliès demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de la commune de Parentis-en-Born la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de l'urbanisme ;<br/>
              - la loi n° 2018-1021 du 23 novembre 2018 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Florian Roussel, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Cécile Barrois de Sarigny, rapporteur public,<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP L. Poulet, Odent, avocat de la SARL du Mouliès  et à la SCP Nicolaÿ, de Lanouvelle, Hannotin, avocat de la commune de Parentis-en-Born ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que le maire de Parentis-en-Born (Landes) a, par un arrêté du 20 octobre 2008, accordé à la société du Mouliès un permis d'aménager pour un projet de lotissement sur le territoire de la commune, au vu du plan local d'urbanisme alors en vigueur, qui avait été approuvé par une délibération du 18 décembre 2006. Toutefois, le tribunal administratif de Pau a annulé cette délibération du 18 décembre 2006 par un jugement du 1er décembre 2009, devenu définitif, qui a eu pour effet de remettre rétroactivement en vigueur, en vertu de l'article L. 121-8 du code de l'urbanisme alors applicable, devenu l'article L. 600-12 du même code, le plan d'occupation des sols immédiatement antérieur, approuvé le 18 décembre 1998, dont le règlement classait les parcelles du projet de lotissement en zone non constructible. <br/>
<br/>
              2. La société du Mouliès se pourvoit en cassation contre l'arrêt du 3 mai 2018 par lequel la cour administrative d'appel de Bordeaux a rejeté l'appel qu'elle avait formé contre le jugement du 16 décembre 2014 par lequel le tribunal administratif de Pau a rejeté sa demande tendant à ce que la commune soit condamnée à l'indemniser du préjudice résultant, selon elle, de l'impossibilité de réaliser le projet de construction envisagé du fait de l'illégalité du plan local d'urbanisme du 18 décembre 2006.<br/>
<br/>
              3. Aux termes de l'article L. 442-14 du code de l'urbanisme dans sa rédaction applicable à l'espèce : " Dans les cinq ans suivant l'achèvement d'un lotissement, constaté dans les conditions prévues par décret en Conseil d'Etat, le permis de construire ne peut être refusé ou assorti de prescriptions spéciales sur le fondement de dispositions d'urbanisme intervenues postérieurement à l'autorisation du lotissement ". Si ces dispositions font obstacle à ce que, dans le délai qu'elles prévoient, des dispositions d'urbanisme adoptées après l'autorisation du lotissement puissent fonder un refus de permis de construire au sein de ce lotissement, elles n'ont, en revanche, pas pour effet de faire obstacle à un refus fondé sur des dispositions d'urbanisme antérieures remises en vigueur, conformément aux dispositions de l'article L. 121-8 du code de l'urbanisme, devenu l'article L.600-12 du même code, par l'effet d'une annulation contentieuse intervenue postérieurement à l'autorisation du lotissement. Ce n'est d'ailleurs que la loi du 23 novembre 2018 portant évolution du logement, de l'aménagement et du numérique, inapplicable en l'espèce, qui a ajouté à l'article L. 442-14 du code de l'urbanisme un dernier alinéa selon lequel " l'annulation, totale ou partielle, ou la déclaration d'illégalité d'un schéma de cohérence territoriale, d'un plan local d'urbanisme, d'un document d'urbanisme en tenant lieu ou d'une carte communale pour un motif étranger aux règles d'urbanisme applicables au lotissement ne fait pas obstacle, pour l'application du présent article, au maintien de l'application des règles au vu desquelles le permis d'aménager a été accordé ou la décision de non-opposition a été prise ".<br/>
<br/>
              4. Il résulte de ce qui précède qu'en se fondant, pour juger qu'il n'existait pas de lien de causalité direct entre l'illégalité du plan local d'urbanisme du 18 décembre 2006 ayant conduit à son annulation et l'impossibilité, pour la société du Mouliès, de réaliser le lotissement en litige, sur la circonstance que les dispositions alors applicables de l'article L. 442-14 du code de l'urbanisme faisaient obstacle à ce que les disposition du plan d'occupation des sols du 18 décembre 1998 puissent être opposées aux demandes de permis de construire présentées dans le délai de cinq ans suivant l'achèvement du lotissement, la cour administrative d'appel a entaché son arrêt d'une erreur de droit. Par suite, sans qu'il soit besoin d'examiner les autres moyens de son pourvoi, la société du Mouliès est fondée à demander l'annulation de l'arrêt qu'elle attaque.<br/>
<br/>
              5. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mise à la charge de la société du Mouliès la somme que demande, à ce titre, la commune de Parentis-en-Born. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la commune de Parentis-en-Born le versement à la société du Mouliès d'une somme de 3 000 euros au titre de ces mêmes dispositions.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Bordeaux du 3 mai 2018 est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Bordeaux.<br/>
Article 3 : La commune de Parentis-en-Born versera à la société du Mouliès une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Les conclusions présentées par la commune de Parentis-en-Born au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée à la société du Mouliès et à la commune de Parentis-en-Born.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">68-02-04-03 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. PROCÉDURES D'INTERVENTION FONCIÈRE. LOTISSEMENTS. RÉALISATION DU LOTISSEMENT. - DÉLAI DURANT LEQUEL NE SONT PAS OPPOSABLES À UNE DEMANDE DE PERMIS DE CONSTRUIRE LES RÈGLES D'URBANISME POSTÉRIEURES À L'AUTORISATION DE LOTIR (ART. L. 442-14 DU CODE DE L'URBANISME) - APPLICATION AUX RÈGLES D'URBANISME REMISES EN VIGUEUR DU FAIT D'UNE ANNULATION CONTENTIEUSE - ABSENCE.
</SCT>
<ANA ID="9A"> 68-02-04-03 Si l'article L. 442-14 du code de l'urbanisme fait obstacle à ce que, dans le délai de cinq ans suivant l'achèvement d'un lotissement, des dispositions d'urbanisme adoptées après l'autorisation du lotissement puissent fonder un refus de permis de construire au sein de ce lotissement, il n'a, en revanche, pas pour effet de faire obstacle à un refus fondé sur des dispositions d'urbanisme qui auraient été seulement remises en vigueur, conformément aux dispositions de l'article L. 121-8 du code de l'urbanisme, devenu l'article L. 600-12 du même code, par l'effet d'une annulation contentieuse intervenue postérieurement à l'autorisation du lotissement.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
