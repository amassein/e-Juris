<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028090977</ID>
<ANCIEN_ID>JG_L_2013_10_000000370481</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/09/09/CETATEXT000028090977.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème et 1ère sous-sections réunies, 17/10/2013, 370481</TITRE>
<DATE_DEC>2013-10-17</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>370481</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème et 1ère sous-sections réunies</FORMATION>
<TYPE_REC>Autres</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROGER, SEVAUX, MATHONNET</AVOCATS>
<RAPPORTEUR>Mme Sophie Roussel</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Suzanne von Coester</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:370481.20131017</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu l'ordonnance n° 12NC01691 du 22 juillet 2013 par laquelle le président de la 1ère chambre de la cour administrative d'appel de Nancy, avant qu'il soit statué sur l'appel de la commune d'Illkirch-Graffenstaden tendant à l'annulation du jugement n° 0504645 du 11 juin 2009 par lequel le tribunal administratif de Strasbourg a rejeté sa demande tendant à l'annulation de l'arrêté du 16 juin 2005 par lequel le préfet du Bas-Rhin a autorisé la société Entreprise de Travaux et Matériaux (ETM) à mettre en sécurité les berges de la gravière situées route d'Eschau par remblayage avec apport de matériaux, a décidé, par application des dispositions de l'article 23-2 de l'ordonnance n° 58-1067 du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel, de transmettre au Conseil d'Etat la question de la conformité aux droits et libertés garantis par la Constitution de l'article L. 512-3 du code de l'environnement ; <br/>
<br/>
<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 2 octobre 2013, présentée pour la société Entreprise de Travaux et Matériaux ; <br/>
<br/>
              Vu la Constitution, notamment son Préambule et son article 61-1 ;<br/>
<br/>
              Vu l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
<br/>
              Vu le code de l'environnement ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Sophie Roussel, Auditeur,  <br/>
<br/>
              - les conclusions de Mme Suzanne von Coester, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Roger, Sevaux, Mathonnet, avocat de la société Entreprise de travaux et matériaux ;<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il résulte des dispositions de l'article 23-4 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel que, lorsqu'une juridiction relevant du Conseil d'Etat a transmis à ce dernier, en application de l'article 23-2 de cette même ordonnance, la question de la conformité à la Constitution d'une disposition législative, le Conseil constitutionnel est saisi de cette question de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux ;<br/>
<br/>
              2. Considérant, en premier lieu, que la commune d'Illkirch-Graffenstaden soutient que les dispositions de l'article L. 512-3 du code de l'environnement méconnaissent l'article 7 de la Charte de l'environnement, en tant qu'elles ne prévoient pas de procédure permettant au public de participer à l'élaboration des arrêtés complémentaires pris postérieurement à l'autorisation initiale d'exploiter une installation classée pour la protection de l'environnement ; que, toutefois, la faculté réservée à l'autorité administrative de compléter, par des arrêtés complémentaires, l'autorisation initiale d'exploiter une installation classée pour la protection de l'environnement, accordée notamment, en vertu de l'article L. 512-2  du code, après une enquête publique relative aux incidences éventuelles du projet sur les intérêts mentionnés à l'article L. 511-1 et après avis des conseils municipaux intéressés, est réservée aux cas dans lesquels les modifications apportées à l'installation, à son mode d'utilisation ou à son voisinage ne sont pas substantielles ; que si les modifications apportées par l'arrêté complémentaire sont de nature à entraîner des dangers ou inconvénients nouveaux ou à accroître de manière sensible les dangers ou les inconvénients de l'installation, une nouvelle autorisation, instruite selon les modalités de la demande initiale et soumise notamment à enquête publique, doit être sollicitée ; que, dès lors, les arrêtés complémentaires prévus par l'article L. 512-3 du code de l'environnement ne constituent pas des décisions ayant une incidence significative sur l'environnement et ne sont pas au nombre des décisions visées par l'article 7 de la Charte de l'environnement ; que, par suite, la question de la conformité des dispositions contestées à l'article 7 de la Charte de l'environnement, qui n'est pas nouvelle, ne présente pas un caractère sérieux ; <br/>
<br/>
              3. Considérant, en second lieu, que la requérante soutient que l'article L. 512-3 du code de l'environnement méconnaît également l'article 17 de la Déclaration des droits de l'homme et du citoyen, en ce qu'il permettrait à l'autorité administrative d'imposer, sans limitation dans le temps, des sujétions portant atteinte au droit de propriété ; que, toutefois, la faculté d'édicter des arrêtés complétant ou modifiant les prescriptions applicables à une installation classée, notamment en ce qui concerne la remise en état du site, n'a ni pour objet, ni pour effet d'autoriser la dépossession d'un bien et n'entre dès lors pas dans le champ d'application de la garantie instituée à l'article 17 de la Déclaration des droits de l'homme et du citoyen du 26 août 1789 ; que, par suite, la question de la conformité des dispositions contestées à l'article 17 de la Déclaration des droits de l'homme et du citoyen, qui n'est pas nouvelle, ne présente pas un caractère sérieux ; <br/>
<br/>
              4. Considérant qu'il résulte de ce qui précède qu'il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité invoquée ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité transmise par la cour administrative d'appel de Nancy. <br/>
Article 2 : La présente décision sera notifiée à la commune d'Illkirch-Graffenstaden, au ministre de l'écologie, du développement durable et de l'énergie et à la société Entreprise de travaux et de matériaux.<br/>
Copie en sera adressée au Conseil constitutionnel, au Premier ministre ainsi qu'à la cour administrative d'appel de Nancy. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-04-005 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - VIOLATION DIRECTE DE LA RÈGLE DE DROIT. CONSTITUTION ET PRINCIPES DE VALEUR CONSTITUTIONNELLE. - CHARTE DE L'ENVIRONNEMENT - PRINCIPE DE PARTICIPATION À L'ÉLABORATION DES DÉCISIONS PUBLIQUES AYANT UNE INCIDENCE SUR L'ENVIRONNEMENT (ART. 7) - NOTION DE DÉCISION PUBLIQUE AYANT UNE INCIDENCE SUR L'ENVIRONNEMENT - ARRÊTÉS COMPLÉMENTAIRES À UNE AUTORISATION D'EXPLOITER UNE ICPE (ART. L. 512-3 DU CODE DE L'ENVIRONNEMENT) - EXCLUSION, EN L'ABSENCE D'INCIDENCE SIGNIFICATIVE - CONSÉQUENCE - QPC ARGUANT DE LA VIOLATION DE L'ARTICLE 7 - ABSENCE DE CARACTÈRE SÉRIEUX.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">44-005-07-01 NATURE ET ENVIRONNEMENT. - NOTION DE DÉCISION PUBLIQUE AYANT UNE INCIDENCE SUR L'ENVIRONNEMENT - ARRÊTÉS COMPLÉMENTAIRES À UNE AUTORISATION D'EXPLOITER UNE ICPE (ART. L. 512-3 DU CODE DE L'ENVIRONNEMENT) - EXCLUSION, EN L'ABSENCE D'INCIDENCE SIGNIFICATIVE.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">44-02-02-01-02 NATURE ET ENVIRONNEMENT. INSTALLATIONS CLASSÉES POUR LA PROTECTION DE L'ENVIRONNEMENT. RÉGIME JURIDIQUE. POUVOIRS DU PRÉFET. MODIFICATION DES PRESCRIPTIONS IMPOSÉES AUX TITULAIRES. - ARRÊTÉS COMPLÉMENTAIRES (ART. L. 512-3 DU CODE DE L'ENVIRONNEMENT) - NOTION DE DÉCISION PUBLIQUE AYANT UNE INCIDENCE SUR L'ENVIRONNEMENT AU SENS DE L'ARTICLE 7 DE LA CHARTE DE L'ENVIRONNEMENT (PRINCIPE DE PARTICIPATION À L'ÉLABORATION DE TELLES DÉCISIONS) - EXCLUSION, EN L'ABSENCE D'INCIDENCE SIGNIFICATIVE.
</SCT>
<SCT ID="8D" TYPE="PRINCIPAL">54-10-05-04-02 PROCÉDURE. - QUESTION DE LA CONFORMITÉ À L'ARTICLE 7 DE LA CHARTE DE L'ENVIRONNEMENT (PRINCIPE DE PARTICIPATION À L'ÉLABORATION DES DÉCISIONS PUBLIQUES AYANT UNE INCIDENCE SUR L'ENVIRONNEMENT) DE L'ARTICLE L. 512-3 DU CODE DE L'ENVIRONNEMENT (FACULTÉ DE COMPLÉTER PAR ARRÊTÉ L'AUTORISATION D'EXPLOITER UNE ICPE) - ABSENCE DE CARACTÈRE SÉRIEUX, LES ARRÊTÉS COMPLÉMENTAIRES NE CONSTITUANT PAS DES DÉCISIONS PUBLIQUES AYANT UNE INCIDENCE SUR L'ENVIRONNEMENT, EN L'ABSENCE D'INCIDENCE SIGNIFICATIVE.
</SCT>
<ANA ID="9A"> 01-04-005 Les arrêtés complémentaires que l'autorité administrative peut prendre sur le fondement de l'article L. 512-3 du code de l'environnement pour compléter l'autorisation initiale d'exploiter une installation classée pour la protection de l'environnement (ICPE), réservés aux cas dans lesquels les modifications apportées à l'installation, à son mode d'utilisation ou à son voisinage ne sont pas substantielles, ne constituent pas des décisions ayant une incidence significative sur l'environnement et ne sont pas au nombre des décisions visées par l'article 7 de la Charte de l'environnement. La question prioritaire de constitutionnalité (QPC) portant sur la conformité des dispositions de l'article L. 512-3 du code de l'environnement à l'article 7 de la Charte de l'environnement ne présente donc pas un caractère sérieux.</ANA>
<ANA ID="9B"> 44-005-07-01 Les arrêtés complémentaires que l'autorité administrative peut prendre sur le fondement de l'article L. 512-3 du code de l'environnement pour compléter l'autorisation initiale d'exploiter une installation classée pour la protection de l'environnement (ICPE), réservés aux cas dans lesquels les modifications apportées à l'installation, à son mode d'utilisation ou à son voisinage ne sont pas substantielles, ne constituent pas des décisions ayant une incidence significative sur l'environnement et ne sont pas au nombre des décisions visées par l'article 7 de la Charte de l'environnement.</ANA>
<ANA ID="9C"> 44-02-02-01-02 Les arrêtés complémentaires que l'autorité administrative peut prendre sur le fondement de l'article L. 512-3 du code de l'environnement pour compléter l'autorisation initiale d'exploiter une installation classée pour la protection de l'environnement (ICPE), réservés aux cas dans lesquels les modifications apportées à l'installation, à son mode d'utilisation ou à son voisinage ne sont pas substantielles, ne constituent pas des décisions ayant une incidence significative sur l'environnement et ne sont pas au nombre des décisions visées par l'article 7 de la Charte de l'environnement.</ANA>
<ANA ID="9D"> 54-10-05-04-02 Les arrêtés complémentaires que l'autorité administrative peut prendre sur le fondement de l'article L. 512-3 du code de l'environnement pour compléter l'autorisation initiale d'exploiter une installation classée pour la protection de l'environnement (ICPE), réservés aux cas dans lesquels les modifications apportées à l'installation, à son mode d'utilisation ou à son voisinage ne sont pas substantielles, ne constituent pas des décisions ayant une incidence significative sur l'environnement et ne sont pas au nombre des décisions visées par l'article 7 de la Charte de l'environnement. La question de la conformité des dispositions de l'article L. 512-3 du code de l'environnement à l'article 7 de la Charte de l'environnement ne présente donc pas un caractère sérieux.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
