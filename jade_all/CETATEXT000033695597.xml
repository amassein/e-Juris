<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000033695597</ID>
<ANCIEN_ID>JG_L_2016_12_000000398074</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/33/69/55/CETATEXT000033695597.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème - 7ème chambres réunies, 23/12/2016, 398074</TITRE>
<DATE_DEC>2016-12-23</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>398074</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème - 7ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>RICARD ; SCP MATUCHANSKY, POUPOT, VALDELIEVRE</AVOCATS>
<RAPPORTEUR>M. Yves Doutriaux</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Xavier Domino</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2016:398074.20161223</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 18 mars, 18 mai et 30 août  2016 au secrétariat du contentieux du Conseil d'Etat, Mme A...C...demande au Conseil d'Etat l'annulation de la décision du 23 avril 2015, corrigée le 20 mai suivant, de l'Agence française de lutte contre le dopage (AFLD) prononçant à son encontre la sanction de l'interdiction de participer pendant trois ans aux compétitions et manifestations sportives organisées ou autorisées par les fédérations sportives françaises.<br/>
<br/>
<br/>
              Vu :<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - la convention internationale contre le dopage dans le sport, ensemble ses annexes et appendices ; <br/>
              - le code du sport ;<br/>
              - le décret n° 2014-1005 du 4 septembre 2014 ;<br/>
              - le code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Yves Doutriaux, conseiller d'Etat, <br/>
<br/>
              - les conclusions de M. Xavier Domino, rapporteur public,<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Ricard, avocat de MmeC..., et à la SCP Matuchansky, Poupot, Valdelièvre, avocat de l'Agence française de lutte contre le dopage ; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1.	Considérant que MmeC..., qui figure sur la liste du " groupe cible " de la fédération internationale d'athlétisme, s'est soustraite le 7 novembre 2014 à un contrôle inopiné diligenté par le directeur des contrôles de l'agence française de lutte contre le dopage à Matemale (Pyrénées-Orientales), puis a fait l'objet, le même jour, dans les locaux de la gendarmerie de Prades, de prélèvements urinaires et sanguins ; que les analyses effectuées par le département des analyses de l'Agence française de lutte contre le dopage ont fait ressortir la présence d'érythropoïétine ; que la fédération française d'athlétisme ayant indiqué, par lettre du 24 novembre 2014, que Mme C...n'était plus licenciée, l'Agence française de lutte contre le dopage a, par une décision du 23 avril 2015 rectifiée le 20 mai suivant, prononcé à son encontre la sanction d'interdiction de participer pendant trois ans aux compétitions et manifestations sportives organisées ou autorisées par les fédérations sportives françaises ; que Mme C...demande l'annulation de cette décision ;<br/>
<br/>
              2.	Considérant que la décision attaquée, qui énonce les considérations de fait et de droit sur lesquelles elle se fonde, est suffisamment motivée ;  <br/>
<br/>
              3.	Considérant qu'aux termes de l'article L. 232-5 du code du sport : " I.- L'Agence française de lutte contre le dopage, autorité publique indépendante dotée de la personnalité morale, définit et met en oeuvre les actions de lutte contre le dopage (...)./ A cet effet : 1° Elle définit un programme annuel de contrôles ; 2° Elle diligente les contrôles (...) 6° Elle réalise ou fait réaliser l'analyse des prélèvements effectués lors des contrôles ; 7° elle exerce un pouvoir disciplinaire (...)/ II.- Les missions de contrôle, les missions d'analyse et les compétences disciplinaires de l'Agence française de lutte contre le dopage ne peuvent être exercées par les mêmes personnes. " ; qu'aux termes de l'article L. 232-18 de ce code : " Les analyses des prélèvements effectués par l'Agence française de lutte contre le dopage sont réalisées sous la responsabilité scientifique et technique du directeur du département des analyses... " ; qu'aux termes de l'article R. 232-43 du  même code : " Le département des analyses ne procède aux analyses mentionnées à l'article L. 232-18 que si les échantillons qui lui sont transmis sont anonymes. Ces analyses sont effectuées conformément aux normes internationales. Pour leur réalisation, le directeur du département des analyses ne peut recevoir aucune instruction " ; qu'aux termes de l'article 4.1.8 du standard international pour les laboratoires de l'agence mondiale antidopage, rendu applicable en droit interne par l'article R. 232-43 du code du sport : " le laboratoire sera opérationnellement indépendant des organisations antidopage afin d'assurer une complète confiance en sa compétence, son impartialité, son jugement et son intégrité opérationnelle " ;<br/>
<br/>
              4.	Considérant que les dispositions précitées des articles L. 232-18 et R. 232-43 du code du sport garantissent l'indépendance opérationnelle du département des analyses de l'Agence française de lutte contre le dopage, conformément aux exigences du standard international pour les laboratoires de l'agence mondiale antidopage, laquelle l'a, au demeurant, agréé ; que, par ailleurs, la requérante ne soutient pas que, en l'espèce, les modalités de fonctionnement du département auraient méconnu ces exigences ; que, par suite, le moyen tiré de ce que les analyses auraient été effectuées par un  laboratoire qui ne serait pas indépendant ne peut qu'être écarté ; <br/>
<br/>
              5.	Considérant qu'aux termes de l'article R. 232-64 du code du sport : " Le département des analyses de l'Agence française de lutte contre le dopage ou le laboratoire auquel il a été fait appel en application de l'article L. 232-18 procède à l'analyse de l'échantillon A, transmis en application de l'article R. 232-62./ Il conserve l'échantillon B en vue d'une éventuelle analyse de contrôle. Celle-ci est de droit à la demande de l'intéressé. Elle est effectuée à ses frais et en présence éventuellement d'un expert convoqué par ses soins et choisi par lui, le cas échéant, sur une liste arrêtée par l'agence et transmise à l'intéressé... " ; <br/>
<br/>
              6.	Considérant que Mme C...a été informée, par une lettre de l'AFLD du 3 décembre 2014, de la possibilité qui lui était offerte de contester les analyses effectuées sur les échantillons A par le département des analyses, en faisant effectuer une analyse de contrôle sur les échantillons B, sous réserve du paiement des frais de cette analyse ; qu'en réponse à cette lettre, Mme C...a demandé que cette contre-expertise soit réalisée par un autre laboratoire que celui qui avait réalisé l'analyse des échantillons A ; que, par une lettre du 17 décembre, l'Agence, après avoir rejeté cette demande, a proposé à l'intéressée plusieurs dates pour la réalisation de la seconde analyse et lui a transmis un formulaire à retourner, accompagné du règlement des frais, fixés à 580 ou 830 euros selon qu'elle souhaiterait ou non la convocation d'un témoin indépendant ; qu'il résulte de l'instruction que MmeC..., à qui il appartenait de demander à bénéficier de la faculté ouverte par les dispositions de l'article R. 232-64 du code du sport, n'a pas acquitté le montant de ces frais et n'a pas sollicité d'autres dates que celles qui lui étaient proposées ; qu'ainsi, elle doit être regardée comme ayant renoncé à exiger l'analyse des échantillons B et ne peut utilement soutenir que les analyses de contrôle auraient dû être réalisées dans les sept jours suivant les analyses des échantillons A ; qu'elle ne peut davantage se prévaloir à cet égard ni de la méconnaissance de l'article 6 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ni, en tout état de cause, du principe du " caractère contradictoire afférent aux contre-expertises " qu'elle invoque ;<br/>
<br/>
              7.	Considérant qu'aux termes de l'article R. 232-92 du code du sport, le sportif mis en cause et son défenseur sont convoqués devant la formation disciplinaire de l'Agence par lettre recommandée avec demande d'avis de réception quinze jours au moins avant la date de la séance au cours de laquelle l'Agence est appelée à se prononcer ; qu'il résulte de l'instruction qu'à la demande du conseil de MmeC..., qui avait reçu le 2 mars, par voie postale, toutes les pièces du dossier, l'Agence a accepté un premier report de la séance d'examen par sa formation disciplinaire, prévue initialement le 4 mars 2015 ; que MmeC..., qui avait accusé réception le 9 décembre 2014 des copies des rapports analytiques des échantillons urinaires et sanguins établis le 21 novembre 2014, qui lui avaient été envoyés par l'Agence le 3 décembre, n'est pas fondée à soutenir qu'en refusant de reporter une seconde fois la séance de sa formation disciplinaire l'Agence aurait méconnu l'article 6 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
<br/>
               8. Considérant qu'en vertu de l'article L. 232-23 du code du sport, dans sa version en vigueur à la date des faits reprochés, " I.- L'Agence française de lutte contre le dopage, dans l'exercice de son pouvoir de sanction en matière de lutte contre le dopage, peut prononcer :/ 1° A l'encontre des sportifs ayant enfreint les dispositions des articles L. 232-9, (...) L. 232-17 :/ a) Un avertissement ;/ b) Une interdiction temporaire ou définitive de participer aux manifestations sportives autorisées par une fédération délégataire ou organisées par une fédération agréée ainsi qu'aux entraînements y préparant organisés par une fédération agréée ou l'un des membres de celle-ci ;/ La sanction prononcée à l'encontre d'un sportif peut être complétée par une sanction pécuniaire dont le montant ne peut excéder 45 000 &#128;. " ; qu'en dehors du cas où est apportée la preuve d'une prescription médicale à des fins thérapeutiques justifiées, l'existence d'une violation des dispositions relatives au dopage est établie par la présence, dans un prélèvement, de l'une des substances mentionnées dans la liste annexée au décret du 4 septembre 2014 portant publication de la liste 2014 des substances et méthodes interdites dans le sport ; qu'en vertu  de l'article L. 232-17 du code du sport, le sportif qui tente de se soustraire aux contrôles est également passible de sanctions ; qu'il résulte de l'instruction que Mme C...a tenté de se soustraire aux contrôles ; que les analyses effectuées sur les échantillons A le 21 septembre 2014, à la suite du contrôle antidopage du 7 novembre 2014, ont fait ressortir la présence d'érythropoïétine dans ses urines et son sang ; qu'ainsi, l'intéressée n'est fondée à soutenir ni que l'élément matériel de l'utilisation de substances proscrites ne serait pas établi  ni que la sanction infligée serait disproportionnée ;<br/>
<br/>
              9. Considérant qu'il résulte de tout ce qui précède que Mme C...n'est pas fondée à demander l'annulation de la sanction qui lui a été infligée ; <br/>
<br/>
              10. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de Mme C...la somme de 3 000 euros à verser à l'AFLD au titre des dispositions de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : La requête de Mme C...est rejetée.<br/>
Article 2 : Mme C...versera à l'ALFD une somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 3 : La présente décision sera notifiée à Mme A...C...et à l'Agence française de lutte contre le dopage. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">63-05-05 SPORTS ET JEUX. SPORTS. - INDÉPENDANCE OPÉRATIONNELLE DU LABORATOIRE ANALYSANT LES PRÉLÈVEMENTS AU SEIN DE L'AFLD - EXISTENCE (ART. L. 232-18 ET R. 232-43 DU CODE DU SPORT).
</SCT>
<ANA ID="9A"> 63-05-05 Les dispositions des articles L. 232-18 et R. 232-43 du code du sport garantissent l'indépendance opérationnelle du département des analyses de l'Agence française de lutte contre le dopage (AFLD), conformément aux exigences du standard international pour les laboratoires de l'agence mondiale antidopage, laquelle l'a, au demeurant, agréé. Un moyen mettant en cause cette indépendance sans soutenir que, en l'espèce, les modalités de fonctionnement du département auraient méconnu ces exigences, ne peut donc qu'être écarté.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
