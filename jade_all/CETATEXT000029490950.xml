<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029490950</ID>
<ANCIEN_ID>JG_L_2014_09_000000362345</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/49/09/CETATEXT000029490950.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème / 3ème SSR, 19/09/2014, 362345, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-09-19</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>362345</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème / 3ème SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BOUZIDI, BOUHANNA</AVOCATS>
<RAPPORTEUR>M. Mathieu Herondart</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Nathalie Escaut</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:362345.20140919</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 30 août et 29 novembre 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour la SNC Prisma Media, dont le siège social est 13, rue Henri Barbusse à Gennevilliers (92624 cedex) ; elle demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 11PA00582 du 22 juin 2012 par lequel la cour administrative d'appel de Paris a rejeté sa requête tendant, d'une part, à l'annulation du jugement n° 0713055 du 30 novembre 2010 du tribunal administratif de Paris rejetant sa demande tendant à l'annulation de la décision du 20 juin 2007 par laquelle le ministre du budget, des comptes publics et de la fonction publique lui a refusé l'agrément prévu au II de l'article 209 du code général des impôts et, d'autre part, d'enjoindre au ministre de réexaminer sa demande dans un délai de trois mois suivant l'arrêt à intervenir ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat une somme de 7 500 euros au titre de l'article L. 761-1 du code de justice administrative ainsi que le remboursement, en application de l'article R. 761-1 du même code, de la contribution pour l'aide juridique ; <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code général des impôts et le livre des procédures fiscales ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Mathieu Herondart, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Nathalie Escaut, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Bouzidi, Bouhanna, avocat de la SNC Prisma Media ;<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que la société en nom collectif Prisma Presse et la société RBA Editions ont créé en 1999 la société en nom collectif NG France pour assurer l'édition d'une version française du mensuel américain " National Geographic " ; que les parts de la société NG France, qui n'avait pas opté pour l'impôt sur les sociétés de capitaux, étaient détenues, à parité, par l'EURL PP1, elle-même détenue à 100 % par la société Prisma Presse, et par l'EURL PP2, elle-même détenue à 100 % par la société RBA Editionsdétenue à 100 % par la société Prisma Presse, et par l'EURL PP2, elle-même détenue à 100 % par la société RBA Editions; que l'activité de la société NG a été déficitaire ; que la société RBA Editions a cédé à la société Prisma Presse sa participation dans l'EURL PP2 ; que cette dernière a fait l'objet, par acte du 25 avril 2006, d'une dissolution et d'une transmission universelle de son patrimoine à la société Prisma Presse en application des dispositions de l'article 1844-5 du code civil ; que la société Prisma Presse, qui était soumise sur option à l'impôt sur les sociétés, a sollicité, le 24 avril 2006, la délivrance de l'agrément prévu par le II de l'article 209 du code général des impôts en vue de transférer les  déficits non encore déduits par l'EURL PP2 avant sa dissolution, soit un montant de 11 564 425 euros à la date du 31 décembre 2004 ; que, par une décision du 20 juin 2007, le ministre du budget, des comptes publics et de la fonction publique a refusé de lui délivrer cet agrément ; que la société Prisma Presse demande l'annulation de l'arrêt du 22 juin 2012 par lequel la cour administrative d'appel de Paris a rejeté sa requête dirigée contre le jugement du 30 novembre 2010 du tribunal administratif de Paris rejetant sa demande tendant à l'annulation, pour excès de pouvoir, de cette décision ;  <br/>
<br/>
              2. Considérant qu'aux termes du II de l'article 209 du code général des impôts, dans sa rédaction applicable en 2007: " II. En cas de fusion ou opération assimilée placée sous le régime de l'article 210 A, les déficits antérieurs et la fraction d'intérêts mentionnée au sixième alinéa du 1 du II de l'article 212 non encore déduits par la société absorbée ou apporteuse sont transférés, sous réserve d'un agrément délivré dans les conditions prévues à l'article 1649 nonies, à la ou aux sociétés bénéficiaires des apports, et imputables sur ses ou leurs bénéfices ultérieurs dans les conditions prévues respectivement au troisième alinéa du I et au sixième alinéa du 1 du II de l'article 212. En cas de scission ou d'apport partiel d'actif, les déficits transférés sont ceux afférents à la branche d'activité apportée. / L'agrément est délivré lorsque : / a. L'opération est justifiée du point de vue économique et obéit à des motivations principales autres que fiscales ; / b. L'activité à l'origine des déficits ou des intérêts dont le transfert est demandé est poursuivie par la ou les sociétés bénéficiaires des apports pendant un délai minimum de trois ans (...) " ;<br/>
<br/>
              3. Considérant que ces dispositions ne font pas obstacle à ce que les déficits enregistrés, en l'absence d'intégration fiscale, par une société holding absorbée à raison des déficits réalisés par des sociétés qu'elle détenait soient transférés à la société holding absorbante, dès lors que celle-ci continue à détenir les titres de participation dans les sociétés dont l'activité est à l'origine des déficits pendant un délai minimum de trois ans et que ces sociétés poursuivent pendant ce même délai cette activité ; que, par suite, en jugeant que ces dispositions interdisaient le transfert à une société holding absorbante de déficits antérieurs de filiales détenues par la société absorbée et n'ayant pas opté pour l'impôt sur les sociétés, la cour a commis une erreur de droit ; que, dès lors, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, la société Prisma Média est fondée à demander l'annulation de l'arrêt qu'elle attaque ;<br/>
<br/>
              4. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 5 000 euros à verser à la société Prisma Média au titre des dispositions de l'article L. 761-1 et R. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>                D E C I D E :<br/>
                               --------------<br/>
<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Paris du 22 juin 2012 est annulé.<br/>
<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Paris.<br/>
<br/>
Article 3 : L'Etat versera à la société Prisma Média une somme de 5 000 euros au titre des articles L. 761-1 et R. 761-1 du code de justice administrative.<br/>
<br/>
Article 4 : La présente décision sera notifiée à la SNC Prisma Média et au ministre des finances et des comptes publics. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
