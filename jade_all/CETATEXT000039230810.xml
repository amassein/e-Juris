<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039230810</ID>
<ANCIEN_ID>JG_L_2019_10_000000425226</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/23/08/CETATEXT000039230810.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème - 7ème chambres réunies, 16/10/2019, 425226, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-10-16</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>425226</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème - 7ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP SPINOSI, SUREAU</AVOCATS>
<RAPPORTEUR>M. Fabio Gennari</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Guillaume Odinet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:425226.20191016</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés les 6 novembre 2018 et 11 mars 2019 au secrétariat du contentieux du Conseil d'Etat, M. A... B... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret du 17 octobre 2018 par lequel le Président de la République lui a infligé une sanction d'exclusion temporaire de fonctions pour une durée de six mois ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat une somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le règlement (CE) n° 810/2009 du Parlement européen et du Conseil du 13 juillet 2009 établissant un code communautaire des visas ; <br/>
              - le code des relations entre le public et l'administration ;<br/>
              - la loi n° 83-634 du 13 juillet 1983 ;<br/>
              - la loi n° 84-16 du 11 janvier 1984 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Fabio Gennari, auditeur,  <br/>
<br/>
              - les conclusions de M. Guillaume Odinet, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Spinosi, Sureau, avocat de M. B... ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1.	M. A... B..., conseiller des affaires étrangères, a exercé les fonctions de premier conseiller de l'ambassade de France en République Centrafricaine du 24 août 2011 au 9 septembre 2015, puis celles d'adjoint au porte-parole du ministère de l'Europe et des affaires étrangères. Une procédure disciplinaire engagée à son encontre, à raison d'incidents relatifs à la délivrance irrégulière d'un grand nombre de visas lorsqu'il était en poste en République centrafricaine, a abouti à une sanction d'exclusion temporaire de fonctions d'une durée de six mois, par décret non publié du Président de la République du 17 octobre 2018. M. B... demande l'annulation pour excès de pouvoir de ce décret.  <br/>
<br/>
              Sur la légalité externe : <br/>
<br/>
              2.	En premier lieu, aux termes de l'article L. 211-2 du code des relations entre le public et l'administration : " Les personnes physiques ou morales ont le droit d'être informées sans délai des motifs des décisions administratives individuelles défavorables qui les concernent. / A cet effet, doivent être motivées les décisions qui (...) infligent une sanction ". L'article L. 211-5 de ce code précise que : " La motivation exigée par le présent chapitre doit être écrite et comporter l'énoncé des considérations de droit et de fait qui constituent le fondement de la décision. ". Il ressort des pièces du dossier que le décret attaqué, qui repose sur trois motifs circonstanciés, énonce les considérations de droit et de fait sur lesquelles il se fonde. Il est ainsi suffisamment motivé.<br/>
<br/>
              3.	En deuxième lieu, la circonstance que Mme C..., directrice générale de l'administration et de la modernisation du ministère de l'Europe et des affaires étrangères, ait engagé les poursuites disciplinaires à l'encontre de M. B... ne faisait pas obstacle à ce qu'elle pût régulièrement présider la commission administrative paritaire, siégeant en conseil de discipline, dès lors qu'il ne ressort pas des pièces du dossier qu'elle aurait, dans la conduite des débats, manqué à l'impartialité requise ou manifesté une animosité particulière à l'égard de l'intéressé. Il ne ressort pas davantage des pièces du dossier que M. D..., qui siégeait au conseil de discipline, aurait manqué à l'impartialité requise ou manifesté une animosité particulière à l'égard de M. B.... <br/>
<br/>
              4.	En troisième lieu, d'une part, si M. B... soutient qu'il n'a pas été mis en mesure de consulter les dossiers et les fiches d'analyse des visas litigieux avant la réunion du conseil de discipline et, en tout état de cause, avant la publication du décret attaqué, il ressort des pièces du dossier que le requérant, qui était au demeurant représenté par un avocat, avait connaissance de l'existence de ces pièces et qu'il n'en a pas demandé la communication, ni avant ni après la tenue de ce conseil. Dans ces conditions, il ne peut se prévaloir de ce qu'elles ne lui auraient pas été communiquées, en méconnaissance du principe du respect des droits de la défense. D'autre part, la circonstance que la procédure disciplinaire engagée à son encontre le 24 août 2017 n'a abouti à une sanction que le 17 octobre 2018 est sans incidence sur la légalité du décret attaqué. Le moyen tiré de la méconnaissance du principe du respect des droits de la défense ne peut par suite qu'être écarté. <br/>
<br/>
              Sur la légalité interne :<br/>
<br/>
              5.	Il appartient au juge de l'excès de pouvoir, saisi de moyens en ce sens, de rechercher si les faits reprochés à un agent public ayant fait l'objet d'une sanction disciplinaire constituent des fautes de nature à justifier une sanction et si la sanction retenue est proportionnée à la gravité de ces fautes.<br/>
<br/>
              6.	Il ressort des pièces du dossier qu'entre décembre 2013 et septembre 2015, M. B... a activement participé, en tant que premier conseiller de l'ambassade de France à Bangui, à la pratique de délivrance des visas menée à l'initiative de l'ambassadeur en poste, laquelle a abouti à la délivrance d'un grand nombre de visas au vu de dossiers incomplets, de faux documents ou de demandes ne remplissant pas les conditions posées par le règlement du 13 juillet 2009 établissant un code communautaire des visas. Il ressort également des pièces du dossier que M. B..., qui adhérait à cette pratique, a personnellement délivré 72 de ces visas, dont un au fils mineur de sa compagne. <br/>
<br/>
              7.	Compte tenu du grade, des fonctions et de l'expérience de l'intéressé, le fait d'avoir activement participé à la délivrance d'un grand nombre de visas et personnellement délivré 72 d'entre eux, en méconnaissance du code communautaire des visas, constitue, par sa nature et son ampleur, un grave manquement à ses missions. La délivrance d'un visa au fils mineur de sa compagne, en situation manifeste de conflit d'intérêts, constitue en outre un manquement au devoir d'impartialité. Ces agissements ont porté atteinte aux intérêts de l'Etat, particulièrement à l'intérêt public qui s'attache à la lutte contre l'immigration irrégulière, et constituent des fautes de nature à justifier une sanction, sans que la circonstance qu'ils soient intervenus dans un contexte de crise ne puisse exonérer M. B... de sa responsabilité.<br/>
<br/>
              8.	Dans ces conditions, compte tenu de l'emploi, des fonctions de l'intéressé et des responsabilités qui s'attachent à l'exercice de celles-ci, de la gravité des fautes, de leur caractère répété et de la durée pendant laquelle elles ont été commises, le Président de la République n'a pas prononcé à l'encontre de M. B... une sanction disproportionnée en l'excluant de ses fonctions pour une durée de six mois. <br/>
<br/>
              9.	Il résulte de ce qui précède que M. B... n'est pas fondé à demander l'annulation du décret qu'il attaque. Ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative ne peuvent par suite qu'être rejetées.  <br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				--------------<br/>
<br/>
Article 1er : La requête de M. B... est rejetée.<br/>
<br/>
Article 2 : La présente décision sera notifiée à M. A... B..., au ministre de l'Europe et des affaires étrangères et au Premier ministre. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
