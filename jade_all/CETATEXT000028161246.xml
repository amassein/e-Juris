<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028161246</ID>
<ANCIEN_ID>JG_L_2013_11_000000351740</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/16/12/CETATEXT000028161246.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème et 4ème sous-sections réunies, 06/11/2013, 351740</TITRE>
<DATE_DEC>2013-11-06</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>351740</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème et 4ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP POTIER DE LA VARDE, BUK LAMENT</AVOCATS>
<RAPPORTEUR>Mme Marie Gautier-Melleray</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:351740.20131106</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu le pourvoi, enregistré le 9 août 2011 au secrétariat du contentieux du Conseil d'Etat, présenté par le ministre de l'intérieur, de l'outre-mer, des collectivités territoriales et de l'immigration ; le ministre demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler le jugement n° 1100056 du 9 juin 2011 par lequel le tribunal administratif de Nouvelle-Calédonie a condamné l'Etat à verser à M. A...B...une indemnité mensuelle de 85 000 francs CFP en réparation du préjudice subi par le requérant en raison du refus de concours de la force publique, en tant qu'il fixe la période de responsabilité de l'Etat ;<br/>
<br/>
              2°) réglant l'affaire au fond, de substituer la période de responsabilité de l'Etat du 4 juin 2009 au 4 septembre 2010 à celle du 3 juin 2009 au 9 juin 2011 ;<br/>
<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
	Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Marie Gautier-Melleray, Maître des Requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Potier de la Varde, Buk Lament, avocat de M. B... ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, par une décision implicite acquise le 14 novembre 2006, le haut-commissaire de la République en Nouvelle-Calédonie a refusé à M. B...le concours de la force publique pour l'exécution d'une ordonnance du 21 juin 2006 du tribunal de première instance de Nouméa ordonnant l'expulsion de l'occupant d'un logement lui appartenant ; que, saisi d'un recours indemnitaire de l'intéressé, tendant à la réparation des préjudices qu'il subissait du fait de ce refus, le tribunal administratif de la Nouvelle-Calédonie, par jugements des 25 octobre 2007 et 3 juin 2009, a condamné l'Etat à lui verser des indemnités au titre des périodes comprises entre le 14 novembre 2006 et le 25 octobre 2007 et entre le 26 octobre 2007 et le 3 juin 2009 ; que, par un jugement du 9 juin 2011, le même tribunal a condamné l'Etat à verser à M. B... une indemnité mensuelle de 85 000 francs CFP au titre de la période comprise entre le 3 juin 2009 et le 9 juin 2011 ; que le ministre de l'intérieur, de l'outre-mer, des collectivités territoriales et de l'immigration se pourvoit en cassation contre ce jugement en tant qu'il fixe la période de responsabilité de l'Etat ; <br/>
<br/>
              2. Considérant que le haut-commissaire de la République en Nouvelle-Calédonie a soutenu devant le tribunal administratif qu'ainsi qu'il ressortait notamment d'un message électronique en date du 5 novembre 2010, la procédure d'expulsion avait été suspendue à la demande de l'huissier instrumentaire ; qu'en ne répondant pas à ce moyen, qui n'était pas inopérant, le tribunal a entaché son jugement d'irrégularité ; que, par suite, sans qu'il soit besoin de se prononcer sur l'autre moyen du pourvoi, le jugement du 9 juin 2011 doit être annulé en tant qu'il fixe la période de responsabilité de l'Etat ;<br/>
<br/>
              3. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler, dans cette mesure, l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              4. Considérant que, par ses jugements des 25 octobre 2007 et 3 juin 2009, le tribunal administratif de Nouvelle-Calédonie a indemnisé M. B... pour les dommages subis en raison du refus du haut-commissaire d'accorder le concours de la force publique pour l'exécution de l'ordonnance d'expulsion de l'occupant sans titre pour la période comprise entre le 14 juin 2006 et le 3 juin 2009 ; qu'il résulte de l'instruction que l'occupation irrégulière s'est poursuivie au-delà de cette dernière date, le haut-commissaire n'étant pas revenu sur son refus d'accorder le concours de la force publique ; que l'Etat doit dès lors réparer les préjudices subis par M. B...à compter du 4 juin 2009 ; <br/>
<br/>
              5. Considérant que la période de responsabilité de l'Etat pour refus de concours de la force publique pour l'exécution d'une décision de justice s'interrompt si l'huissier mandaté par le propriétaire fait connaître au représentant de l'Etat qu'il suspend les effets de sa demande de concours de la force publique ; que la période de responsabilité de l'Etat ne recommence alors à courir, le cas échéant, qu'à compter de la décision rejetant une nouvelle réquisition de la force publique ; <br/>
<br/>
              6. Considérant qu'il résulte de l'instruction, notamment d'un message électronique envoyé le 5 novembre 2010 par l'étude de l'huissier mandaté par M. B...à un fonctionnaire du haut-commissariat de la République en Nouvelle-Calédonie, que l'huissier instrumentaire a suspendu les effets de sa demande de concours de la force publique pour l'expulsion de l'occupant sans titre de son bien ; qu'aucune nouvelle demande de concours de la force publique n'a été présentée après cette date pour l'exécution de l'ordonnance du tribunal de première instance de Nouméa du 21 juin 2006 ; que, dans ces conditions, la période de responsabilité de l'Etat au titre du refus de concours de la force publique a pris fin à la date à laquelle les services du haut-commissaire ont été avisés par l'huissier de son intention de suspendre la demande de concours de la force publique, soit le 5 novembre 2010 ; <br/>
<br/>
              7. Considérant qu'il résulte de ce qui précède que l'indemnité mensuelle de 85 000 francs CFP fixée par le jugement du 9 juin 2011, devenu définitif sur ce point, du tribunal administratif de Nouvelle-Calédonie, doit être versée par l'Etat à M. B... au titre de la période allant du 4 juin 2009 au 5 novembre 2010 ; <br/>
<br/>
              8. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'article 1er du jugement du tribunal administratif de Nouvelle-Calédonie du 9 juin 2011 est annulé en tant qu'il fixe la période d'indemnisation de M.B....<br/>
Article 2 : L'indemnité mensuelle de 85 000 francs CFP fixée par le jugement du 9 juin 2011 sera versée par l'Etat à M. B...au titre de la période comprise entre le 4 juin 2009 et le 5 novembre 2010. <br/>
Article 3 : Les conclusions de M. B...présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative et le surplus de ses conclusions sont rejetées.<br/>
Article 4 : La présente décision sera notifiée au ministre des outre-mer et à M. A... B....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">37-05-01 JURIDICTIONS ADMINISTRATIVES ET JUDICIAIRES. EXÉCUTION DES JUGEMENTS. CONCOURS DE LA FORCE PUBLIQUE. - REFUS DE CONCOURS - RESPONSABILITÉ DE L'ETAT - HUISSIER MANDATÉ FAISANT CONNAÎTRE AU PRÉFET QU'IL SUSPEND LES EFFETS DE SA DEMANDE DE CONCOURS - EFFET - SUSPENSION DE LA PÉRIODE DE RESPONSABILITÉ DE L'ETAT JUSQU'À L'ÉDICTION D'UNE NOUVELLE DÉCISION DE REFUS.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">60-02-03-01-03 RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. RESPONSABILITÉ EN RAISON DES DIFFÉRENTES ACTIVITÉS DES SERVICES PUBLICS. SERVICES DE POLICE. SERVICES DE L'ETAT. EXÉCUTION DES DÉCISIONS DE JUSTICE. - REFUS DE CONCOURS DE LA FORCE PUBLIQUE - HUISSIER MANDATÉ FAISANT CONNAÎTRE AU PRÉFET QU'IL SUSPEND LES EFFETS DE SA DEMANDE DE CONCOURS - EFFET - SUSPENSION DE LA PÉRIODE DE RESPONSABILITÉ DE L'ETAT JUSQU'À L'ÉDICTION D'UNE NOUVELLE DÉCISION DE REFUS.
</SCT>
<ANA ID="9A"> 37-05-01 La période de responsabilité de l'Etat pour refus de concours de la force publique pour l'exécution d'une décision de justice s'interrompt si l'huissier mandaté par le propriétaire fait connaître au représentant de l'Etat qu'il suspend les effets de sa demande de concours de la force publique et ne recommence alors à courir, le cas échéant, qu'à compter de la décision rejetant une nouvelle réquisition de la force publique.</ANA>
<ANA ID="9B"> 60-02-03-01-03 La période de responsabilité de l'Etat pour refus de concours de la force publique pour l'exécution d'une décision de justice s'interrompt si l'huissier mandaté par le propriétaire fait connaître au représentant de l'Etat qu'il suspend les effets de sa demande de concours de la force publique et ne recommence alors à courir, le cas échéant, qu'à compter de la décision rejetant une nouvelle réquisition de la force publique.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
