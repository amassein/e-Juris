<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038823634</ID>
<ANCIEN_ID>JG_L_2019_07_000000418462</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/82/36/CETATEXT000038823634.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère chambre, 24/07/2019, 418462, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-07-24</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>418462</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Marie Sirinelli</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Charles Touboul</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2019:418462.20190724</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Le département de Meurthe-et-Moselle a demandé à la Commission centrale d'aide sociale de fixer le domicile de secours de M. A...B...dans le département de la Meuse. Par une décision n° 140172 du 3 avril 2015, la Commission centrale d'aide sociale a fait droit à sa demande.<br/>
<br/>
              Par une décision n° 392023 du 12 octobre 2016, le Conseil d'Etat, statuant au contentieux, saisi d'un pourvoi présenté par le département de la Meuse, a annulé la décision de la Commission centrale d'aide sociale du 3 avril 2015 et renvoyé l'affaire à cette juridiction.<br/>
<br/>
              Par une décision n° 160549 du 5 juillet 2017, la Commission centrale d'aide sociale a fixé, à nouveau, le domicile de secours de M. B...dans le département de la Meuse.<br/>
<br/>
              Par un pourvoi et un mémoire en réplique, enregistrés les 21 février 2018 et 11 janvier 2019 au secrétariat du contentieux du Conseil d'Etat, le département de la Meuse demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler la décision de la Commission centrale d'aide sociale du 5 juillet 2017 ; <br/>
<br/>
              2°) réglant l'affaire au fond, de fixer le domicile de secours de M. B...dans le département de Meurthe-et-Moselle à compter du 1er février 2014.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code de l'action sociale et des familles ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Marie Sirinelli, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Charles Touboul, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes du premier alinéa de l'article L. 122-1 du code de l'action sociale et des familles : " Les dépenses d'aide sociale prévues à l'article L. 121-1 sont à la charge du département dans lequel les bénéficiaires ont leur domicile de secours ". Aux termes du premier alinéa de l'article L. 122-2 de ce code : " Nonobstant les dispositions des articles 102 à 111 du code civil, le domicile de secours s'acquiert par une résidence habituelle de trois mois dans un département postérieurement à la majorité ou à l'émancipation, sauf pour les personnes admises dans des établissements sanitaires ou sociaux (...), qui conservent le domicile de secours qu'elles avaient acquis avant leur entrée dans l'établissement (...). Le séjour dans ces établissements (...) est sans effet sur le domicile de secours ". Enfin, l'article L. 122-3 du même code prévoit que : " Le domicile de secours se perd : / 1° Par une absence ininterrompue de trois mois postérieurement à la majorité ou à l'émancipation, sauf si celle-ci est motivée par un séjour dans un établissement sanitaire ou social (...) ; / 2° Par l'acquisition d'un autre domicile de secours (...) ".<br/>
<br/>
              2. Pour l'application de ces dispositions, seule l'admission et le séjour dans un établissement sanitaire ou social relevant de l'article L. 312-1 du code de l'action sociale et des familles et autorisé sur le fondement de l'article L. 313-1 du même code sont sans effet sur le domicile de secours antérieurement acquis par le bénéficiaire de l'aide sociale.<br/>
<br/>
              3. Pour se prononcer sur le domicile de secours de M.B..., qui résidait chez ses parents, dans le département de la Meuse, avant d'être admis dans une structure d'accueil située dans le département de Meurthe-et-Moselle, la Commission centrale d'aide sociale a jugé que la circonstance que la structure en cause n'était pas un établissement sanitaire ou social relevant de l'article L. 312-1 du code de l'action sociale et des familles ne faisait pas obstacle à ce que cette structure soit traitée comme celles pour lesquelles les articles L. 122-2 et L. 122-3 du code de l'action sociale et des familles excluent que le séjour entraîne un changement de domicile de secours. Il résulte de ce qui a été dit au point 2 qu'en statuant ainsi, elle a commis une erreur de droit. <br/>
<br/>
              4. Il résulte de ce qui précède que le département de la Meuse est fondé à demander l'annulation de la décision qu'il attaque. Le moyen retenu suffisant à entraîner cette annulation, il n'est pas nécessaire d'examiner l'autre moyen du pourvoi.<br/>
<br/>
              5. Aux termes du second alinéa de l'article L. 821-2 du code de justice administrative : " Lorsque l'affaire fait l'objet d'un second pourvoi en cassation, le Conseil d'Etat statue définitivement sur cette affaire ". Le Conseil d'Etat étant saisi, en l'espèce, d'un second pourvoi en cassation, il lui incombe de régler l'affaire au fond.<br/>
<br/>
              6. En premier lieu, il résulte de l'instruction que M.B..., majeur handicapé, a résidé, à compter du 4 novembre 2013, à la maison l'En-Vie, située dans le département de Meurthe-et-Moselle et gérée par l'association des familles de traumatisés crâniens et cérébrolésés de Lorraine, titulaire d'un agrément délivré par un arrêté du préfet de la région Lorraine du 23 octobre 2012, sur le fondement de l'article L. 365-4 du code de la construction et de l'habitation, en vue d'une activité d'intermédiation locative et de gestion locative sociale. La maison l'En-Vie ne constitue pas un établissement sanitaire ou social relevant de l'article L. 312 1 du code de l'action sociale et des familles et autorisé sur le fondement de l'article L. 313-1 du même code. Dans ces conditions, l'admission et le séjour de M. B...dans cette structure ne peuvent être regardés comme ayant fait obstacle à l'acquisition d'un domicile de secours dans le département de résidence habituelle. <br/>
<br/>
              7. En second lieu, il résulte des dispositions des articles L. 122-2 et L. 122-3 du code de l'action sociale et des familles cités au point 1 que s'il se perd, notamment, par une absence ininterrompue de trois mois, le domicile de secours s'acquiert en revanche par une résidence qui, selon les termes mêmes de l'article L. 122-2, doit être habituelle, sans nécessairement être continue. Par suite, la circonstance que M. B...revenait au domicile de ses parents, dans la Meuse, toutes les fins de semaine ne faisait pas obstacle à l'acquisition d'un domicile de secours en Meurthe-et-Moselle.<br/>
<br/>
              8. Il résulte de ce qui précède que l'hébergement de M. B...dans la structure l'En-Vie située en Meurthe-et-Moselle lui a fait acquérir, au terme de trois mois d'une résidence qui était habituelle, un nouveau domicile de secours dans ce département. Par suite, le département de la Meuse est fondé à demander la fixation du domicile de secours dans ce département, à compter du 4 février 2014.  <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : La décision de la Commission centrale d'aide sociale du 5 juillet 2017 est annulée.<br/>
Article 2 : Le domicile de secours de M. A...B...est fixé dans le département de Meurthe-et-Moselle à compter du 4 février 2014.<br/>
Article 3 : La présente décision sera notifiée au département de la Meuse et au département de Meurthe-et-Moselle.<br/>
Copie en sera adressée à M. A...B....<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
