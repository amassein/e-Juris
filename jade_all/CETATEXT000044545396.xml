<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044545396</ID>
<ANCIEN_ID>JG_L_2021_12_000000449640</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/54/53/CETATEXT000044545396.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème chambre, 21/12/2021, 449640, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-12-21</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>449640</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BOUZIDI, BOUHANNA</AVOCATS>
<RAPPORTEUR>M. Frédéric Gueudar Delahaye</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Marc Pichon de Vendeuil</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:449640.20211221</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. B... C... a demandé au tribunal administratif d'Orléans de condamner l'université d'Orléans à lui payer la somme de 1 457, 23 euros, majorée des intérêts de retard à compter de septembre 2014, en réparation de son préjudice financier lié aux retenues sur son traitement effectuées en juillet, août, septembre et octobre 2014 et, de condamner l'université d'Orléans à lui payer la somme de 1 500 euros, majorée des intérêts au taux légal, en réparation des préjudices subis du fait du non-paiement de sa rémunération. Par un jugement n° 1701041 du 20 septembre 2018, le tribunal administratif d'Orléans a rejeté sa demande. <br/>
<br/>
              Par un arrêt n° 19NT00377 du 29 septembre 2020, la cour administrative d'appel de Nantes a rejeté l'appel formé par M. C... contre ce jugement.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 12 février et 4 mai 2021 au secrétariat du contentieux du Conseil d'Etat, M. C... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 000 euros à verser à la SCP Bouzidi, Bouhanna, son avocat, au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 modifiée.<br/>
<br/>
<br/>
              Par une ordonnance du 4 octobre 2021, la clôture d'instruction a été fixée au 21 octobre 2021.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Frédéric Gueudar Delahaye, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de M. Marc Pichon de Vendeuil, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Bouzidi, Bouhanna, avocat de M. C... ;<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que M. C..., lauréat du concours de technicien d'exploitation, de maintenance et de traitement des données, organisé par le ministère de l'éducation nationale au titre de l'année 2012, a été affecté au service informatique de l'institut universitaire de technologie de l'université d'Orléans, sur un poste de technicien de maintenance, en qualité de stagiaire, à compter du 1er octobre 2012. Par arrêté du 27 novembre 2013, le ministre de l'enseignement supérieur et de la recherche a prorogé son stage pour une durée d'un an. A l'issue de sa seconde année de stage, le ministre a refusé de prononcer sa titularisation et, par arrêté du 19 novembre 2014, a prononcé sa radiation des cadres à compter du 1er janvier 2015. M. C... a demandé au tribunal administratif d'Orléans de condamner l'université d'Orléans à lui verser la somme de 1 457,23 euros, majorée des intérêts de retard à compter de septembre 2014, en réparation de son préjudice financier lié aux retenues sur son traitement effectuées en juillet, août, septembre et octobre 2014 et, de condamner l'université d'Orléans à lui verser la somme de 1 500 euros, majorée des intérêts au taux légal, en réparation des préjudices subis du fait du non-paiement de sa rémunération. Par un jugement du 20 septembre 2018 et un arrêt du 29 septembre 2020, le tribunal administratif d'Orléans et la cour administrative d'appel de Nantes ont, respectivement, rejeté la demande de M. C... et son appel contre le jugement.<br/>
<br/>
              2. D'une part, aux termes de l'article R. 811-1 du code de justice administrative : " Toute partie présente dans une instance devant le tribunal administratif ou qui y a été régulièrement appelée (...) peut interjeter appel contre toute décision juridictionnelle rendue dans cette instance. / Toutefois, le tribunal administratif statue en premier et dernier ressort : / (...) 8° Sauf en matière de contrat de la commande publique sur toute action indemnitaire ne relevant pas des dispositions précédentes, lorsque le montant des indemnités demandées n'excède pas le montant déterminé par les articles R. 222-14 et R. 222-15 ; / (...) / Par dérogation aux dispositions qui précèdent, en cas de connexité avec un litige susceptible d'appel, les décisions portant sur les actions mentionnées au 8° peuvent elles-mêmes faire l'objet d'un appel (...) ". En vertu de l'article R. 222-14 de ce code, le montant des indemnités visées par le 8° de l'article R. 811-1, déterminé conformément à ce que prévoit l'article R.222-15, est fixé à 10 000 euros.<br/>
<br/>
              3. La demande d'un fonctionnaire ou d'un agent public tendant seulement au versement de traitements, rémunérations, indemnités, avantages ou soldes impayés, sans chercher la réparation d'un préjudice distinct du préjudice matériel objet de cette demande pécuniaire, ne revêt pas le caractère d'une action indemnitaire au sens du 8° de l'article R. 811-1 du code de justice administrative. Par suite, une telle demande n'entre pas, quelle que soit l'étendue des obligations qui pèseraient sur l'administration au cas où il y serait fait droit, dans le champ de l'exception, prévue à ce 8°, en vertu de laquelle le tribunal administratif statue en dernier ressort.<br/>
<br/>
              4. D'autre part, il résulte des dispositions des articles R. 351-2 et R. 351-4 du code de justice administrative que, sauf à rejeter les conclusions comme entachées d'une irrecevabilité manifeste insusceptible d'être couverte en cours d'instance, à constater qu'il n'y a pas lieu de statuer sur tout ou partie des conclusions ou à rejeter la requête en se fondant sur l'irrecevabilité manifeste de la demande de première instance, une cour administrative d'appel saisie de conclusions qui relèvent de la compétence du Conseil d'Etat doit les lui transmettre sans délai.<br/>
<br/>
              5. En se prononçant, par son jugement du 20 septembre 2018, sur les conclusions de M. C... tendant à la fois à la réparation de son préjudice financier et de son préjudice moral liés aux retenues sur son traitement, le tribunal administratif d'Orléans a statué en premier et dernier ressort. Par suite, en statuant à son tour sur le bien-fondé de cette demande, la cour administrative d'appel de Nantes a méconnu le champ de sa compétence d'appel. Sans qu'il soit besoin de se prononcer sur les moyens du pourvoi, il y a donc lieu d'annuler l'arrêt attaqué. Les conclusions que M. C... a présentées devant la cour administrative d'appel de Nantes, dirigées contre le jugement, statuant en premier et dernier ressort doivent être regardées comme un pourvoi en cassation. <br/>
<br/>
              6. Aux termes de l'article L. 822-1 du code de justice administrative : " Le pourvoi en cassation devant le Conseil d'Etat fait l'objet d'une procédure préalable d'admission. L'admission est refusée par décision juridictionnelle si le pourvoi est irrecevable ou n'est fondé sur aucun moyen sérieux ".<br/>
<br/>
              7. Pour demander l'annulation du jugement du 20 septembre 2018 du tribunal administratif d'Orléans, M. C... soutient que le tribunal a commis une première erreur de droit en jugeant qu'il n'établissait pas la réalité de ses prétentions alors que l'université d'Orléans n'ayant pas produit de mémoire en défense, elle était réputée avoir acquiescé aux faits et a commis une seconde erreur de droit ou dénaturé les pièces du dossier en ne retenant pas la responsabilité pour faute de l'université d'Orléans s'agissant du préjudice moral résultant des retenues sur son traitement mensuel.<br/>
<br/>
              8. Aucun de ces moyens n'est de nature à permettre l'admission du pourvoi.<br/>
<br/>
<br/>
<br/>			D E C I D E :<br/>
              			--------------<br/>
<br/>
Article 1er : L'arrêt du 29 septembre 2020 de la cour administrative d'appel de Nantes est annulé.<br/>
Article 2 : Le pourvoi de M. C... n'est pas admis.<br/>
Article 3 : La présente décision sera notifiée à M. B... C... et à l'université d'Orléans.<br/>
              Délibéré à l'issue de la séance du 6 décembre 2021 où siégeaient : M. Benoît Bohnert, assesseur, présidant ; M. Gilles Pellissier, conseiller d'Etat et M. Frédéric Gueudar Delahaye, conseiller d'Etat-rapporteur. <br/>
<br/>
              Rendu le 21 décembre 2021.<br/>
<br/>
                                   Le président : <br/>
                                   Signé : M. Benoît Bohnert<br/>
<br/>
<br/>
Le rapporteur : <br/>
Signé : M. Frédéric Gueudar Delahaye<br/>
<br/>
                                   La secrétaire :<br/>
                                   Signé : Mme D... A...<br/>
<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
