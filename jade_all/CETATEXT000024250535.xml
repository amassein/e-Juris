<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000024250535</ID>
<ANCIEN_ID>JG_L_2011_06_000000320744</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/24/25/05/CETATEXT000024250535.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème et 8ème sous-sections réunies, 22/06/2011, 320744</TITRE>
<DATE_DEC>2011-06-22</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>320744</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème et 8ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FABIANI, LUC-THALER ; COPPER-ROYER</AVOCATS>
<RAPPORTEUR>Mme Emilie Bokdam-Tognetti</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Edouard Geffray</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2011:320744.20110622</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi, enregistré le 16 septembre 2008 au secrétariat du contentieux du Conseil d'Etat, présenté pour Mme B...A..., demeurant... ; Mme A...demande au Conseil d'Etat d'annuler le jugement n° 0801071-2 du 8 juillet 2008 par lequel le tribunal administratif de Nancy a rejeté sa demande tendant à la condamnation du département de Meurthe-et-Moselle à lui verser la somme de 4 450 euros en réparation de divers préjudices qu'elle affirme avoir subis à raison de violences commises le 11 avril 2005 par un mineur confié au service départemental de l'aide sociale à l'enfance ;<br/>
<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code civil ;<br/>
<br/>
              Vu le code de l'action sociale et des familles ;<br/>
<br/>
              Vu le code de la sécurité sociale ;<br/>
<br/>
              Vu la décision n° 2010-8 QPC du 18 juin 2010 du Conseil constitutionnel ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique : <br/>
<br/>
              - le rapport de Mme Emilie Bokdam-Tognetti, Auditeur,<br/>
<br/>
              - les observations de la SCP Fabiani, Luc-Thaler, avocat de Mme A... et de Me Copper-Royer, avocat du département de Meurthe-et-Moselle, <br/>
<br/>
              - les conclusions de M. Edouard Geffray, rapporteur public, <br/>
<br/>
              La parole ayant été à nouveau donnée à SCP Fabiani, Luc-Thaler, avocat de Mme A...et à Me Copper-Royer, avocat du département de Meurthe-et-Moselle ;<br/>
<br/>
<br/>
<br/>
              Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que MmeA..., employée comme monitrice éducatrice contractuelle par le département de Meurthe-et-Moselle du 26 mai 2004 au 30 mai 2005, a, le 11 avril 2005, été frappée d'une série de coups ayant entraîné une incapacité totale de travail inférieure à huit jours, prolongée par la suite pendant deux mois, par un mineur confié à l'aide sociale à l'enfance du département au titre de l'assistance éducative ; que Mme A...se pourvoit en cassation contre le jugement du 8 juillet 2008 par lequel le tribunal administratif de Nancy a rejeté sa demande tendant à la condamnation du département à lui verser une somme de 4 450 euros en réparation de troubles dans ses conditions d'existence ainsi que des préjudices causés par ses souffrances physiques et de préjudices financiers qu'elle affirme avoir subis du fait de l'agression dont elle a été victime ;<br/>
<br/>
              Considérant qu'aux termes de l'article L. 451-1 du code de la sécurité sociale : " Sous réserve des dispositions prévues aux articles L. 452-1 à L. 452-5, L. 454-1, L. 455-1, L. 455-1-1 et L. 455-2 aucune action en réparation des accidents et maladies mentionnés par le présent livre ne peut être exercée conformément au droit commun, par la victime ou ses ayants droit " ; qu'aux termes de l'article L. 452-1 du code : " Lorsque l'accident est dû à la faute inexcusable de l'employeur ou de ceux qu'il s'est substitués dans la direction, la victime ou ses ayants droit ont droit à une indemnisation complémentaire dans les conditions définies aux articles suivants " ; que l'article L. 452-3 de ce code, tel qu'interprété par le Conseil constitutionnel dans sa décision n° 2010-8 QPC du 18 juin 2010, prévoit que, dans le cas d'une faute inexcusable de l'employeur, la victime a le droit de demander à l'employeur, devant la juridiction de sécurité sociale, la réparation de l'ensemble des dommages non couverts par le livre IV du code de la sécurité sociale qui sont résultés pour elle de l'accident ; qu'aux termes du premier alinéa de l'article L. 452-5 du même code : " Si l'accident est dû à la faute intentionnelle de l'employeur ou de l'un de ses préposés, la victime ou ses ayants droit conserve contre l'auteur de l'accident le droit de demander la réparation du préjudice causé, conformément aux règles du droit commun, dans la mesure où ce préjudice n'est pas réparé par application du présent livre " ; que le premier alinéa de l'article L. 454-1 de ce code dispose : " Si la lésion dont est atteint l'assuré social est imputable à une personne autre que l'employeur ou ses préposés, la victime ou ses ayants droit conserve contre l'auteur de l'accident le droit de demander la réparation du préjudice causé, conformément aux règles de droit commun, dans la mesure où ce préjudice n'est pas réparé par application du présent livre " ; qu'enfin, aux termes de l'article L. 421-9 du code de l'action sociale et des familles dans sa rédaction alors applicable, dont les dispositions sont désormais reprises à l'article L. 421-13 du même code : " Les assistants maternels agréés employés par des particuliers doivent obligatoirement s'assurer pour les dommages que les enfants gardés pourraient provoquer et pour ceux dont ils pourraient être victimes. (...). / Les assistants maternels agréés employés par des personnes morales sont obligatoirement couverts contre les mêmes risques par les soins desdites personnes morales " ;<br/>
<br/>
              Considérant qu'il résulte des dispositions précitées qu'un agent contractuel de droit public peut demander au juge administratif la réparation par son employeur du préjudice que lui a causé l'accident du travail dont il a été victime, dans la mesure où ce préjudice n'est pas réparé par application du code de la sécurité sociale, lorsque cet accident est dû à la faute intentionnelle de cet employeur ou de l'un de ses préposés ; qu'il peut également exercer une action en réparation de l'ensemble des préjudices résultant de cet accident non couverts par le livre IV du code de la sécurité sociale, contre son employeur, devant la juridiction de sécurité sociale, en cas de faute inexcusable de ce dernier, ou contre une personne autre que l'employeur ou ses préposés, conformément aux règles du droit commun, lorsque la lésion dont il a été la victime est imputable à ce tiers ;<br/>
<br/>
              Considérant qu'il résulte, en revanche, des mêmes dispositions qu'en dehors des hypothèses dans lesquelles le législateur a entendu instituer un régime de responsabilité particulier, comme c'est le cas, notamment, pour les assistants maternels agréés, envers lesquels la responsabilité du département, dont relève le service d'aide sociale à l'enfance, est engagée, même sans faute, pour les dommages subis du fait d'un enfant dont l'accueil leur a été confié, un agent contractuel de droit public, dès lors qu'il ne se prévaut pas d'une faute intentionnelle de son employeur ou de l'un des préposés de celui-ci, ne peut exercer contre cet employeur une action en réparation devant les juridictions administratives, conformément aux règles du droit commun, à la suite d'un accident du travail dont il a été la victime et ne peut, en particulier, lorsque cet accident est imputable à un mineur dont le juge des enfants avait confié la garde à son employeur dans le cadre d'une mesure d'assistance éducative et avait ainsi transféré à ce dernier la responsabilité d'organiser, diriger et contrôler la vie de ce mineur, rechercher l'engagement de la responsabilité sans faute de son employeur en tant que personne responsable de ce mineur ; <br/>
<br/>
              Considérant, en premier lieu, que, dès lors qu'un département emploie une monitrice éducatrice contractuelle lors de l'agression dont celle-ci est victime dans les locaux du réseau éducatif départemental, sa qualité d'employeur prévaut, pour les actions en réparation de cet accident du travail, sur celle de personne chargée de la responsabilité d'organiser, diriger et contrôler la vie du mineur auquel cet accident est imputable ; que, dès lors, en relevant, d'une part, que MmeA..., monitrice éducatrice contractuelle du département lors de l'agression dont elle a été victime le 11 avril 2005, n'avait pas la qualité de tiers vis-à-vis du service départemental d'aide sociale à l'enfance, et, d'autre part, qu'elle n'avait pas la qualité d'assistante maternelle agréée relevant du régime spécifique de responsabilité qui découle de l'article L. 421-9 du code de l'action sociale et des familles, et en déduisant de ces constatations que Mme A...ne pouvait rechercher la responsabilité sans faute du département à raison des préjudices consécutifs à l'agression dont elle a été la victime, le tribunal administratif, qui a suffisamment motivé son jugement sur ce point, n'a pas commis d'erreur de droit ; <br/>
<br/>
              Considérant, en second lieu, qu'en jugeant qu'il ne ressortait pas des pièces du dossier, notamment du procès-verbal du 13 avril 2005, que l'employeur de Mme A...aurait commis une faute de nature à engager sa responsabilité selon les règles du droit commun dans le cadre d'une action en réparation devant le juge administratif, le tribunal administratif a exactement qualifié les faits qui lui étaient soumis ;<br/>
<br/>
              Considérant qu'il résulte de tout ce qui précède que Mme A... n'est pas fondée à demander l'annulation du jugement du tribunal administratif de Nancy du 8 juillet 2008 ;<br/>
<br/>
              Considérant qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par le département de Meurthe-et-Moselle tendant à l'application des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de Mme A...est rejeté.<br/>
Article 2 : Les conclusions du département de Meurthe-et-Moselle tendant à l'application de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La présente décision sera notifiée à Mme B...A...et au département de Meurthe-et-Moselle.<br/>
Une copie en sera adressée pour information au ministre de l'intérieur, de l'outre-mer, des collectivités territoriales et de l'immigration.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">36-13-03 FONCTIONNAIRES ET AGENTS PUBLICS. CONTENTIEUX DE LA FONCTION PUBLIQUE. CONTENTIEUX DE L'INDEMNITÉ. - AGENT CONTRACTUEL DE DROIT PUBLIC BLESSÉ PAR UN MINEUR CONFIÉ AU SERVICE DÉPARTEMENTAL DE L'AIDE SOCIALE À L'ENFANCE - 1) RÉPARATION DEVANT LE JUGE ADMINISTRATIF - EXISTENCE - CONDITIONS - PRÉJUDICE NON RÉPARÉ PAR LE RÉGIME DES ACCIDENTS DU TRAVAIL ET FAUTE INTENTIONNELLE DE L'EMPLOYEUR OU DE L'UN DE SES PRÉPOSÉS - 2) RÉPARATION DEVANT LA JURIDICTION DE SÉCURITÉ SOCIALE - RÉPARATION INTÉGRALE DU PRÉJUDICE - EXISTENCE - CONDITION - FAUTE INEXCUSABLE DE L'EMPLOYEUR [RJ1] - 3) RESPONSABILITÉ SANS FAUTE DE L'EMPLOYEUR - ABSENCE, EN DEHORS DES RÉGIMES DE RESPONSABILITÉ PARTICULIERS INSTITUÉS PAR LE LÉGISLATEUR [RJ2].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">60-01-02-01-005 RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. FAITS SUSCEPTIBLES OU NON D'OUVRIR UNE ACTION EN RESPONSABILITÉ. FONDEMENT DE LA RESPONSABILITÉ. RESPONSABILITÉ SANS FAUTE. CAS DANS LESQUELS LE TERRAIN DE LA RESPONSABILITÉ SANS FAUTE NE PEUT ÊTRE UTILEMENT INVOQUÉ. - AGENT CONTRACTUEL DE DROIT PUBLIC BLESSÉ PAR UN MINEUR CONFIÉ AU SERVICE DÉPARTEMENTAL DE L'AIDE SOCIALE À L'ENFANCE - EXCEPTION - RÉGIMES DE RESPONSABILITÉ PARTICULIERS INSTITUÉS PAR LE LÉGISLATEUR [RJ2].
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">60-01-02-02 RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. FAITS SUSCEPTIBLES OU NON D'OUVRIR UNE ACTION EN RESPONSABILITÉ. FONDEMENT DE LA RESPONSABILITÉ. RESPONSABILITÉ POUR FAUTE. - AGENT CONTRACTUEL DE DROIT PUBLIC BLESSÉ PAR UN MINEUR CONFIÉ AU SERVICE DÉPARTEMENTAL DE L'AIDE SOCIALE À L'ENFANCE - 1) RÉPARATION DEVANT LE JUGE ADMINISTRATIF - EXISTENCE - CONDITIONS - PRÉJUDICE NON RÉPARÉ PAR LE RÉGIME DES ACCIDENTS DU TRAVAIL ET FAUTE INTENTIONNELLE DE L'EMPLOYEUR OU DE L'UN DE SES PRÉPOSÉS - 2) RÉPARATION DEVANT LA JURIDICTION DE SÉCURITÉ SOCIALE - RÉPARATION INTÉGRALE DU PRÉJUDICE - EXISTENCE - CONDITION - FAUTE INEXCUSABLE DE L'EMPLOYEUR [RJ1].
</SCT>
<SCT ID="8D" TYPE="PRINCIPAL">60-02-012 RESPONSABILITÉ DE LA PUISSANCE PUBLIQUE. RESPONSABILITÉ EN RAISON DES DIFFÉRENTES ACTIVITÉS DES SERVICES PUBLICS. SERVICES SOCIAUX. - AGENT CONTRACTUEL DE DROIT PUBLIC BLESSÉ PAR UN MINEUR CONFIÉ AU SERVICE DÉPARTEMENTAL DE L'AIDE SOCIALE À L'ENFANCE - 1) RÉPARATION DEVANT LE JUGE ADMINISTRATIF - EXISTENCE - CONDITIONS - PRÉJUDICE NON RÉPARÉ PAR LE RÉGIME DES ACCIDENTS DU TRAVAIL ET FAUTE INTENTIONNELLE DE L'EMPLOYEUR OU DE L'UN DE SES PRÉPOSÉS - 2) RÉPARATION DEVANT LA JURIDICTION DE SÉCURITÉ SOCIALE - RÉPARATION INTÉGRALE DU PRÉJUDICE - EXISTENCE - CONDITION - FAUTE INEXCUSABLE DE L'EMPLOYEUR [RJ1] - 3) POSSIBILITÉ DE RECHERCHER LA RESPONSABILITÉ SANS FAUTE DE L'EMPLOYEUR - ABSENCE, EN DEHORS DES RÉGIMES DE RESPONSABILITÉ PARTICULIERS INSTITUÉS PAR LE LÉGISLATEUR [RJ2].
</SCT>
<ANA ID="9A"> 36-13-03 1) Un agent contractuel de droit public peut demander au juge administratif la réparation par son employeur du préjudice que lui a causé l'accident du travail dont il a été victime, dans la mesure où ce préjudice n'est pas réparé par application du code de la sécurité sociale, lorsque cet accident est dû à la faute intentionnelle de cet employeur ou de l'un de ses préposés.... ...2) Il peut également exercer une action en réparation de l'ensemble des préjudices résultant de cet accident non couverts par le livre IV du code de la sécurité sociale, contre son employeur, devant la juridiction de sécurité sociale, en cas de faute inexcusable de ce dernier, ou contre une personne autre que l'employeur ou ses préposés, conformément aux règles du droit commun, lorsque la lésion dont il a été la victime est imputable à ce tiers.... ...3) En revanche, en dehors des hypothèses dans lesquelles le législateur a entendu instituer un régime de responsabilité particulier, comme c'est le cas, notamment, pour les assistants maternels agréés, envers lesquels la responsabilité du département, dont relève le service d'aide sociale à l'enfance, est engagée, même sans faute, pour les dommages subis du fait d'un enfant dont l'accueil leur a été confié, un agent contractuel de droit public, dès lors qu'il ne se prévaut pas d'une faute intentionnelle de son employeur ou de l'un des préposés de celui-ci, ne peut exercer contre cet employeur une action en réparation devant les juridictions administratives, conformément aux règles du droit commun, à la suite d'un accident du travail dont il a été la victime. Il ne peut pas non plus, en particulier, lorsque cet accident est imputable à un mineur dont le juge des enfants avait confié la garde à son employeur dans le cadre d'une mesure d'assistance éducative et avait ainsi transféré à ce dernier la responsabilité d'organiser, diriger et contrôler la vie de ce mineur, rechercher l'engagement de la responsabilité sans faute de son employeur en tant que personne responsable de ce mineur.</ANA>
<ANA ID="9B"> 60-01-02-01-005 En dehors des hypothèses dans lesquelles le législateur a entendu instituer un régime de responsabilité particulier, comme c'est le cas, notamment, pour les assistants maternels agréés, envers lesquels la responsabilité du département, dont relève le service d'aide sociale à l'enfance, est engagée, même sans faute, pour les dommages subis du fait d'un enfant dont l'accueil leur a été confié, un agent contractuel de droit public, dès lors qu'il ne se prévaut pas d'une faute intentionnelle de son employeur ou de l'un des préposés de celui-ci, ne peut exercer contre cet employeur une action en réparation devant les juridictions administratives, conformément aux règles du droit commun, à la suite d'un accident du travail dont il a été la victime. Il ne peut pas non plus, en particulier, lorsque cet accident est imputable à un mineur dont le juge des enfants avait confié la garde à son employeur dans le cadre d'une mesure d'assistance éducative et avait ainsi transféré à ce dernier la responsabilité d'organiser, diriger et contrôler la vie de ce mineur, rechercher l'engagement de la responsabilité sans faute de son employeur en tant que personne responsable de ce mineur.</ANA>
<ANA ID="9C"> 60-01-02-02 1) Un agent contractuel de droit public peut demander au juge administratif la réparation par son employeur du préjudice que lui a causé l'accident du travail dont il a été victime, dans la mesure où ce préjudice n'est pas réparé par application du code de la sécurité sociale, lorsque cet accident est dû à la faute intentionnelle de cet employeur ou de l'un de ses préposés.... ...2) Il peut également exercer une action en réparation de l'ensemble des préjudices résultant de cet accident non couverts par le livre IV du code de la sécurité sociale, contre son employeur, devant la juridiction de sécurité sociale, en cas de faute inexcusable de ce dernier, ou contre une personne autre que l'employeur ou ses préposés, conformément aux règles du droit commun, lorsque la lésion dont il a été la victime est imputable à ce tiers.</ANA>
<ANA ID="9D"> 60-02-012 1) Un agent contractuel de droit public peut demander au juge administratif la réparation par son employeur du préjudice que lui a causé l'accident du travail provoqué, dans la mesure où ce préjudice n'est pas réparé par application du code de la sécurité sociale, lorsque cet accident est dû à la faute intentionnelle de cet employeur ou de l'un de ses préposés.... ...2) Il peut également exercer une action en réparation de l'ensemble des préjudices résultant de cet accident non couverts par le livre IV du code de la sécurité sociale, contre son employeur, devant la juridiction de sécurité sociale, en cas de faute inexcusable de ce dernier, ou contre une personne autre que l'employeur ou ses préposés, conformément aux règles du droit commun, lorsque la lésion dont il a été la victime est imputable à ce tiers.... ...3) En revanche, en dehors des hypothèses dans lesquelles le législateur a entendu instituer un régime de responsabilité particulier, comme c'est le cas, notamment, pour les assistants maternels agréés, envers lesquels la responsabilité du département, dont relève le service d'aide sociale à l'enfance, est engagée, même sans faute, pour les dommages subis du fait d'un enfant dont l'accueil leur a été confié, un agent contractuel de droit public, dès lors qu'il ne se prévaut pas d'une faute intentionnelle de son employeur ou de l'un des préposés de celui-ci, ne peut exercer contre cet employeur une action en réparation devant les juridictions administratives, conformément aux règles du droit commun, à la suite d'un accident du travail dont il a été la victime. Il ne peut pas non plus, en particulier, lorsque cet accident est imputable à un mineur dont le juge des enfants avait confié la garde à son employeur dans le cadre d'une mesure d'assistance éducative et avait ainsi transféré à ce dernier la responsabilité d'organiser, diriger et contrôler la vie de ce mineur, rechercher l'engagement de la responsabilité sans faute de son employeur en tant que personne responsable de ce mineur.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr. Cons. Const., 18 juin 2010, décision n° 2010-8 QPC, Époux L.,,[RJ2] Comp. CE, 21 juin 1895, Cames, n° 82490, p. 509.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
