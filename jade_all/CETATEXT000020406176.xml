<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000020406176</ID>
<ANCIEN_ID>JG_L_2007_07_000000283892</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/20/40/61/CETATEXT000020406176.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère et 6ème sous-sections réunies, 06/07/2007, 283892</TITRE>
<DATE_DEC>2007-07-06</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>283892</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère et 6ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Delarue</PRESIDENT>
<AVOCATS>SCP LYON-CAEN, FABIANI, THIRIEZ</AVOCATS>
<RAPPORTEUR>Mlle Anne  Courrèges</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Devys Christophe</COMMISSAIRE_GVT>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu, avec les pièces qui y sont visées, la décision en date du 19 octobre 2005 par laquelle le Conseil d'Etat, statuant au contentieux sur les requêtes enregistrées sous les n°s 283892, 284472, 284555 et 284718, présentées respectivement pour la CONFEDERATION GENERALE DU TRAVAIL, pour la CONFEDERATION FRANCAISE DEMOCRATIQUE DU TRAVAIL, pour la CONFEDERATION FRANCAISE DE L'ENCADREMENT CGC et la CONFEDERATION FRANCAISE DES TRAVAILLEURS CHRETIENS et pour la CONFEDERATION GENERALE DU TRAVAIL - FORCE OUVRIERE et tendant à l'annulation, pour excès de pouvoir, de l'ordonnance n° 2005-892 du 2 août 2005 relative à l'aménagement des règles de décompte des effectifs des entreprises, a sursis à statuer jusqu'à ce que la Cour de justice des Communautés européennes se soit prononcée sur les questions de savoir :<br/>
<br/>
              1°) si, compte tenu de l'objet de la directive 2002/14/CE du 11 mars 2002, qui est, aux termes du 1 de son article 1er, d'établir un cadre général fixant des exigences minimales pour le droit à l'information et à la consultation des travailleurs dans les entreprises ou les établissements situés dans la Communauté, le renvoi aux Etats membres du soin de déterminer le mode de calcul des seuils de travailleurs employés que cette directive énonce, doit être regardé comme permettant à ces Etats de procéder à la prise en compte différée de certaines catégories de travailleurs pour l'application de ces seuils ;<br/>
<br/>
              2°) dans quelle mesure la directive 98/59/CE du 20 juillet 1998 peut être interprétée comme autorisant un dispositif ayant pour effet que certains établissements occupant habituellement plus de vingt travailleurs se trouvent dispensés, fût-ce temporairement, de l'obligation de créer une structure de représentation des travailleurs en raison de règles de décompte des effectifs excluant la prise en compte de certaines catégories de salariés pour l'application des dispositions organisant cette représentation ;<br/>
<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu la directive 98/59/CE du 20 juillet 1998 concernant le rapprochement des législations des Etats membres relatives aux licenciements collectifs ;<br/>
<br/>
              Vu la directive 2002/14/CE du 11 mars 2002 établissant un cadre général relatif à l'information et à la consultation des travailleurs dans la Communauté européenne ;<br/>
<br/>
              Vu le code du travail ;<br/>
<br/>
              Vu la loi n° 2005-846 du 26 juillet 2005 habilitant le gouvernement à prendre, par ordonnance, des mesures d'urgence pour l'emploi, notamment le 5° de son article 1er ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mlle Anne Courrèges, Maître des Requêtes,  <br/>
<br/>
              - les observations de la SCP Lyon-Caen, Fabiani, Thiriez, avocat de la CONFEDERATION GENERALE DU TRAVAIL, de la SCP Masse-Dessen Thouvenin, avocat de la CONFEDERATION FRANCAISE DEMOCRATIQUE DU TRAVAIL, de la CONFEDERATION FRANCAISE DE L'ENCADREMENT CGC et de la CONFEDERATION FRANCAISE DES TRAVAILLEURS CHRETIENS et de Me Haas, avocat de la CONFEDERATION GENERALE DU TRAVAIL - FORCE OUVRIERE,<br/>
<br/>
              - les conclusions de M. Christophe Devys, Commissaire du gouvernement ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant que la directive 2002/14/CE du 11 mars 2002, qui définit comme travailleur toute personne protégée à ce titre par la législation nationale sur l'emploi, prescrit aux Etats membres d'organiser l'information et la consultation des travailleurs dans les établissements qui, selon son article 3, paragraphe 1, en emploient au moins vingt ou les entreprises qui en emploient au moins cinquante ; que, selon la directive 98/59/CE du 20 juillet 1998, cette information et cette consultation doivent prendre la forme de procédures portant sur les possibilités d'éviter ou de réduire le nombre des licenciements collectifs ainsi que sur les possibilités d'en atténuer les conséquences par des mesures sociales d'accompagnement dans tous les cas où, d'après son article 1er, paragraphe 1, sous a), au moins dix travailleurs ont été licenciés pendant une période de trente jours, dans les entreprises employant habituellement plus de vingt et moins de cent travailleurs ; que la directive 2002/14/CE précise que les Etats membres déterminent le mode de calcul des seuils de travailleurs employés ;<br/>
<br/>
              Considérant que l'article 1er de l'ordonnance attaquée,  prise en vertu du 5° de l'article 1er de la loi du 26 juillet 2005 ayant habilité le gouvernement à prendre, par ordonnance, des mesures visant à « aménager les règles de décompte des effectifs utilisées pour la mise en oeuvre de dispositions relatives au droit du travail ou d'obligations financières imposées par d'autres législations, pour favoriser, à compter du 22 juin 2005, l'embauche par les entreprises de salariés âgés de moins de vingt-six ans », dispose que le salarié embauché à compter du 22 juin 2005 et âgé de moins de vingt-six ans n'est pas pris en compte, jusqu'à ce qu'il ait atteint cet âge, dans le calcul de l'effectif du personnel de l'entreprise dont il relève, quelle que soit la nature du contrat qui le lie à cette entreprise ; que dans le cas d'établissements comportant plus de vingt travailleurs, mais parmi lesquels moins de onze sont âgés de vingt-six ans ou plus, l'application de ces dispositions peut avoir pour conséquence de dispenser l'employeur de l'obligation d'assurer l'élection des délégués du personnel, et, partant, de faire obstacle au respect de l'obligation de consultation qui lui incombe, en vertu de l'article L. 321-2 du code du travail, lorsqu'il envisage de procéder au licenciement pour motif économique d'au moins dix salariés dans une même période de trente jours ;<br/>
<br/>
              Considérant que dans l'arrêt du 18 janvier 2007 par lequel elle s'est prononcée sur les questions dont le Conseil d'Etat, statuant au contentieux, l'avait saisie à titre préjudiciel après avoir écarté les autres moyens des requêtes, la Cour de justice des Communautés européennes a dit pour droit que l'article 3, paragraphe 1, de la directive 2002/14/CE du 11 mars 2002 et l'article 1er, paragraphe 1, sous a), de la directive 98/59/CE du 20 juillet 1998 doivent être interprétés comme s'opposant à une réglementation nationale excluant, fût-ce temporairement, une catégorie déterminée de travailleurs du calcul du nombre de travailleurs employés au sens de ces dispositions ;<br/>
<br/>
              Considérant qu'il découle de l'interprétation ainsi donnée par la Cour de justice des Communautés européennes que, dès lors qu'elles procèdent à une telle exclusion, les dispositions de l'article 1er de l'ordonnance attaquée, qui peuvent avoir pour effet de soustraire certains employeurs aux obligations prévues par les directives du 11 mars 2002 et du 20 juillet 1998, et, par là même, de priver les travailleurs qu'ils emploient des droits que celles-ci leur reconnaissent, sont incompatibles avec ces directives ; qu'il suit de là que les confédérations requérantes sont fondées à demander l'annulation pour excès de pouvoir de l'ordonnance litigieuse ;<br/>
<br/>
              Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat le versement à chacune des confédérations requérantes d'une somme de 5 000 euros en application des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'ordonnance n° 2005-892 du 2 août 2005 est annulée.<br/>
<br/>
Article 2 : L'Etat versera à la CONFEDERATION GENERALE DU TRAVAIL, à la CONFEDERATION FRANCAISE DEMOCRATIQUE DU TRAVAIL, à la CONFEDERATION FRANCAISE DE L'ENCADREMENT CGC, à la CONFEDERATION FRANCAISE DES TRAVAILLEURS CHRETIENS et à la CONFEDERATION GENERALE DU TRAVAIL - FORCE OUVRIERE une somme de 5 000 euros à chacune au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
Article 3 : La présente décision sera notifiée à la CONFEDERATION GENERALE DU TRAVAIL, à la CONFEDERATION FRANCAISE DEMOCRATIQUE DU TRAVAIL, à la CONFEDERATION FRANCAISE DE L'ENCADREMENT CGC, à la CONFEDERATION FRANCAISE DES TRAVAILLEURS CHRETIENS, à la CONFEDERATION GENERALE DU TRAVAIL - FORCE OUVRIERE, au Premier ministre et au ministre du travail, des relations sociales et de la solidarité.<br/>
<br/>
</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">15-02-04 COMMUNAUTÉS EUROPÉENNES ET UNION EUROPÉENNE. PORTÉE DES RÈGLES DE DROIT COMMUNAUTAIRE ET DE L'UNION EUROPÉENNE. DIRECTIVES COMMUNAUTAIRES. - DIRECTIVES 98/59/CE DU 20 JUILLET 1998 ET 2002/14/CE DU 11 MARS 2002 - INFORMATION ET CONSULTATION DES TRAVAILLEURS - CONDITION - SEUIL D'EFFECTIFS - NON PRISE EN COMPTE DANS LE CALCUL DE L'EFFECTIF DES SALARIÉS ÂGÉS DE MOINS DE VINGT-SIX ANS (ORDONNANCE DU 2 AOÛT 2005) - INCOMPATIBILITÉ - EXISTENCE [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">15-03-03-01 COMMUNAUTÉS EUROPÉENNES ET UNION EUROPÉENNE. APPLICATION DU DROIT COMMUNAUTAIRE PAR LE JUGE ADMINISTRATIF FRANÇAIS. PRISE EN COMPTE DES ARRÊTS DE LA COUR DE JUSTICE. INTERPRÉTATION DU DROIT COMMUNAUTAIRE. - DIRECTIVES 98/59/CE DU 20 JUILLET 1998 ET 2002/14/CE DU 11 MARS 2002 - INFORMATION ET CONSULTATION DES TRAVAILLEURS - CONDITION - SEUIL D'EFFECTIFS - NON PRISE EN COMPTE DANS LE CALCUL DE L'EFFECTIF DES SALARIÉS ÂGÉS DE MOINS DE VINGT-SIX ANS (ORDONNANCE DU 2 AOÛT 2005) - INCOMPATIBILITÉ - EXISTENCE [RJ1].
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">15-05-17 COMMUNAUTÉS EUROPÉENNES ET UNION EUROPÉENNE. RÈGLES APPLICABLES. POLITIQUE SOCIALE. - DIRECTIVES 98/59/CE DU 20 JUILLET 1998 ET 2002/14/CE DU 11 MARS 2002 - INFORMATION ET CONSULTATION DES TRAVAILLEURS - CONDITION - SEUIL D'EFFECTIFS - NON PRISE EN COMPTE DANS LE CALCUL DE L'EFFECTIF DES SALARIÉS ÂGÉS DE MOINS DE VINGT-SIX ANS (ORDONNANCE DU 2 AOÛT 2005) - INCOMPATIBILITÉ - EXISTENCE [RJ1].
</SCT>
<SCT ID="8D" TYPE="PRINCIPAL">66-04 TRAVAIL ET EMPLOI. INSTITUTIONS REPRÉSENTATIVES DU PERSONNEL. - OBLIGATION D'ASSURER L'ÉLECTION DE DÉLÉGUÉS DU PERSONNEL (ART. L. 321-1 DU CODE DU TRAVAIL) ET DE CONSULTER LE COMITÉ D'ENTREPRISE OU LES DÉLÉGUÉS DU PERSONNEL EN CAS DE LICENCIEMENT POUR MOTIF ÉCONOMIQUE (ART. L. 321-2 DU MÊME CODE) - CHAMP D'APPLICATION - INCIDENCE DE L'ORDONNANCE DU 2 AOÛT 2005 EXCLUANT LES SALARIÉS DE MOINS DE VINGT-SIX ANS DU CALCUL DES EFFECTIFS - COMPATIBILITÉ AVEC LES OBJECTIFS DES DIRECTIVES 2002/14/CE DU 11 MARS 2002 ET 98/59/CE DU 20 JUILLET 1998 - ABSENCE - CONSÉQUENCE - ANNULATION [RJ1].
</SCT>
<SCT ID="8E" TYPE="PRINCIPAL">66-04-03-01 TRAVAIL ET EMPLOI. INSTITUTIONS REPRÉSENTATIVES DU PERSONNEL. DÉLÉGUÉS DU PERSONNEL. ORGANISATION DES ÉLECTIONS. - OBLIGATION D'ASSURER L'ÉLECTION DE DÉLÉGUÉS DU PERSONNEL (ART. L. 321-1 DU CODE DU TRAVAIL) ET DE CONSULTER LE COMITÉ D'ENTREPRISE OU LES DÉLÉGUÉS DU PERSONNEL EN CAS DE LICENCIEMENT POUR MOTIF ÉCONOMIQUE (ART. L. 321-2 DU MÊME CODE) - CHAMP D'APPLICATION - INCIDENCE DE L'ORDONNANCE DU 2 AOÛT 2005 EXCLUANT LES SALARIÉS DE MOINS DE VINGT-SIX ANS DU CALCUL DES EFFECTIFS - COMPATIBILITÉ AVEC LES OBJECTIFS DES DIRECTIVES 2002/14/CE DU 11 MARS 2002 ET 98/59/CE DU 20 JUILLET 1998 - ABSENCE - CONSÉQUENCE - ANNULATION [RJ1].
</SCT>
<SCT ID="8F" TYPE="PRINCIPAL">66-07-02-02-02 TRAVAIL ET EMPLOI. LICENCIEMENTS. AUTORISATION ADMINISTRATIVE - SALARIÉS NON PROTÉGÉS - LICENCIEMENT POUR MOTIF ÉCONOMIQUE. PROCÉDURE PRÉALABLE À L'AUTORISATION ADMINISTRATIVE. LICENCIEMENT COLLECTIF. - OBLIGATION D'ASSURER L'ÉLECTION DE DÉLÉGUÉS DU PERSONNEL (ART. L. 321-1 DU CODE DU TRAVAIL) ET DE CONSULTER LE COMITÉ D'ENTREPRISE OU LES DÉLÉGUÉS DU PERSONNEL EN CAS DE LICENCIEMENT POUR MOTIF ÉCONOMIQUE (ART. L. 321-2 DU MÊME CODE) - CHAMP D'APPLICATION - INCIDENCE DE L'ORDONNANCE DU 2 AOÛT 2005 EXCLUANT LES SALARIÉS DE MOINS DE VINGT-SIX ANS DU CALCUL DES EFFECTIFS - COMPATIBILITÉ AVEC LES OBJECTIFS DES DIRECTIVES 2002/14/CE DU 11 MARS 2002 ET 98/59/CE DU 20 JUILLET 1998 - ABSENCE - CONSÉQUENCE - ANNULATION [RJ1].
</SCT>
<ANA ID="9A"> 15-02-04 Selon l'arrêt du 18 janvier 2007 de la Cour de justice des communautés européennes, rendu sur renvoi préjudiciel du Conseil d'Etat, les stipulations des directives 2002/14/CE du 11 mars 2002 et 98/59/CE du 20 juillet 1998 qui prévoient l'information et la consultation des travailleurs dans les établissements qui en emploient au moins vingt (paragraphe 1 de l'article 3 de la directive de 2002), notamment dans le cadre d'une procédure de licenciement (a) du paragraphe 1 de l'article 1er de la directive de 1998), doivent être interprétées comme s'opposant à une réglementation nationale excluant, fût-ce temporairement, une catégorie déterminée de travailleurs du calcul du nombre de travailleurs employés au sens de ces dispositions. Il découle de cette interprétation que les dispositions de l'article 1er de l'ordonnance n° 2005-892 du 2 août 2005, en excluant les salariés embauchés à compter du 22 juin 2005, âgés de moins de vingt-six ans, du calcul des effectifs des entreprises dont ils relèvent, peuvent avoir pour effet de soustraire certains employeurs aux obligations prévues par ces directives et, par là, de priver les travailleurs qu'ils emploient des droits que celles-ci leur reconnaissent et sont par suite incompatibles avec ces directives. Annulation pour excès de pouvoir de l'ordonnance.</ANA>
<ANA ID="9B"> 15-03-03-01 Selon l'arrêt du 18 janvier 2007 de la Cour de justice des communautés européennes, rendu sur renvoi préjudiciel du Conseil d'Etat, les stipulations des directives 2002/14/CE du 11 mars 2002 et 98/59/CE du 20 juillet 1998 qui prévoient l'information et la consultation des travailleurs dans les établissements qui en emploient au moins vingt (paragraphe 1 de l'article 3 de la directive de 2002), notamment dans le cadre d'une procédure de licenciement (a) du paragraphe 1 de l'article 1er de la directive de 1998), doivent être interprétées comme s'opposant à une réglementation nationale excluant, fût-ce temporairement, une catégorie déterminée de travailleurs du calcul du nombre de travailleurs employés au sens de ces dispositions. Il découle de cette interprétation que les dispositions de l'article 1er de l'ordonnance n° 2005-892 du 2 août 2005, en excluant les salariés embauchés à compter du 22 juin 2005, âgés de moins de vingt-six ans, du calcul des effectifs des entreprises dont ils relèvent, peuvent avoir pour effet de soustraire certains employeurs aux obligations prévues par ces directives et, par là, de priver les travailleurs qu'ils emploient des droits que celles-ci leur reconnaissent et sont par suite incompatibles avec ces directives. Annulation pour excès de pouvoir de l'ordonnance.</ANA>
<ANA ID="9C"> 15-05-17 Selon l'arrêt du 18 janvier 2007 de la Cour de justice des communautés européennes, rendu sur renvoi préjudiciel du Conseil d'Etat, les stipulations des directives 2002/14/CE du 11 mars 2002 et 98/59/CE du 20 juillet 1998 qui prévoient l'information et la consultation des travailleurs dans les établissements qui en emploient au moins vingt (paragraphe 1 de l'article 3 de la directive de 2002), notamment dans le cadre d'une procédure de licenciement (a) du paragraphe 1 de l'article 1er de la directive de 1998), doivent être interprétées comme s'opposant à une réglementation nationale excluant, fût-ce temporairement, une catégorie déterminée de travailleurs du calcul du nombre de travailleurs employés au sens de ces dispositions. Il découle de cette interprétation que les dispositions de l'article 1er de l'ordonnance n° 2005-892 du 2 août 2005, en excluant les salariés embauchés à compter du 22 juin 2005, âgés de moins de vingt-six ans, du calcul des effectifs des entreprises dont ils relèvent, peuvent avoir pour effet de soustraire certains employeurs aux obligations prévues par ces directives et, par là, de priver les travailleurs qu'ils emploient des droits que celles-ci leur reconnaissent et sont par suite incompatibles avec ces directives. Annulation pour excès de pouvoir de l'ordonnance.</ANA>
<ANA ID="9D"> 66-04 Selon l'arrêt du 18 janvier 2007 de la Cour de justice des communautés européennes, rendu sur renvoi préjudiciel du Conseil d'Etat, les stipulations des directives 2002/14/CE du 11 mars 2002 et 98/59/CE du 20 juillet 1998 qui prévoient l'information et la consultation des travailleurs dans les établissements qui en emploient au moins vingt (paragraphe 1 de l'article 3 de la directive de 2002), notamment dans le cadre d'une procédure de licenciement (a) du paragraphe 1 de l'article 1er de la directive de 1998), doivent être interprétées comme s'opposant à une réglementation nationale excluant, fût-ce temporairement, une catégorie déterminée de travailleurs du calcul du nombre de travailleurs employés au sens de ces dispositions. Il découle de cette interprétation que les dispositions de l'article 1er de l'ordonnance n° 2005-892 du 2 août 2005, en excluant les salariés embauchés à compter du 22 juin 2005, âgés de moins de vingt-six ans, du calcul des effectifs des entreprises dont ils relèvent, peuvent avoir pour effet de soustraire certains employeurs aux obligations prévues par ces directives et, par là, de priver les travailleurs qu'ils emploient des droits que celles-ci leur reconnaissent et sont par suite incompatibles avec ces directives. Annulation pour excès de pouvoir de l'ordonnance.</ANA>
<ANA ID="9E"> 66-04-03-01 Selon l'arrêt du 18 janvier 2007 de la Cour de justice des communautés européennes, rendu sur renvoi préjudiciel du Conseil d'Etat, les stipulations des directives 2002/14/CE du 11 mars 2002 et 98/59/CE du 20 juillet 1998 qui prévoient l'information et la consultation des travailleurs dans les établissements qui en emploient au moins vingt (paragraphe 1 de l'article 3 de la directive de 2002), notamment dans le cadre d'une procédure de licenciement (a) du paragraphe 1 de l'article 1er de la directive de 1998), doivent être interprétées comme s'opposant à une réglementation nationale excluant, fût-ce temporairement, une catégorie déterminée de travailleurs du calcul du nombre de travailleurs employés au sens de ces dispositions. Il découle de cette interprétation que les dispositions de l'article 1er de l'ordonnance n° 2005-892 du 2 août 2005, en excluant les salariés embauchés à compter du 22 juin 2005, âgés de moins de vingt-six ans, du calcul des effectifs des entreprises dont ils relèvent, peuvent avoir pour effet de soustraire certains employeurs aux obligations prévues par ces directives et, par là, de priver les travailleurs qu'ils emploient des droits que celles-ci leur reconnaissent et sont par suite incompatibles avec ces directives. Annulation pour excès de pouvoir de l'ordonnance.</ANA>
<ANA ID="9F"> 66-07-02-02-02 Selon l'arrêt du 18 janvier 2007 de la Cour de justice des communautés européennes, rendu sur renvoi préjudiciel du Conseil d'Etat, les stipulations des directives 2002/14/CE du 11 mars 2002 et 98/59/CE du 20 juillet 1998 qui prévoient l'information et la consultation des travailleurs dans les établissements qui en emploient au moins vingt (paragraphe 1 de l'article 3 de la directive de 2002), notamment dans le cadre d'une procédure de licenciement (a) du paragraphe 1 de l'article 1er de la directive de 1998), doivent être interprétées comme s'opposant à une réglementation nationale excluant, fût-ce temporairement, une catégorie déterminée de travailleurs du calcul du nombre de travailleurs employés au sens de ces dispositions. Il découle de cette interprétation que les dispositions de l'article 1er de l'ordonnance n° 2005-892 du 2 août 2005, en excluant les salariés embauchés à compter du 22 juin 2005, âgés de moins de vingt-six ans, du calcul des effectifs des entreprises dont ils relèvent, peuvent avoir pour effet de soustraire certains employeurs aux obligations prévues par ces directives et, par là, de priver les travailleurs qu'ils emploient des droits que celles-ci leur reconnaissent et sont par suite incompatibles avec ces directives. Annulation pour excès de pouvoir de l'ordonnance.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] V. aussi, sur cette question, la décision renvoyant à la CJCE la question préjudicielle, Section, 19 octobre 2005, Confédération générale du travail et autres, n°s 283892 284472 284555 284718, p. 434 et CJCE, 18 janvier 2007, Confédération générale du travail et a., aff. C-385/05, Rec. p. I-611.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
