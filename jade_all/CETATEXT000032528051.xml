<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032528051</ID>
<ANCIEN_ID>JG_L_2016_05_000000375161</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/52/80/CETATEXT000032528051.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 6ème - 1ère chambres réunies, 11/05/2016, 375161, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-05-11</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>375161</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>6ème - 1ère chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP SPINOSI, SUREAU ; SCP LE BRET-DESACHE</AVOCATS>
<RAPPORTEUR>Mme Mireille Le Corre</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Suzanne von Coester</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2016:375161.20160511</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. C...A..., M. B...A..., Mme H...A..., M. G...E..., Mme F...E..., M. J...-C... E...et Mme I...D...ont demandé au tribunal administratif de Versailles d'annuler pour excès de pouvoir l'arrêté du 24 janvier 2007 par lequel le préfet des Hauts-de-Seine a, d'une part, déclaré d'utilité publique la réalisation d'un programme de logements sociaux de la commune de Levallois-Perret et, d'autre part, prononcé la cessibilité des parcelles K9 et K8 situées respectivement aux n° 116 et 118 de la rue Anatole France. Par deux jugements n° 0703074 et 0703978 du 5 février 2009, le tribunal administratif de Versailles a rejeté leur demande.<br/>
<br/>
              Par un arrêt n° 09VE01036-09VE01180 du 24 juin 2010, la cour administrative d'appel de Versailles, a, d'une part, à la demande de M. C...A..., M. B...A..., Mme H... A..., M. G...E..., Mme F...E...et M. J...-C... E...et de Mme I...D..., annulé ces jugements du tribunal administratif de Versailles et, d'autre part, annulé cet arrêté.<br/>
<br/>
              Par une décision n° 343070 du 19 octobre 2012, le Conseil d'Etat, à la demande de la commune de Levallois-Perret, a annulé cet arrêt de la cour administrative d'appel de Versailles et renvoyé l'affaire à cette cour.<br/>
<br/>
              Par un arrêt n° 12VE03596 du 19 novembre 2013, la cour administrative d'appel de Versailles a annulé ces jugements du tribunal administratif de Versailles ainsi que l'arrêté du 24 janvier 2007.<br/>
<br/>
              Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique enregistrés les 3 février 2014, 5 mai 2014 et 4 mars 2016 au secrétariat du contentieux du Conseil d'Etat, la commune de Levallois-Perret demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter les requêtes des consorts A...et E...et de Mme D...;<br/>
<br/>
              3°) de mettre à la charge des consorts A...et E...et de Mme D...la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              -	le code général des collectivités territoriales ;<br/>
              -	le code de la construction et de l'habitation ; <br/>
              -	le code de l'expropriation pour cause d'utilité publique ;<br/>
              -	la loi n° 2000-1208 du 13 décembre 2000 ; <br/>
              -	le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Mireille Le Corre, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Suzanne von Coester, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Spinosi, Sureau, avocat de la commune de Levallois-perret et à la SCP Le Bret-Desaché, avocat de M. C... A...et autres ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 14 avril 2016, présentée par M. C...A..., M. B...A..., Mme H...A..., M. G...E..., Mme F...E..., M. J...-C... E...; <br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que, par un arrêté du 24 janvier 2007, le préfet des Hauts-de-Seine a, d'une part, déclaré d'utilité publique la réalisation d'un programme de logements sociaux et, d'autre part, prononcé la cessibilité des parcelles cadastrées K9 et K8, situées respectivement aux n° 116 et 118 de la rue Anatole France, nécessaires à la réalisation de cette opération ; que, par deux jugements nos 0703074 et 0703978 du 5 février 2009, le tribunal administratif de Versailles a rejeté les demandes d'annulation pour excès de pouvoir de cet arrêté présentées respectivement par les consorts A...et E...et par Mme D...; que, par un arrêt n° 09VE01036-09VE01180 du 24 juin 2010, la cour administrative d'appel de Versailles, sur les requêtes des intéressés, a annulé ces jugements et cet arrêté ; que, sur le pourvoi de la commune de Levallois-Perret, par une décision n° 343070 du 19 octobre 2012, le Conseil d'Etat statuant au contentieux a annulé cet arrêt et renvoyé l'affaire à cette cour ; que, par un arrêt n° 12VE03596 du 19 novembre 2013, contre lequel la commune de Levallois-Perret se pourvoit en cassation, la cour administrative d'appel de Versailles a, de nouveau, annulé ces jugements et l'arrêté du 24 janvier 2007 ; <br/>
<br/>
              Sur le pourvoi : <br/>
<br/>
              2. Considérant qu'il appartient au juge administratif, lorsqu'il doit se prononcer sur le caractère d'utilité publique d'une opération nécessitant l'expropriation d'immeubles ou de droits réels immobiliers, de contrôler successivement qu'elle répond à une finalité d'intérêt général, que l'expropriant n'était pas en mesure de réaliser l'opération dans des conditions équivalentes sans recourir à l'expropriation, notamment en utilisant des biens se trouvant dans son patrimoine et, enfin, que les atteintes à la propriété privée, le coût financier et, le cas échéant, les inconvénients d'ordre social ou économique que comporte l'opération ne sont pas excessifs eu égard à l'intérêt qu'elle présente ;<br/>
<br/>
              3. Considérant qu'il appartient au juge administratif,  lorsque la nécessité de l'expropriation est contestée devant lui, d'apprécier si l'expropriant ou, le cas échéant, le bénéficiaire de l'expropriation, disposait effectivement de terrains qui, eu égard, d'une part, à leurs caractéristiques, et notamment à leur situation, leur superficie et leur configuration et, d'autre part, à la nature de l'opération projetée, auraient permis de réaliser le projet dans des conditions équivalentes, sans recourir à l'expropriation ;<br/>
<br/>
              4. Considérant que, pour estimer que l'expropriation n'était pas en l'espèce nécessaire et annuler en conséquence la déclaration d'utilité publique litigieuse, la cour s'est bornée à relever que la commune était propriétaire, dans le secteur concerné par l'opération, de différents terrains susceptibles de permettre la construction de plusieurs dizaines de logements sociaux dans des conditions équivalentes, notamment sur les parcelles cadastrées 181, 166 et 169 ; qu'en se prononçant ainsi, sans préciser en quoi, compte tenu de l'argumentation détaillée présentée par la commune contestant ce point, ces terrains permettaient, eu égard à leurs caractéristiques et à la nature de l'opération projetée, de réaliser celle-ci sans expropriation, la cour a entaché son arrêt d'une insuffisance de motivation ; que, par suite, et sans qu'il soit besoin d'examiner les autres moyens du pourvoi, la commune de Levallois-Perret est fondée à demander l'annulation de l'arrêt qu'elle attaque ;<br/>
<br/>
              5. Considérant qu'aux termes du second alinéa de l'article L. 821-2 du code de justice administrative : " Lorsque l'affaire fait l'objet d'un second pourvoi en cassation, le Conseil d'Etat statue définitivement sur cette affaire " ; qu'il appartient, par suite, au Conseil d'Etat de régler l'affaire au fond ;<br/>
<br/>
              Sur les requêtes d'appel : <br/>
<br/>
              6. Considérant que les requêtes sont dirigées contre deux jugements statuant sur le même arrêté ; qu'il y a lieu de les joindre pour statuer par une seule décision ;<br/>
<br/>
              En ce qui concerne la fin de non-recevoir opposée par les requérants : <br/>
<br/>
              7. Considérant qu'il ressort des pièces du dossier que le maire de Levallois-Perret a été régulièrement habilité, par délibérations des 25 mars 2008 et 29 juin 2009, à représenter la commune pour défendre dans l'instance devant la cour administrative d'appel de Versailles ; que, par suite, et contrairement à ce que soutiennent les requérants, les mémoires de la commune sont recevables ; <br/>
<br/>
<br/>
<br/>
<br/>
              En ce qui concerne la déclaration d'utilité publique :<br/>
<br/>
              S'agissant de la légalité externe :<br/>
<br/>
              8. Considérant, en premier lieu, que s'agissant de la régularité de la procédure d'enquête publique, les requérants ont soulevé les mêmes moyens que ceux présentés devant le tribunal administratif de Versailles et tirés de l'insuffisance de la notice explicative du projet, de la sous-évaluation manifeste des coûts retenus dans l'appréciation sommaire des dépenses, ainsi que des insuffisances du rapport du commissaire enquêteur ; qu'il y a lieu, par adoption des motifs retenus par le tribunal administratif, de les écarter ; qu'en outre, aucune disposition n'impose à l'administration de transmettre le dossier d'enquête publique aux propriétaires concernés ; qu'il n'est pas établi que ce dossier aurait été incomplet s'agissant notamment des justifications du choix du projet ;<br/>
<br/>
              9. Considérant, en deuxième lieu, qu'il ressort des pièces du dossier que l'avis du commissaire-enquêteur n'a été assorti que de recommandations, qui ne peuvent être regardées comme une réserve ou une condition de nature à remettre en cause son sens favorable ; que par suite, le moyen tiré sur ce point de l'irrégularité de la procédure de l'enquête publique doit être écarté ;<br/>
<br/>
              S'agissant de la légalité interne :<br/>
<br/>
              10. Considérant, en premier lieu, que contrairement à ce que soutiennent les requérants, le délai de convocation des élus à la séance du conseil municipal est régi non par l'article L. 2121-11 du code général des collectivités territoriales, mais par l'article L. 2121-12 du même code, s'agissant d'une commune qui compte plus de 3 500 habitants ; qu'au demeurant, la convocation a été adressée aux élus le 21 février 2006, en vue d'une séance le 27 février 2006, dans le délai de cinq jours francs prescrit par ce dernier article ; que la note explicative de synthèse jointe à la convocation a permis une information suffisante des membres du conseil municipal ; que, dès lors, les requérants ne sont pas fondés à soutenir, par un moyen soulevé par la voie de l'exception, que la délibération du 27 février 2006 par laquelle le conseil municipal de Levallois-Perret a demandé au préfet de prendre l'arrêté attaqué serait illégale en raison de la méconnaissance des règles de convocation et d'information des conseillers municipaux ; <br/>
<br/>
              11. Considérant, en deuxième lieu, que les moyens tirés de ce que la délibération précitée du 27 février 2006 et l'arrêté du préfet portant ouverture de l'enquête publique n'auraient pas fait l'objet des mesures de publicité prescrites par la réglementation en vigueur ne sont pas assortis des précisions suffisantes permettant d'en apprécier le bien-fondé ; qu'aucune disposition n'impose, par ailleurs, ni que la délibération comporte le résultat du vote, ni que le maire prenne une décision de transmission du dossier nécessaire à la déclaration d'utilité publique ; <br/>
<br/>
              12. Considérant, en troisième lieu, que l'opération envisagée a eu pour objet d'accroître le parc de logements sociaux de la commune de Levallois-Perret, afin de se conformer aux exigences en matière de mixité sociale résultant notamment de la loi du 13 décembre 2000 relative à la solidarité et au renouvellement urbains ; qu'elle répond ainsi à une finalité d'intérêt général ;<br/>
<br/>
              13. Considérant que, pour contester la nécessité de recourir à l'expropriation, les requérants soutiennent que d'autres parcelles appartenant à la commune auraient permis de réaliser l'opération projetée dans des conditions équivalentes ; qu'eu égard toutefois à l'objet de l'opération projetée, qui tend à la construction, dans un secteur déterminé de la commune et conformément aux orientations de la politique municipale en matière d'habitat, d'un immeuble collectif de six étages comportant trente-cinq logements sociaux, il ne ressort pas des pièces du dossier que les diverses parcelles mentionnées par les requérants, eu égard à leur situation, leur superficie, leur configuration, ou à leur disponibilité, seraient de nature à permettre de réaliser l'opération projetée dans des conditions équivalentes, soit parce qu'elles sont trop éloignées du quartier dans lequel la commune souhaite réaliser son projet, soit parce qu'elles sont d'une emprise insuffisante pour accueillir l'ensemble du projet, d'autant que celui-ci prévoit une marge verte, soit parce qu'elles sont déjà affectées à un autre usage ou à un projet précisément envisagé ; qu'en particulier, il ne ressort pas des pièces du dossier que les parcelles situées à proximité de l'intersection des rues Marius Aufan et Paul Vaillant Couturier étaient disponibles eu égard à la délibération du conseil municipal du 25 juin 1998 les destinant à accueillir une autre opération immobilière portant sur la réalisation de logements, qui, d'ailleurs, a été effectivement réalisée postérieurement à la décision attaquée ; que les requérants ne peuvent utilement exciper de la circonstance que la commune aurait pu acquérir par voie de préemption un autre terrain pour réaliser cette opération ;<br/>
<br/>
              14. Considérant, enfin, que si les requérants soutiennent que l'opération projetée risque d'entraîner la densification du quartier où elle est située, la disparition d'espaces verts, la fermeture d'un atelier de réparation automobile et d'un concessionnaire automobile employant une dizaine de salariés et aurait un coût excessif, il ne ressort pas des pièces du dossier que ces inconvénients et les atteintes portées à la propriété privée qu'ils invoquent seraient de nature à la priver d'utilité publique, eu égard à l'intérêt qui s'attache à la réalisation du programme de logement social envisagé ;<br/>
<br/>
              En ce qui concerne la cessibilité des parcelles :<br/>
<br/>
              15. Considérant qu'il ressort des écritures produites par les requérants qu'ils n'ont soulevé, dans le délai de recours contentieux, que des moyens de légalité interne contre l'arrêté attaqué, en tant qu'il prononce la cessibilité des parcelles ; que, dès lors, c'est à bon droit que le tribunal administratif de Versailles a estimé, après avoir relevé ce moyen d'office, qu'ils n'étaient pas recevables à soulever des moyens de légalité externe, qui relevaient d'une cause juridique distincte, postérieurement à l'expiration du délai de recours contentieux ;<br/>
<br/>
              16. Considérant qu'il résulte de ce qui a été dit ci-dessus que les autres moyens dirigés contre l'arrêté attaqué en tant qu'il prononce la cessibilité des parcelles et tirés de l'illégalité de la déclaration d'utilité publique ne peuvent qu'être écartés ; <br/>
<br/>
              17. Considérant qu'il résulte de tout ce qui précède, sans qu'il soit besoin de statuer sur la fin de non-recevoir opposée par la commune, que les requérants ne sont pas fondés à soutenir que c'est à tort que, par les deux jugements n° 0703074 et 0703978 attaqués du 5 février 2009, le tribunal administratif de Versailles a rejeté leurs demandes tendant à l'annulation pour excès de pouvoir de l'arrêté du 24 janvier 2007 ; <br/>
<br/>
              Sur les conclusions tendant à l'application de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              18. Considérant qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions  présentées par la commune de Levallois-Perret au titre des dispositions de l'article L. 761-1 du code de justice administrative ; que ces dispositions font obstacle à ce qu'une somme soit mise à ce titre à la charge de la commune qui n'est pas, dans la présente instance, la partie perdante ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt n° 12VE03596 du 19 novembre 2013 de la cour administrative d'appel de Versailles est annulé.<br/>
<br/>
Article 2 : Les requêtes des consorts A...et E...et de Mme D...sont rejetées.<br/>
<br/>
Article 3 : Les conclusions présentées par la commune de Levallois-Perret et par les consorts A...et E...et Mme D...au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
<br/>
Article 4 : La présente décision sera notifiée à la commune de Levallois-Perret, M. C...A..., M. B...A..., Mme H...A..., M. G...E..., Mme F...E..., M. J...-C... E...et Mme I...D....<br/>
      Copie en sera adressée pour information au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
