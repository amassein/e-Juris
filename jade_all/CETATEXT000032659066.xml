<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032659066</ID>
<ANCIEN_ID>JG_L_2014_11_000000377234</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/65/90/CETATEXT000032659066.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème / 7ème SSR, 21/11/2014, 377234</TITRE>
<DATE_DEC>2014-11-21</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>377234</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème / 7ème SSR</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>OCCHIPINTI</AVOCATS>
<RAPPORTEUR>M. Camille Pascal</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Béatrice Bourgeois-Machureau</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:377234.20141121</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête sommaire et le mémoire complémentaire, enregistrés les 7 avril et 23 mai 2014 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. B...A..., demeurant ...; M. A...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret du 27 janvier 2014 accordant son extradition aux autorités russes ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat le paiement de la somme de 3 000 euros à verser à son avocat au titre des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
<br/>
              Vu la convention  européenne d'extradition du 13 décembre 1957 ;<br/>
<br/>
              Vu le code pénal ; <br/>
<br/>
              Vu le code de procédure pénale ;<br/>
<br/>
              Vu la loi n° 79-587 du 11 juillet 1979 ;<br/>
<br/>
              Vu la loi n° 91-647 du 10 juillet 1991 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Camille Pascal, conseiller d'Etat,<br/>
<br/>
              - les conclusions de Mme Béatrice Bourgeois-Machureau, rapporteur public,<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Occhipinti, avocat de M. A...; <br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              Sur la légalité externe du décret attaqué :<br/>
<br/>
              1.	Considérant que, par le décret attaqué, le Premier ministre a accordé aux autorités de la Fédération de Russie l'extradition de M. B...A..., ressortissant russe, pour l'exécution d'un mandat d'arrêt délivré le 4 mai 2010 par le tribunal du district Promyshlenny de la ville de Smolensk pour des faits de tentatives de vente illégale de stupéfiants ;<br/>
<br/>
              2.	Considérant, en premier lieu, qu'il n'appartient pas au Conseil d'Etat, saisi d'un recours contre un décret d'extradition, d'examiner les moyens de forme ou de procédure mettant en cause la régularité de l'avis émis par la chambre de l'instruction ; que, par suite, les moyens tirés de ce que la chambre de l'instruction se serait, dans son avis, insuffisamment prononcée sur l'absence de traitement inhumain et dégradant ainsi que sur la prescription de l'action publique, ne sauraient être accueillis ;<br/>
<br/>
              3.	Considérant, en second lieu, que le décret attaqué comporte l'énoncé des considérations de fait et de droit qui en constituent le fondement ; qu'il satisfait ainsi à l'exigence de motivation prévue par l'article 3 de la loi du 11 juillet 1979 relative à la motivation des actes administratifs et à l'amélioration des relations entre l'administration et le public ;<br/>
<br/>
              Sur la légalité interne du décret attaqué :<br/>
<br/>
              4.	Considérant, en premier lieu, qu'aux termes de l'article 12 de la convention européenne d'extradition du 13 décembre 1957 : " (...) / 2- Il sera produit à l'appui de la requête : / a) L'original ou l'expédition authentique soit d'une décision de condamnation exécutoire, soit d'un mandat d'arrêt ou de tout autre acte ayant la même force, délivré dans les formes prescrites par la loi de la Partie requérante / b) Un exposé des faits pour lesquels l'extradition est demandée. Le temps et le lieu de leur perpétration, leur qualification légale et les références aux dispositions légales qui leur sont applicables seront indiqués le plus exactement possible ; et / c) Une copie des dispositions légales applicables ou, si cela n'est pas possible, une déclaration sur le droit applicable, ainsi que le signalement aussi précis que possible de l'individu réclamé et tous autres renseignements de nature à déterminer son identité et sa nationalité " ; <br/>
<br/>
              5.	Considérant qu'il ressort des pièces du dossier que la demande d'extradition était accompagnée de toutes les pièces requises par les stipulations de l'article 12 de la convention européenne d'extradition, notamment d'une copie certifiée conforme du mandat d'arrêt décerné le 4 mai 2010, accompagnée de sa traduction en français qui forme acte authentique au regard de cette convention ; qu'elle mentionnait les faits et les infractions pour laquelle l'extradition était requise et contenait la référence précise des dispositions pénales enfreintes par le requérant ; que l'examen du texte de ces dispositions et, en particulier, de l'article 228-1, alinéa 2 du code pénal de la Fédération de Russie, faisait apparaître le quantum de la peine ; qu'ainsi, le moyen tiré de ce que le décret attaqué aurait fait droit à une demande d'extradition sans disposer des éléments que l'Etat requérant devait présenter aux autorités françaises en vertu des stipulations de l'article 12 de la convention européenne d'extradition doit être écarté ; <br/>
<br/>
              6.	Considérant, en second lieu, que si M. A...soutient que les conditions de détention dans les prisons russes sont de nature à l'exposer à des traitements contraires à l'article 3 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, il se borne, dans le cadre de la présente instance, à se référer à des articles de presse dont la portée est très générale, sans apporter aucun élément circonstancié de nature à établir la réalité de tels risques pour ce qui le concerne personnellement ; <br/>
<br/>
              7.	Considérant qu'il résulte de l'ensemble de ce qui précède que M. A...n'est pas fondé à demander l'annulation pour excès de pouvoir du décret attaqué ; que les conclusions présentées sur le fondement des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 ne peuvent, en conséquence, qu'être rejetées ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : La requête de M. A...est rejetée.<br/>
<br/>
Article 2 : La présente décision sera notifiée à M. B...A...et à la garde des sceaux, ministre de la justice.  <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-05 ACTES LÉGISLATIFS ET ADMINISTRATIFS. VALIDITÉ DES ACTES ADMINISTRATIFS - MOTIFS. - DEMANDE D'EXTRADITION - COMPLÉTUDE DU DOSSIER AU VU DUQUEL L'ADMINISTRATION SE PRONONCE - CONDITION DE LA LÉGALITÉ INTERNE DE LA DÉCISION ADMINISTRATIVE STATUANT SUR CETTE DEMANDE [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">335-04-03-02 ÉTRANGERS. EXTRADITION. DÉCRET D'EXTRADITION. LÉGALITÉ INTERNE. - COMPLÉTUDE DU DOSSIER AU VU DUQUEL L'ADMINISTRATION SE PRONONCE - CONDITION DE LA LÉGALITÉ INTERNE DE LA DÉCISION ADMINISTRATIVE STATUANT SUR CETTE DEMANDE [RJ1].
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">54-07-01-04 PROCÉDURE. POUVOIRS ET DEVOIRS DU JUGE. QUESTIONS GÉNÉRALES. MOYENS. - CAUSE JURIDIQUE - LÉGALITÉ INTERNE - MOYEN TIRÉ DE CE QUE L'ADMINISTRATION A STATUÉ SUR UNE DEMANDE D'EXTRADITION AU VU D'UN DOSSIER INCOMPLET [RJ1].
</SCT>
<ANA ID="9A"> 01-05 Le fait pour l'administration, saisie d'une demande d'extradition, de se prononcer au vu d'un dossier contenant toutes les pièces requises par les stipulations de l'article 12 de la convention européenne d'extradition du 13 décembre 1957 est une condition de la légalité interne de la décision administrative statuant sur cette demande.</ANA>
<ANA ID="9B"> 335-04-03-02 Le fait pour l'administration, saisie d'une demande d'extradition, de se prononcer au vu d'un dossier contenant toutes les pièces requises par les stipulations de l'article 12 de la convention européenne d'extradition du 13 décembre 1957 est une condition de la légalité interne de la décision administrative statuant sur cette demande.</ANA>
<ANA ID="9C"> 54-07-01-04 Le fait pour l'administration, saisie d'une demande d'extradition, de se prononcer au vu d'un dossier contenant toutes les pièces requises par les stipulations de l'article 12 de la convention européenne d'extradition du 13 décembre 1957 est une condition de la légalité interne de la décision administrative statuant sur cette demande.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Rappr., pour une demande de permis de construire, CE, 9 décembre 1992, Mlle Richert, n° 90058, T. pp. 1259-1386-1389-1397. Comp., pour une demande d'autorisation au titre de la législation des installations classées, CE, 22 septembre 2014, SIETOM de la région de Tournan-en-Brie, n° 367889, à mentionner aux Tables.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
