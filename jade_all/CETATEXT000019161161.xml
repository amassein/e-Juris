<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000019161161</ID>
<ANCIEN_ID>JG_L_2008_07_000000293632</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/19/16/11/CETATEXT000019161161.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 1ère et 6ème sous-sections réunies, 07/07/2008, 293632</TITRE>
<DATE_DEC>2008-07-07</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>293632</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>1ère et 6ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Martin</PRESIDENT>
<AVOCATS>SCP BOULLOCHE ; SCP NICOLAY, DE LANOUVELLE, HANNOTIN</AVOCATS>
<RAPPORTEUR>Mme Laure  Bédier</RAPPORTEUR>
<COMMISSAIRE_GVT>Mlle Courrèges Anne</COMMISSAIRE_GVT>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi, enregistré le 22 mai 2006 au secrétariat du contentieux du Conseil d'Etat, présenté pour M. Yves C, demeurant Le Mas du Juge, 765, chemin de Junas à Aubais (30250) ; M. C demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt du 16 mars 2006 par lequel la cour administrative d'appel de Marseille a rejeté sa requête tendant, d'une part, à l'annulation du jugement du 18 mars 2004 du tribunal administratif de Montpellier rejetant sa demande dirigée contre la décision du 3 janvier 2000 du maire de Sommières lui délivrant, sur le fondement de l'article L. 410-1 du code de l'urbanisme, un certificat négatif pour l'opération de construction envisagée sur le terrain de Mme D situé au Mas de Gajan et, d'autre part, à l'annulation de cette décision ;<br/>
<br/>
              2°) réglant l'affaire au fond, d'annuler le jugement du 18 mars 2004 du tribunal administratif de Montpellier et d'annuler la décision du 3 janvier 2000 du maire de Sommières ;<br/>
<br/>
              3°) de mettre à la charge de la commune de Sommières le versement de la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de l'urbanisme ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Laure Bédier, Maître des Requêtes,  <br/>
<br/>
              - les observations de la SCP Boulloche, avocat de M. C et de la SCP Nicolaÿ, de Lanouvelle, Hannotin, avocat de la commune de Sommières, <br/>
<br/>
              - les conclusions de Mlle Anne Courrèges, Commissaire du gouvernement ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>Considérant que, par une décision en date du 3 janvier 2000, le maire de la commune de Sommières a délivré à M. C, chargé par Mme D d'une mission de maîtrise d'oeuvre pour la réalisation de travaux d'aménagement de bâtiments lui appartenant, un certificat d'urbanisme négatif aux motifs que le projet ne respectait pas les dispositions du règlement du plan d'occupation des sols, qui limitent dans cette zone les extensions des bâtiments existants à 50 % de la surface hors oeuvre brute existante et à 250 m2 de surface hors oeuvre nette et que la demande de certificat d'urbanisme ne présentait aucun élément permettant d'apprécier si les protections du forage envisagé contre les risques de pollution directe imposées étaient effectives ou susceptibles d'être mises en oeuvre de façon certaine et si l'ancien réservoir pouvait être remis en service pour la lutte contre l'incendie ; que M. C, dont la responsabilité professionnelle était recherchée par Mme D devant le tribunal de grande instance de Nîmes, a demandé au tribunal administratif de Montpellier l'annulation de la décision du 3 janvier 2000 ; que cette demande a été rejetée par un jugement en date du 18 mars 2004, confirmé par un arrêt du 16 mars 2006 de la cour administrative d'appel de Marseille ; que M. C se pourvoit en cassation contre cet arrêt ;<br/>
<br/>
              Considérant qu'aux termes de l'article L. 112-7 du code de l'urbanisme, dans sa rédaction applicable à la date de la décision attaquée : « Des décrets en Conseil d'Etat (...) définissent notamment la surface de plancher développée hors oeuvre d'une construction et les conditions dans lesquelles sont exclus de cette surface les combles et sous-sols non aménageables pour l'habitation ou pour d'autres activités (...) ainsi que les surfaces des bâtiments d'exploitation agricole (...) » ; qu'aux termes de l'article R. 112-2 du même code, dans sa rédaction alors en vigueur : « La surface de plancher hors oeuvre brute d'une construction est égale à la somme des surfaces de plancher de chaque niveau de construction. / La surface de plancher hors oeuvre nette d'une construction est égale à la surface hors oeuvre brute de cette construction après déduction : / (...) d) Des surfaces de planchers hors oeuvre des bâtiments affectés au logement des récoltes, des animaux ou du matériel agricole ainsi que des surfaces des serres de production (...) » ; que, pour l'application de ces dispositions au cas d'une demande de certificat d'urbanisme portant sur l'aménagement de bâtiments existants, il y a lieu, pour déterminer leur surface hors oeuvre nette avant travaux et hors le cas de fraude, de prendre en considération leur mode d'utilisation effectif à la date de la demande, sans qu'il soit besoin de rechercher si ce mode d'utilisation avait été autorisé par la délivrance d'un permis de construire ;<br/>
<br/>
              Considérant que, pour déterminer la surface hors oeuvre nette des bâtiments existants, la cour administrative d'appel a exclu les locaux annexes faisant l'objet des travaux projetés, au motif que M. C n'établissait ni n'alléguait que le changement de destination de ceux-ci, à l'origine à usage agricole, en locaux à usage d'habitation aurait été autorisé par un permis de construire ; qu'il résulte de ce qui précède qu'elle a, ce faisant, entaché son arrêt d'erreur de droit ; que, par suite, et sans qu'il soit besoin d'examiner l'autre moyen du pourvoi, l'arrêt attaqué doit être annulé ;<br/>
<br/>
              Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              Considérant, en premier lieu, qu'aux termes de l'article ND1 du règlement du plan d'occupation des sols de la commune de Sommières, approuvé par délibération en date du 24 septembre 1996, sont interdites : « Les constructions de toute nature, sauf celles visées à l'article ND2 » ; que l'article ND2 mentionne : « Les travaux ayant pour objet l'entretien, la restauration ou l'extension des bâtiments existants sous réserve que les extensions soient intégrées au corps du bâtiment principal, et dans les limites de 50 % de la surface hors oeuvre brute existante et de 250 m2 de SHON (existant compris) pour les habitations existantes » ;<br/>
<br/>
              Considérant qu'il n'est pas contesté que les locaux faisant l'objet des travaux projetés n'étaient pas utilisés à des fins agricoles à la date de la demande du certificat d'urbanisme ; qu'ils ne pouvaient donc pas entrer dans le champ des bâtiments visés au d) de l'article R. 112-2 du code de l'urbanisme ; qu'il résulte de ce qui précède qu'ils devaient être pris en compte pour la détermination de la surface hors oeuvre nette ; que, dès lors, le projet présenté ne conduisait pas à la création de surfaces hors oeuvre nette et ne méconnaissait donc pas les dispositions précitées de l'article ND2 du règlement du plan d'occupation des sols ; <br/>
<br/>
              Considérant, en second lieu, qu'en application de l'article L. 410-1 du code de l'urbanisme, dans sa rédaction applicable à la date de la décision attaquée, un certificat d'urbanisme a pour objet de constater qu'un terrain est ou n'est pas constructible à la date de la délivrance du certificat, compte tenu des règles d'urbanisme applicables et des équipements existants ou prévus susceptibles de desservir le terrain concerné et d'indiquer si le terrain peut être utilisé pour la réalisation projetée ; que la demande de certificat d'urbanisme présentée par M. C prévoyait que l'alimentation en eau du bâtiment se ferait par un forage qui avait été autorisé, sous réserve de prescriptions particulières, par un arrêté préfectoral du 22 novembre 1999 et qu'un réservoir d'eau de 700 m3 serait remis en service pour répondre aux besoins de la sécurité incendie ; qu'il ne ressort pas des pièces du dossier que les prescriptions de l'arrêté préfectoral ne pourraient pas être respectées ni que l'ancien réservoir ne pourrait pas techniquement être remis en service ; que le maire de la commune de Sommières ne pouvait en conséquence regarder comme insuffisants les équipements en matière d'eau potable et de sécurité incendie, alors même qu'ils étaient prévus et susceptibles de desservir le terrain concerné ;<br/>
<br/>
              Considérant qu'il résulte de ce qui précède que M. C est fondé à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Montpellier a rejeté sa demande tendant à l'annulation du certificat d'urbanisme négatif délivré le 3 janvier 2000 ; qu'il y a lieu, dans les circonstances de l'espèce, de faire application des dispositions de l'article L. 761-1 du code de justice administrative et de mettre à la charge de la commune de Sommières le versement à M. C d'une somme de 3 000 euros ; que ces dispositions font en revanche obstacle à ce que soit mis à la charge de M. C, qui n'est pas, dans la présente instance, la partie perdante, le versement d'une somme au titre des frais exposés par la commune et non compris dans les dépens ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Marseille en date du 16 mars 2006, le jugement du tribunal administratif de Montpellier en date du 18 mars 2004 et la décision du 3 janvier 2000 du maire de la commune de Sommières délivrant un certificat d'urbanisme négatif sont annulés.<br/>
Article 2 : La commune de Sommières versera à M. C la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : Les conclusions de la commune de Sommières tendant à l'application des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée à M. Yves C et à la commune de Sommières.<br/>
<br/>
<br/>
<br/>
<br/>
</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">68-025 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. CERTIFICAT D'URBANISME. - AMÉNAGEMENTS DE BÂTIMENTS EXISTANTS - DÉTERMINATION DE LA SURFACE HORS OEUVRE NETTE AVANT TRAVAUX, EN APPLICATION DES ARTICLES L. 112-7 ET R. 112-2 DU CODE DE L'URBANISME - PRISE EN CONSIDÉRATION DU MODE D'UTILISATION À LA DATE DE LA DEMANDE [RJ1].
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">68-025-02-01 URBANISME ET AMÉNAGEMENT DU TERRITOIRE. CERTIFICAT D'URBANISME. MODALITÉS DE DÉLIVRANCE. INSTRUCTIONS DES DEMANDES DE CERTIFICAT. - AMÉNAGEMENTS DE BÂTIMENTS EXISTANTS - DÉTERMINATION DE LA SURFACE HORS OEUVRE NETTE AVANT TRAVAUX, EN APPLICATION DES ARTICLES L. 112-7 ET R. 112-2 DU CODE DE L'URBANISME - PRISE EN CONSIDÉRATION DU MODE D'UTILISATION À LA DATE DE LA DEMANDE [RJ1].
</SCT>
<ANA ID="9A"> 68-025 Pour l'application des dispositions des articles L. 112-7 et R. 112-2 du code de l'urbanisme au cas d'une demande de certificat d'urbanisme portant sur l'aménagement de bâtiments existants, il y a lieu, pour déterminer la surface hors oeuvre nette avant travaux et hors cas de fraude, de prendre en considération leur mode d'utilisation effectif à la date de la demande, sans qu'il soit besoin de rechercher si ce mode d'utilisation avait été autorisé par la délivrance d'un permis de construire.</ANA>
<ANA ID="9B"> 68-025-02-01 Pour l'application des dispositions des articles L. 112-7 et R. 112-2 du code de l'urbanisme au cas d'une demande de certificat d'urbanisme portant sur l'aménagement de bâtiments existants, il y a lieu, pour déterminer la surface hors oeuvre nette avant travaux et hors cas de fraude, de prendre en considération leur mode d'utilisation effectif à la date de la demande, sans qu'il soit besoin de rechercher si ce mode d'utilisation avait été autorisé par la délivrance d'un permis de construire.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. 17 décembre 2003, M. et Mme Mignon, n° 242448, T. p. 1028. Rappr. 12 janvier 2007, M. et Mme Fernandez, n° 274362, à publier aux tables.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
