<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038466959</ID>
<ANCIEN_ID>JG_L_2019_05_000000430134</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/46/69/CETATEXT000038466959.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 07/05/2019, 430134, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-05-07</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>430134</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP ROUSSEAU, TAPIE ; SCP MATUCHANSKY, POUPOT, VALDELIEVRE</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2019:430134.20190507</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Par une requête, enregistrée le 25 avril 2019 au secrétariat du contentieux du Conseil d'Etat, Mlle B...D...demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative :<br/>
<br/>
              1°) de suspendre l'exécution de la décision du 6 février 2019 par laquelle la commission des sanctions de l'Agence française de lutte contre le dopage lui a, d'une part, interdit, pendant une durée de neuf mois, de participer directement ou indirectement à l'organisation et au déroulement de toute manifestation sportive donnant lieu à une remise de prix en argent ou en nature, de participer directement ou indirectement à l'organisation et au déroulement de toute manifestation sportive autorisée ou organisée par une fédération sportive française délégataire ou agréée, ainsi qu'aux entraînements y préparant organisés par une fédération agréée ou l'un des membres de celle-ci et d'exercer les fonctions définies à l'article L. 212-1 du code du sport ainsi que toute fonction d'encadrement au sein d'une fédération agréée ou d'un groupement ou d'une association affiliés à une telle fédération, d'autre part, demandé à la fédération française d'haltérophilie-musculation d'annuler les résultats qu'elle a obtenus le 26 mai 2018 à l'occasion du championnat de France de musculation, ainsi qu'entre le 26 mai 2018 et la date de notification de la décision, avec toutes les conséquences en découlant, y compris le retrait de médailles, points, prix et gains ;<br/>
<br/>
              2°) de mettre à la charge de l'Agence française de lutte contre le dopage la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
              Elle soutient que :<br/>
              - la condition d'urgence est remplie dès lors que la décision litigieuse, d'une part, la prive de la possibilité de participer à des compétitions sportives imminentes, d'autre part, porte atteinte à ses intérêts financiers en l'empêchant d'exercer son activité professionnelle, enfin, porte atteinte à son image et à sa réputation ;<br/>
              - il existe un doute sérieux quant à la légalité de la décision litigieuse ;<br/>
              - la décision litigieuse a été prise sur le fondement des dispositions du code du sport dont elle conteste par mémoire distinct la conformité aux droits et libertés garantis par la Constitution ;<br/>
              - elle a été prise au terme d'une procédure irrégulière dès lors que, d'une part, il n'en ressort pas que tous les membres de la commission des sanctions ont été régulièrement convoqués à la séance au cours de laquelle lui a été infligée la sanction, d'autre part, il n'est pas établi que M.A..., dont elle avait demandé qu'il l'assiste et soit entendu lors de cette séance, ait pu y présenter ses observations ;<br/>
              - la commission des sanctions de l'Agence française de lutte contre le dopage a été saisie sur le fondement de l'article 15 de l'ordonnance du 11 juillet 2018, qui est contraire aux principes d'indépendance et d'impartialité garantis par l'article 16 de la Déclaration de 1789 en ce qu'il n'opère pas de séparation entre les autorités ou fonctions de poursuite et de jugement ;<br/>
              - la sanction prononcée par la décision litigieuse est disproportionnée eu égard à sa durée et en ce qu'elle lui interdit d'exercer les fonctions définies à l'article L. 212-1 du code du sport.<br/>
              Par un mémoire distinct, enregistré le 25 avril 2019, présenté en application de l'article 23-5 de l'ordonnance du 7 novembre 1958, Mlle D...demande au juge des référés du Conseil d'Etat de renvoyer au Conseil constitutionnel la question de la conformité aux droits et libertés garantis par la Constitution du d) du 1° du I de l'article L. 232-23 du code du sport. Elle soutient que cette disposition, applicable au litige et n'ayant pas été déclarée conforme à la Constitution, prévoit une sanction disproportionnée et non nécessaire, méconnaissant l'article 8 de la Déclaration des droits de 1789 et portant, au regard de l'objectif poursuivi, une atteinte disproportionnée à la liberté d'entreprendre résultant de l'article 4 de la même déclaration. <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la Constitution, notamment son Préambule et son article 61-1 ;<br/>
              - le code du sport, notamment son article L. 232-23 ;<br/>
              - la loi n° 2016-41 du 26 janvier 2016 ;<br/>
              - l'ordonnance n° 58-1067 du 7 novembre 1958 ; <br/>
              - l'ordonnance n° 2015-1207 du 30 septembre 2015 ;<br/>
              - l'ordonnance n° 2018-603 du 11 juillet 2018 ;<br/>
              - le décret n° 2018-6 du 4 janvier 2018 ;<br/>
              - le décret n° 2018-1283 du 27 décembre 2018 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision ".<br/>
<br/>
              2. Mlle B...D..., haltérophile française professionnelle, pratiquant son sport sans affiliation à une fédération sportive, a fait l'objet le 26 mai 2018, à Paris, d'un contrôle antidopage diligenté par l'Agence française de lutte contre le dopage lors des épreuves de championnat de France de musculation. Ce contrôle a mis en évidence la présence dans ses urines d'heptaminol, substance figurant sur la liste annexée au décret du 4 janvier 2018 dans la catégorie S6 au nombre des " stimulants spécifiés " interdits en compétition. Elle a été informée le 28 juin 2018, d'une part, que l'Agence française de lutte contre le dopage était saisie de ces faits en application, alors, du 1° de l'article L. 232-22 du code du sport, lui donnant compétence disciplinaire à l'égard des personnes, non licenciées auprès d'une fédération sportive agréée ou délégataire, participant à des manifestations sportives organisées ou autorisées par une telle fédération, en cas d'utilisation en méconnaissance de l'article L. 232-9 du même code d'une substance interdite figurant sur cette liste, d'autre part, du déroulement de la procédure disciplinaire et notamment de ses droits, enfin des sanctions encourues. Par une décision du 6 février 2019, notifiée à l'intéressée par un courrier du 6 mars 2019, la commission des sanctions de l'Agence française de lutte contre le dopage a, d'une part, interdit à Mlle D..., pendant une durée de neuf mois, de participer à l'organisation et au déroulement de toute manifestation sportive donnant lieu à une remise de prix, de participer à l'organisation et au déroulement de toute manifestation sportive autorisée ou organisée par une fédération sportive française délégataire ou agréée, ainsi qu'aux entraînements y préparant organisés par une fédération agréée ou l'un des membres de celle-ci et d'exercer les fonctions définies à l'article L. 212-1 du code du sport ainsi que toute fonction d'encadrement au sein d'une fédération agréée ou d'un groupement ou d'une association affiliés à une telle fédération, d'autre part, demandé à la fédération française d'haltérophilie-musculation d'annuler les résultats obtenus par Mlle D...le 26 mai 2018 à l'occasion du championnat de France de musculation, ainsi qu'entre le 26 mai 2018 et la date de notification de la décision, avec toutes les conséquences en découlant, y compris le retrait de médailles, points, prix et gains. Mlle D... demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-1 du code de justice administrative, de suspendre l'exécution de cette décision.<br/>
              Sur la question prioritaire de constitutionnalité :<br/>
<br/>
              3. Aux termes du premier alinéa de l'article 23-5 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel : " Le moyen tiré de ce qu'une disposition législative porte atteinte aux droits et libertés garantis par la Constitution peut être soulevé (...) à l'occasion d'une instance devant le Conseil d'Etat (...) ". Il résulte des dispositions de ce même article que le Conseil constitutionnel est saisi de la question prioritaire de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et qu'elle soit nouvelle ou présente un caractère sérieux.<br/>
<br/>
              4. Les dispositions de l'article L. 232-23 du code du sport déterminent les sanctions susceptibles d'être prononcées par la commission des sanctions de l'Agence française de lutte contre le dopage. Au nombre des sanctions susceptibles d'être prononcées à l'encontre des sportifs ayant utilisé une substance interdite en méconnaissance de l'article L. 232-9 du même code, le d) du 1° du I de l'article L. 232-23, dans sa rédaction applicable à la date de la décision attaquée, prévoit " une interdiction temporaire ou définitive d'exercer les fonctions définies à l'article L. 212-1 ", c'est-à-dire, aux termes de ce dernier article, " contre rémunération, enseigner, animer ou encadrer une activité physique ou sportive ou entraîner ses pratiquants, à titre d'occupation principale ou secondaire, de façon habituelle, saisonnière ou occasionnelle ". <br/>
              5. Mlle D...soutient que ces dispositions prévoient une sanction disproportionnée et non nécessaire, méconnaissant l'article 8 de la Déclaration des droits de 1789 et portant, au regard de l'objectif poursuivi, une atteinte disproportionnée à la liberté d'entreprendre résultant de l'article 4 de la même déclaration. Elle demande au juge des référés du Conseil d'Etat de renvoyer au Conseil constitutionnel la question de leur conformité aux droits et libertés garantis par la Constitution.<br/>
<br/>
              6. La sanction appliquée en l'espèce par la commission des sanctions de l'Agence française de lutte contre le dopage sur le fondement de ces dispositions étant encourue à la date de la commission des faits en cause, leur version applicable au litige est celle en vigueur à la date de la décision du 6 février 2019. Les dispositions du d) du 1° du I de l'article L. 232-23 du code du sport applicables à ce titre sont issues de l'ordonnance du 30 septembre 2015, ratifiée par le I de l'article 221 de la loi du 26 janvier 2016, sans avoir été affectées par les modifications apportées à l'article L. 232-23 du code du sport par l'ordonnance du 11 juillet 2018, non encore ratifiée. Elles sont ainsi au nombre des dispositions législatives visées par l'article 61-1 de la Constitution et l'article 23-5 de l'ordonnance du 7 novembre 1958.<br/>
              7. D'une part, les principes garantis par l'article 8 de la Déclaration de 1789, aux termes duquel notamment " la loi ne doit établir que des peines strictement et évidemment nécessaires ", s'étendent à toute sanction ayant le caractère d'une punition. D'autre part, il est loisible au législateur d'apporter à la liberté d'entreprendre, résultant de l'article 4 de la Déclaration de 1789, des limitations liées à des exigences constitutionnelles ou justifiées par l'intérêt général, à la condition qu'il n'en résulte pas d'atteintes disproportionnées au regard de l'objectif poursuivi. En prévoyant la possibilité de sanctionner la méconnaissance des interdictions, relatives aux substances ou méthodes dopantes, faites à tout sportif par l'article L. 232-9 du code du sport, par une interdiction, temporaire ou définitive, d'enseigner, animer ou encadrer une activité physique ou sportive ou entraîner ses pratiquants, contre rémunération, activité dont l'article L. 212-1 de ce code réglemente l'exercice en le subordonnant à la justification d'une compétence en matière de sécurité des pratiquants et des tiers dans l'activité considérée, le législateur, dont la nécessité des peines attachées aux infractions relève du pouvoir d'appréciation, ne peut être regardé, ni comme ayant prévu une sanction manifestement disproportionnée ou inadéquate au regard de la gravité et de la nature de l'infraction réprimée, ni comme ayant opéré une conciliation manifestement disproportionnée entre la liberté d'entreprendre et l'objectif poursuivi de lutte contre dopage.<br/>
               8. Par suite, sans qu'il soit besoin de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité invoquée, l'une des conditions de son renvoi n'étant pas remplie, dès lors qu'elle n'est pas nouvelle et ne présente pas un caractère sérieux, le moyen tiré de ce que les dispositions du d) du 1° du I de l'article L. 232-23 du code du sport portent atteinte aux droits et libertés garantis par la Constitution n'est pas, en l'état de l'instruction, propre à créer un doute sérieux sur la légalité de la décision litigieuse.<br/>
              Sur les autres moyens soulevés :<br/>
<br/>
              9. En premier lieu, aucun texte ni aucun principe n'impose que la décision de sanction prise par la commissions des sanctions de l'Agence française de lutte contre le dopage mentionne les conditions de la convocation de ses membres. Par suite, le moyen par lequel Mlle D... se borne à soutenir qu'il ne ressort pas de la décision de sanction litigieuse que les membres de la commission des sanctions auraient été convoqués dans des conditions régulières ne peut être regardé comme propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de cette décision.<br/>
              10. En deuxième lieu, le quatrième alinéa de l'article R. 232-93 du code du sport dispose que : " L'intéressé et son conseil, le cas échéant, la ou les personnes investies de l'autorité parentale ou le représentant légal et le représentant du collège peuvent demander que soient entendues les personnes de leur choix dont ils communiquent le nom au moins six jours avant l'audience. Le président de la commission des sanctions ou de la section appelée à se prononcer peut refuser, par décision motivée, les demandes d'audition manifestement abusives ". D'une part, il résulte des pièces produites par la requérante que le secrétaire de la commission des sanctions a, le 15 janvier 2019, accusé réception de l'information, donnée par Mlle D...le 13 janvier 2019, qu'elle serait accompagnée lors de l'audience du 6 février 2019 par M. C... A.... Mlle D...a confirmé le 4 février 2019 cette information à la commission des sanctions, dont le secrétaire a, à son tour, confirmé le jour même la prise en compte. D'autre part, il résulte des mentions de la décision litigieuse qu'elle a pu se présenter devant la commission des sanctions accompagnée de M.A.... Aucun texte ni aucun principe n'impose que la décision de sanction mentionne l'audition d'une personne entendue à la demande de la personne poursuivie. Par suite, le moyen par lequel Mlle D...soutient qu'il ne serait pas établi, à la lecture de la décision attaquée, que<br/>
M. A...aurait pu être entendu lors de l'audience devant la commission des sanctions ne peut être regardé, en l'état de l'instruction, comme propre à créer un doute sérieux quant à la légalité de la décision.<br/>
<br/>
              11. En troisième lieu, l'article 15 de l'ordonnance du 11 juillet 2018, non encore ratifiée, prévoit, après avoir fixé au 1er septembre 2018 la date d'entrée en vigueur de cette ordonnance, que, " lorsque des griefs notifiés par l'Agence française de lutte contre le dopage n'ont pas encore, à cette date, donné lieu à décision de son collège, la commission des sanctions de l'agence est saisie du dossier en l'état. La notification des griefs est réputée avoir été transmise par le collège à la commission des sanctions ". La commission des sanctions ayant été, par application de ces dispositions, saisie automatiquement, elle ne saurait, contrairement à ce que soutient la requérante, être regardée comme l'ayant été en vertu de dispositions qui lui confieraient un quelconque pouvoir, qui impliquerait de sa part une appréciation sur l'engagement des poursuites, de se saisir d'office des manquements sur lesquels il lui revient ensuite de se prononcer. Le moyen tiré de ce que la décision litigieuse aurait été prise en application de dispositions portant atteinte, pour ce motif, aux principes d'indépendance et d'impartialité découlant de l'article 16 de la Déclaration des droits de l'homme et du citoyen ne peut ainsi être regardé, en l'état de l'instruction, comme propre à créer un doute sérieux quant à la légalité de cette décision.<br/>
              12. En quatrième lieu, les dispositions de l'article L. 232-9 du code du sport interdisent, dans leur rédaction applicable au litige, à tout sportif d'utiliser ou de tenter d'utiliser, en dehors d'une autorisation à des fins thérapeutiques ou d'une raison médicale dûment justifiée, les substances ou méthodes figurant sur la liste élaborée en application de la convention internationale contre le dopage dans le sport qui, dans sa version applicable à la date des faits, a été publiée au Journal officiel de la République française par le décret du 4 janvier 2018. Il en résulte qu'en dehors des cas où le sportif se prévaut d'une autorisation pour usage à des fins thérapeutiques ou fait état d'une raison médicale dûment justifiée, la violation de l'interdiction qu'elles posent est établie par la seule présence, dans un prélèvement urinaire ou sanguin, de l'une des substances figurant sur la liste élaborée en application de la convention internationale contre le dopage dans le sport, sans qu'il y ait lieu de rechercher si l'usage de cette substance a revêtu un caractère intentionnel. Il n'est pas contesté que les analyses, effectuées le 13 juin 2018 sur l'échantillon A des urines de Mlle D...par le département des analyses de l'Agence française de lutte contre le dopage, à la suite du contrôle antidopage du 26 mai 2018, ont fait ressortir la présence, à une concentration estimée à 1 814 nanogrammes par millilitre, d'heptaminol, qui est une substance dite " spécifiée " interdite en compétition figurant sur la liste résultant de l'annexe I de la convention internationale contre le dopage dans le sport publiée par le décret du 4 janvier 2018, ainsi d'ailleurs qu'elle le demeure sur celle publiée par le décret du 27 décembre 2018. En vertu de l'article L. 232-23-3-3 du code du sport, tant la durée d'interdiction de participation à toute manifestation sportive que celle d'exercer les fonctions définies à l'article L. 212-1 du code du sport est en principe de deux ans lorsque le manquement est consécutif à l'usage d'une substance spécifiée, la durée étant portée à quatre ans lorsqu'il est démontré que le sportif a eu l'intention de commettre le manquement. Par ailleurs, l'article L. 232-23-3-10 du même code, dans sa rédaction alors applicable, permet de réduire ces durées de principe pour prendre en compte les circonstances propres à chaque espèce. La commission des sanctions a réduit à neuf mois la durée des interdictions, qu'elle a prononcées, de participation à toute manifestation sportive et d'exercice des fonctions définies à l'article L. 212-1 du code du sport, tenant ainsi compte de l'ensemble des circonstances de l'espèce, notamment de la faute de l'intéressée, qui avait fait valoir que la présence dans son organisme de la substance interdite était due à la prise d'un médicament en vente libre auquel elle avait l'habitude de recourir sur la recommandation d'une " naturopathe " pour soulager des troubles veineux et dont elle n'avait pas conscience qu'il pouvait comporter une telle substance, de la nature et de la concentration de la substance décelée dans son organisme, dont elle avait démontré l'origine, de l'absence de suivi médical de sa pratique sportive, ainsi que du jeune âge de l'intéressée et de son faible niveau d'éducation antidopage. Par suite, le moyen tiré de ce que la commission des sanctions aurait prononcé une sanction disproportionnée n'est pas de nature, en l'état de l'instruction, à créer un doute sérieux sur la légalité de la décision attaquée.<br/>
              13. Aucun des moyens soulevés n'étant manifestement, en l'état de l'instruction, de nature à créer un doute sérieux sur la légalité de la décision litigieuse, il y a lieu, sans qu'il soit besoin de statuer sur l'urgence, de rejeter la requête selon la procédure prévue à l'article L. 522-3 du code de justice administrative, y compris les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : Il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée par MlleD....<br/>
Article 2 : La requête de Mlle D...est rejetée.<br/>
Article 3 : La présente ordonnance sera notifiée à Mlle B...D....<br/>
Copie en sera adressée à l'Agence française de lutte contre le dopage, au Conseil constitutionnel, au Premier ministre et à la ministre des sports.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
