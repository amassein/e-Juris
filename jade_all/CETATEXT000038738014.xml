<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038738014</ID>
<ANCIEN_ID>JG_L_2019_07_000000419553</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/73/80/CETATEXT000038738014.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème chambre, 08/07/2019, 419553, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-07-08</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>419553</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP MONOD, COLIN, STOCLET</AVOCATS>
<RAPPORTEUR>M. Jean-Dominique Langlais</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Nicolas Polge</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2019:419553.20190708</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme A...B...-C... a demandé au tribunal administratif de Marseille d'annuler la décision du directeur de la caisse d'allocations familiales des Bouches-du-Rhône du 3 novembre 2016 en tant qu'elle ne lui accorde qu'une remise partielle de sa dette au titre d'un indu d'aide personnalisée au logement. Par un jugement n° 1700273 du 5 février 2018, le tribunal administratif a rejeté sa demande.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 3 avril et 19 juin 2018 au secrétariat du contentieux du Conseil d'Etat, Mme B...-C... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 000 euros à verser à la SCP Monod, Colin, Stoclet, son avocat, au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de la construction et de l'habitation ;<br/>
              -	la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Dominique Langlais, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Nicolas Polge, rapporteur public.<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Monod, Colin, Stoclet, avocat de Mme B...-C....<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que, par une lettre du 20 mai 2016, la caisse d'allocations familiales des Bouches-du-Rhône a notifié à Mme B... -C... un indu au titre de l'aide personnalisée au logement d'un montant de 5 329,57 euros. Par une décision du 3 novembre 2016, le directeur de la caisse d'allocations familiales des Bouches-du-Rhône n'a que partiellement fait droit à la demande de remise gracieuse présentée par Mme B...-C... en lui accordant une remise de 1 297,39 euros sur les 5 189,54 euros restant dus à cette date. Par un jugement du 5 février 2018, le tribunal administratif de Marseille a rejeté la demande de Mme B...-C... tendant à l'annulation de cette décision en tant qu'elle ne lui avait pas accordé une remise totale. Mme B...-C... se pourvoit en cassation contre ce jugement.<br/>
<br/>
              2. Lorsqu'il statue sur un recours dirigé contre une décision refusant ou ne faisant que partiellement droit à une demande de remise gracieuse d'un indu d'une prestation ou d'une allocation versée au titre, notamment, du logement, il appartient au juge administratif, eu égard tant à la finalité de son intervention qu'à sa qualité de juge de plein contentieux, non de se prononcer sur les éventuels vices propres de la décision attaquée, mais d'examiner si une remise gracieuse totale ou partielle est susceptible d'être accordée, en se prononçant lui-même sur la demande au regard des dispositions applicables et des circonstances de fait dont il est justifié par l'une et l'autre parties à la date de sa propre décision.<br/>
<br/>
              3. Il ressort des termes du jugement attaqué que le juge du fond s'est estimé saisi d'un recours pour excès de pouvoir et s'est borné à vérifier que la décision du directeur de la caisse d'allocations familiales des Bouches-du-Rhône n'était pas entachée d'une erreur manifeste d'appréciation. Il s'est ainsi mépris sur son office. Par suite, sans qu'il soit besoin d'examiner les moyens du pourvoi, le jugement attaqué doit être annulé.<br/>
<br/>
              4. Mme B...-C... a obtenu le bénéfice de l'aide juridictionnelle. Par suite, son avocat peut se prévaloir des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991. Il y a lieu, dans les circonstances de l'espèce, et sous réserve que la SCP Monod, Colin, Stoclet, avocat de Mme B...-C..., renonce à percevoir la somme correspondant à la part contributive de l'Etat, de mettre à la charge de l'Etat la somme de 2 000 euros à verser à la SCP Monod, Colin, Stoclet.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : Le jugement du tribunal administratif de Marseille du 5 février 2018 est annulé.<br/>
Article 2 : L'affaire est renvoyée au tribunal administratif de Marseille.<br/>
Article 3 : L'Etat versera à la SCP Monod, Colin, Stoclet, avocat de Mme B...-C..., une somme de 2 000 euros en application des dispositions du deuxième alinéa de l'article 37 de la loi du 10 juillet 1991, sous réserve que cette société renonce à percevoir la somme correspondant à la part contributive de l'Etat.<br/>
Article 4 : La présente décision sera notifiée à Mme A...B...-C... et à la ministre de la cohésion des territoires et des relations avec les collectivités territoriales.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
