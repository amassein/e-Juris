<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029762321</ID>
<ANCIEN_ID>JG_L_2014_11_000000364792</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/76/23/CETATEXT000029762321.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  9ème sous-section jugeant seule, 14/11/2014, 364792, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-11-14</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>364792</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 9ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BARADUC, DUHAMEL, RAMEIX</AVOCATS>
<RAPPORTEUR>M. Julien Anfruns</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Marie-Astrid Nicolazo de Barmon</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2014:364792.20141114</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
Procédure contentieuse antérieure<br/>
<br/>
              La société TSB a demandé au tribunal administratif de Paris la décharge des cotisations supplémentaires d'impôt sur les sociétés et des pénalités pour mauvaise foi auxquelles elle a été assujettie au titre des années 2002 et 2003. Par un jugement n°0713326 du 28 décembre 2010, le tribunal administratif de Paris l'a déchargée des pénalités pour mauvaise foi mais a rejeté le surplus des conclusions de sa demande.<br/>
<br/>
              Par un arrêt n° 11PA00559 du 25 octobre 2012, la cour administrative d'appel de Paris a rejeté l'appel formé contre ce jugement du tribunal administratif de Paris par la société TSB.<br/>
<br/>
 Procédure devant le Conseil d'État<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 26 décembre 2012 et 26 mars 2013 au secrétariat du contentieux du Conseil d'État, la société TSB demande au Conseil d'État :<br/>
<br/>
              1°) d'annuler l'arrêt n°11PA00559 du 25 octobre 2012 de la cour administrative d'appel de Paris ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de l'État la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu :<br/>
              - les autres pièces du dossier ;<br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - le code de justice administrative.<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Julien Anfruns, maître des requêtes en service extraordinaire,<br/>
<br/>
              - les conclusions de Mme Marie-Astrid Nicolazo de Barmon, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Baraduc, Duhamel, Rameix, avocat de la société TSB ;<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond qu'à la suite d'une vérification de comptabilité, l'administration a remis en cause le caractère déductible du résultat imposable de la société TSB, au titre des exercices clos en 2002 et en 2003, de provisions représentant une fraction des sommes détournées au préjudice de cette société par l'une de ses salariées. Après avoir vainement réclamé contre les cotisations supplémentaires d'impôt sur les sociétés et de contribution assise sur cet impôt et les pénalités en résultant, la société a soumis le litige au tribunal administratif de Paris qui, par un jugement du 28 décembre 2010, a rejeté sa demande. La société TSB se pourvoit en cassation contre l'arrêt du 25 octobre 2012 par lequel la cour administrative d'appel de Paris a rejeté l'appel qu'elle avait formé contre ce jugement.<br/>
<br/>
              2. En premier lieu, à supposer même que le mémoire enregistré le 2 janvier 2012 au greffe de la cour administrative d'appel de Paris, présenté par le ministre chargé du budget en réponse au mémoire en réplique de la société TSB, puisse être regardé comme contenant des éléments nouveaux relatifs aux difficultés de trésorerie de cette société, il ressort des termes mêmes de l'arrêt attaqué que la cour, contrairement à ce que soutient la société requérante, ne s'est pas fondée sur ces éléments pour rejeter la requête qui lui était présentée. Par suite, dès lors que la cour n'a fondé sa décision sur aucun argument de fait ou de droit auquel la société requérante n'a pas été mise en mesure de répondre, les moyens tirés de la méconnaissance des dispositions de l'article R. 611-1 du code de justice administrative et de celle des stipulations du paragraphe 1 de l'article 6 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales doivent être écartés.<br/>
<br/>
              3. En second lieu, les provisions constituées par une société pour faire face aux risques de pertes résultant des détournements de fonds commis à son détriment ne sont pas déductibles de ses résultats si ces détournements ont été commis par les principaux dirigeants, mandataires sociaux ou associés ou si, commis par une autre personne, ils n'ont pu être ignorés d'eux ou ont été rendus possibles par leur comportement délibéré ou leur carence manifeste dans la mise en oeuvre des dispositifs de contrôle interne de l'entreprise. Pour juger que les sommes détournées au détriment de la société requérante par son ancienne salariée n'étaient pas déductibles de ses bénéfices sociaux et ne pouvaient, en conséquence, donner lieu à la constitution des provisions déductibles, la cour a d'abord relevé qu'il résultait de l'instruction que les détournements en cause n'avaient été rendus possibles que par la pratique constante, durant six années, du dirigeant de la société de laisser à la disposition de cette salariée des chèques en blanc signés par lui, puis en a déduit, d'une part, que ce comportement délibéré avait été à l'origine directe des détournements et, d'autre part, que compte tenu de la petite taille de l'entreprise, ces détournements répétés et imputés pour partie sur le compte courant du dirigeant ne pouvaient échapper au contrôle que ce dernier devait normalement exercer sur la société, nonobstant les circonstances que les commissaires aux comptes n'auraient décelé aucune anomalie dans la comptabilité de la société et que cette dernière avait intenté contre son ancienne salariée une action civile en réparation de son préjudice. En statuant ainsi, la cour, qui a suffisamment motivé son arrêt et souverainement apprécié les faits de l'espèce et les pièces du dossier sans les dénaturer, n'a pas commis d'erreur de droit.<br/>
<br/>
              4. Il résulte de tout ce qui précède que le pourvoi de la société TSB doit être rejeté, y compris ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
              --------------<br/>
<br/>
Article 1er : Le pourvoi de la société TSB est rejeté.<br/>
Article 2 : La présente décision sera notifiée à la société TSB et au ministre des finances et des comptes publics.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
