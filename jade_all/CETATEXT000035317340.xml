<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000035317340</ID>
<ANCIEN_ID>JG_L_2017_07_000000411546</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/35/31/73/CETATEXT000035317340.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème chambre, 28/07/2017, 411546, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2017-07-28</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>411546</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème chambre</FORMATION>
<TYPE_REC>Autres</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>Mme Ophélie Champeaux</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emilie Bokdam-Tognetti</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2017:411546.20170728</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. et Mme A...B..., à l'appui de leur demande tendant à la décharge des prélèvements sociaux auxquels ils ont été assujettis au titre de l'année 2014 à raison d'une plus-value immobilière, ont produit un mémoire, enregistré le 27 avril 2017 au greffe du tribunal administratif de Versailles, en application de l'article 23-1 de l'ordonnance n° 58-1067 du 7 novembre 1958, par lequel ils soulèvent une question prioritaire de constitutionnalité.<br/>
<br/>
              Par une ordonnance n° 1503365 du 15 juin 2017, enregistrée le même jour au secrétariat du contentieux du Conseil d'Etat, le président de la 5ème chambre du tribunal administratif de Versailles, avant qu'il soit statué sur la demande de M. et Mme B..., a décidé, par application de l'article 23-2 de l'ordonnance n° 58-1067 du 7 novembre 1958, de transmettre au Conseil d'Etat la question de la conformité aux droits et libertés garantis par la Constitution du 1° du II de l'article 244 bis A et du 2° du II de l'article 150 U du code général des impôts.<br/>
<br/>
              Dans la question prioritaire de constitutionnalité transmise et un nouveau mémoire, enregistré le 11 juillet 2017, M. et Mme B... soutiennent que le 1° du II de l'article 244 bis A et le 2° du II de l'article 150 U du code général des impôts, applicables au litige, méconnaissent les articles 6 et 13 de la Déclaration des droits de l'homme et du citoyen de 1789.<br/>
<br/>
              Par un mémoire, enregistré le 10 juillet 2017, le ministre de l'action et des comptes publics soutient que les conditions posées par l'article 23-4 de l'ordonnance du 7 novembre 1958 ne sont pas remplies et, en particulier, que la question transmise ne présente pas un caractère sérieux.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - la Constitution, notamment son article 61-1 ;<br/>
              - l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
              - le code général des impôts ;<br/>
              - le code de la sécurité sociale ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Ophélie Champeaux, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Emilie Bokdam-Tognetti, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il résulte de l'article 23-4 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel que, lorsqu'une juridiction relevant du Conseil d'Etat a transmis à ce dernier, en application de l'article 23-2 de cette même ordonnance, la question de la conformité à la Constitution d'une disposition législative, le Conseil constitutionnel est saisi de cette question de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux.<br/>
<br/>
              2. En vertu du I bis de l'article L. 136-7 du code de la sécurité sociale, sont assujetties aux prélèvements sociaux sur les revenus du capital les plus-values imposées au prélèvement prévu par l'article 244 bis A du code général des impôts, lorsqu'elles sont réalisées par des personnes physiques qui ne sont pas fiscalement domiciliées en France. Pour le calcul des plus-values imposables, le 1° du II de cet article 244 bis A renvoie aux règles définies aux 2° à 9° du II de l'article 150 U du code général des impôts. Le 1° du II de ce dernier article, qui exonère d'imposition les immeubles constituant la résidence principale du cédant au jour de la cession, n'est donc pas applicable si, au jour de la cession réalisée dans un délai normal de vente, le cédant n'est plus un résident fiscal français. Ce dernier ne peut alors bénéficier que de l'exonération, limitée à 150 000 euros de plus-value nette imposable, prévue par le 2° du II de l'article 150 U.<br/>
<br/>
              3. Le 1° du II de l'article 244 bis A, en tant qu'il renvoie au 2° du II et non également au 1° du II de l'article 150 U du code général des impôts, est applicable au litige dont est saisi le tribunal administratif de Versailles. Cette disposition n'a pas déjà été déclarée conforme à la Constitution par le Conseil constitutionnel. Le moyen tiré de ce qu'elle porte atteinte aux droits et libertés garantis par la Constitution, et notamment aux principes d'égalité devant la loi et devant les charges publiques garantis respectivement par les articles 6 et 13 de la Déclaration des droits de l'homme et du citoyen de 1789, soulève une question présentant un caractère sérieux. Ainsi, il y a lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité invoquée.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : La question de la conformité à la Constitution des dispositions combinées du 1° du II de l'article 244 bis A et du 2° du II de l'article 150 U du code général des impôts est renvoyée au Conseil constitutionnel.<br/>
Article 2 : La présente décision sera notifiée à M. et Mme A... B...et au ministre de l'action et des comptes publics.<br/>
Copie en sera adressée au Premier ministre et au ministre de l'économie et des finances, ainsi qu'au tribunal administratif de Versailles.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
