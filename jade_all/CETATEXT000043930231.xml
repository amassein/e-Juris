<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043930231</ID>
<ANCIEN_ID>J1_L_2021_07_00019PA00904</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/93/02/CETATEXT000043930231.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>CAA de PARIS, 7ème chambre, 20/07/2021, 19PA00904, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-07-20</DATE_DEC>
<JURIDICTION>CAA de PARIS</JURIDICTION>
<NUMERO>19PA00904</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème chambre</FORMATION>
<TYPE_REC>plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. JARDIN</PRESIDENT>
<AVOCATS>ERNST &amp; YOUNG, SOCIETE D'AVOCATS</AVOCATS>
<RAPPORTEUR>M. Khalil  AGGIOURI</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme STOLTZ-VALETTE</COMMISSAIRE_GVT>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
       Procédure contentieuse antérieure :<br/>
<br/>
       La société anonyme (SA) Caisse d'Epargne et de Prévoyance d'Ile-de-France a demandé au Tribunal administratif de Paris de prononcer la décharge des amendes qui lui ont été infligées, au titre des années 2007 et 2008, en application de l'article 1739 du code général des impôts.<br/>
<br/>
       Par un jugement n° 1429007/1-1 du 9 décembre 2015, le Tribunal administratif de Paris a rejeté sa demande.<br/>
<br/>
       Par un arrêt n° 16PA00555 du 30 décembre 2016, la Cour a rejeté l'appel formé par la société Caisse d'Epargne et de Prévoyance d'Ile-de-France contre ce jugement.<br/>
<br/>
       Par une décision n° 408264 du 27 février 2019, le Conseil d'Etat, statuant au contentieux sur un pourvoi de la société Caisse d'Epargne et de Prévoyance d'Ile-de-France, a annulé cet arrêt et a renvoyé l'affaire devant la Cour.<br/>
<br/>
       Procédure devant la Cour :<br/>
<br/>
       Par une requête et des mémoires, enregistrés le 5 février 2016, le 7 octobre 2016, le 10 juillet 2019 et le 8 octobre 2019, la société Caisse d'Epargne et de Prévoyance d'Ile-de-France, représentée par Me A... et Me B..., demande à la Cour, dans le dernier état de ses écritures :<br/>
<br/>
       1°) d'annuler le jugement n° 1429007/1-1 du 9 décembre 2015 du Tribunal administratif de Paris ;<br/>
<br/>
       2°) de prononcer la décharge des amendes en litige ; <br/>
<br/>
       3°) de mettre à la charge de l'Etat une somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
       Elle soutient que :<br/>
       - les amendes contestées constituent des sanctions administratives, de sorte que les délais de contestation prévues par le livre des procédures fiscales ne pouvaient être appliquées ; <br/>
       - les sanctions en cause constituent des actes inexistants dès lors que l'agent qui a dressé le procès-verbal d'infraction n'a pas été mandaté à cet effet par le ministre chargé de l'économie, que l'administration a méconnu le principe " non bis idem ", que l'infliction des sanctions est intervenue au terme d'une vérification de comptabilité dont l'objet a été détourné à la seule fin de contrôler l'absence d'infraction aux dispositions régissant l'ouverture et le maintien des produits d'épargne réglementée, que l'administration s'est bornée à infliger les sanctions par catégorie de compte, sans identifier précisément les infractions constatées ;<br/>
       - l'inexistence des décisions de sanction a pour conséquence qu'aucun délai de recours ne peut lui être opposé ;<br/>
       - à supposer qu'un délai de recours soit en l'espèce opposable, sa demande devant le Tribunal administratif de Paris n'était pas tardive, le service lui ayant notifié des indications erronées relatives aux voies et délais de recours ;<br/>
       - les circonstances particulières justifient donc en l'espèce que le délai raisonnable d'un an ne soit pas computé à compter de la date de l'avis de mise en recouvrement ; <br/>
       - un délai d'un an à compter de la date de la décision d'admission partielle pourrait lui être reconnu ;<br/>
       - le délai contentieux pourrait à titre subsidiaire être calculé en additionnant au délai accordé par l'administration pour contester l'avis de mise en recouvrement un délai d'un an ; <br/>
       - les amendes doivent être déchargées dès lors que le procès-verbal est issu d'une application détournée d'une vérification de comptabilité ; <br/>
       - l'agent ayant dressé le procès-verbal était incompétent pour ce faire, de sorte que les sanctions ont été infligées à l'issue d'un détournement de procédure ; <br/>
       - le secret professionnel a été méconnu ; <br/>
       - l'amende en cause est contraire à l'article premier du premier protocole additionnel à la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales et à l'article 6 de cette convention ;<br/>
       - certaines infractions n'ont pas été constatées par procès-verbal ;<br/>
       - des amendes ont été irrégulièrement cumulées ; <br/>
       - le recours à la méthode par extrapolation est irrégulier ;<br/>
       - les amendes relatives aux livrets jeunes ne sont pas fondées ;<br/>
       - la règle d'unicité n'a pas été correctement appliquée ; <br/>
       - la méthode utilisée ne permet pas de sanctionner des infractions réelles.<br/>
<br/>
       Par des mémoires en défense, enregistrés le 10 juin 2016 et le 23 juillet 2019, le ministre de l'action et des comptes publics conclut au rejet de la requête.<br/>
<br/>
       Il soutient que la demande présentée par la société requérante devant les premiers juges était tardive et par suite irrecevable.<br/>
<br/>
       Vu les autres pièces du dossier.<br/>
<br/>
       Vu :<br/>
       - le code général des impôts et le livre des procédures fiscales ;<br/>
       - le code monétaire et financier ;<br/>
       - le code de justice administrative.<br/>
<br/>
       Les parties ont été régulièrement averties du jour de l'audience.<br/>
<br/>
       Ont été entendus au cours de l'audience publique :<br/>
       - le rapport de M. C..., <br/>
       - les conclusions de Mme Stoltz-Valette, rapporteure publique,<br/>
       - et les observations de Me D..., substituant Me A... et Me B..., avocats de la société Caisse d'Epargne et de Prévoyance d'Ile-de-France. <br/>
<br/>
       Considérant ce qui suit :<br/>
<br/>
       1. La société Caisse d'Epargne et de Prévoyance d'Ile-de-France (CEPIDF) a fait l'objet d'une vérification de comptabilité portant sur les exercices clos en 2007 et 2008, à l'issue de laquelle un procès-verbal relevant des infractions à des dispositions du code monétaire et financier relatives à l'ouverture et à la tenue de comptes d'épargne réglementée lui a été notifié. Elle a été, en conséquence, assujettie à des amendes prononcées sur le fondement de l'article 1739 du code général des impôts et mises en recouvrement par un avis du 14 avril 2011. Par un jugement du 9 décembre 2015, le Tribunal administratif de Paris a rejeté la demande de la société CEPIDF tendant à la décharge des amendes restant à sa charge. L'arrêt du 30 décembre 2016 par laquelle la Cour a rejeté l'appel qu'elle a formé contre ce jugement a été annulé par une décision du Conseil d'Etat du 27 février 2019, qui a renvoyé l'affaire devant la Cour. <br/>
<br/>
       2. Aux termes du I de l'article 1739 du code général des impôts, dans sa rédaction applicable à la date de la décision litigieuse : " Nonobstant toutes dispositions contraires, il est interdit à tout établissement de crédit qui reçoit du public des fonds à vue ou à moins de cinq ans, et par quelque moyen que ce soit, d'ouvrir ou de maintenir ouverts dans des conditions irrégulières des comptes bénéficiant d'une aide publique, notamment sous forme d'exonération fiscale, ou d'accepter sur ces comptes des sommes excédant les plafonds autorisés./ Sans préjudice des sanctions disciplinaires qui peuvent être infligées par l'Autorité de contrôle prudentiel, les infractions aux dispositions du présent article sont punies d'une amende fiscale dont le taux est égal au montant des intérêts payés, sans que cette amende puisse être inférieure à 75 Euros '...' ". Il ressort de ces dispositions, qui figurent également, dans des termes analogues, à l'article L. 221-35 du code monétaire et financier, que les sanctions qu'elles prévoient ont pour objet de réprimer les cas d'inobservation, par les établissements de crédit, de prescriptions relatives non pas à leurs obligations fiscales, mais aux modalités de gestion de certains produits d'épargne. Ainsi que le précise l'article L. 221-36 du code monétaire et financier, les procès-verbaux d'infraction sont dressés à la requête du ministre chargé de l'économie. <br/>
<br/>
       3. Il résulte de la combinaison de ces dispositions que l'amende prévue par l'article 1739 du code général des impôts et par l'article L. 221-35 du code monétaire et financier relève du droit commun des recours de plein contentieux contre les sanctions que l'administration inflige à un administré, comme l'a jugé le Conseil d'Etat, statuant au contentieux, dans sa décision de cassation dotée de l'autorité de chose jugée sur ce point.<br/>
<br/>
       Sur le moyen tiré du caractère inexistant des sanctions infligées à la société :<br/>
<br/>
       4. Un acte ne peut être regardé comme inexistant que s'il est dépourvu d'existence matérielle ou s'il est entaché d'un vice d'une gravité telle qu'il affecte, non seulement sa légalité, mais son existence même.<br/>
<br/>
       5. La société CEPIDF soutient que les sanctions qui lui ont été infligées constitueraient des actes inexistants, de sorte que les délais de recours ne pourraient lui être opposables. Toutefois, les moyens qu'elle soulève tirés de ce que l'agent qui a dressé le procès-verbal d'infraction n'a pas été mandaté à cet effet par le ministre chargé de l'économie, que l'administration a méconnu le principe " non bis idem ", que l'infliction des sanctions en litige est intervenue au terme d'une vérification de comptabilité dont l'objet aurait été détourné à la seule fin de contrôler l'absence d'infractions aux dispositions régissant l'ouverture et le maintien des produits d'épargne réglementée, et que l'administration s'est bornée à infliger les sanctions par catégorie de compte, sans identifier précisément les infractions constatées, ne sont, en tout état de cause, pas de nature à affecter l'existence même des sanctions contestées. Par suite, la société CEPIDF n'est pas fondée à soutenir que les sanctions qui lui ont été infligées seraient des actes inexistants dont le juge administratif pourrait constater la nullité sans condition de délai.<br/>
<br/>
       Sur la fin de non-recevoir opposée par le ministre de l'action et des comptes publics : <br/>
<br/>
       6. Aux termes de l'article R. 421-1 du code de justice administrative, dans sa version applicable au litige : " Sauf en matière de travaux publics, la juridiction ne peut être saisie que par voie de recours formé contre une décision, et ce, dans les deux mois à partir de la notification ou de la publication de la décision attaquée. '...' ". Il résulte de ces dispositions que lorsque la notification ne comporte par les mentions requises, ce délai n'est pas opposable. <br/>
<br/>
       7. Toutefois, le principe de sécurité juridique, qui implique que ne puissent être remises en cause sans condition de délai des situations consolidées par l'effet du temps, fait obstacle à ce que puisse être contestée indéfiniment une décision administrative individuelle qui a été notifiée à son destinataire, ou dont il est établi, à défaut d'une telle notification, que celui-ci a eu connaissance. En une telle hypothèse, si le non-respect de l'obligation d'informer l'intéressé sur les voies et les délais de recours, ou l'absence de preuve qu'une telle information a bien été fournie, ne permet pas que lui soient opposés les délais de recours fixés par le code de justice administrative, le destinataire de la décision ne peut exercer de recours juridictionnel au-delà d'un délai raisonnable. En règle générale et sauf circonstances particulières dont se prévaudrait le requérant, ce délai ne saurait, sous réserve de l'exercice de recours administratifs pour lesquels les textes prévoient des délais particuliers, excéder un an à compter de la date à laquelle une décision expresse lui a été notifiée ou de la date à laquelle il est établi qu'il en a eu connaissance.<br/>
<br/>
       8. L'avis de mise en recouvrement du 14 avril 2011, notifiée à la société CEPIDF le 18 avril 2011, indiquait, en son annexe, que " pour être recevable, la réclamation doit être présentée au plus tard le 31 décembre de la deuxième année suivant celle de la notification du présent avis, ou, dans l'hypothèse où les impositions ont été mises en recouvrement à la suite d'une procédure de rehaussement, le 31 décembre de la troisième année suivant celle au cours de laquelle est intervenue la proposition de rectification, lorsque ce délai est plus favorable " et ne mentionnait aucun délai de recours contentieux. Ainsi, en indiquant que les amendes en litige devaient être contestées dans des délais correspondant à ceux, applicables au contentieux fiscal, prévus par les articles R. 196-1 et suivants du livre des procédures fiscales, alors que ces amendes relèvent du droit commun des recours de plein contentieux contre les sanctions que l'administration inflige à un administré, l'administration a fourni à la société requérante une information erronée. La société CEPIDF fait valoir que le manquement par l'administration à son devoir d'information, qui a eu pour conséquence de l'induire en erreur, caractérise des " circonstances particulières ", justifiant que le " délai raisonnable ", mentionné au point 7, soit computé, non pas à compter de la notification de l'avis de mise en recouvrement mais à compter de la date de notification de la décision d'admission partielle de sa réclamation du 10 septembre 2014.<br/>
<br/>
       9. Dès lors que la société CEPIDF a reçu, en annexe de l'avis de mise en recouvrement, une information erronée quant aux délais applicables en l'espèce, la circonstance que sa réclamation n'a été présentée que le 26 décembre 2013, soit dans le délai qui avait été fixé, à tort, par l'administration, ne saurait lui être opposée. A cet égard, le dépôt, dans le délai fixé par l'administration, de cette réclamation, qui devait en réalité être regardée comme un recours gracieux, a nécessairement prolongé le délai de recours contentieux. En revanche, si la société requérante soutient qu'un délai d'un an à compter de la notification de la décision portant admission partielle de sa réclamation devait lui être accordé, il résulte des mentions de cette décision que l'administration a valablement indiqué que la société disposait d'un délai de deux mois pour présenter sa demande devant le Tribunal administratif. Cette mention, qui n'est pas erronée, ne saurait ouvrir à la société le bénéfice d'un délai plus long. La société CEPIDF ne peut davantage soutenir qu'un délai d'un an devrait lui être accordé à compter de la fin du délai de réclamation prévu par l'article R. 196-1 du livre des procédures fiscales. Dans ces conditions, le " délai raisonnable " dont pouvait se prévaloir la société CEPIDF pour présenter sa demande devant le Tribunal administratif s'étend sur la période comprise entre la date de notification de l'avis de mise en recouvrement et la date de notification de la décision portant admission partielle de la réclamation de la société, prolongée d'une période de deux mois correspondant au délai valablement mentionné par le service dans cette décision. Ainsi, et dès lors que la décision d'acceptation partielle de la réclamation a été notifiée à la société CEPIDF le 11 septembre 2014, la demande enregistrée au greffe du Tribunal administratif de Paris le 15 novembre 2014 doit être regardée comme tardive et donc irrecevable.<br/>
<br/>
       10. Il résulte de tout ce qui précède que la société CEPIDF n'est pas fondée à se plaindre de ce que, par le jugement attaqué, le Tribunal administratif de Paris a rejeté sa demande. Ses conclusions présentées sur le fondement de l'article L. 761-1 du code de justice administrative doivent donc être également rejetées.<br/>
<br/>
D E C I D E :<br/>
<br/>
<br/>
Article 1er : La requête de la société Caisse d'Epargne et de Prévoyance d'Ile-de-France est rejetée.<br/>
<br/>
Article 2 : Le présent arrêt sera notifié à la société Caisse d'Epargne et de Prévoyance d'Ile-de-France et au ministre de l'économie, des finances et de la relance.<br/>
Copie en sera adressée à la direction des vérifications nationales et internationales.<br/>
Délibéré après l'audience du 6 juillet 2021, à laquelle siégeaient :<br/>
<br/>
- M. Jardin, président de chambre,<br/>
- M. C..., premier conseiller.<br/>
- M. Sibilli, premier conseiller.<br/>
<br/>
Rendu public par mise à disposition au greffe, le 20 juillet 2021.<br/>
       Le rapporteur,<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
       K. C...<br/>
<br/>
<br/>
<br/>
<br/>
       Le président,<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
       C. JARDIN       La greffière,<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
       C. BUOT<br/>
<br/>
La République mande et ordonne au ministre de l'économie, des finances et de la relance en ce qui le concerne ou à tous huissiers de justice à ce requis en ce qui concerne les voies de droit commun contre les parties privées, de pourvoir à l'exécution de la présente décision.<br/>
<br/>
<br/>
N° 19PA00904		2<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">13-04 Capitaux, monnaie, banques. - Banques.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">19-01-04 Contributions et taxes. - Généralités. - Amendes, pénalités, majorations.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">54-01-07 Procédure. - Introduction de l'instance. - Délais.
</SCT>
<SCT ID="8D" TYPE="PRINCIPAL">59-02-01 Répression. - Domaine de la répression administrative - Nature de la sanction administrative.
</SCT>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
