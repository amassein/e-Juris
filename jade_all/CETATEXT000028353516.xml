<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028353516</ID>
<ANCIEN_ID>JG_L_2013_12_000000344309</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/35/35/CETATEXT000028353516.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème / 9ème SSR, 20/12/2013, 344309, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-12-20</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>344309</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème / 9ème SSR</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP CELICE, BLANCPAIN, SOLTNER</AVOCATS>
<RAPPORTEUR>M. François Loloum</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Delphine Hedary</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2013:344309.20131220</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 15 novembre 2010 et 14 février 2011 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. et Mme B...A..., demeurant... ; M. et Mme A...demandent au Conseil d'Etat :<br/>
<br/>
              1) d'annuler l'arrêt n° 09NT01069 du 29 septembre 2010 par lequel la cour administrative d'appel de Nantes, faisant droit au recours du ministre du budget, des comptes publics et de la fonction publique, a, d'une part, annulé le jugement n° 06-1443 du 30 décembre 2008 du tribunal administratif de Rennes en tant qu'il a fait droit à la demande des exposants, d'autre part, remis à la charge de ceux-ci les cotisations supplémentaires d'impôt sur le revenu et de contributions sociales et les pénalités correspondantes auxquelles ils ont été assujettis au titre de l'année 2000 ;<br/>
<br/>
              2) de mettre à la charge de l'Etat la somme de 3 500 euros en application des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code général des impôts ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. François Loloum, Conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Delphine Hedary, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Célice, Blancpain, Soltner, avocat de M. et Mme A...;<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que M. et Mme A...exploitaient un laboratoire d'analyses médicales sous la forme d'une société civile professionnelle (SCP), dont ils détenaient chacun 50 % des parts ; que, le 28 décembre 1998, ils ont procédé, au bilan de la SCP, à une réévaluation libre du droit de présentation de la clientèle ; que, le 1er septembre 2000, cette société de personnes a été transformée en une société d'exercice libéral à responsabilité limitée, société de capitaux passible de l'impôt sur les sociétés, la valeur du droit de présentation de la clientèle réévaluée en 1998 étant portée au bilan de la nouvelle société  ; qu'à la suite d'une vérification de comptabilité de la SCP, l'administration, estimant que les conditions d'un report d'imposition n'étaient pas réunies, a imposé entre les mains des contribuables la plus-value latente à long terme résultant de la réévaluation libre du droit de présentation de la clientèle ; que, par un jugement du 30 décembre 2008, le tribunal administratif de Rennes a fait droit à la demande de M. et Mme A... et les a déchargés des impositions supplémentaires et pénalités mises à leur charge au titre de l'année 2000 ; que, par un arrêt du 29 septembre 2010 contre lequel ils se pourvoient en cassation, la cour administrative d'appel de Nantes a annulé le jugement du tribunal administratif de Nantes et remis à la charge de M. et Mme A...les impositions et pénalités en litige ; <br/>
<br/>
              2. Considérant, d'une part, qu'en vertu du 1 de l'article 93 du code général des impôts, le bénéfice à retenir dans les bases de l'impôt sur le revenu tient compte des gains ou des pertes provenant notamment de la réalisation des éléments d'actif affectés à l'exercice de la profession ; qu'aux termes de l'article 93 quater de ce code : " I. Les plus-values réalisées sur des immobilisations sont soumises au régime des articles 39 duodecies à 39 quindecies " ; qu'en vertu de l'article 39 duodecies du même code, le régime des plus-values à long terme est applicable aux plus-values provenant de la cession d'éléments non amortissables de l'actif immobilisé détenus depuis plus de deux ans ; qu'aux termes de l'article 8 ter du même code : " Les associés des sociétés civiles professionnelles constituées pour l'exercice en commun de la profession de leurs membres et fonctionnant conformément aux dispositions de la loi n° 66-879 du 29 novembre 1966 modifiée sont personnellement soumis à l'impôt sur le revenu pour la part des bénéfices sociaux qui leur est attribuée (...) " ;<br/>
<br/>
              3. Considérant, d'autre part, qu'aux termes de l'article 202 du code général des impôts : " 1. Dans le cas de cessation de l'exercice d'une profession non commerciale, l'impôt sur le revenu dû en raison des bénéfices provenant de l'exercice de cette profession y compris ceux qui proviennent de créances acquises et non encore recouvrées et qui n'ont pas encore été imposés est immédiatement établi. (...) " ; qu'en application de l'article 202 ter de ce code, dans sa rédaction applicable aux impositions en litige : " I. L'impôt sur le revenu est établi dans les conditions prévues aux articles 201 et 202 lorsque les sociétés ou organismes placés sous le régime des sociétés de personnes défini aux articles 8 à 8 ter cessent totalement ou partiellement d'être soumis à ce régime ou s'ils changent leur objet social ou leur activité réelle (...) / Toutefois en l'absence de création d'une personne morale nouvelle, les bénéfices en sursis d'imposition et les plus-values latentes incluses dans l'actif social ne font pas l'objet d'une imposition immédiate à la double condition qu'aucune modification ne soit apportée aux écritures comptables et que l'imposition desdits bénéfices et plus-values demeure possible sous le nouveau régime fiscal applicable à la société ou à l'organisme concerné. (...) " ; <br/>
<br/>
              4. Considérant qu'il résulte de l'ensemble de ces dispositions que si la transformation d'une société civile professionnelle en société d'exercice libéral à responsabilité limitée implique, en principe, l'imposition immédiate des plus-values latentes, il peut être sursis à cette imposition à la triple condition que la transformation de la société n'entraîne pas la création d'une personne morale nouvelle, qu'elle n'emporte aucune modification des écritures comptables et qu'elle n'ait pas pour effet d'empêcher l'imposition des plus values sous le nouveau régime fiscal applicable à la société issue de la transformation ; que la condition relative à l'absence de modification des écritures comptables doit s'apprécier au regard des valeurs figurant dans les écritures des sociétés concernées au moment de la transformation d'une société en une autre ;    <br/>
<br/>
              5. Considérant que, pour confirmer le redressement litigieux, la cour a jugé que la réévaluation de un à cinq millions de francs de la valeur de la clientèle de la SCP " Morvan-A... " réalisée le 28 décembre 1998 et transcrite dans les comptes de cette société, puis son inscription deux ans plus tard à l'identique à l'actif du bilan d'ouverture de la société d'exercice libéral à responsabilité limitée " Morvan-A... " issue de la transformation de la SCP le 1er décembre 2000, devaient être regardées comme réalisant une modification des écritures comptables au sens des dispositions de l'article 202 ter du code général des impôts, dès lors que l'écriture passée en 1998 avait pour effet de rendre impossible l'imposition d'une plus value latente de 4 millions ; qu'en statuant ainsi, alors qu'il résulte de ce qui a été dit au point 4 qu'il lui appartenait de s'assurer, d'une part, que les écritures comptables étaient demeurées constantes au moment du changement de statut et de rechercher, d'autre part, si, en dépit de la constance des écritures, tout ou partie des plus-values latentes pouvaient, en raison de la transformation statutaire, échapper ultérieurement à l'imposition, la cour, qui a interprété l'un des critères de l'article 202 ter en fonction des finalités d'un autre, et non l'un après l'autre, a méconnu les dispositions de l'article 202 ter du code général des impôts ; que son arrêt doit, pour ce motif, être annulé ; <br/>
<br/>
              6. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat la somme de 3 000 euros en application des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>				D E C I D E :<br/>
              				--------------<br/>
Article 1 : L'arrêt de la cour administrative d'appel de Nantes du 29 septembre 2010 est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Nantes.<br/>
Article 3 : L'Etat versera à M. et Mme A...une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à M. et Mme B... A...et au ministre de l'économie et des finances. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
