<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000026454666</ID>
<ANCIEN_ID>JG_L_2012_10_000000360952</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/26/45/46/CETATEXT000026454666.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème et 2ème sous-sections réunies, 03/10/2012, 360952</TITRE>
<DATE_DEC>2012-10-03</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>360952</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème et 2ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BORE ET SALVE DE BRUNETON</AVOCATS>
<RAPPORTEUR>M. Jean-Dominique Nuttens</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Bertrand Dacosta</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2012:360952.20121003</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 11 et 23 juillet 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour la société Déménagements Le Gars - Hauts-de-Seine Déménagements, dont le siège est 68 bis boulevard Pereire à Paris (75017), représentée par son gérant en exercice ; la société demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler l'ordonnance n° 1204013-2 du 5 juin 2012 par laquelle le président de la 2ème chambre du tribunal administratif de Melun, statuant en application de l'article L. 551-1 du code de justice administrative a annulé, à la demande de la société Organidem, la procédure de passation du marché lancé par le département du Val-de-Marne ayant pour objet les transferts et déménagements de mobiliers et matériels sur les sites et établissements départementaux du Val-de-Marne ; <br/>
<br/>
              2°) statuant en référé, de rejeter la demande de la société Organidem ; <br/>
<br/>
              3°) de mettre à la charge de la société Organidem le versement de la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code des marchés publics ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Jean-Dominique Nuttens, Maître des Requêtes en service extraordinaire,  <br/>
<br/>
              - les observations de la SCP Boré et Salve de Bruneton, avocat de la société Déménagements Le Gars - Hauts-de-Seine Déménagements,<br/>
<br/>
              - les conclusions de M. Bertrand Dacosta, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Boré et Salve de Bruneton, avocat de la société Déménagements Le Gars - Hauts-de-Seine Déménagements ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes de l'article L. 551-1 du code de justice administrative : " Le président du tribunal administratif, ou le magistrat qu'il délègue, peut être saisi en cas de manquement aux obligations de publicité et de mise en concurrence auxquelles est soumise la passation par les pouvoirs adjudicateurs de contrats administratifs ayant pour objet l'exécution de travaux, la livraison de fournitures ou la prestation de services, avec une contrepartie économique constituée par un prix ou un droit d'exploitation, ou la délégation d'un service public (...) " ; qu'aux termes de l'article L. 551-2 de ce code : " Le juge peut ordonner à l'auteur du manquement de se conformer à ses obligations et suspendre l'exécution de toute décision qui se rapporte à la passation du contrat, sauf s'il estime en considération de l'ensemble des intérêts susceptibles d'être lésés et notamment de l'intérêt public, que les conséquences négatives de ces mesures pourraient l'emporter sur leurs avantages. / Il peut, en outre, annuler les décisions qui se rapportent à la passation du contrat et supprimer les clauses ou prescriptions destinées à figurer dans le contrat et qui méconnaissent lesdites obligations. " ; qu'aux termes de l'article L. 551-10 : " Les personnes habilitées à engager les recours prévus aux articles L. 551-1 et L. 551-5 sont celles qui ont un intérêt à conclure le contrat et qui sont susceptibles d'être lésées par le manquement invoqué (...) " ;<br/>
<br/>
              2. Considérant qu'il ressort des pièces du dossier soumis au juge des référés que, par un avis d'appel public à la concurrence du 23 décembre 2011, le département du <br/>
Val-de-Marne a lancé une procédure d'appel d'offres ouvert en vue de l'attribution d'un marché à bons de commande ayant pour objet les transferts et déménagements de mobiliers et matériels sur les sites et établissements départementaux du Val-de-Marne ; qu'à l'issue de la procédure, le pouvoir adjudicateur a retenu l'offre de la société Déménagements Le Gars - Hauts-de-Seine Déménagements ; que cette société se pourvoit en cassation contre l'ordonnance du 5 juin 2012 par laquelle le juge des référés du tribunal administratif de Melun, à la demande de la société Organidem, a annulé la procédure de passation du marché ;<br/>
<br/>
              3. Considérant, en premier lieu, qu'il ressort des pièces du dossier que, conformément aux dispositions de l'article R. 742-5 du code de justice administrative, et contrairement à ce que soutient la requérante, la minute de l'ordonnance attaquée comporte la signature du magistrat qui l'a rendue ;<br/>
<br/>
              4. Considérant, en deuxième lieu, que le code des marchés publics fixe précisément et limitativement la liste des documents qui peuvent être exigés par le pouvoir adjudicateur à l'appui des candidatures, ainsi que les motifs pour lesquels les candidatures peuvent être écartées sur la foi de ces informations ; que la prise en compte par le pouvoir adjudicateur de renseignements erronés relatifs aux capacités professionnelles, techniques et financières d'un candidat est susceptible de fausser l'appréciation portée sur les mérites de cette candidature au détriment des autres candidatures et ainsi de porter atteinte au principe d'égalité de traitement entre les candidats ; <br/>
<br/>
              5. Considérant que le juge des référés, par une appréciation souveraine des pièces du dossier, a relevé que des informations figurant dans le dossier de candidature présenté par la société Déménagements Le Gars - Hauts-de-Seine Déménagements étaient fausses et qu'en particulier, la société déclarait pour l'année 2010 un chiffre d'affaires de 3 770 700 euros, très supérieur à celui de 770 637 euros figurant dans son bilan et son compte de résultats obtenus par la société Organidem par l'intermédiaire du site " Infogreffe " et produits au cours de l'instance ; qu'il a également relevé que les informations relatives au montant des salaires et à la valeur des véhicules figurant dans ces documents étaient incompatibles avec les déclarations relatives à l'effectif salarié et au nombre de véhicules figurant dans le dossier de candidature de la société Déménagements Le Gars - Hauts-de-Seine Déménagements ; que le juge des référés n'a ainsi ni commis d'erreur de droit ni inexactement qualifié les faits de l'espèce en jugeant que le choix de l'offre de la société Déménagements Le Gars - Hauts-de-Seine Déménagements, fondé sur de fausses déclarations, avait porté atteinte au principe d'égalité de traitement des candidats et de transparence des procédures ;<br/>
<br/>
              6. Considérant, en troisième lieu, qu'il appartient au juge des référés précontractuels, en application des dispositions de l'article L. 551-10 du code de justice administrative, de rechercher si l'entreprise qui le saisit se prévaut de manquements qui, eu égard à leur portée et au stade de la procédure auquel ils se rapportent, sont susceptibles de l'avoir lésée ou risquent de la léser, fût-ce de façon indirecte en avantageant une entreprise concurrente ; que le choix de l'offre d'un candidat dont la candidature a été retenue sur la base d'informations relatives à ses capacités financières et professionnelles erronées est susceptible d'avoir lésé le candidat qui invoque ce manquement, à moins qu'il ne résulte de l'instruction que sa candidature devait elle-même être écartée, ou que l'offre qu'il présentait ne pouvait qu'être éliminée comme inappropriée, irrégulière ou inacceptable ; qu'en jugeant que le manquement qu'il avait relevé avait été susceptible d'avoir lésé la société Organidem, quel qu'ait été son propre rang de classement à l'issue du jugement des offres, le juge des référés du tribunal administratif de Melun n'a pas commis d'erreur de droit ;<br/>
<br/>
              7. Considérant qu'il résulte de ce qui précède que le pourvoi de la société Déménagements Le Gars - Hauts-de-Seine Déménagements doit être rejeté, y compris ses conclusions tendant à l'application des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi présenté par la société Déménagements Le Gars - Hauts-de-Seine Déménagements est rejeté.<br/>
Article 2 : La présente décision sera notifiée à la société Déménagements Le Gars - Hauts-de-Seine Déménagements, à la société Organidem et au département du Val-de-Marne.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">39-02-02-03 MARCHÉS ET CONTRATS ADMINISTRATIFS. FORMATION DES CONTRATS ET MARCHÉS. MODE DE PASSATION DES CONTRATS. APPEL D'OFFRES. - PRISE EN COMPTE PAR LE POUVOIR ADJUDICATEUR DE RENSEIGNEMENTS ERRONÉS SUR LES CAPACITÉS DES CANDIDATS - MANQUEMENT AUX OBLIGATIONS DE PUBLICITÉ ET DE MISE EN CONCURRENCE - EXISTENCE - CONSÉQUENCE - MANQUEMENT SAISISSABLE PAR LA VOIE DU RÉFÉRÉ PRÉCONTRACTUEL.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">39-08-015-01 MARCHÉS ET CONTRATS ADMINISTRATIFS. RÈGLES DE PROCÉDURE CONTENTIEUSE SPÉCIALES. PROCÉDURES D'URGENCE. - MANQUEMENT AUX OBLIGATIONS DE PUBLICITÉ ET DE MISE EN CONCURRENCE - NOTION - PRISE EN COMPTE PAR LE POUVOIR ADJUDICATEUR DE RENSEIGNEMENTS ERRONÉS SUR LES CAPACITÉS DES CANDIDATS - INCLUSION.
</SCT>
<ANA ID="9A"> 39-02-02-03 La prise en compte par le pouvoir adjudicateur de renseignements erronés relatifs aux capacités professionnelles, techniques et financières d'un candidat est susceptible de fausser l'appréciation portée sur les mérites de cette candidature au détriment des autres candidatures et ainsi de porter atteinte au principe d'égalité de traitement entre les candidats. Un tel manquement est saisissable par la voie du référé précontractuel.</ANA>
<ANA ID="9B"> 39-08-015-01 La prise en compte par le pouvoir adjudicateur de renseignements erronés relatifs aux capacités professionnelles, techniques et financières d'un candidat est susceptible de fausser l'appréciation portée sur les mérites de cette candidature au détriment des autres candidatures et ainsi de porter atteinte au principe d'égalité de traitement entre les candidats. Un tel manquement est donc saisissable par la voie du référé précontractuel.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
