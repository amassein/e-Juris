<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042434178</ID>
<ANCIEN_ID>JG_L_2020_10_000000420092</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/43/41/CETATEXT000042434178.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 9ème - 10ème chambres réunies, 15/10/2020, 420092, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-10-15</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>420092</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>9ème - 10ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BUK LAMENT - ROBILLOT</AVOCATS>
<RAPPORTEUR>M. Matias de Sainte Lorette</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Céline  Guibé</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:420092.20201015</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La communauté urbaine de Dunkerque a demandé au tribunal administratif de Lille de condamner l'Etat à réparer, à concurrence de la somme de 10 319 537 euros, le préjudice que lui a causé la faute commise par l'administration fiscale lors de l'établissement de la taxe professionnelle des années 2006 à 2009 de la société Polimeri Europa France et lors de l'établissement de la " compensation-relais " due au titre de 2010. Par un jugement avant dire droit n° 1207173 du 26 novembre 2015, le tribunal administratif de Lille a jugé que la responsabilité de l'Etat était engagée à son égard à raison des fautes commises dans l'établissement de la taxe professionnelle pour les années 2006 et 2009 et pour la " compensation-relais " de 2010, a ordonné un supplément d'instruction avant de statuer sur l'évaluation du préjudice, et rejeté les conclusions tendant à l'indemnisation du préjudice résultant de l'absence d'application des intérêts de retard. Par un jugement n° 1207173 du 28 décembre 2016, ce même tribunal a condamné l'Etat à verser à la communauté urbaine de Dunkerque la somme de 2 790 575 euros assortie des intérêts au taux légal.<br/>
<br/>
              Par un arrêt n° 17DA00496 du 22 février 2018, la cour administrative d'appel de Douai a rejeté l'appel formé par le ministre de l'économie et des finances contre l'article 1er du jugement du 28 décembre 2016.<br/>
<br/>
              Par un pourvoi et un nouveau mémoire, enregistrés le 23 avril 2018 et le 14 novembre 2019 au secrétariat du contentieux du Conseil d'Etat, le ministre de l'action et des comptes publics demande au Conseil d'Etat d'annuler cet arrêt.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le code général des impôts ;<br/>
              - la loi n° 2009-1673 du 30 décembre 2009 ;<br/>
              - la décision n° 360973 du 7 novembre 2013 du Conseil d'Etat, statuant au contentieux ; <br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Matias de Sainte Lorette, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Céline Guibé, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Buk-Lament - Robillot, avocat de la communauté urbaine de Dunkerque ;<br/>
<br/>
<br/>
<br/>1. Il ressort des pièces du dossier soumis aux juges du fond que la société Polimeri Europa France SNC a été absorbée le 28 février 2005, par traité de fusion avec transmission universelle de patrimoine, par la société Polimeri Europa Distribution France SAS, devenue société Polimeri Europa France SAS. La communauté urbaine de Dunkerque a estimé que les cotisations primitives de taxe professionnelle auxquelles a été assujettie la société Polimeri Europa France SAS au titre des années 2006 à 2009 et la compensation de la disparition de la taxe professionnelle, dite " compensation-relais ", au titre de l'année 2010, étaient insuffisantes au regard des conséquences qu'avait entraîné sur les bases d'imposition la fusion-absorption du 18 mars 2005, et elle a, par réclamation du 23 décembre 2010, demandé à l'Etat le versement d'une indemnité en réparation du préjudice qu'elle estime avoir subi à raison des fautes commises par l'administration fiscale dans la détermination des cotisations de taxe professionnelle auxquelles la société Polimeri Europa France SAS a été assujettie au titre des années 2006 à 2009 et de la " compensation-relais " au titre de 2010. Par un jugement du 28 décembre 2016, le tribunal administratif de Lille a condamné l'Etat, en réparation de ce préjudice, à verser à la communauté urbaine de Dunkerque une somme de 2 790 575 euros. Le ministre de l'action et des comptes publics se pourvoit en cassation contre l'arrêt du 22 février 2018 par lequel la cour administrative d'appel de Douai a rejeté son appel contre ce jugement.<br/>
<br/>
              Sur le bien-fondé de l'arrêt en tant qu'il retient l'existence d'une faute commise par l'administration fiscale :<br/>
<br/>
              2. Une faute commise par l'administration lors de l'exécution d'opérations se rattachant aux procédures d'établissement ou de recouvrement de l'impôt est de nature à engager la responsabilité de l'Etat à l'égard d'une collectivité territoriale ou de toute autre personne publique si elle lui a directement causé un préjudice. Un tel préjudice peut être constitué des conséquences matérielles des décisions prises par l'administration et notamment du fait de ne pas avoir versé à cette collectivité ou à cette personne des impôts ou taxes qui auraient dû être mis en recouvrement à son profit. Le montant du préjudice indemnisable doit être calculé en prenant en compte, le cas échéant, des impositions qui ont pu être perçues à tort par la même collectivité, à condition que cette perception ait un lien direct avec la faute qui se trouve à l'origine du préjudice dont la réparation est demandée.<br/>
<br/>
              3. Aux termes de l'article 1467 du code général des impôts, dans sa version applicable jusqu'au 1er janvier 2010 : " La taxe professionnelle a pour base : / (...) a. la valeur locative, telle qu'elle est définie aux articles 1469, 1518 A et 1518 B, des immobilisations corporelles dont le redevable a disposé pour les besoins de son activité professionnelle pendant la période de référence définie aux articles 1467 A et 1478, à l'exception de celles qui ont été détruites ou cédées au cours de la même période ; ". Aux termes de l'article 1469 du même code, dans sa version issue de la loi du 30 décembre 2004 de finances rectificative pour 2004 : " La valeur locative est déterminée comme suit : / (...) 3° quater Le prix de revient d'un bien cédé n'est pas modifié lorsque ce bien est rattaché au même établissement avant et après la cession et lorsque, directement ou indirectement : / a. l'entreprise cessionnaire contrôle l'entreprise cédante ou est contrôlée par elle ; / b. ou ces deux entreprises sont contrôlées par la même entreprise ; ". Aux termes de l'article 1518 B, dans sa rédaction applicable jusqu'au 1er janvier 2010 : " A compter du 1er janvier 1980, la valeur locative des immobilisations corporelles acquises à la suite d'apports, de scissions, de fusions de sociétés ou de cessions d'établissements réalisés à partir du 1er janvier 1976 ne peut être inférieure aux deux tiers de la valeur locative retenue l'année précédant l'apport, la scission, la fusion ou la cession. / (...) Pour les opérations mentionnées au premier alinéa réalisées à compter du 1er janvier 1992, la valeur locative des immobilisations corporelles ne peut être inférieure aux quatre cinquièmes de son montant avant l'opération. ".<br/>
<br/>
              4. Il résulte des termes mêmes des dispositions du 3° quater de l'article 1469 du code général des impôts que les cessions de biens qu'elles visent s'entendent des seuls transferts de propriété consentis entre un cédant et un cessionnaire. Ces dispositions, dont les termes renvoient à une opération définie et régie par le droit civil, ne sauraient, dès lors, s'entendre comme incluant toutes autres opérations qui, sans constituer des cessions proprement dites, ont pour conséquence une mutation patrimoniale. Si, en vertu de l'article 1844-4 du code civil, les opérations de fusion-absorption emportent transfert du patrimoine de la société absorbée à la société absorbante, cette mutation patrimoniale, qui entraîne obligatoirement la dissolution sans liquidation de la société absorbée qui disparaît, peut être regardée comme une cession au regard du droit civil, dès lors que le traité de fusion-absorption conclu traduit une rencontre de volonté des sociétés cédantes et cessionnaires et que celles-ci sont liées.<br/>
<br/>
              5. Il ressort des énonciations de l'arrêt attaqué, non contestées sur ce point, que l'opération de restructuration réalisée le 28 février 2005 entrait dans le champ des dispositions du 3° quater de l'article 1469 du code général des impôts, sans qu'y fassent obstacle les dispositions de l'article 1518 B de ce code.<br/>
<br/>
              6. En premier lieu, en jugeant que la circonstance que l'interprétation pertinente des dispositions précitées du 3° quater de l'article 1469 du code général des impôts n'aurait été donnée que par une décision du Conseil d'Etat statuant au contentieux du 7 novembre 2013, postérieure aux années au titre desquelles ont été établies les impositions en litige, alors que plusieurs cours administrative d'appel avaient statué en sens contraire, n'est pas de nature à exonérer l'administration de sa responsabilité, la cour n'a pas commis d'erreur de droit. <br/>
<br/>
              7. En second lieu, aux termes du II de l'article 1640 B du code général des impôts : " les collectivités territoriales, à l'exception de la région Ile-de-France, et les établissements publics de coopération intercommunale dotés d'une fiscalité propre reçoivent au titre de l'année 2010, en lieu et place du produit de la taxe professionnelle, une compensation relais. / Le montant de cette compensation relais est, pour chaque collectivité ou établissement public de coopération intercommunale à fiscalité propre, égal au plus élevé des deux montants suivants : / - le produit de la taxe professionnelle qui résulterait pour cette collectivité territoriale ou cet établissement public de l'application, au titre de l'année 2010, des dispositions relatives à cette taxe dans leur version en vigueur au 31 décembre 2009. Toutefois, pour le calcul de ce produit, d'une part, il est fait application des délibérations applicables en 2009 relatives aux bases de taxe professionnelle, d'autre part, le taux retenu est le taux de taxe professionnelle de la collectivité territoriale ou de l'établissement public pour les impositions au titre de l'année 2009 dans la limite du taux voté pour les impositions au titre de l'année 2008 majoré de 1 % ; / - le produit de la taxe professionnelle de la collectivité territoriale ou de l'établissement public au titre de l'année 2009. / (...) ". Aux termes du I du 1.4 de l'article 78 de la loi du 30 décembre 2009 : " En tant que de besoin, le montant de la compensation relais prévue au II de l'article 1640 B du code général des impôts est corrigé sur la base des impositions à la taxe professionnelle et à la cotisation foncière des entreprises émises jusqu'au 30 juin 2011 et des dégrèvements de taxe professionnelle et de cotisation foncière des entreprises ordonnancés jusqu'à la même date. Le montant de la correction est, le cas échéant, notifié à la collectivité territoriale concernée pour le 31 juillet 2011. ".<br/>
<br/>
              8. Pour juger que l'administration avait commis une faute de nature à engager la responsabilité de l'Etat en s'abstenant de procéder au rehaussement de la cotisation de la compensation relais assignée à la société Polimeri Europa France SAS au titre de l'année 2010, ainsi que la communauté urbaine de Dunkerque le lui avait demandé par un courrier du 23 décembre 2010, la cour s'est fondée sur la circonstance que le délai de six mois dont aurait disposé l'administration fiscale pour procéder à un tel rehaussement constituait un délai raisonnable pour recueillir les éléments et informations nécessaires, notamment dans le cadre d'une vérification de comptabilité. En statuant ainsi, la cour, qui n'avait pas à rechercher si l'évaluation des bases de la société Polimeri Europa France SAS comportait une difficulté particulière, n'a pas inexactement qualifié les faits qui lui étaient soumis.<br/>
<br/>
              Sur le bien-fondé de l'arrêt en tant qu'il statue sur le montant du préjudice indemnisable de la communauté urbaine :<br/>
<br/>
              9. Ainsi qu'il a été dit au point 2, le montant du préjudice indemnisable doit être calculé en tenant compte, le cas échéant, des impositions qui ont pu être perçues à tort par la collectivité territoriale en cause, à condition qu'une telle perception ait un lien direct avec la faute qui se trouve à l'origine du préjudice dont la réparation est demandée. Par suite, en jugeant que les recettes fiscales correspondant à deux dégrèvements accordés à d'autres contribuables, en raison d'erreurs commises par ceux-ci lors de la souscription de leurs déclarations, dans le montant des éléments servant au calcul des bases de la taxe professionnelle, ne pouvaient pas être pris en compte pour la détermination du montant du préjudice indemnisable de la communauté urbaine de Dunkerque, la cour administrative d'appel de Douai n'a pas commis d'erreur de droit.<br/>
<br/>
              10. Il résulte de tout ce qui précède que le ministre de l'action et des comptes publics n'est pas fondé à demander l'annulation de l'arrêt qu'il attaque. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat une somme de 3 000 euros à verser à la communauté urbaine de Dunkerque au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi du ministre de l'action et des comptes publics est rejeté.<br/>
Article 2 : L'Etat versera une somme de 3 000 euros à la communauté urbaine de Dunkerque au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : La présente décision sera notifiée au ministre de l'économie, des finances et de la relance et à la communauté urbaine de Dunkerque.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
