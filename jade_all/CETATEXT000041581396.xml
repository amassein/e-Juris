<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000041581396</ID>
<ANCIEN_ID>JG_L_2020_02_000000432217</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/41/58/13/CETATEXT000041581396.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème chambre, 12/02/2020, 432217, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-02-12</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>432217</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR>M. Réda Wadjinny-Green</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Alexandre Lallet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2020:432217.20200212</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme A... E... épouse D... et M. B... C... ont demandé au juge des référés du tribunal administratif de Strasbourg, sur le fondement des dispositions de l'article L. 521-1 du code de justice administrative, d'une part, de suspendre l'exécution des décisions implicites par lesquelles le préfet du Bas-Rhin a prolongé le délai de leur transfert vers l'Autriche et refusé d'enregistrer leurs demandes d'asile et, d'autre part, d'enjoindre sous astreinte au préfet de leur délivrer un formulaire de demande d'asile et une attestation de demande d'asile. Par deux ordonnances n° 1903322 et n° 1903320 du 21 mai 2019, le juge des référés du tribunal administratif de Strasbourg a rejeté leurs demandes.<br/>
<br/>
              1° Sous le n° 432217, par un pourvoi sommaire, un mémoire complémentaire et un nouveau mémoire, enregistrés les 3 et 18 juillet 2019 et le 24 janvier 2020 au secrétariat du contentieux du Conseil d'Etat, Mme E... épouse D... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'ordonnance n° 1903322 du 21 mai 2019 ;<br/>
<br/>
              2°) statuant en référé, de faire droit à ses demandes ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 000 euros à verser à la SCP Piwnica et Molinié, son avocat, au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991. <br/>
<br/>
<br/>
              2° Sous le n° 432218, par un pourvoi sommaire, un mémoire complémentaire et un nouveau mémoire, enregistrés les 3 et 18 juillet 2019 et le 24 janvier 2020 au secrétariat du contentieux du Conseil d'Etat, M. C... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'ordonnance n° 1903320 du 21 mai 2019 ;<br/>
<br/>
              2°) statuant en référé, de faire droit à ses demandes ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 000 euros à verser à la SCP Piwnica et Molinié, son avocat, au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991.<br/>
<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu :<br/>
              - le règlement (CE) n° 1560/2003 de la Commission du 2 septembre 2003 ;<br/>
              - le règlement (UE) n° 604/2013 du Parlement européen et du Conseil du 26 juin 2013 ;<br/>
              - le règlement d'exécution (UE) n° 118/2014 de la Commission du 30 janvier 2014 ;<br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Réda Wadjinny-Green, auditeur, <br/>
<br/>
              - les conclusions de M. Alexandre Lallet, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Piwnica, Molinié, avocat de Mme D... et de M. C... ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
              1. Les pourvois de Mme E... épouse D... et M. C... présentent à juger les mêmes questions. Il y a lieu de les joindre pour statuer par une seule décision.<br/>
<br/>
              2. Il ressort des pièces des dossiers soumis au juge des référés du tribunal administratif de Strasbourg que Mme E... épouse D... et M. C..., ressortissants russes, sont entrés irrégulièrement en France avec leurs filles mineures et ont présenté une demande d'asile à la préfecture du Bas-Rhin le 26 juin 2018. À la suite des recherches effectuées dans le fichier Eurodac, qui ont révélé qu'ils avaient précédemment sollicité l'asile en Autriche, le préfet du Bas-Rhin a décidé de les remettre aux autorités autrichiennes par arrêtés du 23 juillet 2018 et les a assignés à résidence. Par un jugement nos 1805293, 1805294 du 5 septembre 2018, le tribunal administratif de Strasbourg a rejeté les demandes présentées par M. et Mme C... tendant à l'annulation pour excès de pouvoir de ces arrêtés. Le 6 novembre 2018, l'Autriche a été avisée par le préfet du Bas-Rhin de la fuite de M. et Mme C... et de l'extension du délai de leur transfert à dix-huit mois, en application du paragraphe 2 de l'article 29 du règlement (UE) n° 604/2013 du Parlement européen et du Conseil du 26 juin 2013. M. et Mme C... ont présenté de nouvelles demandes d'asile au préfet du Bas-Rhin les 12 novembre et 6 décembre 2018. Ils se pourvoient en cassation contre les ordonnances par lesquelles le juge des référés du tribunal administratif de Strasbourg a rejeté leurs demandes tendant, d'une part, à la suspension de l'exécution des décisions implicites par lesquelles le préfet du Bas-Rhin a prolongé le délai de leur transfert vers l'Autriche et refusé d'enregistrer leurs demandes d'asile et, d'autre part, à ce qu'il soit enjoint au préfet de leur délivrer un formulaire de demande d'asile et une attestation de demande d'asile.<br/>
<br/>
              3. L'article L. 521-1 du code de justice administrative dispose que : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision. / Lorsque la suspension est prononcée, il est statué sur la requête en annulation ou en réformation de la décision dans les meilleurs délais. La suspension prend fin au plus tard lorsqu'il est statué sur la requête en annulation ou en réformation de la décision ". <br/>
<br/>
              4. L'article 29 du règlement (UE) n° 604/2013 du Parlement européen et du Conseil du 26 juin 2013, qui fixe les conditions de transfert du demandeur d'asile ayant introduit une demande dans un autre Etat membre, dispose, au paragraphe 1, que : " Le transfert du demandeur ou d'une autre personne visée à l'article 18, paragraphe 1, point c) ou d), de l'État membre requérant vers l'État membre responsable s'effectue conformément au droit national de l'État membre requérant, après concertation entre les États membres concernés, dès qu'il est matériellement possible et, au plus tard, dans un délai de six mois à compter de l'acceptation par un autre État membre de la requête aux fins de prise en charge ou de reprise en charge de la personne concernée ou de la décision définitive sur le recours ou la révision lorsque l'effet suspensif est accordé conformément à l'article 27, paragraphe 3 ". Aux termes du paragraphe 2 de cet article : " Si le transfert n'est pas exécuté dans le délai de six mois, l'État membre responsable est libéré de son obligation de prendre en charge ou de reprendre en charge la personne concernée et la responsabilité est alors transférée à l'État membre requérant. Ce délai peut être porté à un an au maximum s'il n'a pas pu être procédé au transfert en raison d'un emprisonnement de la personne concernée ou à dix-huit mois au maximum si la personne concernée prend la fuite ". La notion de fuite doit s'entendre comme visant le cas où un ressortissant étranger se serait soustrait de façon intentionnelle et systématique au contrôle de l'autorité administrative en vue de faire obstacle à une mesure d'éloignement le concernant. Dans l'hypothèse d'un départ contrôlé dont l'Etat responsable du transfert assure l'organisation matérielle, en prenant en charge le titre de transport permettant de rejoindre l'Etat responsable de l'examen de la demande d'asile depuis le territoire français ainsi que, le cas échéant, le pré-acheminement du lieu de résidence du demandeur jusqu'à l'embarquement vers son lieu de destination, le demandeur d'asile qui se soustrait délibérément à l'exécution de son transfert ainsi organisé doit être regardé comme en fuite, au sens de ces dispositions. <br/>
<br/>
              5. Il ressort des pièces des dossiers soumis au juge des référés que, par procès-verbal du 2 novembre 2018, la direction centrale de la police aux frontières a constaté que Mme E... épouse D..., M. C... et leurs enfants ne s'étaient pas présentés à l'aéroport de Roissy pour leur départ prévu le 30 octobre 2018, alors qu'une convocation leur avait été régulièrement notifiée et que des billets de train leur avaient été proposés pour se rendre à l'aéroport le 29 octobre 2018. S'il ressort également des pièces de ces dossiers qu'il n'était pas contesté que les requérants s'étaient jusqu'alors présentés à l'ensemble des autres convocations administratives et qu'ils ont, postérieurement à cette date, signalé à la préfecture que leur fille se trouvait dans l'incapacité de prendre l'avion en raison d'une pathologie grave, ainsi qu'en attestait un certificat médical établi le 7 novembre 2018, il n'en ressort pas qu'ils auraient avisé les autorités de leur absence le 30 octobre 2018, ni qu'ils justifieraient ne pas avoir pu les en aviser en temps utile. Dès lors, c'est sans erreur de droit ni dénaturation des pièces des dossiers qui lui étaient soumis que le juge des référés du tribunal administratif de Strasbourg, qui a porté une appréciation souveraine sur le caractère sérieux des moyens invoqués devant lui, a estimé que celui tiré de ce que les requérants n'étaient pas en situation de fuite n'était pas de nature à créer un doute sérieux sur la légalité des décisions dont ils demandaient la suspension.<br/>
<br/>
              6. Il résulte de tout ce qui précède que Mme E... épouse D... et M. C... ne sont pas fondés à demander l'annulation des ordonnances qu'ils attaquent. Leurs pourvois doivent être rejetés, y compris leurs conclusions présentées au titre de l'article L.761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : Les pourvois de Mme E... épouse D... et M. C... sont rejetés.<br/>
Article 2 : La présente décision sera notifiée à Mme A... E... épouse D..., à M. B... C..., au préfet du Bas-Rhin et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
