<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000025284597</ID>
<ANCIEN_ID>JG_L_2012_02_000000339388</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/25/28/45/CETATEXT000025284597.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème et 3ème sous-sections réunies, 01/02/2012, 339388, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2012-02-01</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>339388</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème et 3ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>M. Christian Vigouroux</PRESIDENT>
<AVOCATS>SCP ODENT, POULET</AVOCATS>
<RAPPORTEUR>M. Maxime Boutron</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Nathalie Escaut</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2012:339388.20120201</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 10 mai et 9 août 2010 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. Tomas A, demeurant ... ; M. A demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 07NT03601-07NT03603 du 4 mars 2010 par lequel la cour administrative d'appel de Nantes a rejeté ses requêtes tendant, d'une part, à l'annulation des jugements n° 041622 et 054206 du 4 octobre 2007 du tribunal administratif de Nantes rejetant ses demandes tendant à la décharge des cotisations supplémentaires d'impôt sur le revenu et de contributions sociales auxquelles il a été assujetti au titre des années 1998, 1999 et 2000, ainsi que des pénalités dont elles ont été assorties, d'autre part, à ce que soit prononcée la décharge demandée ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales et son premier protocole additionnel ;<br/>
<br/>
              Vu le code général des impôts et le livre des procédures fiscales ;<br/>
<br/>
              Vu la loi n° 2008-776 du 4 août 2008 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Maxime Boutron, Auditeur,  <br/>
<br/>
              - les observations de la SCP Odent, Poulet, avocat de M. A, <br/>
<br/>
              - les conclusions de Mme Nathalie Escaut, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Odent, Poulet, avocat de M. A ; <br/>
<br/>
<br/>
<br/>
<br/>
              Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que la SARL LOGMO dont M. A est le gérant de fait et le salarié, a fait l'objet d'une vérification de comptabilité à l'issue de laquelle l'administration a établi des cotisations supplémentaires d'impôt sur les sociétés ; qu'ayant constaté lors de l'examen contradictoire de la situation fiscale personnelle de M. A, au titre des années 1998, 1999 et 2000, que les recettes de la société avaient été encaissées pendant cette période sur un compte ouvert au nom du requérant à la Société Générale, elle a regardé ces sommes comme des revenus distribués imposables entre ses mains dans la catégorie des revenus de capitaux mobiliers en application du 1° du 1 de l'article 109 du code général des impôts ; que M. A se pourvoit en cassation contre l'arrêt du 4 mars 2010 de la cour administrative d'appel de Nantes qui a rejeté ses requêtes tendant à l'annulation des jugements du 4 octobre 2007 du tribunal administratif de Nantes rejetant ses demandes tendant à la décharge des cotisations supplémentaires d'impôt sur le revenu et de contributions sociales auxquelles il a été assujetti au titre des années 1998, 1999 et 2000 ainsi que des pénalités dont elles ont été assorties ; <br/>
<br/>
              Considérant, en premier lieu, que les moyens contestant la régularité de la procédure d'imposition suivie à l'encontre d'une société soumise au régime d'imposition des sociétés de capitaux sont inopérants au regard des impositions mises à la charge de l'un de ses associés ou gérants ; que, par suite, en jugeant que les irrégularités qui auraient entaché la procédure d'établissement des impositions supplémentaires auxquelles a été assujettie la SARL Logmo au titre des années 1998, 1999 et 2000 étaient sans incidence sur l'impôt sur le revenu mis à la charge de M. A, la cour n'a pas commis d'erreur de droit ;<br/>
<br/>
              Considérant, en deuxième lieu, qu'aux termes de l'article 1er du premier protocole additionnel à la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales : " Toute personne physique ou morale a droit au respect de ses biens. Nul ne peut être privé de sa propriété que pour cause d'utilité publique et dans les conditions prévues par la loi et les principes généraux du droit international. / Les dispositions précédentes ne portent pas atteinte au droit que possèdent les Etats de mettre en vigueur les lois qu'ils jugent nécessaires pour réglementer l'usage des biens conformément à l'intérêt général ou pour assurer le paiement des impôts ou d'autres contributions ou amendes " ; qu'il résulte des termes mêmes de ces stipulations que le droit au respect de ses biens reconnu à toute personne physique ou morale suppose la propriété d'un bien qu'elles ont pour objet de protéger et à laquelle il aurait été porté atteinte ; qu'à défaut de créance certaine, l'espérance légitime d'obtenir la restitution d'une somme d'argent doit être regardée comme un bien au sens de ces stipulations ; <br/>
<br/>
              Considérant qu'aux termes de l'article L. 16 B du livre des procédures fiscales, dans sa rédaction alors applicable : " I. Lorsque l'autorité judiciaire, saisie par l'administration fiscale, estime qu'il existe des présomptions qu'un contribuable se soustrait à l'établissement ou au paiement des impôts sur le revenu ou sur les bénéfices ou de la taxe sur la valeur ajoutée (...), elle peut (...), autoriser les agents de l'administration des impôts, ayant au moins le grade d'inspecteur et habilités à cet effet par le directeur général des impôts, à rechercher la preuve de ces agissements, en effectuant des visites en tous lieux, même privés, où les pièces et documents s'y rapportant sont susceptibles d'être détenus et procéder à leur saisie. II. Chaque visite doit être autorisée par une ordonnance du juge des libertés et de la détention du tribunal de grande instance dans le ressort duquel sont situés les lieux à visiter. (...) L'ordonnance mentionnée au premier alinéa n'est susceptible que d'un pourvoi en cassation selon les règles prévues par le code de procédure pénale ; ce pourvoi n'est pas suspensif. Les délais de pourvoi courent à compter de la notification ou de la signification de l'ordonnance. (...) " ;<br/>
<br/>
              Considérant qu'à la suite de l'arrêt Ravon et autres c/France (n°18497/03) du 21 février 2008 par lequel la Cour européenne des droits de l'homme a jugé que les voies de recours ouvertes aux contribuables pour contester la régularité des visites et saisies opérées sur le fondement de l'article L. 16 B du livre des procédures fiscales ne garantissaient pas l'accès à un procès équitable au sens de l'article 6 paragraphe 1 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales, l'article 164 de la loi du 4 août 2008 de modernisation de l'économie a prévu, pour les opérations prévues à l'article L. 16 B pour lesquelles le procès-verbal ou l'inventaire avait été remis ou réceptionné antérieurement à l'entrée en vigueur de cette loi, une procédure d'appel devant le premier président de la cour d'appel contre l'ordonnance autorisant la visite et un recours devant ce même juge contre le déroulement des opérations de visite et de saisie, les ordonnances rendues par ce dernier étant susceptibles d'un pourvoi en cassation ; que le d) du 1 du IV du même article 164 dispose que cet appel et ce recours sont ouverts pour les procédures de visite et de saisie ayant permis, comme en l'espèce, à l'administration d'obtenir des éléments à partir desquels des impositions faisant l'objet d'un recours contentieux ont été établies ; que le 3 du IV de ce même article fait obligation à l'administration d'informer les personnes visées par l'ordonnance ou par les opérations de visite et de saisie de l'existence de ces voies de recours et du délai de deux mois ouverts à compter de la réception de cette information pour, le cas échéant, faire appel contre l'ordonnance ou former un recours contre le déroulement des opérations de visite ou de saisie, cet appel et ce recours étant exclusifs de toute appréciation par le juge du fond de la régularité du déroulement de ces opérations ;<br/>
<br/>
              Considérant que, ce faisant, le législateur s'est contenté de donner une nouvelle rédaction à l'article L. 16 B conforme aux exigences de la convention, et a pu instituer à titre transitoire la possibilité de bénéficier rétroactivement de ces nouvelles voies de recours contre l'ordonnance autorisant les opérations de visite et de saisie ainsi que contre le déroulement de telles opérations antérieures à l'entrée en vigueur de l'article 164 de cette loi, sans priver les contribuables d'aucune espérance légitime, et par suite sans porter atteinte à un bien au sens des stipulations de l'article 1er du premier protocole additionnel à la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales  ;<br/>
<br/>
              Considérant que, par suite, et dès lors qu'il ressort des pièces du dossier soumis aux juges du fond qu'à la suite de la mise en conformité du droit interne avec le droit conventionnel, M. A a été mis à même d'exercer les voies de droit nouvelles offertes par cette loi et de saisir le premier président de la cour d'appel d'un recours contre l'ordonnance autorisant la visite ou contre les opérations de saisies effectuées lors de la visite domiciliaire, la cour a suffisamment motivé son arrêt et n'a pas commis d'erreur de droit en jugeant qu'il n'avait pas été privé d'une espérance légitime par l'effet des dispositions de l'article 164 de la loi du 4 août 2008 ;<br/>
<br/>
              Considérant, en troisième lieu, qu'en dehors des cas et conditions où il est saisi sur le fondement de l'article 61-1 de la Constitution, il n'appartient pas au Conseil d'Etat, statuant au contentieux, de se prononcer sur un moyen tiré de la non conformité de la loi à une norme de valeur constitutionnelle ; que, par suite, les moyens tirés de ce que le caractère rétroactif de la loi du 4 août 2008 de modernisation de l'économie porterait atteinte au principe de séparation des pouvoirs garanti par l'article 16 de la Déclaration des droits de l'homme et du citoyen et au principe d'égalité de tous les citoyens devant la loi protégé par son article 6 ne peuvent qu'être écartés ; que le moyen tiré de ce que ces dispositions porteraient atteinte au droit à un procès équitable et au principe d'égalité des armes est sans incidence sur le bien-fondé de l'arrêt attaqué ; 	<br/>
<br/>
              Considérant, en quatrième lieu, qu'aux termes de l'article 109 du code général des impôts : " 1. Sont considérés comme revenus distribués : 1° Tous les bénéfices ou produits qui ne sont pas mis en réserve ou incorporés au capital ; (...) " ; et qu'aux termes de l'article 110 du même code : " Pour l'application du 1° du 1 de l'article 109, les bénéfices s'entendent de ceux qui ont été retenus pour l'assiette de l'impôt sur les sociétés (...) " ;<br/>
<br/>
              Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que M. A n'a pas contesté que les recettes réalisées par la société, laquelle n'avait déclaré aucun bénéfice, avaient été encaissées sur un compte bancaire dont il était titulaire à la Société Générale en 1998, 1999 et 2000 et s'est borné à soutenir que l'administration aurait dû procéder à un examen critique de ses comptes ; que, par suite, la cour qui a relevé ces faits et les a souverainement appréciés n'a pas méconnu les règles relatives à la charge de la preuve en en déduisant que l'administration devait être regardée comme apportant la preuve qui lui incombait que M. A avait appréhendé les sommes distribuées par la SARL LOGMO ;<br/>
<br/>
              Considérant qu'il résulte de ce qui précède que le pourvoi de M. A doit être rejeté, y compris ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
<br/>
<br/>		D E C I D E :<br/>
              		--------------<br/>
<br/>
Article 1er : Le pourvoi de M. A est rejeté.<br/>
<br/>
Article 2 : La présente décision sera notifiée à M. Tomas A et à la ministre du budget, des comptes publics et de la réforme de l'Etat, porte-parole du Gouvernement.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
