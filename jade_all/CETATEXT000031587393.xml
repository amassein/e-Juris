<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000031587393</ID>
<ANCIEN_ID>JG_L_2015_12_000000388926</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/31/58/73/CETATEXT000031587393.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème SSJS, 07/12/2015, 388926</TITRE>
<DATE_DEC>2015-12-07</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>388926</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème SSJS</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Lionel Collet</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Laurence Marion</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2015:388926.20151207</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A...B...a demandé au tribunal administratif de Montreuil d'annuler les décisions du ministre de l'intérieur retirant respectivement un et trois points de son permis de conduire à la suite d'infractions commises les 5 mars 2011 et 11 juillet 2012, ainsi que la décision du même ministre constatant la perte de validité de son permis de conduire pour solde de points nul et lui enjoignant de le restituer. Par un jugement n° 1402759 du 22 janvier 2015, le tribunal administratif de Montreuil a annulé la décision du ministre de l'intérieur portant retrait de trois points à la suite de l'infraction commise le 11 juillet 2012 et rejeté le surplus des conclusions du requérant.<br/>
<br/>
              Par un pourvoi enregistré le 25 mars 2015 au secrétariat du contentieux du Conseil d'Etat, le ministre de l'intérieur demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement en tant qu'il annule sa décision portant retrait de trois points à la suite de l'infraction commise le 11 juillet 2012 ;<br/>
<br/>
              2°) réglant l'affaire au fond, de rejeter les conclusions de M. B... dirigées contre cette décision.<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu les pièces d'où il résulte que le pourvoi a été communiqué à M. B..., qui n'a pas produit de mémoire ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Lionel Collet, conseiller d'Etat,  <br/>
<br/>
              - les conclusions de Mme Laurence Marion, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que M. B... a demandé au tribunal administratif de Montreuil d'annuler les décisions du ministre de l'intérieur retirant des points de son permis de conduire à la suite d'infractions commises les 5 mars 2011 et 11 juillet 2012, ainsi que la décision du même ministre constatant la perte de validité de son permis de conduire pour solde de points nul ; que,  par un jugement du 22 janvier 2015, le tribunal administratif a rejeté comme tardives les conclusions dirigées contre la décision constatant la perte de validité du permis et comme dépourvues d'objet et, par suite, irrecevables les décisions dirigées contre la décision retirant un point à la suite de l'infraction du 5 mars 2011, ce point ayant été ultérieurement rétabli ; que le tribunal a, en revanche, annulé la décision retirant trois points à la suite de l'infraction du 11 juillet 2012 et enjoint à l'administration de rétablir ces points ; que le ministre de l'intérieur se pourvoit en cassation contre le jugement en tant qu'il prononce cette annulation et cette injonction ; <br/>
<br/>
              2. Considérant que des conclusions tendant à l'annulation d'une décision du ministre de l'intérieur portant retrait de points d'un permis de conduire sont dépourvues d'objet si la décision par laquelle ce ministre a constaté la perte de validité de ce permis pour solde de points nul est devenue définitive ; <br/>
<br/>
              3. Considérant que, dès lors qu'il jugeait que la décision du ministre de l'intérieur constatant la perte de validité du permis de conduire de M. B...avait été régulièrement notifiée à l'intéressé le 22 février 2013, le tribunal administratif devait en déduire non seulement, comme il l'a fait, que la requête de l'intéressé, présentée le 31 mars 2014, était tardive en tant qu'elle tendait à l'annulation de cette décision mais aussi que cette requête était dépourvue d'objet et, par suite, irrecevable, en tant qu'elle tendait à l'annulation des décisions retirant des points de ce permis ; qu'en admettant la recevabilité des conclusions dirigées contre la décision de retrait de points consécutives à l'infraction du 11 juillet 2012 le tribunal a commis une erreur de droit ; que son jugement doit être annulé en tant qu'il annule cette décision et prononce une injonction ; <br/>
<br/>
              4. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application de l'article L. 821-2 du code de justice administrative dans la mesure de la cassation prononcée ;<br/>
<br/>
              5. Considérant qu'il résulte du jugement du 22 janvier 2015 du tribunal administratif de Montreuil, devenu définitif sur ce point, que la décision constatant la perte de validité du permis de conduire de M. B...était devenue définitive à la date à laquelle l'intéressé a saisi le tribunal administratif ; que, par suite, les conclusions de cette requête tendant à l'annulation de la décision retirant trois points de ce permis à la suite de l'infraction du 11 juillet 2012 étaient, dès leur introduction, dépourvues d'objet et, par suite irrecevables ; qu'elles doivent dès lors être rejetées, ainsi que les conclusions tendant à ce qu'il soit enjoint au ministre de l'intérieur de rétablir les points retirés ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du 22 janvier 2015 du tribunal administratif de Montreuil est annulé en tant qu'il annule la décision du ministre de l'intérieur retirant trois points du permis de conduire de M. B...à la suite de l'infraction du 11 juillet 2012 et enjoint au ministre de rétablir ces points.<br/>
<br/>
Article 2 : Les conclusions présentées par M. B...devant le tribunal administratif de Montreuil, tendant à l'annulation de la décision du ministre de l'intérieur retirant trois points de son permis de conduire à la suite de l'infraction du 11 juillet 2012 et à ce qu'il soit enjoint au ministre de rétablir ces points sont rejetées. <br/>
<br/>
Article 3 : La présente décision sera notifiée au ministre de l'intérieur et à M. A... B....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">49-04-01-04-025 POLICE. POLICE GÉNÉRALE. CIRCULATION ET STATIONNEMENT. PERMIS DE CONDUIRE. - CONCLUSIONS TENDANT À L'ANNULATION D'UNE DÉCISION PORTANT RETRAIT DE POINTS - NON-LIEU LORSQUE DEVIENT DÉFINITIVE LA DÉCISION CONSTATANT LA PERTE DE VALIDITÉ DU PERMIS POUR SOLDE DE POINTS NUL.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-05-05-02 PROCÉDURE. INCIDENTS. NON-LIEU. EXISTENCE. - CONCLUSIONS TENDANT À L'ANNULATION D'UNE DÉCISION PORTANT RETRAIT DE POINTS, LORSQUE DEVIENT DÉFINITIVE LA DÉCISION CONSTATANT LA PERTE DE VALIDITÉ DU PERMIS POUR SOLDE DE POINTS NUL.
</SCT>
<ANA ID="9A"> 49-04-01-04-025 Des conclusions tendant à l'annulation d'une décision du ministre de l'intérieur portant retrait de points d'un permis de conduire sont dépourvues d'objet si la décision par laquelle ce ministre a constaté la perte de validité de ce permis pour solde de points nul est devenue définitive.</ANA>
<ANA ID="9B"> 54-05-05-02 Des conclusions tendant à l'annulation d'une décision du ministre de l'intérieur portant retrait de points d'un permis de conduire sont dépourvues d'objet si la décision par laquelle ce ministre a constaté la perte de validité de ce permis pour solde de points nul est devenue définitive.</ANA>
</SOMMAIRE>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
