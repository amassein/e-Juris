<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000039198225</ID>
<ANCIEN_ID>JG_L_2019_10_000000423749</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/39/19/82/CETATEXT000039198225.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème - 7ème chambres réunies, 09/10/2019, 423749, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-10-09</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>423749</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème - 7ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP SPINOSI, SUREAU</AVOCATS>
<RAPPORTEUR>Mme Sophie-Caroline de Margerie</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Sophie Roussel</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:423749.20191009</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et un mémoire complémentaire, enregistrés les 30 août et 30 novembre 2018 au secrétariat du contentieux du Conseil d'Etat, le Comité inter-mouvements auprès des évacués (CIMADE), l'Association pour la reconnaissance des droits des personnes homosexuelles et transsexuelles à l'immigration et au séjour (ARDHIS), la Fédération des associations de solidarité avec tou-te-s les immigré-e-s (FASTI), le Groupe d'information et de soutien des immigré.e.s (GISTI) et la Ligue des droits de l'homme (LDH) demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler le décret n° 2018-528 du 28 juin 2018 pris pour l'application de l'article 1er de la loi n° 2018-187 du 20 mars 2018 permettant une bonne application du régime d'asile européen ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat le versement d'une somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le règlement (UE) 604/2013 du Parlement européen et du Conseil du 26 juin 2013 ;<br/>
              - la directive n° 2013/33/UE du Parlement européen et du Conseil du 26 juin 2013;<br/>
              - la loi n° 2018-187 du 20 mars 2018 ;<br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Sophie-Caroline de Margerie, conseiller d'Etat, <br/>
<br/>
              - les conclusions de Mme Sophie Roussel, rapporteur public,<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Spinosi, Sureau, avocat de la Cimade et autres ; <br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1.	Le décret n° 2018-528 du 28 juin 2018 a été pris pour l'application de l'article 1er de la loi n° 2018-187 du 20 mars 2018 permettant une bonne application du régime d'asile européen. Ce décret modifie des dispositions de la section 2 du chapitre III du titre V du livre V du code de l'entrée et du séjour des étrangers et du droit d'asile relatives au droit des étrangers retenus. Il complète l'article R. 553-12 pour prévoir que les étrangers faisant l'objet d'une mesure de rétention administrative sont examinés par un médecin de l'unité médicale du centre de rétention administrative s'ils en font la demande. Il modifie en outre l'article R. 553-13 du même code pour permettre à l'étranger ou le demandeur d'asile qui fait l'objet d'une procédure de transfert et qui est placé en rétention administrative en application du II de l'article L. 551-1, " indépendamment de l'examen de son état de vulnérabilité par l'autorité administrative lors de son placement en rétention, [de ] faire l'objet, à sa demande, d'une évaluation de son état de vulnérabilité par des agents de l'Office français de l'immigration et de l'intégration ". Le Comité inter-mouvements auprès des évacués (CIMADE), l'Association pour la reconnaissance des droits des personnes homosexuelles et transsexuelles à l'immigration et au séjour (ARDHIS), la Fédération des associations de solidarité avec tou-te-s les immigré-e-s (FASTI), le Groupe d'information et de soutien des immigré.e.s (GISTI) et la Ligue des droits de l'homme (LDH) demandent l'annulation pour excès de pouvoir de ce décret. <br/>
<br/>
              Sur la légalité externe du décret attaqué : <br/>
<br/>
              2.	En réponse au moyen des associations requérantes tiré de ce que le décret attaqué ne serait conforme ni à la version soumise au Conseil d'Etat ni à celle adoptée par lui, le ministre de l'intérieur a versé au dossier le texte du projet adopté par le Conseil d'Etat, qui a été communiqué aux requérantes. Il résulte de ce texte que la rédaction du décret ne diffère pas de celle adoptée par le Conseil d'Etat. Le moyen doit, par suite, être écarté.<br/>
<br/>
              Sur la légalité interne du décret attaqué :<br/>
<br/>
              3.	En premier lieu, la compatibilité d'une disposition législative avec les stipulations d'un traité international ou d'une règle du droit de l'Union européenne ne peut être utilement invoquée à l'appui de conclusions dirigées contre un acte réglementaire que si ce dernier a été pris pour son application ou si elle en constitue la base légale.<br/>
<br/>
              4.	Ainsi qu'il a été dit au point 1, les dispositions de l'article R. 553-13 du code de l'entrée et du séjour des étrangers et du droit d'asile résultant du décret attaqué sont relatives au droit de l'étranger ayant fait l'objet d'une mesure de rétention de faire procéder à l'évaluation de son état de vulnérabilité. Elles ont été prises sur le fondement de l'article L. 553-6 de ce code ayant pour objet les " actions d'accueil, d'information et de soutien " dont bénéficient " les étrangers maintenus en rétention " et qui renvoie à un décret en Conseil d'Etat le soin de préciser " les modalités de prise en compte de la vulnérabilité et, le cas échéant, des besoins particuliers des demandeurs d'asile ou des étrangers faisant l'objet d'une requête aux fins de prise en charge ou de reprise en charge (...) ". Ainsi, elles n'ont été prises pour l'application ni de l'article L. 551-1 de ce code relatif aux conditions du placement en rétention de l'étranger, ni de l'article L. 556-1 du même code relatif à la demande d'asile de l'étranger placé ou maintenu en rétention, lesquels n'en constituent pas la base légale. Il suit de là que les associations requérantes ne peuvent utilement invoquer, par la voie de l'exception, l'incompatibilité des dispositions de ces derniers articles avec les dispositions du règlement (UE) 604/2013 du Parlement européen et du Conseil du 26 juin 2013 et les objectifs de la directive 2013/33/UE du Parlement européen et du Conseil du 26 juin 2013 établissant des normes pour l'accueil des personnes demandant la protection internationale. Par ailleurs, l'incompatibilité alléguée des dispositions de l'article L. 553-6 du même code avec ces textes européens n'est assortie d'aucune précision permettant d'en apprécier le bien-fondé.<br/>
<br/>
              5.	En second lieu, selon l'article L. 723-3 du code de l'entrée et du séjour des étrangers et du droit d'asile : " Pendant toute la durée de la procédure d'examen de la demande, l'office peut définir les modalités particulières d'examen qu'il estime nécessaires pour l'exercice des droits d'un demandeur en raison de sa situation particulière ou de sa vulnérabilité. / Pour l'application du premier alinéa du présent article, l'office tient compte des informations sur la vulnérabilité qui lui sont transmises en application de l'article L. 744-6 et des éléments de vulnérabilité dont il peut seul avoir connaissance au vu de la demande ou des déclarations de l'intéressé. " L'article L. 744-6 du même code dispose que " A la suite de la présentation d'une demande d'asile, l'Office français de l'immigration et de l'intégration est chargé de procéder, dans un délai raisonnable et après un entretien personnel avec le demandeur d'asile, à une évaluation de la vulnérabilité de ce dernier afin de déterminer, le cas échéant, ses besoins particuliers en matière d'accueil. "<br/>
<br/>
              6.	D'une part, contrairement à ce qui est soutenu, il résulte de ces dispositions que la modification apportée par le décret attaqué à l'article R. 553-13 du code de l'entrée et du séjour des étrangers et du droit d'asile, a précisément pour objet de mettre en oeuvre l'obligation, fixée à l'article L. 744-6 du même code, d'évaluation de la vulnérabilité du demandeur d'asile en vue d'adapter, le cas échéant, ses conditions matérielles de rétention, dont ce décret fait application sans le méconnaître. En précisant que cette évaluation se fait " indépendamment de l'examen de [son] état de vulnérabilité par l'autorité administrative lors de son placement en rétention ", l'article R. 553-13 se borne à rappeler l'exigence posée par l'article L. 551-1 du même code, dans sa rédaction issue de la loi du 20 mars 2018, selon laquelle l'autorité administrative peut placer en rétention administrative l'étranger qui fait l'objet d'une décision de transfert après l'intervention de la décision de transfert " sur la base d'une évaluation individuelle prenant en compte l'état de vulnérabilité de l'intéressé ".<br/>
<br/>
              7.	D'autre part, si l'Office français de protection des réfugiés et apatrides (OFPRA), qui, d'ailleurs, n'a pas vocation à intervenir à l'égard d'un demandeur d'asile faisant l'objet d'une procédure de transfert, peut définir les modalités particulières d'examen de la demande d'asile nécessaires à l'exercice des droits d'un demandeur en raison de sa situation particulière ou de sa vulnérabilité, dont il a été informé par l'Office français de l'immigration et de l'intégration  en application de l'article L. 744-6 précité, ou dont il a lui-même pris connaissance, l'article L. 723-3 précité ne confie aucune compétence à l'OFPRA pour se prononcer lui-même sur l'état de vulnérabilité d'un demandeur d'asile en vue d'adapter, le cas échéant, ses conditions de rétention. Par suite, contrairement à ce qui est soutenu, la disposition réglementaire attaquée ne saurait, en tout état de cause, être regardée comme méconnaissant les dispositions de l'article L. 723-3 du code de l'entrée et du séjour des étrangers et du droit d'asile.<br/>
<br/>
              8.	Il résulte de tout ce qui précède que les requérants ne sont pas fondés à demander l'annulation du décret qu'ils attaquent. Leurs conclusions présentées au titre de l'article L. 761-1 du code de justice administrative ne peuvent, en conséquence, qu'être rejetées.<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : La requête du Comité inter-mouvements auprès des évacués (CIMADE), de l'Association pour la reconnaissance des droits des personnes homosexuelles et transsexuelles à l'immigration et au séjour (ARDHIS), de la Fédération des associations de solidarité avec tou-te-s les immigré-e-s (FASTI), du Groupe d'information et de soutien des immigré.e.s (GISTI) et de la Ligue des droits de l'homme (LDH) est rejetée.<br/>
<br/>
Article 2 : La présente décision sera notifiée au Comité inter-mouvements auprès des évacués (CIMADE), premier requérant dénommé, au Premier ministre et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
