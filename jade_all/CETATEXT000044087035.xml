<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000044087035</ID>
<ANCIEN_ID>JG_L_2021_08_000000455776</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/44/08/70/CETATEXT000044087035.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 30/08/2021, 455776, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-08-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>455776</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2021:455776.20210830</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête, enregistrée le 21 août 2021 au secrétariat du contentieux du Conseil d'Etat, l'association Ligue de défense pour les libertés politiques et naturelles demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative :<br/>
<br/>
              1°) d'enjoindre au ministre de l'éducation nationale d'abroger l'instruction visant l'interdiction pour les élèves non vaccinés d'aller à l'école s'ils sont cas contact ; <br/>
<br/>
              2°) d'enjoindre au ministre de l'éducation nationale de prendre toutes les mesures nécessaires pour mettre fin aux atteintes graves et manifestement illégales à plusieurs libertés fondamentales ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
              Elle soutient que :<br/>
              - le Conseil d'Etat est compétent en premier et dernier ressort pour connaître de sa requête ;<br/>
              - elle justifie d'un intérêt à agir en ce qu'elle a pour objet statutaire la défense du principe de proportionnalité contre les mesures prises par le gouvernement pour lutter contre l'épidémie de covid-19 ; <br/>
              - la condition d'urgence est satisfaite dès lors que, d'une part, la décision attaquée s'appliquera à la rentrée scolaire pour une durée indéterminée et, d'autre part, elle porte une atteinte grave et manifestement illégale à plusieurs libertés fondamentales ; <br/>
              - il est porté une atteinte grave et manifestement illégale au droit de mener une vie familiale normale, à la liberté de l'instruction, au droit au respect de la vie privée, au respect de la dignité de la personne humaine et à l'obligation de recueillir le consentement libre et éclairé du patient pour lui prodiguer des soins médicaux ; <br/>
              - la décision attaquée méconnaît ces libertés fondamentales dès lors que, en premier lieu, elle crée une discrimination sur un motif sanitaire entre les élèves vaccinés et ceux qui ne le sont pas, en deuxième lieu, cette discrimination n'est prévue par aucun texte législatif ou réglementaire, en troisième lieu, le seul fait d'obtenir un test négatif devrait suffire à établir le fait qu'un élève cas-contact n'a pas la maladie et ne peut donc pas la transmettre, en quatrième lieu, la vaccination n'empêche pas la transmission du virus et, en dernier lieu, les personnes âgées de douze à dix-sept ans ne sont pas un public à risque.<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de la santé publique ; <br/>
              - la loi n° 2021-1040 du 5 août 2021 ; <br/>
              - le décret n° 2021-1059 du 7 août 2021 ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. (...) ". En vertu de l'article L. 522-3 du même code, le juge des référés peut, par une ordonnance motivée, rejeter une requête sans instruction ni audience notamment lorsque la condition d'urgence n'est pas remplie.<br/>
<br/>
              2. Le requérant qui saisit le juge des référés sur le fondement des dispositions de l'article L. 521-2 du code de justice administrative rappelées au point 1 doit justifier des circonstances particulières caractérisant la nécessité pour lui de bénéficier à très bref délai d'une mesure de la nature de celles qui peuvent être ordonnées sur le fondement de cet article. La circonstance qu'une atteinte à une liberté fondamentale serait avérée n'est pas de nature à caractériser, à elle seule, l'existence d'une situation d'urgence au sens de cet article.<br/>
<br/>
              3. L'association Ligue de défense pour les libertés politiques et naturelles demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, de suspendre l'exécution de la mesure édictée par le ministre de l'éducation nationale prévoyant, dans le cadre du protocole sanitaire prévu pour l'année scolaire 2021-2022, publié sur le site internet du ministère, que, dans les collèges et les lycées, les élèves contact à risque justifiant d'une vaccination complète poursuivront les cours en présentiel tandis que ceux ne justifiant pas d'une vaccination complète poursuivront pendant sept jours leurs apprentissages à distance.<br/>
<br/>
              4. En se bornant à soutenir que l'urgence à suspendre l'exécution de la disposition contestée découle de l'atteinte qu'elle porterait, à compter de la rentrée scolaire, à plusieurs libertés fondamentale, sans faire état d'aucune circonstance précise en rapport avec les intérêts qu'elle s'est donnée pour objet de défendre, la requérante ne justifie d'aucun élément de nature à caractériser l'existence, pour elle-même, d'une situation d'urgence exigeant une intervention du juge des référés dans un très bref délai, sur le fondement de l'article L. 521-2 du code de justice administrative. <br/>
<br/>
              5. Il résulte de ce que précède que la requête de l'association Ligue de défense pour les libertés politiques et naturelles, y compris ses conclusions à fin d'injonction et celles présentées au titre de l'article L. 761-1 du code de justice administrative, ne peut qu'être rejetée, selon la procédure prévue par l'article L. 522-3 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>
O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de l'association Ligue de défense pour les libertés politiques et naturelles est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à l'association Ligue de défense pour les libertés politiques et naturelles.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
