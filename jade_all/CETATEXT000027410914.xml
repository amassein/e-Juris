<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027410914</ID>
<ANCIEN_ID>JG_L_2013_05_000000349954</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/41/09/CETATEXT000027410914.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  3ème sous-section jugeant seule, 15/05/2013, 349954, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-05-15</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>349954</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 3ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP PIWNICA, MOLINIE</AVOCATS>
<RAPPORTEUR>M. Christian Fournier</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emmanuelle Cortot-Boucher</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2013:349954.20130515</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et les mémoires complémentaires, enregistrés les 6 juin 2011, 6 septembre 2011 et 6 mars 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour M. et Mme A...B..., demeurant ... ; M. et Mme B...demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 09PA00902 du 6 avril 2011 par lequel la cour administrative d'appel de Paris a rejeté l'appel qu'ils ont formé contre l'ordonnance n° 08082212 du 17 décembre 2008 par laquelle le président de la deuxième section du tribunal administratif de Paris a rejeté leur demande tendant à la décharge des cotisations supplémentaires d'impôt sur le revenu et de contributions sociales auxquelles ils ont été assujettis au titre des années 1999, 2000 et 2001, ainsi que des pénalités correspondantes ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à leur appel ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
<br/>
              Vu le code général des impôts et le livre des procédures fiscales ;<br/>
<br/>
              Vu le code des postes et des télécommunications électroniques ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Christian Fournier, Maître des Requêtes,  <br/>
<br/>
              - les conclusions de Mme Emmanuelle Cortot-Boucher, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée avant et après les conclusions à la SCP Piwnica, Molinié, avocat de M. et Mme B...;<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond qu'à l'issue de l'examen contradictoire de situation fiscale personnelle dont ils ont fait l'objet au cours de l'année 2002, M. et Mme B...ont été assujettis au titre des années 1999, 2000 et 2001 à des cotisations supplémentaires d'impôt sur le revenu assorties de pénalités pour mauvaise foi ; qu'ils ont contesté ces rappels par une réclamation du 18 avril 2005 adressée à l'administration par l'intermédiaire de leur avocat, au cabinet duquel ils avaient élu domicile au cours du contrôle ; que, par décision du 15 septembre 2006, notifiée par lettre recommandée avec demande d'avis de réception adressée au domicile des contribuables, l'administration a rejeté leur réclamation ; que, par requête enregistrée le 2 mai 2008, M. et Mme B...ont saisi le tribunal administratif de Paris de conclusions tendant à la décharge, en droits et pénalités, des suppléments d'impositions qui leur ont été assignés ; qu'ils se pourvoient en cassation contre l'arrêt du 6 avril 2011 par lequel la cour administrative d'appel de Paris a rejeté l'appel qu'ils ont interjeté de l'ordonnance du 17 décembre 2008 du président de la deuxième section du tribunal administratif de Paris rejetant leur demande comme irrecevable, faute d'avoir été présentée dans le délai de recours prévu par l'article R*199-1 du livre des procédures fiscales ;<br/>
<br/>
              2. Considérant, en premier lieu, qu'aux termes de l'article R. 190-1 du livre des procédures fiscales, dans sa rédaction applicable au litige : " Le contribuable qui désire contester tout ou partie d'un impôt qui le concerne doit d'abord adresser une réclamation au service territorial (...) de l'administration des impôts (...) dont dépend le lieu de l'imposition " ; qu'aux termes de l'article R. 198-10 du même livre : " (...) En cas de rejet total ou partiel de la réclamation, la décision doit être motivée. / Les décisions de l'administration sont notifiées dans les mêmes conditions que celles prévues pour les notifications faites au cours de la procédure devant le tribunal administratif " ; qu'aux termes de l'article R. 431-1 du code de justice administrative : " Lorsqu'une partie est représentée devant le tribunal administratif ou la cour administrative d'appel par un des mandataires mentionnés à l'article R. 431-2, les actes de procédure, à l'exception de la notification de la décision prévue aux articles R. 751-3 et suivants, ne sont accomplis qu'à l'égard de ce mandataire " ; qu'aux termes de l'article R. 751-3 du même code : " Sauf disposition contraire, les jugements, les ordonnances et arrêts sont notifiés par les soins du greffe à toutes les parties en cause, à leur domicile réel, par lettre recommandée avec demande d'avis de réception " ; qu'aux termes, enfin, du premier alinéa de l'article R. 199-1 du même livre : " L'action doit être introduite devant le tribunal compétent dans le délai de deux mois à partir du jour de la réception de l'avis par lequel l'administration notifie au contribuable la décision prise sur la réclamation (...) " ; <br/>
<br/>
              3. Considérant qu'il résulte de la combinaison de ces dispositions qu'en indiquant que les décisions par lesquelles l'administration statue sur une réclamation sont notifiées dans les mêmes conditions que celles prévues pour les notifications faites au cours de la procédure devant le tribunal administratif, l'article R. 198-10 du livre des procédures fiscales a entendu renvoyer aux dispositions du code de justice administrative qui régissent la notification des décisions clôturant l'instance ; qu'il suit de là que le délai de recours devant le tribunal administratif ne court qu'à compter du jour où la notification de la décision de l'administration statuant sur la réclamation du contribuable a été faite au contribuable lui-même, à son domicile réel, alors même que cette réclamation aurait été présentée par l'intermédiaire d'un mandataire au nombre de ceux mentionnés à l'article R. 431-2 du code de justice administrative ; que la circonstance que le contribuable aurait élu domicile au cabinet de son mandataire est sans incidence sur l'application de cette règle ; qu'enfin, dans le cas où le pli recommandé adressé au contribuable a été retourné par le service des postes avec la mention " non réclamé ", faute d'avoir été retiré dans le délai imparti, l'administration n'est pas tenue de procéder à une nouvelle notification de sa décision au mandataire du contribuable, dès lors qu'une telle règle ne trouve pas à s'appliquer à la notification des décisions clôturant l'instance devant le tribunal administratif ; que, dès lors, la cour n'a commis aucune erreur de droit au regard des principes du droit à un procès équitable et du droit à un recours juridictionnel effectif ni, en tout état de cause, méconnu les stipulations des articles 6, paragraphe 1, et 13 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales en jugeant, après avoir relevé que le pli présenté le 3 octobre 2006 au domicile des contribuables contenant la décision de rejet de leur réclamation avait été retourné à l'administration le 19 octobre suivant avec la mention " non réclamé ", que, nonobstant la circonstance qu'ils avaient élu domicile chez leur conseil, l'administration n'était pas tenue de procéder à une seconde notification de cette décision à leur mandataire ; <br/>
<br/>
              4. Considérant, en deuxième lieu, qu'il incombe à l'administration, lorsqu'elle oppose une fin de non-recevoir tirée de la tardiveté de l'action introduite devant un tribunal administratif, d'établir que le contribuable a reçu notification régulière de la décision prise sur sa réclamation ; qu'en cas de retour à l'administration du pli contenant la notification, cette preuve peut résulter soit des mentions précises, claires et concordantes portées sur l'enveloppe, soit, à défaut, d'une attestation de l'administration postale ou d'autres éléments de preuve établissant la délivrance par le préposé du service postal, conformément à la réglementation en vigueur, d'un avis d'instance prévenant le destinataire de ce que le pli était à sa disposition au bureau de poste ; que, dès lors, la cour n'a commis aucune erreur de droit en se fondant sur l'attestation postale produite par l'administration pour juger que celle-ci avait régulièrement notifié à M. et Mme B...la décision prise sur leur réclamation ; que si, par un motif surabondant, la cour a estimé que l'attestation litigieuse avait été obtenue en application de l'article L. 83 du livre des procédures fiscales, alors que le droit de communication reconnu à l'administration fiscale par les articles L. 81 à L. 96 du même livre a seulement pour objet de permettre au service de recueillir des renseignements ou de prendre connaissance de documents pour l'établissement et le contrôle de l'assiette d'un contribuable, cette circonstance est cependant sans incidence sur le bien-fondé de l'arrêt attaqué et ne justifie pas, en tout état de cause, la cassation demandée ;<br/>
<br/>
              5. Considérant, en troisième lieu, que si M. et Mme B...soutiennent que l'attestation litigieuse aurait été obtenue par l'administration en méconnaissance de l'article L. 9 du code des postes et des communications électroniques, qui fixe à un an le délai durant lequel les réclamations relatives aux envois postaux sont recevables, ce moyen est, en tout état de cause, nouveau en cassation et ne peut qu'être écarté ;<br/>
<br/>
              6. Considérant, enfin, que les requérants, qui ont reçu communication de l'attestation produite par l'administration et ont pu ainsi utilement en débattre, ne sont pas fondés à soutenir que la cour aurait porté atteinte au principe de l'égalité des armes en se fondant sur les termes de cette attestation pour juger que la notification de la décision prise sur leur réclamation était régulière et en regardant comme inopérante la circonstance tirée de ce que leur propre demande de renseignements du 5 janvier 2009 avait été rejetée par les services postaux en raison de sa tardiveté ;<br/>
<br/>
              7. Considérant qu'il résulte de tout ce qui précède que le pourvoi de M. et Mme B... doit être rejeté, y compris leurs conclusions présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : Le pourvoi de M. et Mme B...est rejeté.<br/>
Article 2 : La présente décision sera notifiée à M. et Mme A...B...et au ministre de l'économie et des finances.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
