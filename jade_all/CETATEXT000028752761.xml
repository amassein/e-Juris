<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028752761</ID>
<ANCIEN_ID>JG_L_2014_03_000000376232</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/75/27/CETATEXT000028752761.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 19/03/2014, 376232</TITRE>
<DATE_DEC>2014-03-19</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>376232</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>DELAMARRE</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2014:376232.20140319</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 11 mars 2014 au secrétariat du contentieux du Conseil d'Etat, présentée pour M. A...B..., domicilié... ; le requérant demande au juge des référés du Conseil d'Etat : <br/>
<br/>
              1°) d'annuler l'ordonnance n° 1301538 du 29 novembre 2013 par laquelle le juge des référés du tribunal administratif de Besançon, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, a rejeté sa demande tendant à ce qu'il soit enjoint au préfet du Doubs de lui délivrer une autorisation provisoire de séjour portant la mention " en vue de démarches auprès de l'OFPRA " dans un délai de soixante-douze heures à compter de l'ordonnance à intervenir, sous astreinte de 200 euros par jour de retard ;<br/>
<br/>
              2°) de faire droit à sa demande de première instance ; <br/>
<br/>
              3°) de condamner l'Etat à verser une somme de 3 000 euros à Me Delamarre en application des dispositions des articles 37 et 75-I de la loi du 10 juillet 1991 ; <br/>
<br/>
<br/>
              il soutient que : <br/>
              - la condition d'urgence est remplie, dès lors que le refus d'accorder une autorisation provisoire de séjour en vue de démarches auprès de l'Office français de protection des réfugiés et apatrides constitue une atteinte au droit d'asile ; <br/>
              - elle est entachée d'une insuffisance de motivation, dès lors que le juge des référés n'a pas indiqué le texte sur lequel il se fondait pour rejeter sa demande ;<br/>
              - elle est entachée d'une erreur de droit en ce qu'elle ajoute une condition à l'article 10 du règlement Dublin II du 18 février 2003 ;<br/>
<br/>
<br/>
<br/>
<br/>
	Vu l'ordonnance attaquée ;<br/>
<br/>
              Vu l'ordonnance du président de la section du contentieux du 19 février 2014 accordant l'aide juridictionnelle à M.B... ;<br/>
<br/>
              Vu le mémoire en défense, enregistré le 13 mars 2014, présenté par le ministre de l'intérieur qui conclut au rejet de la requête ; le ministre soutient principalement que l'urgence n'est pas constituée ; que le comportement frauduleux du requérant fait obstacle à ce que soit prise en compte la durée de son séjour en France et le délai écoulé depuis son départ d'Italie pour l'application de l'article 10 du règlement du 18 février 2003 ; que, subsidiairement, les dispositions du 4° de l'article L. 741-4 du code de l'entrée et du séjour des étrangers et du droit d'asile font obstacle à ce qu'une demande d'asile frauduleuse puisse être accueillie ; <br/>
<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, M. B...et, d'autre part, le ministre de l'intérieur ;<br/>
<br/>
              Vu le procès-verbal de l'audience publique du 14 mars 2014 à 15 heures, au cours de laquelle ont été entendus : <br/>
<br/>
              - Me Delamarre, avocat au Conseil d'Etat et à la Cour de cassation, avocat de M. B...;<br/>
              - les représentants du ministre de l'intérieur ; <br/>
              et à l'issue de laquelle le juge des référés a prolongé l'instruction jusqu'au mardi 18 mars 2014 à 18 heures ;<br/>
<br/>
              Vu, enregistré le 17 mars 2014, le mémoire de production présenté par le ministre de l'intérieur ; <br/>
              Les parties ayant été invitées, le 17 mars 2014, à présenter leurs observations sur la substitution de base légale de la décision litigieuse envisagée par le juge des référés du Conseil d'Etat ; <br/>
<br/>
              Vu, enregistré le 18 mars 2014, le mémoire présenté par le ministre de l'intérieur, qui conclut aux mêmes fins par les mêmes moyens ; il soutient, en outre, qu'à supposer que le juge des référés considère que l'ordonnance attaquée serait entachée d'une insuffisance de motivation, il y aurait lieu de procéder à une substitution de base légale en fondant la décision de réadmission de M. B...sur la combinaison des articles 5.2, 10 et 16 du règlement (CE) n° 343/2003 du 18 février 2003 ;<br/>
<br/>
              Vu les autres pièces du dossier ; <br/>
              Vu le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              Vu le règlement (CE) no 343/2003 du Conseil du 18 février 2003 ;<br/>
<br/>
              Vu la loi n° 91-647 du 10 juillet 1991 ;<br/>
	Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale " ;<br/>
<br/>
              2. Considérant qu'il résulte de l'instruction que M. A...B..., de nationalité tchadienne, est entré irrégulièrement en France le 18 septembre 2012 ; que, se présentant comme mineur, il a été pris en charge par les services de l'aide sociale à l'enfance du département du Doubs ; que, saisi par ces services, le juge des mineurs du tribunal de grande instance de Besançon a jugé le 3 juillet 2013, au vu des expertises médicales effectuées, que M. B... était majeur et qu'il n'y avait donc pas lieu de prendre à son égard une mesure de tutelle ; que, le 9 octobre 2013, l'intéressé a sollicité la reconnaissance du statut de réfugié auprès des services de la préfecture du Doubs ; qu'après avoir constaté, par la consultation du fichier Eurodac que les empreintes de M. B...avaient déjà été relevées en Italie le 24 novembre 2011, le préfet du Doubs a adressé à l'intéressé, le 17 octobre 2013, une convocation intitulée " demandeur d'asile placé en procédure Dublin II " et, ultérieurement, envoyé, le 30 décembre 2013, une demande de reprise en charge aux autorités italiennes ; que, le 28 novembre 2013, M. B...a saisi le juge des référés du tribunal administratif de Besançon d'une demande, présentée sur le fondement de l'article L. 521-2 du code de justice administrative, tendant à ce qu'il soit enjoint à l'administration préfectorale de l'admettre au séjour à titre provisoire et de transmettre sa demande à l'Office français de protection des réfugiés et apatrides ; que, par l'ordonnance attaquée, le juge des référés du tribunal administratif de Besançon a rejeté sa demande ; <br/>
<br/>
              3. Considérant que le droit constitutionnel d'asile, qui a le caractère d'une liberté fondamentale, a pour corollaire le droit de solliciter le statut de réfugié ; que, s'il implique que l'étranger qui sollicite la reconnaissance de la qualité de réfugié soit, en principe, autorisé à demeurer sur le territoire jusqu'à ce qu'il ait été statué sur sa demande, ce droit s'exerce dans les conditions définies par l'article L. 741-4 du code de l'entrée et du séjour des étrangers et du droit d'asile ; qu'en vertu du 1° de cet article, l'admission en France d'un étranger qui demande à être admis au bénéfice de l'asile peut être refusée, si l'examen de sa demande relève de la compétence d'un autre Etat en application des dispositions du règlement (CE) n° 343/2003 du 18 février 2003 établissant les critères et mécanismes de détermination de l'État membre responsable de l'examen d'une demande d'asile présentée dans l'un des Etats membres par un ressortissant d'un pays tiers  alors applicable ; qu'aux termes de l'article 3 de ce règlement  " 1. Les États membres examinent toute demande d'asile présentée par un ressortissant d'un pays tiers à l'un quelconque d'entre eux, que ce soit à la frontière ou sur le territoire de l'Etat membre concerné. La demande d'asile est examinée par un seul Etat membre, qui est celui que les critères énoncés au chapitre III désignent comme responsable (...) " ; qu'aux termes de l'article 4 de ce règlement : " 1. Le processus de détermination de l'Etat membre responsable en vertu du présent règlement est engagé dès qu'une demande d'asile est introduite pour la première fois auprès d'un Etat membre (...) " ; qu'aux termes de l'article 5 du même règlement : " 1. Les critères pour la détermination de l'Etat membre responsable qui sont établis s'appliquent dans l'ordre dans lequel ils sont présentés dans le présent chapitre./ 2. La détermination de l'Etat membre responsable en application des critères se fait sur la base de la situation qui existait au moment où le demandeur d'asile a présenté sa demande pour la première fois auprès d'un État membre " ; que les articles 6 à 10 du règlement fixent les critères permettant de déterminer l'Etat membre responsable de l'examen d'une demande d'asile ; qu'aux termes de l'article 10 : " 1. Lorsqu'il est établi (...) que le demandeur d'asile a franchi irrégulièrement, ..., la frontière d'un Etat membre dans lequel il est entré en venant d'un Etat tiers, cet Etat membre est responsable de l'examen de la demande d'asile. Cette responsabilité prend fin douze mois après la date du franchissement irrégulier de la frontière./ 2. Lorsqu'un Etat membre ne peut, ou ne peut plus, être tenu pour responsable conformément au paragraphe 1 et qu'il est établi (...) que le demandeur d'asile qui est entré irrégulièrement sur les territoires des Etats membres ou dont les circonstances de l'entrée sur ce territoire ne peuvent être établies a séjourné dans un Etat membre pendant une période continue d'au moins cinq mois avant l'introduction de sa demande, cet État membre est responsable de l'examen de la demande d'asile (...) " ; qu'aux termes de l'article 16 du même règlement : " 1. L'État membre responsable de l'examen d'une demande d'asile en vertu du présent règlement est tenu de : (...)/c) reprendre en charge, dans les conditions prévues à l'article 20, le demandeur d'asile dont la demande est en cours d'examen et qui se trouve, sans en avoir reçu la permission, sur le territoire d'un autre État membre ...e) reprendre en charge, dans les conditions prévues à l'article 20, le ressortissant d'un pays tiers dont il a rejeté la demande et qui se trouve, sans en avoir reçu la permission, sur le territoire d'un autre Etat membre./ .. " ; qu'il résulte de la combinaison de ces dispositions, que les critères prévus à l'article 10 du règlement ne sont susceptibles de s'appliquer que lorsque le ressortissant d'un pays tiers présente une demande d'asile pour la première fois depuis son entrée sur le territoire de l'un ou l'autre des Etats membres ; qu'en particulier, les dispositions de cet article ne s'appliquent pas, lorsque le ressortissant d'un pays tiers présente, fût-ce pour la première fois, une demande d'asile dans un Etat membre après avoir déposé une demande d'asile dans un autre Etat membre, que cette dernière ait été rejetée ou soit encore en cours d'instruction  ;<br/>
<br/>
              4. Considérant qu'il ressort des pièces du dossier, et notamment du numéro d'immatriculation affecté à M. B...dans le fichier Eurodac, que ce dernier avait sollicité en Italie le statut de demandeur d'asile avant son entrée en France ; que le préfet du Doubs ne pouvait, dès lors se fonder, pour prendre la décision litigieuse, laquelle résulte des mentions de la convocation adressée à l'intéressé le 17 octobre 2013, sur les dispositions précitées de l'article 10 du règlement n° 343/2003 qui n'étaient pas applicables ; qu'il en a, au surplus, méconnu la portée, en estimant que les déclarations mensongères de M. B...l'empêchait de se prévaloir, pour solliciter que sa demande d'asile soit instruite en France, tant de la durée de plus de 12 mois écoulée depuis son entrée en Italie, que de celle de plus de 5 mois de son séjour en France ; <br/>
<br/>
              5. Considérant que la demande de substitution de base légale faite par le ministre de l'intérieur sur le fondement des dispositions du 4° de l'article L. 741-4 du code de l'entrée et du séjour des étrangers et du droit d'asile relatives aux demandes d'asile frauduleuses ne saurait être accueillie, dès lors qu'il résulte des termes de l'article L. 742-6 du même code que l'étranger qui présente une telle demande bénéficie du droit de se maintenir en France jusqu'à la décision de l'OFPRA ; que la décision litigieuse trouve, en revanche, son fondement légal dans les dispositions précitées de l'article 16 du règlement 343/2003, dont il résulte que l'Italie est l'Etat responsable du traitement de la demande d'asile de M. B...; que du silence gardé par les autorités de cet Etat sur la demande de reprise en charge qui leur a été adressée est né, au demeurant, le 1er février 2014, un accord implicite pour cette reprise en charge ; que le préfet du Doubs n'a, par suite, pas commis d'erreur grave et manifeste, en refusant à M. B... tout droit au séjour sur le territoire français et en le plaçant en procédure dite Dublin II ;  <br/>
<br/>
              6. Considérant qu'il résulte de ce qui précède, et sans qu'il y ait lieu de se prononcer sur la condition d'urgence, que M. B...n'est pas fondé à se plaindre de ce que, par l'ordonnance attaquée, qui est suffisamment motivée, le juge des référés du tribunal administratif de Besançon a rejeté sa demande présentée sur le fondement de l'article L. 521-2 du code de justice administrative ; qu'il y a lieu, par suite, de rejeter sa requête, y compris ses conclusions tendant à l'application des dispositions des articles 37 et 75-1 de la loi du 10 juillet 1991 ;<br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
------------------<br/>
Article 1er : La requête de M. B...est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à M. A...B...et au ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">095-02-03 - COMPUTATION DES DÉLAIS PRÉVUS À L'ARTICLE 10 DU RÈGLEMENT DU 18 FÉVRIER 2003 - DÉCLARATIONS MENSONGÈRES DE L'ÉTRANGER.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">54-035-03-04 PROCÉDURE. PROCÉDURES INSTITUÉES PAR LA LOI DU 30 JUIN 2000. RÉFÉRÉ TENDANT AU PRONONCÉ DE MESURES NÉCESSAIRES À LA SAUVEGARDE D'UNE LIBERTÉ FONDAMENTALE (ART. L. 521-2 DU CODE DE JUSTICE ADMINISTRATIVE). POUVOIRS ET DEVOIRS DU JUGE. - SUBSTITUTION DE BASE LÉGALE - FACULTÉ POUR LE JUGE DU RÉFÉRÉ LIBERTÉ D'Y PROCÉDER DE SA PROPRE INITIATIVE - EXISTENCE [RJ1] - CONDITIONS - OBLIGATION DE METTRE LES PARTIES À MÊME DE PRÉSENTER LEURS OBSERVATIONS.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">54-07-01-05 PROCÉDURE. POUVOIRS ET DEVOIRS DU JUGE. QUESTIONS GÉNÉRALES. SUBSTITUTION DE BASE LÉGALE. - FACULTÉ POUR LE JUGE DU RÉFÉRÉ LIBERTÉ D'Y PROCÉDER DE SA PROPRE INITIATIVE - EXISTENCE [RJ1] - CONDITIONS - OBLIGATION DE METTRE LES PARTIES À MÊME DE PRÉSENTER LEURS OBSERVATIONS.
</SCT>
<ANA ID="9A"> 095-02-03 L'administration méconnaît la portée des dispositions de l'article 10 du règlement (CE) 343/2003 du Conseil du 18 février 2003 en estimant que des déclarations mensongères de l'étranger peuvent empêcher que ne courent les délais objectifs prévus par ces dispositions pour la détermination de l'Etat responsable de l'examen de la demande d'asile.</ANA>
<ANA ID="9B"> 54-035-03-04 Le juge des référés saisi sur le fondement de l'article L. 521-2 du code de justice administrative peut opérer d'office une substitution de base légale, après avoir mis à même les parties de présenter leurs observations.</ANA>
<ANA ID="9C"> 54-07-01-05 Le juge des référés saisi sur le fondement de l'article L. 521-2 du code de justice administrative peut opérer d'office une substitution de base légale, après avoir mis à même les parties de présenter leurs observations.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, Section, 3 décembre 2003, Préfet de la Seine-Maritime c/ El Bahi, n° 240267, p. 479.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
