<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000024615274</ID>
<ANCIEN_ID>JG_L_2011_09_000000338048</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/24/61/52/CETATEXT000024615274.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  6ème sous-section jugeant seule, 30/09/2011, 338048, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2011-09-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>338048</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 6ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT>Mme Christine Maugüé</PRESIDENT>
<AVOCATS>SCP WAQUET, FARGE, HAZAN</AVOCATS>
<RAPPORTEUR>M. Didier Ribes</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Mattias Guyomar</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2011:338048.20110930</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête sommaire et le mémoire complémentaire, enregistrés les 29 mars et 29 juin 2010 au secrétariat du contentieux du Conseil d'État, présentés pour la FEDERATION DEPARTEMENTALE DES CHASSEURS DE LA HAUTE SAVOIE dont le siège est situé Maison de la Chasse de la Faune et de la Nature à Villy-le-Pelloux (74350), représentée par son président ; la FEDERATION DEPARTEMENTALE DES CHASSEURS DE LA HAUTE SAVOIE demande au Conseil d'État :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir le décret du 27 janvier 2010 portant reclassement de la réserve naturelle nationale des Aiguilles rouges (Haute-Savoie), en tant qu'il prévoit une interdiction totale de la chasse dans la réserve naturelle ;<br/>
<br/>
              2°) subsidiairement d'annuler le décret dans son intégralité ;<br/>
<br/>
              3°) de mettre à la charge de l'État la somme de 5 000 euros en application de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code de l'environnement ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Didier Ribes, Maître des requêtes,  <br/>
<br/>
              - les observations de la SCP Waquet, Farge, Hazan, avocat de la FEDERATION DEPARTEMENTALE DES CHASSEURS DE LA HAUTE SAVOIE, <br/>
<br/>
              - les conclusions de M. Mattias Guyomar, rapporteur public ;<br/>
<br/>
              La parole ayant été à nouveau donnée à la SCP Waquet, Farge, Hazan, avocat de la FEDERATION DEPARTEMENTALE DES CHASSEURS DE LA HAUTE SAVOIE ;<br/>
<br/>
<br/>
<br/>Sur la légalité externe du décret :<br/>
<br/>
              Considérant, en premier lieu, qu'aux termes de l'article R. 332-1 du code de l'environnement  : " (...) après consultation du Conseil national de la protection de la nature, sur la base d'une étude scientifique attestant d'un intérêt écologique au regard des objectifs prévus aux articles L. 332-1 et L. 332-2, de l'indication des milieux à protéger et de leur superficie approximative ainsi que de la liste des sujétions envisagées, le ministre chargé de la protection de la nature saisit le préfet du projet de classement d'un territoire comme réserve naturelle nationale pour qu'il engage les consultations nécessaires (...) " ; que l'article R. 332-3 du même code précise que " le dossier soumis aux consultations et à l'enquête comprend : (...) 6° Un résumé de l'étude scientifique prévue à l'article R. 332-1 " ; qu'aucune disposition n'impose que soit mentionnée dans les visas du décret de classement l'étude scientifique prévue par l'article R. 332-1 précité ; qu'il ressort des pièces du dossier que, conformément à l'article R. 332-3 du code de l'environnement, une synthèse de cette étude scientifique figure dans le dossier soumis à enquête publique ; que les moyens tirés du défaut de visa et de l'absence d'une telle étude doivent, par suite, être écartés ; <br/>
<br/>
              Considérant, en second lieu, qu'aux termes de l'article R. 332-9 du code de l'environnement : " I. - Le projet de classement, modifié s'il y a lieu pour tenir compte des résultats de l'enquête et des consultations, est soumis à l'avis du Conseil national de la protection de la nature et des ministres chargés de l'agriculture, de la défense, du budget, de l'urbanisme, des transports, de l'industrie et des mines. / II. - Le ministre chargé de la protection de la nature doit recueillir l'accord : 1° Du ministre affectataire et du ministre chargé du domaine lorsque tout ou partie du territoire de la réserve projetée est inclus dans le domaine de l'État ; / 2° Du ministre chargé de la forêt lorsque le classement intéresse une forêt relevant du régime forestier au titre des dispositions du 1° de l'article L. 111-1 du code forestier ; / 3° Du ministre de la défense et du ministre chargé de l'aviation civile lorsque le classement entraîne des contraintes pour le survol du territoire ; / 4° Du ministre de la défense et du ministre chargé de la mer lorsque le classement intéresse les eaux territoriales. / III. - Les autorités mentionnées aux I et II du présent article doivent se prononcer dans le délai de trois mois qui suit leur saisine. Passé ce délai, les avis sont réputés favorables et les accords réputés donnés " ;  que,  contrairement à ce que soutient la FEDERATION DEPARTEMENTALE DES CHASSEURS DE LA HAUTE SAVOIE, les dispositions de l'article R. 332-9  précité n'imposent pas qu'un décret de classement précise la qualité des ministres qui ont  été consultés préalablement à son adoption ;  que le fait que le  décret attaqué du 27 janvier 2010 portant reclassement de la réserve naturelle nationale des Aiguilles rouges (Haute-Savoie), qui vise l'avis des ministres intéressés, n'indique pas expressément que les ministres chargés de l'agriculture, du budget, de l'urbanisme, des transports, de l'industrie, des mines, de l'aviation civile et de la défense ont été consultés est sans influence sur sa légalité ; qu'il ressort, au demeurant, des pièces du dossier que les consultations des ministres intéressés  prescrites par les dispositions rappelées ci-dessus ont bien été opérées ; que les moyens tirés de la méconnaissance de l'article R. 332-9 doivent, par suite, être écartés ;<br/>
<br/>
              Sur la légalité interne du décret : <br/>
<br/>
              Considérant qu'aux termes de l'article L. 332-1 du code de l'environnement : " I. - Des parties du territoire d'une ou de plusieurs communes peuvent être classées en réserve naturelle lorsque la conservation de la faune, de la flore, du sol, des eaux, des gisements de minéraux et de fossiles et, en général, du milieu naturel présente une importance particulière ou qu'il convient de les soustraire à toute intervention artificielle susceptible de les dégrader. (...) / II. - Sont prises en considération à ce titre : 1° La préservation d'espèces animales ou végétales et d'habitats en voie de disparition sur tout ou partie du territoire national ou présentant des qualités remarquables ; (...) " ; que le I de l'article L. 332-3 du même code dispose que : " l'acte de classement d'une réserve naturelle nationale peut soumettre à un régime particulier et, le cas échéant, interdire à l'intérieur de la réserve toute action susceptible de nuire au développement naturel de la faune et de la flore et, plus généralement, d'altérer le caractère de ladite réserve, notamment la chasse (... )" ; <br/>
<br/>
              Considérant que si la fédération requérante soutient que l'interdiction de la chasse dans la réserve naturelle nationale des Aiguilles rouges, prévue par l'article 8 du décret attaqué, fait obstacle à la pratique de tirs sélectifs permettant de réguler des espèces animales banales et de maintenir ainsi l'équilibre agro-sylvo-cynégétique posé à l'article L. 420-1 du code de l'environnement, un tel objectif ne figure pas au nombre de ceux énumérés par l'article L. 332-1 du même code ; que le II de l'article 4 du décret attaqué permet au demeurant au préfet de prendre les mesures nécessaires pour limiter ou réguler les populations d'animaux surabondants ou susceptibles de provoquer des déséquilibres biologiques et des dégâts préjudiciables aux milieux naturels et aux espèces de la réserve ; que les dispositions du I de l'article L. 332-3 rappelées ci-dessus autorisent l'autorité administrative à interdire les actions susceptibles de nuire au développement naturel de la faune et de la flore et, plus généralement, d'altérer le caractère de ladite réserve, et notamment la chasse ; qu'il ressort des pièces du dossier que la pratique de la chasse sur le territoire de la réserve naturelle sous la forme de tirs sélectifs visant des espèces animales banales, interdite dans cette réserve, serait  de nature à altérer la quiétude nécessaire à la conservation de plusieurs espèces animales rares et fragiles,  en contradiction avec les finalités poursuivies par le classement en réserve naturelle nationale ;  qu'ainsi, en interdisant la chasse sur le territoire de la  réserve des Aiguilles rouges, l'auteur du décret n'a ni méconnu les objectifs énoncés par les articles L. 332-1 et L. 332-3 précités ni entaché ce texte d'une erreur dans l'appréciation des conséquences d'une telle mesure d'interdiction ; <br/>
<br/>
              Considérant qu'il résulte de tout ce qui précède que la FEDERATION DEPARTEMENTALE DES CHASSEURS DE LA HAUTE SAVOIE n'est pas fondée à demander l'annulation du décret attaqué ; que les conclusions qu'elle présente au titre de l'article L. 761-1 du code de justice administrative ne peuvent, par suite, qu'être rejetées ;<br/>
<br/>
<br/>
<br/>
<br/>
              			D E C I D E :<br/>
              			--------------<br/>
<br/>
Article 1er : La requête de la FEDERATION DEPARTEMENTALE DES CHASSEURS DE LA HAUTE SAVOIE est rejetée.<br/>
<br/>
Article 2 : La présente décision sera notifiée à la FEDERATION DEPARTEMENTALE DES CHASSEURS DE LA HAUTE SAVOIE, au Premier ministre et à la ministre de l'écologie, du développement durable, des transports et du logement.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
