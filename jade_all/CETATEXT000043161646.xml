<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043161646</ID>
<ANCIEN_ID>JG_L_2021_02_000000449011</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/16/16/CETATEXT000043161646.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, , 16/02/2021, 449011, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-02-16</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>449011</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION/>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2021:449011.20210216</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par une requête et un mémoire en réplique, enregistrés les 22 janvier et 9 février 2021 au secrétariat du contentieux du Conseil d'Etat, M. A... B... demande au juge des référés du Conseil d'Etat, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, d'ordonner la suspension de l'exécution de l'article 56-2 du décret n° 2020-1310 du 29 octobre 2020 prescrivant les mesures générales nécessaires pour faire face à l'épidémie de covid-19 dans le cadre de l'état d'urgence sanitaire, dans sa rédaction issue du décret n° 2021-31 du 15 janvier 2021, en ce qu'il fait obligation à toute personne arrivant en France en provenance du Royaume-Uni de présenter un document attestant du résultat d'un examen biologique de dépistage virologique réalisé sur le territoire britannique moins de 72 heures avant l'embarquement, ne concluant pas à une contamination par le covid-19.<br/>
<br/>
<br/>
              Il soutient que :<br/>
              - la condition d'urgence est satisfaite dès lors que, en vertu de sa qualité de Français résidant de manière temporaire au Royaume-Uni, il souhaite être rapatrié en France ; <br/>
              - le décret attaqué porte une atteinte grave et manifestement illégale à la liberté de circulation, au droit au rapatriement et méconnaît le principe d'égalité ; <br/>
              - il instaure une obligation disproportionnée dès lors qu'il soumet, sans prévoir d'exemption, toute personne arrivant en France en provenance du Royaume-Uni à la présentation du résultat d'un examen biologique de dépistage virologique réalisé sur le territoire britannique moins de 72 heures avant l'embarquement ne concluant pas à une contamination par la Covid-19, alors même que la faculté de réaliser un tel examen et d'en obtenir les résultats dans les 72 heures varie selon les circonstances de temps et de lieu ;<br/>
              - il l'empêche d'effectuer un déplacement essentiel entre le Royaume-Uni et la France ;<br/>
              - il présente un caractère discriminatoire dès lors que les dispositions litigieuses ne visent que le Royaume-Uni, et ce sans justification sanitaire.<br/>
<br/>
              Par un mémoire en défense, enregistré le 8 février 2020, le ministre des solidarités et de la santé conclut au rejet de la requête. Il soutient que la condition d'urgence n'est pas satisfaite et qu'il n'est porté aucune atteinte grave et manifestement illégale aux libertés fondamentales invoquées.<br/>
<br/>
              La requête a été communiquée au Premier ministre qui n'a pas présenté d'observations. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le décret n° 2020-1262 du 16 octobre 2020 ;<br/>
              - le décret n° 2020-1310 du 29 octobre 2020 ;<br/>
              - le décret n° 2021-31 du 15 janvier 2021 ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Les parties ont été informées, sur le fondement de l'article 3 de l'ordonnance n° 2020-1402 du 18 novembre 2020 portant adaptation des règles applicables aux juridictions administratives, de ce qu'aucune audience ne se tiendrait et de ce que la clôture de l'instruction serait fixée le 15 février 2021 à 18 heures. <br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 521-2 du même code : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. (...) ". <br/>
<br/>
              Sur l'office du juge des référés :<br/>
<br/>
              2. Il résulte de la combinaison des dispositions des articles L. 511-1, L. 521-2 et L. 521-4 du code de justice administrative qu'il appartient au juge des référés, lorsqu'il est saisi sur le fondement de l'article L. 521-2 précité et qu'il constate une atteinte grave et manifestement illégale portée par une personne morale de droit public à une liberté fondamentale, de prendre les mesures qui sont de nature à faire disparaître les effets de cette atteinte. Ces mesures doivent en principe présenter un caractère provisoire, sauf lorsqu'aucune mesure de cette nature n'est susceptible de sauvegarder l'exercice effectif de la liberté fondamentale à laquelle il est porté atteinte. Le juge des référés peut, sur le fondement de l'article L. 521-2 du code de justice administrative, ordonner à l'autorité compétente de prendre, à titre provisoire, une mesure d'organisation des services placés sous son autorité lorsqu'une telle mesure est nécessaire à la sauvegarde d'une liberté fondamentale. Toutefois, le juge des référés ne peut, sur le fondement de l'article L. 521-2 précité, qu'ordonner les mesures d'urgence qui lui apparaissent de nature à sauvegarder, dans un délai de quarante-huit heures, la liberté fondamentale à laquelle il est porté une atteinte grave et manifestement illégale. Eu égard à son office, il peut également, le cas échéant, décider de déterminer dans une décision ultérieure prise à brève échéance les mesures complémentaires qui s'imposent et qui peuvent également être très rapidement mises en oeuvre. Dans tous les cas, l'intervention du juge des référés dans les conditions d'urgence particulière prévues par l'article L. 521-2 précité est subordonnée au constat que la situation litigieuse permette de prendre utilement et à très bref délai les mesures de sauvegarde nécessaires. Compte tenu du cadre temporel dans lequel se prononce le juge des référés sur le fondement de l'article L. 521-2, les mesures qu'il peut ordonner doivent s'apprécier en tenant compte des moyens dont dispose l'autorité administrative compétente et des mesures qu'elle a déjà prises.<br/>
<br/>
              Sur les circonstances et le cadre du litige :<br/>
<br/>
              3. Aux termes de l'article L. 3131-12 du code de la santé publique, issu de la loi du 23 mars 2020 d'urgence pour faire face à l'épidémie de Covid-19 : " L'état d'urgence sanitaire peut être déclaré sur tout ou partie du territoire métropolitain ainsi que du territoire des collectivités régies par les articles 73 et 74 de la Constitution et de la Nouvelle-Calédonie en cas de catastrophe sanitaire mettant en péril, par sa nature et sa gravité, la santé de la population ". L'article L. 3131-13 du même code dispose que : " L'état d'urgence sanitaire est déclaré par décret en conseil des ministres pris sur le rapport du ministre chargé de la santé. Ce décret motivé détermine la ou les circonscriptions territoriales à l'intérieur desquelles il entre en vigueur et reçoit application. Les données scientifiques disponibles sur la situation sanitaire qui ont motivé la décision sont rendues publiques. / (...) / La prorogation de l'état d'urgence sanitaire au-delà d'un mois ne peut être autorisée que par la loi, après avis du comité de scientifiques prévu à l'article L. 131-19. ". Aux termes du I de l'article L. 3131-15 du même code : " Dans les circonscriptions territoriales où l'état d'urgence sanitaire est déclaré, le Premier ministre peut, par décret réglementaire pris sur le rapport du ministre chargé de la santé, aux seules fins de garantir la santé publique : (...) 1° Réglementer ou interdire la circulation des personnes (...) ". Ce même article précise à son III que les mesures prises en application de ses dispositions " sont strictement proportionnées aux risques sanitaires encourus et appropriées aux circonstances de temps et de lieu " et " qu'il y est mis fin sans délai lorsqu'elles ne sont plus nécessaires ".<br/>
<br/>
              4. L'émergence d'un nouveau coronavirus, responsable de la maladie à coronavirus 2019 ou covid-19 et particulièrement contagieux, a été qualifiée d'urgence de santé publique de portée internationale par l'Organisation mondiale de la santé le 30 janvier 2020, puis de pandémie le 11 mars 2020. La propagation du virus sur le territoire français a conduit le ministre chargé de la santé puis le Premier ministre à prendre, à compter du 4 mars 2020, des mesures de plus en plus strictes destinées à réduire les risques de contagion. Pour faire face à l'aggravation de l'épidémie, la loi du 23 mars 2020 a créé un régime d'état d'urgence sanitaire, défini aux articles L. 3131-12 à L. 3131-20 du code de la santé publique, et a déclaré l'état d'urgence sanitaire pour une durée de deux mois à compter du 24 mars 2020. La loi du 11 mai 2020 prorogeant l'état d'urgence sanitaire et complétant ces dispositions a prorogé cet état d'urgence sanitaire jusqu'au 10 juillet 2020.<br/>
<br/>
              5. Une nouvelle progression de l'épidémie a conduit le Président de la République à prendre le 14 octobre dernier, sur le fondement des articles L. 3131-12 et L. 3131-13 du code de la santé publique, un décret déclarant l'état d'urgence sanitaire à compter du 17 octobre sur l'ensemble du territoire national. L'article 1er de la loi du 14 novembre 2020 autorisant la prorogation de l'état d'urgence sanitaire et portant diverses mesures de gestion de la crise sanitaire a prorogé l'état d'urgence sanitaire jusqu'au 16 février 2021 inclus. Le Premier ministre a pris, sur le fondement de l'article L. 3131-15 du code de la santé publique, le décret du 29 octobre 2020 prescrivant les mesures générales nécessaires pour faire face à l'épidémie de covid-19 dans le cadre de l'urgence sanitaire, modifié en dernier lieu par un décret du 5février 2021. Aux termes de l'article 56-2 de ce décret, contesté par le requérant, dans sa rédaction issue du décret du 15 janvier 2020 : " A compter du 23 décembre 2020 à zéro heure et jusqu'au 21 février 2021 inclus, toute personne arrivant en France en provenance du Royaume-Uni présente, à l'entreprise de transport, avant son embarquement : / (...) 2° Si elle est âgée de onze ans ou plus, le résultat d'un examen biologique de dépistage virologique réalisé sur le territoire britannique moins de 72 heures avant l'embarquement ne concluant pas à une contamination par le covid-19 (...) / A défaut de présentation des documents mentionnés aux 1° et 2°, l'embarquement est refusé et la personne est reconduite à l'extérieur des espaces concernés ".<br/>
<br/>
              Sur la demande en référé :<br/>
<br/>
              6. En premier lieu, il résulte de l'instruction que la situation sanitaire demeure critique en France. Ainsi, à la date du 7 février 2021, 3 337 048 cas sont confirmés positifs au virus en France, en augmentation de 19 715 dans les dernières vingt-quatre heures, et 78 965 décès liés à l'épidémie sont à déplorer, en hausse de 171 personnes dans les dernières vingt-quatre heures. Le taux d'incidence est de 210,61. Le taux d'occupation des lits en réanimation par des patients atteints de la covid-19 demeure à un niveau élevé avec une moyenne nationale de 64%, mettant sous tension l'ensemble du système de santé. Ces données, qui révèlent une dégradation de la situation sanitaire au cours de la période récente à partir d'un " plateau épidémique " déjà élevé, pourraient être aggravées par l'apparition de nouveaux variants du SARS-Cov-2, dont celui nommé VUI 202012/01 détecté au Royaume-Uni à la mi-décembre. Les données des enquêtes épidémiologiques et virologiques britanniques ont indiqué une transmission plus importante de ce variant, qui aurait progressivement remplacé les autres virus circulants, avec un potentiel estimé d'augmentation du nombre de reproduction d'au moins 0,4. Ce variant a contribué au développement de l'épidémie au Royaume-Uni, qui y a conduit à un nouveau confinement de la population pour une durée de deux mois à compter du 4 janvier 2021. L'administration fait valoir que cette situation a justifié la mesure litigieuse, dans le but de freiner le développement de l'épidémie en France, en particulier la diffusion du nouveau variant. <br/>
<br/>
              7. Il résulte par ailleurs de l'instruction que de nombreux établissements médicaux au Royaume-Uni ont mis en place des guichets piétons ou automobiles pour procéder à un test dit PCR et que des kits de test PCR à réaliser à domicile sont délivrés sur demande. Le gouvernement britannique a également publié la liste des établissements qui organisent des tests PCR, notamment dans le cadre des voyages internationaux. Par ailleurs, si le requérant fait valoir que les délais pour obtenir les résultats des tests PCR sont supérieurs à 72 heures, il résulte de l'instruction que les différents laboratoires qui réalisent des tests PCR prévoient des délais compris entre 24 et 48 heures pour l'obtention des résultats. <br/>
<br/>
              8. En outre, si le requérant soutient qu'aucune exemption à l'obligation prévue par les dispositions litigieuse de l'article 56-2 du décret du 29 octobre 2020 n'a été prévue, il résulte de l'instruction qu'un dispositif de dispense du test PCR en cas de motifs impérieux a été mis en place le 25 janvier par l'administration, subordonné à un isolement prophylactique d'une durée de sept jours dans des établissements désignés par le préfet du lieu d'arrivée, et à la réalisation d'un test au terme de celui-ci. <br/>
<br/>
              9. Il résulte de ce qui précède que l'obligation faite aux personnes âgées de onze ans ou plus en provenance du Royaume-Uni de présenter le résultat d'un examen biologique de dépistage virologique réalisé sur le territoire britannique moins de 72 heures avant l'embarquement ne concluant pas à une contamination par le covid-19, qui ne peut être regardée comme n'étant pas nécessaire et adaptée au risque liés au flux de personnes susceptibles d'être infectées par le variant anglais ni justifiée par des circonstances de temps et de lieu, ne porte pas une atteinte grave et manifestement illégale à la liberté de circulation et au droit des ressortissants français d'entrer sur le territoire national. <br/>
<br/>
              10. En deuxième lieu, si certaines discriminations peuvent constituer des atteintes à une liberté fondamentale au sens de l'article L. 521-2 du code de justice administrative, eu égard aux motifs qui les inspirent ou aux effets qu'elles produisent sur l'exercice d'une telle liberté, la méconnaissance du principe d'égalité ne révèle pas, par elle-même une atteinte de cette nature. Par ailleurs, non seulement les personnes en provenance du Royaume-Uni sont placées dans une situation différente des personnes en provenance d'une autre destination du fait de la situation sanitaire de ce pays, mais l'obligation litigieuse s'applique désormais à toute personne arrivant en France depuis un pays étranger. Dans ces conditions, le moyen tiré d'une atteinte grave et manifestement illégale au principe d'égalité ne peut être qu'écarté.<br/>
<br/>
              11. Il résulte de tout ce qui précède, et sans qu'il y ait lieu de se prononcer sur la condition d'urgence, que la requête présentée par M. B... doit être rejetée. <br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
              ------------------<br/>
<br/>
Article 1er : La requête de M. B... est rejetée.<br/>
Article 2 : La présente ordonnance sera notifiée à M. A... B..., au ministre des solidarités et de la santé et au Premier ministre.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
