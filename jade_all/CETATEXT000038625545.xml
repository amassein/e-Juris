<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038625545</ID>
<ANCIEN_ID>JG_L_2019_06_000000414458</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/62/55/CETATEXT000038625545.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème - 2ème chambres réunies, 14/06/2019, 414458</TITRE>
<DATE_DEC>2019-06-14</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>414458</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème - 2ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GARREAU, BAUER-VIOLAS, FESCHOTTE-DESBOIS ; SCP COUTARD, MUNIER-APAIRE</AVOCATS>
<RAPPORTEUR>M. Thomas Pez-Lavergne</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Mireille Le Corre</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2019:414458.20190614</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. C...et Mme A...B...ont demandé au tribunal administratif de Grenoble de condamner la société ERDF à leur verser une provision de 368 216,38 euros sur le fondement de l'article R. 541-1 du code de justice administrative en réparation des préjudices causés par l'implantation irrégulière de poteaux et lignes électriques sur les terrains dont ils étaient propriétaires. Ils ont également saisi le même tribunal d'une même demande au fond. Par un jugement n°s 147176, 1407185 du 28 mai 2015, le tribunal administratif de Grenoble a rejeté leurs demandes comme portées devant un ordre de juridiction incompétent pour en connaître.<br/>
<br/>
              Par un arrêt n° 15LY02639 du 27 juillet 2017, la cour administrative d'appel de Lyon a rejeté l'appel formé par M. et Mme B...contre ce jugement.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 20 septembre et 7 décembre 2017 au secrétariat du contentieux du Conseil d'Etat, M. et Mme B... demandent au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à leur appel ;<br/>
<br/>
              3°) de mettre à la charge de la société Enedis, venant aux droits de la société ERDF, la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la Constitution, notamment son Préambule ;<br/>
              - le code de l'énergie ;<br/>
              - la loi des 16-24 août 1790 et le décret du 16 fructidor an III ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Thomas Pez-Lavergne, maître des requêtes en service extraordinaire,<br/>
<br/>
              - les conclusions de Mme Mireille Le Corre, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Garreau, Bauer-Violas, Feschotte-Desbois, avocat de M. et Mme B...et à la SCP Coutard, Munier-Apaire, avocat de la société Enedis (ERDF) ;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que M. et Mme B..., propriétaires d'une parcelle de 3 760 m² située sur le territoire de la commune de Saint-Gervais-les-Bains, ont demandé, en vue de la réalisation d'un projet de lotissement et de construction, le 28 juillet 2011, à la société ERDF, désormais dénommée Enedis, de procéder à la suppression des poteaux et lignes électriques implantés, selon eux irrégulièrement, sur leur propriété. A la suite de la réitération de cette demande les 16 novembre 2011, 26 février, 23 octobre et 14 novembre 2012, la société ERDF a, le 5 décembre 2012, adressé à M. et Mme B...un devis relatif au coût de la mise sous terre des lignes électriques et de dépose des ouvrages litigieux. Après avoir accepté ces devis et versé des acomptes, à hauteur de 50 % du montant total des travaux, M. et Mme B...ont demandé à ERDF de les indemniser des préjudices qu'ils estimaient avoir subis. Par un jugement du 28 mai 2015, le tribunal administratif de Grenoble a rejeté leur demande tendant à la condamnation de la société ERDF à leur verser la somme de 368 216,38 euros en réparation de leurs préjudices comme présentée devant un ordre de juridiction incompétent pour en connaître. Par un arrêt du 27 juillet 2017 contre lequel M. et Mme B...se pourvoient en cassation, la cour administrative d'appel de Lyon a rejeté leur appel.<br/>
<br/>
              2. Il ressort des énonciations de l'arrêt attaqué que la cour administrative d'appel de Lyon a retenu que la convention par laquelle M. et Mme B...ont confié à la société ERDF la réalisation des travaux de mise en souterrain des lignes électriques et de dépose des supports des ouvrages avait le caractère d'un contrat de droit privé. Elle a regardé les conclusions de M. et Mme B...comme étant relatives à l'indemnisation des préjudices découlant de l'application de cette convention et en a déduit qu'elles relevaient de la compétence des juridictions de l'ordre judiciaire.<br/>
<br/>
              3. Sauf dispositions législatives contraires, la responsabilité qui peut incomber à l'Etat ou aux autres personnes morales de droit public en raison des dommages imputés à leurs services publics administratifs est soumise à un régime de droit public et relève en conséquence de la juridiction administrative. Cette compétence, qui découle du principe de la séparation des autorités administratives et judiciaires posé par l'article 13 de la loi des 16-24 août 1790 et par le décret du 16 fructidor an III, ne vaut toutefois que sous réserve des matières dévolues à l'autorité judiciaire par des règles ou principes de valeur constitutionnelle. Dans le cas d'une décision administrative portant atteinte à la propriété privée, le juge administratif, compétent pour statuer sur le recours en annulation d'une telle décision et, le cas échéant, pour adresser des injonctions à l'administration, l'est également pour connaître de conclusions tendant à la réparation des conséquences dommageables de cette décision administrative, hormis le cas où elle aurait pour effet l'extinction du droit de propriété.<br/>
<br/>
              4. Il ressort des pièces du dossier soumis aux juges du fond que les conclusions de M. et Mme B...tendaient principalement à faire constater le caractère irrégulier de l'implantation d'ouvrages publics sur les terrains dont ils étaient propriétaires par la société ERDF, qui ne justifiait d'aucun titre l'autorisant à les occuper, et à faire condamner cette dernière à les indemniser des préjudices qui leur avaient été causés par cette atteinte à leur propriété, indépendamment de l'engagement de la responsabilité contractuelle d'ERDF pour la mauvaise exécution du contrat portant sur l'enlèvement de ces ouvrages, né de l'acceptation par M. et Mme B...du devis qui leur avait été adressé. Ainsi, la juridiction administrative est compétente pour statuer sur les conclusions tendant à la réparation des conséquences de l'atteinte portée à la propriété privée de M. et MmeB..., laquelle n'a pas pour effet l'extinction de leur droit de propriété. En estimant que les préjudices allégués par M. et Mme B...résultaient de l'exécution du contrat que ceux-ci avaient passé avec la société ERDF, la cour administrative d'appel s'est méprise sur la portée des conclusions dont elle était saisie et a entaché son arrêt d'insuffisance de motivation en ne statuant pas sur l'engagement de la responsabilité de la société ERDF à raison de l'atteinte portée à la propriété privée des intéressés. Par suite, sans qu'il soit besoin d'examiner l'autre moyen du pourvoi, M. et Mme B...sont fondés à demander l'annulation de l'arrêt qu'ils attaquent.<br/>
<br/>
              5. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la société Enedis, venant aux droits de la société ERDF, la somme de 3 000 euros à verser à M. et MmeB..., au titre des dispositions de l'article L. 761-1 du code de justice administrative. En revanche, ces dispositions font obstacle à ce qu'une somme soit mise à ce titre à la charge de M. et Mme B...qui ne sont pas, dans la présente instance, les parties perdantes.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt du 27 juillet 2017 de la cour administrative d'appel de Lyon est annulé.<br/>
Article 2 : L'affaire est renvoyée à la cour administrative d'appel de Lyon.<br/>
Article 3 : La société Enedis versera à M. et Mme B... une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Les conclusions de la société Enedis présentées au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée à M.C... et Mme A...B...et à la société Enedis.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">17-03-02-08-02 COMPÉTENCE. RÉPARTITION DES COMPÉTENCES ENTRE LES DEUX ORDRES DE JURIDICTION. COMPÉTENCE DÉTERMINÉE PAR UN CRITÈRE JURISPRUDENTIEL. LIBERTÉ INDIVIDUELLE, PROPRIÉTÉ PRIVÉE ET ÉTAT DES PERSONNES. PROPRIÉTÉ. - ORDRE DE JURIDICTION COMPÉTENT POUR RÉPARER LE PRÉJUDICE RÉSULTANT DE L'ATTEINTE PORTÉE À UNE PROPRIÉTÉ PRIVÉE À CARACTÈRE IMMOBILIER - 1) PRINCIPE - COMPÉTENCE ADMINISTRATIVE - EXCEPTION - CAS OÙ L'ATTEINTE AURAIT POUR EFFET L'EXTINCTION DU DROIT DE PROPRIÉTÉ [RJ1] - 2) APPLICATION - DEMANDE INDEMNITAIRE EN RAISON DE L'ATTEINTE PORTÉE AU DROIT DE PROPRIÉTÉ D'UN PARTICULIER PAR L'IMPLANTATION IRRÉGULIÈRE D'UN OUVRAGE PUBLIC - COMPÉTENCE ADMINISTRATIVE.
</SCT>
<ANA ID="9A"> 17-03-02-08-02 1) Sauf dispositions législatives contraires, la responsabilité qui peut incomber à l'Etat ou aux autres personnes morales de droit public en raison des dommages imputés à leurs services publics administratifs est soumise à un régime de droit public et relève en conséquence de la juridiction administrative. Cette compétence, qui découle du principe de la séparation des autorités administratives et judiciaires posé par l'article 13 de la loi des 16 24 août 1790 et par le décret du 16 fructidor an III, ne vaut toutefois que sous réserve des matières dévolues à l'autorité judiciaire par des règles ou principes de valeur constitutionnelle. Dans le cas d'une décision administrative portant atteinte à la propriété privée, le juge administratif, compétent pour statuer sur le recours en annulation d'une telle décision et, le cas échéant, pour adresser des injonctions à l'administration, l'est également pour connaître de conclusions tendant à la réparation des conséquences dommageables de cette décision administrative, hormis le cas où elle aurait pour effet l'extinction du droit de propriété.,,,2) Il ressort des pièces du dossier soumis aux juges du fond que les conclusions des requérants tendaient principalement à faire constater le caractère irrégulier de l'implantation d'ouvrages publics sur les terrains dont ils étaient propriétaires par la société ERDF, qui ne justifiait d'aucun titre l'autorisant à les occuper, et à faire condamner cette dernière à les indemniser des préjudices qui leur avaient été causés par cette atteinte à leur propriété, indépendamment de l'engagement de la responsabilité contractuelle d'ERDF pour la mauvaise exécution du contrat portant sur l'enlèvement de ces ouvrages, né de l'acceptation par les requérants du devis qui leur avait été adressé. Ainsi, la juridiction administrative est compétente pour statuer sur les conclusions tendant à la réparation des conséquences de l'atteinte portée à la propriété privée des requérants, laquelle n'a pas pour effet l'extinction de leur droit de propriété.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. TC, 9 décembre 2013, M. et Mme Panizzon c/ commune de Saint-Palais-sur-Mer, n° 3931, p. 376.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
