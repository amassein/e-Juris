<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000030192210</ID>
<ANCIEN_ID>JG_L_2015_02_000000367724</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/30/19/22/CETATEXT000030192210.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 5ème / 4ème SSR, 04/02/2015, 367724</TITRE>
<DATE_DEC>2015-02-04</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>367724</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>5ème / 4ème SSR</FORMATION>
<TYPE_REC/>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DIDIER, PINET ; SCP ROCHETEAU, UZAN-SARANO</AVOCATS>
<RAPPORTEUR>Mme Leïla Derouich</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Fabienne Lambolez</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2015:367724.20150204</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 12 avril et 11 juillet 2013 au secrétariat du contentieux du Conseil d'Etat, présentés pour le centre hospitalier de Hyères, dont le siège est avenue du maréchal Juin à Hyères cedex (83407) ; le centre hospitalier de Hyères demande au Conseil d'Etat : <br/>
<br/>
               1°) d'annuler l'arrêt n° 12MA01304 du 12 février 2013 par lequel la cour administrative d'appel de Marseille a rejeté son appel contre le jugement n° 1002532 du tribunal administratif de Toulon du 16 mars 2012 annulant la décision du 30 août 2010 de son directeur licenciant Mme A...B...à compter du 1er septembre 2010 ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ;<br/>
<br/>
              3°) de mettre à la charge de Mme B...le versement d'une somme de 2 500 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la loi n° 83-634 du 13 juillet 1983 ; <br/>
<br/>
              Vu la loi n° 91-647 du 10 juillet 1991 ; <br/>
<br/>
              Vu le décret n° 91-155 du 6 février 1991 ; <br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Leïla Derouich, auditeur,  <br/>
<br/>
              - les conclusions de Mme Fabienne Lambolez, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Didier, Pinet, avocat du centre hospitalier de Hyères et à la SCP Rocheteau, Uzan-Sarano, avocat de Mme B...;<br/>
<br/>
<br/>
<br/>
<br/>
              1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond que Mme B..., agent d'entretien recruté en 2002 par le centre hospitalier de Hyères dans le cadre d'un contrat aidé, a bénéficié à compter du 1er novembre 2007 de contrats à durée déterminée successifs puis, à compter du 1er janvier 2010, d'un contrat à durée indéterminée ; qu'au début de l'année 2010 le centre hospitalier, envisageant de la titulariser dans le corps des agents des services hospitaliers, a demandé communication du bulletin n° 2 de son casier judiciaire afin de vérifier qu'elle remplissait les conditions prévues par le troisième alinéa de l'article 5 de la loi du 13 juillet 1983 en vertu duquel nul ne peut avoir la qualité de fonctionnaire si les mentions portées à ce bulletin sont incompatibles avec l'exercice des fonctions qui lui sont confiées ; qu'ayant appris à cette occasion que Mme B...avait été condamnée par un jugement du tribunal correctionnel de Toulon du 24 janvier 2008 à une peine de trente mois d'emprisonnement avec sursis pour complicité de trafic de stupéfiants, le directeur du centre hospitalier a décidé, le 1er juillet 2010, d'interrompre la procédure de titularisation ; que, par une décision du 30 août 2010, il a licencié Mme B...à compter du 1er septembre 2010 au double motif, d'une part, qu'en vertu de l'article 3 du décret du 6 février 1991 relatif au statut des agents contractuels de la fonction publique hospitalière, les mentions portées au bulletin n° 2 de son casier judiciaire n'étaient pas compatibles avec l'exercice de ses fonctions et, d'autre part, qu'elle avait commis une faute disciplinaire de nature à justifier la sanction du licenciement sans préavis ni indemnité ; que le tribunal administratif de Toulon a annulé cette décision par un jugement du 16 mars 2012 ; que le centre hospitalier de Hyères se pourvoit en cassation contre l'arrêt du 12 février 2013 par lequel la cour administrative d'appel de Marseille a rejeté son appel contre ce jugement ;<br/>
<br/>
              2. Considérant qu'il ressort également des pièces du dossier soumis aux juges du fond que le centre hospitalier de Hyères soutenait devant ceux-ci que Mme B...avait commis une faute en ne l'informant pas de la condamnation pénale dont elle avait fait l'objet en cours d'exécution de son contrat et que cette circonstance était de nature à préjudicier gravement à l'intérêt du service public en portant atteinte à la réputation de cet établissement hospitalier ; qu'en jugeant que le directeur avait prononcé une sanction manifestement disproportionnée en licenciant l'intéressée, sans répondre à cette argumentation, qui n'était pas inopérante, la cour administrative d'appel de Marseille a entaché son arrêt d'insuffisance de motivation ; qu'il suit de là, sans qu'il soit besoin d'examiner les autres moyens du pourvoi, que son arrêt doit être annulé ; <br/>
<br/>
              3. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative ;<br/>
<br/>
              Sur la régularité du jugement du tribunal administratif de Toulon :<br/>
<br/>
              4. Considérant qu'il ressort des pièces du dossier que le centre hospitalier de Hyères s'est borné, dans son mémoire en défense devant le tribunal administratif de Toulon, à réfuter les moyens présentés par MmeB..., tirés de ce que son directeur n'avait pu légalement, sur le fondement des articles 39 et 39-2 du décret du 6 février 1991, prendre à son encontre la sanction du licenciement ; que, dès lors, en annulant la décision attaquée au motif que le directeur avait commis une erreur manifeste d'appréciation en prononçant cette sanction, le tribunal administratif n'a omis de répondre à aucun moyen présenté en défense ; <br/>
<br/>
              Sur la légalité de la décision du 30 août 2010 du directeur du centre hospitalier de Hyères :<br/>
<br/>
              En ce qui concerne l'application des dispositions des articles 39 et 39-2 du décret du 6 février 1991 relatif au statut des agents contractuels de la fonction publique hospitalière ; <br/>
<br/>
              5. Considérant qu'aux termes de l'article 39-2 du décret du 6 février 1991 : " Tout manquement au respect des obligations auxquelles sont assujettis les agents publics, commis par un agent contractuel dans l'exercice ou à l'occasion de l'exercice de ses fonctions, est constitutif d'une faute l'exposant à une sanction disciplinaire, sans préjudice, le cas échéant, des peines prévues par le code pénal " ; que le licenciement sans préavis ni indemnité est mentionné à l'article 39 du même décret parmi les sanctions susceptibles d'être infligées aux agents contractuels ; qu'aucune disposition législative ou réglementaire n'interdit à l'administration de se fonder sur des faits ayant motivé une condamnation pénale pour déclencher une procédure disciplinaire à  l'encontre d'un agent ; <br/>
<br/>
              6. Considérant qu'il ressort des pièces du dossier que, ainsi qu'il a été dit ci-dessus, Mme B... a été condamnée par un jugement du tribunal correctionnel de Toulon du 24 janvier 2008 devenu définitif à une peine de trente mois de prison avec sursis pour complicité de transport, de détention, d'acquisition, d'offre ou de cession de stupéfiants ; qu'il ressort des motifs de ce jugement que Mme B...hébergeait à son domicile son conjoint, qui se livrait à un trafic de résine de cannabis pour lequel il a été condamné à huit années de prison ferme par le même jugement et qu'elle s'est rendue coupable de complicité en acceptant que soient conservés à son domicile des substances et des fonds liés à ce trafic  ; qu'ainsi que l'a relevé ce même jugement du tribunal correctionnel de Toulon, Mme B...a toutefois, par la suite, dénoncé ces agissements illicites aux services de police et accepté de collaborer au déroulement de l'enquête diligentée par ceux-ci ; qu'il  ne ressort pas des pièces du dossier que la condamnation et les faits que celle-ci avait pour objet de réprimer, qui n'ont pas fait l'objet d'une publicité particulière, aient eu des conséquences préjudiciables pour le service public, notamment en portant atteinte à la réputation du centre hospitalier de Hyères ; qu'il ressort des pièces du dossier que Mme B...s'est toujours acquittée de ses fonctions d'agent d'entretien dans des conditions satisfaisantes ; qu'il  n'est pas établi qu'elle serait susceptible, dans le cadre de ses fonctions, de soustraire des produits pharmaceutiques relevant de la réglementation sur les stupéfiants ; que, par ailleurs, aucune disposition législative ou réglementaire n'imposait à Mme B...d'informer son employeur de la condamnation pénale dont elle a fait l'objet postérieurement à son recrutement ;  que, dans ces conditions, en estimant que les faits ayant motivé la condamnation pénale infligée à Mme B...étaient de nature à justifier la sanction du licenciement sans préavis ni indemnité, le directeur du centre hospitalier de Hyères a entaché sa décision d'une erreur d'appréciation ; <br/>
<br/>
              En ce qui concerne l'application des dispositions de l'article 3 du même décret : <br/>
<br/>
              7. Considérant qu'aux termes de l'article 3 du décret du 6 février 1991 : " Aucun agent contractuel ne peut être recruté si, étant de nationalité française : (...) 2° Les mentions portées au bulletin n° 2 de son casier judiciaire sont incompatibles avec l'exercice des fonctions " ; <br/>
<br/>
              8. Considérant que, sauf s'il présente un caractère fictif ou frauduleux, le contrat de recrutement d'un agent contractuel de droit public crée des droits au profit de celui-ci ; que, lorsque le contrat est entaché d'irrégularité, l'administration est tenue de proposer à l'agent contractuel en cause une régularisation de son contrat afin que son exécution se poursuive régulièrement ; que si le contrat ne peut être régularisé, il appartient à l'administration, dans la limite des droits résultant du contrat initial, de proposer à l'agent un emploi de niveau équivalent, ou à défaut d'un tel emploi et si l'agent le demande, tout autre emploi, afin de régulariser sa situation ; que si l'intéressé refuse la régularisation de son contrat ou si la régularisation de sa situation, dans les conditions précisées ci-dessus, est impossible, l'administration est tenue de le licencier ; <br/>
<br/>
              9. Considérant que, lorsque l'administration apprend que des mentions avaient été portées au bulletin n° 2 du casier judiciaire d'un agent avec lequel elle a conclu un contrat de recrutement, il lui appartient, pour déterminer si ce contrat est entaché d'irrégularité, d'apprécier si, eu égard, d'une part, à l'objet des mentions en cause et à l'ensemble des motifs de la condamnation pénale dont l'agent a fait l'objet, d'autre part, aux caractéristiques des fonctions qu'il exerce, ces mentions sont incompatibles avec l'exercice de ces fonctions ; <br/>
<br/>
              10. Considérant qu'eu égard, d'une part, aux motifs de la condamnation pénale infligée à MmeB..., qui ont été précisés au point 6 ci-dessus, d'autre part aux caractéristiques de ses fonctions d'agent d'entretien, le centre hospitalier de Hyères a, dans les circonstances de l'espèce, commis une erreur d'appréciation en estimant que les mentions portées au bulletin n° 2 de son casier judiciaire étaient incompatibles avec l'exercice, par MmeB..., de ses fonctions et que son contrat était par suite entaché d'une irrégularité qui  justifiait son  licenciement ; <br/>
<br/>
              11. Considérant qu'il résulte de tout ce qui précède que le centre hospitalier de Hyères n'est pas fondé à soutenir que c'est à tort que, par le jugement attaqué, le tribunal administratif de Toulon a annulé la décision de son directeur du 30 août 2010 prononçant le licenciement de Mme B...; <br/>
<br/>
              Sur les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative : <br/>
<br/>
              12. Considérant que les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce que soit mise à la charge de MmeB..., qui n'est pas la partie perdante dans la présente instance, la somme que demande à ce titre le centre hospitalier de Hyères  ; qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge du centre hospitalier de Hyères le versement de la somme de 3 000 euros à la SCP R. Rocheteau et C. Uzan-Sarano, avocat de MmeB..., au titre des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991, sous réserve que cette société renonce à percevoir la somme correspondant à la part contributive de l'Etat ;  <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Marseille du 12 février 2013 est annulé. <br/>
Article 2 : La requête d'appel présentée par le centre hospitalier de Hyères devant la cour administrative d'appel de Marseille et ses conclusions présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
Article 3 : Le centre hospitalier de Hyères versera la somme de 3 000 euros à la SCP R. Rocheteau et C. Uzan-Sarano, avocat de MmeB..., au titre des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991, sous réserve que cette société renonce à percevoir la somme correspondant à la part contributive de l'Etat.  <br/>
Article 4 : La présente décision sera notifiée au centre hospitalier de Hyères et à Mme A...B.... <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">36-12-03-01 FONCTIONNAIRES ET AGENTS PUBLICS. AGENTS CONTRACTUELS ET TEMPORAIRES. FIN DU CONTRAT. LICENCIEMENT. - CONTRAT D'ENGAGEMENT D'UN AGENT PUBLIC IRRÉGULIER - 1) OBLIGATION POUR L'ADMINISTRATION DE RÉGULARISER LE CONTRAT OU LA SITUATION DE L'AGENT ET, SINON, DE LICENCIER L'AGENT (JURISPRUDENCE CAVALLO [RJ1]) - 2) APPRÉCIATION DE L'IRRÉGULARITÉ - CAS OÙ L'ADMINISTRATION DÉCOUVRE EN COURS D'EXÉCUTION DU CONTRAT QUE CERTAINES MENTIONS AVAIENT ÉTÉ PORTÉES AU CASIER JUDICIAIRE DE L'AGENT AVANT LA CONCLUSION DU CONTRAT - APPRÉCIATION DU CARACTÈRE COMPATIBLE DE CES MENTIONS AVEC LES FONCTIONS.
</SCT>
<ANA ID="9A"> 36-12-03-01 1) Sauf s'il présente un caractère fictif ou frauduleux, le contrat de recrutement d'un agent contractuel de droit public crée des droits au profit de celui-ci. Lorsque le contrat est entaché d'irrégularité, l'administration est tenue de proposer à l'agent contractuel en cause une régularisation de son contrat ou de sa situation. Si l'intéressé refuse la régularisation de son contrat ou si la régularisation de sa situation est impossible, l'administration est tenue de le licencier....  ,,2) Lorsque l'administration apprend que des mentions avaient été portées au bulletin n° 2 du casier judiciaire d'un agent avec lequel elle a conclu un contrat de recrutement, il lui appartient, pour déterminer si ce contrat est entaché d'irrégularité, d'apprécier si, eu égard, d'une part, à l'objet des mentions en cause et à l'ensemble des motifs de la condamnation pénale dont l'agent a fait l'objet, d'autre part, aux caractéristiques des fonctions qu'il exerce, ces mentions sont incompatibles avec l'exercice de ces fonctions. En l'espèce, compatibilité avec l'exercice des fonctions des mentions inscrites au casier judiciaire avant la conclusion d'un CDI. Par suite, le contrat n'était pas entaché d'une irrégularité justifiant un licenciement.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1]Cf. CE, Section, 31 décembre 2008, Cavallo, n° 283256, p. 481.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
