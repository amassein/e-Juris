<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043587839</ID>
<ANCIEN_ID>JG_L_2021_06_000000425551</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/58/78/CETATEXT000043587839.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème chambre, 01/06/2021, 425551, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-06-01</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>425551</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>CABINET MUNIER-APAIRE ; SCP MATUCHANSKY, POUPOT, VALDELIEVRE</AVOCATS>
<RAPPORTEUR>M. Pierre Vaiss</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Raphaël Chambon</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:425551.20210601</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu les procédures suivantes :<br/>
<br/>
              Mme A... B... a demandé au tribunal administratif de Paris, par trois demandes distinctes :<br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision implicite du 8 octobre 2013 par laquelle la ministre des affaires sociales et de la santé a rejeté sa demande tendant à ce que Mme C... D... soit traduite devant la chambre disciplinaire de première instance de la région Ile-de-France de l'ordre des médecins ;<br/>
<br/>
              2°) d'annuler pour excès de pouvoir la décision du 17 décembre 2013 par laquelle le conseil départemental de la Ville de Paris de l'ordre des médecins a rejeté sa demande tendant à ce que Mme C... D... soit traduite devant la chambre disciplinaire de première instance de la région Ile-de-France de l'ordre des médecins ;<br/>
<br/>
              3°) de condamner l'Assistance publique-Hôpitaux de Paris à lui verser la somme de 215 000 euros en réparation des préjudices qu'elle estime avoir subis du fait de la prise en charge tardive de son cancer du sein. <br/>
<br/>
              Par un jugement n° 1317430, 1402949, 1414319 du 7 juillet 2016, le tribunal administratif a rejeté les conclusions à fin d'annulation, a déclaré l'Assistance publique-Hôpitaux de Paris entièrement responsable des conséquences dommageables de la prise en charge tardive du cancer de Mme B... et ordonné avant dire droit une expertise afin de déterminer le préjudice subi. <br/>
<br/>
              Par un arrêt n° 16PA02928, 16PA02929 et 16PA02930 du 25 septembre 2018, la cour administrative d'appel de Paris a, sur appel de Mme B..., d'une part, après avoir annulé ce jugement en tant qu'il a rejeté comme irrecevables ses conclusions tendant à l'annulation de la décision du 17 décembre 2013 du conseil départemental de la Ville de Paris de l'ordre des médecins, rejeté ces mêmes conclusions, d'autre part, rejeté le surplus des conclusions d'appel de Mme B.... <br/>
<br/>
              Par un pourvoi sommaire et trois nouveaux mémoires, enregistrés le 21 novembre 2018, le 6 février et le 20 juin 2019 et le 19 octobre 2020 au secrétariat du contentieux du Conseil d'Etat, Mme B... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cet arrêt en tant qu'il rejette le surplus de ses conclusions d'appel ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire doit à ses conclusions d'appel ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat et du conseil départemental de la Ville de Paris de l'ordre des médecins la somme de 3 000 euros chacun au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code de la santé publique ;<br/>
              - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Pierre Vaiss, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Raphaël Chambon, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Munier-Apaire, avocat de Mme B... et à la SCP Matuchansky, Poupot, Valdelièvre, avocat du conseil départemental de la Ville de Paris de l'Ordre des Médecins ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que Mme A... B..., qui présentait une mastopathie fibrokystique importante, était suivie depuis 1992 par son gynécologue de ville et des spécialistes de l'hôpital Saint-Louis, relevant de l'Assistance Publique-Hôpitaux de Paris, et de l'Hôpital américain de Paris, avant que soit détectée, en septembre 2006, la présence d'une tumeur maligne au sein gauche. A la suite de cette découverte, Mme B... a subi en octobre 2006 une mastectomie totale du sein gauche, réalisée à la clinique Hartmann, puis une chimiothérapie jusqu'à l'été 2007 et enfin une hormonothérapie d'une durée de cinq ans. Saisie par Mme B..., la vice-présidente du tribunal de grande instance de Versailles, par une ordonnance du 23 juillet 2009, a prescrit la réalisation d'une expertise par Mme C... D.... Postérieurement à la remise de l'expertise, Mme B... a demandé au conseil départemental de la Ville de Paris de l'ordre des médecins d'une part, à la ministre chargée de la santé d'autre part, de porter plainte contre Mme D... devant la juridiction disciplinaire de cet ordre. Par des décisions en date, respectivement, du 8 octobre 2013 et du 17 décembre 2013, la ministre chargée de la santé et le conseil départemental de la Ville de Paris de l'ordre des médecins ont rejeté ces demandes. Mme B... a également recherché la responsabilité de l'Assistance Publique - Hôpitaux de Paris (AP-HP) à raison des préjudices subis du fait des conditions de prise en charge de son cancer. Par un jugement du 7 juillet 2016, le tribunal administratif de Paris a, d'une part, jugé que la responsabilité de l'AP-HP était engagée à l'égard de Mme B... et ordonné une expertise avant-dire droit, d'autre part rejeté les conclusions de Mme B... tendant à l'annulation des décisions rejetant ses demandes de saisine de la juridiction disciplinaire. Par un arrêt du 25 septembre 2018, la cour administrative d'appel de Paris, d'une part, après avoir annulé le jugement du tribunal administratif en tant qu'il avait rejeté comme irrecevable la demande de Mme B... tendant à l'annulation de la décision du 17 décembre 2013 du conseil départemental de la Ville de Paris de l'ordre des médecins, a rejeté cette demande, d'autre part, a rejeté le surplus de ses conclusions d'appel. Mme B... se pourvoit en cassation contre cet arrêt en tant qu'il rejette le surplus de ses conclusions d'appel.<br/>
<br/>
              2. Aux termes de l'article L. 4124-2 du code de la santé publique : " Les médecins, les chirurgiens-dentistes ou les sages-femmes chargés d'un service public et inscrits au tableau de l'ordre ne peuvent être traduits devant la chambre disciplinaire de première instance, à l'occasion des actes de leur fonction publique, que par le ministre chargé de la santé, le représentant de l'Etat dans le département, le directeur général de l'agence régionale de santé, le procureur de la République, le conseil national ou le conseil départemental au tableau duquel le praticien est inscrit ". Aux termes de l'article R. 4127-105 du même code : " Un médecin ne doit pas accepter une mission d'expertise dans laquelle sont en jeu ses propres intérêts, ceux d'un de ses patients, d'un de ses proches, d'un de ses amis ou d'un groupement qui fait habituellement appel à ses services ". <br/>
<br/>
              3. Après avoir relevé, par une appréciation souveraine exempte de dénaturation, que Mme D... n'avait pas de relations habituelles avec la clinique Hartmann, la cour administrative d'appel a jugé que le conseil départemental de la Ville de Paris de l'ordre des médecins avait pu légalement estimer qu'en réalisant l'expertise prescrite par le tribunal de grande instance de Versailles, elle n'avait pas méconnu les dispositions de l'article R. 4127-105 du code de la santé publique. Ce faisant, elle n'a pas entaché d'erreur de droit son arrêt, lequel est suffisamment motivé, alors même qu'il ne mentionne pas un jugement du tribunal de grande instance de Paris du 18 décembre 2017, frappé d'appel, engageant, à raison de la même expertise, la responsabilité civile professionnelle de Mme D.... <br/>
<br/>
              4. Il résulte de tout ce qui précède que Mme B... n'est pas fondée à demander l'annulation de l'arrêt qu'elle attaque. Son pourvoi doit être rejeté, y compris ses conclusions tendant à l'application de l'article L. 761-1 du code de justice administrative. Il n'y a pas lieu, dans les circonstances de l'espèce, de mettre à la charge de Mme B... le versement d'une somme au conseil départemental de la Ville de Paris de l'ordre des médecins au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de Mme B... est rejeté. <br/>
Article 2 : Les conclusions du conseil départemental de la Ville de Paris de l'ordre des médecins présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La présente décision sera notifiée à Mme A... B..., au conseil départemental de la Ville de Paris de l'ordre des médecins, à Mme C... D... et au ministre des solidarités et de la santé. <br/>
Copie en sera adressée au Conseil national de l'ordre des médecins. <br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
