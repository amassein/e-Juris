<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000037631770</ID>
<ANCIEN_ID>JG_L_2018_11_000000419804</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/37/63/17/CETATEXT000037631770.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème - 8ème chambres réunies, 21/11/2018, 419804</TITRE>
<DATE_DEC>2018-11-21</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>419804</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème - 8ème chambres réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FOUSSARD, FROGER ; SCP GADIOU, CHEVALLIER</AVOCATS>
<RAPPORTEUR>M. Laurent Cytermann</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Emmanuelle Cortot-Boucher</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2018:419804.20181121</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              La société Fêtes Loisirs a demandé au juge des référés du tribunal administratif de Paris d'ordonner, sur le fondement des dispositions de l'article L. 521-1 du code de justice administrative, la suspension de l'exécution de la décision de la ville de Paris de mettre fin au 5 juillet 2018 à la convention d'occupation du domaine public du 4 juillet 2016 pour l'exploitation d'une grande roue place de la Concorde, d'ordonner le maintien provisoire des relations contractuelles jusqu'à ce qu'il soit statué au fond sur la légalité de cette décision, et d'enjoindre à la ville de Paris de répondre, dans un délai de 48 heures à compter de la présente ordonnance, aux questions posées par sa demande du 1er décembre 2017 de connaître les dates de début et fin de la période d'exploitation 2018-2019, sous astreinte de 5 000 euros par jour de retard.<br/>
<br/>
              Par une ordonnance n° 1803810 du 28 mars 2018, le juge des référés du tribunal administratif de Paris a rejeté cette demande.<br/>
<br/>
              Par un pourvoi, enregistré le 12 avril 2018 au secrétariat du contentieux du Conseil d'Etat, la société Fêtes Loisirs demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) réglant l'affaire en référé, de faire droit à sa demande ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 4 000 euros au titre de l'article L. 761-1 du code de justice administrative. <br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - la charte des droits fondamentaux de l'Union européenne ;<br/>
              - la directive 2006/123/CE du Parlement européen et du Conseil du 12 décembre 2006 relative aux services dans le marché intérieur ;<br/>
              - le code général de la propriété des personnes publiques ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Laurent Cytermann, maître des requêtes,  <br/>
<br/>
              - les conclusions de Mme Emmanuelle Cortot-Boucher, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Gadiou, Chevallier, avocat de la société Fêtes Loisirs et à la SCP Foussard, Froger, avocat de la ville de Paris ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 25 octobre 2018, présentée par la société Fêtes Loisirs. <br/>
<br/>
              Vu la note en délibéré, enregistrée le 19 novembre 2018, présentée par la société Fêtes Loisirs.<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis au juge des référés du tribunal administratif de Paris qu'une convention d'occupation du domaine public a été conclue le 4 juillet 2016 entre la ville de Paris et la société Fêtes Loisirs, prévoyant l'exploitation d'une grande roue et de trois structures de vente annexes place de la Concorde, pour une durée de deux ans renouvelable deux fois. Par une lettre envoyée le 1er décembre 2017 à la ville de Paris, la société Fêtes Loisirs lui a demandé de confirmer le renouvellement de la convention au-delà du terme de la première période de deux ans. Par une lettre du 21 mars 2018, la directrice de l'attractivité et de l'emploi de la ville de Paris a notifié à la société la décision de ne pas renouveler la convention. La société Fêtes Loisirs a saisi le tribunal administratif de Paris d'une demande tendant à l'annulation de la décision de non-renouvellement et à la poursuite des relations contractuelles, assortie d'une demande de suspension de la décision de non-renouvellement sur le fondement des dispositions de l'article L. 521-1 du code de justice administrative. Le juge des référés du tribunal administratif de Paris a rejeté cette demande par une ordonnance du 28 mars 2018, contre laquelle la société Fêtes Loisirs se pourvoit en cassation. <br/>
<br/>
              2. Le Conseil d'Etat a informé les parties que la décision était susceptible d'être fondée sur le moyen relevé d'office tiré de l'irrecevabilité de la demande de la société Fêtes Loisirs devant le tribunal administratif, la décision de l'administration de ne pas renouveler le contrat parvenu à son terme ne pouvant faire l'objet d'un recours en reprise des relations contractuelles mais seulement, si le requérant s'y croit fondé, d'une demande d'indemnisation.<br/>
<br/>
              3. Le juge des référés du tribunal administratif a relevé, par une appréciation souveraine dont il n'est pas soutenu qu'elle serait entachée de dénaturation, que la lettre du 21 mars 2018 était constitutive d'une décision de ne pas renouveler la convention d'occupation du domaine public lorsqu'elle serait parvenue à son terme initial, et non d'une décision de résilier le contrat avant son terme. Au demeurant, il ressort des pièces du dossier soumis au juge des référés que cette lettre, qui précédait de plus de trois mois le terme stipulé de la convention, informait le gérant de la société de la décision de ne pas renouveler celle-ci. Le moyen tiré de ce que la durée fixée par une convention serait illégale est inopérant à l'appui d'une demande tendant ce que soit constatée la nullité de la décision de non-renouvellement et ordonnée la reprise des relations contractuelles.<br/>
<br/>
              4. Le juge du contrat ne peut, en principe, lorsqu'il est saisi par une partie d'un litige relatif à une mesure d'exécution d'un contrat, que rechercher si cette mesure est intervenue dans des conditions de nature à ouvrir droit à indemnité. Toutefois, une partie à un contrat administratif peut, eu égard à la portée d'une telle mesure d'exécution, former devant le juge du contrat un recours de plein contentieux contestant la validité de la résiliation de ce contrat et tendant à la reprise des relations contractuelles. Cette exception relative aux décisions de résiliation ne s'étend pas aux décisions de non-renouvellement, qui sont des mesures d'exécution du contrat et qui n'ont ni pour objet, ni pour effet de mettre unilatéralement un terme à une convention en cours. <br/>
<br/>
              5. Dès lors, les conclusions présentées par la société Fêtes Loisirs devant le tribunal administratif tendant à l'annulation de la décision de non-renouvellement de la convention et à la poursuite des relations contractuelles ainsi que sa demande de suspension à l'encontre de la même décision sont irrecevables. Compte tenu de ce qui a été dit au point 4, qui ne constitue pas un revirement de jurisprudence, la société, qui peut, si elle s'y croit fondée, demander au juge du contrat la réparation de son préjudice, n'est pas privée du droit à un recours  juridictionnel garanti par l'article 13 de la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales et par l'article 47 de la charte des droits fondamentaux de l'Union européenne, et ni la garantie des droits, ni le principe de légalité ne sont méconnus. En outre, si l'ancien cocontractant de l'administration peut contester devant le juge du contrat le nouveau contrat conclu par celle-ci avec une autre entreprise, c'est en qualité de tiers à ce nouveau contrat et il n'est dès lors pas placé dans une situation analogue à celle du cocontractant ne pouvant contester la décision de non-renouvellement. Le moyen tiré de la méconnaissance du principe d'égalité doit donc être également écarté.<br/>
<br/>
              6. Ce motif d'irrecevabilité, qui ne comporte l'appréciation d'aucune circonstance de fait qui n'ait pas été relevée par le juge des référés, doit être substitué aux motifs retenus par l'ordonnance attaquée, dont il justifie légalement le dispositif. Par suite, il n'est pas nécessaire d'examiner les moyens du pourvoi, qui doit être rejeté. Les conclusions à fin d'injonction présentées par la société Fêtes Loisirs doivent de même être rejetées par voie de conséquence.<br/>
<br/>
              7. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de la société Fêtes Loisirs la somme de 3 000 euros à verser à la ville de Paris au titre des dispositions de l'article L. 761-1 du code de justice administrative. Les mêmes dispositions font obstacle à ce qu'une somme soit mise à ce titre à la charge de la ville de Paris qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de la société Fêtes Loisirs est rejeté.<br/>
Article 2 : Les conclusions présentées par la société Fêtes Loisirs au titre de l'article L. 911-1 du code de justice administrative sont rejetées.<br/>
Article 3 : La société Fêtes Loisirs versera à la ville de Paris une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à la société Fêtes Loisirs et à la ville de Paris.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">39-08-01 MARCHÉS ET CONTRATS ADMINISTRATIFS. RÈGLES DE PROCÉDURE CONTENTIEUSE SPÉCIALES. RECEVABILITÉ. - COCONTRACTANT CONTESTANT LA DÉCISION D'UNE COMMUNE, PRISE DANS LE RESPECT DU DÉLAI DE PRÉAVIS, DE NE PAS RECONDUIRE UNE CONVENTION PARVENUE À SON TERME INITIAL - RECEVABILITÉ DU RECOURS EN REPRISE DES RELATIONS CONTRACTUELLES [RJ1] - PRINCIPE - ABSENCE [RJ2] - PRINCIPE CONSTITUANT UN REVIREMENT DE JURISPRUDENCE - ABSENCE.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">39-08-03-02 MARCHÉS ET CONTRATS ADMINISTRATIFS. RÈGLES DE PROCÉDURE CONTENTIEUSE SPÉCIALES. POUVOIRS ET OBLIGATIONS DU JUGE. POUVOIRS DU JUGE DU CONTRAT. - COCONTRACTANT CONTESTANT LA DÉCISION D'UNE COMMUNE, PRISE DANS LE RESPECT DU DÉLAI DE PRÉAVIS, DE NE PAS RECONDUIRE UNE CONVENTION PARVENUE À SON TERME INITIAL - RECEVABILITÉ DU RECOURS EN REPRISE DES RELATIONS CONTRACTUELLES [RJ1] - PRINCIPE - ABSENCE [RJ2] - PRINCIPE CONSTITUANT UN REVIREMENT DE JURISPRUDENCE - ABSENCE.
</SCT>
<ANA ID="9A"> 39-08-01 Le juge du contrat ne peut, en principe, lorsqu'il est saisi par une partie d'un litige relatif à une mesure d'exécution d'un contrat, que rechercher si cette mesure est intervenue dans des conditions de nature à ouvrir droit à indemnité. Toutefois, une partie à un contrat administratif peut, eu égard à la portée d'une telle mesure d'exécution, former devant le juge du contrat un recours de plein contentieux contestant la validité de la résiliation de ce contrat et tendant à la reprise des relations contractuelles. Cette exception relative aux décisions de résiliation ne s'étend pas aux décisions de non-renouvellement, qui sont des mesures d'exécution du contrat et qui n'ont ni pour objet, ni pour effet de mettre unilatéralement un terme à une convention en cours.... ...Cette règle jurisprudentielle ne constitue pas un revirement de jurisprudence.</ANA>
<ANA ID="9B"> 39-08-03-02 Le juge du contrat ne peut, en principe, lorsqu'il est saisi par une partie d'un litige relatif à une mesure d'exécution d'un contrat, que rechercher si cette mesure est intervenue dans des conditions de nature à ouvrir droit à indemnité. Toutefois, une partie à un contrat administratif peut, eu égard à la portée d'une telle mesure d'exécution, former devant le juge du contrat un recours de plein contentieux contestant la validité de la résiliation de ce contrat et tendant à la reprise des relations contractuelles. Cette exception relative aux décisions de résiliation ne s'étend pas aux décisions de non-renouvellement, qui sont des mesures d'exécution du contrat et qui n'ont ni pour objet, ni pour effet de mettre unilatéralement un terme à une convention en cours.... ...Cette règle jurisprudentielle ne constitue pas un revirement de jurisprudence.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, Section, 21 mars 2011, Commune de Béziers, n° 304806, p. 117., ,[RJ2] Cf. CE, 6 juin 2018, Société Orange, n° 411053, à mentionner aux Tables.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
