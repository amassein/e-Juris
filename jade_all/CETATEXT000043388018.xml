<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043388018</ID>
<ANCIEN_ID>JG_L_2021_04_000000432786</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/38/80/CETATEXT000043388018.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème chambre, 16/04/2021, 432786, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2021-04-16</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>432786</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème chambre</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP CELICE, TEXIDOR, PERIER</AVOCATS>
<RAPPORTEUR>Mme Pauline Berne</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Marie-Gabrielle Merloz</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2021:432786.20210416</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>
<br/>
              Vu la procédure suivante :<br/>
<br/>
              Par une décision du 27 décembre 2019, le Conseil d'Etat, statuant au contentieux a prononcé l'admission des conclusions du pourvoi de la société par actions simplifiée (SAS) Décoration Protection des Métaux dirigées contre le jugement n° 1701341-1801813 du 21 mai 2019 du tribunal administratif de Caen, en tant seulement qu'il a statué sur l'imposition des immobilisations référencées sous les numéros 141003 et 130017.<br/>
<br/>
              Par un mémoire en défense, enregistré le 22 juin 2020, le ministre de l'action et des comptes publics conclut au rejet du pourvoi. Il soutient que les moyens soulevés ne sont pas fondés.<br/>
<br/>
              Par un mémoire en réplique, enregistré le 10 août 2020, la SAS Décoration Protection des Métaux reprend les conclusions de son pourvoi et les mêmes moyens. <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code général des impôts et le livre des procédures fiscales ;<br/>
              - le code de justice administrative et le décret n° 2020-1406 du 18 novembre 2020 ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Pauline Berne, maître des requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de Mme Marie-Gabrielle Merloz, rapporteure publique ;<br/>
<br/>
              La parole ayant été donnée, après les conclusions, à la SCP Célice, Texidor, Perier, avocat de la société Décoration Protection des Métaux ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis aux juges du fond que la société par actions simplifiée (SAS) Décoration Protection des Métaux est propriétaire sur le territoire de la commune de Perche-en-Nocé (Orne) d'un établissement où elle exerce une activité de traitement de surface des métaux. Elle a demandé au tribunal administratif de Caen de prononcer la décharge des cotisations supplémentaires de taxe foncière sur les propriétés bâties auxquelles elle a été assujettie au titre des années 2015 et 2016, assises sur la valeur locative de l'établissement, évaluée selon la méthode comptable prévue à l'article 1499 du code général des impôts. Par un jugement du 21 mai 2019, le tribunal administratif de Caen a rejeté ses demandes. La société Décoration Protection des Métaux s'est pourvue en cassation contre ce jugement. Par une décision du 27 décembre 2019, le Conseil d'Etat statuant au contentieux a prononcé l'admission des conclusions de son pourvoi en tant seulement que ce jugement a statué sur l'imposition des immobilisations référencées sous les numéros 141003 et 130017.<br/>
<br/>
              2. L'article 1380 du code général des impôts dispose : " La taxe foncière est établie annuellement sur les propriétés bâties sises en France à l'exception de celles qui en sont expressément exonérées par les dispositions du présent code ". Selon l'article 1381 du même code : " Sont également soumis à la taxe foncière sur les propriétés bâties : / 1° Les installations destinées à abriter des personnes ou des biens ou à stocker des produits ainsi que les ouvrages en maçonnerie présentant le caractère de véritables constructions tels que, notamment, les cheminées d'usine, les réfrigérants atmosphériques, les formes de radoub, les ouvrages servant de support aux moyens matériels d'exploitation ; / 2° Les ouvrages d'art et les voies de communication (...) ". Selon l'article 1382 du code général des impôts : " Sont exonérés de la taxe foncière sur les propriétés bâties : / (...) / 11° Les outillages et autres installations et moyens matériels d'exploitation des établissements industriels à l'exclusion de ceux visés aux 1° et 2° de l'article 1381 ". Aux termes du premier alinéa de l'article 1495 de ce code : " Chaque propriété ou fraction de propriété est appréciée d'après sa consistance, son affectation, sa situation et son état, à la date de l'évaluation ". Aux termes du II de l'article 324 B de l'annexe III au même code : " Pour l'appréciation de la consistance il est tenu compte de tous les travaux équipements ou éléments d'équipement existant au jour de l'évaluation ".<br/>
<br/>
              3. Pour apprécier, en application de l'article 1495 du code général des impôts et de l'article 324 B de son annexe III, la consistance des propriétés qui entrent, en vertu de ses articles 1380 et 1381, dans le champ de la taxe foncière sur les propriétés bâties, il est tenu compte, non seulement de tous les éléments d'assiette mentionnés par ces deux derniers articles mais également des biens faisant corps avec eux. Sont toutefois exonérés de cette taxe, en application du 11° de l'article 1382 du même code, ceux de ces biens qui font partie des outillages, autres installations et moyens matériels d'exploitation d'un établissement industriel, c'est-à-dire ceux de ces biens qui relèvent d'un établissement qualifié d'industriel au sens de l'article 1499, qui sont spécifiquement adaptés aux activités susceptibles d'être exercées dans un tel établissement et qui ne sont pas au nombre des éléments mentionnés aux 1° et 2° de l'article 1381.<br/>
<br/>
              4. Il est constant, tout d'abord, que les deux immobilisations restant en litige relèvent de l'établissement dont la société requérante est propriétaire sur le territoire de la commune de Perche-en-Nocé, dont il est également constant qu'il doit être regardé comme un établissement industriel au sens de l'article 1499 du code général des impôts. <br/>
<br/>
              5. Il ressort des pièces du dossier soumis aux juges du fond, ensuite, que les immobilisations en cause, référencées sous les numéros 141003 et 130017, sont constituées, pour la première, d'un transformateur électrique d'une puissance de 1250 kilowatts, inscrit en comptabilité pour une valeur de 55 720 euros et pour, la seconde, d'un tableau général basse tension et de ses câbles d'alimentation, inscrits pour une valeur de 107 306,90 euros. Il en ressort également que ces équipements assuraient la fourniture en courant électrique des lignes de production de l'établissement de Perche-en-Nocé qui, eu égard à son activité de traitement de surface des métaux, notamment par galvanoplastie, requérait une alimentation électrique spécifique.<br/>
<br/>
              6. Il ressort des pièces du dossier soumis aux juges du fond et n'est d'ailleurs pas contesté, enfin, que ni la première, ni la seconde des immobilisations restant en litige ne sont au nombre des éléments mentionnés aux 1° et 2° de l'article 1381 du code général des impôts. <br/>
<br/>
              7. Par suite, le tribunal administratif de Caen a commis une erreur de qualification juridique des faits en jugeant que les immobilisations référencées sous les numéros 141003 et 130017 ne pouvaient être regardées comme des biens exonérés de taxe foncière sur les propriétés bâties en application des dispositions du 11° de l'article 1382 du code général des impôts. La société Décoration Protection des Métaux est fondée, pour ce motif, et sans qu'il soit besoin d'examiner l'autre moyen soulevé sur ce point dans son pourvoi, à demander l'annulation du jugement attaqué, en tant qu'il a statué sur l'imposition des immobilisations référencées sous les numéros 141003 et 130017.<br/>
<br/>
              8. Il y a lieu, dans les circonstances de l'espèce, de régler, dans cette mesure, l'affaire au fond en application des dispositions de l'article L. 821-2 du code de justice administrative.<br/>
<br/>
              9. Ainsi qu'il a été dit ci-dessus, il est constant, tout d'abord, que les deux immobilisations restant en litige relèvent d'un établissement industriel au sens de l'article 1499 du code général des impôts. <br/>
<br/>
              10. Il résulte de l'instruction, ensuite, que la première des immobilisations en cause, référencée sous le numéro 141003, est constituée d'un transformateur électrique d'une puissance de 1250 kilowatts, alimenté en courant électrique de tension 20 000 volts, qu'il transforme en courant électrique de tension 400 volts afin de répondre aux besoins particuliers en électricité de l'établissement de la société. La seconde des immobilisations en cause, référencée sous le numéro 130017, est constituée d'un tableau général basse tension et de ses câbles d'alimentation, dimensionnés pour assurer la distribution du courant électrique venant du transformateur vers les neuf lignes de production de l'établissement de la société. Eu égard à ces caractéristiques, de tels biens sont spécifiquement adaptés aux activités susceptibles d'être exercées dans un établissement industriel au sens de l'article 1499 du code général des impôts. <br/>
<br/>
              11. Il résulte de l'instruction et n'est d'ailleurs pas contesté, enfin, que les immobilisations restant en litige ne sont pas au nombre des éléments mentionnés aux 1° et 2° de l'article 1381 du code général des impôts. <br/>
<br/>
              12. Il résulte des points 9 à 11 qui précèdent que les deux immobilisations restant en litige doivent être regardées comme des biens exonérés de taxe foncière sur les propriétés bâties en application des dispositions du 11° de l'article 1382 du code général des impôts. Par suite, la société Décoration Protection des Métaux est fondée à demander la réduction des cotisations supplémentaires de taxe foncière sur les propriétés bâties auxquelles elle a été assujettie au titre des années 2015 et 2016 à raison de son établissement de Perche-en-Nocé, correspondant à l'exclusion de ses bases d'imposition de ces deux immobilisations. <br/>
<br/>
              13. Il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat une somme de 3 000 euros à verser à la société Décoration Protection des Métaux au titre de l'article L. 761-1 du code de justice administrative<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Le jugement du tribunal administratif de Caen du 21 mai 2019 est annulé en tant qu'il se prononce sur l'imposition à la taxe foncière sur les propriétés bâties des immobilisations référencées sous les numéros 141003 et 130017.<br/>
Article 2 : La société Décoration Protection des Métaux est déchargée de la différence entre les montants des cotisations supplémentaires de taxe foncière sur les propriétés bâties auxquelles elle a été assujettie au titre des années 2015 et 2016 à raison de son établissement de Perche-en-Nocé et ceux résultant de l'exclusion de ses bases d'imposition des deux immobilisations mentionnées à l'article 1er de la présente décision. <br/>
Article 3 : L'Etat versera à la société Décoration Protection des Métaux une somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : La présente décision sera notifiée à la SAS Décoration Protection des Métaux et au ministre de l'économie, des finances et de la relance.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
