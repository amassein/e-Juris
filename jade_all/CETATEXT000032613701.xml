<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032613701</ID>
<ANCIEN_ID>JG_L_2016_05_000000389829</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/61/37/CETATEXT000032613701.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 2ème chambre, 30/05/2016, 389829, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-05-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>389829</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>2ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP COUTARD, MUNIER-APAIRE</AVOCATS>
<RAPPORTEUR>M. Paul Bernard</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Xavier Domino</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2016:389829.20160530</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme C...A...a demandé au tribunal administratif de Montpellier d'annuler pour excès de pouvoir l'arrêté du préfet de l'Hérault du 20 décembre 2013 lui refusant la délivrance d'un titre de séjour, l'obligeant à quitter le territoire français et fixant le pays de destination de la mesure d'éloignement. Par un jugement n° 1400638 du 6 mai 2014, le tribunal administratif a rejeté sa demande.<br/>
<br/>
              Par un arrêt n° 14MA02518, 14MA02967 du 11 décembre 2014, la cour administrative d'appel de Marseille a rejeté l'appel formé par Mme A...contre ce jugement.<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 28 avril et 27 juillet 2015 au secrétariat du contentieux du Conseil d'Etat, Mme A...demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler cet arrêt ; <br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à son appel ; <br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 2 500 euros à verser à la SCP Coutard et Munier-Apaire, son avocat, au titre des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la convention européenne de sauvegarde des droits de l'homme et des libertés fondamentales ;<br/>
              - la convention de Genève du 28 juillet 1951 relative au statut des réfugiés et le protocole signé à New York le 31 janvier 1967 ;<br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              - la loi  n° 91-647 du 10 juillet 1991 ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Paul Bernard, maître des requêtes, <br/>
<br/>
              - les conclusions de M. Xavier Domino, rapporteur public,<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Coutard, Munier-Apaire, avocat de Mme A...; <br/>
<br/>
              Vu la note en délibéré, enregistrée le 17 mai 2016, présentée par Mme A...;<br/>
<br/>
<br/>
<br/>
<br/>
<br/>
              1.	Considérant que Mme A...se pourvoit contre l'arrêt du 11 décembre 2014 par lequel la cour administrative d'appel de Marseille a rejeté l'appel qu'elle avait formé contre le jugement du tribunal administratif de Montpellier rejetant sa demande dirigée contre la décision du 20 décembre 2013 du préfet de l'Hérault refusant de lui délivrer un titre de séjour, lui faisant obligation de quitter le territoire français et fixant le pays de destination ;<br/>
<br/>
              2.	Considérant qu'aux termes de l'article L. 313-11 du code de l'entrée et du séjour des étrangers et du droit d'asile alors applicable : " Sauf si sa présence constitue une menace pour l'ordre public, la carte de séjour temporaire portant la mention "vie privée et familiale" est délivrée de plein droit : / 7° A l'étranger ne vivant pas en état de polygamie, qui n'entre pas dans les catégories précédentes ou dans celles qui ouvrent droit au regroupement familial, dont les liens personnels et familiaux en France, appréciés notamment au regard de leur intensité, de leur ancienneté et de leur stabilité, des conditions d'existence de l'intéressé, de son insertion dans la société française ainsi que de la nature de ses liens avec la famille restée dans le pays d'origine, sont tels que le refus d'autoriser son séjour porterait à son droit au respect de sa vie privée et familiale une atteinte disproportionnée au regard des motifs du refus, sans que la condition prévue à l'article L. 311-7 soit exigée. (...) ; " ; <br/>
<br/>
              3.	Considérant qu'il ressort des pièces soumises aux juges du fond que Mme A..., ressortissante turque, est entrée en France le 5 juillet 2012 via l'Allemagne sous couvert d'un visa touristique valable du 1er juillet 2012 au 1er septembre 2012, délivré par le consulat d'Allemagne en Turquie ; que son mariage en France avec M.B..., a été célébré le 14 août 2012, soit trois semaines à peine après son entrée sur le territoire national ; que Mme A... s'est maintenue irrégulièrement sur le territoire français malgré un précédent refus de titre de séjour assorti d'une obligation de quitter le territoire français opposé le 5 octobre 2012 ; que la requérante n'est pas dépourvue d'attache dans son pays d'origine, où elle a vécu jusqu'à l'âge de 28 ans et où résident ses parents et l'ensemble de sa fratrie ;<br/>
<br/>
              4.	Considérant que, par une appréciation souveraine des pièces du dossier, la cour administrative d'appel de Marseille a retenu que Mme A...n'établissait la réalité ni de son mariage avec M. B...en Turquie en 2008, ni de la poursuite de leurs relations jusqu'à leur mariage en France en 2012 ; que la seule mention manuscrite du nom de Mme A...sur une copie du formulaire de demande d'asile de M. B...ne permet pas d'estimer que l'appréciation que la cour a porté sur l'ensemble des éléments versés au dossier serait entachée de dénaturation ;<br/>
<br/>
              5.	Considérant qu'en déduisant de l'ensemble de ces éléments que le refus litigieux de délivrance du titre de séjour ne portait pas une atteinte disproportionnée au droit de l'intéressée au respect de sa vie familiale, la cour, qui a suffisamment motivé son arrêt, a exactement qualifié les faits de l'espèce ; qu'elle n'a, en outre et en tout état de cause, pas commis d'erreur de droit en écartant le moyen tiré de ce que le préfet aurait méconnu les principes généraux du droit applicables aux réfugiés ;  <br/>
<br/>
              6.	Considérant qu'il résulte de ce qui précède que Mme A...n'est pas fondée à demander l'annulation de l'arrêt de la cour administrative d'appel de Marseille ; que les conclusions présentées sur le fondement des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 par la SCP Coutard et Munier-Apaire, son avocat, ne peuvent, en conséquence, qu'être rejetées ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : Le pourvoi de Mme A...est rejeté.<br/>
Article 2 : La présente décision sera notifiée à Mme C...A...et au ministre de l'intérieur.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
