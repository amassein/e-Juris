<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043754064</ID>
<ANCIEN_ID>JG_L_2021_07_000000434665</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/75/40/CETATEXT000043754064.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 4ème - 1ère chambres réunies, 01/07/2021, 434665, Publié au recueil Lebon</TITRE>
<DATE_DEC>2021-07-01</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>434665</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>4ème - 1ère chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>A</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP HEMERY, THOMAS-RAQUIN, LE GUERER</AVOCATS>
<RAPPORTEUR>M. Edouard Solier</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Frédéric Dieu</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:434665.20210701</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
               Mme B... A... a demandé au tribunal administratif de Toulouse, d'une part, d'annuler seize titres de perception émis à son encontre, d'autre part, d'enjoindre à l'État de la décharger des sommes mises à sa charge par ces titres et de lui rembourser les sommes déjà acquittées, enfin de condamner l'État à lui verser la somme de 62 956,94 euros en réparation des préjudices qu'elle estime avoir subis. Par un jugement n° 1404130 du 24 mai 2017, le tribunal administratif a annulé ces titres de perception, déchargé Mme A... de l'obligation de payer les sommes correspondantes, enjoint à l'Etat de rembourser à Mme A... la somme de 4 722,80 euros dans le délai de quatre mois à compter de la notification de ce jugement, condamné l'État à verser à Mme A... la somme de 1 000 euros en réparation de son préjudice moral, et rejeté le surplus des conclusions de Mme A....<br/>
<br/>
               Par un arrêt n° 17BX02616 du 16 juillet 2019, la cour administrative d'appel de Bordeaux a rejeté l'appel formé par le ministre de l'éducation nationale et de la jeunesse tendant à l'annulation de ce jugement en tant qu'il a, d'une part, déchargé Mme A... du paiement des sommes mises à sa charge par les titres annulés, d'autre part enjoint à l'Etat de rembourser à Mme A... la somme de 4 722,80 euros.<br/>
<br/>
               Par un pourvoi, enregistré le 17 septembre 2019 au secrétariat du contentieux du Conseil d'Etat, le ministre de l'éducation nationale et de la jeunesse demande au Conseil d'Etat d'annuler cet arrêt.<br/>
<br/>
<br/>
               Vu les autres pièces du dossier ;<br/>
<br/>
               Vu : <br/>
               - le code civil ;<br/>
               - la loi n° 2000-321 du 12 avril 2000 ;<br/>
               - la loi n° 2011-1978 du 28 décembre 2011 ; <br/>
               - le code de justice administrative ;<br/>
<br/>
               Après avoir entendu en séance publique :<br/>
<br/>
               - le rapport de M. Edouard Solier, maître des requêtes,  <br/>
<br/>
               - les conclusions de M. Frédéric Dieu, rapporteur public ;<br/>
<br/>
               La parole ayant été donnée, après les conclusions, à la SCP Hémery, Thomas-Raquin, Le Guerer, avocat de Mme B... A... ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
               1. Il ressort des pièces du dossier soumis aux juges du fond que Mme B... A..., affectée en sa qualité d'agent non titulaire du ministère de l'éducation nationale au rectorat de Toulouse et placée en congé de maladie ordinaire au mois de mai 2009 puis en congé de longue maladie à compter du 15 mars 2010, a continué à percevoir sa rémunération tout en percevant des indemnités journalières de la sécurité sociale. En 2011 puis en 2013, l'administration a émis à l'encontre de Mme A... seize titres de perception, d'un montant total de 18 640,81 euros, au titre de ce trop-perçu de rémunération. Par un jugement du 24 mai 2017, le tribunal administratif de Toulouse a annulé ces titres de perception, déchargé Mme A... de l'obligation de payer les sommes correspondantes et enjoint à l'Etat de lui reverser la somme de 4 722,80 euros. Par un arrêt du 16 juillet 2019, la cour administrative d'appel de Bordeaux a rejeté l'appel du ministre de l'éducation nationale et de la jeunesse tendant à l'annulation de ce jugement en tant qu'il a déchargé Mme A... du paiement des sommes mises à sa charge par les titres annulés et enjoint à l'Etat de reverser à cette dernière la somme de 4 722,80 euros. Le ministre se pourvoit contre en cassation contre cet arrêt.<br/>
<br/>
               2. D'une part, l'annulation par une décision juridictionnelle d'un titre exécutoire pour un motif de régularité en la forme ou de l'incompétence de son auteur n'implique pas nécessairement que les sommes perçues par l'administration sur le fondement du titre ainsi dépourvu de base légale soient immédiatement restituées à l'intéressé, dès lors qu'il est loisible à l'administration, si elle s'y croit fondée et si, en particulier, aucune règle de prescription n'y fait obstacle, de reprendre régulièrement et dans le respect de l'autorité de la chose jugée, sous le contrôle du juge, une nouvelle décision. Lorsque tout ou partie de l'indu a été recouvré, il appartient au juge, s'il est saisi de conclusions tendant à ce qu'il soit enjoint à l'administration de rembourser la somme déjà recouvrée, de déterminer le délai dans lequel l'administration, en exécution de sa décision, doit procéder à ce remboursement, sauf à régulariser sa décision de récupération si celle-ci n'a été annulée que pour un vice de forme ou de procédure. <br/>
<br/>
               3. D'autre part, aux termes de l'article 37-1 de la loi du 12 avril 2000, dans sa rédaction issue de l'article 94 de la loi du 28 décembre 2011 portant loi de finances rectificative pour 2011 : " Les créances résultant de paiements indus effectués par les personnes publiques en matière de rémunération de leurs agents peuvent être répétées dans un délai de deux années à compter du premier jour du mois suivant celui de la date de mise en paiement du versement erroné, y compris lorsque ces créances ont pour origine une décision créatrice de droits irrégulière devenue définitive (...) ".<br/>
<br/>
               4. Il résulte de ces dispositions qu'une somme indûment versée par une personne publique à l'un de ses agents au titre de sa rémunération peut, en principe, être répétée dans un délai de deux ans à compter du premier jour du mois suivant celui de sa date de mise en paiement sans que puisse y faire obstacle la circonstance que la décision créatrice de droits qui en constitue le fondement ne peut plus être retirée. <br/>
<br/>
               5. Sauf dispositions spéciales, les règles fixées par l'article 37-1 de la loi du 12 avril 2000 sont applicables à l'ensemble des sommes indûment versées par des personnes publiques à leurs agents à titre de rémunération. En l'absence de toute autre disposition applicable, les causes d'interruption et de suspension de la prescription biennale instituée par les dispositions de cet article 37-1 sont régies par les principes dont s'inspirent les dispositions du titre XX du livre III du code civil. <br/>
<br/>
               6. Il résulte des principes dont s'inspirent les dispositions des articles 2241 et 2242 du code civil, tels qu'applicables aux rapports entre une personne publique et un de ses agents, qu'un recours juridictionnel, quel que soit l'auteur du recours, interrompt le délai de prescription et que l'interruption du délai de prescription par cette demande en justice produit ses effets jusqu'à l'extinction de l'instance.<br/>
<br/>
               7. Il s'ensuit qu'en jugeant que la prescription biennale instituée par l'article 37-1 de la loi du 12 avril 2000, applicable aux créances afférentes aux trop-perçus de rémunération de Mme A..., ne pouvait avoir été interrompue par le recours contentieux formé par l'intéressée tendant à l'annulation des titres de perception émis par l'administration en vue du recouvrement de ces créances, et en en déduisant qu'aucune régularisation des titres de perception annulés n'était possible, la cour administrative d'appel de Bordeaux a commis une erreur de droit. <br/>
<br/>
               8. Il résulte de ce qui précède que le ministre de l'éducation nationale et de la jeunesse est fondé à demander l'annulation de l'arrêt qu'il attaque.<br/>
<br/>
               9. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
       ---------------<br/>
Article 1er : L'arrêt de la cour administrative d'appel de Bordeaux du 16 juillet 2019 est annulé.<br/>
Article 2 : L'affaire est renvoyée devant la cour administrative d'appel de Bordeaux.<br/>
Article 3 : Les conclusions de Mme A... présentées au titre de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 4 : La présente décision sera notifiée au ministre de l'éducation nationale, de la jeunesse et des sports et à Mme B... A....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">01-01-06-02-01 ACTES LÉGISLATIFS ET ADMINISTRATIFS. - DIFFÉRENTES CATÉGORIES D'ACTES. - ACTES ADMINISTRATIFS - CLASSIFICATION. - ACTES INDIVIDUELS OU COLLECTIFS. - ACTES CRÉATEURS DE DROITS. - DÉCISION OCTROYANT UNE RÉMUNÉRATION À UN AGENT PUBLIC - RÉPÉTITION D'UNE SOMME INDÛMENT VERSÉE PAR UNE PERSONNE PUBLIQUE À L'UN DE SES AGENTS AU TITRE DE SA RÉMUNÉRATION - DÉLAI DE RÉPÉTITION - 1) DÉLAI DE DEUX ANS, EN PRINCIPE [RJ1] - 2) CAUSES D'INTERRUPTION ET DE SUSPENSION - CAUSES RÉGIES PAR LES PRINCIPES DONT S'INSPIRE LE TITRE XX DU LIVRE III DU CODE CIVIL [RJ1] - CONSÉQUENCE - INTERRUPTION DU DÉLAI DE PRESCRIPTION PAR UN RECOURS JURIDICTIONNEL, QUEL QU'EN SOIT L'AUTEUR.
</SCT>
<SCT ID="8B" TYPE="PRINCIPAL">18-03-02 COMPTABILITÉ PUBLIQUE ET BUDGET. - CRÉANCES DES COLLECTIVITÉS PUBLIQUES. - RECOUVREMENT. - DÉCISION OCTROYANT UNE RÉMUNÉRATION À UN AGENT PUBLIC - RÉPÉTITION D'UNE SOMME INDÛMENT VERSÉE PAR UNE PERSONNE PUBLIQUE À L'UN DE SES AGENTS AU TITRE DE SA RÉMUNÉRATION - DÉLAI DE RÉPÉTITION - 1) DÉLAI DE DEUX ANS, EN PRINCIPE [RJ1] - 2) CAUSES D'INTERRUPTION ET DE SUSPENSION - CAUSES RÉGIES PAR LES PRINCIPES DONT S'INSPIRE LE TITRE XX DU LIVRE III DU CODE CIVIL [RJ1] - CONSÉQUENCE - INTERRUPTION DU DÉLAI DE PRESCRIPTION PAR UN RECOURS JURIDICTIONNEL, QUEL QU'EN SOIT L'AUTEUR.
</SCT>
<SCT ID="8C" TYPE="PRINCIPAL">36-08-01 FONCTIONNAIRES ET AGENTS PUBLICS. - RÉMUNÉRATION. - QUESTIONS D'ORDRE GÉNÉRAL. - DÉCISION OCTROYANT UNE RÉMUNÉRATION À UN AGENT PUBLIC - RÉPÉTITION D'UNE SOMME INDÛMENT VERSÉE PAR UNE PERSONNE PUBLIQUE À L'UN DE SES AGENTS AU TITRE DE SA RÉMUNÉRATION - DÉLAI DE RÉPÉTITION - 1) DÉLAI DE DEUX ANS, EN PRINCIPE [RJ1] - 2) CAUSES D'INTERRUPTION ET DE SUSPENSION - CAUSES RÉGIES PAR LES PRINCIPES DONT S'INSPIRE LE TITRE XX DU LIVRE III DU CODE CIVIL [RJ1] - CONSÉQUENCE - INTERRUPTION DU DÉLAI DE PRESCRIPTION PAR UN RECOURS JURIDICTIONNEL, QUEL QU'EN SOIT L'AUTEUR.
</SCT>
<ANA ID="9A"> 01-01-06-02-01 1) Il résulte de l'article 37-1 de la loi n° 2000-321 du 12 avril 2000, dans sa rédaction issue de l'article 94 de la loi n° 2011-1978 du 28 décembre 2011, qu'une somme indûment versée par une personne publique à l'un de ses agents au titre de sa rémunération peut, en principe, être répétée dans un délai de deux ans à compter du premier jour du mois suivant celui de sa date de mise en paiement sans que puisse y faire obstacle la circonstance que la décision créatrice de droits qui en constitue le fondement ne peut plus être retirée.......Sauf dispositions spéciales, les règles fixées par l'article 37-1 de la loi du 12 avril 2000 sont applicables à l'ensemble des sommes indûment versées par des personnes publiques à leurs agents à titre de rémunération. ......2) En l'absence de toute autre disposition applicable, les causes d'interruption et de suspension de la prescription biennale instituée par les dispositions de cet article 37-1 sont régies par les principes dont s'inspire le titre XX du livre III du code civil. ......Il résulte des principes dont s'inspirent les articles 2241 et 2242 du code civil, tels qu'applicables aux rapports entre une personne publique et un de ses agents, qu'un recours juridictionnel, quel que soit l'auteur du recours, interrompt le délai de prescription et que l'interruption du délai de prescription par cette demande en justice produit ses effets jusqu'à l'extinction de l'instance.</ANA>
<ANA ID="9B"> 18-03-02 1) Il résulte de l'article 37-1 de la loi n° 2000-321 du 12 avril 2000, dans sa rédaction issue de l'article 94 de la loi n° 2011-1978 du 28 décembre 2011, qu'une somme indûment versée par une personne publique à l'un de ses agents au titre de sa rémunération peut, en principe, être répétée dans un délai de deux ans à compter du premier jour du mois suivant celui de sa date de mise en paiement sans que puisse y faire obstacle la circonstance que la décision créatrice de droits qui en constitue le fondement ne peut plus être retirée.......Sauf dispositions spéciales, les règles fixées par l'article 37-1 de la loi du 12 avril 2000 sont applicables à l'ensemble des sommes indûment versées par des personnes publiques à leurs agents à titre de rémunération. ......2) En l'absence de toute autre disposition applicable, les causes d'interruption et de suspension de la prescription biennale instituée par les dispositions de cet article 37-1 sont régies par les principes dont s'inspire le titre XX du livre III du code civil. ......Il résulte des principes dont s'inspirent les articles 2241 et 2242 du code civil, tels qu'applicables aux rapports entre une personne publique et un de ses agents, qu'un recours juridictionnel, quel que soit l'auteur du recours, interrompt le délai de prescription et que l'interruption du délai de prescription par cette demande en justice produit ses effets jusqu'à l'extinction de l'instance.</ANA>
<ANA ID="9C"> 36-08-01 1) Il résulte de l'article 37-1 de la loi n° 2000-321 du 12 avril 2000, dans sa rédaction issue de l'article 94 de la loi n° 2011-1978 du 28 décembre 2011, qu'une somme indûment versée par une personne publique à l'un de ses agents au titre de sa rémunération peut, en principe, être répétée dans un délai de deux ans à compter du premier jour du mois suivant celui de sa date de mise en paiement sans que puisse y faire obstacle la circonstance que la décision créatrice de droits qui en constitue le fondement ne peut plus être retirée.......Sauf dispositions spéciales, les règles fixées par l'article 37-1 de la loi du 12 avril 2000 sont applicables à l'ensemble des sommes indûment versées par des personnes publiques à leurs agents à titre de rémunération. ......2) En l'absence de toute autre disposition applicable, les causes d'interruption et de suspension de la prescription biennale instituée par les dispositions de cet article 37-1 sont régies par les principes dont s'inspire le titre XX du livre III du code civil. ......Il résulte des principes dont s'inspirent les articles 2241 et 2242 du code civil, tels qu'applicables aux rapports entre une personne publique et un de ses agents, qu'un recours juridictionnel, quel que soit l'auteur du recours, interrompt le délai de prescription et que l'interruption du délai de prescription par cette demande en justice produit ses effets jusqu'à l'extinction de l'instance.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf. CE, 31 mars 2017, Mme Dittoo et Mme Holterbosch, n° 405797, p. 104..</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
