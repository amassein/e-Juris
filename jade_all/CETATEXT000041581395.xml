<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000041581395</ID>
<ANCIEN_ID>JG_L_2020_02_000000431623</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/41/58/13/CETATEXT000041581395.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème chambre, 12/02/2020, 431623, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-02-12</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>431623</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème chambre</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP GADIOU, CHEVALLIER ; SCP LEVIS</AVOCATS>
<RAPPORTEUR>M. Réda Wadjinny-Green</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Alexandre Lallet</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2020:431623.20200212</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. E... C... a demandé au juge des référés du tribunal administratif de Nice, sur le fondement de l'article L. 521-2 du code de justice administrative, d'ordonner à l'Office français de l'immigration et de l'intégration le rétablissement du bénéfice de l'allocation pour demandeurs d'asile. <br/>
<br/>
              Par une ordonnance n° 1902304 du 20 mai 2019, le juge des référés du tribunal administratif de Nice a, sur le fondement de l'article L. 522-3 du code de justice administrative, rejeté sa demande.<br/>
<br/>
              Par un pourvoi et un nouveau mémoire, enregistrés les 11 juin et 9 juillet 2019 au secrétariat du contentieux du Conseil d'Etat, M. C... demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) de mettre à la charge de l'Office français de l'immigration et de l'intégration la somme de 2 000 euros à verser à la SCP Gadiou, Chevallier, son avocat, au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu :<br/>
              - le règlement (CE) no 1560/2003 de la Commission du 2 septembre 2003 ;<br/>
              - le règlement (UE) no 604/2013 du Parlement européen et du Conseil du 26 juin 2013 ;<br/>
              - le règlement d'exécution (UE) no 118/2014 de la Commission du 30 janvier 2014 ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ;<br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Réda Wadjinny-Green, auditeur, <br/>
<br/>
              - les conclusions de M. Alexandre Lallet, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Gadiou, Chevallier avocat de M. C... et à la SCP Levis avocat de l'OFII ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis au juge des référés que M. C... et son épouse, Mme A... D..., accompagnés de leurs deux enfants mineurs, ainsi que la mère de M. C..., Mme B... D..., ressortissants russes, ont déposé une première demande d'asile le 18 juillet 2017 et ont bénéficié à ce titre d'une prise en charge de leurs conditions matérielles d'accueil par l'Office français de l'immigration et de l'intégration. Par deux décisions du 17 janvier 2018 et une décision du 30 janvier 2018, devenues définitives à la suite des décisions de la Cour nationale du droit d'asile du 8 octobre 2018, l'Office français de protection des réfugiés et apatrides (OFPRA) a rejeté leurs demandes d'asile. M. C... et Mmes D... ont déposé de nouvelles demandes d'asile qui ont été enregistrées en procédure accélérée. Par deux décisions des 9 janvier et 1er avril 2019, l'Office français de l'immigration et de l'intégration a refusé le bénéfice des conditions matérielles d'accueil des demandeurs d'asile respectivement à Mme B... D... et à M. C..., Mme A... D... et leurs enfants. M. C... a demandé au juge des référés du tribunal administratif de Nice, sur le fondement des dispositions de l'article L. 521 2 du code de justice administrative, d'ordonner à l'Office français de l'immigration et de l'intégration de rétablir le bénéfice de l'allocation pour demandeurs d'asile pour sa famille. M. C... se pourvoit en cassation contre l'ordonnance en date du 20 mai 2019 par laquelle le juge des référés du tribunal administratif de Nice a, sur le fondement des dispositions de l'article L. 522-3 du code de justice administrative, rejeté sa demande.<br/>
<br/>
              2. Le premier alinéa de l'article L. 744-1 du code de l'entrée et du séjour des étrangers et du droit d'asile dispose que : " Les conditions matérielles d'accueil du demandeur d'asile, au sens de la directive 2013/33/UE du Parlement européen et du Conseil, du 26 juin 2013, établissant des normes pour l'accueil des personnes demandant la protection internationale, sont proposées à chaque demandeur d'asile par l'Office français de l'immigration et de l'intégration après l'enregistrement de la demande d'asile par l'autorité administrative compétente, en application du présent chapitre. Les conditions matérielles d'accueil comprennent les prestations et l'allocation prévues au présent chapitre ". L'article L. 744-9 de ce code prévoit que " Le demandeur d'asile qui a accepté les conditions matérielles d'accueil proposées en application de l'article L. 744-1 bénéficie d'une allocation pour demandeur d'asile s'il satisfait à des conditions d'âge et de ressources, dont le versement est ordonné par l'Office français de l'immigration et de l'intégration. / Le versement de l'allocation prend fin au terme du mois au cours duquel le droit du demandeur de se maintenir sur le territoire français dans les conditions prévues aux articles L. 743 1 et L. 743-2 a pris fin ou à la date du transfert effectif vers un autre Etat si sa demande relève de la compétence de cet Etat. (...) ". Aux termes de l'article L. 743-1 de ce code : " Le demandeur d'asile dont l'examen de la demande relève de la compétence de la France et qui a introduit sa demande auprès de l'Office français de protection des réfugiés et apatrides bénéficie du droit de se maintenir sur le territoire français jusqu'à la notification de la décision de l'office ou, si un recours a été formé, dans le délai prévu à l'article L. 731-2 contre une décision de rejet de l'office, soit jusqu'à la date de la lecture en audience publique de la décision de la Cour nationale du droit d'asile, soit, s'il est statué par ordonnance, jusqu'à la date de la notification de celle-ci. L'attestation délivrée en application de l'article L. 741-1, dès lors que la demande d'asile a été introduite auprès de l'office, vaut autorisation provisoire de séjour et est renouvelable jusqu'à ce que l'office et, le cas échéant, la cour statuent ". Aux termes de l'article L. 744-8 du même code : " Outre les cas, mentionnés à l'article L. 744-7, dans lesquels il est immédiatement mis fin de plein droit au bénéfice des conditions matérielles d'accueil, le bénéfice de celles-ci peut être : (...) 2° Refusé si le demandeur présente une demande de réexamen de sa demande d'asile (...) ". Le dernier alinéa de cet article précise que " La décision de retrait des conditions matérielles d'accueil prise en application du présent article est écrite et motivée. Elle prend en compte la vulnérabilité du demandeur. Elle est prise après que l'intéressé a été mis en mesure de présenter ses observations écrites selon des modalités définies par décret ". Aux termes du deuxième alinéa de l'article L. 744-6 de ce code : " L'évaluation de la vulnérabilité vise, en particulier, à identifier les mineurs, les mineurs non accompagnés, les personnes en situation de handicap, les personnes âgées, les femmes enceintes, les parents isolés accompagnés d'enfants mineurs, les victimes de la traite des êtres humains, les personnes atteintes de maladies graves, les personnes souffrant de troubles mentaux et les personnes qui ont subi des tortures, des viols ou d'autres formes graves de violence psychologique, physique ou sexuelle, telles que des mutilations sexuelles féminines ". <br/>
<br/>
              3. Il ressort des énonciations de l'ordonnance attaquée que, pour rejeter, sur le fondement des dispositions de l'article L. 522-3 du code de justice administrative, la demande de M. C... comme manifestement infondée, le juge des référés du tribunal administratif de Nice a estimé, d'une part, que la situation de vulnérabilité de la famille de M. C... n'était pas étayée et, d'autre part, qu'il existait une incertitude sur le statut de demandeur d'asile de la famille dès lors que l'attestation de demandeur d'asile de Mme B... D... n'avait pas été renouvelée. Toutefois, il ressort des pièces du dossier soumis au juge des référés que d'une part, M. C... ne disposait d'aucune ressource alors qu'il avait à sa charge deux enfants mineurs ainsi que sa mère, et d'autre part, l'attestation de demande d'asile de Mme B... D... avait été renouvelée le 3 mai 2019 et était valable jusqu'au 2 novembre 2019. Par suite, le juge des référés a dénaturé les pièces du dossier. Il résulte de ce qui précède, sans qu'il soit besoin de se prononcer sur l'autre moyen du pourvoi, que M. C... est fondé à demander l'annulation de l'ordonnance qu'il attaque.<br/>
<br/>
              4. Il y a lieu, dans les circonstances de l'espèce, de régler l'affaire au titre de la procédure de référé engagée par M. C..., en application des dispositions de l'article L. 821-2 du code de justice administrative.<br/>
<br/>
              5. Par des ordonnances en date des 19 juillet et 28 août 2019 et enfin par une ordonnance du 17 mai 2019, postérieure au dernier renouvellement de l'attestation de demande d'asile de Mme B... D..., la Cour nationale du droit d'asile a rejeté les demandes présentées respectivement par M. C..., Mme A... D... et Mme B... D... tendant à l'annulation des décisions respectivement des 30 avril 2019 et 31 janvier 2019 par lesquelles l'OFPRA a rejeté leurs demandes de réexamen de leurs demandes d'asile. M. C... et Mmes D... n'ayant plus la qualité de demandeurs d'asile, la demande de M. C... tendant à ce que le juge des référés ordonne, sur le fondement des dispositions de l'article L. 521-2 du code de justice administrative, à l'Office français de l'immigration et de l'intégration de rétablir le versement de l'allocation pour demandeur d'asile pour sa famille ne peut dès lors qu'être rejetée, y compris, par voie de conséquence, les conclusions présentées au titre des articles 37 de la loi du 10 juillet 1991 et L. 761-1 du code de justice administrative. Il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées sur le fondement de l'article L. 761-1 du code de justice administrative par l'Office français de l'immigration et de l'intégration.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
<br/>
Article 1er : L'ordonnance du 20 mai 2019 du juge des référés du tribunal administratif de Nice est annulée.<br/>
Article 2 : La demande présentée par M. C... devant le juge des référés du tribunal administratif de Nice ainsi que ses conclusions présentées au titre des articles 37 de la loi du 10 juillet 1991 et L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 3 : Les conclusions présentées par l'Office français de l'immigration et de l'intégration au titre de l'article L. 761-1 du code de justice administrative sont rejetées. <br/>
Article 4 : La présente décision sera notifiée à M. E... C... et à l'Office français de l'immigration et de l'intégration.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
