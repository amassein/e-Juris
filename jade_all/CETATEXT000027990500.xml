<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000027990500</ID>
<ANCIEN_ID>JG_L_2013_08_000000370831</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/27/99/05/CETATEXT000027990500.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 27/08/2013, 370831, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-08-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>370831</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP PEIGNOT, GARREAU, BAUER-VIOLAS</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2013:370831.20130827</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 2 août 2013 au secrétariat du contentieux du Conseil d'Etat, présentée pour la société Mercedes-Benz France, dont le siège social est Parc de Rocquencourt à Rocquencourt (78150) ; la société Mercedes-Benz France demande au juge des référés du Conseil d'Etat :<br/>
<br/>
              1°) d'ordonner, sur le fondement de l'article L. 521-1 du code de justice administrative, la suspension de l'exécution de la décision du 26 juillet 2013 par laquelle le ministre de l'écologie, du développement durable et de l'énergie a refusé l'immatriculation sur le territoire français des véhicules du constructeur Daimler ayant les numéros de réception communautaires e1*98/14*0169*19 (type 230) du 6 juin 2013 à l'exception de la version SZBBA200 et e1*2001/116*0470/04 (type 245G) du 3 juin 2013 à l'exception des versions de la variante Y2GBM2, pour une durée maximale de six mois ;<br/>
<br/>
              2°) d'enjoindre au ministre de l'écologie, du développement durable et de l'énergie de lui délivrer les codes nationaux d'identification du type (CNIT) des véhicules concernés, permettant l'immatriculation des véhicules des types 230 et 245 G de la marque Mercedes-Benz, et de cesser toute entrave à l'immatriculation de ces véhicules, dans le délai de deux jours à compter de la notification de l'ordonnance à intervenir et sous astreinte de 1 000 euros par jour de retard ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat le versement de la somme de 6 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              elle soutient que le Conseil d'Etat est compétent en premier et dernier ressort pour statuer sur sa demande de suspension ; que la condition d'urgence est remplie ; que la décision contestée porte, en effet, une atteinte grave et immédiate à ses intérêts, en lui causant un grave préjudice financier, commercial et d'image ; qu'elle porte également gravement atteinte aux intérêts de son réseau de distributeurs ; qu'elle porte atteinte à l'intérêt public qui s'attache à la libre circulation des biens au sein du marché européen ; qu'il existe un doute sérieux quant à la légalité de la décision contestée ; qu'en effet, la libre circulation organisée par la directive 2007/46/CE du 5 septembre 2007 établissant un cadre pour la réception des véhicules à moteur s'oppose à ce que soit refusée l'immatriculation de véhicules ayant fait l'objet d'une réception communautaire, sauf mise en oeuvre d'une clause de sauvegarde strictement encadrée ; qu'il n'appartient pas aux autorités françaises de prendre des mesures correctives afin de préserver les objectifs assignés aux Etats membres par la directive 2006/40/CE du 17 mai 2006 ; que la décision contestée est entachée d'un détournement de procédure ; que la décision méconnaît les dispositions de l'article R. 321-14 du code de la route, les motifs retenus par le ministre étant inopérants ou insuffisants à justifier l'application de la clause de sauvegarde ; que la décision est entachée d'une rétroactivité illégale ; qu'elle méconnaît le principe d'égalité et est entachée de discrimination illégale dès lors que d'autres constructeurs ont pu obtenir de l'autorité nationale française compétente la réception communautaire pour des véhicules utilisant le même gaz de réfrigération ; <br/>
<br/>
              Vu la décision dont la suspension de l'exécution est demandée ;<br/>
<br/>
              Vu la copie de la requête à fin d'annulation de cette décision ; <br/>
              Vu le mémoire en défense, enregistré le 22 août 2013, présenté par le ministre de l'écologie, du développement durable et de l'énergie, qui conclut au rejet de la requête ; il soutient que la condition d'urgence n'est pas remplie ; que la société requérante ne justifie pas de la réalité des préjudices qu'elle invoque ; que la société requérante ne peut se prévaloir, pour justifier de l'urgence, de la situation dans laquelle elle s'est elle-même placée ; que l'intérêt public s'attache au maintien de l'exécution de la décision contestée, compte tenu de ce que la dangerosité du nouveau fluide n'est pas établie, que la décision a pour effet de rétablir la concurrence qui a été altérée par la situation créée par la société requérante et que l'absence d'utilisation du nouveau gaz porte atteinte à l'environnement ; que la décision contestée est à bon droit fondée sur le motif tiré de la nuisance grave à l'environnement prévu à l'article R. 321-14 du code de la route ; que la décision contestée n'est pas entachée d'une rétroactivité illégale ; que la décision contestée ne méconnaît pas le principe d'égalité ;<br/>
<br/>
              Vu les observations complémentaires, enregistrées le 22 août 2013, présentées pour la société Mercedes-Benz France, qui reprend les conclusions de sa requête ;<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le traité sur l'Union européenne ;<br/>
<br/>
              Vu le traité sur le fonctionnement de l'Union européenne ;<br/>
<br/>
              Vu la directive 2006/40/CE du Parlement européen et du Conseil du 17 mai 2006 concernant les émissions provenant des systèmes de climatisation des véhicules à moteurs ;<br/>
<br/>
              Vu la directive 2007/46/CE du Parlement européen et du Conseil du 5 septembre 2007 établissant un cadre pour la réception des véhicules à moteurs, de leurs remorques et des systèmes, des composants et des entités techniques destinés à ces véhicules ;<br/>
<br/>
              Vu le code de la route ;<br/>
<br/>
              Vu l'arrêté du 21 décembre 2007 relatif à la réception des véhicules automobiles en ce qui concerne les systèmes de climatisation ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, la société Mercedes-Benz France, d'autre part, le ministre de l'écologie, du développement durable et de l'énergie ; <br/>
              Vu le procès-verbal de l'audience publique du 23 août 2013 à 10 heures au cours de laquelle ont été entendus : <br/>
<br/>
              - Me Garreau, avocat au Conseil d'Etat et à la Cour de cassation, avocat de la société Mercedes-Benz France ;<br/>
<br/>
              - les représentants de la société Mercedes-Benz France ;<br/>
              - les représentants du ministre de l'écologie, du développement durable et de l'énergie ;<br/>
<br/>
              et à l'issue de laquelle l'instruction a été close ;<br/>
<br/>
<br/>
<br/>1. Considérant qu'aux termes du premier alinéa de l'article L. 521-1 du code de justice administrative : " Quand une décision administrative, même de rejet, fait l'objet d'une requête en annulation ou en réformation, le juge des référés, saisi d'une demande en ce sens, peut ordonner la suspension de l'exécution de cette décision, ou de certains de ses effets, lorsque l'urgence le justifie et qu'il est fait état d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision " ; <br/>
<br/>
              2. Considérant que la société Mercedes-Benz France a saisi le juge des référés du Conseil d'Etat d'une demande, fondée sur ces dispositions de l'article L. 521-1 du code de justice administrative, tendant à la suspension de l'exécution de la décision prise le 26 juillet 2013 par le ministre de l'écologie, du développement durable et de l'énergie refusant à titre temporaire l'immatriculation en France de certains véhicules produits par la société Daimler AG ;<br/>
<br/>
              Sur les dispositions applicables au litige :<br/>
<br/>
              3. Considérant, d'une part, que la directive 2007/46/CE du Parlement européen et du Conseil du 5 septembre 2007 établit un cadre harmonisé pour la réception des véhicules neufs à moteur ainsi que des systèmes, des composants et des entités techniques destinés à ces véhicules, en vue de faciliter leur immatriculation, leur vente et leur mise en service dans toute l'Union européenne ; qu'elle organise à cette fin des procédures de réception CE par type de véhicules ; que ces procédures conduisent les autorités compétentes désignées par chaque Etat membre à accorder ou refuser la réception CE par type au vu des exigences de la directive ; que les réceptions accordées peuvent faire l'objet de modifications, sous forme de révision ou d'extension, décidées par l'autorité compétente de l'Etat membre qui avait accordé la réception initiale ; que les véhicules fabriqués conformément au type de véhicule réceptionné font l'objet d'un certificat de conformité délivré par le constructeur ; que les autres Etats membres ne peuvent s'opposer à l'immatriculation, à la vente ou à la mise en service de véhicules accompagnés d'un certificat de conformité en cours de validité qu'en vertu des clauses de sauvegarde figurant aux articles 29 et 30 de la directive ;<br/>
<br/>
              4. Considérant qu'aux termes de l'article 29 de la directive : " 1. Si un Etat membre considère que de nouveaux véhicules, systèmes, composants ou entités techniques compromettent gravement la sécurité routière ou nuisent fortement à l'environnement ou à la santé publique bien qu'ils respectent les exigences applicables ou soient marqués d'une façon adéquate, cet Etat membre peut, pendant six mois au maximum, refuser d'immatriculer de tels véhicules ou d'autoriser la vente ou la mise en service sur son territoire de tels véhicules, composants ou entités techniques. / Dans de tels cas, l'Etat membre concerné en informe immédiatement le constructeur, les autres Etats membres et la Commission, en motivant sa décision et en indiquant en particulier si elle découle: / - de lacunes dans les actes réglementaires applicables, ou / - de l'application incorrecte des exigences applicables. / 2. La Commission consulte les parties concernées dans les meilleurs délais, et notamment l'autorité compétente en matière de réception qui a accordé la réception par type, afin de préparer une décision (...) " ; <br/>
<br/>
              5. Considérant qu'en vertu de l'article R. 321-11 du code de la route, " Tout véhicule dont le type a fait l'objet d'une réception CE et qui est muni d'un certificat de conformité valide peut être librement commercialisé et mis en circulation " ; que l'article R. 321-14 du code de la route, qui procède à la transposition des objectifs poursuivis par l'article 29 de la directive, dispose que : " S'il est établi que des véhicules, systèmes ou équipements d'un type ayant fait l'objet d'une réception CE compromettent gravement la sécurité routière ou nuisent gravement à l'environnement ou à la santé publique alors qu'ils sont accompagnés d'un certificat de conformité en cours de validité ou qu'ils portent une marque de réception valide, le ministre chargé des transports peut, pour une durée de six mois au maximum, refuser d'immatriculer ces véhicules ou interdire la vente ou la mise en service de ces véhicules, systèmes ou équipements. Il en informe immédiatement le constructeur et les autorités compétentes en matière de réception des autres Etats et la Commission européenne en motivant sa décision. La décision doit également être notifiée au constructeur intéressé et indiquer les voies et délais de recours " ;<br/>
<br/>
              6. Considérant, d'autre part, que la directive 2006/40/CE du Parlement européen et du Conseil du 17 mai 2006 concernant les émissions provenant des systèmes de climatisation des véhicules à moteur a prévu, au paragraphe 4 de son article 5, que les Etats membres n'accordent plus la réception CE d'un type de véhicule équipé d'un système de climatisation conçu pour contenir des gaz à effet de serre fluorés dont le potentiel de réchauffement planétaire est supérieur à 150 à compter du 1er janvier 2011 ; que cette date d'effet a été reportée au 1er janvier 2013 en raison des difficultés d'approvisionnement constatées à compter de 2011 pour le seul gaz commercialisé répondant aux exigences de la directive ; que le paragraphe 5 du même article 5 a retenu la date du 1er janvier 2017 pour imposer à l'ensemble des véhicules neufs, qu'ils relèvent ou non d'un nouveau type de véhicules, l'utilisation d'un gaz au potentiel de réchauffement planétaire inférieur à 150 ; <br/>
<br/>
              Sur le litige :<br/>
<br/>
              7. Considérant qu'il résulte des éléments produits dans le cadre de l'instruction de la demande de suspension présentée au juge des référés du Conseil d'Etat, ainsi que des indications données au cours de l'audience publique du 23 août 2013, que la société Daimler AG a introduit après le début de l'année 2011 auprès du Kraftfahrt-Bundesamt (KBA), autorité compétente en matière de réception désignée par la République Fédérale d'Allemagne, des demandes de réception CE pour des types de véhicules qui répondent à la dénomination commerciale de classe A, classe B, classe CLA et classe SL, pour lesquels le système de climatisation devait utiliser un nouveau gaz, dénommé " R 1234 yf ", dont le potentiel de réchauffement planétaire est inférieur à 150, en lieu et place du gaz dénommé " R 134 a ", utilisé antérieurement et dont le potentiel de réchauffement est de 1 300 ; que le KBA a délivré la réception CE pour ces différents types de véhicules en 2011 et 2012 ; que toutefois, après que des articles de presse eurent mis l'accent sur des risques que pourrait présenter ce nouveau gaz, la société Daimler AG a procédé à de nouveaux tests à compter du mois d'août 2012 ; qu'elle fait valoir que ces tests ont mis en évidence que le gaz " R 1234 yf " se révélerait plus dangereux que le gaz précédemment utilisé, en raison de risques sérieux d'inflammation en cas de choc en conditions de circulation et de dégagements de fluorure d'hydrogène hautement toxiques ; que la société Daimler AG, après avoir informé les autorités allemandes et européennes des résultats de ces tests, a demandé au KBA une modification, sous forme d'extension, des réceptions CE antérieurement délivrées pour les classes A, B, CLA et SL pour équiper ces véhicules avec le gaz " R 134 a " ; que ces extensions ont été accordées par deux décisions du KBA, l'une du 3 juin 2013 ayant pour numéro " e1*2001/116*0470*04 " et concernant les classes A, B et CLA, l'autre du 6 juin 2013 ayant pour numéro " e1*98/14*0169*19 " et concernant la classe SL ;<br/>
<br/>
              8. Considérant qu'en dépit des décisions prises par le KBA les 3 et 6 juin 2013, le ministre de l'écologie, du développement durable et de l'énergie, par une décision non formalisée, a donné instruction, au cours du mois de juin 2013, à l'Organisme technique central français de ne pas délivrer les codes nationaux d'identification du type (CNIT) des véhicules considérés, avec pour conséquence de faire obstacle à l'immatriculation en France de ces véhicules ; que l'exécution de cette décision a été suspendue par une ordonnance du juge des référés du tribunal administratif de Versailles en date du 25 juillet 2013 ; <br/>
<br/>
              9. Considérant que, le 26 juillet 2013, le ministre de l'écologie, du développement durable et de l'énergie a pris une nouvelle décision, de portée équivalente mais fondée sur les dispositions de l'article R. 321-14 du code de la route, traduisant la mise en oeuvre de la clause de sauvegarde prévue par l'article 29 de la directive 2007/46/CE du 5 septembre 2007 ; que cette décision refuse l'immatriculation sur le territoire français des véhicules produits par la société Daimler AG portant les numéros de réception communautaire " e1*98/14*0169*19 " (type 230) du 6 juin 2013, à l'exception de la version SZBBA200, et " e1*2001/116*0470/04 " (type 245G) du 3 juin 2013, à l'exception des versions de la variante Y2GBM2, jusqu'à l'intervention d'une décision de la Commission européenne ou au plus tard pour une durée de six mois ; que cette décision est motivée par la considération que l'extension des réceptions communautaires résultant des décisions du KBA des 3 et 6 juin 2013 constitue un détournement permettant au constructeur de s'exonérer des exigences de la directive 2006/40/CE du 17 mai 2006, en contournant l'application de l'article 5 § 4 de cette directive ainsi que les principes de la réception par type de la directive 2007/46/CE du 5 septembre 2007, que cette opération porte préjudice à l'environnement et aux efforts de réduction des gaz à effet de serre et qu'elle crée une distorsion manifeste de concurrence entre les constructeurs automobiles sur le marché européen ; qu'elle expose que, pour ces raisons, les autorités françaises " souhaitent prendre les mesures correctives nécessaires, afin de préserver les objectifs assignés aux Etats membres par la directive [2006/40/CE] et de mettre un terme à cette infraction " ; que la société Mercedes-Benz France demande la suspension de l'exécution de cette décision du 26 juillet 2013 ;<br/>
<br/>
              Sur les conclusions à fin de suspension :<br/>
<br/>
              10. Considérant qu'en vertu des pouvoirs que lui confère l'article L. 521-1 du code de justice administrative, le juge des référés peut ordonner la suspension de l'exécution d'une décision administrative qui fait parallèlement l'objet d'une requête en annulation ou en réformation s'il estime que deux conditions sont remplies, l'une tenant à l'existence d'une situation d'urgence justifiant l'intervention de mesures provisoires ordonnées en référé dans l'attente du jugement de la requête au fond, l'autre tenant à l'existence d'un moyen propre à créer, en l'état de l'instruction, un doute sérieux quant à la légalité de la décision contestée ; que le juge des référés statue, conformément aux termes de l'article L. 511-1 du même code, dans les meilleurs délais, par des mesures qui présentent un caractère provisoire, sans être saisi du principal ;<br/>
<br/>
              En ce qui concerne la condition relative à la légalité de la décision contestée :<br/>
<br/>
              11. Considérant que l'article R. 321-11 du code de la route prévoit, conformément aux objectifs de la directive 2007/46/CE du 5 septembre 2007, que tout véhicule dont le type a fait l'objet d'une réception CE et qui est muni d'un certificat de conformité valide peut être librement commercialisé et mis en circulation ; que ces dispositions ont pour effet d'interdire aux autorités administratives françaises de faire obstacle à l'immatriculation de tels véhicules pour des motifs tenant au respect des exigences de la directive, sauf à ce que soit légalement mise en oeuvre une clause de sauvegarde prévue par la directive ; que l'article R. 321-14 du code de la route, qui procède à la transposition de la clause de sauvegarde de l'article 29 de la directive, ne permet au ministre chargé des transports de déroger temporairement à la libre commercialisation et à la libre mise en circulation qu'aux seuls motifs qu'il soit établi que les véhicules, systèmes ou équipements en cause, ayant fait l'objet d'une réception CE et accompagnés d'un certificat de conformité, " compromettent gravement la sécurité routière ou nuisent gravement à l'environnement ou à la santé publique " ; <br/>
<br/>
              12. Considérant qu'il en découle que les motifs énoncés par la décision contestée et tirés de ce que l'extension des réceptions CE résultant des décisions du KBA des 3 et 6 juin 2013 constituerait un détournement permettant de contourner les exigences de la directive 2006/40/CE du 17 mai 2006 et les principes de la directive 2007/46/CE du 5 septembre 2007 et qu'elle créerait une distorsion manifeste de concurrence entre les constructeurs automobiles sur le marché européen, justifiant ainsi que les autorités françaises prennent de leur propre initiative et unilatéralement des " mesures correctives " pour " mettre un terme à cette infraction ", sont étrangers aux motifs limitativement énumérés par l'article R. 321-14, lesquels sont seuls susceptibles de justifier des refus d'immatriculation temporaire en application de cette clause de sauvegarde ;<br/>
<br/>
              13. Considérant, il est vrai, que la décision contestée se fonde également sur le motif tiré de ce que la mise en circulation des véhicules en cause " porte préjudice à l'environnement et aux efforts de réduction des gaz à effet de serre " ; qu'à cet égard, le ministre de l'écologie, du développement durable et de l'énergie fait valoir que chaque véhicule équipé avec le gaz " R 134 a " produit, sur l'ensemble de sa durée de vie, 629 kg équivalent CO² de plus que s'il était équipé avec le gaz " R 1234 yf ", que les quelques 4 500 véhicules dont l'immatriculation est actuellement bloquée émettraient ainsi de l'ordre de 2 800 tonnes équivalent CO² sur leur durée de vie, chiffre que le ministre extrapole à 81 000 tonnes équivalent CO² pour la durée de quatre années courant jusqu'au 1er janvier 2017 si les véhicules des types considérés étaient tous mis en circulation avec le gaz " R 134 a " jusqu'en 2017 ; que le ministre soutient, en outre, que la mise en circulation des véhicules considérés conduirait nécessairement les autres constructeurs à entreprendre des démarches similaires et estime alors les émissions supplémentaires, sur quatre années et pour la France, entre 2,8 et 4,6 millions de tonnes équivalent CO² ;<br/>
<br/>
              14. Considérant toutefois qu'il ressort des éléments versés au dossier et des indications données à l'audience, d'une part, que moins de 6 % des nouveaux modèles de véhicules immatriculés en France en 2013 ont été équipés du gaz " R 1234 yf " ; que, sur l'ensemble des véhicules neufs immatriculés en France en 2013, seulement 1,74 % ont été équipés avec ce gaz ; que le nouveau gaz n'équipe qu'une part infime des véhicules actuellement en circulation en France ; que, d'autre part, les véhicules produits par Daimler AG faisant l'objet de la décision contestée ne représentent qu'une très faible part du parc automobile français ; qu'enfin la directive 2006/40/CE du 17 mai 2006 a elle-même prévu un passage progressif à des gaz au potentiel de réchauffement planétaire inférieur à 150, en ne l'imposant qu'aux véhicules neufs et non à l'ensemble du parc automobile et en prévoyant un calendrier étalé sur une longue période, retenant la date du 1er janvier 2011 pour les nouveaux types de véhicules, date de fait repoussée à 2013 par les autorités européennes, et celle du 1er janvier 2017 pour l'ensemble des véhicules neufs ;<br/>
<br/>
              15. Considérant, dans ces conditions, et en l'état de l'instruction, qu'il n'apparaît pas que la mise en circulation en France des véhicules concernés par la décision contestée puisse être regardée comme étant de nature, par elle-même, à nuire gravement à l'environnement au sens de l'article R. 321-14 du code de la route ; qu'il s'ensuit qu'à supposer que le ministre chargé des transports aurait pris la même décision en se fondant uniquement sur le motif tiré de la nuisance grave à l'environnement, le moyen tiré de ce que la décision contestée fait une inexacte application des dispositions de l'article R. 321-14 du code de la route est, à lui seul et sans qu'il soit besoin de se prononcer sur les autres moyens de la requête, de nature, en l'état de l'instruction, à faire sérieusement douter de la légalité de cette décision ; <br/>
<br/>
              En ce qui concerne la condition d'urgence :<br/>
<br/>
              16. Considérant que l'urgence justifie que soit prononcée la suspension d'un acte administratif, lorsque l'exécution de celui-ci porte atteinte, de manière suffisamment grave et immédiate, à un intérêt public, à la situation du requérant ou aux intérêts qu'il entend défendre ; qu'il appartient au juge des référés d'apprécier concrètement, compte tenu des justifications fournies par le requérant, si les effets de l'acte litigieux sont de nature à caractériser une urgence justifiant que, sans attendre le jugement de la requête au fond, l'exécution de la décision soit suspendue ; que l'urgence doit être appréciée objectivement et compte tenu de l'ensemble des circonstances de l'affaire, à la date à laquelle le juge des référés se prononce ;<br/>
<br/>
              17. Considérant que la décision contestée a pour objet et pour effet, pour une durée maximale de six mois, de faire obstacle à l'immatriculation en France des véhicules des classes A, B, CLA et SL produits par la société Daimler AG ayant fait l'objet des extensions de réception CE accordées par les décisions du KBA des 3 et 6 juin 2013 ; qu'il ressort des éléments versés au dossier et des indications données au cours de l'audience de référé que les ventes des véhicules des classes A, B et CLA représentaient, pour les six premiers mois de l'année 2013, plus de 60 % des ventes de la société Mercedes-Benz France et de l'ordre de 40 % de son chiffre d'affaires ; que les ventes de ces types de véhicules étaient en croissance jusqu'au blocage des immatriculations au mois de juin 2013 ; qu'il résulte d'un constat établi par un huissier de justice le 5 juillet 2013 qu'à cette date 2 704 véhicules déjà vendus ne pouvaient être immatriculés et ne pouvaient, en conséquence, être livrés à leurs destinataires ; que la société Mercedes-Benz France a été conduite, en raison du blocage des immatriculations, à proposer à ses clients des solutions d'attente ou de remplacement, lui occasionnant de ce fait un préjudice financier direct et des difficultés commerciales ; qu'il n'est pas douteux que la persistance du blocage ne peut qu'exposer la société Mercedes-Benz France ainsi que le réseau de ses distributeurs à des annulation des commandes et à des pertes de ventes et de clients, leur occasionnant ainsi, de manière suffisamment certaine et alors même que la décision contestée n'a qu'une portée temporaire, un grave préjudice commercial, financier et d'image ; <br/>
<br/>
              18. Considérant que si le ministre de l'écologie, du développement durable et de l'énergie fait valoir que la société requérante ne saurait invoquer, pour justifier de l'urgence, une situation dans laquelle elle s'est elle-même placée, il ressort des éléments versés au dossier que la société Mercedes-Benz France et la société Daimler AG peuvent, à la date de la présente ordonnance, se prévaloir de décisions en vigueur prises par le KBA les 3 et 6 juin 2013, sur lesquelles il n'appartient pas au juge des référés du Conseil d'Etat de se prononcer ;<br/>
<br/>
              19. Considérant, enfin, qu'il ne ressort pas des éléments soumis au juge des référés, compte tenu de ce qui a été dit au point 14 ci-dessus, que l'intérêt public qui s'attache à la protection de l'environnement soit de nature, dans les circonstances de l'espèce, à justifier à lui seul le maintien de l'exécution de la décision contestée ;<br/>
<br/>
              20. Considérant, ainsi, que l'atteinte grave et immédiate que l'exécution de la décision contestée porte aux intérêts de la société requérante traduit une situation d'urgence justifiant le prononcé de mesures provisoires en référé ;<br/>
<br/>
              21. Considérant qu'il résulte de tout ce qui précède que la société Mercedes-Benz France est fondée à demander la suspension de l'exécution de la décision du 26 juillet 2013 du ministre de l'écologie, du développement durable et de l'énergie ; <br/>
<br/>
              Sur les conclusions à fin d'injonction :<br/>
<br/>
              22. Considérant que la suspension, décidée par la présente ordonnance, de l'exécution de la décision contestée faisant obstacle à l'immatriculation des véhicules considérés implique nécessairement, à titre provisoire et conservatoire, que les autorités administratives françaises délivrent les éléments nécessaires à l'immatriculation de ces véhicules et garantissent leur libre mise en circulation ; qu'il est ainsi enjoint à titre provisoire et conservatoire au ministre de l'écologie, du développement durable et de l'énergie de délivrer dans les deux jours suivant le prononcé de la présente ordonnance les codes d'identification (CNIT) des types de véhicules en cause afin de permettre leur immatriculation, sans qu'il soit nécessaire en l'état d'assortir cette injonction d'une astreinte ; <br/>
<br/>
              Sur les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative : <br/>
<br/>
              23. Considérant qu'il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de l'Etat, sur le fondement de l'article L. 761-1 du code de justice administrative, la somme de 3 000 euros à verser à la société Mercedes-Benz-France au titre des frais exposés par elle dans le cadre de l'instance de référé et non compris dans les dépens ;<br/>
<br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
------------------<br/>
Article 1er : L'exécution de la décision du 26 juillet 2013 par laquelle le ministre de l'écologie, du développement durable et de l'énergie a refusé d'immatriculer sur le territoire français les véhicules du constructeur Daimler ayant les numéros de réception communautaires e1*98/14*0169*19 (type 230) du 6 juin 2013 à l'exception de la version SZBBA200 et e1*2001/116*0470/04 (type 245G) du 3 juin 2013 à l'exception des versions de la variante Y2GBM2, est suspendue.<br/>
Article 2 : Il est enjoint à titre provisoire au ministre de l'écologie, du développement durable et de l'énergie de délivrer les codes d'identification (CNIT) des types de véhicules visés par la décision du 26 juillet 2013 afin de permettre leur immatriculation en France dans les deux jours suivant le prononcé de la présente ordonnance. <br/>
Article 3 : L'Etat versera à la société Mercedes-Benz-France une somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Le surplus des conclusions de la requête est rejeté.<br/>
Article 5 : La présente ordonnance sera notifiée à la société Mercedes-Benz France et au ministre de l'écologie, du développement durable et de l'énergie.<br/>
<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
