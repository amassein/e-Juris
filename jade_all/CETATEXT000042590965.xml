<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000042590965</ID>
<ANCIEN_ID>JG_L_2020_11_000000443967</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/42/59/09/CETATEXT000042590965.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 8ème - 3ème chambres réunies, 30/11/2020, 443967, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2020-11-30</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>443967</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>8ème - 3ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>CABINET BRIARD</AVOCATS>
<RAPPORTEUR>M. Charles-Emmanuel Airy</RAPPORTEUR>
<COMMISSAIRE_GVT>Mme Karin Ciavaldini</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2020:443967.20201130</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Par deux mémoires, enregistrés les 11 septembre et 20 octobre 2020 au secrétariat du contentieux du Conseil d'Etat, présentés en application de l'article 23-5 de l'ordonnance n° 58-1067 du 7 novembre 1958, la société à responsabilité limitée (SARL) Financière La Rotonde demande au Conseil d'Etat, à l'appui de son pourvoi contre l'arrêt n° 19PA02576 du 10 juillet 2020, par lequel la cour administrative d'appel de Paris a, sur l'appel du ministre de l'action et des comptes publics, annulé le jugement du 2 avril 2019 par lequel le tribunal administratif de Paris a prononcé la décharge des cotisations supplémentaires d'impôt sur les sociétés auxquelles elle a été assujettie au titre des exercices clos les 31 mars et 31 décembre 2013, de renvoyer au Conseil constitutionnel la question de la conformité aux droits et libertés garantis par la Constitution du VI de l'article L.16 B du livre des procédures fiscales.<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - la Constitution, notamment son Préambule et son article 61-1 ;<br/>
              - l'ordonnance n° 58-1067 du 7 novembre 1958 ;<br/>
              - le livre des procédures fiscales, notamment son article L. 16 B ; <br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Charles-Emmanuel Airy, auditeur,  <br/>
<br/>
              - les conclusions de Mme Karin Ciavaldini, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, au Cabinet Briard, avocat de la société Financière La Rotonde ;<br/>
<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Aux termes du premier alinéa de l'article 23-5 de l'ordonnance du 7 novembre 1958 portant loi organique sur le Conseil constitutionnel : " Le moyen tiré de ce qu'une disposition législative porte atteinte aux droits et libertés garantis par la Constitution peut être soulevé, y compris pour la première fois en cassation, à l'occasion d'une instance devant le Conseil d'Etat (...) ". Il résulte des dispositions de ce même article que le Conseil constitutionnel est saisi de la question prioritaire de constitutionnalité à la triple condition que la disposition contestée soit applicable au litige ou à la procédure, qu'elle n'ait pas déjà été déclarée conforme à la Constitution dans les motifs et le dispositif d'une décision du Conseil constitutionnel, sauf changement des circonstances, et que la question soit nouvelle ou présente un caractère sérieux.<br/>
<br/>
              2. Aux termes du VI de l'article L. 16 B du livre des procédures fiscales, dans sa rédaction, applicable aux opérations de vérification conduites à l'encontre de la société Financière La Rotonde, issue de l'article 11 de la loi du 29 décembre 2012 de finances rectificative pour 2012 : " L'administration des impôts ne peut opposer au contribuable les informations recueillies, y compris celles qui procèdent des traitements mentionnés au troisième alinéa, qu'après restitution des pièces et documents saisis ou de leur reproduction et mise en oeuvre des procédures de contrôle visées aux premier et deuxième alinéas de l'article L. 47. / (...) En présence d'une comptabilité tenue au moyen de systèmes informatisés saisie dans les conditions prévues au présent article, l'administration communique au contribuable, au plus tard lors de l'envoi de la proposition de rectification prévue au premier alinéa de l'article L. 57 ou de la notification prévue à l'article L. 76, sous forme dématérialisée ou non au choix de ce dernier, la nature et le résultat des traitements informatiques réalisés sur cette saisie qui concourent à des rehaussements, sans que ces traitements ne constituent le début d'une procédure de vérification de comptabilité. Le contribuable est informé des noms et adresses administratives des agents par qui, et sous le contrôle desquels, les opérations sont réalisées. "<br/>
<br/>
              3. La société Financière La Rotonde soutient que les dispositions du VI de l'article L. 16 B du livre des procédures fiscales méconnaissent les principes des droits de la défense, du droit à un procès équitable et, ainsi, la garantie des droits découlant de l'article 16 de la Déclaration des droits de l'homme et du citoyen de 1789 en ce que, en méconnaissance de la compétence que le législateur tire de l'article 34 de la Constitution, elles n'assortissent pas de garanties suffisantes, au profit du contribuable, la faculté laissée à l'administration de procéder à des traitements informatiques sur une comptabilité tenue au moyen de systèmes informatisés saisie lors d'une visite domiciliaire autorisée par ordonnance du juge judiciaire, en tant qu'elles ne prévoient la faculté pour le contribuable ni d'engager un débat contradictoire, oral ou écrit, sur les traitements informatiques réalisés par l'administration ainsi que sur leurs conséquences fiscale, ni de soumettre ces traitements à un ou plusieurs experts indépendants afin de permettre au juge de l'impôt d'exercer un contrôle effectif sur ces traitements.<br/>
<br/>
              4. Toutefois, les principes constitutionnels du respect des droits de la défense et du droit à un procès équitable ne s'appliquent pas aux décisions émanant des autorités administratives, sauf lorsqu'elles prononcent une sanction ayant le caractère d'une punition. Par suite, ces principes ne sauraient être utilement invoqués à l'encontre des dispositions contestées en tant qu'elles régissent la procédure administrative d'établissement de l'impôt. En tant que cette procédure peut conduire à l'application de pénalités, le cas échéant, par l'administration fiscale, ces dispositions instituent des garanties spécifiques au profit du contribuable vérifié lorsque l'administration mène des investigations sur une comptabilité tenue au moyen de systèmes informatisés. Ces garanties s'ajoutent sans s'y substituer à celles qui s'attachent à toute vérification de comptabilité, lesquelles permettent notamment au contribuable de se faire assister d'un tiers de son choix, d'engager un dialogue contradictoire avec le vérificateur ainsi que de contester les modalités des traitements effectués par l'administration et leurs conséquences fiscales auprès de cette dernière. La mise en oeuvre des dispositions contestées n'est pas de nature à faire obstacle à l'exercice, par le juge de l'impôt, d'un contrôle effectif sur les traitements informatiques en cause ainsi que leurs conséquences fiscales. Dans ces conditions, les dispositions législatives contestées ne sauraient méconnaître les principes du respect des droits de la défense et du droit à un procès équitable. Il en résulte que la question soulevée, qui n'est pas nouvelle, ne présente pas un caractère sérieux.<br/>
<br/>
              5. Par suite, il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité invoquée.<br/>
<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Il n'y a pas lieu de renvoyer au Conseil constitutionnel la question prioritaire de constitutionnalité soulevée par la société Financière La Rotonde. <br/>
Article 2 : La présente décision sera notifiée à la société à responsabilité limitée Financière La Rotonde et au ministre de l'économie, des finances et de la relance. <br/>
Copie en sera adressée au Conseil constitutionnel et au Premier ministre.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
