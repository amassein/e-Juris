<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000038444237</ID>
<ANCIEN_ID>JG_L_2019_05_000000412342</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/38/44/42/CETATEXT000038444237.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème chambre, 06/05/2019, 412342, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2019-05-06</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>412342</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème chambre</FORMATION>
<TYPE_REC>Contentieux des pensions</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP BOUZIDI, BOUHANNA</AVOCATS>
<RAPPORTEUR>M. Marc Pichon de Vendeuil</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Gilles Pellissier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHS:2019:412342.20190506</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              Mme B...D...a demandé au tribunal administratif de Poitiers le bénéfice d'une pension de réversion du chef de son mari décédé. Par un jugement n° 1502364 du 4 mai 2017, le tribunal administratif de Poitiers a rejeté sa demande.<br/>
<br/>
              Par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 10 juillet et 11 octobre 2017 au secrétariat du contentieux du Conseil d'Etat, Mme D...demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler ce jugement ;<br/>
<br/>
              2°) réglant l'affaire au fond, de faire droit à sa demande ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 3 000 euros à verser à la SCP Bouzidi Bouhanna, avocat de MmeD..., au titre des dispositions des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991.<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le code civil ;<br/>
              - le code des pensions civiles et militaires de retraite ;<br/>
              - la loi n° 91-647 du 10 juillet 1991 ; <br/>
              - la loi n° 2010-1657 du 29 décembre 2010 ;<br/>
              - le décret n° 2010-1691 du 30 décembre 2010 ;<br/>
              - le code de justice administrative.<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Marc Pichon de Vendeuil, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Gilles Pellissier, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Bouzidi, Bouhanna, avocat de Mme D...;<br/>
<br/>
<br/>
<br/>Considérant ce qui suit : <br/>
<br/>
              1. Il ressort des pièces du dossier soumis au juge du fond que M. C... D..., ressortissant marocain, a été rayé des contrôles de l'armée active le 17 février 1950 et qu'il a obtenu une pension militaire de retraite proportionnelle. Il est décédé le 1er mars 2010. MmeD..., néeA..., a demandé au tribunal administratif de Poitiers l'annulation de la décision en date du 30 juillet 2015 rejetant sa demande de pension de réversion présentée le 4 décembre 2013 ainsi que le bénéfice d'une telle pension. Par un jugement du 4 mai 2017, le tribunal administratif de Poitiers a rejeté sa demande. <br/>
<br/>
              2. Aux termes de l'article 211 de la loi du 29 décembre 2010 de finances pour 2011, applicable aux demandes de pension de réversion : " I. - Les pensions militaires d'invalidité, les pensions civiles et militaires de retraite et les retraites du combattant servies aux ressortissants des pays ou territoires ayant appartenu à l'Union française ou à la Communauté ou ayant été placés sous le protectorat ou sous la tutelle de la France sont calculées dans les conditions prévues aux paragraphes suivants. (...) / V. - Les demandes de pensions présentées en application du présent article sont instruites dans les conditions prévues par le code des pensions militaires d'invalidité et des victimes de la guerre et par le code des pensions civiles et militaires de retraite. (...) / VIII. - Un décret fixe les modalités d'application du présent article, notamment les mesures d'information des bénéficiaires ainsi que les modalités de présentation et d'instruction des demandes mentionnées aux III, IV et V. "<br/>
<br/>
              3. Aux termes de l'article L. 39 du code des pensions civiles et militaires de retraite, rendu applicable à MmeD..., ayant cause d'un militaire, par l'article L. 47 du même code : " Le droit à pension de veuve est subordonné à la condition : a) si le mari a obtenu ou pouvait obtenir une pension accordée dans le cas [de cessation d'activité pour départ en retraite], que depuis la date du mariage jusqu'à la cessation de l'activité du mari, celui-ci ait accompli deux années au moins de services valables pour la retraite, sauf si un ou plusieurs enfants sont issus du mariage antérieur à ladite cessation ; b) si le mari a obtenu ou pouvait obtenir une pension accordée dans le cas [de radiation pour invalidité], que le mariage soit antérieur à l'événement qui a amené la mise à la retraite ou la mort du mari. (...) Nonobstant les conditions d'antériorité prévues ci-dessus, le droit à pension de veuve est reconnu : 1° Si un ou plusieurs enfants sont issus du mariage ; 2° Ou si le mariage, antérieur ou postérieur à la cessation de l'activité, a duré au moins quatre années. ".<br/>
<br/>
              4. Aux termes, enfin, de l'article 3 du décret du 30 décembre 2010, pris pour l'application des dispositions précitées de la loi de finances pour 2011 : " Un arrêté conjoint des ministres chargés de la défense, des affaires étrangères, des anciens combattants et du budget énumère les pièces justificatives à produire à l'appui de toute demande visée à l'article 1er ". L'annexe 3 de l'arrêté du 30 décembre 2010 cite, parmi les pièces exigées pour une demande de pension d'un ayant cause, " l'acte de mariage mentionnant la date de transcription sur les registres d'état-civil ". Aux termes de l'article 46 du code civil : " Lorsqu'il n'aura pas existé de registres, ou qu'ils seront perdus, la preuve en sera reçue tant par titres que par témoins ; et, dans ces cas, les mariages, naissances et décès pourront être prouvés tant par les registres et papiers émanés des pères et mères décédés, que par témoins ".  L'article 47 du code civil dispose : " Tout acte de l'état civil des Français et des étrangers fait en pays étranger et rédigé dans les formes usitées dans ce pays fait foi, sauf si d'autres actes ou pièces détenus, des données extérieures ou des éléments tirés de l'acte lui-même établissent, le cas échéant après toutes vérifications utiles, que cet acte est irrégulier, falsifié ou que les faits qui y sont déclarés ne correspondent pas à la réalité ".<br/>
<br/>
              5. Il ressort des pièces du dossier soumis au juge du fond que, pour justifier de son mariage, Mme D...a produit au ministre de la défense un jugement rendu le 20 janvier 2011 par le tribunal de première instance d'Azilal (Maroc), qui fait état d'une union depuis l'année 1952 et jusqu'au décès de son époux, survenu le 1er mars 2010. Toutefois, ce jugement n'a pas été transcrit sur un registre d'état-civil et Mme D...ne soutient pas qu'une telle transcription n'aurait pas été possible. Par suite, le tribunal administratif de Poitiers n'a pas commis d'erreur de droit en considérant que MmeD..., faute de produire l'inscription de son mariage sur un registre d'état-civil, n'en apportait pas la preuve.<br/>
<br/>
              6. Il résulte de ce qui précède que Mme D...n'est pas fondée à demander l'annulation du jugement attaqué. <br/>
<br/>
              7. Les dispositions de l'article L. 761-1 du code de justice administrative font obstacle à ce qu'une somme soit mise à ce titre à la charge de l'Etat, qui n'est pas, dans la présente instance, la partie perdante.<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de Mme D...est rejeté.<br/>
Article 2 : La présente décision sera notifiée à Mme B...D...et à la ministre des armées.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
