<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000029255209</ID>
<ANCIEN_ID>JG_L_2014_07_000000369736</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/29/25/52/CETATEXT000029255209.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème et 8ème sous-sections réunies, 16/07/2014, 369736, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-07-16</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>369736</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème et 8ème sous-sections réunies</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS/>
<RAPPORTEUR>M. Christian Fournier</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Vincent Daumas</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:369736.20140716</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête, enregistrée le 27 juin 2013 au secrétariat du contentieux du Conseil d'Etat, présentée par la communauté de communes du Val de Sèvre, dont le siège est 34 rue du Prieuré à Azay Le Brûlé (79400), représentée par son président ; la communauté de communes du Val de Sèvre demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler les dispositions des circulaires n° COT/B/12/05598/C du 15 mars 2012 et n° INTB1309069C du 5 avril 2013 du ministre chargé des collectivités territoriales en ce qu'elles réitèrent les dispositions des paragraphes 1.2.4.2 et 1.2.4.3 de l'article 77 de la loi n° 2009-1673 du 30 décembre 2009 de finances pour 2010 et prévoient que ces dispositions s'appliquent aux années postérieures à 2011 ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 5 000 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu le code général des collectivités territoriales ;<br/>
<br/>
              Vu loi n° 2009-1673 du 30 décembre 2009 de finances pour 2010 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Christian Fournier, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Vincent Daumas, rapporteur public ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant que, par les circulaires des 15 mars 2012 et 5 avril 2013, le ministre de l'intérieur a indiqué aux préfets les modalités de répartition de la dotation de compensation des établissements publics de coopération intercommunale (EPCI) au titre des années 2012 et 2013 ;<br/>
<br/>
              Sur la fin de non-recevoir opposée par le ministre de l'intérieur :<br/>
<br/>
              2. Considérant que l'interprétation que, par voie de circulaires ou d'instructions, l'autorité administrative donne des lois et règlements qu'elle a pour mission de mettre en oeuvre n'est pas susceptible d'être déférée au juge de l'excès de pouvoir lorsque, étant dénuée de tout caractère impératif, elle ne saurait, quel qu'en soit le bien-fondé, faire grief ; qu'en revanche, les dispositions impératives à caractère général d'une circulaire ou d'une instruction doivent être regardées comme faisant grief ; <br/>
<br/>
              3. Considérant que les circulaires attaquées des 15 mars 2012 et 5 avril 2013 prévoient, notamment, que la dotation de compensation des EPCI au titre respectivement des années 2012 et 2013 doit être minorée du produit de la taxe sur les surfaces commerciales (TASCOM) perçu par l'Etat sur le territoire de la collectivité en 2010 et que si le montant de la dotation de compensation est insuffisant pour assurer la minoration dans sa totalité, le solde est prélevé sur les recettes fiscales directes de la collectivité ; que, si le ministre de l'intérieur soutient que les circulaires se bornent à résumer les modalités de répartition de la dotation de compensation des EPCI prévues par le législateur et que, par suite, elle ne sont pas susceptibles d'être déférées au juge de l'excès de pouvoir, ce moyen doit être écarté dès lors que ces circulaires présentent un caractère impératif ; qu'elles sont donc susceptibles de faire l'objet d'un recours pour excès de pouvoir ;<br/>
<br/>
              Sur les conclusions dirigées contre la circulaire du 15 mars 2012 :<br/>
<br/>
              4. Considérant, d'une part, qu'aux termes de l'article R. 421-1 du code de justice administrative : " Sauf en matière de travaux publics, la juridiction ne peut être saisie que par voie de recours formé contre une décision, et ce, dans les deux mois à partir de la notification ou de la publication de la décision attaquée " ; que, d'autre part, la publication d'une circulaire dans un recueil autre que le Journal officiel fait courir le délai du recours contentieux à l'égard de tous les tiers si l'obligation de publier cette circulaire dans le recueil résulte d'un texte législatif ou règlementaire lui-même publié au Journal officiel de la République française ; qu'en l'absence d'une telle obligation, cet effet n'est attaché à la publication que si le recueil peut, eu égard à l'ampleur et aux modalités de sa diffusion, être aisément consultable par toutes les personnes susceptibles d'avoir un intérêt leur donnant qualité pour contester la circulaire ; qu'aux termes de l'article 29 du décret du 30 décembre 2005 relatif à la liberté d'accès aux documents administratifs et à la réutilisation des informations publiques, pris pour l'application de la loi du 17 juillet 1978, les directives, instructions, circulaires, ainsi que les notes et réponses ministérielles qui comportent une interprétation du droit positif ou une description des procédures administratives sont " publiés dans des bulletins ayant une périodicité au moins trimestrielle et comportant dans leur titre la mention "Bulletin officiel" / Des arrêtés ministériels déterminent, pour chaque administration, le titre exact du ou des bulletins la concernant, la matière couverte par ce ou ces bulletins ainsi que le lieu ou le site internet où le public peut les consulter ou s'en procurer copie " ; <br/>
<br/>
              5. Considérant que l'arrêté ministériel du 11 septembre 1980, publié au Journal officiel du 24 septembre 1980, prévoit la publication au bulletin officiel du ministère de l'intérieur (BOMI) des directives, instructions, circulaires, notes et réponses ministérielles émanant du ministère de l'intérieur et des secrétaires d'Etat placés sous son autorité et précise le lieu où le public peut les consulter ou s'en procurer copie ; qu'il ressort de l'instruction que la circulaire du 15 mars 2012 a été publiée au BOMI n° 2012-03 paru le 24 septembre 2012 ; que le délai de recours contentieux ouvert à son encontre courait jusqu'au lundi 26 novembre 2012 ; que, par suite, la requête de la communauté de communes du Val de Sèvre, enregistrée le 27 juin 2013, est tardive et par suite irrecevable en ce qu'elle tend à l'annulation pour excès de pouvoir de la circulaire du 15 mars 2012 ;<br/>
<br/>
              Sur les conclusions dirigées contre la circulaire du 5 avril 2013 :<br/>
<br/>
              6. Considérant, en premier lieu, qu'aux termes du paragraphe 1.2.4.2 de l'article 77 de la loi du 30 décembre 2009 de finances pour 2010 : " Le montant de la compensation prévue au D de l'article 44 de la loi de finances pour 1999 (n° 98-1266 du 30 décembre 1998) ou de la dotation de compensation prévue à l'article L. 5211-28-1 du code général des collectivités territoriales est diminué en 2011 d'un montant égal, pour chaque collectivité territoriale ou établissement public de coopération intercommunale à fiscalité propre, au produit de la taxe sur les surfaces commerciales perçu par l'État en 2010 sur le territoire de la collectivité territoriale ou de l'établissement public de coopération intercommunale " ; qu'aux termes du b) du 2° du paragraphe 1.2.4.3 de l'article 77 de la même loi, l'article L. 2334-7 du code général des collectivités territoriales est ainsi modifié : " Il est ajouté un alinéa ainsi rédigé : "Pour les communes et établissements publics de coopération intercommunale à fiscalité propre, lorsque le montant de la compensation prévue au D de l'article 44 de la loi de finances pour 1999 (n° 98-1266 du 30 décembre 1998) ou de la dotation de compensation prévue à l'article L. 5211-28-1 du présent code est, en 2011, inférieur au montant de la diminution à opérer en application du 1.2.4.2 de l'article 77 de la loi n° 2009-1673 du 30 décembre 2009 de finances pour 2010, le solde est prélevé au profit du budget général de l'Etat, prioritairement sur le montant correspondant aux montants antérieurement perçus au titre du 2° bis du II de l'article 1648 B du code général des impôts dans sa rédaction antérieure à la loi n° 2003-1311 du 30 décembre 2003 de finances pour 2004 et enfin sur le produit de la taxe foncière sur les propriétés bâties, de la taxe foncière sur les propriétés non bâties, de la taxe d'habitation et de la contribution économique territoriale perçu au profit de ces communes et établissements" " ; que si la requérante soutient que la circulaire du 5 avril 2013 méconnaît, en réitérant les dispositions des paragraphes 1.2.4.2 et 1.2.4.3 de l'article 77 de la loi du 30 décembre 2009 de finances pour 2010, le principe de libre administration des collectivités territoriales, ce moyen ne peut en tout état de cause qu'être écarté dès lors que le Conseil constitutionnel, par sa décision n° 2013-355 QPC du 22 novembre 2013, a déclaré ces dispositions conformes à la Constitution ;<br/>
<br/>
              7. Considérant, en second lieu, qu'il résulte des termes mêmes des dispositions citées au point 6 que les mécanismes de diminution et de prélèvement portant sur les dotations et sur les recettes fiscales perçues par les EPCI, mis en place pour compenser le transfert du produit de la taxe sur les surfaces commerciales de l'Etat à ces établissements publics, ne sont applicables qu'au titre de la seule année 2011 ; qu'aucune disposition du code général des collectivités territoriales applicable en 2013, ni aucun autre texte ne prévoit que ces mécanismes s'appliquent aux EPCI au titre de l'année 2013 ; que, par suite, le ministre de l'intérieur a ajouté aux dispositions législatives applicables en indiquant dans sa circulaire du 5 avril 2013 que : " Je vous rappelle en outre que la dotation de compensation des EPCI est minorée depuis 2011 du produit de la taxe sur les surfaces commerciales (TASCOM) perçu par l'Etat sur le territoire de la collectivité en 2010. Si le montant de la dotation est insuffisant pour assurer la minoration dans sa totalité, le solde est prélevé sur les recettes fiscales directes de la collectivité " ; que la communauté de communes du Val de Sèvre est donc fondée à demander l'annulation des dispositions citées ci-dessus de la circulaire du 5 avril 2013 ;<br/>
<br/>
              8. Considérant qu'il résulte de tout ce qui précède que la communauté de communes du Val de Sèvre n'est fondée à demander que l'annulation du passage de la circulaire du 5 avril 2013 cité au point 7 ;<br/>
<br/>
              Sur les conclusions présentées au titre de l'article L. 761-1 du code de justice administrative :<br/>
<br/>
              9. Considérant que, dans les circonstances de l'espèce, il y a lieu de mettre à la charge de l'Etat la somme de 3 000 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Les dispositions suivantes de la circulaire du 5 avril 2013 du ministre de l'intérieur relative à la dotation de compensation des EPCI de la dotation globale de fonctionnement pour l'exercice 2013 sont annulées : " Je vous rappelle en outre que la dotation de compensation des EPCI est minorée depuis 2011 du produit de la taxe sur les surfaces commerciales (TASCOM) perçu par l'Etat sur le territoire de la collectivité en 2010. Si le montant de la dotation est insuffisant pour assurer la minoration dans sa totalité, le solde est prélevé sur les recettes fiscales directes de la collectivité ".<br/>
Article 2 : L'Etat versera à la communauté de communes du Val de Sèvre la somme de 3 000 euros en application de l'article L. 761-1 du code de justice administrative.<br/>
Article 3 : Le surplus des conclusions de la requête est rejeté.<br/>
Article 4 : La présente décision sera notifiée à la communauté de communes du Val de Sèvre et au ministre de l'intérieur.<br/>
Copie sera adressée pour information au ministre des finances et des comptes publics.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
