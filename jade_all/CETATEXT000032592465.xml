<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000032592465</ID>
<ANCIEN_ID>JG_L_2016_05_000000395863</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/32/59/24/CETATEXT000032592465.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 7ème - 2ème chambres réunies, 27/05/2016, 395863, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2016-05-27</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>395863</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>7ème - 2ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DELAPORTE, BRIARD ; SCP NICOLAY, DE LANOUVELLE, HANNOTIN</AVOCATS>
<RAPPORTEUR>M. François Lelièvre</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Gilles Pellissier</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2016:395863.20160527</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              La société Détection Electronique Française et la société Siemens ont demandé au juge du référé précontractuel du tribunal administratif d'Amiens d'annuler, sur le fondement de l'article L. 551-1 du code de justice administrative, la procédure d'appel d'offres lancée par le centre hospitalier Philippe Pinel en vue de la passation du lot n° 2 d'un marché public ayant pour objet la réalisation de prestations de " maintenance des systèmes de sécurité incendie, des équipements de désenfumage et des asservissements associés ". Par deux ordonnances n°s 1503560 et 1503561 du 21 décembre 2015, le juge des référés du tribunal administratif d'Amiens a annulé la procédure au stade de l'analyse des offres.<br/>
<br/>
              1° Sous le n° 395863, par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 4 et 19 janvier 2016 au secrétariat du contentieux du Conseil d'Etat, la société Nord Picardie Maintenance Service demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'ordonnance n° 1503560 ;<br/>
<br/>
              2°) statuant en référé, de rejeter la demande de la société Détection Electronique Française ;<br/>
<br/>
              3°) de mettre à la charge de la société Détection Electronique Française la somme de 3 500 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              2° Sous le n° 395864, par un pourvoi sommaire et un mémoire complémentaire, enregistrés les 4 et 19 janvier 2016 au secrétariat du contentieux du Conseil d'Etat, la société Nord Picardie Maintenance Service demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'ordonnance n° 1503561 ;<br/>
<br/>
              2°) statuant en référé, de rejeter la demande de la société Siemens ;<br/>
<br/>
              3°) de mettre à la charge de la société Siemens la somme de 3 500 euros au titre des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
			....................................................................................<br/>
<br/>
              Vu les autres pièces des dossiers ;<br/>
<br/>
              Vu :<br/>
              - le code des marchés publics ;<br/>
              - le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. François Lelièvre, maître des requêtes,  <br/>
<br/>
              - les conclusions de M. Gilles Pellissier, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Delaporte, Briard, avocat de la société Nord Picardie Maintenance Service et à la SCP Nicolaÿ, de Lanouvelle, Hannotin, avocat de la société Détection Electronique Française ; <br/>
<br/>
<br/>
<br/>1. Considérant que les pourvois n°s 395863 et 395864 sont relatifs à la passation du même marché public lancé par le centre hospitalier Philippe Pinel ayant pour objet la réalisation de prestations de " maintenance des systèmes de sécurité incendie, des équipements de désenfumage et des asservissements associés " ; qu'il y a lieu de les joindre pour qu'ils fassent l'objet d'une seule décision ;<br/>
<br/>
              2. Considérant qu'aux termes de l'article L. 551-1 du code de justice administrative : " Le président du tribunal administratif, ou le magistrat qu'il délègue, peut être saisi en cas de manquement aux obligations de publicité et de mise en concurrence auxquelles est soumise la passation par les pouvoirs adjudicateurs de contrats administratifs ayant pour objet l'exécution de travaux, la livraison de fournitures ou la prestation de services, avec une contrepartie économique constituée par un prix ou un droit d'exploitation, ou la délégation d'un service public (...) " ;<br/>
<br/>
              3. Considérant, d'une part, qu'aux termes de l'article 4.2 du cahier des clauses techniques particulières applicables au marché en litige : " Le titulaire assure 24 heures/24 et 7 jours/7 un service d'astreinte chargé de prendre les mesures en cas d'incident sur les installations, signalé par un appel téléphonique du responsable des services techniques. / ... / Les délais d'intervention de l'astreinte sont comme suit : temps d'intervention : 4 heures ; temps de dépannage : 8 heures : temps de réparation : 72 heures " ; que, d'autre part, l'article 6.2 du règlement de la consultation, relatif au jugement des offres, prévoit que le service d'astreinte technique fera l'objet d'une note sur 5, coefficient 2, et sera apprécié au regard du " délai d'intervention ", de la " présentation fonctionnelle du service " et de la " composition et qualification du personnel d'astreinte " ;<br/>
<br/>
              4. Considérant qu'en estimant que ces stipulations faisaient obstacle à ce que les candidats au marché puissent proposer un délai d'intervention inférieur à la durée de quatre heures qu'elles mentionnent, le juge du référé précontractuel du tribunal administratif d'Amiens a entaché ses ordonnances de dénaturation ; que, par suite, la société Nord Picardie Maintenance est fondée, sans qu'il soit besoin d'examiner les autres moyens des pourvois, à demander l'annulation des ordonnances attaquées ;<br/>
<br/>
              5. Considérant qu'il y a lieu, en application des dispositions de l'article L. 821-2 du code de justice administrative, de régler l'affaire au titre des procédures de référé engagées ;<br/>
<br/>
              6. Considérant qu'il résulte de l'instruction et qu'il n'est pas contesté que le marché en litige a été signé ; que, par suite, les demandes tendant à l'annulation de la procédure d'appel d'offres présentées par les sociétés Détection Electronique Française et Siemens sont devenues sans objet ;<br/>
<br/>
              7. Considérant qu'il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées par les sociétés Détection Electronique Française et Siemens au titre de l'article L. 761-1 du code de justice administrative ; qu'en revanche, il y a lieu, dans les circonstances de l'espèce, de mettre à la charge de ces deux sociétés le versement d'une somme de 2 000 euros chacune à la société Nord Picardie Maintenance Service au titre des frais exposés par celle-ci devant le Conseil d'Etat et le tribunal administratif d'Amiens ainsi que d'une somme de 1 000 euros chacune au centre hospitalier Philippe Pinel au titre des frais exposés par celui-ci devant le tribunal administratif d'Amiens ;<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Les ordonnances n°s 1503560 et 1503561 du 21 décembre 2015 du juge des référés du tribunal administratif d'Amiens sont annulées.<br/>
Article 2 : Il n'y a pas lieu de statuer sur les demandes tendant à l'annulation de la procédure d'appel d'offres présentées par les sociétés Siemens et Détection Electronique Française devant le juge des référés du tribunal administratif d'Amiens.<br/>
Article 3 : Les sociétés Siemens et Détection Electronique Française verseront chacune la somme de 2 000 euros à la société Nord Picardie Maintenance Service et une somme de 1 000 euros chacune au centre hospitalier Philippe Pinel en application des dispositions de l'article L. 761-1 du code de justice administrative.<br/>
Article 4 : Les conclusions présentées par les sociétés Siemens et Détection Electronique Française au titre des dispositions de l'article L. 761-1 du code de justice administrative sont rejetées.<br/>
Article 5 : La présente décision sera notifiée à la société Nord Picardie Maintenance Service, au centre hospitalier Philippe Pinel, à la société Siemens et à la société Détection Electronique Française.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
