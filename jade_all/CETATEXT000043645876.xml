<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000043645876</ID>
<ANCIEN_ID>JG_L_2021_06_000000440383</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/43/64/58/CETATEXT000043645876.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 10ème - 9ème chambres réunies, 10/06/2021, 440383</TITRE>
<DATE_DEC>2021-06-10</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>440383</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>10ème - 9ème chambres réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>B</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP FOUSSARD, FROGER ; SCP THOUVENIN, COUDRAY, GREVY</AVOCATS>
<RAPPORTEUR>M. Arno Klarsfeld</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Laurent Domingo</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CECHR:2021:440383.20210610</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
         M. B... A... a demandé à la Cour nationale du droit d'asile, d'une part, d'annuler la décision du 23 novembre 2018 par laquelle l'Office français de protection des réfugiés et apatrides (OFPRA) a mis fin, sur le fondement du 2° de l'article L. 711-6 du code de l'entrée et du séjour des étrangers et du droit d'asile, au statut de réfugié dont il bénéficiait et, d'autre part, de le rétablir dans ce statut. Par une décision n° 18058463 du 2 mars 2020 la Cour nationale du droit d'asile a fait droit à sa demande. <br/>
<br/>
         Par un pourvoi sommaire, un mémoire complémentaire et un mémoire en réplique, enregistrés les 4 mai et 31 juillet 2020 et le 21 mai 2021 au secrétariat du contentieux du Conseil d'Etat, l'Office français de protection des réfugiés et apatrides demande au Conseil d'Etat d'annuler cette décision.<br/>
<br/>
<br/>
<br/>
         Vu les autres pièces du dossier ;<br/>
<br/>
         Vu :<br/>
         - la convention de Genève du 28 juillet 1951 relative au statut des réfugiés et le protocole signé à New York le 31 janvier 1967 relatif au statut des réfugiés ;<br/>
         - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
         - la loi n° 91-647 10 juillet 1991 ;<br/>
         - le code de justice administrative ;<br/>
<br/>
<br/>
         Après avoir entendu en séance publique :<br/>
<br/>
         - le rapport de M. Arno Klarsfeld, Conseiller d'Etat,  <br/>
<br/>
         - les conclusions de M. Laurent Domingo, rapporteur public ;<br/>
<br/>
         La parole ayant été donnée, après les conclusions, à la SCP Foussard, Froger, avocat de l'OFPRA et à la SCP Thouvenin, Coudray, Grevy, avocat de M. B... A... ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Il ressort des pièces du dossier soumis à la Cour nationale du droit d'asile que M. A..., de nationalité afghane, s'est vu reconnaître la qualité de réfugié par une décision de l'Office français de protection des réfugiés et apatrides (OFPRA) du 5 mai 2010. M. A... a été condamné par le tribunal correctionnel de Paris, par un jugement devenu définitif du 27 novembre 2013, à une peine de quatre ans d'emprisonnement pour aide à l'entrée, à la circulation ou au séjour irréguliers d'un étranger en France ou dans un Etat partie à la convention de Schengen, en bande organisée, et pour participation à association de malfaiteurs en vue de la préparation d'un délit puni de dix ans d'emprisonnement, ainsi, à titre complémentaire, qu'à une interdiction du territoire français pour une durée de dix ans. Par une décision du 23 novembre 2018, prise sur le fondement du 2° de l'article L. 711-6 du code de l'entrée et du séjour des étrangers et du droit d'asile, l'OFPRA a mis fin au statut de réfugié dont M. A... bénéficiait depuis le 5 mai 2010, au motif que la présence en France de l'intéressé constituait une menace grave pour la société. Par une décision du 2 mars 2020, contre laquelle l'OFPRA se pourvoit en cassation, la Cour nationale du droit d'asile a annulé cette décision et a rétabli à M. A... le bénéfice du statut de réfugié.<br/>
<br/>
              Sur les conclusions dirigées contre la décision attaquée en tant qu'elle se prononce sur la non-application à M. A... de la clause d'exclusion prévue par le c) du F de l'article 1er de la convention de Genève :<br/>
<br/>
              2. Le 2° du A de l'article 1er de la convention de Genève du 28 juillet 1951 stipule que doit être considérée comme réfugiée toute personne qui : " craignant avec raison d'être persécutée du fait de sa race, de sa religion, de sa nationalité, de son appartenance à un certain groupe social ou de ses opinions politiques, se trouve hors du pays dont elle a la nationalité et qui ne peut, ou, du fait de cette crainte, ne veut se réclamer de la protection de ce pays ". Aux termes du F de cet article : " Les dispositions de cette Convention ne seront pas applicables aux personnes dont on aura des raisons sérieuses de penser : (...) c) qu'elles se sont rendues coupables d'agissements contraires aux buts et aux principes des Nations unies ". Aux termes de l'article L. 711-4 du code de l'entrée et du séjour des étrangers et du droit d'asile, dans sa rédaction alors applicable : " L'office met également fin à tout moment, de sa propre initiative ou à la demande de l'autorité administrative, au statut de réfugié lorsque [...] / 3° Le réfugié doit, compte tenu de circonstances intervenues après la reconnaissance de cette qualité, en être exclu en application des sections D, E ou F de l'article 1er de la convention de Genève, du 28 juillet 1951 (...) ". Constituent des agissements contraires aux buts et aux principes des Nations unies ceux qui sont susceptibles d'affecter la paix et la sécurité internationale, les relations pacifiques entre Etats ainsi que les violations graves des droits de l'homme. <br/>
<br/>
              3. Pour rejeter les conclusions de l'OFPRA tendant à ce qu'elle fasse application à M. A... de la clause d'exclusion prévue par le c) du F de l'article 1er de la convention de Genève, la Cour nationale du droit d'asile a retenu qu'il ne résultait pas de l'instruction devant elle que les agissements du requérant aient présenté une gravité suffisante pour être regardés comme des violations graves des droits de l'homme ayant porté atteinte aux buts et principes des Nations-Unies. En statuant ainsi sur la situation de M. A..., la cour, qui n'a pas exclu par principe que les agissements d'une personne impliquée dans un réseau d'immigration clandestine puissent constituer des violations graves des droits de l'homme, a suffisamment motivé sa décision et n'a pas inexactement qualifié les faits de l'espèce.<br/>
<br/>
              Sur les conclusions dirigées contre la décision attaquée en tant qu'elle se prononce sur la décision de l'OFPRA de mettre fin au statut de réfugié de M. A... :<br/>
<br/>
              4. L'article L. 711-6 du code de l'entrée et du séjour des étrangers et du droit d'asile, dans sa rédaction applicable au litige, dispose que : " Le statut de réfugié peut être refusé ou il peut être mis fin à ce statut lorsque : (...) 2° La personne concernée a été condamnée en dernier ressort en France soit pour un crime, soit pour un délit constituant un acte de terrorisme ou puni de dix ans d'emprisonnement, et sa présence constitue une menace grave pour la société ". Il résulte de ces dispositions que la possibilité de refuser le statut de réfugié ou d'y mettre fin, qui est sans incidence sur le fait que l'intéressé a ou conserve la qualité de réfugié dès lors qu'il en remplit les conditions, est subordonnée à deux conditions cumulatives. Il appartient à l'OFPRA et, en cas de recours, à la Cour nationale du droit d'asile, d'une part, de vérifier si l'intéressé a fait l'objet de l'une des condamnations que visent les dispositions précitées et, d'autre part, d'apprécier si sa présence sur le territoire français est de nature à constituer, à la date de leur décision, une menace grave pour la société au sens des dispositions précitées, c'est-à-dire si elle est de nature à affecter un intérêt fondamental de la société, compte tenu des infractions pénales commises - lesquelles ne sauraient, à elles seules, justifier légalement une décision refusant le statut de réfugié ou y mettant fin - et des circonstances dans lesquelles elles ont été commises, mais aussi du temps qui s'est écoulé et de l'ensemble du comportement de l'intéressé depuis la commission des infractions ainsi que de toutes les circonstances pertinentes à la date à laquelle ils statuent.<br/>
<br/>
              5. Pour annuler la décision de l'OFPRA, la Cour nationale du droit d'asile, après avoir relevé que la première des deux conditions fixées par le 2° de l'article L. 711-6, tenant à l'existence d'une condamnation en dernier ressort pour un délit puni de dix ans d'emprisonnement, était en l'espèce remplie, a estimé que la présence en France de M. A... ne constituait pas une menace grave pour la société, dès lors qu'il avait apparemment eu un comportement exemplaire en détention, comme en attestait le fait qu'il avait bénéficié de dix-sept mois de remise de peine sur quarante-huit, qu'il n'existait pas d'éléments laissant supposer qu'il continuait d'entretenir des liens avec ses anciens complices, qu'il ne s'était pas fait défavorablement remarquer depuis sa libération en janvier 2015, qu'il vivait désormais avec son épouse, dont il avait eu un enfant, et qu'il avait démontré une stabilité professionnelle et affective et une volonté avérée d'intégration au sein de la société française. <br/>
<br/>
              6. Si, ainsi qu'il a été dit au point 4, les infractions pénales commises par un réfugié ne sauraient, à elles seules, justifier légalement une décision mettant fin au statut de réfugié, il appartient à l'OFPRA et, en cas de recours, à la Cour nationale du droit d'asile, d'examiner la gravité de la menace que constitue la présence de l'intéressé en France en tenant compte, parmi d'autres éléments, de la nature des infractions commises, des atteintes aux intérêts fondamentaux de la société auxquels la réitération de ces infractions exposerait celle-ci et du risque d'une telle réitération. La seule circonstance qu'un réfugié, condamné pour des faits qui, lorsqu'ils ont été commis, établissaient que sa présence constituait une menace grave pour la société, se soit abstenu, postérieurement à sa libération, de tout comportement répréhensible, n'implique pas, par elle-même, du moins avant l'expiration d'un certain délai, et en l'absence de tout autre élément positif significatif en ce sens, que cette menace ait disparue. En l'espèce, il ressort des pièces du dossier soumis à la cour que M. A... a été condamné pour son implication dans l'organisation d'un réseau d'immigration clandestine à destination de divers pays européens dont il était un des principaux instigateurs, et était, au demeurant, à la date de la décision attaquée, toujours sous le coup d'une interdiction judiciaire du territoire français d'une durée de dix ans. S'il a affirmé avoir cessé tout lien avec les membres de son réseau et n'a pas attiré l'attention des autorités depuis sa libération, ces circonstances, non plus que sa situation familiale, le fait qu'il exerce une activité professionnelle en tant qu'intérimaire et son apprentissage de la langue française, ne permettent de tenir pour acquis que sa présence en France ne constituait plus, à la date de la décision attaquée, une menace grave pour la société française. L'OFPRA est, par suite, fondé à soutenir que la Cour nationale du droit d'asile a inexactement qualifié les faits qui lui étaient soumis.<br/>
<br/>
              7. Il résulte de ce qui précède que l'OFPRA est fondé à demander l'annulation de la décision de la Cour nationale du droit d'asile qu'il attaque en tant qu'elle annule sa décision du 23 novembre 2018 et rétabli le bénéfice du statut de réfugié à l'intéressé. <br/>
<br/>
              8. Il n'y a pas lieu, dans les circonstances de l'espèce, de faire droit aux conclusions présentées en défense au titre de l'article 37 de la loi du 10 juillet 1991. <br/>
<br/>
<br/>
<br/>
<br/>
D E C I D E :<br/>
--------------<br/>
Article 1er : Les articles 1er et 2 de la décision de la Cour nationale du droit d'asile du 2 mars 2020 sont annulés. <br/>
Article 2 : L'affaire est, dans cette mesure, renvoyée à la Cour nationale du droit d'asile.<br/>
Article 3 : Le surplus des conclusions du pourvoi et les conclusions présentées au titre des articles L. 761-1 du code de justice administrative et 37 de la loi du 10 juillet 1991 sont rejetés.<br/>
Article 4 : La présente décision sera notifiée à l'Office français de protection des réfugiés et apatrides à M. B... A....<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE>
<SCT ID="8A" TYPE="PRINCIPAL">095-04 - REFUS OU RÉVOCATION DU STATUT DE RÉFUGIÉ SUR LE FONDEMENT DE L'ARTICLE L. 711-6 DU CESEDA - PRÉSENCE CONSTITUANT UNE MENACE GRAVE POUR LA SOCIÉTÉ - 1) MODALITÉS D'APPRÉCIATION [RJ1] - CAS D'UN RÉFUGIÉ AYANT COMMIS DES INFRACTIONS PÉNALES - 2) ESPÈCE.
</SCT>
<ANA ID="9A"> 095-04 1) Les infractions pénales commises par un réfugié ne sauraient, à elles seules, justifier légalement une décision mettant fin au statut de réfugié. Il appartient à l'Office français de protection des réfugiés et (OFPRA) et, en cas de recours, à la Cour nationale du droit d'asile (CNDA), d'examiner la gravité de la menace que constitue la présence de l'intéressé en France en tenant compte, parmi d'autres éléments, de la nature des infractions commises, des atteintes aux intérêts fondamentaux de la société auxquels la réitération de ces infractions exposerait celle-ci et du risque d'une telle réitération.... ,,La seule circonstance qu'un réfugié, condamné pour des faits qui, lorsqu'ils ont été commis, établissaient que sa présence constituait une menace grave pour la société, se soit abstenu, postérieurement à sa libération, de tout comportement répréhensible, n'implique pas, par elle-même, du moins avant l'expiration d'un certain délai, et en l'absence de tout autre élément positif significatif en ce sens, que cette menace ait disparue.... ,,2) Intéressé ayant été condamné pour son implication dans l'organisation d'un réseau d'immigration clandestine à destination de divers pays européens dont il était un des principaux instigateurs, et étant, au demeurant, à la date de la décision attaquée, toujours sous le coup d'une interdiction judiciaire du territoire français d'une durée de dix ans.... ,,S'il a affirmé avoir cessé tout lien avec les membres de son réseau et n'a pas attiré l'attention des autorités depuis sa libération, ces circonstances, non plus que sa situation familiale, le fait qu'il exerce une activité professionnelle en tant qu'intérimaire et son apprentissage de la langue française, ne permettent de tenir pour acquis que sa présence en France ne constituait plus, à la date de la décision attaquée, une menace grave pour la société française.</ANA>
</SOMMAIRE>

<CITATION_JP>
<CONTENU>[RJ1] Cf., pour les modalités d'appréciation générales d'une telle menace, CE, 19 juin 2020, Office français de protection des réfugiés et apatrides c/ M.,, n° 428140, T. p. 610.</CONTENU>
</CITATION_JP>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
