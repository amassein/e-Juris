<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028376582</ID>
<ANCIEN_ID>JG_L_2013_12_000000353821</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/37/65/CETATEXT000028376582.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État,  4ème sous-section jugeant seule, 23/12/2013, 353821, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2013-12-23</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>353821</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION> 4ème sous-section jugeant seule</FORMATION>
<TYPE_REC>Excès de pouvoir</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP TIFFREAU, CORLAY, MARLANGE</AVOCATS>
<RAPPORTEUR>Mme Florence Chaltiel-Terral</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Rémi Keller</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESJS:2013:353821.20131223</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la requête enregistrée le 2 novembre 2011 au secrétariat du contentieux du Conseil d'Etat, présentée par la SCI Elomidel, dont le siège est Rue Jules-Ferry, Zac des Sablons à Montmagny (95600), représentée par son président directeur général en exercice ; la SCI Elomidel demande au Conseil d'Etat : <br/>
<br/>
              1°) d'annuler pour excès de pouvoir la décision n° 867 T, 870 T, 871 T, 873 T, 881 T, 884 T, 885 T et 902 T du 28 juillet 2011 par laquelle la Commission nationale d'aménagement commercial a refusé de lui accorder l'autorisation préalable en vue de procéder à la création d'un ensemble commercial de 2 750 m² de surface totale de vente, comportant un supermarché de 2 000 m² à l'enseigne "Intermarché", un espace culturel de 500 m² et une galerie commerciale de 250 m², à Eaubonne (Val d'Oise) ; <br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 000 euros au titre de l'article L. 761-1 du code de justice administrative ; <br/>
<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la note en délibéré, enregistrée le 10 décembre 2013, présentée par la SCI Elomidel ;<br/>
<br/>
              Vu le code de commerce ;<br/>
<br/>
              Vu la loi n° 73-1193 du 27 décembre 1973 ;<br/>
<br/>
              Vu la loi n° 2008-776 du 4 août 2008 ;<br/>
<br/>
              Vu le décret n° 2011-921 du 1er août 2011 ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de Mme Florence Chaltiel-Terral, Maître des Requêtes en service extraordinaire,  <br/>
<br/>
              - les conclusions de M. Rémi Keller, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à la SCP Tiffreau, Corlay, Marlange, avocat de la commune de Soisy-sous-Montmorency ;<br/>
<br/>
<br/>
<br/>
<br/>Sur la légalité de la décision attaquée :<br/>
<br/>
              En ce qui concerne le moyen tiré de l'incompétence de la commission nationale : <br/>
<br/>
              1. Considérant que si la société requérante soutient que la Commission nationale d'aménagement commercial n'était pas compétente pour prendre, le 28 juillet 2011, une décision annulant la décision de la commission départementale du Val d'Oise et refusant d'autoriser son projet dès lors qu'elle avait laissé naître, le 26 juillet 2011, une décision implicite ayant autorisé son projet, le délai de quatre mois prévu à l'article L. 752-17 du code de commerce imparti à la commission nationale pour statuer n'est pas imparti à peine de dessaisissement ; que la commission nationale pouvait donc retirer la décision implicite née de son silence, à condition qu'elle soit illégale, dans le délai de deux mois à compter de son intervention ; que, dès lors, le moyen tiré de ce que la décision du 28 juillet 2011 aurait été incompétemment prise doit être écarté ;<br/>
<br/>
              En ce qui concerne les avis des ministres intéressés : <br/>
<br/>
              2. Considérant qu'il ressort des pièces du dossier que les avis des ministres chargés du commerce, de l'urbanisme et de l'environnement ont été présentés à la commission nationale et qu'ils ont été signés par les personnes dûment habilitées à le faire ; que, dès lors, les moyens tirés d'une absence d'avis des ministres intéressés et d'un défaut de signature de ces avis par des personnes habilitées manquent en fait et doivent être écartés ; <br/>
<br/>
<br/>
              En ce qui concerne l'appréciation de la commission nationale : <br/>
<br/>
              3. Considérant qu'il appartient aux commissions d'aménagement commercial, lorsqu'elles se prononcent sur un projet d'exploitation commerciale soumis à autorisation en application de l'article L. 752-1 du code de commerce, d'apprécier la conformité de ce projet aux objectifs prévus à l'article 1er de la loi du 27 décembre 1973 et à l'article L. 750-1 du code de commerce, au vu des critères d'évaluation mentionnés à l'article L. 752-6 du même code ; que l'autorisation ne peut être refusée que si, eu égard à ses effets, le projet compromet la réalisation de ces objectifs ;<br/>
<br/>
              4. Considérant que pour refuser d'autoriser le projet de la SCI Elomidel, la commission nationale a estimé que sa réalisation d'une part compromettrait l'objectif d'aménagement du territoire dans la mesure où il ne contribuerait pas à l'animation de la vie de la commune d'implantation et conduirait à une augmentation du trafic automobile faute pour le site d'implantation d'être desservi par les transports en commun ou les modes de transports doux et, d'autre part, compromettrait l'objectif de développement durable en contribuant à l'artificialisation des sols et à l'étalement urbain ;<br/>
<br/>
              5. Considérant qu'il ressort des pièces du dossier, et notamment de l'avis des ministres intéressés, que le projet en cause serait de nature à rééquilibrer l'offre commerciale dans un secteur où elle était peu développée ; qu'en outre, des arrêts d'autobus se trouvent à proximité du site d'implantation, qui est accessible par les piétons ; qu'enfin l'accroissement du trafic automobile est susceptible d'être absorbé par les voies existantes ; qu'ainsi le motif tiré de ce que le projet compromettrait l'objectif d'aménagement du territoire est erroné et ne saurait, par suite, être légalement retenu pour fonder la décision attaquée ; <br/>
<br/>
              6. Considérant toutefois qu'il ressort des pièces du dossier que le terrain d'assiette du projet est constitué d'une zone boisée reliant deux secteurs forestiers protégés dans lequel la faune et la flore se sont développées, et que la réalisation du projet, qui impliquerait notamment l'abattage de nombreux arbres, contribuerait à l'artificialisation des sols et à l'étalement urbain et mettrait en péril les nombreuses espèces protégées sur le site ; que, par suite, le moyen tiré de ce que la commission nationale aurait commis une erreur d'appréciation en estimant que le projet compromettait l'objectif de développement durable doit être écarté ;<br/>
<br/>
              7. Considérant qu'il résulte de l'instruction que la commission nationale aurait pris la même décision si elle ne s'était fondée que sur le second motif qui, à lui seul, était de nature à justifier la décision prise ; <br/>
<br/>
              8. Considérant qu'il résulte de ce qui précède, et sans qu'il soit besoin de statuer sur la fin de non-recevoir opposée à la requête, que la SCI Elomidel n'est pas fondée à demander l'annulation de la décision attaquée ; <br/>
<br/>
              Sur les conclusions tendant à l'application des dispositions de l'article L. 761-1 du code de justice administrative : <br/>
<br/>
              9. Considérant que ces dispositions font obstacle à ce que soit mise à ce titre à la charge de l'Etat qui n'est pas, dans la présente instance, la partie perdante, la somme que demande la SCI Elomidel ; qu'il y a lieu, en revanche, en application de ces dispositions, de mettre à la charge de la SCI Elomidel le versement de 1 000 euros à l'association du centre commercial des deux cèdres, à l'association des commerçants du centre d'Eaubonne, à l'association Val d'Oise Environnement, à la société Atac et à la commune de Soisy-sous-Montmorency ; <br/>
<br/>
<br/>
<br/>
<br/>			D E C I D E :<br/>
              			--------------<br/>
Article 1er : La requête de la SCI Elomidel est rejetée. <br/>
Article 2 : La SCI Elomidel versera la somme de 1 000 euros à l'association du centre commercial des deux cèdres, à l'association des commerçants du centre d'Eaubonne, à l'association Val d'Oise Environnement, à la société Atac et à la commune de Soisy-sous-Montmorency.  <br/>
Article 3 : La présente décision sera notifiée à la SCI Elomidel, à l'association des commerçants du centre commercial des deux cèdres, à l'association des commerçants du centre d'Eaubonne, à l'association Val d'Oise Environnement, à la commune de Soisy-sous-Montmorency et à la Commission nationale d'aménagement commercial.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
