<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000036566153</ID>
<ANCIEN_ID>JG_L_2018_01_000000417251</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/36/56/61/CETATEXT000036566153.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, Juge des référés, 26/01/2018, 417251, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2018-01-26</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>417251</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>Juge des référés</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>SCP DE NERVO, POUPET</AVOCATS>
<RAPPORTEUR/>
<COMMISSAIRE_GVT/>
<ECLI>ECLI:FR:CEORD:2018:417251.20180126</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu la procédure suivante :<br/>
<br/>
              M. A...B...a demandé au juge des référés du tribunal administratif de Nice, statuant sur le fondement de l'article L. 521-2 du code de justice administrative, d'enjoindre au préfet des Alpes-Maritimes d'enregistrer sa demande d'asile et de lui délivrer une attestation de demandeur d'asile dans un délai de huit jours à compter de la notification de l'ordonnance sous astreinte de 200 euros par jour de retard. Par une ordonnance n° 1705438 du 20 décembre 2017, le juge des référés du tribunal administratif de Nice a rejeté cette demande.<br/>
<br/>
              Par une requête et deux mémoires complémentaires, enregistrés les 12, 22 et 24 janvier 2018 au secrétariat du contentieux du Conseil d'Etat, M. B...demande au juge des référés du Conseil d'Etat :<br/>
<br/>
              1°) d'annuler cette ordonnance ;<br/>
<br/>
              2°) de faire droit à sa demande de première instance ;<br/>
<br/>
              3°) de mettre à la charge de l'Etat la somme de 1 000 euros au titre de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
              Il soutient que :<br/>
              - la condition d'urgence est remplie dès lors qu'il se trouve dans une situation de précarité portant atteinte à sa dignité et à son intégrité physique et qu'une réadmission en Italie l'exposerait à des risques de mauvais traitements ; <br/>
              - il est porté une atteinte grave et manifestement illégale à son droit d'asile, dès lors qu'il ne peut être regardé comme ayant été en fuite faute de s'être présenté aux convocations qui ont suivi l'acceptation par l'Italie de sa réadmission dans ce pays et que, par voie de conséquence, la France est désormais compétente pour examiner sa demande d'asile ;<br/>
              - le mémoire produit le 19 janvier 2018 par le ministre de l'intérieur est irrecevable dès lors qu'il a été produit après expiration du délai qui lui avait été imparti pour présenter sa défense.<br/>
<br/>
              Par un mémoire en défense et des observations, enregistrés les 19 et 24 janvier 2018, le ministre d'Etat, ministre de l'intérieur conclut dans, le dernier état de ses écritures, à titre principal, au non-lieu à statuer sur la requête dès lors qu'elle a perdu son objet, M. B...ayant été invité à enregistrer sa demande d'asile le 2 mars 2018 et, à titre subsidiaire, au rejet de la requête. Il soutient que les moyens invoqués ne sont pas fondés. <br/>
<br/>
<br/>
              Après avoir convoqué à une audience publique, d'une part, M.B..., d'autre part, le ministre d'Etat, ministre de l'intérieur ; <br/>
<br/>
              Vu le procès-verbal de l'audience publique du mardi 23 janvier 2018 à 11 heures au cours de laquelle ont été entendus :<br/>
<br/>
              - Me Poupet, avocat au Conseil d'Etat et à la Cour de cassation, avocat de M. B... ;<br/>
              - les représentants du ministre d'Etat, ministre de l'intérieur ;<br/>
              et à l'issue de laquelle le juge des référés a différé la clôture de l'instruction au  mercredi 24 janvier 2018 à 18 heures ;<br/>
<br/>
<br/>
              Vu les autres pièces du dossier ;<br/>
<br/>
              Vu : <br/>
              - le règlement (UE) n° 604/2013 du Parlement européen et du Conseil du 26 juin 2013 ;<br/>
              - le code de l'entrée et du séjour des étrangers et du droit d'asile ;<br/>
              - le code de justice administrative ;<br/>
<br/>
<br/>
<br/>
<br/>
              Considérant ce qui suit :<br/>
<br/>
              1. Aux termes de l'article L. 521-2 du code de justice administrative : " Saisi d'une demande en ce sens justifiée par l'urgence, le juge des référés peut ordonner toutes mesures nécessaires à la sauvegarde d'une liberté fondamentale à laquelle une personne morale de droit public ou un organisme de droit privé chargé de la gestion d'un service public aurait porté, dans l'exercice d'un de ses pouvoirs, une atteinte grave et manifestement illégale. Le juge des référés se prononce dans un délai de quarante-huit heures ". <br/>
<br/>
              2. Le droit constitutionnel d'asile, qui a le caractère d'une liberté fondamentale, a pour corollaire le droit de solliciter le statut de réfugié. S'il implique que l'étranger qui sollicite la reconnaissance de la qualité de réfugié soit, en principe, autorisé à demeurer sur le territoire jusqu'à ce qu'il ait été statué sur sa demande, ce droit s'exerce dans les conditions définies par le code de l'entrée et du séjour des étrangers et du droit d'asile. L'article L. 742-3 de ce code prévoit que l'étranger dont l'examen de la demande d'asile relève de la responsabilité d'un autre Etat peut faire l'objet d'un transfert vers l'Etat qui est responsable de cet examen en application des dispositions du règlement du Parlement européen et du Conseil du 26 juin 2013. Ce transfert peut avoir lieu pendant une période de six mois à compter de l'acceptation de la demande de prise en charge, susceptible d'être portée à douze ou dix-huit mois dans les conditions prévues à l'article 29 de ce règlement si l'intéressé " prend la fuite ", cette notion devant s'entendre comme visant le cas où un ressortissant étranger non admis au séjour se serait soustrait de façon intentionnelle et systématique au contrôle de l'autorité administrative en vue de faire obstacle à une mesure d'éloignement le concernant.<br/>
<br/>
              3. Il résulte de l'instruction et notamment du mémoire en défense qui a été enregistré, avant l'audience, le 19 janvier 2018 à 18h40, et qu'il n'y a pas lieu d'écarter des débats du seul fait que le ministre de l'intérieur avait été invité à présenter ses observations avant 17 heures, que si M.B..., de nationalité marocaine, déclare être entré irrégulièrement sur le territoire français le 7 août 2016, la consultation du fichier " Eurodac " a permis d'établir que ses empreintes digitales avaient été préalablement relevées par les autorités italiennes. Une demande de prise en charge a été adressée à celles-ci, le 6 octobre 2016. Elle a été acceptée par un accord implicite le 6 décembre 2016, date à laquelle a commencé à courir le délai de six mois dont disposaient les autorités françaises, en application de l'article 29 du règlement du Parlement européen et du Conseil du 26 juin 2013, pour procéder à la réadmission de M. B...vers l'Italie, sauf à ce que ce délai soit porté à 18 mois si l'intéressé avait pris la fuite. <br/>
<br/>
              4. Il est constant qu'à la date du 14 novembre 2017 à laquelle M. B...s'est vu opposé, au guichet de la préfecture des Alpes-Maritimes, le refus d'enregistrement de sa demande d'asile qui est à l'origine de la présente demande de référé-liberté, le délai précité de six mois était déjà expiré, nonobstant la prorogation de ce délai à laquelle a donné lieu le recours contentieux formé par l'intéressé contre l'arrêté préfectoral du 21 février 2017 décidant de sa remise aux autorités italiennes en application de l'article L. 531-1 du code de l'entrée et du séjour des étrangers et du droit d'asile. Si en première instance et dans le mémoire en défense présenté en appel, l'administration soutenait que M. B...devait, toutefois, être regardé comme ayant été en fuite, faute de s'être présenté aux convocations dont il a fait l'objet les 11 avril et 26 avril ainsi que 9 mai 2017, elle a admis, dans les observations qu'elle a présentées à l'issue de l'audience, que l'intéressé était présent le 11 avril et que son absence le 9 mai était liée à une hospitalisation et elle a produit le message qu'elle a transmis le 24 janvier 2018 à M. B...l'invitant à enregistrer sa demande d'asile le 2 mars 2018. Le préfet des Alpes-Maritimes, s'étant ainsi reconnu compétent pour examiner cette demande, il ne saurait, désormais, exécuter son arrêté du 21 février 2017 décidant de la remise de l'intéressé aux autorités italiennes. <br/>
<br/>
              5. Dans ces conditions, les conclusions de M. B...tendant à ce qu'il soit enjoint au préfet des Alpes-Maritimes d'enregistrer sa demande d'asile sont privées d'objet. Par ailleurs, dès lors que l'intéressé ne justifie pas de l'urgence qu'il y aurait à ce que, sans attendre le 2 mars 2018, cette demande soit enregistrée et qu'une attestation de demandeur d'asile lui soit délivrée, il n'est pas fondé à se plaindre de ce que le juge des référés du tribunal administratif de Nice a rejeté le surplus de ses conclusions. Dans les circonstances de l'espèce, il y a lieu de mettre à la charge de l'Etat la somme de 1 000 euros en application de l'article L. 761-1 du code de justice administrative.<br/>
<br/>
<br/>
<br/>
<br/>O R D O N N E :<br/>
------------------<br/>
Article 1er : Il n'y a pas lieu de statuer sur les conclusions de M. B...dirigées contre l'article 2 de l'ordonnance du juge des référés du tribunal administratif de Nice du 20 décembre 2017 et tendant à ce que le juge des référés enjoigne au préfet des Alpes-Maritimes d'enregistrer sa demande d'asile.<br/>
Article 2 : L'Etat versera à M. B...une somme de 1 000 euros en application de l'article L. 761- 1 du code de justice administrative.<br/>
Article 3 : Le surplus des conclusions de la requête est rejeté.<br/>
Article 4 : La présente ordonnance sera notifiée à M. A...B...et au ministre d'Etat, ministre de l'intérieur.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
