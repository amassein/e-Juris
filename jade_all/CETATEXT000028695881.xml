<?xml version="1.0" encoding="UTF-8"?>
<TEXTE_JURI_ADMIN>
<META>
<META_COMMUN>
<ID>CETATEXT000028695881</ID>
<ANCIEN_ID>JG_L_2014_03_000000362827</ANCIEN_ID>
<ORIGINE>CETAT</ORIGINE>
<URL>texte/juri/admin/CETA/TEXT/00/00/28/69/58/CETATEXT000028695881.xml</URL>
<NATURE>Texte</NATURE>
</META_COMMUN>
<META_SPEC>
<META_JURI>
<TITRE>Conseil d'État, 3ème et 8ème sous-sections réunies, 06/03/2014, 362827, Inédit au recueil Lebon</TITRE>
<DATE_DEC>2014-03-06</DATE_DEC>
<JURIDICTION>Conseil d'État</JURIDICTION>
<NUMERO>362827</NUMERO>
<SOLUTION/>
</META_JURI>
<META_JURI_ADMIN>

<FORMATION>3ème et 8ème sous-sections réunies</FORMATION>
<TYPE_REC>Plein contentieux</TYPE_REC>
<PUBLI_RECUEIL>C</PUBLI_RECUEIL>
<DEMANDEUR/>
<DEFENDEUR/>
<PRESIDENT/>
<AVOCATS>LE PRADO</AVOCATS>
<RAPPORTEUR>M. Christophe Pourreau</RAPPORTEUR>
<COMMISSAIRE_GVT>M. Vincent Daumas</COMMISSAIRE_GVT>
<ECLI>ECLI:FR:CESSR:2014:362827.20140306</ECLI>
</META_JURI_ADMIN>
</META_SPEC>
</META>
<TEXTE>
<BLOC_TEXTUEL>
<CONTENU>Vu le pourvoi sommaire et le mémoire complémentaire, enregistrés les 17 septembre et 17 décembre 2012 au secrétariat du contentieux du Conseil d'Etat, présentés pour la société Pacoclean.com, dont le siège est ZAC de Munerie, route d'Aoste à Romagnieu (38480), représentée par son gérant ; la société Pacoclean.com demande au Conseil d'Etat :<br/>
<br/>
              1°) d'annuler l'arrêt n° 10LY00564 de la cour administrative d'appel de Lyon du 13 juillet 2012 en tant qu'après avoir dit qu'il n'y avait pas lieu de statuer sur les conclusions de sa requête à concurrence, en matière d'impôt sur les sociétés, d'un déficit reportable de 113 970 euros au titre de l'exercice 2003 et, en matière de taxe sur la valeur ajoutée, d'une même somme de 113 970 euros au titre de la période du 1er janvier 2001 au 31 décembre 2003, il a, d'une part, sur appel incident du ministre du budget, des comptes publics et de la réforme de l'Etat, annulé l'article 2 du jugement n° 0505411-0506019 du tribunal administratif de Grenoble du 22 décembre 2009 et mis à sa charge un rappel de taxe sur la valeur ajoutée d'un montant de 1 806 euros et, d'autre part, rejeté le surplus des conclusions de sa requête tendant à l'annulation de l'article 5 du même jugement rejetant le surplus des conclusions de sa demande de décharge des cotisations supplémentaires d'impôt sur les sociétés au titre de l'exercice 2003 et des rappels de taxe sur la valeur ajoutée au titre de la période du 1er janvier 2001 au 31 décembre 2003 auxquels elle a été assujettie ainsi que des pénalités correspondantes ;<br/>
<br/>
              2°) de mettre à la charge de l'Etat la somme de 3 500 euros au titre de l'article L. 761-1 du code de justice administrative ;<br/>
<br/>
<br/>
	Vu les autres pièces du dossier ;<br/>
<br/>
              Vu la directive n° 77/388/CEE du 17 mai 1977 modifiée ;<br/>
<br/>
              Vu le code général des impôts et le livre des procédures fiscales ;<br/>
<br/>
              Vu le code de justice administrative ;<br/>
<br/>
              Après avoir entendu en séance publique :<br/>
<br/>
              - le rapport de M. Christophe Pourreau, Maître des Requêtes,<br/>
<br/>
              - les conclusions de M. Vincent Daumas, rapporteur public ;<br/>
<br/>
              La parole ayant été donnée, avant et après les conclusions, à Me Le Prado, avocat de la Société Pacoclean.com ;<br/>
<br/>
<br/>
<br/>
<br/>1. Considérant qu'il ressort des pièces du dossier soumis aux juges du fond qu'à l'issue d'une vérification de comptabilité portant sur la période du 1er janvier 2001 au 31 décembre 2003 de la société Pacoclean.com, qui a pour activité la vente de robots culinaires aux professionnels de la restauration, l'administration a notamment remis en cause l'exonération de taxe sur la valeur ajoutée afférente à des livraisons en Allemagne, au cours des exercices 2002 et 2003, de biens commandés par les sociétés Pacatec Vertrieb et Consulting et Gastroneo GmbH, établies en Suisse, au motif que la réalité de ces livraisons intracommunautaires n'était pas établie ; que, le 2 novembre 2005, la société Pacoclean.com a saisi le tribunal administratif de Grenoble d'une demande de décharge des rappels de taxe sur la valeur ajoutée auxquelles elle a été assujettie et des pénalités correspondantes ; que, par un jugement du 22 décembre 2009, le tribunal administratif a déchargé la société des droits, d'un montant de 1 806 euros, et pénalités correspondant à une facture à la société Pacatec Vertrieb et Consulting du 18 décembre 2002 et rejeté le surplus des conclusions de la demande de la société dirigées contre les rappels de taxe sur la valeur ajoutée mis à sa charge ; que, par un arrêt du 13 juillet 2012, contre lequel la société se pourvoit en cassation, la cour administrative d'appel de Lyon, après avoir notamment dit qu'il n'y avait pas lieu de statuer sur les conclusions de la requête de la société à concurrence, en matière de taxe sur la valeur ajoutée, d'une somme de 113 970 euros, d'une part, faisant droit à l'appel incident formé par le ministre chargé du budget, a mis à la charge de la société un  rappel de taxe sur la valeur ajoutée correspondant à la facture litigieuse du 18 décembre 2002 et, d'autre part, a rejeté les conclusions de la requête de la société tendant à l'annulation du jugement du tribunal administratif en tant qu'il a rejeté le surplus des conclusions de sa demande de décharge des rappels de taxe sur la valeur ajoutée auxquels elle a été assujettie ;<br/>
<br/>
              2. Considérant, en premier lieu, qu'aux termes du I de l'article 262 ter du code général des impôts: " Sont exonérés de la taxe sur la valeur ajoutée : / 1° Les livraisons de biens expédiés ou transportés sur le territoire d'un autre Etat membre de la Communauté européenne à destination d'un autre assujetti ou d'une personne morale non assujettie (...) " ; qu'il appartient au juge de l'impôt, au vu de l'instruction et compte tenu du fait que seul le redevable de la taxe sur la valeur ajoutée est en mesure de produire les documents relatifs au transport des biens, lorsqu'il l'a lui-même assuré, ou tout document de nature à justifier leur livraison effective, lorsque le transport a été assuré par l'acquéreur, d'apprécier si la condition de l'exonération de taxe sur la valeur ajoutée tenant à ce que les biens ont été effectivement expédiés ou transportés hors de France par le vendeur, par l'acquéreur ou par un tiers pour leur compte, à destination d'un autre Etat membre est remplie ;<br/>
<br/>
              3. Considérant que la cour administrative d'appel a, d'une part,  jugé, par une appréciation souveraine exempte de dénaturation, qu'aucun des documents présentés par la société Pacoclean.com à l'appui de sa demande de décharge des rappels de taxe sur la valeur ajoutée mis à sa charge, qui consistaient en des bons de commande et des factures à en-tête de la société mentionnant des adresses de livraison en Allemagne, y compris la facture litigieuse du 18 décembre 2002, ainsi qu'en trois courriers d'une société de transport datés des 11 et 20 mai 2005 attestant avoir livré des biens en Allemagne et en des factures de Chronopost, " n'établissait le flux physique des marchandises " vers l'Allemagne ; qu'elle a, d'autre part, relevé, sans dénaturer les pièces du dossier qui lui était soumis, à titre d'éléments complémentaires, qu'aucun de ces documents ne mentionnait le numéro intracommunautaire de taxe sur la valeur ajoutée des sociétés acquéreuses, que les ventes avaient été comptabilisées au crédit du compte 70702 " ventes marchandises export " sans qu'apparaisse la mention " exonération TVA, article 262 ter du code général des impôts " et que les déclarations d'échange de biens avaient été remises " après coup " ; qu'en déduisant de l'ensemble de ses constatations que les livraisons litigieuses ne remplissaient pas les conditions d'exonération de la taxe sur la valeur ajoutée, alors même qu'aucun des éléments complémentaires relevés n'était, par lui-même, de nature à établir l'absence de livraison effective des biens en Allemagne, la cour administrative d'appel, qui n'a pas mis la preuve de la réalité de la livraison à la charge exclusive de la société, n'a pas commis d'erreur de droit ;<br/>
<br/>
              4. Considérant, en second lieu, que la société Pacoclean.com ne peut utilement contester le bien-fondé de l'arrêt attaqué en invoquant, pour la première fois devant le juge de cassation, le moyen tiré de ce qu'à supposer que les biens n'aient pas été livrés en Allemagne, ils l'avaient été en Suisse, de sorte que les livraisons litigieuses auraient en tout état de cause été exonérées de taxe sur la valeur ajoutée ;<br/>
<br/>
              5. Considérant qu'il résulte de tout ce qui précède que la société Pacoclean.com n'est pas fondée à demander l'annulation de l'arrêt attaqué ; que ses conclusions au titre de l'article L. 761-1 du code de justice administrative doivent, par suite, être rejetées ;<br/>
<br/>
<br/>
<br/>
<br/>D E C I D E :<br/>
--------------<br/>
Article 1er : Le pourvoi de la société Pacoclean.com est rejeté.<br/>
Article 2 : La présente décision sera notifiée à la société Pacoclean.com et au ministre de l'économie et des finances.<br/>
<br/>

</CONTENU>
</BLOC_TEXTUEL>
<SOMMAIRE/>

<CITATION_JP/>
</TEXTE>
<LIENS/>
</TEXTE_JURI_ADMIN>
